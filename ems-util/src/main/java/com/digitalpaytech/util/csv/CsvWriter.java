package com.digitalpaytech.util.csv;

import java.io.IOException;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CsvWriter {
	private static final Pattern PATTERN_ESCAPE_CHARS = Pattern.compile("(,|\")", Pattern.COMMENTS);
	private static final String DBLQUOTE = "\"";
	private static final String ESCAPED_DBLQUOTE = "\"\"";
	
	private int expectedColumn = -1;
	private Writer writer;
	
	private int printedColumn;
	
	public CsvWriter(Writer writer, String... headers) throws IOException {
		this(writer, headers.length);
		
		if(headers.length > 0) {
			int i = -1;
			while(++i < headers.length) {
				writeColumn(headers[i]);
			}
			
			writeLine();
		}
	}
	
	public CsvWriter(Writer writer) {
		this(writer, -1);
	}
	
	public CsvWriter(Writer writer, int expectedColumn) {
		this.expectedColumn = expectedColumn;
		this.writer = writer;
		
		this.printedColumn = 0;
	}
	
	public void writeColumn(Number columnData) throws IOException {
		writeColumn((columnData == null) ? "" : columnData.toString());
	}
	
	public void writeColumn(Boolean columnData) throws IOException {
		if(columnData == null) {
			writeColumn("No");
		}
		else {
			writeColumn(columnData.booleanValue());
		}
	}
	
	public void writeColumn(boolean columnData) throws IOException {
		writeColumn((columnData) ? "Yes" : "No");
	}
	
	public void writeColumn() throws IOException {
		writeColumn((String) null);
	}
	
	public void writeColumn(String columnData) throws IOException {
		if(this.printedColumn > 0) {
			writer.write(",");
		}
		
		if(columnData != null) {
			Matcher m = PATTERN_ESCAPE_CHARS.matcher(columnData);
			int previous = 0;
			boolean shouldQuote = m.find();
			if(shouldQuote) {
				writer.write(DBLQUOTE);
				do {
					writer.write(columnData, previous, (m.start() - previous));
					if(DBLQUOTE.equals(m.group(0))) {
						writer.write(ESCAPED_DBLQUOTE);
					}
					else {
						writer.write(columnData, m.start(), m.end() - m.start());
					}
					
					previous = m.end();
				} while(m.find());
			}
			
			writer.write(columnData, previous, columnData.length() - previous);
			if(shouldQuote) {
				writer.write(DBLQUOTE);
			}
		}
		
		++this.printedColumn;
	}
	
	public void writeLine() throws IOException {
		if(this.printedColumn != this.expectedColumn) {
			throw new IllegalStateException("Insufficient CSV's columns !");
		}
		
		writer.write("\r\n");
		
		this.printedColumn = 0;
	}
}
