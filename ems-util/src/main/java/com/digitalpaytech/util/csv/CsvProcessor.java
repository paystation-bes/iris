package com.digitalpaytech.util.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreUtil;

public abstract class CsvProcessor<T> {
    
    public static final String CTXKEY_RESULT = "result";
    public static final String CTXKEY_CUSTOMER = "customer";
    public static final String CTXKEY_CUSTOMER_TIME_ZONE = "customerTimeZone";
    public static final String CTXKEY_IS_LINK = "isLink";
    
    protected static final String DELIMITER = ",";
    protected static Logger logger;
    
    private static final String UNABLE_TO_READ_FILE = "Unable to read file.";
    
    protected final String[] columnHeaders;
    
    protected CsvProcessor(final String... columnHeaders) {
        // initialize logger
        logger = Logger.getLogger(getClass());
        this.columnHeaders = columnHeaders;
    }
    
    /**
     * Process a line. Implement this method to define Csv conversion logic.
     * This method has been modified to not use exception mechanism to provide error indication because it performs better this way for thousand lines of CSV.
     * 
     * @param line
     * @param lineNum
     * @param userArg
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    protected abstract void processLine(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, String line, int lineNum,
        Map<String, Object> context, boolean foundErrors);
    
    /**
     * Create a line in Csv file
     * 
     * @param writer
     * @param data
     * @param lineNumber
     * @throws IOException
     */
    protected abstract void createLine(CsvWriter writer, T data, int lineNumber, Map<String, Object> context) throws IOException;
    
    /**
     * Initialize this CsvProcessor. This can be override to add custom initialization logic.
     * 
     */
    protected void init(final Map<String, Object> context, final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings) {
        
    }
    
    /**
     * Initialize this CsvProcessor. This can be override to add custom initialization logic.
     * 
     */
    protected void initForCreate(final Map<String, Object> context) {
        
    }
    
    /**
     * Process a multipart file. Should only be called by subclass.<br>
     * Each implementation should create their own public function to pares a
     * file.<br>
     * So that userArg format is only known by the subclass itself.
     * 
     * @param multipartFile
     * @param userArg
     *            User arguments that will be passed to processLine()
     * @throws InvalidCSVDataException
     * @throws CryptoException
     * @throws SystemException
     */
    public void processFile(final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings, final MultipartFile multipartFile,
        final Map<String, Object> context, final int maxFileSize) throws InvalidCSVDataException {
        if (multipartFile.getSize() < 1) {
            throw new InvalidCSVDataException("The file is empty");
        }
        if (multipartFile.getSize() > maxFileSize) {
            throw new InvalidCSVDataException("The file size is greater than " + maxFileSize / StandardConstants.BYTES_PER_MEGABYTE + " MB ");
        }
        
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
            processFile(errors, warnings, reader, context);
        } catch (IOException e) {
            throw new InvalidCSVDataException(UNABLE_TO_READ_FILE);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    /**
     * Process a zip file entry. Should only be called by subclass.<br>
     * Each implementation should create their own public function to pares a
     * file.<br>
     * So that userArg format is only known by the subclass itself.
     * 
     * @param zipFile
     * @param zipEntry
     * @param userArg
     *            User arguments that will be passed to processLine()
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    public void processFile(final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings, final ZipFile zipFile,
        final ZipEntry zipEntry, final Map<String, Object> context) throws InvalidCSVDataException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
            processFile(errors, warnings, reader, context);
        } catch (IOException e) {
            throw new InvalidCSVDataException(UNABLE_TO_READ_FILE);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    /**
     * Process all lines
     * 
     * @param reader
     * @param userArg
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    public void processFile(final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings, final BufferedReader reader,
        final Map<String, Object> context) throws InvalidCSVDataException {
        init(context, errors, warnings);
        
        final List<CsvProcessingError> actualErrors = (errors != null) ? errors : new ArrayList<CsvProcessingError>();
        final List<CsvProcessingError> actualWarnings = (warnings != null) ? warnings : new ArrayList<CsvProcessingError>();
        
        int lineNum = 1;
        boolean continueProcess = true;
        try {
            // process each line
            String line = reader.readLine();
            while (continueProcess && (line != null)) {
                final ArrayList<CsvProcessingError> lineErrors = new ArrayList<CsvProcessingError>();
                final ArrayList<CsvProcessingError> lineWarnings = new ArrayList<CsvProcessingError>();
                if (line.trim().length() <= 0) {
                    if (lineNum == 1) {
                        errors.add(new CsvProcessingError("Required Header Data", lineNum, false));
                        continueProcess = false;
                    }
                } else {
                    processLine(lineErrors, lineWarnings, line, lineNum, context, errors.size() > 0);
                    Iterator<CsvProcessingError> itr = lineErrors.iterator();
                    while (itr.hasNext()) {
                        final CsvProcessingError error = itr.next();
                        itr.remove();
                        
                        error.setLineNumber(lineNum);
                        actualErrors.add(error);
                        
                        if (!error.isShouldContinue()) {
                            continueProcess = false;
                        }
                    }
                    
                    itr = lineWarnings.iterator();
                    while (itr.hasNext()) {
                        final CsvProcessingError warning = itr.next();
                        itr.remove();
                        
                        warning.setLineNumber(lineNum);
                        actualWarnings.add(warning);
                    }
                }
                
                lineNum++;
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new InvalidCSVDataException(UNABLE_TO_READ_FILE);
        }
    }
    
    /**
     * Create a Csv Stream from Collection of Objects.
     * 
     * @param writer
     * @param data
     * @param headers
     * @throws IOException
     */
    public void createCsv(final Writer writer, final Collection<T> data, final Map<String, Object> context) throws IOException {
        initForCreate(context);
        
        int lineNum = 2;
        final CsvWriter csvWriter = new CsvWriter(writer, this.columnHeaders);
        for (T t : data) {
            createLine(csvWriter, t, lineNum, context);
            csvWriter.writeLine();
            ++lineNum;
            
            if ((lineNum % 100) == 0) {
                writer.flush();
            }
        }
    }
}
