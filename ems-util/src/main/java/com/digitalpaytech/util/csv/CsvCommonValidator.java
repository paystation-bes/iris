package com.digitalpaytech.util.csv;

import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;

import com.digitalpaytech.util.CommonValidator;

public abstract class CsvCommonValidator<DTO> extends CommonValidator<List<CsvProcessingError>> implements MessageSourceAware {
    private MessageSourceAccessor messageAccessor;
    
    public abstract void validateRow(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, DTO dto, Map<String, Object> context);
    
    public String getMessage(final String code) {
        return this.messageAccessor.getMessage(code);
    }
    
    @Override
    public String getMessage(final String code, final Object... args) {
        return this.messageAccessor.getMessage(code, args);
    }
    
    @Override
    public void setMessageSource(final MessageSource messageSource) {
        this.messageAccessor = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    protected void addError(final List<CsvProcessingError> messageContainer, final String fieldName, final String message) {
        messageContainer.add(new CsvProcessingError(message));
    }
}
