package com.digitalpaytech.util.primitive;

import java.math.BigInteger;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class BigIntegerIntepreter extends PrimitiveIntepreter<BigInteger> {
	@Override
	public BigInteger encode(byte[] data, int offset) {
		BigInteger result = null;
		
		int len = data.length - offset;
		byte[] resultBuffer = new byte[len];
		System.arraycopy(data, offset, resultBuffer, 0, len);
		try {
			result = new BigInteger(resultBuffer);
		}
		catch(NumberFormatException nfe) {
			// DO NOTHING.
		}
		
		return result;
	}

	@Override
	public byte[] decode(BigInteger data) {
		return data.toByteArray();
	}
}
