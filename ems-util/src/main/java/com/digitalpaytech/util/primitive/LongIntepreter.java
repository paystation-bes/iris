package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class LongIntepreter extends PrimitiveIntepreter<Long> {
	@Override
	public Long encode(byte[] data, int offset) {
		Long result = null;
		
		int dataIdx = offset;
		if((data.length - dataIdx) >= 8) {
			long resultBuffer = (data[dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			
			result = resultBuffer;
		}
		
		return result;
	}

	@Override
	public byte[] decode(Long data) {
		byte[] result = new byte[8];
		
		long dataBuffer = data;
		
		result[7] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[6] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[5] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[4] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[3] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[2] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[1] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[0] = (byte) (dataBuffer & MASK_LONG_BYTE);
		
		return result;
	}
}
