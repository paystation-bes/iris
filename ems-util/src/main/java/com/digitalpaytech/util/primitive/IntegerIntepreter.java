package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class IntegerIntepreter extends PrimitiveIntepreter<Integer> {
	@Override
	public Integer encode(byte[] data, int offset) {
		Integer result = null;
		
		int dataIdx = offset;
		if((data.length - dataIdx) >= 4) {
			int resultBuffer = data[dataIdx] & MASK_BYTE;
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			
			result = resultBuffer;
		}
		
		return result;
	}

	@Override
	public byte[] decode(Integer data) {
		byte[] result = new byte[4];
		
		int dataBuffer = data;
		
		result[3] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[2] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[1] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[0] = (byte) (dataBuffer & MASK_BYTE);
		
		return result;
	}
}
