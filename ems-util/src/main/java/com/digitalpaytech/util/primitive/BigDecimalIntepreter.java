package com.digitalpaytech.util.primitive;

import java.math.BigDecimal;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class BigDecimalIntepreter extends PrimitiveIntepreter<BigDecimal> {
	@Override
	public BigDecimal encode(byte[] data, int offset) {
		BigDecimal result = null;
		try {
			result = new BigDecimal(new String(data, offset, data.length - offset));
		}
		catch(NumberFormatException nfe) {
			// DO NOTHING.
		}
		
		return result;
	}

	@Override
	public byte[] decode(BigDecimal data) {
		return data.toString().getBytes();
	}
}
