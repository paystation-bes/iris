package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class DoubleIntepreter extends PrimitiveIntepreter<Double> {
	@Override
	public Double encode(byte[] data, int offset) {
		Double result = null;
		
		int dataIdx = offset;
		if((data.length - dataIdx) >= 8) {
			long resultBuffer = (data[dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_LONG_BYTE);
			
			result = Double.longBitsToDouble(resultBuffer);
		}
		
		return result;
	}

	@Override
	public byte[] decode(Double data) {
		byte[] result = new byte[8];
		
		long dataBuffer = Double.doubleToLongBits(data);
		
		result[7] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[6] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[5] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[4] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[3] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[2] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[1] = (byte) (dataBuffer & MASK_LONG_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[0] = (byte) (dataBuffer & MASK_LONG_BYTE);
		
		return result;
	}
}
