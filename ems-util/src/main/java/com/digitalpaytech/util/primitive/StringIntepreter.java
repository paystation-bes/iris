package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class StringIntepreter extends PrimitiveIntepreter<String> {
	@Override
	public String encode(byte[] data, int offset) {
		return new String(data, offset, data.length - offset);
	}

	@Override
	public byte[] decode(String data) {
		return data.getBytes();
	}
}
