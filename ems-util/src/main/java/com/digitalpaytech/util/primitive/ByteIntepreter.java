package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class ByteIntepreter extends PrimitiveIntepreter<Byte> {
	@Override
	public Byte encode(byte[] data, int offset) {
		return (data.length > offset) ? data[offset] : null;
	}

	@Override
	public byte[] decode(Byte data) {
		return new byte[] { data };
	}
}
