package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class ShortIntepreter extends PrimitiveIntepreter<Short> {
	@Override
	public Short encode(byte[] data, int offset) {
		Short result = null;
		
		int dataIdx = offset;
		if((data.length - dataIdx) >= 2) {
			int resultBuffer = data[dataIdx] & MASK_BYTE;
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			
			result = (short) resultBuffer;
		}
		
		return result;
	}

	@Override
	public byte[] decode(Short data) {
		byte[] result = new byte[2];
		
		int dataBuffer = data;
		
		result[1] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[0] = (byte) (dataBuffer & MASK_BYTE);
		
		return result;
	}
}
