package com.digitalpaytech.util.primitive;

import com.digitalpaytech.util.PrimitiveIntepreter;

public class FloatIntepreter extends PrimitiveIntepreter<Float> {
	@Override
	public Float encode(byte[] data, int offset) {
		Float result = null;
		
		int dataIdx = offset;
		if((data.length - dataIdx) >= 4) {
			int resultBuffer = data[dataIdx] & MASK_BYTE;
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			resultBuffer = (resultBuffer << 8) | (data[++dataIdx] & MASK_BYTE);
			
			result = Float.intBitsToFloat(resultBuffer);
		}
		
		return result;
	}

	@Override
	public byte[] decode(Float data) {
		byte[] result = new byte[4];
		
		int dataBuffer = Float.floatToIntBits(data);
		
		result[3] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[2] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[1] = (byte) (dataBuffer & MASK_BYTE);
		dataBuffer = dataBuffer >>> 8;
		result[0] = (byte) (dataBuffer & MASK_BYTE);
		
		return result;
	}
}
