package com.digitalpaytech.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.exception.InvalidDataException;

//PMD: Utility class has too many public methods and public static final variables
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveClassLength", "PMD.ExcessivePublicCount", "PMD.SimpleDateFormatNeedsLocale", "PMD.CommentSize",
    "PMD.TooManyMethods" })

/**
 * DateUtil.java is way too heavy and often it's confusing to select the correct methods.
 * replaced by {@link #com.digitalpaytech.util.StableDateUtil}
 */
@Deprecated
public final class DateUtil {
    
    public static final String GMT = "GMT";
    public static final String UTC = "UTC";
    public static final String DATE_ONLY_FORMAT = "yyyy-MM-dd";
    public static final String DATE_ONLY_FORMAT_WIDGET = "MMM dd, yyyy";
    public static final String MONTH_ONLY_FORMAT = "yyyy-MM";
    public static final String DATABASE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String WIDGET_LABEL_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
    public static final String WIDGET_BY_HOUR_LABEL_FORMAT = "MMMMM dd, yyyy HH:mm:ss.S";
    public static final String UI_FORMAT = "MM/dd/yyyy HH:mm:ss";
    public static final String UI_FORMAT_GMT = UI_FORMAT + " 'GMT'";
    public static final String COLON_DELIMITED_DATE_FORMAT = "yyyy:MM:dd:HH:mm:ss:z";
    public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss z";
    public static final String DATE_FORMAT = "MMM dd, yyyy h:mm a";
    public static final String DATE_FORMAT_TWO_DIGITS_YEAR = "MMddyy";
    public static final String PANEXP_FORMAT = "MMyy";
    public static final String MM_DD = "MMdd";
    
    public static final String UI_DEFAULT_START_TIME = "00:00:00";
    public static final String UI_DEFAULT_END_TIME = "23:59:59";
    public static final String EXPORT_DATA_DATE_TIME = "MMddyyyy_HHmmss";
    
    public static final String DATE_UI_FORMAT = "MM/dd/yyyy";
    public static final String TIME_UI_FORMAT = "HH:mm";
    public static final String TIME_WITH_AMPM_FORMAT = "h:mm a";
    public static final String TIME_FORMAT_HOUR_MINUTE_SECOND = "HHmmss";
    
    // alert format
    public static final String ALERT_FORMAT = "MMM dd, yyyy HH:mm:z";
    
    // report formats
    public static final String REPORTS_FORMAT = "yyyy-MM-dd-HH:mm:ss";
    public static final String REPORTS_OUTPUT_FORMAT = "MM/dd/yyyy h:mm:ss a z";
    public static final String REPORTS_OUTPUT_FORMAT_NO_TIMEZONE = "MM/dd/yyyy h:mm:ss a";
    public static final String REPORTS_DATE_FORMAT_ON_FOOTER = "MM/dd/yyyy h:mm a z";
    public static final String REPORTS_SHORT_DATE_FORMAT = "MM/dd/yy" + StandardConstants.STRING_EMPTY_SPACE + TIME_WITH_AMPM_FORMAT;
    
    public static final String START_END_DATE_FORMAT = "MM/dd/yyyy hh:mm:ss";
    
    public static final String MIGRATION_DATE_FORMAT = "E MMM dd, yyyy";
    public static final String MIGRATION_DATE_TIME_FORMAT = "E MMM dd, yyyy h:mm a";
    
    // New Linux PS date format
    public static final String UTC_TRANSACTION_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
    public static final String UTC_CPS_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S";
    public static final String FMS_SCHEDULE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    
    public static final long ONE_DAY = 24 * 60 * 60 * 1000;
    
    private static final Logger LOGGER = Logger.getLogger(DateUtil.class);
    
    private static final String DAY = "day";
    private static final String DAYS = "days";
    private static final String HOUR = "hour";
    private static final String HOURS = "hours";
    private static final String MINUTE = "minute";
    private static final String MINUTES = "minutes";
    private static final String SECOND = "second";
    private static final String SECONDS = "seconds";
    private static final String JUST_NOW = "just now";
    private static final String SOON = "soon";
    
    private static final int QUARTER = 4;
    private static final int QUARTER_IN_MINUTES = 15;
    
    private static EasyKeyValuePool<String, DateFormat> dateFormatPool =
            new EasyKeyValuePool<String, DateFormat>(new EasyObjectBuilder<DateFormat>() {
                @Override
                public DateFormat create(final Object pattern) {
                    return new SimpleDateFormat((String) pattern);
                }
            }, StandardConstants.LIMIT_5, StandardConstants.LIMIT_5);
    
    private DateUtil() {
    }
    
    /**
     * This method is incorrectly implemented. It doesn't convert the time to the new time zone, it only switches the time zone label.
     * Use {@link com.digitalpaytech.util.DateUtil.convertTimeZone(String, String, Date)} to convert from one time zone to another.
     */
    @Deprecated
    public static Date changeTimeZone(final Date date, final String timeZone) {
        String timeZoneToUse = timeZone;
        if (timeZoneToUse == null || StandardConstants.STRING_EMPTY_STRING.equals(timeZoneToUse)) {
            timeZoneToUse = GMT;
        }
        
        // Change a date in another timezone
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZoneToUse));
        cal.setTimeInMillis(date.getTime());
        
        final Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        output.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        output.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
        output.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
        output.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
        output.set(Calendar.SECOND, cal.get(Calendar.SECOND));
        output.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));
        return output.getTime();
    }
    
    /**
     * Adjust the timestamp of the {@link java.util.Date} Object in source time
     * zone to the timestamp in the target time zone. Because {@link java.util.Date} has no knowledge of its' timezone (it is simply
     * just a representation of Long timestamp). To correctly manipulate
     * displayed time between UI and database, you need to adjust the timestamp
     * with the time zone's offset milliseconds.
     * 
     * @param targetTimeZone
     *            Target time zone to convert to.
     * @param srcTimeZone
     *            Time zone of the "srcDate".
     * @param srcDate
     *            Source Date Object.
     * @return Date object with its' timestamp adjusted to target time zone.
     */
    public static Date convertTimeZone(final String targetTimeZone, final String srcTimeZone, final Date srcDate) {
        Date result = srcDate;
        if (targetTimeZone != null && srcTimeZone != null && srcDate != null && !targetTimeZone.equalsIgnoreCase(srcTimeZone)) {
            final long timestamp = srcDate.getTime();
            final int timestampOffset =
                    TimeZone.getTimeZone(targetTimeZone).getOffset(timestamp) - TimeZone.getTimeZone(srcTimeZone).getOffset(timestamp);
            
            result = new Date(srcDate.getTime() + timestampOffset);
        }
        
        return result;
    }
    
    /**
     * @deprecated Simple statement as "new Date()" is already returning timestamp in GMT !
     * 
     *             Create {@link java.util.Date} Object for GMT time zone with the current
     *             date time.
     * 
     * @return Date Object with its' timestamp set to now
     */
    public static Date nowGMT() {
        return convertTimeZone(GMT, TimeZone.getDefault().getID(), new Date());
    }
    
    /**
     * @deprecated It is better to just create an instance of Calendar for the needed timezone and reuse that instance.
     * 
     *             Create {@link java.util.Date} Object for GMT time zone using the
     *             specified by int time-part.
     * 
     * @param srcTimeZone
     *            Time zone of the input integer date.
     * @param year
     * @param month
     *            month of the year (start from 0).
     * @param date
     *            date of the month (1-31)
     * @param hourOfDay
     *            hour of day (0-23)
     * @param minute
     *            minute of hour (0-59)
     * @param second
     *            second of minute (0-59)
     * @return Date Object.
     */
    public static Date createGMTDate(final String srcTimeZone, final int year, final int month, final int date, final int hourOfDay, final int minute,
        final int second) {
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(srcTimeZone));
        cal.set(year, month, date, hourOfDay, minute, second);
        
        return convertTimeZone(GMT, srcTimeZone, cal.getTime());
    }
    
    public static String getRelativeTimeString(final Date dateTimeGMT, final String timeZoneId) {
        return getRelativeTimeString(dateTimeGMT, TimeZone.getTimeZone(timeZoneId));
    }
    
    /**
     * A version of getRelativeTimeString that allows the user to disable the generation of "special case"
     * date formatting for times that are in the near future or recent past (such as "in XX minutes" or "just now").
     * 
     * @param useSpecialFormat
     *            : set to true for generation of "in XX minutes"; false to display only the date-time.
     * @return
     */
    public static String getRelativeTimeString(final Date dateTimeGMT, final String timeZoneId, final boolean useSpecialFormat) {
        return getRelativeTimeString(dateTimeGMT, TimeZone.getTimeZone(timeZoneId), useSpecialFormat);
    }
    
    /**
     * A version of getRelativeTimeString that allows the user to disable the generation of "special case"
     * date formatting for times that are in the near future or recent past (such as "in XX minutes" or "just now").
     * 
     * @param useSpecialFormat
     *            : set to true for generation of "in XX minutes" formatting and including the time in the date string;
     *            false to display only the date (no time), and never use special case formatting.
     * @return
     */
    public static String getRelativeTimeString(final Date dateTimeGMT, final TimeZone timeZone, final boolean useSpecialFormat) {
        if (useSpecialFormat) {
            return getRelativeTimeString(dateTimeGMT, timeZone);
        }
        if (dateTimeGMT == null) {
            return WebCoreConstants.EMPTY_STRING;
        }
        
        final String relativeDateTime;
        
        // Date time of activity in GMT
        /*
         * Calendar dateTime = Calendar.getInstance();
         * dateTime.setTime(activityDateGMT);
         */
        
        final DateFormat timeFmt = new SimpleDateFormat(TIME_WITH_AMPM_FORMAT);
        timeFmt.setTimeZone(timeZone);
        
        // Date time of activity in local time zone
        final Calendar lastSeen = Calendar.getInstance(timeZone);
        lastSeen.setTime(dateTimeGMT);
        
        final SimpleDateFormat dateFmt = new SimpleDateFormat(WebCoreConstants.DATE_NO_TIME_FORMAT);
        dateFmt.setTimeZone(timeZone);
        
        relativeDateTime = dateFmt.format(lastSeen.getTime());
        
        return relativeDateTime;
    }
    
    /**
     * Format the datetime according to relative datetime mentioned in SRS. This also convert datetime to the specific timezone.
     * If the different between the datetime and current time is greater than a day, this method will return full date time format.
     * But it will also added "Yesterday" and "Tomorrow" when the difference is less than 2 days.
     * 
     * *** The source code of this method might look specific for activity log because the original purpose is to use this for just that.
     * But this should be used across the project for consistency of how relative date-time are displayed through out the site. ***
     * 
     * @param dateTimeGMT
     *            Date object that need to be format
     * @param targetTimeZone
     *            Timezone of the target datetime String
     * @return Datetime String
     */
    public static String getRelativeTimeString(final Date dateTimeGMT, final TimeZone timeZone) {
        
        if (dateTimeGMT == null) {
            return WebCoreConstants.EMPTY_STRING;
        } else if (dateTimeGMT.getTime() < 1) {
            return WebCoreConstants.N_A_STRING;
        }
        
        final String relativeDateTime;
        
        // Date time of activity in GMT
        /*
         * Calendar dateTime = Calendar.getInstance();
         * dateTime.setTime(activityDateGMT);
         */
        
        final DateFormat timeFmt = new SimpleDateFormat(TIME_WITH_AMPM_FORMAT);
        timeFmt.setTimeZone(timeZone);
        
        // Date time of activity in local time zone
        final Calendar lastSeen = Calendar.getInstance(timeZone);
        lastSeen.setTime(dateTimeGMT);
        
        /*
         * lastSeen.setTimeInMillis(dateTime.getTimeInMillis()); int offset =
         * TimeZone.getTimeZone(timeZone).getRawOffset();
         * lastSeen.add(Calendar.MILLISECOND, offset);
         */
        
        // Difference in milliseconds between activity time and current time
        final Calendar customerCurrent = Calendar.getInstance(timeZone);
        if (lastSeen.get(Calendar.YEAR) == customerCurrent.get(Calendar.YEAR)
            && lastSeen.get(Calendar.DAY_OF_YEAR) == customerCurrent.get(Calendar.DAY_OF_YEAR)) {
            final long diffInMilliseconds = customerCurrent.getTime().getTime() - lastSeen.getTime().getTime();
            final boolean hideHours = Math.abs(diffInMilliseconds) < StandardConstants.HOURS_IN_MILLIS_1;
            
            relativeDateTime = formatRelativeTime(-diffInMilliseconds, true, hideHours, !hideHours);
        } else {
            // Check for Yesterday
            customerCurrent.add(Calendar.DAY_OF_YEAR, -1);
            if (lastSeen.get(Calendar.YEAR) == customerCurrent.get(Calendar.YEAR)
                && lastSeen.get(Calendar.DAY_OF_YEAR) == customerCurrent.get(Calendar.DAY_OF_YEAR)) {
                relativeDateTime = "Yesterday at " + timeFmt.format(lastSeen.getTime());
            } else {
                customerCurrent.add(Calendar.DAY_OF_YEAR, 2);
                if (lastSeen.get(Calendar.YEAR) == customerCurrent.get(Calendar.YEAR)
                    && lastSeen.get(Calendar.DAY_OF_YEAR) == customerCurrent.get(Calendar.DAY_OF_YEAR)) {
                    relativeDateTime = "Tomorrow at " + timeFmt.format(lastSeen.getTime());
                } else {
                    final SimpleDateFormat dateFmt = getDateFormat();
                    dateFmt.setTimeZone(timeZone);
                    
                    relativeDateTime = dateFmt.format(lastSeen.getTime());
                }
            }
        }
        
        return relativeDateTime;
    }
    
    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(DATE_FORMAT);
    }
    
    /**
     * Format the relative time according to SRS with extra features to hide any
     * time-part of the result. If all of the time parts are 0 or hidden, the
     * return value will be "just now" or "soon".
     * 
     * @param diffInMilliseconds
     *            relative time in milliseconds. minus value can be specified
     *            which indicate that the time is in the past.
     * @param hideDays
     *            if true, days will not be displayed in the result and all of
     *            its' remaining milliseconds will be add to the lower priority
     *            time part. For example, if the relative time is
     *            "2 days 1 hour ago", the formatted time will be
     *            "49 hours ago".
     * @param hideHours
     *            if true, hours will not be displayed in the result and all of
     *            its' remaining milliseconds will be add to the lower priority
     *            time part. For example, if the relative time is
     *            "in 1 hours 1 seconds", the formatted time will be
     *            "in 3601 seconds".
     * @param hideMinutes
     *            if true, minutes will not be displayed in the result and all
     *            of its' remaining milliseconds will be add to the lower
     *            priority time part. For example, if the relative time is
     *            "in 1 minutes 15 seconds", the formatted time will be
     *            "in 75 seconds".
     * @param hideSeconds
     *            if true, minutes will not be displayed in the result and all
     *            of its' remaining milliseconds will be ignored.
     * @return Relative time String represents "diffInMilliseconds".
     */
    public static String formatRelativeTime(final long diffInMilliseconds, final boolean hideDays, final boolean hideHours,
        final boolean hideMinutes) {
        final boolean isFuture = diffInMilliseconds > 0;
        boolean isClose = true;
        
        long millis = Math.abs(diffInMilliseconds);
        long days = 0;
        long hours = 0;
        long mins = 0;
        if (!hideDays) {
            days = TimeUnit.MILLISECONDS.toDays(millis);
            if (days > 0) {
                millis -= TimeUnit.DAYS.toMillis(days);
                isClose = false;
            }
        }
        
        if (!hideHours) {
            hours = TimeUnit.MILLISECONDS.toHours(millis);
            if (hours > 0) {
                millis -= TimeUnit.HOURS.toMillis(hours);
                isClose = false;
            }
        }
        
        if (!hideMinutes) {
            mins = TimeUnit.MILLISECONDS.toMinutes(millis);
            if (mins > 0) {
                millis -= TimeUnit.MINUTES.toMillis(mins);
                isClose = false;
            }
        }
        
        String result = null;
        if (isClose) {
            if (isFuture && millis > StandardConstants.SECONDS_IN_MILLIS_10) {
                result = SOON;
            } else {
                result = JUST_NOW;
            }
        } else {
            result = createRelativeTimeString(isFuture, days, hours, mins);
        }
        
        return result;
    }
    
    private static String createRelativeTimeString(final boolean isFuture, final long days, final long hours, final long mins) {
        final StringBuilder resultBuffer = new StringBuilder();
        if (isFuture) {
            resultBuffer.append("in ");
        }
        
        if (days > 0) {
            resultBuffer.append(days);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
            resultBuffer.append(days > 1 ? DAYS : DAY);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
        }
        if (hours > 0) {
            resultBuffer.append(hours);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
            resultBuffer.append(hours > 1 ? HOURS : HOUR);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
        }
        if (mins > 0) {
            resultBuffer.append(mins);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
            resultBuffer.append(mins > 1 ? MINUTES : MINUTE);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
        }
        
        if (!isFuture) {
            resultBuffer.append("ago ");
        }
        return resultBuffer.substring(0, resultBuffer.length() - 1);
    }
    
    public static Date getCurrentGmtDate() {
        return changeTimeZone(new Date(), GMT);
    }
    
    public static String createDateOnlyString(final Date dateToFormat) {
        final DateFormat dateFormat = new SimpleDateFormat(DATE_ONLY_FORMAT);
        
        return dateFormat.format(dateToFormat);
    }
    
    /**
     * If monthFirst, formats to MM/DD/YYYY (used by some UI elements for calendar date selection)
     * ELSE, formats to YYYY-MM-DD
     * 
     * @param dateToFormat
     * @param monthFirst
     * @return
     */
    public static String createDateOnlyString(final Date dateToFormat, final boolean monthFirst) {
        if (!monthFirst) {
            return createDateOnlyString(dateToFormat);
        }
        final DateFormat dateFormat = new SimpleDateFormat(DATE_UI_FORMAT);
        return dateFormat.format(dateToFormat);
    }
    
    public static String createDateOnlyString(final Date dateToFormat, final String timeZone) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_ONLY_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        
        return dateFormat.format(dateToFormat);
    }
    
    public static String createUIDateOnlyString(final Date dateToFormat, final TimeZone timeZone) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_ONLY_FORMAT_WIDGET);
        dateFormat.setTimeZone(timeZone);
        
        return dateFormat.format(dateToFormat);
    }
    
    public static String createMigrationDateOnlyString(final Date dateToFormat, final TimeZone timeZone) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(MIGRATION_DATE_FORMAT);
        dateFormat.setTimeZone(timeZone);
        
        return dateFormat.format(dateToFormat);
    }
    
    public static String createMigrationDateTimeString(final Date dateToFormat, final TimeZone timeZone) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(MIGRATION_DATE_TIME_FORMAT);
        dateFormat.setTimeZone(timeZone);
        
        return dateFormat.format(dateToFormat);
    }
    
    public static Date createDateByMilliseconds(final long milliseconds) {
        final Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(milliseconds);
        return cal.getTime();
    }
    
    public static int createCurrentYYMM() {
        final Calendar cal = Calendar.getInstance();
        // The first month of the year is JANUARY which is 0
        return (cal.get(Calendar.YEAR) - StandardConstants.CONSTANT_2000) * StandardConstants.CONSTANT_100 + cal.get(Calendar.MONTH) + 1;
    }
    
    public static String convertUTCTimeToDateTimeByTZ(final TimeZone timeZone, final Date date, final String format) {
        final DateFormat simpleDateFormat = new SimpleDateFormat(format);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }
    
    public static DateFormat getDefaultDateFormat() {
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
    }
    
    public static DateFormat getShortDateFormat() {
        return new SimpleDateFormat(REPORTS_SHORT_DATE_FORMAT);
    }
    
    public static DateFormat getColonDelimitedDateFormat() {
        return new SimpleDateFormat(COLON_DELIMITED_DATE_FORMAT);
    }
    
    public static DateFormat getDatabaseDateFormat() {
        return new SimpleDateFormat(DATABASE_FORMAT);
    }
    
    public static String createDatabaseDateString(final Date dateToFormat) {
        return createDateString(getDatabaseDateFormat(), dateToFormat);
    }
    
    public static String createColonDelimitedDateString() {
        return createDateString(getColonDelimitedDateFormat(), new Date());
    }
    
    public static String createColonDelimitedDateString(final Date dateToFormat) {
        return createDateString(getColonDelimitedDateFormat(), dateToFormat);
    }
    
    public static String createColonDelimitedDateString(final String timeZone, final Date dateToFormat) {
        return createDateString(getColonDelimitedDateFormat(), timeZone, dateToFormat);
    }
    
    public static String createDateString() {
        return createDateString(getDefaultDateFormat(), new Date());
    }
    
    public static String createDateString(final Date dateToFormat) {
        return createDateString(getDefaultDateFormat(), dateToFormat);
    }
    
    public static String createDateString(final String timeZone, final Date dateToFormat) {
        return createDateString(getDefaultDateFormat(), timeZone, dateToFormat);
    }
    
    public static String createDateString(final DateFormat dateFormat, final Date dateToFormat) {
        return createDateString(dateFormat, TimeZone.getDefault(), dateToFormat);
    }
    
    public static String createDateString(final DateFormat dateFormat, final String timeZone, final Date dateToFormat) {
        return createDateString(dateFormat, TimeZone.getTimeZone(timeZone), dateToFormat);
    }
    
    public static Date createDate(final DateFormat dateFormat, final TimeZone timeZone, final String dateString) throws ParseException {
        dateFormat.setTimeZone(timeZone);
        
        return dateFormat.parse(dateString);
    }
    
    public static String createDateString(final DateFormat dateFormat, final TimeZone timeZone, final Date dateToFormat) {
        if (dateToFormat == null) {
            return "null";
        }
        
        dateFormat.setTimeZone(timeZone);
        
        return dateFormat.format(dateToFormat);
    }
    
    public static String createDateString(final String dateFormat, final TimeZone timeZone, final Date dateToFormat) {
        final PooledResource<DateFormat> fmt = takeDateFormat(dateFormat, timeZone);
        final String result = fmt.get().format(dateToFormat);
        fmt.close();
        
        return result;
    }
    
    public static Date convertFromReportOutputFormatWithNoTimeZoneDateString(final String dateString) throws InvalidDataException {
        try {
            final DateFormat dateFormat = new SimpleDateFormat(REPORTS_OUTPUT_FORMAT_NO_TIMEZONE);
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Error: Unable to Parse \"" + REPORTS_OUTPUT_FORMAT_NO_TIMEZONE + "\" formatted String: " + dateString, e);
        }
    }
    
    public static Date convertFromColonDelimitedDateString(final String dateString) throws InvalidDataException {
        try {
            final DateFormat dateFormat = getColonDelimitedDateFormat();
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Error: Unable to Parse Colon Delimited Date String: " + dateString, e);
        }
    }
    
    public static Calendar convertFromColonDelimitedDateStringToCalendar(final String dateString) throws InvalidDataException {
        final Calendar cal = new GregorianCalendar();
        cal.setTime(convertFromColonDelimitedDateString(dateString));
        return cal;
    }
    
    /**
     * This method convert any time zone <b>Date</b> to GMT <b>Date</b>
     * 
     * @param dateLocal
     * @param localTimeZone
     * @return Date in GMT
     */
    public static Date getDatetimeInGMT(final Date dateLocal, final String localTimeZone) {
        final TimeZone timezone = TimeZone.getTimeZone(localTimeZone);
        Date newDate = null;
        if (timezone.inDaylightTime(dateLocal)) {
            newDate = new Date(dateLocal.getTime() - timezone.getRawOffset() - timezone.getDSTSavings());
        } else {
            newDate = new Date(dateLocal.getTime() - timezone.getRawOffset());
        }
        return newDate;
    }
    
    public static Date addDaysToCurrentDatetimeInGMT(final int days) {
        return DateUtils.addDays(getDatetimeInGMT(new Date(), TimeZone.getDefault().getDisplayName()), days);
    }
    
    public static String getAlertDateString(final Date dateToFormat, final TimeZone timezone) {
        final SimpleDateFormat alertFormat = new SimpleDateFormat(ALERT_FORMAT);
        alertFormat.setTimeZone(timezone);
        return alertFormat.format(dateToFormat);
    }
    
    /**
     * Use this function when the time is meant to be "user timezone", and is
     * stored in database using GMT.
     * 
     * @param timeZone
     *            the target time zone.
     * @param date
     *            date object that needs to be converted.
     * @return String date-time using target time zone.
     */
    public static String convertTimeZoneAndCreateDateString(final String targetTimeZone, final Date date) {
        final DateFormat dateFormat = getDefaultDateFormat();
        dateFormat.setTimeZone(TimeZone.getTimeZone(targetTimeZone));
        return dateFormat.format(date);
    }
    
    public static String getFileNameTimestamp(final String timeFormat, final String timeZone) {
        final Date now = new Date();
        return getFileNameTimestamp(now, timeFormat, timeZone);
    }
    
    public static String getFileNameTimestamp(final Date date, final String timeFormat, final String timeZone) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(timeFormat);
        final TimeZone zone = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(zone);
        
        return dateFormat.format(date);
    }
    
    public static Date createCurrentWebServiceCallLogDate(final String timeZone) {
        
        final Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone(timeZone));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
    }
    
    public static Date createUTCStartOfMonthDate(final Date specialDate, final String timeZoneStr) {
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneStr);
        final Calendar calendar = new GregorianCalendar(timeZone);
        calendar.setTime(specialDate);
        final Date startDate = createStartOfMonthDate(calendar);
        return convertDateToUTC(startDate, timeZone);
    }
    
    public static Date convertDateToUTC(final Date date, final TimeZone timeZone) {
        final Calendar local = new GregorianCalendar(timeZone);
        local.setTime(date);
        return new Date(local.getTimeInMillis());
    }
    
    private static Date createStartOfMonthDate(final Calendar timeZoneSpecificCalendar) {
        timeZoneSpecificCalendar.set(Calendar.DAY_OF_MONTH, 1);
        timeZoneSpecificCalendar.set(Calendar.HOUR_OF_DAY, 0);
        timeZoneSpecificCalendar.set(Calendar.MINUTE, 0);
        timeZoneSpecificCalendar.set(Calendar.SECOND, 0);
        timeZoneSpecificCalendar.set(Calendar.MILLISECOND, 0);
        
        return timeZoneSpecificCalendar.getTime();
    }
    
    public static String[] addMinIfMissing(final String... hrMin) {
        String[] returnedHrMin = hrMin;
        if (returnedHrMin.length == 1) {
            final List<String> list = new ArrayList<String>(Arrays.asList(returnedHrMin));
            list.add(StandardConstants.STRING_ZERO);
            returnedHrMin = list.toArray(new String[list.size()]);
        }
        return returnedHrMin;
    }
    
    public static DateFormat getNewPs2DateFormat() {
        return new SimpleDateFormat(WebCoreConstants.DEFAULT_NEW_PS2_FORMAT);
    }
    
    public static Date convertFromRESTDateString(final String dateString) throws InvalidDataException {
        try {
            final DateFormat dateFormat = getNewPs2DateFormat();
            dateFormat.setLenient(false);
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Exception: Unable to Parse the Date to String in REST API: " + dateString, e);
        }
    }
    
    public static String createNewPs2DateString(final Date dateToFormat) {
        final DateFormat dateFormat = getNewPs2DateFormat();
        
        return dateFormat.format(dateToFormat);
    }
    
    public static String calculateDuration(final Date fromDate, final Date toDate) {
        final StringBuilder resultBuffer = new StringBuilder();
        final long fromTime = fromDate.getTime();
        final long toTime = toDate.getTime();
        
        final long differenceMillis = toTime - fromTime;
        
        long days = 0;
        long hours = 0;
        long mins = 0;
        long secs = 0;
        days = TimeUnit.MILLISECONDS.toDays(differenceMillis);
        if (days == 0) {
            hours = TimeUnit.MILLISECONDS.toHours(differenceMillis);
            if (hours == 0) {
                mins = TimeUnit.MILLISECONDS.toMinutes(differenceMillis);
                if (mins == 0) {
                    secs = TimeUnit.MILLISECONDS.toSeconds(differenceMillis);
                    resultBuffer.append(secs);
                    resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
                    resultBuffer.append(secs > 1 ? SECONDS : SECOND);
                } else {
                    resultBuffer.append(mins);
                    resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
                    resultBuffer.append(mins > 1 ? MINUTES : MINUTE);
                }
            } else {
                resultBuffer.append(hours);
                resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
                resultBuffer.append(hours > 1 ? HOURS : HOUR);
            }
        } else {
            resultBuffer.append(days);
            resultBuffer.append(StandardConstants.STRING_EMPTY_SPACE);
            resultBuffer.append(days > 1 ? DAYS : DAY);
        }
        
        return resultBuffer.toString();
    }
    
    public static Date createStartOfTodayDate() {
        return createStartOfTodayDate(Calendar.getInstance());
    }
    
    private static Date createStartOfTodayDate(final Calendar timeZoneSpecificCalendar) {
        timeZoneSpecificCalendar.set(Calendar.HOUR_OF_DAY, 0);
        timeZoneSpecificCalendar.set(Calendar.MINUTE, 0);
        timeZoneSpecificCalendar.set(Calendar.SECOND, 0);
        timeZoneSpecificCalendar.set(Calendar.MILLISECOND, 0);
        
        return timeZoneSpecificCalendar.getTime();
    }
    
    public static Date convertFromNewPs2DateString(final String dateString) throws InvalidDataException {
        try {
            final DateFormat dateFormat = getNewPs2DateFormat();
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Error: Unable to Parse New Ps2 Format Date String: " + dateString, e);
        }
    }
    
    public static Date createEndOfTodayDate() {
        return createEndOfTodayDate(Calendar.getInstance());
    }
    
    private static Date createEndOfTodayDate(final Calendar timeZoneSpecificCalendar) {
        timeZoneSpecificCalendar.set(Calendar.HOUR_OF_DAY, StandardConstants.LAST_HOUR_OF_THE_DAY);
        timeZoneSpecificCalendar.set(Calendar.MINUTE, StandardConstants.LAST_MIN_SECOND);
        timeZoneSpecificCalendar.set(Calendar.SECOND, StandardConstants.LAST_MIN_SECOND);
        timeZoneSpecificCalendar.set(Calendar.MILLISECOND, StandardConstants.CONSTANT_999);
        
        return timeZoneSpecificCalendar.getTime();
    }
    
    public static int getTimeZoneOffset(final String timeZone) {
        TimeZone timezone = TimeZone.getTimeZone(timeZone);
        if (timezone == null) {
            timezone = TimeZone.getDefault();
        }
        
        final Date datetime = new Date();
        
        return timezone.getOffset(datetime.getTime()) / StandardConstants.HOURS_IN_MILLIS_1;
    }
    
    public static Byte convertCloseTime(final byte previousValue, final String oldTimeZoneString, final String newTimeZoneString) {
        
        final int newOffset = getTimeZoneOffset(newTimeZoneString);
        final int oldOffset = getTimeZoneOffset(oldTimeZoneString);
        
        byte returnValue = (byte) (previousValue - (oldOffset - newOffset) * StandardConstants.CONSTANT_4);
        if (returnValue < 0) {
            returnValue = (byte) (WebCoreConstants.QUARTERS_HOURS_PER_DAY + returnValue);
        } else {
            returnValue %= WebCoreConstants.QUARTERS_HOURS_PER_DAY;
        }
        return returnValue;
    }
    
    public static PooledResource<DateFormat> takeDateFormat(final String pattern, final String timeZoneId) {
        return takeDateFormat(pattern, TimeZone.getTimeZone(timeZoneId));
    }
    
    public static PooledResource<DateFormat> takeDateFormat(final String pattern, final TimeZone timeZone) {
        final PooledResource<DateFormat> result = dateFormatPool.take(pattern);
        result.get().setTimeZone(timeZone);
        
        return result;
    }
    
    public static String format(final Date date, final String pattern) {
        return format(date, pattern, TimeZone.getDefault().getID());
    }
    
    public static String format(final Date date, final String pattern, final String timeZoneId) {
        return format(date, pattern, TimeZone.getTimeZone(timeZoneId));
    }
    
    public static String format(final Date date, final String pattern, final String timeZoneId, final String defaultVal) {
        return format(date, pattern, TimeZone.getTimeZone(timeZoneId), defaultVal);
    }
    
    public static String format(final Date date, final String pattern, final TimeZone timeZone) {
        final PooledResource<DateFormat> fmt = takeDateFormat(pattern, timeZone);
        final String result = fmt.get().format(date);
        fmt.close();
        
        return result;
    }
    
    public static String format(final Date date, final String pattern, final TimeZone timeZone, final String defaultVal) {
        String result = null;
        if (date == null) {
            result = defaultVal;
        } else {
            final PooledResource<DateFormat> fmt = takeDateFormat(pattern, timeZone);
            result = fmt.get().format(date);
            fmt.close();
        }
        
        return result;
    }
    
    public static Date parse(final String dateStr, final String pattern, final String timeZoneId) {
        return parse(dateStr, pattern, TimeZone.getTimeZone(timeZoneId));
    }
    
    public static Date parse(final String dateStr, final String pattern, final String timeZoneId, final boolean throwException) {
        return parse(dateStr, pattern, TimeZone.getTimeZone(timeZoneId), throwException);
    }
    
    public static Date parse(final String dateStr, final String pattern, final TimeZone timeZone, final boolean throwException) {
        Date result = null;
        
        if (dateStr != null) {
            final PooledResource<DateFormat> fmt = takeDateFormat(pattern, timeZone);
            try {
                result = fmt.get().parse(dateStr);
            } catch (ParseException pe) {
                LOGGER.error("Invalid date string '" + dateStr + "' for pattern '" + pattern + "' !", pe);
                if (throwException) {
                    throw new RuntimeException(pe);
                }
            } finally {
                fmt.close();
            }
        }
        
        return result;
    }
    
    public static Date parse(final String dateStr, final String pattern, final TimeZone timeZone) {
        return parse(dateStr, pattern, timeZone, false);
    }
    
    /**
     * Get first and last day of this week.
     * 
     * @param numOfWeeks
     *            Number of weeks ago.
     * @return Calendar[] { first date calendar, last date calendar }
     */
    public static Calendar[] getFirstLastDayofThisWeeks() {
        final Calendar firstDayCal = setBeginOfDayHourMinuteSecond(Calendar.getInstance());
        // First day of last week(s)
        firstDayCal.set(Calendar.DAY_OF_WEEK, firstDayCal.getFirstDayOfWeek());
        
        final Calendar lastDayCal = setEndOfDayHourMinuteSecond(Calendar.getInstance());
        // First day of this week.
        lastDayCal.set(Calendar.DAY_OF_YEAR, firstDayCal.get(Calendar.DAY_OF_YEAR) + StandardConstants.CONSTANT_6);
        
        return new Calendar[] { firstDayCal, lastDayCal };
    }
    
    /**
     * Get first and last day of last "completed" week(s).
     * 
     * @param numOfWeeks
     *            Number of weeks ago.
     * @return Calendar[] { first date calendar, last date calendar }
     */
    public static Calendar[] getFirstLastDayofLastWeeks(final int numOfWeeks) {
        final Calendar firstDayCal = setBeginOfDayHourMinuteSecond(Calendar.getInstance());
        // Last week(s)
        firstDayCal.add(Calendar.WEEK_OF_YEAR, numOfWeeks * -1);
        // First day of last week(s)
        firstDayCal.set(Calendar.DAY_OF_WEEK, firstDayCal.getFirstDayOfWeek());
        
        final Calendar lastDayCal = setEndOfDayHourMinuteSecond(Calendar.getInstance());
        // First day of this week.
        lastDayCal.set(Calendar.DAY_OF_WEEK, lastDayCal.getFirstDayOfWeek());
        // Last day is ended on Sat.
        lastDayCal.add(Calendar.DAY_OF_WEEK, -1);
        
        return new Calendar[] { firstDayCal, lastDayCal };
    }
    
    /**
     * Get first date and last date of previous "completed" months.
     * 
     * @param numOfMonthsAgo
     *            how many months ago, e.g.
     *            Current month: Oct. Last month: Sep. 1 ~ 30
     *            Current month: Dec. Last 3 months: Sep., Oct., Nov.
     *            Current month: Mar. Last 6 months: Sep., Oct., Nov., Dec. Jan. Feb.
     * @return Calendar[] { first date calendar, last date calendar }
     */
    public static Calendar[] getFirstLastOfLastMonths(final int numOfMonthsAgo) {
        final Calendar currentCal = new GregorianCalendar();
        final int currentMonth = currentCal.get(Calendar.MONTH);
        final int endMonth = currentMonth - 1;
        final int beginMonth = currentMonth - numOfMonthsAgo;
        
        // Set the first day of previous month(s).
        Calendar firstDayOfPrevMonth = new GregorianCalendar();
        firstDayOfPrevMonth = setBeginOfDayHourMinuteSecond(firstDayOfPrevMonth);
        firstDayOfPrevMonth.set(Calendar.MONTH, beginMonth);
        firstDayOfPrevMonth.set(Calendar.DAY_OF_MONTH, firstDayOfPrevMonth.getActualMinimum(Calendar.DAY_OF_MONTH));
        
        // Set the last day of previous month.
        Calendar lastDayOfPrevMonth = new GregorianCalendar();
        lastDayOfPrevMonth = setEndOfDayHourMinuteSecond(lastDayOfPrevMonth);
        lastDayOfPrevMonth.set(Calendar.MONTH, endMonth);
        lastDayOfPrevMonth.set(Calendar.DAY_OF_MONTH, lastDayOfPrevMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
        
        return new Calendar[] { firstDayOfPrevMonth, lastDayOfPrevMonth };
    }
    
    /**
     * Get first and last day of last "completed" week(s).
     * 
     * @param numOfWeeks
     *            Number of weeks ago.
     * @return Calendar[] { first date calendar, last date calendar }
     */
    public static Calendar[] getFirstLast24Hrs() {
        final Calendar firstHourCal = new GregorianCalendar();
        final int hour = firstHourCal.get(Calendar.HOUR_OF_DAY);
        final int dayOfYear = firstHourCal.get(Calendar.DAY_OF_YEAR);
        firstHourCal.set(Calendar.DAY_OF_YEAR, dayOfYear - 1);
        firstHourCal.set(Calendar.HOUR_OF_DAY, hour);
        firstHourCal.set(Calendar.MINUTE, 0);
        firstHourCal.set(Calendar.SECOND, 0);
        firstHourCal.set(Calendar.MILLISECOND, 0);
        
        final Calendar lastHourCal = new GregorianCalendar();
        lastHourCal.set(Calendar.MINUTE, 0);
        lastHourCal.set(Calendar.SECOND, 0);
        lastHourCal.set(Calendar.MILLISECOND, 0);
        
        return new Calendar[] { firstHourCal, lastHourCal };
    }
    
    /**
     * Get last year Jan. to Dec.
     * 
     * @return Calendar[] { first date calendar, last date calendar }
     */
    public static Calendar[] getLastYear() {
        Calendar firstDayCal = new GregorianCalendar();
        firstDayCal = setBeginOfDayHourMinuteSecond(firstDayCal);
        firstDayCal.add(Calendar.YEAR, -1);
        firstDayCal.set(Calendar.MONTH, Calendar.JANUARY);
        firstDayCal.set(Calendar.DAY_OF_MONTH, 1);
        
        Calendar lastDayCal = new GregorianCalendar();
        lastDayCal = setEndOfDayHourMinuteSecond(lastDayCal);
        lastDayCal.add(Calendar.YEAR, -1);
        lastDayCal.set(Calendar.MONDAY, Calendar.DECEMBER);
        lastDayCal.set(Calendar.DAY_OF_MONTH, StandardConstants.CONSTANT_31);
        
        return new Calendar[] { firstDayCal, lastDayCal };
        
    }
    
    /**
     * Set hours to 23, minutes to 59, seconds to 59.
     */
    public static Calendar setEndOfDayHourMinuteSecond(final Calendar endCal) {
        endCal.set(Calendar.HOUR_OF_DAY, StandardConstants.LAST_HOUR_OF_THE_DAY);
        endCal.set(Calendar.MINUTE, StandardConstants.LAST_MIN_SECOND);
        endCal.set(Calendar.SECOND, StandardConstants.LAST_MIN_SECOND);
        return endCal;
    }
    
    /**
     * Set startCal hours to 0, minutes to 0, seconds to 0.
     */
    public static Calendar setBeginOfDayHourMinuteSecond(final Calendar startCal) {
        startCal.set(Calendar.HOUR_OF_DAY, 0);
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);
        return startCal;
    }
    
    /**
     * Check if input date is between the start and end date.
     * 
     * @param date
     *            If date object is between startDate and endDate.
     * @param startDate
     *            Start date.
     * @param endDate
     *            End date.
     * @return boolean return 'true' if within the start and end date range.
     */
    public static boolean isInBetween(final Date date, final Date startDate, final Date endDate) {
        final Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        
        final Calendar startCal = new GregorianCalendar();
        startCal.setTime(startDate);
        startCal.set(Calendar.SECOND, 0);
        
        final Calendar endCal = new GregorianCalendar();
        endCal.setTime(endDate);
        endCal.set(Calendar.SECOND, 0);
        
        if ((cal.after(startCal) || isSameHourMinute(cal.getTime(), startCal.getTime())) && cal.before(endCal)
            || isSameHourMinute(cal.getTime(), endCal.getTime())) {
            return true;
        }
        return false;
    }
    
    public static boolean isSameHourMinute(final Date date1, final Date date2) {
        final Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date1);
        
        final Calendar cal2 = new GregorianCalendar();
        cal2.setTime(date2);
        
        if (cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY) && cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE)) {
            return true;
        }
        return false;
    }
    
    public static Date removeTime(final long millisecs) {
        return new Date((long) (Math.floor(millisecs / StandardConstants.DAYS_IN_MILLIS_1) * StandardConstants.DAYS_IN_MILLIS_1));
    }
    
    public static Date createStartDate(final String date, final TimeZone timeZone) {
        final StringBuilder fullDate = new StringBuilder(date);
        fullDate.append(StandardConstants.STRING_EMPTY_SPACE).append(UI_DEFAULT_START_TIME);
        
        return parse(fullDate.toString(), START_END_DATE_FORMAT, timeZone);
    }
    
    public static Date createEndDate(final String date, final TimeZone timeZone) {
        final StringBuilder fullDate = new StringBuilder(date);
        fullDate.append(StandardConstants.STRING_EMPTY_SPACE).append(UI_DEFAULT_END_TIME).append(StandardConstants.STRING_EMPTY_SPACE)
                .append(timeZone);
        
        return parse(fullDate.toString(), START_END_DATE_FORMAT, timeZone);
        
    }
    
    public static int parseMinutes(final String timeString, final String separator) {
        final String[] timeArr = timeString.split(separator);
        if (timeArr.length <= 0) {
            throw new NumberFormatException("Invalid time string \"" + timeString + "\"!");
        }
        
        int result = Integer.parseInt(timeArr[0], StandardConstants.LIMIT_10) * StandardConstants.HOURS_IN_MINS_1;
        if (timeArr.length >= 2) {
            result += Integer.parseInt(timeArr[1], StandardConstants.LIMIT_10);
        }
        
        return result;
    }
    
    public static String formateMinutes(final int minutes) {
        final int hrs = minutes / StandardConstants.HOURS_IN_MINS_1;
        final int mins = minutes % StandardConstants.HOURS_IN_MINS_1;
        
        final StringBuilder result = new StringBuilder();
        if (hrs < StandardConstants.LIMIT_10) {
            result.append(StandardConstants.STRING_ZERO);
        }
        result.append(hrs).append(StandardConstants.STRING_COLON);
        
        if (mins < StandardConstants.LIMIT_10) {
            result.append(StandardConstants.STRING_ZERO);
        }
        result.append(mins);
        
        return result.toString();
    }
    
    public static int calendarDayDistant(final long srcMillisecs, final long targetMillisecs) {
        long src = srcMillisecs % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
        src = srcMillisecs - src;
        
        long target = targetMillisecs % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
        target = targetMillisecs - target;
        
        return (int) Math.floor((target - src) / WebCoreConstants.ONE_DAY_IN_MILLISECONDS);
    }
    
    /**
     * Get the quarter of day by time zone. Defaults to GMT if argument is null or empty string
     * 
     * @param timeZone
     * @return - integer as quarter of day
     */
    public static int getCurrentQuarterOfDayByTimezone(final String timeZoneArg) {
        String timeZone = timeZoneArg;
        
        if (timeZone == null || timeZone.isEmpty()) {
            timeZone = GMT;
        } else {
            boolean isTimeZone = false;
            final String[] timezones = TimeZone.getAvailableIDs();
            for (String tz : timezones) {
                if (tz.equals(timeZone)) {
                    isTimeZone = true;
                    break;
                }
            }
            if (!isTimeZone) {
                throw new IllegalArgumentException(timeZone);
            }
        }
        
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        calendar.set(Calendar.MILLISECOND, 0);
        final int quarterOfHour = calendar.get(Calendar.MINUTE) / QUARTER_IN_MINUTES;
        
        return calendar.get(Calendar.HOUR_OF_DAY) * QUARTER + quarterOfHour;
    }
    
    public static Date addMinutesToDate(final Date dt, final int minutesToAdd) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.MINUTE, minutesToAdd);
        return cal.getTime();
    }
    
    public static Date subSecondsToDate(final Date dt, int secondsToSub) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        secondsToSub = -1 * secondsToSub;
        cal.add(Calendar.SECOND, secondsToSub);
        return cal.getTime();
    }
    

    public static String colonDelimitedStringToZuluString(final String colonDelimited) throws InvalidDataException {
        final Date date = DateUtil.convertFromColonDelimitedDateString(colonDelimited);
        return date.toInstant().toString();
    }
    
    public static Date convertUTCToGMT(final String utcDate) {
        final Date utcDateObj = parse(utcDate, DateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT, ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC);
        return convertTimeZone(WebCoreConstants.GMT, ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC, utcDateObj);
    }
    
    public static Date convertGMTToUTC(final Date gmt) {
        return convertTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC, WebCoreConstants.GMT, gmt);
    }
    
    public static String convertGMTToUTCStringFormat(final Date gmt, final String utcFormat) {
        final Date utc = convertGMTToUTC(gmt);
        final SimpleDateFormat formatter = new SimpleDateFormat(utcFormat);
        return formatter.format(utc);
    }
    
    public static String getDateFormattedWithChronoUnitDays(final String format, final String timeZoneId, final Date date) {
        final ZoneId zoneId = ZoneId.of(timeZoneId);
        final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(date.toInstant(), zoneId).truncatedTo(ChronoUnit.DAYS);
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return zonedDateTime.format(dateTimeFormatter);
    }

}
