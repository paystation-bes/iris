package com.digitalpaytech.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.digitalpaytech.exception.InvalidDataException;

public final class StableDateUtil {
    public static final String DATE_ONLY_FORMAT = "yyyy-MM-dd";
    public static final String DATE_ONLY_FORMAT_MONTH_DAY_YEAR = "MMM dd, yyyy";
    public static final String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String COLON_DELIMITED_DATE_FORMAT = "yyyy:MM:dd:HH:mm:ss:z";
    public static final String UTC_TRANSACTION_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
    public static final String GMT = "GMT";
    public static final String UTC = "UTC";
    
    private StableDateUtil() {
    }
    
    public static Date convertFromColonDelimitedDateString(final String dateString) throws InvalidDataException {
        try {
            final DateFormat dateFormat = new SimpleDateFormat(COLON_DELIMITED_DATE_FORMAT);
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Error: Unable to Parse Colon Delimited Date String: " + dateString, e);
        }
    }
    
    public static String getDateFormattedWithChronoUnitDays(final String format, final String timeZoneId, final Date date) {
        final ZoneId zoneId = ZoneId.of(timeZoneId);
        final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(date.toInstant(), zoneId).truncatedTo(ChronoUnit.DAYS);
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return zonedDateTime.format(dateTimeFormatter);
    }

    public static Instant convertFromGMTDate(final Date gmt) {
        final String utcStr = convertFromGMTDateToUTC(gmt);
        return Instant.parse(utcStr);
    }
    
    public static String convertFromGMTDateToUTC(final Date gmt) {
        final GregorianCalendar gmtCal = new GregorianCalendar();
        gmtCal.setTime(gmt);
        final SimpleDateFormat sdf = new SimpleDateFormat(UTC_TRANSACTION_DATE_TIME_FORMAT);
        return sdf.format(gmtCal.getTime());
    }
    
    public static int createCurrentYYMM() {
        final Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.YEAR) - StandardConstants.CONSTANT_2000) * StandardConstants.CONSTANT_100 + cal.get(Calendar.MONTH) + 1;
    }
    
    public static boolean isDateNDaysInTheFuture(final long numOfDays, final Date dateTime, final ZoneId zoneId) {
        final LocalDate ldDate = dateTime.toInstant().atZone(zoneId).toLocalDate();
        final LocalDate ldFuture = LocalDate.now(zoneId).plusDays(numOfDays);
        
        return ldDate.isAfter(ldFuture) || ldDate.isEqual(ldFuture);
    }
    
    public static Date getCurrentGmtDate() {
        return Date.from(ZonedDateTime.now(ZoneId.of(GMT)).toInstant());
    }
}
