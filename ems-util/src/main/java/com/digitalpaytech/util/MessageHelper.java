package com.digitalpaytech.util;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component("MessageHelper")
@SuppressWarnings({ "checkstyle:designforextension" })
public class MessageHelper implements MessageSourceAware {
    private MessageSourceAccessor msg;
    
    public String getMessage(final String messageKey) {
        return this.msg.getMessage(messageKey, messageKey);
    }
    
    public String getMessage(final String messageKey, final Object... params) {
        return this.msg.getMessage(messageKey, params, messageKey);
    }
    
    public String getMessageDefaultToKey(final String messageKey) {
        return this.msg.getMessage(messageKey, messageKey);
    }
    
    public String getMessageWithDefault(final String messageKey, final String defaultVal) {
        return this.msg.getMessage(messageKey, defaultVal);
    }
    
    public String getMessageWithKeyParams(final String messageKey, final String... messageKeys) {
        final Object[] params = new Object[messageKeys.length];
        int idx = params.length;
        while (--idx >= 0) {
            params[idx] = this.getMessage(messageKeys[idx]);
        }
        
        return this.getMessage(messageKey, params);
    }
    
    @Override
    public void setMessageSource(final MessageSource msgsrc) {
        if (msgsrc != null) {
            this.msg = new MessageSourceAccessor(msgsrc);
        }
    }
    
}
