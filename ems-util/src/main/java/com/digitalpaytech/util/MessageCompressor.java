package com.digitalpaytech.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * Compress string by using GZIP
 */

public class MessageCompressor {
    
    private static Logger logger = Logger.getLogger(MessageCompressor.class);
    private final static String DEFAULT_CHARSET = "ISO-8859-1";
    
    /**
     * Gzip the input string into a based64 encoded string
     * 
     * @return
     */
    public static String zipString(String input) {
        String zippedStr = null;
        ByteArrayOutputStream bos = null;
        BufferedOutputStream bufos = null;
        try {
            bos = new ByteArrayOutputStream();
            bufos = new BufferedOutputStream(new GZIPOutputStream(bos));
            bufos.write(input.getBytes(DEFAULT_CHARSET));
            bufos.close();
            byte[] retval = bos.toByteArray();
            bos.close();
            
            zippedStr = new String(Base64.encodeBase64(retval), DEFAULT_CHARSET);
            
            if (logger.isDebugEnabled()) {
                logger.debug("+++ original size: " + input.length() + " +++");
                logger.debug("+++   zipped size: " + zippedStr.length() + " +++");
            }
        } catch (Exception e) {
            logger.error("zipString error:", e);
        } finally {
            if (bos != null) {
                try { bos.close(); } catch (IOException ioe) { }
            }
            if (bufos != null) {
                try { bufos.close(); } catch (IOException ioe) { }
            }
        }
        return zippedStr;
    }
    
    /**
     * Gzip the input byte[] into a byte[]
     * 
     * @param input
     * @return
     */
    public static byte[] zipBytes(byte[] input) {
        byte[] retval = null;
        ByteArrayOutputStream bos = null;
        BufferedOutputStream bufos = null;
        try {
            bos = new ByteArrayOutputStream();
            bufos = new BufferedOutputStream(new GZIPOutputStream(bos));
            bufos.write(input);
            bufos.close();
            retval = bos.toByteArray();
            bos.close();
            
            if (logger.isDebugEnabled()) {
                logger.debug("+++ original size: " + input.length + " +++");
                logger.debug("+++   zipped size: " + retval.length + " +++");
            }
        } catch (Exception e) {
            logger.error("zipString error:", e);
        } finally {
            if (bos != null) {
                try { bos.close(); } catch (IOException ioe) { }
            }
            if (bufos != null) {
                try { bufos.close(); } catch (IOException ioe) { }
            }
        }
        return retval;
    }
    
    /**
     * GUNZIP a string
     * 
     * @param zippedStr
     * @return
     */
    public static String unzipString(String zippedStr) {
        String unzippedStr = null;
        ByteArrayOutputStream bos = null;
        ByteArrayInputStream bis = null;
        BufferedInputStream bufis = null;
        try {
            byte[] bytes = Base64.decodeBase64(zippedStr.getBytes(DEFAULT_CHARSET));
            bis = new ByteArrayInputStream(bytes);
            bufis = new BufferedInputStream(new GZIPInputStream(bis));
            bos = new ByteArrayOutputStream();
            byte[] buf = new byte[bytes.length];
            int len;
            while ((len = bufis.read(buf)) > 0) {
                bos.write(buf, 0, len);
            }
            unzippedStr = bos.toString();
            bis.close();
            bufis.close();
            bos.close();
            
            if (logger.isDebugEnabled()) {
                logger.debug("+++   zipped size: " + zippedStr.length() + " +++");
                logger.debug("+++ unzipped size: " + unzippedStr.length() + " +++");
            }
        } catch (Exception e) {
            logger.error("unzipString error:", e);
        } finally {
            if (bos != null) {
                try { bos.close(); } catch (IOException ioe) { }
            }
            if (bis != null) {
                try { bis.close(); } catch (IOException ioe) { }
            }
            if (bufis != null) {
                try { bufis.close(); } catch (IOException ioe) { }
            }
        }
        return unzippedStr;
    }
    
    public static void doFileZip(String filename, String zipfilename) {
        // Create a buffer for reading the files
        int buf = 2048;
        FileInputStream in = null;
        ZipOutputStream out = null;
        try {
            in = new FileInputStream(filename);
            out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipfilename)));
            out.putNextEntry(new ZipEntry(filename));
            byte data[] = new byte[buf];
            BufferedInputStream origin = new BufferedInputStream(in, buf);
            
            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = origin.read(data, 0, buf)) > 0) {
                out.write(data, 0, len);
            }
            
            origin.close();
            
            // Complete the entry
            out.closeEntry();
            
            //Complete the ZIP file
            out.close();
        } catch (IOException e) {
            logger.error("doFileZip error:", e);
        } finally {
            if (in != null) {
                try { in.close(); } catch (IOException ioe) { }
            }
            if (out != null) {
                try { out.close(); } catch (IOException ioe) { }
            }
        }
    }
    
    private MessageCompressor() {
    }
}
