package com.digitalpaytech.util;

import java.util.Map;

public class KeyValuePair<K, V> implements Map.Entry<K, V> {
    private K key;
    private V value;
    
    public KeyValuePair(final K key, final V value) {
        this.key = key;
        this.value = value;
    }
    
    @Override
    public final K getKey() {
        return this.key;
    }
    
    @Override
    public final V getValue() {
        return this.value;
    }
    
    @Override
    public final V setValue(final V newValue) {
        this.value = newValue;
        return this.value;
    }
}
