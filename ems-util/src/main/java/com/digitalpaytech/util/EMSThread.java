package com.digitalpaytech.util;

/**
 * Base class for all Thread in EMS. Every time an this class get instantiated, the instance will register itself with an {@link EMSThreadManager}.
 * 
 * 
 * @author wittawatv
 *
 */
public class EMSThread extends Thread {
    private boolean shutdown = false;
    
    /**
     * Create a Thread. The name should be specified to properly identify the Thread itself.
     * 
     * @param name represents this Thread
     */
    public EMSThread(String name) {
        super(name);
        
        EMSThreadManager.getInstance().register(this);
    }
    
    /**
     * Query the shutdown flag.
     * 
     * @return
     */
    public boolean isShutdown() {
        return shutdown;
    }
    
    /**
     * Set the shutdown flag to "true" to let this instance know that it should clear the current work and prepare itself for shutting down.
     * 
     */
    public void shutdown() {
        this.shutdown = true;
    }
}
