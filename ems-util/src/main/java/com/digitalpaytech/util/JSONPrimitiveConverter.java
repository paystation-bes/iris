package com.digitalpaytech.util;

import org.apache.log4j.Logger;
import org.codehaus.jettison.mapped.TypeConverter;

public class JSONPrimitiveConverter implements TypeConverter {
    private static final String MAX_FRACTION = "4503599627370496";
    
    private static final String STR_TRUE = "true";
    private static final String STR_FALSE = "false";
    private static final Logger LOGGER = Logger.getLogger(JSONPrimitiveConverter.class);
    
    @Override
    public final Object convertToJSONPrimitive(final String text) {
        Object result = null;
        if (text != null) {
            final String inText = text.trim();
            
            result = isBoolean(inText);
            
            if (result == null) {
                final Double d = getDoubleValue(inText);
                
                // At this point, this can be convert to number. However, it must also match with regex "^(0|([1-9][0-9]*))(\\.(0|([0-9]*[1-9])))?$"
                boolean isNaN = false;
                if (d != null && !d.isInfinite() && !d.isNaN()) {
                    final StringBuilder digits = new StringBuilder();
                    int i = 0;
                    int len = inText.length() - 1;
                    
                    // Eliminate sign and insignificant zeroes.
                    int zeroCnt = 0;
                    while (i <= len && inText.charAt(i) != '.' && (inText.charAt(i) <= '0' || inText.charAt(i) > '9')) {
                        if (inText.charAt(i) == '0') {
                            ++zeroCnt;
                        }
                        ++i;
                    }
                    
                    // Check whether there are more than one prefix zero or there is a prefix zero follow by digit(s) to mark this as NaN.
                    if (zeroCnt > 1 || zeroCnt == 1 && i <= len && inText.charAt(i) != '.') {
                        // Force this to end !
                        i = len + 1;
                        isNaN = true;
                    }
                    
                    // Append every digits before dot.
                    while (i <= len && inText.charAt(i) != '.') {
                        digits.append(inText.charAt(i));
                        ++i;
                    }
                    
                    if (i <= len) {
                        // Skip dot
                        ++i;
                        isNaN = i == 1 || i > len;
                    }
                    
                    // Eliminate insignificant zeroes.
                    zeroCnt = 0;
                    while (len >= i && (inText.charAt(len) <= '0' || inText.charAt(len) > '9')) {
                        if (inText.charAt(len) == '0') {
                            ++zeroCnt;
                        }
                        --len;
                    }
                    
                    if (zeroCnt > 1 || zeroCnt == 1 && i <= len) {
                        // Force this to end !
                        i = len + 1;
                        isNaN = true;
                    }
                    
                    if (digits.length() == 0) {
                        // Eliminate sign and insignificant zeroes.
                        // Focusing on the fraction part (completely ignored the integer part).
                        // The purpose is to detect precision lost when convert String to Double.
                        while (i <= len && (inText.charAt(i) <= '0' || inText.charAt(i) > '9')) {
                            ++i;
                        }
                    }
                    
                    // Append every other digits.
                    while (i <= len) {
                        digits.append(inText.charAt(i));
                        ++i;
                    }
                    
                    if (digits.length() > 1 && (digits.charAt(digits.length() - 1) == 'd' || digits.charAt(digits.length() - 1) == 'f'
                                                || digits.charAt(digits.length() - 1) == 'l')) {
                        digits.deleteCharAt(digits.length() - 1);
                    }
                    
                    if (!isNaN && (digits.length() < MAX_FRACTION.length()
                            || ((digits.length() == MAX_FRACTION.length()) && (digits.toString().compareTo(MAX_FRACTION) <= 0)))) {
                        result = d;
                    }
                }
                
                if (result == null) {
                    result = inText;
                }
            }
        }
        
        return result;
    }
    
    private Double getDoubleValue(final String inText) {
        Double d = null;
        try {
            if (inText.toLowerCase(WebCoreConstants.DEFAULT_LOCALE).matches("[^a-z]+")) {
                d = Double.valueOf(inText);
            }
            
        } catch (NumberFormatException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("NumberFormatException: " + e.getMessage());
            }
        }
        return d;
    }
    
    private Object isBoolean(final String inText) {
        Boolean result = null;
        if (inText.length() == STR_TRUE.length() || inText.length() == STR_FALSE.length()) {
            final String lowerCaseText = inText.toLowerCase(WebCoreConstants.DEFAULT_LOCALE);
            if (STR_TRUE.equals(lowerCaseText) || STR_FALSE.equals(lowerCaseText)) {
                result = Boolean.valueOf(lowerCaseText);
            }
        }
        return result;
    }
}
