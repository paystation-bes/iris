package com.digitalpaytech.util;

public final class RestCoreConstants {
    public static final String ACCOUNT_NAME_EXP = "[0-9a-zA-Z| |.|,|\\-|=|_|\\#|\\@|\\(|\\)|/|:|\\||\\{|\\}|\\\\]*";
    public static final String PS_COMMADDRESS_EXP = "[0-9a-zA-Z]*";
    
    /* Default REST session token expiry interval: 30 minutes */
    public static final int SESSION_TOKEN_EXPIRY_INTERVAL = 30;
    
    public static final String MULTI_VALUE_SEPERATOR = "[|]";
    public static final int SESSION_TOKEN_LENGTH = 32;
    
    public static final int MIN_LENGTH_1 = 1;
    public static final int MAX_LENGTH_10 = 10;
    public static final int MAX_LENGTH_20 = 20;
    public static final int MAX_LENGTH_30 = 30;
    public static final int MAX_LENGTH_32 = 32;
    public static final int MAX_LENGTH_255 = 255;
    public static final int MIN_LENGTH_8 = 8;
    public static final int MAX_MEDIUM_INT = 16777215;
    public static final int PS_MAX_LENGTH_12 = 12;
    public static final int PS_MIN_LENGTH_6 = 6;
    public static final int CREDITCARD_LAST4DIGIT_LENGTH = 4;
    
    public static final String THIRD_PARTY = "Third Party";
    public static final String PAYMENT_TYPE_CREDITCARD = "CreditCard";
    public static final String PAYMENT_TYPE_CASH = "Cash";
    
    /* Response Code */
    public static final int SUCCESS_GET_PERMIT = 100;
    public static final int SUCCESS = 200;
    public static final int SUCCESS_PERMIT = 201;
    public static final int SUCCESS_RELEASE_TOKEN = 204;
    
    public static final int FAILURE_INVALID_TOKEN = 401;
    public static final int FAILURE_OBJECT_NOT_FOUND = 404;
    public static final int FAILURE_INVALID_TIMESTAMP = 401;
    public static final int FAILURE_INVALID_PERMIT_NUMBER = 303;
    public static final int FAILURE_INVALID_PARAMETER = 400;
    public static final int FAILURE_NOT_AUTHORIZED_OPERATION = 401;
    public static final int FAILURE_INCORRECT_CREDENTIALS = 403;
    public static final int FAILURE_NOT_FOUND = 404;
    public static final int FAILURE_OBJECT_ALREADY_EXISTS = 412;
    public static final int FAILURE_INVALID_REQUEST_ENTITIES = 413;
    public static final int FAILURE_INVALID_REQUEST_URI = 414;
    public static final int FAILURE_INTERNAL_ERROR = 500;
    
    /* HTTP Method */
    public static final String HTTP_METHOD_GET = "GET";
    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_PUT = "PUT";
    public static final String HTTP_METHOD_DELETE = "DELETE";
    
    // request encoding
    public static final String HTTP_REST_ENCODE_CHARSET = "UTF-8";
    public final static String AMPERSAND = "&";
    public final static String EQUAL_SIGN = "=";
    
    /* REST ENDPOINT */
    public final static String ENDPOINT_GET_TOKEN = "getToken";
    public final static String ENDPOINT_RELEASE_TOKEN = "releaseToken";
    public final static String ENDPOINT_CREATE_PERMIT = "createPermit";
    public final static String ENDPOINT_UPDATE_PERMIT = "updatePermit";
    public final static String ENDPOINT_GET_PERMIT = "getPermit";
    public final static String ENDPOINT_GET_REGION = "getRegion";
    
    public final static String PATH_TOKEN = "/Token";
    public final static String PATH_PERMIT = "/Permit";
    public final static String PATH_REGION = "/Region";
    
    public final static String UNKNOWN_ACCOUNT_NAME = "Unknown";
    
    public final static String ENDPOINT_GET_COUPON = "getCoupon";
    public final static String ENDPOINT_CREATE_COUPON = "createCoupon";
    public final static String ENDPOINT_UPDATE_COUPON = "updateCoupon";
    public final static String ENDPOINT_DELETE_COUPON = "deleteCoupon";
    public final static String ENDPOINT_GET_COUPONS = "getCoupons";
    public final static String ENDPOINT_CREATE_COUPONS = "createCoupons";
    public final static String ENDPOINT_UPDATE_COUPONS = "updateCoupons";
    public final static String ENDPOINT_DELETE_COUPONS = "deleteCoupons";
    
    public final static String EMS_UNAVAILABLE = "EMSUnavailable";
}