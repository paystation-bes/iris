package com.digitalpaytech.util;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class EasySeedGenerator {
	public static EasySeedGenerator getInstance() {
		return SingletonHolder.instance;
	}
	
	private static class SingletonHolder {
		private static EasySeedGenerator instance = new EasySeedGenerator();
	}
	
	private Semaphore lock;
	private Random random;
	
	private EasySeedGenerator() {
		this.lock = new Semaphore(1);
		this.random = new Random(System.currentTimeMillis());
	}
	
	public byte[] generateSeed(int bytes) {
		byte[] result = new byte[bytes];
		this.lock.acquireUninterruptibly();
		try {
			this.random.nextBytes(result);
		}
		finally {
			this.lock.release();
		}
		
		return result;
	}
	
	public long nextLong() {
		long result;
		this.lock.acquireUninterruptibly();
		try {
			result = this.random.nextLong();
		}
		finally {
			this.lock.release();
		}
		
		return result;
	}
	
	public int nextInt(int limit) {
		int result;
		this.lock.acquireUninterruptibly();
		try {
			result = this.random.nextInt(limit);
		}
		finally {
			this.lock.release();
		}
		
		return result;
	}
}
