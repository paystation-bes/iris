package com.digitalpaytech.util;

public final class DeviceGroupConstants {
    public static final String FORM_SEARCH = "deviceGroupSearchForm";
    public static final String FORM_EDIT = "deviceGroupForm";
    public static final String MODEL_GROUPS_LIST = "groupsList";
    public static final String FORM_BULK_SCHEDULE = "bulkSchedule";
    public static final String CONFIGURATION_GROUP_ERROR_KEY =
            "label.settings.locations.payStationConfiguration.missingPayStation.contactIris.no.link";
    public static final String UNRECOGNIZED_DEVICES = "unrecognizedDevices";
    public static final String UPGRADE_STATUS = "upgradeStatus";
    public static final String PAYSTATION_TYPE = "paystationType";
    public static final String SERIAL_NUMBER = "serialNumber";
    public static final String PAY_STATION_NAME = "payStationName";
    public static final String MAP_INFO = "mapInfo";
    public static final String DEVICE_LIST = "deviceList";
    public static final String DEVICE_ID = "deviceId";
    public static final String IS_POS_ACTIVATED = "isPosActivated";
    public static final String IS_POS_DECOMMISSIONED = "isPosDecommissioned";
    public static final String IS_POS_HIDDEN = "isPosHidden";
    public static final String COMMUNICATION_FAILURE_WITH_FMSERVER = "Communication Failure with fmserver!";
    public static final String FAILED_TO_DESERIALIZE_DEVICE_GROUP_LIST_JSON = "Failed to deserialize Device Group List JSON!";
    public static final String CONFIGURATION_GROUP_ERROR_LOG = "DeviceGroup is null or no groupId: ";
    public static final String MALFORMED_BULK_SCHEDULE_REQUEST = "Bulk schedule request is malformed";
    public static final String BULK_SCHEDULE_NO_GROUP_SELECTED = "Bulk schedule request was received with an empty list of groupIds";
    public static final String BULK_SCHEDULE_NO_GROUP_SELECTED_ERROR_KEY = "error.settings.payStationConfiguration.bulk.schedule.no.groups.selected";
    public static final String FAILED_TO_CONFIG_CARD_PROCESSING_ERROR_KEY = "error.settings.payStationConfiguration.card.processing";
    public static final String FAILED_TO_CONFIG_SCHEDULE_ERROR_KEY = "error.settings.payStationConfiguration.schedule";
    public static final String FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY = 
            "error.settings.payStationConfiguration.card.processing.scheduled.cannot.update";
    
    private DeviceGroupConstants() {
    }
}
