package com.digitalpaytech.util;

public final class WidgetConstants {
    
    /* Dashboard widget chart type */
    public static final int CHART_TYPE_NA = 0;
    public static final int CHART_TYPE_LIST = 1;
    public static final int CHART_TYPE_PIE = 2;
    public static final int CHART_TYPE_LINE = 3;
    public static final int CHART_TYPE_BAR = 4;
    public static final int CHART_TYPE_COLUMN = 5;
    public static final int CHART_TYPE_AREA = 6;
    public static final int CHART_TYPE_SINGLE_VALUE = 7;
    public static final int CHART_TYPE_MAP = 8;
    public static final int CHART_TYPE_HEAT_MAP = 9;
    public static final int CHART_TYPE_OCCUPANCY_MAP = 10;
    
    /* Dashboard widget tier type */
    public static final int TIER_TYPE_NA = 0;
    public static final int TIER_TYPE_15MINUTES = 1;
    public static final int TIER_TYPE_HOUR = 2;
    public static final int TIER_TYPE_DAY = 3;
    public static final int TIER_TYPE_WEEK = 4;
    public static final int TIER_TYPE_MONTH = 5;
    public static final int TIER_TYPE_ALL_ORG = 101;
    //TODO will be removed
//    public static final int TIER_TYPE_PARENT_ORG = 102;
    public static final int TIER_TYPE_ORG = 103;
    public static final int TIER_TYPE_PARENT_LOCATION = 104;
    public static final int TIER_TYPE_LOCATION = 105;
    public static final int TIER_TYPE_ROUTE = 106;
    public static final int TIER_TYPE_PAYSTATION = 107;
    public static final int TIER_TYPE_RATE = 108;
    public static final int TIER_TYPE_MERCHANT_ACCOUNT = 109;
    public static final int TIER_TYPE_CITATION_TYPE = 110;
    public static final int TIER_TYPE_REVENUE_TYPE = 201;
    public static final int TIER_TYPE_TRANSACTION_TYPE = 202;
    public static final int TIER_TYPE_COLLECTION_TYPE = 203;
    public static final int TIER_TYPE_ALERT_TYPE = 204;
    public static final int TIER_TYPE_CARD_PROCESS_METHOD_TYPE = 205;
    
    /* Dashboard widget range type */
    public static final int RANGE_TYPE_NA = 0;
    public static final int RANGE_TYPE_NOW = 1;
    public static final int RANGE_TYPE_TODAY = 2;
    public static final int RANGE_TYPE_YESTERDAY = 3;
    public static final int RANGE_TYPE_LAST_24HOURS = 4;
    public static final int RANGE_TYPE_LAST_7DAYS = 5;
    public static final int RANGE_TYPE_LAST_30DAYS = 6;
    public static final int RANGE_TYPE_LAST_12MONTHS = 7;
//    public static final int RANGE_TYPE_THIS_WEEK = 8;
    public static final int RANGE_TYPE_THIS_MONTH = 9;
//    public static final int RANGE_TYPE_THIS_YEAR = 10;
    public static final int RANGE_TYPE_LAST_WEEK = 11;
    public static final int RANGE_TYPE_LAST_MONTH = 12;
    public static final int RANGE_TYPE_LAST_YEAR = 13;
    public static final int RANGE_TYPE_YEAR_TO_DATE = 14;
    
    /* Dashboard widget metric type */
    public static final int METRIC_TYPE_REVENUE = 1;
    public static final int METRIC_TYPE_COLLECTIONS = 2;
    public static final int METRIC_TYPE_SETTLED_CARD = 3;
    public static final int METRIC_TYPE_PURCHASES = 4;
    public static final int METRIC_TYPE_PAID_OCCUPANCY = 5;
    public static final int METRIC_TYPE_TURNOVER = 6;
    public static final int METRIC_TYPE_UTILIZATION = 7;
    public static final int METRIC_TYPE_DURATION = 8;
    public static final int METRIC_TYPE_ACTIVE_ALERTS = 9;
    public static final int METRIC_TYPE_ALERT_STATUS = 10;
    public static final int METRIC_TYPE_MAP = 11;
    public static final int METRIC_TYPE_AVERAGE_PRICE = 12;
    public static final int METRIC_TYPE_AVERAGE_REVENUE = 13;
    public static final int METRIC_TYPE_AVERAGE_DURATION = 14;
    public static final int METRIC_TYPE_CARD_PROCESSING_REV = 15;
    public static final int METRIC_TYPE_CARD_PROCESSING_TX = 16;
    public static final int METRIC_TYPE_CITATIONS = 17;
    public static final int METRIC_TYPE_CITATION_MAP = 18;
    public static final int METRIC_TYPE_PHYSICAL_OCCUPANCY = 19;
    public static final int METRIC_TYPE_COMPARE_OCCUPANCY = 20;
    public static final int METRIC_TYPE_OCCUPANCY_MAP = 21;

    /* Dashboard widget filter type */
    public static final int FILTER_TYPE_ALL = 0;
    public static final int FILTER_TYPE_SUBSET = 1;
    public static final int FILTER_TYPE_TOP = 2;
    public static final int FILTER_TYPE_BOTTOM = 3;
    public static final int FILTER_TYPE_TOP_BOTTOM = 4;
    
    //used in verification of widget settings 
    public static final Integer[] FILTER_OPTIONS = { 0, 5, 10, 15, 20, 25, 50, 100 };
    
    /* Dashboard widget limit type */
    public static final int LIMIT_TYPE_0 = 0;
    public static final int LIMIT_TYPE_5 = 1;
    public static final int LIMIT_TYPE_10 = 2;
    public static final int LIMIT_TYPE_15 = 3;
    public static final int LIMIT_TYPE_25 = 4;
    public static final int LIMIT_TYPE_50 = 5;
    public static final int LIMIT_TYPE_100 = 6;
    
    /* Dashboard widget list type */
    public static final int LIST_TYPE_MASTER = 1;
    public static final int LIST_TYPE_DEFAULT = 2;
    public static final int LIST_TYPE_USER = 3;
    public static final int LIST_TYPE_PARENT_MASTER = 4;
    public static final int LIST_TYPE_DEFAULT_PARENT = 5;
    
    /* Purchase/Revenue/Card Process Widget table type */
    public static final String TABLE_RANGE_TYPE_HOUR_STRING = "Hour";
    public static final String TABLE_RANGE_TYPE_DAY_STRING = "Day";
    public static final String TABLE_RANGE_TYPE_MONTH_STRING = "Month";
    public static final int TABLE_RANGE_TYPE_HOUR = 1;
    public static final int TABLE_RANGE_TYPE_DAY = 2;
    public static final int TABLE_RANGE_TYPE_MONTH = 3;
    
    /* Purchase/Revenue Widget table type */
    public static final int TABLE_DEPTH_TYPE_TOTAL = 1;
    public static final int TABLE_DEPTH_TYPE_DETAIL = 2;
    public static final int TABLE_DEPTH_TYPE_REVENUE = 3;
    
    public static final int DAYS_TO_KEEP_HOURLY_DATA = 2;
    public static final int DAYS_TO_KEEP_DAILY_DATA = 62;
    public static final int DAYS_TO_KEEP_MONTHLY_DATA = 731;

    public static final int TIER_1 = 1;
    public static final int TIER_2 = 2;
    public static final int TIER_3 = 3;
    
    public static final int BUCKET_IN_MILLISECONDS = 900000;
    
    private WidgetConstants() {
        
    }
}
