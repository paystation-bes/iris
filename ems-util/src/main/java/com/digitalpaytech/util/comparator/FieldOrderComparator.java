package com.digitalpaytech.util.comparator;

import java.lang.reflect.Field;
import java.util.Comparator;

import com.digitalpaytech.annotation.FieldOrder;

public class FieldOrderComparator implements Comparator<Field> {
    @Override
    public int compare(final Field field1, final Field field2) {
        final FieldOrder field1Order = field1.getAnnotation(FieldOrder.class);
        final FieldOrder field2Order = field2.getAnnotation(FieldOrder.class);
        
        int comparison = -1;
        if ((field1Order == null) || (field2Order == null)) {
            comparison = field1.getName().compareTo(field2.getName());
        } else {
            comparison = field1Order.value() - field2Order.value();
        }
        
        return comparison;
    }
}
