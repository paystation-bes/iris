package com.digitalpaytech.util;

public final class LicensePlateConstants {
    
    public static final int DEFAULT_MAX_LIMITED_USE = 9;
    public static final String LICENSE_PLATE_END_POINT = "/api/v1/licenseplates";
    public static final String PARAM_LICENSE_PLATE = "licensePlate";
    public static final String PARAM_CUSTOMER_ID = "customerId";
    public static final String PARAM_LOCATION_NAME = "locationName";
    public static final String PARAM_LOCAL_DATE = "localDate";
    
    private LicensePlateConstants() {
    }
}
