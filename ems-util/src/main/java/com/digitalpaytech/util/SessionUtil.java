package com.digitalpaytech.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class SessionUtil {
	public static String getCurrentUserName(HttpServletRequest request) {
		String result = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			User user = (User) auth.getPrincipal();
			if(user != null) {
				result = user.getUsername();
			}
		}
		
		return result;
	}
	
	public static String getCurrentUserNameEncoded(HttpServletRequest request) {
		String result = null;
		try {
			result = URLEncoder.encode(getCurrentUserName(request), "UTF-8");
		}
		catch(UnsupportedEncodingException uee) {
			// DO NOTHING.
		}
		
		return result;
	}
}
