package com.digitalpaytech.util;

import java.io.File;
import java.util.ArrayList;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotification {
	private InternetAddress[] toAddrs = null;
	private InternetAddress[] ccAddrs = null;
	private InternetAddress[] bccAddrs = null;
	private String subject = null;
	private String content = null;
	private ArrayList<AttachmentInfo> attachments = new ArrayList<AttachmentInfo>();

	private MimeMessage message = null;

	public class AttachmentInfo {
		private String fileName;
		private File file;

		public AttachmentInfo(String fileName, File file) {
			super();
			this.fileName = fileName;
			this.file = file;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public File getFile() {
			return file;
		}

		public void setFile(File file) {
			this.file = file;
		}
	}

	public EmailNotification(InternetAddress[] toAddrs,
			InternetAddress[] ccAddrs, InternetAddress[] bccAddrs,
			String subject, String content) {
		super();
		this.toAddrs = toAddrs;
		this.ccAddrs = ccAddrs;
		this.bccAddrs = bccAddrs;
		this.subject = subject;
		this.content = content;
	}

	public EmailNotification(MimeMessage message) {
		this.message = message;
	}

	public void addAttachment(String fileName, File file) {
		attachments.add(new AttachmentInfo(fileName, file));
	}

	public ArrayList<AttachmentInfo> getAttachments() {
		return attachments;
	}

	public InternetAddress[] getToAddrs() {
		return toAddrs;
	}

	public void setToAddrs(InternetAddress[] toAddrs) {
		this.toAddrs = toAddrs;
	}

	public InternetAddress[] getCcAddrs() {
		return ccAddrs;
	}

	public void setCcAddrs(InternetAddress[] ccAddrs) {
		this.ccAddrs = ccAddrs;
	}

	public InternetAddress[] getBccAddrs() {
		return bccAddrs;
	}

	public void setBccAddrs(InternetAddress[] bccAddrs) {
		this.bccAddrs = bccAddrs;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public MimeMessage getMessage() {
		return message;
	}

	public void setMessage(MimeMessage message) {
		this.message = message;
	}

}
