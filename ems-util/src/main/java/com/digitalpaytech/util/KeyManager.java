package com.digitalpaytech.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.impl.CipherIdGenerator;
import com.digitalpaytech.util.impl.EasyAES128;

public class KeyManager {
    public static final String CTXKEY_CACHE = "cache";
    
    private static final int KEY_CONCURRENCY_LEVEL = 1;
    
    private EasyCacheTrimmer cacheTrimmer;
    
    private int possibleCiphersCount = 1;
    
    private int minCacheSize = 100000;
    
    private int rotationSecs = 900;
    private long rotationMillisecs = rotationSecs * 1000;
    
    private Random rand;
    
    private ReadWriteLock lock;
    private EasyCipherKey[] possibleCiphers;
    private Map<EasyCipherKey, Map<String, Object>> possibleContexts;
    
    private long lastRotationTime;
    
    public KeyManager() {
        this.rand = new Random();
        
        this.lock = new ReentrantReadWriteLock(true);
        this.possibleContexts = new HashMap<EasyCipherKey, Map<String, Object>>(this.possibleCiphersCount * 4, 0.8F);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    public void rotateCipher() {
        // Pre-Create new Cipher for rotation to shorten the time needed to hold write-lock.
        int i = this.possibleCiphersCount;
        final EasyCipherKey[] newCiphers = new EasyCipherKey[i];
        final Map<EasyCipherKey, Map<String, Object>> newContexts = new HashMap<EasyCipherKey, Map<String, Object>>(i * 4, 0.8F);
        
        final Lock readLock = this.lock.readLock();
        readLock.lock();
        try {
            while (--i >= 0) {
                newCiphers[i] = new EasyCipherKey();
                
                Map<String, Object> context = this.possibleContexts.get(newCiphers[i]);
                if (context == null) {
                    context = createContext();
                }
                
                newContexts.put(newCiphers[i], context);
            }
        } catch (CryptoException ce) {
            throw new RuntimeException("Could not rotate Cipher", ce);
        } finally {
            readLock.unlock();
        }
        
        EasyCipherKey[] oldCiphers = null;
        Map<EasyCipherKey, Map<String, Object>> oldContexts = null;
        
        // Lock & Perform the switch !
        final Lock lock = this.lock.writeLock();
        lock.lock();
        try {
            final long passed = System.currentTimeMillis() - this.lastRotationTime;
            if (passed > this.rotationMillisecs) {
                oldCiphers = this.possibleCiphers;
                this.possibleCiphers = newCiphers;
                
                oldContexts = this.possibleContexts;
                this.possibleContexts = newContexts;
                
                this.lastRotationTime = System.currentTimeMillis();
            }
        } finally {
            lock.unlock();
        }
        
        // Clean up.
        if (oldContexts != null) {
            oldContexts.clear();
        }
        
        if (oldCiphers != null) {
            i = oldCiphers.length;
            while (--i >= 0) {
                oldCiphers[i] = null;
            }
        }
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    public RandomKeyMapping createKeyMapping() {
        CipherIdGenerator result = null;
        final long passed = System.currentTimeMillis() - this.lastRotationTime;
        if (passed > this.rotationMillisecs) {
            rotateCipher();
        }
        
        final Lock lock = this.lock.readLock();
        lock.lock();
        try {
            result = createCipherIdGenerator();
        } finally {
            lock.unlock();
        }
        
        return result;
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    public RandomKeyMapping ensureInit(final RandomKeyMapping keyMapping) {
        Map<String, Object> context = this.possibleContexts.get(keyMapping.getContextIdentity());
        if (context == null) {
            final Lock lock = this.lock.writeLock();
            lock.lock();
            try {
                context = this.possibleContexts.get(keyMapping.getContextIdentity());
                if (context == null) {
                    context = createContext();
                }
            } finally {
                lock.unlock();
            }
        }
        
        keyMapping.ensureInit(context);
        return keyMapping;
    }
    
    private CipherIdGenerator createCipherIdGenerator() {
        final EasyCipherKey cipherKey = this.possibleCiphers[this.rand.nextInt(this.possibleCiphersCount)];
        final CipherIdGenerator result = new CipherIdGenerator(cipherKey, new EasyPool<EasyCipher>(new CipherBuilder(), cipherKey, KEY_CONCURRENCY_LEVEL));
        
        final Map<String, Object> context = this.possibleContexts.get(cipherKey);
        result.ensureInit(context);
        
        return result;
    }
    
    private Map<String, Object> createContext() {
        final Map<String, Object> context = new HashMap<String, Object>(3, 0.5F);
        context.put(CTXKEY_CACHE, new EasyCache<ByteArray, ByteArray>(this.minCacheSize, this.cacheTrimmer));
        
        return context;
    }
    
    public final int getPossibleCiphersCount() {
        return this.possibleCiphersCount;
    }
    
    public final void setPossibleCiphersCount(final int possibleCiphersCount) {
        this.possibleCiphersCount = possibleCiphersCount;
    }
    
    public final int getMinCacheSize() {
        return this.minCacheSize;
    }
    
    public final void setMinCacheSize(final int minCacheSize) {
        this.minCacheSize = minCacheSize;
    }
    
    public final int getRotationSecs() {
        return this.rotationSecs;
    }
    
    public final void setRotationSecs(final int rotationSecs) {
        this.rotationSecs = rotationSecs;
        this.rotationMillisecs = rotationSecs * 1000;
    }
    
    public final void setCacheTrimmer(final EasyCacheTrimmer cacheTrimmer) {
        this.cacheTrimmer = cacheTrimmer;
    }
    
    private static class CipherBuilder implements EasyObjectBuilder<EasyCipher> {
        private static final long serialVersionUID = 453296348683231514L;

        @Override
        public final EasyCipher create(final Object configurations) {
            final EasyAES128 cipher = new EasyAES128((EasyCipherKey) configurations);
            cipher.ensureInit();
            
            return cipher;
        }
    }
}
