package com.digitalpaytech.util;

import java.io.Serializable;

public interface CharsetConverter extends Serializable {
    StringBuilder encode(byte[] data);
    
    byte[] decode(String data);
    
    void ensureInit();
}
