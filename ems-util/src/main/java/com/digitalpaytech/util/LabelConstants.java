package com.digitalpaytech.util;

public final class LabelConstants {
    
    public static final String AND = "label.connector.and";
    public static final String LABEL_COMMENT = "label.comment";
    public static final String LABEL_EMAIL = "label.email";
    public static final String LABEL_SCHEDULE_DATE = "label.scheduleDate";
    public static final String LABEL_SETTINGS_GLOBALPREF_MIGRATION_TIMERANGE = "label.settings.globalPref.migration.timeRange";
    public static final String LABEL_COUPON_LOCATION_NAME = "label.coupon.location.name";
    public static final String LABEL_COUPON_SEARCH_WITH_DOT = "label.coupon.search.status.";
    public static final String ERROR_CUSTOMER_MIGRATION_FAIL = "error.customer.migration.fail";
    public static final String ERROR_CUSTOMER_NOT_MIGRATED = "error.customer.notMigrated";
    
    public static final String REPORT_ERROR_PROPERTY = "error.report.status.general";
    public static final String REPORT_ERROR_TOO_LARGE = "error.report.status.data.too.large";
    public static final String REP_EMAIL_SUBJECT = "label.reporting.email.subject";
    public static final String REP_EMAIL_CONTENT = "label.reporting.email.content";
    
    public static final String ALERT_EMAIL_PAYSTATION_SUBJECT_TEMPLATE = "alert.email.paystation.subject.template";
    public static final String ALERT_RECOVERY_EMAIL_PAYSTATION_SUBJECT_TEMPLATE = "alert.recovery.email.paystation.subject.template";
    
    public static final String ALERT_OPERATION_NONPRIMARY_NOT_ALLOW = "alert.operation.nonprimary.not.allow";
    
    public static final String CM_LABEL_ALLBLOCKERSPASSFORCUSTOMER = "cm.label.AllBlockersPassForCustomer";
    public static final String CM_WARNING_ALLBLOCKERSPASSFORCUSTOMER = "cm.warning.AllBlockersPassForCustomer";
    public static final String CM_URL_ALLBLOCKERSPASSFORCUSTOMER = "cm.URL.AllBlockersPassForCustomer";
    
    public static final String CM_EMAIL_ADMIN_MIGRATIONCANCEL_SUBJECT = "customermigration.email.admin.migrationcancelled.subject";
    public static final String CM_EMAIL_ADMIN_MIGRATIONCANCEL_DV_CONTENT = "customermigration.email.admin.migrationcancelled.dataValidation.content";
    
    public static final String LABEL_REPORTING_TRANSACTIONSEARCH_NOEXPIRYDATE = "label.reporting.transactionSearch.noExpiryDate";
    
    public static final String ALERT_EMAIL_COLLECTION_SUBJECT_TEMPLATE = "alert.email.collection.subject.template";
    
    public static final String LABEL_CSV_COLUMN_NAME_VALUE = "label.csv.column.name.value";
    public static final String LABEL_CSV_COLUMN_NAME_PERCENT_SIGN_BRACKETS = "label.csv.column.name.percent.sign.brackets";
    public static final String LABEL_CSV_COLUMN_NAME_DOLLAR_SIGN_BRACKETS = "label.csv.column.name.dollar.sign.brackets";
    public static final String LABEL_CSV_COLUMN_NAME_OCCUPANCY_TYPE = "label.csv.column.name.occupancyType";
    
    public static final String PROCESSOR_TRANSACTION_TYPE_CANCELLED_ID = "processorTransactionType.cancelled.id";
    public static final String PROCESSOR_TRANSACTION_TYPE_PREAUTH_ID = "processorTransactionType.preauth.id";
    public static final String PROCESSOR_TRANSACTION_TYPE_STORE_FORWARD_ID = "processorTransactionType.chargeSfWithNoRefunds.id";
    public static final String PROCESSOR_TRANSACTION_TYPE_INVALID_DATA_ID = "processorTransactionType.invalidData.id";
    public static final String PROCESSOR_TRANSACTION_TYPE_N_A_ID = "processorTransactionType.na.id";
    public static final String CARD_RETRY_TRANSACTION_TYPE_IMPORTED_STORED_FORWARD_ID = "cardRetryTransactionType.imported.storedForward.id";
    public static final String CARD_RETRY_TRANSACTION_TYPE_BATCHED_AUTO_EMS_RETRY_ID = "cardRetryTransactionType.batched.autoEmsRetry.id";
    public static final String TRANSACTION_TYPE_ADD_TIME_EBP_ID = "transaction.type.addtime.ebp.id";
    public static final String TRANSACTION_TYPE_REGULAR_EBP_ID = "transaction.type.regular.ebp.id";
    public static final String SMS_MESSAGE_TYPE_TRANSACTION_RECEIVED_ID = "smsMessageType.transaction.received.id";
    
    public static final String CARD_MANAGEMENT_COMMENT = "label.cardManagement.bannedCard.comment";
    public static final String CARD_MANAGEMENT_BANNED_CARD = "label.cardManagement.bannedCard";
    public static final String CARD_SETTINGS_CARD_TYPE = "label.settings.cardSettings.CardType";
    
    public static final String CARD_SETTINGS_CARD_NUMBER = "label.settings.cardSettings.CardNumber";
    public static final String CARD_SETTINGS_EXPIRY = "label.settings.cardSettings.CardExpiry";
    
    private LabelConstants() {
    }
}
