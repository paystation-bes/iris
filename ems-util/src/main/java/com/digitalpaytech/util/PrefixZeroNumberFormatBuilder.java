package com.digitalpaytech.util;

import java.text.DecimalFormat;

public class PrefixZeroNumberFormatBuilder implements EasyObjectBuilder<DecimalFormat> {
    private String format;
    
    public PrefixZeroNumberFormatBuilder(final int length) {
        final StringBuilder formatBuffer = new StringBuilder(length);
        int i = length;
        while (--i >= 0) {
            formatBuffer.append("0");
        }
        
        this.format = formatBuffer.toString();
    }
    
    @Override
    public final DecimalFormat create(final Object configurations) {
        return new DecimalFormat(this.format);
    }
}
