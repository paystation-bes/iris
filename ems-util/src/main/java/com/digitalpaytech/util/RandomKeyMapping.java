package com.digitalpaytech.util;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.util.support.WebObjectId;

@SuppressWarnings({ "checkstyle:designforextension" })
public abstract class RandomKeyMapping implements Serializable {
    private static final Logger LOG = Logger.getLogger(RandomKeyMapping.class);
    
    private static final int BITCNT_LONG = 64;
    private static final BigInteger BITMASK_LONG = BigInteger.valueOf(Long.MAX_VALUE).setBit(BITCNT_LONG - 1);
    
    private static final long serialVersionUID = -2958791807184994582L;
    
    protected RandomKeyMapping() {
        
    }
    
    public abstract String getRandomString(Class<?> beanType, Object id);
    
    public abstract WebObjectId getKeyObject(String value);
    
    public abstract void clear();
    
    /*
     * logs the values in the reverse map at trace level for debugging
     */
    public abstract void logReverseMap();
    
    public static BigInteger pack(final Long... ids) {
        int idIdx = 0;
        BigInteger result = convertToBigInteger(ids[idIdx]);
        while (++idIdx < ids.length) {
            result = result.shiftLeft(BITCNT_LONG).or(convertToBigInteger(ids[idIdx]));
        }
        
        return result;
    }
    
    public static Long[] unpackLong(final BigInteger bundledIds) {
        int idx = (int) Math.ceil(((double) bundledIds.bitLength()) / BITCNT_LONG);
        final Long[] result = new Long[idx];
        BigInteger currentBundle = bundledIds;
        while (--idx >= 0) {
            final long tmp = BITMASK_LONG.and(currentBundle).longValue();
            if (tmp != Long.MIN_VALUE) {
                result[idx] = tmp;
            }
            
            currentBundle = currentBundle.shiftRight(BITCNT_LONG);
        }
        
        return result;
    }
    
    private static BigInteger convertToBigInteger(final Long id) {
        return BITMASK_LONG.and(BigInteger.valueOf((id == null) ? Long.MIN_VALUE : id));
    }
    
    public String getRandomString(final Object obj, final String name) {
        Object beanObj = obj;
        Object substitute = null;
        try {
            // if the object is HibernateProxy get the real domain object
            if (beanObj instanceof HibernateProxy) {
                beanObj = unproxy(beanObj);
            }
            
            // hide the objectId
            substitute = getRandomString(beanObj.getClass(), PropertyUtils.getProperty(beanObj, name));
        } catch (IllegalAccessException iae) {
            LOG.error(iae);
            throw new RuntimeException(iae);
            
        } catch (InvocationTargetException ite) {
            LOG.error(ite);
            throw new RuntimeException(ite);
        } catch (NoSuchMethodException nsme) {
            LOG.error(nsme);
            throw new RuntimeException(nsme);
        }
        // return pkObject.getPrimaryKey().toString();
        // replace the property's value with a random key
        // PropertyUtils.setProperty(bean, name, substitute);
        
        return (substitute == null) ? null : substitute.toString();
    }
    
    /**
     * Hides a JavaBean's property value by substituting it with a randomly
     * generated value.
     * 
     * @param bean
     *            a JavaBean
     * @param name
     *            name of the property to hide
     * 
     * @throws RandomKeyMappingException
     */
    public WebObject hideProperty(final Object bean, final String name) {
        return new WebObject(getRandomString(bean, name), bean);
    }
    
    /**
     * Hides a Collection of JavaBeans' property values by substituting them
     * with randomly generated values.
     * 
     * @param collection
     *            a Collection of JavaBeans
     * @param name
     *            name of the property to hide
     * 
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Collection hideProperties(final Collection collection, final String name) {
        final List newCollection = new ArrayList();
        final Iterator i = collection.iterator();
        // iterate thru the collection of beans
        while (i.hasNext()) {
            final Object object = i.next();
            
            if (object instanceof Collection) {
                // process nested collection
                final Collection subCollection = hideProperties((Collection) object, name);
                newCollection.add(subCollection);
            } else {
                // hide this bean's property
                final Object newObject = hideProperty(object, name);
                newCollection.add(newObject);
            }
        }
        return newCollection;
    }
    
    /**
     * Retrieves a property key from the Map using its Map.Entry value
     * 
     * @param value
     *            a Map.Entry value
     * 
     * @return the corresponding Map.Entry key
     */
    public Object getKey(final String value) {
        final WebObjectId objId = getKeyObject(value);
        Object obj = null;
        if (objId != null) {
            obj = objId.getId();
        }
        return obj;
    }
    
    public List<Object> getAllKeys(final String values) {
        return this.extracyKeys(new ArrayList<Object>(), values, (Class<?>) null, Object.class);
    }
    
    @SuppressWarnings("unchecked")
    public <KT> KT getKey(final String value, final Class<?> beanType, final Class<KT> expectedKeyClass) {
        Object result = null;
        WebObjectId objId = getKeyObject(value);
        if ((objId != null) && (beanType != null) && (!beanType.isAssignableFrom(objId.getObjectType()))) {
            objId = null;
        }
        if (objId != null) {
            result = objId.getId();
        }
        
        if ((result != null) && (expectedKeyClass != null) && (!expectedKeyClass.isAssignableFrom(result.getClass()))) {
            result = null;
        }
        
        return (KT) result;
    }
    
    public <KT> List<KT> getAllKeys(final String values, final Class<?> beanType, final Class<KT> expectedKeyClass) {
        return this.extracyKeys(new ArrayList<KT>(), values, beanType, expectedKeyClass);
    }
    
    protected <KT> List<KT> extracyKeys(final List<KT> result, final String commaSeparatedKeys, final Class<?> beanType,
        final Class<KT> expectedKeyClass) {
        if (commaSeparatedKeys != null) {
            final StringTokenizer tokenizer = new StringTokenizer(commaSeparatedKeys, ",");
            while (tokenizer.hasMoreTokens()) {
                final String raw = tokenizer.nextToken().trim();
                final KT key = getKey(raw, beanType, expectedKeyClass);
                if (key == null) {
                    throw new IllegalStateException("Unable to resolve key for string: " + raw);
                }
                
                result.add(key);
            }
        }
        
        return result;
    }
    
    /**
     * Converts an HibernateProxy object to the domain object
     * 
     * @param proxied
     * @return
     */
    public Object unproxy(final Object proxied) {
        Object entity = proxied;
        if (entity != null && entity instanceof HibernateProxy) {
            Hibernate.initialize(entity);
            entity = (Object) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
        }
        return entity;
    }
    
    // For subclass to implement
    public Object getContextIdentity() {
        return null;
    }
    
    // For subclass to implement
    public void ensureInit(final Map<String, Object> context) {
        // DO NOTHING.
    }
    
}
