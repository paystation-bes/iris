package com.digitalpaytech.util;

import java.io.Serializable;

public class TransientWrapper<D> implements Serializable {
	private static final long serialVersionUID = 7259618442235795078L;
	
	private transient D data;
	
	public TransientWrapper() {
		
	}
	
	public TransientWrapper(D data) {
		this.data = data;
	}
	
	public D get() {
		return data;
	}
	
	public void set(D data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return (this.data == null) ? "null" : this.data.toString();
	}
}
