package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

public class EasyCacheTrimmer {
    private Semaphore lock;
    
    private ConcurrentHashMap<EasyCache<?, ?>, EasyCache<?, ?>> requested;
    private List<EasyCache<?, ?>> toBeTrimmed;
    
    public EasyCacheTrimmer() {
        this.lock = new Semaphore(1);
        
        this.requested = new ConcurrentHashMap<EasyCache<?, ?>, EasyCache<?, ?>>();
        this.toBeTrimmed = new ArrayList<EasyCache<?, ?>>();
    }
    
    public final void trimSoon(final EasyCache<?, ?> cache) {
        this.requested.putIfAbsent(cache, cache);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    public void trim() {
        this.lock.acquireUninterruptibly();
        try {
            process();
            fetchNext();
        } finally {
            this.lock.release();
        }
    }
    
    private void process() {
        final Iterator<EasyCache<?, ?>> itr = this.toBeTrimmed.iterator();
        while (itr.hasNext()) {
            itr.next().trim();
            itr.remove();
        }
    }
    
    private void fetchNext() {
        final Iterator<Map.Entry<EasyCache<?, ?>, EasyCache<?, ?>>> entryItr = this.requested.entrySet().iterator();
        while (entryItr.hasNext()) {
            this.toBeTrimmed.add(entryItr.next().getValue());
            entryItr.remove();
        }
    }
}
