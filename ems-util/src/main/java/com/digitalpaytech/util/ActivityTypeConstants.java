package com.digitalpaytech.util;

/**
 * WebCoreConstants.
 * 
 * @author David Clarke
 *
 */
public final class ActivityTypeConstants {
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int LOGIN = 100;
    
    /* Child Activity Types */
    public static final int USER_LOGIN = 101;
    public static final int USER_LOGOUT = 102;
    public static final int LOGIN_FAILURE = 103;
    public static final int LOCKED_OUT = 104;
    public static final int USER_SWITCHED_IN = 105;
    public static final int USER_SWITCHED_OUT = 106;
    
    public static final int[] LOGIN_TYPES = new int[] { USER_LOGIN, USER_LOGOUT, LOGIN_FAILURE, };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int BOSS = 200;
    
    /* Child Activity Types */
    public static final int CONFIGURATION_UPLOAD = 201;
    public static final int TRANSACTION_UPLOAD = 203;
    
    public static final int[] BOSS_CONFIG_TYPES = new int[] { CONFIGURATION_UPLOAD, TRANSACTION_UPLOAD, };
    
    public static final int BAD_CARD_LIST_DOWNLOAD = 202;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int COUPON_CONFIG = 300;
    
    /* Child Activity Types */
    public static final int COUPON_BULK_EXPORT = 301;
    public static final int COUPON_BULK_IMPORT = 302;
    
    public static final int[] COUPON_IMORT_EXPORT_TYPES = new int[] { COUPON_BULK_EXPORT, COUPON_BULK_IMPORT, };
    
    public static final int ADD_COUPON = 303;
    public static final int EDIT_COUPON = 304;
    public static final int DELETE_COUPON = 305;
    
    public static final int[] COUPON_CONFIG_TYPES = new int[] { ADD_COUPON, EDIT_COUPON, DELETE_COUPON };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int CARD_MANAGEMENT_CONFIG = 400;
    
    /* Child Activity Types */
    public static final int CARD_BULK_EXPORT = 401;
    public static final int CARD_BULK_IMPORT = 402;
    
    public static final int[] CARD_IMPORT_EXPORT_TYPES = new int[] { CARD_BULK_EXPORT, CARD_BULK_IMPORT, };
    
    public static final int ADD_PERMITTED_RECORD = 403;
    public static final int EDIT_PERMITTED_RECORD = 404;
    public static final int DELETE_PERMITTED_RECORD = 405;
    public static final int ADD_BANNED_RECORD = 406;
    public static final int EDIT_BANNED_RECORD = 407;
    public static final int DELETE_BANNED_RECORD = 408;
    
    public static final int[] CARD_CONFIG_TYPES = new int[] { ADD_PERMITTED_RECORD, EDIT_PERMITTED_RECORD, DELETE_PERMITTED_RECORD,
        ADD_BANNED_RECORD, EDIT_BANNED_RECORD, DELETE_BANNED_RECORD };
    
    public static final int ADD_CARD_TYPE = 409;
    public static final int EDIT_CARD_TYPE = 410;
    public static final int DELETE_CARD_TYPE = 411;
    
    public static final int[] CUSTOM_CARD_CONFIG_TYPES = new int[] { ADD_CARD_TYPE, EDIT_CARD_TYPE, DELETE_CARD_TYPE };
    
    public static final int CREDIT_CARD_REFUND = 412;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int LOCATION_CONFIG = 500;
    
    /* Child Activity Types */
    public static final int ADD_LOCATION = 501;
    public static final int EDIT_LOCATION = 502;
    public static final int DELETE_LOCATION = 503;
    
    public static final int[] LOCATION_CONFIG_TYPES = new int[] { ADD_LOCATION, EDIT_LOCATION, DELETE_LOCATION };
    
    public static final int ASSIGN_PAY_STATION_TO_LOCATION = 504;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int PAY_STATION_CONFIG = 600;
    
    /* Child Activity Types */
    public static final int PAY_STATION_IS_VISIBLE = 601;
    public static final int PAY_STATION_IS_HIDDEN = 602;
    public static final int ACTIVATE_PAY_STATION = 603;
    public static final int DEACTIVATE_PAY_STATION = 604;
    public static final int EDIT_SETTINGS = 605;
    public static final int CREATE_PAY_STATION = 606;
    
    public static final int[] PAY_STATION_CONFIG_TYPES = new int[] { PAY_STATION_IS_VISIBLE, PAY_STATION_IS_HIDDEN, ACTIVATE_PAY_STATION,
        DEACTIVATE_PAY_STATION, EDIT_SETTINGS, CREATE_PAY_STATION, };
    
    public static final int MOVE_PAY_STATION = 607;
    
    public static final int CONFIGURATION_DOWNLOAD_STATUS = 608;
    public static final int ENCRYPTION_KEY_DOWNLOAD = 610;
    
    public static final int[] PAY_STATION_DOWNLOAD_TYPES = new int[] { CONFIGURATION_DOWNLOAD_STATUS, ENCRYPTION_KEY_DOWNLOAD };
    
    public static final int SOFTWARE_UPDATED = 609;
    public static final int TEST_ACTIVATE_PAY_STATION = 611;
    public static final int TEST_DEACTIVATE_PAY_STATION = 612;
    public static final int BILL_PAY_STATION_MONTHLY = 613;
    public static final int DO_NOT_BILL_PAY_STATION_MONTHLY = 614;
    public static final int PAY_STATION_HAS_BUNDLED_DATA = 615;
    public static final int PAY_STATION_DOES_NOT_HAVE_BUNDLED_DATA = 616;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int ROUTE_CONFIG = 700;
    
    /* Child Activity Types */
    public static final int ADD_ROUTE = 701;
    public static final int EDIT_ROUTE = 702;
    public static final int DELETE_ROUTE = 703;
    public static final int ASSIGN_PAY_STATION_TO_ROUTE = 704;
    
    public static final int[] ROUTE_CONFIG_TYPES = new int[] { EDIT_ROUTE, DELETE_ROUTE };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int ALERT_CONFIG = 800;
    
    /* Child Activity Types */
    public static final int ADD_ALERT = 801;
    
    public static final int EDIT_ALERT = 802;
    public static final int DELETE_ALERT = 803;
    
    public static final int[] ALERT_CONFIG_TYPES = new int[] { EDIT_ALERT, DELETE_ALERT };
    
    public static final int PAY_STATION_ALERT = 804;
    public static final int USER_DEFINED_ALERT = 805;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int EXTEND_BY_PHONE_CONFIG = 900;
    
    /* Child Activity Types */
    public static final int ADD_RATE = 901;
    public static final int EDIT_RATE = 902;
    public static final int DELETE_RATE = 903;
    
    public static final int[] RATE_CONFIG_TYPES = new int[] { ADD_RATE, EDIT_RATE, DELETE_RATE };
    
    public static final int ADD_POLICY = 904;
    public static final int EDIT_POLICY = 905;
    public static final int DELETE_POLICY = 906;
    
    public static final int[] POLICY_CONFIG_TYPES = new int[] { ADD_POLICY, EDIT_POLICY, DELETE_POLICY };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int USER_CONFIG = 1000;
    
    /* Child Activity Types */
    public static final int ADD_USER_ACCOUNT = 1001;
    public static final int EDIT_USER_ACCOUNT = 1002;
    public static final int DELETE_USER_ACCOUNT = 1003;
    
    public static final int[] USER_CONFIG_TYPES = new int[] { ADD_USER_ACCOUNT, EDIT_USER_ACCOUNT, DELETE_USER_ACCOUNT };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int ROLE_CONFIG = 1100;
    
    /* Child Activity Types */
    public static final int ADD_ROLE = 1101;
    public static final int EDIT_ROLE = 1102;
    public static final int DELETE_ROLE = 1103;
    //TODO: public static final int ASSIGN_ROLE = 1104?;
    //TODO: public static final int UNASSIGN_ROLE = 1105?;
    
    public static final int[] ROLE_CONFIG_TYPES = new int[] { ADD_ROLE, EDIT_ROLE, DELETE_ROLE };
    
    public static final int[] ROLE_ASSIGN_CONFIG_TYPES = new int[] {
    //TODO: assign user to role
    //TODO: remove user from role
    };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int REPORTING_CONFIG = 1200;
    
    /* Child Activity Types */
    public static final int ADD_SCHEDULED_REPORT = 1201;
    public static final int EDIT_SCHEDULED_REPORT = 1201;
    public static final int DELETE_SCHEDULED_REPORT = 1203;
    
    public static final int[] REPORTING_CONFIG_TYPES = new int[] { ADD_SCHEDULED_REPORT, EDIT_SCHEDULED_REPORT, DELETE_SCHEDULED_REPORT };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int COMPANY_PREFERENCES_CONFIG = 1300;
    
    /* Child Activity Types */
    public static final int EDIT_VALUE = 1301;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int DPT_ADMIN = 1400;
    
    /* Child Activity Types */
    public static final int PROCESS_NEXT_EXTERNALKEY = 1401;
    public static final int PROCESS_UPLOADED_FILES = 1402;
    public static final int PROCESS_CARD_RETRY = 1403;
    public static final int PROMOTE_SERVER = 1404;
    public static final int SHUTDOWN_TOWNCAT_SERVER = 1405;
    
    public static final int[] DPT_PROCESS_TYPES = new int[] { PROCESS_NEXT_EXTERNALKEY, PROCESS_CARD_RETRY };
    
    public static final int[] DPT_SERVER_CONFIG_TYPES = new int[] { PROCESS_UPLOADED_FILES, PROMOTE_SERVER, SHUTDOWN_TOWNCAT_SERVER };
    
    public static final int ADD_SYSTEM_NOTIFICATION = 1406;
    public static final int EDIT_SYSTEM_NOTIFICATION = 1407;
    public static final int DELETE_SYSTEM_NOTIFICATION = 1408;
    public static final int ARCHIVE_SYSTEM_NOTIFICATION = 1409;
    
    public static final int[] NOTIFICATION_TYPES = new int[] { ADD_SYSTEM_NOTIFICATION, EDIT_SYSTEM_NOTIFICATION, DELETE_SYSTEM_NOTIFICATION,
        ARCHIVE_SYSTEM_NOTIFICATION, };
    
    public static final int ADD_MERCHANT_ACCOUNT = 1410;
    public static final int EDIT_MERCHANT_ACCOUNT = 1411;
    public static final int DELETE_MERCHANT_ACCOUNT = 1412;
    
    public static final int[] MERCHANT_ACCOUNT_CONFIG_TYPES = new int[] { EDIT_MERCHANT_ACCOUNT, DELETE_MERCHANT_ACCOUNT };
    
    public static final int ADD_DIGITAL_API = 1413;
    public static final int EDIT_DIGITAL_API = 1414;
    public static final int DELETE_DIGITAL_API = 1415;
    
    public static final int[] DPT_API_TYPES = new int[] { ADD_DIGITAL_API, EDIT_DIGITAL_API, DELETE_DIGITAL_API };
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int DASHBOARD_WIDGET_CONFIG = 1500;
    
    /* Child Activity Types */
    public static final int ASSIGN_DEFAULT_DASHBAORD = 1501;
    public static final int CREATE_CUSTOMIZED_DASHBAORD = 1502;
    
    // ----------------------------------------------------------------------------
    
    /* Parent */
    public static final int CUSTOMER_AGREEMENG_CONFIG = 1600;
    
    /* Child Activity Types */
    public static final int ADD_CUSTOMER_AGREEMENT = 1601;
    public static final int EDIT_CUSTOMER_AGREEMENT = 1602;
    public static final int DELETE_CUSTOMER_AGREEMENT = 1603;
    
    private ActivityTypeConstants() {
        
    }
}
