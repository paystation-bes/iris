package com.digitalpaytech.util;

import java.util.Map;
import java.util.Iterator;
import java.util.List;

import com.digitalpaytech.domain.Processor;

public final class SystemAdminUtil {
    private SystemAdminUtil() {
    }
    
    /**
     * createProcessorNamesMap creates a java.util.Map that key is message key and value is database processor name. 
     * E.g. processor.direct.concord            |   Concord
     *      processor.direct.firstDataNashville |   First Data Nashville
     * @param map Empty java.util.Map<String, Processor>
     * @param messageKey Key value stores in message.properties.
     * @param processors List of Processor objects.
     * @return String Processor name.
     */
    public static Map<String, Processor> populateProcessorNamesMap(final Map<String, Processor> map,
                                                                   final String messageKey, 
                                                                   final List<Processor> processors) {
        final String key = messageKey.toLowerCase().trim();
        String processorNameNoSpace;
        final Iterator<Processor> iter = processors.iterator();
        while (iter.hasNext()) {
            final Processor pro = iter.next();
            processorNameNoSpace = pro.getName().toLowerCase().trim();
            if (processorNameNoSpace.indexOf(key) != -1) {
                map.put(messageKey, pro);
                break;
            }
        }
        return map;
    }

    public static boolean isCreditCardProcessor(final Processor processor) {
        if (!processor.isIsForValueCard() && !processor.isIsExcluded() && !processor.getIsEMV()) {
            return true;
        }
        return false;
    }
    
    public static boolean isEMVProcessor(final Processor processor) {
        return processor.getIsEMV();
    }
}
