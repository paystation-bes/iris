package com.digitalpaytech.util;

public class ComparableRange<T extends Comparable<T>> {
    private boolean openStart;
    private T start;
    
    private boolean openEnd;
    private T end;
    
    public ComparableRange(final T start, final boolean openStart, final T end, final boolean openEnd) {
        this.openStart = openStart;
        this.openEnd = openEnd;
        
        if (start.compareTo(end) <= 0) {
            this.start = start;
            this.end = end;
        } else {
            this.start = end;
            this.end = start;
        }
    }
    
    public final boolean isOverlapWith(final ComparableRange<T> other) {
        boolean result = false;
        final int startAfterEnd = this.end.compareTo(other.start);
        if (startAfterEnd == 0) {
            if (!this.openEnd && !other.openStart) {
                result = true;
            }
        } else if (startAfterEnd > 0) {
            final int endBeforeStart = other.end.compareTo(this.start);
            if (endBeforeStart == 0) {
                if (!this.openStart || !other.openEnd) {
                    result = true;
                }
            } else if (endBeforeStart > 0) {
                result = true;
            }
        }
        
        return result;
    }
    
    @Override
    public final String toString() {
        final StringBuilder result = new StringBuilder();
        result.append((this.openStart) ? "(" : "[");
        result.append(this.start);
        result.append(", ");
        result.append(this.end);
        result.append((this.openEnd) ? ")" : "]");
        
        return result.toString();
    }
    
    public final boolean isOpenStart() {
        return this.openStart;
    }
    
    public final T getStart() {
        return this.start;
    }
    
    public final boolean isOpenEnd() {
        return this.openEnd;
    }
    
    public final T getEnd() {
        return this.end;
    }
/*    
    public static void main(String[] args) {
        ComparableRange<Integer> first, second;
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(10, true, 11, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(9, true, 11, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(2, true, 5, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(0, true, 5, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(1, true, 5, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(6, true, 10, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(1, true, 10, true);
        second = new ComparableRange<Integer>(6, true, 13, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(-1, true, -10, true);
        second = new ComparableRange<Integer>(1, true, 10, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
        
        first = new ComparableRange<Integer>(5, true, -10, true);
        second = new ComparableRange<Integer>(1, true, 10, true);
        System.out.println(first + " VS " + second + " overlap: " + first.isOverlapWith(second));
    }
*/    
}
