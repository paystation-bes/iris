package com.digitalpaytech.util;

import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.List;

public final class CardProcessingConstants {
    public static final String PROCESSORS_PROPERTIES = "/processors.properties";
    
    public static final String AUTHORIZED_STRING = "Authorized";
    public static final String DENIED_STRING = "Denied";
    public static final String SUCCESS_STRING = "Success";
    public static final String BAD_CARD_STRING = "BadCard";
    public static final String INVALID_CARD_STRING = "InvalidCard";
    public static final String PROCESSOR_OFFLINE_STRING = "Offline";
    public static final String UNSUBSCRIBED_SERVICE_STRING = "UnsubscribedService";
    public static final String FAILED_STRING = "Failed";
    public static final String FAILED_GENERIC_ERROR = "Unknown Error";
    public static final String EMPTY_STRING = "";
    public static final String NA = "N/A";
    
    public static final int CARD_STATUS_BAD_CARD = 0;
    public static final int CARD_STATUS_DENIED = 1;
    public static final int CARD_STATUS_SUCCESS = 2;
    public static final int CARD_STATUS_EMS_UNAVAILABLE = 3;
    public static final int CARD_STATUS_AUTHORIZED = 4;
    public static final int CARD_STATUS_UNSUBSCRIBED_SERVICE = 5;
    public static final int CARD_STATUS_INVALID_CARD = 6;
    
    public static final String CARD_PROCESSOR_ERROR = "Card Processor";
    public static final long DEFAULT_MAX_REVERSAL_RETRIES = 120;
    
    public static final int NONE = 0;
    public static final int MOD10 = 1;
    public static final boolean CHECK_DIGIT_ALG_NONE = false;
    public static final boolean CHECK_DIGIT_ALG_MOD10 = true;
    
    // Digital Pioneer Reserved
    public static final String NAME_PATROLLER_CARD = "Patroller";
    public static final String PATROLLER_PREFIX = "456789012";
    
    public static final int CARD_TYPE_UNKNOWN = 0;
    public static final int CARD_TYPE_CREDIT_CARD = 1;
    public static final int CARD_TYPE_SMART_CARD = 2;
    public static final int CARD_TYPE_VALUE_CARD = 3;
    public static final int CARD_TYPE_PATROLLER_CARD = 4;
    
    public static final int CC_TYPE_AUTHORIZATION = 1;
    public static final int CC_TYPE_SETTLEMENT = 2;
    public static final int CC_TYPE_SALE = 3;
    public static final int CC_TYPE_REFUND = 4;
    public static final int CC_TYPE_REVERSAL = 5;
    
    public static final String MASK = "X";
    public static final char TRACK_2_DELIMITER = '=';
    public static final String TRACK_2_UNKNOWN = ".*";
    public static final String SINGLE_WILD_CARD = "?";
    public static final String MULTIPLE_WILD_CARD = "*";
    public static final String MYSQL_SINGLE_WILD_CARD = "_";
    public static final String MYSQL_MULTIPLE_WILD_CARD = "%";
    public static final String ESCAPE_CHARACTER = "\\";
    
    public static final String NAME_AMEX = "AMEX";
    public static final String NAME_DINERS = "Diners Club";
    public static final String NAME_DINERS_WO_CLUB = "Diners";
    public static final String NAME_DISCOVER = "Discover";
    public static final String NAME_MASTERCARD = "MasterCard";
    public static final String NAME_VISA = "VISA";
    public static final String NAME_CREDIT_CARD = "CreditCard";
    public static final String NAME_UNKNOWN = "Other";
    public static final String NAME_ENROUTE = "enRoute";
    public static final String NAME_JCB = "JCB";
    public static final String NAME_UNION_PAY = "UnionPay";
    
    // Default Credit Card Name list supported
    public static final List<String> DEFAULT_CREDIT_CARD_LIST =
            Arrays.asList(NAME_MASTERCARD, NAME_VISA, NAME_AMEX, NAME_DINERS, NAME_DISCOVER, NAME_JCB, NAME_ENROUTE, NAME_UNION_PAY);
    
    public static final String XML_RESPONSE_ROOT_ELEMENT_NAME = "Reply";
    
    // Credit Card Processor
    public static final String SIX_ZERO = "000000";
    public static final int NOT_ENCRYPTED_CARD_DATA_MAX_LENGTH = 40;
    public static final String FIRST_TWELVE_CARD_NUMBER = "XXXXXXXXXXXX";
    
    /* card form input fields */
    public static final int CREDIT_CARD_LAST_4_DIGITS_LENGTH = 4;
    public static final int CREDIT_CARD_MIN_LENGTH = 12;
    public static final int VALUE_CARD_MIN_LENGTH = 1;
    public static final int SMART_CARD_MIN_LENGTH = 1;
    public static final int CREDIT_CARD_MAX_LENGTH = 16;
    public static final int BAD_SMART_CARD_MAX_LENGTH = 20;
    public static final int BAD_VALUE_CARD_MAX_LENGTH = 37;
    public static final int LAST_4_DIGITS_LENGTH = 4;
    
    public static final String CHARSET_ISO_8859_1 = "ISO-8859-1";
    
    public static final long FIFTEEN_MINUTES_MS = 900000;
    public static final long MS_IN_A_DAY = 24 * 60 * 60 * 1000;
    public static final int DAYS_TO_ATTEMPT_SETTLEMENT = 7;
    public static final long MS_TO_ATTEMPT_SETTLEMENT = DAYS_TO_ATTEMPT_SETTLEMENT * MS_IN_A_DAY;
    
    public static final String SPACE_CHARACTER = " ";
    
    public static final int STATUS_BAD_CARD = 0;
    public static final int STATUS_DENIED = 1;
    public static final int STATUS_SUCCESS = 2;
    public static final int STATUS_EMS_UNAVAILABLE = 3;
    public static final int STATUS_AUTHORIZED = 4;
    public static final int STATUS_UNSUBSCRIBED_SERVICE = 5;
    public static final int STATUS_INVALID_CARD = 6;
    
    // 90 minutes
    public static final long PREAUTH_TIME_RANGE = 5400000;
    public static final String DEFAULT_TEST_TD_MERCHANT_CARD_NUMBER_EXPIRY_MONTH = "12";
    public static final String RFID_DATA = "1";
    
    public static final String ZIP_ENTRY_BAD_CREDITCARD = "badlist.bo2";
    
    //-----------------------------------------------------------------
    // Copied from EMS 6.3.11 com.digitalpioneer.cardprocessing.ProcessorTransactionType
    
    // Refer to ProcessorTransactionType database table.
    public static final int PROCESSOR_TRANSACTION_TYPE_NA = 0;
    public static final int PROCESSOR_TRANSACTION_TYPE_AUTHORIZED = 1;
    public static final int PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS = 2;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS = 3;
    public static final int PROCESSOR_TRANSACTION_TYPE_REFUND = 4;
    public static final int PROCESSOR_TRANSACTION_TYPE_REVERSAL = 5;
    public static final int PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS = 6;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS = 7;
    public static final int PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE = 8;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL = 9;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS = 10;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS = 11;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_REFUNDS = 12;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS = 13;
    public static final int PROCESSOR_TRANSACTION_TYPE_INVALID_DATA = 14;
    public static final int PROCESSOR_TRANSACTION_TYPE_REFUND_3RD_PARTY = 15;
    public static final int PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL_WITH_REFUND = 16;
    public static final int PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS = 17;
    public static final int PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND = 18;
    public static final int PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS = 19;
    public static final int PROCESSOR_TRANSACTION_TYPE_RECOVERABLE = 20;
    public static final int PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS = 21;
    public static final int PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_REFUND = 22;
    public static final int PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS = 23;
    public static final int PROCESSOR_TRANSACTION_TYPE_NOT_SETTLED = 23;
    public static final int PROCESSOR_TRANSACTION_TYPE_EXPIRED_REFUND = 24;
    public static final int PROCESSOR_TRANSACTION_TYPE_CANCELLED = 99;
    
    public static final int[] REFUNDED_PROCESSOR_TRANSACTION_TYPE_IDS =
            new int[] { CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS, };
    
    public static final int[] REFUNDABLE_PROCESSOR_TRANSACTION_TYPE_IDS =
            new int[] {
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS,
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS,
            };
    
    /**
     * Placeholder to prevent imports from BOSS(during upgrade to 6.2) for cancelled trans that
     * have authorization information due to this PS bug
     */
    public static final int TYPE_CANCELLED = 99;
    
    public static final String AUTHORIZED_TYPE_STRING = "Auth PlaceHolder";
    public static final String SETTLE_WITH_NO_REFUNDS_TYPE_STRING = "Real-Time";
    public static final String CHARGE_WITH_NO_REFUNDS_TYPE_STRING = "Batch";
    public static final String REFUND_TYPE_STRING = "Refund";
    public static final String REVERSAL_TYPE_STRING = "Reversal";
    public static final String SETTLE_WITH_REFUNDS_TYPE_STRING = "Settlement";
    public static final String CHARGE_WITH_REFUNDS_TYPE_STRING = "Charge";
    public static final String UNCLOSEABLE_TYPE_STRING = "Uncloseable";
    public static final String CHARGE_EXTERNAL_TYPE_STRING = "3rd Party";
    public static final String RECOVERABLE_TYPE_STRING = "Recoverable";
    
    public static final String CHARGE_EMS_RETRY_WITH_NO_REFUNDS_TYPE_STRING = "Batch";
    public static final String CHARGE_SF_WITH_NO_REFUNDS_TYPE_STRING = "Store & Fwd";
    public static final String CHARGE_EMS_RETRY_WITH_REFUNDS_TYPE_STRING = "Batch";
    public static final String CHARGE_SF_WITH_REFUNDS_TYPE_STRING = "Store & Fwd";
    public static final String INVALID_DATA_TYPE_STRING = "Encryption Error";
    public static final String TYPE_REFUND_3RD_PARTY_STRING = "Refund for 3rd Party";
    public static final String CHARGE_EXTERNAL_WITH_REFUND_TYPE_STRING = "3rd Party with Refunds";
    
    public static final String AUTHORIZED_STATUS_NAME = "Authorized";
    public static final String SETTLE_WITH_NO_REFUNDS_STATUS_NAME = "Real-Time";
    public static final String CHARGE_WITH_NO_REFUNDS_STATUS_NAME = "Batched";
    public static final String REFUND_TYPE_STATUS_NAME = "Refunded";
    public static final String REVERSAL_TYPE_STATUS_NAME = "Reversal";
    public static final String SETTLE_WITH_REFUNDS_STATUS_NAME = "Real-Time";
    public static final String CHARGE_WITH_REFUNDS_STATUS_NAME = "Batched";
    public static final String UNCLOSEABLE_STATUS_NAME = "Uncloseable";
    public static final String CHARGE_EXTERNAL_STATUS_NAME = "3rd Party";
    public static final String RECOVERABLE_STATUS_NAME = "Recoverable";
    
    public static final String CHARGE_EMS_RETRY_WITH_NO_REFUNDS_STATUS_NAME = "Batched";
    public static final String CHARGE_SF_WITH_NO_REFUNDS_STATUS_NAME = "Store & Fwd";
    public static final String CHARGE_EMS_RETRY_WITH_REFUNDS_STATUS_NAME = "Batched";
    public static final String CHARGE_SF_WITH_REFUNDS_STATUS_NAME = "Store & Fwd";
    public static final String INVALID_DATA_STATUS_NAME = "Encryption Error";
    public static final String TYPE_REFUND_3RD_PARTY_NAME = "Refund for 3rd Party";
    public static final String TYPE_CHARGE_EXTERNAL_WITH_REFUND_NAME = "3rd Party with Refunds";
    
    public static final String CPS_RETURNED_NULL = "Card Processing Server returned null response.";
    
    public static final int PROCESSOR_ID_FIRST_DATA_CONCORD = 1;
    public static final int PROCESSOR_ID_FIRST_DATA_NASHVILLE = 2;
    public static final int PROCESSOR_ID_HEARTLAND = 3;
    public static final int PROCESSOR_ID_LINK2GOV = 4;
    public static final int PROCESSOR_ID_PARCXMART = 5;
    public static final int PROCESSOR_ID_ALLIANCE = 6;
    public static final int PROCESSOR_ID_PAYMENTECH = 7;
    public static final int PROCESSOR_ID_PARADATA = 8;
    public static final int PROCESSOR_ID_MONERIS = 9;
    public static final int PROCESSOR_ID_FIRST_HORIZON = 10;
    public static final int PROCESSOR_ID_AUTHORIZE_NET = 11;
    public static final int PROCESSOR_ID_DATAWIRE = 12;
    public static final int PROCESSOR_ID_BLACK_BOARD = 13;
    public static final int PROCESSOR_ID_TOTAL_CARD = 14;
    public static final int PROCESSOR_ID_NU_VISION = 15;
    public static final int PROCESSOR_ID_ELAVON = 16;
    public static final int PROCESSOR_ID_CBORD = 17;
    public static final int PROCESSOR_ID_CBORD_ODYSSEY = 18;
    public static final int PROCESSOR_ID_ELAVON_VIACONEX = 19;
    public static final int PROCESSOR_ID_CREDITCALL = 20;
    public static final int PROCESSOR_ID_TD_MERCHANT = 21;
    public static final int PROCESSOR_ID_HEARTLAND_SPPLUS = 22;
    public static final int PROCESSOR_ID_CREDITCALL_LINK = 23;
    
    public static final int[] EXCLUDED_PROCESSOR_IDS = { PROCESSOR_ID_ELAVON_VIACONEX };
    public static final int[] ONE_MERCHANT_ACCOUNT_ONE_POS_PROCESSOR_IDS = { PROCESSOR_ID_ELAVON_VIACONEX };
    public static final int[] SINGLE_PHASE_PROCESSOR_IDS = { PROCESSOR_ID_HEARTLAND_SPPLUS };
    
    public static final int ROTATION_STATUS_ID_EMS6_KEY_CREATED = 0;
    public static final int ROTATION_STATUS_ID_EMS6_REENCRYPTION_STARTED = 1;
    public static final int ROTATION_STATUS_ID_IRIS_KEY_MISSING = 2;
    public static final int ROTATION_STATUS_ID_IRIS_REENCRYPTION_STARTED = 3;
    public static final int ROTATION_STATUS_ID_REENCRYPTION_COMPLETED = 4;
    
    public static final int CC_TYPE_ID_AMEX = 4;
    public static final int CC_TYPE_ID_VISA = 2;
    public static final int CC_TYPE_ID_MASTERCARD = 3;
    public static final int CC_TYPE_ID_DISCOVER = 5;
    public static final int CC_TYPE_ID_DINERS = 6;
    public static final int CC_TYPE_ID_CREDIT_CARD = 7;
    public static final int CC_TYPE_ID_JCB = 9;
    public static final int CC_TYPE_ID_UNION_PAY = 10;
    
    public static final int CARD_RETRY_TX_TYPE_BATCH = 0;
    public static final int CARD_RETRY_TX_TYPE_S_AND_F = 1;
    
    public static final String RESULT_OK = "OK";
    public static final String RESULT_STATUS_A = "A";
    public static final String RESULT_GOOD_BATCH_RESPONSE_1 = "GBOK";
    public static final String RESULT_GOOD_BATCH_RESPONSE_2 = "GB OK";
    public static final String RESULT_DECLINED_BATCH_RESPONSE = "ND";
    
    public static final int BATCH_STATUS_TYPE_OPEN = 1;
    public static final int BATCH_STATUS_TYPE_CLOSE = 2;
    public static final int BATCH_STATUS_TYPE_PARTIALLY_CLOSED = 3;
    public static final int BATCH_STATUS_TYPE_PARTIALLY_RB = 4;
    public static final int BATCH_STATUS_TYPE_FAILED = 5;
    public static final int BATCH_STATUS_TYPE_IN_PROCESS = 6;
    
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_PREAUTH_ID = 0;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID = 1;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID = 2;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_FAILED_TO_SETTLE_ID = 3;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_CANNOT_SETTLE_ID = 4;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_EXPIRED_ID = 5;
    public static final int TRANSACTION_SETTLEMENT_STATUS_TYPE_EXCLUDED_ID = 6;
    
    public static final int BATCH_SETTLEMENT_TYPE_NO_OF_TRANZACTIONS = 1;
    public static final int BATCH_SETTLEMENT_TYPE_TIME_OF_DAY = 2;
    public static final int BATCH_SETTLEMENT_TYPE_REFUND = 3;
    
    // Beanstream
    public static final int BEANSTREAM_COMPLETION_GREATER_THAN_REMAINING_CODE = 208;
    public static final int BEANSTREAM_COMPLETION_GREATER_THAN_REMAINING_CATEGORY = 2;
    public static final int BEANSTREAM_TRANSACTION_SEARCH_RECORD_NOT_FOUND_CODE = 9;
    public static final int BEANSTREAM_TRANSACTION_SEARCH_RECORD_NOT_FOUND_CATEGORY = 3;

    // Creditcall responses
    public static final String[] TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES = new String[] { 
        "transactionalreadyrefunded", "transaction already refunded",
        "transactionalreadyvoided", "transaction already voided",
    };
    public static final String[] TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES = new String[] { 
        "transactionalreadysettled", "transaction already settled",
    };
    
    /*
     * Temporary CardExpiry value in CardRetryTransaction that would satisfy
     * 'Restrictions.ge(CardRetryTransaction.COLUMN_NAME_CARD_EXPIRY, cardExpiry)' <- 'cardExpiry' = current year + month rule.
     * It's for CardRetryTransaction records that are supposed to send to Core CPS but failed.
     */
    public static final short MAX_YEAR_MONTH_VALUE = 9912;
    
    //-----------------------------------------------------------------
    // Copied from EMS 6.3.11 com.digitalpioneer.cardprocessing.CardRetryTransactionType
    public static final int TYPE_OFFLINE = 0;
    public static final int TYPE_STORE_FORWARD = 1;
    
    // fixme ensure refunds, reports, boss processing look at new types
    
    public static final String OFFLINE_TYPE_STRING = "Batch";
    public static final String STORE_FORWARD_TYPE_STRING = "Store & Forward";
    
    public static final short CC_RETRY_LOST_STOLEN = Short.MAX_VALUE;
    public static final short CC_RETRY_MAX_EXCEEDED = Short.MAX_VALUE - 1;
    
    public static final int AURA_CHARGE_SUCCEED = 0;
    public static final int AURA_CHARGE_DECLINE = 1;
    public static final int AURA_CHARGE_UNKNOWN = 2;
    public static final int AURA_CHARGE_SEVER_SHUTDOWN = 3;
    
    public static final Pattern UUID_PATTERN = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
    public static final String CHARGE_TOKEN = "chargeToken";
    public static final String PROC_TRANS_TYPE = "processorTransactionType";
    public static final String CUSTOMER = "customer";
    public static final String APPROVED = "Approved";
    
    private CardProcessingConstants() {
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    public static String getProcessorTransactionTypeName(final int type) {
        switch (type) {
            case PROCESSOR_TRANSACTION_TYPE_AUTHORIZED:
                return AUTHORIZED_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS:
                return CHARGE_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_REFUND:
                return REFUND_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_REVERSAL:
                return REVERSAL_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS:
                return CHARGE_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE:
                return UNCLOSEABLE_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL:
                return CHARGE_EXTERNAL_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS:
                return CHARGE_EMS_RETRY_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS:
                return CHARGE_SF_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_REFUNDS:
                return CHARGE_EMS_RETRY_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS:
                return CHARGE_SF_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_INVALID_DATA:
                return INVALID_DATA_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_REFUND_3RD_PARTY:
                return TYPE_REFUND_3RD_PARTY_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL_WITH_REFUND:
                return CHARGE_EXTERNAL_WITH_REFUND_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND:
                return REFUND_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_RECOVERABLE:
                return RECOVERABLE_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_REFUND:
                return REFUND_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_TYPE_STRING;
            default:
                break;
        }
        return WebCoreConstants.UNKNOWN;
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    public static String getStatusName(final int type) {
        switch (type) {
            case PROCESSOR_TRANSACTION_TYPE_AUTHORIZED:
                return AUTHORIZED_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS:
                return CHARGE_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_REFUND:
                return REFUND_TYPE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_REVERSAL:
                return REVERSAL_TYPE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS:
                return CHARGE_WITH_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE:
                return UNCLOSEABLE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL:
                return CHARGE_EXTERNAL_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS:
                return CHARGE_EMS_RETRY_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS:
                return CHARGE_SF_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_REFUNDS:
                return CHARGE_EMS_RETRY_WITH_REFUNDS_TYPE_STRING;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS:
                return CHARGE_SF_WITH_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_INVALID_DATA:
                return INVALID_DATA_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_REFUND_3RD_PARTY:
                return TYPE_REFUND_3RD_PARTY_NAME;
            case PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL_WITH_REFUND:
                return TYPE_CHARGE_EXTERNAL_WITH_REFUND_NAME;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND:
                return REFUND_TYPE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_RECOVERABLE:
                return RECOVERABLE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS:
                return SETTLE_WITH_NO_REFUNDS_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_REFUND:
                return REFUND_TYPE_STATUS_NAME;
            case PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS:
                return SETTLE_WITH_REFUNDS_STATUS_NAME;
            default:
                break;
        }
        return WebCoreConstants.UNKNOWN;
    }
    
    public static String getCardRetryTransactionTypeName(final int type) {
        switch (type) {
            case TYPE_OFFLINE:
                return OFFLINE_TYPE_STRING;
            case TYPE_STORE_FORWARD:
                return STORE_FORWARD_TYPE_STRING;
            default:
                break;
        }
        return WebCoreConstants.UNKNOWN;
    }
}
