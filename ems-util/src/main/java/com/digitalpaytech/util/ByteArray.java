package com.digitalpaytech.util;

import java.io.Serializable;

public class ByteArray implements Serializable {
    private static final long serialVersionUID = 937984122877811511L;
    
    private static final int BITSHIFT_TO_MULTIPLY_32 = 5;
    
    private byte[] data;
    
    public ByteArray(final byte[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Byte Array cannot be null !");
        }
        
        this.data = data;
    }
    
    @Override
    public final int hashCode() {
        int result = 0;
        int idx = this.data.length;
        while (--idx >= 0) {
            // Optimized version of (result * 31) + this.data[idx];
            result = (result << BITSHIFT_TO_MULTIPLY_32) - result + this.data[idx];
        }
        
        return result;
    }
    
    @Override
    public final boolean equals(final Object obj) {
        boolean result = false;
        if (obj instanceof ByteArray) {
            final byte[] otherData = ((ByteArray) obj).get();
            if (this.data == otherData) {
                result = true;
            } else if (this.data.length == otherData.length) {
                result = true;
                int idx = this.data.length;
                while (result && (--idx >= 0)) {
                    result = this.data[idx] == otherData[idx];
                }
            }
        }
        
        return result;
    }
    
    public final byte[] get() {
        return this.data;
    }
    
    @Override
    public final String toString() {
        String result;
        if (this.data == null) {
            result = super.toString();
        } else if (this.data.length <= 0) {
            result = "[ ]";
        } else {
            int i = 0;
            final StringBuilder resultBuffer = new StringBuilder();
            resultBuffer.append("[");
            resultBuffer.append(this.data[0]);
            while (++i < this.data.length) {
                resultBuffer.append(",");
                resultBuffer.append(this.data[i]);
            }
            
            resultBuffer.append("]");
            
            result = resultBuffer.toString();
        }
        
        return result;
    }
}
