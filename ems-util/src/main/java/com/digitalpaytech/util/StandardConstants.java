package com.digitalpaytech.util;

import java.math.BigDecimal;

public final class StandardConstants {
    
    public static final long DAYS_IN_MILLIS_1 = 86400000;
    public static final int HOURS_IN_MILLIS_1 = 3600000;
    public static final int MINUTES_IN_MILLIS_1 = 60000;
    public static final int MINUTES_IN_MILLIS_2 = 120000;
    public static final int SECONDS_IN_MILLIS_1 = 1000;
    public static final int SECONDS_IN_MILLIS_3 = 3000;
    public static final int SECONDS_IN_MILLIS_10 = 10000;
    
    public static final int DAYS_IN_A_WEEK = 7;
    public static final int WEEKS_IN_DAYS_2 = 14;
    public static final int DAYS_IN_A_YEAR = 365;
    
    public static final int MINUTES_IN_SECS_1 = 60;
    public static final double MINUTES_IN_SECS_1_DBL = 60d;
    public static final int HOURS_IN_MINS_1 = 60;
    public static final int LAST_MIN_SECOND = 59;
    public static final int LAST_HOUR_OF_THE_DAY = 23;
    public static final int HOURS_IN_DAYS_1 = 24;
    
    public static final int MONTHS_IN_A_YEAR = 12;
    public static final int DAYS_29 = 29;
    public static final int MAX_DAYS_IN_MONTH = 31;
    public static final float AVG_DAYS_IN_MONTH = 30.5F; 
    public static final int WEEKS_IN_MONTH_5 = 5;
    
    public static final int ONE_KILOBYTE = 1024;
    
    /* amount calculations */
    public static final int DOLLAR_AMOUNT_IN_CENTS_1 = 100;
    public static final float DOLLAR_AMOUNT_IN_CENTS_FLOAT_1 = 100f;
    
    public static final float FLOAT_THOUSAND = 1000f;
    public static final float FLOAT_HUNDRED = 100f;
    
    public static final int LIMIT_5 = 5;
    public static final int LIMIT_10 = 10;
    public static final int LIMIT_15 = 15;
    public static final int LIMIT_20 = 20;
    public static final int LIMIT_30 = 30;
    
    public static final int COIN_5 = 5;
    public static final int COIN_10 = 10;
    public static final int COIN_25 = 25;
    public static final int COIN_100 = 100;
    public static final int COIN_200 = 200;
    
    public static final int CONSTANT_0 = 0;
    public static final int CONSTANT_1 = 1;
    public static final int CONSTANT_2 = 2;
    public static final int CONSTANT_3 = 3;
    public static final int CONSTANT_4 = 4;
    public static final int CONSTANT_5 = 5;
    public static final int CONSTANT_6 = 6;
    public static final int CONSTANT_7 = 7;
    public static final int CONSTANT_8 = 8;
    public static final int CONSTANT_9 = 9;
    public static final int CONSTANT_10 = 10;
    public static final int CONSTANT_11 = 11;
    public static final int CONSTANT_12 = 12;
    public static final int CONSTANT_20 = 20;
    public static final int CONSTANT_28 = 28;
    public static final int CONSTANT_30 = 30;
    public static final int CONSTANT_31 = 31;
    public static final int CONSTANT_40 = 40;
    public static final int CONSTANT_100 = 100;
    public static final int CONSTANT_999 = 999;
    public static final int CONSTANT_1000 = 1000;
    public static final int CONSTANT_2000 = 2000;
    public static final int CONSTANT_2002 = 2002;
    public static final int CONSTANT_2100 = 2100;
    public static final int CONSTANT_3000 = 3000;
    
    public static final int INT_MINUS_ONE = -1;
    
    public static final String STRING_COMMA = ",";
    public static final String STRING_DOT = ".";
    public static final String STRING_DASH = "-";
    public static final String STRING_QUOTE = "'";
    public static final String STRING_EMPTY_SPACE = " ";
    public static final String STRING_EMPTY_STRING = "";
    public static final String STRING_COLON = ":";
    public static final String STRING_SEMICOLON = ";";
    public static final String STRING_UNDERSCORE = "_";
    public static final String STRING_DOUBLE_QUOTES = "\"";
    public static final String STRING_END_OF_LINE = "\n";
    public static final String STRING_OPEN_PARENTHESIS = "(";
    public static final String STRING_CLOSE_PARENTHESIS = ")";
    public static final String STRING_OPEN_BRACKET = "[";
    public static final String STRING_CLOSE_BRACKET = "]";
    public static final String STRING_FOUR_STARS = "****";
    public static final String STRING_ZERO = "0";
    public static final String STRING_ONE = "1";
    public static final String STRING_TWO = "2";
    public static final String STRING_MINUS_ONE = "-1";
    public static final String STRING_FOUR_ZEROS = "0000";
    public static final String STRING_FOUR_NUMBER_SIGNS = "####";
    public static final String STRING_PUBLIC = "public";
    public static final String STRING_PERCENTAGE = "%";
    public static final String STRING_NA_PARANTHESIS = "(n/a)";
    public static final String STRING_SLASH = "/";
    public static final String STRING_CURLY_BRACE_OPEN = "{";
    public static final String STRING_CURLY_BRACE_CLOSE = "}";
    public static final String REGEX_NEWLINE = "\\r?\\n";
    
    public static final String STRING_LABEL_LINE_SEPARATOR = "line.separator";
    
    public static final String STRING_EXCLAMATION_POINT_PERCENT_SIGN = "!%";
    public static final String STRING_EXCLAMATION_POINT_UNDERSCORE = "!_";
    public static final String STRING_EXCLAMATION_POINT = "!";
    public static final String STRING_COMMA_SPACE = ", ";
    public static final String STRING_NULL = "null";
    public static final String STRING_EQAL = "=";
    public static final String STRING_AT_SIGN = "@";
    
    public static final String STRING_NEXT_LINE = "\r\n";
    public static final String STRING_TAB = "\r\t";
    
    public static final String STRING_YEAR = "year";
    public static final String STRING_MONTH = "month";
    public static final String STRING_DAY = "day";
    public static final String STRING_HOUR = "hour";
    
    public static final String STRING_JANUARY = "January";
    public static final String STRING_FEBRUARY = "February";
    public static final String STRING_MARCH = "March";
    public static final String STRING_APRIL = "April";
    public static final String STRING_MAY = "May";
    public static final String STRING_JUNE = "June";
    public static final String STRING_JULY = "July";
    public static final String STRING_AUGUST = "August";
    public static final String STRING_SEPTEMBER = "September";
    public static final String STRING_OCTOBER = "October";
    public static final String STRING_NOVEMBER = "November";
    public static final String STRING_DECEMBER = "December";
    
    public static final String STRING_ACTIVE = "active";
    public static final String STRING_SELECTED = "selected";
    
    public static final String STRING_STATUS_CODE_ACCEPTED = "accepted";
    public static final String STRING_STATUS_CODE_DECLINED = "declined";
    public static final String STRING_STATUS_CODE_NOT_PROCESSED = "notprocessed";
    public static final String STRING_STATUS_CODE_IGNORED = "ignored";
    
    public static final String STRING_X = "X";
    
    public static final String SEPARATOR_URL = "/";
    
    public static final char CHAR_COMMA = ',';
    public static final char CHAR_DASH = '-';
    public static final char CHAR_QUOTE = '\'';
    public static final char CHAR_EMPTY_SPACE = ' ';
    public static final char CHAR_COLON = ':';
    public static final char CHAR_SEMICOLON = ';';
    public static final char CHAR_UNDERSCORE = '_';
    public static final char CHAR_DOT = '.';
    public static final char CHAR_EQUALS = '=';
    public static final char CHAR_CURLY_BRACE_OPEN = '{';
    
    public static final String SYSTEM_NEW_LINE = "line.separator";
    
    public static final String HTTP_UTF_8_ENCODE_CHARSET = "UTF-8";
    
    public static final BigDecimal BIGDECIMAL_100 = new BigDecimal(100);
    
    public static final int BYTES_PER_MEGABYTE = 1048576;
    
    public static final String FORMAT_TWO_DECIMALS = "%.2f";
    
    private StandardConstants() {
    }
}
