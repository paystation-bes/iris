package com.digitalpaytech.util;

import org.apache.commons.beanutils.PropertyUtils;

public class ReflectionUtil {
	public static Object getAttribute(Object bean, String attributeName) {
		Object result = null;
		try {
			result = PropertyUtils.getProperty(bean, attributeName);
		}
		catch(Exception e) {
			throw new RuntimeException("Could not call getter", e);
		}
		
		return result;
	}
}
