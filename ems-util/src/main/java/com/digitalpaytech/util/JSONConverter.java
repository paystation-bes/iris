package com.digitalpaytech.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.digitalpaytech.util.queue.MessageConverter;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JSONConverter implements MessageConverter {
    private ObjectMapper jsonMapper;
    
    private boolean ignoreClassName;
    
    public JSONConverter() {
        this(false);
    }
    
    public JSONConverter(final boolean ignoreClassName) {
        this.ignoreClassName = ignoreClassName;
        
        this.jsonMapper = new ObjectMapper();
        this.jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    @Override
    public String toMessage(final Object obj) {
        return toJSON(obj);
    }

    public String toJSON(final Object obj) {
        final StringWriter result = new StringWriter();
        if ((!this.ignoreClassName) && (obj != null)) {
            result.append(obj.getClass().getName());
        }
        
        try {
            this.jsonMapper.writeValue(result, obj);
        } catch (JsonMappingException jme) {
            throw new RuntimeException("Unable to map object '" + obj + "' to json", jme);
        } catch (JsonGenerationException jge) {
            throw new RuntimeException("Failed to generate json for object '" + obj + "'", jge);
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to generate json for object '" + obj + "'", ioe);
        }
        
        return result.toString();
    }
    
    @Override
    public Object toObject(final String json) {
        Object result = null;
        
        final int startIdx = (json == null) ? -1 : json.indexOf("{");
        if (startIdx > 0) {
            final String className = json.substring(0, startIdx);
            Class<?> targetClass = null;
            try {
                targetClass = Class.forName(className);
            } catch (ClassNotFoundException cnfe) {
                throw new RuntimeException("Could not load class: " + className);
            }
            
            result = toObject(json.substring(startIdx), targetClass);
        }
        
        return result;
    }
    
    public <T> T toObject(final String json, final Class<T> type) {
        T result = null;
        try {
            result = this.jsonMapper.readValue(json, type);
        } catch (JsonMappingException jme) {
            throw new RuntimeException("Unable to map json to object of class: " + type.getName());
        } catch (JsonParseException jpe) {
            throw new RuntimeException("Failed to generate object from json string", jpe);
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to generate object from json string", ioe);
        }
        
        return result;
    }
    
    public static void main(String... args) {
        JSONConverter cnvrtr = new JSONConverter(true);
        Map<String, String> data = new HashMap<String, String>();
        data.put("x", "1");
        data.put("y", "2");
        
        System.out.println(cnvrtr.toJSON(data));
    }
}
