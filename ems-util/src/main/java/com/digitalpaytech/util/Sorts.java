package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Sorts {
	public static <T> List<T> sortSeparateSortedList(List<T> original, Comparator<T> comparator, int firstOffset, int secondOffset) {
		ArrayList<T> result = new ArrayList<T>(original.size());
		int firstIdx = -1;
		while(++firstIdx < firstOffset) {
			result.add(original.get(firstIdx));
		}
		
		int secondIdx = secondOffset;
		while((firstIdx < secondOffset) && (secondIdx < original.size())) {
			if(comparator.compare(original.get(firstIdx), original.get(secondIdx)) <= 0) {
				result.add(original.get(firstIdx++));
			}
			else {
				result.add(original.get(secondIdx++));
			}
		}
		
		while(firstIdx < secondOffset) {
			result.add(original.get(firstIdx++));
		}
		
		while(secondIdx < original.size()) {
			result.add(original.get(secondIdx++));
		}
		
		return result;
	}
}
