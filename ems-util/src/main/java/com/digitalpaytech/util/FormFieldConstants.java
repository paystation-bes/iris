package com.digitalpaytech.util;

public final class FormFieldConstants {
    
    public static final String COMMENTS = "comments";
    
    public static final String MIGRATION_CONTACTS = "migrationContacts";
    public static final String CUSTOMER_MIGRATION = "customerMigration";
    public static final String SCHEDULE_DATE = "scheduleDate";
    public static final String SCHEDULE_TIME = "scheduleTime";
    public static final String SETTINGS_RECEIVED_DATE = "settingsReceivedDate";
    public static final String FILE = "file";
    public static final String IMPORT_MODE = "importMode";
    public static final String REPLACE_MODE = "Replace";
    public static final String LABEL_IMPORT_MODE = "label.import.mode";
    
    public static final String PARAMETER_CUSTOMER_ID = "customerID";
    public static final String PARAMETER_RANDOM_ID = "randomId";
    public static final String PARAMETER_SEARCH = "search";
    public static final String PARAMETER_IS_PARENT = "isParent";
    public static final String PARAMETER_USER_ACCOUNT_ID = "userAccountID";
    public static final String PARAMETER_ROLE_ID = "roleID";
    public static final String PARAMETER_CHILD_PERMISSION_ID = "childPermissionId";
    public static final String PARAMETER_GROUP_ID = "groupId";
    public static final String PARAMETER_UNSCHEDULE_CHECK = "unscheduleCheck";
    
    private FormFieldConstants() {
    }
}
