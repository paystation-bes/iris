package com.digitalpaytech.util.scripting.operator;

import java.util.Map;

import com.digitalpaytech.util.scripting.CalculableNode;

public class LogicalNOT extends SingleOperator<Boolean> {
	private static final long serialVersionUID = 5531603427586692450L;

	public LogicalNOT() {
		
	}
	
	public LogicalNOT(CalculableNode<Boolean> operand) {
		super(operand);
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		return !this.getOperand().execute(context);
	}

	@Override
	public boolean isPrefixOperator() {
		return true;
	}

	@Override
	protected String getStringRepresentation() {
		return "!";
	}
}
