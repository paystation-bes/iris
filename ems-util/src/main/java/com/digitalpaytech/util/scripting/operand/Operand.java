package com.digitalpaytech.util.scripting.operand;

import com.digitalpaytech.util.scripting.CalculableNode;

public abstract class Operand<R> extends CalculableNode<R>{
	private static final long serialVersionUID = 5366920114794193553L;

	protected Operand() {
		
	}

	@Override
	public boolean isOperator() {
		return false;
	}
	
	@Override
	public boolean isGroupingOperator() {
		return false;
	}

	@Override
	public boolean isPrefixOperator() {
		return false;
	}

	@Override
	public boolean canAcceptMoreOperand() {
		return false;
	}

	@Override
	public void addOperand(CalculableNode<R> operand) {
		throw new UnsupportedOperationException();
	}
}
