package com.digitalpaytech.util.scripting.operator;

import java.text.MessageFormat;

import com.digitalpaytech.util.scripting.CalculableNode;

public abstract class TupleOperator<R> extends Operator<R> {
	private static final long serialVersionUID = 1691542802872256717L;
	
	private static final String PRINT_PATTERN = "({1} {0} {2})";
	
	private CalculableNode<R> left;
	private CalculableNode<R> right;
	
	protected TupleOperator() {
		
	}
	
	protected TupleOperator(CalculableNode<R> left, CalculableNode<R> right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return MessageFormat.format(PRINT_PATTERN, this.getStringRepresentation(), this.left, this.right);
	}

	@Override
	public boolean isGroupingOperator() {
		return false;
	}

	@Override
	public boolean canAcceptMoreOperand() {
		return (this.left == null) || (this.right == null);
	}

	@Override
	public void addOperand(CalculableNode<R> operand) {
		if(this.left == null) {
			this.left = operand;
		}
		else if(this.right == null) {
			this.right = operand;
		}
		else {
			throw new IllegalStateException("This operator can accept only two operands !");
		}
	}

	@Override
	public CalculableNode<R> switchOperand(CalculableNode<R> newOperand) {
		if(this.right == null) {
			throw new IllegalStateException("The right operand has not been set !");
		}
		
		CalculableNode<R> result = this.right;
		this.right = newOperand;
		
		return result;
	}

	public CalculableNode<R> getLeft() {
		return left;
	}

	public void setLeft(CalculableNode<R> left) {
		this.left = left;
	}

	public CalculableNode<R> getRight() {
		return right;
	}

	public void setRight(CalculableNode<R> right) {
		this.right = right;
	}
}
