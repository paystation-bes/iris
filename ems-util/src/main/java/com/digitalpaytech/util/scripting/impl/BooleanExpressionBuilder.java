package com.digitalpaytech.util.scripting.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.digitalpaytech.util.scripting.CalculableNode;
import com.digitalpaytech.util.scripting.ExpressionBuilder;
import com.digitalpaytech.util.scripting.InvalidExpressionException;
import com.digitalpaytech.util.scripting.operand.LogicalConstant;
import com.digitalpaytech.util.scripting.operand.Operand;
import com.digitalpaytech.util.scripting.operator.GroupingOperator;
import com.digitalpaytech.util.scripting.operator.LogicalAND;
import com.digitalpaytech.util.scripting.operator.LogicalNOT;
import com.digitalpaytech.util.scripting.operator.LogicalOR;
import com.digitalpaytech.util.scripting.operator.Operator;

public abstract class BooleanExpressionBuilder implements ExpressionBuilder<CharSequence, Boolean> {
	protected static final Pattern PTTRN_BOOLEAN = Pattern.compile("(true|1)|false|0", Pattern.CASE_INSENSITIVE);
	
	public static final Operand<Boolean> OPERAND_TRUE = new LogicalConstant(true);
	public static final Operand<Boolean> OPERAND_FALSE = new LogicalConstant(false);
	
	protected BooleanExpressionBuilder() {
		
	}
	
	@Override
	public int compare(Operator<Boolean> o1, Operator<Boolean> o2) {
		int result = -1;
		if(o1.getClass().isAssignableFrom(o2.getClass()) || o2.getClass().isAssignableFrom(o1.getClass())) {
			result = 0;
		}
		else if(o1 instanceof GroupingOperator) {
			result = 1;
		}
		else if(o1 instanceof LogicalNOT) {
			result = (o2 instanceof GroupingOperator) ? -1 : 1;
		}
		else if(o1 instanceof LogicalAND) {
			result = (o2 instanceof LogicalOR) ? 1 : -1;
		}
		else if(o1 instanceof LogicalOR) {
			result = -1;
		}
		
		return result;
	}

	@Override
	public CalculableNode<Boolean> createOperator(CharSequence input)
			throws InvalidExpressionException {
		Operator<Boolean> result = null;
		
		char c = input.charAt(0);
		switch(c) {
			case '&': {
				result = new LogicalAND();
				break;
			}
			case '|': {
				result = new LogicalOR();
				break;
			}
			case '!': {
				result = new LogicalNOT();
				break;
			}
			default: {
				throw new InvalidExpressionException();
			}
		}
		
		return result;
	}

	@Override
	public CalculableNode<Boolean> createOperand(CharSequence input)
			throws InvalidExpressionException {
		Operand<Boolean> result = null;
		
		Matcher m = PTTRN_BOOLEAN.matcher(input);
		if(!m.matches()) {
			throw new InvalidExpressionException();
		}
		else {
			if(m.group(1) != null) {
				result = OPERAND_TRUE;
			}
			else {
				result = OPERAND_FALSE;
			}
		}
		
		return result;
	}

	@Override
	public Operand<Boolean> createDefault() {
		return new LogicalConstant(false);
	}
}
