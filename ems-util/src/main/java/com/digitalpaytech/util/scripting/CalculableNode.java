package com.digitalpaytech.util.scripting;

import java.io.Serializable;
import java.util.Map;

import com.digitalpaytech.util.scripting.operator.GroupingOperator;
import com.digitalpaytech.util.scripting.operator.Operator;
import com.digitalpaytech.util.scripting.operator.TupleOperator;

public abstract class CalculableNode<R> implements Serializable {
	private static final long serialVersionUID = 3065333656549362473L;

	public abstract boolean isOperator();
	public abstract boolean isGroupingOperator();
	public abstract boolean isPrefixOperator();
	
	public abstract boolean canAcceptMoreOperand();
	
	public abstract void addOperand(CalculableNode<R> operand);
	
	public abstract R execute(Map<String, Object> context);
	
	protected CalculableNode() {
		
	}
	
	public static <I, O> CalculableNode<O> createOperation(ExpressionTokenizer<I, O> tokenizer, ExpressionBuilder<I, O> builder) throws InvalidExpressionException, InvalidTokenException {
		return createOperation(tokenizer, builder, 0);
	}
	
	private static <I, O> CalculableNode<O> createOperation(ExpressionTokenizer<I, O> tokenizer, ExpressionBuilder<I, O> builder, int level) throws InvalidExpressionException, InvalidTokenException {
		CalculableNode<O> root = null;
		
		I rawCurr = null;
		if(!tokenizer.hasNext()) {
			throw new InvalidExpressionException("Expecting either operand, operator, or start of group");
		}
		else {
			rawCurr = tokenizer.next();
			if(tokenizer.isOperand()) {
				root = builder.createOperand(rawCurr);
			}
			else if(tokenizer.isGroupStart()) {
				root = new GroupingOperator<O>(createOperation(tokenizer, builder, level + 1));
			}
			else if(tokenizer.isOperator()) {
				root = builder.createOperator(rawCurr);
				if(!root.isPrefixOperator()) {
					throw new InvalidExpressionException("The operator \"" + rawCurr + "\" is not allowed at the beginning of expression");
				}
			}
			else {
				throw new InvalidExpressionException("Un-recognized symbol \"" + rawCurr + "\"");
			}
		}
		
		CalculableNode<O> prev = root;
		boolean inGroup = true;
		while(inGroup && tokenizer.hasNext()) {
			rawCurr = tokenizer.next();
			if(tokenizer.isOperand()) {
				if((!prev.isOperator()) || (!prev.canAcceptMoreOperand())) {
					throw new InvalidExpressionException("An operand \"" + rawCurr + "\" is not allowed after \"" + prev + "\"");
				}
				
				prev.addOperand(builder.createOperand(rawCurr));
			}
			else if(tokenizer.isGroupStart()) {
				if((!prev.isOperator()) || (!prev.canAcceptMoreOperand())) {
					throw new InvalidExpressionException("Group start character \"" + rawCurr + "\" is not allowed after \"" + prev + "\"");
				}
				
				prev.addOperand(new GroupingOperator<O>(createOperation(tokenizer, builder, level + 1)));
			}
			else if(tokenizer.isGroupEnd()) {
				inGroup = false;
				if(prev.canAcceptMoreOperand()) {
					throw new InvalidExpressionException("The current group \"" + rawCurr + "\" still allow more sub-elements");
				}
				if(level <= 0) {
					throw new InvalidExpressionException("Un-expected end of group \"" + rawCurr + "\"");
				}
			}
			else if(tokenizer.isOperator()) {
				CalculableNode<O> newOperator = builder.createOperator(rawCurr);
				if(!prev.isOperator()) {
					newOperator.addOperand(prev);
					if(prev == root) {
						root = newOperator;
					}
					
					prev = newOperator;
				}
				else if(prev.canAcceptMoreOperand() && !prev.isGroupingOperator()) {
					if(!newOperator.isPrefixOperator()) {
						throw new InvalidExpressionException("Expecting right-side operand for \"" + prev + "\" but found an operator \"" + rawCurr + "\"");
					}
					
					prev.addOperand(newOperator);
					if(!tokenizer.hasNext()) {
						throw new InvalidExpressionException("Expecting an operand for operator \"" + rawCurr + "\"");
					}
					
					rawCurr = tokenizer.next();
					if(tokenizer.isOperand()) {
						newOperator.addOperand(builder.createOperand(rawCurr));
					}
					else if(tokenizer.isGroupStart()) {
						newOperator.addOperand(new GroupingOperator<O>(createOperation(tokenizer, builder, level + 1)));
					}
					else {
						throw new InvalidExpressionException("Un-expected symbol \"" + rawCurr + "\"");
					}
				}
				else {
					if(builder.compare((Operator<O>) prev, (Operator<O>) newOperator) >= 0) {
						newOperator.addOperand(prev);
						if(prev == root) {
							root = newOperator;
						}
					}
					else {
						if(!(prev instanceof TupleOperator)) {
							throw new InvalidExpressionException();
						}
						
						TupleOperator<O> tuple = (TupleOperator<O>) prev;
						newOperator.addOperand(tuple.switchOperand(newOperator));
					}
					
					prev = newOperator;
				}
			}
			else {
				throw new InvalidExpressionException("Un-recognized symbol \"" + rawCurr + "\"");
			}
		}
		
		if(inGroup && (level > 0)) {
			throw new InvalidExpressionException("Expecting group ending for \"" + root + "\"");
		}
		
		return root;
	}
}
