package com.digitalpaytech.util.scripting.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.digitalpaytech.util.scripting.ExpressionTokenizer;
import com.digitalpaytech.util.scripting.InvalidTokenException;

public class RegExTokenizer<O> implements ExpressionTokenizer<CharSequence, O> {
	private int operatorGroupIdx;
	private int operandGroupIdx;
	private int groupStartGroupIdx;
	private int groupEndGroupIdx;
	
	private CharSequence expression;
	
	private Matcher m;
	private int previousIdx;
	
	private boolean operator;
	private boolean groupStart;
	private boolean groupEnd;
	private boolean operand;
	
	public RegExTokenizer(Pattern tokenPattern, int operatorGroupIdx, int operandGroupIdx, int groupStartIdx, int groupEndIdx, CharSequence expression) {
		this.operatorGroupIdx = operatorGroupIdx;
		this.operandGroupIdx = operandGroupIdx;
		this.groupStartGroupIdx = groupStartIdx;
		this.groupEndGroupIdx = groupEndIdx;
		
		this.expression = expression;
		
		this.m = tokenPattern.matcher(expression);
		if(this.m.find()) {
			this.previousIdx = 0;
		}
		else {
			this.previousIdx = expression.length();
		}
	}
	
	@Override
	public boolean hasNext() {
		return this.previousIdx < expression.length();
	}
	
	@Override
	public CharSequence next() {
		String result = this.m.group();
		checkForInvalidToken(this.m.start());
		
		this.operator = (this.m.group(this.operatorGroupIdx) != null);
		this.operand = (this.m.group(this.operandGroupIdx) != null);
		this.groupStart = (this.m.group(this.groupStartGroupIdx) != null);
		this.groupEnd = (this.m.group(this.groupEndGroupIdx) != null);
		
		this.previousIdx = this.m.end();
		if(!this.m.find()) {
			checkForInvalidToken(expression.length());
			this.previousIdx = expression.length();
		}
		
		return result;
	}
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	public boolean isOperator() {
		return operator;
	}

	public boolean isOperand() {
		return operand;
	}

	public boolean isGroupStart() {
		return groupStart;
	}

	public boolean isGroupEnd() {
		return groupEnd;
	}

	private void checkForInvalidToken(int currentTokenStartIdx) {
		CharSequence ignored = expression.subSequence(this.previousIdx, currentTokenStartIdx);
		if(ignored.toString().trim().length() > 0) {
			throw new InvalidTokenException(ignored);
		}
	}
}
