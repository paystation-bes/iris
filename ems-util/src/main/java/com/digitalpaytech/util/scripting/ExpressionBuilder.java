package com.digitalpaytech.util.scripting;

import java.util.Comparator;

import com.digitalpaytech.util.scripting.operand.Operand;
import com.digitalpaytech.util.scripting.operator.Operator;

public interface ExpressionBuilder<I, O> extends Comparator<Operator<O>> {
	public CalculableNode<O> createOperand(I input) throws InvalidExpressionException;
	public CalculableNode<O> createOperator(I input) throws InvalidExpressionException;
	
	public Operand<O> createDefault();
}
