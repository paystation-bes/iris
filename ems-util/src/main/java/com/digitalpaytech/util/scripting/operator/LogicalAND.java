package com.digitalpaytech.util.scripting.operator;

import java.util.Map;

import com.digitalpaytech.util.scripting.CalculableNode;

public class LogicalAND extends TupleOperator<Boolean> {
	private static final long serialVersionUID = -5886872414787494685L;

	public LogicalAND() {
		
	}
	
	public LogicalAND(CalculableNode<Boolean> left, CalculableNode<Boolean> right) {
		super(left, right);
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		return this.getLeft().execute(context) & this.getRight().execute(context);
	}

	@Override
	protected String getStringRepresentation() {
		return "&";
	}
}
