package com.digitalpaytech.util.scripting.operator;

import java.util.Map;

import com.digitalpaytech.util.scripting.CalculableNode;

public class LogicalOR extends TupleOperator<Boolean> {
	private static final long serialVersionUID = -7107475926841390432L;

	public LogicalOR() {
		
	}
	
	public LogicalOR(CalculableNode<Boolean> left, CalculableNode<Boolean> right) {
		super(left, right);
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		return this.getLeft().execute(context) | this.getRight().execute(context);
	}

	@Override
	protected String getStringRepresentation() {
		return "|";
	}
}
