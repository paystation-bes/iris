package com.digitalpaytech.util.scripting.operator;

import java.text.MessageFormat;

import com.digitalpaytech.util.scripting.CalculableNode;

public abstract class SingleOperator<R> extends Operator<R> {
	private static final long serialVersionUID = -2292076991368720806L;
	
	private static final String PRINT_PATTERN_PREFIX = "{0}{1}";
	private static final String PRINT_PATTERN_SUFFIX = "{1}{0}";
	
	private CalculableNode<R> operand;
	
	protected SingleOperator() {
		
	}
	
	protected SingleOperator(CalculableNode<R> operand) {
		this.operand = operand;
	}

	@Override
	public String toString() {
		return MessageFormat.format(this.isPrefixOperator() ? PRINT_PATTERN_PREFIX : PRINT_PATTERN_SUFFIX, this.getStringRepresentation(), this.operand);
	}

	@Override
	public boolean isGroupingOperator() {
		return false;
	}

	@Override
	public boolean canAcceptMoreOperand() {
		return (this.operand == null);
	}

	@Override
	public void addOperand(CalculableNode<R> operand) {
		if(!this.canAcceptMoreOperand()) {
			throw new IllegalStateException("This operator can accept only one operand !");
		}
		
		this.operand = operand;
	}
	
	@Override
	public CalculableNode<R> switchOperand(CalculableNode<R> newOperand) {
		if(this.canAcceptMoreOperand()) {
			throw new IllegalStateException("There is no operand to switch !");
		}
		
		CalculableNode<R> result = this.operand;
		this.operand = newOperand;
		
		return result;
	}

	public CalculableNode<R> getOperand() {
		return operand;
	}
}
