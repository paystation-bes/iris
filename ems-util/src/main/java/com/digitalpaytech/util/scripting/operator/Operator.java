package com.digitalpaytech.util.scripting.operator;

import com.digitalpaytech.util.scripting.CalculableNode;

public abstract class Operator<R> extends CalculableNode<R> {
	private static final long serialVersionUID = 6893229776508859416L;
	
	public abstract CalculableNode<R> switchOperand(CalculableNode<R> newOperand);
	
	protected abstract String getStringRepresentation();

	protected Operator() {
		
	}
	
	@Override
	public boolean isOperator() {
		return true;
	}

	@Override
	public boolean isPrefixOperator() {
		return false;
	}
}
