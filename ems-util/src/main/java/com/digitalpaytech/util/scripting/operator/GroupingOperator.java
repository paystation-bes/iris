package com.digitalpaytech.util.scripting.operator;

import java.text.MessageFormat;
import java.util.Map;

import com.digitalpaytech.util.scripting.CalculableNode;

public class GroupingOperator<R> extends SingleOperator<R> {
	private static final long serialVersionUID = -1541891253960692848L;
	
	private static final String PRINT_PATTERN = "({0})";
	
	public GroupingOperator() {
		
	}
	
	public GroupingOperator(CalculableNode<R> operand) {
		super(operand);
	}
	
	@Override
	public String toString() {
		return MessageFormat.format(PRINT_PATTERN, this.getOperand());
	}

	@Override
	public boolean isGroupingOperator() {
		return true;
	}

	@Override
	public R execute(Map<String, Object> context) {
		return this.getOperand().execute(context);
	}

	@Override
	protected String getStringRepresentation() {
		return "()";
	}
}
