package com.digitalpaytech.util.scripting.operator;

import com.digitalpaytech.util.scripting.CalculableNode;

public class ReferenceOperator<R> extends GroupingOperator<R> {
	private static final long serialVersionUID = 1799774691700927158L;
	
	public ReferenceOperator() {
		
	}
	
	public ReferenceOperator(CalculableNode<R> operand) {
		super(operand);
	}
	
	public boolean isEmpty() {
		return (this.getOperand() == null);
	}
}
