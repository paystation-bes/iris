package com.digitalpaytech.util.scripting.impl;

import java.util.regex.Pattern;


public class BooleanExpressionTokenizer extends RegExTokenizer<Boolean> {
	private static Pattern tokenPattern = Pattern.compile("(&|\\||\\!)|(\\()|(\\))|([a-zA-Z_]+[0-9a-zA-Z_.]*)");
	
	public BooleanExpressionTokenizer(CharSequence expression) {
		super(tokenPattern, 1, 4, 2, 3, expression);
	}
}
