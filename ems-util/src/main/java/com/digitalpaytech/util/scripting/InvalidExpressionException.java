package com.digitalpaytech.util.scripting;

public class InvalidExpressionException extends RuntimeException {
	private static final long serialVersionUID = 2902322220476715458L;
	
	public InvalidExpressionException() {
		
	}
	
	public InvalidExpressionException(String message) {
		super(message);
	}
}
