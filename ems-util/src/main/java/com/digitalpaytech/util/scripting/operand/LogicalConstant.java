package com.digitalpaytech.util.scripting.operand;

import java.util.Map;

public class LogicalConstant extends Operand<Boolean> {
	private static final long serialVersionUID = -205313887739681487L;
	
	private Boolean value;
	
	public LogicalConstant(boolean value) {
		this.value = value;
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		return value;
	}
}
