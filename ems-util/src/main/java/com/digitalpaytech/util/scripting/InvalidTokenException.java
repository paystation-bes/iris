package com.digitalpaytech.util.scripting;

public class InvalidTokenException extends RuntimeException {
	private static final long serialVersionUID = 7804839956555110527L;
	
	private CharSequence token;
	
	public InvalidTokenException(CharSequence token) {
		super("Invalid token \"" + token + "\" !");
		this.token = token;
	}
	
	public CharSequence getToken() {
		return token;
	}
}
