package com.digitalpaytech.util.scripting;

import java.util.Iterator;

public interface ExpressionTokenizer<I, O> extends Iterator<I> {
	public boolean isOperator();
	public boolean isOperand();
	public boolean isGroupStart();
	public boolean isGroupEnd();
}
