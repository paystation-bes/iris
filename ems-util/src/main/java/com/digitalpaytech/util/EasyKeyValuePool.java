package com.digitalpaytech.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class EasyKeyValuePool<K, V> {
	private EasyObjectBuilder<V> objectBuilder;
	
	private int minPoolSize;
	private ConcurrentMap<K, EasyPool<V>> core;
	
	public EasyKeyValuePool(EasyObjectBuilder<V> objectBuilder, int minPoolSize, int concurrencyLevel) {
		this.objectBuilder = objectBuilder;
		this.minPoolSize = minPoolSize;
		this.core = new ConcurrentHashMap<K, EasyPool<V>>(this.minPoolSize * 2, 0.5F, concurrencyLevel);
	}
	
	public PooledResource<V> take(K key) {
		return locatePool(key).take();
	}
	
	public int getMinPoolSize() {
		return minPoolSize;
	}

	public void setMinPoolSize(int newSize) {
		for(K key : this.core.keySet()) {
			this.core.get(key).setMinPoolSize(newSize);
		}
		
		this.minPoolSize = newSize;
	}
	
	private EasyPool<V> locatePool(K key) {
		EasyPool<V> pool = this.core.get(key);
		if(pool == null) {
			pool = new EasyPool<V>(this.objectBuilder, key, this.minPoolSize);
			
			EasyPool<V> existing = this.core.putIfAbsent(key, pool);
			if(existing != null) {
				pool = existing;
			}
		}
		
		return pool;
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.core.clear();
		this.core = null;
		
		this.objectBuilder = null;
		
		super.finalize();
	}
}
