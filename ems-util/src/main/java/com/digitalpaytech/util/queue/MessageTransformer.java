package com.digitalpaytech.util.queue;

import java.util.List;

public interface MessageTransformer<M> {
    List<M> transform(M message);
}
