package com.digitalpaytech.util.queue;

public interface RoutingKeyResolver<M> {
    String resolve(M message);
}
