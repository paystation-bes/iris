package com.digitalpaytech.util.queue;

public interface MessageConverter {
    String toMessage(Object obj);
    Object toObject(String message);
}
