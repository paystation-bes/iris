package com.digitalpaytech.util.queue;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

public final class ReDeliveredMessage<O> implements Serializable {
    private static final long serialVersionUID = -7770924325379737951L;
    
    private transient O object;
    
    private String message;
    private int counter = 0;
    
    public ReDeliveredMessage() {
    	
    }
    
    public ReDeliveredMessage(final byte[] message, final O object) {
    	try {
    		this.message = new String(message, "UTF-8");
    	} catch (UnsupportedEncodingException uee) {
    		throw new Error("This system doesn't support UTF-8");
    	}
    	
    	this.object = object;
    }
    
    public ReDeliveredMessage(final String message, final O object) {
        this.message = message;
        this.object = object;
    }

    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(final String message) {
    	this.message = message;
    }
    
    public O getObject() {
    	return this.object;
    }
    
    public void setObject(final O object) {
    	this.object = object;
    }

    public int getCounter() {
        return this.counter;
    }
    
    public void setCounter(final int counter) {
    	this.counter = counter;
    }

    public void increment() {
        this.counter += 1;
    }
    
}
