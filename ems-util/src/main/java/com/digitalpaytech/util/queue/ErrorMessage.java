package com.digitalpaytech.util.queue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.digitalpaytech.util.StackTraceCompactor;

public final class ErrorMessage<M> implements Serializable {
    private static final long serialVersionUID = -3036089575334818112L;
    
    private static final String EXCEPTION_PACKAGE = "com.digitalpaytech.";
    private static final int EXCEPTION_CHARACTERS_LIMIT = 1000;
    
    private static StackTraceCompactor compactor = new StackTraceCompactor(EXCEPTION_PACKAGE, EXCEPTION_CHARACTERS_LIMIT);
    
    private String id;
    private String time;
    
    private List<String> errors;
    
    private M message;
    
    public ErrorMessage() {
        this(UUID.randomUUID().toString());
    }
    
    public ErrorMessage(final String id) {
        this.id = id;
        this.time = (new Date()).toString();
        
        this.errors = new ArrayList<String>();
    }
    
    @Override
    public String toString() {
        return "ErrorMessage[" + this.id + "]@" + this.time;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getTime() {
        return this.time;
    }
    
    public List<String> getErrors() {
        return this.errors;
    }
    
    public void add(final Throwable t) {
        this.errors.add(compactor.compact(t));
    }
    
    public int size() {
        return this.errors.size();
    }
    
    public M getMessage() {
        return this.message;
    }
    
    public void setMessage(final M message) {
        this.message = message;
    }
}
