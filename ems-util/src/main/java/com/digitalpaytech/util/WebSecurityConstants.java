package com.digitalpaytech.util;

/**
 * WebSecurityConstants
 * 
 * @author Brian Kim
 */
public final class WebSecurityConstants {
    
    public static final int USERNAME_MIN_LENGTH = 8;
    public static final int USERNAME_MAX_LENGTH = 255;
    public static final String EMS_REGULAR_EXP = "[0-9a-zA-Z| |.|,|\\-|=|_|\\#|\\@|\\(|\\)|/|:|\\||\\{|\\}|\\\\]*";
    public static final String COMPLEX_PASSWORD_EXP = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\p{Punct}).*$";
    public static final String EMS_REGULAR_EXP_EMAIL = "^[\\w\\.-]{1,}\\@([\\da-zA-Z-]{1,}\\.){1,}[\\da-zA-Z-]+$";
    
    public static final String DEFAULT_SERVICE_AGREEMENT_LOCATION = System.getProperty("catalina.home") + "/agreement/";
    
    // Random Key min length (max is 16 for Long Integer)
    public static final int RANDOMKEY_MIN_HEX_LENGTH = 16;
    public static final int RANDOMKEY_MAX_HEX_LENGTH = 16;
    
    // Action Result
    public static final boolean ACTION_FAILURE = false;
    
    // Action Flag
    public static final int ACTION_CREATE = 0;
    public static final int ACTION_UPDATE = 1;
    public static final int ACTION_SEARCH = 2;
    public static final int ACTION_REFUND = 3;
    public static final int ACTION_VIEW = 4;
    
    public static final char[] ALLOWED_WILDCARD_CHARACTERS = { '*', '?' };
    public static final String EMPTY_STRING = "";
    
    // ems session token for http get request
    //TODO Remove SessionToken
    public static final String SESSION_TOKEN = "sessionToken";
    
    // login result type
    public static final int LOGIN_RESULT_TYPE_LOGIN_SUCCESS = 1;
    public static final int LOGIN_RESULT_TYPE_INVALID_CREDENTIAL = 2;
    public static final int LOGIN_RESULT_TYPE_INVALID_CREDENTIAL_LOCKOUT = 3;
    public static final int LOGIN_RESULT_TYPE_USER_LOGOUT = 4;
    public static final int LOGIN_RESULT_TYPE_TIMEOUT_LOGOUT = 5;
    public static final int LOGIN_RESULT_TYPE_USER_SWITCHED_IN = 6;
    public static final int LOGIN_RESULT_TYPE_USER_SWITCHED_OUT = 7;
    
    public static final int USER_PASSWORD_MIN_LENGTH = 7;
    public static final int USER_PASSWORD_MAX_LENGTH = 15;
    
    // user name encoding charset
    public static final String URL_ENCODING_LATIN1 = "ISO-8859-1";
    
    // RoleId, customerId, values that will NOT change in database.
    public static final int DPT_ADMIN_USER_ACCOUNT_ID = 1;
    public static final int ROLE_ID_SYSTEM_ADMIN = 1;
    public static final int ROLE_ID_PARENT_ADMIN = 2;
    public static final int ROLE_ID_CHILD_ADMIN = 3;
    public static final int CUSTOMER_TYPE_ID_SYSTEM_ADMIN = 1;
    
    // ModelMap name values.
    public static final String WEB_SECURITY_FORM = "webSecurityForm";
    
    // user permissions
    public static final int PERMISSION_REPORTS_MANAGEMENT = 100;
    public static final int PERMISSION_MANAGE_REPORTS = 101;
    public static final int PERMISSION_MANAGE_TRANSACTION_REPORTS = 102;
    
    public static final int PERMISSION_ALERTS_MANAGEMENT = 200;
    public static final int PERMISSION_VIEW_USER_DEFINED_ALERTS = 201;
    public static final int PERMISSION_MANAGE_USER_DEFINED_ALERT = 202;
    public static final int PERMISSION_VIEW_MAINTENANCE_CENTER = 203;
    public static final int PERMISSION_CLEAR_ACTIVE_ALERTS = 204;
    public static final int PERMISSION_COLLECTIONS_MANAGEMENT = 300;
    public static final int PERMISSION_VIEW_COLLECTION_STATUS = 301;
    public static final int PERMISSION_CC_SMARTCARD_PROCESSING_MANAGEMENT = 400;
    public static final int PERMISSION_ISSUE_REFUNDS = 401;
    public static final int PERMISSION_VIEW_BANNED_CARDS = 402;
    public static final int PERMISSION_MANAGE_BANNED_CARDS = 403;
    public static final int PERMISSION_PROCESS_CARD_CHARGE_FROM_BOSS = 404;
    public static final int PERMISSION_SETUP_CREDIT_CARD_PROCESSING = 405;
    public static final int PERMISSION_VIEW_CAMPUS_CARD = 406;
    public static final int PERMISSION_MANAGE_CAMPUS_CARD = 407;
    public static final int PERMISSION_CONFIGURE_CAMPUS_CARD = 408;
    public static final int PERMISSION_VIEW_BANNED_SMART_CARD = 409;
    public static final int PERMISSION_MANAGE_BANNED_SMART_CARD = 410;
    public static final int PERMISSION_ACCOUNT_MANAGEMENT = 500;
    public static final int PERMISSION_VIEW_CUSTOM_CARDS = 501;
    public static final int PERMISSION_MANAGE_CUSTOM_CARDS = 502;
    public static final int PERMISSION_CONFIGURE_CUSTOM_CARDS = 503;
    public static final int PERMISSION_VIEW_COUPONS = 504;
    public static final int PERMISSION_MANAGE_COUPONS = 505;
    public static final int PERMISSION_VIEW_CONSUMER_ACCOUNTS = 506;
    public static final int PERMISSION_MANAGE_CONSUMER_ACCOUNTS = 507;
    public static final int PERMISSION_EXTEND_BY_PHONE = 600;
    public static final int PERMISSION_VIEW_RATES_AND_POLICIES = 601;
    public static final int PERMISSION_MANAGE_RATES_AND_POLICIES = 602;
    public static final int PERMISSION_USER_ACCOUNT_MANAGEMENT = 700;
    public static final int PERMISSION_VIEW_USERS_AND_ROLES = 701;
    public static final int PERMISSION_MANAGE_USERS_AND_ROLES = 702;
    public static final int PERMISSION_PAY_STATION_CONFIG_SETTINGS = 800;
    public static final int PERMISSION_UPLOAD_CONFIG_FROM_BOSS = 801;
    public static final int PERMISSION_SYSTEM_ACTIVITY = 900;
    public static final int PERMISSION_VIEW_SYSTEM_ACTIVITIES = 901;
    public static final int PERMISSION_PAYSTATION_MANAGEMENT = 1000;
    public static final int PERMISSION_VIEW_LOCATIONS = 1001;
    public static final int PERMISSION_MANAGE_LOCATIONS = 1002;
    public static final int PERMISSION_VIEW_PAYSTATIONS = 1003;
    public static final int PERMISSION_MANAGE_PAYSTATIONS = 1004;
    public static final int PERMISSION_VIEW_ROUTES = 1005;
    public static final int PERMISSION_MANAGE_ROUTES = 1006;
    public static final int PERMISSION_SYSTEM_SETTINGS = 1007;
    public static final int PERMISSION_VIEW_PAYSTATION_SETTINGS = 1008;
    public static final int PERMISSION_MANAGE_PAYSTATION_SETTINGS = 1009;
    public static final int PERMISSION_MANAGE_PAYSTATION_PLACEMENT = 1010;
    public static final int PERMISSION_SCHEDULE_PAYSTATION_SETTINGS_UPDATE = 1011;
    public static final int PERMISSION_REMOTE_ACCESS = 1050;
    public static final int PERMISSION_ACCESS_DIGITAL_API = 1051;
    public static final int PERMISSION_PDA_ACCESS = 1052;
    public static final int PERMISSION_DPT_PAYSTATION_ADMINISTRATION = 1100;
    public static final int PERMISSION_DPT_VIEW_PAYSTATION = 1101;
    public static final int PERMISSION_DPT_MANAGE_PAYSTATION = 1102;
    public static final int PERMISSION_DPT_MOVE_PAYSTATION = 1103;
    public static final int PERMISSION_DPT_SET_BILLING_PARAMETERS = 1104;
    public static final int PERMISSION_DPT_CREATE_PAYSTATIONS = 1105;
    public static final int PERMISSION_DPT_MANAGE_PAYSTATION_PLACEMENT = 1106;
    public static final int PERMISSION_DPT_VIEW_PAYSTATION_GROUPS = 1107;
    public static final int PERMISSION_DPT_CUSTOMER_ADMINISTRATION = 1200;
    public static final int PERMISSION_CREATE_NEW_CUSTOMER = 1201;
    public static final int PERMISSION_EDIT_EXISTING_CUSTOMER = 1202;
    public static final int PERMISSION_VIEW_CUSTOMERS = 1203;
    public static final int PERMISSION_SYSTEM_NOTIFICATION_ADMINISTRATION = 1300;
    public static final int PERMISSION_VIEW_NOTIFICATIONS = 1301;
    public static final int PERMISSION_MANAGE_NOTIFICATIONS = 1302;
    public static final int PERMISSION_DIGITAL_API_ADMINISTRATION = 1400;
    public static final int PERMISSION_VIEW_LICENSES = 1401;
    public static final int PERMISSION_MANAGE_LICENSES = 1402;
    public static final int PERMISSION_SERVER_ADMINISTRATION = 1500;
    public static final int PERMISSION_VIEW_SERVER_STATUS = 1501;
    public static final int PERMISSION_SERVER_OPERATIONS = 1502;
    public static final int PERMISSION_DASHBOARD_MANAGEMENT = 1600;
    public static final int PERMISSION_VIEW_DASHBOARD = 1601;
    public static final int PERMISSION_MANAGE_DASHBOARD = 1602;
    public static final int PERMISSION_MOBILEAPP = 1700;
    public static final int PERMISSION_VIEW_MOBILEAPP = 1701;
    public static final int PERMISSION_MANAGE_MOBILEAPP = 1702;
    public static final int PERMISSION_MOBILEDEVICE = 1800;
    public static final int PERMISSION_VIEW_MOBILEDEVICE = 1801;
    public static final int PERMISSION_MANAGE_MOBILEDEVICE = 1802;
    public static final int PERMISSION_ACCESS_SIGNINGSERVER = 1900;
    public static final int PERMISSION_CREATE_SIGNATURE_SIGNINGSERVER = 1901;
    public static final int PERMISSION_ACCESS_ONLINE_RATES = 2000;
    public static final int PERMISSION_VIEW_RATES = 2001;
    public static final int PERMISSION_MANAGE_RATES = 2002;
    
    public static final int PERMISSION_CUSTOMER_MIGRATION_ADMINISTRATION = 2100;
    public static final int PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS = 2101;
    public static final int PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS = 2102;
    
    public static final int PERMISSION_VIEW_FLEX = 2201;
    public static final int PERMISSION_MANAGE_FLEX = 2202;
    public static final int PERMISSION_DPT_VIEW_FLEX = 2301;
    public static final int PERMISSION_DPT_MANAGE_FLEX = 2302;
    
    public static final int PERMISSION_DPT_VIEW_CASE = 2401;
    public static final int PERMISSION_DPT_MANAGE_CASE = 2402;
    
    // Settings Alerts
    public static final int MIN_THRESHOLD_COUNT = 1;
    public static final int MAX_THRESHOLD_COUNT = 9999;
    public static final int MIN_THRESHOLD_HOUR = 1;
    public static final int MAX_THRESHOLD_HOUR = 999;
    public static final double MIN_THRESHOLD_DOLLAR = 1;
    public static final double MAX_THRESHOLD_DOLLAR = 9999;
    public static final int MIN_TYPEID = 1;
    public static final int MAX_TYPEID = 7;
    public static final int MIN_SERIALNUMBER_LENGTH = 1;
    public static final int MAX_SERIALNUMBER_LENGTH = 12;
    public static final int MIN_THRESHOLD_DAY = 1;
    public static final int MAX_THRESHOLD_DAY = 365;
    
    // Merchant Account
    public static final int MERCHANT_ACCOUNT_NAME_MIN_LENGTH = 5;
    public static final int MERCHANT_ACCOUNT_NAME_MAX_LENGTH = 30;
    public static final int MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH = 1;
    public static final int MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH = 100;
    public static final int MERCHANT_ACCOUNT_LINK2GOV_MERCHANT_CODE_LENGTH = 16;
    public static final int MERCHANT_ACCOUNT_LINK2GOV_SETTLE_MERCHANT_CODE_LENGTH = 17;
    public static final int MERCHANT_ACCOUNT_CREDITCALL_TERMINALID_CODE_LENGTH = 8;
    public static final int MERCHANT_ACCOUNT_CREDITCALL_TRANSACTION_KEY_MAX_LENGTH = 20;
    public static final int MERCHANT_ACCOUNT_LINK2GOV_MAX_PASSWORD_LENGTH = 16;
    public static final int MERCHANT_ACCOUNT_LINK2GOV_MIN_PASSWORD_LENGTH = 8;
    public static final int MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MIN_CLOSE_BATCH_LENGTH = 1;
    public static final int MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MAX_CLOSE_BATCH_LENGTH = 100;
    public static final int MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MAX_CLOSE_BATCH_SIZE = 8000;
    public static final int MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_TERMINAL_ID_LENGTH = 22;
    
    // Unsuccessful login messages
    public static final String LOGIN_FAILURE_MESSAGE_USER_NAME_REQUIRED = "<b>User Name</b> is required.";
    public static final String LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID_CHARACTER = "<b>User Name</b> has invalid character.";
    public static final String LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID = "<b>User Name</b> is invalid.";
    public static final String LOGIN_FAILURE_MESSAGE_PASSWORD_INVALID = "<b>User Password</b> is invalid.";
    public static final String LOGIN_FAILURE_MESSAGE_PASSWORD_REQUIRED = "<b>User Password</b> is required.";
    public static final String LOGIN_FAILURE_MESSAGE_CREDENTIAL_LOCKOUT = "Bad credentials, locking user.";
    public static final String LOGIN_FAILURE_MESSAGE_USER_LOCKED = "User has been locked.";
    public static final String LOGIN_FAILURE_MESSAGE_BAD_CREDENTIALS = "Bad credentials";
    public static final String LOGIN_FAILURE_TOKEN_EXPIRED = "Token Expired";
    
    // Pay station serial number constants
    public static final String SERIAL_NUMBER_LUKE_REGEX = "^1[0-9]{11}$";
    public static final String SERIAL_NUMBER_SHELBY_REGEX = "^2[0-9]{11}$";
    public static final String SERIAL_NUMBER_LUKE_RADIUS_REGEX = "^3[0-9]{11}$";
    public static final String SERIAL_NUMBER_LUKE_2_REGEX = "^5([0-2]|[4-9])([0-9]){10}$";
    public static final String SERIAL_NUMBER_LUKE_B_REGEX = "^53[0-9]{10}$";
    public static final String SERIAL_NUMBER_DPT_INTERNAL_REGEX = "^8[0-9]{11}$";
    public static final String SERIAL_NUMBER_VIRTUAL_REGEX = "^9[0-9]{11}$";
    public static final String SERIAL_NUMBER_INTELLAPAY_REGEX = "^[0-9a-zA-Z]{8}$";
    
    // URIs for redirecting when insufficient permissions on other pages.
    public static final String NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS = "redirect:/secure/settings/global/index.html";
    public static final String NO_PERMISSION_REDIRECT_URI_DASHBOARD = "redirect:/secure/dashboard/index.html";
    
    private WebSecurityConstants() {
    }
}
