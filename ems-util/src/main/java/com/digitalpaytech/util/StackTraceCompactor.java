package com.digitalpaytech.util;

import java.text.MessageFormat;

public class StackTraceCompactor {
    public static final String DEFAULT_STACK_TRACE_CLASS_PREFIX = "com.digitalpaytech.";
    public static final String DEFAULT_STACK_TRACE_FORMAT = "{1}.{2}#{3}";
    
    private String stackTraceFormat;
    private String classNamePrefix;
    private int characterLimit;
    
    public StackTraceCompactor() {
        this(DEFAULT_STACK_TRACE_CLASS_PREFIX, DEFAULT_STACK_TRACE_FORMAT, 0);
    }
    
    public StackTraceCompactor(final String classNamePrefix, final int characterLimit) {
        this(classNamePrefix, DEFAULT_STACK_TRACE_FORMAT, characterLimit);
    }
    
    public StackTraceCompactor(final int characterLimit) {
        this(DEFAULT_STACK_TRACE_CLASS_PREFIX, DEFAULT_STACK_TRACE_FORMAT, characterLimit);
    }
    
    public StackTraceCompactor(final String classNamePrefix, final String stackTraceFormat, final int characterLimit) {
        this.classNamePrefix = classNamePrefix;
        this.stackTraceFormat = stackTraceFormat;
        this.characterLimit = characterLimit;
    }
    
    public final String compact(final Throwable t) {
        final StringBuilder result = new StringBuilder();
        if (t == null) {
            result.append("No Exception!");
        } else {
            result.append(formatStackTrace(t));
            
            boolean inLimit = inLimit(result);
            Throwable cause = t.getCause();
            while (inLimit && (cause != null)) {
                result.append("\n...by: ");
                result.append(formatStackTrace(cause));
                
                cause = cause.getCause();
                inLimit = inLimit(result);
            }
            
            if (!inLimit) {
                result.append("\n...");
            }
        }
        
        return result.toString();
    }
    
    private String formatStackTrace(final Throwable t) {
        final StringBuilder result = new StringBuilder();
        result.append(t.getClass().getName());
        if (t.getMessage() != null) {
            result.append(": ");
            result.append(t.getMessage());
        }
        
        final StackTraceElement[] stackTraces = t.getStackTrace();
        if ((stackTraces != null) && (stackTraces.length > 0)) {
            result.append("@{ ");
            result.append(formatStackTrace(stackTraces, 0));
            
            boolean inLimit = inLimit(result);
            int idx = 0;
            while (inLimit && (++idx < stackTraces.length)) {
                if (stackTraces[idx].getClassName().startsWith(this.classNamePrefix)) {
                    result.append(" <= ");
                    result.append(formatStackTrace(stackTraces, idx));
                    
                    inLimit = inLimit(result);
                }
            }
            
            if (!inLimit) {
                result.append(" ...");
            }
            
            result.append(" }");
        }
        
        return result.toString();
    }
    
    private boolean inLimit(final CharSequence sequence) {
        return (this.characterLimit <= 0) || (this.characterLimit >= sequence.length());
    }
    
    private String formatStackTrace(final StackTraceElement[] stackTraceElements, final int idx) {
        final StackTraceElement current = stackTraceElements[idx];
        
        String shortClassName;
        if ((idx > 0) && current.getClassName().equals(stackTraceElements[idx - 1].getClassName())) {
            shortClassName = "";
        } else {
            shortClassName = current.getClassName().substring(current.getClassName().lastIndexOf(".") + 1);
        }
        
        return MessageFormat.format(this.stackTraceFormat, current.getClassName(), shortClassName, current.getMethodName(),
                                    current.getLineNumber());
    }
}
