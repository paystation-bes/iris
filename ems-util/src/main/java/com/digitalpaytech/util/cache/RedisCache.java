package com.digitalpaytech.util.cache;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.codec.Base64;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.digitalpaytech.util.EasyObjectBuilder;
import com.digitalpaytech.util.EasyPool;
import com.digitalpaytech.util.KeyValuePair;
import com.digitalpaytech.util.PooledResource;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.UnsafeInput;
import com.esotericsoftware.kryo.io.UnsafeOutput;

public abstract class RedisCache<O extends Serializable> {
    public static final int NUM_INSTANCE_KRYO = 5;
    public static final int TIMEOUT_LOCK_SECS = 5;
    
    public static final String SUFFIX_HASH = ":hash";
    
    public static final int META_COUNT = 3;
    
    public static final int META_IDX_CACHE_NAME = 0;
    public static final int META_IDX_KEY_START = 1;
    public static final int META_IDX_ARG_START = 2;
    
    public static final int KEY_IDX_CACHE_STRUCTURE = 0;
    public static final int KEY_IDX_KEY_CACHE_GROUP = 1;
    public static final int KEY_IDX_KEY = 2;
    
    public static final int ARG_IDX_VALUE = 0;
    
    protected static final String TPL_COMMAND_EXPIRE = " "
            + "for si=" + cmdKeyStartIdx(KEY_IDX_CACHE_STRUCTURE) + "," + cmdKeyEndIdx(KEY_IDX_CACHE_STRUCTURE) + " do "
            + "redis.call(''expire'',KEYS[si],{0,number,#####}) "
            + "end ";
    
    protected static final String COMMAND_CLEAR = "local updated=0 "
            + "local mki=tonumber(KEYS[" + (META_IDX_KEY_START + 1) + "]) "
            + "for ci=" + cmdKeyStartIdx(KEY_IDX_KEY_CACHE_GROUP) + "," + cmdKeyEndIdx(KEY_IDX_KEY_CACHE_GROUP) + " do "
            + "updated=updated+redis.call('del',KEYS[ci]) "
            + "end ";
    
    protected static final String COMMAND_REMOVE_SETUP = "local updated=0 local prevUpdated=0 local idx=0 "
            + "local ki=" + cmdKeyStartIdx(KEY_IDX_KEY) + "  ";
    protected static final String COMMAND_REMOVE_PRE = " "
            + "for ci=" + cmdKeyStartIdx(KEY_IDX_KEY_CACHE_GROUP) + "," + cmdKeyEndIdx(KEY_IDX_KEY_CACHE_GROUP) + " do "
            + "prevUpdated=redis.call('hdel',KEYS[ci],KEYS[ki])"
            + "updated=updated+prevUpdated ";
    protected static final String COMMAND_REMOVE_POST = " "
            + "ki=ki+1 idx=idx+1 "
            + "end ";
    
    protected static final String COMMAND_PUT_SETUP = "local updated=0 local prevUpdated=0 local idx=0 "
            + "local ki=" + cmdKeyStartIdx(KEY_IDX_KEY) + " local vi=" + cmdArgStartIdx(ARG_IDX_VALUE) + " ";
    protected static final String COMMAND_PUT_PRE = " "
            + "for ci=" + cmdKeyStartIdx(KEY_IDX_KEY_CACHE_GROUP) + "," + cmdKeyEndIdx(KEY_IDX_KEY_CACHE_GROUP) + " do "
            + "prevUpdated=redis.call('hset',KEYS[ci],KEYS[ki],ARGV[vi]) "
            + "updated=updated+prevUpdated ";
    protected static final String COMMAND_PUT_POST = " "
            + "ki=ki+1 vi=vi+1 idx=idx+1 "
            + "end ";
    
    protected static final String COMMAND_PUT_IF_ABSENT_SETUP = "local updated=0 local prevUpdated=0 local idx=0 "
            + "local ki=" + cmdKeyStartIdx(KEY_IDX_KEY) + " local vi=" + cmdArgStartIdx(ARG_IDX_VALUE) + " ";
    protected static final String COMMAND_PUT_IF_ABSENT_PRE = " "
            + "local existing=redis.call('hget',KEYS[ci],KEYS[ki]) "
            + "prevUpdated=redis.call('hsetnx',KEYS[ci],KEYS[ki],ARGV[vi]) "
            + "updated=updated+prevUpdated ";
    protected static final String COMMAND_PUT_IF_ABSENT_POST = " "
            + "idx=idx+1 ";
    
    protected static final String BYTES_XX = "XX";
    protected static final String BYTES_EX = "EX";
    
    private static EasyPool<Kryo> kryoPool = new EasyPool<Kryo>(new EasyObjectBuilder<Kryo>() {
        @Override
        public Kryo create(final Object configurations) {
            return new Kryo();
        }
    }, null, NUM_INSTANCE_KRYO);
    
    private JedisPool jedisPool;
    private int dbIndex;
    
    private String cacheName;
    private Class<O> cacheType;
    
    private String cacheLockNamePrefix;
    
    private int expirySecs;
    
    protected RedisCache(final String cacheName, final Class<O> cacheType, final JedisPool jedisPool, final int dbIndex) {
        this.cacheName = this.transformHashName(cacheName) + SUFFIX_HASH;
        this.cacheLockNamePrefix = this.cacheName + ":lock";
        
        this.cacheType = cacheType;
        this.jedisPool = jedisPool;
        this.dbIndex = dbIndex;
    }
    
    protected static final String cmdKeyStartIdx(final int keyIndex) {
        return cmdIdx(true, keyIndex, 0);
    }
    
    protected static final String cmdKeyEndIdx(final int keyIndex) {
        return cmdIdx(true, keyIndex + 1, -1);
    }
    
    protected static final String cmdArgStartIdx(final int argIndex) {
        return cmdIdx(false, argIndex, 0);
    }
    
    protected static final String cmdArgEndIdx(final int argIndex) {
        return cmdIdx(false, argIndex + 1, -1);
    }
    
    private static String cmdIdx(final boolean isKey, final int idx, final int valIncr) {
        final StringBuilder result = new StringBuilder();
        result.append("tonumber(KEYS[tonumber(KEYS[");
        
        if (isKey) {
            result.append(META_IDX_KEY_START + 1);
        } else {
            result.append(META_IDX_ARG_START + 1);
        }
        
        result.append("])+");
        result.append(idx);
        result.append("])");
        
        if (valIncr != 0) {
            result.append(valIncr);
        }
        
        return result.toString();
    }
    
    protected abstract String resolveKeyGroup(String key);
    
    protected abstract void appendCommandClear(final StringBuilder command, final Map<String, Object> ctx);
    
    protected abstract void appendParamsClear(final LuaParameters params, final Map<String, Object> ctx);
    
    protected abstract void appendCommandRemove(final StringBuilder command, final Map<String, Object> ctx);
    
    protected abstract void appendParamsRemove(final LuaParameters params, final String key, final Map<String, Object> ctx);
    
    protected abstract void appendCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx);
    
    protected abstract void appendParamsExtendExpiry(final LuaParameters params, final Map<String, Object> ctx);
    
    protected abstract void appendCommandPut(final StringBuilder command, final Map<String, Object> ctx);
    
    protected abstract void appendCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx);
    
    protected abstract void appendParamsPut(final LuaParameters params, final Map.Entry<String, O> entry, final Map<String, Object> ctx);
    
    protected Jedis connect() {
        final Jedis jedis = this.jedisPool.getResource();
        jedis.select(this.dbIndex);
        
        return jedis;
    }
    
    public final Class<O> getCacheType() {
        return this.cacheType;
    }
    
    public final int getExpirySecs() {
        return this.expirySecs;
    }

    public final void setExpirySecs(final int expirySecs) {
        this.expirySecs = expirySecs;
    }

    public final O get(final String key) {
        O result = null;
        
        try (Jedis jedis = connect()) {
            result = deserialize(jedis.hget(this.resolveCacheName(key), key), this.cacheType);
            this.extendExpiry();
        }
        
        return result;
    }
    
    public final void clear() {
        final Map<String, Object> ctx = new HashMap<String, Object>();
        
        try (Jedis jedis = connect()) {
            final LuaParameters params = this.paramsClear(ctx);
            
            final StringBuilder command = new StringBuilder();
            command.append(COMMAND_CLEAR);
            this.appendCommandClear(command, ctx);
            
            jedis.eval(command.toString(), params.getMetaSize() + params.getKeysSize(), params.consolidate());
        }
        
        ctx.clear();
    }
    
    public final void remove(final String... keys) {
        final Map<String, Object> ctx = new HashMap<String, Object>();
        
        final LuaParameters params = this.paramsRemove(keys, ctx);
        try (Jedis jedis = connect()) {
            final StringBuilder command = new StringBuilder();
            this.appendPreCommandRemove(command, ctx);
            command.append(COMMAND_REMOVE_PRE);
            
            this.appendCommandRemove(command, ctx);
            this.appendPostCommandRemove(command, ctx);
            
            if (this.getExpirySecs() > 0) {
                command.append(MessageFormat.format(TPL_COMMAND_EXPIRE, this.getExpirySecs()));
            }
            
            command.append(COMMAND_REMOVE_POST);
            
            jedis.eval(command.toString(), params.getMetaSize() + params.getKeysSize(), params.consolidate());
        }
        
        ctx.clear();
    }
    
    public final void put(final String key, final O value) {
        if (key == null) {
            throw new IllegalArgumentException("Key is required for putting data in Redis!");
        }
        
        this.put(this.generateMapEntry(key, value));
    }
    
    public final void put(final Collection<Map.Entry<String, O>> entries) {
        if (entries.size() > 0) {
            final Map<String, Object> ctx = new HashMap<String, Object>();
            
            final LuaParameters params = this.paramsPut(entries, ctx);
            try (Jedis jedis = connect()) {
                final StringBuilder command = new StringBuilder();
                this.appendPreCommandPut(command, ctx);
                command.append(COMMAND_PUT_PRE);
                
                this.appendCommandPut(command, ctx);
                this.appendPostCommandPut(command, ctx);
                
                if (this.getExpirySecs() > 0) {
                    command.append(MessageFormat.format(TPL_COMMAND_EXPIRE, this.getExpirySecs()));
                }
                
                command.append(COMMAND_PUT_POST);
                
                command.append(" return { updated } ");
                
                final String[] paramStrs = params.consolidate();
                
                jedis.eval(command.toString(), params.getMetaSize() + params.getKeysSize(), paramStrs);
            }
            
            ctx.clear();
        }
    }
    
    public final O putIfAbsent(final String key, final O value) {
        if (key == null) {
            throw new IllegalArgumentException("Key is required for putting data (if absent) in Redis!");
        }
        
        O result = null;
        
        final Map<String, Object> ctx = new HashMap<String, Object>();
        
        final LuaParameters params = this.paramsPut(this.generateMapEntry(key, value), ctx);
        try (Jedis jedis = connect()) {
            this.lock(key, jedis);
            
            final StringBuilder command = new StringBuilder();
            this.appendPreCommandPutIfAbsent(command, ctx);
            command.append(COMMAND_PUT_IF_ABSENT_PRE);
            
            this.appendCommandPut(command, ctx);
            this.appendPostCommandPutIfAbsent(command, ctx);
            
            if (this.getExpirySecs() > 0) {
                command.append(MessageFormat.format(TPL_COMMAND_EXPIRE, this.getExpirySecs()));
            }
            
            command.append(COMMAND_PUT_IF_ABSENT_POST);
            
            command.append(" return { existing }");
            
            result = this.deserialize((String) jedis.eval(command.toString(), params.getMetaSize() + params.getKeysSize(), params.consolidate()),
                                      this.cacheType);
        }
        
        ctx.clear();
        
        return result;
    }
    
    protected final void extendExpiry() {
        if (this.getExpirySecs() > 0) {
            final Map<String, Object> ctx = new HashMap<String, Object>();
            
            final LuaParameters params = this.paramsExtendExpiry(ctx);
            try (Jedis jedis = connect()) {
                final StringBuilder command = new StringBuilder();
                
                this.appendPreCommandExtendExpiry(command, ctx);
                this.appendCommandExtendExpiry(command, ctx);
                this.appendPostCommandExtendExpiry(command, ctx);
                
                final String[] paramStrs = params.consolidate();
                
                jedis.eval(command.toString(), params.getMetaSize() + params.getKeysSize(), paramStrs);
            }
            
            ctx.clear();
        }
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(COMMAND_REMOVE_SETUP);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        // DO NOTHING.
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(MessageFormat.format(TPL_COMMAND_EXPIRE, this.getExpirySecs()));
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx) {
        // DO NOTHING.
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(COMMAND_PUT_SETUP);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        // DO NOTHING.
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(COMMAND_PUT_IF_ABSENT_SETUP);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        // DO NOTHING.
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String transformHashName(final String someCacheName) {
        return someCacheName;
    }
    
    protected final List<Map.Entry<String, O>> generateMapEntry(final String key, final O value) {
        final List<Map.Entry<String, O>> result = new ArrayList<Map.Entry<String, O>>(1);
        result.add(new KeyValuePair<String, O>(key, value));
        
        return result;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected LuaParameters paramsClear(final Map<String, Object> ctx) {
        final LuaParameters result = new LuaParameters();
        appendParamsClear(result, ctx);
        
        return result;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected LuaParameters paramsRemove(final String[] keys, final Map<String, Object> ctx) {
        final LuaParameters result = new LuaParameters();
        
        this.appendCacheStructures(result);
        for (String k : keys) {
            result.addKey(KEY_IDX_KEY_CACHE_GROUP, this.resolveCacheName(k));
            result.addKey(KEY_IDX_KEY, k);
            
            appendParamsRemove(result, k, ctx);
        }
        
        return result;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected LuaParameters paramsExtendExpiry(final Map<String, Object> ctx) {
        final LuaParameters result = new LuaParameters();
        
        this.appendCacheStructures(result);
        result.addKey(KEY_IDX_KEY, "");
        appendParamsExtendExpiry(result, ctx);
        
        return result;
    }
    
    protected final LuaParameters paramsPut(final Collection<Map.Entry<String, O>> entries, final Map<String, Object> ctx) {
        final LuaParameters result = new LuaParameters();
        
        this.appendCacheStructures(result);
        for (Map.Entry<String, O> e : entries) {
            result.addKey(KEY_IDX_KEY_CACHE_GROUP, this.resolveCacheName(e.getKey()));
            result.addKey(KEY_IDX_KEY, e.getKey());
            
            result.addArg(ARG_IDX_VALUE, this.serialize(e.getValue()));
            
            appendParamsPut(result, e, ctx);
        }
        
        return result;
    }
    
    protected final void lock(final String key, final Jedis jedis) {
        final String lockName = this.cacheLockNamePrefix + key;
        jedis.set(lockName, Long.toString(System.currentTimeMillis()), BYTES_XX, BYTES_EX, TIMEOUT_LOCK_SECS);
        jedis.watch(lockName);
    }
    
    protected final String serialize(final Object obj) {
        String result = null;
        if (obj != null) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream(8192);
            final UnsafeOutput out = new UnsafeOutput(baos);
            
            final PooledResource<Kryo> kryo = kryoPool.take();
            kryo.get().writeObject(out, obj);
            kryo.close();
            out.close();
            
            result = new String(Base64.encode(baos.toByteArray()));
        }
        
        return result;
    }
    
    protected final <T> T deserialize(final String serializedObj, final Class<T> objType) {
        T result = null;
        byte[] objBytes = null;
        if (serializedObj != null) {
            objBytes = Base64.decode(serializedObj);
        }
        
        if ((objBytes != null) && (objBytes.length > 0)) {
            final UnsafeInput in = new UnsafeInput(new ByteArrayInputStream(objBytes));
            
            final PooledResource<Kryo> kryo = kryoPool.take();
            result = kryo.get().readObject(in, objType);
            kryo.close();
            in.close();
        }
        
        return result;
    }
    
    private String resolveCacheName(final String key) {
        final StringBuilder resultBuffer = new StringBuilder();
        resultBuffer.append(this.cacheName);
        
        final String keyGroup = this.resolveKeyGroup(key);
        if (keyGroup != null) {
            resultBuffer.append(":");
            resultBuffer.append(keyGroup);
        }
        
        return resultBuffer.toString();
    }
    
    private void appendCacheStructures(final LuaParameters params) {
        try (Jedis jedis = connect()) {
            params.addAllKeys(KEY_IDX_CACHE_STRUCTURE, jedis.keys(this.cacheName + "*"));
        }
    }
    
    /**
     * Lua Parameter is simply just array of byte array. The data in each index look like this...
     * 
     * {
     * meta-CacheName, meta-key-meta-start, meta-arg-meta-start
     * , meta-group-1-param-start, meta-group-2-param-start, ... , meta-group-N-param-start, meta-group-N-param-end
     * , key-1, key-2, ... , key-N
     * , arg-1, arg-2, ... ,arg-N
     * }
     *
     */
    protected final class LuaParameters {
        private List<Collection<String>> keysGroups;
        private int keysSize;
        
        private List<Collection<String>> argsGroups;
        private int argsSize;
        
        public LuaParameters() {
            this.keysGroups = new ArrayList<Collection<String>>();
            this.keysSize = 0;
            
            this.argsGroups = new ArrayList<Collection<String>>();
            this.argsSize = 0;
        }
        
        public void addKey(final int groupIndex, final String key) {
            while (this.keysGroups.size() <= groupIndex) {
                this.keysGroups.add(new ArrayList<String>());
            }
            
            this.keysGroups.get(groupIndex).add(key);
            ++this.keysSize;
        }
        
        public void addAllKeys(final int groupIndex, final Collection<String> keys) {
            while (this.keysGroups.size() <= groupIndex) {
                this.keysGroups.add(new ArrayList<String>());
            }
            
            final Collection<String> group = this.keysGroups.get(groupIndex);
            if (group.size() <= 0) {
                this.keysGroups.set(groupIndex, keys);
            } else {
                group.addAll(keys);
            }
            
            this.keysSize += keys.size();
        }
        
        public void addArg(final int groupIndex, final String value) {
            while (this.argsGroups.size() <= groupIndex) {
                this.argsGroups.add(new ArrayList<String>());
            }
            
            this.argsGroups.get(groupIndex).add(value);
            ++this.argsSize;
        }
        
        public void addAllArgs(final int groupIndex, final Collection<String> args) {
            while (this.argsGroups.size() <= groupIndex) {
                this.argsGroups.add(new ArrayList<String>());
            }
            
            final Collection<String> group = this.argsGroups.get(groupIndex);
            if (group.size() <= 0) {
                this.argsGroups.set(groupIndex, args);
            } else {
                group.addAll(args);
            }
            
            this.argsSize += args.size();
        }
        
        public int getKeysSize() {
            return this.keysSize;
        }
        
        public int getMetaSize() {
            return META_COUNT + this.keysGroups.size() + this.argsGroups.size() + 1;
        }
        
        public String[] consolidate() {
            final int metaSize = this.getMetaSize();
            
            final String[] result = new String[metaSize + this.keysSize + this.argsSize];
            result[META_IDX_CACHE_NAME] = RedisCache.this.cacheName;
            
            int metaIdx = META_COUNT;
            int paramIdx = metaSize;
            
            if (this.keysGroups.size() <= 0) {
                result[META_IDX_KEY_START] = Integer.toString(-1);
            } else {
                result[META_IDX_KEY_START] = Integer.toString(metaIdx + 1);
                for (Collection<String> keys : this.keysGroups) {
                    result[metaIdx++] = Integer.toString(paramIdx + 1);
                    for (String k : keys) {
                        result[paramIdx++] = k;
                    }
                }
            }
            
            if (this.argsGroups.size() <= 0) {
                result[META_IDX_ARG_START] = Integer.toString(-1);
                if (this.keysGroups.size() > 0) {
                    result[metaIdx++] = Integer.toString(paramIdx + 1);
                }
            } else {
                int argIdx = 0;
                // Reset argIdx because KEYS and ARGS are going to get separated in Lua.
                result[META_IDX_ARG_START] = Integer.toString(metaIdx + 1);
                for (Collection<String> args : this.argsGroups) {
                    result[metaIdx++] = Integer.toString(argIdx + 1);
                    for (String a : args) {
                        result[paramIdx++] = a;
                        ++argIdx;
                    }
                }
                
                result[metaIdx] = Integer.toString(argIdx + 1);
            }
            
            return result;
        }
    }
}
