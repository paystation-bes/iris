package com.digitalpaytech.util.cache;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public abstract class RedisOrderedCache<O extends Serializable> extends RedisCache<O> {
    public static final String CTXKEY_SCORE_MIN = "minScore";
    public static final String CTXKEY_SCORE_MAX = "maxScore";
    
    public static final int KEY_IDX_SET_NAME = 3;
    
    public static final int ARG_IDX_SCORE = 2;
    
    protected static final String SUFFIX_SORTED_SET = ":set";
    
    protected static final String TPL_COMMAND_EXPIRE_SORTED_SET =
            "redis.call(''expire'',KEYS[" + cmdKeyStartIdx(KEY_IDX_SET_NAME) + "],{0,number,#####}) ";
    
    protected static final String COMMAND_CLEAR_SORTED_SET = 
            "redis.call('del',KEYS[" + cmdKeyStartIdx(KEY_IDX_SET_NAME) + "])";
    
    protected static final String FRGMNT_STAMP_SET_SIZE = "curSetSize=redis.call('zcard',KEYS[" + cmdKeyStartIdx(KEY_IDX_SET_NAME) + "]) ";
    protected static final String COMMAND_STAMP_CURRENT_SIZE = "local " + FRGMNT_STAMP_SET_SIZE;
    
    protected static final String COMMAND_REMOVE_FROM_SORTED_SET = " prevUpdated=curSetSize "
            + "redis.call('zrem',KEYS[" + cmdKeyStartIdx(KEY_IDX_SET_NAME) + "],KEYS[ki]) "
            + FRGMNT_STAMP_SET_SIZE
            + "prevUpdated=prevUpdated-curSetSize ";
    
    protected static final String COMMAND_ADD_TO_SORTED_SET = " prevUpdated=curSetSize "
            + "redis.call('zadd',KEYS[" + cmdKeyStartIdx(KEY_IDX_SET_NAME) + "],tonumber(ARGV[" + cmdArgStartIdx(ARG_IDX_SCORE) + "+idx]),KEYS[ki]) "
            + FRGMNT_STAMP_SET_SIZE
            + "prevUpdated=curSetSize-prevUpdated ";
    
    private String cacheSetName;
    
    public RedisOrderedCache(final String cacheName, final Class<O> cacheType, final JedisPool jedisPool, final int dbIndex) {
        super(cacheName, cacheType, jedisPool, dbIndex);
        
        this.cacheSetName = this.transformSortedSetName(cacheName) + SUFFIX_SORTED_SET;
    }
    
    public abstract float score(O obj, final Map<String, Object> ctx);
    
    public final Set<String> keyRange(final long startIdx, final int len) {
        Set<String> result = null;
        try (Jedis jedis = connect()) {
            result = jedis.zrange(this.cacheSetName, startIdx, startIdx + len - 1);
        }
        
        this.extendExpiry();
        
        return (result != null) ? result : new TreeSet<String>();
    }
    
    public final List<O> range(final long startIdx, final int len) {
        List<O> result = null;
        
        final Set<String> keys = this.keyRange(startIdx, len);
        this.extendExpiry();
        
        if (keys.size() <= 0) {
            result = new ArrayList<O>(0);
        } else {
            result = new ArrayList<O>(keys.size());
            for (String key : keys) {
                result.add(this.get(key));
            }
        }
        
        return result;
    }
    
    protected final Set<String> revKeyRange(final long startIdx, final int len) {
        Set<String> result = null;
        try (Jedis jedis = connect()) {
            result = jedis.zrevrange(this.cacheSetName, startIdx, startIdx + len);
        }
        
        this.extendExpiry();
        
        return (result != null) ? result : new TreeSet<String>();
    }
    
    @SuppressWarnings({"checkstyle:designforextension"})
    protected String transformSortedSetName(final String someSetName) {
        return someSetName;
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandClear(final StringBuilder cmd, final Map<String, Object> ctx) {
        cmd.append(COMMAND_CLEAR_SORTED_SET);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsClear(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        params.addKey(KEY_IDX_SET_NAME, this.cacheSetName);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(COMMAND_REMOVE_FROM_SORTED_SET);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandRemove(command, ctx);
        
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_SORTED_SET, this.getExpirySecs()));
        }
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsRemove(final RedisCache<O>.LuaParameters params, final String key, final Map<String, Object> ctx) {
        params.addKey(KEY_IDX_SET_NAME, this.cacheSetName);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_SORTED_SET, this.getExpirySecs()));
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsExtendExpiry(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        params.addKey(KEY_IDX_SET_NAME, this.cacheSetName);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPreCommandPut(command, ctx);
        
        command.append(COMMAND_STAMP_CURRENT_SIZE);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        command.append(COMMAND_ADD_TO_SORTED_SET);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandPut(command, ctx);
        
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_SORTED_SET, this.getExpirySecs()));
        }
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPreCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPreCommandPutIfAbsent(command, ctx);
        
        command.append(COMMAND_STAMP_CURRENT_SIZE);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPutIfAbsent(final StringBuilder cmd, final Map<String, Object> ctx) {
        cmd.append(COMMAND_ADD_TO_SORTED_SET);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandPutIfAbsent(command, ctx);
        
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_SORTED_SET, this.getExpirySecs()));
        }
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsPut(final RedisCache<O>.LuaParameters params, final Entry<String, O> entry, final Map<String, Object> ctx) {
        final Float score = Float.valueOf(this.score(entry.getValue(), ctx));
        
        final Float minScore = (Float) ctx.get(CTXKEY_SCORE_MIN);
        if ((minScore == null) || (minScore.compareTo(score) > 0)) {
            ctx.put(CTXKEY_SCORE_MIN, score);
        }
        
        final Float maxScore = (Float) ctx.get(CTXKEY_SCORE_MAX);
        if ((maxScore == null) || (maxScore.compareTo(score) < 0)) {
            ctx.put(CTXKEY_SCORE_MAX, score);
        }
        
        params.addKey(KEY_IDX_SET_NAME, this.cacheSetName);
        params.addArg(ARG_IDX_SCORE, score.toString());
    }
}
