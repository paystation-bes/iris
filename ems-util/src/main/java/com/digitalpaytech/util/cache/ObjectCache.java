package com.digitalpaytech.util.cache;

public interface ObjectCache {
    <T> T get(Class<T> type, String key);
    
    boolean has(Class<?> type, String key);
    
    void put(Class<?> type, String key, Object obj);
    
    <T> T putIfAbsent(Class<T> type, String key, T obj);
    
    void remove(Class<?> type, String key);
    
    long generateUnique(Class<?> type);
    
    void registerTTL(ObjectTTL... ttlCalculators);
}
