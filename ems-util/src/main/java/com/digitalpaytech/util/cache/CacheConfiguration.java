package com.digitalpaytech.util.cache;

import java.io.Serializable;

public class CacheConfiguration implements Serializable {
    private static final long serialVersionUID = -8800100568213683584L;
    
    private String host;
    private int port;
    
    private int permitDatabaseIndex;
    
    public CacheConfiguration() {
        
    }

    public final String getHost() {
        return this.host;
    }

    public final void setHost(final String host) {
        this.host = host;
    }

    public final int getPort() {
        return this.port;
    }

    public final void setPort(final int port) {
        this.port = port;
    }

    public final int getPermitDatabaseIndex() {
        return this.permitDatabaseIndex;
    }

    public final void setPermitDatabaseIndex(final int permitDatabaseIndex) {
        this.permitDatabaseIndex = permitDatabaseIndex;
    }
}
