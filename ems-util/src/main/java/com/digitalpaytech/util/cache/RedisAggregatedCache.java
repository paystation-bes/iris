package com.digitalpaytech.util.cache;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanResult;

public abstract class RedisAggregatedCache<O extends Serializable> extends RedisOrderedCache<O> {
    public static final int KEY_IDX_MEMBER_HASH_NAME = 4;
    public static final int KEY_IDX_SATURATION_NAME = 5;
    
    public static final int ARG_IDX_MEMBER_NAME = 3;
    
    public static final String VAL_SATURATED = "saturated";
    
    protected static final String SUFFIX_MEMBERS = ":membersHash";
    protected static final String SUFFIX_SATURATION = ":saturation";
    
    protected static final String CURSOR_START = "0";
    
    protected static final String TPL_COMMAND_EXPIRE_MEMBERS_HASH = ""
            + "redis.call(''expire'',KEYS[" + cmdKeyStartIdx(KEY_IDX_MEMBER_HASH_NAME) + "],{0,number,#####}) "
            + "redis.call(''expire'',KEYS[" + cmdKeyStartIdx(KEY_IDX_SATURATION_NAME) + "],{0,number,#####}) ";
    
    protected static final String COMMAND_CLEAR_MEMBERS_HASH = "if prevUpdated>0 then "
            + "prevUpdated=redis.call('del',KEYS[" + cmdKeyStartIdx(KEY_IDX_MEMBER_HASH_NAME) + "]) "
            + "redis.call('del',KEYS[" + cmdKeyStartIdx(KEY_IDX_SATURATION_NAME) + "]) "
            + "end ";
    
    protected static final String COMMAND_DECREASE_FROM_MEMBERS_HASH = "if prevUpdated>0 then "
            + "prevUpdated=redis.call('hincrby',KEYS[" + cmdKeyStartIdx(KEY_IDX_MEMBER_HASH_NAME) + "]"
                    + ",ARGV[" + cmdArgStartIdx(ARG_IDX_MEMBER_NAME) + "+idx],-1) "
            + "end ";
    
    protected static final String COMMAND_INCREASE_TO_MEMBERS_HASH = "if prevUpdated>0 then "
            + "prevUpdated=redis.call('hincrby',KEYS[" + cmdKeyStartIdx(KEY_IDX_MEMBER_HASH_NAME) + "]"
                    + ",ARGV[" + cmdArgStartIdx(ARG_IDX_MEMBER_NAME) + "+idx],1) "
            + "end ";
    
    private String cacheMembersHashName;
    private String cacheSaturationName;
    
    public RedisAggregatedCache(final String cacheName, final Class<O> cacheType, final JedisPool jedisPool, final int dbIndex) {
        super(cacheName, cacheType, jedisPool, dbIndex);
        
        final String aggregatedCacheName = this.transformAggregatedCacheName(cacheName);
        this.cacheMembersHashName = aggregatedCacheName + SUFFIX_MEMBERS;
        this.cacheSaturationName = aggregatedCacheName + SUFFIX_SATURATION;
    }
    
    public abstract String identifyKeyType(final String key);
    
    public abstract List<String> keyTypes();
    
    public final Map<String, Long> sizes() {
        final Map<String, Long> result = new HashMap<String, Long>();
        
        try (Jedis jedis = connect()) {
            String cursor = CURSOR_START;
            do {
                final ScanResult<Map.Entry<String, String>> scanResult = jedis.hscan(this.cacheMembersHashName, cursor);
                cursor = scanResult.getStringCursor();
                
                final List<Map.Entry<String, String>> resultBuffer = scanResult.getResult();
                for (Map.Entry<String, String> entry : resultBuffer) {
                    result.put(entry.getKey(), Long.valueOf(entry.getValue()));
                }
            } while (!CURSOR_START.equals(cursor));
        }
        
        return result;
    }
    
    public final long size(final String type) {
        Long result = null;
        
        try (Jedis jedis = connect()) {
            final String buffer = jedis.hget(this.cacheMembersHashName, type);
            if (buffer == null) {
                result = 0L;
            } else {
                result = Long.valueOf(buffer);
            }
        }
        
        return (result == null) ? 0 : result.longValue();
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public boolean isSaturated(final String type, final long firstRecord, final long lastRecord) {
        boolean saturated = false;
        
        try (Jedis jedis = connect()) {
            saturated = VAL_SATURATED.equals(jedis.hget(this.cacheSaturationName, type));
            if (!saturated) {
                saturated = lastRecord <= this.size(type);
            }
        }
        
        return saturated;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public void saturate(final String type) {
        try (Jedis jedis = connect()) {
            jedis.hset(this.cacheSaturationName, type, VAL_SATURATED);
        }
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String transformAggregatedCacheName(final String someName) {
        return someName;
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandClear(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendCommandClear(command, ctx);
        command.append(COMMAND_CLEAR_MEMBERS_HASH);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsClear(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        super.appendParamsClear(params, ctx);
        params.addKey(KEY_IDX_MEMBER_HASH_NAME, this.cacheMembersHashName);
        params.addKey(KEY_IDX_SATURATION_NAME, this.cacheSaturationName);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendCommandRemove(command, ctx);
        command.append(COMMAND_DECREASE_FROM_MEMBERS_HASH);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandRemove(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandRemove(command, ctx);
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_MEMBERS_HASH, this.getExpirySecs()));
        }
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsRemove(final RedisCache<O>.LuaParameters params, final String key, final Map<String, Object> ctx) {
        super.appendParamsRemove(params, key, ctx);
        params.addKey(KEY_IDX_MEMBER_HASH_NAME, this.cacheMembersHashName);
        params.addKey(KEY_IDX_SATURATION_NAME, this.cacheSaturationName);
        params.addArg(ARG_IDX_MEMBER_NAME, this.identifyKeyType(key));
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendCommandExtendExpiry(command, ctx);
        command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_MEMBERS_HASH, this.getExpirySecs()));
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsExtendExpiry(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        super.appendParamsExtendExpiry(params, ctx);
        params.addKey(KEY_IDX_MEMBER_HASH_NAME, this.cacheMembersHashName);
        params.addKey(KEY_IDX_SATURATION_NAME, this.cacheSaturationName);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendCommandPut(command, ctx);
        command.append(COMMAND_INCREASE_TO_MEMBERS_HASH);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPut(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandPut(command, ctx);
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_MEMBERS_HASH, this.getExpirySecs()));
        }
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendCommandPutIfAbsent(command, ctx);
        command.append(COMMAND_INCREASE_TO_MEMBERS_HASH);
    }
    
    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendPostCommandPutIfAbsent(final StringBuilder command, final Map<String, Object> ctx) {
        super.appendPostCommandPutIfAbsent(command, ctx);
        if (this.getExpirySecs() > 0) {
            command.append(MessageFormat.format(TPL_COMMAND_EXPIRE_MEMBERS_HASH, this.getExpirySecs()));
        }
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsPut(final RedisCache<O>.LuaParameters params, final Entry<String, O> entry, final Map<String, Object> ctx) {
        super.appendParamsPut(params, entry, ctx);
        params.addKey(KEY_IDX_MEMBER_HASH_NAME, this.cacheMembersHashName);
        params.addKey(KEY_IDX_SATURATION_NAME, this.cacheSaturationName);
        params.addArg(ARG_IDX_MEMBER_NAME, this.identifyKeyType(entry.getKey()));
    }
}
