package com.digitalpaytech.util.cache;

import java.text.MessageFormat;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public final class MemoryObjectCache implements ObjectCache {
    private static final String FMT_KEY = "{0}:{1}";
    
    private static Random rand = new Random();
    
    private ConcurrentHashMap<String, Object> core;
    
    public MemoryObjectCache() {
        this.core = new ConcurrentHashMap<String, Object>();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(final Class<T> type, final String key) {
        return (T) this.core.get(MessageFormat.format(FMT_KEY, type.getName(), key));
    }
    
    @Override
    public boolean has(final Class<?> type, final String key) {
        return this.core.containsKey(MessageFormat.format(FMT_KEY, type.getName(), key));
    }
    
    @Override
    public void put(final Class<?> type, final String key, final Object obj) {
        if (obj != null) {
            this.core.put(MessageFormat.format(FMT_KEY, type.getName(), key), obj);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T putIfAbsent(final Class<T> type, final String key, final T obj) {
        T result = null;
        if (obj != null) {
            result = (T) this.core.putIfAbsent(MessageFormat.format(FMT_KEY, type.getName(), key), obj);
        }
        
        return result;
    }
    
    @Override
    public void remove(final Class<?> type, final String key) {
        this.core.remove(MessageFormat.format(FMT_KEY, type.getName(), key));
    }
    
    @Override
    public long generateUnique(final Class<?> type) {
        return rand.nextLong();
    }

    @Override
    public void registerTTL(final ObjectTTL... ttlCalculators) {
        // DO NOTHING
    }
}
