package com.digitalpaytech.util.cache;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import redis.clients.jedis.JedisPool;

public class RedisSimpleCache<O extends Serializable> extends RedisCache<O> {
    public RedisSimpleCache(final String cacheName, final Class<O> cacheType, final JedisPool jedisPool, final int dbIndex) {
        super(cacheName, cacheType, jedisPool, dbIndex);
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected String resolveKeyGroup(final String hashKey) {
        return null;
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandClear(final StringBuilder cmd, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsClear(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandRemove(final StringBuilder cmd, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsRemove(final RedisCache<O>.LuaParameters params, final String key, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandExtendExpiry(final StringBuilder command, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsExtendExpiry(final RedisCache<O>.LuaParameters params, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPut(final StringBuilder cmd, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendCommandPutIfAbsent(final StringBuilder cmd, final Map<String, Object> ctx) {
        // DO NOTHING.
    }

    @Override
    @SuppressWarnings("checkstyle:designforextension")
    protected void appendParamsPut(final RedisCache<O>.LuaParameters params, final Entry<String, O> entry, final Map<String, Object> ctx) {
        // DO NOTHING.
    }
}
