package com.digitalpaytech.util.cache;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.digitalpaytech.util.transport.Transporter;

public class RedisObjectCache implements ObjectCache {
    public static final int DEFAULT_EXPIRY_SECS = 60;
    
    public static final String REDIS_RESP_OK = "OK";
    
    protected static final String TYPE_SET_IF_ABSENT = "NX";
    protected static final String TYPE_SET_IF_EXISTS = "XX";
    
    protected static final String TYPE_EXPIRE_SECS = "EX";
    
    private static final String FMT_CACHE_KEY = "{0}:{1}";
    private static final String FMT_UNIQUE_KEY = "UNIQUE:{0}";
    
    private static final String ERROR_CACHE_FAILED = "Failed to cache '{key: {0}, obj: {1}'}";
    
    private Transporter transporter;
    
    private JedisPool jedisPool;
    private int dbIndex;
    private int expirySecs;
    
    private ReentrantReadWriteLock lock;
    private Map<Class<?>, ObjectTTL> ttlCalculators;
    
    public RedisObjectCache(final Transporter transporter, final JedisPool jedisPool, final int dbIndex) {
        this(transporter, jedisPool, dbIndex, DEFAULT_EXPIRY_SECS);
    }
    
    public RedisObjectCache(final Transporter transporter, final JedisPool jedisPool, final int dbIndex, final int expirySecs) {
        this.transporter = transporter;
        
        this.jedisPool = jedisPool;
        this.dbIndex = dbIndex;
        this.expirySecs = expirySecs;
        
        this.lock = new ReentrantReadWriteLock();
        this.ttlCalculators = new HashMap<Class<?>, ObjectTTL>();
    }
    
    @Override
    public final <T> T get(final Class<T> type, final String key) {
        T result = null;
        
        String raw = null;
        try (Jedis jedis = connect()) {
            raw = jedis.get(MessageFormat.format(FMT_CACHE_KEY, type.getName(), key));
        }
        
        if ((raw != null) && (raw.length() > 0)) {
            result = this.transporter.deserialize(type, raw);
        }
        
        return result;
    }
    
    @Override
    public final boolean has(final Class<?> type, final String key) {
        boolean result = false;
        try (Jedis jedis = connect()) {
            result = jedis.exists(MessageFormat.format(FMT_CACHE_KEY, type.getName(), key));
        }
        
        return result;
    }

    @Override
    public final void put(final Class<?> type, final String key, final Object obj) {
        put(type, key, obj, false);
    }
    
    @Override
    public final <T> T putIfAbsent(final Class<T> type, final String key, final T obj) {
        return put(type, key, obj, true);
    }
    
    @SuppressWarnings("unchecked")
    protected final <T> T put(final Class<?> type, final String key, final T obj, final boolean whenAbsent) {
        T result = null;
        if (obj != null) {
            final String cacheKey = MessageFormat.format(FMT_CACHE_KEY, type.getName(), key);
            final String value = this.transporter.serialize((Class<T>) obj.getClass(), obj);
            final int ttl = calculateTTL(key, obj);
            
            String opRes = null;
            try (Jedis jedis = connect()) {
                if (whenAbsent) {
                    opRes = jedis.set(cacheKey, value, TYPE_SET_IF_ABSENT, TYPE_EXPIRE_SECS, ttl);
                } else {
                    opRes = jedis.setex(cacheKey, ttl, value);
                }
            }
            
            if (whenAbsent) {
                if (opRes == null) {
                    result = (T) get(obj.getClass(), key);
                }
            } else {
                if (opRes == null) {
                    throw new RuntimeException(MessageFormat.format(ERROR_CACHE_FAILED, key, obj));
                }
                
                result = obj;
            }
        }
        
        return result;
    }
    
    @Override
    public final void remove(final Class<?> type, final String key) {
        final String cacheKey = MessageFormat.format(FMT_CACHE_KEY, type.getName(), key);
        try (Jedis jedis = connect()) {
            jedis.expireAt(cacheKey, 0L);
        }
    }
    
    @Override
    public final long generateUnique(final Class<?> type) {
        long result;
        
        final String uniqueKey = MessageFormat.format(FMT_UNIQUE_KEY, type.getName());
        try (Jedis jedis = connect()) {
            result = jedis.incr(uniqueKey);
        }
        
        return result;
    }

    @Override
    public final void registerTTL(final ObjectTTL... calculators) {
        final WriteLock writeLock = this.lock.writeLock();
        writeLock.lock();
        try {
            for (ObjectTTL ttl : calculators) {
                this.ttlCalculators.put(ttl.type(), ttl);
            }
        } finally {
            writeLock.unlock();
        }
    }

    protected final Jedis connect() {
        final Jedis jedis = this.jedisPool.getResource();
        jedis.select(this.dbIndex);
        
        return jedis;
    }
    
    @SuppressWarnings("unchecked")
    private int calculateTTL(final String key, final Object obj) {
        int result = -1;
        if (obj != null) {
            ObjectTTL calc;
            
            final ReadLock readLock = this.lock.readLock();
            readLock.lock();
            try {
                calc = (ObjectTTL) this.ttlCalculators.get(obj.getClass());
            } finally {
                readLock.unlock();
            }
            
            if (calc != null) {
                result = calc.ttlInSecs(key, obj);
            }
        }
        
        if (result == -1) {
            result = this.expirySecs;
        }
        
        return result;
    }
}
