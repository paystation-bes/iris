package com.digitalpaytech.util.cache;

public abstract class ObjectTTL {
    private Class<?> type;
    
    public ObjectTTL(final Class<?> type) {
        this.type = type;
    }
    
    public abstract int ttlInSecs(String key, Object obj);
    
    public final Class<?> type() {
        return this.type;
    }
}
