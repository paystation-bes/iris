package com.digitalpaytech.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class ItemLocatorCriterionAggregator implements Serializable {
    private static final long serialVersionUID = 5825083946403044647L;
    
    private List<OrderingCondition> conditions;
    
    public ItemLocatorCriterionAggregator() {
        this.conditions = new ArrayList<OrderingCondition>();
    }
    
    public final ItemLocatorCriterionAggregator ascending(final String propertyName, final Object value, final Object minValue) {
        this.conditions.add(new OrderingCondition(propertyName, value, minValue, false));
        return this;
    }
    
    public final ItemLocatorCriterionAggregator descending(final String propertyName, final Object value, final Object minValue) {
        this.conditions.add(new OrderingCondition(propertyName, value, minValue, true));
        return this;
    }
    
    public final Criterion aggregateCriterion() {
        Criterion result = null;
        if (this.conditions.size() > 0) {
            final OrderingCondition[] ordering = this.conditions.toArray(new OrderingCondition[this.conditions.size()]);
            
            int i = 0;
            result = createOrderingCriterion(ordering[i]);
            while (++i < ordering.length) {
                Criterion current = createOrderingCriterion(ordering[i]);
                if (current == null) {
                    current = createPreviousEqualsCondition(ordering, i);
                }
                else {
                    current = Restrictions.and(createPreviousEqualsCondition(ordering, i), current);
                }
                
                if (result == null) {
                    result = current;
                }
                else {
                    result = Restrictions.or(result, current);
                }
            }
        }
        
        return result;
    }
    
    private Criterion createPreviousEqualsCondition(final OrderingCondition[] ordering, final int startIdx) {
        int i = startIdx - 1;
        Criterion result = createEqualCriterion(ordering[i]);
        while (--i >= 0) {
            result = Restrictions.and(result, createEqualCriterion(ordering[i]));
        }
        
        return result;
    }
    
    private Criterion createOrderingCriterion(final OrderingCondition condition) {
        Criterion result = null;
        if (condition.isDescending()) {
            if (condition.getValue() != null) {
                result = Restrictions.gt(condition.getPropertyName(), condition.getValue());
            }
        } else {
            result = Restrictions.lt(condition.getPropertyName(), (condition.getValue() != null) ? condition.getValue() : condition.getMinValue());
        }
        
        return result;
    }
    
    private Criterion createEqualCriterion(final OrderingCondition condition) {
        Criterion result = null;
        if (condition.getValue() == null) {
            result = Restrictions.isNull(condition.getPropertyName());
        } else {
            result = Restrictions.eq(condition.getPropertyName(), condition.getValue());
        }
        
        return result;
    }
    
    private class OrderingCondition implements Serializable {
        private static final long serialVersionUID = 6517259390080129309L;
        
        private String propertyName;
        private Object value;
        private boolean descending;
        private Object minValue;
        
        public OrderingCondition(final String propertyName, final Object value, final Object minValue, final boolean descending) {
            this.propertyName = propertyName;
            this.value = value;
            this.minValue = minValue;
            this.descending = descending;
        }
        
        public String getPropertyName() {
            return this.propertyName;
        }
        
        public Object getValue() {
            return this.value;
        }
        
        public boolean isDescending() {
            return this.descending;
        }
        
        public Object getMinValue() {
            return this.minValue;
        }
    }
}
