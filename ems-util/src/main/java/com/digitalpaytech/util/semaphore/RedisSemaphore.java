package com.digitalpaytech.util.semaphore;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;


public abstract class RedisSemaphore {
    private static final Logger LOG = Logger.getLogger(RedisSemaphore.class);
    
    @Autowired
    private JedisConnectionFactory connectionFactory;
    
    protected RedisSemaphore() {
    }
    
    protected RedisSemaphore(final JedisConnectionFactory c) {
        c.afterPropertiesSet();
        this.connectionFactory = c;
    }
    
    protected final int incrementAndGetCounter(final String redisKey) {
        final RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, this.connectionFactory);
        
        return txnCount.incrementAndGet();
    }

    /**
     * @param redisKey
     * @param expire int timeout value in milliseconds.
     * @return int 
     */
    protected final int incrementSetExpireAndGetCounter(final String redisKey, final int milliseconds) {
        final RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, this.connectionFactory);
        final int count = txnCount.incrementAndGet();
        
        /*
         * Boolean expire(long timeout, TimeUnit unit) - true if expiration was set, false otherwise.
         * Only set expire when we have the lock (count = 1).
         */
        if (count == 1) {
            final Boolean result = txnCount.expire(milliseconds, TimeUnit.MILLISECONDS);
            
            if (LOG.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("redisKey: ").append(redisKey).append(" has expire value in milliseconds: ");
                bdr.append(milliseconds).append(", result: ").append(result.booleanValue());
                LOG.debug(bdr.toString());
            }
        }
        return count;
    }
    
    protected final int resetCounter(final String redisKey) {
        final RedisAtomicInteger txnCount = new RedisAtomicInteger(redisKey, this.connectionFactory, 0);
        
        return txnCount.intValue();
    }
}
