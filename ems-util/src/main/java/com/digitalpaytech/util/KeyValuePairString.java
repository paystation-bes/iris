package com.digitalpaytech.util;

import java.util.Map;

public class KeyValuePairString implements Map.Entry<String, String> {
    private String key;
    private String value;
    
    public KeyValuePairString(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    @Override
    public final String getKey() {
        return this.key;
    }
    
    @Override
    public final String getValue() {
        return this.value;
    }
    
    @Override
    public final String setValue(final String newValue) {
        this.value = newValue;
        return this.value;
    }
}
