package com.digitalpaytech.util.xml;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("xmlRulesDriver")
public class XMLRulesDriver {
	
	private static final Logger logger = Logger.getLogger(XMLRulesDriver.class);

	public static final String XML_TRANSACTION_RULE = "transactionRules.xml";
	public static final String XML_TRANSACTION_PUSH_RULE = "transactionPushRules.xml";
	public static final String XML_ACTION_SERVICE_RULE = "actionServiceRules.xml";

	public Digester getTransactionParser() {
	    if (logger.isDebugEnabled()) {
	        logger.debug("getTransactionParser called");
	    }
		return DigesterLoader.createDigester(this.getClass().getClassLoader()
				.getResource(XML_TRANSACTION_RULE));
	}

	public Digester getActionServicesParser() {
	    if (logger.isDebugEnabled()) {
	        logger.debug("getActionSrvicesParser called");
	    }
		return DigesterLoader.createDigester(this.getClass().getClassLoader()
				.getResource(XML_ACTION_SERVICE_RULE));
	}
}