package com.digitalpaytech.util.xml;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

// Copied from EMS 6.3.11 com.digitalpioneer.util.xml.XMLBuilderBase

public abstract class XMLBuilderBase {
    private static final String COMMA = ",";
    private static final String ERROR_MSG = "Unable to create named element with name: ";
    private static Logger logger = Logger.getLogger(XMLBuilderBase.class);
    protected String xmlDTDLocation;
    protected String xmlMessageDestination;
    protected DocumentBuilder documentBuilder;


    public XMLBuilderBase(final String dtdLocation, final String messageDestination) {
        this.xmlDTDLocation = dtdLocation;
        this.xmlMessageDestination = messageDestination;
    }

    public XMLBuilderBase(final String dtdLocation) {
        this.xmlDTDLocation = dtdLocation;
    }

    public final Element getDestinationNode(final Document xmlDocument) {
        return getNamedElement(xmlDocument, this.xmlMessageDestination);
    }

    public static final String documentToString(final Document xmlDocument) {
        final StringWriter stringwriter = new StringWriter();
        final OutputFormat outputformat = new OutputFormat(xmlDocument, null, true);
        outputformat.setDoctype(null, null);
        outputformat.setStandalone(false);

        if (logger.isDebugEnabled()) {
            outputformat.setIndenting(true);
        }

        final XMLSerializer xmlserializer = new XMLSerializer(stringwriter, outputformat);

        try {
            xmlserializer.asDOMSerializer();

            xmlserializer.serialize(xmlDocument);
        } catch (java.io.IOException e) {
            throw new RuntimeException("Failed to serialize XML document!", e);
        }
        return stringwriter.toString();
    }

    public final Document fromXMLString(final String xmlDocumentAsString) {
        Document parseddocument = null;

        // StringReader stringreader = new StringReader(xmlDocumentAsString);

        final InputSource inputsource = new InputSource(new StringReader(xmlDocumentAsString));

        if (logger.isDebugEnabled()) {
            logger.debug("Attempting to convert the XML document string = \n"
                    + xmlDocumentAsString + " to a DOM object!");
        }
        
        try {
            parseddocument = this.documentBuilder.parse(inputsource);
        } catch (org.xml.sax.SAXException e) {
            throw new RuntimeException("Error parsing XML document from string to DOM!", e);
        } catch (java.io.IOException e) {
            throw new RuntimeException("IO problem parsing DOM!", e);
        }

        return parseddocument;
    }

    public final void initialize() {
        final DocumentBuilderFactory documentbuilderfactory = DocumentBuilderFactory.newInstance();

        /*
         * JAXP RI 1.1 has a bug that affects namespace processing. If you use
         * JAXP to instantiate a parser, then the JAXP getNamespaceAware()
         * method reports that namespace processing is turned off, while the
         * parser behaves as if namespace processing is turned on. This bug has
         * been fixed in JAXP RI 1.1.1 so that by default namespace processing
         * is off. Therefore, if you modify your application to call
         * setNamespaceAware(true) as described above, it will work in all
         * versions of the RI.
         */
        documentbuilderfactory.setNamespaceAware(true);
        
        // Set to true when DTD is finalized.
        documentbuilderfactory.setValidating(true);
        
        try {
            this.documentBuilder = documentbuilderfactory.newDocumentBuilder();
        } catch (javax.xml.parsers.ParserConfigurationException e) {
            throw new RuntimeException("Failed to configure document builder!", e);
        }
    }

    /**
     * Helper method to create an element with the given name, and attach it to
     * the provided parent element.
     * 
     * @param <tt>void</tt>
     * @return <tt>void</tt>
     */
    protected final Element createNamedElement(final Document containingDocument, 
                                               final Node parentNode, 
                                               final String elementName) {
        return createNamedElement(containingDocument, parentNode, elementName, null);
    }

    /**
     * Helper method to create an element with the given name value pair, and
     * attatch it to the provided parent element. A child text element is
     * created only for non-null values.
     * 
     * @param <tt>void</tt>
     * @return <tt>void</tt>
     */
    protected final Element createNamedElement(final Document containingDocument, 
                                               final Node parentNode, 
                                               final String elementName, 
                                               final String elementValue) {
        if (elementName == null) {
            throw new NullPointerException(ERROR_MSG + elementName
                                           + " and value: " + elementValue);
        }
        
        final Element namedelement = containingDocument.createElement(elementName);
        parentNode.appendChild(namedelement);

        if (elementValue != null) {
            final Text textelement = containingDocument.createTextNode(elementValue);
            namedelement.appendChild(textelement);
        }

        return namedelement;
    }

    public final Element getNamedElement(final Document xmlDocument, final String elementName) {
        final NodeList nodelist = xmlDocument.getElementsByTagName(this.xmlMessageDestination);

        if (nodelist.getLength() != 1) {
            throw new RuntimeException("Failed to find a single destination node for document!");
        }

        return (Element) nodelist.item(0);
    }
    
    protected final Element createNamedElementWithXMLBody(final Document containingDocument,
                                                          final Node parentNode,
                                                          final String elementName,
                                                          final NodeList xml) {
        if (elementName == null || xml == null) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(ERROR_MSG).append(elementName).append(" and xml value is empty.");
            throw new IllegalStateException(bdr.toString());
        }
        final Element namedElement = containingDocument.createElement(elementName);
        parentNode.appendChild(namedElement);
        
        for(int i = 0; i < xml.getLength(); i++) {
            Node item = xml.item(i).cloneNode(true);
            containingDocument.adoptNode(item);
            namedElement.appendChild(item);
        }
        return namedElement;
    }

    
    protected final Element createNamedElementWithAttributes(final Document containingDocument, 
                                                             final Node parentNode, 
                                                             final String elementName,
                                                             final Map<String, String> attributeNamesValues) {
        final int minAttributeNames = 3;
        if (elementName == null || attributeNamesValues == null || attributeNamesValues.isEmpty()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(ERROR_MSG).append(elementName).append(" and attribute names/values is empty.");
            throw new IllegalStateException(bdr.toString());
        }
        if (attributeNamesValues.size() < minAttributeNames) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Element name: ").append(elementName).append(", attribute name/value pair size is invalid. Attribute names: ");
            for (String key : attributeNamesValues.keySet()) {
                bdr.append(key).append("/").append(attributeNamesValues.get(key)).append(COMMA);
            }
            throw new IllegalStateException(bdr.toString());
        }
        
        final Element namedElement = containingDocument.createElement(elementName);
        parentNode.appendChild(namedElement);
        for (String key : attributeNamesValues.keySet()) {
            namedElement.setAttribute(key, attributeNamesValues.get(key));
        }
        return namedElement;
    }
}
