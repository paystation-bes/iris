package com.digitalpaytech.util.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digitalpaytech.exception.ApplicationException;

// Copied from EMS 6.3.11 com.digitalpioneer.util.xml.XMLSAXHandlerBase
/*
 * The DefaultHandler contains no-op implementations for all SAX events.
 */
public abstract class XmlSaxHandlerBase extends DefaultHandler implements XmlHandler {
    
    private static Logger log = Logger.getLogger(XmlSaxHandlerBase.class);
    protected String responseMessage;
    protected String requestMessage;
    
    public final String process(final byte[] xmlDocument) throws ApplicationException {
        if (xmlDocument == null) {
            throw new RuntimeException("XML document is null.");
        }
        
        try {
            // encode the '&' character
            final ByteArrayInputStream oldStream = new ByteArrayInputStream(xmlDocument);
            final byte[] encodedXmlDocument = XMLUtil.encodeXmlDocAmpersand(oldStream);
            
            // set the requestMessage
            this.requestMessage = new String(encodedXmlDocument);
            if (log.isDebugEnabled()) {
                String logRequest = XMLUtil.hideAllElementValues(this.requestMessage, "CardData");
                logRequest = XMLUtil.hideAllElementValues(logRequest, "CardNumber");
                logRequest = XMLUtil.hideAllElementValues(logRequest, "ExpiryDate");
                // PBPRequest XML-RPC transaction.
                logRequest = XMLUtil.hideAllElementValues(logRequest, "CreditCardData");
                log.debug("Parsing document: \n<START>\n" + logRequest + "<END>");
                // xxx should mask passwords etc for opening tag
            }
            
            final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setValidating(false);
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(encodedXmlDocument);
            parserFactory.newSAXParser().parse(inputStream, this);
            
        } catch (SAXException e) {
            throw new ApplicationException("Unable to parse document.", e);
        } catch (ParserConfigurationException e) {
            throw new ApplicationException("Unable to configure parser.", e);
        } catch (IOException e) {
            throw new ApplicationException("Unable to read document from I/O stream.", e);
        }
        return this.responseMessage;
    }
}
