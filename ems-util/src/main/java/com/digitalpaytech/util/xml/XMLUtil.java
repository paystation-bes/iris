package com.digitalpaytech.util.xml;

import java.io.IOException;
import java.io.InputStream;

import com.digitalpaytech.util.StandardConstants;

// Copied from EMS 6.3.11 com.digitalpioneer.util.xml.XMLUtilities

public final class XMLUtil {
    
    private static final String CLOSE_TAG = ">";
    private static final String OPEN_TAG = "<";
    
    private XMLUtil() {
    }
    
    /**
     * This will return a string with all the element values for the specified
     * tagname replaced with "XXXX"
     */
    public static String hideAllElementValues(final String request, final String tagName) {
        final StringBuilder bdr = new StringBuilder();
        final String regex = bdr.append(OPEN_TAG).append(tagName).append(">.*</").append(tagName).append(CLOSE_TAG).toString();
        
        bdr.replace(0, bdr.length(), "");
        final String replacement = bdr.append(OPEN_TAG).append(tagName).append(">XXXX</").append(tagName).append(CLOSE_TAG).toString();
        
        return request.replaceAll(regex, replacement);
    }
    
    /**
     * This will return a string containing the first element value for the specified
     * tagname.
     */
    public static String getFirstElementValue(final String request, final String tagName) {
        String elementValue = "";
        int startIndex = -1;
        int endIndex = -1;
        
        startIndex = request.indexOf(OPEN_TAG + tagName, startIndex);
        
        if (startIndex > -1) {
            startIndex = request.indexOf(CLOSE_TAG, startIndex) + 1;
            
            // This is a "<tagname/>" tag which indicates no data
            if (request.charAt(startIndex - 2) == '/') {
                // no value to return
            } else {
                endIndex = request.indexOf("</" + tagName, startIndex);
                if (endIndex > startIndex) {
                    elementValue = request.substring(startIndex, endIndex);
                }
            }
        }
        return elementValue.trim();
    }
    
    /**
     * This will return a string with all the attributes for the specified
     * tagname replaced with "XXXX"
     */
    public static String hideAllElementAttributes(final String request, final String tagName) {
        final StringBuilder bdr = new StringBuilder();
        final String regex = bdr.append(OPEN_TAG).append(tagName).append(".*[\n]?+.*>").toString();
        
        bdr.replace(0, bdr.length(), "");
        final String replacement = bdr.append(OPEN_TAG).append(tagName).append(" XXXX>").toString();
        return request.replaceFirst(regex, replacement);
    }
    
    /**
     * Encode the '&' character in the input stream.
     * 
     * @param is
     *            InputStream
     * @return A byte[] with the encoded document.
     * @throws IOException
     */
    public static byte[] encodeXmlDocAmpersand(final InputStream is) throws IOException {
        final StringBuilder sb = new StringBuilder();
        
        int read = is.read();
        final StringBuilder tempEscape = new StringBuilder();
        while (read != -1) {
            final char curr = (char) read;
            if (tempEscape.length() != 0) {
                if (curr == '&') {
                    sb.append("&amp;");
                    sb.append(tempEscape.substring(1).toString());
                    tempEscape.delete(0, tempEscape.length());
                    tempEscape.append(curr);
                } else {
                    tempEscape.append(curr);
                    if (tempEscape.length() == 2) {
                        if (curr != 'l' && curr != 'a' && curr != 'g' && curr != 'q') {
                            sb.append("&amp;");
                            sb.append(tempEscape.substring(1).toString());
                            tempEscape.delete(0, tempEscape.length());
                        }
                    } else if (tempEscape.length() == 3) {
                        if (!tempEscape.toString().equals("&lt") && !tempEscape.toString().equals("&am") && !tempEscape.toString().equals("&gt")
                            && !tempEscape.toString().equals("&qu") && !tempEscape.toString().equals("&ap")) {
                            sb.append("&amp;");
                            sb.append(tempEscape.substring(1).toString());
                            tempEscape.delete(0, tempEscape.length());
                        }
                    } else if (tempEscape.length() == 4) {
                        if (tempEscape.toString().equals("&lt;") || tempEscape.toString().equals("&gt;")) {
                            sb.append(tempEscape.toString());
                            tempEscape.delete(0, tempEscape.length());
                        } else if (!tempEscape.toString().equals("&amp") && !tempEscape.toString().equals("&quo")
                                   && !tempEscape.toString().equals("&apo")) {
                            sb.append("&amp;");
                            sb.append(tempEscape.substring(1).toString());
                            tempEscape.delete(0, tempEscape.length());
                        }
                    } else if (tempEscape.length() == 5) {
                        if (tempEscape.toString().equals("&amp;")) {
                            sb.append(tempEscape.toString());
                            tempEscape.delete(0, tempEscape.length());
                        } else if (!tempEscape.toString().equals("&quot") && !tempEscape.toString().equals("&apos")) {
                            sb.append("&amp;");
                            sb.append(tempEscape.substring(1).toString());
                            tempEscape.delete(0, tempEscape.length());
                        }
                    } else if (tempEscape.length() == 6) {
                        if (tempEscape.toString().equals("&quot;") || tempEscape.toString().equals("&apos;")) {
                            sb.append(tempEscape.toString());
                            tempEscape.delete(0, tempEscape.length());
                        } else {
                            sb.append("&amp;");
                            sb.append(tempEscape.substring(1).toString());
                            tempEscape.delete(0, tempEscape.length());
                        }
                    } else {
                        // not quite possible but handle it anyway
                        sb.append("&amp;");
                        sb.append(tempEscape.substring(1).toString());
                        tempEscape.delete(0, tempEscape.length());
                    }
                }
            } else if (curr != '&') {
                sb.append(curr);
            } else {
                tempEscape.append(curr);
            }
            
            read = is.read();
        }
        
        if (tempEscape.length() != 0) {
            sb.append("&amp;");
            sb.append(tempEscape.substring(1).toString());
            tempEscape.delete(0, tempEscape.length());
        }
        
        return sb.toString().getBytes();
    }
    
    /**
     * Converts a number string to a boolean with the convention that 1 == true and 0 == false, every other value == null
     * 
     * @param numString
     * @return the boolean derived from numString
     */
    public static Boolean numStringToBooleanObject(final String numString) {
        return toBooleanObject(numString, StandardConstants.STRING_ONE, StandardConstants.STRING_ZERO);
    }
    
    /**
     * Converts a string to a Boolean. If it does not match either trueValue or falseValue, returns null
     * See BooleanUtils.toBooleanObject for other variations
     * 
     * @param str
     *            the value to convert to a boolean
     * @param trueValue
     *            the value that maps to true
     * @param falseValue
     *            the value that maps to false
     * @return the boolean derived from str
     */
    public static Boolean toBooleanObject(final String str, final String trueValue, final String falseValue) {
        if (str == null) {
            return null;
        } else if (str.equalsIgnoreCase(trueValue)) {
            return true;
        } else if (str.equalsIgnoreCase(falseValue)) {
            return false;
        } else {
            return null;
        }
    }
    
}
