package com.digitalpaytech.util.xml;

import com.digitalpaytech.exception.ApplicationException;

// Copied from EMS 6.3.11 com.digitalpaytech.utils.xml.IXMLHandler

public interface XmlHandler {
    String process(byte[] xmlPaystationDocument) throws ApplicationException;
}
