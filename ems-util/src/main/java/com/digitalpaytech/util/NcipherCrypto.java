package com.digitalpaytech.util;

import com.digitalpaytech.exception.CryptoException;

public final class NcipherCrypto extends Crypto
{
	private final static String JCE_PROVIDER = "nCipherKM";
	private final static String KEYSTORE_TYPE = "nCipher.sworld";
	private final static String KEYSTORE_PROVIDER = "nCipherKM";

	protected NcipherCrypto(String keystorePath) throws CryptoException
	{
		super(keystorePath);
		initializeKeystore(KEYSTORE_TYPE, KEYSTORE_PROVIDER);
	}

	public String getJceProvider()
	{
		return (JCE_PROVIDER);
	}
}
