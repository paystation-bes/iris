package com.digitalpaytech.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import org.apache.log4j.Logger;

public final class CurrencyUtil {
    private static final Logger LOG = Logger.getLogger(CurrencyUtil.class);
    private static final Map<String, Integer> CURRENCY_CODES_NUMBERRS_MAP;
    static {
        BufferedReader br = null;
        CURRENCY_CODES_NUMBERRS_MAP = new HashMap<>();
        try {
            br = new BufferedReader(new InputStreamReader(CurrencyUtil.class.getResourceAsStream("/currency.codes.numbers.properties")));
            String s;
            String[] codeAndNumArr;
            while ((s = br.readLine()) != null) {
                codeAndNumArr = s.trim().split(StandardConstants.STRING_EQAL);
                CURRENCY_CODES_NUMBERRS_MAP.put(codeAndNumArr[StandardConstants.CONSTANT_0], Integer.parseInt(codeAndNumArr[StandardConstants.CONSTANT_1]));
            }
        } catch (IOException ioe) {
            final String msg = "Cannot find or read currency.codes.numbers.properties";
            LOG.error(msg, ioe);
            throw new IllegalStateException(msg, ioe);
        }
    }
    
    private CurrencyUtil() {
    }
    
    /**
     * Get currency code by currency code, e.g. "CAD" -> 124, "USD" -> 840.
     * @param currencyCode three characters code
     * @return int currency code or -1 if code doesn't exist.
     */
    public static int getCurrencyNumber(final String currencyCode) {
        final Integer num = CURRENCY_CODES_NUMBERRS_MAP.get(currencyCode);
        if (num == null) {
            return StandardConstants.INT_MINUS_ONE;
        }
        return num;
    }
    
    public static Map<String, Integer> getAllCurrencies() {
        return CURRENCY_CODES_NUMBERRS_MAP;
    }
}
