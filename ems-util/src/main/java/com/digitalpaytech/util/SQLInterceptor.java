package com.digitalpaytech.util;

import org.hibernate.EmptyInterceptor;

public class SQLInterceptor extends EmptyInterceptor {
    private static final long serialVersionUID = -2866037902428049676L;

    @Override
    public String onPrepareStatement(String sql) {
        String result = null;
        if (sql != null) {
            if (sql.charAt(0) == '|') {
                result = sql.substring(1).replaceAll("\\|=", ":=");
            } else {
                result = sql;
            }
        }
        
        return result;
    }
    
}
