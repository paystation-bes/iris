package com.digitalpaytech.util.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.ByteArray;
import com.digitalpaytech.util.CharsetConverter;
import com.digitalpaytech.util.EasyCache;
import com.digitalpaytech.util.EasyCipher;
import com.digitalpaytech.util.EasyPool;
import com.digitalpaytech.util.KeyManager;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.PrimitiveIntepreter;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.primitive.BigDecimalIntepreter;
import com.digitalpaytech.util.primitive.BigIntegerIntepreter;
import com.digitalpaytech.util.primitive.ByteIntepreter;
import com.digitalpaytech.util.primitive.DoubleIntepreter;
import com.digitalpaytech.util.primitive.FloatIntepreter;
import com.digitalpaytech.util.primitive.IntegerIntepreter;
import com.digitalpaytech.util.primitive.LongIntepreter;
import com.digitalpaytech.util.primitive.ShortIntepreter;
import com.digitalpaytech.util.primitive.StringIntepreter;
import com.digitalpaytech.util.support.WebObjectId;

/**
 * The method ensureInit() is not thread-safe. Caller must control thread-safety when using this method.
 * 
 * @author wittawatv
 *
 */
public class CipherIdGenerator extends RandomKeyMapping {
    public static final String REPRESENTATION_NULL = " ";
    
    private static final long serialVersionUID = 7707715906512679398L;
    
    private static final int ASCII_DEC_BEFORE_0 = 47;
    private static final int ASCII_DEC_AFTER_9 = 58;
    private static final int ASCII_DEC_BEFORE_UPPER_A = 64;
    private static final int ASCII_DEC_AFTER_UPPER_Z = 91;
    private static final int ASCII_DEC_BEFORE_LOWER_A = 96;
    private static final int ASCII_DEC_AFTER_LOWER_Z = 123;
    
    private static final int LENGTH_RANDOM_META_ARRAY = 3;
    private static final int MIN_LENGTH_VALID_RANDOM_BYTE_ARRAY = LENGTH_RANDOM_META_ARRAY + 1;
    
    private static final char[] POSSIBLE_CHARS = new char[62];
    static {
        int idx = 0;
        int i = ASCII_DEC_BEFORE_UPPER_A;
        while (++i < ASCII_DEC_AFTER_UPPER_Z) {
            POSSIBLE_CHARS[idx++] = (char) i;
        }
        
        i = ASCII_DEC_BEFORE_LOWER_A;
        while (++i < ASCII_DEC_AFTER_LOWER_Z) {
            POSSIBLE_CHARS[idx++] = (char) i;
        }
        
        i = ASCII_DEC_BEFORE_0;
        while (++i < ASCII_DEC_AFTER_9) {
            POSSIBLE_CHARS[idx++] = (char) i;
        }
    }
    
    private static final byte TYPE_STRING = 0;
    private static final byte TYPE_BYTE = 1;
    private static final byte TYPE_SHORT = 2;
    private static final byte TYPE_INTEGER = 3;
    private static final byte TYPE_LONG = 4;
    private static final byte TYPE_FLOAT = 5;
    private static final byte TYPE_DOUBLE = 6;
    private static final byte TYPE_BIG_INTEGER = 7;
    private static final byte TYPE_BIG_DECIMAL = 8;
    
    @SuppressWarnings("rawtypes")
    private static final PrimitiveIntepreter[] BYTE_INTERPRETERS = new PrimitiveIntepreter[9];
    
    static {
        BYTE_INTERPRETERS[TYPE_BYTE] = new ByteIntepreter();
        BYTE_INTERPRETERS[TYPE_SHORT] = new ShortIntepreter();
        BYTE_INTERPRETERS[TYPE_INTEGER] = new IntegerIntepreter();
        BYTE_INTERPRETERS[TYPE_LONG] = new LongIntepreter();
        BYTE_INTERPRETERS[TYPE_FLOAT] = new FloatIntepreter();
        BYTE_INTERPRETERS[TYPE_DOUBLE] = new DoubleIntepreter();
        BYTE_INTERPRETERS[TYPE_BIG_INTEGER] = new BigIntegerIntepreter();
        BYTE_INTERPRETERS[TYPE_BIG_DECIMAL] = new BigDecimalIntepreter();
        BYTE_INTERPRETERS[TYPE_STRING] = new StringIntepreter() {
            @Override
            public String encode(final byte[] data, final int offset) {
                String result = super.encode(data, offset);
                if (REPRESENTATION_NULL.equals(result)) {
                    result = null;
                }
                
                return result;
            }
        };
    }
    
    private static final Logger LOG = Logger.getLogger(CipherIdGenerator.class);
    
    private Object contextIdentity;
    private EasyPool<EasyCipher> cipherPool;
    private transient EasyCache<ByteArray, ByteArray> cache;
    private CharsetConverter converter;
    
    private Random rand;
    private Map<Class<?>, Short> typesMap;
    private Map<Short, Class<?>> typesMapReverse;
    
    public CipherIdGenerator(final Object contextIdentity, final EasyPool<EasyCipher> cipherPool) {
        this.contextIdentity = contextIdentity;
        this.cipherPool = cipherPool;
        
        clear();
    }
    
    @Override
    public void clear() {
        this.converter = new RandomCharsetConverter(5, POSSIBLE_CHARS);
        
        this.rand = new Random();
        this.typesMap = new HashMap<Class<?>, Short>();
        this.typesMapReverse = new HashMap<Short, Class<?>>();
    }
    
    @Override
    public WebObjectId getKeyObject(final String value) {
        WebObjectId result = null;
        
        byte[] encrypted = null;
        if (value != null) {
            encrypted = this.converter.decode(value);
        } else {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Couldn't decrypt null randomId will just return null for actualId.");
            }
        }
        
        if ((encrypted == null) || (encrypted.length <= 0)) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Fail to decode randomId \"" + value + "\" to byte array");
            }
        } else {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Trying to decrypt randomId: " + value);
            }
            
            byte[] decrypted = null;
            
            final ByteArray encryptedBI = new ByteArray(encrypted);
            ByteArray decryptedBI = this.cache.getKey(encryptedBI);
            if (decryptedBI != null) {
                decrypted = decryptedBI.get();
                if (LOG.isTraceEnabled()) {
                    LOG.trace("Cache hit for randomId: " + value + " !");
                }
            } else {
                try {
                    final PooledResource<EasyCipher> cipher = this.cipherPool.take();
                    decrypted = cipher.get().decrypt(encrypted);
                    cipher.close();
                    
                    decryptedBI = new ByteArray(decrypted);
                    this.cache.putIfAbsent(decryptedBI, encryptedBI);
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Successfully decrypted randomId \"" + value + "\" to " + decryptedBI);
                    }
                } catch (CryptoException ce) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Exception occured while trying to decrypt randomId: " + value, ce);
                    }
                }
            }
            
            if ((decrypted != null) && (decrypted.length >= MIN_LENGTH_VALID_RANDOM_BYTE_ARRAY)) {
                final short typeId = (Short) BYTE_INTERPRETERS[TYPE_SHORT].encode(decrypted);
                final Class<?> type = this.typesMapReverse.get(typeId);
                if (type != null) {
                    final byte flag = decrypted[2];
                    final PrimitiveIntepreter<?> interpreter = BYTE_INTERPRETERS[flag];
                    if (interpreter != null) {
                        result = new WebObjectId(type, interpreter.encode(decrypted, LENGTH_RANDOM_META_ARRAY));
                        if (LOG.isTraceEnabled()) {
                            LOG.trace(String.format("Decrypted randomId [%s] to class [%s] actualId [%s] typeId [%s]", value, result.getObjectType(),
                                                    result.getId(), typeId));
                        }
                    }
                } else {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Unknown data type Id: " + typeId + " (randomId: " + value + "\"");
                    }
                }
            }
        }
        
        if (LOG.isTraceEnabled()) {
            if (result == null) {
                LOG.trace("Resolve actualId to null for randomId \"" + value + "\"");
            } else if (result.getId() == null) {
                LOG.trace("Resolve class \"" + result.getObjectType() + "\" actualId to null for randomId \"" + value + "\"");
            }
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public String getRandomString(final Class<?> beanType, final Object id) {
        String result = null;
        if (beanType != null) {
            final Object idBuffer;
            if (id != null) {
                idBuffer = id;
                if (LOG.isTraceEnabled()) {
                    LOG.trace("Creating randomId for class \"" + beanType + "\" with actualId \"" + id + "\"");
                }
            } else {
                idBuffer = REPRESENTATION_NULL;
                if (LOG.isTraceEnabled()) {
                    LOG.trace("Creating null randomId for class \"" + beanType + "\"");
                }
            }
            
            byte[] decrypted = null;
            
            final byte type;
            if (idBuffer instanceof Integer) {
                type = TYPE_INTEGER;
            } else if (idBuffer instanceof Long) {
                type = TYPE_LONG;
            } else if (idBuffer instanceof Short) {
                type = TYPE_SHORT;
            } else if (idBuffer instanceof String) {
                type = TYPE_STRING;
            } else if (idBuffer instanceof BigInteger) {
                type = TYPE_BIG_INTEGER;
            } else if (idBuffer instanceof Byte) {
                type = TYPE_BYTE;
            } else if (idBuffer instanceof BigDecimal) {
                type = TYPE_BIG_DECIMAL;
            } else if (idBuffer instanceof Double) {
                type = TYPE_DOUBLE;
            } else if (idBuffer instanceof Float) {
                type = TYPE_FLOAT;
            } else {
                throw new IllegalStateException("UnSupported Object Type: " + idBuffer.getClass());
            }
            
            final byte[] decryptedType = BYTE_INTERPRETERS[TYPE_SHORT].decode(getTypeId(beanType));
            final byte[] decryptedData = BYTE_INTERPRETERS[type].decode(idBuffer);
            
            decrypted = new byte[LENGTH_RANDOM_META_ARRAY + decryptedData.length];
            decrypted[0] = decryptedType[0];
            decrypted[1] = decryptedType[1];
            decrypted[2] = type;
            
            System.arraycopy(decryptedData, 0, decrypted, LENGTH_RANDOM_META_ARRAY, decryptedData.length);
            
            if ((decrypted != null) && (decrypted.length > 0)) {
                final ByteArray decryptedBI = new ByteArray(decrypted);
                ByteArray encryptedBI = this.cache.get(decryptedBI);
                
                byte[] encrypted = null;
                if (encryptedBI != null) {
                    encrypted = encryptedBI.get();
                } else {
                    try {
                        final PooledResource<EasyCipher> cipher = this.cipherPool.take();
                        encrypted = cipher.get().encrypt(decrypted);
                        cipher.close();
                        
                        encryptedBI = new ByteArray(encrypted);
                        this.cache.putIfAbsent(decryptedBI, encryptedBI);
                        
                        if (LOG.isTraceEnabled()) {
                            LOG.trace("Successfully encrypted class \"" + beanType + "\" with actualId \"" + id + "\" to " + encryptedBI);
                        }
                    } catch (CryptoException ce) {
                        if (LOG.isTraceEnabled()) {
                            LOG.trace("Exception occured while trying to encrypt class \"" + beanType + "\" with actualId \"" + id + "\"", ce);
                        }
                    }
                }
                
                if ((encrypted != null) && (encrypted.length > 0)) {
                    result = this.converter.encode(encrypted).toString();
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Encrypted class \"" + beanType + "\" with actualId \"" + id + "\" to randomId \"" + result + "\"");
                    }
                }
            }
        }
        
        return result;
    }
    
    private Short getTypeId(final Class<?> beanType) {
        Short type = this.typesMap.get(beanType);
        if (type == null) {
            synchronized (this.rand) {
                int i = Integer.MAX_VALUE;
                type = 0;
                while (this.typesMapReverse.containsKey(type)) {
                    type = new Short((short) this.rand.nextInt(Short.MAX_VALUE));
                    if (--i <= 0) {
                        throw new IllegalStateException("Could not generate any more bean type id !");
                    }
                }
                
                this.typesMap.put(beanType, type);
                this.typesMapReverse.put(type, beanType);
            }
        }
        
        return type;
    }
    
    @Override
    public Object getContextIdentity() {
        return this.contextIdentity;
    }
    
    /**
     * This method is not thread-safe. Caller must control thread-safety when using this method.
     * 
     */
    @SuppressWarnings("unchecked")
    @Override
    public void ensureInit(final Map<String, Object> context) {
        if (this.cache == null) {
            this.cache = (EasyCache<ByteArray, ByteArray>) context.get(KeyManager.CTXKEY_CACHE);
        }
        
        this.converter.ensureInit();
        this.cipherPool.ensureInit();
    }
    
    @Override
    public void logReverseMap() {
        
        if (this.typesMapReverse != null && this.typesMapReverse.keySet() != null) {
            if (this.typesMapReverse.keySet().isEmpty()) {
                LOG.trace("reverseMap keyset is empty");
            } else {
                final StringBuilder sb = new StringBuilder();
                this.typesMapReverse.forEach((x, y) -> sb.append(String.valueOf(x)).append("->").append(String.valueOf(y)).append("\n"));
                LOG.trace(sb.toString());
            }
        } else {
            LOG.trace("reverseMap or keyset is null");
        }
    }
}
