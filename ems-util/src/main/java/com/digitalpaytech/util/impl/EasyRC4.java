package com.digitalpaytech.util.impl;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.digitalpaytech.util.EasyCipher;
import com.digitalpaytech.util.EasySeedGenerator;

public class EasyRC4 implements EasyCipher {
	private static final long serialVersionUID = 6820327297455709580L;
	
	private SecretKey secret;
	
	private transient Cipher encryptor;
	private transient Cipher decryptor;
	
	public EasyRC4() {
		this((String) null);
	}
	
	public EasyRC4(String password) {
		EasySeedGenerator seedGenerator = EasySeedGenerator.getInstance();
		try {
			if(password == null) {
				KeyGenerator generator = KeyGenerator.getInstance("RC4");
				this.secret = generator.generateKey();
			}
			else {
				byte[] salt = ByteBuffer.allocate(8).putLong(seedGenerator.nextLong()).array();
				SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
				KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 2, 128);
				
				SecretKey tmp = factory.generateSecret(spec);
				this.secret = new SecretKeySpec(tmp.getEncoded(), "RC4");
			}
		}
		catch(NoSuchAlgorithmException nsae) {
			throw new IllegalStateException("Unsupported encryption algorithm !");
		}
		catch(InvalidKeySpecException ikse) {
			throw new RuntimeException("Invalid Key Spec for: " + password);
		}
	}

	@Override
	public byte[] encrypt(byte[] plain) {
		byte[] result = null;
		try {
			result = this.encryptor.doFinal(plain);
		}
		catch(BadPaddingException bpe) {
			throw new IllegalStateException("Incorrect padding setup !");
		}
		catch(IllegalBlockSizeException ibse) {
			throw new IllegalStateException("Invalid block size setup !");
		}
		
		return result;
	}

	@Override
	public byte[] decrypt(byte[] encrypted) {
		byte[] result = null;
		if(encrypted != null) {
			try {
				result = this.decryptor.doFinal(encrypted);
			}
			catch(BadPaddingException bpe) {
				System.err.println(this.decryptor.getProvider() + " vs " + this.encryptor.getProvider());
				System.err.println(encrypted.length);
				bpe.printStackTrace(System.err);
				
				throw new IllegalStateException("Incorrect padding setup !");
			}
			catch(IllegalBlockSizeException ibse) {
				ibse.printStackTrace(System.err);
				throw new IllegalStateException("Invalid block size setup !");
			}
		}
		
		return result;
	}
	
	private Cipher createCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
		return Cipher.getInstance("RC4");
	}

	@Override
	public void ensureInit() {
		if(this.encryptor == null) {
			try {
				Cipher encryptor = createCipher();
				encryptor.init(Cipher.ENCRYPT_MODE, this.secret);
				
				this.encryptor = encryptor;
				
				Cipher decryptor = createCipher();
				decryptor.init(Cipher.DECRYPT_MODE, this.secret);
				
				this.decryptor = decryptor;
			}
			catch(NoSuchPaddingException nspe) {
				throw new IllegalStateException("Incorrect Padding !");
			}
			catch(NoSuchAlgorithmException nsae) {
				throw new IllegalStateException("Incorrect Algorithm !");
			}
			catch(InvalidKeyException ike) {
				throw new IllegalStateException("Key is unexpectedly invalid !");
			}
		}
	}
}
