package com.digitalpaytech.util.impl;

import com.digitalpaytech.util.CharsetConverter;
import com.digitalpaytech.util.EasySeedGenerator;

/**
 * The method ensureInit() is not thread-safe. Caller must control thread-safety when using this method.
 * 
 * @author wittawatv
 *
 */
public class RandomCharsetConverter implements CharsetConverter {
	private static final long serialVersionUID = -3237670229004998538L;

	private static final int MASK_BYTE = 0xFF;
	private static final int MASK_OCT = 7;
	
	private int minCharCode = Integer.MAX_VALUE;
	private int maxCharCode = Integer.MIN_VALUE;
	
	private byte bitsPerChar;
	private char[] encodeMap;
	
	private transient int charMask;
	private transient int[] decodeMap;
	
	public RandomCharsetConverter(int bitsPerChar, char[] possibleChars) {
		if(bitsPerChar > 8) {
			throw new IndexOutOfBoundsException();
		}
		
		this.bitsPerChar = (byte) bitsPerChar;
		int charMask = createCharMask(bitsPerChar);
		
		this.encodeMap = new char[charMask + 1];
		if(possibleChars.length < this.encodeMap.length) {
			throw new IllegalArgumentException();
		}
		
		EasySeedGenerator seedGenerator = EasySeedGenerator.getInstance();
		boolean [] used = new boolean[possibleChars.length];
		int i = this.encodeMap.length;
		while(--i >= 0) {
			int candidateIdx = -1;
			do {
				candidateIdx = seedGenerator.nextInt(used.length);
			} while(used[candidateIdx]);
			
			if(possibleChars[candidateIdx] > MASK_BYTE) {
				throw new IllegalArgumentException("Invalid character: " + possibleChars[candidateIdx]);
			}
			
			this.encodeMap[i] = possibleChars[candidateIdx];
			
			used[candidateIdx] = true;
			this.minCharCode = Math.min(minCharCode, (int) this.encodeMap[i]);
			this.maxCharCode = Math.max(maxCharCode, (int) this.encodeMap[i]);
		}
		
		ensureInit();
	}
	
	@Override
	public StringBuilder encode(byte[] data) {
		StringBuilder result = null;
		{
			int totalResult = data.length << 3;
			int totalRemaining = totalResult % this.bitsPerChar;
			if(totalRemaining > 0) {
				totalResult += this.bitsPerChar;
			}
			
			result = new StringBuilder((int) (totalResult / this.bitsPerChar));
		}
		
		int dataIdx = -1;
		
		int remaining = 0;
		int buffer = 0;
		while(++dataIdx < data.length) {
			remaining += 8;
			buffer = (buffer << 8) | (data[dataIdx] & MASK_BYTE);
			while(remaining > this.bitsPerChar) {
				remaining -= this.bitsPerChar;
				result.append(this.encodeMap[(buffer >>> remaining) & this.charMask]);
			}
		}
		
		if(remaining > 0) {
			result.append(this.encodeMap[(buffer << (this.bitsPerChar - remaining)) & charMask]);
		}
		
		return result;
	}

	@Override
	public byte[] decode(String data) {
		byte[] result = null;
		int len = data.length();
		{
			int totalResult = len * this.bitsPerChar;
			int totalRemaining = totalResult & MASK_OCT;
			if(totalRemaining > 0) {
				totalResult = totalResult - totalRemaining;
			}
			
			result = new byte[totalResult >> 3];
		}
		
		int resultIdx = 0;
		int dataIdx = -1;
		
		int total = 0;
		int buffer = 0;
		boolean valid = true;
		while(valid && (++dataIdx < len)) {
			int curr = ((int) data.charAt(dataIdx)) - this.minCharCode;
			
			int decoded = -1;
			if((curr >= 0) && (curr < this.decodeMap.length)) {
				decoded = this.decodeMap[curr];
				valid = decoded >= 0;
			}
			else {
				valid = false;
			}
			
			if(valid) {
				buffer = (buffer << this.bitsPerChar) | decoded;
				total += this.bitsPerChar;
				int newTotal = total - 8;
				if(newTotal >= 0) {
					result[resultIdx++] = (byte) ((buffer >>> newTotal) & MASK_BYTE);
					total = newTotal;
				}
			}
		}
		
		return (valid) ? result : new byte[0];
	}
	
	private int createCharMask(int bitsPerChar) {
		return MASK_BYTE >> (8 - bitsPerChar);
	}
	
	/**
	 * This method is not thread-safe. Caller must control thread-safety when using this method.
	 * 
	 */
	@Override
	public void ensureInit() {
		if(this.decodeMap == null) {
			this.charMask = createCharMask(this.bitsPerChar);
			this.decodeMap = new int[this.maxCharCode - this.minCharCode + 1];
			
			int i = this.decodeMap.length;
			while(--i >= 0) {
				this.decodeMap[i] = -1;
			}
			
			i = this.encodeMap.length;
			while(--i >= 0) {
				this.decodeMap[(int) this.encodeMap[i] - this.minCharCode] = i;
			}
		}
	}
}
