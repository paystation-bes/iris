package com.digitalpaytech.util.impl;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

public class RandomIdGenerator extends RandomKeyMapping {
    private static final long serialVersionUID = -429082391610117180L;
    private static final Logger LOG = Logger.getLogger(RandomIdGenerator.class);
    
    // 1 random seed for each instance
    private Random random = new Random();
    // Using HashMap (not synchronized); switch to HashTable if needed
    private Map map = new ConcurrentHashMap();
    private Map reverseMap = new ConcurrentHashMap();
    
    public RandomIdGenerator() {
        
    }
    
    /**
     * Creates a randomly generated hex string using a random long integer
     * 
     * @param minLength
     *            the min hex length allowed
     * @param maxLength
     *            the max hex length allowed
     * 
     * @return a randomly generated hex string
     */
    public String getRandomLongAsHexString(final int minLength, final int maxLength) {
        long number;
        String hex;
        
        do {
            number = this.random.nextLong();
            hex = Long.toHexString(number);
        } while (!GenericValidator.isInRange(hex.length(), minLength, maxLength));
        
        return hex;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.util.RandomKeyMapping#clear()
     */
    @Override
    public void clear() {
        this.map.clear();
        this.reverseMap.clear();
    }
    
    /**
     * Removes a Map.Entry from the Map
     * 
     * @param key
     *            the Map.Entry key
     */
    public void remove(final Object key) {
        if (key != null) {
            final Object value = this.map.get(key);
            
            this.map.remove(key);
            this.reverseMap.remove(value);
        }
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.util.RandomKeyMapping#getRandomString(java.lang.Class, java.lang.Object)
     */
    @Override
    public String getRandomString(final Class<?> beanType, final Object id) {
        final Object substitute = put(new WebObjectId(beanType, id));
        return (substitute == null) ? null : substitute.toString();
    }
    
    /**
     * Hides a JavaBean's property value by substituting it with a randomly
     * generated value.
     * 
     * @param bean
     *            a JavaBean
     * @param name
     *            name of the property to hide
     * 
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    //	public void hidePkBeanProperty(Object pkBean, String name)
    //			throws IllegalAccessException, InvocationTargetException,
    //			NoSuchMethodException {
    //		// get the property's value
    //		Object value = PropertyUtils.getProperty(pkBean, name);
    //		// create a objectId to collect id and class type info
    //		WebObjectId objId = new WebObjectId(value, pkBean);
    //		// hide the objectId
    //		Object substitute = put(objId);
    //		// replace the property's value with a random key
    //		PropertyUtils.setProperty(pkBean, "primaryKey", substitute);
    //	}
    
    /**
     * Hides a Collection of JavaBeans' property values by substituting them
     * with randomly generated values.
     * 
     * @param collection
     *            a Collection of JavaBeans which have primary key property
     * @param name
     *            name of the property to hide
     * 
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    //	public void hidePkListProperties(Collection pkList, String name)
    //			throws IllegalAccessException, InvocationTargetException,
    //			NoSuchMethodException {
    //		Iterator i = pkList.iterator();
    //		// iterate thru the collection of beans
    //		while (i.hasNext()) {
    //			Object object = i.next();
    //
    //			if (object instanceof Collection) {
    //				// process nested collection
    //				hidePkListProperties((Collection) object, name);
    //			} else {
    //				// hide this bean's property
    //				hidePkBeanProperty(object, name);
    //			}
    //		}
    //	}
    
    /**
     * Retrieves a property value from the Map using its Map.Entry key
     * 
     * @param key
     *            a Map.Entry key
     * 
     * @return the corresponding Map.Entry value
     */
    public Object getValue(final Object key) {
        if (key == null) {
            return null;
        }
        return this.map.get(key);
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.util.RandomKeyMapping#getKeyObject(java.lang.Object)
     */
    @Override
    public WebObjectId getKeyObject(final String value) {
        if (value == null) {
            return null;
        }
        return (WebObjectId) this.reverseMap.get(value);
    }
    
    /**
     * Puts an object into the Map & returns a randomly generated object as its
     * substitute
     * 
     * @param object
     *            the object to hide
     * 
     * @return a randomly generated replacement object
     */
    private Object put(final Object object) {
        final Object substitute;
        
        // test if the object is already in the Map
        if (this.map.containsKey(object)) {
            // return current random value
            substitute = this.map.get(object);
        } else {
            // get a random hex string
            substitute = getRandomLongAsHexString(WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH, WebCoreConstants.RANDOMKEY_MAX_HEX_LENGTH).toUpperCase();
            
            // add entry to the Map
            this.map.put(object, substitute);
            this.reverseMap.put(substitute, object);
        }
        
        return substitute;
    }
    
    @Override
    public void logReverseMap() {
        if (this.reverseMap != null && this.reverseMap.keySet() != null) {
            if (this.reverseMap.keySet().isEmpty()) {
                LOG.trace("reverseMap keyset is empty");
            } else {
                final StringBuilder sb = new StringBuilder();
                this.reverseMap.forEach((x, y) -> sb.append(String.valueOf(x)).append("->").append(String.valueOf(y)).append("\n"));
                LOG.trace(sb.toString());
            }
        } else {
            LOG.trace("reverseMap or keyset is null");
        }
        
    }
}
