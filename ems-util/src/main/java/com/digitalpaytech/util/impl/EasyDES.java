package com.digitalpaytech.util.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.EasyCipher;

@SuppressWarnings("restriction")
public class EasyDES implements EasyCipher {
    /**
     * 
     */
    private static final long serialVersionUID = 408929457635699717L;
    private static Logger log = Logger.getLogger(EasyDES.class);
    private static final String DES_ENCRYPTION_KEY = "DptString@";
    private static final String DATA_ENCRYPTION_STANDARD = "DES";
    
    private SecretKey secret;
    private SecretKey desKey;
    
    private transient Cipher encryptor;
    private transient Cipher decryptor;
    
    public EasyDES() {
        try {
            KeyGenerator generator = KeyGenerator.getInstance(DATA_ENCRYPTION_STANDARD);
            this.secret = generator.generateKey();
            
            DESKeySpec keySpec = new DESKeySpec(DES_ENCRYPTION_KEY.getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DATA_ENCRYPTION_STANDARD);
            this.desKey = keyFactory.generateSecret(keySpec);
        } catch (NoSuchAlgorithmException nsae) {
            log.error(nsae);
            throw new IllegalStateException("Unsupported encryption algorithm !", nsae);
        } catch (InvalidKeyException ie) {
            log.error(ie);
            throw new IllegalStateException(ie);
        } catch (UnsupportedEncodingException ue) {
            log.error(ue);
            throw new IllegalStateException(ue);
        } catch (InvalidKeySpecException ike) {
            log.error(ike);
            throw new IllegalStateException(ike);
        }
    }
    
    @Override
    public byte[] encrypt(byte[] plain) {
        byte[] result = null;
        try {
            result = this.encryptor.doFinal(plain);
        } catch (BadPaddingException bpe) {
            throw new IllegalStateException("Incorrect padding setup !");
        } catch (IllegalBlockSizeException ibse) {
            throw new IllegalStateException("Invalid block size setup !");
        }
        
        return result;
    }
    
    @Override
    public byte[] decrypt(byte[] encrypted) {
        byte[] result = null;
        if (encrypted != null) {
            try {
                result = this.decryptor.doFinal(encrypted);
            } catch (BadPaddingException bpe) {
                StringBuilder bdr = new StringBuilder();
                bdr.append(this.decryptor.getProvider()).append(" vs ").append(this.encryptor.getProvider()).append(", encrypted.length: ").append(encrypted.length);
                log.error(bdr.toString(), bpe);
                throw new IllegalStateException("Incorrect padding setup !", bpe);
            } catch (IllegalBlockSizeException ibse) {
                StringBuilder bdr = new StringBuilder();
                bdr.append(this.decryptor.getProvider()).append(" vs ").append(this.encryptor.getProvider()).append(", encrypted.length: ").append(encrypted.length);
                log.error(bdr.toString(), ibse);
                throw new IllegalStateException("Invalid block size setup !", ibse);
            }
        }
        
        return result;
    }
    
    public String encryptBase64(byte[] plain) {
        String encodedData = null;
        try {
            this.encryptor.init(Cipher.ENCRYPT_MODE, this.desKey);
            byte[] encryptedData = this.encryptor.doFinal(plain);
            encodedData = new BASE64Encoder().encode(encryptedData);
            
        } catch (BadPaddingException bpe) {
            log.error(bpe);
            throw new IllegalStateException("Incorrect padding setup !", bpe);
        } catch (IllegalBlockSizeException ibse) {
            log.error(ibse);
            throw new IllegalStateException("Invalid block size setup !", ibse);
        } catch (InvalidKeyException ike) {
            log.error(ike);
            throw new IllegalStateException(ike);
        }
        return encodedData;
    }
    
    public String decryptBase64(String encrypted) {
        String plain = null;
        encrypted = encrypted.replace(' ', '+');
        try {
            this.decryptor.init(Cipher.DECRYPT_MODE, this.desKey);
            byte[] result = this.decryptor.doFinal(new BASE64Decoder().decodeBuffer(encrypted));
            plain = new String(result);
        } catch (BadPaddingException bpe) {
            StringBuilder bdr = new StringBuilder();
            bdr.append(this.decryptor.getProvider()).append(" vs ").append(this.encryptor.getProvider()).append(", encrypted.length: ").append(encrypted.length());
            log.error(bdr.toString(), bpe);
            throw new IllegalStateException("Incorrect padding setup ! " + bdr.toString(), bpe);
        } catch (IllegalBlockSizeException ibse) {
            log.error(ibse);
            throw new IllegalStateException("Invalid block size setup !", ibse);
        } catch (IOException ioe) {
            log.error(ioe);
            throw new IllegalStateException("IO error, ", ioe);
        } catch (InvalidKeyException ike) {
            log.error(ike);
            throw new IllegalStateException("Invalid key, ", ike);
        }
        return plain;
        
    }
    
    @Override
    public void ensureInit() {
        if (this.encryptor == null) {
            try {
                this.encryptor = Cipher.getInstance(DATA_ENCRYPTION_STANDARD);
                this.encryptor.init(Cipher.ENCRYPT_MODE, this.secret);
                
                this.decryptor = Cipher.getInstance(DATA_ENCRYPTION_STANDARD);
                this.decryptor.init(Cipher.DECRYPT_MODE, this.secret);
            } catch (NoSuchPaddingException nspe) {
                log.error(nspe);
                throw new IllegalStateException("Incorrect Padding !", nspe);
            } catch (NoSuchAlgorithmException nsae) {
                log.error(nsae);
                throw new IllegalStateException("Incorrect Algorithm !", nsae);
            } catch (InvalidKeyException ike) {
                log.error(ike);
                throw new IllegalStateException("Key is unexpectedly invalid !", ike);
            }
        }
    }
}
