package com.digitalpaytech.util.impl;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

import org.apache.log4j.Logger;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.util.EasyCipherKey;
import com.digitalpaytech.util.EasyCipher;

@SuppressWarnings("restriction")
public class EasyAES128 implements EasyCipher {
    private static final long serialVersionUID = 5827189629017661671L;
    
    private static final String JCE_PROVIDER = "SunJCE";
    
    private static final String ADVANCED_ENCRYPTION_STANDARD = "AES";
    // For AES, key must be 128, 192 or 256 bits. This key is 16 bytes (128 bits)
    private static final String AES_ENCRYPTION_KEY = "DptSigningKey@V2";
    private static final String AES_CIPHER_MODE = "AES/CBC/PKCS5Padding";
    
    private static final String MSG_INVALID_ALGORITHM = "Invalid algorithm, ";
    private static final String MSG_INVALID_KEY = "Key is unexpectedly invalid !";
    
    private static final Logger LOG = Logger.getLogger(EasyDES.class);
    
    private EasyCipherKey key;
    
    private transient Cipher encryptor;
    private transient Cipher decryptor;
    
    public EasyAES128() {
    }
    
    public EasyAES128(final EasyCipherKey key) {
        this.key = key;
    }
    
    @Override
    public final byte[] encrypt(final byte[] plain) throws CryptoException {
        byte[] result = null;
        if ((plain == null) || (plain.length <= 0)) {
            result = new byte[0];
        } else {
            try {
                result = this.encryptor.doFinal(plain);
            } catch (BadPaddingException | IllegalBlockSizeException e) {
                throw new CryptoException("Encryption Failed !", e);
            }
        }
        
        return result;
    }
    
    @Override
    public final byte[] decrypt(final byte[] encrypted) throws CryptoException {
        byte[] result = null;
        if ((encrypted == null) || (encrypted.length <= 0)) {
            result = new byte[0];
        } else {
            try {
                result = this.decryptor.doFinal(encrypted);
            } catch (BadPaddingException | IllegalBlockSizeException bpe) {
                throw new CryptoException("Decryption Failed !", bpe);
            }
        }
        
        return result;
    }
    
    @Override
    public final void ensureInit() {
        if (this.encryptor == null) {
            try {
                final IvParameterSpec iv = new IvParameterSpec(this.key.getIv());
                
                final Cipher newEncryptor = Cipher.getInstance(AES_CIPHER_MODE, JCE_PROVIDER);
                newEncryptor.init(Cipher.ENCRYPT_MODE, this.key.getSecret(), iv);
                
                this.encryptor = newEncryptor;
                
                final Cipher newDecryptor = Cipher.getInstance(AES_CIPHER_MODE, JCE_PROVIDER);
                newDecryptor.init(Cipher.DECRYPT_MODE, this.key.getSecret(), iv);
                
                this.decryptor = newDecryptor;
            } catch (NoSuchProviderException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException e) {
                throw new IllegalStateException("Incorrect Cipher Parameters !", e);
            }
        }
    }
    
    public final String encryptBase64(final String plain) {
        String encryptedString = null;
        Cipher cipher;
        final SecretKeySpec secretKeySpec = new SecretKeySpec(AES_ENCRYPTION_KEY.getBytes(), ADVANCED_ENCRYPTION_STANDARD);
        try {
            cipher = Cipher.getInstance(ADVANCED_ENCRYPTION_STANDARD, JCE_PROVIDER);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            
            final byte[] encrypted = cipher.doFinal(plain.getBytes());
            
            final BASE64Encoder base64Encoder = new BASE64Encoder();
            encryptedString = base64Encoder.encodeBuffer(encrypted);
            
        } catch (NoSuchAlgorithmException nae) {
            LOG.error(nae);
            throw new IllegalStateException(MSG_INVALID_ALGORITHM, nae);
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException(MSG_INVALID_KEY, ike);
        } catch (NoSuchProviderException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
            throw new IllegalStateException(e);
        }
        return encryptedString;
    }
    
    public final String decryptBase64(final String encrypted) {
        String decryptedString = null;
        Cipher cipher;
        final String encryptedBuff = encrypted.replace(' ', '+');
        final SecretKeySpec secretKeySpec = new SecretKeySpec(AES_ENCRYPTION_KEY.getBytes(), ADVANCED_ENCRYPTION_STANDARD);
        try {
            cipher = Cipher.getInstance(ADVANCED_ENCRYPTION_STANDARD, JCE_PROVIDER);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            
            final BASE64Decoder base64Decoder = new BASE64Decoder();
            final byte[] decodedByte = base64Decoder.decodeBuffer(encryptedBuff);
            
            final byte[] decrypted = cipher.doFinal(decodedByte);
            
            decryptedString = new String(decrypted).trim();
        } catch (NoSuchAlgorithmException nae) {
            LOG.error(nae);
            throw new IllegalStateException(MSG_INVALID_ALGORITHM, nae);
        } catch (InvalidKeyException ike) {
            throw new IllegalStateException(MSG_INVALID_KEY, ike);
        } catch (NoSuchProviderException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException | IOException e) {
            throw new IllegalStateException(e);
        }
        return decryptedString;
    }
}
