package com.digitalpaytech.util;

public final class MobileConstants {
    
    public static final String DEFAULT_MOBILE_LOGIN_URI = "/mobile/login";
    public static final String DEFAULT_MOBILE_COLLECT_SUMMARY_URI = "/mobile/collect/summary";
    public static final String DEFAULT_MOBILE_COLLECT_STATUS_URI = "/mobile/collect/status";
    public static final String DEFAULT_MOBILE_COLLECT_COLLECTED_URI = "/mobile/collect/collected";
    public static final String DEFAULT_MOBILE_PROVISION_URI = "/mobile/provision";
    public static final String DEFAULT_MOBILE_LOGOUT_URI = "/mobile/logout";
    
    public static final int NO_OF_PARAMS_IN_PROVISION_REQUEST = 6;
    public static final int NO_OF_PARAMS_IN_LOGIN_REQUEST = 9;
    public static final int NO_OF_PARAMS_IN_SUMMARY_REQUEST = 6;
    public static final int NO_OF_PARAMS_IN_LOGOUT_REQUEST = 6;
    public static final int NO_OF_PARAMS_IN_COLLECTED_REQUEST = 10;
    
    public static final String PARAM_NAME_SESSION_TOKEN = "sessiontoken";
    public static final String PARAM_NAME_DEVICE_ID = "device";
    public static final String PARAM_NAME_APPLICATION_ID = "application";
    public static final String PARAM_NAME_USER_NAME = "j_username";
    public static final String PARAM_NAME_USER_PASSWORD = "j_password";
    public static final String PARAM_NAME_LAT = "lat";
    public static final String PARAM_NAME_LONG = "lon";
    public static final String PARAM_NAME_ROUTE = "route";
    public static final String PARAM_NAME_LAST_UPDATE = "lastupdate";
    public static final String PARAM_NAME_ACTIVE = "active";
    public static final String PARAM_NAME_TIMESTAMP = "timestamp";
    public static final String PARAM_NAME_SIGNATURE = "signature";
    public static final String PARAM_NAME_SIGNATURE_VERSION = "signatureversion";
    public static final String PARAM_NAME_SERIAL_NUMBER = "serialnumber";
    public static final String PARAM_NAME_MOBILE_START_GMT = "mobilestartgmt";
    public static final String PARAM_NAME_MOBILE_END_GMT = "mobileendgmt";
    public static final String PARAM_NAME_COLLECTION_TYPE = "collectiontypeid";
    
    public static final String JSON_NAME_FIELD_BILL = "Bill";
    public static final String JSON_NAME_FIELD_COIN = "Coin";
    public static final String JSON_NAME_FIELD_CREDIT = "Credit";
    public static final String JSON_NAME_FIELD_RUNNING_TOTAL = "Running Total";
    public static final String JSON_NAME_FIELD_OVERDUE_COLLECTION = "Overdue Collection";
    
    public static final String ERROR_PREFIX_PROVISION = "1";
    public static final String ERROR_PREFIX_LOGIN = "2";
    public static final String ERROR_PREFIX_SUMMARY = "3";
    public static final String ERROR_PREFIX_STATUS = "4";
    
    public static final String ERROR_INVALID_USERNAME_PASSWORD = "00";
    public static final String ERROR_USER_NOT_FOUND = "01";
    public static final String ERROR_DEVICE_BLOCKED = "02";
    public static final String ERROR_APPLICATION_ID_NOT_INTEGER = "03";
    public static final String ERROR_NO_AVAILABLE_KEY = "04";
    public static final String ERROR_DEVICE_NOT_FOUND = "05";
    public static final String ERROR_KEY_NOT_FOUND = "06";
    public static final String ERROR_SIGNATURE_DOES_NOT_MATCH = "07";
    public static final String ERROR_SYSTEM_ERROR = "08";
    public static final String ERROR_UNABLE_TO_VALIDATE_TOKEN = "09";
    public static final String ERROR_TOKEN_EXPIRED = "10";
    public static final String ERROR_INVALID_SIGNATURE_VERSION = "11";
    public static final String ERROR_ROUTE_NOT_FOR_CUSTOMER = "12";
    public static final String ERROR_UNABLE_TO_VALIDATE_TIMESTAMP = "13";
    public static final String ERROR_UNABLE_TO_PARSE_LASTUPDATED = "14";
    public static final String ERROR_LATITUDE_LONGITUDE_REQUIRED = "15";
    public static final String ERROR_LATITUDE_LONGITUDE_CANNOT_PARSE = "16";
    public static final String ERROR_ROUTE_DOES_NOT_EXIST = "17";
    public static final String ERROR_ACTIVE_PARAMETER_INCORRECT = "18";
    public static final String ERROR_UNABLE_TO_VALIDATE_SERIALNUMBER = "19";
    public static final String ERROR_UNABLE_TO_VALIDATE_COLLECTION_TYPE = "20";
    
    private MobileConstants() {
        
    }
}
