package com.digitalpaytech.util;

import java.util.Random;

import com.digitalpaytech.util.queue.RoutingKeyResolver;

public class RandomizedKeyResolver<M> implements RoutingKeyResolver<M> {
    private Random random;
    
    public RandomizedKeyResolver() {
        this.random = new Random();
    }
    
    @Override
    public String resolve(final M message) {
        return Long.toString(this.random.nextLong());
    }
}
