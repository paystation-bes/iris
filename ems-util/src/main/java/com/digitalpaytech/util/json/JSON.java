package com.digitalpaytech.util.json;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.digitalpaytech.exception.JsonException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public final class JSON {
    private static final JSONConfigurationBean DEFAULT_CONFIG = new JSONConfigurationBean();
    
    static {
        DEFAULT_CONFIG.setAcessByField(true);
        DEFAULT_CONFIG.setFailOnUnknowProperties(false);
        DEFAULT_CONFIG.setAllowCaseInsensitive(false);
        DEFAULT_CONFIG.setWriteNullMapValues(false);
    }
    
    private ObjectMapper jsonMapper;
    
    public JSON() {
        this(DEFAULT_CONFIG);
    }
    
    public JSON(final JSONConfigurationBean configurationBean) {
        this.jsonMapper = new ObjectMapper();
        
        if (configurationBean.isAcessByField()) {
            this.jsonMapper.setVisibilityChecker(this.jsonMapper.getSerializationConfig().getDefaultVisibilityChecker()
                    .withFieldVisibility(JsonAutoDetect.Visibility.ANY).withGetterVisibility(JsonAutoDetect.Visibility.NONE));
        }
        
        this.jsonMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, configurationBean.isAllowCaseInsensitive());
        this.jsonMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, configurationBean.isWriteNullMapValues());
        this.jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, configurationBean.isFailOnUnknowProperties());
    }
    
    public String serialize(final Object obj) throws JsonException {
        try {
            return this.jsonMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new JsonException(e);
        }
    }
    
    public <T> T deserialize(final String json, final Class<T> rootClass) throws JsonException {
        try {
            return this.jsonMapper.readValue(json, rootClass);
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    public <T> T deserialize(final String json, final TypeReference<T> typeReference) throws JsonException {
        try {
            return this.jsonMapper.readValue(json, typeReference);
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    public <T> T deserialize(final byte[] array, final TypeReference<T> typeReference) throws JsonException {
        try {
            
            return this.jsonMapper.readValue(array, typeReference);
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    public Map<String, Object> deserialize(final String json) throws JsonException {
        try {
            
            return this.jsonMapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    public Map<String, Object> deserialize(final File json) throws JsonException {
        try {
            
            return this.jsonMapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    public Map<String, Object> deserialize(final InputStream json) throws JsonException {
        try {
            
            return this.jsonMapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
        } catch (IOException e) {
            throw new JsonException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> convertValue(final Object pojo) throws JsonException {
        try {
            return this.jsonMapper.convertValue(pojo, Map.class);
        } catch (IllegalArgumentException iae) {
            throw new JsonException(iae);
        }
    }
}
