package com.digitalpaytech.util.json;

public class JSONConfigurationBean {
    private boolean allowCaseInsensitive;
    private boolean writeNullMapValues;
    private boolean failOnUnknowProperties;
    private boolean acessByField;
    
    public JSONConfigurationBean() {
        super();
    }
    
    public JSONConfigurationBean(final boolean allowCaseInsensitive, final boolean writeNullMapValues, final boolean failOnUnknowProperties) {
        super();
        this.allowCaseInsensitive = allowCaseInsensitive;
        this.writeNullMapValues = writeNullMapValues;
        this.failOnUnknowProperties = failOnUnknowProperties;
    }
    
    public final boolean isAllowCaseInsensitive() {
        return this.allowCaseInsensitive;
    }
    
    public final void setAllowCaseInsensitive(final boolean allowCaseInsensitive) {
        this.allowCaseInsensitive = allowCaseInsensitive;
    }
    
    public final boolean isWriteNullMapValues() {
        return this.writeNullMapValues;
    }
    
    public final void setWriteNullMapValues(final boolean writeNullMapValues) {
        this.writeNullMapValues = writeNullMapValues;
    }
    
    public final boolean isFailOnUnknowProperties() {
        return this.failOnUnknowProperties;
    }
    
    public final void setFailOnUnknowProperties(final boolean failOnUnknowProperties) {
        this.failOnUnknowProperties = failOnUnknowProperties;
    }

    public final boolean isAcessByField() {
        return this.acessByField;
    }

    public final void setAcessByField(final boolean acessByField) {
        this.acessByField = acessByField;
    }
    
}
