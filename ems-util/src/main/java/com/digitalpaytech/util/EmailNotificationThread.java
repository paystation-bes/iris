package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.digitalpaytech.util.EmailNotification.AttachmentInfo;

public class EmailNotificationThread extends Thread {
	private final static String MAIL_HOST_KEY = "mail.smtp.host";

	private String mailServerUrl;
	private InternetAddress fromAddr;
	private ArrayList<EmailNotification> notifications = new ArrayList<EmailNotification>();
	private ArrayList<EmailNotificationCallback> callbacks = new ArrayList<EmailNotificationCallback>();

	public EmailNotificationThread(String mailServerUrl,
			InternetAddress fromAddr) {
		super();
		this.mailServerUrl = mailServerUrl;
		this.fromAddr = fromAddr;
	}

	public ArrayList<EmailNotification> getNotifications() {
		return notifications;
	}

	public void setNotifications(ArrayList<EmailNotification> notifications) {
		this.notifications = notifications;
	}

	public ArrayList<EmailNotificationCallback> getCallbacks() {
		return callbacks;
	}

	public void setCallbacks(ArrayList<EmailNotificationCallback> callbacks) {
		this.callbacks = callbacks;
	}

	public void run() {
		for (int i = 0; i < notifications.size(); i++) {
			EmailNotification notification = notifications.get(i);
			EmailNotificationCallback callback = callbacks.get(i);
			try {
				MimeMessage message = notification.getMessage();

				if (message == null) {
					Properties props = new Properties();
					props.put(MAIL_HOST_KEY, mailServerUrl);
					Session sess = Session.getDefaultInstance(props);

					message = new MimeMessage(sess);
					message.setSubject(notification.getSubject());
					message.setFrom(fromAddr);
					Address[] addrs = notification.getToAddrs();
					if (addrs != null && addrs.length > 0) {
						message.addRecipients(RecipientType.TO, addrs);
					}
					addrs = notification.getCcAddrs();
					if (addrs != null && addrs.length > 0) {
						message.addRecipients(RecipientType.CC, addrs);
					}
					addrs = notification.getBccAddrs();
					if (addrs != null && addrs.length > 0) {
						message.addRecipients(RecipientType.BCC, addrs);
					}

					MimeMultipart multipart = new MimeMultipart();
					String content = notification.getContent();
					if (content != null) {
						MimeBodyPart textPart = new MimeBodyPart();
						textPart.setText(content);
						multipart.addBodyPart(textPart);
					}
					Collection<AttachmentInfo> attachments = notification
							.getAttachments();
					for (AttachmentInfo attachment : attachments) {
						MimeBodyPart attachmentPart = new MimeBodyPart();
						DataSource source = new FileDataSource(
								attachment.getFile());
						attachmentPart.setDataHandler(new DataHandler(source));
						attachmentPart.setFileName(attachment.getFileName());
						multipart.addBodyPart(attachmentPart);

					}
					message.setContent(multipart);
				}
				Transport.send(message);

				callback.onSuccess();
			} catch (Exception e) {
				callback.onFailure(e);
			}
		}
	}
}
