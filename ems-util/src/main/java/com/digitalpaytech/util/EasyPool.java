package com.digitalpaytech.util;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class EasyPool<T> implements Serializable {
    private static final long serialVersionUID = 3931478981882120248L;
    
    private EasyObjectBuilder<T> objectBuilder;
	private Object buildConfig;
	
	private transient Queue<T> core;
	private int minPoolSize;
	private int approximateSize;
	
	public EasyPool(final EasyObjectBuilder<T> objectBuilder, final Object buildConfig, final int minPoolSize) {
		this.objectBuilder = objectBuilder;
		this.buildConfig = buildConfig;
		
		this.minPoolSize = minPoolSize;
		
		ensureInit();
	}
	
	public PooledResource<T> take() {
		T value = this.core.poll();
		if(value != null) {
			--this.approximateSize;
		}
		else {
			this.approximateSize = 0;
			value = this.objectBuilder.create(this.buildConfig);
			if(value == null) {
				throw new IllegalStateException("Could not create Resource !");
			}
		}
		
		return new Resource<T>(this, value);
	}
	
	public int getMinPoolSize() {
		return minPoolSize;
	}
	
	public void setMinPoolSize(final int newSize) {
		while(this.approximateSize > newSize) {
			this.core.poll();
			--this.approximateSize;
		}
		
		this.minPoolSize = newSize;
	}
	
	private void returnToPool(final Resource<T> r) {
		if(this.approximateSize < this.minPoolSize) {
			this.core.add(r.value);
			++this.approximateSize;
		}
	}
	
	public void ensureInit() {
	    if (this.core == null) {
    	    this.core = new ConcurrentLinkedQueue<T>();
            this.approximateSize = 0;
	    }
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.core.clear();
		this.core = null;
		this.buildConfig = null;
		this.objectBuilder = null;
		
		super.finalize();
	}
	
	protected static class Resource<R> implements PooledResource<R> {
		private EasyPool<R> pool;
		private R value;
		
		protected Resource(final EasyPool<R> pool, final R value) {
			this.pool = pool;
			this.value = value;
		}
		
		@Override
		public R get() {
			if(this.pool == null) {
				throw new IllegalStateException("Resource has been closed !");
			}
			
			return this.value;
		}
		
		@Override
		public void close() {
			this.pool.returnToPool(this);
			this.pool = null;
		}

		@Override
		protected void finalize() throws Throwable {
			this.close();
			super.finalize();
		}
	}
}
