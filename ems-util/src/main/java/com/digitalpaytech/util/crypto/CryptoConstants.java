package com.digitalpaytech.util.crypto;

import java.util.regex.Pattern;

public final class CryptoConstants {
    
    public static final byte CRYPTOKEY_STATUS_NORMAL = 0;
    public static final byte CRYPTOKEY_STATUS_DELETED = 1;
    
    // *********************************************************************
    // SHA service, hashTypes
    // *********************************************************************
    public static final int HASH_BAD_CREDIT_CARD = 0;
    public static final int HASH_PUBLIC_ENCRYPTION_KEY = 1;
    public static final int HASH_CREDIT_CARD_PROCESSING = 2;
    public static final int HASH_FILE_UPLOAD = 3;
    public static final int HASH_IMPORT_ERROR = 4;
    public static final int HASH_TRANSACTION_UPLOAD = 5;
    public static final int HASH_LINK_BAD_CREDIT_CARD = 6;
    
    // *********************************************************************    
    // Hash Algorithms
    // *********************************************************************    
    public static final byte HASH_ALGORITHM_TYPE_SHA1 = 1;
    public static final byte HASH_ALGORITHM_TYPE_SHA256 = 2;
    public static final byte HASH_ALGORITHM_TYPE_SHA1_WITH_RSA = 3;
    public static final byte HASH_ALGORITHM_TYPE_SHA256_WITH_RSA = 4;
    public static final byte HASH_ALGORITHM_TYPE_SHA256_IRIS = 5;
    
    // UPDATE WHEN NEW ALGO SUPPORT ADDED
    public static final byte CURRENT_LATEST_SIGNING_ALGO = HASH_ALGORITHM_TYPE_SHA256_WITH_RSA;
    
    public static final String ALGORITHM_TYPE_ID_PARAMETER = "algtype";
    public static final String SERVICE_CRYPTO_PACKAGE_PREFIX = "com.digitalpaytech.service.crypto.impl.";
    public static final String CRYPTO_ALGORITHM_FILE_SUFFIX = "AlgorithmImpl";
    
    public static final String SHA_1 = "SHA-1";
    public static final String SHA_256 = "SHA-256";
    public static final String SHA_512 = "SHA-512";
    public static final String SHA_256_IRIS = "SHA-256-IRIS";
    
    // KEY Prefixes
    public static final String IRIS_KEY_SOURCE = "Iris";
    public static final String LINK_KEY_SOURCE = "Link";
    public static final String IRIS_EXTERNAL_KEY_PREFIX = "000";
    public static final String IRIS_INTERNAL_KEY_PREFIX = "002";
    public static final String LINK_EXTERNAL_KEY_PREFIX = "004";
    public static final String LINK_INTERNAL_KEY_PREFIX = "006";
    public static final Pattern PTTRN_EXTERNAL_KEY = Pattern.compile("(000|004)(\\d{3})");
    public static final Pattern PTTRN_INTERNAL_KEY = Pattern.compile("(002|006)(\\d{3})");
    
    private CryptoConstants() {
    }
}
