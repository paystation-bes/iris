package com.digitalpaytech.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class EasyCache<K, V> {
    private int minSize;
    private EasyCacheTrimmer cacheTrimmer;
    
    private ReadWriteLock lock;
    private CacheMap<K, V> cache;
    private CacheMap<V, K> reverseCache;
    
    private boolean accessedAfterLast;
    
    private boolean scheduled;
    
    public EasyCache(final int minSize, final EasyCacheTrimmer cacheTrimmer) {
        this.minSize = minSize;
        this.cacheTrimmer = cacheTrimmer;
        
        this.lock = new ReentrantReadWriteLock();
        this.cache = new CacheMap<K, V>();
        this.reverseCache = new CacheMap<V, K>();
    }
    
    public final V get(final K key) {
        V result = null;
        
        final Lock lock = this.lock.readLock();
        lock.lock();
        try {
            result = this.cache.get(key);
        } finally {
            lock.unlock();
        }
        
        this.accessedAfterLast = true;
        return result;
    }
    
    public final K getKey(final V value) {
        K result = null;
        
        final Lock lock = this.lock.readLock();
        lock.lock();
        try {
            result = this.reverseCache.get(value);
        } finally {
            lock.unlock();
        }
        
        this.accessedAfterLast = true;
        return result;
    }
    
    public final V putIfAbsent(final K key, final V value) {
        V result = null;
        
        final Lock lock = this.lock.writeLock();
        lock.lock();
        try {
            result = this.cache.get(key);
            if (result == null) {
                result = value;
                this.cache.put(key, result);
                this.reverseCache.put(result, key);
            }
        } finally {
            lock.unlock();
        }
        
        this.accessedAfterLast = true;
        return result;
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    public void trim() {
        this.scheduled = false;
        if (this.accessedAfterLast) {
            scheduleForTrim();
        } else {
            final Lock lock = this.lock.writeLock();
            lock.lock();
            try {
                this.cache.trim();
                this.reverseCache.trim();
            } finally {
                lock.unlock();
            }
        }
    }
    
    private void scheduleForTrim() {
        if ((!this.scheduled) && (this.cacheTrimmer != null)) {
            this.cacheTrimmer.trimSoon(this);
            
            this.accessedAfterLast = false;
            this.scheduled = true;
        }
    }
    
    private class CacheMap<CK, CV> extends LinkedHashMap<CK, CV> {
        private static final long serialVersionUID = 2820302431358449209L;
        
        private boolean scheduleCleaning;
        private int minSize;
        
        public CacheMap() {
            super(EasyCache.this.minSize, 0.5F, true);
            this.minSize = EasyCache.this.minSize;
        }
        
        @Override
        protected boolean removeEldestEntry(final Entry<CK, CV> eldest) {
            if (!this.scheduleCleaning) {
                if (this.size() > this.minSize) {
                    this.scheduleCleaning = true;
                    scheduleForTrim();
                }
            }
            
            return false;
        }
        
        public final void trim() {
            int cleanSize = this.size() - this.minSize;
            final Iterator<CK> itr = this.keySet().iterator();
            while ((cleanSize > 0) && itr.hasNext()) {
                itr.next();
                itr.remove();
                --cleanSize;
            }
            
            this.scheduleCleaning = false;
        }
    }
}