package com.digitalpaytech.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.util.support.SubscriptionEntry;

public final class CustomerAdminUtil {
    private static Logger log = Logger.getLogger(CustomerAdminUtil.class);
    private static List<CustomerPropertyType> customerPropertyTypes;
    
    private CustomerAdminUtil() {
    }
    
    public static CustomerPropertyType findCustomerPropertyType(final int customerPropertyTypeId) {
        if (customerPropertyTypes == null) {
            return null;
        }
        CustomerPropertyType type;
        final Iterator<CustomerPropertyType> iter = customerPropertyTypes.iterator();
        while (iter.hasNext()) {
            type = iter.next();
            if (type.getId() == customerPropertyTypeId) {
                return type;
            }
        }
        return null;
    }
    
    public static CustomerProperty findCustomerProperty(final int customerPropertyTypeId, final Iterator<CustomerProperty> customerPropertyIter) {
        CustomerProperty prop;
        while (customerPropertyIter.hasNext()) {
            prop = customerPropertyIter.next();
            if (prop.getCustomerPropertyType() != null && prop.getCustomerPropertyType().getId() == customerPropertyTypeId) {
                return prop;
            }
        }
        return null;
    }
    
    /**
     * The logic in createStatusTypes is from SystemAdminMainController, createNewCustomerAndUserAccount method.
     * 
     * @param customer
     * @param customerStatusType
     * @param userStatusType
     * @param accountStatus
     * @param trialDayString
     */
    public static void createStatusTypes(final Customer customer, final CustomerStatusType customerStatusType, final UserStatusType userStatusType,
        final String accountStatus, final String trialDayString, final String timeZone) {
        customer.setTrialExpiryGmt(null);
        if (accountStatus.equals(WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED_LABEL)) {
            customerStatusType.setId(WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED);
            userStatusType.setId(WebCoreConstants.USER_STATUS_TYPE_DISABLED);
        } else if (accountStatus.equals(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED_LABEL)) {
            customerStatusType.setId(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED);
            userStatusType.setId(WebCoreConstants.USER_STATUS_TYPE_ENABLED);
        } else if (accountStatus.equals(WebCoreConstants.CUSTOMER_STATUS_TYPE_TRIAL_LABEL)) {
            customerStatusType.setId(WebCoreConstants.CUSTOMER_STATUS_TYPE_TRIAL);
            userStatusType.setId(WebCoreConstants.USER_STATUS_TYPE_ENABLED);
            final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            
            try {
                //update the expiry date/time to the end of the day
                Date expiryDate = format.parse(trialDayString);
                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(expiryDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                expiryDate = calendar.getTime();
                
                //change this time to a UTC value
                final Date expiryDateGMT = DateUtil.convertTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT, timeZone, expiryDate);
                customer.setTrialExpiryGmt(expiryDateGMT);
            } catch (ParseException e) {
                log.error("+++ Exception occurred when creating TrialExpiryGMT. User Input: " + trialDayString, e);
            }
        }
    }
    
    /**
     * Creates Map<Integer, SubscriptionType>, name is randomId.
     * 
     * @param subscriptionTypes
     *            List of SubscritionType from the database.
     * @return Map<String, SubscriptionType> Populated Map object.
     */
    public static Map<Integer, SubscriptionEntry> createSubscriptionTypeMap(final List<SubscriptionType> subscriptionTypes) {
        final Map<Integer, SubscriptionEntry> map = new HashMap<Integer, SubscriptionEntry>(subscriptionTypes.size());
        final Iterator<SubscriptionType> iter = subscriptionTypes.iterator();
        while (iter.hasNext()) {
            final SubscriptionType sub = iter.next();
            map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), null, false, true));
        }
        return map;
    }
    
    /**
     * Creates Map<Integer, SubscriptionType>, name is randomId.
     * 
     * @param subscriptionTypes
     *            List of SubscritionType from the database.
     * @param customerSubscriptionList
     *            List of Customer Subscriptions from the database
     * @return Map<String, SubscriptionType> Populated Map object.
     */
    public static Map<Integer, SubscriptionEntry> createSubscriptionTypeMap(final List<SubscriptionType> subscriptionTypes,
        final List<SubscriptionType> availableSubscriptionTypes, final List<CustomerSubscription> customerSubscriptionList) {
        final Map<Integer, SubscriptionEntry> map = new HashMap<Integer, SubscriptionEntry>(subscriptionTypes.size());
        final Iterator<SubscriptionType> iter = subscriptionTypes.iterator();
        while (iter.hasNext()) {
            final SubscriptionType sub = iter.next();
            boolean isFound = false;
            for (CustomerSubscription customerSubscription : customerSubscriptionList) {
                if (customerSubscription.getSubscriptionType().getId() == sub.getId()) {
                    for (SubscriptionType availableType : availableSubscriptionTypes) {
                        if (sub.getId() == availableType.getId()) {
                            map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), customerSubscription.getRandomId(),
                                    customerSubscription.isIsEnabled(), true));
                            availableSubscriptionTypes.remove(availableType);
                            isFound = true;
                            break;
                        }
                    }
                    if (!isFound) {
                        map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), customerSubscription.getRandomId(), false, false));
                    }
                    customerSubscriptionList.remove(customerSubscription);
                    break;
                }
            }
        }
        return map;
    }
    
    /**
     * Creates Map<Integer, SubscriptionType>, name is randomId.
     * 
     * @param customerSubscriptionList
     *            List of Customer Subscriptions from the database
     * @return Map<String, SubscriptionType> Populated Map object.
     */
    public static Map<Integer, SubscriptionEntry> createAvailableSubscriptionTypeMap(final List<SubscriptionType> subscriptionTypes,
        final List<SubscriptionType> availableSubscriptionTypes) {
        final Map<Integer, SubscriptionEntry> map = new HashMap<Integer, SubscriptionEntry>(subscriptionTypes.size());
        final Iterator<SubscriptionType> iter = subscriptionTypes.iterator();
        while (iter.hasNext()) {
            final SubscriptionType sub = iter.next();
            boolean isAvailable = false;
            for (SubscriptionType availableType : availableSubscriptionTypes) {
                if (sub.getId() == availableType.getId()) {
                    map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), null, false, true));
                    availableSubscriptionTypes.remove(availableType);
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), null, false, false));
            }
        }
        return map;
    }
    
    public static Map<Integer, SubscriptionEntry> createSubscriptionTypeMapWithId(final List<SubscriptionType> subscriptionTypes) {
        final Map<Integer, SubscriptionEntry> map = new HashMap<Integer, SubscriptionEntry>(subscriptionTypes.size());
        final Iterator<SubscriptionType> iter = subscriptionTypes.iterator();
        while (iter.hasNext()) {
            final SubscriptionType sub = iter.next();
            map.put(sub.getId(), new SubscriptionEntry(sub.getName(), sub.getId(), null, false, true));
        }
        return map;
    }
    
    public static List<SubscriptionType> setRandomIds(final RandomKeyMapping keyMapping, final List<SubscriptionType> subscriptionTypes) {
        final Iterator<SubscriptionType> iter = subscriptionTypes.iterator();
        while (iter.hasNext()) {
            final SubscriptionType sub = iter.next();
            sub.setRandomId(keyMapping.getRandomString(sub, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        return subscriptionTypes;
    }
    
    public static void setCustomerPropertyTypes(final List<CustomerPropertyType> customerPropertyTypes) {
        CustomerAdminUtil.customerPropertyTypes = customerPropertyTypes;
    }
    
    public static Optional<Integer> getUnifiId(final Customer customer) {
        
        final Optional<Integer> unifiId;
        if (customer.getUnifiId() != null) {
            unifiId = Optional.ofNullable(customer.getUnifiId());
        } else {
            unifiId = Optional.empty();
        }
        return unifiId;
    }
}
