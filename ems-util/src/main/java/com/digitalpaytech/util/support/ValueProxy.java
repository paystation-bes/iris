package com.digitalpaytech.util.support;

import java.io.Serializable;

//This Class is not for deserialize !
public class ValueProxy<V, I extends Comparable<I>> implements Serializable, Comparable<ValueProxy<?, I>> {
	private static final long serialVersionUID = -3758815737831653115L;
	
	private V value;
	private I id;
	
	public ValueProxy(I id) {
		this.id = id;
	}
	
	public ValueProxy(I id, V value) {
		this.id = id;
		this.value = value;
	}
	
	public I getId() {
		return id;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		boolean isEquals = (this == obj);
		if((!isEquals) && (obj != null) && (obj instanceof ValueProxy)) {
			ValueProxy<?, ?> other = (ValueProxy<?, ?>) obj;
			isEquals = (this.id == other.id) || ((this.id != null) && (this.id.equals(other.id)));
		}
		
		return isEquals;
	}

	@Override
	public int compareTo(ValueProxy<?, I> o) {
		return (o == null) ? 1 : this.id.compareTo(o.id);
	}

	@Override
	public String toString() {
		return (this.value == null) ? "null" : this.value.toString();
	}
}
