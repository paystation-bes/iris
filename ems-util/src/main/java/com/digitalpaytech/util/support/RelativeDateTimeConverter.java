package com.digitalpaytech.util.support;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class RelativeDateTimeConverter extends AbstractSingleValueConverter {
	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return RelativeDateTime.class.isAssignableFrom(type);
	}

	@Override
	public Object fromString(String str) {
		return null;
	}

}
