package com.digitalpaytech.util.support;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

/*
 * Copied from EMS 6.3.11 com.digitalpioneer.rpc.lotsettings.PaystationSettingZipReader
 */

public class PaystationSettingZipReader extends DefaultHandler {
    public static final String SETTING_FILE_NAME = "bo2data.xml";
    private final static Logger logger__ = Logger.getLogger(PaystationSettingZipReader.class);
    private static final String UNIQUE_ID_TAG = "ConfigID";
    private static final String SETTING_NAME_TAG = "GroupName";
    private static final String ACTIVATION_TIME_TAG = "ActivationDateTime";
    private static final String TIME_ZONE_TAG = "TimeZone";
    private static final String ELECTRONIC_ID_TAG = "ElectronicID";
    
    private StringBuilder elementValue;// = new StringBuilder();
    private SettingsFile paystationSetting_;
    private boolean conversionError = false;
    private String filePath;
    private List<String> serialNumbers = new ArrayList<String>();
    private ZipInputStream zipStream;
    
    public PaystationSettingZipReader(String filePath) {
        this.filePath = filePath;
    }
    
    public PaystationSettingZipReader(byte[] fileContent) {
        this.zipStream = new ZipInputStream(new ByteArrayInputStream(fileContent));
    }
    
    public boolean isSerialNumberInFile(String serialNumber) {
        
        ZipEntry zipEntry = null;
        try {
            while ((zipEntry = this.zipStream.getNextEntry()) != null) {
                if (zipEntry.getName().equals(SETTING_FILE_NAME)) {
                    paystationSetting_ = new SettingsFile();
                    parseXML((InputStream) this.zipStream);
                    for (String existingSerialNumber : serialNumbers) {
                        if (serialNumber.equals(existingSerialNumber)) {
                            return true;
                        }
                    }
                }
            }
        } catch (IOException io) {
            logger__.error(io);
            return false;
        } catch (ParserConfigurationException pc) {
            logger__.error(pc);
            return false;
        } catch (SAXException se) {
            logger__.error(se);
            return false;
        } finally {
            try {
                if (zipStream != null) {
                    zipStream.close();
                }
            } catch (IOException ioe) {
                logger__.error("Unable to close the ZipFileReader inputstream", ioe);
            }
        }
        return false;
    }
    
    public SettingsFile readPaystationSettingZip() throws InvalidDataException {
        InputStream inputStream = null;
        
        try {
            ZipFile zipFile = new ZipFile(filePath);
            Enumeration entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.getName().equals(SETTING_FILE_NAME)) {
                    paystationSetting_ = new SettingsFile();
                    inputStream = zipFile.getInputStream(zipEntry);
                    parseXML(inputStream);
                    if (conversionError) {
                        paystationSetting_ = null;
                        throw new InvalidDataException();
                    }
                    
                    if (checkPaystationSettingData(paystationSetting_)) {
                        return paystationSetting_;
                    }
                }
            }
            logger__.error("File :" + filePath + " does not contain the correct data");
            throw new InvalidDataException("File :" + filePath + " does not contain the correct data");
        } catch (IOException io) {
            logger__.error(io);
            throw new InvalidDataException(io);
        } catch (ParserConfigurationException pc) {
            logger__.error(pc);
            throw new InvalidDataException(pc);
        } catch (SAXException se) {
            logger__.error(se);
            throw new InvalidDataException(se);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ioe) {
                logger__.error("Unable to close the ZipFileReader inputstream", ioe);
            }
        }
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        elementValue = new StringBuilder();
    }
    
    public void endElement(String uri, String name, String qName) {
        if (UNIQUE_ID_TAG.equals(qName)) {
            try {
                paystationSetting_.setUniqueIdentifier(Integer.valueOf(elementValue.toString()));
            } catch (NumberFormatException nfe) {
                logger__.error("Error creating UniqueId: error converting '" + elementValue + "' to Integer", nfe);
                conversionError = true;
            }
        } else if (SETTING_NAME_TAG.equals(qName)) {
            paystationSetting_.setName(elementValue.toString());
        } else if (ACTIVATION_TIME_TAG.equals(qName)) {
            try {
                DateFormat dateFormat = DateUtil.getColonDelimitedDateFormat();
                Date activationDate = dateFormat.parse(elementValue.toString());
                paystationSetting_.setActivationGmt(activationDate);
            } catch (ParseException pe) {
                logger__.error("Error creating activationdate: error converting '" + elementValue + "' to Date", pe);
                conversionError = true;
            }
        } else if (TIME_ZONE_TAG.equals(qName)) {
            logger__.error("Upload PaystationSetting Zip content has TimeZone: " + elementValue.toString());
        } else if (ELECTRONIC_ID_TAG.equals(qName)) {
            serialNumbers.add(elementValue.toString());
        }
    }
    
    public void characters(char ch[], int start, int length) {
        elementValue.append(ch, start, length);
    }
    
    private void parseXML(InputStream inputStream) throws SAXException, IOException, ParserConfigurationException {
        SAXParserFactory sax_parser_factory = SAXParserFactory.newInstance();
        sax_parser_factory.setValidating(false);
        sax_parser_factory.newSAXParser().parse(inputStream, this);
        inputStream.close();
    }
    
    private boolean checkPaystationSettingData(SettingsFile paystationSetting) {
        boolean isNullData = false;
        
        // make sure all the data is not null
        if (paystationSetting.getUniqueIdentifier() == 0) {
            isNullData = true;
        } else if (paystationSetting.getName() == null || paystationSetting.getName().equals("")
                   || !paystationSetting.getName().matches(WebCoreConstants.REGEX_EXTRA_STANDARD_TEXT)) {
            isNullData = true;
        } else if (paystationSetting.getActivationGmt() == null) {
            isNullData = true;
        }
        
        if (isNullData) {
            logger__.error("Cannot have null settings for lot setting");
            return (false);
        }
        
        // convert the date and timezones correctly as per Roy's request
        adjustActivationDate(paystationSetting);
        
        return (true);
    }
    
    private void adjustActivationDate(SettingsFile paystationSetting) {
        // Due to BO2 capability, GMT is sent on the activation date tag
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        calendar.setTime(paystationSetting.getActivationGmt());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        
        // set the new timezone
        GregorianCalendar newCalendar = new GregorianCalendar();
        newCalendar.set(year, month, date, hour, minute, second);
        
        // set the newActivationDate
        paystationSetting.setActivationGmt(newCalendar.getTime());
    }
    
    public List<String> getSerialNumbers() {
        return this.serialNumbers;
    }
    
}
