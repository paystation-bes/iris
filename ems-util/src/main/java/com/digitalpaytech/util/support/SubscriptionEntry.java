package com.digitalpaytech.util.support;

import java.io.Serializable;

public class SubscriptionEntry implements Serializable {
    
    private static final long serialVersionUID = -488079892381780886L;
    private String name;
    private Integer subscriptionTypeId;
    private String customerSubscriptionRandomId;
    private Boolean isActivated;
    private Boolean isAvailable;
    
    public SubscriptionEntry() {
    }
    
    public SubscriptionEntry(String name, Integer subscriptionTypeId, String customerSubscriptionRandomId, boolean isActivated, boolean isAvailable) {
        this.name = name;
        this.subscriptionTypeId = subscriptionTypeId;
        this.customerSubscriptionRandomId = customerSubscriptionRandomId;
        this.isActivated = isActivated;
        this.isAvailable = isAvailable;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Integer getSubscriptionTypeId() {
        return subscriptionTypeId;
    }
    
    public void setSubscriptionTypeId(Integer subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }
    
    public String getCustomerSubscriptionRandomId() {
        return customerSubscriptionRandomId;
    }
    
    public void setCustomerSubscriptionRandomId(String customerSubscriptionRandomId) {
        this.customerSubscriptionRandomId = customerSubscriptionRandomId;
    }
    
    public boolean getIsActivated() {
        return isActivated;
    }
    
    public void setIsActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }
    
    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }
}
