package com.digitalpaytech.util.support;

public interface IWebObject {
	public Object getPrimaryKey();

	public void setPrimaryKey(Object primaryKey);
}
