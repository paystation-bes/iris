package com.digitalpaytech.util.support;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.util.DateUtil;

// This Class is not for deserialize !
public class RelativeDateTime implements Serializable {
	private static final long serialVersionUID = 1529786693853808075L;
	
	public Date date;
	public TimeZone timeZone;
	
	public RelativeDateTime() {
		
	}
	
	public RelativeDateTime(String timeZoneId) {
		this(new Date(), TimeZone.getTimeZone(timeZoneId));
	}
	
	public RelativeDateTime(TimeZone timeZone) {
		this(new Date(), timeZone);
	}
	
	public RelativeDateTime(Date date, String timeZoneId) {
		this(date, TimeZone.getTimeZone(timeZoneId));
	}
	
	public RelativeDateTime(Date date, TimeZone timeZone) {
		this.date = date;
		this.timeZone = timeZone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return DateUtil.getRelativeTimeString(this.date, this.timeZone);
	}
}
