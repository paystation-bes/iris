package com.digitalpaytech.util.support;

import java.io.Serializable;

public class ProcessorNameFormNamePair implements Serializable, Comparable<ProcessorNameFormNamePair> {
	private static final long serialVersionUID = 4203023467203244038L;
	
	private String name;
    private String formName;
    
    public ProcessorNameFormNamePair(String name, String formName) {
        this.name = name;
        this.formName = formName;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFormName() {
        return formName;
    }
    public void setFormName(String formName) {
        this.formName = formName;
    }

	@Override
	public int compareTo(ProcessorNameFormNamePair o) {
		int result = -1;
		if((o == null) || (o.name == null)) {
			result = 1;
		}
		else if(this.name != null) {
			result = this.name.compareTo(o.name);
		}
		
		return result;
	}
    
}
