package com.digitalpaytech.util.support;

import java.io.Serializable;

public class WebObject implements IWebObject, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2226996799870912019L;
	private Object primaryKey;
	private Object wrappedObject;

	public WebObject() {
		this.primaryKey = null;
		this.wrappedObject = null;
	}

	public WebObject(Object primaryKey, Object wrappedObject) {
		this.primaryKey = primaryKey;
		this.wrappedObject = wrappedObject;
	}

	public Object getPrimaryKey() {
		//
		return primaryKey;
	}

	public void setPrimaryKey(Object primaryKey) {
		//
		this.primaryKey = primaryKey;
	}

	public Object getWrappedObject() {
		return wrappedObject;
	}

	public void setWrappedObject(Object wrappedObject) {
		this.wrappedObject = wrappedObject;
	}
}
