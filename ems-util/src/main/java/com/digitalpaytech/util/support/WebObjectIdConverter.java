package com.digitalpaytech.util.support;

import com.digitalpaytech.util.SessionTool;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class WebObjectIdConverter extends AbstractSingleValueConverter {
	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return WebObjectId.class.isAssignableFrom(type);
	}

	@Override
	public Object fromString(String str) {
		return SessionTool.getInstance().getKeyMapping().getKeyObject(str);
	}

}
