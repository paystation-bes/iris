package com.digitalpaytech.util.support;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// This is not Thread Safe !
public class LookAheadMap<K, V> implements Map<K, V> {
	private ObjectHelper<V, K> valueBuilder;
	private Map<K, V> core;
	private Set<K> lookAheadKeys;
	
	public LookAheadMap(ObjectHelper<V, K> valueBuilder) {
		this.valueBuilder = valueBuilder;
		this.core = new HashMap<K, V>();
		this.lookAheadKeys = new HashSet<K>();
	}
	
	public Set<K> lookAheadKeySet() {
		return Collections.unmodifiableSet(this.lookAheadKeys);
	}

	public int size() {
		return core.size();
	}

	public boolean isEmpty() {
		return core.isEmpty();
	}

	public boolean containsKey(Object key) {
		return core.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return core.containsValue(value);
	}

	public V get(Object key) {
		V result = this.core.get(key);
		if(result == null) {
			@SuppressWarnings("unchecked")
			K keyObj = (K) key;
			
			result = this.valueBuilder.createObject(keyObj);
			
			this.core.put(keyObj, result);
			this.lookAheadKeys.add(keyObj); // Throw ClassCastException because there are no way to handle this in here
		}
		
		return result;
	}

	public V put(K key, V value) {
		V existingValue = null;
		if(this.lookAheadKeys.contains(key)) {
			existingValue = this.core.get(key);
		}
		
		if(existingValue == null) {
			existingValue = this.core.put(key, value);
		}
		else {
			this.valueBuilder.copyProperties(value, existingValue);
			this.lookAheadKeys.remove(key);
			
			existingValue = null;
		}
		
		return existingValue;
	}

	public V remove(Object key) {
		this.lookAheadKeys.remove(key);
		return this.core.remove(key);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		if(m instanceof LookAheadMap) {
			throw new IllegalArgumentException("putAll does not support argument that is another LookAheadMap !");
		}
		
		for(Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
		    this.put(entry.getKey(), entry.getValue());
		}
	}

	public void clear() {
		this.lookAheadKeys.clear();
		this.core.clear();
	}

	public Set<K> keySet() {
		return core.keySet();
	}

	public Collection<V> values() {
		return core.values();
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return core.entrySet();
	}

	public boolean equals(Object o) {
		return core.equals(o);
	}

	public int hashCode() {
		return core.hashCode();
	}
}
