package com.digitalpaytech.util.support;

public interface ObjectHelper<T, IV> {
	public T createObject(IV initVal);
	public <ST extends T> void copyProperties(ST source, ST destination);
}
