package com.digitalpaytech.util.support;

import org.apache.commons.beanutils.PropertyUtils;

public class ReflectionObjectHelper<T, IV> implements ObjectHelper<T, IV> {
	private Class<T> clazz;
	
	public ReflectionObjectHelper(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public T createObject(IV initVal) {
		T result = null;
		try {
			result = this.clazz.newInstance();
		}
		catch(Exception e) {
			throw new RuntimeException("Could not create object of type \"" + this.clazz.getName() + "\" via reflection!", e);
		}
		
		return result;
	}

	@Override
	public <ST extends T> void copyProperties(ST source, ST destination) {
		if(destination == null) {
			throw new NullPointerException("Destination Object must not be null");
		}
		
		if(source != null) {
			try {
				PropertyUtils.copyProperties(destination, source);
			}
			catch(Exception e) {
				throw new RuntimeException("Could not copy properties!");
			}
		}
	}
}
