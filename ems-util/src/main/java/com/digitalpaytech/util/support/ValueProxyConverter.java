package com.digitalpaytech.util.support;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class ValueProxyConverter extends AbstractSingleValueConverter {
	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return ValueProxy.class.isAssignableFrom(type);
	}

	@Override
	public Object fromString(String str) {
		return null;
	}
}
