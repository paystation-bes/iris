package com.digitalpaytech.util.support;

import java.io.Serializable;

import com.digitalpaytech.util.SessionTool;

public final class WebObjectId implements Serializable {
    private static final long serialVersionUID = -4902326217510484007L;
    
    private Class<?> objectType;
    private Object id;
    
    public WebObjectId() {
        
    }
    
    public WebObjectId(final Class<?> objectType, final Object id) {
        this();
        
        this.id = id;
        this.objectType = objectType;
    }
    
    public Object getId() {
        return this.id;
    }
    
    public void setId(final Object id) {
        this.id = id;
    }
    
    public Class<?> getObjectType() {
        return this.objectType;
    }
    
    public void setObjectType(final Class<?> objectType) {
        this.objectType = objectType;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if ((obj == null) || !(obj instanceof WebObjectId)) {
            return false;
        }
        
        final WebObjectId objId = (WebObjectId) obj;
        boolean eq = true;
        if (this.id != null) {
            eq = eq && this.id.equals(objId.getId());
        } else {
            eq = eq && (objId.getId() == null);
        }
        if (this.objectType != null) {
            eq = eq && this.objectType.equals(objId.getObjectType());
        } else {
            eq = eq && (objId.getObjectType() == null);
        }
        
        return eq;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result + ((this.objectType == null) ? 0 : this.objectType.hashCode());
        return result;
    }
    
    // This is for Xstream to be able to handle this Object type.
    @Override
    public String toString() {
        // return "[" + this.objectType + " : " + this.id + "]";
        return SessionTool.getInstance().getKeyMapping().getRandomString(this.objectType, this.id);
    }
}
