package com.digitalpaytech.util;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.digitalpaytech.exception.CryptoException;

public class EasyCipherKey implements Serializable {
	private static final long serialVersionUID = -5194783923038761600L;
	
	private SecretKey secret;
	private byte[] iv;
	
	public EasyCipherKey() throws CryptoException {
		this((String) null);
	}
	
	public EasyCipherKey(String password) throws CryptoException {
		EasySeedGenerator seedGenerator = EasySeedGenerator.getInstance();
		this.iv = seedGenerator.generateSeed(16);
		try {
			if(password == null) {
				KeyGenerator generator = KeyGenerator.getInstance("AES");
				generator.init(128);
				
				this.secret = generator.generateKey();
			}
			else {
				byte[] salt = ByteBuffer.allocate(8).putLong(seedGenerator.nextLong()).array();
				SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
				KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 8, 128);
				
				SecretKey tmp = factory.generateSecret(spec);
				this.secret = new SecretKeySpec(tmp.getEncoded(), "AES");
			}
		}
		catch(NoSuchAlgorithmException nsae) {
			throw new CryptoException("AES with PBKDF2WithHmacSHA1 key are not supported !", nsae);
		}
		catch(InvalidKeySpecException ikse) {
			throw new CryptoException("Invalid AES Key Spec for: " + password, ikse);
		}
	}

	public SecretKey getSecret() {
		return secret;
	}

	public byte[] getIv() {
		return iv;
	}

	@Override
	public int hashCode() {
		return this.secret.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if(obj instanceof EasyCipherKey) {
			result = this.secret.equals(((EasyCipherKey) obj).secret);
		}
		
		return result;
	}
}
