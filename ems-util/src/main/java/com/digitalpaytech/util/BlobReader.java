package com.digitalpaytech.util;

import java.io.IOException;
import java.io.InputStream;

public class BlobReader {
    private InputStream inputStream;
    private long blobLength;
    
    public BlobReader(final InputStream inputStream, final long blobLength) {
        super();
        this.inputStream = inputStream;
        this.blobLength = blobLength;
    }
    
    public final InputStream getInputStream() {
        return this.inputStream;
    }
    
    public final void setInputStream(final InputStream inputStream) {
        this.inputStream = inputStream;
    }
    
    public final long getBlobLength() {
        return this.blobLength;
    }
    
    public final void setBlobLength(final long blobLength) {
        this.blobLength = blobLength;
    }
    
    public final void close() throws IOException {
        if (this.inputStream != null) {
            this.inputStream.close();
        }
    }
    
    @Override
    protected final void finalize() throws Throwable {
        super.finalize();
        close();
    }
}
