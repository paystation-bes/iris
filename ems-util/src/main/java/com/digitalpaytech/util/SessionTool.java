package com.digitalpaytech.util;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

// This is not threadsafe but guarantee serialization access to http session's attribute
public class SessionTool {
	public static final String PREFIX_POST_TOKEN = "_DPTPT_";
	
	private static Logger log = Logger.getLogger(SessionTool.class);
	
	private static ThreadLocal<SessionTool> instances = new ThreadLocal<SessionTool>();
	
	private static KeyManager keyManager = new KeyManager(); // This is for the test-cases (ideally, all the test-cases should be run under spring!).
	
	public static void setKeyManager(KeyManager keyManager) {
		SessionTool.keyManager = keyManager;
	}
	
	public static SessionTool getInstance() {
		SessionTool result = instances.get();
		if(result == null) {
			result = getInstance(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest());
		}
		
		return result;
	}
	
	public static SessionTool getInstance(HttpServletRequest request) {
		SessionTool result = instances.get();
		if(result == null) {
			result = new SessionTool(request);
			instances.set(result);
		}
		
		return result;
	}
	
	public static void clear() {
		instances.remove();
	}
	
	private HttpSession session;
	private String sessionLock;
	
	private int temporaryId = -1;
	
	private volatile RandomKeyMapping keyMapping;
	
	/**
	 * This is dummy constructor for KeyManager injection only !
	 */
	public SessionTool() {
		
	}
	
	private SessionTool(HttpServletRequest request) {
		this(request.getSession(false));
	}
	
	private SessionTool(HttpSession session) {
		if(session == null) {
			throw new IllegalStateException("There are no Session associated with this request !");
		}
		
		this.session = session;
		this.sessionLock = this.session.getId().intern();
	}
	
	public RandomKeyMapping getKeyMapping() {
		if(this.keyMapping == null) {
			synchronized(this.sessionLock) {
				if(this.keyMapping == null) {
					this.keyMapping = (RandomKeyMapping) this.session.getAttribute(WebCoreConstants.RANDOM_KEY_MAPPING);
					if(this.keyMapping != null) {
						keyManager.ensureInit(keyMapping);
					}
					else {
						this.keyMapping = keyManager.createKeyMapping();
						this.session.setAttribute(WebCoreConstants.RANDOM_KEY_MAPPING, keyMapping);
					}
				}
			}
		}
		
		return this.keyMapping;
	}
	
	public Object getTransientAttribute(String key) {
		@SuppressWarnings("unchecked")
		TransientWrapper<Object> wrapper = (TransientWrapper<Object>) getAttribute(key);
		return (wrapper == null) ? null : wrapper.get();
	}
	
	public Object getAttribute(String key) {
		return this.session.getAttribute(key);
	}
	
	// WARNING! When memcached turn on and configured for non-sticky, this method would not store the data at all !
	public void setTransientAttribute(String key, Object value) {
		@SuppressWarnings("unchecked")
		TransientWrapper<Object> wrapper = (TransientWrapper<Object>) getAttribute(key);
		if(wrapper == null) {
			wrapper = new TransientWrapper<Object>();
			setAttribute(key, wrapper);
		}
		
		wrapper.set(value);
	}
	
	public void setAttribute(String key, Object value) {
		if(log.isDebugEnabled()) {
			if((value != null) && !(value instanceof Serializable)) {
				log.warn("Putting non-serializable object into HttpSession {'" + key + "' : '" + value.getClass() + "'}");
			}
		}
		
		synchronized(this.sessionLock) {
			this.session.setAttribute(key, value);
		}
	}
	
	public Object setAttributeIfAbsent(String key, Object value) {
		if(log.isDebugEnabled()) {
			if((value != null) && !(value instanceof Serializable)) {
				log.warn("Putting non-serializable object into HttpSession {'" + key + "' : '" + value.getClass() + "'}");
			}
		}
		
		Object result = null;
		synchronized(this.sessionLock) {
			result = this.session.getAttribute(key);
			if(result == null) {
				this.session.setAttribute(key, value);
				result = value;
			}
		}
		
		return result;
	}
	
	public void removeAttribute(String key) {
		synchronized(this.sessionLock) {
			this.session.removeAttribute(key);
		}
	}
	
	public PostToken locatePostToken(String formId) {
		String postTokenId = PREFIX_POST_TOKEN + formId;
		
		PostToken token = null;
		synchronized(this.sessionLock) {
			token = (PostToken) this.session.getAttribute(postTokenId);
			if(token == null) {
				token = new PostToken();
				this.session.setAttribute(postTokenId, token);
			}
		}
		
		return token;
	}
	
	public int getTemporaryIntId() {
		int result;
		synchronized(this.sessionLock) {
			result = this.temporaryId;
			--this.temporaryId;
			if(this.temporaryId == Integer.MIN_VALUE) {
				this.temporaryId = -1;
			}
		}
		
		return result;
	}
}
