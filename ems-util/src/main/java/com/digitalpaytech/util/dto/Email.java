package com.digitalpaytech.util.dto;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public final class Email {
    public static final String ADDRESS_SEPARATOR = ",";
    
    private String sender;
    
    private StringBuilder recipients;
    private StringBuilder recipientsCC;
    private StringBuilder recipientsBCC;
    
    private String subject;
    
    private String type;
    private Object content;
    
    public Email() {
        this.recipients = new StringBuilder();
        this.recipientsCC = new StringBuilder();
        this.recipientsBCC = new StringBuilder();
    }
    
    public MimeMessage buildMimeMessage(final Session mailSession) throws MessagingException {
        final MimeMessage message = new MimeMessage(mailSession);
        
        if (this.sender != null) {
            message.setFrom(new InternetAddress(this.sender));
        }
        
        if (this.recipients.length() > 0) {
            message.setRecipients(Message.RecipientType.TO, this.recipients.substring(1));
        }
        if (this.recipientsCC.length() > 0) {
            message.setRecipients(Message.RecipientType.CC, this.recipientsCC.substring(1));
        }
        if (this.recipientsBCC.length() > 0) {
            message.setRecipients(Message.RecipientType.BCC, this.recipientsCC.substring(1));
        }
        
        message.setSubject(this.subject);
        
        if (this.type == null) {
            message.setText((String) this.content);
        } else {
            message.setContent(this.content, this.type);
        }
        
        return message;
    }
    
    public Email from(final String address) {
        this.sender = address;
        
        return this;
    }
    
    public Email to(final String... addresses) {
        for (String recipient : addresses) {
            this.recipients.append(ADDRESS_SEPARATOR);
            this.recipients.append(recipient);
        }
        
        return this;
    }
    
    public Email cc(final String... addresses) {
        for (String recipient : addresses) {
            this.recipientsCC.append(ADDRESS_SEPARATOR);
            this.recipientsCC.append(recipient);
        }
        
        return this;
    }
    
    public Email bcc(final String... addresses) {
        for (String recipient : addresses) {
            this.recipientsBCC.append(ADDRESS_SEPARATOR);
            this.recipientsBCC.append(recipient);
        }
        
        return this;
    }
    
    public Email subject(final String message) {
        this.subject = message;
        
        return this;
    }
    
    public Email content(final String mimeType, final Object message) {
        this.type = mimeType;
        this.content = message;
        
        return this;
    }
    
    public Email content(final String message) {
        this.type = null;
        this.content = message;
        
        return this;
    }
}
