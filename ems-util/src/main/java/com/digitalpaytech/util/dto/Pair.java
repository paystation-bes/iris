package com.digitalpaytech.util.dto;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Pair<L, R> {
    private L left;
    private R right;
    
    public Pair(final L left, final R right) {
        this.left = left;
        this.right = right;
    }
    
    public L getLeft() {
        return this.left;
    }
    
    public void setLeft(final L left) {
        this.left = left;
    }
    
    public R getRight() {
        return this.right;
    }
    
    public void setRight(final R right) {
        this.right = right;
    }
    
    @Override
    public int hashCode() {
        return this.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        final Pair that = (Pair) obj;
        return new EqualsBuilder().append(this.left, that.left).append(this.right, that.right).isEquals();
    }
    
}
