package com.digitalpaytech.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.server.ServletServerHttpRequest;

public class RestLoggingFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(RestLoggingFilter.class);
    
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
        throws IOException, ServletException {
        ServletRequest requestWrapper = request;
        ServletResponse responseWrapper = response;
        long startTime = 0;
        long endTime = 0;
        
        if (LOGGER.isDebugEnabled()) {
            try {
                requestWrapper = new DPTHttpServletRequestWrapper((HttpServletRequest) request);
                responseWrapper = LoggingFilterUtil.initAndWrapResponse(response);
                logRequest((DPTHttpServletRequestWrapper) requestWrapper);
            } catch (Exception e) {
                LOGGER.error("Exception thrown while logging Api request. CorrelationId:" + LoggingFilterUtil.getCorrelationId() + " Exception: "
                             + e.getMessage());
                
                // reset request response if there were issues setting it so main call doesn't fail     
                requestWrapper = requestWrapper == null ? new DPTHttpServletRequestWrapper((HttpServletRequest) request) : requestWrapper;
                responseWrapper = responseWrapper == null ? LoggingFilterUtil.initAndWrapResponse(response) : responseWrapper;
            }
            startTime = System.currentTimeMillis();
        } else {
            requestWrapper = new DPTHttpServletRequestWrapper((HttpServletRequest) request);
            responseWrapper = new DPTHttpServletResponseWrapper((HttpServletResponse) response);
        }
        
        chain.doFilter(requestWrapper, responseWrapper);
        
        if (LOGGER.isDebugEnabled()) {
            endTime = System.currentTimeMillis();
            long timeTaken = endTime - startTime;
            try {
                if (responseWrapper instanceof DPTCopyableResponseWrapper) {
                    logResponse((DPTCopyableResponseWrapper) responseWrapper, timeTaken);
                } else {
                    LOGGER.error("Response was not wrapped correctly, hence not logging response. CorrelationId: "
                                 + LoggingFilterUtil.getCorrelationId());
                }
            } catch (Exception e) {
                LOGGER.error("Exception thrown while logging Api response. CorrelationId:" + LoggingFilterUtil.getCorrelationId() + " Exception: "
                             + e.getMessage());
            }
        }
    }
    
    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        
    }
    
    private void logResponse(DPTCopyableResponseWrapper responseWrapper, long timeTaken) {
        StringBuilder responseBuilder = new StringBuilder();
        responseBuilder.append("CorrelationId:" + LoggingFilterUtil.getCorrelationId()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Status:" + responseWrapper.getStatus()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Time taken (ms):" + timeTaken).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("ContentType:" + responseWrapper.getContentType()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Headers:" + responseWrapper.getHeaders()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("ResponseBody:" + responseWrapper.getBody());
        
        LOGGER.debug("Api Response: " + responseBuilder.toString());
    }
    
    private void logRequest(DPTHttpServletRequestWrapper requestWrapper) {
        StringBuilder requestBuilder = new StringBuilder();
        
        requestBuilder.append("CorrelationId:" + LoggingFilterUtil.getCorrelationId()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("URI:" + LoggingFilterUtil.getFullURL(requestWrapper)).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("Method:" + requestWrapper.getMethod()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("ContentType:" + requestWrapper.getContentType()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("Headers:" + new ServletServerHttpRequest(requestWrapper).getHeaders()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("RequestBody:" + requestWrapper.getBody());
        
        LOGGER.debug("Api Request: " + requestBuilder.toString());
    }
}
