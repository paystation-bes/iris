package com.digitalpaytech.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * This class wraps HttpServletRequest and enables to read request input stream
 * multiple time.
 * 
 * @author Brian Kim
 * 
 */
public class DPTHttpServletResponseWrapper extends HttpServletResponseWrapper {
    protected int httpStatus = SC_OK;
    protected Map<String, String> headers;
    
    public DPTHttpServletResponseWrapper(HttpServletResponse response) {
        super(response);
        this.headers = new HashMap<String, String>(5, 0.5f);
    }

	@Override
    public void sendError(int sc) throws IOException {
        httpStatus = sc;
        super.sendError(sc);
    }
    
    @Override
    public void sendError(int sc, String msg) throws IOException {
        httpStatus = sc;
        super.sendError(sc, msg);
    }
    
    @Override
    public void setStatus(int sc) {
        httpStatus = sc;
        super.setStatus(sc);
    }
    
    public int getStatus() {
        return httpStatus;
    }
    
    @Override
	public void setHeader(String name, String value) {
		this.headers.put(name, value);
		super.setHeader(name, value);
	}

	@Override
	public void addHeader(String name, String value) {
		this.headers.put(name, value);
		super.addHeader(name, value);
	}
	
	public String getHeader(String name) {
		return this.headers.get(name);
	}
}
