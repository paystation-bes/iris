package com.digitalpaytech.servlet;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class RequestMonitorFilter implements Filter {
	public static final String INITPARAM_MILLISECS_INTERVAL = "millisecsInterval";
	public static final String INITPARAM_REPEATS = "repeats";
	
	public static final long DEFAULT_MONITORING_MILLISECS_INTERVAL = 30000L;
	public static final int DEFAULT_MONITORING_REPEAT_TIMES = 5;
	
	public static final String DEFAULT_QUARTZ_GROUP = "ReqMntrng";
	
	public static final String FMT_REQUEST_ID = "{1} \"{0}\"";
	
	private static final String CTXKEY_THREAD = "currentThread";
	private static final String CTXKEY_REQUEST_URI = "requestURI";
	
	private static final Logger logger = Logger.getLogger(RequestMonitorFilter.class);
	
	private Scheduler scheduler;
	
	private JobDetail jobDetail;
	
	private long millisecsInterval = -1L;
	private int repeats = -1;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		String configBuffer = config.getInitParameter(INITPARAM_MILLISECS_INTERVAL);
		if(configBuffer != null) {
			try {
				this.millisecsInterval = Long.parseLong(configBuffer);
			}
			catch(NumberFormatException nfe) {
				logger.warn("Invalid \"" + INITPARAM_MILLISECS_INTERVAL + "\" configuration using defaul value: " + DEFAULT_MONITORING_MILLISECS_INTERVAL);
			}
		}
		
		if(this.millisecsInterval <= 0L) {
			this.millisecsInterval = DEFAULT_MONITORING_MILLISECS_INTERVAL;
		}
		
		configBuffer = config.getInitParameter(INITPARAM_REPEATS);
		if(configBuffer != null) {
			try {
				this.repeats = Integer.parseInt(configBuffer);
			}
			catch(NumberFormatException nfe) {
				logger.warn("Invalid \"" + INITPARAM_REPEATS + "\" configuration using defaul value: " + DEFAULT_MONITORING_REPEAT_TIMES);
			}
		}
		
		if(this.repeats <= 0) {
			this.repeats = DEFAULT_MONITORING_REPEAT_TIMES;
		}
		
		// Create Quartz scheduler programmaticly to make this independent from other codes.
		Properties conf = new Properties();
		conf.setProperty("org.quartz.threadPool.threadCount", "1");
		conf.setProperty(StdSchedulerFactory.PROP_SCHED_SKIP_UPDATE_CHECK, "true");
		
		try {
			this.scheduler = (new StdSchedulerFactory(conf)).getScheduler();
			this.scheduler.start();
			
			this.jobDetail = JobBuilder
					.newJob()
					.withIdentity("RequestMonitoring", DEFAULT_QUARTZ_GROUP)
					.ofType(MonitoringJob.class)
					.storeDurably(true)
					.build();
			this.scheduler.addJob(this.jobDetail, false);
		}
		catch(SchedulerException se) {
			throw new RuntimeException("Could not initialize Quartz Scheduler !", se);
		}
	}
	
	@Override
	public void destroy() {
		try {
			this.scheduler.shutdown(false);
		}
		catch(SchedulerException se) {
			logger.error("Could not shutdown Quartz Scheduler !", se);
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String requestURI;
		if(request instanceof HttpServletRequest) {
			HttpServletRequest httpReq = (HttpServletRequest) request;
			requestURI = httpReq.getRequestURI();
		}
		else {
			requestURI = "unidentified";
		}
		
		Thread currentThread = Thread.currentThread();
		
		String requestIdentity = MessageFormat.format(FMT_REQUEST_ID, requestURI, currentThread.getName());
		
		Trigger trigger = TriggerBuilder.newTrigger()
				.withSchedule(SimpleScheduleBuilder
						.simpleSchedule()
						.withIntervalInMilliseconds(this.millisecsInterval)
						.withRepeatCount(this.repeats))
				.withIdentity(requestIdentity + " - Trigger", DEFAULT_QUARTZ_GROUP)
				.forJob(this.jobDetail)
				.startAt(new Date(System.currentTimeMillis() + this.millisecsInterval))
				.build();
		
		Map<String, Object> contextData = trigger.getJobDataMap();
		contextData.put(CTXKEY_THREAD, currentThread);
		contextData.put(CTXKEY_REQUEST_URI, requestURI);
		
		boolean scheduled = false;
		try {
			this.scheduler.scheduleJob(trigger);
			scheduled = true;
		}
		catch(SchedulerException se) {
			logger.error("Error while trying to initialize monitoring job!", se);
		}
		
		try {
			chain.doFilter(request, response);
		}
		finally {
			if(scheduled) {
				try {
					this.scheduler.unscheduleJob(trigger.getKey());
				}
				catch(SchedulerException se) {
					logger.error("Error while trying to delete monitoring job!", se);
				}
			}
		}
	}
	
	protected static class MonitoringJob implements Job {
		public MonitoringJob() {
			
		}
		
		@Override
		public void execute(JobExecutionContext context) {
			Trigger trigger = context.getTrigger();
			Map<String, Object> dataMap = trigger.getJobDataMap();
			
			Thread mainThread = (Thread) dataMap.get(CTXKEY_THREAD);
			StackTraceElement[] callStacks = mainThread.getStackTrace();
			
			logger.error("Request took too long to complete for \"" + dataMap.get(CTXKEY_REQUEST_URI) + "\", Dumping stack trace...");
			for(StackTraceElement elem : callStacks) {
			    if (elem.getClassName().startsWith("com.digitalpaytech")) {
			        logger.error(elem.toString());
			    }
			}
		}
	}
}
