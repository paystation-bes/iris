package com.digitalpaytech.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.stream.Collectors;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class DPTCopyableResponseWrapper extends DPTHttpServletResponseWrapper {
    
    private static final Logger LOGGER = Logger.getLogger(DPTCopyableResponseWrapper.class);
    private ServletOutputStream outputStream;
    private PrintWriter writer;
    private ServletOutputStreamCopier copier;
    
    public DPTCopyableResponseWrapper(HttpServletResponse response) throws IOException {
        super(response);
    }
    
    public String getHeaders() {
        if (headers == null) {
            return null;
        }
        return headers.keySet().stream().map(key -> key + "=" + headers.get(key)).collect(Collectors.joining(", ", "{", "}"));
    }
    
    public String getBody() {
        String body = null;
        try {
            flushBuffer();
        } catch (IOException e) {
            LOGGER.error("IO Exception thrown while getting responseBody. CorrelationId:" + LoggingFilterUtil.getCorrelationId() + " Exception: "
                         + e.getMessage());
        } finally {
            byte[] copy = getCopy();
            try {
                body = new String(copy, super.getCharacterEncoding());
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Exception thrown while converting response data to String. CorrelationId:" + LoggingFilterUtil.getCorrelationId()
                             + " Exception: " + e.getMessage());
            }
        }
        return body;
    }
    
    @Override
    public void sendRedirect(String location) throws IOException {
        httpStatus = SC_MOVED_TEMPORARILY;
        super.sendRedirect(location);
    }
    
    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (writer != null) {
            throw new IllegalStateException("getWriter() has already been called on this response.");
        }
        
        if (outputStream == null) {
            outputStream = getResponse().getOutputStream();
            copier = new ServletOutputStreamCopier(outputStream);
        }
        
        return copier;
    }
    
    @Override
    public PrintWriter getWriter() throws IOException {
        if (outputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called on this response.");
        }
        
        if (writer == null) {
            copier = new ServletOutputStreamCopier(getResponse().getOutputStream());
            writer = new PrintWriter(new OutputStreamWriter(copier, getResponse().getCharacterEncoding()), true);
        }
        
        return writer;
    }
    
    @Override
    public void flushBuffer() throws IOException {
        if (writer != null) {
            writer.flush();
        } else if (outputStream != null) {
            copier.flush();
        }
    }
    
    private byte[] getCopy() {
        if (copier != null) {
            return copier.getCopy();
        } else {
            return new byte[0];
        }
    }
}

class ServletOutputStreamCopier extends ServletOutputStream {
    
    private OutputStream outputStream;
    private ByteArrayOutputStream copy;
    
    public ServletOutputStreamCopier(OutputStream outputStream) {
        this.outputStream = outputStream;
        this.copy = new ByteArrayOutputStream(1024);
    }
    
    @Override
    public void write(int b) throws IOException {
        outputStream.write(b);
        copy.write(b);
    }
    
    public byte[] getCopy() {
        return copy.toByteArray();
    }
}
