package com.digitalpaytech.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.cxf.helpers.IOUtils;
import org.apache.log4j.Logger;

/**
 * This class wraps HttpServletRequest and enables to read request input stream
 * multiple time.
 * 
 * @author Brian Kim
 * 
 */
public class DPTHttpServletRequestWrapper extends HttpServletRequestWrapper {
	private static final Logger logger = Logger.getLogger(DPTHttpServletRequestWrapper.class);
	private final String body;

	public DPTHttpServletRequestWrapper(HttpServletRequest request) {
		super(request);
		String in = null;
		try {
			in = new String(IOUtils.readBytesFromStream(request.getInputStream()));
		} catch (IOException e) {
			logger.error("Error reading the request body.");
		}

		this.body = in;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body.getBytes());
		return new CustomServletInputStream(byteArrayInputStream);
	}

	private static class CustomServletInputStream extends ServletInputStream {

		private ByteArrayInputStream byteArrayInputStream;

		CustomServletInputStream(ByteArrayInputStream byteArrayInputStream) {
			this.byteArrayInputStream = byteArrayInputStream;
		}

		@Override
		public int read() throws IOException {
			return byteArrayInputStream.read();
		}
	}
	
	public String getBody() {
	    return this.body;
	}
}
