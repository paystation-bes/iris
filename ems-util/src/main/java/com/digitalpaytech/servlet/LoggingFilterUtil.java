package com.digitalpaytech.servlet;

import java.io.IOException;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggingFilterUtil {
    public static final String UTF8 = "UTF-8";
    public static final String DELIMITER = " ";
    
    public static DPTCopyableResponseWrapper initAndWrapResponse(final ServletResponse response) throws IOException {
        DPTCopyableResponseWrapper responseWrapper;
        if (response.getCharacterEncoding() == null) {
            response.setCharacterEncoding(UTF8);
        }
        responseWrapper = new DPTCopyableResponseWrapper((HttpServletResponse) response);
        return responseWrapper;
    }
    
    public static String getCorrelationId() {
        return Thread.currentThread().getName() + Thread.currentThread().getId();
    }
    
    public static String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();
        
        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }
}
