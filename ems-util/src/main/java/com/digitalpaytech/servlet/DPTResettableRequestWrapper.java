package com.digitalpaytech.servlet;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class DPTResettableRequestWrapper extends HttpServletRequestWrapper {
    private static final Logger LOGGER = Logger.getLogger(DPTResettableRequestWrapper.class);
    private byte[] rawData;
    private ResettableServletInputStream servletStream;
    
    public DPTResettableRequestWrapper(HttpServletRequest request) {
        super(request);
        this.servletStream = new ResettableServletInputStream();
    }
    
    private String getBody(boolean resetStream) {
        try {
            return IOUtils.toString(getReader());
        } catch (IOException e) {
            LOGGER.error("IO Exception thrown while converting reader data to String. CorrelationId:" + LoggingFilterUtil.getCorrelationId()
                         + " Exception: " + e.getMessage());
            return null;
        } catch (Exception e) {
            LOGGER.error("Exception thrown while converting reader data to String. CorrelationId:" + LoggingFilterUtil.getCorrelationId()
                         + " Exception: " + e.getMessage());
            return null;
        } finally {
            if (resetStream) {
                resetInputStream();
            }
        }
    }
    
    public String getBody() {
        return getBody(false);
    }
    
    public String getBodyAndResetStream() {
        return getBody(true);
    }
    
    private void resetInputStream() {
        servletStream.stream = new ByteArrayInputStream(rawData);
    }
    
    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (rawData == null) {
            try {
                rawData = IOUtils.toByteArray(super.getInputStream());
            } catch (Exception e) {
                LOGGER.debug("Exception reading the request body from the stream. CorrelationId:" + LoggingFilterUtil.getCorrelationId()
                             + " Exception: " + e.getMessage());
                rawData = new byte[0];
            }
            servletStream.stream = new ByteArrayInputStream(rawData);
        }
        return servletStream;
    }
    
    @Override
    public BufferedReader getReader() throws IOException {
        if (rawData == null) {
            try {
                rawData = IOUtils.toByteArray(super.getReader());
            } catch (Exception e) {
                LOGGER.debug("Exception reading the request body from the reader. CorrelationId:" + LoggingFilterUtil.getCorrelationId()
                             + " Exception: " + e.getMessage());
                rawData = new byte[0];
            }
            servletStream.stream = new ByteArrayInputStream(rawData);
        }
        return new BufferedReader(new InputStreamReader(servletStream));
    }
    
    private class ResettableServletInputStream extends ServletInputStream {
        
        private InputStream stream;
        
        @Override
        public int read() throws IOException {
            return stream.read();
        }
    }
}
