package com.digitalpaytech.exception;

public class AuthorizationFailedException extends ApplicationException
{
  /**
   * Constructor for AuthorizationFailedException.
   */
  public AuthorizationFailedException()
  {
    super();
  }

  /**
   * Constructor for AuthorizationFailedException.
   * @param message
   */
  public AuthorizationFailedException(String message)
  {
    super(message);
  }

  /**
   * Constructor for AuthorizationFailedException.
   * @param message
   * @param cause
   */
  public AuthorizationFailedException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for AuthorizationFailedException.
   * @param cause
   */
  public AuthorizationFailedException(Throwable cause)
  {
    super(cause);
  }
}
