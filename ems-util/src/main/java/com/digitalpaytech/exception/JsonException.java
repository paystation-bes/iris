package com.digitalpaytech.exception;

public class JsonException extends Exception {


    /**
     * 
     */
    private static final long serialVersionUID = -7638858156908808286L;

    public JsonException(final String message) {
        super(message);
    }
    
    public JsonException(final Throwable cause) {
        super(cause);
    }
    
    public JsonException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
