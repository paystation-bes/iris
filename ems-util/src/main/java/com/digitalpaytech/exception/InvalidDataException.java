package com.digitalpaytech.exception;

public class InvalidDataException extends ApplicationException
{
  /**
   * Constructor for InvalidDataException.
   */
  public InvalidDataException()
  {
    super();
  }

  /**
   * Constructor for InvalidDataException.
   * @param message
   */
  public InvalidDataException(String message)
  {
    super(message);
  }

  /**
   * Constructor for InvalidDataException.
   * @param message
   * @param cause
   */
  public InvalidDataException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for InvalidDataException.
   * @param cause
   */
  public InvalidDataException(Throwable cause)
  {
    super(cause);
  }
}
