package com.digitalpaytech.exception;

public class InvalidPermitNumberException extends ApplicationException
{
  /**
   * Constructor for InvalidPermitNumberException.
   */
  public InvalidPermitNumberException()
  {
    super();
  }

  /**
   * Constructor for InvalidPermitNumberException.
   * @param message
   */
  public InvalidPermitNumberException(String message)
  {
    super(message);
  }

  /**
   * Constructor for InvalidPermitNumberException.
   * @param message
   * @param cause
   */
  public InvalidPermitNumberException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for InvalidPermitNumberException.
   * @param cause
   */
  public InvalidPermitNumberException(Throwable cause)
  {
    super(cause);
  }
}
