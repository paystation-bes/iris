package com.digitalpaytech.exception;

public class WebWidgetServiceException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = -5762370309381365098L;
	public WebWidgetServiceException(String msg) {
		super(msg);
	}
	public WebWidgetServiceException(Throwable t) {
		super(t);
	}
	public WebWidgetServiceException(String msg, Throwable t) {
		super(msg, t);
	}
}
