package com.digitalpaytech.exception;

public class InvalidTimestampException extends ApplicationException
{
  /**
   * Constructor for InvalidTimestampException.
   */
  public InvalidTimestampException()
  {
    super();
  }

  /**
   * Constructor for InvalidTimestampException.
   * @param message
   */
  public InvalidTimestampException(String message)
  {
    super(message);
  }

  /**
   * Constructor for InvalidTimestampException.
   * @param message
   * @param cause
   */
  public InvalidTimestampException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for InvalidTimestampException.
   * @param cause
   */
  public InvalidTimestampException(Throwable cause)
  {
    super(cause);
  }
}
