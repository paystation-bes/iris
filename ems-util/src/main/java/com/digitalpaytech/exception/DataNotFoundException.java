package com.digitalpaytech.exception;

public class DataNotFoundException extends ApplicationException
{
  /**
   * Constructor for DataNotFoundException.
   */
  public DataNotFoundException()
  {
    super();
  }

  /**
   * Constructor for DataNotFoundException.
   * @param message
   */
  public DataNotFoundException(String message)
  {
    super(message);
  }

  /**
   * Constructor for DataNotFoundException.
   * @param message
   * @param cause
   */
  public DataNotFoundException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for DataNotFoundException.
   * @param cause
   */
  public DataNotFoundException(Throwable cause)
  {
    super(cause);
  }
}
