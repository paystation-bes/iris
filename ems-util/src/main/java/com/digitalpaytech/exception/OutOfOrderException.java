package com.digitalpaytech.exception;

public final class OutOfOrderException extends ApplicationException {
    private static final long serialVersionUID = 5726586021187150585L;

    public OutOfOrderException() {
        super();
    }
    
    public OutOfOrderException(final String message) {
        super(message);
    }
}
