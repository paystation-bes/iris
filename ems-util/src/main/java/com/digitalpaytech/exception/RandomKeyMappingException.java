package com.digitalpaytech.exception;

public class RandomKeyMappingException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = -1946549849077013732L;

	public RandomKeyMappingException(String cause) {
		super(cause);
	}
	
	public RandomKeyMappingException(String cause, Throwable t) {
		super(cause, t);
	}
	
	public RandomKeyMappingException(Throwable t) {
		super(t);
	}
}
