package com.digitalpaytech.exception;

public class InvalidCharactersException extends ApplicationException {
    /**
     * 
     */
    private static final long serialVersionUID = 104608273396263459L;

    public InvalidCharactersException() {
        super();
    }
    
    public InvalidCharactersException(String message) {
        super(message);
    }
    
    public InvalidCharactersException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public InvalidCharactersException(Throwable cause) {
        super(cause);
    }
}
