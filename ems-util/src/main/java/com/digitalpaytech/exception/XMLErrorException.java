package com.digitalpaytech.exception;

public class XMLErrorException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = -5514725692982942784L;
	public XMLErrorException(String msg) {
		super(msg);
	}
	public XMLErrorException(Throwable t) {
		super(t);
	}
	public XMLErrorException(String msg, Throwable t) {
		super(msg, t);
	}
}
