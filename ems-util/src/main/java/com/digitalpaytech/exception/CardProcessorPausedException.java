package com.digitalpaytech.exception;

public class CardProcessorPausedException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -1120161503633471616L;
    public CardProcessorPausedException() {
    }
    public CardProcessorPausedException(final String msg) {
        super(msg);
    }
}
