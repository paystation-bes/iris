package com.digitalpaytech.exception;

public class CryptoException extends ApplicationException {
    /**
     * 
     */
    private static final long serialVersionUID = 8768990395554935235L;
    
    /**
     * Constructor for CryptoException.
     */
    public CryptoException() {
        super();
    }
    
    /**
     * Constructor for CryptoException.
     * 
     * @param message
     */
    public CryptoException(final String message) {
        super(message);
    }
    
    /**
     * Constructor for CryptoException.
     * 
     * @param message
     * @param cause
     */
    public CryptoException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor for CryptoException.
     * 
     * @param cause
     */
    public CryptoException(final Throwable cause) {
        super(cause);
    }
}
