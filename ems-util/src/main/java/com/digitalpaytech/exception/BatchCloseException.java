package com.digitalpaytech.exception;

public final class BatchCloseException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -2435779769786349371L;
    public BatchCloseException() {
        super();
    }
    public BatchCloseException(final String msg) {
        super(msg);
    }
    public BatchCloseException(final Throwable t) {
        super(t);
    }
    public BatchCloseException(final String msg, final Throwable t) {
        super(msg, t);
    }
}
