package com.digitalpaytech.exception;

public class IncorrectCredentialsException extends ApplicationException
{
  /**
   * Constructor for IncorrectCredentialsException.
   */
  public IncorrectCredentialsException()
  {
    super();
  }

  /**
   * Constructor for IncorrectCredentialsException.
   * @param message
   */
  public IncorrectCredentialsException(String message)
  {
    super(message);
  }

  /**
   * Constructor for IncorrectCredentialsException.
   * @param message
   * @param cause
   */
  public IncorrectCredentialsException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructor for IncorrectCredentialsException.
   * @param cause
   */
  public IncorrectCredentialsException(Throwable cause)
  {
    super(cause);
  }
}
