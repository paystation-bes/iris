package com.digitalpaytech.exception;

public class CardProcessorException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = 7706394097726143885L;
	public CardProcessorException(String msg) {
		super(msg);
	}
	public CardProcessorException(Throwable t) {
		super(t);
	}
	public CardProcessorException(String msg, Throwable t) {
		super(msg, t);
	}
}
