package com.digitalpaytech.exception;

public class MissingCryptoKeyException extends CryptoException {
	private static final long serialVersionUID = 3810045824766666785L;
	
	private String keyInfo;
	
	public MissingCryptoKeyException(String keyInfo) {
		super("Key is missing from keystore. This might happened because keystore hasn't been updated after key rotation");
		
		this.keyInfo = keyInfo;
	}

	public String getKeyInfo() {
		return keyInfo;
	}
}
