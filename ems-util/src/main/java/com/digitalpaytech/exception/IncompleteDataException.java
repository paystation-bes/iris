package com.digitalpaytech.exception;

public class IncompleteDataException extends ApplicationException {
    private static final long serialVersionUID = -5150770549417380869L;

    public static final String DEFAULT_MESSAGE = "";
    
    private Class<?> type;
    private Object id;
    
    public IncompleteDataException(final String message, final Class<?> type, final Object id) {
        super(message);
        
        this.type = type;
        this.id = id;
    }
    
    public IncompleteDataException(final Class<?> type, final Object id) {
        this(DEFAULT_MESSAGE, type, id);
    }

    public final Class<?> getType() {
        return this.type;
    }

    public final Object getId() {
        return this.id;
    }
}
