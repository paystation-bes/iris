package com.digitalpaytech.exception;

public class DuplicateObjectException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = -8538799707487748645L;
	public DuplicateObjectException(String msg) {
		super(msg);
	}
	public DuplicateObjectException(Throwable t) {
		super(t);
	}
	public DuplicateObjectException(String msg, Throwable t) {
		super(msg, t);
	}
}
