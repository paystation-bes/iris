package com.digitalpaytech.exception;

public class ProcessorAuthenticationException extends RuntimeException {
    

    /**
     * 
     */
    private static final long serialVersionUID = 2330169810212067093L;

    public ProcessorAuthenticationException(final String message, final Throwable t) {
        super(message, t);
    }
    
}
