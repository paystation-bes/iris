package com.digitalpaytech.exception;

public class DuplicateTransactionRequestException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 3932423324659147997L;
    public DuplicateTransactionRequestException(String msg) {
        super(msg);
    }
    public DuplicateTransactionRequestException(Throwable t) {
        super(t);
    }
    public DuplicateTransactionRequestException(String msg, Throwable t) {
        super(msg, t);
    }
}
