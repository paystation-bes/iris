package com.digitalpaytech.exception;

/**
 * Handy Exception to let spring rollback the transaction when the business-logic is not actually error!
 * 
 * @author wittawatv
 *
 */
public class CardTransactionIgnorableException extends RuntimeException {
    private static final long serialVersionUID = 8959891257253078207L;
    
}
