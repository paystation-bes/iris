package com.digitalpaytech.exception;

/**
 * This exception indicates some low-level exceptions from the system which
 * normally the application is not able to recover from such as an IOException.
 * This exception class supports the J2ME API.
 */
public class SystemException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4193667153478509144L;

	Throwable rootCause_ = null;

	/**
	 * Constructor for SystemException.
	 */
	public SystemException() {
		super();
	}

	/**
	 * Constructor for SystemException.
	 * 
	 * @param message
	 */
	public SystemException(String message) {
		super(message);
	}

	/**
	 * Constructor for SystemException.
	 * 
	 * @param message
	 * @param cause
	 */
	public SystemException(String message, Throwable cause) {
		super(message);

		rootCause_ = cause;
	}

	/**
	 * Constructor for SystemException.
	 * 
	 * @param cause
	 */
	public SystemException(Throwable cause) {
		rootCause_ = cause;
	}

	public Throwable getCause() {
		return rootCause_;
	}
}