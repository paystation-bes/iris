package com.digitalpaytech.exception;

public class RESTCouponPushException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -6967065647502121943L;

    public RESTCouponPushException() {
        super();
    }
    
    public RESTCouponPushException(int status) {
        super(String.valueOf(status));
    }
    
    public RESTCouponPushException(Throwable cause) {
        super(cause);
    }
    
    public RESTCouponPushException(int status, Throwable cause) {
        super(String.valueOf(status), cause);
    }
}
