package com.digitalpaytech.exception;

public class CcProcessingRestrictionException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = 8653784620462291363L;
    
    private boolean unsubscribedService;

    public CcProcessingRestrictionException(String msg) {
		super(msg);
	}
    
    public CcProcessingRestrictionException(String msg, boolean unsubscribedService) {
    	super(msg);
    	this.unsubscribedService = unsubscribedService;
    }
	
	public CcProcessingRestrictionException(Throwable t) {
		super(t);
	}
	
	public CcProcessingRestrictionException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public boolean isUnsubscribedService() {
		return unsubscribedService;
	}
}
