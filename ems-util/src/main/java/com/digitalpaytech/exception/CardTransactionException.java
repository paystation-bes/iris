package com.digitalpaytech.exception;

public class CardTransactionException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = 7549942678006006964L;

	public CardTransactionException(String msg) {
		super(msg);
	}
	
	public CardTransactionException(Throwable t) {
		super(t);
	}
	
	public CardTransactionException(String msg, Throwable t) {
		super(msg, t);
	}
}
