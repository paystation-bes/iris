package com.digitalpaytech.exception;

public class DuplicateMerchantAccountException extends Exception {
	/**
     * 
     */
    private static final long serialVersionUID = 5812809317224820553L;
	public DuplicateMerchantAccountException(String msg) {
		super(msg);
	}
	public DuplicateMerchantAccountException(Throwable t) {
		super(t);
	}
	public DuplicateMerchantAccountException(String msg, Throwable t) {
		super(msg, t);
	}
}
