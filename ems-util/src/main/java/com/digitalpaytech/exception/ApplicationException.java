package com.digitalpaytech.exception;

/**
 * This exception indicates an application error the end user may be able to
 * recover from.
 */
public class ApplicationException extends Exception {
    private static final long serialVersionUID = 4647700112416008990L;
    
    /**
     * Constructor for ApplicationException.
     */
    public ApplicationException() {
        super();
    }
    
    /**
     * Constructor for ApplicationException.
     * 
     * @param message
     */
    public ApplicationException(final String message) {
        super(message);
    }
    
    /**
     * Constructor for ApplicationException.
     * 
     * @param message
     * @param cause
     */
    public ApplicationException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor for ApplicationException.
     * 
     * @param cause
     */
    public ApplicationException(final Throwable cause) {
        super(cause);
    }
}
