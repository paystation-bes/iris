package com.digitalpaytech.exception;

public class InvalidCSVDataException extends ApplicationException {
    /**
	 * 
	 */
    private static final long serialVersionUID = -8648222053978048595L;
    
    //	Throwable rootCause_ = null;
    
    /**
     * Constructor for InvalidCSVDataException.
     */
    public InvalidCSVDataException() {
        super();
    }
    
    /**
     * Constructor for InvalidCSVDataExceptionn.
     * 
     * @param message
     */
    public InvalidCSVDataException(String message) {
        super(message);
    }
    
    /**
     * Constructor for InvalidCSVDataException.
     * 
     * @param message
     * @param cause
     */
    public InvalidCSVDataException(String message, Throwable cause) {
        super(message, cause);
        //		rootCause_ = cause;
    }
    
    /**
     * Constructor for InvalidCSVDataException.
     * 
     * @param cause
     */
    public InvalidCSVDataException(Throwable cause) {
        super(cause);
        //		rootCause_ = cause;
    }
    
    /*
     * public Throwable getCause() {
     * return rootCause_;
     * }
     */
}
