package com.digitalpaytech.velocity.tools;

public class VelocityStringTools {
	public static String[] ensureArrayLength(String[] src, int expectedLength) {
		String[] result = null;
		
		String[] srcArray = src;
		if(srcArray == null) {
			srcArray = new String[0];
		}
		
		if(srcArray.length >= expectedLength) {
			result = srcArray;
		}
		else {
			result = new String[expectedLength];
			
			System.arraycopy(srcArray, 0, result, 0, srcArray.length);
			
			int idx = srcArray.length - 1;
			while(++idx < result.length) {
				result[idx] = "";
			}
		}
		
		return result;
	}
}
