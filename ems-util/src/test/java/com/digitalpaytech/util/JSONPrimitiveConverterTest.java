package com.digitalpaytech.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class JSONPrimitiveConverterTest {
    @Test
    @SuppressWarnings({"checkstyle:multiplestringliterals", "checkstyle:magicnumber"})
    public final void convertAndPrintTest() {
        assertEquals(convert("0.0"), Double.valueOf(0.0));
        assertEquals(convert("0."), "0.");
        assertEquals(convert(".0"), ".0");
        assertEquals(convert("00.0"), "00.0");
        assertEquals(convert("0.00"), "0.00");
        assertEquals(convert("0"), Double.valueOf(0.0));
        assertEquals(convert("1.0"), Double.valueOf(1.0));
        assertEquals(convert("1."), "1.");
        assertEquals(convert(".1"), ".1");
        assertEquals(convert("01.0"), "01.0");
        assertEquals(convert("0.01"), Double.valueOf(0.01));
        assertEquals(convert("0.0001"), Double.valueOf(0.0001));
        assertEquals(convert("0.00010"), "0.00010");
        assertEquals(convert("1.42342"), Double.valueOf(1.42342));
        assertEquals(convert("123234321.0"), Double.valueOf(123234321.0));
        assertEquals(convert("123234321.343253245"), "123234321.343253245");
        assertEquals(convert("0.343253245"), Double.valueOf(0.343253245));
        assertEquals(convert("0.3432532450"), "0.3432532450");
        assertEquals(convert("3432532450"), Double.valueOf(3432532450D));
        assertEquals(convert("4503599627370496"), 4503599627370496D);
        assertEquals(convert("4503599627370497"), "4503599627370497");
        assertEquals(convert("0.4503599627370496"), 0.4503599627370496D);
        assertEquals(convert("0.4503599627370497"), "0.4503599627370497");
        assertEquals(convert("402F"), "402F");
        assertEquals(convert("402D"), "402D");
        assertEquals(convert(String.valueOf(Float.valueOf("402"))), 402D);
        assertEquals(convert(String.valueOf(403D)), 403D);
        assertEquals(convert(Double.toString(403D)), 403D);
        assertEquals(convert(Float.toString(403F)), 403D);
        assertEquals(convert("true"), true);
        assertEquals(convert("false"), false);
    }
    
    private Object convert(final String text) {
        final JSONPrimitiveConverter cnvtr = new JSONPrimitiveConverter();
        final Object result = cnvtr.convertToJSONPrimitive(text);
        return result;
    }
}
