package com.digitalpaytech.util;

import org.junit.Test;
import static org.junit.Assert.*;

public class TelemetryUtilTest {
    
    @Test
    public final void getUpgradeProgressMessageKey() {
        assertEquals("upgrade.progress.init", TelemetryUtil.getUpgradeProgressMessageKey(0));
        assertEquals("upgrade.progress.sent.upgrade.notification", TelemetryUtil.getUpgradeProgressMessageKey(1));
        assertEquals("upgrade.progress.received.upgrade.details", TelemetryUtil.getUpgradeProgressMessageKey(2));
        assertEquals("upgrade.progress.reschedule", TelemetryUtil.getUpgradeProgressMessageKey(3));
        assertEquals("upgrade.progress.received.download.started", TelemetryUtil.getUpgradeProgressMessageKey(4));
        assertEquals("upgrade.progress.received.download.complete", TelemetryUtil.getUpgradeProgressMessageKey(5));
        assertEquals("upgrade.progress.received.upgrade.complete", TelemetryUtil.getUpgradeProgressMessageKey(6));
        assertEquals("upgrade.progress.error", TelemetryUtil.getUpgradeProgressMessageKey(7));
        assertEquals("upgrade.progress.not.started", TelemetryUtil.getUpgradeProgressMessageKey(999));
    }
}
