package com.digitalpaytech.util.kafka;

import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.json.JSON;

public final class MockMessageProducer extends AbstractMessageProducer {
    
    private Properties producerConf = new Properties();
    
    private Properties kafkaTopicsProperties = new Properties();
    
    private Queue<Completion> nextCompletions = new ConcurrentLinkedQueue<>();
    
    public MockMessageProducer() {
        super.setJson(new JSON());
        this.setProducer(new MockProducer<>(false, new StringSerializer(), new StringSerializer()));
    }
    
    public static MockMessageProducer getInstance() {
        return InstanceHolder.INSTANCE;
    }
    
    @Override
    public Properties getTopicProperties() {
        return this.kafkaTopicsProperties == null ? new Properties() : this.kafkaTopicsProperties;
    }
    
    public void overrideTopicConf(final Properties topicConf) {
        final Properties mainTopicConf = getTopicProperties();
        synchronized (mainTopicConf) {
            mainTopicConf.putAll(topicConf);
        }
    }
    
    public void overrideTopicConf(final String key, final String val, final boolean force) {
        final Properties mainTopicConf = getTopicProperties();
        synchronized (mainTopicConf) {
            if (force || !mainTopicConf.contains(key)) {
                mainTopicConf.put(key, val);
            }
            
            if (force || !mainTopicConf.contains(val)) {
                mainTopicConf.put(val, key);
            }
        }
    }
    
    public void clear() {
        this.nextCompletions.clear();
        mockProducer().clear();
    }
    
    public void successNext() {
        this.nextCompletions.add(new Completion());
    }
    
    public void failNext(final RuntimeException error) {
        this.nextCompletions.add(new Completion(error));
    }
    
    public List<ProducerRecord<String, String>> history() {
        return mockProducer().history();
    }
    
    @Override
    public <T> Future<Boolean> send(final String topicType, final String key, final T data) throws InvalidTopicTypeException, JsonException {
        overrideTopicConf(topicType, topicType, false);
        
        final Future<Boolean> result = super.send(topicType, key, data);
        final Completion completion = this.nextCompletions.poll();
        if (completion == null) {
            mockProducer().completeNext();
        } else {
            mockProducer().errorNext(completion.error());
        }
        
        return result;
    }
    
    @Override
    public <T> Future<Boolean> sendWithByteArray(final String topicType, final String key, final T data)
        throws InvalidTopicTypeException, JsonException {
        return send(topicType, key, data);
    }
    
    @Override
    protected void init() {
        if (super.getProducer() == null) {
            final StringSerializer serializer = new StringSerializer();
            super.setProducer(new KafkaProducer<>(this.producerConf, serializer, serializer));
        }
    }
    
    private MockProducer<String, String> mockProducer() {
        return (MockProducer<String, String>) this.producer();
    }
    
    private static class Completion {
        private RuntimeException error;
        
        Completion(final RuntimeException error) {
            this.error = error;
        }
        
        Completion() {
            this((RuntimeException) null);
        }
        
        public RuntimeException error() {
            return this.error;
        }
    }
    
    @Override
    protected void validate(final Properties topicsProps, final CompletableFuture<Boolean> result, final String topicType) {
        if (topicType == null || topicType.isEmpty() || topicType.contains("@")) {
            result.completeExceptionally(new MessageProducingException(
                    MessageFormat.format("Topic configuration could not be found for : {0}", this.getClass().getName())));
        }
    }
    
    private static class InstanceHolder {
        public static final MockMessageProducer INSTANCE = new MockMessageProducer();
    }
    
}
