package com.digitalpaytech.util.kafka.impl;

import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Map;

import org.junit.*;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;
import org.apache.commons.io.IOUtils;

import com.digitalpaytech.util.kafka.MessageValues;

public class MessageHeadersServiceImplTest {
    private static final Logger LOG = Logger.getLogger(MessageHeadersServiceImplTest.class);
    private String messageWithHeaders;
    
    @Before
    public void loadSnfSuccessfulChargeFile() {
        try {
            this.messageWithHeaders = IOUtils.toString(new InputStreamReader(MessageHeadersServiceImplTest.class.getResourceAsStream("/snf_successful_charge.kafkamsg")));
            if (this.messageWithHeaders == null || this.messageWithHeaders.length() == 0) {
                fail("Cannot read snf_successful_charge.kafkamsg");
            }
        } catch (IOException ioe) {
            LOG.error(ioe);
            fail(ioe.getMessage());
        }
    }
    
    
    @Test
    public void extractHeaders() {
        final MessageHeadersServiceImpl serv = new MessageHeadersServiceImpl();
        try {
            final MessageValues extractHeaders = serv.extractHeaders(this.messageWithHeaders);
            final Map<String, String> headers = extractHeaders.getHeadersMap();
            final Object payload = extractHeaders.getPayload();

            headers.forEach((key, value) -> LOG.info("key: " + key + " - value: " + value));
            LOG.info(payload);

            assertNotNull(headers);
            assertEquals(2, headers.size());
            assertNotNull(payload);
        
            
        } catch (IOException e) {
            LOG.error(e);
        }
    }
}
