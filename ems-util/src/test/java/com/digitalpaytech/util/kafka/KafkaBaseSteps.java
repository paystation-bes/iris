package com.digitalpaytech.util.kafka;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.jbehave.core.annotations.Then;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.json.JSON;

import junit.framework.Assert;

public class KafkaBaseSteps extends AbstractSteps {
    private static final String WHITESPACE = "\\s";
    private static final String MSG_FMT_SENT = "The message has been sent to topic {0}";
    private static final String MSG_FMT_NOT_SENT = "The message has not been sent to topic {0}";
    
    private final JSON json = new JSON();
    
    public KafkaBaseSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    /**
     * Assert that message is sent to the kafka topic.
     * 
     * @.example Then the message is sent to kafka's topic 'device.upgrade'
     * @param topicName
     *            name of the target topic
     */
    @Then("the message is sent to kafka's topic '$topicName'")
    public final void messageSent(final String topicName) {
        boolean matched = false;
        final Iterator<ProducerRecord<String, String>> itr = MockMessageProducer.getInstance().history().iterator();
        while (!matched && itr.hasNext()) {
            final ProducerRecord<String, String> record = itr.next();
            matched = record.topic().equals(topicName);
        }
        
        Assert.assertTrue(MessageFormat.format(MSG_FMT_SENT, topicName), matched);
    }
    
    /**
     * Assert that message is sent to the kafka topic with the given data.
     * Does not check if other data is present.
     * Useful for ignoring some fields such as dates which are set by Iris.
     * 
     * @.example Then the message is sent to kafka's topic 'device.upgrade' with data {"deviceId" : "123456789"}
     * @param topicName
     *            name of the target topic
     * @throws JsonException
     * @throws ClassNotFoundException
     */
    @Then("the message is sent to kafka's topic '$topicName' with data $data")
    public final void messageSentWithData(final String topicName, final String data) throws JsonException, ClassNotFoundException {
        boolean matched = false;
        String sentData = null;
        final Iterator<ProducerRecord<String, String>> itr = MockMessageProducer.getInstance().history().iterator();
        while (!matched && itr.hasNext()) {
            final ProducerRecord<String, String> record = itr.next();
            matched = record.topic().equals(topicName);
            if (matched) {
                sentData = record.value();
            }
        }
        
        Assert.assertTrue(MessageFormat.format(MSG_FMT_SENT, topicName), matched);
        
        final boolean matchesExactly = data.replaceAll(WHITESPACE, "").equals(sentData.replaceAll(WHITESPACE, ""));
        
        if (!matchesExactly) {
            
            final Map<String, Object> dataMap = this.json.deserialize(data);
            final Map<String, Object> sentMap = this.json.deserialize(sentData);
            
            for (String key : dataMap.keySet()) {
                final Object expected = dataMap.get(key);
                final Object actual = sentMap.get(key);
                
                Assert.assertNotNull(key, actual);
                Assert.assertEquals(key, expected, actual);
            }
        }
    }
    
    /**
     * Assert that message is not sent to the kafka topic.
     * 
     * @.example Then the message is not sent to kafka's topic 'device.upgrade'
     * @param topicName
     *            name of the target topic
     */
    @Then("the message is not sent to kafka's topic '$topicName'")
    public final void messageNotSent(final String topicName) {
        boolean matched = false;
        final Iterator<ProducerRecord<String, String>> itr = MockMessageProducer.getInstance().history().iterator();
        while (!matched && itr.hasNext()) {
            final ProducerRecord<String, String> record = itr.next();
            matched = record.topic().equals(topicName);
        }
        
        Assert.assertFalse(MessageFormat.format(MSG_FMT_NOT_SENT, topicName), matched);
    }
    
}
