package com.digitalpaytech.util;

import org.junit.Test;

import static org.junit.Assert.*;
import junit.framework.Assert;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.CreditCardType;


public class CardProcessingUtilTest {
    
    private static final char DEMI = '=';
    private static Logger log = Logger.getLogger(CardProcessingUtilTest.class);
    
    @Test
    public void loadExampleCardNumbers() {
        CardProcessingUtil.loadExampleCardNumbers();
        assertNotNull(CardProcessingUtil.getExampleCardNumbers());
        
        List<String> nums = CardProcessingUtil.getExampleCardNumbers();
        assertEquals(16, nums.size());
    }
    
    @Test
    public void isValidPattern() {
        assertTrue(CardProcessingUtil.isValidPattern("5[1-5]\\d{14}=\\d{7,20}"));
        assertTrue(CardProcessingUtil.isValidPattern("4\\d{15}=\\d{7,20}"));
        assertTrue(CardProcessingUtil.isValidPattern("7\\d{15}=\\d{8,21}"));
    }
    
    @Test
    public void getCentsAmountInDollars() {
        int dollar = 100;
        float cents = CardProcessingUtil.getCentsAmountInDollars(dollar).floatValue();
        assertEquals(1, cents, 0);
        
        dollar = 275;
        cents = CardProcessingUtil.getCentsAmountInDollars(dollar).floatValue();
        assertEquals(2.75, cents, 0);
        
        dollar = 1005;
        String centsStr = CardProcessingUtil.getCentsAmountInDollars(dollar).toString();
        assertEquals("10.05", centsStr);
        
        Double centsDou = CardProcessingUtil.getCentsAmountInDollars(dollar).doubleValue();
        log.info("Double toString: " + centsDou.toString());
        assertEquals(10.05, centsDou, 0);
        
        // Use floatValue would cast down BigDecimal and unable to produce exact value.
        Float centsFlo = CardProcessingUtil.getCentsAmountInDollars(dollar).floatValue();
        log.info("Float toString: " + centsFlo.toString());
        //assertEquals(10.05, centsFlo, 0);
        
        dollar = 75;
        cents = CardProcessingUtil.getCentsAmountInDollars(dollar).floatValue();
        assertEquals(0.75, cents, 0);
        
        dollar = 1000;
        cents = CardProcessingUtil.getCentsAmountInDollars(dollar).floatValue();
        assertEquals(10, cents, 0);
    }
    
    @Test
    public void createAccountNumber() {
        String cardNum = "4111111111111111";
        String cardExpMMYY = "0630";
        assertEquals("4111111111111111=3006", CardProcessingUtil.createAccountNumber(cardNum, cardExpMMYY));
        
    }
    
    @Test
    public void getFirst6DigitsOfAccountNumber() {
        final String track2 = "4111111111111111=2011000000000000";
        assertEquals("411111", CardProcessingUtil.getFirst6DigitsOfAccountNumber(track2, DEMI));
        
        final String acctNum = "424242424242424";
        assertEquals("424242", CardProcessingUtil.getFirst6DigitsOfAccountNumber(acctNum, DEMI));
    }
    
    @Test
    public void getFirst6Last4DigitsOfAccountNumber() {
        final String track2 = "4343434343437777=2011000000000000";
        assertEquals("434343******7777", CardProcessingUtil.getFirst6Last4DigitsOfAccountNumber(track2, DEMI));
        
        final String acctNum = "511111424240123";
        assertEquals("511111******0123", CardProcessingUtil.getFirst6Last4DigitsOfAccountNumber(acctNum, DEMI));
    }
    
    @Test
    public void createRedisKey() {
        final MerchantAccount ma = new MerchantAccount();
        ma.setField1("000123");
        ma.setField2("777");
        final String key = CardProcessingUtil.createRedisKey(ma);
        assertEquals("000123-777", key);
    }
    
    @Test
    public void createMD5CheckSum() {
        final String fileName = "checkSumTest.txt";
        final File f = new File(fileName);
        if (!f.exists()) {
            FileWriter fw = null;
            BufferedWriter bw = null;
            try {
                fw = new FileWriter(f);
                bw = new BufferedWriter(fw);
                bw.write("This is a checkSum file !!");
                
            } catch (IOException e) {
                e.printStackTrace();
                fail();
            } finally {
                if (bw != null) {
                    try {
                        bw.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                        fail();
                    }
                }
                if (fw != null) {
                    try {
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        
        String checkSum = null;
        try {
            checkSum = CardProcessingUtil.createMD5Checksum(fileName);
        } catch (IOException | NoSuchAlgorithmException   e) {
            Assert.fail(e.toString());
        }
        log.debug("-=> " + checkSum);
        assertNotNull(checkSum);
        
        if (f.exists()) {
            f.delete();
        }
    }
    
    @Test
    public void generateAlphanumericHash() {
        int pointOfSaleId = 88116;
        
        String code1 = CardProcessingUtil.generateAlphanumericHash(pointOfSaleId);
        assertNotNull(code1);
        String code2 = CardProcessingUtil.generateAlphanumericHash(pointOfSaleId);
        assertEquals(code1, code2);
        assertEquals(4, code1.length());
        assertEquals(4, code2.length());
        assertEquals('0', code1.charAt(3));
        
        // generate 3 chars
        pointOfSaleId = 238327;
        String code3 = CardProcessingUtil.generateAlphanumericHash(pointOfSaleId);
        assertNotNull(code3);
        assertEquals(4, code3.length());
        if (code1.equals(code3)) {
            fail("code 1 should NOT be the same as code 3, code1: " + code1 + ", code3: " + code3);
        }
        
        // generate 4 chars
        pointOfSaleId = 238328;
        String code4 = CardProcessingUtil.generateAlphanumericHash(pointOfSaleId);
        assertEquals(4, code4.length());
        
        // Too Short Collision
        int base = 61;
        for (int i = 1; i <= base; i++) {
            int candidate = i + i * base;
            if (CardProcessingUtil.generateAlphanumericHash(i).equals(CardProcessingUtil.generateAlphanumericHash(candidate))) {
               fail(i + " collides with " + candidate);
            }
        }
    }
    
    @Test
    public void hasAuthorizationNumberAndPreAuthId() {
        assertTrue(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId("G2345R:23"));
        assertTrue(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId("5:2"));
        assertFalse(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId("5"));
        assertFalse(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId("5:"));
        assertFalse(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId(""));
        assertFalse(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId(null));
        assertFalse(CardProcessingUtil.hasAuthorizationNumberAndPreAuthId(":3"));
    }
    
    @Test
    public void isSinglePhaseProcessor() {
        assertTrue(CardProcessingUtil.isSinglePhaseProcessor(CardProcessingConstants.PROCESSOR_ID_HEARTLAND_SPPLUS));
        assertFalse(CardProcessingUtil.isSinglePhaseProcessor(CardProcessingConstants.PROCESSOR_ID_HEARTLAND));
        assertFalse(CardProcessingUtil.isSinglePhaseProcessor(CardProcessingConstants.PROCESSOR_ID_FIRST_DATA_CONCORD));
        assertFalse(CardProcessingUtil.isSinglePhaseProcessor(0));
        assertFalse(CardProcessingUtil.isSinglePhaseProcessor(-1));
        
    }
    
    @Test
    public void isNotNullAndIsLink() {
        MerchantAccount ma = null;
        assertFalse(CardProcessingUtil.isNotNullAndIsLink(ma));
        ma = new MerchantAccount();
        assertFalse(CardProcessingUtil.isNotNullAndIsLink(ma));
        ma.setIsLink(true);
        assertTrue(CardProcessingUtil.isNotNullAndIsLink(ma));
    }
    
    @Test
    public void createCardProcessorPausedExceptionMessage() {
        final String msg = CardProcessingUtil.createCardProcessorPausedExceptionMessage(1, "testMA");
        assertNotNull(msg);
        if (msg.isEmpty()) {
            fail();
        }
    }
    
    @Test
    public void buildRefundProcTrans() {
        final ProcessorTransaction origPtd = new ProcessorTransaction();
        origPtd.setId(new Long(1));
        origPtd.setAmount(500);
        origPtd.setReferenceNumber("aaa");
        origPtd.setIsApproved(true);
        origPtd.setProcessingDate(new Date());
        origPtd.setProcessorTransactionId("processorTransactionId");
        origPtd.setAuthorizationNumber("authorizationNumber");
        
        final ProcessorTransaction refund = CardProcessingUtil.buildRefundProcTrans(origPtd);
        assertEquals(false, refund.isIsApproved());
        assertEquals(500, refund.getAmount());
        assertEquals(StandardConstants.STRING_ZERO, refund.getReferenceNumber());
        assertEquals("", refund.getProcessorTransactionId());
        assertNull(refund.getProcessingDate());
        assertNull(refund.getAuthorizationNumber());
    }
    
    @Test
    public void getFirstCardTypeOrNA() {
        assertEquals(WebCoreConstants.N_A_STRING, CardProcessingUtil.getFirstCardTypeOrNA(new ArrayList<PaymentCard>()));
        assertEquals(WebCoreConstants.N_A_STRING, CardProcessingUtil.getFirstCardTypeOrNA(null));
        
        final CreditCardType visa = new CreditCardType(2, "VISA", new Date(), 1);
        final PaymentCard visaPc = new PaymentCard();
        visaPc.setCreditCardType(visa);

        final CreditCardType mc = new CreditCardType(3, "MasterCard", new Date(), 1);
        final PaymentCard mcPc = new PaymentCard();
        mcPc.setCreditCardType(mc);
        
        final Collection<PaymentCard> col = new ArrayList<PaymentCard>();
        col.add(visaPc);
        col.add(mcPc);
        assertEquals("VISA", CardProcessingUtil.getFirstCardTypeOrNA(col));
    }
    
    @Test
    public void isBlankOrZero() {
        assertTrue(CardProcessingUtil.isBlankOrZero(""));
        assertTrue(CardProcessingUtil.isBlankOrZero(" "));
        assertTrue(CardProcessingUtil.isBlankOrZero(null));
        assertTrue(CardProcessingUtil.isBlankOrZero("0"));
        assertFalse(CardProcessingUtil.isBlankOrZero("1"));
    }
    
    @Test
    public void isCreditcallRequest() {
        //public static boolean isCreditcallRequest(final Processor creditcallProcessor, final String processorName) {
        final Processor creditcall = new Processor();
        creditcall.setId(20);
        creditcall.setName("processor.creditcall");
        
        final Processor creditcallLink = new Processor();
        creditcallLink.setId(23);
        creditcallLink.setName("processor.creditcall.link");
        
        assertTrue(CardProcessingUtil.isCreditcallRequest("processor.creditcall", creditcall, creditcallLink));
        assertTrue(CardProcessingUtil.isCreditcallRequest("processor.creditcall.link", creditcall, creditcallLink));
        
        assertTrue(CardProcessingUtil.isCreditcallRequest("processor.creditcall", creditcall));
        assertTrue(CardProcessingUtil.isCreditcallRequest("processor.creditcall.link", creditcallLink));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.creditcall", creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.creditcall.link", creditcall));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.elavon", creditcall, creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.elavon", creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.elavon", creditcall));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.moneris", creditcall, creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.moneris", creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.moneris", creditcall));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, null));
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, new Processor[0]));
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, new Processor[] { null }));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.paymentech", null));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.paymentech", new Processor[0]));
        assertFalse(CardProcessingUtil.isCreditcallRequest("processor.paymentech", new Processor[] { null }));
        
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, creditcall, creditcallLink));
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, creditcall));
        assertFalse(CardProcessingUtil.isCreditcallRequest(null, creditcallLink));
    }
    
    @Test
    public void getAcceptedIfAcceptedOrAlreadyDone() {
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                                                                           "", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                                                                           "", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                                                                           null, 
                                                                           CardProcessingConstants.TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                                                                           null, 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "TransactionAlreadyRefunded", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "Transaction Already Refunded", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));

        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "TransactionAlreadyVoided", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "Transaction Already Voided", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));        

        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "TransactionAlreadySettled", 
                                                                           CardProcessingConstants.TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES));    
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_ACCEPTED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "Transaction Already Settled", 
                                                                           CardProcessingConstants.TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           null, 
                                                                           CardProcessingConstants.TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "DECLINED", 
                                                                           CardProcessingConstants.TX_ALREADY_SETTLED_WITH_WITHOUT_SPACES));
        
        assertEquals(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                     CardProcessingUtil.getAcceptedIfAcceptedOrAlreadyDone(StandardConstants.STRING_STATUS_CODE_DECLINED, 
                                                                           "declined", 
                                                                           CardProcessingConstants.TX_ALREADY_REFUNDED_VOIDED_WITH_WITHOUT_SPACES));
        
    }
    
    @Test
    public void hasFSwipeCardType() {
        String cardType = "'Card type':'FSwipe'";
        String cardEaseDataJson1 = 
                "{'Transaction type':'SALE','CardReference':'***','CardHash':'***',"; 
        String cardEaseDataJson2 = ",'Total':'2.90','CVM':'NO CARDHOLDER VERIFICATION',"
                + "'Err code':'0000','Date':'17/01/19','Time':'16:01','Ref':'***','Currency':'***','Auth Code':'***','Acquirer':'VISA','Merchant':'***'," 
                + "'Pan':'***','Application':'VISA','AID':'','95':'','9F02':'***','9F10':'','9F26':'','9F36':'','9B':'','TID':'99962907'}";
        
        String cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertTrue(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
        
        cardType = "'Card type':'Swipe'";
        cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertFalse(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
        
        cardType = "'Card type':'FS'";
        cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertFalse(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
        
        cardType = "'Card type':''";
        cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertFalse(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
        
        cardType = "'Card type':";
        cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertFalse(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
        
        cardType = "";
        cardEaseDataJson = cardEaseDataJson1 + cardType + cardEaseDataJson2;
        assertFalse(CardProcessingUtil.hasFSwipeCardType(new MockMessageHelper(), cardEaseDataJson));
    }    
}
