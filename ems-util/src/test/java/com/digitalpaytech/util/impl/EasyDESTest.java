package com.digitalpaytech.util.impl;

import org.apache.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;


public class EasyDESTest {
    private static final String PLAIN_PASSWORD = "DptSignV2";
    private static Logger log = Logger.getLogger(EasyDESTest.class);
    
    @Test
    public void encrypt() {
        String encodedData = createEncodedData();
        assertNotNull(encodedData);
        log.info("Encoded data: " + encodedData);
    }

    private String createEncodedData() {
        EasyDES des = new EasyDES();
        des.ensureInit();
        String encodedData = des.encryptBase64(PLAIN_PASSWORD.getBytes());
        return encodedData;
    }
    
    @Test
    public void decrypt() {
        String encodedData = createEncodedData();
        log.info("1st, encrypt & encode: " + encodedData);
        
        EasyDES des = new EasyDES();
        des.ensureInit();
        String plain = des.decryptBase64(encodedData);
        log.info("2nd, plain: " + plain);
        assertNotNull(plain);
    }
    
    @Test
    public void ensureInit() {
        try {
            EasyDES des = new EasyDES();
            des.ensureInit();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
