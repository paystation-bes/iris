package com.digitalpaytech.util.impl;

import org.apache.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

public class EasyAES128Test {
    private static final String PLAIN_PASSWORD = "Test123";
    private static Logger log = Logger.getLogger(EasyDESTest.class);
    
    @Test
    public void encryptBase64() {
        EasyAES128 aes = new EasyAES128();
        String encrypted = aes.encryptBase64(PLAIN_PASSWORD);
        assertNotNull(encrypted);
        log.info(encrypted);
    }
    
    @Test
    public void decryptBase64() {
        EasyAES128 aes = new EasyAES128();
        String encrypted = aes.encryptBase64(PLAIN_PASSWORD);
        assertNotNull(encrypted);

        String decrypted = aes.decryptBase64(encrypted);
        assertNotNull(decrypted);
        log.info("before: " + encrypted + ", after: " + decrypted);
    }
}
