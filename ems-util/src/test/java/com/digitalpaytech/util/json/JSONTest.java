package com.digitalpaytech.util.json;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Map;
import org.apache.log4j.Logger;

import com.digitalpaytech.exception.JsonException;

public class JSONTest {
    private static final Logger LOG = Logger.getLogger(JSONTest.class);
    
    @Test
    public void convertValue() {
        final JSON json = new JSON();
        try {
            final Map<String, Object> map = json.convertValue(createCardEaseData());
            assertNotNull(map);
            assertTrue(map.size() > 0);
            
            LOG.info(map);
            
            
        } catch (JsonException je) {
            LOG.error(je);
            fail(je.getMessage());
        }
    }
    
    private CardEaseData createCardEaseData() {
        final CardEaseData data = new CardEaseData();
        data.setAcquirer("Test Acquirer");
        data.setAid("Test aid");
        data.setApl("Test apl");
        data.setApplication("test application");
        data.setAuthCode("test authCode");
        data.setCardHash("TEST cardHash");
        data.setCardReference("Test cardReference");
        data.setCvm("Test cvm");
        data.setDate("test date");
        data.setPan("test pan");
        data.setRef("TEST ref");
        data.setTid("TEST tid");
        return data;
    }
}
