package com.digitalpaytech.util.json;

import java.io.Serializable;

import com.digitalpaytech.util.StandardConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class CardEaseData implements Serializable {
    private static final long serialVersionUID = 5389886060720546067L;
    @JsonProperty("pan")
    private String pan;
    @JsonProperty("ref")
    private String ref;
    @JsonProperty("cardReference")
    private String cardReference;
    @JsonProperty("cardHash")
    private String cardHash;
    @JsonProperty("date")
    private String date;
    @JsonProperty("acquirer")
    private String acquirer;
    @JsonProperty("cvm")
    private String cvm;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("Auth Code")
    private String authCode;
    
    // Data in JSON is using key name "Application".
    @JsonProperty("application")
    private String application;
    // Data for CPS needs to be using key name "APL".
    @JsonProperty("apl")
    private String apl;
    
    @JsonProperty("aid")
    private String aid;
    
    public CardEaseData() {
        //Empty Constructor
    }
    
    public final String getPan() {
        return this.pan;
    }
    
    public final void setPan(final String pan) {
        this.pan = pan;
    }
    
    public final String getRef() {
        return this.ref;
    }
    
    public final void setRef(final String ref) {
        this.ref = ref;
    }
    
    public final String getAcquirer() {
        return this.acquirer;
    }
    
    public final void setAcquirer(final String acquirer) {
        this.acquirer = acquirer;
    }
    
    public final String getCardReference() {
        return this.cardReference;
    }
    
    public final void setCardReference(final String cardReference) {
        this.cardReference = cardReference;
    }
    
    public final String getCardHash() {
        return this.cardHash;
    }
    
    public final void setCardHash(final String cardHash) {
        this.cardHash = cardHash;
    }
    
    public final String getDate() {
        return this.date;
    }
    
    public final void setDate(final String date) {
        this.date = date;
    }
    
    public final String getTid() {
        return this.tid;
    }
    
    public final void setTid(final String tid) {
        this.tid = tid;
    }
    
    public final String getCvm() {
        return this.cvm;
    }
    
    public final void setCvm(final String cvm) {
        this.cvm = cvm;
    }
    
    public final String getApplication() {
        return this.application;
    }
    
    public final void setApplication(final String application) {
        this.application = application;
    }
    
    public final String getApl() {
        return this.apl;
    }
    
    public final void setApl(final String apl) {
        this.apl = apl;
    }
    
    public final String getAid() {
        return this.aid;
    }
    
    public final void setAid(final String aid) {
        this.aid = aid;
    }
    
    public final String getAuthCode() {
        return this.authCode;
    }
    
    public final void setAuthCode(final String authCode) {
        this.authCode = authCode;
    }
    
    public final void switchApplicationToApl() {
        if (this.apl == null) {
            this.apl = this.application;
        }
        this.application = null;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("pan: ").append(this.pan).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("ref: ").append(this.ref).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("cardReference: ").append(this.cardReference).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("cardHash: ").append(this.cardHash).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("date: ").append(this.date).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("acquirer: ").append(this.acquirer).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("cvm: ").append(this.cvm).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("tid: ").append(this.tid).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("apl (Application): ").append(this.application).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("aplForCPS: ").append(this.apl).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("aid: ").append(this.aid).append(StandardConstants.STRING_NEXT_LINE);
        bdr.append("authCode: ").append(this.authCode);
        return bdr.toString();
    }
}
