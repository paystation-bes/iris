package com.digitalpaytech.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.log4j.Logger;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.domain.Processor;


public class SystemAdminUtilTest {

	private static Logger log = Logger.getLogger(SystemAdminUtilTest.class);
	
	@Test
	public void populateProcessorNamesMap() {
		List<Processor> pros = createProcessors();
//		Map<String, Processor> map = new HashMap<String, Processor>(pros.size()); 
		// NOT valid anymore cannot be tested because of dependency changes
//		for (String name : WebCoreConstants.PROCESSORS) {
//			SystemAdminUtil.populateProcessorNamesMap(map, name, pros);	
//		}
//		assertEquals(4, map.size());
//		log.info(map);
//		
//		assertEquals("First Data Nashville", map.get(WebCoreConstants.FIRST_DATA_NASHVILLE).getName());
//		assertEquals("Alliance", map.get(WebCoreConstants.ALLIANCE).getName());
	}
	
	
	private List<Processor> createProcessors() {
		Processor p1 = new Processor();
		p1.setId(1);
		p1.setName("First Data Nashville");

		Processor p2 = new Processor();
		p2.setId(2);
		p2.setName("Concord");
		
		Processor p3 = new Processor();
		p3.setId(3);
		p3.setName("Heartland");
		
		Processor p4 = new Processor();
		p4.setId(4);
		p4.setName("Alliance");
		
		List<Processor> list = new ArrayList<Processor>();
		list.add(p1);
		list.add(p2);
		list.add(p3);
		list.add(p4);
		return list;
	}
}
