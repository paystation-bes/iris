package com.digitalpaytech.util.queue;

import javax.naming.OperationNotSupportedException;

public interface QueuePublisher<T> {
    void offer(T item);
    
    T poll() throws OperationNotSupportedException;
    
    T poll(long millisecsTimeout) throws OperationNotSupportedException;
}
