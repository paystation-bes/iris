package com.digitalpaytech.util;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.netflix.client.ClientException;
import com.netflix.hystrix.exception.HystrixRuntimeException;


import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import org.springframework.mock.web.MockHttpServletResponse;
import org.apache.log4j.Logger;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.exception.*;

public class WebCoreUtilTest {
	
	private static Logger log = Logger.getLogger(WebCoreUtilTest.class);
	private RandomKeyMapping mapping;
	
	@Before
	public void setUp() {
		mapping = (new KeyManager()).createKeyMapping();
	}
	
	
	@Test
	public void verifyRandomIdAndReturnActual() {
		HttpServletResponse res = new MockHttpServletResponse();
		Integer id = 111;	
		
		// Valid id
		String randomId = getRandomKeyMapping(id);
		log.debug("randomId: " + randomId);
		
		String[] errorMessages = new String[] { "test1", "test2" };
		Integer newId = WebCoreUtil.verifyRandomIdAndReturnActual(res, mapping, randomId, errorMessages);
		log.debug("newId: " + newId);
		
		assertEquals(id, newId);
		
		//------------------------------------------------------------------------------
		
		id = null;
		randomId = getRandomKeyMapping(id);
		newId = WebCoreUtil.verifyRandomIdAndReturnActual(res, mapping, randomId, errorMessages);
		assertNull(newId);
	}
	
	@Test
	public void convertBooleanToInteger() {
	    assertEquals(1, WebCoreUtil.convertBooleanToInteger(true).intValue());
	    assertEquals(0, WebCoreUtil.convertBooleanToInteger(false).intValue());
	}
	
	
	private String getRandomKeyMapping(Integer id) {
		
		Customer cust = new Customer();
		cust.setId(id);
		String key = mapping.getRandomString(cust, "id");
		return key;
	}
	
	
	@Test
	public void convertToBase100IntValue() {
	    assertEquals(639, WebCoreUtil.convertToBase100IntValue("6.39"));
	    assertEquals(250, WebCoreUtil.convertToBase100IntValue("2.5"));
	    assertEquals(100, WebCoreUtil.convertToBase100IntValue("1"));
	    assertEquals(25, WebCoreUtil.convertToBase100IntValue("0.25"));
	    assertEquals(1, WebCoreUtil.convertToBase100IntValue("0.01"));
	    assertEquals(0, WebCoreUtil.convertToBase100IntValue("0.00"));
	    assertEquals(0, WebCoreUtil.convertToBase100IntValue("0"));
	}
	
	@Test(expected = NumberFormatException.class)
	public void convertToBase100IntValueWithException() {
	    WebCoreUtil.convertToBase100IntValue("$6.39");
        WebCoreUtil.convertToBase100IntValue("");	    
	}

	
	@Test
	public void convertToBase100ShortValue() {
        assertEquals(250, WebCoreUtil.convertToBase100ShortValue("2.5"));
        assertEquals(100, WebCoreUtil.convertToBase100ShortValue("1"));
        assertEquals(12, WebCoreUtil.convertToBase100ShortValue("0.12"));
        assertEquals(1, WebCoreUtil.convertToBase100ShortValue("0.01"));
        assertEquals(0, WebCoreUtil.convertToBase100ShortValue("0.00"));
	}

    @Test(expected = NumberFormatException.class)
    public void convertToBase100ShortValueWithException() {
        WebCoreUtil.convertToBase100ShortValue("$6.39");
        WebCoreUtil.convertToBase100ShortValue("");       
    }
	
	
	@Test
	public void convertBase100ValueToFloatString() {
	    assertEquals("6.39", WebCoreUtil.convertBase100ValueToFloatString(639));
        assertEquals("2.50", WebCoreUtil.convertBase100ValueToFloatString(250));
        assertEquals("1.00", WebCoreUtil.convertBase100ValueToFloatString(100));
        assertEquals("0.25", WebCoreUtil.convertBase100ValueToFloatString(25));
        assertEquals("0.01", WebCoreUtil.convertBase100ValueToFloatString(1));
        assertEquals("0.00", WebCoreUtil.convertBase100ValueToFloatString(0));

	}
	
	@Test
	public void combineWordsAndChangeCase() {
	    assertEquals("testtest", WebCoreUtil.combineWordsAndChangeCase(" ", WebCoreUtil.LOWER_UPPER_CASES.LOWERCASE, "TesT TesT"));
	    assertEquals("MOMOMOMO", WebCoreUtil.combineWordsAndChangeCase(",", WebCoreUtil.LOWER_UPPER_CASES.UPPERCASE, "momo,    momo"));
	}
	
	@Test
	public void convertToBoolean() {
	    assertFalse(WebCoreUtil.convertToBoolean(null));
	    assertFalse(WebCoreUtil.convertToBoolean(""));
	    assertFalse(WebCoreUtil.convertToBoolean("TEST"));
	    assertFalse(WebCoreUtil.convertToBoolean("123"));
	    
	    assertFalse(WebCoreUtil.convertToBoolean("FALSE"));
	    assertFalse(WebCoreUtil.convertToBoolean("false"));
	    assertFalse(WebCoreUtil.convertToBoolean("FaLsE"));
	    assertFalse(WebCoreUtil.convertToBoolean("0"));
	    assertTrue(WebCoreUtil.convertToBoolean("true"));
	    assertTrue(WebCoreUtil.convertToBoolean("1"));
	}
	
	
	@Test
    public void convertToBooleanFromInt() {
	    assertFalse(WebCoreUtil.convertToBoolean(-1));
	    assertFalse(WebCoreUtil.convertToBoolean(0));
	    assertFalse(WebCoreUtil.convertToBoolean(2));
	    assertFalse(WebCoreUtil.convertToBoolean(366));
	    assertTrue(WebCoreUtil.convertToBoolean("1"));
    }
	
	@Test
	public void returnEmptyIfBlank() {
	    assertEquals("", WebCoreUtil.returnEmptyIfBlank(null));
	    assertEquals("", WebCoreUtil.returnEmptyIfBlank(""));
	    assertEquals("", WebCoreUtil.returnEmptyIfBlank("           "));
	    assertEquals("      DPT ", WebCoreUtil.returnEmptyIfBlank("      DPT "));
	}
	
	
	@Test
	public void escapeCharsAndCheckIfAllDigits() {
	    char[] chs = { '=' };
	    assertFalse(WebCoreUtil.escapeCharsAndCheckIfAllDigits(chs, "1122wRTK9amRvyq=yrZwbqNPEXpSCcOREr5rX"));
	    assertTrue(WebCoreUtil.escapeCharsAndCheckIfAllDigits(chs, "1234567889000000"));
	    assertTrue(WebCoreUtil.escapeCharsAndCheckIfAllDigits(chs, "1234567889000000=2322"));
	    assertTrue(WebCoreUtil.escapeCharsAndCheckIfAllDigits(chs, "123=456=789"));
	    
	    char[] chs2 = { '=', '-' };
	    assertTrue(WebCoreUtil.escapeCharsAndCheckIfAllDigits(chs2, "123=456-789"));
	}
	
	@Test
	public void hasNumericNotZeroValue() {
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("data"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue(""));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("1.1"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("1:1"));
	    assertTrue(WebCoreUtil.hasNumericNotZeroValue("1"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("0"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("00"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("000"));
	    assertFalse(WebCoreUtil.hasNumericNotZeroValue("0000"));
	}
	
	@Test
	public void prefixZeroes() {
	    assertEquals("0012", WebCoreUtil.prefixZeroes((short) 12, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
	    assertEquals("0006", WebCoreUtil.prefixZeroes((short) 6, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
	    assertEquals("0026", WebCoreUtil.prefixZeroes((short) 26, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
	    assertEquals("0149", WebCoreUtil.prefixZeroes((short) 149, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
	    assertEquals("5149", WebCoreUtil.prefixZeroes((short) 5149, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
	}
	
    @Test 
    public void prefixStrings() {
        assertEquals("XXXX1115", WebCoreUtil.prefixStrings("1115", StandardConstants.STRING_X, 8));
        assertEquals("XXXXXXXXXXXXXXX5", WebCoreUtil.prefixStrings("5", StandardConstants.STRING_X, 16));
        assertEquals("1234", WebCoreUtil.prefixStrings("1234", StandardConstants.STRING_X, 4));
    }
    
    @Test
    public void createWithCreditCardMask() {
        final String Xs = "XXXXXXXXXXXX";
        assertEquals(Xs + "0001", WebCoreUtil.createWithCreditCardMask((short) 1, StandardConstants.STRING_X));
        assertEquals(Xs + "0022", WebCoreUtil.createWithCreditCardMask((short) 22, StandardConstants.STRING_X));
        assertEquals(Xs + "0333", WebCoreUtil.createWithCreditCardMask((short) 333, StandardConstants.STRING_X));
        assertEquals(Xs + "4444", WebCoreUtil.createWithCreditCardMask((short) 4444, StandardConstants.STRING_X));
    }
	
	@Test
	public void isValidBooleanValue() {
	    assertTrue(WebCoreUtil.isValidBooleanValue(null));
	    assertTrue(WebCoreUtil.isValidBooleanValue(""));
	    assertTrue(WebCoreUtil.isValidBooleanValue("ON"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("off"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("True"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("falSE"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("yes"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("NO"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("N"));
	    assertTrue(WebCoreUtil.isValidBooleanValue("y"));
	    
	    assertFalse(WebCoreUtil.isValidBooleanValue("yesNO"));
	    assertFalse(WebCoreUtil.isValidBooleanValue("yn"));
	    assertFalse(WebCoreUtil.isValidBooleanValue("nope"));
	    assertFalse(WebCoreUtil.isValidBooleanValue("1"));
	}
	
    @Test
    public void isUnknowCardType() {
        int ccCardTypeId = 1;      // CC
        int scCardTypeId = 2;      // Smart Card
        int vcCardTypeId = 3;      // Passcard
        
        assertFalse(WebCoreUtil.isUnknowCardType(ccCardTypeId));
        assertFalse(WebCoreUtil.isUnknowCardType(scCardTypeId));
        assertFalse(WebCoreUtil.isUnknowCardType(vcCardTypeId));
        assertTrue(WebCoreUtil.isUnknowCardType(0));
        assertTrue(WebCoreUtil.isUnknowCardType(4));
    }
    
    @Test
    public void secureDelete() {
        String filename = "secureDeleteTest.test";
        try {
            WebCoreUtil.createFile("./target/test-classes/", filename, "This is a secureDelete test file.".getBytes());
        } catch (IOException ioe) {
            log.error("Cannot create the test file, ", ioe);
            return;
        }
        try {
            File f = new File("./target/test-classes/" + filename);
            log.info("test file location: " + f.getAbsolutePath() + ", size: " + f.length());
            
            boolean result = WebCoreUtil.secureDelete(f);
            log.info("result: " + result);
            
            if (!result) {
                fail();
            }
            if (f.exists()) {
                fail();
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }    
    
    @Test
    public void encodeBase64() {
        String encoded = WebCoreUtil.encodeBase64("Test123@");
        assertNotNull(encoded);
    }
    
    @Test
    public void decodeBase64() {
        String plain = "Test123@";
        String encoded = WebCoreUtil.encodeBase64(plain);
        String newPlain = WebCoreUtil.decodeBase64(encoded);
        assertEquals(plain, newPlain);
    }

    @Test
    public void isInArray() {
        int t = 3;
        int[] arr = { 1, 2, 3, 4, 5 };
        assertTrue(WebCoreUtil.isInArray(t, arr));
        
        t = 9;
        assertFalse(WebCoreUtil.isInArray(t, arr));
    }
    
    @Test
    public void divideBase100Value() {
        assertEquals(5, WebCoreUtil.divideBase100ValueBy100("500"));
        assertEquals(1, WebCoreUtil.divideBase100ValueBy100("100"));
        assertEquals(90, WebCoreUtil.divideBase100ValueBy100("9000"));
        assertEquals(45, WebCoreUtil.divideBase100ValueBy100("4500"));
        assertEquals(100, WebCoreUtil.divideBase100ValueBy100("10000"));
        assertEquals(3, WebCoreUtil.divideBase100ValueBy100("325"));
        assertEquals(0, WebCoreUtil.divideBase100ValueBy100("50"));
    }
    
    @Test
    public void concatErrors() {
        final String[] arr = new String[] { "aa", "bb", "cc", "dd", "ee", "ff", "gg" };
        final List<String> list = Arrays.asList(arr);
        final String concateStr = WebCoreUtil.concatErrors(list, 500);
        log.info("??? string: " + concateStr);
        assertNotNull(concateStr);
        assertEquals("aa;bb;cc;dd;ee;ff;gg;", concateStr);
        
        assertNull(WebCoreUtil.concatErrors(null, 600));
    }
    
    @Test
    public void returnNAIfBlank() {
        assertEquals(WebCoreConstants.N_A_STRING, WebCoreUtil.returnNAIfBlank(null));
        assertEquals(WebCoreConstants.N_A_STRING, WebCoreUtil.returnNAIfBlank(""));
        assertEquals(WebCoreConstants.N_A_STRING, WebCoreUtil.returnNAIfBlank("          "));
        assertEquals("1", WebCoreUtil.returnNAIfBlank("1"));
    }
    
    @Test
    public void isNetworkException() {
        final ConnectException ce = new ConnectException("Connection refused");
        final SystemException se = new SystemException(new InvalidDataException(ce));
        assertTrue(WebCoreUtil.isNetworkException(se));
        
        final UnknownHostException uhe = new UnknownHostException("TEST");
        final SystemException unknownSe = new SystemException(new InvalidDataException(uhe));
        assertTrue(WebCoreUtil.isNetworkException(unknownSe));
        
        final SystemException invalidSe = new SystemException(new InvalidDataException("TEST"));
        assertFalse(WebCoreUtil.isNetworkException(invalidSe));
        
        final InvalidDataException ide = new InvalidDataException("TEST");
        assertFalse(WebCoreUtil.isNetworkException(ide));
    }
    
    @Test
    public void causeByClientException() {
        final ClientException ce = new ClientException(WebCoreConstants.CLIENT_EXCEPTION_MESSAGE_NO_AVAILABLE_SERVER + ": getAccess", new IOException("TEST"));
        final HystrixRuntimeException hre = new HystrixRuntimeException(null, null, "EXP", ce, ce);
        assertTrue(WebCoreUtil.causeByClientException(hre));

        final ClientException ceExceeded = new ClientException(WebCoreConstants.CLIENT_EXCEPTION_MESSAGE_NUM_RETRIES_EXCEEDED_MAX, new IOException("TEST"));
        final HystrixRuntimeException hreExceeded = new HystrixRuntimeException(null, null, "EXP", ceExceeded, ceExceeded);
        assertTrue(WebCoreUtil.causeByClientException(hreExceeded));
        
        final IOException ioe = new IOException("TEST");
        final HystrixRuntimeException ioHre = new HystrixRuntimeException(null, null, "EXP", ioe, ioe);
        assertFalse(WebCoreUtil.causeByClientException(ioHre));
    }

    @Test
    public void isNetworkCommunicationException() {
        final UnknownHostException uhe = new UnknownHostException("TEST");
        HystrixRuntimeException hre = new HystrixRuntimeException(null, null, "EXP", uhe, uhe);
        assertTrue(WebCoreUtil.isNetworkCommunicationException(hre));
        
        final UnknownServiceException use = new UnknownServiceException("TEST");
        hre = new HystrixRuntimeException(null, null, "EXP", use, use);
        assertTrue(WebCoreUtil.isNetworkCommunicationException(hre));
        
        final MalformedURLException mue = new MalformedURLException("TEST");
        hre = new HystrixRuntimeException(null, null, "EXP", mue, mue);
        assertTrue(WebCoreUtil.isNetworkCommunicationException(hre));
        
        final SocketException se = new SocketException("TEST");
        hre = new HystrixRuntimeException(null, null, "EXP", se, se);
        assertTrue(WebCoreUtil.isNetworkCommunicationException(hre));
        
        final IOException ioe = new IOException("TEST");
        hre = new HystrixRuntimeException(null, null, "EXP", ioe, ioe);
        assertFalse(WebCoreUtil.isNetworkCommunicationException(hre));
    }
    
    @Test
    public void getProcessorNameInMerchantService() {
        final Map<String, String> processorNamesMap = new HashMap<>();
        processorNamesMap.put("processor.concord.link", "processor.concord"); 
        processorNamesMap.put("processor.firstDataNashville.link", "processor.firstDataNashville"); 
        processorNamesMap.put("processor.heartland.link", "processor.heartland"); 
        processorNamesMap.put("processor.link2gov.link", "processor.link2gov"); 
        processorNamesMap.put("processor.parcxmart.link", "processor.parcxmart"); 
        processorNamesMap.put("processor.alliance.link", "processor.alliance"); 
        processorNamesMap.put("processor.paymentech.link", "processor.paymentech"); 
        processorNamesMap.put("processor.paradata.link", "processor.paradata"); 
        processorNamesMap.put("processor.moneris.link", "processor.moneris"); 
        processorNamesMap.put("processor.firstHorizon.link", "processor.firstHorizon"); 
        processorNamesMap.put("processor.authorize.net.link", "processor.authorize.net"); 
        processorNamesMap.put("processor.datawire.link", "processor.datawire"); 
        processorNamesMap.put("processor.blackboard.link", "processor.blackboard"); 
        processorNamesMap.put("processor.totalcard.link", "processor.totalcard"); 
        processorNamesMap.put("processor.nuvision.link", "processor.nuvision"); 
        processorNamesMap.put("processor.elavon.link", "processor.elavon"); 
        processorNamesMap.put("processor.cbord.csgold.link", "processor.cbord.csgold"); 
        processorNamesMap.put("processor.cbord.odyssey.link", "processor.cbord.odyssey"); 
        processorNamesMap.put("processor.elavon.viaconex.link", "processor.elavon.viaconex"); 
        processorNamesMap.put("processor.creditcall.link", "processor.creditcall"); 
        processorNamesMap.put("processor.tdmerchant.link", "processor.tdmerchant");          
        processorNamesMap.put("processor.heartland.spplus.link", "processor.heartland.spplus");
        
        processorNamesMap.keySet().forEach(k -> {
            String actual = WebCoreUtil.getProcessorNameInMerchantService(".link", k);
            String expect = processorNamesMap.get(k);
            log.info("expect: " + expect + ", actual: " + actual);
            assertEquals(expect, actual);
        });
        
        final Map<String, String> processorNamesMapNotLink = new HashMap<>();
        processorNamesMapNotLink.put("processor.concord", "processor.concord"); 
        processorNamesMapNotLink.put("processor.firstDataNashville", "processor.firstDataNashville"); 
        processorNamesMapNotLink.put("processor.heartland", "processor.heartland"); 
        processorNamesMapNotLink.put("processor.link2gov", "processor.link2gov"); 
        processorNamesMapNotLink.put("processor.parcxmart", "processor.parcxmart"); 
        processorNamesMapNotLink.put("processor.alliance", "processor.alliance"); 
        processorNamesMapNotLink.put("processor.paymentech", "processor.paymentech"); 
        processorNamesMapNotLink.put("processor.paradata", "processor.paradata"); 
        processorNamesMapNotLink.put("processor.moneris", "processor.moneris"); 
        processorNamesMapNotLink.put("processor.firstHorizon", "processor.firstHorizon"); 
        processorNamesMapNotLink.put("processor.authorize.net", "processor.authorize.net"); 
        processorNamesMapNotLink.put("processor.datawire", "processor.datawire"); 
        processorNamesMapNotLink.put("processor.blackboard", "processor.blackboard"); 
        processorNamesMapNotLink.put("processor.totalcard", "processor.totalcard"); 
        processorNamesMapNotLink.put("processor.nuvision", "processor.nuvision"); 
        processorNamesMapNotLink.put("processor.elavon", "processor.elavon"); 
        processorNamesMapNotLink.put("processor.cbord.csgold", "processor.cbord.csgold"); 
        processorNamesMapNotLink.put("processor.cbord.odyssey", "processor.cbord.odyssey"); 
        processorNamesMapNotLink.put("processor.elavon.viaconex", "processor.elavon.viaconex"); 
        processorNamesMapNotLink.put("processor.creditcall", "processor.creditcall"); 
        processorNamesMapNotLink.put("processor.tdmerchant", "processor.tdmerchant");          
        processorNamesMapNotLink.put("processor.heartland.spplus", "processor.heartland.spplus");
        
        processorNamesMapNotLink.keySet().forEach(k -> {
            String actual = WebCoreUtil.getProcessorNameInMerchantService(".link", k);
            String expect = processorNamesMapNotLink.get(k);
            assertEquals(expect, actual);
        });        
    }
    
    @Test
    public void isUUID() {
        final UUID uuid1 = UUID.randomUUID();
        assertTrue(WebCoreUtil.isUUID(uuid1.toString()));
        
        assertFalse(WebCoreUtil.isUUID(null));
        assertFalse(WebCoreUtil.isUUID(""));
        assertFalse(WebCoreUtil.isUUID("abc"));
    }
}
