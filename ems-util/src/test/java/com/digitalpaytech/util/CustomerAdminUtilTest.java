package com.digitalpaytech.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.util.support.SubscriptionEntry;

public class CustomerAdminUtilTest {

	@Test
	public void findCustomerPropertyType() {
		
		CustomerAdminUtil.setCustomerPropertyTypes(getCustomerPropertyTypes());
		
		assertEquals(3, CustomerAdminUtil.findCustomerPropertyType(3).getId());
		assertEquals(2, CustomerAdminUtil.findCustomerPropertyType(2).getId());
		assertEquals(1, CustomerAdminUtil.findCustomerPropertyType(1).getId());
		assertEquals("Timezone", CustomerAdminUtil.findCustomerPropertyType(1).getName());
		assertNull(CustomerAdminUtil.findCustomerPropertyType(6));
	}
	

	@Test
	public void findCustomerProperty() {
		CustomerProperty cp = CustomerAdminUtil.findCustomerProperty(2, getCustomerPropertyIter());
		assertEquals("1", cp.getPropertyValue());

		cp = CustomerAdminUtil.findCustomerProperty(1, getCustomerPropertyIter());
		assertEquals("US/Pacific", cp.getPropertyValue());
		
		cp = CustomerAdminUtil.findCustomerProperty(3, getCustomerPropertyIter());
		assertEquals("99", cp.getPropertyValue());
	}
	

	@Test
	public void createStatusTypes() {
		Customer cust = new Customer();
		CustomerStatusType customerStatusType = new CustomerStatusType();
		UserStatusType userStatusType = new UserStatusType();
		
		CustomerAdminUtil.createStatusTypes(cust, customerStatusType, userStatusType, WebCoreConstants.CUSTOMER_STATUS_TYPE_TRIAL_LABEL, "02/20/2012", "PST");
		assertEquals(WebCoreConstants.CUSTOMER_STATUS_TYPE_TRIAL, customerStatusType.getId());
		assertEquals(WebCoreConstants.USER_STATUS_TYPE_ENABLED, userStatusType.getId());
		assertNotNull(cust.getTrialExpiryGmt());
		
		//----------------------------------------------------------------------------
		
		CustomerAdminUtil.createStatusTypes(cust, customerStatusType, userStatusType, WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED_LABEL, "02/20/2012", "PST");
		assertEquals(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED, customerStatusType.getId());
		assertEquals(WebCoreConstants.USER_STATUS_TYPE_ENABLED, userStatusType.getId());
		assertNull(cust.getTrialExpiryGmt());

		//----------------------------------------------------------------------------
		
		CustomerAdminUtil.createStatusTypes(cust, customerStatusType, userStatusType, WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED_LABEL, "02/20/2012", "PST");
		assertEquals(WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED, customerStatusType.getId());
		assertEquals(WebCoreConstants.USER_STATUS_TYPE_DISABLED, userStatusType.getId());
		assertNull(cust.getTrialExpiryGmt());
		
	}
	
	@Test
	public void createSubscriptionTypeMap() {
	    Map<Integer, SubscriptionEntry> map = CustomerAdminUtil.createSubscriptionTypeMap(getSubscriptionTypes());
		assertNotNull(map);
		assertEquals(4, map.size());
		assertEquals("Real-Time Credit Card Procesing", map.get(new Integer("300")).getName());
		
		assertEquals(new Integer("400"), map.get(new Integer("400")).getSubscriptionTypeId());
		assertEquals("Standard Reports", map.get(new Integer("100")).getName());
		assertEquals(new Integer("200"), map.get(new Integer("200")).getSubscriptionTypeId());
	}

	@Test
	public void getUnifiId() {
	    final Customer customer = new Customer();
	    customer.setUnifiId(null);
	    Optional<Integer> optCust = CustomerAdminUtil.getUnifiId(customer);
	    assertFalse(optCust.isPresent());
	    
        customer.setUnifiId(1);
        optCust = CustomerAdminUtil.getUnifiId(customer);
        assertTrue(optCust.isPresent());
	}
	
	private List<SubscriptionType> getSubscriptionTypes() {
		SubscriptionType reportSub = new SubscriptionType();
		reportSub.setId(100);
		reportSub.setName("Standard Reports");
		reportSub.setRandomId("randomId100");
		
		SubscriptionType alertstSub = new SubscriptionType();
		alertstSub.setId(200);
		alertstSub.setName("Alerts");
		alertstSub.setRandomId("randomId200");
		
		SubscriptionType ccSub = new SubscriptionType();
		ccSub.setId(300);
		ccSub.setName("Real-Time Credit Card Procesing");
		ccSub.setRandomId("randomId300");
		
		SubscriptionType batchCcSub = new SubscriptionType();
		batchCcSub.setId(400);
		batchCcSub.setName("Batch Credit Card Processing");
		batchCcSub.setRandomId("randomId400");
		
		List<SubscriptionType> list = new ArrayList<SubscriptionType>();
		list.add(reportSub);
		list.add(alertstSub);
		list.add(ccSub);
		list.add(batchCcSub);
		return list;
	}
	
	private Iterator<CustomerProperty> getCustomerPropertyIter() {
		List<CustomerProperty> list = new ArrayList<CustomerProperty>();
		Iterator<CustomerPropertyType> iter = getCustomerPropertyTypes().iterator();
		while (iter.hasNext()) {
			CustomerPropertyType type = iter.next();
			CustomerProperty cp = new CustomerProperty();
			cp.setCustomerPropertyType(type);
			if (type.getId() == 1) {
				cp.setId(1);
				cp.setPropertyValue("US/Pacific");
			} else if (type.getId() == 2) {
				cp.setId(2);
				cp.setPropertyValue("1");
			} else if (type.getId() == 3) {
				cp.setId(3);
				cp.setPropertyValue("99");
			}
			list.add(cp);
		}
		return list.iterator();
	}
	
	private List<CustomerPropertyType> getCustomerPropertyTypes() {
		CustomerPropertyType type1 = new CustomerPropertyType();
		type1.setId(1);
		type1.setName("Timezone");

		CustomerPropertyType type2 = new CustomerPropertyType();
		type2.setId(2);
		type2.setName("Query Spaces By");

		CustomerPropertyType type3 = new CustomerPropertyType();
		type3.setId(3);
		type3.setName("Max User Accounts");
		
		List<CustomerPropertyType> list = new ArrayList<CustomerPropertyType>();
		list.add(type2);
		list.add(type1);
		list.add(type3);
		return list;
	}
}
