package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.util.WebSecurityConstants;

import static org.junit.Assert.*;

public class DashboardUtilTest {

	@Test
	public void resetTemporaryId() {
		// TempT11
		DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX);
		// TempT12
		DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX);
		DashboardUtil.resetTemporaryId(WebSecurityConstants.COMPLEX_PASSWORD_EXP);
		assertEquals("TempW11", DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_WIDGET_ID_PREFIX));

		// Password is incorrect, id will NOT be reset
		DashboardUtil.resetTemporaryId("test");
		assertEquals("TempW12", DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_WIDGET_ID_PREFIX));
	}
	
	@Test
	public void getTemporaryId() {
		DashboardUtil.resetTemporaryId(WebSecurityConstants.COMPLEX_PASSWORD_EXP);
		assertEquals("TempT11", DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX));
		assertEquals("TempS12", DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_SECTION_ID_PREFIX));
		assertEquals("TempW13", DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_WIDGET_ID_PREFIX));
	}
	
	@Test
	public void test_validateRandomId() {
		
		boolean result = DashboardUtil.validateRandomId(null);
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDB123=");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDB123*&");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDB123ak&l");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ABD@gGYUUJHH^");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBHGJ%4YRRFSE2%");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBHGJ 4YRRFSE2$");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBhGJK4YRRFSE2#");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBHGJK4YRRFSE2123@");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBHGJK4YRRFSE!");
		assertEquals(result, false);
		
		result = DashboardUtil.validateRandomId("ASDBHGJK4YRRFSE2");
		assertEquals(result, true);
	}
	
	@Test
	public void isInteger() {
		assertTrue(DashboardUtil.isInteger("7"));
		assertTrue(DashboardUtil.isInteger("0"));
		assertTrue(DashboardUtil.isInteger("-5"));
		assertFalse(DashboardUtil.isInteger("test"));
		assertFalse(DashboardUtil.isInteger("7.0"));
		assertFalse(DashboardUtil.isInteger(""));
		
	}
	
	@Test
	public void isPositiveInteger() {
		assertFalse(DashboardUtil.isPositiveInteger("-5"));
		assertTrue(DashboardUtil.isPositiveInteger("7"));
		assertTrue(DashboardUtil.isPositiveInteger("0"));
	}

	
	@Test
	public void setRandomIds() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession ses = new MockHttpSession();
		request.setSession(ses);

		Tab tab = DashboardUtil.setRandomIds(request, createTabSectionsWidgets());
		assertNotNull(tab.getRandomId());
		if (tab.getRandomId().trim().length() == 0) {
			fail("tab has no randomId");
		}
		Iterator<Section> secIter = tab.getSections().iterator();
		while (secIter.hasNext()) {
			Section sec = secIter.next();
			assertNotNull(sec.getRandomId());
			if (sec.getRandomId().trim().length() == 0) {
				fail("Section has no randomId");
			}
			
			Iterator<Widget> widIter = sec.getWidgets().iterator();
			while (widIter.hasNext()) {
				Widget wid = widIter.next();
				assertNotNull(wid.getRandomId());
				if (wid.getRandomId().trim().length() == 0) {
					fail("Widget has no randomId");
				}				
			}
		}
	}
	
	
	@Test
	public void setTabRandomId() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession ses = new MockHttpSession();
		request.setSession(ses);
		
		Tab tab = DashboardUtil.setTabRandomId(request, createTabSectionsWidgets());
		assertNotNull(tab.getRandomId());
		if (tab.getRandomId().trim().length() == 0) {
			fail("tab has no randomId");
		}
	}
	
	private Tab createTabSectionsWidgets() {
		Widget w1 = new Widget();
		Widget w2 = new Widget();
		w1.setId(1);
		w2.setId(2);
		ArrayList<Widget> ws = new ArrayList<Widget>();
		ws.add(w1);
		ws.add(w2);
		
		Section se = new Section();
		se.setId(3);
		se.setWidgets(ws);
		ArrayList<Section> ss = new ArrayList<Section>();
		ss.add(se);
		
		Tab tab = new Tab();
		tab.setId(4);
		tab.setSections(ss);
		return tab;
	}
	
	@Test
	public void isSameRandomId() {
		String s1 = "";
		String s2 = null;
		assertFalse(DashboardUtil.isSameRandomId(s1, s2));
		
		s2 = "";
		assertFalse(DashboardUtil.isSameRandomId(s1, s2));
		
		s1 = "ABC123";
		s2 = "abc123";
		assertFalse(DashboardUtil.isSameRandomId(s1, s2));
		
		s2 = "ABC123";
		assertTrue(DashboardUtil.isSameRandomId(s1, s2));
	}
}
