package com.digitalpaytech.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;

import com.digitalpaytech.exception.InvalidDataException;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Date;
import java.time.ZonedDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * DateUtil.java is way too heavy and often it's confusing to select the correct methods.
 */
@Deprecated
public class DateUtilTest {
    private static final Logger LOG = Logger.getLogger(DateUtilTest.class);
    
    @Test
    public final void convertFromColonDelimitedDateStringToCalendar() {
        final String dateStr = "2012:12:03:16:57:54:GMT";
        Calendar cal = null;
        try {
            cal = DateUtil.convertFromColonDelimitedDateStringToCalendar(dateStr);
        } catch (InvalidDataException e) {
            fail(e.getMessage());
        }
        
        LOG.debug(cal.getTime().toString());
        
        final int year2012 = 2012;
        assertEquals(year2012, cal.get(Calendar.YEAR));
        final int april = 3;
        assertEquals(april, cal.get(Calendar.DATE));
        final int minutes57 = 57;
        assertEquals(minutes57, cal.get(Calendar.MINUTE));
    }
    
    @Test
    public final void isInBetween() {
        final int year2014 = 2014;
        final int date22 = 22;
        final int hr8 = 8;
        final int hr6 = 6;
        final int hr10 = 10;
        final int hr18 = 18;
        final int min30 = 30;
        final int min31 = 31;
        
        Date startDate = new GregorianCalendar(year2014, 1, date22, 0, 0, 0).getTime();
        Date endDate = new GregorianCalendar(year2014, 1, date22, hr8, min30, 0).getTime();
        Date time = new GregorianCalendar(year2014, 1, date22, hr6, min30, 0).getTime();
        assertTrue(DateUtil.isInBetween(time, startDate, endDate));
        
        startDate = new GregorianCalendar(year2014, 1, date22, hr10, 0, 0).getTime();
        endDate = new GregorianCalendar(year2014, 1, date22, hr18, min30, 0).getTime();
        time = new GregorianCalendar(year2014, 1, date22, hr6, min30, 0).getTime();
        assertFalse(DateUtil.isInBetween(time, startDate, endDate));
        
        time = new GregorianCalendar(year2014, 1, date22, hr10, 0, 0).getTime();
        assertTrue(DateUtil.isInBetween(time, startDate, endDate));
        
        time = new GregorianCalendar(year2014, 1, date22, hr18, min30, 0).getTime();
        assertTrue(DateUtil.isInBetween(time, startDate, endDate));
        
        time = new GregorianCalendar(year2014, 1, date22, hr18, min31, 0).getTime();
        assertFalse(DateUtil.isInBetween(time, startDate, endDate));
    }
    
    @Test
    public final void isSameHourMinute() {
        final Calendar cal1 = new GregorianCalendar(2014, 1, 22, 5, 0, 0);
        final Calendar cal2 = new GregorianCalendar(2014, 1, 22, 5, 0, 30);
        assertTrue(DateUtil.isSameHourMinute(cal1.getTime(), cal2.getTime()));
        
        final Calendar cal3 = new GregorianCalendar(2014, 1, 22, 15, 0, 1);
        final Calendar cal4 = new GregorianCalendar(2014, 1, 22, 15, 0, 59);
        assertTrue(DateUtil.isSameHourMinute(cal3.getTime(), cal4.getTime()));
        
        final Calendar cal5 = new GregorianCalendar(2014, 1, 22, 15, 0, 0);
        final Calendar cal6 = new GregorianCalendar(2014, 1, 22, 15, 1, 0);
        assertFalse(DateUtil.isSameHourMinute(cal5.getTime(), cal6.getTime()));
    }
    
    @Test
    public final void convertTimeZone() {
        final String gmt = "GMT";
        final String usPaci = "US/Pacific";
        final Calendar srcCal = new GregorianCalendar(2015, 0, 1, 23, 59, 59);
        final Date gmtDate = DateUtil.convertTimeZone(gmt, usPaci, srcCal.getTime());
        LOG.debug("-------------- gmt " + gmtDate);
        assertNotNull(gmtDate);
        
        final Date pstDate = DateUtil.convertTimeZone(usPaci, gmt, gmtDate);
        LOG.debug("-------------- local (pst) " + pstDate);
        assertNotNull(pstDate);
        
        final Calendar pstCal = new GregorianCalendar();
        pstCal.setTime(pstDate);
        assertEquals(srcCal.getTimeInMillis(), pstCal.getTimeInMillis());
    }
    
    @Test
    public void convertUTCToGMT() {
        final SimpleDateFormat utcFormat = new SimpleDateFormat(DateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT);
        utcFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
        final String utcDate = utcFormat.format(new Date());
        
        final Date gmtDate = DateUtil.convertUTCToGMT(utcDate);
        LOG.debug("UTC Date: " + utcDate);
        LOG.debug("GMT Date: " + gmtDate.toString());
        assertNotNull(gmtDate);
        
        try {
            final GregorianCalendar utcCal = new GregorianCalendar();
            utcCal.setTime(utcFormat.parse(utcDate));
            
            final GregorianCalendar gmtCal = new GregorianCalendar();
            gmtCal.setTime(gmtDate);
            
            assertEquals(utcCal.get(Calendar.YEAR), gmtCal.get(Calendar.YEAR));
            assertEquals(utcCal.get(Calendar.MONTH), gmtCal.get(Calendar.MONTH));
            assertEquals(utcCal.get(Calendar.DATE), gmtCal.get(Calendar.DATE));
            assertEquals(utcCal.get(Calendar.HOUR_OF_DAY), gmtCal.get(Calendar.HOUR_OF_DAY));
            assertEquals(utcCal.get(Calendar.MINUTE), gmtCal.get(Calendar.MINUTE));
            assertEquals(utcCal.get(Calendar.SECOND), gmtCal.get(Calendar.SECOND));
            assertEquals(utcCal.get(Calendar.MILLISECOND), gmtCal.get(Calendar.MILLISECOND));
            
        } catch (ParseException pe) {
            LOG.error(pe.getMessage());
            fail();
        }
    }
    
    @Test
    public final void getDateFormattedWithChronoUnitDays() {
        final String fmt = DateUtil.DATE_ONLY_FORMAT;
        final String tz = WebCoreConstants.GMT;
        final Date purDate = new Date();
        final String dateOnly = DateUtil.getDateFormattedWithChronoUnitDays(fmt, tz, purDate);
        LOG.debug(dateOnly);
        assertNotNull(dateOnly);
    }
    
    public void convertGMTToUTC() {
        final Date utc = DateUtil.convertGMTToUTC(DateUtil.getCurrentGmtDate());
        LOG.debug("utc: " + utc);
        assertNotNull(utc);
    }
    
    @Test
    public void convertGMTToUTCStringFormat() {
        final String utcStr = DateUtil.convertGMTToUTCStringFormat(DateUtil.getCurrentGmtDate(), DateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT);
        LOG.debug("utc string: " + utcStr);
        assertNotNull(utcStr);
    }
}
