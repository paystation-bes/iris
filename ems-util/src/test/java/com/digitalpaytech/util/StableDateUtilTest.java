package com.digitalpaytech.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Test;

public class StableDateUtilTest {
    private static final Logger LOG = Logger.getLogger(StableDateUtilTest.class);
    
    @Test
    public void convertFromGmtDate() {
        final Date nowGmt = DateUtil.getCurrentGmtDate();
        LOG.debug("-- date: " + nowGmt + ", convert to Instant now...");
        final Instant ins = StableDateUtil.convertFromGMTDate(nowGmt);
        LOG.debug(ins);
        assertNotNull(ins);
    }
    
    @Test
    public void convertFromGMTDateToUTC() {
        final Date nowGmt = DateUtil.getCurrentGmtDate();
        final String s = StableDateUtil.convertFromGMTDateToUTC(nowGmt);
        LOG.debug("-------> " + s);
        assertTrue(s.indexOf("T") != -1);
        assertTrue(s.indexOf(".") != -1);
        assertTrue(s.endsWith("Z"));
    }
    
    @Test
    public void isDateNDaysInTheFuture() {
        final ZoneId gmtZone = ZoneId.of(StableDateUtil.GMT);
        final LocalDate ldPlus2Days = LocalDate.now(gmtZone).plusDays(2);
        final Date date = Date.from(ldPlus2Days.atStartOfDay(ZoneId.systemDefault()).toInstant());
        
        final boolean result = StableDateUtil.isDateNDaysInTheFuture(2, date, gmtZone);
        assertTrue(result);
    }
}
