package com.digitalpaytech.util;

import org.junit.Test;

import static org.junit.Assert.*;
import org.apache.log4j.Logger;
import java.util.Map;

public class CurrencyUtilTest {
    private static final Logger LOG = Logger.getLogger(CurrencyUtilTest.class);
    
    @Test
    public void getAllCurrencies() {
        final Map<String, Integer> map = CurrencyUtil.getAllCurrencies();
        LOG.info(map);
        if (map == null || map.isEmpty()) {
            fail();
        }
    }
    
    @Test
    public void getCurrencyNumber() {
        final int usdNum = CurrencyUtil.getCurrencyNumber("USD");
        assertEquals(840, usdNum);
        
        final int cadNum = CurrencyUtil.getCurrencyNumber("CAD");
        assertEquals(124, cadNum);
        
        final int unknown = CurrencyUtil.getCurrencyNumber("UKD");
        assertEquals(-1, unknown);
    }
}
