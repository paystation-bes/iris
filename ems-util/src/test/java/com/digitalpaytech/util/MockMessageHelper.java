package com.digitalpaytech.util;

import com.digitalpaytech.util.MessageHelper;

public class MockMessageHelper extends MessageHelper {

    @Override
    public String getMessage(final String messageKey) {
        if (messageKey.equals("label.cardeasedata.cardtype")) {
            return "card type";
        } else if (messageKey.equals("label.cardeasedata.fswipe")) {
            return "fswipe";
        } else {
            return null;
        }
    }

}
