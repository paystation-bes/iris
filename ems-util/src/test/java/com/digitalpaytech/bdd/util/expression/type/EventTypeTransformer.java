package com.digitalpaytech.bdd.util.expression.type;

import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.InvalidStoryObjectException;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.bdd.util.expression.TransformationException;

public final class EventTypeTransformer implements StoryValueTransformer {
    
    private static final long serialVersionUID = 8382176809060986698L;
    
    @Override
    public void init(final String[] params) throws InvalidStoryObjectException {
        // DO NOTHING
    }
    
    @Override
    public String transform(final String value, final StoryObjectBuilder builder) throws TransformationException {
        final String eventTypeLabel = eventTypeLabel(value);
        if (eventTypeLabel == null) {
            throw new TransformationException("Unknown Event Type: " + value);
        }
        return eventTypeLabel;
    }
    
    public static String eventTypeLabel(final String eventTypeStr) {
        String result = null;
        if (eventTypeStr != null) {
            final String targetEventType = eventTypeStr.trim();
            result = TestLookupTables.getEventTypeMapping().get(targetEventType);
        }
        return result;
    }
}
