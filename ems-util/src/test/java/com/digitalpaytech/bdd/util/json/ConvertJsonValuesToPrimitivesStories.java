package com.digitalpaytech.bdd.util.json;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.jsonprimitiveconverter.TestConvertToJSONPrimitive;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class ConvertJsonValuesToPrimitivesStories extends AbstractStories {
    
    public ConvertJsonValuesToPrimitivesStories() {
        super();
        testHandlers.registerTestHandler("valuesconvert", TestConvertToJSONPrimitive.class);
        testHandlers.registerTestHandler("valuesconvertedcorrectly", TestConvertToJSONPrimitive.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new AbstractSteps(this.testHandlers), new BaseSteps(this.testHandlers),
                new JSONConverterSteps(this.testHandlers));
    }
    
}
