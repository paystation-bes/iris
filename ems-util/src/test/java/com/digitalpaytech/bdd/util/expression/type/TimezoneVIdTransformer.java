package com.digitalpaytech.bdd.util.expression.type;

import com.digitalpaytech.bdd.util.expression.InvalidStoryObjectException;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.bdd.util.expression.TransformationException;
import com.digitalpaytech.domain.TimezoneVId;

public final class TimezoneVIdTransformer implements AttributeValueTransformer {
    private static final long serialVersionUID = -5589873017227060039L;

    @Override
    public void init(final String[] params) throws InvalidStoryObjectException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Object transform(final Object value, final StoryObjectBuilder builder) throws TransformationException {
        if (!(value instanceof TimezoneVId)) {
            throw new InvalidStoryObjectException("Expected the type of the object to be TimezoneVId");
        }
        
        return ((TimezoneVId) value).getId();
    }
    
}
