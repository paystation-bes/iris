package com.digitalpaytech.bdd.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.bdd.util.expression.StoryObjectParser;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventException;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventStatusType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.TimezoneV;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "checkstyle:multiplestringliterals", "PMD.UseConcurrentHashMap", "checkstyle:magicnumber" })
public final class TestLookupTables {
    private static final String BDD_MVC_PACKAGE = "com.digitalpaytech.bdd.mvc";
    private static final String BDD_SCHEDULING_PACKAGE = "com.digitalpaytech.bdd.scheduling";
    private static final String BDD_BGPROC_ALERT_SERVICES_PACKAGE = "com.digitalpaytech.bdd.scheduling.alert";
    
    private static final Map<String, String> EVENT_TYPE_MAPPING = new HashMap<String, String>();
    
    private static final Map<String, Byte> QUEUE_TYPE_MAP = new HashMap<String, Byte>();
    
    static {
        QUEUE_TYPE_MAP.put("recent event", WebCoreConstants.QUEUE_TYPE_RECENT_EVENT);
        QUEUE_TYPE_MAP.put("current status", WebCoreConstants.QUEUE_TYPE_RECENT_EVENT);
        QUEUE_TYPE_MAP.put("historical event", WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT);
        QUEUE_TYPE_MAP.put("alert processing", WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
        QUEUE_TYPE_MAP.put("notification", WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL);
        QUEUE_TYPE_MAP.put("customer alert type", WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE);
        
        QUEUE_TYPE_MAP.put("recent event", WebCoreConstants.QUEUE_TYPE_RECENT_EVENT);
        QUEUE_TYPE_MAP.put("historical event", WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT);
        QUEUE_TYPE_MAP.put("alert processing", WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
        QUEUE_TYPE_MAP.put("notification", WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL);
        QUEUE_TYPE_MAP.put("customer alert type", WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE);
        
        EVENT_TYPE_MAPPING.put("Battery Level", "eventType.battery.level");
        EVENT_TYPE_MAPPING.put("Bill Acceptor Present", "eventType.billacceptor.present");
        EVENT_TYPE_MAPPING.put("Bill Acceptor Jam", "eventType.billacceptor.jam");
        EVENT_TYPE_MAPPING.put("Bill Stacker Unable To Stack", "eventType.billacceptor.unableToStack");
        EVENT_TYPE_MAPPING.put("Bill Stacker Present", "eventType.billstacker.present");
        EVENT_TYPE_MAPPING.put("Bill Stacker Level", "eventType.billstacker.level");
        EVENT_TYPE_MAPPING.put("Bill Canister Removed", "eventType.billcanister.remove");
        EVENT_TYPE_MAPPING.put("Card Reader Present", "eventType.cardreader.present");
        EVENT_TYPE_MAPPING.put("Coin Acceptor Present", "eventType.coinacceptor.present");
        EVENT_TYPE_MAPPING.put("Coin Acceptor Jam", "eventType.coinacceptor.jam");
        EVENT_TYPE_MAPPING.put("Coin Changer Present", "eventType.coinchanger.present");
        EVENT_TYPE_MAPPING.put("Coin Changer Jam", "eventType.coinchanger.jam");
        EVENT_TYPE_MAPPING.put("Coin Changer Level", "eventType.coinchanger.level");
        EVENT_TYPE_MAPPING.put("Coin Hopper 1 Present", "eventType.coinhopper1.present");
        EVENT_TYPE_MAPPING.put("Coin Hopper 1 Level", "eventType.coinhopper1.level");
        EVENT_TYPE_MAPPING.put("Coin Hopper 2 Present", "eventType.coinhopper2.present");
        EVENT_TYPE_MAPPING.put("Coin Hopper 2 Level", "eventType.coinhopper2.level");
        EVENT_TYPE_MAPPING.put("Door Open", "eventType.door.open");
        EVENT_TYPE_MAPPING.put("Pedestal Door Open", "eventType.doorlower.open");
        EVENT_TYPE_MAPPING.put("Maintenance Door Open", "eventType.doorupper.open");
        EVENT_TYPE_MAPPING.put("eventType.maintenancedoor.open", "eventType.maintenancedoor.open");
        EVENT_TYPE_MAPPING.put("Pay Station Battery Depleted", "eventType.paystation.lowerpowershutdown");
        EVENT_TYPE_MAPPING.put("Pay Station Encryption Key Updated", "eventType.paystation.publickeyupdate");
        EVENT_TYPE_MAPPING.put("Pay Station In Service Mode", "eventType.paystation.servicemode");
        EVENT_TYPE_MAPPING.put("Pay Station Software Updated", "eventType.paystation.upgrade");
        EVENT_TYPE_MAPPING.put("Pay Station Ticket Footer Updated", "eventType.paystation.ticketfootersuccess");
        EVENT_TYPE_MAPPING.put("Printer Present", "eventType.printer.present");
        EVENT_TYPE_MAPPING.put("Printer Cutter Fault", "eventType.printer.cuttererror");
        EVENT_TYPE_MAPPING.put("Printer Head Fault", "eventType.printer.headerror");
        EVENT_TYPE_MAPPING.put("Printer Level Disengaged", "eventType.printer.leverdisengaged");
        EVENT_TYPE_MAPPING.put("Paper Status", "eventType.printer.paperstatus");
        EVENT_TYPE_MAPPING.put("Printer Temperature Fault", "eventType.printer.temperatureerror");
        EVENT_TYPE_MAPPING.put("Printer Voltage Fault", "eventType.printer.voltageerror");
        EVENT_TYPE_MAPPING.put("Shock Alarm", "eventType.shockalarm.on");
        EVENT_TYPE_MAPPING.put("PCM Upgraded", "eventType.pcm.upgrader.on");
        EVENT_TYPE_MAPPING.put("PCM - Wireless Signal Strength", "eventType.pcm.wirelesssignalstrength");
        EVENT_TYPE_MAPPING.put("Coin Canister Present", "eventType.coincanister.present");
        EVENT_TYPE_MAPPING.put("Coin Canister Removed", "eventType.coincanister.remove");
        EVENT_TYPE_MAPPING.put("Maintenance Door Open", "eventType.doorupper.open");
        EVENT_TYPE_MAPPING.put("Cash Vault Door Open", "eventType.cashvaultdoor.open");
        EVENT_TYPE_MAPPING.put("Coin Escrow Present", "eventType.coinescrow.present");
        EVENT_TYPE_MAPPING.put("Coin Escrow Jam", "eventType.coinescrow.jam");
        EVENT_TYPE_MAPPING.put("Modem Type: GSM, CDMA, WiFi, Ethernet", "eventType.modem.type");
        EVENT_TYPE_MAPPING.put("Modem CCID (SIM Number)", "eventType.modem.ccid");
        EVENT_TYPE_MAPPING.put("Modem Network Carrier", "eventType.modem.networkcarrier");
        EVENT_TYPE_MAPPING.put("Modem APN (Access Point Name)", "eventType.modem.apn");
        EVENT_TYPE_MAPPING.put("Customer Defined Alert", "eventType.customerdefinedralert.on");
        EVENT_TYPE_MAPPING.put("Coin Changer Level", "eventType.paystation.coinchanger.level");
        EVENT_TYPE_MAPPING.put("Coin Changer Jam", "eventType.paystation.coinchanger.jam");
        EVENT_TYPE_MAPPING.put("RFID Card Reader Present", "eventType.rfidcardreader.present");
        EVENT_TYPE_MAPPING.put("Printer Paper Jam", "eventType.printer.paperjam");
        EVENT_TYPE_MAPPING.put("Ticket Not Taken", "eventType.paystation.ticketnottaken");
        EVENT_TYPE_MAPPING.put("Ticket Did Not Print", "eventType.printer.ticketdidnotprint");
        EVENT_TYPE_MAPPING.put("Printing Failure", "eventType.printer.printingfailure");
        EVENT_TYPE_MAPPING.put("Pay Station - Mac Address Updated", "eventType.paystation.uid.changed");
        EVENT_TYPE_MAPPING.put("Duplicate SerialNumber", "eventType.paystation.uid");
        EVENT_TYPE_MAPPING.put("Pay Station - Clock Reset Requested", "eventType.paystation.clock.changed");
        EVENT_TYPE_MAPPING.put("Violation Status", "eventType.cardReader.violationStatus");
    }
    
    private TestLookupTables() {
    }
    
    @Deprecated
    public static StoryObjectParser getExpressionParser() {
        return TestContext.getInstance().getObjectParser();
    }
    
    @Deprecated
    public static StoryObjectBuilder getObjectBuilder() {
        return TestContext.getInstance().getObjectBuilder();
    }
    
    public static <T> Object findObjectsByFieldFromLookupList(final Object value, final String aliasAttrName, final Class<T> classType) {
        final List<Object> returnList = new ArrayList<Object>();
        final LookupCriteria<?> criteria = new LookupCriteria<Class<?>>(classType, TestLookupTables.getObjectBuilder().locate(classType)
                .getAccessor(aliasAttrName));
        
        DBHelper.lookup(returnList, criteria);
        if (returnList.isEmpty()) {
            return null;
        }
        
        return returnList.get(0);
    }
    
    public static List<TimezoneVId> getTimeZoneList() {
        final List<TimezoneV> timezones = DBHelper.list(TimezoneV.class);
        final List<TimezoneVId> timezoneIds = new ArrayList<TimezoneVId>(timezones.size());
        for (TimezoneV tzv : timezones) {
            timezoneIds.add(tzv.getId());
        }
        
        return timezoneIds;
    }
    
    public static int findMatchingTZ(final String tz) {
        for (TimezoneVId tzVid : TestLookupTables.getTimeZoneList()) {
            if (tzVid.getName().toLowerCase(WebCoreConstants.DEFAULT_LOCALE).equals(tz.toLowerCase(WebCoreConstants.DEFAULT_LOCALE))) {
                return tzVid.getId();
            }
        }
        return 0;
    }
    
    public static String findMatchingTZById(final int id) {
        for (TimezoneVId tzVid : TestLookupTables.getTimeZoneList()) {
            if (tzVid.getId() == id) {
                return tzVid.getName();
            }
        }
        return "";
    }
   
    public static List<Object> lookup(final LookupCriteria<String> criteria, final Transformer... transformers) {
        final List<Object> result = new ArrayList<Object>();
        DBHelper.lookup(result, criteria, transformers);
        
        return result;
    }
    
    public static void lookup(final Collection<? extends Object> result, final LookupCriteria<String> criteria, final Transformer... transformers) {
        DBHelper.lookup(result, criteria, transformers);
    }
    
    @Deprecated
    public static List<EventDefinition> getEventDefinationList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventDefinition.class);
    }
    
    @Deprecated
    public static List<EventException> getEventExceptionList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventException.class);
    }
    
    @Deprecated
    public static List<EventActionType> getEventActionList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventActionType.class);
    }
    
    @Deprecated
    public static List<EventStatusType> getEventStatusList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventStatusType.class);
    }
    
    @Deprecated
    public static List<EventSeverityType> getEventSeverityList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventSeverityType.class);
    }
    
    @Deprecated
    public static Map<String, String> getEventTypeMapping() {
        return EVENT_TYPE_MAPPING;
    }
    
    @Deprecated
    public static List<EventType> getEventTypeList() {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return db.find(EventType.class);
    }
    
    @Deprecated
    public static Map<String, Byte> getQueueTypeMap() {
        return QUEUE_TYPE_MAP;
    }
}
