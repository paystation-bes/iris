package com.digitalpaytech.bdd.util.jsonprimitiveconverter;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.json.JSONConverterSteps;
import com.digitalpaytech.bdd.util.json.ValueAndDataType;
import com.digitalpaytech.util.JSONPrimitiveConverter;

public class TestConvertToJSONPrimitive implements JBehaveTestHandler {
    
    public static final String STRING_TYPE = "string";
    public static final String DOUBLE_TYPE = "double";
    public static final String DOUBLE_TYPE_OF_D_NOTATION = "double type of D notation";
    public static final String DOUBLE_TYPE_WITH_D_NOTATION = "double type with D notation";
    public static final String DOUBLE_TYPE_WITH_D_NOTATION_NO_DECIMALS = "double type with D notation no decimals";
        
    private boolean noErrors = true;
    
    public final void testConvertToJSONPrimitive() {
        
        @SuppressWarnings("unchecked")        
        final List<ValueAndDataType> valueDataTypeList = TestDBMap.findObjectByIdFromMemory(JSONConverterSteps.VAL_AND_TYPE_LIST, ArrayList.class);
        
        for (ValueAndDataType vdt : valueDataTypeList) {
            if (TestConvertToJSONPrimitive.STRING_TYPE.equals(vdt.getDataType())) {
                checkStringType(vdt.getDataValue());
            } else if (TestConvertToJSONPrimitive.DOUBLE_TYPE.equals(vdt.getDataType())) {
                checkDoubleType(vdt.getDataValue());
            } else if (TestConvertToJSONPrimitive.DOUBLE_TYPE_OF_D_NOTATION.equals(vdt.getDataType())) {
                checkDoubleTypeOfDNotation(vdt.getDataValue());
            } else if (TestConvertToJSONPrimitive.DOUBLE_TYPE_WITH_D_NOTATION.equals(vdt.getDataType())) {
                checkDoubleTypeWithDNotation(vdt.getDataValue());
            } else if (TestConvertToJSONPrimitive.DOUBLE_TYPE_WITH_D_NOTATION_NO_DECIMALS.equals(vdt.getDataType())) {                
                checkDoubleTypeWithDNotationNoDecimals(vdt.getDataValue());
            } else {
                checkBooleanType(vdt.getDataValue());
            }
            
            if (!this.noErrors) {
                return;
            }
        }
    }
    
    private Object convert(final String text) {
        final JSONPrimitiveConverter cnvtr = new JSONPrimitiveConverter();
        return cnvtr.convertToJSONPrimitive(text);        
    }
    
    private void checkStringType(final String dataVal) {
        if (!dataVal.equals(convert(dataVal))) {
            this.noErrors = false;
        }
    }
    
    private void checkDoubleType(final String dataVal) {
        if (!Double.valueOf(Double.parseDouble(dataVal)).equals(convert(dataVal))) {
            this.noErrors = false;
        }
    }
    
    private void checkDoubleTypeOfDNotation(final String dataVal) {
        if (!Double.valueOf(String.valueOf(dataVal)).equals(convert(dataVal))) {
            this.noErrors = false;
        }
    }
    
    private void checkDoubleTypeWithDNotation(final String dataVal) {
        if (Double.parseDouble(String.valueOf(dataVal)) != ((Double) convert(dataVal)).doubleValue()) {
            this.noErrors = false;
        }
    }
    
    private void checkDoubleTypeWithDNotationNoDecimals(final String dataVal) {
        if (Double.parseDouble(String.valueOf(dataVal)) != ((Double) convert(dataVal)).doubleValue()) {
            this.noErrors = false;
        }
    }
    
    private void checkBooleanType(final String dataVal) {
        if (Boolean.parseBoolean(dataVal) != ((Boolean) convert(dataVal)).booleanValue()) {
            this.noErrors = false;
        }
    }    
    
    public final void assertValuesConvertedCorrectly() {
        Assert.assertEquals(this.noErrors, true);
    }
    
    public final void assertValuesConvertedInCorrectly() {
        Assert.assertEquals(this.noErrors, false);
    }    
    
    @Override
    public final Object performAction(final Object... objects) {
        testConvertToJSONPrimitive();
        return null;
    }

    @Override
    public final Object assertSuccess(final Object... objects) {
        assertValuesConvertedCorrectly();
        return null;
    }

    @Override
    public final Object assertFailure(final Object... objects) {
        assertValuesConvertedInCorrectly();
        return null;
    }    
    
    
}
