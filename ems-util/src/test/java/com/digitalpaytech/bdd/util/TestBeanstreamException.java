package com.digitalpaytech.bdd.util;

import com.beanstream.exceptions.BeanstreamApiException;

public class TestBeanstreamException extends BeanstreamApiException {        

    private static final long serialVersionUID = 2783492774605997485L;
    
    private final boolean isUserError;    

    public TestBeanstreamException(final int code, final int category, final String message, final int httpStatusCode, final boolean isUserError) {
        super(code, category, message, httpStatusCode);
        this.isUserError = isUserError; 
    }
    
    public final boolean isUserError() {
        return this.isUserError;
    }
    
}
