package com.digitalpaytech.bdd.util.expression.type;

import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.expression.InvalidStoryObjectException;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;

public final class RandomKeyTransformer implements AttributeValueTransformer {
    public static final int MIN_PARAMS_LENGTH = 1;
    
    public static final int PARAM_IDX_RANDOM_KEY_TYPE = 0;
    
    private static final long serialVersionUID = -8572724004742480187L;
    
    private String randomKeyType;
    
    @Override
    public void init(final String[] params) throws InvalidStoryObjectException {
        if (params.length < MIN_PARAMS_LENGTH) {
            throw new InvalidStoryObjectException("This transformer required at least " + MIN_PARAMS_LENGTH + " parameters, the RandomKey's type!");
        }
        
        this.randomKeyType = params[PARAM_IDX_RANDOM_KEY_TYPE].trim();
        if (this.randomKeyType.isEmpty()) {
            throw new InvalidStoryObjectException("Please provide type of the randomKey!");
        }
    }

    @Override
    public Object transform(final Object value, final StoryObjectBuilder builder) {
        Class<?> type = builder.resolveType(this.randomKeyType);
        if (type == null) {
            try {
                type = Class.forName(this.randomKeyType);
            } catch (ClassNotFoundException cnfe) {
                throw new InvalidStoryObjectException("Unable to locate RandomKey's type, class name is " + this.randomKeyType);
            }
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(TestDBMap.getControllerFieldsWrapperFromMem().getRequest());
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        return keyMapping.getRandomString(type, value);
    }
    
}
