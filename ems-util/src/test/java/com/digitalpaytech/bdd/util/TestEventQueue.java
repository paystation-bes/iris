package com.digitalpaytech.bdd.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.UseConcurrentHashMap", "PMD.BeanMembersShouldSerialize" })
public class TestEventQueue {
    private final Map<Byte, Queue<Object>> queueMap = new HashMap<Byte, Queue<Object>>();
    
    public TestEventQueue() {
        this.queueMap.put(WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE, new LinkedList<Object>());
        this.queueMap.put(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT, new LinkedList<Object>());
        this.queueMap.put(WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL, new LinkedList<Object>());
        this.queueMap.put(WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT, new LinkedList<Object>());
        this.queueMap.put(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT, new LinkedList<Object>());
    }
    
    public final Queue<Object> getQueue(final String queueTypeString) {
        final Byte queueType = getQueueTypeFromString(queueTypeString);
        return getQueue(queueType);
    }
    
    public final Queue<Object> getQueue(final Byte queueType) {
        return this.queueMap.get(queueType);
        
    }
    
    public final void offerToQueue(final Object o, final String queueTypeString) {
        final Byte queueType = getQueueTypeFromString(queueTypeString);
        offerToQueue(o, queueType);
    }
    
    public final void offerToQueue(final Object o, final Byte queueType) {
        this.queueMap.get(queueType).offer(o);
    }
    
    public final Object pollQueue(final String queueTypeString) {
        final Byte queueType = getQueueTypeFromString(queueTypeString);
        return pollQueue(queueType);
    }
    
    public final Object pollQueue(final Byte queueType) {
        return this.queueMap.get(queueType).poll();
    }
    
    public final Byte getQueueTypeFromString(final String queueTypeString) {
        return TestLookupTables.getQueueTypeMap().get(queueTypeString.toLowerCase(WebCoreConstants.DEFAULT_LOCALE));
    }
}
