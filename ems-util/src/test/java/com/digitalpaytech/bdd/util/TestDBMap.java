package com.digitalpaytech.bdd.util;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

// PMD.AvoidPrintStackTrace: Test code stack trace in log is not an issue.
@SuppressWarnings({ "PMD.AvoidPrintStackTrace", "PMD.LooseCoupling" })
public final class TestDBMap {
    
    public static final int WEB_SECURITY_FORM_ID = 99999;
    public static final int CONTROLLER_FIELDS_WRAPPER_ID = 99998;
    public static final int OBJECT_MAP_ID = 99997;
    public static final int TEST_EVENT_QUEUE_ID = 99996;
    
    private TestDBMap() {
    
    }
    
    @Deprecated
    /**
     * Please use the method in TestContext.getInstance().getDatabase() instead of this class
     * 
     */
    public static void addToMemory(final Object id, final Object object) {
        DBHelper.save(id, object);
    }
    
    @Deprecated
    /**
     * Please use the method in TestContext.getInstance().getDatabase() instead of this class
     * 
     */
    public static <T> void deleteFromMemory(final Object id, final Class<T> classType) {
        DBHelper.remove(classType, id);
    }
    
    @Deprecated
    /**
     * Please use the method in TestContext.getInstance().getDatabase() instead of this class
     * 
     */
    public static <T> T findObjectByFieldFromMemory(final String value, final String aliasAttrName, final Class<T> classType) {
        return DBHelper.findOne(classType, aliasAttrName, value);
    }
    
    @Deprecated
    /**
     * Please use the method in TestContext.getInstance().getDatabase() instead of this class
     * 
     */
    public static <T> T findObjectByIdFromMemory(final Object id, final Class<T> classType) {
        return DBHelper.findById(classType, id);
    }
    
    @Deprecated
    /**
     * Please use the method in TestContext.getInstance().getDatabase() instead of this class
     * 
     */
    public static <T> List<T> getTypeListFromMemory(final Class<T> classType) {
        return DBHelper.list(classType);
    }
    
    public static ControllerFieldsWrapper getControllerFieldsWrapperFromMem() {
        return DBHelper.findById(ControllerFieldsWrapper.class, TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID);
    }
    
    public static void saveControllerFieldsWrapperToMem(final ControllerFieldsWrapper controllerFields) {
        DBHelper.save(TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID, controllerFields);
    }
    
    public static void saveObjectMapToMem(final Map<String, Object> objectMap) {
        DBHelper.save(TestDBMap.OBJECT_MAP_ID, objectMap);
    }
    
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getObjectMapFromMem() {
        return DBHelper.findById(HashMap.class, OBJECT_MAP_ID);
    }
    
    public static TestEventQueue getTestEventQueueFromMem() {
        final List<TestEventQueue> testEventQueueList = DBHelper.list(TestEventQueue.class);
        
        Assert.assertEquals(1, testEventQueueList.size());
        return (TestEventQueue) testEventQueueList.toArray()[0];
    }
    
    public static void saveTestEventQueueToMem(final TestEventQueue teq) {
        DBHelper.save(TestDBMap.TEST_EVENT_QUEUE_ID, teq);
    }
    
    public static SessionTool getSessionTool() {
        return SessionTool.getInstance(TestDBMap.getControllerFieldsWrapperFromMem().getRequest());
    }
    
    public static RandomKeyMapping getKeyMapping() {
        return getSessionTool().getKeyMapping();
    }
    
}
