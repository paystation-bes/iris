package com.digitalpaytech.bdd.util.expression.type;

import com.digitalpaytech.bdd.util.expression.InvalidStoryObjectException;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.bdd.util.expression.TransformationException;
import com.digitalpaytech.util.WebCoreConstants;

public class PrinterStatusTransformer implements StoryValueTransformer {
    
    public static final String EMPTY = "Empty";
    public static final String FULL = "Full";
    public static final String LOW = "Low";
    public static final String NORMAL = "Normal";
    public static final String JAM = "Jam";
    public static final int MIN_PARAMS_LENGTH = 0;
    private static final long serialVersionUID = 6869290626761481311L;
    
    @Override
    public final void init(final String[] params) throws InvalidStoryObjectException {
        if (params.length < MIN_PARAMS_LENGTH) {
            throw new InvalidStoryObjectException("This transformer required at least " + MIN_PARAMS_LENGTH + " parameters");
        }
        
    }
    
    @Override
    public final String transform(final String value, final StoryObjectBuilder builder) throws TransformationException {
        switch (value) {
            case JAM:
                return String.valueOf(WebCoreConstants.LEVEL_JAM);
            case NORMAL:
                return String.valueOf(WebCoreConstants.LEVEL_NORMAL);
            case LOW:
                return String.valueOf(WebCoreConstants.LEVEL_LOW);
            case FULL:
                return String.valueOf(WebCoreConstants.LEVEL_FULL);
            case EMPTY:
                return String.valueOf(WebCoreConstants.LEVEL_EMPTY);
            default:
                return value;
        }
    }
    
}
