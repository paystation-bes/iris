package com.digitalpaytech.bdd.util.json;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;

public class JSONConverterSteps extends AbstractSteps {
    
    public static final String VAL_AND_TYPE_LIST = "valAndTypeList";
    
    public JSONConverterSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    @Given("there is a string <value> that converts to <dataType> type")
    public final void getConversionValueAndDataType(@Named("value") final String value, @Named("dataType") final String dataType) {
        TestContext.getInstance().close();
        final List<ValueAndDataType> valueDataTypeList = new ArrayList<ValueAndDataType>();
        TestDBMap.addToMemory(VAL_AND_TYPE_LIST, valueDataTypeList);
        valueDataTypeList.add(new ValueAndDataType(value, dataType));
        TestDBMap.addToMemory(VAL_AND_TYPE_LIST, valueDataTypeList);
    }
    
}