package com.digitalpaytech.bdd.util.json;

public final class ValueAndDataType {

    private String dataValue;
    
    private String dataType;

    public ValueAndDataType(String dataValue, String dataType) {
        this.dataValue = dataValue;
        this.dataType = dataType;
    }
    
    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}