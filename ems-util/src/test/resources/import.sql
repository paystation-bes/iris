SET DATABASE REFERENTIAL INTEGRITY FALSE;

-- -----------------------------------------------------
-- Table AccessPoint
-- -----------------------------------------------------

INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'digitalpaytech.com',   CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'INTERNET.COM',         CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'TELARGO.T-MOBILE.COM', CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'EPC.TMOBILE.COM',      CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'ISP.CINGULAR',         CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'WAP.CINGULAR',         CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'SP.TELUS.COM',         CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'at*netapn?',           CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'OK',                   CURRENT_TIME, 1) ;
INSERT INTO AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'ERROR',                CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table AlertClassType
-- -----------------------------------------------------

INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Communication', 	CURRENT_TIME, 1); 
INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Collection', 		CURRENT_TIME, 1);
INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Pay Station Alert', CURRENT_TIME, 1);


-- -----------------------------------------------------
-- Table AlertType
-- -----------------------------------------------------

INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 1,	1, 				  'Last Seen Interval',			1,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 2,	2, 				  'Running Total',				1,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id,	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 3,	2, 				  'Coin Canister',				1,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 4,	2, 				  'Bill Stacker',				1,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 5,	2, 				  'Unsettled Credit Cards',		1,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 6,	3, 				  'Pay Station Alert',			0,				CURRENT_TIME, 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES( 7,	2, 				  'Last Collection Interval',	1,				CURRENT_TIME, 1 );

						
-- -----------------------------------------------------
-- Table AlertThresholdType
-- -----------------------------------------------------						
						
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, 						LastModifiedGMT, LastModifiedByUserId) VALUES(1,	1,			 'Last Seen Interval Hour', CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, 						LastModifiedGMT, LastModifiedByUserId) VALUES(2,	2,			 'Running Total Dollar',   	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES(6,	3,			 'Coin Canister Count',   	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES(7,	3,			 'Coin Canister Dollar',  	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES(8,	4,			 'Bill Stacker Count',   	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES(9,	4,			 'Bill Stacker Dollar',   	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES(10,	5,			 'Unsettled Credit Card Count',  CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES(11,	5,			 'Unsettled Credit Card Dollar', CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, LastModifiedByUserId) VALUES(12,	6,			 'Pay Station Alert',   	CURRENT_TIME,  		1 );
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES(13,	7,			 'Collection Overdue',      CURRENT_TIME,  		1 );

						
-- -----------------------------------------------------
-- Table ActivityType
-- -----------------------------------------------------

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, NULL, 'Login',                                  NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101,  100, 'User Login',                             NULL, 'UserLogin'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102,  100, 'User Logout',                            NULL, 'UserLogout'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 103,  100, 'Login Failure',                          NULL, 'LoginFailure'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 104,  100, 'Locked Out',                             NULL, 'UserLockedOut'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 105,  100, 'User Switched In',                       NULL, 'UserSwitchedIn'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 106,  100, 'User Switched out',                      NULL, 'UserSwitchedOut'					,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 200, NULL, 'BOSS',                                   NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 201,  200, 'Configuration Upload',                   NULL, 'ConfigurationUpload'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 202,  200, 'Bad Card List Download',                 NULL, 'BadCardListDownload'				,0,CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 203,  200, 'Transaction Upload',                     NULL, 'TransactionUpload'				,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 300, NULL, 'Coupon Configuration',                   NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 301,  300, 'Coupon Bulk Export',                     NULL, 'CouponBulkExport'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 302,  300, 'Coupon Bulk Import',                     NULL, 'CouponBulkImport'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 303,  300, 'Add Coupon',                             NULL, 'AddCoupon'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 304,  300, 'Edit Coupon',                            NULL, 'EditCoupon'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 305,  300, 'Delete Coupon',                          NULL, 'DeleteCoupon'						,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 400, NULL, 'Card Management Configuration',          NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 401,  400, 'Card Bulk Export',                       NULL, 'CardBulkExport'			 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 402,  400, 'Card Bulk Import',                       NULL, 'CardBulkImport'			 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 403,  400, 'Add Permitted Record',                   NULL, 'AddPermittedRecord'		 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 404,  400, 'Edit Permitted Record',                  NULL, 'EditPermittedRecord'		 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 405,  400, 'Delete Permitted Record',                NULL, 'DeletePermittedRecord'		 	,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 406,  400, 'Add Banned Record',                      NULL, 'AddBannedRecord'			 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 407,  400, 'Edit Banned Record',                     NULL, 'EditBannedRecord'			 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 408,  400, 'Delete Banned Record',                   NULL, 'DeleteBannedRecord'		 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 409,  400, 'Add Card Type',                          NULL, 'AddCardType'				 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 410,  400, 'Edit Card Type',                         NULL, 'EditCardType'				 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 411,  400, 'Delete Card Type',                       NULL, 'DeleteCardType'			 		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 412,  400, 'Credit Card Refund',                     NULL, 'CreditCardRefund'			 		,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 500, NULL, 'Location Configuration',                 NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 501,  500, 'Add Location',                           NULL, 'AddLocation'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 502,  500, 'Edit Location',                          NULL, 'EditLocation'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 503,  500, 'Delete Location',                        NULL, 'DeleteLocation'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 504,  500, 'Assign Pay Station to Location',         NULL, 'AssignPayStationtoLocation'		,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 600, NULL, 'Pay Station Configuration',              NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 601,  600, 'Pay Station is Visible',                 NULL, 'PayStationisVisible'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 602,  600, 'Pay Station is Hidden',                  NULL, 'PayStationisHidden'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 603,  600, 'Activate Pay Station',                   NULL, 'ActivatePayStation'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 604,  600, 'Deactivate Pay Station',                 NULL, 'DeactivatePayStation'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 605,  600, 'Edit Settings',                          NULL, 'EditSettings'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 606,  600, 'Create Pay Station',                     NULL, 'CreatePayStation'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 607,  600, 'Move Pay Station',                       NULL, 'MovePayStation'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 608,  600, 'Configuration Download Status',          NULL, 'ConfigurationDownloadStatus'		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 609,  600, 'Software Updated',                       NULL, 'SoftwareUpdated'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 610,  600, 'Encryption Key Download',                NULL, 'EncryptionKeyDownload'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 611,  600, 'Test Activate Pay Station',              NULL, 'TestActivatePayStation'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 612,  600, 'Test Deactivate Pay Station',            NULL, 'TestDeactivatePayStation'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 613,  600, 'Bill Pay Station Monthly',               NULL, 'BillPayStationMonthly'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 614,  600, 'Do Not Bill Pay Station Monthly',        NULL, 'DoNotBillPayStationMonthly'		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 615,  600, 'Pay Station has Bundled Data',           NULL, 'PayStationhasBundledData'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 616,  600, 'Pay Station does Not have Bundled Data', NULL, 'PayStationdoesNothaveBundledData'	,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 700, NULL, 'Route Configuration',                    NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 701,  700, 'Add Route',                              NULL, 'AddRoute'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 702,  700, 'Edit Route',                             NULL, 'EditRoute'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 703,  700, 'Delete Route',                           NULL, 'DeleteRoute'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 704,  700, 'Assign Pay Station to Route',            NULL, 'AssignPayStationtoRoute'			,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 800, NULL, 'Alert Configuration',                    NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 801,  800, 'Add Alert',                              NULL, 'AddAlert'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 802,  800, 'Edit Alert',                             NULL, 'EditAlert'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 803,  800, 'Delete Alert',                           NULL, 'DeleteAlert'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 804,  800, 'Pay Station Alert',                      NULL, 'PayStationAlert'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 805,  800, 'User Defined Alert',                     NULL, 'UserDefinedAlert'					,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 900, NULL, 'Extend-By-Phone Configuration',          NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 901,  900, 'Add Rate',                               NULL, 'AddRate'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 902,  900, 'Edit Rate',                              NULL, 'EditRate'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 903,  900, 'Delete Rate',                            NULL, 'DeleteRate'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 904,  900, 'Add Policy',                             NULL, 'AddPolicy'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 905,  900, 'Edit Policy',                            NULL, 'EditPolicy'						,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 906,  900, 'Delete Policy',                          NULL, 'DeletePolicy'						,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1000, NULL, 'User Configuration',                     NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1001, 1000, 'Add User Account',                       NULL, 'AddUserAccount'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1002, 1000, 'Edit User Account',                      NULL, 'EditUserAccount'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1003, 1000, 'Delete User Account',                    NULL, 'DeleteUserAccount'				,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1100, NULL, 'Role Configuration',                     NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1101, 1100, 'Add Role',                               NULL, 'AddRole'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1102, 1100, 'Edit Role',                              NULL, 'EditRole'							,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1103, 1100, 'Delete Role',                            NULL, 'DeleteRole'						,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1200, NULL, 'Reporting Configuration',                NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1201, 1200, 'Add Scheduled Report',                   NULL, 'AddScheduledReport'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1202, 1200, 'Edit Scheduled Report',                  NULL, 'EditScheduledReport'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1203, 1200, 'Delete Scheduled Report',                NULL, 'DeleteScheduledReport'			,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1300, NULL, 'Company Preferences Configuration',      NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1301, 1300, 'Edit Value',                             NULL, 'EditValue'						,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1400, NULL, 'DTP Admin',                              NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1401, 1400, 'Process Next External Key',              NULL, 'ProcessNextExternalKey'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1402, 1400, 'Process Uploaded Files',                 NULL, 'ProcessUploadedFiles'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1403, 1400, 'Process Card Retry',                     NULL, 'ProcessCardRetry'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1404, 1400, 'Promote Server',                         NULL, 'PromoteServer'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1405, 1400, 'Shutdown Tomcat Server',                 NULL, 'ShutdownTomcatServer'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1406, 1400, 'Add System Notification',                NULL, 'AddSystemNotification'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1407, 1400, 'Edit System Notification',               NULL, 'EditSystemNotification'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1408, 1400, 'Delete System Notification',             NULL, 'DeleteSystemNotification'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1409, 1400, 'Archive System Notification',            NULL, 'ArchiveSystemNotification'		,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1410, 1400, 'Add Merchant Account',                   NULL, 'AddMerchantAccount'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1411, 1400, 'Edit Merchant Account',                  NULL, 'EditMerchantAccount'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1412, 1400, 'Delete Merchant Account',                NULL, 'DeleteMerchantAccount'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1413, 1400, 'Add Digital API',                        NULL, 'AddDigitalAPI'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1414, 1400, 'Edit Digital API',                       NULL, 'EditDigitalAPI'					,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1415, 1400, 'Delete Digital API',                     NULL, 'DeleteDigitalAPI'					,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1500, NULL, 'Dashboard Widget Configuration',         NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1501, 1500, 'Assign Default Dashboard',               NULL, 'AssignDefaultDashboard'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1502, 1500, 'Create Customized Dashboard',            NULL, 'CreateCustomizedDashboard'		,0, CURRENT_TIME, 1) ;

INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1600, NULL, 'Customer Agreement Configuration',       NULL, ''									,1, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1601, 1600, 'Add Customer Agreement',                 NULL, 'AddCustomerAgreement'				,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1602, 1600, 'Edit Customer Agreement',                NULL, 'EditCustomerAgreement'			,0, CURRENT_TIME, 1) ;
INSERT INTO ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1603, 1600, 'Delete Customer Agreement',              NULL, 'DeleteCustomerAgreement'			,0, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table AuthorizationType
-- -----------------------------------------------------

INSERT INTO AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'None',             CURRENT_TIME, 1) ;
INSERT INTO AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Internal List',    CURRENT_TIME, 1) ;
INSERT INTO AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'External Server',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CardType
-- -----------------------------------------------------

INSERT INTO CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',                       CURRENT_TIME, 1) ;
INSERT INTO CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Credit Card',               CURRENT_TIME, 1) ;
INSERT INTO CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Smart Card',                CURRENT_TIME, 1) ;
INSERT INTO CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Passcard',                CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table Carrier
-- -----------------------------------------------------

INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'AT&T'           , CURRENT_TIME, 1) ;
INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'at*netop?'      , CURRENT_TIME, 1) ;
INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'CAN Rogers Wire', CURRENT_TIME, 1) ;
INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'ROGERS'         , CURRENT_TIME, 1) ;
INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'T-Mobile'       , CURRENT_TIME, 1) ;
INSERT INTO Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Verizon'        , CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table ChangeType
-- -----------------------------------------------------

INSERT INTO ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'N/A',    CURRENT_TIME, 1) ;
INSERT INTO ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Insert', CURRENT_TIME, 1) ;
INSERT INTO ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Update', CURRENT_TIME, 1) ;
INSERT INTO ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Delete', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CollectionType
-- -----------------------------------------------------

INSERT INTO CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'All',  CURRENT_TIME, 1) ;
INSERT INTO CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Bill', CURRENT_TIME, 1) ;
INSERT INTO CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Coin', CURRENT_TIME, 1) ;
INSERT INTO CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Card', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CreditCardType
-- -----------------------------------------------------

INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',           0,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Not Assigned',  0,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'VISA',          1,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'MasterCard',    1,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'AMEX',          1,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Discover',      1,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Diners Club',   1,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'CreditCard',    0,      CURRENT_TIME, 1) ;
INSERT INTO CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Other',         0,      CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerPropertyType
-- -----------------------------------------------------

INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Timezone',                        CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Query Spaces By',                 CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Max User Accounts',               CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Credit Card Offline Retry',       CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Max Offline Retry',               CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'SMS Warning Period',              CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Hidden Pay Stations Reported',     CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'GPS Latitude',                    CURRENT_TIME, 1) ;
INSERT INTO CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'GPS Longitude',                   CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerStatusType
-- -----------------------------------------------------

INSERT INTO CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', CURRENT_TIME, 1) ;
INSERT INTO CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  CURRENT_TIME, 1) ;
INSERT INTO CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Trial',    CURRENT_TIME, 1) ;
INSERT INTO CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Deleted',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerType
-- -----------------------------------------------------

INSERT INTO CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'DPT',    CURRENT_TIME, 1) ;
INSERT INTO CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Parent', CURRENT_TIME, 1) ;
INSERT INTO CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Child',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table DaylightSaving
-- -----------------------------------------------------

INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2007-03-11 02:00:00','2007-11-04 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2008-03-09 02:00:00','2008-11-02 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2009-03-08 02:00:00','2009-11-01 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2010-03-14 02:00:00','2010-11-07 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2011-03-13 02:00:00','2011-11-06 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2012-03-11 02:00:00','2012-11-04 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2013-03-10 02:00:00','2013-11-03 02:00:00', CURRENT_TIME, 1);
INSERT INTO  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2014-03-09 02:00:00','2014-11-02 02:00:00', CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table DayOfWeek
-- -----------------------------------------------------

INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1, 'Sunday'   , 'Sun', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 2, 'Monday'   , 'Mon', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 3, 'Tuesday'  , 'Tue', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 4, 'Wednesday', 'Wed', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 5, 'Thursday' , 'Thu', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 6, 'Friday'   , 'Fri', CURRENT_TIME, 1);
INSERT INTO  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 7, 'Saturday' , 'Sat', CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table DenominationType
-- -----------------------------------------------------

INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 1,     5, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 1,    10, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 1,    25, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1,    50, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 1,   100, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1,   200, CURRENT_TIME, 1) ;

INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 2,   100, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 2,   200, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 2,   500, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (104, 2,  1000, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (105, 2,  2000, CURRENT_TIME, 1) ;
INSERT INTO DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (106, 2,  5000, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table EntityType
-- -----------------------------------------------------

INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'Customer',            CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'Customer Agreement',  CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Customer Properties', CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'Customer Services',   CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'User Account',        CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'User Role',           CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'Role',                CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Role Permission',     CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Customer Role',       CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Notification',        CURRENT_TIME, 1) ;
INSERT INTO EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11, 'Activity Login',      CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table EventActionType
-- -----------------------------------------------------

INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, '',                 CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Alerted',          CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Cleared',          CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Full',             CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Normal',           CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Low',              CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Empty',            CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'High',             CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Info',             CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  'Violated',         CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Tampered',         CURRENT_TIME, 1) ;
INSERT INTO EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Fault',            CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table EventType
-- -----------------------------------------------------

INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1,   0, 'eventType.battery.level',                  1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2,   2, 'eventType.billacceptor.present',           0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3,   2, 'eventType.billacceptor.jam',               1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4,   2, 'eventType.billacceptor.unableToStack',     1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5,   3, 'eventType.billstacker.present',            0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6,   3, 'eventType.billstacker.level',              1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7,   3, 'eventType.billcanister.remove',            1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8,   4, 'eventType.cardreader.present',             1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9,   5, 'eventType.coinacceptor.present',           0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,  5, 'eventType.coinacceptor.jam',               1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,  6, 'eventType.coinchanger.present',            0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,  6, 'eventType.coinchanger.jam',                1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,  6, 'eventType.coinchanger.level',              1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14,  7, 'eventType.coinhopper1.present',            0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15,  7, 'eventType.coinhopper1.level',              1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 16,  8, 'eventType.coinhopper2.present',            0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 17,  8, 'eventType.coinhopper2.level',              1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 18,  9, 'eventType.door.open',                      1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 19, 10, 'eventType.doorlower.open',                 1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 20, 11, 'eventType.doorupper.open',                 1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 21, 12, 'eventType.paystation.lowerpowershutdown',  1, 1, 1, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 22, 12, 'eventType.paystation.publickeyupdate',     0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 23, 12, 'eventType.paystation.servicemode',         1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 24, 12, 'eventType.paystation.upgrade',             0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 25, 12, 'eventType.paystation.ticketfootersuccess', 0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 26, 13, 'eventType.printer.present',                1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 27, 13, 'eventType.printer.cuttererror',            1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 28, 13, 'eventType.printer.headerror',              1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 29, 13, 'eventType.printer.leverdisengaged',        1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 30, 13, 'eventType.printer.paperstatus',            1, 1, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 31, 13, 'eventType.printer.temperatureerror',       1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 32, 13, 'eventType.printer.voltageerror',           1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 33, 14, 'eventType.shockalarm.on',                  1, 1, 1, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 34, 15, 'eventType.pcm.upgrader.on',                0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 35, 15, 'eventType.pcm.wirelesssignalstrength',     0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 36, 16, 'eventType.coincanister.present',           0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 37, 16, 'eventType.coincanister.remove',            1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 38, 17, 'eventType.maintenancedoor.open',           1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 39, 18, 'eventType.cashvaultdoor.open',             1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 40, 19, 'eventType.coinescrow.present',             0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 41, 19, 'eventType.coinescrow.jam',                 1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 42, 21, 'eventType.modem.type',                     0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 43, 21, 'eventType.modem.ccid',                     0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 44, 21, 'eventType.modem.networkcarrier',           0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 45, 21, 'eventType.modem.apn',                      0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 46, 20, 'eventType.customerdefinedralert.on',       1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 47, 12, 'eventType.paystation.coinchanger.level',   1, 0, 0, 1, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 48, 12, 'eventType.paystation.coinchanger.jam',     1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 49, 22, 'eventType.rfidcardreader.present',         0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 50, 13, 'eventType.printer.paperjam',               1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 51, 12, 'eventType.paystation.ticketnottaken',      1, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 52, 13, 'eventType.printer.ticketdidnotprint',      1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 53, 13, 'eventType.printer.printingfailure',        1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 54, 12, 'eventType.paystation.uid.changed',         0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 55, 12, 'eventType.paystation.uid',                 1, 1, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 56, 12, 'eventType.paystation.clock.changed',       0, 0, 0, 0, 0, CURRENT_TIME, 1);
INSERT INTO EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, IsDelayable, LastModifiedGMT, LastModifiedByUserId) VALUES ( 57,  4, 'eventType.cardReader.violationStatus',     1, 1, 0, 1, 0, CURRENT_TIME, 1); 
-- -----------------------------------------------------
-- Table EventDefinition
-- -----------------------------------------------------

INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  1,  0,  3,  4,  0, 'eventdefinition.battery.level.normal',               'Normal',                        1, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  2,  0,  3,  5,  2, 'eventdefinition.battery.level.low',                  'Low',                           1, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  3,  1,  3,  4,  0, 'eventdefinition.battery2.level.normal',              'Normal',                        1, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  4,  1,  3,  5,  2, 'eventdefinition.battery2.level.low',                 'Low',                           1, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  5,  2,  0,  1,  0, 'eventdefinition.billacceptor.present.on',            'Present',                       2, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  2,  0,  2,  0, 'eventdefinition.billacceptor.present.off',           'NotPresent',                    2, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  7,  2,  1,  1,  2, 'eventdefinition.billacceptor.jam.on',                'Jam',                           3, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  8,  2,  1,  2,  0, 'eventdefinition.billacceptor.jam.off',               'JamCleared',                    3, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  9,  2,  2,  1,  2, 'eventdefinition.billacceptor.level.full',            'UnableToStack',                 4, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,  2,  2,  2,  0, 'eventdefinition.billacceptor.level.normal',          'UnableToStackCleared',          4, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,  3,  0,  1,  0, 'eventdefinition.billstacker.present.on',             'Present',                       5, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,  3,  0,  2,  0, 'eventdefinition.billstacker.present.off',            'NotPresent',                    5, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,  3,  3,  3,  2, 'eventdefinition.billstacker.level.full',             'Full',                          6, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14,  3,  3,  4,  0, 'eventdefinition.billstacker.level.normal',           'Normal',                        6, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15,  3, 18,  1,  1, 'eventdefinition.billcanister.remove.on',             'Removed',                       7, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 16,  3, 18,  2,  0, 'eventdefinition.billcanister.remove.off',            'Inserted',                      7, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 17,  4,  0,  1,  0, 'eventdefinition.cardreader.present.on',              'Present',                       8, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 18,  4,  0,  2,  2, 'eventdefinition.cardreader.present.off',             'NotPresent',                    8, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 19,  5,  0,  1,  0, 'eventdefinition.coinacceptor.present.on',            'Present',                       9, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 20,  5,  0,  2,  0, 'eventdefinition.coinacceptor.present.off',           'NotPresent',                    9, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 21,  5,  1,  1,  2, 'eventdefinition.coinacceptor.jam.on',                'Jam',                          10, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 22,  5,  1,  2,  0, 'eventdefinition.coinacceptor.jam.off',               'JamCleared',                   10, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 23,  6,  0,  1,  0, 'eventdefinition.coinchanger.present.on',             'Present',                      11, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 24,  6,  0,  2,  0, 'eventdefinition.coinchanger.present.off',            'NotPresent',                   11, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 25,  6,  1,  1,  2, 'eventdefinition.coinchanger.jam.on',                 'Jam',                          12, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 26,  6,  1,  2,  0, 'eventdefinition.coinchanger.jam.off',                'JamCleared',                   12, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 27,  6,  3,  4,  0, 'eventdefinition.coinchanger.level.normal',           'Normal',                       13, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 28,  6,  3,  5,  1, 'eventdefinition.coinchanger.level.low',              'Low',                          13, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 29,  6,  3,  6,  2, 'eventdefinition.coinchanger.level.empty',            'Empty',                        13, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 30,  7,  0,  1,  0, 'eventdefinition.coinhopper1.present.on',             'Present',                      14, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 31,  7,  0,  2,  0, 'eventdefinition.coinhopper1.present.off',            'NotPresent',                   14, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 32,  7,  3,  4,  0, 'eventdefinition.coinhopper1.level.normal',           'EmptyCleared',                 15, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 33,  7,  3,  6,  2, 'eventdefinition.coinhopper1.level.empty',            'Empty',                        15, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 34,  8,  0,  1,  0, 'eventdefinition.coinhopper2.present.on',             'Present',                      16, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 35,  8,  0,  2,  0, 'eventdefinition.coinhopper2.present.off',            'NotPresent',                   16, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 36,  8,  3,  4,  0, 'eventdefinition.coinhopper2.level.normal',           'EmptyCleared',                 17, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 37,  8,  3,  6,  2, 'eventdefinition.coinhopper2.level.empty',            'Empty',                        17, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 38,  9,  4,  1,  1, 'eventdefinition.door.open.on',                       'DoorOpened',                   18, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 39,  9,  4,  2,  0, 'eventdefinition.door.open.off',                      'DoorClosed',                   18, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 40, 10,  4,  1,  1, 'eventdefinition.doorlower.open.on',                  'DoorOpened',                   19, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 41, 10,  4,  2,  0, 'eventdefinition.doorlower.open.off',                 'DoorClosed',                   19, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 42, 11,  4,  1,  1, 'eventdefinition.doorupper.open.on',                  'DoorOpened',                   20, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 43, 11,  4,  2,  0, 'eventdefinition.doorupper.open.off',                 'DoorClosed',                   20, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 44, 12,  5,  1,  3, 'eventdefinition.paystation.lowerpowershutdown.on',   'LowPowerShutdownOn',           21, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 45, 12,  5,  2,  0, 'eventdefinition.paystation.lowerpowershutdown.off',  'LowPowerShutdownOff',          21, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 46, 12,  6,  0,  0, 'eventdefinition.paystation.publickeyupdate.on',      'PublicKeyUpdate',              22, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 47, 12,  6,  2,  0, 'eventdefinition.paystation.publickeyupdate.off',     'PublicKeyUpdateOff',           22, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 48, 12,  7,  1,  1, 'eventdefinition.paystation.servicemode.on',          'ServiceModeEnabled',           23, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 49, 12,  7,  2,  0, 'eventdefinition.paystation.servicemode.off',         'ServiceModeDisabled',          23, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 50, 12,  8,  0,  0, 'eventdefinition.paystation.upgrade.on',              'Upgrade',                      24, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 51, 12,  9,  2,  0, 'eventdefinition.paystation.ticketfootersuccess.off', '',                             25, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 52, 13,  0,  1,  0, 'eventdefinition.printer.present.on',                 'Present',                      26, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 53, 13,  0,  2,  3, 'eventdefinition.printer.present.off',                'NotPresent',                   26, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 54, 13, 10,  1,  1, 'eventdefinition.printer.cuttererror.on',             'CutterError',                  27, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 55, 13, 10,  2,  0, 'eventdefinition.printer.cuttererror.off',            'CutterErrorCleared',           27, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 56, 13, 11,  1,  1, 'eventdefinition.printer.headerror.on',               'HeadError',                    28, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 57, 13, 11,  2,  0, 'eventdefinition.printer.headerror.off',              'HeadErrorCleared',             28, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 58, 13, 12,  1,  2, 'eventdefinition.printer.leverdisengaged.on',         'LeverDisengaged',              29, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 59, 13, 12,  2,  0, 'eventdefinition.printer.leverdisengaged.off',        'LeverDisengagedCleared',       29, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 60, 13, 13,  4,  0, 'eventdefinition.printer.paperstatus.normal',         'PaperNormal',                  30, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 61, 13, 13,  5,  2, 'eventdefinition.printer.paperstatus.low',            'PaperLow',                     30, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 62, 13, 13,  6,  3, 'eventdefinition.printer.paperstatus.out',            'PaperOut',                     30, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 63, 13, 13,  2,  0, 'eventdefinition.printer.paperstatus.cleared',        'PaperOutCleared',              30, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 64, 13, 14,  1,  1, 'eventdefinition.printer.temperatureerror.on',        'TemperatureError',             31, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 65, 13, 14,  2,  0, 'eventdefinition.printer.temperatureerror.off',       'TemperatureErrorCleared',      31, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 66, 13, 15,  1,  1, 'eventdefinition.printer.voltageerror.on',            'VoltageError',                 32, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 67, 13, 15,  2,  0, 'eventdefinition.printer.voltageerror.off',           'VoltageErrorCleared',          32, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 68, 14, 16,  1,  3, 'eventdefinition.shockalarm.on.on',                   'ShockOn',                      33, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 69, 14, 16,  2,  0, 'eventdefinition.shockalarm.on.off',                  'ShockOff',                     33, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 70, 15,  8,  0,  0, 'eventdefinition.pcm.upgrader.on',                    'Upgrade',                      34, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 71, 15, 17,  0,  0, 'eventdefinition.pcm.wirelesssignalstrength.on',      'WirelessSignalStrength',       35, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 72, 16,  0,  1,  0, 'eventdefinition.coincanister.present.on',            'Present',                      36, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 73, 16,  0,  2,  0, 'eventdefinition.coincanister.present.off',           'NotPresent',                   36, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 74, 16, 18,  1,  1, 'eventdefinition.coincanister.remove.on',             'Removed',                      37, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 75, 16, 18,  2,  0, 'eventdefinition.coincanister.remove.off',            'Inserted',                     37, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 76, 17,  4,  1,  1, 'eventdefinition.maintenancedoor.open.on',            'DoorOpened',                   38, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 77, 17,  4,  2,  0, 'eventdefinition.maintenancedoor.open.off',           'DoorClosed',                   38, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 78, 18,  4,  1,  1, 'eventdefinition.cashvaultdoor.open.on',              'DoorOpened',                   39, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 79, 18,  4,  2,  0, 'eventdefinition.cashvaultdoor.open.off',             'DoorClosed',                   39, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 80, 19,  0,  1,  0, 'eventdefinition.coinescrow.present.on',              'Present',                      40, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 81, 19,  0,  2,  0, 'eventdefinition.coinescrow.present.off',             'NotPresent',                   40, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 82, 19,  1,  1,  2, 'eventdefinition.coinescrow.jam.on',                  'Jam',                          41, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 83, 19,  1,  2,  0, 'eventdefinition.coinescrow.jam.off',                 'JamCleared',                   41, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 84, 21, 19,  0,  0, 'eventdefinition.modem.type.on',                      '',                             42, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 85, 21, 20,  0,  0, 'eventdefinition.modem.ccid.on',                      '',                             43, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 86, 21, 21,  0,  0, 'eventdefinition.modem.networkcarrier.on',            '',                             44, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 87, 21, 22,  0,  0, 'eventdefinition.modem.apn.on',                       '',                             45, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 88, 20, 23,  1,  2, 'eventdefinition.customerdefinedralert.on.on',        'CustomerDefinedAlertOn',       46, CURRENT_TIME, 1);  -- NEW FOR EMS 7
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 89, 20, 23,  2,  0, 'eventdefinition.customerdefinedralert.on.off',       'CustomerDefinedAlertOff',      46, CURRENT_TIME, 1);  -- NEW FOR EMS 7
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 90,  3,  3,  2,  0, 'eventdefinition.billstacker.level.cleared',          'FullCleared',                   6, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 91, 12,  3,  5,  1, 'eventdefinition.paystation.coinchanger.level.low',   'CoinChangerLow',               47, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 92, 12,  3,  7,  1, 'eventdefinition.paystation.coinchanger.level.high',  'CoinChangerHigh',              47, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 93, 12,  1,  1,  1, 'eventdefinition.paystation.coinchanger.jam.on',      'CoinChangerJam',               48, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 94, 12,  1,  2,  0, 'eventdefinition.paystation.coinchanger.jam.off',     'CoinChangerJamCleared',        48, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 99, 22,  0,  1,  0, 'eventdefinition.rfidcardreader.present.on',          'Present',                      49, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, 22, 0,  2,  0, 'eventdefinition.rfidcardreader.present.off',         'NotPresent',                   49, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101, 13, 1,  1,  3, 'eventdefinition.printer.paperjam.on',                'PaperJam',                     50, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102, 13, 1,  2,  0, 'eventdefinition.printer.paperjam.off',               'PaperJamCleared',              50, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 103, 12, 24,  1,  2, 'eventdefinition.paystation.ticketnottaken.on',      'TicketNotTaken',               51, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 104, 12, 24,  2,  0, 'eventdefinition.paystation.ticketnottaken.off',     'TicketNotTakenCleared',        51, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 105, 13, 25,  1,  3, 'eventdefinition.printer.ticketdidnotprint.on',      'TicketDidNotPrint',            52, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 106, 13, 25,  2,  0, 'eventdefinition.printer.ticketdidnotprint.off',     'TicketDidNotPrintCleared',     52, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 107, 13, 26,  1,  3, 'eventdefinition.printer.printingfailure.on',        'PrintingFailure',              53, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 108, 13, 26,  2,  0, 'eventdefinition.printer.printingfailure.off',       'PrintingFailureCleared',       53, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 109, 12, 27,  8,  1, 'eventdefinition.paystation.uid.changed',            'UIDChanged',                   54, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 110, 12, 27,  1,  3, 'eventdefinition.paystation.uid.on',                 'DuplicateSerialNumber',        55, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 111, 12, 27,  2,  0, 'eventdefinition.paystation.uid.off',                'DuplicateSerialNumberCleared', 55, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 112, 12, 28,  8,  1, 'eventdefinition.paystation.clock.changed',          'ClockChanged',                 56, CURRENT_TIME, 1);
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 113,  4, 29,  9,  3, 'eventdefinition.cardReader.violationStatus.violated', 'violationStatusViolated',    57, CURRENT_TIME, 1); 
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 114,  4, 29,  10, 3, 'eventdefinition.cardReader.violationStatus.tampered', 'violationStatusTampered',    57, CURRENT_TIME, 1); 
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 115,  4, 29,  11, 2, 'eventdefinition.cardReader.violationStatus.fault', 'violationStatusFault',          57, CURRENT_TIME, 1); 
INSERT INTO EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 116,  4, 29,  4,  0, 'eventdefinition.cardReader.violationStatus.normal', 'violationStatusOk',            57, CURRENT_TIME, 1); 

-- -----------------------------------------------------
-- Table EventDeviceType
-- -----------------------------------------------------

INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'eventdevicetype.battery',             	'Battery',			      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'eventdevicetype.battery2',              	'Battery2', 			  CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'eventdevicetype.billacceptor',          	'BillAcceptor', 	      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'eventdevicetype.billstacker',           	'BillStacker', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'eventdevicetype.cardreader',            	'CardReader', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'eventdevicetype.coinacceptor',          	'CoinAcceptor', 	      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'eventdevicetype.coinchanger',           	'CoinChanger', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'eventdevicetype.coinhopper1',          	'CoinHopper1', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'eventdevicetype.coinhopper2',          	'CoinHopper2', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'eventdevicetype.door',                   'Door', 		      	  CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'eventdevicetype.door.lower',           	'DoorLower', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'eventdevicetype.door.upper',           	'DoorUpper', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'eventdevicetype.paystation',            	'Paystation', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'eventdevicetype.printer',                'Printer', 			      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'eventdevicetype.shockalarm',            	'ShockAlarm',		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'eventdevicetype.pcm',                    'PCM', 				      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'eventdevicetype.coincanister',          	'CoinCanister', 	      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'eventdevicetype.door.maintain',          'DoorMaintenance', 		  CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'eventdevicetype.door.cashvault',        	'DoorCashVault', 		  CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'eventdevicetype.coinescrow',            	'CoinEscrow', 		      CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'eventdevicetype.customeralert', 	        'CustomerDefinedAlert',   CURRENT_TIME, 1) ;  -- CHANGED FOR EMS 7 (was Alert)
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'eventdevicetype.modem',                  'Modem', 				  CURRENT_TIME, 1) ;
INSERT INTO EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'eventdevicetype.rfidcardreader',       	'RfidCardReader', 	      CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table EventSeverityType
-- -----------------------------------------------------

INSERT INTO EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Clear',          CURRENT_TIME, 1) ;
INSERT INTO EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Minor',          CURRENT_TIME, 1) ;
INSERT INTO EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Major',          CURRENT_TIME, 1) ;
INSERT INTO EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Critical',       CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table EventStatusType
-- -----------------------------------------------------

INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Present',                  CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Jam',                      CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unable To Stack',          CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Level',                    CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Open',                     CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Low Power Shutdown',       CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Public Key Update',        CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Service Mode',             CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Upgrade',                  CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Ticket Footer Success',    CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Cutter Error',             CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Head Error',               CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Lever Disengaged',         CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Paper Status',             CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Temperature Error',        CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Voltage Error',            CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'On',                       CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Wireless Signal Strength', CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Remove',                   CURRENT_TIME, 1) ;
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Type',                     CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'CCID',                     CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Carrier',                  CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'APN',                      CURRENT_TIME, 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 'Customer Defined Alert',   CURRENT_TIME, 1) ;  -- NEW FOR EMS 7
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (24, 'Ticket Cup',				 CURRENT_TIME, 1) ;  -- NEW FOR EMS 7.1
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (25, 'Unable to Print',			 CURRENT_TIME, 1) ;  -- NEW FOR EMS 7.1
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (26, 'Printer Error',			 CURRENT_TIME, 1) ;  -- NEW FOR EMS 7.1
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (27, 'UID',   			         CURRENT_TIME, 1) ;  -- NEW FOR EMS 7.1
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (28, 'Clock',   			     CURRENT_TIME, 1) ;  -- NEW FOR EMS 7.1
INSERT INTO EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (29, 'Card Reader Status',      CURRENT_TIME, 1);
-- -----------------------------------------------------
-- Table EventException
-- -----------------------------------------------------

INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  7, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  8, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 19, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 19, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  7, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  8, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 19, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  6, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  7, CURRENT_TIME, 1);
INSERT INTO EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  8, CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ExtensibleRateType
-- -----------------------------------------------------

INSERT INTO ExtensibleRateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Hourly',       CURRENT_TIME, 1) ;
INSERT INTO ExtensibleRateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Special',      CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table LegacyPaymentType
-- -----------------------------------------------------

INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A'        , 'Not Applicable'      , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Cash'       , 'Cash Only'           , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Credit Card', 'Credit Card Only'    , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Patroller'  , 'Patroller Card Only' , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Smart Card' , 'Smart Card Only'     , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cash/CC'    , 'Cash and Credit Card', CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Cash/SC'    , 'Cash and Smart Card' , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Passcard' , 'Passcard Only'     , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'No Data'    , 'Information Deleted' , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Cash/Passcard' , 'Cash and Passcard' , CURRENT_TIME, 1) ;
INSERT INTO LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unknown'    , 'Unknown Payment Type', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table LegacyTransactionType
-- -----------------------------------------------------

INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown'    , 'Unknown Transaction Type'        , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'AddTime'    , 'Permit Extension'                , 1, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Regular'    , 'Permit'                          , 1, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Citation'   , 'Citation Payment'                , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'SCRecharge' , 'Smart Card Recharge'             , 0, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cancelled'  , 'Cancelled Purchase'              , 1, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Test'       , 'Test Purchase'                   , 0, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'No Data'    , 'Information Deleted'             , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Deposit'    , 'Patroller Deposit'               , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Replenish'  , 'Replenish Transaction'           , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Monthly'    , 'Monthly Permit'                  , 1, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Hopper1Add' , 'Replenish Hopper 1'              , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Hopper2Add' , 'Replenish Hopper 2'              , 0, 0, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'AddTime EbP', 'Extend by Phone Permit Extension', 1, 1, CURRENT_TIME, 1) ;
INSERT INTO LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Regular EbP', 'Extend by Phone Permit'          , 1, 1, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table LoginResultType
-- -----------------------------------------------------

INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Successful Login',                   CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Invalid Credentials',                CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Invalid Credentials and Locked Out', CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'User Logout',                        CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Timeout Logout',                     CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'User Switched In',                   CURRENT_TIME, 1) ;
INSERT INTO LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'User Switched Out',                  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table ModemType
-- -----------------------------------------------------

INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'No Modem',      CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Unknown',       CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'CDMA',          CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Ethernet',      CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'GSM',           CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'WiFi',          CURRENT_TIME, 1) ;
INSERT INTO ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'CellularModem', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table ParkingPermissionType
-- -----------------------------------------------------

INSERT INTO ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Time Limited',    CURRENT_TIME, 1) ;
INSERT INTO ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unlimited',       CURRENT_TIME, 1) ;
INSERT INTO ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Restricted',      CURRENT_TIME, 1) ;
INSERT INTO ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Special',         CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table PaymentType
-- -----------------------------------------------------

-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A', 		 'Not Applicable',					 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Cash', 			 'Cash Only',						 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'CC (Swipe)',		 'Credit Card Swipe Only',			 CURRENT_TIME, 1) ;
-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Patroller', 	 'Patroller Card Only',				 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Smart Card', 	 'Smart Card Only',					 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cash/CC (Swipe)', 'Cash and Credit Card Swipe',		 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Cash/SC',		 'Cash and Smart Card',				 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Passcard', 		 'Passcard Only'     , 				 CURRENT_TIME, 1) ;
-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'No Data', 	 'Information Deleted' , 			 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Cash/Passcard',	 'Cash and Passcard' , 				 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unknown'	,		 'Unknown Payment Type', 			 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'CC (Tap)',		 'Credit Card Contactless Only',	 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Cash/CC (Tap)',	 'Cash and Credit Card Contactless', CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'CC (Chip)',		 'Credit Card Chip Only',			 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Cash/CC (Chip)',	 'Cash and Credit Card Chip',		 CURRENT_TIME, 1) ;
INSERT INTO PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'CC (External)',	 'Credit Card External',			 CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table PaystationType
-- -----------------------------------------------------

INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  0, 'Other',       CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'V1 LUKE',     CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'SHELBY',      CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Radius LUKE', CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'LUKE B', 	 CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'LUKE II',     CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Test/Demo',   CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Virtual',     CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Verrus',      CURRENT_TIME, 1) ;
INSERT INTO PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (255, 'Intellepay',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table ProcessorTransactionType
-- -----------------------------------------------------

INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A'             , 'N/A'             , 'N/A'                               , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Auth PlaceHolder', 'PreAuth'         , 'Authorized but not yet settled'    , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Real-Time'       , 'Real-Time'       , 'Settled with no associated refunds', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Batch'           , 'Batched'         , 'Charged with no associated refunds', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Refund'          , 'Refunded'        , 'Refund of a charge or settlement'  , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Reversal'        , ''                , ''                                  , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Real-Time'       , 'Real-Time'       , 'Settled with associated refunds'   , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Batch'           , 'Batched'         , 'Charged with associated refunds'   , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Uncloseable'     , 'Uncloseable'     , 'PreAuth expired, cannot settle'    , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, '3rd Party'       , '3rd Party'       , 'Charged externally with no refunds', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Batch'           , 'Batched'         , 'Charged EMS retry with no refunds' , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Store & Fwd'     , 'Store & Fwd'     , 'Charged SF with no refunds'        , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Batch'           , 'Batched'         , 'Charged EMS retry with refunds'    , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Store & Fwd'     , 'Store & Fwd'     , 'Charged SF with refunds'           , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Encryption Error', 'Encryption Error', 'Invalid encryption'                , 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Real-Time'       , 'Extend by Phone' , 'Charged with no associated refunds', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Refund'          , 'Extend by Phone' , 'Refund of a charge or settlement'  , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Real-Time'       , 'Extend by Phone' , 'Charged with associated refunds'   , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'Recoverable'     , 'Recoverable'     , 'PreAuth expired but is recoverable', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Real-Time'       , 'Single Trx'      , 'Charged with no associated refunds', 0, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'Refund'          , 'Single Trx'      , 'Refund of a charge'                , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 'Real-Time'       , 'Single Trx'      , 'Charged with associated refunds'   , 1, CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (24, 'Expired-Refund'  , 'Expired Refund'  , 'Expired Refund'                    , 0 ,CURRENT_TIME, 1) ;
INSERT INTO ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 'Cancel'          , 'Cancel'          , 'Cancelled'                         , 0, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table Permission
-- -----------------------------------------------------

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, NULL, 'Reports Management',                      		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101, 100,  'Manage Reports',                      	  		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102, 100,  'Search for Transactions in Permit Lookup', 	NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 200, NULL, 'Alerts Management',        		            NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 201,  200, 'View Current Alerts',           		        NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 202,  200, 'Manage User Defined Alerts',                   NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 300, NULL, 'Collections Management',                       NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 301,  300, 'View Collection Status',                       NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 400, NULL, 'Card Management',                              NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 401,  400, 'Issue Refunds',                                NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 402,  400, 'View Banned Cards',     		                NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 403,  400, 'Manage Banned Cards'	,       	            NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 404,  400, 'Process Card Charges From BOSS',               NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 405,  400, 'Setup Credit Card Processing',                 NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 406,  400, 'View Campus Cards',                            NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 407,  400, 'Manage Campus Cards',                          NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 408,  400, 'Configure Campus Cards',          	      	    NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 500, NULL, 'Customer Account Management',                  NULL, 1, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 501,  500, 'View Passcards',                               NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 502,  500, 'Manage Passcards',                             NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 503,  500, 'Configure Passcards',                          NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 504,  500, 'View Coupons',                                 NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 505,  500, 'Manage Coupons',                               NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 506,  500, 'View Customer Accounts',                       NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 507,  500, 'Manage Customer Accounts',                     NULL, 0, CURRENT_TIME, 1) ;	
	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 600, NULL, 'Extend-By-Phone',                              NULL, 1, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 601,  600, 'View Rates & Policies',                        NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 602,  600, 'Manage Rates & Policies',                      NULL, 0, CURRENT_TIME, 1) ;	

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 700, NULL, 'User Account Management',                      NULL, 1, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 701,  700, 'View Users & Roles',                           NULL, 0, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 702,  700, 'Manage Users & Roles',                         NULL, 0, CURRENT_TIME, 1) ;	
	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 800, NULL, 'Pay Station Configuration Settings',           NULL, 1, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 801,  800, 'Upload Configurations from BOSS',              NULL, 0, CURRENT_TIME, 1) ;	
	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 900, NULL, 'System Activity',                              NULL, 1, CURRENT_TIME, 1) ;	
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 901,  900, 'View System Activities',                       NULL, 0, CURRENT_TIME, 1) ;	

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1000, NULL, 'Pay Station Management',                  		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1001, 1000, 'View Locations',                          		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1002, 1000, 'Manage Locations',                        		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1003, 1000, 'View Pay Stations',                            NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1004, 1000, 'Manage Pay Stations',                          NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1005, 1000, 'View Routes',                                  NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1006, 1000, 'Manage Routes',                                NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1007, 1000, 'System Settings',                              NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1008, 1000, 'View Pay Station Settings',               		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1009, 1000, 'Manage Pay Station Settings',             		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1010, 1000, 'Manage Pay Station Placement',            		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1011, 1000, 'Schedule Pay Station Settings Update',         NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1050, NULL, 'Remote Access',                           		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1051, 1050, 'Access Digital API',                      		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1052, 1050, 'PDA Access',                              		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1100, NULL, 'DPT Pay Station Administration',         		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1101, 1100, 'View Pay Station',                       		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1102, 1100, 'Manage Pay Station',                      		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1103, 1100, 'Move Pay Station',                       		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1104, 1100, 'Set Billing Parameters',                  		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1105, 1100, 'Create Pay Stations',                     		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1106, 1100, 'Manage Pay Station Placement',            		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1200, NULL, 'DPT Customer Administration',            		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1201, 1200, 'Create New Customer',                     		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1202, 1200, 'Edit Existing Customer',                  		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1203, 1200, 'View Customers',                          		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1300, NULL, 'System Notification Administration',      		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1301, 1300, 'View Notifications',                      		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1302, 1300, 'Manage Notifications',                    		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1400, NULL, 'Digital API Administration',              		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1401, 1400, 'View Licenses',                           		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1402, 1400, 'Manage Licenses',                         		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1500, NULL, 'Server Administration',                   		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1501, 1500, 'View Server Status',                      		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1502, 1500, 'Server Operations',                       		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1600, NULL, 'Dashboard Management',                    		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1601, 1600, 'View Dashboard',                   		 	NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1602, 1600, 'Manage Dashboard',                   	  		NULL, 1, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1700, NULL, 'Mobile App License Admin', 				  	NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1701, 1700, 'View Mobile Licenses',					 		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1702, 1700, 'Manage Mobile Licenses',				  		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1800, NULL, 'Mobile Device Administration', 			  	NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1801, 1800, 'View Mobile Devices',					  		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1802, 1800, 'Manage Mobile Devices',				      	NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1900, NULL, 'Signing Server Access', 			  	  		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1901, 1900, 'Generate Signature',					  		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2000, NULL, 'Online Rate Configuration', 			  		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2001, 2000, 'View Online Rates',						  	NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2002, 2000, 'Manage Online Rates',					 		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2100, NULL, 'Customer Migration Administration', 	  		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2101, 2100, 'View Customer Migration Status',		  		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2102, 2100, 'Manage Customer Migration Status',		  		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2200, NULL, 'Flex WS Credentials',                     		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2201, 2200, 'View Flex WS Credentials',                		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2202, 2200, 'Manage Flex WS Credentials',              		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2300, NULL, 'DPT Flex WS Credentials',                 		NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2301, 2300, 'DPT View Flex WS Credentials',            		NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2302, 2300, 'DPT Manage Flex WS Credentials',          		NULL, 0, CURRENT_TIME, 1) ;

INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2400, NULL, 'DPT AutoCount Integration',                 	NULL, 1, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2401, 2400, 'DPT View AutoCount Integration', 	     	    NULL, 0, CURRENT_TIME, 1) ;
INSERT INTO Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2402, 2400, 'DPT Manage AutoCount Integration',	            NULL, 0, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table PermitIssueType
-- -----------------------------------------------------

INSERT INTO PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',              CURRENT_TIME, 1) ;
INSERT INTO PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Pay & Display',    CURRENT_TIME, 1) ;
INSERT INTO PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Pay by Plate',     CURRENT_TIME, 1) ;
INSERT INTO PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Pay by Space',     CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table PermitType
-- -----------------------------------------------------

INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',                     CURRENT_TIME, 1) ;
INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Regular',                 CURRENT_TIME, 1) ;
INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Addtime',                 CURRENT_TIME, 1) ;
INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Regular EbP',             CURRENT_TIME, 1) ;
INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Addtime EbP',             CURRENT_TIME, 1) ;
INSERT INTO PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Monthly',                 CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table POSDateType
-- -----------------------------------------------------

-- These rows are current state: inserting one of these into table POSDate results in an update to table POSStatus
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'Is Provisioned',                     CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'Is Locked',                          CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Is Decommissioned',                  CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'Is Deleted',                         CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'Is Visible',                         CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'Is Billable Monthly on Activation',  CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'Is Digital Connect',                 CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Is Test Activated',                  CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Is Activated',                       CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Is Billable Monthly (for EMS)',      CURRENT_TIME, 1) ;

-- These rows are informational only: inserting one of these into table POSDate does NOT result in an update to table POSStatus
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (201, 'Was Moved To',                       CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (202, 'Was Moved Away',                     CURRENT_TIME, 1) ;
INSERT INTO POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (203, 'Comment Added',                      CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table Processor
-- -----------------------------------------------------

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 1, 'processor.concord', 'https://prod.dw.us.fdcnet.biz/efsnet2.dll', 'https://stg.dw.us.fdcnet.biz/efsnet2.dll', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 2, 'processor.firstDataNashville', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/nocportal/SRS.do', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 3, 'processor.heartland', 'https://sslprod.secureexchange.net:54411', 'https://sslhps.test.secureexchange.net:15031', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 4, 'processor.link2gov', 'https://gate.link2gov.com/api/', 'https://ca.link2gov.com/api/', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 5, 'processor.parcxmart', 'http://www.parcxmart.net/pxtaccept/pxtaccept.dll', 'http://www.parcxmart.net/pxtaccept/pxtaccept.dll', 1, 0, 0, 0, 1, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 6, 'processor.alliance', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/staging_expresso/SRS.do', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 7, 'processor.paymentech', 'https://netconnect1.paymentech.net/NetConnect/controller,https://netconnect2.paymentech.net/NetConnect/controller', 'https://netconnectvar1.paymentech.net/NetConnect/controller,https://netconnectvar2.paymentech.net/NetConnect/controller', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 8, 'processor.paradata', '', '', 1, 1, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 9, 'processor.moneris', 'www3.moneris.com', 'esqa.moneris.com', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 10, 'processor.firstHorizon', 'https://gateway-bmd.nxt.com/FH-Inet/process_transaction.cgi', 'https://gateway-bmd.nxt.com:4443/FH-Inet/process_transaction.cgi', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 11, 'processor.authorize.net', 'https://cardpresent.authorize.net/gateway/transact.dll', 'https://test.authorize.net/gateway/transact.dll', 1, 1, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 12, 'processor.datawire', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/staging_expresso/SRS.do', 1, 0, 0, 0, 1, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 13, 'processor.blackboard', '', '66.210.59.124,69.26.224.124', 1, 0, 0, 1, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 14, 'processor.totalcard', '', 'as400.ucen.ucsb.edu:3030', 1, 0, 0, 1, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 15, 'processor.nuvision', '', '74.92.119.157', 1, 0, 0, 1, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 16, 'processor.elavon', 'https://www.myvirtualmerchant.com/VirtualMerchant/processxml.do', 'https://demo.myvirtualmerchant.com/VirtualMerchantDemo/processxml.do', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 17, 'processor.cbord.csgold', '', '', 1, 0, 0, 1, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 18, 'processor.cbord.odyssey', '', '', 1, 0, 0, 1, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 19, 'processor.elavon.viaconex', 'https://webgate.viaconex.com/cgi-bin/encompass4.cgi', 'https://testgate.viaconex.com/cgi-bin/encompass4.cgi', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 20, 'processor.creditcall', '', '', 1, 0, 1, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 21, 'processor.tdmerchant', '', '', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

INSERT INTO Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,IsLink,IsPaused,VERSION,LastModifiedGMT,LastModifiedByUserId,isActive) VALUES ( 22, 'processor.heartland.spplus', 'https://sslprod.secureexchange.net:54411', 'https://sslhps.test.secureexchange.net:15031', 1, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIME, 1,1) ;

-- -----------------------------------------------------
-- Table QuarterHour
-- -----------------------------------------------------

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 0,  0, 0, '00:00 AM', '00:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 1,  0, 1, '00:15 AM', '00:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 2,  0, 2, '00:30 AM', '00:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 3,  0, 3, '00:45 AM', '00:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 4,  1, 0, '01:00 AM', '01:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 5,  1, 1, '01:15 AM', '01:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 6,  1, 2, '01:30 AM', '01:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 7,  1, 3, '01:45 AM', '01:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 8,  2, 0, '02:00 AM', '02:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 9,  2, 1, '02:15 AM', '02:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (10,  2, 2, '02:30 AM', '02:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (11,  2, 3, '02:45 AM', '02:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (12,  3, 0, '03:00 AM', '03:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (13,  3, 1, '03:15 AM', '03:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (14,  3, 2, '03:30 AM', '03:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (15,  3, 3, '03:45 AM', '03:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (16,  4, 0, '04:00 AM', '04:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (17,  4, 1, '04:15 AM', '04:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (18,  4, 2, '04:30 AM', '04:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (19,  4, 3, '04:45 AM', '04:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (20,  5, 0, '05:00 AM', '05:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (21,  5, 1, '05:15 AM', '05:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (22,  5, 2, '05:30 AM', '05:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (23,  5, 3, '05:45 AM', '05:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (24,  6, 0, '06:00 AM', '06:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (25,  6, 1, '06:15 AM', '06:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (26,  6, 2, '06:30 AM', '06:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (27,  6, 3, '06:45 AM', '06:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (28,  7, 0, '07:00 AM', '07:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (29,  7, 1, '07:15 AM', '07:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (30,  7, 2, '07:30 AM', '07:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (31,  7, 3, '07:45 AM', '07:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (32,  8, 0, '08:00 AM', '08:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (33,  8, 1, '08:15 AM', '08:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (34,  8, 2, '08:30 AM', '08:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (35,  8, 3, '08:45 AM', '08:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (36,  9, 0, '09:00 AM', '09:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (37,  9, 1, '09:15 AM', '09:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (38,  9, 2, '09:30 AM', '09:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (39,  9, 3, '09:45 AM', '09:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (40, 10, 0, '10:00 AM', '10:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (41, 10, 1, '10:15 AM', '10:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (42, 10, 2, '10:30 AM', '10:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (43, 10, 3, '10:45 AM', '10:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (44, 11, 0, '11:00 AM', '11:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (45, 11, 1, '11:15 AM', '11:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (46, 11, 2, '11:30 AM', '11:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (47, 11, 3, '11:45 AM', '11:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (48, 12, 0, '12:00 PM', '12:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (49, 12, 1, '12:15 PM', '12:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (50, 12, 2, '12:30 PM', '12:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (51, 12, 3, '12:45 PM', '12:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (52, 13, 0, '01:00 PM', '13:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (53, 13, 1, '01:15 PM', '13:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (54, 13, 2, '01:30 PM', '13:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (55, 13, 3, '01:45 PM', '13:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (56, 14, 0, '02:00 PM', '14:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (57, 14, 1, '02:15 PM', '14:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (58, 14, 2, '02:30 PM', '14:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (59, 14, 3, '02:45 PM', '14:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (60, 15, 0, '03:00 PM', '15:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (61, 15, 1, '03:15 PM', '15:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (62, 15, 2, '03:30 PM', '15:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (63, 15, 3, '03:45 PM', '15:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (64, 16, 0, '04:00 PM', '16:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (65, 16, 1, '04:15 PM', '16:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (66, 16, 2, '04:30 PM', '16:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (67, 16, 3, '04:45 PM', '16:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (68, 17, 0, '05:00 PM', '17:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (69, 17, 1, '05:15 PM', '17:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (70, 17, 2, '05:30 PM', '17:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (71, 17, 3, '05:45 PM', '17:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (72, 18, 0, '06:00 PM', '18:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (73, 18, 1, '06:15 PM', '18:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (74, 18, 2, '06:30 PM', '18:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (75, 18, 3, '06:45 PM', '18:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (76, 19, 0, '07:00 PM', '19:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (77, 19, 1, '07:15 PM', '19:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (78, 19, 2, '07:30 PM', '19:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (79, 19, 3, '07:45 PM', '19:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (80, 20, 0, '08:00 PM', '20:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (81, 20, 1, '08:15 PM', '20:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (82, 20, 2, '08:30 PM', '20:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (83, 20, 3, '08:45 PM', '20:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (84, 21, 0, '09:00 PM', '21:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (85, 21, 1, '09:15 PM', '21:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (86, 21, 2, '09:30 PM', '21:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (87, 21, 3, '09:45 PM', '21:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (88, 22, 0, '10:00 PM', '22:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (89, 22, 1, '10:15 PM', '22:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (90, 22, 2, '10:30 PM', '22:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (91, 22, 3, '10:45 PM', '22:45', 1, CURRENT_TIME);

INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (92, 23, 0, '11:00 PM', '23:00', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (93, 23, 1, '11:15 PM', '23:15', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (94, 23, 2, '11:30 PM', '23:30', 1, CURRENT_TIME);
INSERT INTO QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (95, 23, 3, '11:45 PM', '23:45', 1, CURRENT_TIME);

-- -----------------------------------------------------
-- Table RestAccountType
-- -----------------------------------------------------

INSERT INTO RestAccountType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Transaction Push',   CURRENT_TIME, 1) ;
INSERT INTO RestAccountType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Coupon Push',   	   CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table RevenueType
-- -----------------------------------------------------

INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, NULL, 1, 'Total'      , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2,    1, 2, 'Cash'       , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3,    1, 2, 'Credit Card'       , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4,    2, 3, 'Coin'       , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5,    2, 3, 'Bill'       , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6,    2, 3, 'Legacy Cash', CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7,    3, 3, 'Credit Card Swipe', CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,   1, 2, 'OtherCards' , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,   3, 3, 'Credit Card Tap' , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,   3, 3, 'Credit Card Chip' , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8,    10, 3, 'Smart Card' , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9,    10, 3, 'Passcard' , CURRENT_TIME, 1) ;
INSERT INTO RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,    3, 3, 'CC External' , CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table RouteType
-- -----------------------------------------------------

INSERT INTO RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Collections',   23, CURRENT_TIME, 1) ;
INSERT INTO RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Maintenance',  975, CURRENT_TIME, 1) ;
INSERT INTO RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Other',       2494, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table SubscriptionType
-- -----------------------------------------------------

INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  100, NULL, 'Standard Reports',                    1, 0,  1, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  200, NULL, 'Alerts',                              1, 0,  2, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  300, NULL, 'Real-Time Credit Card Processing',    1, 0,  3, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  400, NULL, 'Batch Credit Card Processing',        1, 0,  4, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  500, NULL, 'Real-Time Campus Card Processing',    1, 0,  5, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  600, NULL, 'Coupons',                             1, 0,  8, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  700, NULL, 'Passcards',                           1, 0,  6, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  800, NULL, 'Smart Cards',                         1, 0,  7, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (  900, NULL, 'Extend-By-Phone',                     1, 0,  9, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1000, NULL, 'Digital API: Read',                   1, 0, 13, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1100, NULL, 'Digital API: Write',                  1, 0, 14, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1200, NULL, '3rd Party Pay-By-Cell Integration',   1, 0, 16, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1300, NULL, 'Mobile: Digital Collect',             1, 0, 10, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1600, NULL, 'Online Pay Station Configuration',    1, 0, 17, CURRENT_TIME, 1);
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1700, NULL, 'FLEX Integration',                    1, 0, 11, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1800, NULL, 'AutoCount Integration',               1, 0, 12, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1900, NULL, 'Digital API: XChange',                1, 1, 15, CURRENT_TIME, 1) ;
INSERT INTO SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2000, NULL, 'Online Rate Configuration',           1, 0, 18, CURRENT_TIME, 1) ;
-- -----------------------------------------------------
-- Table SmsMessageType
-- -----------------------------------------------------

INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Transaction received from pay station',                                CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Expiry alert sent, option to extend',                                  CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Expiry alert sent, no option to extend',                               CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'User response received',                                               CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Invalid user response received',                                       CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Failure message sent to user',                                         CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Extension confirmation sent',                                          CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'SMS send failure',                                                     CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Credit card declined',                                                 CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unable to process credit card',                                        CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Time auto-extended',                                                   CURRENT_TIME, 1) ;
INSERT INTO SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Message from phone number not associated with active parking session', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table Timezone
-- 
-- Q: Where is Name coming from?
-- -----------------------------------------------------

INSERT INTO TimeZone (Id, Code, Name, CodeDST, NameDST, OffsetFromGMT, OffsetFromDST, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'PST', 'US/Pacific', 'PDT', 'Pacific Daylight Time', '-08:00', '-07:00', CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table TransactionType
-- -----------------------------------------------------

INSERT INTO TransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) SELECT Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId FROM   LegacyTransactionType WHERE  Id IN (1,2,4,5,6,12,16,17);

INSERT INTO TransactionType (Id,Name,Description,IsRevenue,IsPurchase,LastModifiedGMT,LastModifiedByUserId) VALUES (0,'Unknown','Unknown',1,1,CURRENT_TIME,1);

-- -----------------------------------------------------
-- Table UserStatusType
-- -----------------------------------------------------

INSERT INTO UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', CURRENT_TIME, 1) ;
INSERT INTO UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  CURRENT_TIME, 1) ;
INSERT INTO UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Deleted',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WebServiceEndPointType
-- -----------------------------------------------------

INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'StallInfo',        0, CURRENT_TIME, 1) ;
INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'PayStationInfo',   0, CURRENT_TIME, 1) ;
INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'TransactionInfo',  0, CURRENT_TIME, 1) ;
INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'PlateInfo',        0, CURRENT_TIME, 1) ;
INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'AuditInfo',	       0, CURRENT_TIME, 1) ;
INSERT INTO WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'TransactionData',  1, CURRENT_TIME, 1) ; 

-- -----------------------------------------------------
-- Table WidgetSubsetMember
-- -----------------------------------------------------

-- Default List Widgets
INSERT INTO     WidgetSubsetMember (WidgetId, WidgetTierTypeId, MemberId, MemberName,VERSION, LastModifiedGmt) SELECT     Id, 201, 3, 'Card', 0, CURRENT_TIME FROM       Widget WHERE (Name = 'Total Revenue by Month'  OR Name = 'Total Purchases by Month' OR Name = 'Total Purchases by Rev Type'  OR Name = 'Total Purchases by Day' OR Name = 'Total Purchases by Hour'  OR Name = 'Total Revenue by Revenue Type' OR Name = 'Avg Price by Revenue Type');

INSERT INTO     WidgetSubsetMember (WidgetId, WidgetTierTypeId, MemberId, MemberName,VERSION, LastModifiedGmt) SELECT     Id, 201, 2, 'Cash', 0, CURRENT_TIME FROM       Widget WHERE (Name = 'Total Revenue by Month' OR Name = 'Total Purchases by Month' OR Name = 'Total Purchases by Rev Type' OR Name = 'Total Purchases by Day'  OR Name = 'Total Purchases by Hour' OR Name = 'Total Revenue by Revenue Type' OR Name = 'Avg Price by Revenue Type');

-- -----------------------------------------------------
-- Table WidgetChartType
-- -----------------------------------------------------
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'List',         3882, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Pie',          1034, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Line',         8395, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Bar',          9923, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Column',       1184, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Area',         6783, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Single Value', 3850, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'Map',          4956, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (9, 'Heat Map',     4957, CURRENT_TIME, 1) ;
INSERT INTO WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (10,'Occupancy Map',4958, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WidgetFilterType
-- -----------------------------------------------------
INSERT INTO WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'All',        2850, CURRENT_TIME, 1) ;
INSERT INTO WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Subset',     2395, CURRENT_TIME, 1) ;
INSERT INTO WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Top',        5949, CURRENT_TIME, 1) ;
INSERT INTO WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Bottom',     4558, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WidgetLimitType
-- -----------------------------------------------------
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0,  0,           2295, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1,  5,           3844, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 10,           9482, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 15,           3347, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 20,           9386, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 25,           9385, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 50,           2847, CURRENT_TIME, 1) ;
INSERT INTO WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 100,          9387, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WidgetListType
-- -----------------------------------------------------
INSERT INTO WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Master List',       6830, CURRENT_TIME, 1) ;
INSERT INTO WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Default Dashboard', 4030, CURRENT_TIME, 1) ;
INSERT INTO WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'User Dashboard',    4069, CURRENT_TIME, 1) ;
INSERT INTO WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Parent Master List',       5830, CURRENT_TIME, 1) ;
INSERT INTO WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Default Parent Dashboard', 3030, CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table WidgetMetricType
-- -----------------------------------------------------
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Revenue',        7830, 'Description Revenue',        CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Collections',    4782, 'Description Collections',    CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Settled Card',   3820, 'Description Settled Card',   CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Purchases',      3959, 'Description Purchases',      CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Paid Occupancy', 4960, 'Description Paid Occupancy', CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Turnover',       3905, 'Description Turnover',       CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Utilization',    3945, 'Description Utilization',    CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Duration',       1394, 'Description Duration',       CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Active Alerts',  5896, 'Description Active Alerts',  CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Alert Status',   9947, 'Description Alert Status',   CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Map',            4823, 'Description Map',            CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Average Price per Permit',  	 4829, 'Description Average Price',  		 CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Average Revenue per Space',	 4899, 'Description Average Revenue',		 CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Average Duration per Permit', 4875, 'Description Average Duration',		 CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Card Processing - Revenue', 	 9952, 'Description Card Processing - Revenue', CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'Card Processing - Purchases', 9953, 'Description Card Processing - Purchases',  CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Citations', 		9321, 'Description Citations',  	1700, CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Citation Map', 	9322, 'Description Citation Map',  	1700, CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Counted Occupancy',      9368, 'Description Counted Occupancy', 	1800, CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'Counted/Paid Occupancy', 9369, 'Description Counted vs Paid Occupancy',  	1800, CURRENT_TIME, 1) ;
INSERT INTO WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Occupancy Map', 4953, 'Description Occupancy Map',   CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WidgetRangeType
-- -----------------------------------------------------
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',             4583, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Now',             2958, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Today',           5930, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Yesterday',       3344, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Last 24 Hours',   8506, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Last 7 Days',     3850, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Last 30 Days',    3951, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Last 12 Months',  9939, CURRENT_TIME, 1) ;
-- INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'This Week',       2945, CURRENT_TIME, 1) ;
-- INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'This Month',      5942, CURRENT_TIME, 1) ;
-- INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'This Year',       3845, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Last Week',       3854, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Last Month',      2894, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Last Year',       1892, CURRENT_TIME, 1) ;
INSERT INTO WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Year to Date',    1893, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table WidgetTierType
-- -----------------------------------------------------
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  0, 0, 'N/A',                 8406, CURRENT_TIME, 1) ;

INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 1, 'Hour',                3850, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 1, 'Day',                 3958, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 1, 'Month',               1040, CURRENT_TIME, 1) ;

INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 2, 'All Organizations',   1967, CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 2, 'Parent Organization', 9477, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 2, 'Organization',        4758, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (104, 2, 'Parent Location',     8573, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (105, 2, 'Location',            2957, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (106, 2, 'Route',               9938, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (107, 2, 'Pay Station',         7572, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (108, 2, 'Rate',                2947, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (109, 2, 'Merchant Account',    2968, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (110, 3, 'Citation Type',       9323, CURRENT_TIME, 1) ;

INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (201, 3, 'Revenue Type',        9583, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (202, 3, 'Transaction Type',    8375, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (203, 3, 'Collection Type',     8257, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (204, 3, 'Alert Type',          8258, CURRENT_TIME, 1) ;
INSERT INTO WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (205, 3, 'Card Processing Method Type',     9951, CURRENT_TIME, 1) ;

-- ---------------------- These may be used in the future --------------------------
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 1, '15 Minutes',          CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1, 'Week',                CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (204, 3, 'Permit Type',         CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (205, 3, 'Permit Issue Type',   CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (206, 3, 'Paystation Type',     CURRENT_TIME, 1) ;
-- INSERT INTO WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (207, 3, 'Point Of Sale Type',  CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table `Section`
-- -----------------------------------------------------

INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (1, 1, 1, 5, 1, 0, CURRENT_TIME) ; -- Finance
INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (2, 1, 1, 3, 2, 0, CURRENT_TIME) ;
INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (3, 1, 2, 3, 1, 0, CURRENT_TIME) ; -- Operations
INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (4, 1, 3, 1, 1, 0, CURRENT_TIME) ; -- Map
INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (5, 1, 4, 3, 1, 0, CURRENT_TIME) ; -- Parket Metrics
INSERT INTO Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (6, 1, 5, 3, 1, 0, CURRENT_TIME) ; -- Card Processing

-- -----------------------------------------------------
-- Table `Tab`
-- -----------------------------------------------------

INSERT INTO Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (1, 1, 'Finance',       		1, 0, CURRENT_TIME) ;
INSERT INTO Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (2, 1, 'Operations',    		2, 0, CURRENT_TIME) ;
INSERT INTO Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (3, 1, 'Map',           		3, 0, CURRENT_TIME) ;
INSERT INTO Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (4, 1, 'Parker Metrics',		4, 0, CURRENT_TIME) ;
INSERT INTO Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (5, 1, 'Card Processing',	5, 0, CURRENT_TIME) ;

-- -----------------------------------------------------
-- Table `Widget`
-- -----------------------------------------------------

-- Finance
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,1,1,6,3,105,0,0,6,0,0,1,'Total Revenue by Day','Total revenue for last 30 days grouped by day',0,0,700000,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,1,1,6,0,0,0,0,7,0,1,1,'Total Revenue Last 30 Days','The total revenue for last 30 days',0,0,0,0,0,0,0,0,0,CURRENT_TIME); 
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,1,1,5,0,0,0,0,7,0,1,2,'Total Revenue Last 7 Days','The total revenue for last 7 days',0,0,0,0,0,0,0,0,0,CURRENT_TIME);

INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,1,6,105,0,0,0,2,0,0,1,'Total Revenue by Location','Total revenue for the last 30 days grouped by location',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,1,6,105,0,0,2,1,1,0,2,'Top 5 Locations Last 30 Days','The top 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,1,6,105,0,0,0,5,0,1,1,'Total Revenue by Location','Total revenue for this month grouped by location',0,0,0,0,0,0,1,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,1,6,105,0,0,3,1,1,1,2,'Bottom 5 Locations - 30 Day','The bottom 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,4,6,3,108,0,1,5,0,2,1,'Purchases by Rate',NULL,0,0,0,0,1,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,2,1,6,107,0,0,2,1,1,2,2,'Top 5 Pay Stations - 30 Day','The top 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Operations
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,5,1,105,0,0,0,5,0,0,1,'Paid Occupancy by Location',NULL,0,0,8500,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,5,4,2,105,0,0,3,0,0,2,'Paid Occupancy by Hour',NULL,0,0,8500,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,4,4,2,105,0,0,3,0,1,1,'Purchases by Hour',NULL,0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,6,6,3,0,0,0,3,0,1,2,'Space Turnover by Day',NULL,0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,9,1,106,0,0,0,5,0,2,1,'Pay Stations with Alerts',NULL,0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,3,7,3,105,0,0,0,5,0,2,2,'Utilization by Location',NULL,0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Map
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,2,4,11,1,105,0,0,0,8,0,0,1,'Map',NULL,0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Parker Metrics
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,3,5,13,6,3,105,0,0,3,0,0,1,'Avg Revenue by Day','Average revenue per space for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT) VALUES (1,3,5,12,5,3,105,0,0,3,0,1,1,'Avg Price by Day','Average price per permit for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (1,3,5,14,5,3,0,0,0,3,0,2,1,'Avg Duration by Day','Average duration per space for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Default Dashboard
-- Card Processing - Purchases
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		15,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		15,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Revenue by Method - 24h','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
-- Card Processing - Revenue
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		16,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 2,					6,		16,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Purchases by Method - 24h','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);


-- Parent - Finance 
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	1,	1,	6,	3,		103,	0,	0,	6,	0,	0,	1,	'Total Revenue by Day', 			'Total revenue for the last 30 days grouped by day',				0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	1,	1,	6,	0,		0,		0,	0,	7,	0,	1,	1,	'Total Revenue Last 30 Days',		'The total revenue for the the last 30 days ',						0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	1,	1,	5,	0,		0,		0,	0,	7,	0,	1,	2,	'Total Revenue Last 7 Days',		'The total revenue for the last 7 days',							0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	0,	2,	0,	0,	1,	'Total Revenue by Organization',	'Total revenue for the last 30 days grouped by organization',		0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	2,	1,	1,	0,	2,	'Top 5 - 30 Days',			'The top 5 organizations based on total revenue for the last 30 days',0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	3,	1,	1,	0,	3,	'Bottom 5 - 30 Days',		'The bottom 5 organizations based on total revenue for the last 30 days',0,1,0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	0,	5,	0,	1,	1,	'Total Revenue by Organization',	'Total revenue for the last 30 days grouped by organization',		0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	105,	0,		0,	2,	1,	1,	1,	2,	'Top 5 Locations - 30 Days',			'The top 5 locations based on total revenue for the last 30 days',0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	6,	105,	0,		0,	3,	1,	1,	1,	3,	'Bottom 5 Locations - 30 Days',		'The bottom 5 locations based on total revenue for the last 30 days',0,1,0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	7,	5,		201,	0,	1,	5,	0,	2,	1,	'Total Revenue by Month',			'Total revenue for the last 12 months grouped by month',			0,	1,	0,	0,	1,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	2,	1,	4,	2,		0,		0,	0,	5,	0,	2,	2,	'Total Revenue by Hour',			'Total revenue for the last 24 hours grouped by hour',				0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);

-- Operations - Parent
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	7,	5,		201,	0,	1,	5,	0,	0,	1,	'Total Purchases by Month',			'Total purchases for the last 12 months grouped by month',			0,	1,	0,	0,	1,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	6,	201,	0,		0,	1,	5,	0,	0,	2,	'Total Purchases by Rev Type',		'Total number of purchases for the last 30 days grouped by revenue type',0,1,0,	1,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	5,	3,		201,	0,	1,	6,	0,	1,	1,	'Total Purchases by Day',			'Total purchases for the last 7 days grouped by day',				0,	1,	0,	0,	1,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	6,	103,	0,		0,	0,	5,	0,	1,	2,	'Total Purchases by Org',			'Total number of purchases the last 30 days grouped by organization',0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	4,	2,		201,	0,	1,	5,	0,	2,	1,	'Total Purchases by Hour',			'Total purchases for the last 24 hours grouped by hour',			0,	1,	0,	0,	1,	0,	0,	0,	0,	CURRENT_TIME);
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	3,	4,	6,	103,	0,		0,	0,	1,	0,	2,	2,	'Total Purchases by Org',			'Total number of purchases for the last 30 days grouped by organization',0,1,0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);

-- Map - Parent
INSERT INTO Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES 		(5,	1,	4,	11,	1,	103,	0,		0,	0,	8,	0,	0,	1,	'Map',								'',																	0,	1,	0,	0,	0,	0,	0,	0,	0,	CURRENT_TIME);

-- Default Dashboard Parent
-- Card Processing - Purchases - Parent
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		15,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		15,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Revenue by Method - 24h','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
-- Card Processing - Revenue - Parent
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		16,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 5,					6,		16,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Purchases by Method - 24h','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);


-- Widget Master List 

-- Total Revenue                                                                                                                                                                                                                                               
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue Last 30 Days'    ,'The total revenue for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Hour'	       ,'Total revenue for the last 24 hours grouped by hour'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Day'	       ,'Total revenue for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Month'	       ,'Total revenue for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,4                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Rate'	       ,'Total revenue for the last 24 hours grouped by rate'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
-- INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,1               ,1        ,1                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue by Parent Org'   ,'Total revenue for this month grouped by parent organization'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Organization' ,'Total revenue for the last 30 days grouped by organization'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Location(P)'  ,'Total revenue for the last 30 days grouped by parent location'      ,0               ,0         ,0          ,0            ,0            ,0            ,1                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Location'     ,'Total revenue for the last 30 days grouped by location'             ,0               ,0         ,0          ,0            ,0            ,0            ,1                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Route'	       ,'Total revenue for the last 30 days grouped by route'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,202              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Trans Type'   ,'Total revenue for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Revenue by Revenue Type' ,'Total revenue for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,107              ,0                ,0                ,2                 ,1                ,1                ,0       ,1          ,'Top 5 - 30 Days'               ,'The top 5 pay stations based on total revenue for the last 30 days'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,1                 ,6                ,107              ,0                ,0                ,3                 ,1                ,1                ,0       ,1          ,'Bottom 5 - 30 Days'		       ,'The bottom 5 pay stations based on total revenue for the last 30 days'  ,0             ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Total Purchases
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Hour'	   ,'Total purchases for the last 24 hours grouped by hour'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Day'	       ,'Total purchases for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Month'	   ,'Total purchases for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
-- INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,1               ,1        ,4                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Purchases by Parent Org' ,'Total number of purchases this month grouped by parent organization',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Org'	       ,'Total number of purchases the last 30 days grouped by organization' ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Location(P)','Total number of purchases the last 30 days grouped by parent location'   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Location'   ,'Total number of purchases the last 30 days grouped by location'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Route'	   ,'Total number of purchases the last 30 days grouped by route'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rev Type'   ,'Total number of purchases the last 30 days grouped by revenue type' ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,4                 ,6                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rate'       ,'Total number of purchases the last 30 days grouped by rate'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Space Turnover
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,6                 ,6                ,3                ,0              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Turnover by Day'	   		   ,'Space turnover for the last 30 days grouped by day'         		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,6                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Turnover by Month'	      	   ,'Space turnover for the last 12 months grouped by month'     		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,6                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Turnover by Location(P)'       ,'Space turnover for the last 30 days grouped by parent location'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,6                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Turnover by Location'  		   ,'Space turnover for the last 30 days grouped by location'    		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,6                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Turnover 30 Days'        ,'The total turnover for the last 30 days'                            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);

-- Utilization
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,7                 ,6                ,3                ,105              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Utilization by Day'  		   ,'Utilization for the last 30 days grouped by day and location'		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,7                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Utilization by Month'	   	   ,'Utilization for the last 12 months grouped by month'    			 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,7                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Utilization by Location(P)'	   ,'Utilization for the last 30 days grouped by parent location'    	 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,7                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Utilization by Location' 	   ,'Utilization for the last 30 days grouped by location'    			 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,7                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Utilization 30 Days'     ,'The total average utilization for the last 30 days'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);

-- Occupancy
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,5                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Paid Occupancy by Hour'	       ,'Paid occupancy for the last 24 hours grouped by hour'               ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,5                 ,6                ,3                ,105              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Paid Occupancy by Day'  	   ,'Paid occupancy for the last 30 days grouped by day and by location' ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,5                 ,1                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Location(P)' ,'Paid occupancy for the last 30 days grouped by parent location'     ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,5                 ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Location'    ,'Paid occupancy for the last 30 days grouped by location'            ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,5                 ,4                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Rate' 	   ,'Paid occupancy for the last 30 days grouped by rate'                ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Pay Stations with Active Alerts
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,9                 ,1                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Pay Stations with Alerts'	   ,'Total number of pay stations with active alerts'                    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,9                 ,1                ,204              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Type'	               ,'Total number of pay stations with active alerts grouped by type'	 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,9                 ,1                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Location(P)'         ,'Total number of pay stations with active alerts grouped by par location' ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,9                 ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Location'			   ,'Total number of pay stations with active alerts grouped by location',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,9                 ,1                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Route'   			   ,'Total number of pay stations with active alerts grouped by Route'   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Total Collections
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,2                 ,2                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Collections Today' 	   ,'The total collections made today'                                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,2                 ,2                ,106              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Collections by Route'    ,'The total collections made today grouped by route'                  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,2                 ,2                ,203              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Collections by Type'     ,'The total collections made today grouped by collection type'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Total Settled
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,3                 ,3                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Settled Yesterday' 	   ,'Total dollars settled yesterday'                                    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,3                 ,3                ,109              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Settled by Merchant Acct','Total dollars settled grouped by merchant account'                  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Map
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,11                ,1                ,105              ,0                ,0                ,0                 ,8                ,0                ,0       ,1          ,'Pay Station Map'               ,'A map pay stations grouped by location'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Average Price/Permit                                                                                                                                                                                                                                               
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Price by Day'       	   ,'Average price per permit for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Price by Month'	   		   ,'Average price per permit for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,5                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Price by Location(P)'      ,'Average price per permit for the last 7 days grouped by parent location'       ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,5                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Price by Location'         ,'Average price per permit for the last 7 days grouped by location'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Avg Price by Revenue Type'     ,'Average price per permit for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,12                ,6                ,202              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Avg Price by Trans Type'      ,'Average price per permit for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Average Revenue/Space                                                                                                                                                                                                                                               
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,13                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Revenue by Day'	           ,'Average revenue per space for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,13                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Revenue by Month'	       ,'Average revenue per space for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,13                ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Revenue by Location(P)'    ,'Average revenue per space for the last 30 days grouped by parent location'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,13                ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Revenue by Location'       ,'Average revenue per space for the last 30 days grouped by location'           ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Average Duration/Permit                                                                                                                                                                                                                                               
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,14                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Duration by Day'           ,'Average duration per permit for the last 7 days grouped by day'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,14                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Duration by Month'         ,'Average duration per permit for the last 12 months grouped by month'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,14                ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Duration by Location(P)'  ,'Average duration per permit for the last 30 days grouped by parent location'  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,14                ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Duration by Location'      ,'Average duration per permit for the last 30 days grouped by location'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                                   ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,14                ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Average Duration 30 Days','The total average duration for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);

-- Card Processing - Purchases
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					4,					2,			    205,					0,				0,					5,					0,				0,		1,		'Hourly Purchases by Method','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Purchases by Method','Settled Card Purchases for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					6,					105,			205,					0,				0,					5,					0,				1,		1,		'Location Purchases by Method',	'Settled Card Purchases for the last 30 days by location grouped by processing method',  0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		16,					6,					104,			205,					0,				0,					5,					0,				1,		2,		'Location(P) Purchases by Method','Settled Card Purchases for the last 30 days per parent location by processing method ',  0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Card Processing - Revenue
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					4,					2,			    205,						0,				0,					5,					0,				0,		1,		'Hourly Revenue by Method','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Revenue by Method','Settled Card Revenue for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					6,					105,			205,					0,				0,					5,					0,				1,		1,		'Location Revenue by Method',	'Settled Card Revenue for the last 30 days per location by processing method',	0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 1,					6,		15,					6,					104,			205,					0,				0,					5,					0,				1,		2,		'Location(P) Revenue by Method','Settled Card Revenue for the last 30 days per parent location by processing method',	0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Citation By Citation Type
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,17                ,4                ,2                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Hour'              ,'Citations per citation type for last 24 hours grouped by hour'      ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,17                ,5                ,3                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Day'               ,'Citations per citation type for last 7 days grouped by day'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,17                ,7                ,5                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Month'             ,'Citations per citation type for last 12 months grouped by month'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,17                ,5                ,110              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Citations by Citation Type'    ,'Citations for last 7 days grouped by citation type    '             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         


-- Citation Map
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,18                ,4                ,110              ,0                ,0                ,0                 ,9                ,0                ,0       ,1          ,'Citation Map'                  ,'Recently issued citations displayed on a map    '                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Counted Occupancy
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,19                ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted Occupancy by Hour'	   ,'Counted occupancy for the last 24 hours grouped by hour'            ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,19                ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Counted Occupancy by Location' ,'Counted occupancy grouped by location'                              ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Compare Occupancy
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,20                ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted/Paid Occ. by Hour'	   ,'Counted/paid occupancy for the last 24 hours grouped by hour'       ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,20                ,1                ,105              ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted/Paid Occ. by Location' ,'Counted/paid occupancy for now grouped by location'                 ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Occupancy Map
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,21                ,1                ,105              ,0                ,0                ,0                 ,10               ,0                ,0       ,1          ,'Occupancy Map by Location'     ,'Occupancy map by Location'                                          ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,1               ,1        ,21                ,1                ,104              ,0                ,0                ,0                 ,10               ,0                ,0       ,1          ,'Occupancy Map by Location(P)'  ,'Occupancy map by Parent Location'                                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME); 

-- Parent Widget Master List
-- Total Revenue                                                                                                                                                                                                                                               
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue Last 30 Days'    ,'The total revenue for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Hour'	       ,'Total revenue for the last 24 hours grouped by hour'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Day'	       ,'Total revenue for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Month'	       ,'Total revenue for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
-- INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,4               ,1        ,1                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue by Parent Org'   ,'Total revenue for this month grouped by parent organization'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Organization' ,'Total revenue for the last 30 days grouped by organization'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,202              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Trans Type'   ,'Total revenue for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Revenue by Revenue Type' ,'Total revenue for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,2                 ,1                ,1                ,0       ,1          ,'Top 5 - 30 Days'               ,'The top 5 organizations based on total revenue for the last 30 days',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,3                 ,1                ,1                ,0       ,1          ,'Bottom 5 - 30 Days'		       ,'The bottom 5 organizations based on total revenue for the last 30 days',0            ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Total Purchases
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,4                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Hour'	   ,'Total purchases for the last 24 hours grouped by hour'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,4                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Day'	       ,'Total purchases for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,4                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Month'	   ,'Total purchases for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
-- INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,4               ,1        ,4                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Purchases by Parent Org' ,'Total number of purchases this month grouped by parent organization',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,4                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Org'	       ,'Total number of purchases for the last 30 days grouped by organization',0            ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,4                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rev Type'   ,'Total number of purchases for the last 30 days grouped by revenue type',0            ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         

-- Map
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES        (1            ,4               ,1        ,11                ,1                ,103              ,0                ,0                ,0                 ,8                ,0                ,0       ,1          ,'Pay Station Map'               ,'A map pay stations grouped by organization'                         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,CURRENT_TIME);                         


-- Card Processing - Revenue - parent
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		16,					4,					2,				205,					0,				0,					5,					0,				0,		1,		'Hourly Purchases by Method','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		16,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		16,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Purchases by Method','Settled Card Purchases for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		16,					6,					103,			0,						0,				0,					2,					0,				1,		1,		'Purchases by Organization',	'Settled Card Purchases for the last 30 days by Organization grouped by processing method',  0,0,0,0,0,0,0,0,0,CURRENT_TIME);

-- Card Processing - Purchases - parent
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		15,					4,					2,				205,					0,				0,					5,					0,				0,		1,		'Hourly Revenue by Method','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		15,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		15,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Revenue by Method','Settled Card Revenue for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,CURRENT_TIME);
INSERT INTO Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)VALUES (	1,			 4,					6,		15,					6,					103,			0,						0,				0,					2,					0,				1,		1,		'Revenue by Organization',	'Settled Card Purchases for the last 30 days by Organization grouped by processing method',  0,0,0,0,0,0,0,0,0,CURRENT_TIME);


-- -----------------------------------------------------
-- Table RoleStatusType
-- -----------------------------------------------------

INSERT INTO RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', CURRENT_TIME, 1) ;
INSERT INTO RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  CURRENT_TIME, 1) ;
INSERT INTO RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Deleted',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table SensorInfoType
-- -----------------------------------------------------

INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Battery Voltage'       ,     6,	   15, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Battery 2 Voltage'     ,     6,	   15, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Controller Temperature',   -60,	   80, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Ambient Temperature'   ,   -60,	   80, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Relative Humidity'     ,     0,	  100, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Input Current'         , -9999,	99999, CURRENT_TIME, 1) ;
INSERT INTO SensorInfoType (Id, Name, MinValue, MaxValue, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'System Load'           ,     0,	99999, CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table MerchantStatusType
-- -----------------------------------------------------

INSERT INTO MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Disabled', CURRENT_TIME, 1) ;
INSERT INTO MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Enabled',  CURRENT_TIME, 1) ;
INSERT INTO MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Deleted',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CardRetryTransactionType
-- -----------------------------------------------------

INSERT INTO CardRetryTransactionType (Id, TypeName, Description) VALUES ( 0, 'Batched',  'Auto EMS Retry') ;
INSERT INTO CardRetryTransactionType (Id, TypeName, Description) VALUES ( 1, 'Store & Fwd', 'Store and Forward') ;

-- -----------------------------------------------------
-- Table LicencePlate: Id = 1, Number = 'None'
-- -----------------------------------------------------

INSERT INTO LicencePlate (Id,Number, LastModifiedGMT) VALUES (1,'None', CURRENT_TIME) ;

-- -----------------------------------------------------
-- Table MobileNumber: Id = 1, Number = 'None'
-- -----------------------------------------------------

INSERT INTO MobileNumber (Id,Number, LastModifiedGMT) VALUES (1,'None', CURRENT_TIME) ;

-- -----------------------------------------------------
-- Table ReportRepeatType
-- -----------------------------------------------------

INSERT INTO ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Online', 	CURRENT_TIME, 1);
INSERT INTO ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Daily', 	CURRENT_TIME, 1);
INSERT INTO ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Weekly', 	CURRENT_TIME, 1);
INSERT INTO ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Monthly', CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ReportType
-- -----------------------------------------------------

INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (1,		'Transaction - All', 					0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (2,		'Transaction - Cash', 					0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (3,		'Transaction - Credit Card', 			0, 1, 1, CURRENT_TIME, 1); 
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (4,		'Transaction - Credit Card Refund', 	0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (5,		'Transaction - Card Processing', 		0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (6,		'Transaction - Credit Card Retry', 		0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (7,		'Transaction - Patroller Card', 		0, 0, 0, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (8,		'Transaction - Rate', 					0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (9,		'Transaction - Rate Summary', 			0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (10,	'Transaction - Smart Card', 			0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (11,	'Transaction - Passcard', 			    0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (12,	'Stall Reports', 						0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (13,	'Coupon Usage Summary', 				0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (14,	'Collection Reports', 					0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (15,	'Replenish Reports', 					0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (16,	'Pay Station Summary',					0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (17,	'Tax Report', 							0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (18,	'Transaction - Delay', 					0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (19,	'Inventory Report', 					0, 1, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (20,	'Paystation Inventory Report', 			1, 0, 0, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (21,	'Maintenance Summary',					0, 0, 1, CURRENT_TIME, 1);
INSERT INTO ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (22,	'Collection Summary',					0, 0, 1, CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ReportStatusType
-- -----------------------------------------------------

INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Ready', 			CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Scheduled', 		CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Launched', 		CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Completed', 		CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Cancelled', 		CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Failed', 			CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Reran', 		CURRENT_TIME, 1);
INSERT INTO ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'Deleted', 		CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ReportFilterType
-- -----------------------------------------------------

INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  'Primary Date/Time', 		CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  'Secondary Date/Time', 	CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  'Location', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4,  'Space Number', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  'Card Number', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6,  'Approval Status', 		CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7,  'Ticket Number', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8,  'Report Number', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  'Coupon Code', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Transaction', 			CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Report', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Retry', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Group By', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Sort By', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Coupon Type',			CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ReportFilterValue
-- -----------------------------------------------------

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  100,  1,  'Date/Time', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  101,  1,  'Month', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  102,  1,  'Today', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4,  103,  1,  'Yesterday',						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  104,  1,  'Last 24 Hours', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6,  105,  1,  'This Week', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7,  106,  1,  'Last Week', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8,  107,  1,  'This Month', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  108,  1,  'Last Month', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10,  109,  1,  'Last 12 Months', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11,  110,  1,  'Last 30 Days', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12,  111,  1,  'Last 7 Days', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13,  112,  1,  'This Year', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14,  113,  1,  'Last Year', 						CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15,  300,  3,  'Location',			 			CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (16,  301,  3,  'Pay Station Route', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (17,  302,  3,  'Pay Station', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (19,  304,  3,  'Location', 						CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 400,  4,  'N/A', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 401,  4,  'No Stall', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 402,  4,  'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 403,  4,  '=', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (24, 404,  4,  '>=', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (25, 405,  4,  '<=', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (26, 406,  4,  'Between', 							CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (27, 500,  5,  'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (28, 501,  5,  'Specific #', 						CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (29, 600,  6,  'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (30, 601,  6,  'Pre-Auth', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (31, 602,  6,  'Real-Time', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (32, 603,  6,  'Batched', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (33, 604,  6,  'Offline', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (34, 605,  6,  'Recoverable', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (35, 606,  6,  'Uncloseable', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (36, 607,  6,  'Encryption Error', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 608, 6,  'To be Settled',	 				CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (37, 700,  7,  'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (38, 701,  7,  'Specific #', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (39, 702,  7,  'Between', 							CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (40, 800,  8,  'All',				 				CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (41, 801,  8,  '>=', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (42, 802,  8,  '<=', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (43, 803,  8,  'Specific #', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (44, 804,  8,  'Between',							CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (45, 900,  9,  'N/A', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (46, 901,  9,  'All Coupons', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (47, 902,  9,  'No Coupons', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (48, 903,  9,  'Specific Code', 						CURRENT_TIME, 1); 

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (49, 1000, 10, 'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (50, 1001, 10, 'Regular', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (51, 1002, 10, 'Add Time', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (52, 1003, 10, 'Monthly',							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (53, 1004, 10, 'Citation',							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (54, 1005, 10, 'SC Recharge',						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (55, 1006, 10, 'Test', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (56, 1007, 10, 'Replenish', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (57, 1008, 10, 'Extend-by-Phone', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (58, 1009, 10, 'Non-Refund', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (59, 1010, 10, 'Refund', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (60, 1011, 10, 'Deposit',							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (61, 1012, 10, 'Regular', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (62, 1013, 10, 'Cancelled', 						CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (63, 1100, 11, 'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (64, 1101, 11, 'Valid', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (65, 1102, 11, 'Expired', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (66, 1103, 11, 'Collection Report - Coin',		 	CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (67, 1104, 11, 'Collection Report - Bill',			CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (68, 1105, 11, 'Collection Report - Credit Card', 	CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (69, 1106, 11, 'Collection Report - All', 			CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (70, 1200, 12, 'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (71, 1201, 12, 'Batched', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (72, 1202, 12, 'Store Forward', 					CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (73, 1300, 13, 'None', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (74, 1301, 13, 'Day', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (75, 1302, 13, 'Month', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (77, 1304, 13, 'Location', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (78, 1305, 13, 'Pay Station', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (79, 1306, 13, 'Trans Type', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (80, 1307, 13, 'Payment Type', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (81, 1308, 13, 'Card Type', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (82, 1309, 13, 'Card Number', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (83, 1310, 13, 'Day of Week', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (84, 1311, 13, 'Machine', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (85, 1312, 13, 'Tax Name', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (86, 1313, 13, 'Pay Station Route',				CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (87, 1314, 13, 'Organization',						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (88, 1315, 13, 'Last Seen Time',					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (89, 1316, 13, 'Hour',								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (90, 1317, 13, 'Merchant Account',					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 1318, 13, 'Account',							CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (91, 1400, 14, 'Pay Station Name', 				CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (92, 1401, 14, 'Voltage', 							CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (93, 1402, 14, 'Coin Count', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (94, 1403, 14, 'Bill Count', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (95, 1404, 14, 'Paper Status', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (96, 1405, 14, 'Running Total', 					CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (97, 1406, 14, 'Unsettled Credit Card Count', 		CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (98, 1407, 14, 'Unsettled Credit Card Amount',		CURRENT_TIME, 1);

INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 1500, 15, 'All', 								CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (100, 1501, 15, 'Percentage', 						CURRENT_TIME, 1);
INSERT INTO ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 1502, 15, 'Dollar Based',					CURRENT_TIME, 1);


-- -----------------------------------------------------
-- Table ReportCollectionFilterType
-- -----------------------------------------------------

INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (1,0,0,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (2,0,0,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (3,0,0,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (4,0,0,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (5,0,0,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (6,0,0,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (7,0,0,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (8,0,0,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (9,0,0,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (10,0,0,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (11,0,0,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (12,0,0,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (13,0,0,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (14,0,0,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (15,0,0,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (16,0,0,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (17,0,0,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (18,0,0,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (19,0,0,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (20,0,0,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (21,0,0,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (22,0,0,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (23,0,0,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (24,0,0,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (25,0,0,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (26,0,0,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (27,0,0,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (28,0,0,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (29,0,0,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (30,0,0,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (31,0,0,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (32,0,0,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (33,0,0,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (34,0,0,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (35,0,0,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (36,0,0,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (37,0,0,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (38,0,0,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (39,0,0,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (40,0,0,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (41,0,0,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (42,0,0,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (43,0,0,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (44,0,0,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (45,0,0,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (46,0,0,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (47,0,0,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (48,0,0,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (49,0,0,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (50,0,0,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (51,0,0,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (52,0,0,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (53,0,0,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (54,0,0,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (55,0,0,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (56,0,0,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (57,0,0,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (58,0,0,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (59,0,0,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (60,0,0,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (61,0,0,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (62,0,0,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (63,0,0,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (64,0,1,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (65,0,1,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (66,0,1,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (67,0,1,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (68,0,1,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (69,0,1,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (70,0,1,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (71,0,1,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (72,0,1,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (73,0,1,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (74,0,1,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (75,0,1,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (76,0,1,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (77,0,1,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (78,0,1,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (79,0,1,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (80,0,1,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (81,0,1,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (82,0,1,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (83,0,1,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (84,0,1,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (85,0,1,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (86,0,1,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (87,0,1,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (88,0,1,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (89,0,1,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (90,0,1,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (91,0,1,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (92,0,1,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (93,0,1,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (94,0,1,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (95,0,1,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (96,0,1,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (97,0,1,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (98,0,1,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (99,0,1,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (100,0,1,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (101,0,1,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (102,0,1,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (103,0,1,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (104,0,1,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (105,0,1,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (106,0,1,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (107,0,1,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (108,0,1,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (109,0,1,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (110,0,1,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (111,0,1,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (112,0,1,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (113,0,1,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (114,0,1,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (115,0,1,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (116,0,1,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (117,0,1,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (118,0,1,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (119,0,1,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (120,0,1,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (121,0,1,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (122,0,1,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (123,0,1,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (124,0,1,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (125,0,1,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (126,0,1,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (127,0,1,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (128,1,0,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (129,1,0,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (130,1,0,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (131,1,0,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (132,1,0,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (133,1,0,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (134,1,0,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (135,1,0,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (136,1,0,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (137,1,0,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (138,1,0,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (139,1,0,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (140,1,0,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (141,1,0,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (142,1,0,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (143,1,0,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (144,1,0,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (145,1,0,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (146,1,0,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (147,1,0,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (148,1,0,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (149,1,0,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (150,1,0,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (151,1,0,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (152,1,0,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (153,1,0,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (154,1,0,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (155,1,0,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (156,1,0,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (157,1,0,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (158,1,0,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (159,1,0,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (160,1,0,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (161,1,0,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (162,1,0,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (163,1,0,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (164,1,0,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (165,1,0,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (166,1,0,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (167,1,0,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (168,1,0,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (169,1,0,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (170,1,0,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (171,1,0,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (172,1,0,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (173,1,0,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (174,1,0,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (175,1,0,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (176,1,0,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (177,1,0,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (178,1,0,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (179,1,0,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (180,1,0,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (181,1,0,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (182,1,0,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (183,1,0,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (184,1,0,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (185,1,0,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (186,1,0,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (187,1,0,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (188,1,0,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (189,1,0,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (190,1,0,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (191,1,0,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (192,1,1,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (193,1,1,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (194,1,1,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (195,1,1,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (196,1,1,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (197,1,1,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (198,1,1,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (199,1,1,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (200,1,1,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (201,1,1,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (202,1,1,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (203,1,1,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (204,1,1,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (205,1,1,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (206,1,1,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (207,1,1,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (208,1,1,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (209,1,1,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (210,1,1,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (211,1,1,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (212,1,1,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (213,1,1,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (214,1,1,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (215,1,1,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (216,1,1,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (217,1,1,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (218,1,1,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (219,1,1,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (220,1,1,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (221,1,1,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (222,1,1,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (223,1,1,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (224,1,1,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (225,1,1,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (226,1,1,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (227,1,1,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (228,1,1,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (229,1,1,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (230,1,1,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (231,1,1,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (232,1,1,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (233,1,1,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (234,1,1,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (235,1,1,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (236,1,1,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (237,1,1,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (238,1,1,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (239,1,1,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (240,1,1,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (241,1,1,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (242,1,1,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (243,1,1,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (244,1,1,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (245,1,1,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (246,1,1,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (247,1,1,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (248,1,1,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (249,1,1,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (250,1,1,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (251,1,1,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (252,1,1,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (253,1,1,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (254,1,1,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (Id, IsForOverdue, IsForCardAmount, IsForCardCount, IsForBillAmount, IsForBillCount, IsForCoinAmount, IsForCoinCount, IsForRunningTotal) VALUES (255,1,1,1,1,1,1,1,1);

-- -----------------------------------------------------
-- Table ReplenishType
-- -----------------------------------------------------

INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown'          , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'ChangerAdd'       , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'HopperAdd'        , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'ChangerTest'      , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'HopperTest'       , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'HopperRemoval'    , CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'ChangerPreAutoSet', CURRENT_TIME, 1) ;
INSERT INTO ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'ChangerAutoSet'   , CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table LicencePlate: Id = 1, Number = 'None'
-- -----------------------------------------------------
INSERT INTO LicencePlate (Number, LastModifiedGMT) VALUES ('AFB114', CURRENT_TIME);
INSERT INTO LicencePlate (Number, LastModifiedGMT) VALUES ('787POP', CURRENT_TIME);

-- -----------------------------------------------------
-- Table Tax
-- -----------------------------------------------------
-- INSERT INTO Tax (Id, CustomerId, Name, LastModifiedGMT) VALUES (1,5,'GST','2012-01-01 00:00:00');
-- INSERT INTO Tax (Id, CustomerId, Name, LastModifiedGMT) VALUES (2,5,'PST','2012-01-01 00:00:00');
-- INSERT INTO Tax (Id, CustomerId, Name, LastModifiedGMT) VALUES (3,5,'HST','2012-01-01 00:00:00');

-- -----------------------------------------------------
-- Table PurchaseTax
-- -----------------------------------------------------
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (1,1,1,500,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (2,1,2,724,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (3,2,1,500,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (4,2,2,724,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (5,3,1,500,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (6,3,2,724,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (7,4,1,500,100);
-- INSERT INTO PurchaseTax (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (8,4,2,724,100);

-- -----------------------------------------------------
-- Table Purchase
-- -----------------------------------------------------
-- INSERT INTO Purchase (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:13:24',3,1,5,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:15:53');
-- INSERT INTO Purchase (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:15:34',3,17,9,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:32:33');
-- INSERT INTO Purchase (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:17:24',3,1,5,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:15:53');
-- INSERT INTO Purchase (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:19:34',3,17,9,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:32:33');

-- -----------------------------------------------------
-- Table MobileNumber
-- -----------------------------------------------------
INSERT INTO MobileNumber (Id, Number, LastModifiedGMT) VALUES (2,'17783899692','2013-01-03 23:39:50');
INSERT INTO MobileNumber (Id, Number, LastModifiedGMT) VALUES (3,'16043439333','2013-01-03 23:49:51');

-- -----------------------------------------------------
-- Table MobileLicenseStatusType
-- -----------------------------------------------------
INSERT INTO MobileLicenseStatusType (Id, Name) VALUES (1, 'AVAILABLE');
INSERT INTO MobileLicenseStatusType (Id, Name) VALUES (2, 'PROVISIONED');

-- -----------------------------------------------------
-- Table MobileApplicationType
-- -----------------------------------------------------
INSERT INTO MobileApplicationType (Id, Name, SubscriptionTypeId) VALUES (1, 'Digital Collect', 1300);

-- -----------------------------------------------------
-- Table MobileAppActivityType
-- -----------------------------------------------------
INSERT INTO MobileAppActivityType (Id, Name) VALUES (1, 'PROVISION');
INSERT INTO MobileAppActivityType (Id, Name) VALUES (2, 'LOGIN');
INSERT INTO MobileAppActivityType (Id, Name) VALUES (3, 'LOGOUT');
INSERT INTO MobileAppActivityType (Id, Name) VALUES (4, 'BLOCK');
INSERT INTO MobileAppActivityType (Id, Name) VALUES (5, 'UNBLOCK');
INSERT INTO MobileAppActivityType (Id, Name) VALUES (6, 'RELEASE');


-- -----------------------------------------------------
-- Table SmsAlert
-- -----------------------------------------------------
-- INSERT INTO SmsAlert (Id, MobileNumberId, CustomerId, PermitLocationId, PointOfSaleId, PermitBeginGMT, PermitExpireGMT, TicketNumber, AddTimeNumber, PaystationSettingName, LicencePlate, SpaceNumber, NumberOfRetries, IsAutoExtended, IsAlerted, IsLocked) VALUES (1,2,5,14,14,'2013-01-04 07:13:34','2013-01-04 07:43:34',3,0,'Northeast','AFB114',0,0,0,0,0);
-- INSERT INTO SmsAlert (Id, MobileNumberId, CustomerId, PermitLocationId, PointOfSaleId, PermitBeginGMT, PermitExpireGMT, TicketNumber, AddTimeNumber, PaystationSettingName, LicencePlate, SpaceNumber, NumberOfRetries, IsAutoExtended, IsAlerted, IsLocked) VALUES (2,3,5,14,14,'2013-01-04 07:15:35','2013-01-04 07:45:35',3,0,'Northeast','787POP',0,0,0,0,0);

-- -----------------------------------------------------
-- Table ExtensiblePermit
-- -----------------------------------------------------
-- INSERT INTO ExtensiblePermit (Id, OriginalPermitId, MobileNumberId, CardData, Last4Digit, PermitBeginGMT, PermitExpireGMT, IsRFID) VALUES (1,1,2,'',0,'2013-01-04 07:13:34','2013-01-04 07:43:34',0);
-- INSERT INTO ExtensiblePermit (Id, OriginalPermitId, MobileNumberId, CardData, Last4Digit, PermitBeginGMT, PermitExpireGMT, IsRFID) VALUES (2,2,3,'',0,'2013-01-04 07:15:35','2013-01-04 07:45:35',0);

-- -----------------------------------------------------
-- Table Permit
-- -----------------------------------------------------
-- INSERT INTO Permit (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (1,1,3,14,3,2,NULL,1,1,0,0,'2013-01-04 07:13:37','2013-01-04 07:43:37','2013-01-04 07:43:37',0);
-- INSERT INTO Permit (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (2,2,3,14,3,2,NULL,2,2,0,0,'2013-01-04 07:15:34','2013-01-04 07:45:37','2013-01-04 07:45:37',0);
-- INSERT INTO Permit (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (3,3,3,14,3,2,NULL,NULL,1,0,0,'2013-01-04 07:17:34','2013-01-04 07:47:37','2013-01-04 07:47:37',0);
-- INSERT INTO Permit (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (4,4,3,14,3,2,NULL,NULL,3,0,0,'2013-01-04 07:19:34','2013-01-04 07:49:37','2013-01-04 07:49:37',0);

-- -----------------------------------------------------
-- Table Cluster
-- -----------------------------------------------------

INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'VANAPP01', 'vanapp01', 8080, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'VANAPP02', 'vanapp02', 8080, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'VANAPP03', 'vanapp01', 8081, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'VANAPP04', 'vanapp02', 8081, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'VANAPP05', 'vanapp01', 8085, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'VANAPP06', 'vanapp02', 8086, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'VANAPP07', 'vanapp03', 8087, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'VANAPP08', 'vanapp03', 8088, 0, CURRENT_TIME, 1);
INSERT INTO Cluster (Id, Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (9, 'VANAPP09', 'vanapp03', 8089, 0, CURRENT_TIME, 1);

-- -----------------------------------------------------
-- Table ETLStatusType
-- -----------------------------------------------------

INSERT INTO ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Ready'          , CURRENT_TIME, 1) ;
INSERT INTO ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Timezone Error' , CURRENT_TIME, 1) ;
INSERT INTO ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Error'          , CURRENT_TIME, 1) ;
INSERT INTO ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Done'           , CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerMigrationStatusType
-- -----------------------------------------------------

INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Requested', 	      			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Scheduled',       			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Suspended',             		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Cancelled',       			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Queued',               	  	CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'EMS6 Disabled',               CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Waiting Data Migration',      CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Data Migration Confirmed',   	CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Completed',             		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Communication Checked',       CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'All Pay Stations Migrated', 	CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 'Not Boarded', 				CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (90, 'Boarded', 					CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerMigrationStatusType
-- -----------------------------------------------------

INSERT INTO CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',	       			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Blocked',       			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Warned',             		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Passed',       				CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerMigrationFailureType
-- -----------------------------------------------------

INSERT INTO CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown',       				CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Unprocessed Reversal', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unprocessed Card',    		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Pre-migration Validations',  CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table CustomerMigrationValidationType
-- -----------------------------------------------------

INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'cm.label.EMS6DBConnection',        'cm.warning.EMS6DBConnection', 			'cm.URL.EMS6DBConnection',        	1, 0, 1, 0, 1, 'checkEMS6Connection', 			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'cm.label.EMS6SlaveDBConnection',	'cm.warning.EMS6SlaveDBConnection', 	'cm.URL.EMS6SlaveDBConnection', 	1, 0, 1, 0, 1, 'checkEMS6SlaveConnection', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'cm.label.ETLUpToDate', 			'cm.warning.ETLUpToDate', 				'cm.URL.ETLUpToDate',       		1, 0, 1, 1, 1, 'checkMigrationQueueStatus', 	CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'cm.label.CardRetryRunning', 		'cm.warning.CardRetryRunning', 			'cm.URL.CardRetryRunning',      	1, 0, 1, 1, 0, 'checkCardRetry', 				CURRENT_TIME, 1) ;

INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'cm.label.PayStationValid', 		'cm.warning.PayStationValid', 			'cm.URL.PayStationValid',		    1, 1, 0, 0, 0, 'checkPayStationVersion', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'cm.label.RestAccountCheck', 		'cm.warning.RestAccountCheck', 			'cm.URL.RestAccountCheck',	    	0, 1, 0, 0, 0, 'checkRestAccounts', 			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'cm.label.PayStationCheck', 		'cm.warning.PayStationCheck', 			'cm.URL.PayStationCheck',		    1, 0, 0, 0, 0, 'checkPayStationCount', 			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'cm.label.CouponCountCheck', 		'cm.warning.CouponCountCheck', 			'cm.URL.CouponCountCheck',		    0, 0, 0, 0, 0, 'checkCouponCount', 				CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'cm.label.UnusedMerchantAccount', 	'cm.warning.UnusedMerchantAccount', 	'cm.URL.UnusedMerchantAccount',	    1, 0, 0, 0, 0, 'checkUnusedMerchantAccounts', 	CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'cm.label.LocationWithDash', 		'cm.warning.LocationWithDash', 			'cm.URL.LocationWithDash',		    0, 0, 0, 0, 0, 'checkLocationsWithDash', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11, 'cm.label.LongRateNameCheck', 		'cm.warning.LongRateNameCheck', 		'cm.URL.LongRateNameCheck',		    1, 0, 0, 0, 0, 'checkLongRateNames', 			CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12, 'cm.label.MerchantAccountCheck', 	'cm.warning.MerchantAccountCheck', 		'cm.URL.MerchantAccountCheck',	    1, 0, 0, 1, 0, 'checkMerchantAccounts', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13, 'cm.label.ReversalCheck', 			'cm.warning.ReversalCheck', 			'cm.URL.ReversalCheck',		   	    1, 0, 0, 1, 0, 'checkReversals', 				CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14, 'cm.label.CardTransactionCheck',	'cm.warning.CardTransactionCheck', 		'cm.URL.CardTransactionCheck',	    1, 0, 0, 1, 0, 'checkCardTransactions', 		CURRENT_TIME, 1) ;
INSERT INTO CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15, 'cm.label.EnforcementModeCheck',	'cm.warning.EnforcementModeCheck', 		'cm.URL.EnforcementModeCheck',	    0, 1, 0, 0, 0, 'checkEnforcementMode', 			CURRENT_TIME, 1) ;

-- -----------------------------------------------------
-- Table HashAlgorithmType
-- -----------------------------------------------------

INSERT INTO HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'SHA-1',      		0, 'Sha1',	 3, 	0, CURRENT_TIME, 1) ;
INSERT INTO HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'SHA-256',			0, 'Sha256', 4, 	0, CURRENT_TIME, 1) ;
INSERT INTO HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'SHA1withRSA',		0, '', 		 0,		1, CURRENT_TIME, 1) ;
INSERT INTO HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'SHA256withRSA',	0, '', 		 0,		1, CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table RateType
-- -----------------------------------------------------

-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 0, 'Flat Rate', 	CURRENT_TIME, 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Daily', 	CURRENT_TIME, 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Hourly', 	CURRENT_TIME, 1); 
INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Incremental', 	CURRENT_TIME, 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 4, 'Monthly', 	CURRENT_TIME, 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 5, 'Parking Restriction', 	CURRENT_TIME, 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 6, 'Holiday', 	CURRENT_TIME, 1); 

-- -----------------------------------------------------
-- Table RateFlatType
-- -----------------------------------------------------

INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 0, 'Valid For', 	CURRENT_TIME, 1); 
INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Expires At', 	CURRENT_TIME, 1); 
INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Scheduled', 	CURRENT_TIME, 1); 

-- -----------------------------------------------------
-- Table RateHourlyRuleType
-- -----------------------------------------------------

INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Up To', 	CURRENT_TIME, 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Next', 	CURRENT_TIME, 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Per', 	CURRENT_TIME, 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 4, 'From', 	CURRENT_TIME, 1); 


-- -----------------------------------------------------
-- Table RateExpiryDayType
-- -----------------------------------------------------

INSERT INTO RateExpiryDayType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Same Day', 	CURRENT_TIME, 1); 
INSERT INTO RateExpiryDayType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Next Day', 	CURRENT_TIME, 1); 



-- -----------------------------------------------------
-- Table CardProcessMethodType
-- -----------------------------------------------------
INSERT INTO CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Real-Time',  		CURRENT_TIME, 1) ;
INSERT INTO CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Store and Forward',  CURRENT_TIME, 1) ;
INSERT INTO CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Batched',  			CURRENT_TIME, 1) ;
INSERT INTO CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Unclosable',  			CURRENT_TIME, 1) ;
INSERT INTO CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Recoverable',  			CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table FlexETLDataType 
-- -----------------------------------------------------

INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'CitationWidgetData', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'CitationMapData', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'PermitData', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'CitationType', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Facility', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Property', CURRENT_TIME, 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Citation', CURRENT_TIME, 1) ;


-- -----------------------------------------------------
-- Table TransactionSettlementStatusType 
-- -----------------------------------------------------
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(0,'Pre-Authorised','PreAuth Approved',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(1,'To be Settled','Post Auth received',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(2,'Settled','Transaction sucessfully settled',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(3,'Failed to Settle','Transaction failed to settle',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(4,'Cannot Settle','Transaction cannot be settled',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(5,'Expired','Transaction Expired',CURRENT_TIME,1);
INSERT INTO TransactionSettlementStatusType (Id,Name,Description,LastModifiedGMT,LastModifiedByUserId)VALUES(6,'Excluded','Transaction Reversed',CURRENT_TIME,1);
-- Add more status in this Lookup table

-- -----------------------------------------------------
-- Table BatchStatusType 
-- -----------------------------------------------------
INSERT INTO BatchStatusType (Id,Name,LastModifiedGMT,LastModifiedByUserId)VALUES(1,'Open',CURRENT_TIME,1);
INSERT INTO BatchStatusType (Id,Name,LastModifiedGMT,LastModifiedByUserId)VALUES(2,'Close',CURRENT_TIME,1);
INSERT INTO BatchStatusType (Id,Name,LastModifiedGMT,LastModifiedByUserId)VALUES(3,'Partially Closed',CURRENT_TIME,1);
INSERT INTO BatchStatusType (Id,Name,LastModifiedGMT,LastModifiedByUserId)VALUES(4,'Partially RB',CURRENT_TIME,1);
INSERT INTO BatchStatusType (Id,Name,LastModifiedGMT,LastModifiedByUserId)VALUES(5,'Failed',CURRENT_TIME,1);


-- -----------------------------------------------------
-- Table ElavonRequestType 
-- -----------------------------------------------------
INSERT INTO ElavonRequestType (Id, Code, CaptureTranCode, RequestName, Description, LastModifiedGMT,LastModifiedByUserId)VALUES(1, '000', 5, 'Credit Card.Sale', '000 = Credit Card Sale', CURRENT_TIME,1);
INSERT INTO ElavonRequestType (Id, Code, CaptureTranCode, RequestName, Description, LastModifiedGMT,LastModifiedByUserId)VALUES(2, '098', 6, 'Credit Card.Void', '098 = Credit Card Void', CURRENT_TIME,1);
INSERT INTO ElavonRequestType (Id, Code, CaptureTranCode, RequestName, Description, LastModifiedGMT,LastModifiedByUserId)VALUES(3, '099', 0, 'Credit Card.Reversal', '099 = Credit Card Reversal', CURRENT_TIME,1);
INSERT INTO ElavonRequestType (Id, Code, CaptureTranCode, RequestName, Description, LastModifiedGMT,LastModifiedByUserId)VALUES(4, '005', 6, 'Credit Card.Return', '005 = Credit Card Return', CURRENT_TIME,1);

INSERT INTO BatchSettlementType(Id, Description ) VALUES (1 , 'NoOfTransactions');
INSERT INTO BatchSettlementType(Id, Description ) VALUES (2 , 'TimeOfDay'); 
INSERT INTO BatchSettlementType(Id, Description ) VALUES (3 , 'Refund');


-- -----------------------------------------------------
-- Table TelemetryType 
-- -----------------------------------------------------
INSERT INTO TelemetryType (Id, Name, Label, IsPrivate, CreatedGMT) VALUES(1	, 'SerialNumber'		, 'label.telemetry.serialnumber'	, 0, CURRENT_TIME);
INSERT INTO TelemetryType (Id, Name, Label, IsPrivate, CreatedGMT) VALUES(2	, 'FirmwareVersion'	, 'label.telemetry.firmwareversion'	, 1, CURRENT_TIME);
INSERT INTO TelemetryType (Id, Name, Label, IsPrivate, CreatedGMT) VALUES(3	, 'ViolationStatus'				, 'label.telemetry.violationstatus'			, 0, CURRENT_TIME);


-- -----------------------------------------------------
-- Table GatewayProcessor 
-- -----------------------------------------------------
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(1, 8, 'NOVA', 'NOVA', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(2, 8, 'Paymentech Tampa', 'Paymentech Tampa', CURRENT_TIME); 
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(3, 8, 'First Data Merchant Services Nashille', 'First Data Merchant Services Nashille', CURRENT_TIME); 
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(4, 8, 'First Data Merchant Services North', 'First Data Merchant Services North', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(5, 8, 'First Data Merchant Services South', 'First Data Merchant Services South', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(6, 8, 'Global East', 'Global East', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(7, 8, 'Moneris', 'Moneris', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(8, 8, 'Vital', 'Vital', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(9, 8, 'BCE Emergis', 'BCE Emergis', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(10, 11, 'Chase Paymentech Solutions', 'Chase Paymentech Solutions', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(11, 11, 'First Data Merchant Services (FDMS)', 'First Data Merchant Services (FDMS)', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(12, 11, 'Global Payments', 'Global Payments', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(13, 11, 'Nova Information Systems', 'Nova Information Systems', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(14, 11, 'Pay By Touch Payment Solutions', 'Pay By Touch Payment Solutions', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(15, 11, 'RBS Lynk', 'RBS Lynk', CURRENT_TIME);
INSERT INTO GatewayProcessor (Id, ProcessorId, Name, Value, CreatedGMT) VALUES(16, 11, 'TSYS Acquiring Solutions', 'TSYS Acquiring Solutions', CURRENT_TIME);

INSERT INTO Timezone_v(Id, Name) VALUES(360, 'Canada/Atlantic');
INSERT INTO Timezone_v(Id, Name) VALUES(361, 'Canada/Central');
INSERT INTO Timezone_v(Id, Name) VALUES(362, 'Canada/East-Saskatchewan');
INSERT INTO Timezone_v(Id, Name) VALUES(363, 'Canada/Eastern');
INSERT INTO Timezone_v(Id, Name) VALUES(364, 'Canada/Mountain');
INSERT INTO Timezone_v(Id, Name) VALUES(365, 'Canada/Newfoundland');
INSERT INTO Timezone_v(Id, Name) VALUES(366, 'Canada/Pacific');
INSERT INTO Timezone_v(Id, Name) VALUES(367, 'Canada/Saskatchewan');
INSERT INTO Timezone_v(Id, Name) VALUES(368, 'Canada/Yukon');
INSERT INTO Timezone_v(Id, Name) VALUES(473, 'GMT');
INSERT INTO Timezone_v(Id, Name) VALUES(571, 'US/Alaska');
INSERT INTO Timezone_v(Id, Name) VALUES(572, 'US/Aleutian');
INSERT INTO Timezone_v(Id, Name) VALUES(573, 'US/Arizona');
INSERT INTO Timezone_v(Id, Name) VALUES(574, 'US/Central');
INSERT INTO Timezone_v(Id, Name) VALUES(575, 'US/East-Indiana');
INSERT INTO Timezone_v(Id, Name) VALUES(576, 'US/Eastern');
INSERT INTO Timezone_v(Id, Name) VALUES(577, 'US/Hawaii');
INSERT INTO Timezone_v(Id, Name) VALUES(578, 'US/Indiana-Starke');
INSERT INTO Timezone_v(Id, Name) VALUES(579, 'US/Michigan');
INSERT INTO Timezone_v(Id, Name) VALUES(580, 'US/Mountain');
INSERT INTO Timezone_v(Id, Name) VALUES(581, 'US/Pacific');

INSERT INTO MerchantAccountMigrationStatus (Id, Name) VALUES (1,'PENDING');
INSERT INTO MerchantAccountMigrationStatus (Id, Name) VALUES (2,'SUCCESS');
INSERT INTO MerchantAccountMigrationStatus (Id, Name) VALUES (3,'FAILED');

-- Test Service Agreement
INSERT INTO ServiceAgreement (ID, ContentVersion, UploadedGMT, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1, CURRENT_TIME, 0, CURRENT_TIME, 1);

SET DATABASE REFERENTIAL INTEGRITY TRUE;