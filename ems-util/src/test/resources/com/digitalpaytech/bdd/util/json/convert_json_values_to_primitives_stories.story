Narrative: Test various values that will be 
sent from UI and converted to primitives, tests 
issue EMS-10093

Scenario: Test conversion of full range of potential values

Given there is a string <value> that converts to <dataType> type
When I convert the values
Then the values are converted correctly

Examples:
|value|dataType|
|0.0|double|
|0.|string|
|.0|string|
|00.0|string|
|0.00|string|
|0|double|
|1.0|double|
|1.|string|
|.1|string|
|01.0|string|
|0.01|double|
|0.0001|double|
|0.00010|string|
|1.42342|double|
|123234321.0|double|
|123234321.343253245|string|
|0.343253245|double|
|0.3432532450|string|
|3432532450|double|
|4503599627370496|double|
|4503599627370497|string|
|0.4503599627370496|double|
|0.4503599627370497|string|
|402F|string|
|402.0|double|
|402D|string|
|403.0|double|
|true|boolean|
|false|boolean|
|1234567890123456789012345|string|
|4503599627370496|double|
|4503599627370497|string|