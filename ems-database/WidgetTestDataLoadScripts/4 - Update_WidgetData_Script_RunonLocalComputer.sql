
-- This script will update the Widget data with future TimeId's
-- Run this script on Local Computer


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS `OccupancyHourTurnover` ;

DROP TABLE IF EXISTS `OccupancyDayTurnover` ;

DROP TABLE IF EXISTS `OccupancyMonthTurnover` ;

DROP TABLE IF EXISTS `PPPInfo` ;

DROP TABLE IF EXISTS `PPPInfoDetail` ;

DROP TABLE IF EXISTS `Occupancy` ;


DROP TABLE IF EXISTS PPPTotalHour;
DROP TABLE IF EXISTS PPPTotalDay;
DROP TABLE IF EXISTS PPPTotalMonth;
DROP TABLE IF EXISTS PPPDetailHour;
DROP TABLE IF EXISTS PPPDetailDay;
DROP TABLE IF EXISTS PPPDetailMonth;
DROP TABLE IF EXISTS PPPRevenueHour;
DROP TABLE IF EXISTS PPPRevenueDay;
DROP TABLE IF EXISTS PPPRevenueMonth;

DROP TABLE IF EXISTS OccupancyHour;
DROP TABLE IF EXISTS OccupancyDay;
DROP TABLE IF EXISTS OccupancyMonth;

DROP TABLE IF EXISTS UtilizationDay;
DROP TABLE IF EXISTS UtilizationMonth;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- On Local Computer 
-- Execute the DROP Table Script
-- Rename the Widget Tables to KPI Tables

rename table W_PPPTotalHour to PPPTotalHour;
rename table W_PPPTotalDay to PPPTotalDay;
rename table W_PPPTotalMonth to PPPTotalMonth;
rename table W_PPPDetailHour to PPPDetailHour;
rename table W_PPPDetailDay to PPPDetailDay;
rename table W_PPPDetailMonth to PPPDetailMonth;
rename table W_PPPRevenueHour to PPPRevenueHour;
rename table W_PPPRevenueDay to PPPRevenueDay;
rename table W_PPPRevenueMonth to PPPRevenueMonth;

rename table W_OccupancyHour to OccupancyHour;
rename table W_OccupancyDay to OccupancyDay;
rename table W_OccupancyMonth to OccupancyMonth;

rename table W_UtilizationDay to UtilizationDay;
rename table W_UtilizationMonth to UtilizationMonth;

-- DEFINE the Primary Key for KPI Tables
-- Note: FK's are not defined as this is a test data only


ALTER TABLE `UtilizationMonth` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL ,
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `UtilizationDay` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  ,
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `OccupancyMonth` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `OccupancyDay` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  ,
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `OccupancyHour` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPRevenueMonth` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL   ,
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPRevenueDay` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPRevenueHour` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPDetailMonth` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL ,  
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPDetailDay` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL ,  
 ADD PRIMARY KEY (`Id`) ;


ALTER TABLE `PPPDetailHour` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL ,  
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPTotalMonth` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPTotalDay` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

ALTER TABLE `PPPTotalHour` CHANGE COLUMN `Id` `Id` BIGINT(20) UNSIGNED NOT NULL  , 
 ADD PRIMARY KEY (`Id`) ;

-- Delete the data other than Test Customers
-- Note: Not an issue even if data is not deleted. It will only increase the dump file size

DELETE FROM UtilizationMonth
WHERE Customerid not in (3,5);

DELETE FROM UtilizationDay
WHERE Customerid not in (3,5);

DELETE FROM OccupancyMonth
WHERE Customerid not in (3,5);

DELETE FROM OccupancyDay
WHERE Customerid not in (3,5);

DELETE FROM OccupancyHour
WHERE Customerid not in (3,5);

DELETE FROM PPPTotalMonth
WHERE Customerid not in (3,5);

DELETE FROM PPPTotalDay
WHERE Customerid not in (3,5);

DELETE FROM PPPTotalHour
WHERE Customerid not in (3,5);

-- Move the Time ID's accordingly
-- In this senario, Time Id's are moved by 1 year. (Time ID of 2012-05-01 00:00:00 is moved to 2013-05-01 00:00:00)
-- Get the Time Id difference and apply the value to all tables.

UPDATE PPPTotalHour
SET TimeIdGMT = TimeIdGMT + 35040 ,
TimeIdLocal = TimeIdLocal + 35040 ;

UPDATE PPPTotalDay
SET TimeIdLocal = TimeIdLocal + 35040 ;


UPDATE PPPTotalMonth
SET TimeIdLocal = TimeIdLocal + 35040 ;


UPDATE OccupancyHour
SET TimeIdGMT = TimeIdGMT + 35040 ,
TimeIdLocal = TimeIdLocal + 35040 ;


UPDATE OccupancyDay
SET TimeIdLocal = TimeIdLocal + 35040 ;

UPDATE OccupancyMonth
SET TimeIdLocal = TimeIdLocal + 35040 ;

UPDATE UtilizationDay
SET TimeIdLocal = TimeIdLocal + 35040 ;

UPDATE UtilizationMonth
SET TimeIdLocal = TimeIdLocal + 35040 ;

