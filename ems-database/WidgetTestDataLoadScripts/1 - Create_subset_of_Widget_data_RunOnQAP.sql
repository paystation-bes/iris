-- Script Checked in to SVN on June 27 11:30 AM
-- This script will extract Widget data from QAP SERVER and will moved the Time Id's to future date
-- This data is only used for Testing Purpose
-- Do not run the script as is. Change the Customer Id's, Time ID's accordingly

-- STEP 1 
-- Run the Below script on QAP

-- Drop Temp Tables if exists

drop  table IF EXISTS W_PPPTotalHour ;
drop  table IF EXISTS W_PPPTotalDay;
drop  table IF EXISTS W_PPPTotalMonth;
drop  table IF EXISTS W_PPPDetailHour;
drop  table IF EXISTS W_PPPDetailDay;
drop  table IF EXISTS W_PPPDetailMonth;
drop  table IF EXISTS W_PPPRevenueHour;
drop  table IF EXISTS W_PPPRevenueDay;
drop  table IF EXISTS W_PPPRevenueMonth;
drop  table IF EXISTS W_OccupancyHour;
drop  table IF EXISTS W_OccupancyDay;
drop  table IF EXISTS W_OccupancyMonth;
drop  table IF EXISTS W_UtilizationDay;
drop  table IF EXISTS W_UtilizationMonth;

-- Move subset of widget data to Temp Tables on QAP
-- CHange the Customer ID's and TimeID Local accordingly
-- In this senario, Loaded Widget data for specific customers (33, 163,216, 121, 20) between 2012-05-01 and 2012-09-01

create table W_PPPTotalHour
select * from Copy_PPPTotalHour where CustomerId in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

create table W_PPPTotalDay
select * from Copy_PPPTotalDay where CustomerId in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193  ;

create table W_PPPTotalMonth
select * from Copy_PPPTotalMonth where CustomerId in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193  ;

create table W_PPPDetailHour
select A.* from Copy_PPPDetailHour  A, Copy_PPPTotalHour B
WHERE A.PPPTotalHourId = B.Id and B.Customerid in (33, 163,216, 121, 20) and B.TimeIdLocal between 432385 and 444193 ;

create table W_PPPDetailDay
select A.* from Copy_PPPDetailDay  A, Copy_PPPTotalDay B
WHERE A.PPPTotalDayId = B.Id and B.Customerid in (33, 163,216, 121, 20) and B.TimeIdLocal between 432385 and 444193 ;

create table W_PPPDetailMonth
select A.* from Copy_PPPDetailMonth  A, Copy_PPPTotalMonth B
WHERE A.PPPTotalMonthId = B.Id and B.Customerid in (33, 163,216, 121, 20) and B.TimeIdLocal between 432385 and 444193 ;


create table W_PPPRevenueHour
select A.*  from Copy_PPPRevenueHour  A, Copy_PPPDetailHour B, Copy_PPPTotalHour C
WHERE A.PPPDetailHourId = B.Id 
and B.PPPTotalHourId = C.Id and 
C.Customerid in (33, 163,216, 121, 20) and C.TimeIdLocal between 432385 and 444193 ;

create table W_PPPRevenueDay
select A.*  from Copy_PPPRevenueDay  A, Copy_PPPDetailDay B, Copy_PPPTotalDay C
WHERE A.PPPDetailDayId = B.Id 
and B.PPPTotalDayId = C.Id and 
C.Customerid in (33, 163,216, 121, 20) and C.TimeIdLocal between 432385 and 444193 ;


create table W_PPPRevenueMonth
select A.*  from Copy_PPPRevenueMonth  A, Copy_PPPDetailMonth B, Copy_PPPTotalMonth C
WHERE A.PPPDetailMonthId = B.Id 
and B.PPPTotalMonthId = C.Id and 
C.Customerid in (33, 163,216, 121, 20) and C.TimeIdLocal between 432385 and 444193 ;


create table W_OccupancyHour
SELECT * from OccupancyHour 
WHERE Customerid in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

create table W_OccupancyDay
SELECT * from OccupancyDay 
WHERE Customerid in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

create table W_OccupancyMonth
SELECT * from OccupancyMonth 
WHERE Customerid in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

create table W_UtilizationDay
SELECT * from UtilizationDay 
WHERE Customerid in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

create table W_UtilizationMonth
SELECT * from UtilizationMonth 
WHERE Customerid in (33, 163,216, 121, 20) and TimeIdLocal between 432385 and 444193 ;

-- UPDATE with Test Customers --
-- Get the Mapping Test (Customer ID, LocationID, Paystation ID) with Real (Customer ID, LocationID, Paystation ID)
-- In this senario, Updated data for real customers(33,163,216) with Test Customer ID 3
-- and real customers (121,20) with Test customer ID 5

UPDATE W_PPPTotalHour
SET CustomerId = 3 where CustomerId  in (33,163,216) ;

UPDATE W_PPPTotalHour
SET CustomerId = 5 where CustomerId  in (121,20) ;

UPDATE W_PPPTotalDay
SET CustomerId = 3 where CustomerId  in (33,163,216) ;

UPDATE W_PPPTotalDay
SET CustomerId = 5 where CustomerId  in (121,20) ;


UPDATE W_PPPTotalMonth
SET CustomerId = 3 where CustomerId  in (33,163,216) ;

UPDATE W_PPPTotalMonth
SET CustomerId = 5 where CustomerId  in (121,20) ;

-- UPDATE Test Location and POS with Real Location id and POS

UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 1
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8071 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 2
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8072 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 3
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8764 ;

UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 10,
PermitLocationId = 10,
PointOfSaleId = 4
WHERE
PurchaseLocationId = 2304 and
PointOfSaleId = 5398 ;

UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId = 6
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8096 ;

UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId =7
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8608 ;



UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =8
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1074 ;



UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =9
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1079 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 6,
PermitLocationId = 6,
PointOfSaleId =10
WHERE
PurchaseLocationId = 1176 and
PointOfSaleId = 174 ;



UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 7,
PermitLocationId = 7,
PointOfSaleId =11
WHERE
PurchaseLocationId = 1178 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =12
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =13
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 177 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =14
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 673 ;

UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =15
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 680 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =16
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 672 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =17
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 677 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =18
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 3835 ;


UPDATE W_PPPDetailHour
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =24
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 10229 ;

UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 1
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8071 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 2
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8072 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 3
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8764 ;

UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 10,
PermitLocationId = 10,
PointOfSaleId = 4
WHERE
PurchaseLocationId = 2304 and
PointOfSaleId = 5398 ;

UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId = 6
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8096 ;

UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId =7
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8608 ;



UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =8
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1074 ;



UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =9
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1079 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 6,
PermitLocationId = 6,
PointOfSaleId =10
WHERE
PurchaseLocationId = 1176 and
PointOfSaleId = 174 ;



UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 7,
PermitLocationId = 7,
PointOfSaleId =11
WHERE
PurchaseLocationId = 1178 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =12
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =13
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 177 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =14
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 673 ;

UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =15
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 680 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =16
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 672 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =17
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 677 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =18
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 3835 ;


UPDATE W_PPPDetailDay
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =24
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 10229 ;

UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 1
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8071 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 2
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8072 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 12,
PermitLocationId = 12,
PointOfSaleId = 3
WHERE
PurchaseLocationId = 2763 and
PointOfSaleId = 8764 ;

UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 10,
PermitLocationId = 10,
PointOfSaleId = 4
WHERE
PurchaseLocationId = 2304 and
PointOfSaleId = 5398 ;

UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId = 6
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8096 ;

UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 11,
PermitLocationId = 11,
PointOfSaleId =7
WHERE
PurchaseLocationId = 2764 and
PointOfSaleId = 8608 ;



UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =8
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1074 ;



UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 9,
PermitLocationId = 9,
PointOfSaleId =9
WHERE
PurchaseLocationId = 2295 and
PointOfSaleId = 1079 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 6,
PermitLocationId = 6,
PointOfSaleId =10
WHERE
PurchaseLocationId = 1176 and
PointOfSaleId = 174 ;



UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 7,
PermitLocationId = 7,
PointOfSaleId =11
WHERE
PurchaseLocationId = 1178 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =12
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 552 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 8,
PermitLocationId = 8,
PointOfSaleId =13
WHERE
PurchaseLocationId = 1180 and
PointOfSaleId = 177 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =14
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 673 ;

UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =15
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 680 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =16
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 672 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 14,
PermitLocationId = 14,
PointOfSaleId =17
WHERE
PurchaseLocationId = 1971 and
PointOfSaleId = 677 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =18
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 3835 ;


UPDATE W_PPPDetailMonth
SET 
PurchaseLocationId = 3,
PermitLocationId = 3,
PointOfSaleId =24
WHERE
PurchaseLocationId = 1115 and
PointOfSaleId = 10229 ;


UPDATE W_OccupancyHour
SET 
CustomerID = 3 ,
LocationID =  12 
WHERE 
LocationID = 2763 AND
CustomerID = 216  ;


UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  12
WHERE 
LocationID = 2763 AND
CustomerID = 216  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  12
WHERE 
LocationID = 2763 AND
CustomerID = 216  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  12
WHERE 
LocationID = 2763 AND
CustomerID = 216  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  12
WHERE 
LocationID = 2763 AND
CustomerID = 216  ;

UPDATE W_OccupancyHour
SET 
CustomerID = 3,
LocationID =  10
WHERE 
LocationID = 2304 AND
CustomerID = 163  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  10
WHERE 
LocationID = 2304 AND
CustomerID = 163  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  10
WHERE 
LocationID = 2304 AND
CustomerID = 163  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  10
WHERE 
LocationID = 2304 AND
CustomerID = 163  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  10
WHERE 
LocationID = 2304 AND
CustomerID = 163  ;


UPDATE W_OccupancyHour
SET 
CustomerID = 3,
LocationID =  11
WHERE 
LocationID = 2764 AND
CustomerID = 216  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  11
WHERE 
LocationID = 2764 AND
CustomerID = 216  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  11
WHERE 
LocationID = 2764 AND
CustomerID = 216  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  11
WHERE 
LocationID = 2764 AND
CustomerID = 216  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  11
WHERE 
LocationID = 2764 AND
CustomerID = 216  ;


UPDATE W_OccupancyHour
SET 
CustomerID = 3,
LocationID =  9
WHERE 
LocationID = 2295 AND
CustomerID = 163  ;


UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  9
WHERE 
LocationID = 2295 AND
CustomerID = 163  ;


UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  9
WHERE 
LocationID = 2295 AND
CustomerID = 163  ;


UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  9
WHERE 
LocationID = 2295 AND
CustomerID = 163  ;


UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  9
WHERE 
LocationID = 2295 AND
CustomerID = 163  ;


UPDATE W_OccupancyHour
SET 
CustomerID = 3,
LocationID =  6
WHERE 
LocationID = 1176 AND
CustomerID = 33  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  6
WHERE 
LocationID = 1176 AND
CustomerID = 33  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  6
WHERE 
LocationID = 1176 AND
CustomerID = 33  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  6
WHERE 
LocationID = 1176 AND
CustomerID = 33  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  6
WHERE 
LocationID = 1176 AND
CustomerID = 33  ;

UPDATE W_OccupancyHour
SET 
CustomerID = 3,
LocationID =  7
WHERE 
LocationID = 1178 AND
CustomerID = 33  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 3,
LocationID =  7
WHERE 
LocationID = 1178 AND
CustomerID = 33  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 3,
LocationID =  7
WHERE 
LocationID = 1178 AND
CustomerID = 33  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 3,
LocationID =  7
WHERE 
LocationID = 1178 AND
CustomerID = 33  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 3,
LocationID =  7
WHERE 
LocationID = 1178 AND
CustomerID = 33  ;

UPDATE W_OccupancyHour
SET 
CustomerID = 3 ,
LocationID =  8 
WHERE 
LocationID = 1180 AND
CustomerID = 33  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 3 ,
LocationID =  8 
WHERE 
LocationID = 1180 AND
CustomerID = 33  ;


UPDATE W_OccupancyMonth
SET 
CustomerID = 3 ,
LocationID =  8 
WHERE 
LocationID = 1180 AND
CustomerID = 33  ;


UPDATE W_UtilizationDay
SET 
CustomerID = 3 ,
LocationID =  8 
WHERE 
LocationID = 1180 AND
CustomerID = 33  ;


UPDATE W_UtilizationMonth
SET 
CustomerID = 3 ,
LocationID =  8 
WHERE 
LocationID = 1180 AND
CustomerID = 33  ;

UPDATE W_OccupancyHour
SET 
CustomerID = 5 ,
LocationID =  14 
WHERE 
LocationID = 1971 AND
CustomerID = 121  ;



UPDATE W_OccupancyDay
SET 
CustomerID = 5 ,
LocationID =  14 
WHERE 
LocationID = 1971 AND
CustomerID = 121  ;




UPDATE W_OccupancyMonth
SET 
CustomerID = 5 ,
LocationID =  14 
WHERE 
LocationID = 1971 AND
CustomerID = 121  ;




UPDATE W_UtilizationDay
SET 
CustomerID = 5 ,
LocationID =  14 
WHERE 
LocationID = 1971 AND
CustomerID = 121  ;



UPDATE W_UtilizationMonth
SET 
CustomerID = 5 ,
LocationID =  14 
WHERE 
LocationID = 1971 AND
CustomerID = 121  ;

UPDATE W_OccupancyHour
SET 
CustomerID = 5 ,
LocationID =  3 
WHERE 
LocationID = 1115 AND
CustomerID = 20  ;

UPDATE W_OccupancyDay
SET 
CustomerID = 5 ,
LocationID =  3 
WHERE 
LocationID = 1115 AND
CustomerID = 20  ;

UPDATE W_OccupancyMonth
SET 
CustomerID = 5 ,
LocationID =  3 
WHERE 
LocationID = 1115 AND
CustomerID = 20  ;

UPDATE W_UtilizationDay
SET 
CustomerID = 5 ,
LocationID =  3 
WHERE 
LocationID = 1115 AND
CustomerID = 20  ;

UPDATE W_UtilizationMonth
SET 
CustomerID = 5 ,
LocationID =  3 
WHERE 
LocationID = 1115 AND
CustomerID = 20  ;