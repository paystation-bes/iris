#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date

ClusterId = 1;
try:

	
	EMS7Connection = mdb.connect('localhost', 'root','root123','ems7_QA_GA')
	EMS7Cursor = EMS7Connection.cursor()
	
	# Week 1
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'Purchase','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'ProcessorTransaction','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'Permit','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PaymentCard','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'POSCollection','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PurchaseTax','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PurchaseCollection','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PaymentCardForPatrollerCard','2008-01-01 00:00:00','2008-01-07 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	EMS7Connection.commit()
		
	execfile('migrate_transaction_cluster1.py')
	
	print "Completed Migrating Transactions Week 1"

	# Week 2
	
	EMS7Connection = mdb.connect('localhost', 'root','root123','ems7_QA_GA')
	EMS7Cursor = EMS7Connection.cursor()
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'Purchase','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'ProcessorTransaction','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'Permit','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PaymentCard','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'POSCollection','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PurchaseTax','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PurchaseCollection','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	sqlproc = "call sp_GenerateMigrationDateRange(1,'PaymentCardForPatrollerCard','2008-01-08 00:00:00','2008-01-14 23:59:59',8)"
	EMS7Cursor.execute(sqlproc)
	
	EMS7Connection.commit()
		
	execfile('migrate_transaction_cluster1.py')
	
	print "Completed Migrating Transactions Week 2"




   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
