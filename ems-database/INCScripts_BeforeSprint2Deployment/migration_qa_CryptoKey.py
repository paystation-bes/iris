#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert

ClusterId = 1;
try:
	
	EMS6Connection = mdb.connect('172.16.5.14', 'ltikhova', 'Pass.word', 'ems_db_6_3_12'); 
	EMS6ConnectionCerberus = mdb.connect('172.16.5.14', 'ltikhova', 'Pass.word','cerberus_db');
	EMS7Connection = mdb.connect('172.16.5.91', 'widget','W1dg3t','ems7_QA_GA');
	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccessInsert(EMS7Connection, 1), 0)

	# Truncate table CryptoKey;
	# Truncate table CryptoKeyMapping;
	
	controller.logModuleStartStatus(ClusterId,'CryptoKey') 	
	controller.migrateCryptoKey()	
	controller.logModuleEndStatus(ClusterId,'CryptoKey') 

	print "Completed CryptoKey Migration"







   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
