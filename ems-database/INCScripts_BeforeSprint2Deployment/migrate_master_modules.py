#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert

ClusterId = 1;
try:

	
	EMS6Connection = mdb.connect('10.1.2.202', 'apamu', 'apamu', 'ems_db'); 
	EMS6ConnectionCerberus = mdb.connect('10.1.2.202', 'apamu', 'apamu','cerberus_db');
	EMS7Connection = mdb.connect('10.1.2.99', 'migration','migration','ems7_db');
	
	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccessInsert(EMS7Connection, 1), 0)

#1	

	controller.logModuleStartStatus(ClusterId,'Customers') 
	controller.migrateCustomers() #Done
	controller.logModuleEndStatus(ClusterId,'Customers') 
	
#2	
	controller.logModuleStartStatus(ClusterId,'TrialCustomer') 
	controller.migrateTrialCustomer() #Done
	controller.logModuleEndStatus(ClusterId,'TrialCustomer') 

#2.1
	controller.logModuleStartStatus(ClusterId,'UpdateCustExpiry') 
	controller.migrateCustomerExpiry() #Done
	controller.logModuleEndStatus(ClusterId,'UpdateCustExpiry') 


#3

	controller.logModuleStartStatus(ClusterId,'Locations') 
	controller.migrateLocations() #Done
	controller.logModuleEndStatus(ClusterId,'Locations') 

# 	Commented code for UnassignedLocation here. Handled in code
#4

	controller.logModuleStartStatus(ClusterId,'LotSettings') 	
	controller.migrateLotSettings() #Done
	controller.logModuleEndStatus(ClusterId,'LotSettings') 
	
# Added on Aug 22	
	controller.logModuleStartStatus(ClusterId,'LotSettingContent') 	
	controller.migrateLotSettingContent() ##Pending validation for incremental migration #Ashok: NOW
	controller.logModuleEndStatus(ClusterId,'LotSettingContent') 	

#5

	controller.logModuleStartStatus(ClusterId,'ParentRegionIdToLocation') 	
	controller.migrateParentRegionIdToLocation() #Done
	controller.logModuleEndStatus(ClusterId,'ParentRegionIdToLocation') 

#25	Copied from below. This should be before paystation as Merchant Account is necessary for MerchantPOS

	controller.logModuleStartStatus(ClusterId,'MerchantAccount') 		
	controller.migrateMerchantAccount()	#Done (InElsePart)
	controller.logModuleEndStatus(ClusterId,'MerchantAccount') 	


#6

	controller.logModuleStartStatus(ClusterId,'Paystations') 	
	controller.migratePaystations() # PointOfSale table is Migrated from within Paystation table migration  #Done
	controller.logModuleEndStatus(ClusterId,'Paystations') 

#7

	controller.logModuleStartStatus(ClusterId,'Coupons') 	
	controller.migrateCoupons() #Done
	controller.logModuleEndStatus(ClusterId,'Coupons') 

#8

	controller.logModuleStartStatus(ClusterId,'RestAccount') 	
	controller.migrateRestAccount() #Done
	controller.logModuleEndStatus(ClusterId,'RestAccount') 

#9

	controller.logModuleStartStatus(ClusterId,'ModemSettings') 	
	controller.migrateModemSettings()   #Done
	controller.logModuleEndStatus(ClusterId,'ModemSettings') 

#10

	controller.logModuleStartStatus(ClusterId,'EMSRateIntoUnifiedRate') 	
	controller.migrateEMSRateIntoUnifiedRate()  #Done
	controller.logModuleEndStatus(ClusterId,'EMSRateIntoUnifiedRate') 


#11

	controller.logModuleStartStatus(ClusterId,'CryptoKey') 	
	controller.migrateCryptoKey()	#Done
	controller.logModuleEndStatus(ClusterId,'CryptoKey') 

#12

	controller.logModuleStartStatus(ClusterId,'Rates') 	
	controller.migrateRates() #Done
	controller.logModuleEndStatus(ClusterId,'Rates') 

#13

	controller.logModuleStartStatus(ClusterId,'RestSessionToken') 	
	controller.migrateRestSessionToken() #Done
	controller.logModuleEndStatus(ClusterId,'RestSessionToken') 

#14

	controller.logModuleStartStatus(ClusterId,'RatesToEMS7ExtensibleRate') 	
	controller.migrateRatesToEMS7ExtensibleRate() #Done
	controller.logModuleEndStatus(ClusterId,'RatesToEMS7ExtensibleRate') 

#15

	controller.logModuleStartStatus(ClusterId,'CustomerSubscription') 	
	controller.migrateCustomerSubscription() #Done
	controller.logModuleEndStatus(ClusterId,'CustomerSubscription') 
	
#16

	controller.logModuleStartStatus(ClusterId,'CustomerProperties') 	
	controller.migrateCustomerProperties() #Done	
	controller.logModuleEndStatus(ClusterId,'CustomerProperties') 

#17

	controller.logModuleStartStatus(ClusterId,'CustomerWebServiceCal') 	
	controller.migrateCustomerWebServiceCal() #Done
	controller.logModuleEndStatus(ClusterId,'CustomerWebServiceCal') 

#18

	controller.logModuleStartStatus(ClusterId,'CustomerWsToken') 	
	controller.migrateCustomerWsToken() #Done
	controller.logModuleEndStatus(ClusterId,'CustomerWsToken') 	

#20

	controller.logModuleStartStatus(ClusterId,'CustomerCardType') 	
	controller.migrateCustomerCardType() #Done
	controller.logModuleEndStatus(ClusterId,'CustomerCardType') 	


#21

	controller.logModuleStartStatus(ClusterId,'QueryStallsCustomerProperty') 	
	controller.migrateQueryStallsCustomerProperty() # Done This Module populates the CustomerProperty table for the QueryStalls by Porperty Type from Paystation table.
	controller.logModuleEndStatus(ClusterId,'QueryStallsCustomerProperty') 	
	

#22	

	controller.logModuleStartStatus(ClusterId,'BadValueCard') 		
	controller.migrateBadValueCard() #Done
	controller.logModuleEndStatus(ClusterId,'BadValueCard') 	

#23
	
	controller.logModuleStartStatus(ClusterId,'BadCreditCard') 		
	controller.migrateBadCreditCard() #Done
	controller.logModuleEndStatus(ClusterId,'BadCreditCard') 	

#24
	
	controller.logModuleStartStatus(ClusterId,'BadSmartCard') 		
	controller.migrateBadSmartCard() #Done
	controller.logModuleEndStatus(ClusterId,'BadSmartCard') 	


#25.1
	controller.logModuleStartStatus(ClusterId,'UserAccount') 		
	controller.migrateUserAccount() #Done
	controller.logModuleEndStatus(ClusterId,'UserAccount') 		


###26
	controller.logModuleStartStatus(ClusterId,'ServiceAgreement') 		
	controller.migrateServiceAgreement() #Done
	controller.logModuleEndStatus(ClusterId,'ServiceAgreement') 	

#27
	controller.logModuleStartStatus(ClusterId,'ServiceAgreedCustomer') 		
	controller.migrateServiceAgreedCustomer() #Done
	controller.logModuleEndStatus(ClusterId,'ServiceAgreedCustomer') 		
	
###28
	# JIRA 4757 CustomerEmails are not to be migrated Dec 3
	#controller.logModuleStartStatus(ClusterId,'PaystationAlertEmail') 		
	#controller.migratePaystationAlertEmail() #Done
	#controller.logModuleEndStatus(ClusterId,'PaystationAlertEmail') 		
	
#30
	controller.logModuleStartStatus(ClusterId,'POSServiceState') 		
	controller.migratePOSServiceState() #Done
	controller.logModuleEndStatus(ClusterId,'POSServiceState') 		

#31.0)  
	controller.logModuleStartStatus(ClusterId,'PaystationGroup') 		
	controller.migratePaystationGroup() #NOW
	controller.logModuleEndStatus(ClusterId,'PaystationGroup') 	

#31.1)
	controller.logModuleStartStatus(ClusterId,'POSRoute') 		
	controller.migratePOSRoute() #Done
	controller.logModuleEndStatus(ClusterId,'POSRoute') 		
	
#32
	controller.logModuleStartStatus(ClusterId,'ExtensibleRateDayOfWeek') 		
	controller.migrateExtensibleRateDayOfWeek() ## Type table not getting incrementally migrated #Done
	controller.logModuleEndStatus(ClusterId,'ExtensibleRateDayOfWeek') 		

	
#35
	controller.logModuleStartStatus(ClusterId,'ProcessorProperties') 		
	controller.migrateProcessorProperties()  #Done
	controller.logModuleEndStatus(ClusterId,'ProcessorProperties') 	
	
	
	controller.logModuleStartStatus(ClusterId,'ValidCustomCard') 	
	controller.migrateValidCustomCard()  
	controller.logModuleEndStatus(ClusterId,'ValidCustomCard') 	
	

#35.1
#	controller.migrateExtensiblePermit() #Logic error

# TRANSACTION TABLES START

#36

	controller.logModuleStartStatus(ClusterId,'LicencePlate') 		
	controller.migrateLicencePlate() #  Done 
	controller.logModuleEndStatus(ClusterId,'LicencePlate') 	

#37
	controller.logModuleStartStatus(ClusterId,'MobileNumber') 		
	controller.migrateMobileNumber() ## Pending Incremental Migration, This module will be migrted with triggers #Ashok: Done
	controller.logModuleEndStatus(ClusterId,'MobileNumber') 	

# TRANSACTION TABLES END

#	LOG TABLE START. 

	controller.logModuleStartStatus(ClusterId,'ValueCard') 		
	controller.migrateValueCardData() #Done
	controller.logModuleEndStatus(ClusterId,'ValueCard') 	

	controller.logModuleStartStatus(ClusterId,'SmartCard') 
	controller.migrateSmartCardData() #Done
	controller.logModuleEndStatus(ClusterId,'SmartCard') 	

	controller.logModuleStartStatus(ClusterId,'RegionPaystationLog') 
	controller.migrateRegionPaystationLog() #Done
	controller.logModuleEndStatus(ClusterId,'RegionPaystationLog') 	

	controller.logModuleStartStatus(ClusterId,'SMSAlert') 	
	controller.migrateSMSAlert() #Ashok: 
	controller.logModuleEndStatus(ClusterId,'SMSAlert') 		
	

	# Commented on Aug 24 for PROD as SQL is taking too much time on Ashok's computer. 
	#controller.logModuleStartStatus(ClusterId,'POSDateAuditAccess') 		
	#controller.migratePOSDateAuditAccess()
	#controller.logModuleEndStatus(ClusterId,'POSDateAuditAccess') 		

	controller.logModuleStartStatus(ClusterId,'POSDatePaystation') 		
	controller.migratePOSDatePaystation() #Completed
	controller.logModuleEndStatus(ClusterId,'POSDatePaystation') 	

	controller.logModuleStartStatus(ClusterId,'POSHeartBeat') 		
	controller.migratePOSHeartBeat() #ASHOK: 	
	controller.logModuleEndStatus(ClusterId,'POSHeartBeat') 	


#19

#	controller.logModuleStartStatus(ClusterId,'CustomerAlert') 	
#	controller.migrateCustomerAlert() #Done ## Look for the notes to migrate this module, a db change is needed
#	controller.logModuleEndStatus(ClusterId,'CustomerAlert') 	
	

# PAIR	1
	controller.logModuleStartStatus(ClusterId,'POSStatus') 		
	controller.migratePOSStatus() #Completed
	controller.logModuleEndStatus(ClusterId,'POSStatus') 		

# PAIR	1
	controller.logModuleStartStatus(ClusterId,'UpdatePOSActive') 		
	controller.migratePaystationIsActive() 
	controller.logModuleEndStatus(ClusterId,'UpdatePOSActive') 		


	controller.logModuleStartStatus(ClusterId,'EventLogNewIntoPOSAlert') 		
	controller.migrateEventLogNewIntoPOSAlert() # In progress CompletedwithCondition Removed the condition 
	controller.logModuleEndStatus(ClusterId,'EventLogNewIntoPOSAlert') 		

	
# Added this module on Aug 20
	
	controller.logModuleStartStatus(ClusterId,'ParkingPermission') 		
	controller.migrateParkingPermission() #  FK ERROR RegionID 13931 doesnot exists
	controller.logModuleEndStatus(ClusterId,'ParkingPermission') 


#34

	controller.logModuleStartStatus(ClusterId,'ParkingPermissionDayOfWeek') 		
	controller.migrateParkingPermissionDayOfWeek() #Done
	controller.logModuleEndStatus(ClusterId,'ParkingPermissionDayOfWeek') 	



# Added on Aug 21
	controller.logModuleStartStatus(ClusterId,'ThirdPartyServiceAccount') 	
	controller.migrateThirdPartyServiceAccount()
	controller.logModuleEndStatus(ClusterId,'ThirdPartyServiceAccount') 	
	
#########################################	
#	Donot RUN - Aug 13

#35 ERROR CUSTOMER ALERT TYPEID CANNOT BE NULL
#	controller.logModuleStartStatus(ClusterId,'CustomerEmail') 		
#	controller.migrateCustomerEmail() #Error 
#	controller.logModuleEndStatus(ClusterId,'CustomerEmail') 	

	

	
#	THIS IS A LOG TABLE. CAN BE MIGRATED AT THE END	


#	controller.migrateWebServiceCallLog() # NO NEED TO MIGRATE

#	controller.migrateActivePOSAlert() # NO because user defined alerts are not migratedCompletedwithCondition ###### but no data in destination table

#	controller.migrateRestLog()   #NO NEED TO MIGRATE Completed 
#	controller.migrateRestLogTotalCall()  #NO NEED TO MIGRATE


#	controller.migrateBatteryInfo() #NO NEED TO MIGRATE

#	controller.migratePaystationBalance()#NO NEED TO MIGRATE	#ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateClusterMember() #NO NEED TO MIGRATE # for cluster table to work, please disable the auto increment for the EMS7.Cluster table. The auto_increment can be enabled after the initial migration has finished, as we will need to bring all the Id's as they exist in EMS6. #ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateCollectionLock() #NO NEED TO MIGRATE #ASHOK: DID NOT EXECUTED. LATER
#	controller.logModuleStartStatus(ClusterId,'POSDate') 			
#	controller.migratePOSDateBilling()
#	controller.logModuleEndStatus(ClusterId,'POSDate') 
#3.1)	
#	controller.logModuleStartStatus(ClusterId,'UnAsgLocations') 
#	controller.migrateUnassignedLocations() #Done
#	controller.logModuleEndStatus(ClusterId,'UnAsgLocations') 	


	print "Completed Master Data Migration"







   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
