#!/usr/bin/python


# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert
from EMS6DataAccessInsert import EMS6DataAccessInsert

class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS6DataAccessInsert, EMS7DataAccess, EMS7DataAccessInsert, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS7DataAccessInsert = EMS7DataAccessInsert
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6DataAccessInsert = EMS6DataAccessInsert
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6DictCursor = EMS6Connection.cursor(mdb.cursors.DictCursor)
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6InsertCursor = EMS6Connection.cursor() # This cursor is being used for insert purpose only
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}
		
		print " .....starting incremental migration - initiated the migration class variables in the init method......."

	def __migrateCustomers(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			print " ...In __migrateCustomer method........"
			
			if (Action=='Update' and IsProcessed==0):
				self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id from Customer where Id=%s", EMS6Id)
				customer=self.__EMS6Cursor.fetchall()
				for cust in customer:
					self.__EMS7DataAccess.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)		

			elif (Action=='Insert' and IsProcessed==0):
				print "..In Insert action processing step...."
				#self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
				#customer=self.__EMS6Cursor.fetchall()
				customer= self.__EMS6DataAccessInsert.getCustomers(EMS6Id)				
				print "---The customer records count is..." , customer
				for cust in customer:
					self.__EMS7DataAccessInsert.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)
				
			elif (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
				EMS7CustomerId=self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("delete from Customer where Id=%s",EMS7CustomerId)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)		

	# Code begin for ServiceAgreedCustomer
	
	def __migrateServiceAgreedCustomer(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In ServiceAgreedCustomer migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			ServiceAgreementId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccess.UpdateCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccessInsert.addCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccessInsert.deleteCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	
	# Code end for ServiceAgreedCustomer

	def __migrateEMSRateDayOfWeek(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In EMSRateDayOfWeek migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			EMSRateId = Split[0]
			DayOfWeek= Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				for EMSRateDayOfWeek in EMSRateDayOfWeeks:
					self.__EMS7DataAccess.updateExtensibleRateDayOfWeekToEMS7(EMSRateDayOfWeek, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				for EMSRateDayOfWeek in EMSRateDayOfWeeks:
					self.__EMS7DataAccessInsert.addExtensibleRateDayOfWeekToEMS7(EMSRateDayOfWeek)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				#EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				#for EMSRateDayOfWeek in EMSRateDayOfWeeks:
				self.__EMS7DataAccess.deleteExtensibleRateDayOfWeekToEMS7(EMSRateId, DayOfWeek, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	# Code start for ServiceAgreement
	
	def __migrateServiceAgreement(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ...In ServiceAgreement migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
			
			if (Action=='Update' and IsProcessed==0):
				print "step 1"
				ServiceAgreements=self.__EMS6DataAccessInsert.getEMS6ServiceAgreementInc(EMS6Id)
				print "step 2"
				IsProcessed = 1
				for ServiceAgreement in ServiceAgreements:
					self.__EMS7DataAccess.updateServiceAgreementToEMS7(ServiceAgreement, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				print "step 3"
				ServiceAgreements=self.__EMS6DataAccessInsert.getEMS6ServiceAgreementInc(EMS6Id)
				print "step 4"
				IsProcessed = 1
				for ServiceAgreement in ServiceAgreements:
					self.__EMS7DataAccessInsert.addServiceAgreementToEMS7(ServiceAgreement, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				#EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				#for EMSRateDayOfWeek in EMSRateDayOfWeeks:
				self.__EMS7DataAccess.deleteServiceAgreementToEMS7(EMS6Id)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	# Code end for ServiceAgreement
	
	def __migrateCustomerProperties(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ...Migrating customer properties record......."
		try:
			if (Action=='Update' and IsProcessed==0):
				CustomerProperties = self.__getCustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				#CustomerProperties = self.__getCustomerProperties(EMS6Id)
				CustomerProperties= self.__EMS6DataAccessInsert.getEMS6CustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccessInsert.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7DataAccess.deleteCustomerPropertyFromEMS7(EMS6Id, Action)	
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getCustomerProperties(self,EMS6Id):
		CustomerProperty=None
		# JIRA 4764
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,case when TimeZone='' then 'GMT' else TimeZone end  as TimeZone,SMSWarningPeriod from CustomerProperties where CustomerId=%s", EMS6Id)
		if (self.__EMS6Cursor.rowcount<>0):
			CustomerProperty=self.__EMS6Cursor.fetchall()
			return CustomerProperty

	def __migrateCustomerSubscription(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...migrating customer subscription..."
		try:
			Split=MultiKeyId.split(',')
			SubscriptionTypeId=None
			CustomerId = Split[0]
			ServiceId= Split[1]
			if (ServiceId=="1"):
				SubscriptionTypeId=100
			if (ServiceId=="2"):
				SubscriptionTypeId=600
			if (ServiceId=="3"):
				SubscriptionTypeId=200
			if(ServiceId=="4"):
				SubscriptionTypeId=700
			if(ServiceId=="5"):
				SubscriptionTypeId=300
			if(ServiceId=="6"):
				SubscriptionTypeId=1000
			if(ServiceId=="7"):
				SubscriptionTypeId=1200
			if(ServiceId=="8"):
				SubscriptionTypeId=900
			if(Action=='Update' and IsProcessed==0):
				CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Insert' and IsProcessed==0):
				#CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				CustomerServiceRelation=self.__EMS6DataAccessInsert.getEMS6CustomerSubscription(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccessInsert.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS6CursorCerberus.execute("select distinct(EmsCustomerId) from cerberus_db.Customer where Id=%s",(CustomerId))
				if(self.__EMS6CursorCerberus.rowcount<>0):
					EMS6CustomerId=self.__EMS6CursorCerberus.fetchone()
				EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
				self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=0 where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getCustomerServiceRelation(self,CustomerId,ServiceId):
		CustomerServiceRelation=None
		self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId) where c.Id=%s and r.ServiceId=%s",(CustomerId,ServiceId))
		if(self.__EMS6CursorCerberus.rowcount<>0):
			CustomerServiceRelation=self.__EMS6CursorCerberus.fetchall()
		return CustomerServiceRelation

	def __migratePaystations(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				IsDeleted=0
				LastModifiedGMT=date.today()
				LastModifiedByUserId=1
				EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
						# JIRA 4757 Dec 3 Donot Migrate Customer Emails
						#self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				EMS6Paystations=self.__EMS6DataAccessInsert.getEMS6Paystation(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccessInsert.addPaystationToEMS7(paystation, Action)
						# JIRA 4757 Dec 3 Donot Migrate Customer Emails
						#self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)

					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6PaystationId(self,EMS6Id):
		EMS6Rows = None
		# Modifeid on Sep 6 and Sep 10 (added p.LockState) added on March 11 CustomCardMerchantAccountId EMS 5151
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 1 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId, \
			RegionId,LotSettingId,p.ContactURL,p.isActive,p.MerchantAccountId, p.LockState,p.CustomCardMerchantAccountId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rows = self.__EMS6Cursor.fetchall()
		return EMS6Rows

	def __migrateLocations(self, Action, EMS6Id, IsProcessed, MigrationId):
		print "in .....migrate location data........"
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6Regions=self.__getEMS6Region(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				#EMS6Regions=self.__getEMS6Region(EMS6Id)
				EMS6Regions=self.__EMS6DataAccessInsert.getEMS6Locations(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccessInsert.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
					#self.__EMS7Cursor.execute("delete from Location where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
					self.__EMS7Cursor.execute(" Update Location set IsDeleted=1 where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Region(self, EMS6Id):
		EMS6Region=None
		self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Region = self.__EMS6Cursor.fetchall()
		return EMS6Region	

	def __migrateLocationPOSLog(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " migrating Location POS Log"
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			if (Action=='Update' and IsProcessed==0):
				EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
				IsProcessed = 1
				for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
					self.__EMS7DataAccess.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()
					self.__EMS7Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				#EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
				EMS6RegionPaystationLogs=self.__EMS6DataAccessInsert.getEMS6RegionPaystationLog(EMS6Id)
				IsProcessed = 1
				for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
					self.__EMS7DataAccessInsert.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from LocationPOSLog where Id=(select distinct(EMS7LocationPOSLogId) from RegionPaystationLogMapping where EMS6RegionPaystationId=%s)",(EMS6Id))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6RegionPaystationLog(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id, RegionId, PaystationId, CreationDate from RegionPaystationLog where Id=%s", EMS6Id)
		return self.__EMS6Cursor.fetchall()

#	def __printLocation(self, Location):
#		print " -- EMS6 RegionId %s" %Location[0]
#		print " -- Version %s" %Location[1]
#		print " -- AlarmStats %s" %Location[2]
#		print " -- CustomerId %s" %Location[3]
#		print " -- Name %s" %Location[4]
#		print " -- Description %s" %Location[5]
#		print " -- ParentRegion %s" %Location[6]
#		print " -- IsDefault %s" %Location[7]
#		print " -- EMS7CustomerId %s" %Location[8]
#		print " -- EMS7LocationId %s" %Location[9]

	def __migrateCoupons(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In coupon migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
		#	print "Coupon %s" %Split[0]
			Coupon = Split[0]
		#	print "CustmerId %s" %Split[1]
			CustomerId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				EMS6Coupons=self.__EMS6DataAccessInsert.getEMS6Coupons(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccessInsert.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				# Code added on Sep 12
				#self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
				#EMS7Connection.commit()
				#self.__updateMigrationQueue(MigrationId)
				IsProcessed = 1
				self.__EMS7DataAccessInsert.deleteCouponToEMS7(Coupon,CustomerId, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Coupon(self, CustomerId, CouponId):
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled, Coupon.DollarDiscount from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null and CustomerId=%s and Coupon.Id=%s",(CustomerId,CouponId))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSetting(self,Action,EMS6Id,IsProcessed,MigrationId):
		print "...migrating lotsettings data...."
		try:
			if(Action=='Insert' and IsProcessed==0):
				#EMS6LotSettings=self.__getLotSetting(EMS6Id)
				EMS6LotSettings=self.__EMS6DataAccessInsert.getEMS6LotSettings(EMS6Id)
				for LotSetting in EMS6LotSettings:
					self.__EMS7DataAccessInsert.addLotSettingToEMS7(LotSetting, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Update' and IsProcessed==0):
				EMS6LotSettings=self.__getLotSetting(EMS6Id)
				for LotSetting in EMS6LotSettings:
					self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Delete' and IsProcessed==0):
				print "$$ EMS6Id is :", EMS6Id
								
				self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6Id))
						
				print "Record deleted from SettingsFileContent in lotsettings functionality"
				self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s",(EMS6Id))	
				print "Record deleted from SettingsFileContentMapping"
				
				# EMS-4608
				# As there is a FK to PointOfSale delete operation is causing an issue
				# In EMS7 we delete only SettingsFileContent but not SettingsFile. So same approached is implemented here
				#self.__EMS7Cursor.execute("delete from SettingsFile where Id=(select EMS7Id from SettingsFileMapping where EMS6Id=%s)",EMS6Id)
				#print "Deleted from SettingsFile"
				#self.__EMS7Cursor.execute("delete from SettingsFileMapping where EMS6Id=%s",EMS6Id)
				#print "Deleted from SettingsFileMapping"
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getLotSetting(self,EMS6Id):		
		LotSetting = None
		self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL and l.Id=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			LotSetting = self.__EMS6Cursor.fetchall()
		return LotSetting
		
	def __updateMigrationQueue(self,MigrationId):
		self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1, MigratedByProcessId='%s', MigratedDate= now() where Id=%s",(self.__processId, MigrationId))
		EMS6Connection.commit()

	def __getEMS7CustomerId(self,Id):
		self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
		row=self.__EMS7Cursor.fetchone()
		EMS7CustomerId = row[0]
		return EMS7CustomerId

	def __getEMS7LocationId(self,EMS6RegionId):
		LocationId = None
		self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(EMS6RegionId))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			LocationId = row[0]
			return LocationId

	def __getEMS7PaystationId(self,EMS6PaystationId):
		PaystationId=None
		self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PaystationId =row[0]
			return PaystationId

	def __migrateAlert(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ...migrating alert data...."
		try:
			if (Action=='Update' and IsProcessed==0):
				alerts=self.__getEMS6Alerts(EMS6Id)
				for alert in alerts:
					self.__EMS7DataAccess.addCustomerAlertToEMS7(alert,Action)
					# Jira 4757
					#self.__EMS7DataAccess.addCustomerEmail(alert, Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				#alerts=self.__getEMS6Alerts(EMS6Id)				
				alerts=self.__EMS6DataAccessInsert.getEMS6CustomerAlert(EMS6Id)
				for alert in alerts:
					self.__EMS7DataAccessInsert.addCustomerAlertToEMS7(alert,Action)
					# Jira 4757
					#self.__EMS7DataAccess.addCustomerEmail(alert, Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				print " .....deleting customer alert record...."
				self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=(select distinct(EMS7Id) from CustomerAlertTypeMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Alerts(self,EMS6Id):
		Alerts=None
		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert where Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			Alerts = self.__EMS6Cursor.fetchall()	
		return Alerts	

		#This code will be enabled if the Alerts are being deleted
#                elif (Action=='Delete' and IsProcessed==0):
 #                       self.__EMS7Cursor.execute("select distinct(EMS7AlertId) from CustomerAlertTypeMapping where EMS6AlertId=%s", EMS6Id)
  #                      EMS7CustomerId=self.__EMS7Cursor.fetchone()
#			self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=%s",EMS7CustomerId)
#			self.__EMS7Connection.commit()
#	                self.__updateMigrationQueue(MigrationId)

	def __migrateBadValueCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " migrating bad value card records....."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			AccountNumber= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
				IsProcessed = 1
				for BadValueCards in EMS6BadValueCards:
					self.__EMS7DataAccess.addBadValueCardToEMS7(BadValueCards, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
				EMS6BadValueCards=self.__EMS6DataAccessInsert.getEMS6BadValueCard(CustomerId,AccountNumber)
				IsProcessed = 1
				for BadValueCards in EMS6BadValueCards:
					self.__EMS7DataAccessInsert.addBadValueCardToEMS7(BadValueCards, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				print "...deleting bad value card data....."
				if (CustomerId<>None):
				# The section below updates the CustomerBadCard table, The card is uniquely identified by CustomerId and AccountNumber 
					CardTypeId = 3
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
						if (EMS7CustomerId):
							self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
							if(self.__EMS7Cursor.rowcount<>0):
								row = self.__EMS7Cursor.fetchone()
								EMS7CustomerCardTypeId=row[0]
								self.__EMS7Cursor.execute("select Id from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,AccountNumber))
								if(self.__EMS7Cursor.rowcount<>0):
									row =  self.__EMS7Cursor.fetchone()
									EMS7CustomerBadCardId = row[0]
									self.__EMS7Cursor.execute("delete from CustomerBadCard where Id=%s",(EMS7CustomerBadCardId))
									self.__updateMigrationQueue(MigrationId)
									self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

#		if (Action=='Delete'):
#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
#			EMS7Connection.commit()

	def __migrateBadCreditCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...Migrating bad credit card...."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			CardHash = Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6BadCreditCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
				IsProcessed = 1
				for BadCreditCard in EMS6BadCreditCards:
					self.__EMS7DataAccess.addBadCreditCardToEMS7(BadCreditCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6BadCreditCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
				EMS6BadCreditCards=self.__EMS6DataAccessInsert.getEMS6BadCreditCard(CustomerId,CardHash)
				IsProcessed = 1
				for BadCreditCard in EMS6BadCreditCards:
					self.__EMS7DataAccessInsert.addBadCreditCardToEMS7(BadCreditCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete'):
				if(CustomerId):
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
					#This is the section Credit cards 
						CardTypeId = 1	
						self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
						if(self.__EMS7Cursor.rowcount<>0):
							row = self.__EMS7Cursor.fetchone()
							EMS7CustomerCardTypeId=row[0]
							self.__EMS7Cursor.execute("delete from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,CardHash))
							self.__EMS7Connection.commit()
							self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)


	def __migrateBadSmartCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " migrating bad smart cards....."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			CardNumber= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BadSmartCards=self.__getEMS6BadSmartCard(CustomerId,CardNumber)
				IsProcessed = 1
				for BadSmartCard in EMS6BadSmartCards:
					self.__EMS7DataAccess.addBadSmartCardToEMS7(BadSmartCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6BadSmartCards=self.__getEMS6BadSmartCard(CustomerId,CardNumber)
				EMS6BadSmartCards=self.__EMS6DataAccessInsert.getEMS6BadSmartCard(CustomerId,CardNumber)
				IsProcessed = 1
				for BadSmartCard in EMS6BadSmartCards:
					self.__EMS7DataAccessInsert.addBadSmartCardToEMS7(BadSmartCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete'):
				if (CustomerId<>None):
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
					# This is the section bad Smart Cards 
						CardTypeId = 2
						self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
						if(self.__EMS7Cursor.rowcount<>0):
							row = self.__EMS7Cursor.fetchone()
							EMS7CustomerCardTypeId=row[0]
							self.__EMS7Cursor.execute("delete from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,CardNumber))
							self.__EMS7Connection.commit()
	#		if (Action=='Delete'):
	#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
	#			EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6BadCreditCard(self, CustomerId, CardHash):
		self.__EMS6Cursor.execute("select CustomerId,CardHash,CardData,CardExpiry,AddedDate,Comment from BadCreditCard where CustomerId=%s and CardHash=%s",(CustomerId,CardHash))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadValueCard(self, CustomerId, AccountNumber):
		self.__EMS6Cursor.execute("select CustomerId,AccountNumber,version,CardType from BadCard where CustomerId=%s and AccountNumber=%s",(CustomerId,AccountNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadSmartCard(self, CustomerId, CardNumber):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,AddedDate,Comment from BadSmartCard where CustomerId=%s and CardNumber=%s",(CustomerId, CardNumber))
		return self.__EMS6Cursor.fetchall()

	def __migrateBatteryInfo(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " .....migrating battery-info records....."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DateField= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccess.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				EMS6BatteryInfos=self.__EMS6DataAccessInsert.getBatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccessInsert.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId = row[0]
					self.__EMS7Cursor.execute("delete from POSBatteryInfo where PointOfSaleId=%s and SensorGMT=%s",(PointOfSaleId,DateField))
					self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6BatteryInfo(self,PaystationId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo where PaystationId=%s and DateField=%s",(PaystationId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateCardRetryTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId):
		print " ....migrating card retry transactions......"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId=Split[0]           
			print "PaystationId %s" %PaystationId 
			PurchasedDate=Split[1]           
			print "PurchasedDate %s" %PurchasedDate
			TicketNumber=Split[2]            
			print "TicketNumber %s" %TicketNumber

			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				self.__EMS7DataAccess.addCardRetryTransactionToEMS7(MultiKeyId,Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
				EMS6CardRetryTransactions = self.__EMS6DataAccessInsert.getEMS6CardRetryTransaction(PaystationId,PurchasedDate,TicketNumber)
				#print EMS6CardRetryTransactions[0] #commented to resolve tuple index out of range
				if(EMS6CardRetryTransactions):
					for EMS6CardRetryTransaction in EMS6CardRetryTransactions:
						self.__EMS7DataAccessInsert.addCardRetryTransactionToEMS7(EMS6CardRetryTransaction,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
	#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
	#			if(EMS6CardRetryTransactions):
				self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CardRetryTransactions(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID from CardRetryTransaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			row = self.__EMS6Cursor.fetchall()
		return row

	def __migrateEMSExtensiblePermit(self, Action, MultiKeyId, IsProcessed, MigrationId):
		print " ....migrating extensible permit data...."
		try:
			print " -- Action %s" %Action
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s",(MultiKeyId))
				ExtensiblePermits = self.__EMS6Cursor.fetchall()
				IsProcessed = 1
				for ExtensiblePermit in ExtensiblePermits:
					self.__EMS7DataAccess.addExtensiblePermitToEMS7(ExtensiblePermit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				#self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s", MultiKeyId)
				#ExtensiblePermits = self.__EMS6Cursor.fetchall()
				ExtensiblePermits = self.__EMS6DataAccessInsert.getEMS6ExtensiblePermit(MultiKeyId)
				IsProcessed = 1
				for ExtensiblePermit in ExtensiblePermits:
					self.__EMS7DataAccessInsert.addExtensiblePermitToEMS7(ExtensiblePermit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __migrateCryptoKey(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print" ...migrating CryptoKEy....."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			Type = Split[0]
			KeyIndex = Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
				IsProcessed = 1
				for CryptoKey in EMS6CryptoKeys:
					self.__EMS7DataAccess.addCryptoKeyToEMS7(CryptoKey, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
				EMS6CryptoKeys=self.__EMS6DataAccessInsert.getEMS6CryptoKey(Type,KeyIndex)
				IsProcessed = 1
				for CryptoKey in EMS6CryptoKeys:
					self.__EMS7DataAccessInsert.addCryptoKeyToEMS7(CryptoKey, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CryptoKey where Id=(select EMS7Id from CryptoKeyMapping where Type=%s and KeyIndex=%s)",(Type,KeyIndex))	
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CryptoKey(self,Type,KeyIndex):
		self.__EMS6Cursor.execute("select Type,KeyIndex,Hash,Expiry,Signature,Info,NextHash,Status,CreateDate,Comment from CryptoKey where Type=%s and KeyIndex=%s",(Type,KeyIndex))
		return self.__EMS6Cursor.fetchall()

#EMS6.CustomerWsCal= EMS7.CustomerWebServiceCal
	def __migrateCustomerWsCal(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " ..migrating customerwscall record..."
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				CustomerWsCals=self.__getEMS6CustomerWsCal(EMS6Id)
				IsProcessed = 1
				for CustomerWsCal in CustomerWsCals:
					self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Insert' and IsProcessed==0):
				#CustomerWsCals=self.__getEMS6CustomerWsCal(EMS6Id)
				CustomerWsCals=self.__EMS6DataAccessInsert.getEMS6CustomerWebServiceCal(EMS6Id)
				IsProcessed = 1
				for CustomerWsCal in CustomerWsCals:
					self.__EMS7DataAccessInsert.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				print " ....Deleting customerWScall data...."
				self.__EMS7Cursor.execute("delete from CustomerWebServiceCal where Id = (select EMS7Id from CustomerWebServiceCalMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CustomerWsCal(self,EMS6Id):	
		CustomerWsCal=None
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal where Id=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			CustomerWsCal = self.__EMS6Cursor.fetchall()
		return CustomerWsCal		
			
## EMS6.CustomerWsToken = EMS7.WebServiceEndPoint
	def __migrateCustomerWsToken(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				CustomerWsToken=self.__getEMS6CustomerWsToken(EMS6Id)
				IsProcessed = 1
				for CustomerWsToken in CustomerWsToken:
					self.__EMS7DataAccess.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#CustomerWsTokens=self.__getEMS6CustomerWsToken(EMS6Id)
				CustomerWsTokens=self.__EMS6DataAccessInsert.getEMS6CustomerWsToken(EMS6Id)
				IsProcessed = 1
				for CustomerWsToken in CustomerWsTokens:
					self.__EMS7DataAccessInsert.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from WebServiceEndPoint where Id = (select distinct(EMS7Id) from CustomerWsTokenMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CustomerWsToken(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,Token,EndPointId,WsInUse,Id from CustomerWsToken where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMSParkingPermission(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " migrating parkingPermissions"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSParkingPermissions=self.__getParkingPermission(EMS6Id)
				IsProcessed = 1
				for EMSParkingPermission in EMS6EMSParkingPermissions:
					self.__EMS7DataAccess.addParkingPermissionToEMS7(EMSParkingPermission, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6EMSParkingPermissions=self.__getParkingPermission(EMS6Id)
				EMS6EMSParkingPermissions=self.__EMS6DataAccessInsert.getParkingPermission(EMS6Id)
				IsProcessed = 1
				for EMSParkingPermission in EMS6EMSParkingPermissions:
					self.__EMS7DataAccessInsert.addParkingPermissionToEMS7(EMSParkingPermission, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	
	# This is old method. It expects a multikey input but the table has a single column primary key
	
 	def __migrateEMSParkingPermission_old_not_used(self,Action,IsProcessed,MigrationId,MultiKeyId):
 		print " migrating parkingPermissions"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RegionId = Split[0]
			StartTimeHourLocal = Split[1]
			StartTimeMinuteLocal = Split[2]

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
				IsProcessed = 1
				for EMSParkingPermission in EMS6EMSParkingPermissions:
					self.__EMS7DataAccess.addParkingPermissionToEMS7(EMSParkingPermission, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
			     EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
			     IsProcessed = 1
			     for EMSParkingPermission in EMS6EMSParkingPermissions:
				self.__EMS7DataAccessInsert.addParkingPermissionToEMS7(self, EMSParkingPermission, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getParkingPermission(self,EMS6Id):
		self.__EMS6Cursor.execute("select Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,PermissionTypeId,PermissionStatus,SpecialParkingPermissionId,MaxDurationMinutes,CreationDate,IsActive,Id from EMSParkingPermission where Id = %s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

## This Module only adds EMSRate to Unified Rate, The module next to it does the incremental migration of the EMSRates into Extensiblerate.
	def __migrateEMSRateIntoUnifiedRate(self,Action,IsProcessed,MigrationId,EMS6Id,MultiKeyId):
		# Jan 20 EMS-???? Passing MultiKeyId as Input parameter and replacing in palce of EMS6Id
		print " Migrating EMS rates to unified rate "
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			self.__migrateEMS7ExtensibleRate(Action,IsProcessed,MigrationId,MultiKeyId)

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6EMSRates(MultiKeyId)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6EMSRates=self.__getEMS6EMSRates(EMS6Id)
				EMS6EMSRates=self.__EMS6DataAccessInsert.getEMS6EMSRates(MultiKeyId)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccessInsert.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select distinct(EMS7RateId) from EMSRateToUnifiedRateMapping where EMS6RateId=%s and EMS7RateId<>0)",MultiKeyId)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
		
	def __getEMS6EMSRates(self,EMS6Id):
#		self.__EMS6Cursor.execute("select distinct r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")
		## Above SQL revised after incremental migration
		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
#		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMS7ExtensibleRate(self,Action,IsProcessed,MigrationId,EMS6Id):
		print" in migrate EMS extensible rate method with EMS6Id...", EMS6Id
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccessInsert.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ExtensibleRate where Id=(select distinct(EMS7RateId) from ExtensibleRateMapping where EMS6RateId=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			print "-- Errror %s" %e

	def __getEMS6ExtensibleRate(self,EMS6Id):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,IsActive,Id from EMSRate where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateSMSTransactionLog(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " migrating SMSTransaction Log"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			SMSMessageTypeId = Split[3]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6SMSTransactionLogs = self.__getEMS6SMSTransactionLog(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				IsProcessed = 1
				for SMSTransactionLog in EMS6SMSTransactionLogs:
					self.__EMS7DataAccess.addSMSTransactionLogToEMS7(SMSTransactionLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6SMSTransactionLogs = self.__getEMS6SMSTransactionLog(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				EMS6SMSTransactionLogs = self.__EMS6DataAccessInsert.getEMS6SMSTransactionLog(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				IsProcessed = 1
				for SMSTransactionLog in EMS6SMSTransactionLogs:
					self.__EMS7DataAccessInsert.addSMSTransactionLogToEMS7(SMSTransactionLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				IsProcessed=1
				self.__EMS7Cursor.execute("delete from SmsTransactionLog where Id=(select Id from SMSTransactionLogMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s)",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6SMSTransactionLog(self, PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId):
		self.__EMS6Cursor.execute("select SMSMessageTypeId,MobileNumber,PaystationId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp from SMSTransactionLog where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
		return self.__EMS6Cursor.fetchall()

	def __migrateEventLogNew(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ....Migrating eventLogNew records...."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DeviceId = Split[1]
			TypeId = Split[2]
			ActionId = Split[3]
			DateField = Split[4]

			if (Action=='Update' and IsProcessed==0):
				EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccess.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				EMS6EventLogNews=self.__EMS6DataAccessInsert.getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccessInsert.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
		
	def __getEventLogNew(self, PaystationId,DeviceId,TypeId,ActionId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,ClearDateField from EventLogNew where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",(PaystationId,DeviceId,TypeId,ActionId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSettingFileContent(self,Action,EMS6Id,IsProcessed,MigrationId):
			print " ...in migrating lotsettingFileContent..."
			try:
				if (Action=='Update' and IsProcessed==0):
					LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
					for LotSettingFileContent in LotSettingFileContents:
						self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
						self.__updateMigrationQueue(MigrationId)	
				elif (Action=='Insert' and IsProcessed==0):
					#LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
					#Commented on Sep 10
					#LotSettingFileContents=self.__EMS6DataAccessInsert.LotSettingContent(EMS6Id)
					LotSettingFileContents=self.__EMS6DataAccessInsert.getEMS6LotSettingContent(EMS6Id)
					
					for LotSettingFileContent in LotSettingFileContents:
						self.__EMS7DataAccessInsert.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
						self.__updateMigrationQueue(MigrationId)
				elif (Action=='Delete' and IsProcessed==0):
					#Split=EMS6Id.split(',')
					#EMS6IdLotsetting = EMS6Id
					print "In Delete"
					print "EMS6Id is :",EMS6Id
					SettingsFileContentMappings =self.__getEMS7SettingsFileContentMapping(EMS6Id)
					for SettingsFileContentMapping in SettingsFileContentMappings:
						print " $$ SettingsFileContentMapping is ", SettingsFileContentMapping
						sqlString = "delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(SettingsFileContentMapping)
						print sqlString
						
						
						self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(SettingsFileContentMapping))
						
						print "Record deleted from SettingsFileContent"
						self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s",(SettingsFileContentMapping))	
						print "Record deleted from SettingsFileContentMapping"
						
						self.__updateMigrationQueue(MigrationId)
						EMS7Connection.commit()
					#print "$$ EMS6Id in __migrateLotSettingFileContent Delete is :",EMS6IdLotsetting
					#mySql = "delete from SettingsFileContent where SettingsFileId = (select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting)
					#print mySql
					#self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId = (select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting))
					#self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting))
					#self.__updateMigrationQueue(MigrationId)
					
				
			except (Exception, mdb.Error), e:
				self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
						
	def __getEMS6LotSettingFileContent(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __getEMS7SettingsFileContentMapping(self, EMS6Id):
		self.__EMS7Cursor.execute("select EMS6Id from SettingsFileContentMapping where EMS6Id=%s",(EMS6Id))
		return self.__EMS7Cursor.fetchall()
		
	def __migrateMerchantAccount(self,Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id):
		print" ....Migrating merchataccount......."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- EMS6Id %s" %EMS6Id
		       
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				self.__EMS7DataAccess.addMerchantAccount(MultiKeyId, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6MerchantAccounts=self.__getEMS6MerchantAccount(EMS6Id)
				EMS6MerchantAccounts=self.__EMS6DataAccessInsert.getMerchantAccount(EMS6Id)
				IsProcessed = 1
				for EMS6Merchant in EMS6MerchantAccounts:
					self.__EMS7DataAccessInsert.addMerchantAccount(EMS6Merchant, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from MerchantAccount where Id=(select distinct(EMS7MerchantAccountId) from MerchantAccountMapping where EMS6MerchantAccountId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
                        

	def __getEMS6MerchantAccount(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id,version,CustomerId,Name,Field1,Field2,Field3,IsDeleted,ProcessorName,ReferenceCounter,Field4,Field5,Field6,IsValid from MerchantAccount where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateServiceState(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " ..migrating service state record...."
		print " -- Action %s" %Action
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId
		print " -- EMS6Id %s" %EMS6Id
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				EMS6ServiceStates=self.__EMS6DataAccessInsert.getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccessInsert.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				print " ......deleting Service State record....."
				self.__EMS7Cursor.execute("delete from POSServiceState where PointOfSaleId=(select po.Id from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6ServiceState(self,EMS6Id):
		ServiceState=None
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			ServiceState=self.__EMS6Cursor.fetchall()
		return ServiceState

	def __migrateRestAccount(self, Action, MultiKeyId, IsProcessed, MigrationId):
		print"....migrating RESTaccount..."
		try:
			print " -- Action %s" %Action
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)

			elif(Action=='Insert' and IsProcessed==0):
				#RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				RestAccounts=self.__EMS6DataAccessInsert.getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccessInsert.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
				IsProcessed = 1
	#			self.__EMS7Cursor.execute("delete from RestAccount where Id=(select EMS7RestAccountId from RestAccountMapping where EMS6AccountName=%s",(MultiKeyId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

	   	except (Exception, mdb.Error), e:
	   		self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6RestAccount(self, MultiKeyId):
		self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null and r.AccountName=%s",(MultiKeyId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRates(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
	#		print " -- MultiKeyId %s" %EMS6Id
			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			print " PaystationId is %s" %PaystationId
			PurchasedDate = Split[1]
			print " Purchasedate is %s" %PurchasedDate
			TicketNumber = Split[2]
			print " TicketNumeber %s" %TicketNumber
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
				for Rates in EMS6Rates:
					self.__EMS7DataAccess.addRatesToEMS7(Rates, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
				EMS6Rates=self.__EMS6DataAccessInsert.getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
				for rates in EMS6Rates:
					self.__EMS7DataAccessInsert.addRatesToEMS7(rates, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				IsProcessed = 1
				self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select EMS7UnifiedRateId from RateToUnifiedRateMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

#	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
#		EMS6Rates=None	
#		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		if(self.__EMS6Cursor.rowcount<>0):
#			EMS6Rates=self.__EMS6Cursor.fetchall()
	#	return EMS6Rates
#		else:
#			print " no row was found"
#		return EMS6Rates

	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
		EMS6Rates=None
		#self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select PaystationId,TicketNumber,PurchasedDate,CustomerId,RegionId,LotNumber,TypeId,RateId,RateName,RateValue,Revenue from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rates=self.__EMS6Cursor.fetchall()
		return EMS6Rates

	def __migratePaystationGroup(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " ....migrating PlaystationGroup data......"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
		       
			if (Action=='Update' and IsProcessed==0):
				EMS6PaystationGroups = self.__getEMS6PaystationGroup(EMS6Id)
				IsProcessed = 1
				for PaystationGroup in EMS6PaystationGroups:
					self.__EMS7DataAccess.addPaystationGroupToEMS7(PaystationGroup, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6PaystationGroups = self.__getEMS6PaystationGroup(EMS6Id)
				EMS6PaystationGroups = self.__EMS6DataAccessInsert.getEMS6PaystationGroup(EMS6Id)
				IsProcessed = 1
				for PaystationGroup in EMS6PaystationGroups:
					self.__EMS7DataAccessInsert.addPaystationGroupToEMS7(PaystationGroup, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				print "Deleting palystationGroup data"
				self.__EMS7Cursor.execute("delete from Route where Id=(select distinct(EMS7Id) from PaystationGroupRouteMapping where PaystationGroupId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6PaystationGroup(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,CustomerId,Name from PaystationGroup where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateProcessorProperties(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...migrating processor properties..."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			Processor = Split[0]
			Name = Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
				for ProcessorProperties in EMS6ProcessorProperties:
					self.__EMS7DataAccess.addProcessorProperties(ProcessorProperties, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
				EMS6ProcessorProperties=self.__EMS6DataAccessInsert.getProcessorPropertiest(Processor,Name)
				for ProcessorProperties in EMS6ProcessorProperties:
					self.__EMS7DataAccessInsert.addProcessorProperties(ProcessorProperties, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				print"...Deleting the ProcessorProperty record...."
				IsProcessed = 1
				self.__EMS7Cursor.execute("delete from ProcessorProperty where Id=(select EMS7Id from ProcessorPropertiesMapping where Processor=%s and Name=%s)",(Processor,Name))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
				print"...Deletion complete...."
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6ProcessorProperties(self, Processor,Name):
		self.__EMS6Cursor.execute("select Processor,Name,Value from ProcessorProperties where Processor=%s and Name=%s",(Processor,Name))
		return self.__EMS6Cursor.fetchall()


	def __migrateSmsAlert(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " ...Migrating SMS Alert data...."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				SMS=self.__getEMS6SMSAlert(EMS6Id)
				IsProcessed = 1
				for EMSSMS in SMS:
					self.__EMS7DataAccess.addSMSAlertToEMS7(EMSSMS, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#SMS=self.__getEMS6SMSAlert(EMS6Id)
				SMS=self.__EMS6DataAccessInsert.getEMS6SMSAlert(EMS6Id)
				IsProcessed = 1
				for EMSSMS in SMS:
					self.__EMS7DataAccessInsert.addSMSAlertToEMS7(EMSSMS, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				print "....deleting SMS Alert data..."
				self.__EMS7Cursor.execute("delete from SmsAlert where Id=(select EMS7Id from SMSAlertMapping where MobileNumber=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6SMSAlert(self,EMS6Id):
                self.__EMS6Cursor.execute("select MobileNumber,ExpiryDate,PaystationId,PurchasedDate,TicketNumber,PlateNumber,StallNumber,RegionId,NumOfRetry,IsAlerted,IsLocked,IsAutoExtended from SMSAlert where MobileNumber=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

	def __migrateSMSFailedResponse(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ......migrating SMSFailedREsponse data......."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
	#		print " -- MultiKeyId %s" %EMS6Id

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			SMSMessageTypeId = Split[3]
		       
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6FailedResponses=self.__getEMS6SMSFailedResponse(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				for EMS6FailedResponse in EMS6FailedResponses:
					self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(EMS6FailedResponse, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6FailedResponses=self.__getEMS6SMSFailedResponse(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				EMS6FailedResponses=self.__EMS6DataAccessInsert.getEMS6SMSFailedResponse(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				for EMS6FailedResponse in EMS6FailedResponses:
					self.__EMS7DataAccessInsert.addEMS6SMSFailedResponseToEMS7(EMS6FailedResponse, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				print " ...deleting SMSfailedResponse data....."
				IsProcessed = 1
				self.__EMS7Cursor.execute("select EMS7SMSFailedResponseId from SMSFailedResponseMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)) 
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7FailedResponseId = row[0]
					self.__EMS7Cursor.execute("delete from SmsFailedResponse where Id=(select EMS7SMSFailedResponseId from SMSFailedResponseMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s)",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6SMSFailedResponse(self,PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId):	
		SMSFailedReponse = None	
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,IsOk,TrackingId,Number,ConvertedNumber,DeferUntilOccursInThePast,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,BlockedReason,IsBalanceZero,IsInvalidCarrierCode from SMSFailedResponse where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
		if(self.__EMS6Cursor.rowcount<>0):
			SMSFailedReponse = self.__EMS6Cursor.fetchall()
		return SMSFailedReponse

	def __migrateModemSetting(self,Action,IsProcessed,MigrationId,EMS6Id):
		print"..migrating modem settings data..."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccess.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				ModemSettings=self.__EMS6DataAccessInsert.getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccessInsert.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ModemSetting where Id=(select EMS7Id from ModemSettingMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6ModemSettings(self, EMS6Id):
		EMS6ModemSetting=None
		self.__EMS6Cursor.execute("select PaystationId, Type, CCID, Carrier, APN from ModemSetting where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6ModemSetting = self.__EMS6Cursor.fetchall()
		return EMS6ModemSetting 

#	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
#		EMS6Rates=None	
#		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		if(self.__EMS6Cursor.rowcount<>0):
#			EMS6Rates=self.__EMS6Cursor.fetchall()
	#	return EMS6Rates
#		else:
#			print " no row was found"
#		return EMS6Rates

	def __migrateUserAccount(self,Action,IsProcessed,MigrationId,EMS6Id):
		print ".....Migrating user account data....." 
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				UserAccounts=self.__getEMS6UserAccount(EMS6Id)
				IsProcessed = 1
				for UserAccount in UserAccounts:
					self.__EMS7DataAccess.addUserAccountToEMS7(UserAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#UserAccounts=self.__getEMS6UserAccount(EMS6Id)
				UserAccounts=self.__EMS6DataAccessInsert.getEMS6UserAccount(EMS6Id)
				IsProcessed = 1
				for UserAccount in UserAccounts:
					self.__EMS7DataAccessInsert.addUserAccountToEMS7(UserAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from UserAccount where Id=(select distinct(EMS7Id) from UserAccountMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6UserAccount(self, EMS6Id):
		self.__EMS6Cursor.execute("select u.Id,u.version,u.CustomerId,u.Name,u.Password,u.RoleId,u.AccountStatus, concat(u.Name,'@',c.Name) as UserAccount,u.Id, 'LastName' from UserAccount u left join Customer c on(c.Id=u.CustomerId) where u.Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateAudit(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ......migrating audit data......"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			CollectionTypeId = Split[1]
			EndDate = Split[2]
			StartDate = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
				IsProcessed = 1
				for Audit in EMS6Audit:
					self.__EMS7DataAccess.addPOSCollectionToEMS7(Audit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
				EMS6Audit=self.__EMS6DataAccessInsert.getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
				IsProcessed = 1
				for Audit in EMS6Audit:
					self.__EMS7DataAccessInsert.addPOSCollectionToEMS7(Audit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select Id from PointOfSale where PaystationId=(select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s)",(PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId = row[0]	
				self.__EMS7Cursor.execute("delete from POSCollection where PointOfSaleId=%s and CollectionTypeId=%s and StartGMT=%s and EndGMT=%s",(PointOfSaleId,CollectionTypeId,StartDate,EndDate))
				self.__EMS7Connection.commit() 
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Audit(self, PaystationId,CollectionTypeId,EndDate,StartDate):
#		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CustomerId from Audit")
		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CoinAmount,OnesAmount,TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DiscoverAmount,MasterCardAmount,OtherCardAmount,SmartCardAmount,VisaAmount,CitationsPaid,CitationAmount,ChangeIssued,RefundIssued,SmartCardRechargeAmount,ExcessPayment,CustomerId,ReportNumber,NextTicketNumber,AttendantTicketsSold,AttendantTicketsAmount,DinersAmount,AttendantDepositAmount,AcceptedFloatAmount,Tube1Type,Tube1Amount,Tube2Type,Tube2Amount,Tube3Type,Tube3Amount,Tube4Type,Tube4Amount,OverfillAmount,ReplenishedAmount,Hopper1Dispensed,Hopper2Dispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Replenished,Hopper2Replenished,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,CoinCount005,CoinCount010,CoinCount025,CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50 from Audit where PaystationId=%s and CollectionTypeId=%s and EndDate=%s and StartDate=%s",(PaystationId,CollectionTypeId,EndDate,StartDate))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransaction2Push(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			ThirdPartyServiceAccountId = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
				IsProcessed = 1
				for Transaction2Push in EMS6Transaction2Push:
					self.__EMS7DataAccess.addTransaction2Push(self, Transaction2Push, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
				IsProcessed = 1
				for Transaction2Push in EMS6Transaction2Push:
					self.__EMS7DataAccessInsert.addTransaction2Push(self, Transaction2Push, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			print " --Error %s" %e

	def __getEMS6Transaction2Push(self, PaystationId, PurchasedDate, TicketNumber, ThirdPartyServiceAccountId):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RegionId,RetryCount,LastRetryTime,IsOffline,Field1 from Transaction2Push where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and ThirdPartyServiceAccountId=%s",(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransactionPush(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			ThirdPartyServiceAccountId = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
				IsProcessed = 1
				for TransactionPush in EMS6TransactionPush:
					self.__EMS7DataAccess.addTransactionPush(self, TransactionPush, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
				IsProcessed = 1
				for TransactionPush in EMS6TransactionPush:
					self.__EMS7DataAccessInsert.addTransactionPush(self, TransactionPush, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from TransactionPush where Id=(select EMS7Id from TransactionPushMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			print " --Error %s" %e

	def __getEMS6TransactionPush(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId, PurchasedDate, TicketNumber, CustomerId, LotNumber, PlateNumber, ExpiryDate, ChargedAmount, StallNumber, RetryCount, LastRetryTime, IsOffline from TransactionPush where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

#	def __migrateCustomerWsCal(self,Action,IsProcessed,MigrationId,EMS6Id):
 #               print " -- Action %s" %Action
  #              print " -- IsProcessed %s" %IsProcessed
   #             print " -- MigrationId %s" %MigrationId
    #            print " -- EMS6Id %s" %EMS6Id

     #           if (Action=='Update' and IsProcessed==0):
     #                   CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
      #                  IsProcessed = 1
       #                 for WebService in CustomerWsCal:
        #                        self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
         #                       self.__updateMigrationQueue(MigrationId)
          #                      self.__EMS7Connection.commit()

           #     if (Action=='Insert' and IsProcessed==0):
            #            CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
#                        IsProcessed = 1
 #                       for WebService in CustomerWsCal:
  #                              self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
   #                             self.__updateMigrationQueue(MigrationId)
    #                            self.__EMS7Connection.commit()

     #           if(Action=='Delete' and IsProcessed==0):
      #                  self.__EMS7Cursor.execute("delete from CustomerWsCalMapping where Id=(select EMS7Id from CustomerWsCalMapping where EMS6Id=%s",(EMS6Id))
#                        self.__EMS7Connection.commit()
 #                       self.__updateMigrationQueue(MigrationId)

	def __getEMS6CustomerWebServiceCal(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description from CustomerWsCal where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestSessionToken(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print "migrating RestSessionToken...."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %MultiKeyId

			if (Action=='Update' and IsProcessed==0):
				RestSessionToken=self.__getEMS6RestSessionToken(MultiKeyId)
				IsProcessed = 1
				for RestSession in RestSessionToken:
					self.__EMS7DataAccess.addRestSessionToken(RestSession, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				print" ....starting insert process...."
				#RestSessionToken=self.__getEMS6RestSessionToken(MultiKeyId)
				RestSessionToken=self.__EMS6DataAccessInsert.getEMS6RestSessionToken(MultiKeyId)
				IsProcessed = 1
				for RestSession in RestSessionToken:
					self.__EMS7DataAccessInsert.addRestSessionToken(RestSession, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RestSessionToken where Id=(select EMS7Id from RestSessionTokenMapping where EMS6AccountName=%s)",(MultiKeyId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6RestSessionToken(self, MultiKeyId):	
		self.__EMS6Cursor.execute("select AccountName,SessionToken,CreationDate,ExpiryDate from RESTSessionToken where AccountName=%s",(MultiKeyId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLog(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKey Id %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RESTAccountName = Split[0]
			EndpointName = Split[1]
			LoggingDate = Split[2]
			IsError = Split[3]
			CustomerId = Split[4]

			if (Action=='Update' and IsProcessed==0):
				RestLogs=self.__getEMS6RestLogs(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
				IsProcessed = 1
				for RestLog in RestLogs:
					self.__EMS7DataAccess.addRestLogToEMS7(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#RestLogs=self.__getEMS6RestLogs(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
				RestLogs=self.__EMS6DataAccessInsert.getEMS6RestLogs(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
				IsProcessed = 1
				for RestLog in RestLogs:
					self.__EMS7DataAccessInsert.addRestLogToEMS7(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RestLog where Id=(select EMS7Id from RestLogMapping where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s)",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6RestLogs(self,RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId):
		self.__EMS6Cursor.execute("select RestAccountName, EndpointName, LoggingDate, IsError, CustomerId, TotalCalls from RESTLog where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLogTotalCall(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " .......migrating RestLogTotalCall......"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKey Id %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RESTAccountName = Split[0]
			LoggingDate = Split[1]

			if (Action=='Update' and IsProcessed==0):
				RestLogTotalCall=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
				IsProcessed = 1
				for RestLog in RestLogTotalCall:
					self.__EMS7DataAccess.addRestLogTotalCall(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#RestLogTotalCalls=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
				RestLogTotalCalls=self.__EMS6DataAccessInsert.getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
				IsProcessed = 1
				for RestLog in RestLogTotalCalls:
					self.__EMS7DataAccessInsert.addRestLogTotalCall(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				print " .......Deleting the RESTLogTotalCall record...."
				self.__EMS7Cursor.execute("delete from RestLogTotalCall where Id=(select distinct(EMS7Id) from RestLogTotalCallMapping where RESTAccountName=%s and LoggingDate=%s)",(RESTAccountName,LoggingDate))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __migrateReplenish(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ......migrating replenish data....."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split = MultiKeyId.split(',')
			PaystationId = Split[0]
			Date = Split[1]
			Number = Split[2]
			if (Action=='Update' and IsProcessed==0):
				print " .....updating replenish data...."
				EMS6Replenish=self.__getEMS6Replenish(PaystationId,Date,Number)
				IsProcessed = 1
				for Replenish in EMS6Replenish:
					self.__EMS7DataAccess.addReplenishToEMS7(Replenish, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6Replenish=self.__getEMS6Replenish(PaystationId,Date,Number)
				EMS6Replenish=self.__EMS6DataAccessInsert.getEMS6Replenish(PaystationId,Date,Number)
				IsProcessed = 1
				for Replenish in EMS6Replenish:
					self.__EMS7DataAccessInsert.addReplenishToEMS7(Replenish, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				print " .......deleting replenish data....."
				self.__EMS7Cursor.execute("delete from Replenish where Id=(select distinct(EMS7Id) from ReplenishMapping where PaystationId=%s and Date=%s and Number=%s)",(PaystationId,Date,Number))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Replenish(self, PaystationId,Date,Number):
		self.__EMS6Cursor.execute("select PaystationId, Date, Number, TypeId, Tube1Type, Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount, Tube2CurrentCount, Tube3Type, Tube3ChangedCount, Tube3CurrentCount, Tube4Type, Tube4ChangedCount, Tube4CurrentCount, CoinBag005AddedCount, CoinBag010AddedCount, CoinBag025AddedCount, CoinBag100AddedCount, CoinBag200AddedCount from Replenish where PaystationId=%s and Date=%s and Number=%s",(PaystationId,Date,Number))
		return self.__EMS6Cursor.fetchall()

	def __migrateCustomerCardType(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " .....in migrating customer card type...."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
			
			if (Action=='Update' and IsProcessed==0):
				CustomerCardTypes=self.__getEMS6CustomerCardType(EMS6Id)
				IsProcessed = 1
				for CustomerCardType in CustomerCardTypes:
					self.__EMS7DataAccess.addCustomerCardTypeToEMS7(CustomerCardType, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#CustomerCardTypes=self.__getEMS6CustomerCardType(EMS6Id)
				CustomerCardTypes=self.__EMS6DataAccessInsert.getEMS6CardType(EMS6Id)
				IsProcessed = 1
				for CustomerCardType in CustomerCardTypes:
					self.__EMS7DataAccessInsert.addCustomerCardTypeToEMS7(CustomerCardType, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CustomerCardType where Id=(select EMS7Id from CustomerCardTypeMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CustomerCardType(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,version,Name,Track2RegEx,CheckDigitAlg,Description,AuthorizationMethod,Id from CardType where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6RestLogTotalCall(self,RESTAccountName,LoggingDate):	
		self.__EMS6Cursor.execute("select RESTAccountName,LoggingDate,TotalCalls from RESTLogTotalCall where RESTAccountName=%s and LoggingDate=%s",(RESTAccountName,LoggingDate))
		return self.__EMS6Cursor.fetchall()

	def __migratePaystation_PaystationGroup(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print ".....incremental migration paystation_paystationGroup data...."
		try:
			Split=MultiKeyId.split(',')
			PaystationGroupId = Split[0]
			PaystationId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				PaystationGroups=self.__getEMS6Paystation_PaystationGroup(PaystationGroupId,PaystationId)
				for Paystation in PaystationGroups:
					self.__EMS7DataAccess.addPOSRouteToEMS7(Paystation,Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				#PaystationGroups=self.__getEMS6Paystation_PaystationGroup(PaystationGroupId,PaystationId)
				PaystationGroups=self.__EMS6DataAccessInsert.getEMS6Paystation_PaystationGroup(PaystationGroupId,PaystationId)
				for Paystation in PaystationGroups:
					self.__EMS7DataAccessInsert.addPOSRouteToEMS7(Paystation,Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RoutePOS where Id=(select distinct(EMS7Id) from RoutePOSMapping where PaystationGroupId=%s and PaystationId=%s)",(PaystationGroupId,PaystationId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	def __getEMS6Paystation_PaystationGroup(self, PaystationGroupId,PaystationId):
		self.__EMS6Cursor.execute("select PaystationGroupId, PaystationId from Paystation_PaystationGroup where PaystationId=%s and PaystationGroupId=%s",(PaystationId,PaystationGroupId))
		if(self.__EMS6Cursor.rowcount<>0):
			return self.__EMS6Cursor.fetchall()

	def __migrateTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId, TableName):
		print ".......In migrating transaction data......"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- Display MultiKeyId now %s" %MultiKeyId
			print " -- TableName %s" %TableName

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			PaystationId = Split[1]
			TicketNumber = Split[2]
			LotNumber = Split[3]
			StallNumber = Split[4]
			TypeId = Split[5]
			PurchasedDate = Split[6]
			ExpiryDate = Split[7]
			ChargedAmount = Split[8]
			CashPaid = Split[9]
			CardPaid = Split[10]
			ChangeDispensed = Split[11]
			ExcessPayment = Split[12]
			IsRefundSlip = Split[13]
			CustomCardData = Split[14]
			CustomCardType = Split[15]
			CouponNumber = Split[16]
			CitationNumber = Split[17]
			SmartCardData = Split[18]
			SmartCardPaid = Split[19]
			PaymentTypeId = Split[20]
			CreationDate = Split[21]
			RateId = Split[22]
			AuthenticationCard = Split[23]
			RegionId = Split[24]
			AddTimeNum = Split[25]
			OriginalAmount = Split[26]
			IsOffline = Split[27]
			PlateNumber = Split[28]

			# This section populates the array for addValueCardToEMS7
				
			if (Action=='Update' and IsProcessed==0):
				print "Update section has been disabled on purpose, following no updates being received in transactional records"
				## For smart card and value card we never send an update so the section below is being disabled
	#			if(TableName == 'CustomerCard' or TableName =='SmartCard'):
	#				print "CustomerId %s" %CustomerId
	#				print "Regionid %s" %RegionId
	#				print "PaystationId %s" %PaystationId
	#				print "CustomCardData %s" %CustomCardData
	#				print "SmartCardData %s" %SmartCardData
	#				ValueCardToEMS7 = [CustomerId,RegionId,PaystationId,CustomCardData,SmartCardData]
	#				self.__EMS7DataAccess.addValueCardToEMS7(ValueCardToEMS7,Action)
	#				self.__updateMigrationQueue(MigrationId)
	#				self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				if(TableName=='CustomerCard'):
					print "... migrating data from customercard table..."
	#			Adding a module to Add CustomerValueCard to the Transaction table
					if(CustomCardData):
						print "customercard number is:", CustomCardData
						ValueCards = self.__getEMS6ValueSmartCardType(PaystationId, PurchasedDate,TicketNumber)
						if(ValueCards):
							for valueCard in ValueCards:
								self.__EMS7DataAccessInsert.addValueCardToEMS7(valueCard,Action)
								self.__updateMigrationQueue(MigrationId)
								self.__EMS7Connection.commit()
				if(TableName=='SmartCard'):
					print " ...inserting data from smart card table..."
					if(SmartCardData):
						SmartCards = self.__getEMS6SmartCardType(PaystationId,PurchasedDate,TicketNumber)
						for smartcard in SmartCards:
							print "Customer Id %s" %smartcard[0]
							print "ResgionID %s" %smartcard[1]
							print "PaystationId %s" %smartcard[2]
							print "SmartCard %s" %smartcard[4]
							self.__EMS7DataAccessInsert.addSmartCardToEMS7(smartcard, Action)
							self.__updateMigrationQueue(MigrationId)
							self.__EMS7Connection.commit()
				if(TableName=='Purchase'):
					print " inserting data from purchase table..Inside the Purchase Section >>>>>>>>"
					print " PaystationId %s" %PaystationId
					print " PurchasedDate %s" %PurchasedDate
					print " TicketNumber %s" %TicketNumber
					Purchases = self.__getEMS6TransactionForPurchase(PaystationId,PurchasedDate,TicketNumber)
					for purchase in Purchases:
						self.__EMS7DataAccessInsert.addPurchaseToEMS7(purchase,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()
				if(TableName=='Permit'):
					print " inserting record from ...Inside the Permit table  >>>>>"
					Permits = self.__getEMS6TransactionForPermit(PaystationId,PurchasedDate,TicketNumber)
					for permit in Permits:
						self.__EMS7DataAccessInsert.addPermitToEMS7(permit,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()
				if(TableName=='PaymentCard'):
					print " inserting data from paymentCard table...Inside the PaymentCard table  >>>>"
					print " PaystationId %s" %PaystationId
					print " PurchasedDate %s" %PurchasedDate
					print " TicketNumber %s" %TicketNumber
					PaymentCards = self.__getEMS6TransactionForPayment(PaystationId,PurchasedDate,TicketNumber)
					for payment in PaymentCards:
						self.__EMS7DataAccessInsert.addPaymentCardToEMS7(payment,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				# There are is no update and delete being done to transactional data containing smart card and value card
				print "Section disabled following no deletes for transaction record"
	#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
	#			if(EMS6CardRetryTransactions):
			#	self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
			#	EMS7Connection.commit()
			#	self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6TransactionForPayment(self, PaystationId,PurchasedDate,TicketNumber):
		#Rewrite the query to lookup TransactionProcessorId for every transaction that comes up, make sure there is a single Processor transaction id returned (SubQuery) as there could be multiple processor transaction record for a single transaction, we need to find a unique processor transaction record by looking up the max(procssing date) in processor transaction table
		#self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,pt.MerchantAccountId,pt.Amount,pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,pt.Approved,t.SmartCardData,t.SmartCardPaid,t.CustomCardData,t.CustomCardType,t.CustomerId,max(pt.ProcessingDate) from trust t left join ProcessorTransaction pt using (PaystationId,PurchasedDate,TicketNumber) where t.PaystationId=%s and Approved=1 and t.PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,pt.MerchantAccountId,pt.Amount, \
			pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,pt.Approved,t.SmartCardData,t.SmartCardPaid,t.CustomCardData, \
			t.CustomCardType,t.CustomerId,pt.ProcessingDate from Transaction t left join ProcessorTransaction pt using \
			(PaystationId,PurchasedDate,TicketNumber) where t.PaystationId=%s and t.PurchasedDate=%s and \
			TicketNumber=%s and pt.ProcessingDate = ( SELECT max(ProcessingDate) from ProcessorTransaction where PaystationId= %s  and \
			PurchasedDate= %s  and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber,PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall() 

	def __getEMS6TransactionForPermit(self, PaystationId,PurchasedDate,TicketNumber):
		#self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber from trust tr left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		#Aug 06 ITF
		#self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber,tr.CustomerId from Transaction tr left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber,tr.CustomerId from Transaction tr left join SMSAlert ep using (PaystationId,TicketNumber,PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate = %s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6TransactionForPurchase(self, PaystationId,PurchasedDate,TicketNumber):
#Section below commented out to merge Purchae and Permit data in one query
		# A nonexisting table trust is bring referred in this line: self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid from trust tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6ValueSmartCardType(self,PaystationId, PurchasedDate,TicketNumber):
		print "PaystationId %s" %PaystationId
		print "PurchasedDate %s" %PurchasedDate
		print "TicketNumber %s" %TicketNumber
		ValueCards = None
		#self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from trust where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and CustomCardData<>''",(PaystationId, PurchasedDate, TicketNumber))
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from Transaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and CustomCardData<>''",(PaystationId, PurchasedDate, TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			ValueCards = self.__EMS6Cursor.fetchall()	
		return ValueCards

	def __getEMS6SmartCardType(self,PaystationId,PurchasedDate,TicketNumber):
		print " PaystationId %s" %PaystationId
		print " PurchaseDate %s" %PurchasedDate
		print " TicketNumber %s" %TicketNumber

		#self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from trust where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from Transaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __migratePreAuth(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " migrating pre-auth record....."
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6PreAuths=self.__getPreAuth(EMS6Id)
				if(EMS6PreAuths):
					for preauth in EMS6PreAuths:
						self.__EMS7DataAccess.addPreAuth(preauth, Action)
						self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				#EMS6PreAuths=self.__getPreAuth(EMS6Id)
				EMS6PreAuths=self.__EMS6DataAccessInsert.getPreAuth(EMS6Id)
				if(EMS6PreAuths):
					for preauth in EMS6PreAuths:
						self.__EMS7DataAccessInsert.addPreAuth(preauth, Action)
						self.__updateMigrationQueue(MigrationId)
			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					EMS6PreAuths=self.__getPreAuth(EMS6Id)
					for preAuth in EMS6PreAuths:
						self.__EMS7Cursor.execute("select EMS7MerchantAccountId, AuthorizationNumber, ProcessorTransactionId from PreAuthMapping where EMS6MerchantAccountId =%s\
													and AuthorizationNumber = %s and ProcessorTransactionId = %s",(preAuth[5], preAuth[3], preAuth[2]))
						EMS7PreAuthMappingRow = self.__EMS7Cursor.fetchall()
						self.__EMS7Cursor.execute("delete from PreAuth where MerchantAccountId = %s and AuthorizationNumber = %s and ProcessorTransactionId = %s", (EMS7PreAuthMappingRow[0],EMS7PreAuthMappingRow[1], EMS7PreAuthMappingRow[2]))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getPreAuth(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId from PreAuth where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migratePreAuthHolding(self, Action, EMS6Id, IsProcessed, MigrationId):
		print "migrating preAuthHolding record"
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6PreAuths=self.__getPreAuthHolding(EMS6Id)
				if(EMS6PreAuths):
					for preauth in EMS6PreAuths:
						self.__EMS7DataAccess.addPreAuthHolding(preauth, Action)
						self.__updateMigrationQueue(MigrationId)
			if (Action=='Insert' and IsProcessed==0):
				#EMS6PreAuths=self.__getPreAuthHolding(EMS6Id)
				EMS6PreAuths=self.__EMS6DataAccessInsert.getPreAuthHolding(EMS6Id)
				if(EMS6PreAuths):
					for preauth in EMS6PreAuths:
						self.__EMS7DataAccessInsert.addPreAuthHolding(preauth, Action)
						self.__updateMigrationQueue(MigrationId)
			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					EMS6PreAuthHoldings=self.__getPreAuthHolding(EMS6Id)
					for preAuthHolding in EMS6PreAuthHoldings:
						self.__EMS7Cursor.execute("select EMS7MerchantAccountId, AuthorizationNumber, ProcessorTransactionId from PreAuthMapping where EMS6MerchantAccountId =%s\
													and AuthorizationNumber = %s and ProcessorTransactionId = %s",(preAuthHolding[5], preAuthHolding[3], preAuthHolding[2]))
						EMS7PreAuthHoldingMappingRow = self.__EMS7Cursor.fetchall()
						self.__EMS7Cursor.execute("delete from PreAuthHolding where MerchantAccountId = %s and AuthorizationNumber = %s and ProcessorTransactionId = %s", (EMS7PreAuthHoldingMappingRow[0],EMS7PreAuthHoldingMappingRow[1], EMS7PreAuthHoldingMappingRow[2]))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getPreAuthHolding(self, EMS6Id):
                self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,IsRFID,AddedGMT,MovedGMT,CardExpiry from PreAuthHolding where Id=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

	def __migrateProcessorTransaction(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " migrating processor transaction data"
		try:
			if (Action=='Update' and IsProcessed==0):
				#Aug 10 ITF
				#ProcessorTransactions = self.__getProcessorTransaction(PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate)
				#for processorTransaction in ProcessorTransactions:
				self.__EMS7DataAccess.addProcessorTransaction(MultiKeyId,Action)
				self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				#Aug 07 ITF
				#self.__EMS7DataAccessInsert.addProcessorTransaction(Split,Action)
				self.__EMS7DataAccess.addProcessorTransaction(MultiKeyId,Action)
				self.__updateMigrationQueue(MigrationId)		
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getProcessorTransaction(self, PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate):
		self.__EMS6Cursor.execute("select TypeId, PaystationId, MerchantAccountId, TicketNumber, Amount, CardType, Last4DigitsOfCardNumber, CardChecksum, PurchasedDate, ProcessingDate, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, Approved, CardHash, IsUploadedFromBoss, IsRFID from ProcessorTransaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __migratePaystationforPOSDate(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " ...migrating PaystationForPOSdate...."
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6Paystations=self.__getEMS6PaystationForPOSDate(EMS6Id)
				if(EMS6Paystations):
					self.__EMS7DataAccess.addPOSDatePaystationPropertiesToEMS7(EMS6Paystations, Action)
					self.__updateMigrationQueue(MigrationId)
			if (Action=='Insert' and IsProcessed==0):
				#EMS6Paystations=self.__getEMS6PaystationForPOSDate(EMS6Id)
				EMS6Paystations=self.__EMS6DataAccessInsert.getEMS6PaystationForPOSDate(EMS6Id)
				if(EMS6Paystations):
					self.__EMS7DataAccessInsert.addPOSDatePaystationPropertiesToEMS7(EMS6Paystations, Action)
					self.__updateMigrationQueue(MigrationId)
			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6PaystationForPOSDate(self, EMS6Id):
       		self.__EMS6Cursor.execute("select Id,version,Name,CommAddress,ContactURL,CustomerID,IsActive,RegionId,TimeZone,LockState,ProvisionDate,MerchantAccountId,DeleteFlag,CustomCardMerchantAccountID,PaystationType,IsVerrus,QueryStallsBy,LotSettingId from Paystation where Id=%s",EMS6Id)
		row = self.__EMS6Cursor.fetchone()
                return row 

	def __migrateTaxes(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ....migrating taxes record...."
		try:
			if (Action=='Update' and IsProcessed==0):
				Taxes = self.__getTaxes(EMS6Id)
				for Tax in Taxes:
					self.__EMS7DataAccess.addPurchaseTax(Tax,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				#Taxes = self.__getTaxes(EMS6Id)
				Taxes = self.__EMS6DataAccessInsert.getTaxes(EMS6Id)
				for Tax in Taxes:
					self.__EMS7DataAccessInsert.addPurchaseTax(Tax,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getTaxes(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,PaystationId,TicketNumber,PurchasedDate,ProcessingDate,CustomerId,RegionId,TaxName,TaxRate,TaxValue from Taxes where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateWsCallLogs(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " Migrating Webservices Call Log ..."
		
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			Token = Split[0]
			CallDate  = Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6WsCallLogs=self.__getEMS6WsCallLog(Token,CallDate)
				IsProcessed = 1
				for EMS6WsCallLog in EMS6WsCallLogs:
					self.__EMS7DataAccess.addWebServiceCallLog(EMS6WsCallLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6WsCallLogs=self.__getEMS6WsCallLog(Token,CallDate)
				EMS6WsCallLogs=self.__EMS6DataAccessInsert.getEMS6WsCallLog(Token,CallDate)
				IsProcessed = 1
				for EMS6WsCallLog in EMS6WsCallLogs:
					self.__EMS7DataAccessInsert.addWebServiceCallLog(EMS6WsCallLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				#self.__EMS7Cursor.execute("delete from WsCallLog where Id=(select distinct(EMS7WsCallLogId) from WsCallLogMapping where EMS6WsCallLogId=%s and EMS6CustomerId=%s)",(WsCallLog,CustomerId))
				#EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
	   	except (Exception, mdb.Error), e:
	   		self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6WsCallLog(self, Token,CallDate):
		self.__EMS6Cursor.execute("select Token,CallDate,CustomerId,TotalCall from WsCallLog where Token=%s and CallDate=%s",(Token,CallDate))
		return self.__EMS6Cursor.fetchall()

	def __migrateCCFailLog(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " migrating CC FailLog...."
		try:
			if (Action=='Update' and IsProcessed==0):
				IsDeleted=0
				LastModifiedGMT=date.today()
				LastModifiedByUserId=1
				EMS6CCFailLogs=self.__getEMS6CCFailLog(EMS6Id)
				if(EMS6CCFailLogs):
					for CCFailLog in EMS6CCFailLogs:
						self.__EMS7DataAccess.addCCFailLogToEMS7(CCFailLog, Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6CCFailLogs=self.__getEMS6CCFailLog(EMS6Id)
				EMS6CCFailLogs=self.__EMS6DataAccessInsert.getEMS6CCFailLog(EMS6Id)
				if(EMS6CCFailLogs):
					for CCFailLog in EMS6CCFailLogs:
						self.__EMS7DataAccessInsert.addCCFailLogToEMS7(CCFailLog, Action)
						self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from CCFailLog where Id=(select distinct(EMS7Id) from CCFailLogMapping where EMS6Id=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6CCFailLog(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,PaystationId,MerchantAccountId,TicketNumber,ProcessingDate,PurchasedDate,CCType,Reason from CCFailLog where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __migratePaystationHeartBeat(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " in migrating PaystationHeartBeat"
		try:
			#paySationHeartBeat = self.__getPaystationHeartBeat(EMS6Id)
			paySationHeartBeat =self.__EMS6DataAccessInsert.getEMS6PaystationHeartBeat(EMS6Id)
			for psHeartBeat in paySationHeartBeat:
				if (Action =='Insert'  and IsProcessed==0):
					IsProcessed=1
					self.__EMS7DataAccessInsert.addPOSHeartBeat(psHeartBeat, Action)
					self.__updateMigrationQueue(MigrationId)
				elif ((Action =='Update' or Action=='Delete')  and IsProcessed==0):
					IsProcessed=1
					self.__EMS7DataAccess.addPOSHeartBeat(psHeartBeat, Action)
					self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
				
	def __getPaystationHeartBeat(self, EMS6Id):
		self.__EMS6Cursor.execute("select PaystationId, LastHeartBeatGMT from PaystationHeartBeat where PaystationId=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __migrateReversal(self, Action, IsProcessed, MigrationId, MultiKeyId):
		print "migrating Reversal Records"
		try:
			reversalRows = self.__getReversal(MultiKeyId)
			#reversalRows = self.__EMS6DataAccessInsert.getEMS6Reversal(MultiKeyId)
			for reversal in reversalRows:
				if (Action == 'Insert' and IsProcessed==0):
					IsProcessed= 1
					self.__EMS7DataAccessInsert.addReversal(reversal, Action)
					self.__updateMigrationQueue(MigrationId)
				elif((Action == 'Update' or Action == 'Delete') and IsProcessed==0):
					IsProcessed= 1
					self.__EMS7DataAccess.addReversal(reversal, Action)
					self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
		
	def __getReversal(self, MultiKeyId):
		split = MultiKeyId.split(',')
		MerchantAccountId= split[0]
		OriginalReferenceNumber = split[1]
		OriginalTime=split[2]
		self.__EMS6Cursor.execute("select MerchantAccountId, CardNumber, ExpiryDate, OriginalMessageType, OriginalProcessingCode, \
									OriginalReferenceNumber, OriginalTime, OriginalTransactionAmount, LastResponseCode, PurchasedDate, \
									TicketNumber, PaystationId, RetryAttempts, LastRetryTime, Succeeded, Expired, IsRFID from Reversal\
									where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime = %s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
		return self.__EMS6Cursor.fetchall()
		
	def __migrateReversalArchive(self, Action, IsProcessed, MigrationId, MultiKeyId):
		print "migrating Reversal Archival Records"
		try:
			reversalArchiveRows = self.__getReversalArchive(MultiKeyId)
			#reversalArchiveRows = self.__EMS6DataAccessInsert.getEMS6ReversalArchive(MultiKeyId)
			for reversalArchive in reversalArchiveRows:
				if (Action == 'Insert' and IsProcessed==0):
					IsProcessed= 1
					self.__EMS7DataAccessInsert.addReversalArchive(reversalArchive, Action)
					self.__updateMigrationQueue(MigrationId)
				elif((Action == 'Update' or Action == 'Delete') and IsProcessed==0):
					IsProcessed= 1
					self.__EMS7DataAccess.addReversalArchive(reversalArchive, Action)
					self.__updateMigrationQueue(MigrationId)
					
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
			
	def __getReversalArchive(self, MultiKeyId):
		split = MultiKeyId.split(',')
		MerchantAccountId= split[0]
		OriginalReferenceNumber = split[1]
		OriginalTime=split[2]
		self.__EMS6Cursor.execute("select MerchantAccountId, CardNumber, ExpiryDate, OriginalMessageType, OriginalProcessingCode, \
									OriginalReferenceNumber, OriginalTime, OriginalTransactionAmount, LastResponseCode, PurchasedDate, \
									TicketNumber, PaystationId, RetryAttempts, LastRetryTime, Succeeded, Expired from ReversalArchive\
									where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime = %s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
		return self.__EMS6Cursor.fetchall()
	
	def __migrateValidCustomCard(self, Action, IsProcessed, MigrationId, MultiKeyId):
		print "migrating ValidCustomCard Records"
		try:
			validCustomCardRows = self.__getValidCustomCard(MultiKeyId)
			for curVSCardData in validCustomCardRows:
				self.__EMS7DataAccessInsert.addCustomerCard(curVSCardData, Action)
			self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
				
	def __getValidCustomCard(self, MultiKeyId):
		split = MultiKeyId.split(',')
		customerId = split[0]
		cardNumber=split[1]
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,version,StartDate,EndDate,RegionId,PaystationId,\
				MaxValidTicketCount,MaxValidTicketGracePeriod from ValidCustomCard where CustomerId = %s and CardNumber=%s ", (customerId, cardNumber))
		return self.__EMS6Cursor.fetchall()
	
	def __migrateRawSensorData(self, Action, IsProcessed, MigrationId, MultiKeyId):
		print "migrating RawSensordata Records"
		try:
			rawSensorDataRows = self.__getRawSensorData(MultiKeyId)
			for curRawSensorData in rawSensorDataRows:
				self.__EMS7DataAccessInsert.addRawSensorDataToEMS7(curRawSensorData, Action)
			self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
				
	def __getRawSensorData(self, MultiKeyId):
		split = MultiKeyId.split(',')
		ServerName = split[0]
		PaystationCommAddress=split(1)
		Type = split[2]
		Action=split[3]
		Information= split[4]
		DateField=split[5]
		self.__EMS6Cursor.execute("select ServerName, PaystationCommAddress, \
									Type, Action, Information, DateField, LastRetryTime, RetryCount\
							 from RawSensorData where ServerName = %s  and PaystationCommAddress =  %s  \
							and Type,= %s and  Action =  %s and Information= %s and DateField= %s",\
							(ServerName,PaystationCommAddress,Type,Action,Information,DateField))
		return self.__EMS6Cursor.fetchall()
	
	#Added on Jan 31 2014 EMS-4989
	def __migrateCustomerPermissions(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...migrating CustomerPermissions ..."
		try:
			Split=MultiKeyId.split(',')
			SubscriptionTypeId=None
			EMS6CustomerId = Split[0]
			EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)			
			print "EMS7CustomerId is ", EMS7CustomerId
			ReportsEnabled =Split[1]
			
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,100,ReportsEnabled)
			
			print "After Insert"
			
			TextAlertEnabled =Split[2]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,200,TextAlertEnabled)
			
			CreditCardProcessingEnabled=Split[3]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,300,CreditCardProcessingEnabled)
			
			CouponsEnabled=Split[4]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,600,CouponsEnabled)
			
			WebServicesEnabled=Split[5]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1000,WebServicesEnabled)
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1100,WebServicesEnabled)
			
			CardTypesEnabled=Split[6]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,700,CardTypesEnabled)
			
			PayByPhoneEnabled=Split[7]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,900,PayByPhoneEnabled)
						
			PayByCellEnabled=Split[8]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1200,PayByCellEnabled)
			
			self.__updateMigrationQueue(MigrationId)
			
			#if (ServiceId=="1"):
			#	SubscriptionTypeId=100
			#if (ServiceId=="2"):
			#	SubscriptionTypeId=600
			#if (ServiceId=="3"):
			#	SubscriptionTypeId=200
			#if(ServiceId=="4"):
			#	SubscriptionTypeId=700
			#if(ServiceId=="5"):
			#	SubscriptionTypeId=300
			#if(ServiceId=="6"):
			#	SubscriptionTypeId=1000
			#if(ServiceId=="7"):
			#	SubscriptionTypeId=1200
			#if(ServiceId=="8"):
			#	SubscriptionTypeId=900
			
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	
	def IncrementalMigration(self):
		
		"""Create a MigrationProcessLog record"""
		# Added on Jan 10
		self.__EMS6InsertCursor.execute("insert ignore into INCScriptVersion(ModuleName,BuildVersion,LastRunDate) VALUES ('MasterData', '5', now() ) ON DUPLICATE KEY UPDATE LastRunDate=now(), BuildVersion='5'")
		EMS6Connection.commit()
		
		print " ....crating a migrationProcessingLog Record......."
		
		self.__EMS6Cursor.execute("select count(*) from MigrationProcessLog  where ProcessType = 'Incremental Migration MasterData' and EndTime is null")
		processCount = self.__EMS6Cursor.fetchone()
		print "processCount id:",processCount
		if (processCount[0]>=1):
			self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime, EndTime, RecordSelectedCount, RecordProcessedCount, Error)\
                                                       values ('Incremental Migration MasterData',now(), now(), 0, 0,'Too Many Processes Running')")
			EMS6Connection.commit()
			return
		
		print "....Creating MigrationProcessLog record..."
		self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime) values ('Incremental Migration MasterData',now())")
		EMS6Connection.commit()
		self.__processId= self.__EMS6InsertCursor.lastrowid
		
		print "Created migration process log record with id:..", self.__processId
		
		print ".,,selecting record from migration queue....."
		
		self.__EMS7Cursor.execute("select value from EmsProperties where name = 'IncrementalMigrationLimit'")
		MigrationQueueRecordLimit = self.__EMS7Cursor.fetchone()
		
		print "MigrationQueueRecordLimit is :",MigrationQueueRecordLimit
		
		# Code written on Sep 23
		self.__EMS6Cursor.execute("select LowerLimit,UpperLimit,now() from MigrationQueueLimit where ModuleName='MasterData' order by Id desc Limit 1")
		MigrationQueueLimit=self.__EMS6Cursor.fetchone()
		print "MigrationQueueLimit[0] is ", MigrationQueueLimit[0]
		print "MigrationQueueLimit[1] is ", MigrationQueueLimit[1]
		print "Date now() is ", MigrationQueueLimit[2]
		MigrationQueueLowerLimit = MigrationQueueLimit[0]
		MigrationQueueUpperLimit = MigrationQueueLimit[1]
		# Added ServiceAgreedCustomer on Nov 18, Nov 19
		self.__EMS6Cursor.execute("select * FROM MigrationQueue where Id between '%d' and '%d' and TableName in ('Customer','CustomerProperties','Customer_Service_Relation','Region','LotSetting', \
			'Paystation','LotSettingFileContent','Coupon','RESTAccount','ModemSetting','EMSRate','ServiceAgreedCustomer','EMSRateDayOfWeek','ServiceAgreement','CustomerPermissions') and SelectedForMigration = '%d' and \
			SelectedByProcessId='%d' and IsProcessed ='%d' order by Id" % (MigrationQueueLimit[0],MigrationQueueLimit[1],0,0,0))
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		print " --number of records selected for migration is:..", len(MigrationQueue)
		
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set RecordSelectedCount = "%d" where id = "%d"' % (len(MigrationQueue),self.__processId ))
		
		""" mark the selected record from the migration queue table as selected for processing"""
		print "----Flagging the selected record as selected for processing....."
		for rec in MigrationQueue:
			self.__EMS6InsertCursor.execute("update MigrationQueue set SelectedForMigration ='%d' , SelectedByProcessId = '%d' where Id = '%d' " % (1, self.__processId, rec[0]))
			
		EMS6Connection.commit()
		
		error ="None"
		
	#	input = raw_input("Start Migration  ........................... y/n ?   :   ")
	#	if (input=='y') :
		for migration in MigrationQueue:
			MigrationId = migration[0]
			MultiKeyId = migration[1]
			EMS6Id = migration[2]
			TableName = migration[3]
			Action = migration[4]
			IsProcessed = migration[5]
			Date = migration[6]
			
			''' Begin Transaction '''
			print " ----starting incremental data migration....."
			
			if(IsProcessed==0):
				if (TableName=='Customer'):
					self.__migrateCustomers(Action,EMS6Id,IsProcessed,MigrationId)	
					
				if (TableName=='CustomerProperties'):
					self.__migrateCustomerProperties(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='Customer_Service_Relation'):
					self.__migrateCustomerSubscription(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if (TableName=='Paystation'):
					self.__migratePaystations(Action,EMS6Id,IsProcessed,MigrationId)
					
				if (TableName=='Region'):
					self.__migrateLocations(Action,EMS6Id,IsProcessed,MigrationId)
					
				if (TableName=='Coupon'):
					self.__migrateCoupons(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if (TableName=='LotSetting'):
					self.__migrateLotSetting(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='RegionPaystationLog'):
					self.__migrateLocationPOSLog(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='EMSParkingPermission'):
					self.__migrateEMSParkingPermission(Action,IsProcessed,MigrationId,EMS6Id)
					
				## Test for RestAccount is pending
				if(TableName=='RESTAccount'):
					self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
					
				if(TableName=='BadCard'):
					self.__migrateBadValueCard(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if(TableName=='BadSmartCard'):
					self.__migrateBadSmartCard(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if(TableName=='BadCreditCard'):
					self.__migrateBadCreditCard(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if(TableName=='BatteryInfo'):
					self.__migrateBatteryInfo(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if(TableName=='CardRetryTransaction'):
					self.__migrateCardRetryTransaction(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if(TableName=='MerchantAccount'):
					self.__migrateMerchantAccount(Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id)
					
				if(TableName=='ServiceState'):
					self.__migrateServiceState(Action,IsProcessed,MigrationId,EMS6Id)
				#if(TableName=='RESTAccount'): Is it duplication??
					#self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
				if(TableName=='Rates'):
					self.__migrateRates(Action,IsProcessed,MigrationId,MultiKeyId)
				# The section below does incremental migration for EMSRates INTO UnifiedRates, and EMSRates INTO ExtensibleRates
				if(TableName=='EMSRate'):
					# Jan 20 EMS-???? Added MultiKeyId as additional parameter
					self.__migrateEMSRateIntoUnifiedRate(Action,IsProcessed,MigrationId,EMS6Id,MultiKeyId)
					
				if(TableName=='SMSTransactionLog'):
					self.__migrateSMSTransactionLog(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='SMSAlert'):
					self.__migrateSmsAlert(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='SMSFailedResponse'):
					self.__migrateSMSFailedResponse(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='ModemSetting'):
					self.__migrateModemSetting(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='LotSettingFileContent'):
					self.__migrateLotSettingFileContent(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='CryptoKey'):
					self.__migrateCryptoKey(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='RESTLog'):
					self.__migrateRestLog(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='RESTSessionToken'):
					self.__migrateRestSessionToken(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='RESTLogTotalCall'):
					self.__migrateRestLogTotalCall(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='Alert'):
					self.__migrateAlert(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='CustomerWsCal'):
					self.__migrateCustomerWsCal(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='CustomerWsToken'):
					self.__migrateCustomerWsToken(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='Replenish'):
					self.__migrateReplenish(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='CardType'):
					self.__migrateCustomerCardType(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='PaystationGroup'):
					self.__migratePaystationGroup(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='Paystation_PaystationGroup'):
					self.__migratePaystation_PaystationGroup(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='CustomerCard' or TableName=='SmartCard' or TableName=='Purchase' or TableName=='Permit' or TableName=='PaymentCard'):
					self.__migrateTransaction(Action, IsProcessed, MigrationId, MultiKeyId,TableName)
					
				if(TableName=='ProcessorProperties'):
					self.__migrateProcessorProperties(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='ProcessorTransaction'):
					self.__migrateProcessorTransaction(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='EMSExtensiblePermit'):
					self.__migrateEMSExtensiblePermit(Action, MultiKeyId, IsProcessed, MigrationId)
					
				if(TableName=='UserAccount'):
					self.__migrateUserAccount(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='EventLogNew'):
					self.__migrateEventLogNew(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='PaystationForPOSDate'):
					self.__migratePaystationforPOSDate(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='Audit'):
					self.__migrateAudit(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='Taxes'):
					self.__migrateTaxes(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='WsCallLog'):
					self.__migrateWsCallLogs(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='PreAuth'):
					self.__migratePreAuth(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='PreAuthHolding'):
					self.__migratePreAuthHolding(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='CCFailLog'):
					self.__migrateCCFailLog(Action, EMS6Id, IsProcessed, MigrationId)
					
				if(TableName=='PaystationHeartBeat'):
					self.__migratePaystationHeartBeat(Action, EMS6Id, IsProcessed, MigrationId)
					
				if ( TableName=='Reversal'):
					self.__migrateReversal(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if ( TableName=='ReversalArchive'):
					self.__migrateReversalArchive(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if ( TableName=='ValidCustomCard'):
					self.__migrateValidCustomCard(Action, IsProcessed, MigrationId, MultiKeyId)
					
				if ( TableName=='RawSensorData'):
					self.__migrateRawSensorData(Action, IsProcessed, MigrationId, MultiKeyId)
				
				if ( TableName=='ServiceAgreedCustomer'):
					self.__migrateServiceAgreedCustomer(Action,IsProcessed,MigrationId,MultiKeyId)
				
				if ( TableName=='EMSRateDayOfWeek'):
					self.__migrateEMSRateDayOfWeek(Action,IsProcessed,MigrationId,MultiKeyId)
				
				if ( TableName=='ServiceAgreement'):
					self.__migrateServiceAgreement(Action,EMS6Id,IsProcessed,MigrationId)
				
				# Added on Jan 31 2014
				if ( TableName=='CustomerPermissions'):
					self.__migrateCustomerPermissions(Action,IsProcessed,MigrationId,MultiKeyId)
			""" Updete the process log table"""		
						
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set EndTime = now(), RecordProcessedCount = "%d", Error= "%s" where id = "%d"' % (len(MigrationQueue),error, self.__processId ))
		EMS6Connection.commit()
		#Code added on Sep 23
		self.__EMS6Cursor.execute("select max(id),now() from MigrationQueue")
		MigrationQueueMaxId=self.__EMS6Cursor.fetchone()
		
		MigrationQueueCurrentMaxId = MigrationQueueMaxId[0]
		print "$$ MigrationQueueCurrentMaxId is ",MigrationQueueCurrentMaxId
		print "$$ MigrationQueueUpperLimit is ",MigrationQueueUpperLimit
		print "$$ MigrationQueueRecordLimit is ",MigrationQueueRecordLimit[0]
		
		MigrationQueueUpperLimitNew = int(MigrationQueueUpperLimit) + int(MigrationQueueRecordLimit[0])
		
		print "$$ MigrationQueueUpperLimitNew", MigrationQueueUpperLimitNew
		
		if (MigrationQueueUpperLimitNew < MigrationQueueCurrentMaxId):
			print " $$ Inside If"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='MasterData'",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('MasterData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueUpperLimitNew,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()
			
		else:
			print " $$ Inside Else"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='MasterData'",(MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('MasterData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()		
		
EMS6Connection = None
EMS7Connection = None
try:
	print " .....getting EMS6Connection from DB......."
	
	EMS6Connection = mdb.connect('172.16.5.14', 'ltikhova', 'Pass.word', 'ems_db_6_3_12')
	
	
	print " ****.....Got EMS6Connection from DB......."
	
	print " .....getting Cerberus connetion  from DB..This is a substitute connection....."
	
	EMS6ConnectionCerberus = mdb.connect('172.16.5.14', 'ltikhova', 'Pass.word', 'cerberus_db')
	
	
	if (EMS6ConnectionCerberus):
		print " ***Got Cerberus Connection from DB......."
	
	print " .....getting EMS7 Connection from DB......."
	
	EMS7Connection = mdb.connect('172.16.5.91', 'IncMigration','IncMigration','ems7_1_QA')
	
	
	print " .....Got  EMS7Connection from DB......."
	
	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS6DataAccessInsert(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), EMS7DataAccessInsert(EMS7Connection, 1), 1)
	
	startMigration.IncrementalMigration()
except (Exception, mdb.Error), e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
	if EMS6Connection:
		EMS6Connection.close()
	if EMS7Connection:
		EMS7Connection.close()
