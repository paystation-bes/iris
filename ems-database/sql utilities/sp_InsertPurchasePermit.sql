/*  --------------------------------------------------------------------------------------
    Procedure:  sp_InsertPurchasePermit

    Purpose:    Performs a multi-table atomic insert into the following EMS 7 tables:
                1.  Purchase
                2.  Permit
                3.  PurchaseExtra (optional, depends on incoming parameters)
                4.  PaymentCard (optional, depends on incoming parameters).
                
                This procedure may insert records into the following tables, however
                these inserts are not part of an atomic insert:
                1.  LicencePlate
                2.  CellPhone
                
                This procedure is called by:
                1.  EMS 7 business layer logic that accepts an incoming XML RPC 
                    from a Paystation
                2.  EMS 6.3.9 to EMS 7 database migration code 
                
    Warning:    This procedure uses hard coded values to identify foreign key values 
                from table PayCardSubType for table PaymentCard.PayCardSubTypeId. Values in 
                table PayCardSubType must align with the foreign key values hard coded 
                into this procedure.
                
                Two of the values (rows) in PayCardSubType are used to identify legitimate
                missing foreign key data errors, these are:
                1.  PaymentCard.PayCardSubTypeId = 6: Credit Card card type error
                2.  PaymentCard.PayCardSubTypeId = 9: Value Card card type error
                
                Note that a Customer can add their own type of Value Card at anytime using BOSS, 
                whereas only DPT adds Credit Cards (VISA, MasterCard, AMEX, Discover).
                
                If either error occurs a new row must be added to table PayCardSubType, and
                the foreign keys for PaymentCard.PayCardSubTypeId must be subsequently reassigned.
                
                This procedure need not change but can be changed in order to leverage the
                optimization of not performing a SQL SELECT lookup to identify
                the new PayCardSubType.
                
                If PaymentCard.PayCardSubTypeId = 0 then a logic error within this procedure
                has occurred.
                
    Notes:      This procedure performs various data cleansing tasks on 
                incoming parameter values to fix minor data inconsistancies 
                that have (historically) infrequently occurred. 
           
    --------------------------------------------------------------------------------------
    Coding:     1.  A free (complimentary) permit will not have: cash paid, card paid, etc.
                
    --------------------------------------------------------------------------------------
    Questions:  1.  Is CustomerId known?
                2.  PurchaseLocationId known?
                3.  ProcessorTransactionId known?
                4.  How best to handle 'IsRefund': for PaymentCard, PaymentCash, Purchase?
                5.  What about ProcessorTransactionType? (not static, changes over time)
                6.  What does a refund look like coming up from a Paystation where 
                    the original charge was settled 10 days ago?
                7.  How to identify a Free (Complimentary) parking permit? Zero paid? Coupon?
                8.  What about incoming parameter values for non-used parameters?
                    Example: AddTimeNum, is no value a NULL or is no value a Zero?
                    Example: IsRefundSlip, SpaceNumber, ...
                9.  Free (complimentary) permits: how to identify?   
                --------------------------------------------------------------------------
                Q: IsRefund:
                a. PaymentCard: IsOffline, IsRFID, IsRefund
                b. PaymentCash: IsRefund
                c. Purchase: IsRefund
                --------------------------------------------------------------------------
                This area is a scratch pad for SQL useful to developing and testing this
                stored procedure.
                
                SELECT  LocationId AS PurchaseLocationId 
                FROM    PointOfSaleLog
                WHERE   PointOfSaleId = X_PointOfSaleId_X
                AND     BeginEffectiveGMT = (
                        SELECT  MIN(BeginEffectiveGMT)
                        FROM    PointOfSaleLog
                        WHERE   PointOfSaleId = X_PointOfSaleId_X
                        AND     BeginEffectiveGMT >= Y_PurchasedDate_Y)
                        
                        
                Business Rule that must be enforced in SQL:
                In any of the 'Log' tables no EndEffectiveDate can be > a BeginEffectiveDate
                        
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE sp_InsertPurchaseAndPermit;

delimiter //
CREATE PROCEDURE sp_InsertPurchaseAndPermit 
(                                                           /* Valid values for incoming parameters: */                                                           
IN CustomerId               INT UNSIGNED,                   /* not null, greater than zero */  
IN PointOfSaleId            INT UNSIGNED,                   /* not null, greater than zero */
IN TicketNumber             INT UNSIGNED,                   /* not null, greater than zero */
IN AddTimeNum               INT UNSIGNED,                   /* not null, greater than zero */ 
IN PurchaseLocationId       INT UNSIGNED,                   /* not null, greater than zero */ 
IN PermitLocationId         INT UNSIGNED,                   /* not null, greater than zero */
IN LotNumber                VARCHAR(20),                    /* not null, cannot be an empty string */
IN StallNumber              INT UNSIGNED,                   /* not null, greater than or equal to zero */ 
IN TransactionTypeId        TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN PaymentTypeId            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN RateId                   BIGINT UNSIGNED,                /* not null, greater than or equal to zero */
IN PurchasedDate            DATETIME,                       /* not null */
IN ExpiryDate               DATETIME,                       /* not null, greater than or equal to PurchasedDate */
IN PlateNumber              VARCHAR(15),                    /* null permitted */
IN CellPhone                VARCHAR(15),                    /* null permitted */
IN IsOffline                TINYINT UNSIGNED,               /* not null, greater than zero */
IN IsRFID                   TINYINT UNSIGNED,               /* not null, in {0,1} */
IN IsRefundSlip             TINYINT UNSIGNED,               /* not null, in {0,1} */
IN CurrencyId               TINYINT UNSIGNED,               /* not null, greater than or equal to zero, 0=Undefined */
IN OriginalAmount           MEDIUMINT UNSIGNED,             /* not null, greater than zero */
IN ChargedAmount            MEDIUMINT UNSIGNED,             /* not null, greater than or equal to zero */
IN CashPaid                 MEDIUMINT UNSIGNED,             /* not null, greater than or equal to zero */
IN CardPaid                 MEDIUMINT UNSIGNED,             /* not null, greater than or equal to zero */
IN SmartCardPaid            MEDIUMINT UNSIGNED,             /* not null, greater than or equal to zero */
IN ChangeDispensed          MEDIUMINT UNSIGNED,             /* null permitted, or greater than zero */
IN ExcessPayment            MEDIUMINT UNSIGNED,             /* null permitted, or greater than zero */
IN ProcessorTransactionId   INT,                            /* not null, greater than or equal to zero */
IN CardType                 VARCHAR(20),                    /* null permitted */
IN CardLast4Digits          SMALLINT UNSIGNED,              /* null or greater than zero */
IN CustomCardData           VARCHAR(40),                    /* null permitted, if non-null must be paired with non-null CustomCardType */
IN CustomCardType           VARCHAR(20),                    /* null permitted, if non-null must be paired with non-null CustomCardCardData */
IN SmartCardData            VARCHAR(40),                    /* null permitted */
IN CouponNumber             VARCHAR(20),                    /* null permitted */
IN CoinQtyIn005             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyIn010             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyIn025             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyIn050             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyIn100             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyIn200             TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut005            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut010            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut025            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut050            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut100            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN CoinQtyOut200            TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn01              TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn02              TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn05              TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn10              TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn20              TINYINT UNSIGNED,               /* not null, greater than or equal to zero */
IN BillQtyIn50              TINYINT UNSIGNED                /* not null, greater than or equal to zero */
)
BEGIN

    /* local variables */
    DECLARE PurchaseId              INT UNSIGNED;
    DECLARE PayCardSubTypeId        TINYINT UNSIGNED;
    DECLARE PayCardInfoTypeId       INT UNSIGNED;
    DECLARE LotSettingPurchaseId    INT UNSIGNED;
    DECLARE CardInfo_sp             VARCHAR(40);
    DECLARE LicencePlateId          INT UNSIGNED;
    DECLARE CellPhoneId             INT UNSIGNED; 
    DECLARE CardLast4Digits_sp      SMALLINT UNSIGNED;
    DECLARE CardPaid_sp             MEDIUMINT UNSIGNED;
    DECLARE IsQuarantine            TINYINT;
    DECLARE BeginGMT                DATETIME;
    DECLARE InsertSeconds           INT UNSIGNED;
    DECLARE ChangeDispensed_sp      MEDIUMINT UNSIGNED;
    DECLARE ExcessPayment_sp        MEDIUMINT UNSIGNED;   
    DECLARE PurchaseCouponId        INT UNSIGNED;
    DECLARE RealtimeVsMigration     TINYINT UNSIGNED;       /* Real_time=1, Migration_real=2, Migration_test=3*/
    
    /* Specify for which purpose this procedure is being used */
    SET RealtimeVsMigration = 3;
    
    /* initialize local variables */
    SET BeginGMT = UTC_TIMESTAMP();
    SET CardInfo_sp = NULL;         /* applies to: Patroller Card, Smart Card, Value Card; does not apply to Credit Card */
    SET CardPaid_sp = CardPaid;
    SET LotSettingPurchaseId = NULL; 
    SET LicencePlateId = 0;         /* a value will be assigned to (Permit.)LicencePlateId if a licence plate was specified */
    SET CellPhoneId = 0;            /* a value will be assigned to (Permit.)CellPhoneId if a cell phone was specified */
    SET PayCardSubTypeId = NULL;    /* a value will be assigned to (PaymentCard.)PayCardSubTypeId if any type of card is used as payment */
    SET PayCardInfoTypeId = 0;      /* a value will be assigned to (PaymentCard.)PayCardInfoTypeId if a Patroller Card or Smart Card or Value Card is used as payment (note: does not apply to Credit Cards) */
    SET IsQuarantine = 0;           /* a value greater than zero will be assigned to IsQuarantine if there is bad data within the incoming parameter values */ 
    SET PurchaseCouponId = 0;
    SET CardLast4Digits_sp = IFNULL(CardLast4Digits,0);  
    SET ChangeDispensed_sp = NULL;
    SET ExcessPayment_sp = NULL;  
    IF ChangeDispensed > 0 THEN SET ChangeDispensed_sp = ChangeDispensed; END IF;
    IF ExcessPayment > 0 THEN SET ExcessPayment_sp = ExcessPayment; END IF;
    
    /* Data quarantine checks: the ordering of these checks reflects the probability that quarantine is required */
    
    /* Possible Issue: Payment Type is Cash but a Card Type (such as VISA, MasterCard, AMEX, CreditCard) has been specified */
    /* Fix:            Quarantine the purchase. Quarantine Type = 1 */  
    IF PaymentTypeId = 1 THEN
        IF CardType IS NOT NULL THEN
            SET IsQuarantine = 1;
        ELSE
            /* Cash payment does not have Card Last 4 Digits */
            SET CardLast4Digits_sp = NULL;
        END IF; 
      
    /* Possible Issue: Payment Type involves a Credit Card but a non- credit card Card Type (such as a type of Value Card) has been specified */
    /* Fix:            Quarantine the purchase. Quarantine Type = 2 */      
    ELSEIF PaymentTypeId = 2 OR PaymentTypeId = 5 THEN
        IF (CardType != 'VISA' AND CardType != 'MasterCard' AND CardType != 'AMEX' AND CardType != 'Discover' AND CardType != 'Other' AND CardType != 'CreditCard') THEN
            SET IsQuarantine = 2;
        ELSE
            IF CardType     = 'VISA'        THEN SET PayCardSubTypeId = 1; 
            ELSEIF CardType = 'MasterCard'  THEN SET PayCardSubTypeId = 2;
            ELSEIF CardType = 'AMEX'        THEN SET PayCardSubTypeId = 3;
            ELSEIF CardType = 'Discover'    THEN SET PayCardSubTypeId = 4;
            ELSEIF CardType = 'Other'       THEN SET PayCardSubTypeId = 5;
            ELSEIF CardType = 'CreditCard'  THEN SET PayCardSubTypeId = 5;
            /* quarantine the unknown type of Credit Card */
            ELSE SET IsQuarantine = 4;
            END IF;
        END IF;
            
    /* Possible Issue: Payment Type involves a Value Card but a non- value card Card Type (such as a type of Credit Card: VISA, MasterCard) has been specified */
    /* Fix:     Quarantine the purchase. Quarantine Type = 3 */             
    ELSEIF PaymentTypeId = 7 OR PaymentTypeId = 9 THEN
        IF CardType = 'VISA' OR CardType = 'MasterCard' OR CardType = 'AMEX' OR CardType = 'Discover' OR CardType = 'Other' OR CardType = 'CreditCard' THEN
            SET IsQuarantine = 3;
        ELSE
            SET PayCardSubTypeId = 8;
            SET CardInfo_sp = CustomCardData;
        END IF;
        
    /* Payment Type involves a Smart Card */    
    ELSEIF PaymentTypeId = 4 OR PaymentTypeId = 6 THEN
        SET PayCardSubTypeId = 6;
        SET CardInfo_sp = SmartCardData;
        SET CardPaid_sp = SmartCardPaid;
        
    /* Payment Type involves a Patroller Card */    
    ELSEIF PaymentTypeId = 3 THEN
        SET PayCardSubTypeId = 7;
        SET CardInfo_sp = CustomCardData;
     
    /* All permissable values for PaymentTypeId have been checked: quarantine all other values */     
    ELSE
        /* Quarantine the remaining values for PaymentTypeId: these values are not valid payment for a purchase */
        SET IsQuarantine = 5;
    END IF;
    
    /* Verify PayCardSubTypeId has been assigned a value */
    IF IFNULL(PayCardSubTypeId,0) = 0 THEN
        SET IsQuarantine = 6;
    END IF;
    
    /* Verify a LotNumber (an incoming parameter) has been assigned a value */
    IF IFNULL(LotNumber,0) = 0 OR LENGTH(LotNumber) = 0 THEN
        SET IsQuarantine = 7;
    END IF; 
    
    /* This procedure only processes specific TransactionTypeId's: {2, 4, 5, 6, 12, 17}. Quarantine the data if this procedure has been called inadvertantly. */
    IF TransactionTypeId != 2 AND TransactionTypeId != 4 AND TransactionTypeId != 5 AND TransactionTypeId != 6 AND TransactionTypeId != 12 AND TransactionTypeId != 17 THEN
       SET IsQuarantine = 8;
    END IF;    
    
    /* A Cancelled purchase will have ExpiryDate=PurchasedDate. Quarantine all non-Cancelled purchases where ExpiryDate<=PurchasedDate (could be a Permit with zero or negative duration) */
    IF TransactionTypeId != 5 AND ExpiryDate <= PurchasedDate THEN
           SET IsQuarantine = 9;
    END IF; 
    
    /* Essential foreign keys must have a value. Quarantine the data if an essential foreign key does not have a value. The last check (on its own line) verifies the Location for a permit is required. */
    /* Note: these checks are not validating the foreign key values themselves, rather they are simply validating that the foreign key has a value. */
    IF IFNULL(CustomerId,0) = 0 OR IFNULL(PointOfSaleId,0) = 0 OR IFNULL(PurchaseLocationId,0) = 0 OR 
      (IFNULL(PermitLocationId,0) = 0 AND (TransactionTypeId = 2 OR TransactionTypeId = 12 OR TransactionTypeId = 17) AND IsRefundSlip = 0) THEN
           SET IsQuarantine = 10;
    END IF;     
  
    /* Quarantine the data if necessary */
    IF IsQuarantine > 0 THEN
        INSERT  PurchaseQuarantine (
                    QuarantineTypeId,CustomerId,PointOfSaleId,TicketNumber,LotNumber,SpaceNumber,TransactionTypeId,PurchasedDate,ExpiryDate,ChargedAmount,CashPaid,CardPaid,ChangeDispensed,ExcessPayment,IsRefundSlip,
                    CustomCardData,CustomCardType,CouponNumber,SmartCardData,SmartCardPaid,PaymentTypeId,RateId,PurchaseLocationId,PermitLocationId,AddTimeNum,OriginalAmount,IsOffLine,PlateNumber,CellPhoneNumber,
                    CardType,Last4Digits,CurrencyId,IsRFID,CoinQtyIn005,CoinQtyIn010,CoinQtyIn025,CoinQtyIn050,CoinQtyIn100,CoinQtyIn200,CoinQtyOut005,CoinQtyOut010,CoinQtyOut025,CoinQtyOut050,CoinQtyOut100,CoinQtyOut200,
                    BillQtyIn01,BillQtyIn05,BillQtyIn10,BillQtyIn20,BillQtyIn50,LotSettingPurchaseId,LicencePlateId,CellPhoneId,PayCardSubTypeId,PayCardInfoTypeId,LastModifiedGMT)
        VALUES     (IsQuarantine,CustomerId,PointOfSaleId,TicketNumber,LotNumber,SpaceNumber,TransactionTypeId,PurchasedDate,ExpiryDate,ChargedAmount,CashPaid,CardPaid,ChangeDispensed,ExcessPayment,IsRefundSlip,
                    CustomCardData,CustomCardType,CouponNumber,SmartCardData,SmartCardPaid,PaymentTypeId,RateId,PurchaseLocationId,PermitLocationId,AddTimeNum,OriginalAmount,IsOffLine,PlateNumber,CellPhoneNumber,
                    CardType,Last4Digits,CurrencyId,IsRFID,CoinQtyIn005,CoinQtyIn010,CoinQtyIn025,CoinQtyIn050,CoinQtyIn100,CoinQtyIn200,CoinQtyOut005,CoinQtyOut010,CoinQtyOut025,CoinQtyOut050,CoinQtyOut100,CoinQtyOut200,
                    BillQtyIn01,BillQtyIn05,BillQtyIn10,BillQtyIn20,BillQtyIn50,LotSettingPurchaseId,LicencePlateId,CellPhoneId,PayCardSubTypeId,PayCardInfoTypeId,LastModifiedGMT);
        
    /* The incoming parameter data is clean. Checking for NULL parameter values was not performed. Very few database attributes accept NULL values. An INSERT will fail on a not-NULL violation. */
    ELSE
        
        /* If necessary perform database lookup or INSERT to get a value for LotSettingPurchaseId */
        IF EXISTS (SELECT * FROM LotSettingPurchase WHERE CustomerId = CustomerId AND LotSettingName = LotSetting) THEN 
            SELECT Id INTO LotSettingPurchaseId FROM LotSettingPurchase WHERE CustomerId = CustomerId AND LotSettingName = LotSetting; 
        ELSE 
            INSERT INTO LotSettingPurchase (CustomerId,LotSettingName,LastModifiedGMT) VALUES (CustomerId,LotSetting,UTC_TIMESTAMP()); 
            SET LotSettingPurchaseId = LAST_INSERT_ID();
        END IF;
   
        /* If necessary perform database lookup or INSERT to get a value for LicencePlateId */
        IF LENGTH(PlateNumber) > 0 THEN
            IF EXISTS (SELECT * FROM LicencePlate WHERE Number = PlateNumber) THEN 
                SELECT Id INTO LicencePlateId FROM LicencePlate WHERE Number = PlateNumber; 
            ELSE 
                INSERT INTO LicencePlate (Number,LastModifiedGMT) VALUES (PlateNumber,UTC_TIMESTAMP()); 
                SET LicencePlateId = LAST_INSERT_ID();
            END IF;
        END IF;
        
        /* If necessary perform database lookup or INSERT to get a value for CellPhoneId */
        IF LENGTH(CellPhone) > 0 THEN
            IF EXISTS (SELECT * FROM CellPhone WHERE Number = CellPhone) THEN 
                SELECT Id INTO CellPhoneId FROM CellPhone WHERE Number = CellPhone; 
            ELSE 
                INSERT INTO CellPhone (Number,LastModifiedGMT) VALUES (CellPhone,UTC_TIMESTAMP()); 
                SET CellPhoneId = LAST_INSERT_ID();
            END IF;
        END IF;  
        
        /* If necessary perform database lookup or INSERT to get a value for PayCardInfoTypeId */
        IF LENGTH(CardInfo_sp) > 0 AND (PaymentTypeId = 3 OR PaymentTypeId = 4 OR PaymentTypeId = 6 OR PaymentTypeId = 7 OR PaymentTypeId = 9) THEN
            IF EXISTS (SELECT * FROM PayCardInfoType WHERE CustomerId = CustomerId AND CardInfo = CardInfo_sp) THEN 
                SELECT Id INTO PayCardInfoTypeId FROM PayCardInfoType WHERE CustomerId = CustomerId AND CardInfo = CardInfo_sp; 
            ELSE 
                INSERT INTO PayCardInfoType (CustomerId,CardInfo,LastModifiedGMT) VALUES (CustomerId,CardInfo_sp,UTC_TIMESTAMP()); 
                SET PayCardInfoTypeId = LAST_INSERT_ID();
            END IF;
        END IF; 
        
        /* If necessary perform database lookup or INSERT to get a value for PurchaseCouponId */
        IF LENGTH(Coupon) > 0 THEN
            IF EXISTS (SELECT * FROM PurchaseCoupon WHERE CustomerId = CustomerId AND Number = Coupon) THEN 
                SELECT Id INTO PurchaseCouponId FROM PurchaseCoupon WHERE CustomerId = CustomerId AND Number = Coupon; 
            ELSE 
                INSERT INTO PurchaseCoupon (CustomerId,Number,LastModifiedGMT) VALUES (CustomerId,Coupon,UTC_TIMESTAMP()); 
                SET PurchaseCouponId = LAST_INSERT_ID();
            END IF;
        END IF;  
        
        /* Start multi-table atomic database transaction */
        START TRANSACTION;   
    
            /* Insert into Purchase */
            INSERT Purchase (PointOfSaleId,PurchaseGMT,PurchaseNumber,LocationId,TransactionTypeId,PaymentTypeId,LotSettingPurchaseId,PurchaseCouponId,CurrencyId,CreatedGMT,OriginalAmount,ChargedAmount,ChangeDispensed,ExcessPayment,CashPaid,CardPaid,IsOffline,IsRefund)
            VALUES          (PointOfSaleId,PurchasedDate,TicketNumber,PurchaseLocationId,TypeId,PaymentTypeId,LotSettingPurchaseId,PurchaseCouponId,CurrencyId,UTC_TIMESTAMP(),OriginalAmount,ChargedAmount,ChangeDispensed_sp,ExcessPayment_sp,CashPaid,CardPaid_sp,IsOffline,IsRefundSlip);
    
            /* get the auto-increment value of the Purchase record just inserted  */
            SET PurchaseId = LAST_INSERT_ID();
            
            /* Insert into PaymentCard if needed (if payment was made using something other than Cash) */
            IF PaymentTypeId != 1 THEN
                INSERT PaymentCard (PurchaseId,CardActivityGMT,PayCardSubTypeId,PaycardInfoTypeId,ProcessorTransactionId,Amount,CardLast4Digits,IsRFID,IsRefund)
                VALUES             (PurchaseId,PurchasedDate,PayCardSubTypeId,PaycardInfoTypeId,ProcessorTransactionId,CardPaid_sp,CardLast4Digits_sp,IsRFID,IsRefundSlip);
            END IF;
            
            /* Insert into PaymentCash if needed: Coins accepted */
            IF CoinQtyIn005 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,1,CoinQtyIn005); END IF;
            IF CoinQtyIn010 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,2,CoinQtyIn010); END IF;
            IF CoinQtyIn025 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,3,CoinQtyIn025); END IF;
            IF CoinQtyIn050 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,4,CoinQtyIn050); END IF;
            IF CoinQtyIn100 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,5,CoinQtyIn100); END IF;
            IF CoinQtyIn200 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,6,CoinQtyIn200); END IF;
            
            /* Insert into PaymentCash if needed: Bills accepted */
            IF BillQtyIn01 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,101,BillQtyIn01); END IF;
            IF BillQtyIn02 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,102,BillQtyIn02); END IF;
            IF BillQtyIn05 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,103,BillQtyIn05); END IF;
            IF BillQtyIn10 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,104,BillQtyIn10); END IF;
            IF BillQtyIn20 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,105,BillQtyIn20); END IF;
            IF BillQtyIn50 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,1,106,BillQtyIn50); END IF;
            
            /* Insert into PaymentCash if needed: Coins dispensed */
            IF CoinQtyOut005 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,1,CoinQtyOut005); END IF;
            IF CoinQtyOut010 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,2,CoinQtyOut010); END IF;
            IF CoinQtyOut025 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,3,CoinQtyOut025); END IF;
            IF CoinQtyOut050 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,4,CoinQtyOut050); END IF;
            IF CoinQtyOut100 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,5,CoinQtyOut100); END IF;
            IF CoinQtyOut200 > 0 THEN INSERT PaymentCash (PurchaseId,IsAcceptOrDispense,DenominationTypeId,Quantity) VALUES (PurchaseId,2,6,CoinQtyOut200); END IF;
            
            /* Insert into Permit if needed (note: do not create a Permit record for a Test transaction or a Refund Slip) */
            IF (TransactionTypeId = 2 OR TransactionTypeId = 12 OR TransactionTypeId = 17) AND IsRefundSlip = 0 THEN
                INSERT Permit (PurchaseId,PermitNumber,LocationId,RateId,LicencePlateId,CellPhoneId,SpaceNumber,AddtimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT)
                VALUES        (PurchaseId,TicketNumber,PermitLocationId,RateId,LicencePlateId,CellPhoneId,StallNumber,AddTimeNum,PurchasedDate,ExpiryDate,ExpiryDate);
            END IF;
            
            /* Insert into PermitCommit. This table is used by ETL processing to extract Purchase & Purchase Payment & Permit & PermitExtension transactional data */
            /* and subsequently load it into a dimensional database. */
            SET InsertSeconds = TIMEDIFF(UTC_TIMESTAMP(),BeginGMT);
            INSERT PurchaseCommit (PurchaseId,InsertSeconds) VALUES (PurchaseId,IF(InsertSeconds>255,255,InsertSeconds));
                        
        /* Commit multi-table atomic transaction */
        COMMIT;
        
    END IF;
 
END//
delimiter ;

