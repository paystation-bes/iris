-- ----------------------------------------------------------------------------------
-- Note:            Revenue Types are denormalized in the Revenue table
-- ----------------------------------------------------------------------------------

SELECT  
        -- Scenario #1 | Total Revenue | RevenueType.Level = 1
        SUM(R.TotalRevenue) AS TotalRevenue ,
        
        -- Scenario #2 | Cash and Card Revenue | RevenueType.Level = 2
        SUM(R.CashRevenue) AS CashRevenue , 
        SUM(R.CardRevenue) AS CardRevenue ,
        
        -- Scenario #3 | Revenue Detail | RevenueType.Level = 3 (Cash)
        SUM(R.CoinRevenue) AS CoinRevenue ,
        SUM(R.BillRevenue) AS BillRevenue ,
        SUM(LegacyCashRevenue) AS LegacyCashRevenue ,
        -- Scenario #3 | Revenue Detail | RevenueType.Level = 3 (Card)
        SUM(R.CreditCardRevenue) AS CreditCardRevenue ,
        SUM(R.SmartCardRevenue) AS SmartCardRevenue ,
        SUM(R.ValueCardRevenue) AS ValueCardRevenue

-- ----------------------------------------------------------------------------------
-- Widget Query:    Total Revenue | This Month | T1: PayStation | Top & Bottom N
--
-- Note:            For `Top & Bottom N` two separate queries must be issued.
-- ----------------------------------------------------------------------------------

-- Top 5
SELECT  PS.Id, 
        PS.Name, 
        PS.SerialNumber, 
        SUM(R.TotalRevenue) AS TotalRevenue
        -- SUM(R.CashRevenue) AS CashRevenue , 
        -- SUM(R.CardRevenue) AS CardRevenue
FROM    PointOfSale PS
        INNER JOIN Revenue R ON PS.Id = R.PointOfSaleId
        INNER JOIN Time T ON R.TimeIdLocal = T.Id
WHERE   R.CustomerId = 3
AND     R.CustomerId = PS.CustomerId
AND     T.Year >= YEAR(NOW()) AND T.Month = MONTH(NOW()) AND T.DateTime <= NOW()
GROUP BY PS.Id
ORDER BY TotalRevenue DESC 
LIMIT 5;

-- Bottom 5
SELECT  PS.Id, 
        PS.Name, 
        PS.SerialNumber, 
        SUM(R.TotalRevenue) AS TotalRevenue
        -- SUM(R.CashRevenue) AS CashRevenue , 
        -- SUM(R.CardRevenue) AS CardRevenue
FROM    PointOfSale PS
        INNER JOIN Revenue R ON PS.Id = R.PointOfSaleId
        INNER JOIN Time T ON R.TimeIdLocal = T.Id
WHERE   R.CustomerId = 3
AND     R.CustomerId = PS.CustomerId
AND     T.Year >= YEAR(NOW()) AND T.Month = MONTH(NOW()) AND T.DateTime <= NOW()
GROUP BY PS.Id
ORDER BY TotalRevenue ASC 
LIMIT 5;

-- ----------------------------------------------------------------------------------
-- Widget Query:    Total Revenue | This Month | T1: Location | Top N 
--
-- Note:            Use Revenue.PurchaseLocationId (not Revenue.PermitLocationId)
--
--                  Revenue is realized at the Purchase Location (where money changed hands).
--                  Occupancy is realized at the Permit Location (where the vehicle was parked).
-- ----------------------------------------------------------------------------------

SELECT  L.Id, 
        L.Name, 
        SUM(R.TotalRevenue) AS TotalRevenue  
        -- SUM(R.CashRevenue) AS CashRevenue , 
        -- SUM(R.CardRevenue) AS CardRevenue
FROM    Location L
        INNER JOIN Revenue R ON L.Id = R.PurchaseLocationId
        INNER JOIN Time T ON R.TimeIdLocal = T.Id
WHERE   R.CustomerId = 3
AND     T.Year >= YEAR(NOW()) AND T.Month = MONTH(NOW()) AND T.DateTime <= NOW()
GROUP BY L.Id
ORDER BY TotalRevenue DESC 
LIMIT 5;

-- ----------------------------------------------------------------------------------
-- Widget Query:    Total Revenue | Last 30 Days | T1: Day | T2: Rate | T3: Transaction Type
-- ----------------------------------------------------------------------------------

SELECT  T.Date, R.Id AS RateId, R.RateName, TT.Id AS TxnTypeId, TT.Description AS TxnTypeDesc, SUM(V.TotalRevenue) AS TotalRevenue
FROM    Rate R
        INNER JOIN Revenue V ON R.Id = V.RateId
        INNER JOIN Time T ON V.TimeIdLocal = T.Id
        INNER JOIN TransactionType TT ON V.LegacyTransactionTypeId = TT.Id
WHERE   R.CustomerId = 3
AND     T.Date >= DATE_SUB(DATE(NOW()),INTERVAL 30 DAY) AND T.DateTime <= NOW()
AND     V.TotalRevenue > 0
GROUP BY T.Date, R.Id, TT.Id
ORDER BY T.Date, R.Id, TT.Id;

-- ----------------------------------------------------------------------------------
-- Widget Query:    Total Revenue | Last Month | T1: Location | T2: Rate
-- ----------------------------------------------------------------------------------

SELECT  L.Id AS LocationId, 
        L.Name AS LocationName, 
        A.Id AS RateId ,
        A.RateName ,
        SUM(R.TotalRevenue) AS TotalRevenue  
        -- SUM(R.CashRevenue) AS CashRevenue , 
        -- SUM(R.CardRevenue) AS CardRevenue
FROM    Location L
        INNER JOIN Revenue R ON L.Id = R.PurchaseLocationId
        INNER JOIN Rate A ON R.RateId = A.Id
        INNER JOIN Time T ON R.TimeIdLocal = T.Id
WHERE   R.CustomerId = 3
AND     T.Year >= YEAR(DATE_SUB(NOW(),INTERVAL 1 MONTH)) AND T.Month = MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH))
GROUP BY L.Id, A.Id
ORDER BY L.Id, A.Id;

-- ----------------------------------------------------------------------------------
-- Widget Query:    Total Revenue | Last 30 Days | T1: Day | T2: Route | T3: Rate 
-- ----------------------------------------------------------------------------------

SELECT  T.Date, RO.Id AS RouteId, RO.Name AS RouteName, RT.Id AS RateId, RT.RateName, SUM(RV.TotalRevenue) AS TotalRevenue
FROM    Route RO
        INNER JOIN RoutePOS RP ON RO.Id = RP.RouteId
        INNER JOIN Revenue  RV ON RP.PointOfSaleId = RV.PointOfSaleId 
        INNER JOIN Rate     RT ON RT.Id = RV.RateId
        INNER JOIN Time     T  ON RV.TimeIdLocal = T.Id
WHERE   RO.RouteTypeId = 1  -- RouteType.Id = 1 is a `Collections` type of Route
AND     RV.CustomerId = 3
AND     T.Date >= DATE_SUB(DATE(NOW()),INTERVAL 30 DAY) AND T.DateTime <= NOW()
GROUP BY T.Date, RO.Id, RT.Id
ORDER BY T.Date, RO.Id, RT.Id;

