
-- ****************************************************************

-- This is a Draft Version only

-- ****************************************************************

-- PurchaseCollection
-- Force Insert

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT INTO PurchaseCollection (PurchaseId,PointOfSaleId,PurchaseGMT,	CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) VALUES 
								( 101, 1,				UTC_TIMESTAMP(), 0,			100,			1,		200,			2		);
								
								

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


								
-- PreAuth
-- Force Insert

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT INTO PreAuth (MerchantAccountId, Amount, Last4DigitsOfCardNumber, CardType, IsApproved, PreAuthDate, 			PointOfSaleId, CardHash, CardExpiry, IsRFID) VALUES
					(1,					150,	1234,					 'VISA',	1,			UTC_TIMESTAMP(),		1,			    'ABCD',	  1215,			0 );


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


/*
0	All
1	Bill
2	Coin
3	Card
*/
-- POSCollection AUDIT
-- Force Insert

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT INTO POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, 	StartGMT, 		EndGMT) VALUES
						   ( 6,			1,				0,				1,						UTC_TIMESTAMP(), UTC_TIMESTAMP() );
						   
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- POSCollection Card
-- Force Insert

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT INTO POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, 	StartGMT, 		EndGMT) VALUES
						   ( 6,			1,				3,				1,						UTC_TIMESTAMP(), UTC_TIMESTAMP() );
						   
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;