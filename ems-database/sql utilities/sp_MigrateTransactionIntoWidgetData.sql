-- ---------------------------------------------------------------------------------------------------
-- Procedure:   sp_MigrateTransactionIntoWidgetData
--
-- Important:   There is SQL code at the end of the stored procedure to prepare data for the test migration.
--              *** Do NOT run this file like a script! Copy and paste the code you want. ***
-- 
-- Purpose:     Inserts test data records into the following EMS 7 widget data tables:
--              o  PPPInfo
--              o  PPPInfoDetail
--              o  Occupancy
-- 
--              The source of this test data is a subset of rows copied from the EMS 6 `Transaction` and `Rates` tables. 
--              The corresponding tables in Paul's local EMS 7 database are `ems_6_Transaction` and `ems_6_Rates`.
--              The copy of EMS 6 production data used is circa April 2012. 
-- 
--              The data in tables `ems_6_Transaction` and `ems_6_Rates` has been transformed: 
--              the foreign keys point to records in EMS 7 tablesm such as: Customer, Location, PointOfSale, UnifiedRate, etc. 
-- 
--              There are 948,369 test data rows in table `ems_6_Transaction`.
--              There are 124,434 test data rows in table `ems_6_Rates`.
--
--              The challenge in creating widget test data is assigning numerous foreign key values properly 
--              for each record to INSERT. 
--
--
-- Comment:     The value of this stored procedure is not the code but rather the introduction to the logic used
--              to load widget data. In EMS 7 there will be a formal ETL process that extracts data
--              from the refactored EMS 6 Transaction tables (such as Purchase, Permit, PaymentCash, and PaymentCard)
--              transfoms that operational data into the required business data that is stored in the widget data tables
--              (PPPInfo, PPPInfoDetail, Occupancy), and then loads that business data into the widget data tables.
--              That loading will most likely not be individual INSERT statements as used in this procedure
--              but rather a bulk load from a disk file.
--
-- Call:        CALL sp_MigrateTransactionIntoWidgetData;
--
-- Dump File:   To dump the contents of the the widget data tables Time, PPPInfo, PPPInfoDetail, and Occupancy use the following command:
--
--              mysqldump --opt --order-by-primary --result-file=S:\\PaulW\\DatabaseDumps\\ems_db_7_widget_data.sql --user=root --password=foobar ems_db_7 Time PPPInfo PPPInfoDetail Occupancy
--
-- Updated:     This procedure was last updated Friday, January 4, 2013 by Paul W.    
-- ---------------------------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_MigrateTransactionIntoWidgetData;

delimiter //
CREATE PROCEDURE sp_MigrateTransactionIntoWidgetData()
BEGIN
    -- Variable for counting each row returned by the cursor
    DECLARE RowNumber                               INT UNSIGNED DEFAULT 0;
    DECLARE RowsToMigrate                           INT UNSIGNED DEFAULT 0;
   
    -- Columns from EMS 6 Transaction table
    DECLARE t_CustomerId                            MEDIUMINT UNSIGNED;
    DECLARE t_PaystationId                          MEDIUMINT UNSIGNED;
    DECLARE t_TicketNumber                          INT UNSIGNED;
    DECLARE t_LotNumber                             VARCHAR(20);
    DECLARE t_StallNumber                           MEDIUMINT UNSIGNED;
    DECLARE t_TypeId                                TINYINT UNSIGNED;
    DECLARE t_PurchasedDate                         DATETIME;
    DECLARE t_BeginDate                             DATETIME;
    DECLARE t_ExpireDate                            DATETIME;
    DECLARE t_ChargedAmount                         MEDIUMINT UNSIGNED;
    DECLARE t_CashPaid                              MEDIUMINT UNSIGNED;
    DECLARE t_CardPaid                              MEDIUMINT UNSIGNED;
    DECLARE t_ChangeDispensed                       MEDIUMINT UNSIGNED;
    DECLARE t_ExcessPayment                         MEDIUMINT UNSIGNED;
    DECLARE t_IsRefundSlip                          TINYINT UNSIGNED;
    DECLARE t_CustomCardData                        VARCHAR(40);
    DECLARE t_CustomCardType                        VARCHAR(20);
    DECLARE t_CouponNumber                          VARCHAR(20);
    DECLARE t_CitationNumber                        VARCHAR(20);
    DECLARE t_SmartCardData                         VARCHAR(120);
    DECLARE t_SmartCardPaid                         MEDIUMINT UNSIGNED;
    DECLARE t_PaymentTypeId                         TINYINT UNSIGNED;
    DECLARE t_CreationDate                          DATETIME;
    DECLARE t_RateId                                BIGINT UNSIGNED;
    DECLARE t_AuthenticationCard                    VARCHAR(37);
    DECLARE t_RegionId                              MEDIUMINT UNSIGNED;
    DECLARE t_AddTimeNum                            INT UNSIGNED;
    DECLARE t_OriginalAmount                        MEDIUMINT UNSIGNED;
    DECLARE t_IsOffline                             TINYINT UNSIGNED;
    DECLARE t_PlateNumber                           VARCHAR(10);
    DECLARE t_CoinCount                             TINYINT UNSIGNED DEFAULT 0;
    DECLARE t_BillCount                             TINYINT UNSIGNED DEFAULT 0;
    
    -- Columns from EMS 6 Rates table
    DECLARE r_RateName                              VARCHAR(20);
    DECLARE r_RateValue                             MEDIUMINT UNSIGNED;
    DECLARE r_Revenue                               MEDIUMINT;
    
    -- Variables for dates and calculated (derived) values
    DECLARE t_BeginDateLocal                        DATETIME;
    DECLARE t_BeginDateDST                          DATETIME;
    DECLARE t_ExpireDateLocal                       DATETIME;
    DECLARE t_ExpireDateDST                         DATETIME;  
    DECLARE t_PurchasedDate2                        DATETIME;
    DECLARE t_BeginDate2                            DATETIME;
    DECLARE t_ExpireDate2                           DATETIME;
    DECLARE t_PurchasedDateLocal                    DATETIME;
    DECLARE t_PurchasedDateDST                      DATETIME;
        
    -- Columns from EMS 6 ProcessorTransaction table
    DECLARE p_ProcTxnTypeId                         TINYINT UNSIGNED;
    DECLARE p_CardType                              VARCHAR(20);   
    
    -- Primary key Id Columns from EMS 7 tables
    DECLARE t_PurchaseId                            INT UNSIGNED;
    DECLARE t_PermitId                              INT UNSIGNED;
        
    -- Columns from EMS 7 PPPInfo table
    DECLARE ppp_Id	                                INT UNSIGNED;
    DECLARE ppp_TimeIdGMT	                        MEDIUMINT UNSIGNED;
    DECLARE ppp_TimeIdLocal	                        MEDIUMINT UNSIGNED;
    DECLARE ppp_CustomerId	                        MEDIUMINT UNSIGNED;
    DECLARE ppp_PurchaseLocationId	                MEDIUMINT UNSIGNED;
    DECLARE ppp_PermitLocationId	                MEDIUMINT UNSIGNED;
    DECLARE ppp_SpaceNumber	                        MEDIUMINT UNSIGNED;
    DECLARE ppp_PointOfSaleId	                    MEDIUMINT UNSIGNED;
    DECLARE ppp_PaystationId	                    MEDIUMINT UNSIGNED;
    DECLARE ppp_PaystationTypeId	                TINYINT UNSIGNED;
    DECLARE ppp_UnifiedRateId	                    MEDIUMINT UNSIGNED;
    DECLARE ppp_PaystationSettingId	                MEDIUMINT UNSIGNED;
    DECLARE ppp_PurchaseId	                        INT UNSIGNED;
    DECLARE ppp_PermitId	                        INT UNSIGNED;
    DECLARE ppp_PermitIssueTypeId	                TINYINT UNSIGNED;
    DECLARE ppp_PermitTypeId	                    TINYINT UNSIGNED;
    DECLARE ppp_IsCoupon	                        TINYINT UNSIGNED;
    DECLARE ppp_IsRefundSlip	                    TINYINT UNSIGNED;
    DECLARE ppp_IsOffline	                        TINYINT UNSIGNED;
    DECLARE ppp_IsFreePermit                        TINYINT UNSIGNED;
    DECLARE ppp_IsUploadedFromBoss	                TINYINT UNSIGNED;
    DECLARE ppp_IsRFID	                            TINYINT UNSIGNED;
    DECLARE ppp_TransactionTypeId	                TINYINT UNSIGNED;
    DECLARE ppp_PaymentTypeId	                    TINYINT UNSIGNED;
    DECLARE ppp_ProcessorTransactionTypeId          TINYINT UNSIGNED;
    DECLARE ppp_DurationMinutes                     MEDIUMINT UNSIGNED;
    
    -- For QuarterOfDay from TimeIdLocal
    DECLARE ppp_QuarterOfDay                        TINYINT UNSIGNED;
    
    -- Columns from EMS 7 Revenue table
    DECLARE rev_Total_L1                            MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_Cash_L2                             MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_Card_L2                             MEDIUMINT UNSIGNED DEFAULT 0;  
    DECLARE rev_Coin_L3                             MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_Bill_L3                             MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_LegacyCash_L3                       MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_CreditCard_L3                       MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_SmartCard_L3                        MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE rev_ValueCard_L3                        MEDIUMINT UNSIGNED DEFAULT 0; 
    DECLARE rev_PatrollerCard                       MEDIUMINT UNSIGNED DEFAULT 0;
    
    -- A Customer's values for these variables are used to set PPPInfo.TimeIdLocal
    DECLARE c_GMTtoLocal                            CHAR(6);
    DECLARE c_DSTtoLocal                            CHAR(6);
    DECLARE c_IsDST                                 TINYINT UNSIGNED;
    
    -- Occupancy variables  
    DECLARE occ_BeginTimeIdLocal                    MEDIUMINT UNSIGNED;   
    DECLARE occ_ExpireTimeIdLocal                   MEDIUMINT UNSIGNED;
    DECLARE occ_BeginMinutes                        TINYINT UNSIGNED;
    DECLARE occ_ExpireMinutes                       TINYINT UNSIGNED;
    DECLARE occ_PermitBegin                         DATETIME;
    DECLARE occ_PermitExpire                        DATETIME;
    DECLARE occ_DurationMinutes                     MEDIUMINT UNSIGNED;
    DECLARE occ_InitialPermitId                     INT UNSIGNED;
    DECLARE occ_NumberOfExtensions                  TINYINT UNSIGNED DEFAULT 0;
    
  
    -- Cursor for retrieving EMS 6 test data from EMS 6 `Transaction` and EMS 6 `Rates` table.
    -- Actually `Transaction` and `Rates` data from EMS 6 has been brought over into the EMS 7 test data tables `ems_6_Transaction` and `ems_6_Rates`.
    -- The data in tables `ems_6_Transaction` and `ems_6_Rates` has been transformed: the foreign keys in these taqbles align with foreign keys in EMS 7 master data tables, like: `Customer`, `Location`, `PointOfSale`, etc. 
    DECLARE CursorDone INT DEFAULT 0;
    DECLARE TransactionCursor CURSOR FOR    SELECT  T.CustomerId,               -- these attributes from `ems_6_Transaction`
                                                    T.PaystationId,
                                                    T.TicketNumber,
                                                    T.LotNumber,
                                                    T.StallNumber,
                                                    T.TypeId,
                                                    T.PurchasedDate,
                                                    T.ExpiryDate,
                                                    T.ChargedAmount,
                                                    T.CashPaid,
                                                    T.CardPaid,
                                                    T.ChangeDispensed,
                                                    T.ExcessPayment,
                                                    T.IsRefundSlip,
                                                    T.CustomCardData,
                                                    T.CustomCardType,
                                                    T.CouponNumber,
                                                    T.CitationNumber,
                                                    T.SmartCardData,
                                                    T.SmartCardPaid,
                                                    T.PaymentTypeId,
                                                    T.CreationDate,                                                  
                                                    T.RateId,
                                                    T.AuthenticationCard,
                                                    T.RegionId,
                                                    T.AddTimeNum,
                                                    T.OriginalAmount,
                                                    T.IsOffline,
                                                    T.PlateNumber,
                                                    R.RateName,                 -- these attributes from `ems_6_Rates` via LEFT OUTER JOIN because most Transaction records do not have a Rates record
                                                    R.RateValue,
                                                    R.Revenue
                                            FROM    ems_db_pw.ems_6_Transaction T
                                                    LEFT OUTER JOIN ems_db_pw.ems_6_Rates R USING (PaystationId, PurchasedDate, TicketNumber, TypeId, RateId, LotNumber)
                                            ORDER BY T.PurchasedDate;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
    
    -- Open the cursor
    SELECT NOW() AS BeginMigration;
    SELECT 'Opening cursor...' AS Info;
    OPEN TransactionCursor;
    SELECT FOUND_ROWS() INTO RowsToMigrate;

    -- Loop through each row returned by cursor
    Loop_For_Cursor: LOOP
    
        -- Place contents of a cursor row (a `Transaction` record from kpi_db.MigrateTransaction [i.e., EMS 6]) into local variables
        -- This `Transaction` record has been sanitized: Customer, Region and Paystation Id's have been changed to match EMS 7 test data Id's.
        FETCH TransactionCursor INTO    t_CustomerId,
                                        t_PaystationId,
                                        t_TicketNumber,
                                        t_LotNumber,
                                        t_StallNumber,
                                        t_TypeId,
                                        t_PurchasedDate2,
                                        t_ExpireDate2,
                                        t_ChargedAmount,
                                        t_CashPaid,
                                        t_CardPaid,
                                        t_ChangeDispensed,
                                        t_ExcessPayment,
                                        t_IsRefundSlip,
                                        t_CustomCardData,
                                        t_CustomCardType,
                                        t_CouponNumber,
                                        t_CitationNumber,
                                        t_SmartCardData,
                                        t_SmartCardPaid,
                                        t_PaymentTypeId,
                                        t_CreationDate,
                                        t_RateId,
                                        t_AuthenticationCard,
                                        t_RegionId,
                                        t_AddTimeNum,
                                        t_OriginalAmount,
                                        t_IsOffline,
                                        t_PlateNumber,
                                        r_RateName,
                                        r_RateValue,
                                        r_Revenue;
                                        
        -- In EMS 6 the Permit Begin Date is the same as the Purchase Date (in EMS 7 these two datetimes can be different)
        SET t_BeginDate2 = t_PurchasedDate2;
                           
        -- Check to see if the cursor has returned its last row
        IF (CursorDone AND RowNumber = RowsToMigrate) OR RowNumber = RowsToMigrate THEN 
            SELECT RowNumber, RowsToMigrate;
            SELECT t_PaystationId AS Paystation, t_PurchasedDate2 AS PurchasedDate, t_TicketNumber AS TicketNumber;
            LEAVE Loop_For_Cursor;
        END IF;
        
        -- Count each row returned by the cursor
        SET RowNumber = RowNumber + 1;

        -- This is test data, we are not interested in all possible data scenarios
        -- NOTE: IN THE FUTURE MUST DETERMINE HOW TO HANDLE t_IsRefundSlip = 1
        IF t_IsRefundSlip = 0 AND t_TypeId IN (1,2,4,5,6,12,16,17) THEN
        
            -- The source data from EMS 6 is very old, make it more recent
            SET t_PurchasedDate = DATE_ADD(t_PurchasedDate2,INTERVAL 46 WEEK);
            SET t_BeginDate     = DATE_ADD(t_BeginDate2    ,INTERVAL 46 WEEK);
            SET t_ExpireDate    = DATE_ADD(t_ExpireDate2   ,INTERVAL 46 WEEK);
        
            -- Set foreign key for PPPInfo.TimeIdGMT 
            SELECT MAX(Id) INTO ppp_TimeIdGMT FROM Time WHERE DateTime <= t_PurchasedDate;
        
            -- BEGIN Timezone logic ---------------------------------------------------------------------------------------------
            -- 
            -- Get Customer timezone information
            -- This logic relies on tables Timezone and DaylightSaving to perform timezone conversions (GMT to Local time)
            -- Going forward in EMS 7 we will use MySQL timezone tables that enable the use of a MySQL function to perform timezone conversions
            SELECT T.OffsetFromGMT, T.OffsetFromDST INTO c_GMTtoLocal, c_DSTtoLocal FROM CustomerProperty P, Timezone T WHERE P.CustomerPropertyTypeId = 1 AND P.PropertyValue = T.Name AND CustomerId = t_CustomerId;

            -- Determine the Purchased Date in local time (but not adjusting for DST (Daylight Saving Time)) 
            SET t_PurchasedDateLocal = CONVERT_TZ(t_PurchasedDate,'+00:00',c_GMTtoLocal); 
            
            -- Now determine the Purchased Date in local time adjusted for DST
            SET t_PurchasedDateDST = CONVERT_TZ(t_PurchasedDate,'+00:00',c_DSTtoLocal);            
            
            -- Determine which local time to use: local time without or local time with DST (it depends on whether or not the Purchased Date falls into DST)
            SELECT COUNT(*) INTO c_IsDST FROM DaylightSaving WHERE t_PurchasedDateLocal >= BeginDST AND t_PurchasedDateLocal < EndDST;

            -- Set foreign key for PPPInfo.TimeIdLocal
            IF c_IsDST THEN
                SELECT MAX(Id) INTO ppp_TimeIdLocal FROM Time WHERE DateTime <= t_PurchasedDateDST;
            ELSE
                SELECT MAX(Id) INTO ppp_TimeIdLocal FROM Time WHERE DateTime <= t_PurchasedDateLocal;
            END IF;
            -- END Timezone logic -----------------------------------------------------------------------------------------------
           
            -- Determine QuarterOfDay
            SELECT QuarterOfDay INTO ppp_QuarterOfDay FROM Time WHERE Id = ppp_TimeIdLocal;
            
            -- Manufacture some completely fictitious test data: set t_RateId (the Rate used) based on ppp_QuarterOfDay (time of day)
            -- Note: t_RateId must be a foreign key into table `UnifiedRate` for the specified CustomerId 
            --       In a fully functional EMS 7 `UnifiedRate` will have a default record for EACH Customer where UnifiedRate.Name = `Unknown` (or `N/A`))
            -- Note: the logic being used here is purely for test data and is not intended for production code. 
            
            -- ------------------------------------------------------------------------------------------------------------------
            -- WARNING: Test data for Customer.Id = 3, Name = `Park San Francisco` is INCOMPLETE: it has NO data in table ems_6_Rates. 
            --          Test data for Customer.Id = 5, Name = `Park Dallas` is complete: it has data in table ems_6_Rates.
            --          That's why the logic for these two companies is different in the code below. 
            -- ------------------------------------------------------------------------------------------------------------------
            -- Customer.Id = 3, Name = `Park San Francisco`
            IF t_CustomerId = 3 THEN
                SET t_RateId =  IF(ppp_QuarterOfDay>=0  AND ppp_QuarterOfDay<=19,2,
                                IF(ppp_QuarterOfDay>=20 AND ppp_QuarterOfDay<=27,3,
                                IF(ppp_QuarterOfDay>=28 AND ppp_QuarterOfDay<=47,4,
                                IF(ppp_QuarterOfDay>=48 AND ppp_QuarterOfDay<=71,5,
                                IF(ppp_QuarterOfDay>=72 AND ppp_QuarterOfDay<=95,6,1)))));
            -- Customer.Id = 5, Name = `Park Dallas`                    
            ELSEIF t_CustomerId = 5 THEN
                SELECT Id INTO t_RateId FROM UnifiedRate WHERE CustomerId = t_CustomerId AND Name = IFNULL(r_RateName,'Unknown');    
            END IF;
            
            -- Set values other PPPInfo attributes
            SET ppp_CustomerId	                     = t_CustomerId;
            SET ppp_PurchaseLocationId	             = t_RegionId;      -- Needs to be validated. 
            SET ppp_PermitLocationId	             = t_RegionId;      -- Needs to be validated. 
            SET ppp_SpaceNumber	                     = t_StallNumber;
            SET ppp_PointOfSaleId	                 = t_PaystationId;
            SET ppp_PaystationId	                 = t_PaystationId;
            SELECT PaystationTypeId INTO ppp_PaystationTypeId FROM Paystation WHERE Id = t_PaystationId;
            SET ppp_UnifiedRateId	                 = t_RateId;
            SELECT Id INTO ppp_PaystationSettingId FROM PaystationSetting WHERE CustomerId = t_CustomerId AND Name = t_LotNumber;   
            SET ppp_PurchaseId	                     = RowNumber;
            SET ppp_PermitId	                     = IF(t_TypeId IN (1,2,12,16,17),RowNumber,NULL);
            SET ppp_PermitIssueTypeId	             = 0;       -- Default value is zero              
            IF ppp_PermitId > 0 THEN
                SET ppp_PermitIssueTypeId = IF(LENGTH(t_PlateNumber)>0,2,IF(t_StallNumber>0,3,1));   -- TO DO: Valdiate this logic for use in a production environment.
            END IF;
            SET ppp_PermitTypeId	                 = IF(t_TypeId=2,1,IF(t_TypeId=12,5,IF(t_TypeId=17,3,0)));
            SET ppp_IsCoupon	                     = IF(LENGTH(t_CouponNumber)>0,1,0);
            SET ppp_IsRefundSlip	                 = t_IsRefundSlip;
            SET ppp_IsOffline	                     = t_IsOffline;
            SET ppp_IsFreePermit	                 = IF((t_TypeId=2 OR t_TypeId=1 OR t_TypeId=12 OR t_TypeId=17 OR t_TypeId=16) AND t_CashPaid=0 AND t_CardPaid=0 AND t_SmartCardPaid=0,1,0);
            SET ppp_IsUploadedFromBoss	             = 0;       -- Purposely set to zero for this test data (from ProcessorTransaction)
            SET ppp_IsRFID	                         = 0;       -- Purposely set to zero for this test data (from ProcessorTransaction)
            SET ppp_TransactionTypeId	             = t_TypeId;
            SET ppp_PaymentTypeId	                 = t_PaymentTypeId;
            SET ppp_ProcessorTransactionTypeId       = NULL;    -- Purposely set to NULL for this test data (from ProcessorTransaction)
            
            -- Reset values for Revenue attributes
            SET rev_Total_L1                         = 0;
            SET rev_Cash_L2                          = 0;
            SET rev_Card_L2                          = 0;
            SET rev_Coin_L3                          = 0;
            SET rev_Bill_L3                          = 0;
            SET rev_LegacyCash_L3                    = 0;
            SET rev_CreditCard_L3                    = 0;
            SET rev_SmartCard_L3                     = 0;
            SET rev_ValueCard_L3                     = 0;
            SET rev_PatrollerCard                    = 0;
           
            -- Set values for Revenue attributes
            SET rev_Coin_L3         = 0;  -- NOTE: this information is not available in the data set being used (Transaction table data from mid-2011)
            SET rev_Bill_L3         = 0;  -- NOTE: this information is not available in the data set being used (Transaction table data from mid-2011)
            SET rev_LegacyCash_L3   = t_CashPaid - IF(t_TypeId=4 AND t_PaymentTypeId=1, t_ChargedAmount, 0) - IF(t_TypeId=8 AND t_CashPaid > 0, t_CashPaid, 0) - IF(t_CashPaid > 0 AND t_IsRefundSlip, (t_CashPaid + t_CardPaid - t_ChargedAmount), t_ChangeDispensed) - IF(t_TypeId=11 OR t_TypeId=13 OR t_TypeId=14, t_CashPaid, 0);
            SET rev_CreditCard_L3   = IF(t_PaymentTypeId=2 OR t_PaymentTypeId=5, t_CardPaid, 0) - IF(t_TypeId=4 AND t_PaymentTypeId=2, t_ChargedAmount, 0);
            SET rev_SmartCard_L3    = t_SmartCardPaid;
            SET rev_ValueCard_L3    = IF(t_PaymentTypeId=7 OR t_PaymentTypeId=9, t_CardPaid, 0);
            SET rev_PatrollerCard   = IF(t_PaymentTypeId=3 AND t_TypeId<>6, t_CardPaid, 0);
            SET ppp_DurationMinutes = IF(t_TypeId IN (1,2,12,16,17),(TO_SECONDS(t_ExpireDate)-TO_SECONDS(t_BeginDate))/60,0);
            
            -- By definition Value Card, Custom Card and Patroller Card are all the same
            SET rev_ValueCard_L3   = rev_ValueCard_L3 + rev_PatrollerCard;
            
            -- Determine Total Cash and Total Card Revenue (Level 2)
            SET rev_Cash_L2        = rev_Coin_L3 + rev_Bill_L3 + rev_LegacyCash_L3;
            SET rev_Card_L2        = rev_CreditCard_L3 + rev_SmartCard_L3 + rev_ValueCard_L3;

            -- Determine Total Revenue (Level 1)
            SET rev_Total_L1       = rev_Cash_L2 + rev_Card_L2;
            
            -- Insert into PPPInfo
            -- CONSIDER not inserting into PPPInfo & PPPInfoDetail if t_TypeId IN (4,6) [Note: 4=Smart-Card-Recharge and 6=Test (has yet to be determined as of July 9 2012)]
            INSERT  PPPInfo (TimeIdGMT,TimeIdLocal,CustomerId,PurchaseLocationId,PermitLocationId,SpaceNumber,PointOfSaleId,PaystationId,PaystationTypeId,UnifiedRateId,PaystationSettingId,PurchaseId,PermitId,PermitIssueTypeId,PermitTypeId,CoinCount,BillCount,DurationMinutes,IsCoupon,IsRefundSlip,IsOffline,IsFreePermit,IsUploadedFromBoss,IsRFID,TransactionTypeId,PaymentTypeId,ProcessorTransactionTypeId)
            VALUES  (
                        ppp_TimeIdGMT ,
                        ppp_TimeIdLocal ,
                        ppp_CustomerId ,
                        ppp_PurchaseLocationId ,
                        ppp_PermitLocationId ,
                        ppp_SpaceNumber ,
                        ppp_PointOfSaleId ,
                        ppp_PaystationId ,
                        ppp_PaystationTypeId ,
                        ppp_UnifiedRateId ,
                        ppp_PaystationSettingId ,
                        ppp_PurchaseId ,
                        ppp_PermitId ,
                        ppp_PermitIssueTypeId ,
                        ppp_PermitTypeId ,
                          t_CoinCount ,
                          t_BillCount ,
                        ppp_DurationMinutes ,
                        ppp_IsCoupon ,
                        ppp_IsRefundSlip ,
                        ppp_IsOffline ,
                        ppp_IsFreePermit ,
                        ppp_IsUploadedFromBoss ,
                        ppp_IsRFID ,
                        ppp_TransactionTypeId ,
                        ppp_PaymentTypeId ,
                        ppp_ProcessorTransactionTypeId        
                    );  

            -- Get new primary key value for PPPInfo (PPPInfo.Id)
            SELECT LAST_INSERT_ID() INTO ppp_Id;  

            -- This is TEST DATA ONLY
            -- INSERT into PPPInfoDetail
            -- Save Revenue BUT ONLY IF THERE IS MONEY (of some kind)
            IF ppp_PermitId > 0 AND rev_Total_L1 > 0 THEN
            
                -- Total Revenue
                INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                VALUES               (ppp_Id   ,1            ,0         ,0               ,0                ,rev_Total_L1);
 
                -- Total Cash Revenue
                IF rev_Cash_L2 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,2            ,0         ,0               ,0                ,rev_Cash_L2);
                END IF;  
                
                -- Total Card Revenue
                IF rev_Card_L2 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,3            ,0         ,0               ,0                ,rev_Card_L2);
                END IF;  
                
                -- Coin Revenue
                IF rev_Coin_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,4            ,0         ,0               ,0                ,rev_Coin_L3);                
                END IF;                
            
                -- Bill Revenue
                IF rev_Bill_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,5            ,0         ,0               ,0                ,rev_Bill_L3);                
                END IF;
                
                -- Bill Legacy Cash (Coin + Bill)
                IF rev_LegacyCash_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,6            ,0         ,0               ,0                ,rev_LegacyCash_L3);                
                END IF;

                -- Credit Card Revenue
                IF rev_CreditCard_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,7            ,0         ,0               ,0                ,rev_CreditCard_L3);                 
                END IF;

                -- Smart Card Revenue
                IF rev_SmartCard_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,8            ,0         ,0               ,0                ,rev_SmartCard_L3); 
                END IF;

                -- Value Card Revenue
                IF rev_ValueCard_L3 > 0 THEN
                    INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,Amount)
                    VALUES               (ppp_Id   ,9            ,0         ,0               ,0                ,rev_ValueCard_L3); 
                END IF;     
            END IF;   
           
            -- Insert into Occupancy table
            -- Note: Only insert if there is a Permit involved (not all Purchases have a Permit, for example: Smart Card Recharge, Test, Cancelled)
            IF (t_TypeId = 2 OR t_TypeId = 1 OR t_TypeId = 12 OR t_TypeId = 17 OR t_TypeId = 16) THEN
            
                -- There are 4 steps that need to be performed:
                -- 1.   determine occ_BeginTimeIdLocal: the begin datetime for the Permit(foreign key into table Time)
                -- 2.   determine occ_ExpireTimeIdLocal: the expire datetime for the Permit
                -- 3.   determine occ_BeginMinutes: the number of minutes from the beginning 15 time block (identified by occ_BeginTimeIdLocal) that were for paid parking
                -- 4.   determine occ_ExpireMinutes: the number of minutes from the expire 15 time block (identified by occ_ExpireTimeIdLocal) that were for paid parking

                -- ------------------------------------ Steps 1, 3, 4 ------------------------------------
                -- Determine the Begin Date in local time (but not adjusting for DST (Daylight Saving Time)) 
                SET t_BeginDateLocal = CONVERT_TZ(t_BeginDate,'+00:00',c_GMTtoLocal); 
            
                -- Now determine the Begin Date in local time adjusted for DST
                SET t_BeginDateDST = CONVERT_TZ(t_BeginDate,'+00:00',c_DSTtoLocal);            
            
                -- Determine which local time to use: local time without or local time with DST (it depends on whether or not the Begin Date falls into DST)
                SELECT COUNT(*) INTO c_IsDST FROM DaylightSaving WHERE t_BeginDateLocal >= BeginDST AND t_BeginDateLocal < EndDST;

                -- Set foreign key for Occupancy.BeginTimeIdLocal, and get the number of minutes into respective 15 minute segments when the Permit begins and expires
                IF c_IsDST THEN
                    SELECT MAX(Id) INTO occ_BeginTimeIdLocal FROM Time WHERE DateTime <= t_BeginDateDST;
                    SELECT 15-MOD(EXTRACT(MINUTE FROM t_BeginDateDST),15) INTO occ_BeginMinutes;
                    SET occ_PermitBegin = t_BeginDateDST;
                ELSE
                    SELECT MAX(Id) INTO occ_BeginTimeIdLocal FROM Time WHERE DateTime <= t_BeginDateLocal;
                    SELECT 15-MOD(EXTRACT(MINUTE FROM t_BeginDateLocal),15) INTO occ_BeginMinutes;
                    SET occ_PermitBegin = t_BeginDateLocal;
                END IF; 
                SET occ_ExpireMinutes = 15 - occ_BeginMinutes;
                
                -- ------------------------------------ Step 2 ------------------------------------
                -- Determine the Begin Date in local time (but not adjusting for DST (Daylight Saving Time)) 
                SET t_ExpireDateLocal = CONVERT_TZ(t_ExpireDate,'+00:00',c_GMTtoLocal); 
            
                -- Now determine the Begin Date in local time adjusted for DST
                SET t_ExpireDateDST = CONVERT_TZ(t_ExpireDate,'+00:00',c_DSTtoLocal);            
            
                -- Determine which local time to use: local time without or local time with DST (it depends on whether or not the Begin Date falls into DST)
                SELECT COUNT(*) INTO c_IsDST FROM DaylightSaving WHERE t_ExpireDateLocal >= BeginDST AND t_ExpireDateLocal < EndDST;

                -- Set foreign key for Occupancy.BeginTimeIdLocal, and get the number of minutes into respective 15 minute segments when the Permit begins and expires
                -- WARNING: This logic may not be 100% correct, David noticed some discrepencies, but I have yet to follow up
                -- CONCEPT: There is: (1) a beginning 15 minute bucket, (2) an ending 15 minute bucket, and possibly (3) a series of buckets in between (1) and (2)
                --          Note that (1) and (2) can be the same 15 minute bucket.
                --          Total duration of a parking permit in minutes is minutes in bucket (1), plus minutes in bucket (2), plus 15 minutes multiplied by the number of buckets between (1) and (2)                
                IF c_IsDST THEN
                    SELECT MAX(Id) INTO occ_ExpireTimeIdLocal FROM Time WHERE DateTime <= t_ExpireDateDST;
                    SET occ_PermitExpire = t_ExpireDateDST;
                ELSE
                    SELECT MAX(Id) INTO occ_ExpireTimeIdLocal FROM Time WHERE DateTime <= t_ExpireDateLocal;
                    SET occ_PermitExpire = t_ExpireDateLocal;
                END IF; 
                SET occ_DurationMinutes = (TO_SECONDS(occ_PermitExpire) - TO_SECONDS(occ_PermitBegin))/60;
               
                -- INSERT the Occupancy record 
                INSERT  Occupancy (CustomerId,PointOfSaleId,PaystationSettingId,PermitLocationId,UnifiedRateId,BeginTimeIdLocal,ExpireTimeIdLocal,PurchaseId,InitialPermitId,BeginMinutes,ExpireMinutes,DurationMinutes, NumberOfExtensions)
                VALUES  (ppp_CustomerId, ppp_PointOfSaleId, ppp_PaystationSettingId, ppp_PermitLocationId, ppp_UnifiedRateId, occ_BeginTimeIdLocal, occ_ExpireTimeIdLocal, ppp_PurchaseId, ppp_PermitId, occ_BeginMinutes, occ_ExpireMinutes, occ_DurationMinutes, occ_NumberOfExtensions);           
            END IF;
                                       
        END IF;
    END LOOP Loop_For_Cursor;
    
    -- Close the cursor
    CLOSE TransactionCursor;
               
    SELECT 'Processing complete' AS Info;
    SELECT NOW() AS EndMigration;
END //
delimiter ;

-- ---------------------------------------------------------------------------------------------------
-- Table `ems_6_Transaction` contains a subset of rows from the EMS 6 `Transaction` table
-- ---------------------------------------------------------------------------------------------------
   
DROP TABLE IF EXISTS `ems_6_Transaction`;
CREATE TABLE `ems_6_Transaction` (
  `CustomerId` mediumint(8) unsigned NOT NULL default '0',
  `PaystationId` mediumint(8) unsigned NOT NULL default '0',
  `TicketNumber` int(10) unsigned NOT NULL default '0',
  `LotNumber` varchar(20) NOT NULL default '',
  `StallNumber` mediumint(8) unsigned NOT NULL default '0',
  `TypeId` tinyint(3) unsigned NOT NULL default '0',
  `PurchasedDate` datetime NOT NULL ,
  `ExpiryDate` datetime NOT NULL ,
  `ChargedAmount` mediumint(8) unsigned NOT NULL default '0',
  `CashPaid` mediumint(8) unsigned NOT NULL default '0',
  `CardPaid` mediumint(8) unsigned NOT NULL default '0',
  `ChangeDispensed` mediumint(8) unsigned NOT NULL default '0',
  `ExcessPayment` mediumint(8) unsigned NOT NULL default '0',
  `IsRefundSlip` tinyint(1) NOT NULL default '0',
  `CustomCardData` varchar(40) NOT NULL default '',
  `CustomCardType` varchar(20) NOT NULL default '',
  `CouponNumber` varchar(20) NOT NULL default '',
  `CitationNumber` varchar(20) NOT NULL default '',
  `SmartCardData` varchar(120) default NULL,
  `SmartCardPaid` mediumint(8) unsigned NOT NULL default '0',
  `PaymentTypeId` tinyint(3) unsigned NOT NULL default '0',
  `CreationDate` datetime NOT NULL ,
  `RateId` bigint(20) unsigned NOT NULL default '0',
  `AuthenticationCard` varchar(37) default '',
  `RegionId` mediumint(8) unsigned NOT NULL default '0',
  `AddTimeNum` int(10) unsigned NOT NULL default '0',
  `OriginalAmount` mediumint(8) unsigned NOT NULL default '0',
  `IsOffline` tinyint(1) unsigned NOT NULL default '0',
  `PlateNumber` varchar(10) default '',
  PRIMARY KEY  (`PaystationId`,`PurchasedDate`,`TicketNumber`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------------------------------------
-- Load table `ems_6_Transaction` with a subset of rows from the EMS 6 Transaction table. 
-- Uses table `ForeignKeyXref` which maps hand selected entities (Customer, Region/Location,
-- Paystation) from EMS 6 to EMS 7.
-- ---------------------------------------------------------------------------------------------------
   
INSERT  ems_db_pw.ems_6_Transaction
SELECT  *
FROM    ems_db.Transaction T
WHERE   T.PaystationId IN (SELECT PaystationIdEMS6 FROM ems_db_pw.ForeignKeyXref)
AND     T.PurchasedDate >= '2011-01-01' AND T.PurchasedDate < '2012-05-01';

-- ---------------------------------------------------------------------------------------------------
-- Do a foreign key swap: tranform EMS 6 foreign key Id's into EMS 7 foreign key Id's
-- ---------------------------------------------------------------------------------------------------
   
UPDATE  ems_6_Transaction T, ForeignKeyXref X
SET     T.CustomerId   = X.CustomerId,
        T.RegionId     = X.LocationId,
        T.PaystationId = X.PointOfSaleId
WHERE   T.CustomerId   = X.CustomerIdEMS6
AND     T.RegionId     = X.RegionIdEMS6
AND     T.PaystationId = X.PaystationIdEMS6;

-- For LotNumber (EMS 6) which is now PaystationSetting (EMS 7)
UPDATE  ems_6_Transaction T, LotSettingXref X
SET     T.LotNumber = X.Name
WHERE   T.CustomerId = X.CustomerId
AND     T.LotNumber = X.NameEMS6;

-- ---------------------------------------------------------------------------------------------------
-- Validate the new foreign keys. There should be zero rows with a dangling foreign key. 
-- ---------------------------------------------------------------------------------------------------
  
SELECT COUNT(*) FROM ems_6_Transaction WHERE CustomerId     NOT IN (SELECT DISTINCT CustomerId      FROM ForeignKeyXref);

SELECT COUNT(*) FROM ems_6_Transaction WHERE RegionId       NOT IN (SELECT DISTINCT LocationId      FROM ForeignKeyXref);

SELECT COUNT(*) FROM ems_6_Transaction WHERE PaystationId   NOT IN (SELECT DISTINCT PointOfSaleId   FROM ForeignKeyXref);

-- These two COUNT(*) must return the same value:
-- 1 of 2
SELECT  COUNT(*) FROM ems_6_Transaction;
-- 2 of 2
SELECT  COUNT(*)
FROM    ems_6_Transaction T, LotSettingXref X
WHERE   T.CustomerId = X.CustomerId
AND     T.LotNumber = X.Name;

-- ---------------------------------------------------------------------------------------------------
--  Table `ems_6_Rates` contains a subset of rows from the EMS 6 `Rates` table
-- ---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS `ems_6_Rates`;
CREATE TABLE `ems_6_Rates` (
  `PaystationId` mediumint(8) unsigned NOT NULL default '0',
  `TicketNumber` int(10) unsigned NOT NULL default '0',
  `PurchasedDate` datetime NOT NULL ,
  `CustomerId` mediumint(8) unsigned NOT NULL default '0',
  `RegionId` mediumint(8) unsigned NOT NULL default '0',
  `LotNumber` varchar(20) NOT NULL default '',
  `TypeId` tinyint(3) unsigned NOT NULL default '0',
  `RateId` bigint(20) unsigned NOT NULL default '0',
  `RateName` varchar(20) NOT NULL,
  `RateValue` mediumint(8) unsigned NOT NULL,
  `Revenue` mediumint(8) NOT NULL,
  PRIMARY KEY  (`PaystationId`,`PurchasedDate`,`TicketNumber`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------------------------------------
-- Load table `ems_6_Rates` with a subset of rows from the EMS 6 `Rates` table. 
-- Uses table `ForeignKeyXref` which maps hand selected entities (Customer, Region/Location
-- Paystation) from EMS 6 to EMS 7.
-- ---------------------------------------------------------------------------------------------------

INSERT  ems_db_pw.ems_6_Rates
SELECT  *
FROM    ems_db.Rates T
WHERE   T.PaystationId IN (SELECT PaystationIdEMS6 FROM ems_db_pw.ForeignKeyXref)
AND     T.PurchasedDate >= '2011-01-01' AND T.PurchasedDate < '2012-05-01';

-- ---------------------------------------------------------------------------------------------------
-- Do a foreign key swap: tranform EMS 6 foreign key Id's into EMS 7 foreign key Id's
-- ---------------------------------------------------------------------------------------------------
   
UPDATE  ems_6_Rates R, ForeignKeyXref X
SET     R.CustomerId   = X.CustomerId,
        R.RegionId     = X.LocationId,
        R.PaystationId = X.PointOfSaleId
WHERE   R.CustomerId   = X.CustomerIdEMS6
AND     R.RegionId     = X.RegionIdEMS6
AND     R.PaystationId = X.PaystationIdEMS6;

-- For LotNumber (EMS 6) which is now LotSetting (EMS 7)
UPDATE  ems_6_Rates T, LotSettingXref X
SET     T.LotNumber = X.Name
WHERE   T.CustomerId = X.CustomerId
AND     T.LotNumber = X.NameEMS6;

-- ---------------------------------------------------------------------------------------------------
-- Validate the new foreign keys. There should be zero rows with a dangling foreign key. 
-- ---------------------------------------------------------------------------------------------------
  
SELECT COUNT(*) FROM ems_6_Rates WHERE CustomerId     NOT IN (SELECT DISTINCT CustomerId    FROM ForeignKeyXref);

SELECT COUNT(*) FROM ems_6_Rates WHERE RegionId       NOT IN (SELECT DISTINCT LocationId    FROM ForeignKeyXref);

SELECT COUNT(*) FROM ems_6_Rates WHERE PaystationId   NOT IN (SELECT DISTINCT PointOfSaleId FROM ForeignKeyXref);

-- These two COUNT(*) must return the same value:
-- 1 of 2
SELECT  COUNT(*) FROM ems_6_Rates;
-- 2 of 2
SELECT  COUNT(*)
FROM    ems_6_Rates R, LotSettingXref X
WHERE   R.CustomerId = X.CustomerId
AND     R.LotNumber = X.Name;

-- ---------------------------------------------------------------------------------------------------
-- There are two EMS 7 test data customers with widget data: 
-- 1. Id = 3 = `Park San Francisco`
-- 2. Id = 5 = `Park Dallas`
-- Both of these test customers have test data `UnifiedRate` records.
-- The `UnifiedRate` records for `Park San Francisco` are very simplistic: 
--    o  no mapping to actual EMS 6 production `Rates` has been performed
--    o  sp_MigrateTransactionIntoWidgetData performs simplistic rates logic for `Park San Francisco`
-- The `UnifiedRate` records for `Park Dallas` have been mapped to EMS 6 production `Rates` data
--    o  the mapping is performed by table `RatesXref` which is populated in script `ems_db_7_901_test_customers_users_roles.sql`
--    o  using the mapping table `RatesXref` tthe the SQL below to reassign rate names in `ems_6_Rates`
-- ---------------------------------------------------------------------------------------------------

UPDATE  ems_6_Rates R, UnifiedRate U, RatesXref X
SET     R.RateName = U.Name
WHERE   R.CustomerId = U.CustomerId
AND     U.CustomerId = X.CustomerId
AND     U.Id = X.UnifiedRateId
AND     R.RateName = X.RateName
AND     R.CustomerId = 5;  -- CustomerId = 5 = `Park Dallas`

-- View data of interest
SELECT COUNT(*) FROM ems_6_Rates WHERE CustomerId = 5;
SELECT * FROM UnifiedRate WHERE CustomerId = 5;
SELECT * FROM RatesXRef;
SELECT * FROM ems_6_Rates WHERE CustomerId = 5 AND RateName NOT IN (SELECT Name FROM UnifiedRate WHERE CustomerId = 5);

-- ---------------------------------------------------------------------------------------------------
-- `POSServiceState` 
-- ---------------------------------------------------------------------------------------------------
 
DELETE FROM POSServiceState;

INSERT POSServiceState (PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastPaystationSettingUploadGMT, PaystationSettingName, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalAmountSinceLastAudit, UpgradeGMT , WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewPaystationSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT) 
SELECT                  PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastLotSettingUpload          , LotNumber            , PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalSinceLastAudit      , UpgradeDate, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewLotSetting       , IsNewPublicKey, IsNewTicketFooter, version, UTC_TIMESTAMP()
FROM   ForeignKeyXref X, ems_db.ServiceState S
WHERE  X.PaystationIdEMS6 = S.PaystationId
GROUP BY X.PaystationIdEMS6
ORDER BY X.PointOfSaleId;

SELECT * FROM POSServiceState;

-- ---------------------------------------------------------------------------------------------------
-- `ActivePermit` 
--
--  WARNINGS    1.  Providing a default value for `OriginalPermitId` is not possible because there are
--                  no `Permit` records in the database. The default value provided is a hack and is 
--                  not unique (which it should be)
--              2.  Because of the hack identified in point 1. above do not run the INSERT-SELECT
--                  statement on its own because the foreign key constraint into the `Permit` table
--                  for attribute `OriginalPermitId` will fail. Run the INSERT-SELECT statement
--                  from within the first and last SET statement blocks.
--              
-- ---------------------------------------------------------------------------------------------------

-- WARNING:     The value assigned to `ActivePermit.OriginalPermitId` is a hack.
--              When creating this test data there were no `Permit` records in the database so an 
--              `OriginalPermitId` was faked!

--  Start here ---------- Run all this SQL as a block (atomic transaction) ----------
-- 
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
  
INSERT  ActivePermit (  CustomerId,   OriginalPermitId,   PermitLocationId,   PointOfSaleId, PermitBeginGMT                             , PermitOriginalExpireGMT                , PermitExpireGMT                        ,   AddTimeNumber,   SpaceNumber, PaystationSettingName, LicencePlateNumber, NumberOfExtensions)
SELECT                T.CustomerId, T.TicketNumber    , T.RegionId        , T.PaystationId , DATE_ADD(T.PurchasedDate ,INTERVAL 46 WEEK), DATE_ADD(T.ExpiryDate,INTERVAL 46 WEEK), DATE_ADD(T.ExpiryDate,INTERVAL 46 WEEK), T.AddTimeNum   , T.StallNumber, T.LotNumber          , ''                , 0
FROM    ems_6_Transaction T, PaystationSetting L
WHERE   T.CustomerId = L.CustomerId
AND     T.LotNumber = L.Name
AND     T.TypeId IN (1,2,12,16,17)  -- Limit query to transaction type's that are actually permit types (hint: compare the rows in `TransactionType` and `PermitType`)
AND     DATE_ADD(T.PurchasedDate ,INTERVAL 46 WEEK) >= '2012-10-01';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- 
--  End here ---------- Run all this SQL as a block (atomic transaction) ----------






-- ---------------------------------------------------------------------------------------------------
-- Step 1   `POSAlert`: these are active pay station alerts
-- ---------------------------------------------------------------------------------------------------
  
INSERT  POSAlert (CustomerAlertTypeId, PointOfSaleId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, AlertGMT, AlertInfo, IsActive, IsSentEmail, SentEmailGMT, ClearedGMT, ClearedByUserId, CreatedGMT)
SELECT  A.Id AS CustomerAlertTypeId,
        P.Id AS PointOfSaleId, 
        D.EventDeviceTypeId, 
        D.EventStatusTypeId, 
        D.EventActionTypeId, 
        D.EventSeverityTypeId, 
        DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CONCAT('2012-12-',REPEAT('0',2-LENGTH(D.Id)),D.Id,' ',CURRENT_TIME()),INTERVAL D.Id SECOND),INTERVAL D.Id HOUR),INTERVAL P.Id HOUR),INTERVAL P.Id MINUTE),INTERVAL P.Id SECOND) AS AlertGMT,
        NULL AS AlertInfo, 
        1 AS IsActive,
        0 AS IsSentEmail, 
        NULL AS SentEmailGMT, 
        NULL AS ClearedGMT, 
        NULL AS ClearedByUserId, 
        DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CONCAT('2012-12-',REPEAT('0',2-LENGTH(D.Id)),D.Id,' ',CURRENT_TIME()),INTERVAL D.Id SECOND),INTERVAL D.Id HOUR),INTERVAL P.Id HOUR),INTERVAL P.Id MINUTE),INTERVAL P.Id SECOND) AS CreatedGMT
FROM    CustomerAlertType A, Customer C, PointOfSale P, EventDefinition D
WHERE   A.CustomerId = C.Id
AND     C.Id = P.CustomerId
AND     A.AlertTypeId = 12
AND     A.IsActive = 1
AND     A.IsDeleted = 0
AND     D.Id IN (2,7,13,21,28)
ORDER BY A.Id, P.Id, D.Id;

-- ---------------------------------------------------------------------------------------------------
-- Step 2   `ActivePOSAlert`: these are pay station alerts from Step 1
-- ---------------------------------------------------------------------------------------------------
  
INSERT  ActivePOSAlert (CustomerAlertTypeId, PointOfSaleId, POSAlertId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT) 
SELECT                  CustomerAlertTypeId, PointOfSaleId, Id        , EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT
FROM    POSAlert 
WHERE   CustomerAlertTypeId IN (SELECT Id FROM CustomerAlertType WHERE AlertTypeId = 12)   -- AlertTypeId = 12 = 'Pay Station Alert'
ORDER BY AlertGMT;

-- ---------------------------------------------------------------------------------------------------
-- Step 3   `POSAlert`: these are active user defined alerts
-- ---------------------------------------------------------------------------------------------------
  
INSERT  POSAlert (CustomerAlertTypeId, PointOfSaleId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, AlertGMT, AlertInfo, IsActive, IsSentEmail, SentEmailGMT, ClearedGMT, ClearedByUserId, CreatedGMT)
SELECT  A.Id AS CustomerAlertTypeId,
        P.Id AS PointOfSaleId, 
        20 AS EventDeviceTypeId, 
        23 AS EventStatusTypeId, 
        1  AS EventActionTypeId, 
        2  AS EventSeverityTypeId, 
        DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CONCAT('2012-12-',REPEAT('0',2-LENGTH(P.Id)),P.Id,' ',CURRENT_TIME()),INTERVAL P.Id SECOND),INTERVAL P.Id HOUR),INTERVAL P.Id HOUR),INTERVAL P.Id MINUTE),INTERVAL P.Id SECOND) AS AlertGMT,
        NULL AS AlertInfo, 
        1 AS IsActive,
        0 AS IsSentEmail, 
        NULL AS SentEmailGMT, 
        NULL AS ClearedGMT, 
        NULL AS ClearedByUserId, 
        DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CONCAT('2012-12-',REPEAT('0',2-LENGTH(P.Id)),P.Id,' ',CURRENT_TIME()),INTERVAL P.Id SECOND),INTERVAL P.Id HOUR),INTERVAL P.Id HOUR),INTERVAL P.Id MINUTE),INTERVAL P.Id SECOND) AS CreatedGMT
FROM    CustomerAlertType A, Customer C, PointOfSale P
WHERE   A.CustomerId = C.Id
AND     C.Id = P.CustomerId
AND     A.AlertTypeId = 2
AND     A.IsActive = 1
AND     A.IsDeleted = 0
AND     A.Id IN (5,13)
ORDER BY A.Id, P.Id;

-- ---------------------------------------------------------------------------------------------------
-- Step 4   `ActivePOSAlert`: these are active user defined alerts from Step 3
-- ---------------------------------------------------------------------------------------------------
  
INSERT  ActivePOSAlert (CustomerAlertTypeId, PointOfSaleId, POSAlertId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT) 
SELECT                  CustomerAlertTypeId, PointOfSaleId, Id        , EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT
FROM    POSAlert 
WHERE   CustomerAlertTypeId IN (5,13)
ORDER BY AlertGMT;

-- ---------------------------------------------------------------------------------------------------
-- Step 5   `ActivePOSAlert`: set ActivePOSAlert.IsAlerted = 1 for user defined alerts from Step 3
-- ---------------------------------------------------------------------------------------------------
 
UPDATE  POSAlert P, ActivePOSAlert A
SET     A.IsActive            = P.IsActive,
        A.POSAlertId          = P.Id,
        A.AlertGMT            = P.AlertGMT, 
        A.AlertInfo           = P.AlertInfo, 
        A.CreatedGMT          = P.CreatedGMT
WHERE   P.CustomerAlertTypeId = A.CustomerAlertTypeId
AND     P.PointOfSaleId       = A.PointOfSaleId
AND     P.EventDeviceTypeId   = A.EventDeviceTypeId
AND     P.EventStatusTypeId   = A.EventStatusTypeId
AND     P.EventActionTypeId   = A.EventActionTypeId
AND     P.EventSeverityTypeId = A.EventSeverityTypeId
AND     A.CustomerAlertTypeId IN (5,13)
AND     A.EventDeviceTypeId   = 20 
AND     A.EventStatusTypeId   = 23 
AND     A.EventActionTypeId   = 1  
AND     A.EventSeverityTypeId = 2
AND     A.IsActive            = 0;

-- ---------------------------------------------------------------------------------------------------
-- Step 6   Additional `POSAlert` and `ActivePOSAlert` test data to test `Low Power Shut Down` and `Shock Alarm` pay station alerts
-- ---------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------
-- Low Power Shut Down for `POSAlert`: (12,5,1,3)
INSERT  POSAlert (CustomerAlertTypeId, PointOfSaleId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, AlertGMT             , AlertInfo, IsActive, IsSentEmail, SentEmailGMT, ClearedGMT, ClearedByUserId, CreatedGMT)
VALUES           (1                  , 4            , 12               , 5                , 1                , 3                  , '2013-01-11 11:11:11', NULL     , 1       , 0          , NULL        , NULL      , NULL           , '2013-01-11 11:11:11');

-- Low Power Shut Down for `ActivePOSAlert`: (12,5,1,3)
INSERT  ActivePOSAlert (CustomerAlertTypeId, PointOfSaleId, POSAlertId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT) 
SELECT                  CustomerAlertTypeId, PointOfSaleId, Id        , EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT
FROM    POSAlert 
WHERE   CustomerAlertTypeId = 1
AND     PointOfSaleId       = 4
AND     EventDeviceTypeId   = 12
AND     EventStatusTypeId   = 5
AND     EventActionTypeId   = 1
AND     EventSeverityTypeId = 3
AND     AlertGMT            = '2013-01-11 11:11:11'
AND     IsActive            = 1;

-- ---------------------------------------------------------------------------------------------------
-- Shock Alarm for `POSAlert`: (14,16,1,3)
INSERT  POSAlert (CustomerAlertTypeId, PointOfSaleId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, AlertGMT             , AlertInfo, IsActive, IsSentEmail, SentEmailGMT, ClearedGMT, ClearedByUserId, CreatedGMT)
VALUES           (3                  , 15           , 14               , 16               , 1                , 3                  , '2013-01-12 12:12:12', NULL     , 1       , 0          , NULL        , NULL      , NULL           , '2013-01-12 12:12:12');

-- Shock Alarm for `ActivePOSAlert`: (14,16,1,3)
INSERT  ActivePOSAlert (CustomerAlertTypeId, PointOfSaleId, POSAlertId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT) 
SELECT                  CustomerAlertTypeId, PointOfSaleId, Id        , EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT
FROM    POSAlert 
WHERE   CustomerAlertTypeId = 3
AND     PointOfSaleId       = 15
AND     EventDeviceTypeId   = 14
AND     EventStatusTypeId   = 16
AND     EventActionTypeId   = 1
AND     EventSeverityTypeId = 3
AND     AlertGMT            = '2013-01-12 12:12:12'
AND     IsActive            = 1;

-- --------------------------------------------------------------
-- SELECT for `Low Power Shut Down`
-- --------------------------------------------------------------
SELECT  A.*
FROM    ActivePOSAlert A
WHERE   A.EventDeviceTypeId   = 12   -- `Low Power Shut Down`: Device=12, Status=5, Action=1, Severity=3
AND     A.EventStatusTypeId   = 5
AND     A.EventActionTypeId   = 1
-- AND  A.EventSeverityTypeId = 3    -- `Severity` is not needed in the query: Device + Status + Action is sufficient 
AND     A.IsActive            = 1
AND     A.AlertGMT            < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 24 HOUR);

-- --------------------------------------------------------------
-- SELECT for `Shock Alarm`
-- --------------------------------------------------------------
SELECT  A.*
FROM    ActivePOSAlert A
WHERE   A.EventDeviceTypeId   = 14   -- `Shock Alarm`: Device=14, Status=16, Action=1, Severity=3
AND     A.EventStatusTypeId   = 16
AND     A.EventActionTypeId   = 1
-- AND  A.EventSeverityTypeId = 3    -- `Severity` is not needed in the query: Device + Status + Action is sufficient
AND     A.IsActive            = 1
AND     A.AlertGMT            < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 24 HOUR);

-- --------------------------------------------------------------
-- INSERT `POSCOllection`
--
-- This is test data only. Not intended for production use. 
-- --------------------------------------------------------------
-- Added two attributes EndTimeIdGMT and EndTimeIdLocal on April 01 2013
INSERT  POSCollection 
        (
        CustomerId,
        PointOfSaleId,
        CollectionTypeId,
        PaystationSettingId,
		EndTimeIdGMT,
		EndTimeIdLocal,
        ReportNumber,
        StartGMT,
        EndGMT,
        StartTicketNumber,
        EndTicketNumber,
        NextTicketNumber,
        StartTransactionNumber,
        EndTransactionNumber,
        TicketsSold,
        CoinTotalAmount,
        BillTotalAmount,
        CardTotalAmount,
        CoinCount05,
        CoinCount10,
        CoinCount25,
        CoinCount100,
        CoinCount200,
        BillCount1,
        BillCount2,
        BillCount5,
        BillCount10,
        BillCount20,
        BillCount50,
        BillAmount1,
        BillAmount2,
        BillAmount5,
        BillAmount10,
        BillAmount20,
        BillAmount50,
        AmexAmount,
        DinersAmount,
        DiscoverAmount,
        MasterCardAmount,
        VisaAmount,
        SmartCardAmount,
        ValueCardAmount,
        SmartCardRechargeAmount,
        AcceptedFloatAmount,
        AttendantDepositAmount,
        AttendantTicketsAmount,
        AttendantTicketsSold,
        ChangeIssuedAmount,
        ExcessPaymentAmount,
        OverfillAmount,
        PatrollerTicketsSold,
        RefundIssuedAmount,
        ReplenishedAmount,
        TestDispensedChanger,
        TestDispensedHopper1,
        TestDispensedHopper2,
        Hopper1Type,
        Hopper2Type,
        Hopper1Current,
        Hopper2Current,
        Hopper1Dispensed,
        Hopper2Dispensed,
        Hopper1Replenished,
        Hopper2Replenished,
        Tube1Type,
        Tube2Type,
        Tube3Type,
        Tube4Type,
        Tube1Amount,
        Tube2Amount,
        Tube3Amount,
        Tube4Amount
        )
             
SELECT  X.CustomerId,
        X.PointOfSaleId,
        0 AS CollectionTypeId,              -- All (Audit report)
        S.Id AS PaystationSettingId,        -- Table `PaystationSetting` contains dummy data 
		1 AS EndTimeIdGMT, -- contains dummy data
		2 AS EndTimeIdLocal, -- contains dummy data NEW COLUMNS EndTimeIdGMT and EndTimeIdLocal
        A.ReportNumber,
        DATE_ADD(A.StartDate,INTERVAL 46 WEEK) AS StartGMT,      
        DATE_ADD(A.EndDate,INTERVAL 46 WEEK) AS EndGMT,
        0 AS StartTicketNumber,
        0 AS EndTicketNumber,
        A.NextTicketNumber AS NextTicketNumber,
        A.StartTransactionNumber AS StartTransactionNumber,
        A.EndTransactionNumber AS EndTransactionNumber,
        A.TicketsSold AS TicketsSold,
        CAST(A.CoinAmount * 100 AS UNSIGNED) AS CoinTotalAmount,   
        CAST((A.BillCount01 * 100) + (A.BillCount02 * 200) + (A.BillCount05 * 500) + (A.BillCount10 * 1000) + (A.BillCount20 * 2000) + (A.BillCount50 * 5000) AS UNSIGNED) AS BillTotalAmount,
        CAST((A.OtherCardAmount * 100) + (A.AmexAmount * 100) + (A.DinersAmount * 100) + (A.DiscoverAmount * 100) + (A.MasterCardAmount * 100) + (A.VisaAmount * 100) AS UNSIGNED) AS CardTotalAmount,
        A.CoinCount005 AS CoinCount05,
        A.CoinCount010 AS CoinCount10,
        A.CoinCount025 AS CoinCount25,
        A.CoinCount100 AS CoinCount100,
        A.CoinCount200 AS CoinCount200,
        A.BillCount01 AS BillCount1,
        A.BillCount02 AS BillCount2,
        A.BillCount05 AS BillCount5,
        A.BillCount10 AS BillCount10,
        A.BillCount20 AS BillCount20,
        A.BillCount50 AS BillCount50,
        CAST(A.BillCount01 * 100 AS UNSIGNED) AS BillAmount1,
        CAST(A.BillCount02 * 200 AS UNSIGNED) AS BillAmount2,
        CAST(A.BillCount05 * 500 AS UNSIGNED) AS BillAmount5,
        CAST(A.BillCount10 * 1000 AS UNSIGNED) AS BillAmount10,
        CAST(A.BillCount20 * 2000 AS UNSIGNED) AS BillAmount20,
        CAST(A.BillCount50 * 5000 AS UNSIGNED) AS BillAmount50,
        CAST(A.AmexAmount * 100 AS UNSIGNED) AS AmexAmount,              
        CAST(A.DinersAmount * 100 AS UNSIGNED) AS DinersAmount,
        CAST(A.DiscoverAmount * 100 AS UNSIGNED) AS DiscoverAmount,
        CAST(A.MasterCardAmount * 100 AS UNSIGNED) AS MasterCardAmount,
        CAST(A.VisaAmount * 100 AS UNSIGNED) AS VisaAmount,
        CAST(A.SmartCardAmount * 100 AS UNSIGNED) AS SmartCardAmount,
        CAST(0 * 100 AS UNSIGNED) AS ValueCardAmount ,        -- No value for ValueCardAmount?       
        CAST(A.SmartCardRechargeAmount * 100 AS UNSIGNED) AS SmartCardRechargeAmount,
        CAST(A.AcceptedFloatAmount AS UNSIGNED) AS AcceptedFloatAmount,
        CAST(A.AttendantDepositAmount * 100 AS UNSIGNED) AS AttendantDepositAmount,
        CAST(A.AttendantTicketsAmount * 100 AS UNSIGNED) AS AttendantTicketsAmount,
        A.AttendantTicketsSold AS AttendantTicketsSold,
        CAST(A.ChangeIssued AS UNSIGNED) AS ChangeIssuedAmount,    
        CAST(A.ExcessPayment AS UNSIGNED) AS ExcessPaymentAmount,  
        A.OverfillAmount AS OverfillAmount,                   -- Already multiplied by 100? 
        A.PatrollerTicketsSold AS PatrollerTicketsSold,
        CAST(A.RefundIssued * 100 AS UNSIGNED) AS RefundIssuedAmount,
        A.ReplenishedAmount AS ReplenishedAmount,             -- Already multiplied by 100? 
        A.ChangerTestDispensed AS TestDispensedChanger,
        A.Hopper1TestDispensed AS TestDispensedHopper1,
        A.Hopper2TestDispensed AS TestDispensedHopper2,
        A.Hopper1Type AS Hopper1Type,
        A.Hopper2Type AS Hopper2Type,
        A.Hopper1Current AS Hopper1Current,
        A.Hopper2Current AS Hopper2Current,
        A.Hopper1Dispensed AS Hopper1Dispensed,
        A.Hopper2Dispensed AS Hopper2Dispensed,
        A.Hopper1Replenished AS Hopper1Replenished,
        A.Hopper2Replenished AS Hopper2Replenished,
        A.Tube1Type AS Tube1Type,
        A.Tube2Type AS Tube2Type,
        A.Tube3Type AS Tube3Type,
        A.Tube4Type AS Tube4Type,
        A.Tube1Amount AS Tube1Amount,
        A.Tube2Amount AS Tube2Amount,
        A.Tube3Amount AS Tube3Amount,
        A.Tube4Amount AS Tube4Amount

FROM    ems_db.Audit A, ForeignKeyXref X, LotSettingXref L, PaystationSetting S
WHERE   A.PaystationId = X.PaystationIdEMS6
AND     A.EndDate >= '2011-01-01'
AND     A.CustomerId = X.CustomerIdEMS6
AND     X.CustomerId = L.CustomerId
AND     A.LotNumber = L.NameEMS6
AND     L.CustomerId = S.CustomerId
AND     L.Name = S.Name;

