-- ----------------------------------------------------------------------------------------------
-- Specify database
-- ----------------------------------------------------------------------------------------------

USE ems_db_pw;

-- ----------------------------------------------------------------------------------------------
-- View procedures in database
-- ----------------------------------------------------------------------------------------------

SHOW PROCEDURE STATUS WHERE DB = 'ems_db_pw';

-- ----------------------------------------------------------------------------------------------
-- View triggers in database
-- ----------------------------------------------------------------------------------------------

SHOW TRIGGERS FROM ems_db_pw;

-- ----------------------------------------------------------------------------------------------
-- Reset table POSBalance
-- ----------------------------------------------------------------------------------------------

UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';
        
-- ----------------------------------------------------------------------------------------------
-- View POSBalance
-- ----------------------------------------------------------------------------------------------

SELECT * FROM POSBalance;
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
SELECT * FROM POSBalance WHERE PointOfSaleId BETWEEN 1 AND 4;

-- ----------------------------------------------------------------------------------------------
-- Test trigger:    tr_POSCollection_insert
-- 
-- Purpose:         Verify in table POSBalance that attributes LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, 
--                  LastCollectionTypeId have changed and IsRecalcable = 2
-- ----------------------------------------------------------------------------------------------

UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';

TRUNCATE TABLE POSCollection;

INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 0, 7, '2013-01-02 00:00:00', '2013-01-03 00:00:10');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 1, 7, '2013-01-02 00:00:10', '2013-01-03 00:00:20');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 2, 7, '2013-01-02 00:00:20', '2013-01-03 00:00:30');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 3, 7, '2013-01-02 00:00:30', '2013-01-03 00:00:40');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 0, 7, '2013-01-02 00:00:25', '2013-01-03 00:00:26');     -- This is an out of sequence Audit Report
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- ----------------------------------------------------------------------------------------------
-- Test trigger:    tr_PurchaseCollection_insert
-- 
-- Purpose:         Verify in table POSBalance that attribute IsRecalcable = 1
-- ----------------------------------------------------------------------------------------------

UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';

TRUNCATE TABLE PurchaseCollection;

-- BLOCK BEGIN: Must run within a block because there are no Purchase records in the database
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 01:01:01', 500, 0, 0, 0, 0);
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 02:02:02', 0, 325, 3, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 03:03:03', 0, 0, 0, 1000, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 04:04:04', 0, 400, 6, 2000, 1);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- BLOCK END

SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 1;

-- ----------------------------------------------------------------------------------------------
-- Test procedure:  sp_CollectionFlagUnsettledPreAuth
-- 
-- Purpose:         Verify in table POSBalance that attribute IsRecalcable = 4
-- ----------------------------------------------------------------------------------------------

UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';

TRUNCATE TABLE PreAuth;

INSERT PreAuth (PointOfSaleId, MerchantAccountId, PreAuthDate          , Amount, Last4DigitsOfCardNumber, CardType, CardHash, CardExpiry, IsRFID, IsApproved)
VALUES         (1            , 1                , '2013-01-05 05:05:05', 2500  , 1234                   , 'VISA'  , 'hash'  , 1014      , 1     , 1         );

SELECT * FROM PreAuth WHERE PointOfSaleId = 1;

CALL sp_CollectionFlagUnsettledPreAuth (300);

SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- ----------------------------------------------------------------------------------------------
-- Test procedure:  sp_CollectionSetLock
-- 
-- Purpose:         Verify in table POSBalance that attributes that ClusterId and CollectionLockId are now non-NULL.
--                  Also look at the newly created CollectionLock record.
-- ----------------------------------------------------------------------------------------------

UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';
        
DELETE FROM CollectionLock;
TRUNCATE TABLE POSCollection;
DELETE FROM PreAuth;
TRUNCATE TABLE PurchaseCollection;

-- BLOCK BEGIN: Must run within a block because there are no Permit records in the database
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 01:01:01', 500, 0, 0, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-01 02:02:02', 0, 325, 3, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 3, '2013-01-01 03:03:03', 0, 0, 0, 1000, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 4, '2013-01-01 04:04:04', 0, 400, 6, 2000, 1);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- BLOCK END

SELECT * FROM POSBalance         WHERE PointOfSaleId BETWEEN 1 AND 4;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId BETWEEN 1 AND 4;

-- sp_CollectionSetLock (IN Cluster_Id TINYINT UNSIGNED, IN Lock_PS SMALLINT UNSIGNED)
-- Returns via SELECT the new CollectionLock.Id
CALL sp_CollectionSetLock (1, 2);   -- using Cluster.Id = 1 lock up to 2 point-of-sales (there are 4 point-of-sales that can be locked but only 2 will be locked (because the lock batch size is 2)
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance         WHERE PointOfSaleId BETWEEN 1 AND 4;

CALL sp_CollectionSetLock (2, 2);   -- using Cluster.Id = 2 lock up to 2 point-of-sales (the remaining two unlocked point-of-sales will be locked)
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance         WHERE PointOfSaleId BETWEEN 1 AND 4;

-- ----------------------------------------------------------------------------------------------
-- Test procedure:  sp_CollectionReleaseLock
-- 
-- Purpose:         Verify in table POSBalance that attributes ClusterId and CollectionLockId are now NULL
--                  and IsRecalcable = 3
--
-- Important:       Do not DELETE or TRUNCATE the CollectionLock records created by the previous test.
--                  These CollectionLock records will be used in this test. 
--
--                  Do not reset (UPDATE) table POSBalance. Procedure sp_CollectionReleaseLock
--                  will UPDATE (reset) these records itself.
--
--                  Also: update the LockGMT for these CollectionLock records because procedure
--                  sp_CollectionReleaseLock only releases stale (old) CollectionLock records
-- ----------------------------------------------------------------------------------------------
        
SELECT  * FROM CollectionLock;
UPDATE  CollectionLock SET LockGMT = DATE_SUB(LockGMT,INTERVAL 61 MINUTE);
SELECT  * FROM CollectionLock;
SELECT  * FROM POSBalance        WHERE PointOfSaleId BETWEEN 1 AND 4;

-- sp_CollectionReleaseLock (IN Cluster_Id TINYINT UNSIGNED)
CALL sp_CollectionReleaseLock (1);
SELECT * FROM POSBalance         WHERE PointOfSaleId BETWEEN 1 AND 4;

CALL sp_CollectionReleaseLock (2);
SELECT * FROM POSBalance         WHERE PointOfSaleId BETWEEN 1 AND 4;
        
-- ----------------------------------------------------------------------------------------------
-- Test procedure:  sp_CollectionDeleteLock
-- 
-- Purpose:         Verify (aged) records are deleted from table CollectionLock
--
-- Important:       Do not DELETE or TRUNCATE the CollectionLock records created by the previous test.
--                  These CollectionLock records will be used in this test. 
--
--                  Do not reset (UPDATE) table POSBalance. 
--
--                  Also: update the LockGMT for these CollectionLock records because procedure
--                  sp_CollectionDeleteLock only deletes stale (old) CollectionLock records
-- ----------------------------------------------------------------------------------------------        

SELECT  * FROM CollectionLock;
UPDATE  CollectionLock SET LockGMT = DATE_SUB(LockGMT,INTERVAL 8 DAY);
SELECT  * FROM CollectionLock;
SELECT  * FROM POSBalance        WHERE PointOfSaleId BETWEEN 1 AND 4;
      
-- sp_CollectionDeleteLock() takes no parameters
CALL sp_CollectionDeleteLock();
SELECT  * FROM CollectionLock;

-- ----------------------------------------------------------------------------------------------
-- Overview
-- ----------------------------------------------------------------------------------------------
-- Test procedure:  sp_CollectionRecalcBalance
-- 
-- Purpose:         Performs separate collections balance recalcs. 
--
-- Parameters       1.  IN CL_id INT UNSIGNED:
--                      The CollectionLock.Id that has locked point-of-sales ready for recalc
--                      
--                  2.  IN Num_Sec SMALLINT UNSIGNED:
--                      Once a recalc for a point-of-sale is complete the next recalc for the point-of-sale is scheduled Num_Sec into the future
--                      A typical value for Num_Sec is 300 seconds (5 minutes)
--                      Once 300 seconds have passed the point-of-sale becomes a candidate for it's next recalc but that recalc will only occur
--                          if there has been some activity for the point-of-sale (a purchase, a collection report, etc.)
--                      
--                  3.  IN PreAuth_Delay SMALLINT UNSIGNED
--                      Roughly half of all purchases are paid by credit card. When paying by credit card a preauth is required.
--                      Most pre-auths settle within a few seconds. When recalcing separate collections balances we 
--                          want to ignore these most recent preauths because they will most likely settle.
--                      Parameter PreAuth_Delay specifies the number of seconds back from the time of recalc that recent preauths will be ignored.
-- ----------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------
-- Test #1:         sp_CollectionRecalcBalance
--                  1.1     4 `PurchaseCollection` records for 1 `PointOfSale` record (PointOfSale.Id = 1)
--                  1.2     0 `POSCollection` records
--                  1.3     1 `PreAuth` record
--                  1.4     1 `POSServcieState` record with values in attributes `CoinBagCount` and `BillStackerCount`
-- ----------------------------------------------------------------------------------------------

-- Cleanup prior to test
UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';
DELETE FROM CollectionLock;
TRUNCATE TABLE POSCollection;
DELETE FROM PreAuth;
TRUNCATE TABLE PurchaseCollection;

-- Create a `PreAuth` (it's back in time, not near the current date)
INSERT PreAuth (PointOfSaleId, MerchantAccountId, PreAuthDate          , Amount, Last4DigitsOfCardNumber, CardType, CardHash, CardExpiry, IsRFID, IsApproved)
VALUES         (1            , 1                , '2013-01-05 05:05:05', 2500  , 1234                   , 'VISA'  , 'hash'  , 1014      , 1     , 1         );

-- Set `POSServiceState` information (attributes `CoinBagCount` and `BillStackerCount`)
UPDATE  POSServiceState SET CoinBagCount = 123, BillStackerCount = 17 WHERE PointOfSaleId = 1;

-- Create `PurchaseCollection` records
-- BLOCK BEGIN: Must run within a block because there are no Purchase records in the database
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 01:01:01', 500, 0, 0, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 02:02:02', 0, 325, 3, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 03:03:03', 0, 0, 0, 1000, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 1, '2013-01-01 04:04:04', 0, 400, 6, 2000, 1);
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- BLOCK END

-- View test data
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
SELECT * FROM PreAuth WHERE PointOfSaleId = 1;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 1;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by teh call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (1, 2);   -- using Cluster.Id = 1 lock up to 2 point-of-sales (there are 4 point-of-sales that can be locked but only 2 will be locked (because the lock batch size is 2)
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (???, 300, 120);

-- View recalc results 
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
SELECT * FROM POSCollection WHERE PointOfSaleId = 1;
SELECT PointOfSaleId, CoinBagCount, BillStackerCount FROM POSServiceState WHERE PointOfSaleId = 1;
SELECT * FROM PreAuth WHERE PointOfSaleId = 1;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 1;

-- ---------- Expected Results for PointOfSale.Id = 1 ---------- --
-- CashAmount: 500
-- CoinCount: 132
-- CoinAmount: 725
-- BillCount: 20
-- BillAmount: 3000
-- UnsettledCreditCardCount: 1
-- UnsettledCreditCardAmount: 2500
-- TotalAmount: 6725

-- ----------------------------------------------------------------------------------------------
-- Test #2:         sp_CollectionRecalcBalance
--                  Note:   This test works with the existing set of test data created in Test #1
-- ----------------------------------------------------------------------------------------------

-- Receipt of a new Coin Collection will change the recalc values
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 2, 7, '2013-01-01 02:02:02', '2013-01-01 02:02:22');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- The receipt of a new Coin Collection will also impact the POSServiceState table
UPDATE POSServiceState SET CoinBagCount = 0 WHERE PointOfSaleId = 1;
SELECT PointOfSaleId, CoinBagCount, BillStackerCount FROM POSServiceState WHERE PointOfSaleId = 1;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by teh call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (2, 2);   -- using Cluster.Id = 2 lock up to 2 point-of-sales (there are 4 point-of-sales that can be locked but only 2 will be locked (because the lock batch size is 2)
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (???, 300, 120);

-- View recalc results 
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
SELECT * FROM POSCollection WHERE PointOfSaleId = 1;
SELECT PointOfSaleId, CoinBagCount, BillStackerCount FROM POSServiceState WHERE PointOfSaleId = 1;
SELECT * FROM PreAuth WHERE PointOfSaleId = 1;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 1;

-- ---------- Expected Results for PointOfSale.Id = 1 ---------- --
-- CashAmount: 0
-- CoinCount: 6
-- CoinAmount: 400
-- BillCount: 20
-- BillAmount: 3000
-- UnsettledCreditCardCount: 1
-- UnsettledCreditCardAmount: 2500
-- TotalAmount: 5900

-- ----------------------------------------------------------------------------------------------
-- Test #3:         sp_CollectionRecalcBalance
--                  Note:   This test works with the existing set of test data created in Test #2
-- ----------------------------------------------------------------------------------------------

-- Receipt of a new Bill Collection will change the recalc values
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;
INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 1, 1, 7, '2013-01-01 03:03:03', '2013-01-01 03:03:33');
SELECT * FROM POSBalance WHERE PointOfSaleId = 1;

-- The receipt of a new Bill Collection will also impact the POSServiceState table
UPDATE POSServiceState SET BillStackerCount = 0 WHERE PointOfSaleId = 1;
SELECT PointOfSaleId, CoinBagCount, BillStackerCount FROM POSServiceState WHERE PointOfSaleId = 1;

-- Create a `PreAuth` (it's back in time, not near the current date)
INSERT PreAuth (PointOfSaleId, MerchantAccountId, PreAuthDate          , Amount, Last4DigitsOfCardNumber, CardType, CardHash, CardExpiry, IsRFID, IsApproved)
VALUES         (1            , 1                , '2013-01-06 06:06:06', 1850  , 7890                   , 'AMEX'  , 'hash'  , 615       , 1     , 1         );
SELECT * FROM PreAuth WHERE PointOfSaleId = 1;

-- Insert Purchase Collection records for a different point of sale: PointOfSale.Id = 2
-- BLOCK BEGIN: Must run within a block because there are no Purchase records in the database
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-02 01:01:01', 0, 500, 3, 1000, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-02 02:02:02', 0, 200, 5, 0, 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-02 03:03:03', 0, 0, 0, 1500, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-02 04:04:04', 0, 300, 2, 500, 1);
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- BLOCK END

-- Pretend PointOfSale.Id = 2 is configured for separate collections so there is no cash 
UPDATE POSServiceState SET BillStackerCount = 0, CoinBagCount = 0 WHERE PointOfSaleId = 2;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by teh call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (3, 2);   -- using Cluster.Id = 3 lock up to 2 point-of-sales (there are 4 point-of-sales that can be locked but only 2 will be locked (because the lock batch size is 2)
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId IN (1,2);

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (???, 300, 120);

-- View recalc results 
SELECT * FROM CollectionType;
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId IN (1,2);
SELECT * FROM POSCollection WHERE PointOfSaleId IN (1,2);
SELECT PointOfSaleId, CoinBagCount, BillStackerCount FROM POSServiceState WHERE PointOfSaleId IN (1,2);
SELECT * FROM PreAuth WHERE PointOfSaleId IN (1,2);
SELECT * FROM PurchaseCollection WHERE PointOfSaleId IN (1,2);

-- ---------- Expected Results for PointOfSale.Id = 1 ---------- --
-- CashAmount: 0
-- CoinCount: 6
-- CoinAmount: 400
-- BillCount: 1
-- BillAmount: 2000
-- UnsettledCreditCardCount: 2
-- UnsettledCreditCardAmount: 4350
-- TotalAmount: 6750

-- ---------- Expected Results for PointOfSale.Id = 2 ---------- --
-- CashAmount: 0
-- CoinCount: 10
-- CoinAmount: 1000
-- BillCount: 5
-- BillAmount: 3000
-- UnsettledCreditCardCount: 0
-- UnsettledCreditCardAmount: 0
-- TotalAmount: 4000


-- ----------------------------------------------------------------------------------------------
-- Test #4:         There is a recalc bug in the old EMS 6 production code.
--                  When refactoring teh recalc code into EMS 7 I added a new line of code to 
--                  trigger `tr_POSCollection_insert`. Verify that thsi new line of code
--                  solves the problem with bill collection counts being incorrect.
-- ----------------------------------------------------------------------------------------------

-- Cleanup prior to test
UPDATE  POSBalance
SET     ClusterId = NULL,
        CollectionLockId = NULL,
        PrevCollectionLockId = NULL,
        PrevPrevCollectionLockId = NULL,
        CashAmount = 0,
        CoinCount = 0,
        CoinAmount = 0,
        BillCount = 0,
        BillAmount = 0,
        UnsettledCreditCardCount = 0,
        UnsettledCreditCardAmount = 0,
        TotalAmount = 0,
        LastCashCollectionGMT = '2013-01-01',
        LastCoinCollectionGMT = '2013-01-01',
        LastBillCollectionGMT = '2013-01-01',
        LastCardCollectionGMT = '2013-01-01',
        LastCollectionTypeId = 0,
        LastRecalcGMT = '2013-01-01',
        IsRecalcable = 0,
        NextRecalcGMT = '2013-01-01';
DELETE FROM CollectionLock;
TRUNCATE TABLE POSCollection;
DELETE FROM PreAuth;
TRUNCATE TABLE PurchaseCollection;

-- Pretend PointOfSale.Id = 2 is configured for separate collections so there is no cash 
UPDATE POSServiceState SET BillStackerCount = 0, CoinBagCount = 0 WHERE PointOfSaleId = 2;

-- Insert `PurchaseCollection` records for a different point of sale: PointOfSale.Id = 2
-- BLOCK BEGIN: Must run within a block because there are no Purchase records in the database
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-01 01:11:11',   0, 500, 3,  1000, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-02 02:22:22',   0, 200, 5,  0   , 0);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-03 03:33:33',   0, 0  , 0,  1500, 2);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-04 04:44:44',   0, 300, 2,  500 , 1);
INSERT PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, CashAmount, CoinAmount, CoinCount, BillAmount, BillCount) VALUES (0, 2, '2013-01-05 05:55:55', 250, 600, 6, 2000 , 1);
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- BLOCK END

-- Insert an unsettled PreAuth
INSERT PreAuth (PointOfSaleId, MerchantAccountId, PreAuthDate          , Amount, Last4DigitsOfCardNumber, CardType, CardHash, CardExpiry, IsRFID, IsApproved)
VALUES         (2            , 2                , '2013-01-03 03:03:03', 2500  , 1234                   , 'VISA'  , 'hash'  , 1014      , 1     , 1         );

-- Review test data
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 2;
SELECT * FROM POSCollection WHERE PointOfSaleId = 2;
SELECT * FROM PreAuth WHERE PointOfSaleId = 2;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by teh call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (9, 2);   -- using Cluster.Id = 9 lock up to 2 point-of-sales
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (4, 300, 120);
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 2;

-- ---------- Expected Results for PointOfSale.Id = 2 ---------- --
-- CashAmount: 250
-- CoinCount: 16
-- CoinAmount: 1600
-- BillCount: 6
-- BillAmount: 5000
-- UnsettledCreditCardCount: 1
-- UnsettledCreditCardAmount: 2500
-- TotalAmount: 9350

-- Insert a bill collection report and then do a recalc
INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 2, 1, 14, '2013-01-01 03:03:03', '2013-01-02 02:02:02');

-- Validate receipt of Bill Collection report
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM POSCollection WHERE PointOfSaleId = 2;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by the call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (9, 2);   -- using Cluster.Id = 9 lock up to 2 point-of-sales
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (5, 300, 120);
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 2;

-- ---------- Expected Results for PointOfSale.Id = 2 ---------- --
-- CashAmount: 250
-- CoinCount: 16
-- CoinAmount: 1600
-- BillCount: 4
-- BillAmount: 4000
-- UnsettledCreditCardCount: 1
-- UnsettledCreditCardAmount: 2500
-- TotalAmount: 8350

-- Insert a coin collection report and then do a recalc
INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 2, 2, 14, '2013-01-01 03:03:03', '2013-01-03 03:33:34');

-- Validate receipt of Bill Collection report
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM POSCollection WHERE PointOfSaleId = 2;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by the call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (9, 2);   -- using Cluster.Id = 9 lock up to 2 point-of-sales
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (6, 300, 120);
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 2;

-- ---------- Expected Results for PointOfSale.Id = 2 ---------- --
-- CashAmount: 250
-- CoinCount: 8
-- CoinAmount: 900
-- BillCount: 4
-- BillAmount: 4000
-- UnsettledCreditCardCount: 1
-- UnsettledCreditCardAmount: 2500
-- TotalAmount: 7650

-- Insert a card collection report and then do a recalc
INSERT POSCollection (CustomerId, PointOfSaleId, CollectionTypeId, PaystationSettingId, StartGMT, EndGMT) VALUES (3, 2, 3, 14, '2013-01-01 03:03:03', '2013-01-03 03:03:04');

-- Validate receipt of Card Collection report
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM POSCollection WHERE PointOfSaleId = 2;

-- Set a recalc lock
-- IMPORTANT: Save the CollectionLock.Id that is returned by the call to procedure sp_CollectionSetLock, this value is used by the call to procedure sp_CollectionRecalcBalance
CALL sp_CollectionSetLock (9, 2);   -- using Cluster.Id = 9 lock up to 2 point-of-sales
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;

-- Perform a recalc
-- sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
-- IMPORTANT: must replace `???` with the CollectionLock.Id returned from the call to procedure `sp_CollectionSetLock`
CALL sp_CollectionRecalcBalance (7, 300, 120);
SELECT * FROM CollectionLock;
SELECT * FROM POSBalance WHERE PointOfSaleId = 2;
SELECT * FROM PurchaseCollection WHERE PointOfSaleId = 2;
SELECT * FROM POSCollection WHERE PointOfSaleId = 2;

-- ---------- Expected Results for PointOfSale.Id = 2 ---------- --
-- CashAmount: 250
-- CoinCount: 8
-- CoinAmount: 900
-- BillCount: 4
-- BillAmount: 4000
-- UnsettledCreditCardCount: 0
-- UnsettledCreditCardAmount: 0
-- TotalAmount: 5150



