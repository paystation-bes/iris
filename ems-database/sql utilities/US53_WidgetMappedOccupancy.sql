

INSERT INTO LocationLotGeopoint ( LocationId, 
							Latitude, Longitude, LastModifiedGMT, LastModifiedByUserId) VALUES
							( (select Id from Location where CustomerId = (select Id from Customer where Name = 'Oranj') and Name = 'Downtown'),
							'49.28085236522246','-123.12223434448242',UTC_TIMESTAMP(), 1);
							
							
INSERT INTO LocationLine ( Id,  LocationId, LastModifiedGMT, LastModifiedByUserId ) Values 
                         (1, (select Id from Location where CustomerId = (select Id from Customer where Name = 'Oranj') and Name = 'Downtown'),UTC_TIMESTAMP(), 1 );
						 
						 
INSERT INTO LocationLineGeopoint (LocationLineId, Latitude, Longitude) VALUES
								 (1, 			'49.287029', '-123.128521' );
							 
INSERT INTO LocationLineGeopoint (LocationLineId, Latitude, Longitude) VALUES
								 (1, 			'49.286014', '-123.126987' );


INSERT INTO LocationLineGeopoint (LocationLineId, Latitude, Longitude) VALUES
								 (1, 			'49.286714', '-123.125961' );





							 
							 
select * from LocationLine LL inner join LocationLineGeopoint LLG
on LL.Id = LLG.LocationLineId order by LLG.Id,LocationLineId
and LocationId = 2 ;							 

select LL.LocationId, LLG.Latitude, LLG.Longitude from LocationLine LL inner join LocationLineGeopoint LLG
on LL.Id = LLG.LocationLineId order by LLG.Id,LocationLineId
and LocationId = 2 ;	
							 