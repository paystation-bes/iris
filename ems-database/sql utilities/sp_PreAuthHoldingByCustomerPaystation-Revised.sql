--  ------------------------------------------------------------------------------------------------------------------------------
--  Procedure:      sp_PreAuthHoldingByCustomerPaystation
--
--  Purpose:        Counts dollars and records in the PreAuth holding queue table by Customer, by Paystation.
--
--                  The PreAuth holding queue has been implemented in EMS using table 'PreAuthHolding'.
--  
--  Parameters:     Customer_Name   complete or partial customer name, if blank then all customers
--                  Serial_Number   complete or partial paystation serial number, if blank then all paystations
--                  Begin_Date      earliest date a PreAuth record was moved into PreAuthHolding 
--                  End_Date        last date a PreAuth record was moved into PreAuthHolding
--                  Moved           permitted values: 0, 1, 2
--                                  0   only search PreAuthHolding records NOT previously moved into CardRetryTransaction
--                                  1   only search PreAuthHolding records that HAVE been previously moved into CardRetryTransaction
--                                  2   search all PreAuthHolding records
--
-- Example:         CALL sp_PreAuthHoldingByCustomerPaystation ('Laz', '3', '2012-01-01', '2013-01-01', 2);
--  
--                  The above call will report on Customers with 'Laz' in their name, Paystations containing the number '3' in 
--                  their serial number, having PreAuth records inserted into the PreAuthHolding table 
--                  between '2012-01-01' and '2013-01-01', and it doesn't matter if these PreAuthHolding records were
--                  previously moved (inserted) into CardRetryTransaction in the past.
-- Source Code Iteration:   
-- 1.  Date: 22/02/2012    Version: 6.3 Build 1000     First release
-- 2.  Date: 14/05/2012    Version: 6.3 Build 1011     Changed T.TypeId = 8 to T.TypeId = 20 (from Uncloseable to Recoverable)
-- 3.  Date: 29/08/2012                                Commented out: AND T.CardHash = A.CardHash (because ProcessorTransaction will not (or very rarely) have a CardHash)
-- 4.  Date: 04/12/2012    Version: 6.3 Build 1012     Added Transaction.Purchased date in the Where clause.
--  ------------------------------------------------------------------------------------------------------------------------------              
DROP PROCEDURE IF EXISTS sp_PreAuthHoldingByCustomerPaystation;

delimiter //
CREATE PROCEDURE sp_PreAuthHoldingByCustomerPaystation (IN Customer_Name VARCHAR(100), IN Serial_Number VARCHAR(100), IN Begin_Date DATETIME, IN End_Date DATETIME, IN Moved TINYINT UNSIGNED)
BEGIN
    -- Source Code Iteration = 3
    -- Date: Date: 29/08/2012 Commented out: AND T.CardHash = A.CardHash (because ProcessorTransaction will not (or very rarely) have a CardHash)

    DECLARE Customer_Name2 VARCHAR(110);
    DECLARE Serial_Number2 VARCHAR(110);
    
    SET Customer_Name2 = CONCAT('%', Customer_Name, '%');
    SET Serial_Number2 = CONCAT('%', Serial_Number, '%');
    
    -- Search for PreAuthHolding records NOT moved into CardRetryTransaction
    IF Moved = 0 THEN
    
        SELECT  C.Name AS Customer, P.CommAddress AS Paystation, FORMAT(SUM(A.Amount)/100,2) AS Total_Dollars, COUNT(*) AS PreAuthHolding_Records, 'No' AS Moved, C.Id AS C_Id, P.Id AS PS_Id
        FROM    Customer C, Paystation P, ProcessorTransaction T, PreAuthHolding A
        WHERE   LOWER(C.Name) LIKE LOWER(Customer_Name2)
        AND     C.Id = P.CustomerId
        AND     P.CommAddress LIKE Serial_Number2
        AND     P.Id = A.PaystationId
        AND     A.PaystationId = T.PaystationId
        AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
        AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.Id
        -- AND     T.CardHash = A.CardHash
        AND     T.TypeId = 20 
        AND     T.Amount = A.Amount
        AND     T.AuthorizationNumber LIKE '%:%'
        AND     A.Approved = 1
        AND     A.Expired = 1
        AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
   	AND	T.PurchasedDate BETWEEN Begin_Date AND End_Date
        AND     A.MovedGMT IS NULL
        GROUP BY C.Id, P.Id
        ORDER BY SUM(A.Amount) DESC;
    
    -- Search for PreAuthHolding records that HAVE been moved into CardRetryTransaction    
    ELSEIF Moved = 1 THEN

        SELECT  C.Name AS Customer, P.CommAddress AS Paystation, FORMAT(SUM(A.Amount)/100,2) AS Total_Dollars, COUNT(*) AS PreAuthHolding_Records, 'Yes' AS Moved, C.Id AS C_Id, P.Id AS PS_Id
        FROM    Customer C, Paystation P, ProcessorTransaction T, PreAuthHolding A
        WHERE   LOWER(C.Name) LIKE LOWER(Customer_Name2)
        AND     C.Id = P.CustomerId
        AND     P.CommAddress LIKE Serial_Number2
        AND     P.Id = A.PaystationId
        AND     A.PaystationId = T.PaystationId
        AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
        AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.Id
        -- AND     T.CardHash = A.CardHash
        AND     T.TypeId = 20 
        AND     T.Amount = A.Amount
        AND     T.AuthorizationNumber LIKE '%:%'
        AND     A.Approved = 1
        AND     A.Expired = 1
        AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
	AND	T.PurchasedDate BETWEEN Begin_Date AND End_Date
        AND     A.MovedGMT IS NOT NULL
        GROUP BY C.Id, P.Id
        ORDER BY SUM(A.Amount) DESC;
        
    -- Ignore whether or not PreAuthHolding records have been previously moved into CardRetryTransaction 
    ELSE 
    
        SELECT  C.Name AS Customer, P.CommAddress AS Paystation, FORMAT(SUM(A.Amount)/100,2) AS Total_Dollars, COUNT(*) AS PreAuthHolding_Records, 'Any' AS Moved, C.Id AS C_Id, P.Id AS PS_Id
        FROM    Customer C, Paystation P, ProcessorTransaction T, PreAuthHolding A
        WHERE   LOWER(C.Name) LIKE LOWER(Customer_Name2)
        AND     C.Id = P.CustomerId
        AND     P.CommAddress LIKE Serial_Number2
        AND     P.Id = A.PaystationId
        AND     A.PaystationId = T.PaystationId
        AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
        AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.Id
        -- AND     T.CardHash = A.CardHash
        AND     T.TypeId = 20 
        AND     T.Amount = A.Amount
        AND     T.AuthorizationNumber LIKE '%:%'
        AND     A.Approved = 1
        AND     A.Expired = 1
        AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
   	AND	T.PurchasedDate BETWEEN Begin_Date AND End_Date
        GROUP BY C.Id, P.Id
        ORDER BY SUM(A.Amount) DESC;
        
    END IF;
   
END//
delimiter ;


