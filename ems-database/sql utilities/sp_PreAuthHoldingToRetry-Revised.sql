--  ------------------------------------------------------------------------------------------------------------------------------
--  Procedure:      sp_PreAuthHoldingToRetry
--
--  Purpose:        Inserts records into the CardRetryTransaction table based on records in the PreAuthHolding and
--                  ProcessorTransaction tables for the specified Customer and Paystation (serial number).
--  
--  Parameters:     Customer_Name   customer name (full name of one customer)
--                  Serial_Number   paystation serial number (full serial number of one paystation belonging to one customer)
--                  Begin_Date      earliest date a PreAuth record was moved into PreAuthHolding 
--                  End_Date        last date a PreAuth record was moved into PreAuthHolding
--
-- Example:         CALL sp_PreAuthHoldingToRetry ('Laz Atlanta', '300009480059', '2012-01-01', '2013-01-01');
--  
--                  The above call will insert into table CardRetryTransaction records created by joining PreAuthHolding and 
--                  ProcessorTransaction for the specified Customer and Paystation. The records in PreAuthHolding will have
--                  been inserted (from table PreAuth) between the dates '2012-01-01' and '2013-01-01'.
--
--  Warning:        Running the exact same call for this stored procedure more than once will result in a MySQL database 
--                  error message, something like:
--
--                  Error Code: 1062. Duplicate entry '5191-2011-05-19 11:19:02-20654' for key 'PRIMARY'
--
--                  Once PreAuthHolding records have been inserted into the CardRetryTransaction table those records
--                  must be deleted from CardRetryTransaction before the insert can proceeed again.

-- Source Code Iteration:   
-- 1.  Date: 22/02/2012    Version: 6.3 Build 1000     First release
-- 2.  Date: 10/05/2012    Version: 6.3 Build 1010     Changed T.TypeId = 8 to T.TypeId = 20 (from Uncloseable to Recoverable) in 2 places
-- 3.  Date: 14/05/2012    Version: 6.3 Build 1011     Populate CardRetryTransaction using PreAuthHolding.CardHash (not CardHash from Proc.Trans.)
-- 4.  Date: 16/05/2012    Version: 6.3 Build 1012     Column 'CardExpiry' has been added to tables PreAuth and PreAuthHolding. Use PreAuthHolding.CardExpiry when INSERT-ing into CardRetryTransaction.
-- 5.  Date: 04/12/2012	   Version: 6.3 Build 1013     Added Transaction.Purchased Date in the where Clause.
--  ------------------------------------------------------------------------------------------------------------------------------  
DROP PROCEDURE IF EXISTS sp_PreAuthHoldingToRetry;
delimiter //
CREATE PROCEDURE sp_PreAuthHoldingToRetry (IN Customer_Name VARCHAR(100), IN Serial_Number VARCHAR(100), IN Begin_Date DATETIME, IN End_Date DATETIME)
BEGIN
    -- Source Code Iteration = 4
    -- Date: 16/05/2012    Version: 6.3 Build 1012     Column 'CardExpiry' has been added to tables PreAuth and PreAuthHolding. Use PreAuthHolding.CardRetry when INSERT-ing into CardRetryTransaction.

    -- Number of records inserted into table CardRetryTransaction
    DECLARE Inserted INT UNSIGNED;
    
    -- Number of records updated in table PreAuthHolding
    DECLARE Updated INT UNSIGNED;
    
    -- Number of records deleted from table PreAuthHolding
    DECLARE Deleted INT UNSIGNED;

    -- Creation_Date: default GMT value assigned to CardRetryTransaction.CreationDate & PreAuthHolding.MovedGMT
    DECLARE Creation_Date DATETIME;
    
    -- Temporary table Holding maintains list of PreAuthHolding records to be inserted into CardRetryTransaction and deleted from PreAuthHolding
    DROP TEMPORARY TABLE IF EXISTS Holding;
    CREATE TEMPORARY TABLE Holding (
        Id INT UNSIGNED NOT NULL ,
        PRIMARY KEY (`Id`) 
    ) ENGINE = InnoDB;
      
    -- Initialize internal variables
    SET Inserted = 0;
    SET Deleted = 0;
    SET Creation_Date = UTC_TIMESTAMP();
    
    -- Load table Holding. Requires a join and additional logic between tables PreAuthHolding and ProcessorTransaction
    INSERT  Holding (Id)
    SELECT  A.Id 
    FROM    Customer C ,
            Paystation P ,
            ProcessorTransaction T ,
            PreAuthHolding A 
    WHERE   C.Name = Customer_Name
    AND     C.Id = P.CustomerId
    AND     P.CommAddress = Serial_Number
    AND     P.Id = A.PaystationId
    AND     A.PaystationId = T.PaystationId
    AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
    AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.Id
    AND     T.TypeId = 20 
    AND     T.Amount = A.Amount
    AND     T.AuthorizationNumber LIKE '%:%'
    AND     A.Approved = 1
    AND     A.Expired = 1
    AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
    AND	    T.PurchasedDate BETWEEN Begin_Date AND End_Date;
    
    -- start transaction
    START TRANSACTION;

    -- Insert records into CardRetryTransaction
    INSERT  CardRetryTransaction (PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID)
    SELECT  T.PaystationId ,
            T.PurchasedDate ,
            T.TicketNumber ,
            A.PreAuthDate AS LastRetryDate ,
            0 AS NumRetries ,
            A.CardHash ,
            1 AS TypeId ,
            A.CardData ,
            A.CardExpiry ,
            T.Amount ,
            A.CardType ,
            A.Last4DigitsOfCardNumber ,
            Creation_Date AS CreationDate ,
            '' AS BadCardHash ,
            0 AS IgnoreBadCard ,
            0 AS LastResponseCode ,
            A.IsRFID
    FROM    Customer C ,
            Paystation P ,
            ProcessorTransaction T ,
            PreAuthHolding A ,
            Holding H
    WHERE   C.Name = Customer_Name
    AND     C.Id = P.CustomerId
    AND     P.CommAddress = Serial_Number
    AND     P.Id = A.PaystationId
    AND     A.PaystationId = T.PaystationId
    AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
    AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.Id
    AND     T.TypeId = 20 
    AND     T.Amount = A.Amount
    AND     T.AuthorizationNumber LIKE '%:%'
    AND     A.Approved = 1
    AND     A.Expired = 1
    AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
    AND	    T.PurchasedDate BETWEEN Begin_Date AND End_Date
    AND     A.Id = H.Id;

    -- Save number of records inserted
    SET Inserted = ROW_COUNT();
    
    -- Mark PreAuthHolding records as having been moved into CardRetryTransaction
    UPDATE  PreAuthHolding A, Holding H SET A.MovedGMT = Creation_Date WHERE A.Id = H.Id;
    
    -- Save number of records updated
    SET Updated = ROW_COUNT();
   
    -- This section commented out on purpose
    -- Delete records from PreAuthHolding 
    -- IF Inserted > 0 THEN
    --    DELETE  FROM PreAuthHolding WHERE Id IN (SELECT Id FROM Holding);
    --
    --    -- Save number of records deleted
    --    SET Deleted = ROW_COUNT();
    -- END IF;
    
    -- commit transaction
    COMMIT;
    
    -- Display number of records inserted into CardRetryTransaction
    SELECT  Name AS Customer, 
            Inserted AS CardRetryTransaction_inserted, 
            Updated AS PreAuthHolding_updated, 
    --      Deleted AS PreAuthHolding_deleted, 
            Creation_Date AS CreationDate_and_MovedGMT
    FROM    Customer
    WHERE   Name = Customer_Name;
    
END//
delimiter ;