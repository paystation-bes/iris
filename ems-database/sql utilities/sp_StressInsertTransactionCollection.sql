/*
  --------------------------------------------------------------------------------
  Procedure:    sp_StressInsertTransactionCollection
  
  Purpose:      Inserts rows into table TransactionCollection
  
  Parameters:   1.  p_MinutesToRun
                    o  the number of minutes this procedure should run for
                    o  permitted values: 1 through 10
                2.  p_InsertsPerSecond
                    o  the number of INSERTs to perform per second
                    o  permitted values: 1 through 10
                    
  Assumes:      The procedure assumes table `tmp_StressTestPaystations` exists and has 
                been populated with 1,010 rows.
                
                There is code to CREATE TABLE `tmp_StressTestPaystations` and INSERT INTO
                this table included in this file. (See below.)   
                
  Hints:        Call this stored procedure in one MySQL session and perform ad-hoc SELECTs
                in a different MySQL session 
                
  Call:         CALL sp_StressInsertTransactionCollection (1, 1);
              
  Useful SQL:   
  -------------------------------------------------------------------------------- 
*/ 

/*
    Table `tmp_StressTestPaystations` (not a temporary table) is a key-value pair.
    This allows a key (the `Id`) to provide a value (the `PaystationId`).
    For test data purposes limit the number of rows in table `tmp_StressTestPaystations` to 1,010.
    This allows `sp_StressInsertTransactionCollection` to find up to 10 PaystationId's for any Id between 1 and 1000.
*/
CREATE TABLE IF NOT EXISTS `tmp_StressTestPaystations` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PaystationId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `PaystationId` (`PaystationId` ASC))
ENGINE = InnoDB;

/*
    Load table `tmp_StressTestPaystations` with test data (create the key-value pairs).
    This INSERT will only INSERT once. 
    Subsequent calls will not perform an INSERT because the table has already been populated.
    Useful SQL: SELECT * FROM tmp_StressTestPaystations ORDER BY Id;
*/
INSERT  INTO tmp_StressTestPaystations (PaystationId)
SELECT  Id FROM ems_db_6_3_10.Paystation 
WHERE   NOT EXISTS (SELECT * FROM tmp_StressTestPaystations)
LIMIT   1010;

/*
    Stored procedure code
*/
DROP PROCEDURE IF EXISTS sp_StressInsertTransactionCollection;
delimiter //
CREATE PROCEDURE sp_StressInsertTransactionCollection (IN p_MinutesToRun TINYINT UNSIGNED, IN p_InsertsPerSecond TINYINT UNSIGNED)
BEGIN
    
    /* Variables */
    DECLARE v_DoneGMT       DATETIME;
    DECLARE v_Id            MEDIUMINT UNSIGNED;
    DECLARE v_Sleep         MEDIUMINT UNSIGNED;
    DECLARE v_TicketNumber	MEDIUMINT UNSIGNED;
    DECLARE v_CashDollars	MEDIUMINT UNSIGNED;
    DECLARE v_CoinCount	    MEDIUMINT UNSIGNED;
    DECLARE v_CoinDollars	MEDIUMINT UNSIGNED;
    DECLARE v_BillCount	    MEDIUMINT UNSIGNED;
    DECLARE v_BillDollars	MEDIUMINT UNSIGNED;
    DECLARE v_CardCount	    MEDIUMINT UNSIGNED;
    DECLARE v_CardDollars	MEDIUMINT UNSIGNED;
    
    /* Validate parameter values */
    IF p_MinutesToRun >= 1 AND p_MinutesToRun <= 10 AND p_InsertsPerSecond >= 1 AND p_InsertsPerSecond <= 10 THEN
    
        /* Determine when to stop inserting records */
        SELECT ADDDATE(UTC_TIMESTAMP(),INTERVAL p_MinutesToRun MINUTE) INTO v_DoneGMT;
        
        /* Loop until time expires */
        WHILE UTC_TIMESTAMP() < v_DoneGMT DO
        
            /* Sleep for one second */
            SELECT SLEEP(1) INTO v_Sleep;
            
            /* Get the `key` portion of a key-value pair */
            /* `p_InsertsPerSecond` number of INSERTS will be performed starting from `tmp_StressTestPaystations.Id` = v_Id */ 
            SELECT FLOOR(RAND()*1000) INTO v_Id;
            
            /* Enforce valid range of values for v_Id */
            IF v_Id = 0 THEN SET v_Id = 1; END IF;
            IF v_Id > 1000 THEN SET v_Id = 1000; END IF;
            
            /* Look at the last digit of v_Id (when converted to a string). */
            /* Use this last digit to determine Coin, Bill, Card information */
            SET v_TicketNumber = v_id;
            CASE
                /* Legacy Cash (cannot differentiate between coin and bill) */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 0 THEN 
                    SET v_CashDollars	= 150;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 0;
                    SET v_CardDollars	= 0;
                /* Coin only */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 1 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 3;
                    SET v_CoinDollars	= 400;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 0;
                    SET v_CardDollars	= 0;
                /* Bill only */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 2 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 2;
                    SET v_BillDollars	= 600;
                    SET v_CardCount	    = 0;
                    SET v_CardDollars	= 0;
                /* Card only */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 3 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 1;
                    SET v_CardDollars	= 750;
                /* Coin & Bill */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 4 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 6;
                    SET v_CoinDollars	= 150;
                    SET v_BillCount	    = 3;
                    SET v_BillDollars	= 700;
                    SET v_CardCount	    = 0;
                    SET v_CardDollars	= 0;
                /* Coin & Card */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 5 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 7;
                    SET v_CoinDollars	= 125;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 1;
                    SET v_CardDollars	= 475;
                /* Bill & Card */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 6 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 3;
                    SET v_BillDollars	= 700;
                    SET v_CardCount	    = 1;
                    SET v_CardDollars	= 800;
                /* Coin & Bill & Card */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 7 THEN 
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 2;
                    SET v_CoinDollars	= 50;
                    SET v_BillCount	    = 3;
                    SET v_BillDollars	= 300;
                    SET v_CardCount	    = 1;
                    SET v_CardDollars	= 650;
                /* Legacy Cash #2 (cannot differentiate between coin and bill) */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 8 THEN 
                    SET v_CashDollars	= 200;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 0;
                    SET v_CardDollars	= 0;
                /* Card only */
                WHEN SUBSTRING(v_Id,LENGTH(v_Id),1) = 9 THEN  
                    SET v_CashDollars	= 0;
                    SET v_CoinCount	    = 0;
                    SET v_CoinDollars	= 0;
                    SET v_BillCount	    = 0;
                    SET v_BillDollars	= 0;
                    SET v_CardCount	    = 1;
                    SET v_CardDollars	= 900;
            END CASE;
            
            /* INSERT into TransactionCollection */
            INSERT  INTO TransactionCollection (PaystationId, PurchasedDate, TicketNumber, CashDollars, CoinCount, CoinDollars, BillCount, BillDollars, CardCount, CardDollars)
            SELECT  PaystationId, UTC_TIMESTAMP(), v_Id, v_CashDollars, v_CoinCount, v_CoinDollars, v_BillCount, v_BillDollars, v_CardCount, v_CardDollars
            FROM    tmp_StressTestPaystations 
            WHERE   Id >= v_id
            LIMIT   p_InsertsPerSecond;
            
            /* Continue looping until time limit is reached */
        END WHILE;
    
    /* Invalid parameter values where specified */
    ELSE
        SELECT 'Error: invalid parameter values. Both p_MinutesToRun and p_InsertsPerSecond must be in the range 1 through 10.';      
    END IF;

END//
delimiter ;