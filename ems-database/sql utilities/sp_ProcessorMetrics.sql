﻿
/* 

THIS WAS DEVELOPED WITH THE INTENSION TO POPULATE AS WIDGETS.
But later after discussion, SQL query will be necessary as IT grabs accordingly (kust as KPI dashboard) 


DELIMITER $$

DROP PROCEDURE IF EXISTS sp_ProcessorMetrics $$
CREATE PROCEDURE sp_ProcessorMetrics (in P_FromDate Datetime, in P_ToDate Datetime )
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_custId, lv_custParentCustomerId, lv_ProcessorId,lv_PointOfSaleId,lv_MerchantAccountId,lv_TimeIdGMT,lv_TimeIdDayGMT  mediumint;
declare lv_ProcessingDate  datetime;
declare lv_ProcessorTransactionTypeId,lv_IsApproved tinyint ;
declare lv_ProcessorTransactionId varchar(50) ;
declare lv_AuthorizationNumber,lv_ReferenceNumber varchar(30);



declare c1 cursor  for
select cust.Id, cust.ParentCustomerId, ma.ProcessorId, PointOfSaleId, ProcessingDate, ProcessorTransactionTypeId, MerchantAccountId, ProcessorTransactionId, AuthorizationNumber,ReferenceNumber, IsApproved
from ProcessorTransaction ProTrans inner join PointOfSale pos on ProTrans.PointOfSaleId = pos.Id
inner join Customer cust on pos.CustomerId = cust.Id
inner join MerchantAccount ma on ProTrans.MerchantAccountId = ma.Id
where ProcessingDate >= P_FromDate and ProcessingDate < P_ToDate ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_custId, lv_custParentCustomerId, lv_ProcessorId, lv_PointOfSaleId, lv_ProcessingDate, lv_ProcessorTransactionTypeId, lv_MerchantAccountId, 
			lv_ProcessorTransactionId, lv_AuthorizationNumber,lv_ReferenceNumber, lv_IsApproved;

	SELECT id into lv_TimeIdGMT FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= lv_ProcessingDate);
	SELECT Id INTO lv_TimeIdDayGMT FROM Time where Date = date(lv_ProcessingDate) and QuarterOfDay = 0;
	
    IF (lv_IsApproved =1) and (lv_AuthorizationNumber <> '') and (lv_ProcessorTransactionTypeId <>'') THEN
		
		INSERT INTO ProcessorMetricHour 
		(TimeIdGMT, CustomerId, ParentCustomerId, 			MerchantAccountId, 		ProcessorId, ProcessorAuthoriseTypeId) VALUES
		(lv_TimeIdGMT, lv_custId, lv_custParentCustomerId, 	lv_MerchantAccountId,	lv_ProcessorId,	1)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalCount = TotalCount + 1;
		
		INSERT INTO ProcessorMetricDay 
		(TimeIdGMT, CustomerId, ParentCustomerId, 			MerchantAccountId, 		ProcessorId, ProcessorAuthoriseTypeId) VALUES
		(lv_TimeIdDayGMT, lv_custId, lv_custParentCustomerId, 	lv_MerchantAccountId,	lv_ProcessorId,	1)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalCount = TotalCount + 1;
		
	END IF;
	
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;




-- call sp_ProcessorMetrics('2012-01-05 17:36:01','2012-03-05 17:36:01');
-- call sp_ProcessorMetrics('2012-03-05 17:36:01','2013-03-05 17:36:01');
call sp_ProcessorMetrics('2013-03-05 17:36:01','2014-03-05 17:36:01');

*/

-- SQL queries

-- Approved
SELECT  POS.CustomerId, C.ParentCustomerId, MA.Id, MA.Name as "Merchant Account Name", MA.ProcessorId, P.Name, count(*) as "Requests Approved"
FROM
ProcessorTransaction PT 
INNER JOIN PointOfSale POS on PT.PointOfSaleId = POS.Id
INNER JOIN MerchantAccount MA on PT.MerchantAccountId = MA.Id
INNER JOIN Processor P on MA.ProcessorId = P.Id
INNER JOIN Customer C on POS.CustomerId = C.Id
WHERE 
PT.ProcessingDate between '2015-01-16 00:00:00' and '2015-02-20 00:00:00' AND
PT.IsApproved = 1 AND
PT.ProcessorTransactionid <> '' AND
PT.AuthorizationNumber <> '' 
GROUP BY
POS.CustomerId, MA.Id, MA.ProcessorId;

-- Declined
SELECT POS.CustomerId, C.ParentCustomerId, MA.Id as "MerchantId", MA.Name as "Merchant Account Name", MA.ProcessorId, P.Name as"Processor Name", count(*) as "Requests Declined"
FROM
ProcessorTransaction PT 
INNER JOIN PointOfSale POS on PT.PointOfSaleId = POS.Id
INNER JOIN MerchantAccount MA on PT.MerchantAccountId = MA.Id
INNER JOIN Processor P on MA.ProcessorId = P.Id
INNER JOIN Customer C on POS.CustomerId = C.Id
WHERE 
PT.ProcessingDate between '2015-01-16 00:00:00' and '2015-02-20 00:00:00' AND
PT.IsApproved = 0 AND
PT.ProcessorTransactionid <> '' AND
((PT.AuthorizationNumber <> '') OR (PT.AuthorizationNumber IS NULL))
GROUP BY
POS.CustomerId,MA.Id, MA.ProcessorId;