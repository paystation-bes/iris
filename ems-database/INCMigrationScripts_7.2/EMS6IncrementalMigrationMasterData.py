#!/usr/bin/python


# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert
from EMS6DataAccessInsert import EMS6DataAccessInsert
import pika
class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS6DataAccessInsert, EMS7DataAccess, EMS7DataAccessInsert,verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS7DataAccessInsert = EMS7DataAccessInsert
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6DataAccessInsert = EMS6DataAccessInsert
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6DictCursor = EMS6Connection.cursor(mdb.cursors.DictCursor)
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6InsertCursor = EMS6Connection.cursor() # This cursor is being used for insert purpose only
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}
		
		print " .....starting incremental migration - initiated the migration class variables in the init method......."

	def __migrateCustomers(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			print " ...In __migrateCustomer method........"
			
			if (Action=='Update' and IsProcessed==0):
				self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id from Customer where Id=%s", EMS6Id)
				customer=self.__EMS6Cursor.fetchall()
				for cust in customer:
					self.__EMS7DataAccess.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)		

			elif (Action=='Insert' and IsProcessed==0):
				print "..In Insert action processing step...."
				#self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
				#customer=self.__EMS6Cursor.fetchall()
				customer= self.__EMS6DataAccessInsert.getCustomers(EMS6Id)				
				print "---The customer records count is..." , customer
				for cust in customer:
					self.__EMS7DataAccessInsert.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)
				
			elif (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
				EMS7CustomerId=self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("delete from Customer where Id=%s",EMS7CustomerId)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)		

	# Code begin for ServiceAgreedCustomer
	
	def __migrateServiceAgreedCustomer(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In ServiceAgreedCustomer migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			ServiceAgreementId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccess.UpdateCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccessInsert.addCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				ServiceAgreementRecords=self.__EMS6DataAccessInsert.getServiceAgreedCustomers(CustomerId,ServiceAgreementId)
				IsProcessed = 1
				for ServiceAgreementRecord in ServiceAgreementRecords:
					self.__EMS7DataAccessInsert.deleteCustomerAgreementToEMS7(ServiceAgreementRecord, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	
	# Code end for ServiceAgreedCustomer

	def __migrateEMSRateDayOfWeek(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In EMSRateDayOfWeek migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			EMSRateId = Split[0]
			DayOfWeek= Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				for EMSRateDayOfWeek in EMSRateDayOfWeeks:
					self.__EMS7DataAccess.updateExtensibleRateDayOfWeekToEMS7(EMSRateDayOfWeek, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				for EMSRateDayOfWeek in EMSRateDayOfWeeks:
					self.__EMS7DataAccessInsert.addExtensibleRateDayOfWeekToEMS7(EMSRateDayOfWeek)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				#EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				#for EMSRateDayOfWeek in EMSRateDayOfWeeks:
				self.__EMS7DataAccess.deleteExtensibleRateDayOfWeekToEMS7(EMSRateId, DayOfWeek, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	# Code start for ServiceAgreement
	
	def __migrateServiceAgreement(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ...In ServiceAgreement migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
			
			if (Action=='Update' and IsProcessed==0):
				print "step 1"
				ServiceAgreements=self.__EMS6DataAccessInsert.getEMS6ServiceAgreementInc(EMS6Id)
				print "step 2"
				IsProcessed = 1
				for ServiceAgreement in ServiceAgreements:
					self.__EMS7DataAccess.updateServiceAgreementToEMS7(ServiceAgreement, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				print "step 3"
				ServiceAgreements=self.__EMS6DataAccessInsert.getEMS6ServiceAgreementInc(EMS6Id)
				print "step 4"
				IsProcessed = 1
				for ServiceAgreement in ServiceAgreements:
					self.__EMS7DataAccessInsert.addServiceAgreementToEMS7(ServiceAgreement, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				#EMSRateDayOfWeeks=self.__EMS6DataAccessInsert.getEMSRateDayOfWeek(EMSRateId,DayOfWeek)
				IsProcessed = 1
				#for EMSRateDayOfWeek in EMSRateDayOfWeeks:
				self.__EMS7DataAccess.deleteServiceAgreementToEMS7(EMS6Id)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	# Code end for ServiceAgreement
	
	def __migrateCustomerProperties(self,Action,EMS6Id,IsProcessed,MigrationId):
		print " ...Migrating customer properties record......."
		try:
			if (Action=='Update' and IsProcessed==0):
				CustomerProperties = self.__getCustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				#CustomerProperties = self.__getCustomerProperties(EMS6Id)
				CustomerProperties= self.__EMS6DataAccessInsert.getEMS6CustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccessInsert.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7DataAccess.deleteCustomerPropertyFromEMS7(EMS6Id, Action)	
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getCustomerProperties(self,EMS6Id):
		CustomerProperty=None
		# JIRA 4764
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,case when TimeZone='' then 'GMT' else TimeZone end  as TimeZone,SMSWarningPeriod from CustomerProperties where CustomerId=%s", EMS6Id)
		if (self.__EMS6Cursor.rowcount<>0):
			CustomerProperty=self.__EMS6Cursor.fetchall()
			return CustomerProperty

	def __migrateCustomerSubscription(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...migrating customer subscription..."
		try:
			Split=MultiKeyId.split(',')
			SubscriptionTypeId=None
			CustomerId = Split[0]
			ServiceId= Split[1]
			if (ServiceId=="1"):
				SubscriptionTypeId=100
			if (ServiceId=="2"):
				SubscriptionTypeId=600
			if (ServiceId=="3"):
				SubscriptionTypeId=200
			if(ServiceId=="4"):
				SubscriptionTypeId=700
			if(ServiceId=="5"):
				SubscriptionTypeId=300
			if(ServiceId=="6"):
				SubscriptionTypeId=1000
			if(ServiceId=="7"):
				SubscriptionTypeId=1200
			if(ServiceId=="8"):
				SubscriptionTypeId=900
			if(Action=='Update' and IsProcessed==0):
				CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Insert' and IsProcessed==0):
				#CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				CustomerServiceRelation=self.__EMS6DataAccessInsert.getEMS6CustomerSubscription(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccessInsert.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS6CursorCerberus.execute("select distinct(EmsCustomerId) from cerberus_db.Customer where Id=%s",(CustomerId))
				if(self.__EMS6CursorCerberus.rowcount<>0):
					EMS6CustomerId=self.__EMS6CursorCerberus.fetchone()
				EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
				self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=0 where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getCustomerServiceRelation(self,CustomerId,ServiceId):
		CustomerServiceRelation=None
		self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId) where c.Id=%s and r.ServiceId=%s",(CustomerId,ServiceId))
		if(self.__EMS6CursorCerberus.rowcount<>0):
			CustomerServiceRelation=self.__EMS6CursorCerberus.fetchall()
		return CustomerServiceRelation

	def __migratePaystations(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				IsDeleted=0
				LastModifiedGMT=date.today()
				LastModifiedByUserId=1
				EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
						# JIRA 4757 Dec 3 Donot Migrate Customer Emails
						#self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				EMS6Paystations=self.__EMS6DataAccessInsert.getEMS6Paystation(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccessInsert.addPaystationToEMS7(paystation, Action)
						# JIRA 4757 Dec 3 Donot Migrate Customer Emails
						#self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)

					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6PaystationId(self,EMS6Id):
		EMS6Rows = None
		# Modifeid on Sep 6 and Sep 10 (added p.LockState) added on March 11 CustomCardMerchantAccountId EMS 5151
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 1 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId, \
			RegionId,LotSettingId,p.ContactURL,p.isActive,p.MerchantAccountId, p.LockState,p.CustomCardMerchantAccountId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rows = self.__EMS6Cursor.fetchall()
		return EMS6Rows

	def __migrateLocations(self, Action, EMS6Id, IsProcessed, MigrationId,MultiKeyId):
		print "in .....migrate location data........"
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6Regions=self.__getEMS6Region(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				#EMS6Regions=self.__getEMS6Region(EMS6Id)
				EMS6Regions=self.__EMS6DataAccessInsert.getEMS6Locations(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccessInsert.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
					#self.__EMS7Cursor.execute("delete from Location where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
					# Added on Dec 17 2014 (as part of IIS-4)
					Split=MultiKeyId.split(',')
					EMS6Id = Split[0]
					self.__EMS7Cursor.execute(" Update Location set IsDeleted=1 where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Region(self, EMS6Id):
		EMS6Region=None
		self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Region = self.__EMS6Cursor.fetchall()
		return EMS6Region	

	def __migrateCoupons(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...In coupon migration task........"
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
		#	print "Coupon %s" %Split[0]
			Coupon = Split[0]
		#	print "CustmerId %s" %Split[1]
			CustomerId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				EMS6Coupons=self.__EMS6DataAccessInsert.getEMS6Coupons(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccessInsert.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete' and IsProcessed==0):
				# Code added on Sep 12
				#self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
				#EMS7Connection.commit()
				#self.__updateMigrationQueue(MigrationId)
				IsProcessed = 1
				self.__EMS7DataAccessInsert.deleteCouponToEMS7(Coupon,CustomerId, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6Coupon(self, CustomerId, CouponId):
		# Added on Oct 29
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled, Coupon.DollarDiscount,Coupon.IsOffline,Coupon.ValidForNumOfDay,Coupon.SingleUseDate from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null and CustomerId=%s and Coupon.Id=%s",(CustomerId,CouponId))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSetting(self,Action,EMS6Id,IsProcessed,MigrationId):
		print "...migrating lotsettings data...."
		try:
			if(Action=='Insert' and IsProcessed==0):
				#EMS6LotSettings=self.__getLotSetting(EMS6Id)
				EMS6LotSettings=self.__EMS6DataAccessInsert.getEMS6LotSettings(EMS6Id)
				for LotSetting in EMS6LotSettings:
					self.__EMS7DataAccessInsert.addLotSettingToEMS7(LotSetting, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Update' and IsProcessed==0):
				EMS6LotSettings=self.__getLotSetting(EMS6Id)
				for LotSetting in EMS6LotSettings:
					self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Delete' and IsProcessed==0):
				print "$$ EMS6Id is :", EMS6Id
								
				self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6Id))
						
				print "Record deleted from SettingsFileContent in lotsettings functionality"
				self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s",(EMS6Id))	
				print "Record deleted from SettingsFileContentMapping"
				
				# EMS-4608
				# As there is a FK to PointOfSale delete operation is causing an issue
				# In EMS7 we delete only SettingsFileContent but not SettingsFile. So same approached is implemented here
				#self.__EMS7Cursor.execute("delete from SettingsFile where Id=(select EMS7Id from SettingsFileMapping where EMS6Id=%s)",EMS6Id)
				#print "Deleted from SettingsFile"
				#self.__EMS7Cursor.execute("delete from SettingsFileMapping where EMS6Id=%s",EMS6Id)
				#print "Deleted from SettingsFileMapping"
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getLotSetting(self,EMS6Id):		
		LotSetting = None
		self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL and l.Id=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			LotSetting = self.__EMS6Cursor.fetchall()
		return LotSetting
		
	def __updateMigrationQueue(self,MigrationId):
		self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1, MigratedByProcessId='%s', MigratedDate= now() where Id=%s",(self.__processId, MigrationId))
		EMS6Connection.commit()

	def __getEMS7CustomerId(self,Id):
		self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
		row=self.__EMS7Cursor.fetchone()
		EMS7CustomerId = row[0]
		return EMS7CustomerId

	
## This Module only adds EMSRate to Unified Rate, The module next to it does the incremental migration of the EMSRates into Extensiblerate.
	def __migrateEMSRateIntoUnifiedRate(self,Action,IsProcessed,MigrationId,EMS6Id,MultiKeyId):
		# Jan 20 EMS-???? Passing MultiKeyId as Input parameter and replacing in palce of EMS6Id
		print " Migrating EMS rates to unified rate "
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			self.__migrateEMS7ExtensibleRate(Action,IsProcessed,MigrationId,MultiKeyId)

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6EMSRates(MultiKeyId)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6EMSRates=self.__getEMS6EMSRates(EMS6Id)
				EMS6EMSRates=self.__EMS6DataAccessInsert.getEMS6EMSRates(MultiKeyId)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccessInsert.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select distinct(EMS7RateId) from EMSRateToUnifiedRateMapping where EMS6RateId=%s and EMS7RateId<>0)",MultiKeyId)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
		
	def __getEMS6EMSRates(self,EMS6Id):
#		self.__EMS6Cursor.execute("select distinct r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")
		## Above SQL revised after incremental migration
		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
#		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMS7ExtensibleRate(self,Action,IsProcessed,MigrationId,EMS6Id):
		print" in migrate EMS extensible rate method with EMS6Id...", EMS6Id
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccessInsert.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ExtensibleRate where Id=(select distinct(EMS7RateId) from ExtensibleRateMapping where EMS6RateId=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			print "-- Errror %s" %e

	def __getEMS6ExtensibleRate(self,EMS6Id):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,IsActive,Id from EMSRate where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSettingFileContent(self,Action,EMS6Id,IsProcessed,MigrationId):
			print " ...in migrating lotsettingFileContent..."
			try:
				if (Action=='Update' and IsProcessed==0):
					LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
					for LotSettingFileContent in LotSettingFileContents:
						self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
						self.__updateMigrationQueue(MigrationId)	
				elif (Action=='Insert' and IsProcessed==0):
					#LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
					#Commented on Sep 10
					#LotSettingFileContents=self.__EMS6DataAccessInsert.LotSettingContent(EMS6Id)
					LotSettingFileContents=self.__EMS6DataAccessInsert.getEMS6LotSettingContent(EMS6Id)
					
					for LotSettingFileContent in LotSettingFileContents:
						self.__EMS7DataAccessInsert.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
						self.__updateMigrationQueue(MigrationId)
				elif (Action=='Delete' and IsProcessed==0):
					#Split=EMS6Id.split(',')
					#EMS6IdLotsetting = EMS6Id
					print "In Delete"
					print "EMS6Id is :",EMS6Id
					SettingsFileContentMappings =self.__getEMS7SettingsFileContentMapping(EMS6Id)
					for SettingsFileContentMapping in SettingsFileContentMappings:
						print " $$ SettingsFileContentMapping is ", SettingsFileContentMapping
						sqlString = "delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(SettingsFileContentMapping)
						print sqlString
						
						
						self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId=(select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(SettingsFileContentMapping))
						
						print "Record deleted from SettingsFileContent"
						self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s",(SettingsFileContentMapping))	
						print "Record deleted from SettingsFileContentMapping"
						
						self.__updateMigrationQueue(MigrationId)
						EMS7Connection.commit()
					#print "$$ EMS6Id in __migrateLotSettingFileContent Delete is :",EMS6IdLotsetting
					#mySql = "delete from SettingsFileContent where SettingsFileId = (select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting)
					#print mySql
					#self.__EMS7Cursor.execute("delete from SettingsFileContent where SettingsFileId = (select EMS7Id from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting))
					#self.__EMS7Cursor.execute("delete from SettingsFileContentMapping where EMS6Id=%s)",(EMS6IdLotsetting))
					#self.__updateMigrationQueue(MigrationId)
					
				
			except (Exception, mdb.Error), e:
				self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
						
	def __getEMS6LotSettingFileContent(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __getEMS7SettingsFileContentMapping(self, EMS6Id):
		self.__EMS7Cursor.execute("select EMS6Id from SettingsFileContentMapping where EMS6Id=%s",(EMS6Id))
		return self.__EMS7Cursor.fetchall()
		
	def __migrateRestAccount(self, Action, MultiKeyId, IsProcessed, MigrationId):
		print"....migrating RESTaccount..."
		try:
			print " -- Action %s" %Action
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)

			elif(Action=='Insert' and IsProcessed==0):
				#RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				RestAccounts=self.__EMS6DataAccessInsert.getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccessInsert.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
				IsProcessed = 1
				# IIS-7
				self.__EMS7Cursor.execute("UPDATE RestAccount SET IsDeleted = 1 , VERSION=VERSION+1 where Id=(select EMS7RestAccountId from RestAccountMapping where EMS6AccountName=%s)",(MultiKeyId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

	   	except (Exception, mdb.Error), e:
	   		self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6RestAccount(self, MultiKeyId):
		self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null and r.AccountName=%s",(MultiKeyId))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
		EMS6Rates=None
		#self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		self.__EMS6Cursor.execute("select PaystationId,TicketNumber,PurchasedDate,CustomerId,RegionId,LotNumber,TypeId,RateId,RateName,RateValue,Revenue from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rates=self.__EMS6Cursor.fetchall()
		return EMS6Rates

	def __migrateModemSetting(self,Action,IsProcessed,MigrationId,EMS6Id):
		print"..migrating modem settings data..."
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccess.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				ModemSettings=self.__EMS6DataAccessInsert.getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccessInsert.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ModemSetting where Id=(select EMS7Id from ModemSettingMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6ModemSettings(self, EMS6Id):
		EMS6ModemSetting=None
		self.__EMS6Cursor.execute("select PaystationId, Type, CCID, Carrier, APN from ModemSetting where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6ModemSetting = self.__EMS6Cursor.fetchall()
		return EMS6ModemSetting 

#	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
#		EMS6Rates=None	
#		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		if(self.__EMS6Cursor.rowcount<>0):
#			EMS6Rates=self.__EMS6Cursor.fetchall()
	#	return EMS6Rates
#		else:
#			print " no row was found"
#		return EMS6Rates

	#Added on Jan 31 2014 EMS-4989
	def __migrateCustomerPermissions(self,Action,IsProcessed,MigrationId,MultiKeyId):
		print " ...migrating CustomerPermissions ..."
		try:
			Split=MultiKeyId.split(',')
			SubscriptionTypeId=None
			EMS6CustomerId = Split[0]
			EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)			
			print "EMS7CustomerId is ", EMS7CustomerId
			ReportsEnabled =Split[1]
			
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,100,ReportsEnabled)
			
			print "After Insert"
			
			TextAlertEnabled =Split[2]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,200,TextAlertEnabled)
			
			CreditCardProcessingEnabled=Split[3]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,300,CreditCardProcessingEnabled)
			
			CouponsEnabled=Split[4]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,600,CouponsEnabled)
			
			WebServicesEnabled=Split[5]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1000,WebServicesEnabled)
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1100,WebServicesEnabled)
			
			CardTypesEnabled=Split[6]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,700,CardTypesEnabled)
			
			PayByPhoneEnabled=Split[7]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,900,PayByPhoneEnabled)
						
			PayByCellEnabled=Split[8]
			self.__EMS7DataAccessInsert.addCustomerSubscriptionEnable(EMS7CustomerId,1200,PayByCellEnabled)
			
			self.__updateMigrationQueue(MigrationId)
			
			#if (ServiceId=="1"):
			#	SubscriptionTypeId=100
			#if (ServiceId=="2"):
			#	SubscriptionTypeId=600
			#if (ServiceId=="3"):
			#	SubscriptionTypeId=200
			#if(ServiceId=="4"):
			#	SubscriptionTypeId=700
			#if(ServiceId=="5"):
			#	SubscriptionTypeId=300
			#if(ServiceId=="6"):
			#	SubscriptionTypeId=1000
			#if(ServiceId=="7"):
			#	SubscriptionTypeId=1200
			#if(ServiceId=="8"):
			#	SubscriptionTypeId=900
			
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	
	def IncrementalMigration(self):
		
		"""Create a MigrationProcessLog record"""
		# Added on Jan 10
		self.__EMS6InsertCursor.execute("insert ignore into INCScriptVersion(ModuleName,BuildVersion,LastRunDate) VALUES ('MasterData', '5', now() ) ON DUPLICATE KEY UPDATE LastRunDate=now(), BuildVersion='5'")
		EMS6Connection.commit()
		
		print " ....crating a migrationProcessingLog Record......."
		
		self.__EMS6Cursor.execute("select count(*) from MigrationProcessLog  where ProcessType = 'Incremental Migration MasterData' and EndTime is null")
		processCount = self.__EMS6Cursor.fetchone()
		print "processCount id:",processCount
		if (processCount[0]>=1):
			self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime, EndTime, RecordSelectedCount, RecordProcessedCount, Error)\
                                                       values ('Incremental Migration MasterData',now(), now(), 0, 0,'Too Many Processes Running')")
			EMS6Connection.commit()
			return
		
		print "....Creating MigrationProcessLog record..."
		self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime) values ('Incremental Migration MasterData',now())")
		EMS6Connection.commit()
		self.__processId= self.__EMS6InsertCursor.lastrowid
		
		print "Created migration process log record with id:..", self.__processId
		
		print ".,,selecting record from migration queue....."
		
		self.__EMS7Cursor.execute("select value from EmsProperties where name = 'IncrementalMigrationLimit'")
		MigrationQueueRecordLimit = self.__EMS7Cursor.fetchone()
		
		print "MigrationQueueRecordLimit is :",MigrationQueueRecordLimit
		
		# Code written on Sep 23
		self.__EMS6Cursor.execute("select LowerLimit,UpperLimit,now() from MigrationQueueLimit where ModuleName='MasterData' order by Id desc Limit 1")
		MigrationQueueLimit=self.__EMS6Cursor.fetchone()
		print "MigrationQueueLimit[0] is ", MigrationQueueLimit[0]
		print "MigrationQueueLimit[1] is ", MigrationQueueLimit[1]
		print "Date now() is ", MigrationQueueLimit[2]
		MigrationQueueLowerLimit = MigrationQueueLimit[0]
		MigrationQueueUpperLimit = MigrationQueueLimit[1]
		# Added ServiceAgreedCustomer on Nov 18, Nov 19
		self.__EMS6Cursor.execute("select * FROM MigrationQueue where Id between '%d' and '%d' and TableName in ('Customer','CustomerProperties','Customer_Service_Relation','Region','LotSetting', \
			'Paystation','LotSettingFileContent','Coupon','RESTAccount','ModemSetting','EMSRate','ServiceAgreedCustomer','EMSRateDayOfWeek','ServiceAgreement','CustomerPermissions') and SelectedForMigration = '%d' and \
			SelectedByProcessId='%d' and IsProcessed ='%d' order by Id" % (MigrationQueueLimit[0],MigrationQueueLimit[1],0,0,0))
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		print " --number of records selected for migration is:..", len(MigrationQueue)
		
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set RecordSelectedCount = "%d" where id = "%d"' % (len(MigrationQueue),self.__processId ))
		
		""" mark the selected record from the migration queue table as selected for processing"""
		print "----Flagging the selected record as selected for processing....."
		for rec in MigrationQueue:
			self.__EMS6InsertCursor.execute("update MigrationQueue set SelectedForMigration ='%d' , SelectedByProcessId = '%d' where Id = '%d' " % (1, self.__processId, rec[0]))
			
		EMS6Connection.commit()
		
		error ="None"
		
	#	input = raw_input("Start Migration  ........................... y/n ?   :   ")
	#	if (input=='y') :
		for migration in MigrationQueue:
			MigrationId = migration[0]
			MultiKeyId = migration[1]
			EMS6Id = migration[2]
			TableName = migration[3]
			Action = migration[4]
			IsProcessed = migration[5]
			Date = migration[6]
			
			''' Begin Transaction '''
			print " ----starting incremental data migration....."
			
			if(IsProcessed==0):
				if (TableName=='Customer'):
					self.__migrateCustomers(Action,EMS6Id,IsProcessed,MigrationId)	
					
				if (TableName=='CustomerProperties'):
					self.__migrateCustomerProperties(Action,EMS6Id,IsProcessed,MigrationId)
					
				if(TableName=='Customer_Service_Relation'):
					self.__migrateCustomerSubscription(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if (TableName=='Paystation'):
					self.__migratePaystations(Action,EMS6Id,IsProcessed,MigrationId)
					
				if (TableName=='Region'):
					# Added on Dec 12 2014 (as part of IIS-4)
					self.__migrateLocations(Action,EMS6Id,IsProcessed,MigrationId,MultiKeyId)
					
				if (TableName=='Coupon'):
					self.__migrateCoupons(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if (TableName=='LotSetting'):
					self.__migrateLotSetting(Action,EMS6Id,IsProcessed,MigrationId)
				
				if(TableName=='RESTAccount'):
					self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
					
				if(TableName=='EMSRate'):
					# Jan 20 EMS-???? Added MultiKeyId as additional parameter
					self.__migrateEMSRateIntoUnifiedRate(Action,IsProcessed,MigrationId,EMS6Id,MultiKeyId)
					
				if(TableName=='ModemSetting'):
					self.__migrateModemSetting(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='LotSettingFileContent'):
					self.__migrateLotSettingFileContent(Action,EMS6Id,IsProcessed,MigrationId)
					
				if ( TableName=='ServiceAgreedCustomer'):
					self.__migrateServiceAgreedCustomer(Action,IsProcessed,MigrationId,MultiKeyId)
				
				if ( TableName=='EMSRateDayOfWeek'):
					self.__migrateEMSRateDayOfWeek(Action,IsProcessed,MigrationId,MultiKeyId)
				
				if ( TableName=='ServiceAgreement'):
					self.__migrateServiceAgreement(Action,EMS6Id,IsProcessed,MigrationId)
				
				# Added on Jan 31 2014
				if ( TableName=='CustomerPermissions'):
					self.__migrateCustomerPermissions(Action,IsProcessed,MigrationId,MultiKeyId)
			""" Updete the process log table"""		
						
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set EndTime = now(), RecordProcessedCount = "%d", Error= "%s" where id = "%d"' % (len(MigrationQueue),error, self.__processId ))
		EMS6Connection.commit()
		#Code added on Sep 23
		self.__EMS6Cursor.execute("select max(id),now() from MigrationQueue")
		MigrationQueueMaxId=self.__EMS6Cursor.fetchone()
		
		MigrationQueueCurrentMaxId = MigrationQueueMaxId[0]
		print "$$ MigrationQueueCurrentMaxId is ",MigrationQueueCurrentMaxId
		print "$$ MigrationQueueUpperLimit is ",MigrationQueueUpperLimit
		print "$$ MigrationQueueRecordLimit is ",MigrationQueueRecordLimit[0]
		
		MigrationQueueUpperLimitNew = int(MigrationQueueUpperLimit) + int(MigrationQueueRecordLimit[0])
		
		print "$$ MigrationQueueUpperLimitNew", MigrationQueueUpperLimitNew
		
		if (MigrationQueueUpperLimitNew < MigrationQueueCurrentMaxId):
			print " $$ Inside If"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='MasterData'",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('MasterData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueUpperLimitNew,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()
			
		else:
			print " $$ Inside Else"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='MasterData'",(MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('MasterData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()		
		
EMS6Connection = None
EMS7Connection = None
try:
	print " .....getting EMS6Connection from DB......."
	
	EMS6Connection = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'ems_db_6_3_14')
	
	
	print " ****.....Got EMS6Connection from DB......."
	
	print " .....getting Cerberus connetion  from DB..This is a substitute connection....."
	
	EMS6ConnectionCerberus = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'cerberus_db')
	
	
	if (EMS6ConnectionCerberus):
		print " ***Got Cerberus Connection from DB......."
	
	print " .....getting EMS7 Connection from DB......."
	
	EMS7Connection = mdb.connect('172.30.5.184', 'IncMigration_reg','IncMigration_reg','ems7')
	#EMS7Connection = mdb.connect('172.16.1.64', 'root','root123','ems7_QA_July21')
	
	credentials = pika.PlainCredentials('admin', 'Password1$')
	parameters = pika.ConnectionParameters('172.30.5.203',
                                       5672,
                                       '/',
                                       credentials)
	MQconnection = pika.BlockingConnection(parameters)
	channel = MQconnection.channel()
	# print " .....Got  EMS7Connection from DB......."
	
	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS6DataAccessInsert(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), EMS7DataAccessInsert(EMS7Connection,channel, 1), 1)
	print " .....Got  EMS7Connection from DB222......."
	startMigration.IncrementalMigration()
except (Exception, mdb.Error), e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
	if EMS6Connection:
		EMS6Connection.close()
	if EMS7Connection:
		EMS7Connection.close()
	if channel:
		channel.close()
