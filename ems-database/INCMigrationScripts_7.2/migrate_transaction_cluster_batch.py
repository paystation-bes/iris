#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date

ClusterId = 1;
try:

	i = 0
	while i < 53:
		
		EMS7Connection = mdb.connect('localhost', 'root','root123','ems7_QA_ASHOK')
		EMS7Cursor = EMS7Connection.cursor()
	
		print " Connected123"
		EMS7Cursor.execute("select WeekStartDate,WeekEndDate,Id from MigrationWeek where Status =0 order by Id Limit 1")
		MigrationWeek=EMS7Cursor.fetchall()
	
		for eachWeek in MigrationWeek:
			WeekStartDate = eachWeek[0]
			WeekEndDate = eachWeek[1]
			MigrationWeekId = eachWeek[2]
		
			print "WeekStartDate", WeekStartDate
			print "WeekEndDate", WeekEndDate
		

			args = (1,'Purchase',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)

			args = (1,'ProcessorTransaction',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			args = (1,'Permit',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			args = (1,'PaymentCard',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			args = (1,'POSCollection',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			args = (1,'PurchaseTax',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			#args = (1,'PurchaseCollection',WeekStartDate,WeekEndDate,8)
			#EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
		
			args = (1,'PaymentCardForPatrollerCard',WeekStartDate,WeekEndDate,8)
			EMS7Cursor.callproc('sp_GenerateMigrationDateRange', args)
	
			EMS7Cursor.execute("update MigrationWeek set Status=2 where Id=%s",MigrationWeekId)
		
			EMS7Connection.commit()
		
			# execfile('migrate_transaction_cluster1.py')
	
			print "Completed Migrating Transactions Week 1"
			EMS7Connection.close()
			print "Closed"
			i = i+1
	

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
