#!/usr/bin/python


# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert
from EMS6DataAccessInsert import EMS6DataAccessInsert
import pika
class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS6DataAccessInsert, EMS7DataAccess, EMS7DataAccessInsert, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS7DataAccessInsert = EMS7DataAccessInsert
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6DataAccessInsert = EMS6DataAccessInsert
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6DictCursor = EMS6Connection.cursor(mdb.cursors.DictCursor)
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6InsertCursor = EMS6Connection.cursor() # This cursor is being used for insert purpose only
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}
		
		# print " .....starting incremental migration - initiated the migration class variables in the init method......."
	
	# IIS-11 Added on Dec 16
	def __updateMigrationQueue(self,MigrationId):
		self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1, MigratedByProcessId='%s', MigratedDate= now() where Id=%s",(self.__processId, MigrationId))
		EMS6Connection.commit()
	
	def __migrateCardRetryTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId):
		# print " ....migrating card retry transactions......"
		try:
			# Commented for US483
			# print " -- Action %s" %Action
			# print " -- IsProcessed %s" %IsProcessed
			# print " -- MigrationId %s" %MigrationId
			# print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId=Split[0]           
			# print "PaystationId %s" %PaystationId 
			PurchasedDate=Split[1]           
			# print "PurchasedDate %s" %PurchasedDate
			TicketNumber=Split[2]            
			# print "TicketNumber %s" %TicketNumber

			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				self.__EMS7DataAccess.addCardRetryTransactionToEMS7(MultiKeyId,Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				#EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
				EMS6CardRetryTransactions = self.__EMS6DataAccessInsert.getEMS6CardRetryTransaction(PaystationId,PurchasedDate,TicketNumber)
				#print EMS6CardRetryTransactions[0] #commented to resolve tuple index out of range
				if(EMS6CardRetryTransactions):
					for EMS6CardRetryTransaction in EMS6CardRetryTransactions:
						self.__EMS7DataAccessInsert.addCardRetryTransactionToEMS7(EMS6CardRetryTransaction,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
	#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
	#			if(EMS6CardRetryTransactions):
				self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def IncrementalMigration(self):
		
		"""Create a MigrationProcessLog record"""
		# Added on Jan 10
		self.__EMS6InsertCursor.execute("insert ignore into INCScriptVersion(ModuleName,BuildVersion,LastRunDate) VALUES ('CardRetry', '5', now() ) ON DUPLICATE KEY UPDATE LastRunDate=now(), BuildVersion='5'")
		EMS6Connection.commit()
		
		# print " ....crating a migrationProcessingLog Record......."
		
		self.__EMS6Cursor.execute("select count(*) from MigrationProcessLog  where ProcessType = 'CardRetry' and EndTime is null")
		processCount = self.__EMS6Cursor.fetchone()
		# print "processCount id:",processCount
		if (processCount[0]>=1):
			self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime, EndTime, RecordSelectedCount, RecordProcessedCount, Error)\
                                                       values ('CardRetry',now(), now(), 0, 0,'Too Many Processes Running')")
			EMS6Connection.commit()
			return
		
		# print "....Creating MigrationProcessLog record..."
		self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime) values ('CardRetry',now())")
		EMS6Connection.commit()
		self.__processId= self.__EMS6InsertCursor.lastrowid
		
		# print "Created migration process log record with id:..", self.__processId
		
		# print ".,,selecting record from migration queue....."
		
		self.__EMS7Cursor.execute("select value from EmsProperties where name = 'IncrementalMigrationCardRetryLimit'")
		MigrationQueueRecordLimit = self.__EMS7Cursor.fetchone()
		
		# print "MigrationQueueRecordLimit is :",MigrationQueueRecordLimit
		
		# Code written on Sep 23
		self.__EMS6Cursor.execute("select LowerLimit,UpperLimit,now() from MigrationQueueLimit where ModuleName='CardRetry' order by Id desc Limit 1")
		MigrationQueueLimit=self.__EMS6Cursor.fetchone()
		# print "MigrationQueueLimit[0] is ", MigrationQueueLimit[0]
		# print "MigrationQueueLimit[1] is ", MigrationQueueLimit[1]
		# print "Date now() is ", MigrationQueueLimit[2]
		MigrationQueueLowerLimit = MigrationQueueLimit[0]
		MigrationQueueUpperLimit = MigrationQueueLimit[1]
		
		self.__EMS6Cursor.execute("select * FROM MigrationQueue where Id between '%d' and '%d' and TableName in ('CardRetryTransaction') and \
			SelectedForMigration = '%d' and SelectedByProcessId='%d' and IsProcessed ='%d' order by Id" % (MigrationQueueLimit[0],MigrationQueueLimit[1],0,0,0))
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		# print " --number of records selected for migration is:..", len(MigrationQueue)
		
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set RecordSelectedCount = "%d" where id = "%d"' % (len(MigrationQueue),self.__processId ))
		
		""" mark the selected record from the migration queue table as selected for processing"""
		# print "----Flagging the selected record as selected for processing....."
		for rec in MigrationQueue:
			self.__EMS6InsertCursor.execute("update MigrationQueue set SelectedForMigration ='%d' , SelectedByProcessId = '%d' where Id = '%d' " % (1, self.__processId, rec[0]))
			
		EMS6Connection.commit()
		
		error ="None"
		
	#	input = raw_input("Start Migration  ........................... y/n ?   :   ")
	#	if (input=='y') :
		for migration in MigrationQueue:
			MigrationId = migration[0]
			MultiKeyId = migration[1]
			EMS6Id = migration[2]
			TableName = migration[3]
			Action = migration[4]
			IsProcessed = migration[5]
			Date = migration[6]
			
			''' Begin Transaction '''
			# print " ----starting incremental data migration....."
			
			if(IsProcessed==0):
				if(TableName=='CardRetryTransaction'):
					self.__migrateCardRetryTransaction(Action, IsProcessed, MigrationId, MultiKeyId)
					
					
			""" Updete the process log table"""		
						
		# Added on Jan 29 US483
		# sqlproc = "call sp_Delete_MigrationProcessLog(100000)" 
		# self.__EMS6Cursor.execute(sqlproc)
		
		self.__EMS6InsertCursor.execute("DELETE from MigrationProcessLog where StartTime < date_sub(now(), interval 30 day)")
		EMS6Connection.commit()
		
		self.__EMS6InsertCursor.execute("DELETE from MigrationQueue where CreatedDate < date_sub(now(), interval 30 day) and IsProcessed = 1")
		EMS6Connection.commit()
		
		# sqlproc = "call sp_Delete_MigrationQueue(100000)" 
		# self.__EMS6Cursor.execute(sqlproc)
		# EMS6Connection.commit()
		
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set EndTime = now(), RecordProcessedCount = "%d", Error= "%s" where id = "%d"' % (len(MigrationQueue),error, self.__processId ))
		EMS6Connection.commit()
		#Code added on Sep 23
		self.__EMS6Cursor.execute("select max(id),now() from MigrationQueue")
		MigrationQueueMaxId=self.__EMS6Cursor.fetchone()
		
		MigrationQueueCurrentMaxId = MigrationQueueMaxId[0]
		# print "$$ MigrationQueueCurrentMaxId is ",MigrationQueueCurrentMaxId
		# print "$$ MigrationQueueUpperLimit is ",MigrationQueueUpperLimit
		# print "$$ MigrationQueueRecordLimit is ",MigrationQueueRecordLimit[0]
		
		MigrationQueueUpperLimitNew = int(MigrationQueueUpperLimit) + int(MigrationQueueRecordLimit[0])
		
		# print "$$ MigrationQueueUpperLimitNew", MigrationQueueUpperLimitNew
		
		if (MigrationQueueUpperLimitNew < MigrationQueueCurrentMaxId):
			# print " $$ Inside If"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='CardRetry'",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('CardRetry',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueUpperLimitNew,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()
			
		else:
			# print " $$ Inside Else"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='CardRetry'",(MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('CardRetry',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()
		
		
EMS6Connection = None
EMS7Connection = None
try:
	# print " .....getting EMS6Connection from DB......."
	
	EMS6Connection = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'ems_db_6_3_14')
	
	
	# print " ****.....Got EMS6Connection from DB......."
	
	# print " .....getting Cerberus connetion  from DB..This is a substitute connection....."
	
	EMS6ConnectionCerberus = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'cerberus_db')
	
	
	# if (EMS6ConnectionCerberus):
		# print " ***Got Cerberus Connection from DB......."
	
	# print " .....getting EMS7 Connection from DB......."
	
	EMS7Connection = mdb.connect('172.30.5.184', 'IncMigration_reg','IncMigration_reg','ems7')
	#EMS7Connection = mdb.connect('172.16.1.64', 'root','root123','ems7_QA_July21')
	
	
	# print " .....Got  EMS7Connection from DB......."
	
	credentials = pika.PlainCredentials('admin', 'Password1$')
	parameters = pika.ConnectionParameters('172.30.5.203',
                                       5672,
                                       '/',
                                       credentials)
	MQconnection = pika.BlockingConnection(parameters)
	channel = MQconnection.channel()
	
	
	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS6DataAccessInsert(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), EMS7DataAccessInsert(EMS7Connection,channel, 1), 1)
	
	startMigration.IncrementalMigration()
except (Exception, mdb.Error), e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
	if EMS6Connection:
		EMS6Connection.close()
	if EMS7Connection:
		EMS7Connection.close()
	if channel:
		channel.close()	