

DROP PROCEDURE IF EXISTS sp_MigrateRevenueIntoKPI_Inc;

delimiter //

CREATE PROCEDURE sp_MigrateRevenueIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN
-- Last modified on June 11
-- DECLARE SECTION
declare lv_TimeIdGMT MEDIUMINT UNSIGNED ;
declare lv_TimeIdLocal MEDIUMINT UNSIGNED;
declare lv_Customer_Timezone char(25); -- it is char(64) 
declare lv_Purchase_CustomerId MEDIUMINT UNSIGNED ;
declare lv_Purchase_LocationId MEDIUMINT UNSIGNED ;
declare lv_Permit_LocationId MEDIUMINT UNSIGNED ;	
declare lv_Permit_SpaceNumber MEDIUMINT UNSIGNED ;	
declare lv_Purchase_PointOfSaleId MEDIUMINT UNSIGNED ;
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
declare lv_Purchase_UnifiedRateId MEDIUMINT UNSIGNED ;
declare lv_Purchase_PaystationSettingId MEDIUMINT UNSIGNED ;
declare lv_Purchase_Id BIGINT ;
declare lv_Permit_Id BIGINT ;
declare lv_Permit_PermitIssueTypeId TINYINT UNSIGNED ;
declare lv_Permit_PermitTypeId TINYINT UNSIGNED ;
declare lv_Purchase_TransactionTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PaymentTypeId TINYINT UNSIGNED ;
declare lv_ProcessorTransaction_TypeId TINYINT UNSIGNED ;		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
declare lv_Purchase_CoinCount TINYINT UNSIGNED ;				-- PPPInfo.CoinCount
declare lv_Purchase_BillCount TINYINT UNSIGNED ;				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
declare lv_Purchase_CouponId MEDIUMINT UNSIGNED ;				-- PPPInfo.IsCoupon check null/not null
declare lv_Purchase_CashPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_CardPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_IsOffline TINYINT UNSIGNED ;				-- PPPInfo.IsOffline
declare lv_Purchase_IsRefundSlip TINYINT UNSIGNED ;			-- PPPI
DECLARE lv_Purchase_ChargedAmount MEDIUMINT UNSIGNED ;
DECLARE CursorDone INT DEFAULT 0;
DECLARE RowNumber                               INT UNSIGNED DEFAULT 0;
DECLARE RowsToMigrate                           INT UNSIGNED DEFAULT 0;
declare lv_PayStation_Id MEDIUMINT UNSIGNED ;
declare lv_PaystationTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PurchaseGMT DATETIME;
declare lv_ppp_IsFreePermit TINYINT UNSIGNED ;
declare idmaster_pos,indexm_pos int default 0;
declare lv_ppp_Id BIGINT;
declare lv_PermitBeginGMT DATETIME;
declare lv_PermitExpireGMT DATETIME;
-- Payment Card columns
declare lv_PaymentCard_Amount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_CardTypeID TINYINT UNSIGNED ;
declare lv_PaymentCard_CreditCardTypeID TINYINT UNSIGNED ;
declare	lv_PaymentCard_MerchantAccountID MEDIUMINT UNSIGNED ;

declare lv_Purchase_CoinPaidAmount MEDIUMINT UNSIGNED ;
declare lv_Purchase_BillPaidAmount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_Id BIGINT;
declare lv_Permit_OriginalPermitId BIGINT;

declare lv_PermitBeginLocal  DATETIME;
declare lv_occ_BeginTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_occ_ExpireTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_PermitExpireLocal  DATETIME;
declare lv_occ_ExpireTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginMinutes  TINYINT UNSIGNED ;
declare lv_occ_ExpireMinutes  TINYINT UNSIGNED ;
declare lv_occ_DurationMinutes  MEDIUMINT UNSIGNED;

declare lv_TimeIdDayLocal mediumint unsigned;
declare lv_TimeIdMonthLocal mediumint unsigned;
declare lv_PPPTotalHour_Id bigint unsigned;
declare lv_PPPDetailHour_Id  bigint unsigned;
declare lv_PPPTotalDay_Id  bigint unsigned;
declare lv_PPPDetailDay_Id  bigint unsigned;
declare lv_PPPTotalMonth_Id  bigint unsigned;
declare lv_PPPDetailMonth_Id  bigint unsigned;
declare lv_ProcessorTransaction_Id bigint unsigned;
declare lv_ETLLimit INT UNSIGNED;
declare lv_no_of_spaces_location mediumint unsigned ;

-- CURSOR C1 STATEMENT
DECLARE C1 CURSOR FOR
	
		SELECT distinct
		P.CustomerId,
		P.LocationId,
		ifnull(R.LocationId,0),
		ifnull(R.SpaceNumber,0),
		P.PointOfSaleId,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.Id,
		R.Id,
		ifnull(R.PermitIssueTypeId,0),
		ifnull(R.PermitTypeId,0),		
		P.TransactionTypeId,
		P.PaymentTypeId,
		T.ProcessorTransactionTypeId, 
		P.CoinCount,
		P.BillCount,
		P.CouponId,
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.IsOffline,
		P.IsRefundSlip,
		P.PurchaseGMT,
		R.PermitBeginGMT,
		R.PermitExpireGMT,
		C.Amount,
		C.CardTypeID,
		C.CreditCardTypeID,
		C.MerchantAccountID,
		P.CoinPaidAmount,
		P.BillPaidAmount,
		C.Id,
		ifnull(R.OriginalPermitId,R.Id),
		B.PropertyValue,
		P.ChargedAmount+P.ExcessPaymentAmount,
		T.Id			-- Added on June 11 for Incremental ETL
		from StagingPurchase P
		left outer join    StagingPermit R on  P.Id = R.PurchaseId
		left outer join   StagingPaymentCard C on P.Id = C.PurchaseId
		left outer join   StagingProcessorTransaction T on P.Id = T.PurchaseId
		left join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE  -- P.CustomerId in (160) AND /* This was used for micro-migration only */
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL LIMIT lv_ETLLimit;

		--  AND P.PurchaseGMT between P_BeginPurchaseGMT AND P_EndPurchaseGMT
		
		
		-- specify a date range
		
 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PurchaseId,		CustomerId,				PointOfSaleId,				PurchaseGMT, 			 ETLObject, 	record_insert_time )  values(
      lv_Purchase_Id,	lv_Purchase_CustomerId,	lv_Purchase_PointOfSaleId,	lv_Purchase_PurchaseGMT, 'Purchase', 	NOW() );
	  
	  -- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
	  -- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
	  INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
	  IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
	  IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
	  -- commented on Oct 28
	  -- IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
		
	  DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
	  DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
	  DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
	  -- commented on Oct 28
	  -- DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
	  COMMIT ; 
	  -- set autocommit = 1 ;
  END;


 
 -- set lv_Customer_Timezone = null;
 
 -- select PropertyValue into lv_Customer_Timezone
 -- from CustomerProperty where CustomerId = P_CustomerId and CustomerPropertyTypeId=1;
 
 -- DELETE FROM ETLQueueRecordCount where ETLProcessDateRangeId = P_ETLExecutionLogId ;
 
 SELECT Value INTO lv_ETLLimit FROM EmsProperties WHERE Name = 'ETLLimit';
 
 
 
 OPEN C1;
	set idmaster_pos = (select FOUND_ROWS());
-- select idmaster_pos;

	UPDATE ETLExecutionLog
	SET PurchaseRecordCount  = idmaster_pos,
	PPPCursorOpenedTime = NOW()
	WHERE Id = P_ETLExecutionLogId;
	
	/*INSERT INTO ETLQueueRecordCount
	(ETLProcessDateRangeId,		TotalRecords,	ETLRemaingRecords,	LastModifiedGMT) VALUES
	(P_ETLExecutionLogId,	idmaster_pos,	idmaster_pos,		NOW());*/
	
-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
-- set autocommit = 0 ;

while indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	lv_Purchase_CustomerId, -- PPPInfo.CustomerID
					lv_Purchase_LocationId, -- PPPInfo.PurchaseLocationId
					lv_Permit_LocationId,	-- PPPInfo.PermitLocationId
					lv_Permit_SpaceNumber,	-- PPPInfo.SpaceNumber	
					lv_Purchase_PointOfSaleId,	--	PPPInfo.PointOfSaleId
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
					lv_Purchase_UnifiedRateId,	-- PPPInfo.UnifiedRateId
					lv_Purchase_PaystationSettingId,	--	PPPInfo.PaystationSettingId
					lv_Purchase_Id,						-- PPPInfo.PurchaseId
					lv_Permit_Id,						-- PPPInfo.PermitId
					lv_Permit_PermitIssueTypeId,		--	PPPInfo.PermitIssueTypeId
					lv_Permit_PermitTypeId,				--	PPPInfo.PermitTypeId 
					lv_Purchase_TransactionTypeId,		-- PPPInfo.TransactionTypeId
					lv_Purchase_PaymentTypeId,			-- PPPInfo.PaymentTypeId
					lv_ProcessorTransaction_TypeId,		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
					lv_Purchase_CoinCount,				-- PPPInfo.CoinCount
					lv_Purchase_BillCount,				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
					lv_Purchase_CouponId,				-- PPPInfo.IsCoupon check null/not null
					lv_Purchase_CashPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_CardPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_IsOffline,				-- PPPInfo.IsOffline
					lv_Purchase_IsRefundSlip,			-- PPPInfo.IsRefundSlip
					lv_Purchase_PurchaseGMT,
					lv_PermitBeginGMT,
					lv_PermitExpireGMT,
					lv_PaymentCard_Amount,
					lv_PaymentCard_CardTypeID,
					lv_PaymentCard_CreditCardTypeID,
					lv_PaymentCard_MerchantAccountID,
					lv_Purchase_CoinPaidAmount,
					lv_Purchase_BillPaidAmount,
					lv_PaymentCard_Id,
					lv_Permit_OriginalPermitId,
					lv_Customer_Timezone,
					lv_Purchase_ChargedAmount,
					lv_ProcessorTransaction_Id;	
								
			
		-- STEP 1: TimeIdGMT --> Purchase.PurchaseGMT GET the ID ftom TIME Table 
		-- SELECT MAX(Id) into lv_TimeIdGMT FROM time WHERE datetime <= lv_Purchase_PurchaseGMT;
		-- select 'aaa';
		SELECT id into lv_TimeIdGMT FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= lv_Purchase_PurchaseGMT);
		
		-- SELECT MAX(Id) INTO lv_TimeIdLocal FROM time where DateTime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone);
				
		SELECT Id INTO lv_TimeIdLocal FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
					
		
		-- STEP 3: CustomerId --> Purchase.CustomerId
		-- STEP 4: PurchaseLocationId --> Purchase.LocationId
		-- STEP 5: PermitLocationId --> Permit.LocationId
		-- STEP 6: SpaceNumber --> Permit.SpaceNumber
		-- STEP 7: PointOfSaleId --> Purchase.PointOfSaleId
		
		
		-- STEP 8: PaystationId --> LOOKUP from PointofSale to PayStation
		SELECT PayStationId INTO lv_PayStation_Id from PointOfSale where Id = lv_Purchase_PointOfSaleId;
		
		
		-- STEP 9: PaystationTypeId --> paystation.PaystationTypeId
		SELECT PaystationTypeId INTO lv_PaystationTypeId FROM Paystation where Id= lv_PayStation_Id;
		
				
		set lv_ppp_IsFreePermit = 0;
		SET lv_ppp_IsFreePermit  = IF ((lv_Purchase_CashPaidAmount = 0 AND lv_Purchase_CardPaidAmount = 0),1,0);
		
		/*INSERT INTO PPPInfo
		(TimeIdGMT, 	TimeIdLocal, 	CustomerId, 			PurchaseLocationId, 	PermitLocationId, 		SpaceNumber, 			PointOfSaleId, 				PaystationId, 		PaystationTypeId, 		UnifiedRateId, 				PaystationSettingId, 			PurchaseId, 		PermitId, 		PermitIssueTypeId, 				PermitTypeId, 			TransactionTypeId, 				PaymentTypeId, 					ProcessorTransactionTypeId, 	CoinCount, 				BillCount, 				DurationMinutes, 																	IsCoupon, 										IsFreePermit, 					 IsOffline, 			IsRefundSlip, 				IsRFID, IsUploadedFromBoss) VALUES
		(lv_TimeIdGMT,	lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Permit_SpaceNumber,	lv_Purchase_PointOfSaleId,	lv_PayStation_Id,	lv_PaystationTypeId,	lv_Purchase_UnifiedRateId,	lv_Purchase_PaystationSettingId,lv_Purchase_Id,		lv_Permit_Id,	lv_Permit_PermitIssueTypeId,	lv_Permit_PermitTypeId,	lv_Purchase_TransactionTypeId,	lv_Purchase_PaymentTypeId,		lv_ProcessorTransaction_TypeId,	lv_Purchase_CoinCount,	lv_Purchase_BillCount,	ifnull((TO_SECONDS(lv_PermitExpireGMT) - TO_SECONDS(lv_PermitBeginGMT))/60,0),		if(length(lv_Purchase_CouponId)>0,1,0),			lv_ppp_IsFreePermit,			 lv_Purchase_IsOffline,	lv_Purchase_IsRefundSlip,	1,		1				);		*/
		
		
		INSERT INTO PPPTotalHour
		(TimeIdGMT,    TimeIdLocal, 	CustomerId, 			TotalAmount					) VALUES
		(lv_TimeIdGMT, lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount	)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalHour_Id  FROM PPPTotalHour;
		
		-- Insert into PPPDetailHour
		INSERT INTO PPPDetailHour
		(PPPTotalHourId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalHour_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailHour_Id FROM PPPDetailHour;
		
		
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- Oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- Oct 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the change done EMS 4303
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the change done EMS 4303
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		
		-- INSERT FOR PPPTotalDay SECOND BLOCK START
		-- GET the starting Time ID from Time Table for that Day
		-- Rewrite this query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and QuarterOfDay = 0;
		
		INSERT INTO PPPTotalDay
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdDayLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalDay_Id FROM PPPTotalDay ;
		
		-- Insert into PPPDetailDay
		INSERT INTO PPPDetailDay
		(PPPTotalDayId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalDay_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailDay_Id FROM PPPDetailDay;
		
		-- start insert into PPPRevenueDay
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				
				INSERT INTO PPPRevenueDay
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN

				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		-- end insert into PPPDetailDay
		-- SECOND BLOCK END
		
		-- THIRD BLOCK START
		
		-- INSERT FOR PPPTotalMonth
		-- GET the starting Time ID from Time table for that Year and Month
		-- Rewrite this Query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		--			and Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone));
		
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		 AND Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
		
		INSERT INTO PPPTotalMonth
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdMonthLocal,lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPTotalMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalMonth_Id FROM PPPTotalMonth;
		
		-- Insert into PPPDetailMonth
		INSERT INTO PPPDetailMonth
		(PPPTotalMonthId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalMonth_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPDetailMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailMonth_Id FROM PPPDetailMonth;
		
		-- start insert into PPPRevenueMonth
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueMonth
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				
				INSERT INTO PPPRevenueMonth
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash)
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
-- end insert into PPPRevenueMonth
		
		-- START FOR OCCUPANCY
		-- Added on Sep 30. ETL will not process if No. of Sapces for that Location is 0
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_Permit_LocationId;
		
		-- EMS 7.1 Cancelled and Test Transactions are excluded from Occupancy and Utilization
		IF (lv_Permit_Id > 0) and (lv_no_of_spaces_location > 0) and ( lv_Purchase_TransactionTypeId not in (5,6) ) THEN
		
		CALL sp_MigrateOccupancyIntoKPI_Inc( lv_Permit_Id ,
										 P_ETLExecutionLogId , 
										 1 , 
										 lv_PermitBeginGMT, 
										 lv_PermitExpireGMT ,
										 lv_Purchase_CustomerId, 
										 lv_Permit_LocationId, 
										 lv_Purchase_UnifiedRateId,
										 lv_Customer_Timezone,
										 lv_Permit_PermitTypeId);
		
		END IF;
		-- END FOR OCCUPANCY
		
		
		
		-- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
		-- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
		INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
		IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
		IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
		-- commented on Oct 28
		-- IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
	  
		DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
		DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
		DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
		-- commented on Oct 28
		-- DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
		
		
		set lv_TimeIdGMT = NULL;
		set lv_TimeIdLocal = NULL;
		set lv_Purchase_LocationId = NULL;
		set lv_Permit_LocationId = NULL;
		set lv_Purchase_CustomerId = null;
		set lv_Permit_SpaceNumber = null;
		set lv_Purchase_PointOfSaleId = null;
		set lv_PayStation_Id = null;
		set lv_PaystationTypeId = null;
		set lv_Purchase_UnifiedRateId = null;
		set lv_Purchase_PaystationSettingId = null;
		set lv_Purchase_Id = null;
		set lv_Permit_Id = null;
		set lv_Permit_PermitIssueTypeId = null;
		set lv_Permit_PermitTypeId = null;
		set lv_Purchase_TransactionTypeId = null;
		set lv_Purchase_PaymentTypeId = null;
		set lv_Purchase_CoinCount = null;
		set lv_ProcessorTransaction_TypeId = null;
		set lv_Purchase_BillCount = null;
		set lv_Purchase_CouponId= null;
		set lv_ppp_IsFreePermit = null;
		set lv_Purchase_IsOffline = null;
		set lv_Purchase_IsRefundSlip = null;
		set lv_PermitExpireGMT = null;
		set lv_PermitBeginGMT = null ;
		set lv_PaymentCard_Amount = null;
		set lv_PaymentCard_CardTypeID = null;
		set lv_PaymentCard_CreditCardTypeID = null;
		set lv_PaymentCard_MerchantAccountID = null;
		set lv_PaymentCard_Id = null;
		set lv_Permit_OriginalPermitId = null;
		set lv_Purchase_ChargedAmount = null;
		set lv_ProcessorTransaction_Id = null;
		
		
		

		IF mod(indexm_pos,1000) = 0 THEN
			COMMIT ;
		END IF ;
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
               
 COMMIT ;		-- ASHOK2	   
-- set autocommit = 1 ; -- Auto Commit ON
 -- ASHOK3 SET AUTO COMMIT ON
    
    -- SELECT NOW() AS EndMigration;
END //
delimiter ;



DROP PROCEDURE IF EXISTS sp_MigrateSettledTransactionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateSettledTransactionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- June 11 2013 

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;
DECLARE lv_IsRefund TINYINT ; 


-- CURSOR VARIABLES
DECLARE v_ProcessorTxn_Id BIGINT ;
DECLARE v_ProcessorTxn_PointOfSaleId MEDIUMINT ; 
DECLARE v_ProcessorTxn_CustomerID MEDIUMINT ;
DECLARE v_ProcessorTxn_PurchaseId BIGINT ; 
DECLARE v_ProcessorTxn_ProcessorTransactionTypeId  TINYINT ;
DECLARE v_ProcessorTxn_MerchantAccountId MEDIUMINT ; 
DECLARE v_ProcessorTxn_Amount MEDIUMINT ;
DECLARE v_ProcessorTxn_ProcessingDate DATETIME ;
DECLARE v_ProcessorTxn_PropertyValue VARCHAR(40);		
declare lv_ETLLimit INT UNSIGNED;

DECLARE C1 CURSOR FOR
	
		SELECT
			T.Id ,
			T.PointOfSaleId , 
			B.CustomerID,
			0,	
			T.ProcessorTransactionTypeId , 
			T.MerchantAccountId , 
			T.Amount ,
			T.ProcessingDate ,
			P.PropertyValue
		FROM 
			StagingProcessorTransaction T, 
			PointOfSale B, 
			CustomerProperty P
		WHERE
			T.PointOfSaleId = B.Id AND 
			B.CustomerId = P.CustomerId AND
			P.CustomerPropertyTypeId = 1 AND 
			T.IsApproved = 1 AND
			P.PropertyValue IS NOT NULL LIMIT lv_ETLLimit ;
			
			-- Check Performance here
			-- AND	T.ProcessingDate BETWEEN P_BeginDateGMT and  P_EndDateGMT 
			-- EMS 4742 March 12 added 	ifnull(T.PurchaseId,1)
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO SettledTxnNotProcessed ( ProcessorTransactionId , PointOfSaleId , 				ProcessorTransactionTypeId , 				ProcessingDate , 				RecordInsertTime) VALUES
										 ( v_ProcessorTxn_Id,		v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_ProcessingDate, 	now()	 );
	  INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
	  DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;

  END;
 

 -- SET lv_Customer_Timezone = null;
 
 SELECT Value INTO lv_ETLLimit FROM EmsProperties WHERE Name = 'ETLLimit';
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET SettledAmountRecordCount = idmaster_pos,
SettledAmountCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_ProcessorTxn_Id ,
					v_ProcessorTxn_PointOfSaleId , 
					v_ProcessorTxn_CustomerID,
					v_ProcessorTxn_PurchaseId,
					v_ProcessorTxn_ProcessorTransactionTypeId , 
					v_ProcessorTxn_MerchantAccountId , 
					v_ProcessorTxn_Amount ,
					v_ProcessorTxn_ProcessingDate ,
					v_ProcessorTxn_PropertyValue ;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				SET lv_TimeIdLocal = NULL ;
				SET lv_IsRefund = NULL ;
				
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) ;
				
				IF v_ProcessorTxn_ProcessorTransactionTypeId in (2,3, 6, 7, 10, 11, 17, 21,23) THEN  -- Added TypeId  21 on May 21, TypeId 23 on June 03 EMS-5628
					SET lv_IsRefund = 0 ;
				ELSEIF v_ProcessorTxn_ProcessorTransactionTypeId in ( 4, 12, 13, 18, 19,22) THEN  -- Added TypeId  22 on June 03 EMS-5628
					SET lv_IsRefund = 1 ;
				ELSE
					SET lv_IsRefund = 2 ;
				END IF;
				
				
				INSERT INTO SettledTransaction (ProcessorTransactionId, CustomerId, 				PurchaseId,				  TimeIdLocal, 	ProcessorTransactionTypeId , 				MerchantAccountId, 					Amount, 				IsRefund)
										VALUES (v_ProcessorTxn_Id,		v_ProcessorTxn_CustomerID,	v_ProcessorTxn_PurchaseId, lv_TimeIdLocal,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_MerchantAccountId,	v_ProcessorTxn_Amount,	lv_IsRefund) ;
	
				
				INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
				DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;

