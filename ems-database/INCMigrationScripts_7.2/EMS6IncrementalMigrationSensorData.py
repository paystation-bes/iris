#!/usr/bin/python


# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert
from EMS6DataAccessInsert import EMS6DataAccessInsert
import pika
class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS6DataAccessInsert, EMS7DataAccess, EMS7DataAccessInsert, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS7DataAccessInsert = EMS7DataAccessInsert
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6DataAccessInsert = EMS6DataAccessInsert
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6DictCursor = EMS6Connection.cursor(mdb.cursors.DictCursor)
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6InsertCursor = EMS6Connection.cursor() # This cursor is being used for insert purpose only
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}
		
		# print " .....starting incremental migration - initiated the migration class variables in the init method......."

	def __updateMigrationQueue(self,MigrationId):
		self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1, MigratedByProcessId='%s', MigratedDate= now() where Id=%s",(self.__processId, MigrationId))
		EMS6Connection.commit()

	def __getEMS7CustomerId(self,Id):
		self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
		row=self.__EMS7Cursor.fetchone()
		EMS7CustomerId = row[0]
		return EMS7CustomerId

	def __getEMS7LocationId(self,EMS6RegionId):
		LocationId = None
		self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(EMS6RegionId))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			LocationId = row[0]
			return LocationId

	def __getEMS7PaystationId(self,EMS6PaystationId):
		PaystationId=None
		self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PaystationId =row[0]
			return PaystationId

	def __getEMS6Alerts(self,EMS6Id):
		Alerts=None
		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert where Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			Alerts = self.__EMS6Cursor.fetchall()	
		return Alerts	

		#This code will be enabled if the Alerts are being deleted
#                elif (Action=='Delete' and IsProcessed==0):
 #                       self.__EMS7Cursor.execute("select distinct(EMS7AlertId) from CustomerAlertTypeMapping where EMS6AlertId=%s", EMS6Id)
  #                      EMS7CustomerId=self.__EMS7Cursor.fetchone()
#			self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=%s",EMS7CustomerId)
#			self.__EMS7Connection.commit()
#	                self.__updateMigrationQueue(MigrationId)

	def __migrateBatteryInfo(self,Action,IsProcessed,MigrationId,MultiKeyId):
		# print " .....migrating battery-info records....."
		try:
			# print " -- Action %s" %Action
			# print " -- IsProcessed %s" %IsProcessed
			# print " -- MigrationId %s" %MigrationId
			# print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DateField= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccess.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				#EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				EMS6BatteryInfos=self.__EMS6DataAccessInsert.getBatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccessInsert.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId = row[0]
					self.__EMS7Cursor.execute("delete from POSBatteryInfo where PointOfSaleId=%s and SensorGMT=%s",(PointOfSaleId,DateField))
					self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6BatteryInfo(self,PaystationId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo where PaystationId=%s and DateField=%s",(PaystationId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateEventLogNew(self,Action,IsProcessed,MigrationId,MultiKeyId):
		# print " ....Migrating eventLogNew records...."
		try:
			# print " -- Action %s" %Action
			# print " -- IsProcessed %s" %IsProcessed
			# print " -- MigrationId %s" %MigrationId
			# print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DeviceId = Split[1]
			TypeId = Split[2]
			ActionId = Split[3]
			DateField = Split[4]

			if (Action=='Update' and IsProcessed==0):
				EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccess.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				#EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				EMS6EventLogNews=self.__EMS6DataAccessInsert.getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccessInsert.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
		
	def __getEventLogNew(self, PaystationId,DeviceId,TypeId,ActionId,DateField):
		# JIRA 4582 (Oct 30)
		#self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,ClearDateField from EventLogNew where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",(PaystationId,DeviceId,TypeId,ActionId,DateField))
		self.__EMS6Cursor.execute("select A.PaystationId,A.DeviceId,A.TypeId,A.ActionId,A.DateField,A.Information,A.ClearUserAccountId,A.ClearDateField from EventLogNew A, Paystation B where A.PaystationId = B.Id and A.PaystationId=%s and A.DeviceId=%s and A.TypeId=%s and A.ActionId=%s and A.DateField=%s and B.CommAddress not like %s",(PaystationId,DeviceId,TypeId,ActionId,DateField,'9%'))
		return self.__EMS6Cursor.fetchall()

	def __migrateServiceState(self,Action,IsProcessed,MigrationId,EMS6Id):
		# print " ..migrating service state record...."
		# print " -- Action %s" %Action
		# print " -- IsProcessed %s" %IsProcessed
		# print " -- MigrationId %s" %MigrationId
		# print " -- EMS6Id %s" %EMS6Id
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				#EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				EMS6ServiceStates=self.__EMS6DataAccessInsert.getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccessInsert.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				# print " ......deleting Service State record....."
				self.__EMS7Cursor.execute("delete from POSServiceState where PointOfSaleId=(select po.Id from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)

	def __getEMS6ServiceState(self,EMS6Id):
		ServiceState=None
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			ServiceState=self.__EMS6Cursor.fetchall()
		return ServiceState

	def __migrateRawSensorData(self, Action, IsProcessed, MigrationId, MultiKeyId):
		# print "migrating RawSensordata Records"
		try:
			# print "inside try"
			self.__EMS7DataAccessInsert.addRawSensorDataToEMS7(MultiKeyId, Action)
			self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
	
	# Added on Jan 6 2014 EMS-4708
	def __migrateBatteryInfo(self, Action, IsProcessed, MigrationId, MultiKeyId):
		# print "migrating BatteryInfo Records"
		try:
			# print "inside try"
			self.__EMS7DataAccessInsert.addRawBatteryInfoToEMS7(MultiKeyId, Action)
			self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
			
	def __getRawSensorData(self, MultiKeyId):
		split = MultiKeyId.split(',')
		ServerName = split[0]
		PaystationCommAddress=split(1)
		Type = split[2]
		Action=split[3]
		Information= split[4]
		DateField=split[5]
		# print "before EMS6 RawsensorData SQL"
		self.__EMS6Cursor.execute("select ServerName, PaystationCommAddress, \
									Type, Action, Information, DateField, LastRetryTime, RetryCount\
							 from RawSensorData where ServerName = %s  and PaystationCommAddress =  %s  \
							and Type,= %s and  Action =  %s and Information= %s and DateField= %s",\
							(ServerName,PaystationCommAddress,Type,Action,Information,DateField))
		return self.__EMS6Cursor.fetchall()
	
	def __migratePaystationHeartBeat(self, Action, EMS6Id, IsProcessed, MigrationId):
		# print " in migrating PaystationHeartBeat"
		try:
			#paySationHeartBeat = self.__getPaystationHeartBeat(EMS6Id)
			paySationHeartBeat =self.__EMS6DataAccessInsert.getEMS6PaystationHeartBeat(EMS6Id)
			for psHeartBeat in paySationHeartBeat:
				if (Action =='Insert'  and IsProcessed==0):
					IsProcessed=1
					self.__EMS7DataAccessInsert.addPOSHeartBeat(psHeartBeat, Action)
					self.__updateMigrationQueue(MigrationId)
				elif ((Action =='Update' or Action=='Delete')  and IsProcessed==0):
					IsProcessed=1
					self.__EMS7DataAccess.addPOSHeartBeat(psHeartBeat, Action)
					self.__updateMigrationQueue(MigrationId)
		except (Exception, mdb.Error), e:
			self.__EMS6DataAccess.logIncrementalMigrationError(e,MigrationId)
				
	
	
	def IncrementalMigration(self):
		
		"""Create a MigrationProcessLog record"""
		# Added on Jan 10
		self.__EMS6InsertCursor.execute("insert ignore into INCScriptVersion(ModuleName,BuildVersion,LastRunDate) VALUES ('SensorData', '5', now() ) ON DUPLICATE KEY UPDATE LastRunDate=now(), BuildVersion='5'")
		EMS6Connection.commit()
		
		# print " ....crating a migrationProcessingLog Record......."
		
		self.__EMS6Cursor.execute("select count(*) from MigrationProcessLog  where ProcessType = 'Incremental Migration Sensor Data' and EndTime is null")
		processCount = self.__EMS6Cursor.fetchone()
		# print "processCount id:",processCount
		if (processCount[0]>=1):
			self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime, EndTime, RecordSelectedCount, RecordProcessedCount, Error)\
                                                       values ('Incremental Migration Sensor Data',now(), now(), 0, 0,'Too Many Processes Running')")
			EMS6Connection.commit()
			return
		
		# print "....Creating MigrationProcessLog record..."
		self.__EMS6InsertCursor.execute("insert into MigrationProcessLog(ProcessType,StartTime) values ('Incremental Migration Sensor Data',now())")
		EMS6Connection.commit()
		self.__processId= self.__EMS6InsertCursor.lastrowid
		
		# print "Created migration process log record with id:..", self.__processId
		
		# print ".,,selecting record from migration queue....."
		
		self.__EMS7Cursor.execute("select value from EmsProperties where name = 'IncrementalMigrationLimit'")
		MigrationQueueRecordLimit = self.__EMS7Cursor.fetchone()
		
		# print "MigrationQueueRecordLimit is :",MigrationQueueRecordLimit
		
		# Code written on Sep 23
		self.__EMS6Cursor.execute("select LowerLimit,UpperLimit,now() from MigrationQueueLimit where ModuleName='SensorData' order by Id desc Limit 1")
		MigrationQueueLimit=self.__EMS6Cursor.fetchone()
		# print "MigrationQueueLimit[0] is ", MigrationQueueLimit[0]
		# print "MigrationQueueLimit[1] is ", MigrationQueueLimit[1]
		# print "Date now() is ", MigrationQueueLimit[2]
		MigrationQueueLowerLimit = MigrationQueueLimit[0]
		MigrationQueueUpperLimit = MigrationQueueLimit[1]
		
		self.__EMS6Cursor.execute("select * FROM MigrationQueue where Id between '%d' and '%d' and TableName in ('RawSensorData','ServiceState','PaystationHeartBeat','BatteryInfo') \
			and SelectedForMigration = '%d' and SelectedByProcessId='%d' and IsProcessed ='%d' order by Id" % (MigrationQueueLimit[0],MigrationQueueLimit[1],0,0,0))
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		# print " --number of records selected for migration is:..", len(MigrationQueue)
		
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set RecordSelectedCount = "%d" where id = "%d"' % (len(MigrationQueue),self.__processId ))
		
		""" mark the selected record from the migration queue table as selected for processing"""
		# print "----Flagging the selected record as selected for processing....."
		for rec in MigrationQueue:
			self.__EMS6InsertCursor.execute("update MigrationQueue set SelectedForMigration ='%d' , SelectedByProcessId = '%d' where Id = '%d' " % (1, self.__processId, rec[0]))
			
		EMS6Connection.commit()
		
		error ="None"
		
	#	input = raw_input("Start Migration  ........................... y/n ?   :   ")
	#	if (input=='y') :
		for migration in MigrationQueue:
			MigrationId = migration[0]
			MultiKeyId = migration[1]
			EMS6Id = migration[2]
			TableName = migration[3]
			Action = migration[4]
			IsProcessed = migration[5]
			Date = migration[6]
			
			''' Begin Transaction '''
			# print " ----starting incremental data migration....."
			
			if(IsProcessed==0):
				if(TableName=='ServiceState'):
					self.__migrateServiceState(Action,IsProcessed,MigrationId,EMS6Id)
					
				if(TableName=='EventLogNew'):
					self.__migrateEventLogNew(Action,IsProcessed,MigrationId,MultiKeyId)
					
				if(TableName=='PaystationHeartBeat'):
					self.__migratePaystationHeartBeat(Action, EMS6Id, IsProcessed, MigrationId)
					
				if ( TableName=='RawSensorData'):
					self.__migrateRawSensorData(Action, IsProcessed, MigrationId, MultiKeyId)
				# Added on Jan 6 2014 EMS-4708
				if ( TableName=='BatteryInfo'):
					self.__migrateBatteryInfo(Action, IsProcessed, MigrationId, MultiKeyId)
					
			""" Updete the process log table"""		
						
		self.__EMS6InsertCursor.execute('update MigrationProcessLog set EndTime = now(), RecordProcessedCount = "%d", Error= "%s" where id = "%d"' % (len(MigrationQueue),error, self.__processId ))
		EMS6Connection.commit()
		#Code added on Sep 23
		self.__EMS6Cursor.execute("select max(id),now() from MigrationQueue")
		MigrationQueueMaxId=self.__EMS6Cursor.fetchone()
		
		MigrationQueueCurrentMaxId = MigrationQueueMaxId[0]
		# print "$$ MigrationQueueCurrentMaxId is ",MigrationQueueCurrentMaxId
		# print "$$ MigrationQueueUpperLimit is ",MigrationQueueUpperLimit
		# print "$$ MigrationQueueRecordLimit is ",MigrationQueueRecordLimit[0]
		
		MigrationQueueUpperLimitNew = int(MigrationQueueUpperLimit) + int(MigrationQueueRecordLimit[0])
		
		# print "$$ MigrationQueueUpperLimitNew", MigrationQueueUpperLimitNew
		
		if (MigrationQueueUpperLimitNew < MigrationQueueCurrentMaxId):
			# print " $$ Inside If"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='SensorData'",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('SensorData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueUpperLimitNew,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()
			
		else:
			# print " $$ Inside Else"
			#self.__EMS6InsertCursor.execute("update MigrationQueueLimit set LowerLimit = UpperLimit, UpperLimit = %s, MaxId=%s where ModuleName='SensorData'",(MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			self.__EMS6InsertCursor.execute(" insert into MigrationQueueLimit (ModuleName,LowerLimit,UpperLimit,MaxId,CreatedTime) \
				values('SensorData',%s,%s,%s,now())",(MigrationQueueUpperLimit,MigrationQueueCurrentMaxId,MigrationQueueCurrentMaxId))
			EMS6Connection.commit()		
		
EMS6Connection = None
EMS7Connection = None
try:
	# print " .....getting EMS6Connection from DB......."
	
	EMS6Connection = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'ems_db_6_3_14')
	
	
	# print " ****.....Got EMS6Connection from DB......."
	
	# print " .....getting Cerberus connetion  from DB..This is a substitute connection....."
	
	EMS6ConnectionCerberus = mdb.connect('172.30.5.84', 'ltikhova', 'Pass.word', 'cerberus_db')
	
	
	# if (EMS6ConnectionCerberus):
		# print " ***Got Cerberus Connection from DB......."
	
	# print " .....getting EMS7 Connection from DB......."
	
	EMS7Connection = mdb.connect('172.30.5.184', 'IncMigration_reg','IncMigration_reg','ems7')
	#EMS7Connection = mdb.connect('172.16.1.64', 'root','root123','ems7_QA_July21')
	
	
	# print " .....Got  EMS7Connection from DB......."
	
	credentials = pika.PlainCredentials('admin', 'Password1$')
	parameters = pika.ConnectionParameters('172.30.5.203',
                                       5672,
                                       '/',
                                       credentials)
	MQconnection = pika.BlockingConnection(parameters)
	channel = MQconnection.channel()
	
	
	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS6DataAccessInsert(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), EMS7DataAccessInsert(EMS7Connection,channel, 1), 1)
	
	startMigration.IncrementalMigration()
except (Exception, mdb.Error), e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
	if EMS6Connection:
		EMS6Connection.close()
	if EMS7Connection:
		EMS7Connection.close()
	if channel:
		channel.close()	