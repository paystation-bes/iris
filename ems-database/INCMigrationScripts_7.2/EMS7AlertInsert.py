#!/usr/bin/env python

from datetime import date
from datetime import datetime
import MySQLdb as mdb
import time
import re
import urllib
import pika

class EMS7AlertInsert:
	def __init__(self, EMS7Connection, verbose):
		self.__verbose = verbose
		self.__EMS7Connection = EMS7Connection
		self.__EMS7Cursor = EMS7Connection.cursor()

	def addPOSAlertToEMS7Queue(self, EventLogNew, Action):
		tableName='EventLogNew'
		# print "In POSAlert module"
		MultiKeyId=''
		IsActive = 0
		IsSentEmail = 1
		EMS6PaystationId = EventLogNew[0]
		DeviceId = EventLogNew[1]
		TypeId = EventLogNew[2]
		ActionId = EventLogNew[3]
		DateField = EventLogNew[4]
		Information = EventLogNew[5]
		ClearUserAccountID = EventLogNew[6]
		ClearDateField = EventLogNew[7]
		CreatedGMT = date.today()
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		# self.__printEventLogNew(EventLogNew)
		MaxDate = None
		# print "$$ DeviceId is:",DeviceId
		# Code added on Sep 18
		# print "$$ ActionId is: ",ActionId
		if(ActionId==1):
			IsActive = 1
			# print "$$ IsActive is set to 1"
			
		if(ActionId==2):
			IsActive = 0
			# print "$$ IsActive is set to 2"
		
		# Added on Dec 08 2014 EMS 5906
		# Get the PaystationType from PointOfSale
		EventDeviceTypeId=DeviceId
		# print "*** EventDeviceTypeId is :",EventDeviceTypeId
		self.__EMS7Cursor.execute("select PaystationType_Id from PointOfSale_v where PointOfSale_Id = %s ",(PointOfSaleId))
		if(self.__EMS7Cursor.rowcount<>0):
			PaystationTypeId=self.__EMS7Cursor.fetchone()
			# print "*** PaystationTypeId is :",PaystationTypeId[0]
		
		if(PaystationTypeId[0]==1) and (EventDeviceTypeId in (7,8,19)):
			Action = 'Nothing'
			# print "*** Action for PaystationTypeId =1 is :",Action
		if(PaystationTypeId[0]==2) and (EventDeviceTypeId in (19)):
			Action = 'Nothing'
			# print "*** Action for PaystationTypeId =2 is :",Action
		if(PaystationTypeId[0]==3) and (EventDeviceTypeId in (7,8,19)):
			Action = 'Nothing'
			# print "*** Action for PaystationTypeId =3 is :",Action
		if(PaystationTypeId[0]==5) and (EventDeviceTypeId in (6,7,8)):
			Action = 'Nothing'
			# print "*** Action for PaystationTypeId =5 is :",Action			
		
		# print " Action before insert is :", Action
		if(Action=='Insert'):
			#if (DeviceId==20):
			# these are user defined alerts
			#	EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(TypeId)
			#	EventStatusTypeId=23
			#	EventSeverityTypeId = self.__getEventSeverityTypeId(DeviceId,EventStatusTypeId,ActionId)
			#	if (EMS7CustomerAlertTypeId and PointOfSaleId):
			#		self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,DeviceId,EventStatusTypeId,ActionId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
			#		EMS7POSAlertId = self.__EMS7Cursor.lastrowid
			#		self.__EMS7Cursor.execute("insert into EventLogNewMapping(PaystationId,DeviceId,TypeId,ActionId,DateField,POSAlertId) values(%s,%s,%s,%s,%s,%s)",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField,EMS7POSAlertId))
			#		self.__EMS7Connection.commit()
			#	else:
			#		IsMultiKey=1
			#		MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
			#		MultiKeyId = ','.join(map(str,MultiKeyId))
			#		self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			if (DeviceId!=20):
			# These are Paystation Alert, EMS6.EventNewLog.TypeId=EMS6.EventNewType.Id. EMS6.EventNewType=EMS7.EventStatusType
			# EMS7StatusTypeId = self.__getEventStatusTypeId(TypeId)
				PaystationAlertTypeId=12
				EventStatusTypeId=TypeId
				EventActionTypeId=ActionId
				EventDeviceTypeId=DeviceId
				# print "$$ Getting EventSeverityTypeId"
				EventSeverityTypeId=self.__getEventSeverityTypeId(EventDeviceTypeId,EventStatusTypeId,EventActionTypeId)
				# print "$$ EventSeverityTypeId is:",EventSeverityTypeId
				EMS7CustPayId=self.__getCustomerIdFromPointOfSale(EMS6PaystationId)
				if(EMS7CustPayId):
					for CustomerId in EMS7CustPayId:
					#	print " >> CustomerId in EMS7 %s" %CustomerId
					#	print " >> TypeId %s" %TypeId
						EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeIdForPaystationAlert(CustomerId,PaystationAlertTypeId)
						# print "$$ Getting EMS7CustomerAlertTypeId"
						# print "$$ EMS7CustomerAlertTypeId is ",EMS7CustomerAlertTypeId
						# print "$$ PointOfSaleId is ",PointOfSaleId
						# print "$$ EventDeviceTypeId is",EventDeviceTypeId
						# print "$$ EventStatusTypeId is", EventStatusTypeId
						# print "$$ EventActionTypeId is",EventActionTypeId
						# Oct 30 JIRA 4437
						if(EMS7CustomerAlertTypeId and PointOfSaleId):  #and (EventDeviceTypeId !=(2,3,4,5,6,7,8,16,19,22) and EventStatusTypeId !=0 and EventActionTypeId !=2)):
							# print "$$ Inside if EMS7DataAccessInsert JIRA 4437"
							
							if (PointOfSaleId):
								self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,\
									EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) \
									values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
									(EMS7CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,\
									EventSeverityTypeId,DateField,Information,1,IsSentEmail,ClearDateField,\
									ClearUserAccountID,CreatedGMT))
								EMS7POSAlertId = self.__EMS7Cursor.lastrowid
								self.__EMS7Cursor.execute("insert into EventLogNewMapping(PaystationId,DeviceId,TypeId,ActionId,DateField,POSAlertId) \
									values(%s,%s,%s,%s,%s,%s)",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField,EMS7POSAlertId))
								self.__EMS7Connection.commit()
								if (EventActionTypeId in (1,3,5,6)):
									if(EventDeviceTypeId==13) and (EventStatusTypeId==0) and (EventActionTypeId==1):
										# print "$$ Printer present on"
										self.__EMS7Connection.commit()
									else:
										self.__EMS7Cursor.execute("insert into ActivePOSAlert (CustomerAlertTypeId,PointOfSaleId,POSAlertId,EventDeviceTypeId,\
											EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,\
											ClearedGMT,AlertInfo,CreatedGMT) \
											values ( %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE POSAlertId=%s,IsActive=1,AlertGMT=%s,ClearedGMT=NULL",\
											(EMS7CustomerAlertTypeId,PointOfSaleId,EMS7POSAlertId,EventDeviceTypeId, \
											EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, 1,DateField, \
											ClearDateField,Information,CreatedGMT,EMS7POSAlertId,DateField)) 
										# JIRA 4760 Dec 6
										sqlproc = "call sp_UpdatePaystationAlertCount(%s)" % PointOfSaleId
										self.__EMS7Cursor.execute(sqlproc)
										# print "sp_UpdatePaystationAlertCount executed in EventActionTypeId in (1,3,5,6)"
										
										self.__EMS7Connection.commit()
								elif(EventActionTypeId in (2,4,7)):
									self.__EMS7Cursor.execute("select max(AlertGMT) from POSAlert use index (ind_POSAlert_mult1) where PointOfSaleId = %s and EventDeviceTypeId =%s and EventStatusTypeId=%s and EventActionTypeId in (1,3,5,6)",\
										(PointOfSaleId,EventDeviceTypeId,EventStatusTypeId))
									if(self.__EMS7Cursor.rowcount<>0):
										MaxDate=self.__EMS7Cursor.fetchone()
										# print "$$ MaxDate is:",MaxDate
										self.__EMS7Cursor.execute("select Id from POSAlert where PointOfSaleId = %s and EventDeviceTypeId =%s and EventStatusTypeId=%s and \
											AlertGMT = %s",(PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,MaxDate[0]))
										if(self.__EMS7Cursor.rowcount<>0):	
											POSAlertId=self.__EMS7Cursor.fetchone()
											# print "$$ POSAlertId is :",POSAlertId
											# print "$$ POSAlertId[0] is",POSAlertId[0]
										
											# print "$$ Before Insert"
											self.__EMS7Cursor.execute("update ActivePOSAlert set IsActive=0,ClearedGMT=%s where POSAlertId=%s",(DateField,POSAlertId[0]))
											# JIRA 4760 Dec 6
											sqlproc = "call sp_UpdatePaystationAlertCount(%s)" % PointOfSaleId
											self.__EMS7Cursor.execute(sqlproc)
											# print "sp_UpdatePaystationAlertCount executed in EventActionTypeId in (2,4,7)"
											self.__EMS7Cursor.execute("update POSAlert set IsActive=0,ClearedGMT=%s,ClearedByUserId=1 where \
												PointOfSaleId = %s and EventDeviceTypeId =%s and EventStatusTypeId=%s and isActive= 1", \
												(DateField,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId))
											self.__EMS7Connection.commit()
											# AlertGMT = %s ,MaxDate[0])
									if((EventDeviceTypeId == 13) and (EventStatusTypeId == 13)):
										# print "$$ For Printer Clear"
									
										self.__EMS7Cursor.execute("update ActivePOSAlert set IsActive=0,ClearedGMT=%s where PointOfSaleId=%s and EventDeviceTypeId=13 and \
											EventStatusTypeId=13 and IsActive=1",(DateField,PointOfSaleId))
										# JIRA 4760 Dec 6
										sqlproc = "call sp_UpdatePaystationAlertCount(%s)" % PointOfSaleId
										self.__EMS7Cursor.execute(sqlproc)
										# print "sp_UpdatePaystationAlertCount executed in EventActionTypeId in (2,4,7)"
										
										self.__EMS7Connection.commit()
										# print "$$ Printer Clear set normal"
									if((EventDeviceTypeId == 6) and (EventStatusTypeId == 3)):
										# print "$$ For CoinChanger Clear"
										self.__EMS7Cursor.execute("update ActivePOSAlert set IsActive=0,ClearedGMT=%s where PointOfSaleId=%s and EventDeviceTypeId=6 and \
											EventStatusTypeId=3 and IsActive=1",(DateField,PointOfSaleId))	
										# JIRA 4760 Dec 6
										sqlproc = "call sp_UpdatePaystationAlertCount(%s)" % PointOfSaleId
										self.__EMS7Cursor.execute(sqlproc)
										# print "sp_UpdatePaystationAlertCount executed For CoinChanger Clear"
										
										self.__EMS7Connection.commit()
										# print "$$ CoinChanger set normal"
						
						# Modules NOT Present						
						
						if((EventDeviceTypeId==2) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Bill Acceptor - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsBillAcceptor = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
								
						if((EventDeviceTypeId==3) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Bill Stacker - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsBillStacker = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()	
								
						if((EventDeviceTypeId==4) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Card Reader - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCardReader = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==5) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Acceptor - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinAcceptor = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==6) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Changer - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinChanger = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
								
						if((EventDeviceTypeId==7) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Hopper 1 - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinHopper1 = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==8) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Hopper 2 - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinHopper2 = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==16) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Canister - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinCanister = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==19) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Coin Escrow - Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinEscrow = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==22) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#RfidCardReader - Not Present
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsRfidCardReader = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
						
						if((EventDeviceTypeId==13) and (EventStatusTypeId==0) and (EventActionTypeId == 2)):
							#Printer Present - Off
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsPrinter = 0,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
							
						# Modules Present
						
						if((EventDeviceTypeId==2) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Bill Acceptor - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsBillAcceptor = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
								
						if((EventDeviceTypeId==3) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Bill Stacker - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsBillStacker = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()	
								
						if((EventDeviceTypeId==4) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Card Reader - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCardReader = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==5) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Acceptor - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinAcceptor = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==6) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Changer - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinChanger = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
								
						if((EventDeviceTypeId==7) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Hopper 1 - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinHopper1 = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==8) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Hopper 2 - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinHopper2 = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==16) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Canister - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinCanister = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==19) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Coin Escrow - Present - On
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsCoinEscrow = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
									
						if((EventDeviceTypeId==22) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#RfidCardReader - Present
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsRfidCardReader = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
							
						if((EventDeviceTypeId==13) and (EventStatusTypeId==0) and (EventActionTypeId == 1)):
							#Printer - Present
							self.__EMS7Cursor.execute("UPDATE POSServiceState SET IsPrinter = 1,LastModifiedGMT=UTC_TIMESTAMP() WHERE PointOfSaleId=%s",(PointOfSaleId))
							self.__EMS7Connection.commit()
						else:
							IsMultiKey=1
							MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
							MultiKeyId = ','.join(map(str,MultiKeyId))
							self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		if(Action=='Update'):
			if (DeviceId==20):
			# these are user defined alerts
				self.__EMS7Cursor.execute("select distinct(POSAlertId) where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField))
				if(self.__EMS7Cursor.rowcount<>0):
					EMS7POSAlertId = self.__EMS7Cursor.fetchone()
					EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(TypeId)
					EventStatusTypeId=23
					EventSeverityTypeId = self.__getEventSeverityTypeId(DeviceId,EventStatusTypeId,ActionId)
					if (EMS7CustomerAlertTypeId and PointOfSaleId):
						self.__EMS7Cursor.execute("update POSAlert set CustomerAlertTypeId=%s,PointOfSaleId=%s,EventDeviceTypeId=%s,EventStatusTypeId=%s,EventActionTypeId=%s,EventSeverityTypeId=%s,AlertGMT=%s,AlertInfo=%s,IsActive=%s,IsSentEmail=%s,ClearedGMT=%s,ClearedByUserId=%s,CreatedGMT=%s where Id=%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,DeviceId,EventStatusTypeId,ActionId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT,EMS7POSAlertId))
						self.__EMS7Connection.commit()
					else:
						IsMultiKey=1
						MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
						MultiKeyId = ','.join(map(str,MultiKeyId))
						self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
				elif (DeviceId!=20):
				# These are Paystation Alert, EMS6.EventNewLog.TypeId=EMS6.EventNewType.Id. EMS6.EventNewType=EMS7.EventStatusType
				# EMS7StatusTypeId = self.__getEventStatusTypeId(TypeId)
					PaystationAlertTypeId=12
					EventStatusTypeId=TypeId
					EventActionTypeId=ActionId
					EventDeviceTypeId=DeviceId
					EventSeverityTypeId=self.__getEventSeverityTypeId(EventDeviceTypeId,EventStatusTypeId,EventActionTypeId)
					EMS7CustPayId=self.__getCustomerIdFromPointOfSale(EMS6PaystationId)
					if(EMS7CustPayId):
						for CustomerId in EMS7CustPayId:
						#	print " >> CustomerId in EMS7 %s" %CustomerId
						#	print " >> TypeId %s" %TypeId
							EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeIdForPaystationAlert(CustomerId,PaystationAlertTypeId)
							if(EMS7CustomerAlertTypeId and PointOfSaleId):
								self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,\
									EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) \
									values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
									(EMS7CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,\
									EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,\
									ClearUserAccountID,CreatedGMT))
								self.__EMS7Connection.commit()
							else:
								IsMultiKey=1
								MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
								MultiKeyId = ','.join(map(str,MultiKeyId))
								self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

		if(Action=='Nothing'):
			print "In Nothing"
			
		
credentials = pika.PlainCredentials('admin', 'Password1$')
parameters = pika.ConnectionParameters('172.30.5.203',
                                       5672,
                                       '/',
                                       credentials)

MQconnection = pika.BlockingConnection(parameters)
channel = MQconnection.channel()
channel.basic_publish(exchange='123',
                      routing_key='',
                      body='Hello World!232333')
channel.close()					  