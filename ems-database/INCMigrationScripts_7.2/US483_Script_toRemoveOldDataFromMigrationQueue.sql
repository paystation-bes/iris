
-- To remove data from MigrationQueue older than 30 days ( only sucessful records that are migrated to Iris are removed)

DROP PROCEDURE IF EXISTS sp_Delete_MigrationQueue;

delimiter //

CREATE PROCEDURE sp_Delete_MigrationQueue(in p_Limit INT)

 BEGIN


REPEAT 

START TRANSACTION ;

 DELETE from MigrationQueue where CreatedDate < date_sub(date(now()), interval 30 day) and IsProcessed = 1 LIMIT p_Limit ;

UNTIL ROW_COUNT() = 0 END REPEAT;

COMMIT;

END//
delimiter ;


-- To remove data from MigrationProcessLog older than 30 days 

DROP PROCEDURE IF EXISTS sp_Delete_MigrationProcessLog;

delimiter //

CREATE PROCEDURE sp_Delete_MigrationProcessLog(in p_Limit INT)

 BEGIN


REPEAT 

START TRANSACTION ;

 DELETE from MigrationProcessLog where StartTime < date_sub(date(now()), interval 30 day) LIMIT p_Limit ;

UNTIL ROW_COUNT() = 0 END REPEAT;

COMMIT;

END//
delimiter ;