-- Update for Elavon ViaConex SHA-256 url
UPDATE Processor SET TestUrl='https://certgate.viaconex.com/cgi-bin/encompass4.cgi', ProductionUrl='https://prodgate.viaconex.com/cgi-bin/encompass4.cgi' WHERE Id=19 AND Name='processor.elavon.viaconex';
