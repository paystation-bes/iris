
-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateAccount`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateAccount`(
    IN p_CustomerName VARCHAR(25)
)
BEGIN

	DECLARE v_CustomerId MEDIUMINT; -- = sprintf("%04d",(Select Id from Customer where Name = p_CustomerName));
	DECLARE v_TimeZone VARCHAR(20);

    START TRANSACTION;

    -- add check for pay station serial number and customer existance


    --
    -- Create Customer
    --
	SET v_TimeZone = 'Canada/Pacific';
    SET v_CustomerId = 0;
    CALL sp_Preview_InsertCustomer
    ( 
          3,    -- p_CustomerTypeId
          1,    -- p_CustomerStatusTypeId
          NULL, -- p_ParentCustomerId
          p_CustomerName, -- p_CustomerName
          NULL, -- p_TrialExpiryGMT
          0,    -- p_IsParent
          CONCAT('admin%40', p_CustomerName), -- p_UserName
          '58ae83e00383273590f96b385ddd700802c3f07d', -- p_Password
          1,    -- p_IsStandardReports
          0,    -- p_IsAlerts
          0,    -- p_IsRealTimeCC
          0,    -- p_IsBatchCC
          0,    -- p_IsRealTimeValue
          0,    -- p_IsCoupons
          0,    -- p_IsValueCards
          0,    -- p_IsSmartCards
          0,    -- p_IsExtendByPhone
          1,    -- p_IsDigitalAPIRead
          1,    -- p_IsDigitalAPIWrite
          0,    -- p_Is3rdPartyPayByCell
          0,    -- p_IsDigitalCollect
          0,    -- p_IsDigitalMaintain
          0,    -- p_IsDigitalPatrol
          1,    -- p_IsOnlineRate
          v_TimeZone,    -- p_Timezone
          1,    -- p_UserId
          'SaltSaltSaltSalt', -- p_PasswordSalt
		  v_CustomerId
    );
	SELECT v_CustomerId;

    --
    -- Locations
    --
	CALL sp_Preview_CreateLocation(p_CustomerName, 'Downtown'	,'Pay-By-Plate',	0, NULL, 1, 150, 1000000);
    CALL sp_Preview_CreateLocation(p_CustomerName, 'Airport'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000);
		
    --
    -- Pay Stations
    --
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',	CONCAT('3000',LPAD(v_CustomerId,4,'0'), '0001'), 1, v_TimeZone,49.28600334159728,-123.12086105346685);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('3000',LPAD(v_CustomerId,4,'0'), '0002'), 1, v_TimeZone,49.28348390950996,-123.11614036560059);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('3000',LPAD(v_CustomerId,4,'0'), '0003'), 1, v_TimeZone,49.28085236522246,-123.12223434448242);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('3000',LPAD(v_CustomerId,4,'0'), '0004'), 1, v_TimeZone,49.19380640706086,-123.17587852478026);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('3000',LPAD(v_CustomerId,4,'0'), '0005'), 1, v_TimeZone,49.19304917865785,-123.17761659622194);

    COMMIT;

END$$
DELIMITER ;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertCustomer`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_InsertCustomer`(
	IN p_CustomerTypeId TINYINT UNSIGNED, 
	IN p_CustomerStatusTypeId TINYINT UNSIGNED, 
	IN p_ParentCustomerId MEDIUMINT UNSIGNED, 
	IN p_CustomerName VARCHAR(25), 
	IN p_TrialExpiryGMT DATETIME, 
	IN p_IsParent TINYINT(1) UNSIGNED, 
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_IsStandardReports TINYINT(1), 
	IN p_IsAlerts TINYINT(1),
	IN p_IsRealTimeCC TINYINT(1), 
	IN p_IsBatchCC TINYINT(1), 
	IN p_IsRealTimeValue TINYINT(1),
	IN p_IsCoupons TINYINT(1), 
	IN p_IsValueCards TINYINT(1), 
	IN p_IsSmartCards TINYINT(1), 
	IN p_IsExtendByPhone TINYINT(1), 
	IN p_IsDigitalAPIRead TINYINT(1), 
	IN p_IsDigitalAPIWrite TINYINT(1),
	IN p_Is3rdPartyPayByCell TINYINT(1),
	IN p_IsDigitalCollect TINYINT(1),
	IN p_IsDigitalMaintain TINYINT(1),
	IN p_IsDigitalPatrol TINYINT(1),
	IN p_IsOnlineConfiguration TINYINT(1),
	IN p_IsFlexIntegration TINYINT(1),	
	IN p_IsCaseIntegration TINYINT(1),	
	IN p_IsDigitalAPIXChange TINYINT(1),	
	IN p_Timezone VARCHAR(40),
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16), 
	OUT v_CustomerId MEDIUMINT
	)
BEGIN
        
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     
    
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
        
    START TRANSACTION;
        
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP());
        
    SELECT LAST_INSERT_ID() INTO v_CustomerId;    
    
	UPDATE Customer SET UnifiId = v_CustomerId WHERE Id = v_CustomerId;
	
	IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
		INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (1,	 v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);
    ELSE
		INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);
	END IF ;
    
    SELECT LAST_INSERT_ID() INTO v_UserAccountId;
        
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   
        
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);
      

        -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    	INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES          (v_UserAccountId, 				 3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    	-- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    	INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES              (v_CustomerId, 				  3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
    	SELECT 			  v_CustomerId,	UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
    	FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
    	
    	INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
    	FROM UserRole ur 
    	INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    	
    	INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM CustomerRole cr
		INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
		WHERE r.RoleStatusTypeId != 2
		AND r.CustomerTypeId = 3
		AND cr.CustomerId = p_ParentCustomerId;

		INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
		SELECT 							      ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM UserAccount ua 
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    
    END IF;
    
    IF p_CustomerTypeId = 3 THEN
    
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
              
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      11,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Preferred
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      12,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Limited
   

        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                    		0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
            
        SELECT LAST_INSERT_ID() INTO v_LocationId;
                
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
                    
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
            
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
        
    ELSE         
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   
    END IF;
    
    
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
            
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,    0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1400, p_IsDigitalMaintain,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1500, p_IsDigitalPatrol,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration,     0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration,     0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration,     0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange,   0, UTC_TIMESTAMP(), p_UserId);
               
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'Communication Alert Default',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,          		  12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;        
    END IF;
    
    COMMIT;     
	
END$$
DELIMITER ;



-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateLocation`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateLocation`(
    IN p_CustomerName VARCHAR(25),
    IN p_LocationName VARCHAR(25),
    IN p_Description VARCHAR(80),
    IN p_IsParent TINYINT UNSIGNED,
    IN p_ParentLocation VARCHAR(25),
    IN p_UserId MEDIUMINT UNSIGNED,
	IN p_NumberOfSpaces MEDIUMINT UNSIGNED,
	IN p_TargetMonthlyRevenueAmount MEDIUMINT UNSIGNED
)
BEGIN
    DECLARE v_CustomerId           INT UNSIGNED;
    DECLARE v_ParentLocationId     INT UNSIGNED;
    DECLARE v_NewLocationId        INT UNSIGNED;

    SET v_CustomerId = (SELECT Id From Customer WHERE Name=p_CustomerName);
    
    IF p_ParentLocation = NULL THEN
        SET v_ParentLocationId = NULL;
    ELSE
        SET v_ParentLocationId = (SELECT l.Id From Location l WHERE CustomerId=v_CustomerId AND Name=p_ParentLocation AND IsDeleted=0);
    END IF;

    INSERT INTO `Location`
    (   
        `CustomerId`, 
        `ParentLocationId`,
        `Name`,
        `NumberOfSpaces`,
        `TargetMonthlyRevenueAmount`,
        `Description`,
        `IsParent`,
        `IsUnassigned`,
        `IsDeleted`,
        `VERSION`,
        `LastModifiedGMT`,
        `LastModifiedByUserId`
    )
    VALUES
    (
        v_CustomerId,
        v_ParentLocationId,
        p_LocationName,
        p_NumberOfSpaces,
        p_TargetMonthlyRevenueAmount,
        p_Description,
        p_IsParent,
        0,
        0,
        0,
        UTC_TIMESTAMP(),
        1
    );    
    
    IF p_IsParent = 0 THEN
        
        SET v_NewLocationId = (SELECT l.Id From Location l WHERE CustomerId=v_CustomerId AND Name=p_LocationName AND IsDeleted=0);
        
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            1, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            2, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            3, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
           v_NewLocationId, -- p_LocationId
            4, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            5, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            6, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            7, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );

    END IF;

END$$
DELIMITER ;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreatePayStation`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreatePayStation`(
    IN p_CustomerName VARCHAR(25),
    IN p_LocationName VARCHAR(25),
    IN p_SerialNumber VARCHAR(20),
    IN p_UserId MEDIUMINT UNSIGNED,
	IN p_TimeZone VARCHAR(20),
    IN p_Latitude DECIMAL(18,14),
    IN p_Longitude DECIMAL(18,14)
)
BEGIN

    DECLARE v_CustomerId            INT UNSIGNED;
    DECLARE v_LocationId            INT UNSIGNED;
    DECLARE v_PayStationType        INT UNSIGNED;    
    DECLARE v_PointOfSaleId         INT UNSIGNED;

    SET v_CustomerId = (SELECT Id From Customer WHERE Name=p_CustomerName);
    SET v_LocationId = (SELECT Id From Location WHERE CustomerId=v_CustomerId AND Name=p_LocationName AND IsDeleted=0);
    
    SET v_PayStationType = ASCII(p_SerialNumber) - ASCII('0');
    
    CALL sp_InsertPointOfSale
    (
        v_CustomerId, -- p_CustomerId
        v_PayStationType, -- p_PaystationTypeId (LUKE)
        1, -- p_ModemTypeId (Unknown)
        p_SerialNumber, -- p_SerialNumber
        UTC_TIMESTAMP(), -- p_ProvisionedGMT
        1, -- p_UserId
        0, -- p_PaystationId_old
        0 -- p_PointOfSaleId_old
    );
    
    SET v_PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNumber=p_SerialNumber);

    UPDATE PointOfSale
    SET
        `LocationId` = v_LocationId,
        `VERSION` = 1,
        `LastModifiedGMT` = UTC_TIMESTAMP(),
		`Latitude` = p_Latitude,
		`Longitude` = p_Longitude
    WHERE Id=v_PointOfSaleId;

    -- Activate the Pay Station
    UPDATE POSStatus
    SET
        `IsActivated` = 1,
        `IsActivatedGMT` = UTC_TIMESTAMP(),
        `VERSION` = 1,
        `LastModifiedGMT` = UTC_TIMESTAMP()
    WHERE PointOfSaleId=v_PointOfSaleId;

	Call sp_Preview_AddPaystationToAutomation(p_SerialNumber, v_CustomerId, SUBSTRING(p_SerialNumber, -4), p_TimeZone);

END$$
DELIMITER ;

-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `sp_Preview_AddPaystationToAutomation`;
DELIMITER $$

CREATE PROCEDURE `sp_Preview_AddPaystationToAutomation` (
	IN p_PaystationId VARCHAR(20),
	IN p_CustomerId MEDIUMINT,
	IN p_PaystationIdentifier VARCHAR(4),
	IN p_TimeZone VARCHAR(20))
BEGIN

	INSERT PreviewPaystation (SerialNumber, CustomerId, PaystationId, TimeZone)
	VALUES 	(P_PaystationId, p_CustomerId, p_PaystationIdentifier, p_TimeZone);

END$$
DELIMITER ;

-- -----------------------------------------------------
-- Trigger `sp_Preview_InsertTransaction`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertTransaction`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertTransaction`(
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20) ,
  IN p_Number INT UNSIGNED ,
  IN p_LotNumber VARCHAR(20) , 
  IN p_LicensePlateNo VARCHAR(15) ,
  IN p_AddTimeNum INT UNSIGNED ,
  IN p_StallNumber MEDIUMINT(8) UNSIGNED ,
  IN p_Type VARCHAR(30) ,
  IN p_ParkingTimePurchased MEDIUMINT ,
  IN p_OriginalAmount MEDIUMINT UNSIGNED ,
  IN p_CashPaid MEDIUMINT UNSIGNED ,
  IN p_CardPaid MEDIUMINT UNSIGNED ,
  IN p_NumberBillsAccepted MEDIUMINT UNSIGNED ,
  IN p_NumberCoinsAccepted MEDIUMINT UNSIGNED ,
  IN p_BillCol MEDIUMINT UNSIGNED ,
  IN p_CoinCol MEDIUMINT UNSIGNED ,
  IN p_CardData VARCHAR(100) ,
  IN p_IsRefundSlip TINYINT(1) UNSIGNED ,
  IN p_RateName varchar(20) ,
  IN p_RateId MEDIUMINT UNSIGNED ,
  IN p_RateValue MEDIUMINT UNSIGNED 
)
BEGIN

	DECLARE v_PreviewTransactionId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewTransaction (Number, LotNumber, LicensePlateNo, AddTimeNum, StallNumber, Type, ParkingTimePurchased, OriginalAmount, CashPaid, CardPaid, NumberBillsAccepted, NumberCoinsAccepted, BillCol, CoinCol, CardData, IsRefundSlip, RateName, RateId, RateValue)
	VALUES (p_Number, p_LotNumber, p_LicensePlateNo, p_AddTimeNum, p_StallNumber, p_Type, p_ParkingTimePurchased, p_OriginalAmount, p_CashPaid, p_CardPaid, p_NumberBillsAccepted, p_NumberCoinsAccepted, p_BillCol, p_CoinCol, p_CardData, p_IsRefundSlip, p_RateName, p_RateId, p_RateValue);

	SELECT last_insert_id() INTO v_PreviewTransactionId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Transaction' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewTransactionId);

END //
DELIMITER ;


-- -----------------------------------------------------
-- Trigger `sp_Preview_InsertEvent`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertEvent`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertEvent`(
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20) ,
  IN p_Type VARCHAR(40) ,
  IN p_Action VARCHAR(40) ,
  IN p_Information VARCHAR(40)
)
BEGIN

	DECLARE v_PreviewEventId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewEvent (Type, Action, Information)
	VALUES (p_Type, p_Action, p_Information);

	SELECT last_insert_id() INTO v_PreviewEventId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Event' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewEventId);

END //
DELIMITER ;


-- -----------------------------------------------------
-- Trigger `sp_Preview_InsertCollection`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertCollection`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertCollection`(
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20),

  IN p_Type VARCHAR(20), -- Coin/Bill
  IN p_LotNumber VARCHAR(20),
  IN p_MachineNumber VARCHAR(4),
  IN p_StartDate MEDIUMINT UNSIGNED ,
  IN p_EndDate MEDIUMINT UNSIGNED ,  
  IN p_StartTransNumber INT UNSIGNED ,
  IN p_EndTransNumber INT UNSIGNED ,  
  IN p_TicketCount MEDIUMINT UNSIGNED ,
  IN p_BillCount50 SMALLINT UNSIGNED ,
  IN p_BillCount20 SMALLINT UNSIGNED ,
  IN p_BillCount10 SMALLINT UNSIGNED ,
  IN p_BillCount05 SMALLINT UNSIGNED ,
  IN p_BillCount02 SMALLINT UNSIGNED ,
  IN p_BillCount01 SMALLINT UNSIGNED ,
  IN p_CoinCount200 SMALLINT UNSIGNED ,
  IN p_CoinCount100 SMALLINT UNSIGNED ,
  IN p_CoinCount025 SMALLINT UNSIGNED ,
  IN p_CoinCount010 SMALLINT UNSIGNED ,
  IN p_CoinCount005 SMALLINT UNSIGNED 
)
BEGIN

	DECLARE v_PreviewCollectionId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewCollection (Type , LotNumber ,MachineNumber ,StartDate ,EndDate , StartTransNumber ,EndTransNumber ,TicketCount ,CoinCount005 ,CoinCount010 ,CoinCount025 ,CoinCount100 ,CoinCount200 ,BillCount01 ,BillCount02 ,BillCount05 ,BillCount10 ,BillCount20 ,BillCount50 )
						VALUES (p_Type , p_LotNumber ,p_MachineNumber ,p_StartDate ,p_EndDate , p_StartTransNumber ,p_EndTransNumber ,p_TicketCount ,p_CoinCount005 ,p_CoinCount010 ,p_CoinCount025 ,p_CoinCount100 ,p_CoinCount200 ,p_BillCount01 ,p_BillCount02 ,p_BillCount05 ,p_BillCount10 ,p_BillCount20 ,p_BillCount50 );

	SELECT last_insert_id() INTO v_PreviewCollectionId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Collection' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewCollectionId);

END //
DELIMITER ;


