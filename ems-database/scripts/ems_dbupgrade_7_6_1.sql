-- -----------------------------------------------------
-- Schema Changes
-- -----------------------------------------------------
ALTER TABLE `SubscriptionType` ADD COLUMN `DisplayOrder` SMALLINT UNSIGNED NOT NULL DEFAULT 1 AFTER `IsPrivate`;

ALTER TABLE `MerchantAccount` ADD COLUMN `TimeZone` VARCHAR(40) NULL AFTER `CloseQuarterOfDay`;

ALTER TABLE `ElavonTransactionDetail` ADD COLUMN `AVSResponse` VARCHAR(1)  NULL AFTER `ApprovalCode`;
ALTER TABLE `ElavonTransactionDetail` ADD COLUMN `CVV2Response` VARCHAR(1)  NULL AFTER `AVSResponse`;
ALTER TABLE `POSStatus` MODIFY `TelemetryReferenceCounter` MEDIUMINT UNSIGNED;


-- -----------------------------------------------------
-- Routine Changes
-- -----------------------------------------------------

-- EMS-11125
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_UpdateElavonPartialBatchClose $$

CREATE PROCEDURE `sp_UpdateElavonPartialBatchClose`()
BEGIN
    
	-- This update set the BatchClose to "PartiallyClose" in ActiveBatchSummary even if there is one Transaction with the status ToBeSettled"
	
	UPDATE ActiveBatchSummary
	SET BatchStatusTypeId = 3
	WHERE (MerchantAccountId, BatchNumber) IN
	(
	SELECT  A.MerchantAccountId, A.BatchNumber FROM ( SELECT DISTINCT ABS.MerchantAccountId, ABS.BatchNumber FROM
	ActiveBatchSummary ABS INNER JOIN ElavonTransactionDetail ETD
	ON ABS.BatchNumber = ETD.BatchNumber AND ABS.MerchantAccountId = ETD.MerchantAccountId
	WHERE ABS.BatchStatusTypeId = 2 AND ETD.TransactionSettlementStatusTypeId = 1
	AND ABS.LastModifiedGMT >= DATE_SUB(date(UTC_TIMESTAMP()),INTERVAL 7 DAY) 
	) A ) ;
	
    
END $$

DELIMITER ;

-- End sp_VerifyElavonPartialBatchClose


-- -----------------------------------------------------
-- Lookup Data
-- -----------------------------------------------------

-- Add to new Permissions
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 203,  200, 'View Maintenance Center',   	                NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 204,  200, 'Clear Active Alerts', 		               	    NULL, 0, UTC_TIMESTAMP(), 1) ;

-- Role: 'System Admin'
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  204, 0, UTC_TIMESTAMP(), 1);

-- Role: 'Child Admin'
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  203, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  204, 0, UTC_TIMESTAMP(), 1);

-- Assign Display Order for Subscription Type
UPDATE `SubscriptionType` SET `DisplayOrder` = 1 WHERE `Id` = 100;
UPDATE `SubscriptionType` SET `DisplayOrder` = 2 WHERE `Id` = 200;
UPDATE `SubscriptionType` SET `DisplayOrder` = 3 WHERE `Id` = 300;
UPDATE `SubscriptionType` SET `DisplayOrder` = 4 WHERE `Id` = 400;
UPDATE `SubscriptionType` SET `DisplayOrder` = 5 WHERE `Id` = 500;
UPDATE `SubscriptionType` SET `DisplayOrder` = 8 WHERE `Id` = 600;
UPDATE `SubscriptionType` SET `DisplayOrder` = 6 WHERE `Id` = 700;
UPDATE `SubscriptionType` SET `DisplayOrder` = 7 WHERE `Id` = 800;
UPDATE `SubscriptionType` SET `DisplayOrder` = 9 WHERE `Id` = 900;
UPDATE `SubscriptionType` SET `DisplayOrder` = 13 WHERE `Id` = 1000;
UPDATE `SubscriptionType` SET `DisplayOrder` = 14 WHERE `Id` = 1100;
UPDATE `SubscriptionType` SET `DisplayOrder` = 16 WHERE `Id` = 1200;
UPDATE `SubscriptionType` SET `DisplayOrder` = 10 WHERE `Id` = 1300;
UPDATE `SubscriptionType` SET `DisplayOrder` = 17 WHERE `Id` = 1600;
UPDATE `SubscriptionType` SET `DisplayOrder` = 11 WHERE `Id` = 1700;
UPDATE `SubscriptionType` SET `DisplayOrder` = 12 WHERE `Id` = 1800;
UPDATE `SubscriptionType` SET `DisplayOrder` = 15 WHERE `Id` = 1900;

-- Update User Role Permission Name
UPDATE `Permission` SET `Name`='View User Defined Alerts' WHERE `Id` = 201;

-- Updating the TimeZone in MerchantAccount
UPDATE MerchantAccount MA, CustomerProperty CP
SET MA.TimeZone = CP.PropertyValue
WHERE MA.CustomerId = CP.CustomerId
AND CP.CustomerPropertyTypeId = 1
AND MA.TimeZone IS NULL;