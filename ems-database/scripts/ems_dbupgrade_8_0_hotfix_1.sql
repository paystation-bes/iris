-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CREATE Stored Procedure `sp_InsertMovedPointOfSale`
-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS `sp_InsertMovedPointOfSale`$$
CREATE PROCEDURE `sp_InsertMovedPointOfSale`(IN p_CustomerId MEDIUMINT UNSIGNED, IN p_PaystationTypeId TINYINT UNSIGNED, IN p_ModemTypeId TINYINT UNSIGNED, IN p_SerialNumber VARCHAR(20), IN p_ProvisionedGMT DATETIME, IN p_UserId MEDIUMINT UNSIGNED, IN p_PaystationId_old MEDIUMINT UNSIGNED, IN p_PointOfSaleId_old MEDIUMINT UNSIGNED, IN p_IsLinux TINYINT(3) UNSIGNED)
BEGIN
-- Disabling MySQL transaction statements to allow the whole flow to be managed by the Client transaction manager and rollback with any Exception happen in IRIS.
    
    DECLARE v_PaystationId MEDIUMINT UNSIGNED DEFAULT NULL;
    DECLARE v_PointOfSaleId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_CurrentGMT DATETIME;
            
    IF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0) AND (
        SELECT  COUNT(*)
        FROM    PointOfSale
        WHERE   CustomerId = p_CustomerId
        AND     SerialNumber = p_SerialNumber
        AND     Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber)
        ) = 1
        
    THEN    
        SET v_PointOfSaleId = 0;
        
    ELSEIF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0)   
       OR ((SELECT COUNT(*) FROM Paystation WHERE Id = p_PaystationId_old) = 1 AND (SELECT COUNT(*) FROM PointOfSale WHERE Id = p_PointOfSaleId_old) = 1)   
    
    THEN        
        SET v_CurrentGMT = UTC_TIMESTAMP();    
                    
        IF p_PaystationId_old = 0 THEN 
            INSERT Paystation (  PaystationTypeId,   ModemTypeId,   SerialNumber, IsDeleted, VERSION, LastModifiedGMT , LastModifiedByUserId)
            VALUES            (p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, 0        , 0      , p_ProvisionedGMT, p_UserId            );
            
            SELECT LAST_INSERT_ID() INTO v_PaystationId;
            
        ELSE            
            SELECT p_PaystationId_old INTO v_PaystationId;
        END IF;
                
        INSERT PointOfSale (  CustomerId,   PaystationId, SettingsFileId, LocationId, Name          ,   SerialNumber,   ProvisionedGMT, Latitude, Longitude, AltitudeOrLevel, IsLinux, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT              p_CustomerId, v_PaystationId, NULL          , L.Id      , p_SerialNumber, p_SerialNumber, p_ProvisionedGMT, NULL    , NULL     , NULL,            p_IsLinux, 0      , v_CurrentGMT   , p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;
            
        SELECT LAST_INSERT_ID() INTO v_PointOfSaleId;
            
        INSERT  LocationPOSLog (LocationId, PointOfSaleId, AssignedGMT, LastModifiedByUserId)
        SELECT  L.Id, v_PointOfSaleId, p_ProvisionedGMT, p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;         
        
        INSERT POSBalance (  PointOfSaleId, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionGMT, LastRecalcGMT   , NextRecalcGMT   , LastCollectionTypeId)
        VALUES            (v_PointOfSaleId, p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT, 0                   );
            
        INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (v_PointOfSaleId, p_ProvisionedGMT);    
        
        INSERT POSSensorState (  PointOfSaleId, AmbientTemperature, Battery1Voltage, Battery2Voltage, ControllerTemperature, InputCurrent, RelativeHumidity, SystemLoad, WirelessSignalStrength, VERSION, LastModifiedGMT)
        VALUES                (v_PointOfSaleId, 32767             , 32767          , 32767          , 32767                , 32767       , 32767           , 32767     , NULL, 0      , v_CurrentGMT   );                                                    
        
        INSERT POSServiceState (  PointOfSaleId, BBSerialNumber, LastPaystationSettingUploadGMT, NextSettingsFileId, PrimaryVersion, SecondaryVersion, UpgradeGMT, IsSwitchLinkCrypto, VERSION, LastModifiedGMT)
        VALUES                 (v_PointOfSaleId, NULL          , NULL                          , NULL              , NULL          , NULL            , NULL      , 	    1            , 0      , v_CurrentGMT   );                                                                                                        
        
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  1, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  2, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  3, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  4, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  5, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  6, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  7, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  8, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  9, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId, 10, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        
        INSERT POSStatus (  PointOfSaleId, IsProvisioned, IsLocked, IsDecommissioned, IsDeleted, IsVisible, IsBillableMonthlyOnActivation, IsDigitalConnect, IsTestActivated, IsActivated, IsBillableMonthlyForEms, IsProvisionedGMT, IsLockedGMT     , IsDecommissionedGMT, IsDeletedGMT    , IsVisibleGMT    , IsBillableMonthlyOnActivationGMT, IsDigitalConnectGMT, IsTestActivatedGMT, IsActivatedGMT  , IsBillableMonthlyForEmsGMT, VERSION, LastModifiedGMT, LastModifiedByUserId) 
        VALUES           (v_PointOfSaleId, 1            , 0       , 0               , 0        , 1        , 1                            , 0               , 0              , 0          , 0                      , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT   , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT                , p_ProvisionedGMT   , p_ProvisionedGMT  , p_ProvisionedGMT, p_ProvisionedGMT          , 0      , v_CurrentGMT   , p_UserId            );
         
        INSERT POSAlertStatus (  PointOfSaleId, CommunicationMinor, CommunicationMajor, CommunicationCritical, CollectionMinor, CollectionMajor, CollectionCritical, PayStationMinor, PayStationMajor, PayStationCritical, VERSION) 
        VALUES                (v_PointOfSaleId, 0                 , 0                 ,0                     , 0              , 0              , 0                 , 0              , 0              , 0                 , 0      );
         
        INSERT  POSEventCurrent (CustomerAlertTypeId,       EventTypeId,   PointOfSaleId,      EventSeverityTypeId, IsActive, AlertGMT, ClearedGMT, VERSION) 
        SELECT                   Id                 , 46 AS EventTypeId, v_PointOfSaleId, 2 AS EventSeverityTypeId, 0       , NULL    , NULL      , 0        
        FROM    CustomerAlertType 
        WHERE   CustomerId = p_CustomerId 
        AND     AlertThresholdTypeId != 12       
        AND     IsActive = 1             
        AND     IsDeleted = 0            
        AND     RouteId IS NULL         
        AND     LocationId IS NULL      
        ORDER BY Id;

        UPDATE POSEventCurrent
        SET IsHidden = 1
        WHERE 
        PointOfSaleId = v_PointOfSaleId and
        Id IN (SELECT A.Id FROM (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId 
                                       FROM POSEventCurrent pec
                                       INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
                                       WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 1 AND pec.PointOfSaleId = v_PointOfSaleId) AS A
                           INNER JOIN (SELECT pec.PointOfSaleId, cat.AlertThresholdTypeId FROM POSEventCurrent pec
                                       INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
                                       WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 0 AND pec.PointOfSaleId = v_PointOfSaleId) B ON (A.PointOfSaleId = B.PointOfSaleId AND A.AlertThresholdTypeId = B.AlertThresholdTypeId)
                    GROUP BY A.Id);                 
        
        IF p_PointOfSaleId_old > 0 THEN 
            
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId    , 201, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
                        
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old,   3, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old, 202, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            UPDATE POSStatus SET IsDecommissioned = 1, IsDecommissionedGMT = p_ProvisionedGMT, LastModifiedGMT = v_CurrentGMT, LastModifiedByUserId = p_UserId WHERE PointOfSaleId = p_PointOfSaleId_old;
        END IF;        
    END IF;
            
    SELECT v_PointOfSaleId AS PointOfSaleId;
    
END$$
delimiter ;

