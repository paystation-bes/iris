SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



-- -----------------------------------------------------
-- Table `EmsProperties`
-- -----------------------------------------------------

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumLoginAttempt',             '3',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumLoginLockUpMinutes',       '5',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ServerUrl',                       'https://dev.digitalpaytech.com', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MailServerUrl',                   'dptmail.digitalpaytech.com',                             UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ServiceAgreementToEmailAddress',  'dev_emsbilling@digitalpaytech.com',     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumSessionTimeoutMinutes',    '20',                                     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumWidgetDataRows',           '2000',                                  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumPastCustomerAdminNotifications',	'5',  			                  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumCcRetryTimes',			 '10',							 	      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumRetryTimes',				 '30',							 	      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerDefaultTimeZone',	     'US/Pacific',					          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ScheduledCardProcessingServer',	 'VANAPP01',						      UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CryptoDirectory',	             '/opt/tomcat/certs',	      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EncryptionMode ',	             'Development',	                          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MinIntervalToChangeExternalKey',	 '0',	                                  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAccountName',	                 'QA Testing',	                          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAccountToken',	             '3479CUMlMvRRcs2we68hHHwrMnYVfxGoL9xRi', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceLocation',	         'https://sign.digitalpaytech.com:5005/services/SigningService', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAdminAlertToEmailAddress',	 'ems@digitalpaytech.com',                UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAlertFromEmailAddress',	      'no-reply@digitalpaytech.com',          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('HSMDir',	                         '/opt/nfast',                            UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('AutoCryptoKeyProcessingServer',	 'VANAPP01',                              UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DisplayCardProcessingQueueStatus','1',                                     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsVersion', 					 'EMS v7.0.41',						  	  UTC_TIMESTAMP(), 1) ;	
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ConcordEmsVersion', 				 'EMS v6.2.0.28',						  UTC_TIMESTAMP(), 1) ;	
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FirstDataNashvilleMCC', 			 '7523',						  		  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovAuth', 					 'ExecuteAuth.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovCharset', 				 'ISO-8859-1',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovConnectionTimeout', 		 '4000',								  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovConvenienceFee', 		 '0',								  	  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovRefund', 				 'ExecuteRefund.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovSale', 					 'ExecuteCharge.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovSettle', 				 'ExecuteSettle.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ParcxmartConnectionTimeout', 	 '2000',								  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardAuthorizationBackUpMaxHoldingDays',	'90',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnectionTimeout',           '5000',                                UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumberOfBatchReencryptPeakTime',	  '2',							          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumberOfBatchReencryptOffPeakTime','45',							          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SleepInPeakTimeInMilliSecond',	  '1000',							      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SleepInOffPeakTimeInMilliSecond',  '200',							      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('StartPeakTime',	                  '14:00:00',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EndPeakTime',	                  '07:00:00',							  UTC_TIMESTAMP(), 1) ;

-- Reporting variables`
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsPdfTransactionDetailsMaxRecords',		'50000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsCsvTransactionDetailsMaxRecords',		'1000000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTransactionSummaryMaxRecords',	  	'1000',			UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsPdfAuditDetailsMaxRecords',  			'5000',			UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsCsvAuditDetailsMaxRecords',			'1000000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsAuditSummaryMaxRecords',	            '200000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsMaxRecordsUsingVirtualizer',	        '57000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTaxesMaxRecords',	                    '100000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSizeLimitKb',	              			'10240',				UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSizeWarningKb',	           			'9216',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsZippedLimitMb',	              		'9',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTimeLimitDays',	              		'45',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTimeWarningDays',	              		'40',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSystemAdminTimeLimitDays',	        '1',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsQueueTime',	              			'03:00',				UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsFileNameTimeFormat',	           		'MM dd yyyy h.mm.ss a',	UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsQueueIdleTimeLimitMinutes',	        '30',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportBackgroundServer',	        			'VANAPP01',				UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RerouteToEms6Url', 'url/for/rereroute', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RunningTotalCalcInterval', '300', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumPaystationsBeExecuted ', '25', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PreAuthDelayTime', '180'        , UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardStoreForwardThreadCount', 	'1', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardStoreForwardQueueLimit', 	'10000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardSettlementThreadCount', 		'10', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardReversalThreadCount ', 		'1', UTC_TIMESTAMP(), 1) ;


INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SensorDataBatchCount', '100', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SensorProcessingThreadCount', '1', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveSensorPaystationBatchSize', '20', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveMaxDaysForSersorLogAndBatteryInfo', '90', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DisableProcUploadTranFile', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FileParserThreadCount', '1', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CsvUploadProcessingMillisPerThousand', '1000', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsRateInfoCacheSize', '500', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsSimulatedEnv', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IsSMSCallBackIPValidate', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsCallBackIPAddress', '69.28.206.213-69.28.206.215', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaxUploadFileSizeInBytes', '10485760', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MappingURL', 'https://maps.digitalpaytech.com/', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IncrementalMigrationLimit', 4000000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationWaitTime', 2592000000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ETLLimit', 1500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ETLResetInMin', 120, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IncrementalMigrationCardRetryLimit', 4000000, UTC_TIMESTAMP(), 1) ;

-- CustomerMigration Values
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationServer', 'VANAPP001', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RunDailyCustomerMigration', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationBatchSize', 10, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CMProcessDelayInHours', 48, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DataValidationWaitTimeInMins', 15, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CMHeartbeatDelayInMins', 60, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DefaultEMS6URLRerouteId', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationAlertEmail', 'no-reply@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationITEmail', 'no-reply@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TransactionMaxRetryCount', 1, UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RotateKeyInIris',	              '1',								      UTC_TIMESTAMP(), 1) ;

-- Mobile API
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MobileSessionTokenExpiryInterval', 60, UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MobileSignatureVersion', '1', UTC_TIMESTAMP(), 1);

-- Updating IsForValueCard to 1 flags for BlackBoard,TotalCard and NuVision Processors in QA and Production enviroments

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SingleTrxCardDataStoreInDays', '30', UTC_TIMESTAMP(), 1);

-- Dublicate serialNumber alerts
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DuplicateCheckCount', '3', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DuplicateCheckEmail', 'dptcs@digitalpaytech.com', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DPTCustomerId',  (SELECT Id FROM Customer WHERE Name='Digital (DPT)'), UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('HashAlgorithmTypeSha1Id', '1', UTC_TIMESTAMP(), 1) ;

-- Signing Service V2
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2Location', 'http://dev.digitalpaytech.com/services/SigningServiceV2', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2AccountName', 'SigningAdmin', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2Token', 'fWJY7K3SYzB97aEjV1DO+Q==', UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CollectionUserIntervalMins', '10', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PlateReportReturnSize', '1000', UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ProdSupportEmailAddress', 'morgan.madu@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;

-- Authorize.Net default x_solution_id
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('AuthorizeNetSolutionId', 'A1000071', UTC_TIMESTAMP(), 1);

-- Flex
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexDataExtractionProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMapWeekInterpolation', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationWeekInterpolation', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationOffsetMinute', '15', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMaxRowCount', '1000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMaxTryLimit', '20', UTC_TIMESTAMP(), 1) ;


-- Elavon viaConex
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonVersion', '4017', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRegistrationKey', 'TEMP_PORTAL_FAKE_KEY', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonApplicationID', 'TZKQ01GC', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonCloseBatchCount', '7500', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRequestTimeOut', '30', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PostAuthTimeOut', '60', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RealTimeElavonRetryAttempts', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRetryWaitingTime', '10000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsRealTime', 2000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsRealTime', 7000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsSfSinglePhase', 2500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsSfSinglePhase', 3500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsCloseBatch', 2000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsCloseBatch', 5000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsDefault', 3000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsDefault', 8000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonPreAuthExpiryTimeInHours', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReversalTimeoutMaxRetryTimes', 3, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReversalMaxRetryTimes', 3, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRedisCloseBatchLockPerTxnMs', 30000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRedisLockTxnMs', 30000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonCloseTimeIntervalMinutes', 15, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonMaxPreAuthReversalLimit', 100, UTC_TIMESTAMP(), 1) ;

-- Case
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseDataExtractionProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseAuthorization', 'Bearer ZTBjNWQwZjUxZDMwNjZkZmNmOTdiZDA4MDczMTM0MjA=', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseURL', 'http://54.197.238.92', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseRequestTimeoutTime', '300000', UTC_TIMESTAMP(), 1) ;

-- Execute Patch
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ExecutePatch', '0', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `RestProperty`
-- -----------------------------------------------------

INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'DomainName'                    , 'dev.digitalpaytech.com', UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'PermitURI'                     , '/REST/Permit'      , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'LocationURI'                   , '/REST/Location'    , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'RESTSessionTokenExpiryInterval', '30'                , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'SignatureVersion'              , '1'                 , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'TokenURI'                      , '/REST/Token'       , UTC_TIMESTAMP(), 1) ;

-- Processors

UPDATE Processor
SET 
IsForValueCard = 1
WHERE Id IN ( 13, 14, 15) ;

-- Insert data into CryptoKey and CryptoKeyHash on Build Server


INSERT INTO `CryptoKey`
(Id,	KeyType,	KeyIndex,	ExpiryGMT, 								`Comment`, 						`Status`, 	CreatedGMT,				VERSION,	Info)
VALUES
(1, 	0, 			1,			DATE_ADD(curdate(), INTERVAL 1 YEAR), 	'Wrong Expiry Date!', 			0, 			curdate(),				0,			'vWmX1fI44K5dKkvoq3Q+DIp+O1FQRXKFDC0HiT9eyDQqaAR9YKDVXgmiCtLYsgFjaPsnsrR3EWoGB67sCpeP2Cn4bV/W0IwnaBYDoERJRpnfb/V9ZDE6B4PWXRDZNJb4qrxdmntKO/Et5QNGtugpAcLPLBVcj8kPV5wRVOBogcNQ3AkZsOeehcHftrShFLqM/6/mY6AnHoG0yedpMEqJoT+1aYx4BEBuneY2RJnYvrA8NYCmC1kktgiFy89R88aeozeH/22zGNYFkb8moIypREr3+MCMRzehBT+yxqwkSHh/o/IlaD4ridRlYS56sDwUJPNoKj2QOSw0k5EqTkbHmw==:AQAB'),
(2, 	2, 			1, 			DATE_ADD(curdate(), INTERVAL 1 YEAR), 	'Initial Internal Key.', 		0, 			curdate(),				0,			NULL),
(3, 	0, 			2, 			DATE_ADD(curdate(), INTERVAL 1 YEAR), 	'Wrong Expiry Date!', 			0, 			curdate(),				0, 			'pGyGKxym5RitbOLEB6Iy7d5PQgL9MVerWIWY6E0R8BHSedGJ5UVV9dfB/ljbkPy+ngjJszvYa8rerKIzvStlLSPMAAyr0lpBCHG/mE8jRhBPf8Fbcyk/uOqRN5vzs/wO4ex9puZTFq6YfrdD2Eg2bdpGi5j1P5nXIo7WOj1NgRMQsUULsuUrgJPR8F0WDmfBfKQotq8s/g+MOeHwvk/JsuT1i7pChLZTaZT7et+MOyVawQ+cE7OJrRzWMbkU3GfFtQC5pE/QhGVdYqpFXMTGvaqNKY1u5shPa0kQIxqoLlNpAD4BsVMZfrX8WB3QwENBszv9uwuzgMpCepZHuPm6xw==:AQAB'),
(4, 	0, 			3, 			DATE_ADD(curdate(), INTERVAL 1 YEAR), 	'Wrong Expiry Date!.', 			0, 			curdate(),				0, 			'khlPI0AFWlj2mP/mWVlsLXE8Nwz8nxWVhYdHLFe4FmItEiRN2pawFx8qS94DDJuJZu2fUQdeC5jVfM6dOGtFGRPWUdSVOV9lmpEWqUi0ExkicsFntloMjCKvpMsK6iFUGdypfvSJfjfmxHJL4c1gAWji2gkNv0GIUsVu2p0jNdlSuvsX16amJmC3/hFc8JIXuFVcy8j97odPkoNk783SBkPJi/9Xry85VArgfotyvu9k4gz9rzHj0+jvvEhahQUCJtV3Yc0SiDkNEBWDvJCZcsIW6EU8KXPTYCWmcEGVXdYLgen35lqTAkyJunnt7JY63S/byufLc6Hqg2i0HWzIzw==:AQAB'),
(5, 	0, 			4, 			DATE_ADD(curdate(), INTERVAL 1 YEAR), 	'Wrong Expiry Date!', 			0, 			curdate(),				0, 			'nNmwMhQBo7FlZJQWjfkDhx6e2w0wQJi6qlGH0TEHUuK6zrlv9dqYKNNX7enXgZ3hiM7CebS2gYi+tVrHRs6tATjXA85/kpUNyeZ92IqCdSjEW4Lra+yD8O61jkRIvEbHDAo44NJtFPCbYTEUTe4vUK9B4NrEnu2GjG6dz6mH4i+A76tf9T/4j3GlxM0lwTO8CiZJYhVjM5fByxPJ9sfE7aPE8cJkO+mFkdMBPezvvAS/4Xh8QecnrRufw/8puf/wmaMS3Awyga8IWsohvK1JiCrheFEGSQTPeVn9tq7nA76lzuSA1cl7AQXX3VPIBc7j7auUoZm6vwhe5bmsHVHUJQ==:AQAB'),
(6, 	0, 			5, 			'2015-12-02 18:43:17', 					'Initial External Key.', 		0, 			'2015-12-02 18:43:17',	0, 			'm1r7qixwuNMXzU9oafw3TqRHTj/NcNnsUTFpUI/9NGGmyTBZZYKWz/xv4NQEo5WChXgnjxu2NxZOKiKE+XmWESDFkOD+NP/efjUbV48K2u18+RvbN58j1I8DRhTeMJo8kl0cEBj6xMDwmiatoShRZdz0HJiMJnnKnfvQi9KXIkzS2rT6TERTZe3KtLH4pLp3qVJTwCk1ya3XuZMsxDNodfGkde6QnwujKUX/oXOQhXgaJqKdVzZAPwqM40n1ZDXlg/+5P9Xdz3oxvvfAFaMYr0fDHU6iBl3ZiNwt5wqmfIMnxFDMwPSQeblXkTJ4XfM7JrpSZx7lj/rG1t6GtLyYxw==:AQAB');

INSERT INTO `CryptoKeyHash`
(Id,	CryptoKeyId,	HashAlgorithmTypeId,	CreatedGMT,	VERSION,	`Hash`, 											NextHash, 											Signature)
VALUES
(1, 	1, 				1,						curdate(), 	0, 			'gJtBO3duGX023+s8k3pRewyANbQ=', 					'4RNfzmu8agbgdEZIMRMQTAMlmd4=', 					'AQqaFpUiQS8b+nEU1v01zriplbEVK77IEX99sQW5uVhryq/Q1a6Kv+B96jM3CUtL9e2V40+S290+e5PfUisQvnr4/tNzpItElbJN83aAwetjWrtTAxgsEiIUFi3d0uz7+FlNaS1lbFDhXTfYToyOX7+umdStdy4I0fCGJwKtSQDQY0ayftxe6o3jXt97lZH3a1z/UYq7NA0+d4UdSLtYQtUXxKYUxQQrEtfnUwej/CyaWB5mFuSHTjz4AhNQ8y7FvlHR51MoN+olRU/pag5YyyCm7GrDdK98DNCUaOGEqmg2ggkp3P8sww1hUMI0LELfyJeMOYN28qK0uX4y8X/I8LMrCdg+FhALVjwIxHa6Jq2sFdpIRiQ4k6x4QF9r1mZHN3uJM6P9j+6ymoZeFR8ycoYASsG+CVHhDlthNJwMS74ADsZUwz0FcvNXlwY1iWfmiU6NERko3b/JYeymaPzvSoOkRFnyoFI/DinDnjHACrczwjkJMz80KCNGTyhMFfTeV+HXp5wzNFXra//xJJI39SInVSb9keeE2nqmnotYjyiknn5jFg05Hf1csutCRz8td/EsE8rPGbr6foSiGh2SavOmqCh6CCh9CY4TzNew+/v9iOiS0QIByzjM59btnEzmsKZXsgNsF/jcWqnBhBMv08gg66rpp696KpnrnYQ9LFI='),
(2,		1, 				2,						curdate(),	0, 			'cJ0GMQ0BNEJvYZCh7yQj+XxU+pWTps280ZN8jwwqzoE=', 	'3VojU/g+AT9FzPry94v3Xn5AdnkLQYbqAxxU1g/7mJ4=', 	'JV5IfkR21owu1FVWqBFWasq55iKxF7LoY4iS2/K6YLbXvw7yDFFtSc17IiRT9gRnePF4wIUmXbGRClh2HmnUcguac9X0coZUX4ygvHAQ1XkPMfDPCKUeHGQHBF8lq+Ka5EbsbbJvhxhU9RGuGqzf9xLomGXXaLc6OJavSx1x5+lvtugga7BZNdFhHpvcmUaw8gJ+pnpjd2AUAv1+HtYWQBZsxxJY0r8VyhClmGIXZZDGuW6gGw/Qpwa3csKLoHw23fx98DVk05gQrXCgqq4zkhXEmXZ91/MFqG9R4qi7cyVq5QPxmVbC/z72bqgXFpK/KvejMlY++qN7Gxpa055wkmvUXUkixps7lQFrLfMb38EF1VBDOLSeZwopF2aJxGArmUqKWP49j12qbEPmei1OnWyjbX6kSekf6sv5LNE8tuUswqoA02ymtedv0dV5nPQKsZiWKA5X6KyaIhX0TwCoWSQvIhbUZ9zhbHQ0IkakwI0pMP2zZBdXOwPIrAPIg2zDHK/VgbUdOnoxtj3pBjnBlxr21ASXtI8DBlSY3pFxeTE7lEiqbA0m3q8uAdDkyfh4cZCZk05B+dRIZ21veRE9MF4/RZyj1c+MPCk02SOl4EbwESYERY3S0t6lPoYM3zywkHSdHVRnT83vGllL/bPr473gUkHwYPbuUtUTunzr9Xg='),
(3, 	2, 				1, 						curdate(),	0,			NULL, 												NULL, 												NULL),
(4, 	3, 				1,						curdate(), 	0, 			'4RNfzmu8agbgdEZIMRMQTAMlmd4=', 					'+kHkxKnaseaCXd32FxuyNcSP6jU=', 					'NWHym9OTRMg2dceRquUywxd+UgU67GOb8wcbPq4eOHu07YS0dFCfynT65xOOOv2YT/B1wcYVRwczxkSE6lgyZPt6PVZxMD0e1Jknf/cJSDV6V3VZZp1mYY2KmoHffKJMQsA+77MCH6cqWd3QFd/7XblOLcngNKOd7J0XZ0Cb98iwu1FCG1vTY2Jdrdt7SYCPUahFfDIYT+ncn8cmipkAeHQTHLfNOGNcXOy9y051hwB8jiJG3sjzYixGnLZszRk0R3LQAtMo46BKN+W1Hugp/td7Gz4UJpVmYI2Q99NAu8MxGw0R6W8wa3BRQzDcnpLPWELjJwDvOuZf00WQiIgkM+vVkR/HGYEWd4WgrFCmO0sVxckPN+ewvEuPzsF9r+KlSGUT53hdMmFqdZ7iw6/SRncXia7p/UYX2piT8mckr2ltyf91P3/JfSU8qogZDkNk2M+i8UJrtjwKwv0K98dhmMZozxNWCRlHz4UvsGvbRb7s9R+9q0zcw9EaMTcvAeNc13BPsDDToaeh9fMiR++kT8hMpcJFXprBCHnWYZEexlJJZ9inDuAKJ7oP74lBX8f67rraHE+xB88PlrAyKJlej1opTWnvW00/DdZ5KaVyzRq1SNMO4qMkRCz5L6qcmb7C7xzxC/Q65Z5LmK3H3xYKaq6RB26VOPvjXhd0pSDPzP0='),
(5, 	3, 				2,						curdate(), 	0, 			'3VojU/g+AT9FzPry94v3Xn5AdnkLQYbqAxxU1g/7mJ4=', 	'ycKfZHIdfhh73KZj8yC+Z5r88/1isREfKQRL4RXKFWM=',		'dGIi+DUHqJYWKJCgeeT8vrtwXTCfAP3ShbOrw7uS8Ujj7ZJnuWs2AVKEBFsCAzEqwbYW2HlIUFbPBxuhpsWB7U76sUAR3Z1tL7QfqwZ74GBufwCgl8VBE01upBnrPOK4IYcjxVEUb+Xy5XZS5SjcKWnaocGkNRpQ8CAqoWqHA9fcn07HfX+LWenmV9GL9HHh2j8omwyUWmNBBypRG++CPGMfmwi9SPAJZakjM5am7kbzWe7ONp09Xf4XIgoZDGLpq0pCVoBstlx04hhIomu/U+CejbcvzXy60vR0HyrgGRZAziIzcKETOPD/oWZlylLkE6WbinckdcSpIHA6mY49A3mKFBoyfKv67x96FDgXc7tt/cma0p+lf7+oiijjl69bJXZCfYC6vxvQGW9QhRUO6Ai2L0BEeiwgNOWWuJLGpehnCnIXzeIoLPTIwFo+KK8LW5w9iSXXES5ayBhtnUOqPRFjj91W6INS1eOqyNPF3o4o5zkZn2cQ6GGV/V17cL74AQ8paltp2A+V+Pkv4lo/T6ReDBjK7/NYM2gy7NV1adVHrAVumjraitR1bp9R7mXYG2tx+w4vh41Z3tVgbQV4KXFRmcxbV+KbEXq/SILy+BnFF0IevoJkp2rbbTz3D6IDQNjojSyqDMkaf/yhXDcrrkw7IGbl7XyoNY7ix+lFX2U='),
(6, 	4, 				1,						curdate(), 	0, 			'+kHkxKnaseaCXd32FxuyNcSP6jU=', 					'srAUaRnZx+EpyvUdmDkh0aj/S4o=', 					'XxJj15t2znI0OycpxVsreZ7M2zxEouCBxysPa/hAZ6nxz8LUare+cla4Yl1I6YydDVSLLiVBEMWqFQtUlZgteUzhbZeVu18gWsXFsxPWctNC32ScNWe7pFvAye+Rrf5kdbNrhmVjU+xqXK1FU60MqoIm2/WeglHSLRxFH5VQ4fHxIE8lgd3hR3C5n6g8+txoWHKH33TkO1Ohky5E2gRFKj+5fwL2URepVIXI92h8DhwEzseEzPpLyzbM/q+Id13rohecpHHwCYUNbBacNtVya6PGsnTK7ZABggUtzg40pJaMQWwxVPq2et4wKexzG/GKWXaoJmcPfBCvrxVGD4f/dSIj+fb8BMRiif3anHPQzSuj+lUYNJh+Yx0suAgboL0q9NxJkI9du8LlMPH/7a8wwoSS4MSxdtDOb1/TqGi90wY3nbbTLHMXXVjKkx3u0SyCOS/hlCt+ypZlJgw1IwRQSmo8G3IbPU32kLP4ylDSklGxbvgkpjemAIBj0CC6eyc0SoTM0zZhUNeVqKzR0QXQFwL9R5i9Y7XEruGEXq+qEZKOiL5KCOzOuNvl3kL9Al8MjjNBR8611RjBlgZq5vnbetD2HlFMHlvuM6Jq8h5p9qFpvAs3JKXafyTEnXXO4b3ex9ajqz/eRfiE8ZsuFjLlC5O0bBU+XaWiqKKzxcIgne0='),
(7, 	4, 				2,						curdate(), 	0, 			'ycKfZHIdfhh73KZj8yC+Z5r88/1isREfKQRL4RXKFWM=', 	'8Bzt3SFwFOnNJfiFT+vOnsRngEgXAVbo+rbJVVWRXns=',		'RTjflxobj6bxIRdLEAcgMiQFQL+2Ej4Vo0j0ikNb7FxRFM+OJGXv8AXy399P0A2W8FJLtA2xt/F9Fga3YQD06smvBl7gjp9+juxfKiYz5qceSb+awx89vpHOy/TNNbb6KKvhys3I2wBo5vzIneaa7sA+qS71Oj9cPl//ejQVPhaLeU/dLSohphEF//j+jVFhYfva8Hu2lrHvpCnPRi0ZM/cy463HZoDuc9c67DQ1y9/3v3c8AKkVH3Ub2BLcsr5UfhSeyWFXWqaK7pdTmfuDuWJNoVVZpA4NylGWdhAT+zfdLzXZ/yAZLRpN1jn0FY+bOJL0CKqSdqhz2wojjE+3hkW3UnzvyI7BmEiiSf9IdiMG1NhKdBFxacWqrKZlxr6Vx+GorTt7R4DtITtACzqMhzuxgIIduslYJv8+6aaliah3hnpf6PjS+fs01/ICXsODmXuCw00IiEVi+6/KjQk4WDO/0sW5aTwhMkYOv25LL+cSvc1X+0X+06Si0TqM32/0/8uxjgqwHhfBi40dfRyFGDG0idGKPOe8/99OroFBSrC6ifyQH3qL6WS4VrxtNCh8nh5+q+0uK+USt57TMhW45uBuVEHNciE5XXcDMkMwXberpHMyxw8xxe/o2dcZuxNicaqKlLetTm8IqA7cbQcG5S3EOurcaTQVI96zSYaJCcA='),
(8, 	5, 				1,						curdate(), 	0, 			'srAUaRnZx+EpyvUdmDkh0aj/S4o=', 					'i59K0D1IEChhKV4+3R6rOpFJI48=', 					'KwvzmfBLWMa3LGAO/C8DEPI1dI8T9ZN8SdhFwSVxRxh8NCEwxQsUk66vhoFWgzOfL9XPhQ+N5ZewNY/QPpD/A9mbekgC9IMxEUIrJvQ1utvE0AKMCN1jIaMtiYoobRJjIhKVAcAHMqSPPdNvVM31Z2o5I3X5/XOc9Tq4MVyVwkgTYPplyrsZNBf508alg2VzSTGNJWNqxbcrI4VDXOYAE+I07eKgPSd+j0pufSvQsgYrGk6/o5Q6ERagVZZIfFbblIP0DLpdS+TX4oXPk2bWnV9A3xB2AfGBYjse7sooRP1VOJyIjQk8nvZUbUZ47/fFhFKLUqIUrxvZr1LlSccvTj5P6FFsg3irBED2Se5in3x/F2P+XrWo8y/v89UhFdqig2EQQAM6lIgVFUJ3rJFa+aGS9vknZEgFcXfuoXjAYgmlcrYJrjQwODTsXdl3/+T4rfaDr+v0nSYHw9j5wIKD5izxxyAtOkS0XeqmQIwIt26Tp8jhR2SovduAwABB55eu3kzuxe+xBId/Y90n9jtLjbnslvH/CaZjYfebDlGZcKs82fWtn+csTraeEKHWQhtGjq9y9g+oC4AGn/YiKaG8BKJNqtCetp8sjczu5WLf2Lo7orDzWaxIbymvzK0eP3Obc7ic0kBXrtbFa6zR+b+RS9vyjRoWvfXPEy5Id2+gIys='),
(9, 	5, 				2,						curdate(), 	0, 			'8Bzt3SFwFOnNJfiFT+vOnsRngEgXAVbo+rbJVVWRXns=', 	'vZ7zqO107ECPI0qTy0seREi3fzNhrfFNL8RZDP4/YPU=',		'RJLXnLLjV3lnQs5T3l0X0jynCKEK+k4iUpYiM/NelURU2QzSsWH5+I8w9fhVLMeipZ6rZicDXelAAYoeSNur2mM8MvyUaA+ZdblCCdLpddvIZYBJ3PDM20kto7GVgcS8ZjScRNuBMRshFEdsGw2lSK2SD/O8rz3kk5YiWkcefQA2uCJSxBoTJY9C0EmzLcIydVUOmPV99xNIx9x7CJXegAIwGJ40FsFkqSjYI/C+1Y9PYm8Ehxl8jabNb9DSk3enQcb9zfIpzdL3J17l3qt2s+2UUY20OjGJUcOty8l9ba6L8MY4ZS9A/chTQiqCXuOAuNxH7tPmsbAlWeeoy1OfRlWvLK1yXSbz/+9kaRRfNx8xLo8PMVJuEHxhnAVsNnDrpjyDrvmfEHLK7Z3BNWkogSdbOT8mwbZu84gDONiTmBwEYll/FT4Z9Y46NnyyRJw/arA9T7xdbANHPzH2aYzsekejXKp7bbVktnHE9j4FzANQL4Oaau4sFh/s+Gb97cOiN2N3IllAYaUTNAJnA0sqXBMJcRhvWg8KyWbAyRVYeMWDDSMLk+gjkim0dUHvqrMy8BFqG1Cv7HG7J5SJO+3G3aq+q4yU3g4SqyVd/7t4JkAI2RDmSqlQIYnRwEV2tG+DevHR3zUDejiB2SEIy8arD+RUZ7H14R05hhc4ZABFfz4='),
(10, 	6, 				1,						curdate(), 	0, 			'i59K0D1IEChhKV4+3R6rOpFJI48=', 					'4O7jG1GFarYMXQS3yuAmVhv1OjI=', 					'YbNYNpm2qL0SiHJ0sR21FJdgMR7fltY+gzvy9VlXLAaaAb7SgVH+i7TH6ngOGVyNiJEWKRNeeoP/xIDLUjqx1N7RgOGoa6dbJLH3RsuAHZCIb0q6skGPokXjBh5ZikMc3MY+Veg+OpHvMK+FVJobXI8qIPx6LfkMLp++/GXiW78pv78QTfHMw1ZHSgA1VF5ZlRF2jpKbcgmrfIV00LSq9kP+51APyzJW7cJ+iXBO7jY0WrIzsYWH40RjLnhDmnEDN492GgsgXZnRGtJBDfpCc/yTrmmX9LUYvc5L3wNF+B832F45Y0o/31oKj7bWQMAjPZK2WN7mNAK3s6qO46qv6RyQyUVRH0EzBW3s8X+Cv8Rjau0Ss5FGd9AqLOmVePKL1ujJ0W8+flmRvqhnZAtBalm/NYaOW17uPqrse3I8/+FltziuU8LsuJF3FC7/ZL+1jyzt2SRLMImfNeKR9mtcrg94/Kaq2LAk5EtlnigmCt1GbMh3hdKtSG3G8aJgsEVRaFcX7rt/NLReB1Eik7NirVnzdPzklzU9MKmjncy8e9nt18v94qdzzPPzKblBu1vB9rz3IhIiYoCaOJhA++aL21AmIzRLTPnkEypvzleckQpO94BMsOyK4Agzt9g3LyT9SB9pCx5QgL4EEk0GRCU1TTbAEMV2goeZ0lODUFkBwaU='),
(11, 	6, 				2,						curdate(), 	0, 			'vZ7zqO107ECPI0qTy0seREi3fzNhrfFNL8RZDP4/YPU=', 	'6AHZB64rTmBylquydY2Kl6uaWCEKIqSok5i/4gJqK3E=',		'NMPX2V8FtSn/vJAuAHUCTike1psor0fmpoG7dOluzFj2IH96titg85LmEqyTr8uvQIusK6aToiRm//vN7/9yqTZU0HgpbKUZ3gYa7ZsVvoWEG/KQcL53Et6BWZDuA89Kj5Y1EJCnK9qkFRyzQTDFPMQFbegiGgQNA/AAvCbSaQ9/OY8NZuk9sXL4LBt8sTNB+GvTheE7WH+ZKnTTPlnP/Mrf4z2t+3N3KjGyzGZUW1ruKbDQ/DfztpLnVVfRw0c7ypUlfIyMmQWn4W7jrbrtGqvIGnSZitzGqw+NVUiCHS85s4mQDkyMxvLVtd+ZZWwEqfqgKQc8NpWFU5pOei73r+e89OHv8r2zQAM0WMKoeynf+u7XJsagk25DyWpW9ahvq0WtI/hq6iGAts8YnAfWinb6aHLm+f+/BJHh6t91SyVN9emwh7Cxni5ms4ZI15x1T5plDDJVj3SCwCwV5toMzzKXy4hH7SalRSReigxdfFNzc21MGwvDeAvyy7zyzPZ2/47UMCdPiD+zM5/wd8Dq8K9/3iD48Kqtlj2uzD9G5L8IWSOvVPjMxyHJg5Rx1GAn0pxXKQJT2AbbjsGBzkQuyyg3/PQ6y41h7s+AdiSOJ3cvYHUh7MlUmZd5sTJSqfe8YHlROTkIwgWHI4f5b3R8/SuDrWbW06Ta1BoGZyzXTGE=');



-- ------------------------------------------------
-- ArchiveMaxDaysForSersorLogAndBatteryInfo	90
-- ArchiveProcessingServer	VANAPP01
-- ArchiveSensorPaystationBatchSize	20
-- AutoCryptoKeyProcessingServer	VANAPP01
-- CardChargeRetryThreadCount	1
-- CardProcessingQueueStatus	1
-- CardProcessorAllianceIsTest	1
-- CardProcessorAuthorizeNetIsTest	1
-- CardProcessorConcordIsTest	1
-- CardProcessorFirstHorizonIsTest	1
-- CardProcessorMonerisIsTest	0
-- CardProcessorOptimalIsTest	1
-- CardProcessorPaymentechIsTest	1
-- CardStoreForwardThreadCount	1
-- DisableCompressionCheck	1
-- EmsAccountName	QA Testing
-- EmsAccountToken	3479CUMlMvRRcs2H9oSzHwrMnYVfxGoL9xRi
-- EmsAdminAlertToEmailAddress	alan.wong@digitalpaytech.com,martin.yeung@digitalpaytech.com,mariko.liza.tikhova@digitalpaytech.com,arash.taheri@digitalpaytech.com,daniel.agyar@digitalpaytech.com,abel.tse@digitalpaytech.com
-- EmsAlertFromEmailAddress	qa2@digitalpaytech.com
-- EmsTest	1
-- EmsVersion	EMS v6.2.1
-- EncryptionMode	Production
-- GoogleAnalyticsAccount	UA-2956139-2
-- HSMDir	/opt/nfast
-- IsSMSCallBackIPValidate	0
-- LastHeartBeat	1325780848260
-- LicenseServer	VANAPP01
-- LicenseServiceToken	TSNpuJrOCEJVPrk15kNJ0MlipbUSY8fi
-- LicenseServiceUrl	https://cerberus.digitalpaytech.com:5006/cerberus_v2/services/CerberusService
-- LicenseServiceUsername	OzfIJI0edmsQBBoIouTJMYmT8zVs9s2I
-- MailServerUrl	localhost
-- MaxAlertsPerTypeAndCustomer	10
-- MaximumLoginAttempt	10
-- MaximumLoginLockUpMinutes	5
-- MinIntervalToChangeExternalKey	0
-- ParcxmartServerConnectionTimeout	4000
-- PlateReportReturnSize	1000
-- PushDataBatchCount	100
-- PushDataClusterServerName	VANAPP01
-- PushDataMaxWaitingTime	60000
-- PushDataProcessingThreadCount	2
-- reportsAuditSummaryMaxRecords	100000
-- reportsCsvAuditDetailsMaxRecords	100000
-- reportsCsvTransactionDetailsMaxRecords	100000
-- reportsMaxRecordsUsingVitualizer	30000
-- reportsPdfAuditDetailsMaxRecords	100000
-- reportsPdfTransactionDetailsMaxRecords	100000
-- reportsTaxesMaxRecords	100000
-- ScheduledCardProcessingServer	VANAPP01
-- SensorDataBatchCount	100
-- SensorProcessingThreadCount	1
-- ServerOrganization	LAB-QA2
-- ServerUrl	http://qa2.digitalpaytech.com
-- ServiceAgreementToEmailAddress	 dpt.emsbilling@gmail.com
-- SigningServiceLocation	https://sign.digitalpaytech.com:5005/services/SigningService
-- SMSCallBackIPAddress	69.28.206.213-69.28.206.215
-- SmsRateInfoCacheSize	500
-- SmsSimulatedEnv	0
-- SSLWebServiceLists	PlateInfoService | AuditInfoService
-- XmlRpcThreadCount	100
-- HashAlgorithmTypeSha1Id	1
-- SigningServiceV2Location 	http://dev.digitalpaytech.com
-- SigningServiceV2AccountName  SigningAdmin
-- SigningServiceV2Token		fWJY7K3SYzB97aEjV1DO+Q==
-- PlateReportReturnSize	1000
-- CollectionUserIntervalMins	10
-- AuthorizeNetSolutionId	A1000071
-- ProdSupportEmailAddress		morgan.madu@digitalpaytech.com
-- FlexDataExtractionProcessingServer	VANAPP01
-- FlexCitationMapWeekInterpolation		34
-- FlexCitationWeekInterpolation	82
-- ElavonVersion	4017
-- ElavonRegistrationKey				TEMP_PORTAL_FAKE_KEY
-- ElavonApplicationID					TZKQ01GC
-- ElavonCloseBatchCount				7500
-- ElavonRequestTimeOut					30
-- PostAuthTimeOut						60
-- RealTimeElavonRetryAttempts			3
-- ElavonRetryWaitingTime				3
-- ElavonConnTimeoutMsRealTime			2000
-- ElavonReadTimeoutMsRealTime			3000
-- ElavonConnTimeoutMsSfSinglePhase		2500
-- ElavonReadTimeoutMsSfSinglePhase		3500
-- ElavonConnTimeoutMsCloseBatch		2000
-- ElavonReadTimeoutMsCloseBatch		2000
-- ElavonConnTimeoutMsDefault			3000
-- ElavonReadTimeoutMsDefault			3000
-- ElavonRedisCloseBatchLockPerTxnMs    350
-- ElavonRedisLockTxnMs                 1100
-- ElavonCloseTimeIntervalMinutes       15
-- 
-- ------------------------------------------------


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
