
/*
	
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inst1
  
  Purpose:    For testing 3 months of transaction data
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inst1 ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inst1 ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ETLProcessDateRangeId , lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 1;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  select BeginDt, EndDt, id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId 
  from ETLProcessDateRange where ClusterId = 1 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 

  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 lv_Parameter1, 	lv_Parameter2 ,	NOW(),    NOW(), 2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

   select BeginDt, EndDt, Id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId
   from ETLProcessDateRange where ClusterId = 1 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 
  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 lv_Parameter1, 	lv_Parameter2,	NOW(), 1 );

  SELECT LAST_INSERT_ID() INTO lv_ETLProcessDateRangeId;  
  
	CALL sp_MigrateRevenueIntoKPI(lv_ETLProcessDateRangeId,1,lv_Parameter1, lv_Parameter2,lv_ETL_daterangeId);
    	
	UPDATE ETLProcessDateRange set status = 2
	WHERE Id = lv_ETL_daterangeId ;
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE ClusterId = 1 and Id = lv_ETLProcessDateRangeId;
	
		
end if;

END//

delimiter ;

-- --------------------------------------

	
/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inst2
  
  Purpose:    For testing 3 Months of Settled Transactions
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inst2 ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inst2 ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ETLProcessDateRangeId , lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 2;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  select BeginDt, EndDt, id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId 
  from ETLProcessDateRange where ClusterId = 2 and status = 0 and ETLObject ='Settled' order by Id limit 1;
 

  INSERT INTO ETLExecutionLog (ClusterId,	Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(2,			lv_Parameter1, 	lv_Parameter2 ,	NOW(),    NOW(), 2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 2 );
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

   select BeginDt, EndDt, Id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId
   from ETLProcessDateRange where ClusterId = 2 and status = 0 and ETLObject ='Settled' order by Id limit 1;
 
  INSERT INTO ETLExecutionLog (ClusterId,	Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (2,			lv_Parameter1, 	lv_Parameter2,	NOW(), 1 );

  SELECT LAST_INSERT_ID() INTO lv_ETLProcessDateRangeId;  
  
	CALL sp_MigrateSettledTransactionIntoKPI(lv_ETLProcessDateRangeId,2,lv_Parameter1, lv_Parameter2);
	    
	
	UPDATE ETLProcessDateRange set status = 2
	WHERE Id = lv_ETL_daterangeId ;
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE ClusterId =2 AND Id = lv_ETLProcessDateRangeId;
	
		
end if;

END//

delimiter ;

-- 3 

-- --------------------------------------

	
/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inst3
  
  Purpose:    For testing 3 Months of Collections Transactions
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inst3 ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inst3 ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ETLProcessDateRangeId , lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 3;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  select BeginDt, EndDt, id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId 
  from ETLProcessDateRange where ClusterId = 3 and status = 0 and ETLObject ='Collection' order by Id limit 1;
 

  INSERT INTO ETLExecutionLog (ClusterId,	Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(3,			lv_Parameter1, 	lv_Parameter2 ,	NOW(),    NOW(), 2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 3 );
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

   select BeginDt, EndDt, Id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId
   from ETLProcessDateRange where ClusterId = 3 and status = 0 and ETLObject ='Collection' order by Id limit 1;
 
  INSERT INTO ETLExecutionLog (ClusterId,	Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (3,			lv_Parameter1, 	lv_Parameter2,	NOW(), 1 );

  SELECT LAST_INSERT_ID() INTO lv_ETLProcessDateRangeId;  
  
	
	CALL sp_MigrateCollectionIntoKPI(lv_ETLProcessDateRangeId, 2,  lv_Parameter1, lv_Parameter2 );
    
	
	UPDATE ETLProcessDateRange set status = 2
	WHERE Id = lv_ETL_daterangeId ;
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE ClusterId =3 AND Id = lv_ETLProcessDateRangeId;
	
		
end if;

END//

delimiter ;

-- ClusterID = 4

DROP PROCEDURE IF EXISTS sp_ETLStart_Inst4 ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inst4 ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ETLProcessDateRangeId , lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 4;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  select BeginDt, EndDt, id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId 
  from ETLProcessDateRange where ClusterId  = 4 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 

  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(4,		 lv_Parameter1, 	lv_Parameter2 ,	NOW(),    NOW(), 2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 4 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

   select BeginDt, EndDt, Id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId
   from ETLProcessDateRange where ClusterId  = 4 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 
  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (4, 		 lv_Parameter1, 	lv_Parameter2,	NOW(), 1 );

  SELECT LAST_INSERT_ID() INTO lv_ETLProcessDateRangeId;  
  
	CALL sp_MigrateRevenueIntoKPI(lv_ETLProcessDateRangeId,1,lv_Parameter1, lv_Parameter2,lv_ETL_daterangeId);
    	
	UPDATE ETLProcessDateRange set status = 2
	WHERE Id = lv_ETL_daterangeId ;
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE ClusterId = 4 and Id = lv_ETLProcessDateRangeId;
	
		
end if;

END//

delimiter ;














-- Jobs
	
DROP EVENT IF EXISTS EVENT_StartETL1 ;

CREATE EVENT EVENT_StartETL1
 ON SCHEDULE EVERY 10 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inst1();
 		
	
DROP EVENT IF EXISTS EVENT_StartETL2 ;

CREATE EVENT EVENT_StartETL2
 ON SCHEDULE EVERY 30 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inst2();
 		

		
	
DROP EVENT IF EXISTS EVENT_StartETL3 ;

CREATE EVENT EVENT_StartETL3
 ON SCHEDULE EVERY 30 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inst3();

 	
	
DROP EVENT IF EXISTS EVENT_StartETL4 ;

CREATE EVENT EVENT_StartETL4
 ON SCHEDULE EVERY 10 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inst4();

-- ASHOK

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inc
  
  Purpose:    For testing online Transactional data
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inc ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inc ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 1;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 NULL, 				NULL ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 NULL, 			NULL,				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	CALL sp_MigrateRevenueIntoKPI_Inc(lv_ETLExecutionLogId);
	
	UPDATE ETLExecutionLog
	SET ETLCompletedPurchase = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	CALL sp_MigrateCollectionIntoKPI_Inc(lv_ETLExecutionLogId);
   
   	UPDATE ETLExecutionLog
	SET ETLCompletedCollection = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

		