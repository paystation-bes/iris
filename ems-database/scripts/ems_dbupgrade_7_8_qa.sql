DROP PROCEDURE IF EXISTS `sp_Preview_CreateAccount`;

DELIMITER $$
CREATE PROCEDURE `sp_Preview_CreateAccount`(
    IN p_Name VARCHAR(25),
    IN p_Title VARCHAR(25),
    IN p_Organization VARCHAR(25),
    IN p_CustomerName VARCHAR(25),
    IN p_ParentCustomerId MEDIUMINT UNSIGNED
)
BEGIN

	DECLARE v_CustomerId MEDIUMINT; -- = sprintf("%04d",(Select Id from Customer where Name = p_CustomerName));
	DECLARE v_TimeZone VARCHAR(20);
	DECLARE v_NewLocationId MEDIUMINT;
	DECLARE v_Location1Id MEDIUMINT;
	DECLARE v_Location2Id MEDIUMINT;
	DECLARE v_NewRouteId MEDIUMINT;
	DECLARE v_Route1Id MEDIUMINT;
	DECLARE v_Route2Id MEDIUMINT;
	DECLARE v_Route3Id MEDIUMINT;
	DECLARE v_Route4Id MEDIUMINT;
	-- Added on March 16 2015
	DECLARE v_ParentCustomerId MEDIUMINT;
	DECLARE v_CustAgreeRecord TINYINT;

    START TRANSACTION;

    -- add check for pay station serial number and customer existance


    --
    -- Create Customer
    --
	SET v_TimeZone = 'Canada/Pacific';
    SET v_CustomerId = 0;
    CALL sp_Preview_InsertCustomer
    ( 
          3,    -- p_CustomerTypeId
          1,    -- p_CustomerStatusTypeId
          p_ParentCustomerId, -- p_ParentCustomerId
          p_CustomerName, -- p_CustomerName
          NULL, -- p_TrialExpiryGMT
          0,    -- p_IsParent
          CONCAT('admin%40', p_CustomerName), -- p_UserName
          '58ae83e00383273590f96b385ddd700802c3f07d', -- p_Password
          1,    -- p_IsStandardReports
          1,    -- p_IsAlerts
          1,    -- p_IsRealTimeCC
          1,    -- p_IsBatchCC
          1,    -- p_IsRealTimeValue
          1,    -- p_IsCoupons
          1,    -- p_IsValueCards
          1,    -- p_IsSmartCards
          1,    -- p_IsExtendByPhone
          1,    -- p_IsDigitalAPIRead
          1,    -- p_IsDigitalAPIWrite
          1,    -- p_Is3rdPartyPayByCell
          1,    -- p_IsDigitalCollect
          1,    -- p_IsOnlineConfiguration
		  1,    -- p_IsFlexIntegration
		  1,    -- p_IsCaseIntegration
		  1,    -- p_IsDigitalAPIXChange
		  1,    -- p_IsOnlineRate
          v_TimeZone,    -- p_Timezone
          1,    -- p_UserId
          'SaltSaltSaltSalt', -- p_PasswordSalt
		  v_CustomerId
    );
	SELECT v_CustomerId;
	
	-- Code added on March 16 2015
	-- Check if this Customer has Parent.
	-- If Yes, then ParentCustomer has to sign T&C
	-- If No, then Customer has to sign T&C
	
	select ifnull(ParentCustomerId,0) into v_ParentCustomerId from Customer where Id = v_CustomerId; 
	
	select count(*) into v_CustAgreeRecord from CustomerAgreement where CustomerId = v_ParentCustomerId ;
	
	IF (v_ParentCustomerId = 0) THEN  -- Meaning this Customer has no parent
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_CustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	ELSEIF (v_ParentCustomerId > 0) and  (v_CustAgreeRecord = 0) THEN -- this customer is a child and Parent Customer has not signed T&C
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_ParentCustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	
	END IF;
    --
    -- Locations
    --
    SET v_NewLocationId = 0;
	CALL sp_Preview_CreateLocation(p_CustomerName, 'Downtown'	,'Pay-By-Plate',	0, NULL, 1, 150, 1000000, v_NewLocationId);
	SELECT v_NewLocationId INTO v_Location1Id;
    CALL sp_Preview_CreateLocation(p_CustomerName, 'Airport'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000, v_NewLocationId);
	SELECT v_NewLocationId INTO v_Location2Id;

	--
	-- Routes
	--
    SET v_NewRouteId = 0;
	CALL sp_Preview_CreateRoute(v_CustomerId, 1, v_Location1Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route1Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 2, v_Location1Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route2Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 1, v_Location2Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route3Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 2, v_Location2Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route4Id;


    -- Alliance		
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 	Field3, 				Field4,																					CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   6,			1,					 'TestAlliance','DGPT', 'TESTDGPT0101', '000059F54D65E979DC0A',	'https://staging1.datawire.net/sd|https://staging2.datawire.net/sd',					80,				'Canada/Pacific', 100000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- First Data (Concord):
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 																			CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   1,			1,					 'TestConcord','digita06', 'A09F291493206FCD7FD6782D9E6FCD7D672D9E0E7B4263BFE76D704F3DB62AD1', 					80,					'Canada/Pacific',200000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- First Data (Nashville):
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 	Field3, 				Field4,																		 Field5, Field6,			CloseQuarterOfDay,  TimeZone,     ReferenceCounter, IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   2,			1,					 'TestNashville','000000000001065', '01005315', '00010372867883130794',	'https://staging2.datawire.net/sd|https://staging1.datawire.net/sd', '980051211', '0005',					80,	  'Canada/Pacific',	300000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- First Horizon:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 			CloseQuarterOfDay,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   10,			1,					 'TestHorizon','000192000904', '110', 				80,					400000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- Link2Gov:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 							Field3,	CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   4,			1,					 'TestLink2Gov','DPTEC-DPTEC-DPTEC-G', 'DPTEC-DPTEC-DPTEC-00', 	'test1234',			80,		'Canada/Pacific', 500000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- Moneris
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 			CloseQuarterOfDay,TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   9,			1,					 'TestMoneris','store2', 'yesguy', 				80,					'Canada/Pacific', 600000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- Paymentech:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 	Field3, 				Field4,			 Field5, 			CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   7,			1,					 'TestPaymentech','700000002018', '001', 'digitech1',	'digpaytec01', 			'0001', 					80,		'Canada/Pacific',	700000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- TD Merchant Services:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 							Field3,	CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   21,			1,					 'TestTD','117681535', 'D9231CC32Ac1431686121828E1a7e171', 	'CAD',			80,			'Canada/Pacific',	800000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- Authorize.NET:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 							Field3,	CloseQuarterOfDay, TimeZone, ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   11,			1,					 'TestAuthorise','r3RuC5XP', 'Chase Paymentech Solutions', 	'RmDP5KySTbAb39v3',			80,'Canada/Pacific',	900000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- PayPros Paradata:
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 			CloseQuarterOfDay,TimeZone,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   8,			1,					 'TestParadata','EF8F679FCE64540D3188EEF98751A965CDCC2B9646B7C6F6EFA34E238DDD59ED9951B85D75BE384B41', 'NOVA', 				80,	'Canada/Pacific',				450000,				1,			0,		UTC_TIMESTAMP(),	1); 

    -- Elavon Converge
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,					Field1,   	Field2,	 	Field3, 	Field4,			  			CloseQuarterOfDay,TimeZone,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   16,			1,					 'TestElavonConverge','007154', 	'webpage', 'RZ80AL',	'N/A', 						80,				'Canada/Pacific',	1000,				1,			0,		UTC_TIMESTAMP(),	1); 

    -- Heartland
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 	Field3, 				Field4,			  																							CloseQuarterOfDay,TimeZone,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   3,			1,					 'TestHeartland','DGPT', 'TESTDGPT0101', '000059F54D65E979DC0A',	'https://111staging1.datawire.net/sd|https://111staging2.datawire.net/sd', 			 					80,				'Canada/Pacific',950000,				1,			0,		UTC_TIMESTAMP(),	1); 

	-- Heartland SPPlus
	INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	 	Field3, 				Field4,			  																						CloseQuarterOfDay,TimeZone,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (v_CustomerId,	   22,			1,					 'TestHeartlandSPP','DGPT', 'TESTDGPT0101', '000059F54D65E979DC0A',	'https://staging1.datawire.net/sd|https://staging2.datawire.net/sd', 			 					80,				'Canada/Pacific',	650000,				1,			0,		UTC_TIMESTAMP(),	1); 



	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',	CONCAT('5000',LPAD(v_CustomerId,5,'0'), '001'), 1, v_TimeZone,49.28600334159728,-123.12086105346685, v_Route1Id, v_Route2Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '001'), 'TestAlliance');
	

	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('5000',LPAD(v_CustomerId,5,'0'), '002'), 1, v_TimeZone,49.28348390950996,-123.11614036560059, v_Route1Id, v_Route2Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '002'), 'TestConcord');

	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('5000',LPAD(v_CustomerId,5,'0'), '003'), 1, v_TimeZone,49.28085236522246,-123.12223434448242, v_Route1Id, v_Route2Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '003'), 'TestNashville');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '004'), 1, v_TimeZone,49.19380640706086,-123.17587852478026, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '004'), 'TestHorizon');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '005'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '005'), 'TestLink2Gov');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '006'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '006'), 'TestMoneris');

	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '007'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName, CONCAT('5000',LPAD(v_CustomerId,5,'0'), '007'), 'TestPaymentech');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '008'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '008'), 'TestTD');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '009'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '009'), 'TestAuthorise');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '010'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '010'), 'TestParadata');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '011'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '011'), 'TestElavonConverge');


	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '012'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '012'), 'TestHeartland');

	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '013'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
	CALL sp_Preview_AssignMerchantPOS(p_CustomerName,  CONCAT('5000',LPAD(v_CustomerId,5,'0'), '013'), 'TestHeartlandSPP');

	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,5,'0'), '014'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
		

    COMMIT;

END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS `sp_Preview_CreateAdminUser`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateAdminUser`(
	IN p_CustomerName VARCHAR(25),
	IN p_FirstName VARCHAR(25),
	IN p_LastName VARCHAR(25),
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16)
	)
BEGIN
      
	DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerRoleId MEDIUMINT UNSIGNED;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;     
    
    SELECT Id FROM Customer WHERE Name = p_CustomerName INTO v_CustomerId;    
	
	SELECT RoleId FROM CustomerRole WHERE CustomerId = v_CustomerId INTO v_CustomerRoleId;
    
    START TRANSACTION;
        
	INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,   FirstName,   LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES       (v_CustomerId,                1,            NULL, p_UserName, p_FirstName, p_LastName, p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

    SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, v_CustomerRoleId,       0, UTC_TIMESTAMP(),             p_UserId);   
                
    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);
    
    COMMIT;     
	
END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS `sp_Preview_CreateParentAccount`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateParentAccount`(
    IN p_Name VARCHAR(25),
    IN p_Title VARCHAR(25),
    IN p_Organization VARCHAR(25),
    IN p_CustomerName VARCHAR(25),
    IN p_ParentCustomerId MEDIUMINT UNSIGNED
)
BEGIN

	DECLARE v_CustomerId MEDIUMINT; 
	DECLARE v_TimeZone VARCHAR(20);
	DECLARE v_NewLocationId MEDIUMINT;
	DECLARE v_Location1Id MEDIUMINT;
	DECLARE v_Location2Id MEDIUMINT;
	DECLARE v_NewRouteId MEDIUMINT;
	DECLARE v_Route1Id MEDIUMINT;
	DECLARE v_Route2Id MEDIUMINT;
	DECLARE v_Route3Id MEDIUMINT;
	DECLARE v_Route4Id MEDIUMINT;
	
	DECLARE v_ParentCustomerId MEDIUMINT;
	DECLARE v_CustAgreeRecord TINYINT;

    START TRANSACTION;


	SET v_TimeZone = 'Canada/Pacific';
    SET v_CustomerId = 0;
    CALL sp_Preview_InsertCustomer
    ( 
          2,    
          1,    
          NULL, 
          p_CustomerName, 
          NULL, 
          1,    
          CONCAT('admin%40', p_CustomerName), 
          '58ae83e00383273590f96b385ddd700802c3f07d', 
          1,    -- p_IsStandardReports
          1,    -- p_IsAlerts
          1,    -- p_IsRealTimeCC
          1,    -- p_IsBatchCC
          1,    -- p_IsRealTimeValue
          1,    -- p_IsCoupons
          1,    -- p_IsValueCards
          1,    -- p_IsSmartCards
          1,    -- p_IsExtendByPhone
          1,    -- p_IsDigitalAPIRead
          1,    -- p_IsDigitalAPIWrite
          1,    -- p_Is3rdPartyPayByCell
          1,    -- p_IsDigitalCollect
          1,    -- p_IsOnlineConfiguration
		  1,    -- p_IsFlexIntegration
		  1,    -- p_IsCaseIntegration
		  1,    -- p_IsDigitalAPIXChange
		  1,    -- p_IsOnlineRate
          v_TimeZone,
          1,
          'SaltSaltSaltSalt',
		  v_CustomerId
    );
	SELECT v_CustomerId;
		
	
	select ifnull(ParentCustomerId,0) into v_ParentCustomerId from Customer where Id = v_CustomerId; 
	
	select count(*) into v_CustAgreeRecord from CustomerAgreement where CustomerId = v_ParentCustomerId ;
	
	IF (v_ParentCustomerId = 0) THEN  
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_CustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	ELSEIF (v_ParentCustomerId > 0) and  (v_CustAgreeRecord = 0) THEN 
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_ParentCustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	
	END IF;
    
    COMMIT;
    
END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS `sp_Preview_CreateSystemAdminUser`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateSystemAdminUser`(
	IN p_FirstName VARCHAR(25),
	IN p_LastName VARCHAR(25),
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16)
	)
BEGIN
      
	DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerRoleId MEDIUMINT UNSIGNED;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;     
    
    SELECT 1 INTO v_CustomerId;    
	
	SELECT RoleId FROM CustomerRole WHERE CustomerId = v_CustomerId INTO v_CustomerRoleId;
    
    START TRANSACTION;
        
	INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,   FirstName,   LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES       (v_CustomerId,                1,            NULL, p_UserName, p_FirstName, p_LastName, p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

    SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, v_CustomerRoleId,       0, UTC_TIMESTAMP(),             p_UserId);   
    
    COMMIT;     
	
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_Preview_InsertCustomer`;



DELIMITER $$

CREATE PROCEDURE `sp_Preview_InsertCustomer`(
	IN p_CustomerTypeId TINYINT UNSIGNED, 
	IN p_CustomerStatusTypeId TINYINT UNSIGNED, 
	IN p_ParentCustomerId MEDIUMINT UNSIGNED, 
	IN p_CustomerName VARCHAR(25), 
	IN p_TrialExpiryGMT DATETIME, 
	IN p_IsParent TINYINT(1) UNSIGNED, 
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_IsStandardReports TINYINT(1), 
	IN p_IsAlerts TINYINT(1),
	IN p_IsRealTimeCC TINYINT(1), 
	IN p_IsBatchCC TINYINT(1), 
	IN p_IsRealTimeValue TINYINT(1),
	IN p_IsCoupons TINYINT(1), 
	IN p_IsValueCards TINYINT(1), 
	IN p_IsSmartCards TINYINT(1), 
	IN p_IsExtendByPhone TINYINT(1), 
	IN p_IsDigitalAPIRead TINYINT(1), 
	IN p_IsDigitalAPIWrite TINYINT(1),
	IN p_Is3rdPartyPayByCell TINYINT(1),
	IN p_IsDigitalCollect TINYINT(1),
	IN p_IsOnlineConfiguration TINYINT(1),
	IN p_IsFlexIntegration  TINYINT(1),
	IN p_IsCaseIntegration  TINYINT(1),
	IN p_IsDigitalAPIXChange  TINYINT(1),
	IN p_IsOnlineRate TINYINT(1),
	IN p_Timezone VARCHAR(40),
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16), 
	OUT v_CustomerId MEDIUMINT
	)
BEGIN
        
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     
    
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
        
    START TRANSACTION;
        
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP());
        
    SELECT LAST_INSERT_ID() INTO v_CustomerId;    
    
    UPDATE Customer SET UnifiId = v_CustomerId WHERE Id = v_CustomerId;
	
    IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
		INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (1,	 v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        SET v_UserAccountId = 1;
    ELSE
		INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    END IF ;
    
        
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   
        
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);

        -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    	INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES          (v_UserAccountId, 				 3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    	-- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    	INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES              (v_CustomerId, 				  3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
    	SELECT 			  v_CustomerId,	UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
    	FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
    	
    	INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
    	FROM UserRole ur 
    	INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    	
    	INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM CustomerRole cr
		INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
		WHERE r.RoleStatusTypeId != 2
		AND r.CustomerTypeId = 3
		AND cr.CustomerId = p_ParentCustomerId;

		INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
		SELECT 							      ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM UserAccount ua 
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    
    END IF;

    
    IF p_CustomerTypeId = 3 THEN
    
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
              
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   
   
        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                    		0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
            
        SELECT LAST_INSERT_ID() INTO v_LocationId;
                
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
                    
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
            
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
        
    ELSE         
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   
    END IF;
    
    
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
            
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,    0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration,0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration,    0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration,    0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange,  0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 2000, p_IsOnlineRate,         0, UTC_TIMESTAMP(), p_UserId);
               
		
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'Communication Alert Default',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,          		  12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;        
    END IF;
    
    COMMIT;     
	
END$$
DELIMITER ;



DROP PROCEDURE IF EXISTS `sp_Robot_CreateParentWithChildCustomers`;

DELIMITER $$

CREATE PROCEDURE `sp_Robot_CreateParentWithChildCustomers`(
	IN p_name VARCHAR(25),
    IN p_timezone VARCHAR(25),
    IN p_childCustomerId MEDIUMINT UNSIGNED
)
BEGIN
    
DECLARE v_CustomerId MEDIUMINT;    
    
 -- Create Parent Customer
DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0; END;   
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0; END; 

 
SET v_CustomerId = 0; 
CALL sp_Preview_InsertCustomer
    ( 
		  2,    -- p_CustomerTypeId
          1,    -- p_CustomerStatusTypeId
          NULL, -- p_ParentCustomerId
          p_name, -- p_CustomerName
          NULL, -- p_TrialExpiryGMT
          1,    -- p_IsParent
          CONCAT('admin%40', p_name), -- p_UserName
          '58ae83e00383273590f96b385ddd700802c3f07d', -- p_Password
          1,    -- p_IsStandardReports
          1,    -- p_IsAlerts
          1,    -- p_IsRealTimeCC
          1,    -- p_IsBatchCC
          1,    -- p_IsRealTimeValue
          1,    -- p_IsCoupons
          1,    -- p_IsValueCards
          1,    -- p_IsSmartCards
          1,    -- p_IsExtendByPhone
          1,    -- p_IsDigitalAPIRead
          1,    -- p_IsDigitalAPIWrite
          1,    -- p_Is3rdPartyPayByCell
          1,    -- p_IsDigitalCollect
          1,    -- p_IsOnlineConfiguration
		  1,    -- p_IsFlexIntegration
		  1,    -- p_IsCaseIntegration
		  1,    -- p_IsDigitalAPIXChange
		  1,	-- p_IsOnlineRate
          'Canada/Pacific',    -- p_Timezone
          1,    -- p_UserId
          'SaltSaltSaltSalt', -- p_PasswordSalt
		  v_CustomerId
    );

SELECT v_CustomerId;

UPDATE Customer
SET CustomerStatusTypeId=3, ParentCustomerId=v_CustomerId
WHERE Id=p_childCustomerId;

COMMIT;
END$$
DELIMITER ;


