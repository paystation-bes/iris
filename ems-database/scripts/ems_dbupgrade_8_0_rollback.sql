ALTER TABLE `POSServiceState` DROP COLUMN `IsNewBINRange`;
DROP TABLE IF EXISTS `LinuxConfigurationFile`;
ALTER TABLE `POSServiceState` DROP COLUMN `NewConfigurationId`;
ALTER TABLE `POSServiceState` DROP COLUMN `IsNewOTAFirmwareUpdate`;
ALTER TABLE `POSServiceState` DROP COLUMN `IsNewBadCardList`;
ALTER TABLE `POSServiceState` DROP COLUMN `IsNewOTAFirmwareUpdateCancelPending`;

-- -----------------------------------------------------
-- Table `Customer`
-- -----------------------------------------------------
ALTER TABLE `Customer` DROP COLUMN `NewBadCardListHash`,
                       DROP COLUMN `NewBadCardListCreationDate`;
ALTER TABLE Customer MODIFY UnifiId VARCHAR(50);


-- -----------------------------------------------------
-- Table `PaymentType`
-- -----------------------------------------------------
-- CC (FSwipe)
DELETE FROM PaymentType WHERE Id = 16;
-- Cash/CC (FSwipe)
DELETE FROM PaymentType WHERE Id = 17;

-- -------------------------------------------------------------------------------------
-- ROLLBACK CREATE Stored Procedures `sp_InsertMovedPointOfSale`
-- -------------------------------------------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS `sp_InsertMovedPointOfSale`$$
DELIMITER ;                       

-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ROLLBACK ALTER Stored Procedures `sp_MovePointOfSale`
-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS `sp_MovePointOfSale`$$
CREATE PROCEDURE `sp_MovePointOfSale`(IN p_CustomerId_new MEDIUMINT UNSIGNED, IN p_CustomerId_old MEDIUMINT UNSIGNED, IN p_SerialNumber VARCHAR(20), IN p_ProvisionedGMT DATETIME, IN p_UserId MEDIUMINT UNSIGNED)
BEGIN     
       
    DECLARE v_PointOfSaleId MEDIUMINT UNSIGNED;
    DECLARE v_PaystationId MEDIUMINT UNSIGNED;
    DECLARE v_ModemTypeId TINYINT UNSIGNED;
    DECLARE v_PaystationTypeId TINYINT UNSIGNED;
       
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;     
                       
    IF (SELECT  COUNT(*) 
        FROM    Customer C, PointOfSale P, POSStatus S, Paystation A
        WHERE   C.Id = P.CustomerId
        AND     P.Id = S.PointOfSaleId 
        AND     P.PaystationId = A.Id
        AND     P.SerialNumber = A.SerialNumber 
        AND     P.SerialNumber = p_SerialNumber
        AND     C.Id = p_CustomerId_old
        AND     S.IsActivated = 0
        AND     P.Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber)
        ) = 1 
        
    THEN
        SELECT  P.Id, A.Id, A.ModemTypeId, A.PaystationTypeId INTO v_PointOfSaleId, v_PaystationId, v_ModemTypeId, v_PaystationTypeId
        FROM    Customer C, PointOfSale P, POSStatus S, Paystation A
        WHERE   C.Id = P.CustomerId
        AND     P.Id = S.PointOfSaleId 
        AND     P.PaystationId = A.Id
        AND     P.SerialNumber = A.SerialNumber 
        AND     P.SerialNumber = p_SerialNumber
        AND     C.Id = p_CustomerId_old
        AND     S.IsActivated = 0
        AND     P.Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber);
                               
        CALL sp_InsertPointOfSale (p_CustomerId_new, v_PaystationTypeId, v_ModemTypeId, p_SerialNumber, p_ProvisionedGMT, p_UserId, v_PaystationId    , v_PointOfSaleId    );                                             
        
    ELSE         
        SELECT 0 AS PointOfSaleId;          
    END IF;
END$$
DELIMITER ;                       
                       
-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ROLLBACK ALTER Stored Procedures `sp_InsertCustomer`
-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_InsertCustomer;

delimiter //
CREATE PROCEDURE sp_InsertCustomer (
  IN p_CustomerTypeId TINYINT UNSIGNED,
  IN p_CustomerStatusTypeId TINYINT UNSIGNED,
  IN p_ParentCustomerId MEDIUMINT UNSIGNED,
  IN p_CustomerName VARCHAR(25),
  IN p_TrialExpiryGMT DATETIME,
  IN p_IsParent TINYINT(1) UNSIGNED,
  IN p_UserName VARCHAR(765),
  IN p_Password VARCHAR(128),
  IN p_IsStandardReports TINYINT(1),
  IN p_IsAlerts TINYINT(1),
  IN p_IsRealTimeCC TINYINT(1),
  IN p_IsBatchCC TINYINT(1),
  IN p_IsRealTimeValue TINYINT(1),
  IN p_IsCoupons TINYINT(1),
  IN p_IsValueCards TINYINT(1),
  IN p_IsSmartCards TINYINT(1),
  IN p_IsExtendByPhone TINYINT(1),
  IN p_IsDigitalAPIRead TINYINT(1),
  IN p_IsDigitalAPIWrite TINYINT(1),
  IN p_Is3rdPartyPayByCell TINYINT(1),
  IN p_IsDigitalCollect TINYINT(1),
  IN p_IsOnlineConfiguration TINYINT(1),
  IN p_IsFlexIntegration TINYINT(1) ,
  IN p_IsCaseIntegration TINYINT(1) ,
  IN p_IsDigitalAPIXChange TINYINT(1),
  IN p_IsOnlineRate TINYINT(1),
  IN p_Timezone VARCHAR(40),
  IN p_UserId MEDIUMINT UNSIGNED,
  IN p_PasswordSalt VARCHAR(16),
  IN p_UnifiId VARCHAR(50)
)
BEGIN
    -- Version: 1 - Tuesday February 19 2013  
    
    -- Local variables
    DECLARE v_CustomerId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserRoleId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    -- If the time zone specified in parameter `p_Timezone` does not exist in view `Timezone_v.Name` then a default time zone of Id = 473 (Name = 'GMT') will be assigned to the Customer 
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      -- prepare default timezone with Timezone_v.Id = 473 = 'GMT'
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';
	
	DECLARE v_isLinkCryp TINYINT  DEFAULT 0 ;
    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     -- exit on SQL warning
    
    -- Verify the time zone specified by incoming parameter `p_Timezone` is valid (validate against the view `Timezone_v`), otherwise use a default value for Customer.TimezoneId (assigned in DECLARE statement above) 
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
    
    -- Start the atomic transaction
    START TRANSACTION;
    
	-- Code added on May 03 2019 for Link Crypto Switch
	SELECT Value INTO v_isLinkCryp FROM EmsProperties WHERE Name = 'UseLinkCrypto' ;
	
    -- Insert Customer
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate, UnifiId, IsLinkCrypto)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP(), p_UnifiId, v_isLinkCryp);
    
    -- Get Customer.Id
    SELECT LAST_INSERT_ID() INTO v_CustomerId;
	
	
    
    -- Insert UserAccount: each new customer gets a user account with a first name of 'Administrator' (and no last name, i.e., an empty string for last name), immediately below this user will be assigned an administrator role
    
    -- Added on July 23 by Ashok (to take care of autoincrement ID)
    -- Added to force UserAccount.Id = 1 if there are no recrods in UserAccount Table. This is used as FK is many tables.
    
    IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
        INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
        VALUES             (1,   v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);
        SET v_UserAccountId = 1;
    ELSE
        INSERT UserAccount (CustomerId,   UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
        VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,  p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        -- Get UserAccount.Id
        SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    END IF ;

    
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    
    -- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

        -- Insert UserDefaultDashboard (every user has access to the default dashboard)
    -- WARNING: TABLE 'UserDefaultDashboard' ONLY NEEDS TO BE IN THE 'KPI DB' DATABASE BUT IT IS BEING REFERENCED HERE IN THE 'EMS DB'
    --          WHEN ALL TABLES ARE FINALLY SEGREGATED BETWEEN THE 'KPI DB' AND THE 'EMS DB' DATABASES INSERTING INTO TABLE 'UserDefaultDashboard' WILL BE PERFORMED BY A TRIGGER ON THE `UserAccount` TABLE WITHIN THE 'KPI DB'
    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);

    -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
        INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_UserAccountId,                3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
        -- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
        INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES              (v_CustomerId,                3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
        SELECT            v_CustomerId, UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
        FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
        
        INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
        FROM UserRole ur 
        INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
        INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
        INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
        WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

        INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
        FROM CustomerRole cr
        INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
        WHERE r.RoleStatusTypeId != 2
        AND r.CustomerTypeId = 3
        AND cr.CustomerId = p_ParentCustomerId;

        INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                                ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
        FROM UserAccount ua 
        INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
        WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

END IF;
    
  
    -- A 'Child' customer type receives a full set of inserts (but 'Parent' and 'DPT' customer types do not)
    IF p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerCardType
        -- These are `locked` customer card types for credit cards, smart cards, and value cards
        -- These `CustomerCardType` records are used as foreign key parents for tables `CustomerCard` (lists of good/valid credit cards and smart cards) and `CustomerBadCard` (lists of bad/banned credit cards, smart cards, and value cards)
        -- Important: valid value cards (in table CustomerCard) have their own non-locked CustomerCardType record, but bad value cards used the locked 'generic' CustomerCardType record created here
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
      
        -- Insert CustomerProperty: every 'Child' customer has 7 customer properties
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);   -- Timezone
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Query Spaces By (1=Location)
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max User Accounts
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);   -- Credit Card Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);   -- SMS Warning Period
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Hidden Paystations Reported

        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                     11,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Preferred

        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                     12,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Limited

   
        -- Insert Location: every customer has a location named 'Unassigned'
        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                          0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
    
        -- Get Location.Id
        SELECT LAST_INSERT_ID() INTO v_LocationId;
        
        -- Insert LocationDay and LocationOpen: the 'Unassigned' location is open 24 x 7 
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
    
        -- Insert PaystationSetting: every customer has a default paystation setting named 'None' (because some pay-stations/point-of-sales do not have a paystation setting)
        -- There can only be one 'None' PaystationSetting for a Customer, therefore this SQL check
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
    
        -- Insert UnifiedRate: every customer has a default unified rate named 'Unknown' (because older versions of the 'PS App' do not send to EMS the name of the rate used when transacting a purchase)
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
    
    -- 'DPT' and 'Parent' Customers receive a small set of customer properties    
    ELSE 
        -- Insert CustomerProperty: time zone only
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   -- Timezone  
    END IF;
    
    -- 'Parent' and 'Child' customers subscribe to services (but a 'DPT' Customer does not)  
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerSubscription: set all EMS services for the customer, insert the record even if the service is not subscribed to 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId, LicenseCount, LicenseUsed) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,  0, UTC_TIMESTAMP(), p_UserId, 0, 0);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 2000, p_IsOnlineRate,        0, UTC_TIMESTAMP(), p_UserId);
        
        
        -- Insert CustomerAlertType: every customer gets a customer-alert-type of `Pay Station Alert`, BUT it will only be referenced as a foreign key by tables `ActivePOSAlert` and `POSAlert` IF
        --                           the customer has subscribed to `Alerts` as a subscription type (see table `CustomerSubscription`, and table.attribute `SubscriptionType.Id` = 200 = `Alerts`)
        -- This only applies to a child customer, because parent and DPT customers do not have pay stations
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'No Communication In 24 Hours',        25,            1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1,         1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
-- No default alert for Overdue collection
--            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
--            VALUES                   (v_CustomerId,                   13,       NULL,    NULL,       'No Collection in 7 Days',         7,           1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;
        
    END IF;
    
    -- Commit the atomic transaction;
    COMMIT;
    
    -- Return the Id of the newly created Customer record
    SELECT v_CustomerId AS CustomerId;
    
END//
delimiter ;

-- Roll Back for Coin Escrow Disabled Status

-- EventDefinition eventdefinition.coinescrow.disabled.on
 DELETE FROM EventDefinition WHERE Id = 127;
-- EventStatusType Disabled
 DELETE FROM EventStatusType WHERE Id = 35;

-- Roll Back Complete for Coin Escrow Disabled Status 

-- Rollback start
DROP PROCEDURE IF EXISTS sp_InsertPointOfSale;
delimiter //

CREATE  PROCEDURE `sp_InsertPointOfSale`(IN p_CustomerId MEDIUMINT UNSIGNED, IN p_PaystationTypeId TINYINT UNSIGNED, IN p_ModemTypeId TINYINT UNSIGNED, IN p_SerialNumber VARCHAR(20), IN p_ProvisionedGMT DATETIME, IN p_UserId MEDIUMINT UNSIGNED, IN p_PaystationId_old MEDIUMINT UNSIGNED, IN p_PointOfSaleId_old MEDIUMINT UNSIGNED)
BEGIN
    -- Version: 1 - Tuesday February 19 2013 - added check to ensure the customer does not already have a point-of-sale with this serial number (specified by incoming parameter p_SerialNumber)
    
    -- Local variablesa
    DECLARE v_PaystationId MEDIUMINT UNSIGNED DEFAULT NULL;
    DECLARE v_PointOfSaleId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_CurrentGMT DATETIME;
    DECLARE v_IsLinkCrypto TINYINT DEFAULT 0;
    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;     -- exit on SQL warning
    
    -- Perform an important valdiation: the customer CANNOT already be the CURRENT owner of this pay station (prevents the creation of 2 point-of-sales with the same serial number for the same customer at the same time)
    IF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0) AND (
        SELECT  COUNT(*)
        FROM    PointOfSale
        WHERE   CustomerId = p_CustomerId
        AND     SerialNumber = p_SerialNumber
        AND     Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber)
        ) = 1
    THEN
    
        -- This customer already has a point-of-sale with this serial number
        SET v_PointOfSaleId = 0;
    
    -- Validate incoming parameters `p_PaystationId_old` and `p_PointOfSaleId_old`
    ELSEIF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0)   -- new point-of-sale (INSERT both `Paystation` and `PointOfSale`)
       OR ((SELECT COUNT(*) FROM Paystation WHERE Id = p_PaystationId_old) = 1 AND (SELECT COUNT(*) FROM PointOfSale WHERE Id = p_PointOfSaleId_old) = 1)   -- move point-of-sale (moving a point-of-sale from one customer to another (INSERT `PointOfSale` record only))
    THEN 
    
        -- Set v_CurrentGMT
        SET v_CurrentGMT = UTC_TIMESTAMP();
    
        -- Start the atomic transaction
        START TRANSACTION;
    
        -- When creating a new point-of-sale (a `PointOfSale` record) it either has: (1) it's own pay station, or (2) it uses an existing pay station that is being moved from one customer to another
        -- Only insert a new `Paystation` record when NOT moving an existing pay station from one customer to another (about 7% of pay stations end up being moved from one customer to another).
        IF p_PaystationId_old = 0 THEN 
            INSERT Paystation (  PaystationTypeId,   ModemTypeId,   SerialNumber, IsDeleted, VERSION, LastModifiedGMT , LastModifiedByUserId)
            VALUES            (p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, 0        , 0      , p_ProvisionedGMT, p_UserId            );
            -- Get PaystationId of newly created pay station record
            SELECT LAST_INSERT_ID() INTO v_PaystationId;
        ELSE
            -- When moving a pay station from one customer to another move use the Id of the existing pay station being moved
            SELECT p_PaystationId_old INTO v_PaystationId;
        END IF;
    
        -- INSERT PointOfSale
        -- The new PointOfSale gets assigned to the 'Unassigned' location, and is NOT assigned to any Settings File 
        INSERT PointOfSale (  CustomerId,   PaystationId, SettingsFileId, LocationId, Name          ,   SerialNumber,   ProvisionedGMT, Latitude, Longitude, AltitudeOrLevel, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT              p_CustomerId, v_PaystationId, NULL          , L.Id      , p_SerialNumber, p_SerialNumber, p_ProvisionedGMT, NULL    , NULL     , NULL           , 0      , v_CurrentGMT   , p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;
    
        -- Get PointOfSaleId
        SELECT LAST_INSERT_ID() INTO v_PointOfSaleId;
    
        -- INSERT LocationPOSLog: logs the history of point-of-sales assigned to a location (this is a carry over from EMS 6, a future release of EMS 7 will implement proper logging, ) 
        INSERT  LocationPOSLog (LocationId, PointOfSaleId, AssignedGMT, LastModifiedByUserId)
        SELECT  L.Id, v_PointOfSaleId, p_ProvisionedGMT, p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;
		
		-- EMS 5520 Added LastCollectionGMT on May 21 2014
        -- INSERT POSBalance: every new `PointOfSale` record has a `POSBalance` record (1:1 relationship)
        INSERT POSBalance (  PointOfSaleId, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionGMT, LastRecalcGMT   , NextRecalcGMT   , LastCollectionTypeId)
        VALUES            (v_PointOfSaleId, p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT, 0                   );
    
        -- INSERT POSHeartbeat: every new `PointOfSale` record has a `POSHeartbeat` record (1:1 relationship)   
        INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (v_PointOfSaleId, p_ProvisionedGMT);
    
        -- INSERT POSSensorState: every new `PointOfSale` record has a `POSSensorState` record (1:1 relationship)
        INSERT POSSensorState (  PointOfSaleId, AmbientTemperature, Battery1Voltage, Battery2Voltage, ControllerTemperature, InputCurrent, RelativeHumidity, SystemLoad, WirelessSignalStrength, VERSION, LastModifiedGMT)
        VALUES                (v_PointOfSaleId, 32767             , 32767          , 32767          , 32767                , 32767       , 32767           , 32767     , NULL, 0      , v_CurrentGMT   );                                                    

        -- Code added on 03 May 2019 for Link Crypto Switch
		SELECT IsLinkCrypto INTO v_IsLinkCrypto FROM Customer where Id = p_CustomerId ;
		
		-- INSERT POSServiceState: every new `PointOfSale` record has a `POSServiceState` record (1:1 relationship)
        INSERT POSServiceState (  PointOfSaleId, BBSerialNumber, LastPaystationSettingUploadGMT, NextSettingsFileId, PrimaryVersion, SecondaryVersion, UpgradeGMT, VERSION, LastModifiedGMT, IsSwitchLinkCrypto)
        VALUES                 (v_PointOfSaleId, NULL          , NULL                          , NULL              , NULL          , NULL            , NULL      , 0      , v_CurrentGMT,    v_IsLinkCrypto   );                                                    

        --  INSERT POSDate: apply default settings for the new point of sale, insert 10 rows into table `POSDate` (table `POSDate` is a historical logging table, while table `POSStatus` maintains the current state)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  1, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  2, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  3, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  4, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  5, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  6, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  7, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  8, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  9, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId, 10, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);

        --  INSERT POSStatus: apply default settings for the new point of sale, insert 1 row into table `POSStatus` (this row is a mirror of the rows inserted into table `POSDate` above)
        INSERT POSStatus (  PointOfSaleId, IsProvisioned, IsLocked, IsDecommissioned, IsDeleted, IsVisible, IsBillableMonthlyOnActivation, IsDigitalConnect, IsTestActivated, IsActivated, IsBillableMonthlyForEms, IsProvisionedGMT, IsLockedGMT     , IsDecommissionedGMT, IsDeletedGMT    , IsVisibleGMT    , IsBillableMonthlyOnActivationGMT, IsDigitalConnectGMT, IsTestActivatedGMT, IsActivatedGMT  , IsBillableMonthlyForEmsGMT, VERSION, LastModifiedGMT, LastModifiedByUserId) 
        VALUES           (v_PointOfSaleId, 1            , 0       , 0               , 0        , 1        , 1                            , 0               , 0              , 0          , 0                      , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT   , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT                , p_ProvisionedGMT   , p_ProvisionedGMT  , p_ProvisionedGMT, p_ProvisionedGMT          , 0      , v_CurrentGMT   , p_UserId            );
 
        --  INSERT POSAlertStatus: apply default settings for the new point of sale, insert 1 row into table POSStatus but do not insert rows into POSDate
        INSERT POSAlertStatus (  PointOfSaleId, CommunicationMinor, CommunicationMajor, CommunicationCritical, CollectionMinor, CollectionMajor, CollectionCritical, PayStationMinor, PayStationMajor, PayStationCritical, VERSION) 
        VALUES                (v_PointOfSaleId, 0                 , 0                 ,0                     , 0              , 0              , 0                 , 0              , 0              , 0                 , 0      );
 

        --  INSERT POSEVentCurrent: only applies to user defined customer alert type's without routes (because the point of sale is new and it has not yet been assigned to a route) 
        INSERT  POSEventCurrent (CustomerAlertTypeId,       EventTypeId,   PointOfSaleId,      EventSeverityTypeId, IsActive, AlertGMT, ClearedGMT, VERSION) 
        SELECT                   Id                 , 46 AS EventTypeId, v_PointOfSaleId, 2 AS EventSeverityTypeId, 0       , NULL    , NULL      , 0        
        FROM    CustomerAlertType 
        WHERE   CustomerId = p_CustomerId 
        AND     AlertThresholdTypeId != 12       -- user defined alert types only (not pay station alerts)
        AND     IsActive = 1             -- only for active alerts
        AND     IsDeleted = 0            -- only for undeleted alerts
        AND     RouteId IS NULL         -- this point-of-sale is new and has not yet been assigned to a route
        AND     LocationId IS NULL      -- specifying a Location for a CustomerAlertType is not yet supported in EMS 7 (this is a carry over from EMS 6 and the feature was not supported in EMS 6 either)
        ORDER BY Id;

        UPDATE POSEventCurrent
        SET IsHidden = 1
        WHERE 
		Id IN (SELECT A.Id FROM (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId 
                                       FROM POSEventCurrent pec
									   INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
									   WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 1 AND pec.PointOfSaleId = v_PointOfSaleId) AS A
						   INNER JOIN (SELECT pec.PointOfSaleId, cat.AlertThresholdTypeId FROM POSEventCurrent pec
									   INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
									   WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 0 AND pec.PointOfSaleId = v_PointOfSaleId) B ON (A.PointOfSaleId = B.PointOfSaleId AND A.AlertThresholdTypeId = B.AlertThresholdTypeId)
					GROUP BY A.Id)
                    ;
					
        --  IF moving a pay station from customer to customer THEN deactivate the old (preexisting) point-of-sale
        IF p_PointOfSaleId_old > 0 THEN 
            -- The new point-of-sale is now provisioned by moving a paystation to it from another customer, simply add an informational record saying the point-of-sale was provisioned because of a move from another customer 
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId    , 201, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            
            -- The old point-of-sale is now decommissioned (the underlying pay station was moved to a new customer), add an informational record saying the point-of-sale was decommissioned because of a move from another customer
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old,   3, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old, 202, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            UPDATE POSStatus SET IsDecommissioned = 1, IsDecommissionedGMT = p_ProvisionedGMT, LastModifiedGMT = v_CurrentGMT, LastModifiedByUserId = p_UserId WHERE PointOfSaleId = p_PointOfSaleId_old;
        END IF;

        -- Commit the atomic transaction;
        COMMIT;
    END IF;
    
    -- Return the Id of the newly created PointOfSale record
    -- Will return 0 (zero) if the incoming parameters did not pass validation
    SELECT v_PointOfSaleId AS PointOfSaleId;
    
END//
delimiter ;

-- Rollback end
