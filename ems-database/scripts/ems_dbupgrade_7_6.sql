
ALTER TABLE POSServiceState ADD IsNewMerchantAccount TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER IsNewTicketFooter;

ALTER TABLE POSServiceState ADD MerchantAccountChangedGMT DATETIME NULL AFTER UpgradeGMT;

ALTER TABLE POSServiceState ADD MerchantAccountRequestedGMT DATETIME NULL AFTER MerchantAccountChangedGMT;

ALTER TABLE POSServiceState ADD MerchantAccountUploadedGMT DATETIME NULL AFTER MerchantAccountRequestedGMT;

