
-- -------------------------------------------------------------------------
-- Last Modifed on May 22 2015
-- -------------------------------------------------------------------------


-- We specify a Limit as an Input Parameter to delete the data in Batches 
-- Also specify Sleep in seconds for data to catchup for all the nodes

-- call sp_DeleteWidgetData_Inc(5000000,10);

DROP PROCEDURE IF EXISTS sp_DeleteWidgetData_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteWidgetData_Inc(in P_Limit INT, in P_Sleep TINYINT)

BEGIN

DECLARE lv_TimeId_prev_48hour,lv_TimeId_prev_62day,lv_TimeId_prev_24month,lv_TimeId_prev_3day MEDIUMINT UNSIGNED ;
DECLARE lv_past8day_date DATETIME ;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      
	  INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
	 VALUES ('InErrorHandling',now());
		
	  COMMIT ; 
	  -- set autocommit = 1 ;
  END;
  
-- Server Time is GMT ?
-- Hour --> Keep Last 48 hrs
-- Day  --> Keep Last 62 days
-- Month --> Keep Last 24 months

-- 48 hour
SELECT Id into lv_TimeId_prev_48hour from Time where Date = DATE_ADD(current_date(),INTERVAL -48 HOUR) and QuarterOfDay = 0;

-- 62 day
SELECT Id into lv_TimeId_prev_62day from Time where Date = DATE_ADD(current_date(),INTERVAL -62 DAY) and QuarterOfDay = 0;

-- 24 Month
select min(Id) into lv_TimeId_prev_24month from Time where Year = (SELECT Year from Time where Date = DATE_ADD(current_date(),INTERVAL -24 MONTH) and QuarterOfDay = 0 )
and Month = (SELECT Month from Time where Date = DATE_ADD(current_date(),INTERVAL -24 MONTH) and QuarterOfDay = 0 ) ;

-- 3 day
SELECT Id into lv_TimeId_prev_3day from Time where Date = DATE_ADD(current_date(),INTERVAL -3 DAY) and QuarterOfDay = 0;

-- 8 day
select date_sub(date(UTC_TIMESTAMP()), interval 8 day) into lv_past8day_date ;

select now() as DeleteStarted ;

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Data Delete Started in Widget Tables',now());

call sp_PPPRevenueHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPRevenueHour',now());

call sp_PPPDetailHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPDetailHour',now());


call sp_PPPRevenueDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPRevenueDay',now());

call sp_PPPDetailDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPDetailDay',now());

call sp_PPPRevenueMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPRevenueMonth',now());

call sp_PPPDetailMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPDetailMonth',now());


-- NEW CODE on march 6

call sp_PPPTotalHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPTotalHour',now());

-- end 

select 'Delete Completed for PPPTotalHour' as Status ;


-- NEW CODE on march 6

call sp_PPPTotalDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPTotalDay',now());

-- end 

select 'Delete Completed for PPPTotalDay' as Status ;


-- NEW CODE on march 6

call sp_PPPTotalMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for PPPTotalMonth',now());

-- end

select 'Delete Completed for PPPTotalMonth' as Status ;

-- start

-- added on Aug 28 2014 to remove past widget data for Card Processing Widget 
call sp_CardTransactionRevenueHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionRevenueHour',now());

call sp_CardTransactionDetailHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionDetailHour',now());

call sp_CardTransactionRevenueDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionRevenueDay',now());

call sp_CardTransactionDetailDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionDetailDay',now());

call sp_CardTransactionRevenueMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionRevenueMonth',now());

call sp_CardTransactionDetailMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionDetailMonth',now());

call sp_CardTransactionTotalHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionTotalHour',now());

select 'Delete Completed for CardTransactionTotalHour' as Status ;

call sp_CardTransactionTotalDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionTotalDay',now());

select 'Delete Completed for CardTransactionTotalDay' as Status ;

call sp_CardTransactionTotalMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CardTransactionTotalMonth',now());

select 'Delete Completed for CardTransactionTotalMonth' as Status ;


-- end
-- Occupancy

-- NEW CODE on march 6

call sp_OccupancyHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for OccupancyHour',now());

-- end 

select 'Delete Completed for OccupancyHour' as Status ;

-- new code march 6

call sp_OccupancyDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for OccupancyDay',now());

-- end


select 'Delete Completed for OccupancyDay' as Status ;


-- new code march 6

call sp_OccupancyMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for OccupancyMonth',now());


select 'Delete Completed for OccupancyMonth' as Status ;

-- Utilization
-- new code

call sp_UtilizationDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for UtilizationDay',now());




select 'Delete Completed for UtilizationDay' as Status ;


-- new code
-- EMS 5356
-- call sp_UtilizationMonthForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);
call sp_UtilizationMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for UtilizationMonth',now());

-- end



select 'Delete Completed for UtilizationMonth' as Status ;

-- Collection


-- new code

call sp_TotalCollectionForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for TotalCollection',now());

-- end 

select 'Delete Completed for TotalCollection' as Status ;

-- SettledTransaction
-- new code
call sp_SettledTransactionForDelete_Inc(lv_TimeId_prev_3day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for SettledTransaction',now());
-- end


select 'Delete Completed for SettledTransaction' as Status ;

-- ETLLocationDetail Hourly
-- new Code

call sp_ETLLocationDetailForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep,1);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for ETLLocationDetailHourly',now());

-- end


select 'Delete Completed for ETLLocationDetail-Hourly' as Status ;

-- ETLLocationDetail Daily
-- new code
call sp_ETLLocationDetailForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep,2);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for ETLLocationDetailDaily',now());

-- end

select 'Delete Completed for ETLLocationDetailDaily' as Status ;

-- ETLLocationDetail Monthly
-- new code
call sp_ETLLocationDetailForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep,3);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for ETLLocationDetailMonthly',now());
-- end 

select 'Delete Completed for ETLLocationDetailmonthly' as Status ;

-- New Code Added on March 4 2015

-- CitationHour

call sp_CitationHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CitationTotalHour',now());

select 'Delete Completed for CitationTotalHour' as Status ;

-- CitationDay
call sp_CitationDayForDelete_Inc(lv_TimeId_prev_62day,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CitationTotalDay',now());

select 'Delete Completed for CitationTotalDay' as Status ;

-- CitationMonth
call sp_CitationMonthForDelete_Inc(lv_TimeId_prev_24month,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CitationTotalMonth',now());

select 'Delete Completed for CitationTotalMonth' as Status ;

-- CitationMap
-- Added on March 11 2015
call sp_CitationMapForDelete_Inc(lv_past8day_date,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CitationMap',now());

select 'Delete Completed for CitationMap' as Status ;

-- ###### Added on May 22 2015

-- CaseOccupancy

call sp_CaseOccupancyHourForDelete_Inc(lv_TimeId_prev_48hour,P_Limit,P_Sleep);

INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
VALUES ('Delete completed for CaseOccupancyHour',now());

select 'Delete Completed for CaseOccupancyHour' as Status ;


-- ######

select now() as DeleteEnded ;

END//
delimiter ;

--



-- -- -- -- -- -- --
-- 1) To delete PPPRevenueHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPRevenueHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPRevenueHourForDelete_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPRevenueHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPRevenueHour_Inc(lv_TimeId_prev_48hour, P_Limit);

select sleep(P_Sleep) as SLEEP ;

/*select count(PPPRevenueHour.Id) into  lv_Cnt_PPPRevenueHour
FROM PPPRevenueHour INNER JOIN PPPDetailHour INNER JOIN PPPTotalHour
WHERE 
PPPRevenueHour.PPPDetailHourId = PPPDetailHour.Id 
AND PPPDetailHour.PPPTotalHourId = PPPTotalHour.Id 
AND PPPTotalHour.CustomerId = P_CustomerId 
AND PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour ;
*/

select count(*) into  lv_Cnt_PPPRevenueHour
from PPPRevenueHour  where PPPDetailHourId in ( select Id from  PPPDetailHour where PPPTotalHourId in 
(select Id from PPPTotalHour where TimeIdGMT <= lv_TimeId_prev_48hour )) ;

UNTIL lv_Cnt_PPPRevenueHour = 0 END REPEAT;

select 'Delete Completed for PPPRevenueHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPRevenueHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPRevenueHour_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT)

 BEGIN

 declare lv_PPPRevenueHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*select PPPRevenueHour.Id 
FROM PPPRevenueHour INNER JOIN PPPDetailHour INNER JOIN PPPTotalHour
WHERE 
PPPRevenueHour.PPPDetailHourId = PPPDetailHour.Id 
AND PPPDetailHour.PPPTotalHourId = PPPTotalHour.Id 
AND PPPTotalHour.CustomerId = P_CustomerId
AND PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour
Limit P_Limit;
*/

select Id from PPPRevenueHour  where PPPDetailHourId in ( select Id from  PPPDetailHour where PPPTotalHourId in 
(select Id from PPPTotalHour where TimeIdGMT <= lv_TimeId_prev_48hour )) Limit P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;

while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPRevenueHourId;

	DELETE FROM PPPRevenueHour where Id = lv_PPPRevenueHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 COMMIT ;		-- ASHOK2	   
 set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- -- -- -- -- -- --
-- 2) To delete PPPDetailHour
-- -- -- -- -- -- --

DROP PROCEDURE IF EXISTS sp_PPPDetailHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPDetailHourForDelete_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPDetailHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPDetailHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	/*Select count(PPPDetailHour.Id) into lv_Cnt_PPPDetailHour
	FROM  PPPDetailHour INNER JOIN PPPTotalHour
	WHERE 
	PPPDetailHour.PPPTotalHourId = PPPTotalHour.Id 
	AND PPPTotalHour.CustomerId = P_CustomerId
	AND PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour ;*/
	
	select count(*) into  lv_Cnt_PPPDetailHour 
	from  PPPDetailHour where PPPTotalHourId in (select Id from PPPTotalHour where TimeIdGMT < lv_TimeId_prev_48hour  );
	

UNTIL lv_Cnt_PPPDetailHour = 0 END REPEAT;

select 'Delete Completed for PPPDetailHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPDetailHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPDetailHour_Inc(in lv_TimeId_prev_48hour INT,in P_Limit INT)

 BEGIN

 declare lv_PPPDetailHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*Select PPPDetailHour.Id 
FROM  PPPDetailHour INNER JOIN PPPTotalHour
WHERE 
PPPDetailHour.PPPTotalHourId = PPPTotalHour.Id 
AND PPPTotalHour.CustomerId = P_CustomerId
AND PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour Limit P_Limit;
*/

select Id 
from  PPPDetailHour where PPPTotalHourId in (select Id from PPPTotalHour where TimeIdGMT < lv_TimeId_prev_48hour  ) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailHourId;

	DELETE FROM PPPDetailHour where Id = lv_PPPDetailHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;

-- -- -- -- -- -- --
-- 3) To delete PPPRevenueDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPRevenueDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPRevenueDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPRevenueDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPRevenueDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	/*(select count(PPPRevenueDay.Id) into lv_Cnt_PPPRevenueDay
	FROM PPPRevenueDay INNER JOIN PPPDetailDay INNER JOIN PPPTotalDay
	WHERE 
	PPPRevenueDay.PPPDetailDayId = PPPDetailDay.Id 
	AND PPPDetailDay.PPPTotalDayId = PPPTotalDay.Id 
	AND PPPTotalDay.CustomerId = P_CustomerId
	AND PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day ; */
	
	select count(*) into lv_Cnt_PPPRevenueDay from  PPPRevenueDay where PPPDetailDayId in 
	( select Id from  PPPDetailDay where PPPTotalDayId in 
	( select Id from PPPTotalDay where TimeIdLocal < lv_TimeId_prev_62day));

UNTIL lv_Cnt_PPPRevenueDay = 0 END REPEAT;

select 'Delete Completed for PPPRevenueDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPRevenueDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPRevenueDay_Inc(in lv_TimeId_prev_62day INT,in P_Limit INT)

 BEGIN

 declare lv_PPPRevenueDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*select PPPRevenueDay.Id 
FROM PPPRevenueDay INNER JOIN PPPDetailDay INNER JOIN PPPTotalDay
WHERE 
PPPRevenueDay.PPPDetailDayId = PPPDetailDay.Id 
AND PPPDetailDay.PPPTotalDayId = PPPTotalDay.Id 
AND PPPTotalDay.CustomerId = P_CustomerId
AND PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day Limit P_Limit; */

select Id from  PPPRevenueDay where PPPDetailDayId in 
( select Id from  PPPDetailDay where PPPTotalDayId in 
( select Id from PPPTotalDay where TimeIdLocal < lv_TimeId_prev_62day)) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPRevenueDayId;

	DELETE FROM PPPRevenueDay where Id = lv_PPPRevenueDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;

-- -- -- -- -- -- --
-- 4) To delete PPPDetailDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPDetailDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPDetailDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPDetailDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPDetailDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	/*select count(PPPDetailDay.Id) into lv_Cnt_PPPDetailDay
	FROM  PPPDetailDay INNER JOIN PPPTotalDay
	WHERE 
	PPPDetailDay.PPPTotalDayId = PPPTotalDay.Id 
	AND PPPTotalDay.CustomerId = P_CustomerId
	AND PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day ; */
	
	select count(*) into lv_Cnt_PPPDetailDay from  PPPDetailDay where PPPTotalDayId in ( select Id from PPPTotalDay where TimeIdLocal < lv_TimeId_prev_62day);

UNTIL lv_Cnt_PPPDetailDay = 0 END REPEAT;

select 'Delete Completed for PPPDetailDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPDetailDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPDetailDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPDetailDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*select PPPDetailDay.Id 
FROM  PPPDetailDay INNER JOIN PPPTotalDay
WHERE 
PPPDetailDay.PPPTotalDayId = PPPTotalDay.Id 
AND PPPTotalDay.CustomerId = P_CustomerId
AND PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day Limit P_Limit;
*/

select Id from  PPPDetailDay 
where PPPTotalDayId in ( select Id from PPPTotalDay where TimeIdLocal < lv_TimeId_prev_62day) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailDayId;

	DELETE FROM PPPDetailDay where Id = lv_PPPDetailDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;

-- -- -- -- -- -- --
-- 5) To delete PPPRevenueMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPRevenueMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPRevenueMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPRevenueMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPRevenueMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	/*select count(PPPRevenueMonth.Id) into lv_Cnt_PPPRevenueMonth
	FROM PPPRevenueMonth INNER JOIN PPPDetailMonth INNER JOIN PPPTotalMonth
	WHERE 
	PPPRevenueMonth.PPPDetailMonthId = PPPDetailMonth.Id 
	AND PPPDetailMonth.PPPTotalMonthId = PPPTotalMonth.Id 
	AND PPPTotalMonth.CustomerId = P_CustomerId
	AND PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month;
	*/
	
	select count(*) into lv_Cnt_PPPRevenueMonth  
	from  PPPRevenueMonth where PPPDetailMonthId in ( select Id from  PPPDetailMonth where PPPTotalMonthId in 
	( select id from PPPTotalMonth where TimeIdLocal < lv_TimeId_prev_24month));

UNTIL lv_Cnt_PPPRevenueMonth = 0 END REPEAT;

select 'Delete Completed for PPPRevenueMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPRevenueMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPRevenueMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPRevenueMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*select PPPRevenueMonth.Id 
FROM PPPRevenueMonth INNER JOIN PPPDetailMonth INNER JOIN PPPTotalMonth
WHERE 
PPPRevenueMonth.PPPDetailMonthId = PPPDetailMonth.Id 
AND PPPDetailMonth.PPPTotalMonthId = PPPTotalMonth.Id 
AND PPPTotalMonth.CustomerId = P_CustomerId 
AND PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month Limit P_Limit;
*/
select Id 
from  PPPRevenueMonth where PPPDetailMonthId in ( select Id from  PPPDetailMonth where PPPTotalMonthId in 
( select id from PPPTotalMonth where TimeIdLocal < lv_TimeId_prev_24month)) Limit P_Limit;
	
declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPRevenueMonthId;

	DELETE FROM PPPRevenueMonth where Id = lv_PPPRevenueMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;

-- -- -- -- -- -- --
-- 6) To delete PPPDetailMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPDetailMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPDetailMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPDetailMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPDetailMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	/*select count(PPPDetailMonth.Id) into lv_Cnt_PPPDetailMonth
	FROM  PPPDetailMonth INNER JOIN PPPTotalMonth
	WHERE 
	PPPDetailMonth.PPPTotalMonthId = PPPTotalMonth.Id 
	AND PPPTotalMonth.CustomerId = P_CustomerId
	AND PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month; */

	select count(*) into lv_Cnt_PPPDetailMonth 
	from  PPPDetailMonth where PPPTotalMonthId in ( select id from PPPTotalMonth where TimeIdLocal < lv_TimeId_prev_24month) ;
	
UNTIL lv_Cnt_PPPDetailMonth = 0 END REPEAT;

select 'Delete Completed for PPPDetailMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPDetailMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPDetailMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPDetailMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
/*select PPPDetailMonth.Id 
FROM  PPPDetailMonth INNER JOIN PPPTotalMonth
WHERE 
PPPDetailMonth.PPPTotalMonthId = PPPTotalMonth.Id 
AND PPPTotalMonth.CustomerId = P_CustomerId
AND PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month Limit P_Limit; */

select Id
from PPPDetailMonth where PPPTotalMonthId in ( select id from PPPTotalMonth where TimeIdLocal < lv_TimeId_prev_24month) Limit P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailMonthId;

	DELETE FROM PPPDetailMonth where Id = lv_PPPDetailMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- NEW Code written on March 6 

-- -- -- -- -- -- --
-- 7) To delete PPPTotalHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPTotalHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPTotalHourForDelete_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPTotalHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPTotalHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_PPPTotalHour 
	from PPPTotalHour where PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour ;
	
UNTIL lv_Cnt_PPPTotalHour = 0 END REPEAT;

select 'Delete Completed for lv_Cnt_PPPTotalHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPTotalHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPTotalHour_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPTotalHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from PPPTotalHour where PPPTotalHour.TimeIdGMT < lv_TimeId_prev_48hour LIMIT P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPTotalHourId;

	DELETE FROM PPPTotalHour where Id = lv_PPPTotalHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 8) To delete PPPTotalDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPTotalDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPTotalDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPTotalDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPTotalDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_PPPTotalDay 
	from PPPTotalDay where PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day ;
	
UNTIL lv_Cnt_PPPTotalDay = 0 END REPEAT;

select 'Delete Completed for PPPTotalDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPTotalDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPTotalDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPTotalDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from PPPTotalDay where PPPTotalDay.TimeIdLocal < lv_TimeId_prev_62day LIMIT P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPTotalDayId;

	DELETE FROM PPPTotalDay where Id = lv_PPPTotalDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;



-- -- -- -- -- -- --
-- 9) To delete PPPTotalMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PPPTotalMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PPPTotalMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PPPTotalMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeletePPPTotalMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_PPPTotalMonth 
	from PPPTotalMonth where PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month ;
	
UNTIL lv_Cnt_PPPTotalMonth = 0 END REPEAT;

select 'Delete Completed for PPPTotalDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePPPTotalMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePPPTotalMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_PPPTotalMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from PPPTotalMonth where PPPTotalMonth.TimeIdLocal < lv_TimeId_prev_24month LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPTotalMonthId;

	DELETE FROM PPPTotalMonth where Id = lv_PPPTotalMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 10) To delete OccupancyHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_OccupancyHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_OccupancyHourForDelete_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_OccupancyHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteOccupancyHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_OccupancyHour 
	from OccupancyHour where TimeIdLocal < lv_TimeId_prev_48hour ;
	
UNTIL lv_Cnt_OccupancyHour = 0 END REPEAT;

select 'Delete Completed for OccupancyHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteOccupancyHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteOccupancyHour_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT)

 BEGIN

 declare lv_OccupancyHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from OccupancyHour where TimeIdLocal < lv_TimeId_prev_48hour LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_OccupancyHourId;

	DELETE FROM OccupancyHour where Id = lv_OccupancyHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 11) To delete OccupancyDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_OccupancyDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_OccupancyDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_OccupancyDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteOccupancyDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_OccupancyDay 
	from OccupancyDay where TimeIdLocal < lv_TimeId_prev_62day  ;
	
UNTIL lv_Cnt_OccupancyDay = 0 END REPEAT;

select 'Delete Completed for OccupancyDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteOccupancyDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteOccupancyDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_OccupancyDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from OccupancyDay where TimeIdLocal < lv_TimeId_prev_62day  LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_OccupancyDayId;

	DELETE FROM OccupancyDay where Id = lv_OccupancyDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;



-- -- -- -- -- -- --
-- 12) To delete OccupancyMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_OccupancyMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_OccupancyMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_OccupancyMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteOccupancyMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_OccupancyMonth 
	from OccupancyMonth where TimeIdLocal < lv_TimeId_prev_24month  ;
	
UNTIL lv_Cnt_OccupancyMonth = 0 END REPEAT;

select 'Delete Completed for OccupancyMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteOccupancyMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteOccupancyMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_OccupancyMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from OccupancyMonth where TimeIdLocal < lv_TimeId_prev_24month LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_OccupancyMonthId;

	DELETE FROM OccupancyMonth where Id = lv_OccupancyMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- -- -- -- -- -- --
-- 13) To delete UtilizationDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_UtilizationDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_UtilizationDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_UtilizationDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteUtilizationDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_UtilizationDay 
	from UtilizationDay where TimeIdLocal < lv_TimeId_prev_62day   ;
	
UNTIL lv_Cnt_UtilizationDay = 0 END REPEAT;

select 'Delete Completed for UtilizationDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteUtilizationDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteUtilizationDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_UtilizationDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from UtilizationDay where TimeIdLocal < lv_TimeId_prev_62day  LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_UtilizationDayId;

	DELETE FROM UtilizationDay where Id = lv_UtilizationDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- -- -- -- -- -- --
-- 14) To delete UtilizationMonth
-- -- -- -- -- -- --
-- EMS 5356
DROP PROCEDURE IF EXISTS sp_UtilizationMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_UtilizationMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_UtilizationMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteUtilizationMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_UtilizationMonth 
	from UtilizationMonth where TimeIdLocal < lv_TimeId_prev_24month   ;
	
UNTIL lv_Cnt_UtilizationMonth = 0 END REPEAT;

select 'Delete Completed for UtilizationMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteUtilizationMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteUtilizationMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_UtilizationMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from UtilizationMonth where TimeIdLocal < lv_TimeId_prev_24month  LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_UtilizationMonthId;

	DELETE FROM UtilizationMonth where Id = lv_UtilizationMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;



-- -- -- -- -- -- --
-- 15) To delete TotalCollection
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_TotalCollectionForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_TotalCollectionForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_TotalCollection BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteTotalCollection_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_TotalCollection 
	from TotalCollection where EndTimeIdLocal < lv_TimeId_prev_24month  ;
	
UNTIL lv_Cnt_TotalCollection = 0 END REPEAT;

select 'Delete Completed for TotalCollection' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteTotalCollection_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteTotalCollection_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_TotalCollectionId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from TotalCollection where EndTimeIdLocal < lv_TimeId_prev_24month  LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_TotalCollectionId;

	DELETE FROM TotalCollection where Id = lv_TotalCollectionId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 16) To delete SettledTransaction
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_SettledTransactionForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_SettledTransactionForDelete_Inc(in lv_TimeId_prev_3day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_SettledTransaction BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteSettledTransaction_Inc(lv_TimeId_prev_3day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_SettledTransaction 
	from SettledTransaction where TimeIdLocal < lv_TimeId_prev_3day   ;
	
UNTIL lv_Cnt_SettledTransaction = 0 END REPEAT;

select 'Delete Completed for SettledTransaction' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteSettledTransaction_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteSettledTransaction_Inc(in lv_TimeId_prev_3day INT ,in P_Limit INT)

 BEGIN

 declare lv_SettledTransactionId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from SettledTransaction where TimeIdLocal < lv_TimeId_prev_3day LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_SettledTransactionId;

	DELETE FROM SettledTransaction where Id = lv_SettledTransactionId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 17) To delete ETLLocationDetail
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_ETLLocationDetailForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_ETLLocationDetailForDelete_Inc(in lv_TimeId_prev_days INT ,in P_Limit INT, in P_Sleep TINYINT, in P_TypeId TINYINT)

 BEGIN

 declare lv_Cnt_ETLLocationDetail BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteETLLocationDetail_Inc(lv_TimeId_prev_days,P_Limit,P_TypeId);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_ETLLocationDetail 
	from ETLLocationDetail where TimeIdLocal < lv_TimeId_prev_days and ETLLocationDetailTypeId = P_TypeId ;
	
UNTIL lv_Cnt_ETLLocationDetail = 0 END REPEAT;

select 'Delete Completed for ETLLocationDetail' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteETLLocationDetail_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteETLLocationDetail_Inc(in lv_TimeId_prev_days INT ,in P_Limit INT, in P_TypeId TINYINT)

 BEGIN

 declare lv_ETLLocationDetailId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from ETLLocationDetail where TimeIdLocal < lv_TimeId_prev_days and ETLLocationDetailTypeId = P_TypeId LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_ETLLocationDetailId;

	DELETE FROM ETLLocationDetail where Id = lv_ETLLocationDetailId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- New Procedures added for card Processing widget


-- -- -- -- -- -- --
-- 18) To delete CardTransactionRevenueHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionRevenueHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionRevenueHourForDelete_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionRevenueHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionRevenueHour_Inc(lv_TimeId_prev_48hour, P_Limit);

select sleep(P_Sleep) as SLEEP ;


select count(*) into  lv_Cnt_CardTransactionRevenueHour
from CardTransactionRevenueHour  where CardTransactionDetailHourId in ( select Id from  CardTransactionDetailHour where CardTransactionTotalHourId in 
(select Id from CardTransactionTotalHour where TimeIdGMT <= lv_TimeId_prev_48hour )) ;

UNTIL lv_Cnt_CardTransactionRevenueHour = 0 END REPEAT;

select 'Delete Completed for CardTransactionRevenueHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionRevenueHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionRevenueHour_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT)

 BEGIN

 declare lv_CardTransactionRevenueHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for


select Id from CardTransactionRevenueHour  where CardTransactionDetailHourId in ( select Id from  CardTransactionDetailHour where CardTransactionTotalHourId in 
(select Id from CardTransactionTotalHour where TimeIdGMT <= lv_TimeId_prev_48hour )) Limit P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;

while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionRevenueHourId;

	DELETE FROM CardTransactionRevenueHour where Id = lv_CardTransactionRevenueHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 COMMIT ;		-- ASHOK2	   
 set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


 -- -- -- -- -- -- --
-- 19) To delete CardTransactionDetailHour
-- -- -- -- -- -- --

DROP PROCEDURE IF EXISTS sp_CardTransactionDetailHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionDetailHourForDelete_Inc(in lv_TimeId_prev_48hour INT, in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionDetailHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionDetailHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;


	
	select count(*) into  lv_Cnt_CardTransactionDetailHour 
	from  CardTransactionDetailHour where CardTransactionTotalHourId in (select Id from CardTransactionTotalHour where TimeIdGMT < lv_TimeId_prev_48hour  );
	

UNTIL lv_Cnt_CardTransactionDetailHour = 0 END REPEAT;

select 'Delete Completed for CardTransactionDetailHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionDetailHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionDetailHour_Inc(in lv_TimeId_prev_48hour INT,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionDetailHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for


select Id 
from  CardTransactionDetailHour where CardTransactionTotalHourId in (select Id from CardTransactionTotalHour where TimeIdGMT < lv_TimeId_prev_48hour  ) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionDetailHourId;

	DELETE FROM CardTransactionDetailHour where Id = lv_CardTransactionDetailHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;


-- -- -- -- -- -- --
-- 20) To delete CardTransactionRevenueDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionRevenueDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionRevenueDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionRevenueDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionRevenueDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;


	
	select count(*) into lv_Cnt_CardTransactionRevenueDay from  CardTransactionRevenueDay where CardTransactionDetailDayId in 
	( select Id from  CardTransactionDetailDay where CardTransactionTotalDayId in 
	( select Id from CardTransactionTotalDay where TimeIdLocal < lv_TimeId_prev_62day));

UNTIL lv_Cnt_CardTransactionRevenueDay = 0 END REPEAT;

select 'Delete Completed for CardTransactionRevenueDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionRevenueDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionRevenueDay_Inc(in lv_TimeId_prev_62day INT,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionRevenueDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for


select Id from  CardTransactionRevenueDay where CardTransactionDetailDayId in 
( select Id from  CardTransactionDetailDay where CardTransactionTotalDayId in 
( select Id from CardTransactionTotalDay where TimeIdLocal < lv_TimeId_prev_62day)) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionRevenueDayId;

	DELETE FROM CardTransactionRevenueDay where Id = lv_CardTransactionRevenueDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;


-- -- -- -- -- -- --
-- 21) To delete CardTransactionDetailDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionDetailDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionDetailDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionDetailDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionDetailDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;


	
	select count(*) into lv_Cnt_CardTransactionDetailDay from  CardTransactionDetailDay where CardTransactionTotalDayId in ( select Id from CardTransactionTotalDay where TimeIdLocal < lv_TimeId_prev_62day);

UNTIL lv_Cnt_CardTransactionDetailDay = 0 END REPEAT;

select 'Delete Completed for CardTransactionDetailDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionDetailDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionDetailDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionDetailDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for


select Id from  CardTransactionDetailDay 
where CardTransactionTotalDayId in ( select Id from CardTransactionTotalDay where TimeIdLocal < lv_TimeId_prev_62day) Limit P_Limit;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionDetailDayId;

	DELETE FROM CardTransactionDetailDay where Id = lv_CardTransactionDetailDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;


-- -- -- -- -- -- --
-- 22) To delete CardTransactionRevenueMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionRevenueMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionRevenueMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionRevenueMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionRevenueMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;


	
	select count(*) into lv_Cnt_CardTransactionRevenueMonth  
	from  CardTransactionRevenueMonth where CardTransactionDetailMonthId in ( select Id from  CardTransactionDetailMonth where CardTransactionTotalMonthId in 
	( select id from CardTransactionTotalMonth where TimeIdLocal < lv_TimeId_prev_24month));

UNTIL lv_Cnt_CardTransactionRevenueMonth = 0 END REPEAT;

select 'Delete Completed for CardTransactionRevenueMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionRevenueMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionRevenueMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionRevenueMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id 
from  CardTransactionRevenueMonth where CardTransactionDetailMonthId in ( select Id from  CardTransactionDetailMonth where CardTransactionTotalMonthId in 
( select id from CardTransactionTotalMonth where TimeIdLocal < lv_TimeId_prev_24month)) Limit P_Limit;
	
declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionRevenueMonthId;

	DELETE FROM CardTransactionRevenueMonth where Id = lv_CardTransactionRevenueMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON
END//
delimiter ;


-- -- -- -- -- -- --
-- 23) To delete CardTransactionDetailMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionDetailMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionDetailMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionDetailMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionDetailMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;


	select count(*) into lv_Cnt_CardTransactionDetailMonth 
	from  CardTransactionDetailMonth where CardTransactionTotalMonthId in ( select id from CardTransactionTotalMonth where TimeIdLocal < lv_TimeId_prev_24month) ;
	
UNTIL lv_Cnt_CardTransactionDetailMonth = 0 END REPEAT;

select 'Delete Completed for CardTransactionDetailMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionDetailMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionDetailMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionDetailMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for


select Id
from CardTransactionDetailMonth where CardTransactionTotalMonthId in ( select id from CardTransactionTotalMonth where TimeIdLocal < lv_TimeId_prev_24month) Limit P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionDetailMonthId;

	DELETE FROM CardTransactionDetailMonth where Id = lv_CardTransactionDetailMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- -- -- -- -- -- --
-- 24) To delete CardTransactionTotalHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionTotalHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionTotalHourForDelete_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionTotalHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionTotalHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CardTransactionTotalHour 
	from CardTransactionTotalHour where CardTransactionTotalHour.TimeIdGMT < lv_TimeId_prev_48hour ;
	
UNTIL lv_Cnt_CardTransactionTotalHour = 0 END REPEAT;

select 'Delete Completed for lv_Cnt_CardTransactionTotalHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionTotalHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionTotalHour_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionTotalHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CardTransactionTotalHour where CardTransactionTotalHour.TimeIdGMT < lv_TimeId_prev_48hour LIMIT P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionTotalHourId;

	DELETE FROM CardTransactionTotalHour where Id = lv_CardTransactionTotalHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 25) To delete CardTransactionTotalDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionTotalDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionTotalDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionTotalDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionTotalDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CardTransactionTotalDay 
	from CardTransactionTotalDay where CardTransactionTotalDay.TimeIdLocal < lv_TimeId_prev_62day ;
	
UNTIL lv_Cnt_CardTransactionTotalDay = 0 END REPEAT;

select 'Delete Completed for CardTransactionTotalDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionTotalDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionTotalDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionTotalDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CardTransactionTotalDay where CardTransactionTotalDay.TimeIdLocal < lv_TimeId_prev_62day LIMIT P_Limit ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionTotalDayId;

	DELETE FROM CardTransactionTotalDay where Id = lv_CardTransactionTotalDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- -- -- -- -- -- --
-- 26) To delete CardTransactionTotalMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CardTransactionTotalMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CardTransactionTotalMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CardTransactionTotalMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCardTransactionTotalMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CardTransactionTotalMonth 
	from CardTransactionTotalMonth where CardTransactionTotalMonth.TimeIdLocal < lv_TimeId_prev_24month ;
	
UNTIL lv_Cnt_CardTransactionTotalMonth = 0 END REPEAT;

select 'Delete Completed for CardTransactionTotalMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCardTransactionTotalMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCardTransactionTotalMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_CardTransactionTotalMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CardTransactionTotalMonth where CardTransactionTotalMonth.TimeIdLocal < lv_TimeId_prev_24month LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CardTransactionTotalMonthId;

	DELETE FROM CardTransactionTotalMonth where Id = lv_CardTransactionTotalMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;

-- end code for card processing widgets

-- New code added on March 4 2015
-- -- -- -- -- -- --
-- 27) To delete CitationHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CitationHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CitationHourForDelete_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CitationTotalHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCitationHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CitationTotalHour 
	from CitationTotalHour where TimeIdLocal < lv_TimeId_prev_48hour ;
	
UNTIL lv_Cnt_CitationTotalHour = 0 END REPEAT;

select 'Delete Completed for CitationTotalHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCitationHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCitationHour_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT)

 BEGIN

 declare lv_CitationTotalHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CitationTotalHour where TimeIdLocal < lv_TimeId_prev_48hour LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CitationTotalHourId;

	DELETE FROM CitationTotalHour where Id = lv_CitationTotalHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- -- -- -- -- -- --
-- 28) To delete CitationDay
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CitationDayForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CitationDayForDelete_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CitationTotalDay BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCitationDay_Inc(lv_TimeId_prev_62day,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CitationTotalDay 
	from CitationTotalDay where TimeIdLocal < lv_TimeId_prev_62day  ;
	
UNTIL lv_Cnt_CitationTotalDay = 0 END REPEAT;

select 'Delete Completed for CitationTotalDay' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCitationDay_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCitationDay_Inc(in lv_TimeId_prev_62day INT ,in P_Limit INT)

 BEGIN

 declare lv_CitationTotalDayId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CitationTotalDay where TimeIdLocal < lv_TimeId_prev_62day  LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CitationTotalDayId;

	DELETE FROM CitationTotalDay where Id = lv_CitationTotalDayId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;



-- -- -- -- -- -- --
-- 29) To delete CitationMonth
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CitationMonthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CitationMonthForDelete_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CitationTotalMonth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCitationMonth_Inc(lv_TimeId_prev_24month,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CitationTotalMonth 
	from CitationTotalMonth where TimeIdLocal < lv_TimeId_prev_24month  ;
	
UNTIL lv_Cnt_CitationTotalMonth = 0 END REPEAT;

select 'Delete Completed for CitationTotalMonth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCitationMonth_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCitationMonth_Inc(in lv_TimeId_prev_24month INT ,in P_Limit INT)

 BEGIN

 declare lv_CitationTotalMonthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CitationTotalMonth where TimeIdLocal < lv_TimeId_prev_24month LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CitationTotalMonthId;

	DELETE FROM CitationTotalMonth where Id = lv_CitationTotalMonthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;





-- -- -- -- -- -- --
-- 30) To delete data on CitationMap
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CitationMapForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CitationMapForDelete_Inc(in lv_past8day_date DATETIME ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CitationMap BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCitationMap_Inc(lv_past8day_date,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CitationMap 
	from CitationMap where IssueDateGMT < lv_past8day_date  ;
	
UNTIL lv_Cnt_CitationMap = 0 END REPEAT;

select 'Delete Completed for CitationMap' as Status ;
 
END//
delimiter ;



DROP PROCEDURE IF EXISTS sp_DeleteCitationMap_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCitationMap_Inc(in lv_past8day_date DATETIME ,in P_Limit INT)

 BEGIN

 declare lv_CitationMapId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CitationMap where IssueDateGMT < lv_past8day_date LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CitationMapId;

	DELETE FROM CitationMap where Id = lv_CitationMapId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;


-- added on May 22 2015


-- -- -- -- -- -- --
-- 31) To delete CaseOccupancyHour
-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_CaseOccupancyHourForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_CaseOccupancyHourForDelete_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_CaseOccupancyHour BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;

 
REPEAT

call sp_DeleteCaseOccupancyHour_Inc(lv_TimeId_prev_48hour,P_Limit);

select sleep(P_Sleep) as SLEEP ;

	select count(*) into lv_Cnt_CaseOccupancyHour 
	from CaseOccupancyHour where TimeIdLocal < lv_TimeId_prev_48hour ;
	
UNTIL lv_Cnt_CaseOccupancyHour = 0 END REPEAT;

select 'Delete Completed for CaseOccupancyHour' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeleteCaseOccupancyHour_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteCaseOccupancyHour_Inc(in lv_TimeId_prev_48hour INT ,in P_Limit INT)

 BEGIN

 declare lv_CaseOccupancyHourId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for

select Id
from CaseOccupancyHour where TimeIdLocal < lv_TimeId_prev_48hour LIMIT P_Limit;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
 set autocommit = 0 ;
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_CaseOccupancyHourId;

	DELETE FROM CaseOccupancyHour where Id = lv_CaseOccupancyHourId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  COMMIT ;		-- ASHOK2	   
  set autocommit = 1 ; -- Auto Commit ON

END//
delimiter ;
  
  




