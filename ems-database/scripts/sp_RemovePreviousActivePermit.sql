-- To be deployed in EMS7

DROP PROCEDURE IF EXISTS sp_RemovePreviousActivePermit ;

delimiter //

CREATE  PROCEDURE sp_RemovePreviousActivePermit()
BEGIN


DECLARE CursorDone INT DEFAULT 0;
declare idmaster_pos,indexm_pos int default 0;
DECLARE v_ActivePermitId BIGINT UNSIGNED ;

DECLARE C1 CURSOR FOR
	
		SELECT
			Id
		FROM 
			ActivePermit
		WHERE
			PermitExpireGMT < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 14 day) and   -- Can we take this value from EmsProperties?
			SpaceNumber = 0 OR  SpaceNumber IS NULL;
			
				
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
 

 -- SET lv_Customer_Timezone = null;
 
 OPEN C_ActivePermit;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 

WHILE indexm_pos < idmaster_pos do
    
	FETCH C_ActivePermit INTO  	v_ActivePermitId ;	
								
			DELETE FROM ActivePermit 
			WHERE  
			Id = v_ActivePermitId; 
			
					
	set indexm_pos = indexm_pos +1;
end while;

close C_ActivePermit;

COMMIT ;		
        
END//

delimiter ;
