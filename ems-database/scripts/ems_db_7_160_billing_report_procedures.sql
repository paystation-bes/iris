
/*  --------------------------------------------------------------------------------------
    Procedure:  sp_RunBillingReport
    
    Important:  This procedure can run against pre-EMS v6.3.8 as well as EMS v6.3.8 and later 
                database schema. Which database version the procedure runs against is hard 
                coded into procedure variable DBversion. Review the procedure code below.
    
    Purpose:    Creates a new Billing Report or updates an existing Billing Report.
                Calls other procedures that perform Billing Report business logic.
    
                If the previous (last) Billing Report was posted (finalized) a new 
                Billing Report will be created.
                
                It is possible to run an interim Billing Report earlier than the 
                standardized Billing Report run. An interim report is not posted
                (finalized) and can be re-run numerous times.

                This procedure does not generate report outputL it prepares data for 
                output. For report output use procedure sp_BillingReportGetSub.
                
    Call:       CALL sp_RunBillingReport (bdt,edt,post,trace);
    
                Example call: 
                CALL sp_RunBillingReport ('2011-01-01','2011-09-14',0,1);
                
                Interpretation of example call:
                CALL sp_RunBillingReport ('2011-06-28','2011-07-14',1,0);
                Run the Billing Report with report begin and end dates of 2011-06-28
                and 2011-07-14, post (finalize) the report when the report has completed
                running, and do not run a debug trace while the report runs.
    
                Parameters:
                1.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                2.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                3.  post    Post (finalize) this Billing Report at the end of the report run
                            values: 1=post, otherwise do not post
                4.  trace   enable a debug trace as the report runs
                            values: 1=trace, otherwise do not trace
                
    Note:       The begin and end dates for the Billing Report are inclusive.
                Using the example call above the Billing Report window is from the
                beginning of 2011-06-28 until the end of 2011-07-14.
                The next Billing Report would be run from 2011-07-15 to 2011-07-27.
                
    --------------------------------------------------------------------------------------
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.
    
                SELECT  * FROM BillingReport WHERE Id = 22;
                
                SELECT  COUNT(*) FROM BillingReportPaystation WHERE idBillingReport = 2;
              
                SELECT  P.idBillingReportLogicType          AS idLogic, 
                        COUNT(*)                            AS Paystations, 
                        L.Comment                           AS 'Current Billable Status', 
                        IF(L.WasActivated=2,'Yes','No')     AS 'Was Activated', 
                        IF(L.WasBillable=2,'Yes','No')      AS 'Was Billable', 
                        IF(L.NowActivated=2,'Yes','No')     AS 'Now Activated', 
                        If(L.NowBillable=2,'Yes','No')      AS 'Now Billable'
                FROM    BillingReportPaystation P, BillingReportLogicType L
                WHERE   P.idBillingReport = 12 
                AND     P.idBillingReportLogicType = L.Id
                GROUP BY P.idBillingReportLogicType ORDER BY idBillingReportLogicType;
                            
                SELECT  COUNT(*) FROM BillingReportPaystation WHERE idBillingReport = 12 and idBillingReportLogicType = 0;
              
                SELECT  idPaystation, Paystation, WasActivated, NowActivated, WasBillable, NowBillable, ActivationChangeGMT, BillableChangeGMT 
                FROM    BillingReportPaystation 
                WHERE   idBillingReport = 22 
                AND     (ActivationChangeGMT IS NOT NULL OR BillableChangeGMT IS NOT NULL)
                LIMIT   10;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_RunBillingReport;

delimiter //
CREATE PROCEDURE sp_RunBillingReport (IN bdt DATE, IN edt DATE, IN post TINYINT UNSIGNED, IN trace TINYINT UNSIGNED)
BEGIN

    /* task: 1=create-new-billing-report; 2=update-existing-billing-report; 0,3,4,5 (any other value): error; 'task' is set by a call to procedure sp_StartBillingReport() below */
    DECLARE task TINYINT UNSIGNED;    

    /* idBR: stores BillingReport.Id */ 
    DECLARE idBR INT UNSIGNED;
   
    /* DBversion (database version): 1=pre-EMS v6.3.8, 2=EMS v6.3.8 and later */
    DECLARE DBversion TINYINT UNSIGNED;
    SET DBversion = 2;

    /* initialize variables, these variables will be set by the call to procedure sp_StartBillingReport just below */
    SET task = 0, idBR = 0;

    /* start a new or update an existing Billing Report: assigns values to parameters task and idBR */
    CALL sp_StartBillingReport (task, idBR, bdt, edt, trace);

	UPDATE BillingReport SET RunEnd = UTC_TIMESTAMP() 
	WHERE Id = idBR;
  
END//
delimiter ;



/*  --------------------------------------------------------------------------------------
    Procedure:  sp_StartBillingReport 
    
    Purpose:    Starts a new run of the Billing Report.
                This could create new Billing Report or re-run an existing interim Billing Report
                (a non-posted or non-finalized report) to pickup any changes (Billable, Activation or other) 
                that have occurred since the last iteration of the interim report.

    Call:       CALL sp_StartBillingReport (task, idBR, bdt, edt, trace);
   
                Example call:
                CALL sp_StartBillingReport (0, 0, '2011-06-28','2011-07-14');
                
                Parameters:
                1.  task    INOUT parameter, assigned by this procedure,
                            where task = 1 = new-billing-report, and task = 2 = re-run-existing-billing-report
                            any other value for task is an error (most likely related to an incorrect begin or end date)
                2.  idBR    INOUT parameter, assigned by this procedure,
                            the BillingReport.Id of the Billing Report that was newly created or is being re-run
                3.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                4.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                5.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.

                SELECT * FROM BillingReport;
                
                SELECT COUNT(*) FROM BillingReportPaystation;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_StartBillingReport;

delimiter //
CREATE PROCEDURE sp_StartBillingReport (INOUT task TINYINT UNSIGNED, INOUT idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    /* prvbdt & prvedt: the begin and end dates of the most recently posted (i.e., previous) Billing Report */
    DECLARE prvbdt DATE;
    DECLARE prvedt DATE;
    DECLARE prvBR  INT UNSIGNED;
    
    /* get info from the most recently posted Billing Report */
    SELECT Id, DateRangeBegin, DateRangeEnd INTO prvBR, prvbdt, prvedt
    FROM   BillingReport 
    WHERE  Id = (SELECT MAX(Id) FROM BillingReport WHERE IsPosted=1);
   
    /* error check: report end date must be greater than begin date */
    IF bdt > edt THEN SET task = 4; END IF;
                      
    /* check for very first Billing Report (no other Billing Report exists) */
    SELECT IF((SELECT COUNT(*) FROM BillingReport)=0 AND task=0,1,task) INTO task;

    /* determine if a new Billing Report is to be created (the previous Billing Report was posted (closed) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=1 AND task=0,1,task) INTO task;
     
    /* if starting a new Billing Report validate the report's begin and end dates */
    IF task=1 THEN      
        /* the begin date for the next report MUST be the day after the end date from the previous report */
        SELECT IF(ADDDATE(prvedt,INTERVAL 1 DAY)!=bdt,5,task) INTO task;
    END IF;
   
    /* existing Billing Report (existing Billing Report has yet to be posted) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=0 AND task=0,3,task) INTO task;

    /* validate dates for existing report (a continuation of the line directly above) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport) AND DateRangeBegin=bdt AND DateRangeEnd=edt)=0 AND task=3,2,task) INTO task;

    /* task has been assigned a value, examine this value */
    CASE task
        /* task=0 error, task not determined */
        WHEN 0 THEN
        BEGIN
            SELECT 'ERROR. Could not determine whether to create a new Billing Report or update an existing Billing Report.' AS ErrorMessage;
        END;

        /* task=1: create new Billing Report */
        WHEN 1 THEN
        BEGIN
            INSERT BillingReport (DateRangeBegin,DateRangeEnd,RunBegin,IsPosted,IsError) VALUES (bdt,edt,UTC_TIMESTAMP(),0,0);
            SET idBR = last_insert_id();
        END;

        /* task=2: update existing Billing Report */
        WHEN 2 THEN
        BEGIN
            SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt AND IsPosted=0 AND Id=(SELECT MAX(Id) FROM BillingReport) INTO idBR;
            UPDATE BillingReport SET RunBegin=UTC_TIMESTAMP(), RunEnd=UTC_TIMESTAMP(), IsError=0 WHERE Id=idBR;
            DELETE FROM BillingReportPaystation WHERE BillingReportId=idBR;
        END;

        /* task=3: error, invalid dates for existing report: the begin and end dates specified must match the begin and end dates for the current non-posted Billing Report */
        WHEN 3 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. Parameter dates do not match current Billing Report dates: ',bdt,' and ',edt,' do not match ',DateRangeBegin,' and ',DateRangeEnd) AS ErrorMessage
            FROM   BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport);
        END;
         
        /* task=4: error, invalid dates, end date must be greater than begin date */
        WHEN 4 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' cannot be greater than the report end date ',edt) AS ErrorMessage;
        END;
         
         /* task=5: error, the begin date of a new report must be the day after the nend date of the old report */
        WHEN 5 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' must be the next day following the previous reports end date ',prvedt) AS ErrorMessage;
        END;

        /* task>5: error */
        ELSE
        BEGIN
            SELECT 'ERROR. Could not resolve Billing Report parameters.' AS ErrorMessage;
        END;
    END CASE;
    
    /* add Paystations to Billing Report */
    IF task=1 OR task=2 THEN    
        
        /* create master list of Paystations for Billing Report  */
        INSERT INTO BillingReportPaystation 
				(BillingReportId,CustomerId,CustomerName,IsTrial, TrialExpiryGMT, PaystationId,PointOfSaleId,POSName,POSLocationId,ProvisionedGMT,POSLastModifiedGMT,SerialNumber,PaystationSettingName,LocationName,
				LastHeartbeatGMT,IsActivated,IsLocked,IsDigitalConnect,IsBillableMonthlyOnActivation,IsBillableMonthlyForEms,IsDeleted, ActivationChangeGMT,
				ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,ServicePayByPhone,
				ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned,IsEMV,TerminalId)
				Select idBR,c.Id,c.Name,c.CustomerStatusTypeId,c.TrialExpiryGMT,pst.Id,pos.Id,pos.Name,pos.LocationId,pos.ProvisionedGMT,pos.LastModifiedGMT,pos.SerialNumber,pss.PaystationSettingName,l.Name,
				phb.LastHeartbeatGMT,ps.IsActivated,ps.IsLocked, ps.IsDigitalConnect ,ps.IsBillableMonthlyOnActivation ,ps.IsBillableMonthlyForEms ,pst.IsDeleted ,ps.IsActivatedGMT ,
				csStdReports.IsEnabled ,csAlerts.IsEnabled,csCCProcess.IsEnabled,csBatchCCProcess.IsEnabled, csCampusProcess.IsEnabled, csCoupons.IsEnabled, csPasscards.IsEnabled,csSmartcards.IsEnabled,csExtendByPhone.IsEnabled,
				csDigitalRead.IsEnabled, csDigitalWrite.IsEnabled,csPayByCell.IsEnabled,ps.IsDecommissioned,pro.IsEMV,EMVMA.Field1 
				FROM PointOfSale pos 
				INNER JOIN Location l ON pos.LocationId = l.Id
				INNER JOIN Customer c ON pos.CustomerId = c.Id
				INNER JOIN POSServiceState pss ON pos.Id = pss.PointOfSaleId
				INNER JOIN POSHeartbeat phb ON pos.Id = phb.PointOfSaleId
				INNER JOIN POSStatus ps ON ps.PointOfSaleId = pos.Id
				inner join CustomerSubscription csStdReports on csStdReports.CustomerId = c.Id
				inner join CustomerSubscription csAlerts on csAlerts.CustomerId = c.Id
				inner join CustomerSubscription csCCProcess on csCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csBatchCCProcess on csBatchCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csCampusProcess on csCampusProcess.CustomerId = c.Id
				inner join CustomerSubscription csCoupons on csCoupons.CustomerId = c.Id
				inner join CustomerSubscription csPasscards on csPasscards.CustomerId = c.Id
				inner join CustomerSubscription csSmartcards on csSmartcards.CustomerId = c.Id
				inner join CustomerSubscription csExtendByPhone on csExtendByPhone.CustomerId = c.Id
				inner join CustomerSubscription csDigitalRead on csDigitalRead.CustomerId = c.Id
				inner join CustomerSubscription csDigitalWrite on csDigitalWrite.CustomerId = c.Id
				inner join CustomerSubscription csPayByCell on csPayByCell.CustomerId = c.Id
				inner join CustomerStatusType cStatus on c.CustomerStatusTypeId = cStatus.Id
				inner join Paystation pst on pst.Id = pos.PaystationId 
				LEFT JOIN MerchantPOS mpos ON (pos.Id = mpos.PointOfSaleId and mpos.IsDeleted = 0 and mpos.CardTypeId = 1)
				LEFT JOIN MerchantAccount ma ON mpos.MerchantAccountId = ma.Id
				LEFT JOIN  Processor pro ON ma.ProcessorId = pro.Id
				LEFT JOIN MerchantAccount EMVMA on (mpos.MerchantAccountId = EMVMA.Id and EMVMA.ProcessorId = pro.Id and pro.IsEMV=1) 
				where 
				csStdReports.SubscriptionTypeId = 100 and
				csAlerts.SubscriptionTypeId = 200 and
				csCCProcess.SubscriptionTypeId = 300 and
				csBatchCCProcess.SubscriptionTypeId = 400 and
				csCampusProcess.SubscriptionTypeId = 500 and
				csCoupons.SubscriptionTypeId = 600 and 
				csPasscards.SubscriptionTypeId = 700 and
				csSmartcards.SubscriptionTypeId = 800 and
				csExtendByPhone.SubscriptionTypeId = 900 and
				csDigitalRead.SubscriptionTypeId = 1000 and
				csDigitalWrite.SubscriptionTypeId = 1100 and
				csPayByCell.SubscriptionTypeId = 1200 ;

		-- Get Transaction Count		
		CALL sp_BillingReportTxnCount(idBR);
		
		SELECT  Id FROM BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
			
		IF (prvBR IS NOT NULL) THEN
		
			-- Find for any Changes			
			CALL sp_FindBillingReportChanges(idBR,prvBR);
			CALL sp_RecordBillingReportChanges(idBR,prvBR);
		END IF ;
		
    END IF;
 
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_StartBillingReport' AS Trace;
    END IF;
    
END//
delimiter ;

-- Procedure sp_BillingReportTxnCount

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCount $$
CREATE PROCEDURE sp_BillingReportTxnCount (in P_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_DateRangeBegin, lv_DateRangeEnd Date;
declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;
declare lv_EMVTxnCount INT UNSIGNED ;

declare c1 cursor  for
select CustomerId, PointOfSaleId, B.DateRangeBegin, B.DateRangeEnd from BillingReportPaystation A, BillingReport B WHERE 
A.BillingReportId = B.Id and A.BillingReportId = P_BillingReportId
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_PointOfSaleId, lv_DateRangeBegin, lv_DateRangeEnd;

	set lv_TransactionCount = 0;
	set lv_EMVTxnCount = 0 ;
	
	select count(*) into lv_TransactionCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month );
	
	
	-- -------------------------------------------------------------------
    -- Type 16: EmsPsType16TxnCount (AddTime: Extend by Phone Permit Extension)
    
    select count(*) into lv_Type16TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month )
	and TransactionTypeId = 16;
	
	
	/*
	SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 16   -- 16 is for 'AddTime: Extend By Phone AddTime Transaction' 
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
	*/
	
	            
    -- -------------------------------------------------------------------
    -- Type 17: EmsPsType17TxnCount (Regular: Extend by Phone Permit)
   
    select count(*) into lv_Type17TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month )
	and TransactionTypeId = 17;
	
	/*    
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 17   -- 17 is for 'Regular: Extend By Phone Regular Transaction' 
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
	*/ 
	
	-- EMS-10283
	SELECT COUNT(*) into lv_EMVTxnCount
	FROM Purchase P 
	inner join ProcessorTransaction PT on P.Id = PT.PurchaseId
	inner join MerchantAccount MA on PT.MerchantAccountId = MA.Id
	inner join Processor Pro on (MA.ProcessorId = Pro.Id and Pro.IsEMV = 1)
	where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month );
	
	UPDATE BillingReportPaystation
	SET TransactionCount = lv_TransactionCount, 
	Type16TxnCount = lv_Type16TxnCount ,
	Type17TxnCount = lv_Type17TxnCount ,
	EMVCCTxnCount = lv_EMVTxnCount
	where BillingReportId = P_BillingReportId and PointOfSaleId=lv_PointOfSaleId;
  
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_BillingReportTxnCount


-- Start Procedure sp_FindBillingReportChanges

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_FindBillingReportChanges $$
CREATE PROCEDURE sp_FindBillingReportChanges (in P_BillingReportId int unsigned, in P_Previous_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
-- declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
-- declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
-- declare lv_DateRangeBegin, lv_DateRangeEnd Date;
-- declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;
declare lv_SerialNumber varchar(20);
declare lv_cnt MEDIUMINT UNSIGNED ;

declare c1 cursor  for
select SerialNumber from BillingReportPaystation 
WHERE BillingReportId = P_BillingReportId 
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_SerialNumber;

	select count(*) into lv_cnt from
	(
	select SerialNumber,IsActivated, CustomerId, IsTrial, TrialExpiryGMT, ActivationChangeGMT,PaystationSettingName, POSLocationId
	ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,
	ServicePayByPhone,ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned 
	from BillingReportPaystation where SerialNumber = lv_SerialNumber and BillingReportId = P_Previous_BillingReportId
	and PointOfSaleId = ( select max(PointOfSaleId) from BillingReportPaystation where SerialNumber = lv_SerialNumber and BillingReportId = P_Previous_BillingReportId)
	union 
	select SerialNumber,IsActivated, CustomerId, IsTrial, TrialExpiryGMT, ActivationChangeGMT,PaystationSettingName, POSLocationId
	ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,
	ServicePayByPhone,ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned 
	from BillingReportPaystation where SerialNumber = lv_SerialNumber and BillingReportId = P_BillingReportId
	and PointOfSaleId = ( select max(PointOfSaleId) from BillingReportPaystation where SerialNumber = lv_SerialNumber and BillingReportId = P_BillingReportId)
	) A;
	
	If (lv_cnt > 1) THEN  -- Changed
		-- Changed the below query on Sep 24 2014
		UPDATE BillingReportPaystation
		SET HasChanged = 1
		where BillingReportId = P_BillingReportId and SerialNumber=lv_SerialNumber
		and PointOfSaleId = (select id from ( select max(PointOfSaleId) as Id from BillingReportPaystation as A where SerialNumber = lv_SerialNumber and BillingReportId = P_BillingReportId ) B);
	
		
	
	END IF ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_FindBillingReportChanges

-- Start Procedure sp_RecordBillingReportChanges

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_RecordBillingReportChanges $$
CREATE PROCEDURE sp_RecordBillingReportChanges (in P_BillingReportId int unsigned, in P_Previous_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
-- declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
-- declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
-- declare lv_DateRangeBegin, lv_DateRangeEnd Date;
-- declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;

declare lv_cnt,lv_PaystationId, lv_PointOfSaleId, lvc_CustomerId, lvc_POSLocationId MEDIUMINT UNSIGNED ;
declare lv_CustomerName,lv_LocationName,lv_Change_CustomerName varchar(30);

declare lv_prv_SerialNumber, lv_SerialNumber varchar(20);
declare lv_prv_IsActivated, lv_IsActivated tinyint(3) unsigned;
declare lv_prv_CustomerId, lv_CustomerId, lv_prv_POSLocationId, lv_POSLocationId mediumint(8) unsigned;
declare lv_prv_IsTrial, lv_IsTrial tinyint(3) unsigned;
declare lv_prv_TrialExpiryGMT, lv_TrialExpiryGMT, lv_prv_ActivationChangeGMT, lv_ActivationChangeGMT datetime; 
declare lv_prv_PaystationSettingName, lv_PaystationSettingName varchar(20); 
declare lv_prv_ServiceStandardReports, lv_prv_ServiceAlerts, lv_prv_ServiceRealTimeCC, lv_prv_ServiceBatchCC, lv_prv_ServiceCampusCard, lv_prv_ServiceCoupon, lv_prv_ServicePasscard, lv_prv_ServiceSmartCard tinyint(3) unsigned ;
declare	lv_prv_ServicePayByPhone, lv_prv_ServiceDigitalAPIRead, lv_prv_ServiceDigitalAPIWrite, lv_prv_ServiceExtendByCell tinyint(3) unsigned ;
declare	lv_ServiceStandardReports, lv_ServiceAlerts, lv_ServiceRealTimeCC, lv_ServiceBatchCC, lv_ServiceCampusCard, lv_ServiceCoupon, lv_ServicePasscard, lv_ServiceSmartCard tinyint(3) unsigned ;
declare	lv_ServicePayByPhone, lv_ServiceDigitalAPIRead, lv_ServiceDigitalAPIWrite, lv_ServiceExtendByCell  tinyint(3) unsigned ;	
	
declare lv_Change_IsActivated varchar(5);
declare lv_Change_IsTrial varchar(10);
declare lv_Change_TrialExpiryGMT varchar(30);
declare lv_Change_ServiceStandardReports, lv_Change_ServiceAlerts, lv_Change_ServiceRealTimeCC varchar(5);
declare	lv_Change_ServiceBatchCC, lv_Change_ServiceCampusCard, lv_Change_ServiceCoupon varchar(5);				
declare	lv_Change_ServiceSmartCard, lv_Change_ServicePayByPhone, lv_Change_ServiceDigitalAPIRead, lv_Change_ServiceDigitalAPIWrite, lv_Change_ServiceExtendByCell,lv_Change_ServicePasscard varchar(5);
declare lv_Change_ActivationChangeGMT varchar(30);
declare lv_Change_PaystationSettingName, lv_Change_Location varchar(30);
declare IsChangedFlag tinyint ;
				
declare c1 cursor  for
select SerialNumber,PaystationId,PointOfSaleId,CustomerId,POSLocationId,CustomerName, LocationName    from BillingReportPaystation 
WHERE BillingReportId = P_BillingReportId AND HasChanged = 1
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

DELETE FROM BillingReportChanges where BillingReportId = P_BillingReportId;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_SerialNumber, lv_PaystationId, lv_PointOfSaleId, lvc_CustomerId, lvc_POSLocationId, lv_CustomerName, lv_LocationName;

	-- Need to initialize all variables
	set lv_prv_SerialNumber = null;
	set lv_prv_IsActivated= null;
	set lv_prv_CustomerId= null;
	set lv_prv_IsTrial = null;
	set lv_prv_TrialExpiryGMT= null;
	set lv_prv_ActivationChangeGMT= null;
	set lv_prv_PaystationSettingName= null;
	set lv_prv_POSLocationId= null;
	set lv_prv_ServiceStandardReports= null;
	set lv_prv_ServiceAlerts= null;
	set lv_prv_ServiceRealTimeCC= null;
	set lv_prv_ServiceBatchCC= null;
	set lv_prv_ServiceCampusCard= null;
	set lv_prv_ServiceCoupon= null;
	set lv_prv_ServicePasscard= null;
	set lv_prv_ServiceSmartCard= null;
	set lv_prv_ServicePayByPhone= null;
	set lv_prv_ServiceDigitalAPIRead= null;
	set lv_prv_ServiceDigitalAPIWrite= null;
	set lv_prv_ServiceExtendByCell = null;
	
	
	set lv_IsActivated= null;
	set lv_CustomerId= null;
	set lv_IsTrial= null;
	set lv_TrialExpiryGMT= null;
	set lv_ActivationChangeGMT= null;
	set lv_PaystationSettingName= null;
	set lv_POSLocationId = null;
	set lv_ServiceStandardReports= null;
	set lv_ServiceAlerts= null;
	set lv_ServiceRealTimeCC= null;
	set lv_ServiceBatchCC= null;
	set lv_ServiceCampusCard= null;
	set lv_ServiceCoupon= null;
	set lv_ServicePasscard= null;
	set lv_ServiceSmartCard= null;
	set lv_ServicePayByPhone= null;
	set lv_ServiceDigitalAPIRead= null;
	set lv_ServiceDigitalAPIWrite= null;
	set lv_ServiceExtendByCell= null;
	
	set IsChangedFlag = 0;
	
	-- Get data from Previous BillingReport
	select SerialNumber,IsActivated, CustomerId, IsTrial, TrialExpiryGMT, ActivationChangeGMT,PaystationSettingName, POSLocationId,
	ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,
	ServicePayByPhone,ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell 
	into
	lv_prv_SerialNumber,lv_prv_IsActivated, lv_prv_CustomerId, lv_prv_IsTrial, lv_prv_TrialExpiryGMT, lv_prv_ActivationChangeGMT, lv_prv_PaystationSettingName, lv_prv_POSLocationId,
	lv_prv_ServiceStandardReports, lv_prv_ServiceAlerts, lv_prv_ServiceRealTimeCC, lv_prv_ServiceBatchCC, lv_prv_ServiceCampusCard, lv_prv_ServiceCoupon, lv_prv_ServicePasscard, lv_prv_ServiceSmartCard,
	lv_prv_ServicePayByPhone, lv_prv_ServiceDigitalAPIRead, lv_prv_ServiceDigitalAPIWrite, lv_prv_ServiceExtendByCell 
	from BillingReportPaystation where PointOfSaleId = lv_PointOfSaleId and BillingReportId = P_Previous_BillingReportId;
	
	-- Get data from Present BillingReport
	select SerialNumber,IsActivated, CustomerId, IsTrial, TrialExpiryGMT, ActivationChangeGMT,PaystationSettingName, POSLocationId,
	ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,
	ServicePayByPhone,ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell
	into
	lv_SerialNumber,lv_IsActivated, lv_CustomerId, lv_IsTrial, lv_TrialExpiryGMT, lv_ActivationChangeGMT, lv_PaystationSettingName, lv_POSLocationId,
	lv_ServiceStandardReports, lv_ServiceAlerts, lv_ServiceRealTimeCC, lv_ServiceBatchCC, lv_ServiceCampusCard, lv_ServiceCoupon, lv_ServicePasscard, lv_ServiceSmartCard,
	lv_ServicePayByPhone, lv_ServiceDigitalAPIRead, lv_ServiceDigitalAPIWrite, lv_ServiceExtendByCell 	
	from BillingReportPaystation where PointOfSaleId = lv_PointOfSaleId and BillingReportId = P_BillingReportId;
	
	-- IsActivated
	if (lv_prv_IsActivated <> lv_IsActivated) THEN
		select concat('*',if(lv_IsActivated=1,'Yes','No') ,'*') into lv_Change_IsActivated;
		set IsChangedFlag=1;
	else
		select if(lv_IsActivated=1,'Yes','No') into lv_Change_IsActivated;
	END IF;
	
	-- CustomerName
	IF (lv_prv_CustomerId <> lv_CustomerId) THEN
		select concat('*',lv_CustomerName ,'*') into lv_Change_CustomerName;
		set IsChangedFlag=1;
	ELSE
		select lv_CustomerName into lv_Change_CustomerName ;
	END IF;
	
	-- IsTrial
	IF (lv_prv_IsTrial <> lv_IsTrial) THEN
		select concat('*',if(lv_IsTrial=2,'Yes','No') ,'*') into lv_Change_IsTrial;
		set IsChangedFlag=1;
	ELSE
		select lv_IsTrial into lv_Change_IsTrial ;
	END IF;
	
	-- TrialExpiryGMT
	IF (lv_prv_TrialExpiryGMT <> lv_TrialExpiryGMT) THEN
		select concat('*',lv_TrialExpiryGMT,'*') into lv_Change_TrialExpiryGMT;
		set IsChangedFlag=1;
	ELSE
		select lv_TrialExpiryGMT into lv_Change_TrialExpiryGMT ;
	END IF;
	
	-- ActivationChangeGMT
	IF ( lv_prv_ActivationChangeGMT <> lv_ActivationChangeGMT) THEN
		select concat('*',lv_ActivationChangeGMT,'*') into lv_Change_ActivationChangeGMT ; 
		set IsChangedFlag=1;
	ELSE
		select lv_ActivationChangeGMT into lv_Change_ActivationChangeGMT ;
	END IF;
	
	-- PaystationSettingName
	IF ( lv_prv_PaystationSettingName <> lv_PaystationSettingName) THEN
		select concat('*',lv_PaystationSettingName,'*') into lv_Change_PaystationSettingName ;
		set IsChangedFlag=1;
	ELSE
		select lv_PaystationSettingName into lv_Change_PaystationSettingName ;
	END IF;
	
	-- Location
	IF (lv_prv_POSLocationId <> lv_POSLocationId) THEN
		select concat('*',lv_LocationName,'*') into lv_Change_Location ;
		set IsChangedFlag=1;
	ELSE
		select lv_LocationName into lv_Change_Location;
	END IF ;
	
	-- ServiceStandardReports
	IF (lv_prv_ServiceStandardReports <> lv_ServiceStandardReports) THEN
		select concat('*',if(lv_ServiceStandardReports=1,'On','Off'),'*') into lv_Change_ServiceStandardReports ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceStandardReports=1,'On','Off') into lv_Change_ServiceStandardReports ;
	END IF ;
	 
	
	-- ServiceAlerts
	IF (lv_prv_ServiceAlerts <> lv_ServiceAlerts) THEN
		select concat('*',if(lv_ServiceAlerts=1,'On','Off'),'*') into lv_Change_ServiceAlerts ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceAlerts=1,'On','Off') into lv_Change_ServiceAlerts ;
	END IF ;
	
	
	-- ServiceRealTimeCC
	IF (lv_prv_ServiceRealTimeCC <> lv_ServiceRealTimeCC) THEN
		select concat('*',if(lv_ServiceRealTimeCC=1,'On','Off'),'*') into lv_Change_ServiceRealTimeCC ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceRealTimeCC=1,'On','Off') into lv_Change_ServiceRealTimeCC ;
	END IF ;
	
	-- ServiceBatchCC
	IF (lv_prv_ServiceBatchCC <> lv_ServiceBatchCC) THEN
		select concat('*',if(lv_ServiceBatchCC=1,'On','Off'),'*') into lv_Change_ServiceBatchCC ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceBatchCC=1,'On','Off') into lv_Change_ServiceBatchCC ;
	END IF ;
	
	-- ServiceCampusCard
	IF (lv_prv_ServiceCampusCard <> lv_ServiceCampusCard) THEN
		select concat('*',if(lv_ServiceCampusCard=1,'On','Off'),'*') into lv_Change_ServiceCampusCard ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceCampusCard=1,'On','Off') into lv_Change_ServiceCampusCard ;
	END IF ;
	
	-- ServiceCoupon
	IF (lv_prv_ServiceCoupon <> lv_ServiceCoupon) THEN
		select concat('*',if(lv_ServiceCoupon=1,'On','Off'),'*') into lv_Change_ServiceCoupon ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceCoupon=1,'On','Off') into lv_Change_ServiceCoupon ;
	END IF ;
	
	
	
	-- ServicePasscard
	IF (lv_prv_ServicePasscard <> lv_ServicePasscard) THEN
		select concat('*',if(lv_ServicePasscard=1,'On','Off'),'*') into lv_Change_ServicePasscard ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServicePasscard=1,'On','Off') into lv_Change_ServicePasscard ;
	END IF ;
	
	-- ServiceSmartCard
	IF (lv_prv_ServiceSmartCard <> lv_ServiceSmartCard) THEN
		select concat('*',if(lv_ServiceSmartCard=1,'On','Off'),'*') into lv_Change_ServiceSmartCard ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceSmartCard=1,'On','Off') into lv_Change_ServiceSmartCard ;
	END IF ;
	
	
	 
	-- ServicePayByPhone
	IF (lv_prv_ServicePayByPhone <> lv_ServicePayByPhone) THEN
		select concat('*',if(lv_ServicePayByPhone=1,'On','Off'),'*') into lv_Change_ServicePayByPhone ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServicePayByPhone=1,'On','Off') into lv_Change_ServicePayByPhone ;
	END IF ;
	
	-- ServiceDigitalAPIRead
	IF (lv_prv_ServiceDigitalAPIRead <> lv_ServiceDigitalAPIRead) THEN
		select concat('*',if(lv_ServiceDigitalAPIRead=1,'On','Off'),'*') into lv_Change_ServiceDigitalAPIRead ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceDigitalAPIRead=1,'On','Off') into lv_Change_ServiceDigitalAPIRead ;
	END IF ;
	
	-- ServiceDigitalAPIWrite
	IF (lv_prv_ServiceDigitalAPIWrite <> lv_ServiceDigitalAPIWrite) THEN
		select concat('*',if(lv_ServiceDigitalAPIWrite=1,'On','Off'),'*') into lv_Change_ServiceDigitalAPIWrite ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceDigitalAPIWrite=1,'On','Off') into lv_Change_ServiceDigitalAPIWrite ;
	END IF ;
	
	-- ServiceExtendByCell
	IF (lv_prv_ServiceExtendByCell <> lv_ServiceExtendByCell) THEN
		select concat('*',if(lv_ServiceExtendByCell=1,'On','Off'),'*') into lv_Change_ServiceExtendByCell ;
		set IsChangedFlag=1;
	ELSE
		select if(lv_ServiceExtendByCell=1,'On','Off') into lv_Change_ServiceExtendByCell ;
	END IF ;
	
	if (IsChangedFlag=1) THEN
		INSERT INTO BillingReportChanges(BillingReportId,PaystationId,PointOfSaleId,CustomerId,POSLocationId,SerialNumber,IsActivated,CustomerName,IsTrial,TrialExpiryGMT,
				ActivationChangeGMT,PaystationSettingName,Location,ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,
				ServiceBatchCC,ServiceCampusCard,ServiceCoupon,
				ServicePasscard,ServiceSmartCard,ServicePayByPhone,ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell)
		VALUES (P_BillingReportId , lv_PaystationId, lv_PointOfSaleId, lvc_CustomerId, lvc_POSLocationId, lv_SerialNumber, lv_Change_IsActivated, lv_Change_CustomerName,lv_Change_IsTrial, lv_Change_TrialExpiryGMT,
				lv_Change_ActivationChangeGMT, lv_Change_PaystationSettingName, lv_Change_Location,  lv_Change_ServiceStandardReports, lv_Change_ServiceAlerts, lv_Change_ServiceRealTimeCC, 
				lv_Change_ServiceBatchCC, lv_Change_ServiceCampusCard, lv_Change_ServiceCoupon,				
				lv_Change_ServicePasscard, lv_Change_ServiceSmartCard, lv_Change_ServicePayByPhone, lv_Change_ServiceDigitalAPIRead, ServiceDigitalAPIWrite, lv_Change_ServiceExtendByCell);
	END IF;
	
set indexm_pos = indexm_pos +1;
end while;

close c1; 

END $$

DELIMITER ;

-- End Procedure sp_RecordBillingReportChanges


DROP PROCEDURE IF EXISTS sp_BillingReport ;

delimiter //

CREATE PROCEDURE sp_BillingReport ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare lv_BR_Id MEDIUMINT UNSIGNED ;
declare lv_BR_DateRangeBegin, lv_BR_DateRangeEnd DATE ;
declare lv_BR_IsPosted TINYINT UNSIGNED ;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM BillingReportDateRange where Status  = 0 and ExecutionDateTime <= now();

IF (lv_count = 1 ) THEN

  SELECT Id,DateRangeBegin, DateRangeEnd,  IsPosted INTO lv_BR_Id, lv_BR_DateRangeBegin, lv_BR_DateRangeEnd,  lv_BR_IsPosted
  FROM BillingReportDateRange 
  WHERE Status  = 0 and ExecutionDateTime <= now() ;
  
  UPDATE BillingReportDateRange SET Status = 1 WHERE ID = lv_BR_Id ;
  
  CALL sp_RunBillingReport(lv_BR_DateRangeBegin, lv_BR_DateRangeEnd, lv_BR_IsPosted, 0); 

END IF;


END//

delimiter ;

-- The below code is copied from Billing Report Server. (This code exists before GIT. Hence it is not in GIT)

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCountDCOverages ;

delimiter //

CREATE  PROCEDURE `sp_BillingReportTxnCountDCOverages`(in P_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_DateRangeBegin, lv_DateRangeEnd Date;
declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;
declare lv_EMVTxnCount INT UNSIGNED ;

declare c1 cursor  for
select CustomerId, PointOfSaleId, B.DateRangeBegin, B.DateRangeEnd from BillingReportPaystationDCOverages A, BillingReportDCOverages B WHERE 
A.BillingReportId = B.Id and A.BillingReportId = P_BillingReportId
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;



set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_PointOfSaleId, lv_DateRangeBegin, lv_DateRangeEnd;

	set lv_TransactionCount = 0;
	set lv_EMVTxnCount = 0;
	
	-- EMS-10647
	select count(*) into lv_TransactionCount 
	from Purchase P , CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day)) 
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month));
		
	
	-- EMS-10647
	select count(*) into lv_Type16TxnCount 
	from Purchase P , CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day))
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month))
	and TransactionTypeId = 16;
	
	
	-- EMS-10647
	select count(*) into lv_Type17TxnCount 
	from Purchase  P , CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day))
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month))
	and TransactionTypeId = 17;
	
	-- EMS-10283
	SELECT COUNT(*) into lv_EMVTxnCount
	FROM Purchase P 
	inner join ProcessorTransaction PT on P.Id = PT.PurchaseId
	inner join MerchantAccount MA on PT.MerchantAccountId = MA.Id
	inner join Processor Pro on (MA.ProcessorId = Pro.Id and Pro.IsEMV = 1)
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day)) 
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month));
		
	
	UPDATE BillingReportPaystationDCOverages
	SET TransactionCount = lv_TransactionCount, 
	Type16TxnCount = lv_Type16TxnCount ,
	Type17TxnCount = lv_Type17TxnCount ,
	EMVCCTxnCount = lv_EMVTxnCount
	where BillingReportId = P_BillingReportId and PointOfSaleId=lv_PointOfSaleId;
  
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//

delimiter ;


-- This is not used

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCountNewUnits ;

delimiter //

CREATE PROCEDURE `sp_BillingReportTxnCountNewUnits`(in P_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_DateRangeBegin, lv_DateRangeEnd Date;
declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;

declare c1 cursor  for
select CustomerId, PointOfSaleId, B.DateRangeBegin, B.DateRangeEnd from BillingReportPaystation A, BillingReport B WHERE 
A.BillingReportId = B.Id and A.BillingReportId = P_BillingReportId
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;



set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_PointOfSaleId, lv_DateRangeBegin, lv_DateRangeEnd;

	set lv_TransactionCount = 0;
	
	
	
	select count(*) into lv_TransactionCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	PurchaseGMT >= lv_DateRangeBegin
	and PurchaseGMT <= lv_DateRangeEnd;
	
	
    
    
    select count(*) into lv_Type16TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	PurchaseGMT >= lv_DateRangeBegin
	and PurchaseGMT <= lv_DateRangeEnd
	and TransactionTypeId = 16;
	
	
	            
    
    
   
    select count(*) into lv_Type17TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	PurchaseGMT >= lv_DateRangeBegin
	and PurchaseGMT <= lv_DateRangeEnd
	and TransactionTypeId = 17;
	
	
	UPDATE BillingReportPaystationNew
	SET TransactionCount = lv_TransactionCount, 
	Type16TxnCount = lv_Type16TxnCount ,
	Type17TxnCount = lv_Type17TxnCount
	where BillingReportId = P_BillingReportId and PointOfSaleId=lv_PointOfSaleId;
  
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//

delimiter ;


DROP PROCEDURE IF EXISTS sp_RunBillingReportDCOverages ;

delimiter //


CREATE PROCEDURE `sp_RunBillingReportDCOverages`(IN bdt DATE, IN edt DATE, IN post TINYINT UNSIGNED, IN trace TINYINT UNSIGNED)
BEGIN

    
    DECLARE task TINYINT UNSIGNED;    

     
    DECLARE idBR INT UNSIGNED;
   
    
    DECLARE DBversion TINYINT UNSIGNED;
    SET DBversion = 2;

    
    SET task = 0, idBR = 0;

    
    CALL sp_StartBillingReportDCOverages (task, idBR, bdt, edt, trace);

	UPDATE BillingReportDCOverages SET RunEnd = UTC_TIMESTAMP() 
	WHERE Id = idBR;
  
END//

delimiter ;


DROP PROCEDURE IF EXISTS sp_StartBillingReportDCOverages ;

delimiter //

CREATE PROCEDURE `sp_StartBillingReportDCOverages`(INOUT task TINYINT UNSIGNED, INOUT idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    
    DECLARE prvbdt DATE;
    DECLARE prvedt DATE;
    DECLARE prvBR  INT UNSIGNED;
    
    
    SELECT Id, DateRangeBegin, DateRangeEnd INTO prvBR, prvbdt, prvedt
    FROM   BillingReportDCOverages 
    WHERE  Id = (SELECT MAX(Id) FROM BillingReportDCOverages WHERE IsPosted=1);
   
    
    IF bdt > edt THEN SET task = 4; END IF;
                      
    
    SELECT IF((SELECT COUNT(*) FROM BillingReportDCOverages)=0 AND task=0,1,task) INTO task;

    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages))=1 AND task=0,1,task) INTO task;
     
    
    IF task=1 THEN      
        
        SELECT IF(ADDDATE(prvedt,INTERVAL 1 DAY)!=bdt,5,task) INTO task;
    END IF;
   
    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages))=0 AND task=0,3,task) INTO task;

    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages) AND DateRangeBegin=bdt AND DateRangeEnd=edt)=0 AND task=3,2,task) INTO task;

    
    CASE task
        
        WHEN 0 THEN
        BEGIN
            SELECT 'ERROR. Could not determine whether to create a new Billing Report or update an existing Billing Report.' AS ErrorMessage;
        END;

        
        WHEN 1 THEN
        BEGIN
            INSERT BillingReportDCOverages (DateRangeBegin,DateRangeEnd,RunBegin,IsPosted,IsError) VALUES (bdt,edt,UTC_TIMESTAMP(),0,0);
            SET idBR = last_insert_id();
        END;

        
        WHEN 2 THEN
        BEGIN
            SELECT Id FROM BillingReportDCOverages WHERE DateRangeBegin=bdt AND DateRangeEnd=edt AND IsPosted=0 AND Id=(SELECT MAX(Id) FROM BillingReportDCOverages) INTO idBR;
            UPDATE BillingReportDCOverages SET RunBegin=UTC_TIMESTAMP(), RunEnd=UTC_TIMESTAMP(), IsError=0 WHERE Id=idBR;
            DELETE FROM BillingReportPaystationDCOverages WHERE BillingReportId=idBR;
        END;

        
        WHEN 3 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. Parameter dates do not match current Billing Report dates: ',bdt,' and ',edt,' do not match ',DateRangeBegin,' and ',DateRangeEnd) AS ErrorMessage
            FROM   BillingReportDCOverages WHERE Id = (SELECT MAX(Id) FROM BillingReportDCOverages);
        END;
         
        
        WHEN 4 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' cannot be greater than the report end date ',edt) AS ErrorMessage;
        END;
         
         
        WHEN 5 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' must be the next day following the previous reports end date ',prvedt) AS ErrorMessage;
        END;

        
        ELSE
        BEGIN
            SELECT 'ERROR. Could not resolve Billing Report parameters.' AS ErrorMessage;
        END;
    END CASE;
    
    
    IF task=1 OR task=2 THEN    
        
        
        INSERT INTO BillingReportPaystationDCOverages 
				(BillingReportId,CustomerId,CustomerName,IsTrial, TrialExpiryGMT, PaystationId,PointOfSaleId,POSName,POSLocationId,ProvisionedGMT,POSLastModifiedGMT,SerialNumber,PaystationSettingName,LocationName,
				LastHeartbeatGMT,IsActivated,IsLocked,IsDigitalConnect,IsBillableMonthlyOnActivation,IsBillableMonthlyForEms,IsDeleted, ActivationChangeGMT,
				ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,ServicePayByPhone,
				ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned,IsEMV,TerminalId)
				Select idBR,c.Id,c.Name,c.CustomerStatusTypeId,c.TrialExpiryGMT,pst.Id,pos.Id,pos.Name,pos.LocationId,pos.ProvisionedGMT,pos.LastModifiedGMT,pos.SerialNumber,pss.PaystationSettingName,l.Name,
				phb.LastHeartbeatGMT,ps.IsActivated,ps.IsLocked, ps.IsDigitalConnect ,ps.IsBillableMonthlyOnActivation ,ps.IsBillableMonthlyForEms ,pst.IsDeleted ,ps.IsActivatedGMT ,
				csStdReports.IsEnabled ,csAlerts.IsEnabled,csCCProcess.IsEnabled,csBatchCCProcess.IsEnabled, csCampusProcess.IsEnabled, csCoupons.IsEnabled, csPasscards.IsEnabled,csSmartcards.IsEnabled,csExtendByPhone.IsEnabled,
				csDigitalRead.IsEnabled, csDigitalWrite.IsEnabled,csPayByCell.IsEnabled,ps.IsDecommissioned,pro.IsEMV, EMVMA.Field1
				FROM PointOfSale pos 
				INNER JOIN Location l ON pos.LocationId = l.Id
				INNER JOIN Customer c ON pos.CustomerId = c.Id
				INNER JOIN POSServiceState pss ON pos.Id = pss.PointOfSaleId
				INNER JOIN POSHeartbeat phb ON pos.Id = phb.PointOfSaleId
				INNER JOIN POSStatus ps ON ps.PointOfSaleId = pos.Id
				inner join CustomerSubscription csStdReports on csStdReports.CustomerId = c.Id
				inner join CustomerSubscription csAlerts on csAlerts.CustomerId = c.Id
				inner join CustomerSubscription csCCProcess on csCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csBatchCCProcess on csBatchCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csCampusProcess on csCampusProcess.CustomerId = c.Id
				inner join CustomerSubscription csCoupons on csCoupons.CustomerId = c.Id
				inner join CustomerSubscription csPasscards on csPasscards.CustomerId = c.Id
				inner join CustomerSubscription csSmartcards on csSmartcards.CustomerId = c.Id
				inner join CustomerSubscription csExtendByPhone on csExtendByPhone.CustomerId = c.Id
				inner join CustomerSubscription csDigitalRead on csDigitalRead.CustomerId = c.Id
				inner join CustomerSubscription csDigitalWrite on csDigitalWrite.CustomerId = c.Id
				inner join CustomerSubscription csPayByCell on csPayByCell.CustomerId = c.Id
				inner join CustomerStatusType cStatus on c.CustomerStatusTypeId = cStatus.Id
				inner join Paystation pst on pst.Id = pos.PaystationId 
				LEFT JOIN MerchantPOS mpos ON (pos.Id = mpos.PointOfSaleId and mpos.IsDeleted = 0 and mpos.CardTypeId = 1)
				LEFT JOIN MerchantAccount ma ON mpos.MerchantAccountId = ma.Id
				LEFT JOIN  Processor pro ON ma.ProcessorId = pro.Id
				LEFT JOIN MerchantAccount EMVMA on (mpos.MerchantAccountId = EMVMA.Id and EMVMA.ProcessorId = pro.Id and pro.IsEMV=1)
				 
				where 
				csStdReports.SubscriptionTypeId = 100 and
				csAlerts.SubscriptionTypeId = 200 and
				csCCProcess.SubscriptionTypeId = 300 and
				csBatchCCProcess.SubscriptionTypeId = 400 and
				csCampusProcess.SubscriptionTypeId = 500 and
				csCoupons.SubscriptionTypeId = 600 and 
				csPasscards.SubscriptionTypeId = 700 and
				csSmartcards.SubscriptionTypeId = 800 and
				csExtendByPhone.SubscriptionTypeId = 900 and
				csDigitalRead.SubscriptionTypeId = 1000 and
				csDigitalWrite.SubscriptionTypeId = 1100 and
				csPayByCell.SubscriptionTypeId = 1200 
				AND 	UPPER(pos.SerialNumber) NOT LIKE '%X%'     
				AND     pos.SerialNumber NOT LIKE '9%'            
				AND     pos.SerialNumber NOT LIKE '8%'
				AND     UPPER(pos.SerialNumber) NOT LIKE '%VER%';
				
		
		
		CALL sp_BillingReportTxnCountDCOverages(idBR);
		
		SELECT  Id FROM BillingReportDCOverages WHERE Id = (SELECT MAX(Id) FROM BillingReportDCOverages WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
			
		
		
    END IF;
 
    
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_StartBillingReportDCOverages' AS Trace;
    END IF;
    
END

/* These are SQL's to generate Billing Report 
-- 1. Activated Paystations

SELECT  SerialNumber as "Pay Station Serial Number", IF(IsActivated=1,'Yes','No') AS Activated, CustomerName AS "Customer Name", 
        IF(IsTrial=2,'YES','NO') AS 'Is Trial', IF(TrialExpiryGMT IS NULL,'N/A',TrialExpiryGMT) AS "Trial Expires", 
		ActivationChangeGMT AS 'Latest Activation Date', IF(PaystationSettingName IS NULL,'',PaystationSettingName) AS 'BOSS Pay Station Setting',
		LocationName AS Location, TransactionCount AS 'Transactions Processed This Period',
		IF(ServiceStandardReports = 1, 'On','') AS "Standard Reports",
		IF(ServiceAlerts = 1, 'On','') AS "Alerts",
		IF(ServiceRealTimeCC = 1, 'On','') AS "Real-time Credit Card Processing",
		IF(ServiceBatchCC = 1, 'On','') AS "Batch Credit Card Processing",
		IF(ServiceCampusCard = 1, 'On','') AS "Real-time Campus Card Integration",
		IF(ServiceCoupon = 1, 'On','') AS "Coupons",
		IF(ServicePasscard = 1, 'On','') AS "Passcards",
		IF(ServiceSmartCard = 1, 'On','') AS "Smart Cards",
		IF(ServicePayByPhone = 1, 'On','') AS "Extend-By-Phone",
		IF(ServiceDigitalAPIRead = 1, 'On','') AS "Digital API: Read",
		IF(ServiceDigitalAPIWrite = 1, 'On','') AS "Digital API: Write",
		IF(ServiceExtendByCell = 1, 'On','') AS "3rd Party By-By-Cell Integration"      
		
        INTO OUTFILE 'C:\\BillingReport\\Iris_BR_new.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'        
		
FROM    BillingReportPaystation 
WHERE   BillingReportId = (SELECT MAX(Id) FROM BillingReport)
AND     ActivationChangeGMT >= (SELECT DateRangeBegin FROM BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport))
ORDER BY CustomerName, SerialNumber;

-- 2. All Paystations

SELECT  SerialNumber as "Pay Station Serial Number", IF(IsActivated=1,'Yes','No') AS Activated, CustomerName AS "Customer Name",
        IF(IsTrial=2,'YES','NO') AS 'Is Trial', IF(TrialExpiryGMT IS NULL,'N/A',TrialExpiryGMT) AS "Trial Expires", 
		ActivationChangeGMT AS 'Latest Activation Date', IF(PaystationSettingName IS NULL,'',PaystationSettingName) AS 'BOSS Pay Station Setting',
		LocationName AS Location, TransactionCount AS 'Transactions Processed This Period',
		IF(ServiceStandardReports = 1, 'On','') AS "Standard Reports",
		IF(ServiceAlerts = 1, 'On','') AS "Alerts",
		IF(ServiceRealTimeCC = 1, 'On','') AS "Real-time Credit Card Processing",
		IF(ServiceBatchCC = 1, 'On','') AS "Batch Credit Card Processing",
		IF(ServiceCampusCard = 1, 'On','') AS "Real-time Campus Card Integration",
		IF(ServiceCoupon = 1, 'On','') AS "Coupons",
		IF(ServicePasscard = 1, 'On','') AS "Passcards",
		IF(ServiceSmartCard = 1, 'On','') AS "Smart Cards",
		IF(ServicePayByPhone = 1, 'On','') AS "Extend-By-Phone",
		IF(ServiceDigitalAPIRead = 1, 'On','') AS "Digital API: Read",
		IF(ServiceDigitalAPIWrite = 1, 'On','') AS "Digital API: Write",
		IF(ServiceExtendByCell = 1, 'On','') AS "3rd Party By-By-Cell Integration"   
		
        INTO OUTFILE 'C:\\BillingReport\\Iris_BR_all.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'        
		
FROM    BillingReportPaystation 
WHERE   BillingReportId = (SELECT MAX(Id) FROM BillingReport)
ORDER BY CustomerName, SerialNumber;

-- 3. Changes

SELECT  SerialNumber as "Pay Station Serial Number", IsActivated AS Activated, CustomerName AS "Customer Name", 
        IsTrial AS 'Is Trial', IF(TrialExpiryGMT IS NULL,'N/A',TrialExpiryGMT) AS "Trial Expires", 
		ActivationChangeGMT AS 'Latest Activation Date', IF(PaystationSettingName IS NULL,'',PaystationSettingName) AS 'BOSS Pay Station Setting',
		Location AS Location, '' AS 'Transactions Processed This Period',
		ServiceStandardReports,
		ServiceAlerts,
		ServiceRealTimeCC,
		ServiceBatchCC,
		ServiceCampusCard,
		ServiceCoupon,
		ServicePasscard,
		ServiceSmartCard,
		ServicePayByPhone,
		ServiceDigitalAPIRead,
		ServiceDigitalAPIWrite,
		ServiceExtendByCell       
		
        INTO OUTFILE 'C:\\BillingReport\\Iris_BR_changes.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'        
		
FROM    BillingReportChanges 
WHERE   BillingReportId = (SELECT MAX(Id) FROM BillingReport)
ORDER BY CustomerName, SerialNumber;

-- 4. Bundled Data & Extend by Phone

SELECT  SerialNumber AS "Pay Station", CustomerName AS Customer, 
        APNTransactions AS "Bundled Data Transaction Count", IF(APNTransactions>2000,APNTransactions-2000,'') AS "Count Above Threshold of 2000", IF(APNTransactions>2000,(APNTransactions-2000)*0.02,'') AS  "Bundled Data Charge",
        IF(Type16TxnCount>0,Type16TxnCount,'') AS "Extend by Phone Count",
        IF(Type16TxnCount>0,Type16TxnCount*0.25,'') AS "Extend by Phone Charge"
		
		INTO OUTFILE 'C:\\BillingReport\\Iris_BundledDataEbp.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
				
FROM    BillingReportPaystation 
WHERE   BillingReportId = (SELECT MAX(Id) FROM BillingReport) 
AND     (APN IS NOT NULL OR Type16TxnCount > 0)
ORDER BY CustomerName, SerialNumber; 

-- 5. Extend by Phone Utilization

SELECT  SerialNumber AS "Pay Station", CustomerName AS Customer, 
        IF(Type17TxnCount>0,Type17TxnCount,'') AS Issued,
        IF(Type16TxnCount>0,Type16TxnCount,'') AS Extended
		
		INTO OUTFILE 'C:\\BillingReport\\Iris_EbpUtil.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
		
FROM    BillingReportPaystation 
WHERE   BillingReportId = (SELECT MAX(Id) FROM BillingReport) 
AND     (Type17TxnCount > 0 OR Type16TxnCount > 0)
ORDER BY CustomerName, SerialNumber; 

*/

