

0) Clean up
truncate ETLNotProcessed;
truncate ETLQueueRecordCount;
truncate ETLExecutionLog;
truncate StaggingPurchase ;



1) Insert a record in to Purchase/Permit/PaymentCard/ProcessorTransaction/POSCollection
2) Check if the corresponding records exists in StaggingTables
3) Use the Cursor Query in each Stored Procedure to check if data is returned
4) Run the JOB to verify if data is posted in Destination Tables , Deleted from Stagging and Archived to Archived Tables


1)
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

INSERT INTO Purchase (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, 
PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, 
CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline,
 IsRefundSlip, CreatedGMT) VALUES (5,14,now(),3,17,9,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,now());

INSERT INTO `Permit` (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, 
LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES 
(13,19,11,14,3,2,NULL,NULL,3,0,0,'2013-06-11 15:55:00','2013-06-11 16:25:00','2013-06-11 16:25:00',0);

INSERT INTO ProcessorTransaction (Id,PurchaseId,PreAuthId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,
Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,
CardHash,IsUploadedFromBoss,IsRFID,CreatedGMT)
VALUES (2,19,1,14,'2013-06-11 15:55:00',123,1,1,
2000,1,2222,1,'2013-06-11 15:55:00' ,1,222,222,1,
111,1,1,'2013-06-11 15:55:00');

INSERT INTO PaymentCard (Id,PurchaseId,CardTypeId,CreditCardTypeId,CustomerCardId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,
Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved)
VALUES (1,19,1,1,1,1,1,1,200,1233,now(),1,1,1);



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




2)
select * from StaggingPurchase ;
select * from StaggingPermit ;


3)
SELECT distinct
		P.CustomerId,		P.LocationId,		ifnull(R.LocationId,0),		ifnull(R.SpaceNumber,0),		P.PointOfSaleId,		P.UnifiedRateId,
		P.PaystationSettingId,		P.Id,		R.Id,		ifnull(R.PermitIssueTypeId,0),		ifnull(R.PermitTypeId,0),				P.TransactionTypeId,
		P.PaymentTypeId,		T.ProcessorTransactionTypeId, 		P.CoinCount,		P.BillCount,		P.CouponId,		P.CashPaidAmount,
		P.CardPaidAmount,		P.IsOffline,		P.IsRefundSlip,		P.PurchaseGMT,		R.PermitBeginGMT,		R.PermitExpireGMT,
		C.Amount,		C.CardTypeID,		C.CreditCardTypeID,		C.MerchantAccountID,		P.CoinPaidAmount,		P.BillPaidAmount,		C.Id,
		ifnull(R.OriginalPermitId,R.Id),		B.PropertyValue,		P.ChargedAmount,		T.Id			-- Added on June 11 for Incremental ETL
		from StaggingPurchase P
		left outer join    StaggingPermit R on  P.Id = R.PurchaseId
		left outer join   StaggingPaymentCard C on P.Id = C.PurchaseId
		left outer join   StaggingProcessorTransaction T on P.Id = T.PurchaseId
		left join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE  -- P.CustomerId in (160) AND /* This was used for micro-migration only */
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL LIMIT 25;


4) call sp_ETLStart_Inc();

select * from ETLExecutionLog ;
select * from ETLNotProcessed;

select * from PPPTotalHour;
select * from PPPTotalDay;
select * from PPPTotalMonth;
select * from PPPDetailHour;
select * from PPPDetailDay;
select * from PPPDetailMonth;

select * from PPPRevenueHour;
select * from PPPRevenueDay;
select * from PPPRevenueMonth;

select * from OccupancyHour;
select * from OccupancyHourTurnover;
select * from OccupancyDayTurnover;
select * from OccupancyMonthTurnover;
select * from OccupancyDay;
select * from OccupancyMonth;


0) sp_ETLStart_Inst1
1) sp_MigrateTransactions_for_Occupancy( P_ETLProcessDateRangeId ,  P_ClusterId ,  P_BeginPurchaseGMT ,  P_EndPurchaseGMT ,  P_ETL_daterangeId )
1.1) sp_MigrateOccupancyIntoKPI (lv_Permit_Id ,
								P_ETLProcessDateRangeId , 
								P_ClusterId , 
								lv_PermitBeginGMT, 
								lv_PermitExpireGMT ,
								lv_Purchase_CustomerId, 
								lv_Permit_LocationId, 
								lv_Purchase_UnifiedRateId,
								lv_Customer_Timezone,
								lv_Permit_PermitTypeId)
1.2) sp_FillOccupancyTimeslots(lv_GMT_date, P_CustomerId, P_LocationId, P_UnifiedRateId, 0, lv_no_of_spaces_location, P_Customer_Timezone)
1.3) sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone )								


