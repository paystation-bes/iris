
/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inc
  
  Purpose:    For testing online Transactional data
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inc ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inc ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 1;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	CALL sp_MigrateRevenueIntoKPI_Inc(lv_ETLExecutionLogId);
	
	UPDATE ETLExecutionLog
	SET ETLCompletedPurchase = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	CALL sp_MigrateCollectionIntoKPI_Inc(lv_ETLExecutionLogId);
   
   	UPDATE ETLExecutionLog
	SET ETLCompletedCollection = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

		