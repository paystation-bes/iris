
-- 1) TRIGGER on Purchase
-- Inserts data into PurchaseCollection and StaggingPurchase

DROP TRIGGER IF EXISTS Trg_InsertPurchase ;

DELIMITER $$
CREATE TRIGGER Trg_InsertPurchase
AFTER INSERT ON Purchase
FOR EACH ROW
BEGIN

INSERT INTO PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) 
 values(New.Id,New.PointOfSaleId,New.PurchaseGMT,New.CashPaidAmount,New.CoinPaidAmount,New.CoinCount,New.BillPaidAmount,New.BillCount);
 
 -- Below code written by Ashok on June 11 2013
 -- Insert into StaggingPurchase for Incremental ETL
 
INSERT INTO StaggingPurchase(Id, CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,
							PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,
							CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,
							IsOffline,IsRefundSlip,CreatedGMT)
VALUES (New.Id, New.CustomerId, New.PointOfSaleId, New.PurchaseGMT, New.PurchaseNumber,New.TransactionTypeId,New.PaymentTypeId,New.LocationId,
							New.PaystationSettingId,New.UnifiedRateId,New.CouponId,New.OriginalAmount,New.ChargedAmount,New.ChangeDispensedAmount,New.ExcessPaymentAmount,
							New.CashPaidAmount,New.CoinPaidAmount,New.BillPaidAmount,New.CardPaidAmount,New.RateAmount,New.RateRevenueAmount,New.CoinCount,New.BillCount,
							New.IsOffline,New.IsRefundSlip,New.CreatedGMT) ;
 
END$$
DELIMITER ;


-- 2) TRIGGER on Permit
-- Inserts data into StaggingPermit for Incremental ETL

DROP TRIGGER IF EXISTS  Trg_InsertPermit ;

DELIMITER $$
CREATE TRIGGER Trg_InsertPermit
AFTER INSERT ON Permit
FOR EACH ROW
BEGIN

INSERT INTO StaggingPermit (Id,PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,MobileNumberId,
							OriginalPermitId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,
							NumberOfExtensions)
VALUES (New.Id,New.PurchaseId,New.PermitNumber,New.LocationId,New.PermitTypeId,New.PermitIssueTypeId,New.LicencePlateId,New.MobileNumberId,
							New.OriginalPermitId,New.SpaceNumber,New.AddTimeNumber,New.PermitBeginGMT,New.PermitOriginalExpireGMT,New.PermitExpireGMT,
							New.NumberOfExtensions) ;

 
END$$
DELIMITER ;

-- 3) TRIGGER on PaymentCard
-- Inserts data into StaggingPaymentCard for Incremental ETL

DROP TRIGGER IF EXISTS Trg_InsertPaymentCard ;

DELIMITER $$
CREATE TRIGGER Trg_InsertPaymentCard
AFTER INSERT ON PaymentCard
FOR EACH ROW
BEGIN

INSERT INTO StaggingPaymentCard (Id, PurchaseId,CardTypeId,CreditCardTypeId,CustomerCardId,ProcessorTransactionTypeId,
								ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,
								IsApproved)
VALUES (New.Id, New.PurchaseId,New.CardTypeId,New.CreditCardTypeId,New.CustomerCardId,New.ProcessorTransactionTypeId,
								New.ProcessorTransactionId,New.MerchantAccountId,New.Amount,New.CardLast4Digits,New.CardProcessedGMT,New.IsUploadedFromBoss,New.IsRFID,
								New.IsApproved)	;							

 
END$$
DELIMITER ;


-- 4) TRIGGER on ProcessorTransaction
-- Inserts data into StaggingProcessorTransaction for Incremental ETL

DROP TRIGGER IF EXISTS Trg_InsertProcessorTransaction ;

DELIMITER $$
CREATE TRIGGER Trg_InsertProcessorTransaction
AFTER INSERT ON ProcessorTransaction
FOR EACH ROW
BEGIN

INSERT INTO StaggingProcessorTransaction (Id,PurchaseId,PreAuthId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,
										 MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,
										ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,
										IsRFID,CreatedGMT)
VALUES (New.Id,New.PurchaseId,New.PreAuthId,New.PointOfSaleId,New.PurchasedDate,New.TicketNumber,New.ProcessorTransactionTypeId,
										 New.MerchantAccountId,New.Amount,New.CardType,New.Last4DigitsOfCardNumber,New.CardChecksum,New.ProcessingDate,
										New.ProcessorTransactionId,New.AuthorizationNumber,New.ReferenceNumber,New.IsApproved,New.CardHash,New.IsUploadedFromBoss,
										New.IsRFID,New.CreatedGMT) ;


END$$
DELIMITER ;

-- 5) TRIGGER on POSCollection
-- ----------------------------------------------------------------------------------------------------------
-- Trigger:     tr_POSCollection_insert
--
-- Purpose:     On INSERT of a POSCollection record UPDATE the POSBalance record for the corresponding 
--              point-of-sale and SET IsRecalcable = 2 (but only if IsRecalcable = 0, which prevents any 
--              unnecessary updates).
--
--              This trigger logic must account for the arrival of out-of-sequence collections reports coming 
--              up from the paystation.
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release   
-- ----------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS tr_POSCollection_insert;

delimiter //
CREATE TRIGGER tr_POSCollection_insert AFTER INSERT ON POSCollection
FOR EACH ROW BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release 
    
    -- Local variables for lastest collection dates from matching POSBalance record
    DECLARE v_LastCashCollectionGMT DATETIME;   -- from Audit Report
    DECLARE v_LastBillCollectionGMT DATETIME;   -- from Bill Report
    DECLARE v_LastCoinCollectionGMT DATETIME;   -- from Coin Report
    DECLARE v_LastCardCollectionGMT DATETIME;   -- from Card Report
    DECLARE v_LastCollectionTypeId  TINYINT UNSIGNED;
    DECLARE v_IsRecalcable          TINYINT UNSIGNED DEFAULT 0;
    DECLARE v_MaxLastCollectionGMT  DATETIME;
    
    -- Retrieve latest paystation collection report dates from matching POSBalance record
    SELECT  LastCashCollectionGMT, LastBillCollectionGMT, LastCoinCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId
    INTO    v_LastCashCollectionGMT, v_LastBillCollectionGMT, v_LastCoinCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId
    FROM    POSBalance B
    WHERE   B.PointOfSaleId = NEW.PointOfSaleId;
    
    -- Set v_MaxLastCollectionGMT to be the latest of all the different types of Collections Reports for the paystation
    SET v_MaxLastCollectionGMT = v_LastCashCollectionGMT;
    IF v_LastBillCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastBillCollectionGMT; END IF;
    IF v_LastCoinCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCoinCollectionGMT; END IF;
    IF v_LastCardCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCardCollectionGMT; END IF;
    
    -- Determine the collection type of the most recent (last) paystation collections report, used by sp_CollectionRecalcBalance
    IF NEW.EndGMT > v_MaxLastCollectionGMT THEN
        SET v_LastCollectionTypeId = NEW.CollectionTypeId;
    END IF;

    -- Audit report
    IF NEW.CollectionTypeId = 0 THEN
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndGMT > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndGMT > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndGMT > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        
    -- Bill report    
    ELSEIF NEW.CollectionTypeId = 1 THEN
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;   -- TAKE THIS OUT?
        IF NEW.EndGMT > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;

    -- Coin report    
    ELSEIF NEW.CollectionTypeId = 2 THEN
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;   -- TAKE THIS OUT? 
        IF NEW.EndGMT > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;

    -- Card report    
    ELSEIF NEW.CollectionTypeId = 3 THEN       
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
     END IF;
    
    -- The POSBalance is recalc-able
    IF v_IsRecalcable = 2 THEN
        UPDATE  POSBalance
        SET     IsRecalcable          = v_IsRecalcable,
                LastCollectionTypeId  = v_LastCollectionTypeId,
                LastCashCollectionGMT = v_LastCashCollectionGMT,
                LastCoinCollectionGMT = v_LastCoinCollectionGMT,
                LastBillCollectionGMT = v_LastBillCollectionGMT,
                LastCardCollectionGMT = v_LastCardCollectionGMT
        WHERE   PointOfSaleId         = NEW.PointOfSaleId;
    END IF;
	
	
	-- Below Insert is written by Ashok used for Widget ETL 
	INSERT INTO StaggingPOSCollection (Id, CustomerId,PointOfSaleId,CollectionTypeId,EndGMT,CoinTotalAmount,BillTotalAmount,CardTotalAmount)
	VALUES (New.Id, New.CustomerId,New.PointOfSaleId,New.CollectionTypeId,New.EndGMT,New.CoinTotalAmount,New.BillTotalAmount,New.CardTotalAmount) ;

    
END//
delimiter ;

