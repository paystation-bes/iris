
/*
 --------------------------------------------------------------------------------
  Procedure:  sp_MigrateCollectionIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/

DROP PROCEDURE IF EXISTS sp_MigrateCollectionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateCollectionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- Uploaded to Subversion
-- Last modified on June 11 2013
-- DECLARE SECTION
-- FOR ACTUAL MIGRATION THIS PROCEDURE TAKEN ONLY ENDGMT DATA AS INPUT PARAMETER

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;



-- CURSOR VARIABLES
DECLARE v_Collection_Id BIGINT;
DECLARE v_Collection_CustomerId MEDIUMINT;
DECLARE	v_Collection_PointOfSaleId MEDIUMINT;
DECLARE	v_Collection_CollectionTypeId TINYINT;
DECLARE	v_Collection_EndGMT DATETIME;
DECLARE	v_Collection_CoinTotalAmount MEDIUMINT;
DECLARE	v_Collection_BillTotalAmount MEDIUMINT;
DECLARE	v_Collection_CardTotalAmount MEDIUMINT;
DECLARE	v_CollectionTotal MEDIUMINT;
DECLARE v_Customer_Timezone VARCHAR(40);	

-- CURSOR C1 STATEMENT
-- RENAME C1 WITH c_Collection
-- There will be only INSERT operation on POSCollection

DECLARE C1 CURSOR FOR
	
		SELECT
		A.Id,
		A.CustomerId,
		A.PointOfSaleId,
		A.CollectionTypeId,
		A.EndGMT,
		A.CoinTotalAmount,
		A.BillTotalAmount,
		A.CardTotalAmount,
		A.CoinTotalAmount + A.BillTotalAmount +	A.CardTotalAmount,
		B.PropertyValue
		FROM 
		StaggingPOSCollection A, CustomerProperty B
		WHERE 
		A.CustomerId = B.CustomerId AND
		B.CustomerPropertyTypeId = 1 AND
		PropertyValue IS NOT NULL  LIMIT 25;
		
		-- AND	A.CreatedGMT between P_BeginDateGMT AND P_EndDateGMT
		-- A.EndGMT between P_BeginDateGMT AND P_EndDateGMT ; Added on April 19 2013
		-- A.EndGMT must be replace by A.CreateGMT the new attribute to be added
		-- WHERE 
		-- CustomerId = P_CustomerId AND
		-- EndGMT between P_BeginCollectionGMT AND P_EndCollectionGMT;		
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		-- BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50,
		-- AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount,
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		--	+ BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50 + 
		--	AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount ,
		
		
		
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO CollectionNotProcessed ( PosCollectionId , CustomerId , 				PointOfSaleId , 			CollectionTypeId , 				EndGMT , 				RecordInsertTime  ) VALUES
										 ( v_Collection_Id , v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId , v_Collection_EndGMT,	UTC_TIMESTAMP() );
	
	INSERT INTO ArchiveStaggingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
	DELETE FROM StaggingPOSCollection WHERE Id = v_Collection_Id;
	
  END;
 

 SET lv_Customer_Timezone = null;
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET CollectionRecordCount = idmaster_pos,
CollectionCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_Collection_Id,
					v_Collection_CustomerId,
					v_Collection_PointOfSaleId,
					v_Collection_CollectionTypeId,
					v_Collection_EndGMT,
					v_Collection_CoinTotalAmount,
					v_Collection_BillTotalAmount,
					v_Collection_CardTotalAmount,
					v_CollectionTotal,
					v_Customer_Timezone;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				
				SELECT Id INTO lv_TimeIdGMT FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= v_Collection_EndGMT);
						
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_Collection_EndGMT,'GMT',v_Customer_Timezone)) ;
				
				IF (SELECT COUNT(*) FROM TotalCollection 
				WHERE CustomerId = v_Collection_CustomerId AND
				PointOfSaleId = v_Collection_PointOfSaleId AND 
				EndTimeIdLocal= lv_TimeIdLocal AND
				CollectionTypeId = v_Collection_CollectionTypeId ) = 1 	THEN
					
					-- Need to check if it is duplicate record
					
					UPDATE TotalCollection
					SET
						CoinTotalAmount = CoinTotalAmount + v_Collection_CoinTotalAmount,
						BillTotalAmount = BillTotalAmount + v_Collection_BillTotalAmount,
						CardTotalAmount = CardTotalAmount + v_Collection_CardTotalAmount,
						TotalAmount = TotalAmount + v_CollectionTotal
					WHERE 
						CustomerId = v_Collection_CustomerId AND
						PointOfSaleId = v_Collection_PointOfSaleId AND
						EndTimeIdLocal= lv_TimeIdLocal AND
						CollectionTypeId = v_Collection_CollectionTypeId;
						
					-- Need a logging table to see the data ? Atleast till TEST evvironment
					-- Need to write code to make sure the same record should not be fetched again in the Cursor. 
					-- Otherwise amount will show wrong values as it will keepon updating
					
				ELSE
				
					INSERT INTO TotalCollection( POSCollectionId, 	CustomerId, 				PointOfSaleId, 				CollectionTypeId, 				EndTimeIdGMT, 	EndTimeIdLocal, CoinTotalAmount, 				BillTotalAmount, 				CardTotalAmount, 				TotalAmount) VALUES
											   ( v_Collection_Id,	v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId,	lv_TimeIdGMT,	lv_TimeIdLocal,	v_Collection_CoinTotalAmount,	v_Collection_BillTotalAmount,	v_Collection_CardTotalAmount,	v_CollectionTotal);
				
				
				END IF;
						
	
			INSERT INTO ArchiveStaggingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
			DELETE FROM StaggingPOSCollection WHERE Id = v_Collection_Id;
			
			-- DELETE FROM POSCollectionStagging WHERE ID = v_Collection_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;


