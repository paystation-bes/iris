SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `PPPTotalHour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalHour` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalHour_unq` (`TimeIdLocal` ,`CustomerId`) ,
  INDEX idx_PPPTotalHour_CustomerId ( CustomerId ),
  INDEX idx_PPPTotalHour_multi ( CustomerId, TimeIdGMT ),
  INDEX idx_PPPTotalHour_multi2 ( CustomerId, TimeIdLocal ) ,
  CONSTRAINT `fk_PPPTotalHour_time1`
    FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PPPTotalHour_time2`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPTotalDay`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalDay` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalDay_unq` (`TimeIdLocal`,`CustomerId`) ,
  INDEX idx_PPPTotalDay_CustomerId ( CustomerId ) ,
  CONSTRAINT `fk_PPPTotalDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPTotalMonth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalMonth_unq` (`TimeIdLocal`,`CustomerId`) ,
  INDEX idx_PPPTotalMonth_CustomerId ( CustomerId ) ,
  INDEX idx_PPPTotalMonth_TimeIdLocal ( TimeIdLocal ),
  CONSTRAINT `fk_PPPTotalMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPDetailHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailHour` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalHourId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailHour_unq` (`PPPTotalHourId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  CONSTRAINT `fk_PPPDetailHour_PPPTotalHourId`
    FOREIGN KEY (`PPPTotalHourId` )
    REFERENCES `PPPTotalHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPDetailDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailDay` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalDayId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailDay_unq` (`PPPTotalDayId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  CONSTRAINT `fk_PPPDetailDay_PPPTotalDayId`
    FOREIGN KEY (`PPPTotalDayId` )
    REFERENCES `PPPTotalDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPDetailMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalMonthId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailMonth_unq` (`PPPTotalMonthId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  INDEX idx_PPPDetailMonth_TotalMonthId_PurLocId (PPPTotalMonthId, PurchaseLocationId) ,
  INDEX idx_PPPDetailMonth_PurchaseLocationId (PurchaseLocationId) ,
  INDEX idx_PPPDetailMonth_PPPTotalMonthId (PPPTotalMonthId) ,
  CONSTRAINT `fk_PPPDetailMonth_PPPTotalMonthId`
    FOREIGN KEY (`PPPTotalMonthId` )
    REFERENCES `PPPTotalMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `PPPRevenueHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueHour` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailHourId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueHour_unq` (`PPPDetailHourId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueHour_PPPDetailHourId`
    FOREIGN KEY (`PPPDetailHourId` )
    REFERENCES `PPPDetailHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPRevenueDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueDay` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailDayId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueDay_unq` (`PPPDetailDayId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueDay_PPPDetailDayId`
    FOREIGN KEY (`PPPDetailDayId` )
    REFERENCES `PPPDetailDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPRevenueMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailMonthId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueMonth_unq` (`PPPDetailMonthId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueMonth_PPPDetailMonthId`
    FOREIGN KEY (`PPPDetailMonthId` )
    REFERENCES `PPPDetailMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `OccupancyHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyHour` ;

CREATE  TABLE IF NOT EXISTS `OccupancyHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL ,  
  `NoOfPurchases` MEDIUMINT UNSIGNED DEFAULT 0 ,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyHour_unq` (`TimeIdGMT` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
  INDEX ind_OccupancyHour_TimeIdGMT (TimeIdGMT) ,
  CONSTRAINT `fk_OccupancyHour_time1`
     FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OccupancyHour_time2`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `OccupancyHourTurnover`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyHourTurnover` ;

CREATE  TABLE IF NOT EXISTS `OccupancyHourTurnover` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `OccupancyHourId` BIGINT UNSIGNED NOT NULL ,
  `DurationMins` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyHourTurnover_unq` (OccupancyHourId,DurationMins), 
  CONSTRAINT `fk_OccupancyHourTurnover_OccupancyHourId`
    FOREIGN KEY (`OccupancyHourId` )
    REFERENCES `OccupancyHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `OccupancyDayTurnover`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyDayTurnover` ;

CREATE  TABLE IF NOT EXISTS `OccupancyDayTurnover` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `OccupancyDayId` BIGINT UNSIGNED NOT NULL ,
  `DurationMins` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyDayTurnover_unq` (OccupancyDayId,DurationMins), 
  CONSTRAINT `fk_OccupancyDayTurnover_OccupancyDayId`
    FOREIGN KEY (`OccupancyDayId` )
    REFERENCES `OccupancyDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `OccupancyMonthTurnover`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyMonthTurnover` ;

CREATE  TABLE IF NOT EXISTS `OccupancyMonthTurnover` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `OccupancyMonthId` BIGINT UNSIGNED NOT NULL ,
  `DurationMins` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyMonthTurnover_unq` (OccupancyMonthId,DurationMins), 
  CONSTRAINT `fk_OccupancyMonthTurnover_OccupancyMonthId`
    FOREIGN KEY (`OccupancyMonthId` )
    REFERENCES `OccupancyMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `OccupancyDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyDay` ;

CREATE  TABLE IF NOT EXISTS `OccupancyDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of the Day 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL  DEFAULT 1,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyDay_unq` (`TimeIdLocal` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
    CONSTRAINT `fk_OccupancyDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `OccupancyMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyMonth` ;

CREATE  TABLE IF NOT EXISTS `OccupancyMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of first day of the Month 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL DEFAULT 1,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyMonth_unq` (`TimeIdLocal` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
    CONSTRAINT `fk_OccupancyMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ETLQueueRecordCount`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLQueueRecordCount` ;

CREATE  TABLE IF NOT EXISTS `ETLQueueRecordCount` (
  `ETLProcessDateRangeId` INT UNSIGNED NOT NULL ,
  `TotalRecords` INT UNSIGNED NOT NULL , 
  `ETLRemaingRecords`  INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`ETLProcessDateRangeId`) )
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `ETLExecutionLog`
-- -----------------------------------------------------


DROP TABLE IF EXISTS `ETLExecutionLog`;
CREATE TABLE  `ETLExecutionLog` (
  `Id` 	BIGINT NOT NULL AUTO_INCREMENT,
  `ClusterId` TINYINT NOT NULL DEFAULT 1,
  `Parameter1` DATETIME NOT NULL,
  `Parameter2` DATETIME NOT NULL,  
  `ETLStartTime` DATETIME DEFAULT NULL,
  `Status` TINYINT DEFAULT 0,
  `PPPCursorOpenedTime` DATETIME DEFAULT NULL,
  `PurchaseRecordCount` INT DEFAULT NULL,
  `ETLCompletedPurchase` DATETIME DEFAULT NULL,  
  `CollectionCursorOpenedTime` DATETIME DEFAULT NULL,
  `CollectionRecordCount` INT DEFAULT NULL,
  `ETLCompletedCollection` DATETIME DEFAULT NULL,  
  `SettledAmountCursorOpenedTime` DATETIME DEFAULT NULL,
  `SettledAmountRecordCount` INT DEFAULT NULL,
  `ETLCompletedSettledAmount` DATETIME DEFAULT NULL,  
  `ETLEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


