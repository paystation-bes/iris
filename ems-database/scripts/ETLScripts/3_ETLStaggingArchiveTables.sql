-- -----------------------------------------------------
-- Table `StaggingPurchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StaggingPurchase` ;

CREATE  TABLE IF NOT EXISTS `StaggingPurchase` (
  `Id` INT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `PurchaseNumber` INT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `PaymentTypeId` TINYINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `CouponId` MEDIUMINT UNSIGNED NULL ,
  `OriginalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChargedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChangeDispensedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ExcessPaymentAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CashPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateRevenueAmount` MEDIUMINT NOT NULL DEFAULT 0 ,
  `CoinCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `IsOffline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsRefundSlip` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `CreatedGMT` DATETIME NOT NULL ,
  INDEX `idx_stg_purchase_PurchaseGMT` (`PurchaseGMT` ASC),
  INDEX `idx_stg_purchase_created` (`CreatedGMT` ASC) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StaggingPermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StaggingPermit` ;

CREATE  TABLE IF NOT EXISTS `StaggingPermit` (
  `Id` INT UNSIGNED NOT NULL  ,
  `PurchaseId` INT UNSIGNED NOT NULL ,
  `PermitNumber` INT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitTypeId` TINYINT UNSIGNED NOT NULL ,
  `PermitIssueTypeId` TINYINT UNSIGNED NOT NULL ,
  `LicencePlateId` INT UNSIGNED NULL ,
  `MobileNumberId` INT UNSIGNED NULL ,
  `OriginalPermitId` INT UNSIGNED NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `AddTimeNumber` INT UNSIGNED NOT NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitOriginalExpireGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `NumberOfExtensions` TINYINT UNSIGNED NOT NULL ,
  INDEX `idx_stg_permit_purchase` (`PurchaseId` ASC) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StaggingPaymentCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StaggingPaymentCard` ;

CREATE  TABLE IF NOT EXISTS `StaggingPaymentCard` (
  `Id` INT UNSIGNED NOT NULL  ,
  `PurchaseId` INT UNSIGNED NOT NULL ,
  `CardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CreditCardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerCardId` MEDIUMINT UNSIGNED NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NULL ,
  `ProcessorTransactionId` INT UNSIGNED NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardLast4Digits` SMALLINT UNSIGNED NOT NULL ,
  `CardProcessedGMT` DATETIME NOT NULL ,
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL ,
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL ,
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL ,
  INDEX `idx_stg_paymentcard_purchase` (`PurchaseId` ASC) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StaggingProcessorTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StaggingProcessorTransaction`;

CREATE TABLE `StaggingProcessorTransaction` (
  `Id` INT UNSIGNED NOT NULL ,
  `PurchaseId` INT UNSIGNED NULL ,
  `PreAuthId` INT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchasedDate` DATETIME NOT NULL ,  
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardType` VARCHAR(20) NOT NULL ,
  `Last4DigitsOfCardNumber` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `CardChecksum` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0',
  `ProcessingDate` DATETIME NOT NULL ,
  `ProcessorTransactionId` VARCHAR(30) NOT NULL DEFAULT '',
  `AuthorizationNumber` VARCHAR(30) DEFAULT NULL ,
  `ReferenceNumber` VARCHAR(30) NOT NULL DEFAULT '',
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CardHash` VARCHAR(30) NOT NULL DEFAULT '',
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CreatedGMT` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `idx_stg_processortransaction_createdGMT` (`CreatedGMT` ASC)   
) ENGINE=InnoDB;



-- -----------------------------------------------------
-- Table `StaggingPOSCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StaggingPOSCollection` ;

CREATE  TABLE IF NOT EXISTS `StaggingPOSCollection` (
  `Id` BIGINT UNSIGNED NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CollectionTypeId` TINYINT UNSIGNED NOT NULL ,
  `EndGMT` DATETIME NOT NULL ,  
  `CoinTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ) 
ENGINE = InnoDB;


-- Archive Tables

-- -----------------------------------------------------
-- Table `ArchiveStaggingPurchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStaggingPurchase` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStaggingPurchase` (
  `Id` INT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStaggingPermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStaggingPermit` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStaggingPermit` (
  `Id` INT UNSIGNED NOT NULL  ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStaggingPaymentCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStaggingPaymentCard` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStaggingPaymentCard` (
  `Id` INT UNSIGNED NOT NULL  ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStaggingProcessorTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStaggingProcessorTransaction`;

CREATE TABLE `ArchiveStaggingProcessorTransaction` (
  `Id` INT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL   
) ENGINE=InnoDB;



-- -----------------------------------------------------
-- Table `ArchiveStaggingPOSCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStaggingPOSCollection`;

CREATE TABLE `ArchiveStaggingPOSCollection` (
  `Id` INT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL   
) ENGINE=InnoDB;
