-- -----------------------------------------------------
-- create default customer
-- -----------------------------------------------------
CALL sp_Preview_CreateAccount(
	'Grace Hopper',
	'Director of Build',
	'DPT Jenkins',
	'oranj',
	(SELECT Id FROM Customer WHERE Name = 'Oranj Parent'));

	
-- Child Admin Accounts

-- Child Admin Accounts: QA1 Child 1
CALL sp_Preview_CreateAccount(
	'qa1child',
	'Director of Operations',
	'qa1 Child',
	'qa1child',
	(SELECT Id FROM Customer WHERE Name = 'qa1parent'));


-- Added on Aug 7
-- Seattle
call sp_Preview_CreateLocation('qa1child', 'Seattle'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1child', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child'),4,'0'), '0006'), 1, 'Canada/Pacific',47.595753, -122.287204, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child'),4,'0'), '0007'), 1, 'Canada/Pacific',47.602832, -122.284474, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Seattle Maintenance'));

-- Houston
call sp_Preview_CreateLocation('qa1child', 'Houston'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child'),4,'0'), '0008'), 1, 'US/Central',29.758587, -95.371272, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child'),4,'0'), '0009'), 1, 'US/Central',29.757041, -95.368708, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child'),4,'0'), '0010'), 1, 'US/Central',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child') and Name='Houston Maintenance'));


CALL sp_Preview_CreateAdminUser(
	'qa1child',
	'Trevor',
	'Yu',
	'qaAutomation1%40child',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

call sp_InsertPointOfSale((select Id from Customer where Name = 'qa1child'),8,1,	'827300000700',	UTC_TIMESTAMP(),1,0,0);	

update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000700'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000700'
)) ;

-- Child Admin Accounts: QA1 Child 2
CALL sp_Preview_CreateAccount(
	'qa1child2',
	'Director of Operations',
	'qa1 Child2',
	'qa1child2',
	(SELECT Id FROM Customer WHERE Name = 'qa1parent'));

-- Portland
call sp_Preview_CreateLocation('qa1child2', 'Portland'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child2'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child2'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1child2', 'Portland',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child2'),4,'0'), '0006'), 1, 'US/Pacific',45.525854, -122.678353, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child2', 'Portland',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child2'),4,'0'), '0007'), 1, 'US/Pacific',45.526718, -122.678782, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Portland Maintenance'));

-- Springfield
call sp_Preview_CreateLocation('qa1child2', 'Springfield'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child2'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1child2'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1child2', 'Springfield',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child2'),4,'0'), '0008'), 1, 'US/Pacific',29.758587, -95.371272, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child2', 'Springfield',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child2'),4,'0'), '0009'), 1, 'US/Pacific',29.757041, -95.368708, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Maintenance'));
CALL sp_Preview_CreatePayStation('qa1child2', 'Springfield',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1child2'),4,'0'), '0010'), 1, 'US/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1child2') and Name='Springfield Maintenance'));

-- Child Admin Accounts: QA2 Child 1
CALL sp_Preview_CreateAccount(
	'qa2child',
	'Director of Operations',
	'qa2 Child',
	'qa2child',
	(SELECT Id FROM Customer WHERE Name = 'qa2parent'));
	

-- Added on Aug 7
-- Seattle
call sp_Preview_CreateLocation('qa2child', 'Seattle'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2child', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child'),4,'0'), '0006'), 1, 'Canada/Pacific',47.595753, -122.287204, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child'),4,'0'), '0007'), 1, 'Canada/Pacific',47.602832, -122.284474, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Seattle Maintenance'));

-- Houston
call sp_Preview_CreateLocation('qa2child', 'Houston'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child'),4,'0'), '0008'), 1, 'US/Central',29.758587, -95.371272, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child'),4,'0'), '0009'), 1, 'US/Central',29.757041, -95.368708, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child'),4,'0'), '0010'), 1, 'US/Central',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child') and Name='Houston Maintenance'));


CALL sp_Preview_CreateAdminUser(
	'qa2child',
	'Trevor',
	'Yu',
	'qaAutomation2%40child',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	

call sp_InsertPointOfSale((select Id from Customer where Name = 'qa2child'),8,1,	'827300000100',	UTC_TIMESTAMP(),1,0,0);	

update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000100'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000100'
)) ;

-- Child Admin Accounts: QA2 Child 2
CALL sp_Preview_CreateAccount(
	'qa2child2',
	'Director of Operations',
	'qa2 Child2',
	'qa2child2',
	(SELECT Id FROM Customer WHERE Name = 'qa2parent'));

-- Cupertino
call sp_Preview_CreateLocation('qa2child2', 'Cupertino'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child2'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child2'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2child2', 'Cupertino',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child2'),4,'0'), '0006'), 1, 'US/Pacific',37.32992, -122.008989, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child2', 'Cupertino',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child2'),4,'0'), '0007'), 1, 'US/Pacific',37.3316266, -122.0295223, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Cupertino Maintenance'));

-- Redmond
call sp_Preview_CreateLocation('qa2child2', 'Redmond'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child2'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2child2'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2child2', 'Redmond',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child2'),4,'0'), '0008'), 1, 'US/Pacific',47.644416, -122.1298501, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child2', 'Redmond',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child2'),4,'0'), '0009'), 1, 'US/Pacific',47.6434992, -122.1303606, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Maintenance'));
CALL sp_Preview_CreatePayStation('qa2child2', 'Redmond',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2child2'),4,'0'), '0010'), 1, 'US/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2child2') and Name='Redmond Maintenance'));

-- Standalone (no Parent) Child Accounts

CALL sp_Preview_CreateAccount(
	'qa1standalone',
	'Director of Operations',
	'qa1 Standalone',
	'qa1standalone',
	NULL);	

	

-- Added on Aug 7
-- Seattle
call sp_Preview_CreateLocation('qa1standalone', 'Seattle'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1standalone'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1standalone'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1standalone', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1standalone'),4,'0'), '0006'), 1, 'Canada/Pacific',47.595753, -122.287204, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle Maintenance'));
CALL sp_Preview_CreatePayStation('qa1standalone', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1standalone'),4,'0'), '0007'), 1, 'Canada/Pacific',47.602832, -122.284474, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Seattle Maintenance'));

-- Houston
call sp_Preview_CreateLocation('qa1standalone', 'Houston'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1standalone'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa1standalone'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa1standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1standalone'),4,'0'), '0008'), 1, 'US/Central',29.758587, -95.371272, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa1standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1standalone'),4,'0'), '0009'), 1, 'US/Central',29.757041, -95.368708, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa1standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa1standalone'),4,'0'), '0010'), 1, 'US/Central',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa1standalone') and Name='Houston Maintenance'));	

	
CALL sp_Preview_CreateAdminUser(
	'qa1standalone',
	'Trevor',
	'Yu',
	'qaAutomation1%40standalone',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	
	
call sp_InsertPointOfSale((select Id from Customer where Name = 'qa1standalone'),8,1,	'827300000600',	UTC_TIMESTAMP(),1,0,0);	

update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000600'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000600'
)) ;

-- as requested by Arash
-- commented to simplify automation script
-- delete from CustomerAgreement where CustomerId = 
-- ( select Id from Customer where Name = 'qa1standalone');
	
CALL sp_Preview_CreateAccount(
	'qa2standalone',
	'Director of Operations',
	'qa2 Standalone',
	'qa2standalone',
	NULL);		

-- Added on Aug 7
-- Seattle
call sp_Preview_CreateLocation('qa2standalone', 'Seattle'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2standalone'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2standalone'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2standalone', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2standalone'),4,'0'), '0006'), 1, 'Canada/Pacific',47.595753, -122.287204, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle Maintenance'));
CALL sp_Preview_CreatePayStation('qa2standalone', 'Seattle',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2standalone'),4,'0'), '0007'), 1, 'Canada/Pacific',47.602832, -122.284474, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Seattle Maintenance'));

-- Houston
call sp_Preview_CreateLocation('qa2standalone', 'Houston'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2standalone'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'qa2standalone'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('qa2standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2standalone'),4,'0'), '0008'), 1, 'US/Central',29.758587, -95.371272, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa2standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2standalone'),4,'0'), '0009'), 1, 'US/Central',29.757041, -95.368708, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Maintenance'));
CALL sp_Preview_CreatePayStation('qa2standalone', 'Houston',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'qa2standalone'),4,'0'), '0010'), 1, 'US/Central',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'qa2standalone') and Name='Houston Maintenance'));


	
CALL sp_Preview_CreateAdminUser(
	'qa2standalone',
	'Trevor',
	'Yu',
	'qaAutomation2%40standalone',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	


call sp_InsertPointOfSale((select Id from Customer where Name = 'qa2standalone'),8,1,	'827300000200',	UTC_TIMESTAMP(),1,0,0);	

update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000200'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000200'
)) ;

-- as requested by Trevor
-- commented to simplify automation script
-- delete from CustomerAgreement where CustomerId = 
-- ( select Id from Customer where Name = 'qa2standalone');
	
-- -----------------------------------------------------
-- adjust the service agreement so that 1 week worth of 
-- data is injected as soon as the server start
-- -----------------------------------------------------
-- UPDATE CustomerAgreement
-- SET
-- `AgreedGMT` = DATE_ADD(AgreedGMT, INTERVAL -1 WEEK),
-- `VERSION` = VERSION + 1,
-- `LastModifiedGMT` = UTC_TIMESTAMP(),
-- `LastModifiedByUserId` = 1
-- WHERE Organization = 'oranj parking';

-- -----------------------------------------------------
-- create additional user accounts
-- -----------------------------------------------------
CALL sp_Preview_CreateAdminUser(
	'oranj',
	'Lonney',
	'McDonald',
	'lonney%40oranj',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
CALL sp_Preview_CreateSystemAdminUser(
	'Lonney',
	'McDonald',
	'lonney%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

CALL sp_Preview_CreateAdminUser(
	'oranj',
	'Liza',
	'Tikhova',
	'liza%40oranj',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
CALL sp_Preview_CreateSystemAdminUser(
	'Liza',
	'Tikhova',
	'liza%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

CALL sp_Preview_CreateAdminUser(
	'oranj',
	'Robert',
	'Choquette',
	'robert%40oranj',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
CALL sp_Preview_CreateSystemAdminUser(
	'Robert',
	'Choquette',
	'robert%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

CALL sp_Preview_CreateAdminUser(
	'oranj',
	'Andrew',
	'Eastgaard',
	'andrew%40oranj',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
CALL sp_Preview_CreateSystemAdminUser(
	'Andrew',
	'Eastgaard',
	'andrew%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');


-- Create New Admin User for Digital Collect
CALL sp_Preview_CreateAdminUser(
	'qa2child',
	'Trevor',
	'Collect Child',
	'qaautomation2%40digitalcollect',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	
CALL sp_Preview_CreateAdminUser(
	'qa2standalone',
	'Trevor',
	'Collect Standalone',
	'qaautomation2%40digitalcollectsa',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	
CALL sp_Preview_CreateSystemAdminUser(
	'Trevor',
	'Yu',
	'trevor%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

CALL sp_Preview_CreateAdminUser(
	'qa1child',
	'Arash',
	'Collect Child',
	'qaautomation1%40digitalcollect',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	
CALL sp_Preview_CreateAdminUser(
	'qa1standalone',
	'Arash',
	'Collect Standalone',
	'qaautomation1%40digitalcollectsa',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
CALL sp_Preview_CreateSystemAdminUser(
	'Arash',
	'Taheri',
	'arash%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
	
CALL sp_Preview_CreateAdminUser(
	'oranj',
	'Moss',
	'V',
	'moss%40oranj',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
	
CALL sp_Preview_CreateAdminUser(
	'Oranj Parent',
	'OranjParent',
	'QA',
	'oranjparentQA',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
	
CALL sp_Preview_CreateAdminUser(
	'oranj',
	'OranjChild',
	'QA',
	'oranjchildqa',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');	
	
CALL sp_Preview_CreateSystemAdminUser(
	'Moss',
	'V',
	'moss%40dpt',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');
	

-- -----------------------------------------------------
-- ThirdPartyServiceAccount
-- -----------------------------------------------------
INSERT INTO `ThirdPartyServiceAccount`
	(`CustomerId`,
	 `Type`,
	 `EndPointUrl`,
	 `UserName`,
	 `Password`,
	 `Field1`,
	 `Field2`,
	 `IsPaystationBased`)
VALUES
      ((SELECT Id FROM Customer WHERE name='Digital (DPT)'),
	'Upside Wireless',
	'',
	'271b6830-470b-443b-af7b-049c25da8e79',
	'sAsqTKa1Q5zBiINagaEr6Xzo',
	'1',
	'',
	0);

call sp_InsertPointOfSale((select Id from Customer where Name = 'oranj'),8,1,	'827300000001',	UTC_TIMESTAMP(),1,0,0);	

update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000001'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'827300000001'
)) ;




CALL sp_Preview_CreateAccount(
	'Todd P',
	'Director of Operations',
	'City of Todds',
	'City of Burnaby',
	NULL);
	
	
CALL sp_Preview_CreateAccount(
	'Todd Z',
	'Director of Operations',
	'City of Todds',
	'City of Richmond',
	NULL);
	
CALL sp_Preview_CreateAccount(
	'Todd H',
	'Director of Operations',
	'City of Todds',
	'City of Todds',
	NULL);	

-- Create Customer for Paul

CALL sp_Preview_CreateAccount(
	'paul@paulpark',
	'Director of Operations',
	'Paul Park',
	'PaulPark',
	NULL);

CALL sp_Preview_CreateAdminUser(
	'PaulPark',
	'Paul',
	'Breland',
	'paul%40paulpark',
	'58ae83e00383273590f96b385ddd700802c3f07d',
	1,
	'SaltSaltSaltSalt');

call sp_Preview_CreateLocation('PaulPark', 'Burnaby'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000,@v_NewLocationId);

CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'PaulPark'), 1, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby'), @v_NewRouteId);
CALL sp_Preview_CreateRoute((Select Id from Customer where Name = 'PaulPark'), 2, (Select Id from Location where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby'), @v_NewRouteId);

CALL sp_Preview_CreatePayStation('PaulPark', 'Burnaby',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0006'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));
CALL sp_Preview_CreatePayStation('PaulPark', 'Burnaby',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0007'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));
CALL sp_Preview_CreatePayStation('PaulPark', 'Burnaby',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0008'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));
CALL sp_Preview_CreatePayStation('PaulPark', 'Burnaby',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0009'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));
CALL sp_Preview_CreatePayStation('PaulPark', 'Burnaby',     CONCAT('5000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0010'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));
CALL sp_Preview_CreatePayStation('PaulPark', 'Unassigned',  CONCAT('9000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0001'), 1, 'Canada/Pacific',NULL, NULL, (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Collections'), (select Id from Route where CustomerId = (Select Id from Customer where Name = 'PaulPark') and Name='Burnaby Maintenance'));

CALL sp_Preview_CreateMoblieLicense('PaulPark', 1, 20);

INSERT INTO RestAccount(
	`CustomerId`,
	`PointOfSaleId`,
	`RestAccountTypeId`,
	`AccountName`,
	`SecretKey`,
	`IsDeleted`,
	`LastModifiedGMT`,
	`LastModifiedByUserId`,
	`Comments`,
	`VERSION`)
VALUES(
	(Select Id from Customer where Name = 'PaulPark'),
	(Select Id from PointOfSale where SerialNumber = CONCAT('9000',LPAD((Select Id from Customer where Name = 'PaulPark'),4,'0'), '0001')),
	2,
	'Coupon Push Test',
	'oavi5LsujsUSNMMOcwUc80OMkAEC7ZEE+WljU0Z1joY=',
	0,
	curdate(),
	1,
	'',
	0);

-- Create Signing Server Account

CALL sp_Preview_CreateAdminUser(
	'Digital (DPT)',
	'Signing',
	'Admin',
	'SigningAdmin',
	'a1a07cdef911e1e4fab3b57678f705bf93d89f9ef56ddf9d3a446faaa20eb1164154995be4b7b486bd9cc1372be2c33e62ae16457d8920783bf543c815557ead',
	1,
	'iBgy4jh1coD0bBPY');
	
UPDATE UserAccount set IsPasswordTemporary=0 where UserName = 'SigningAdmin' and CustomerId = (Select Id from Customer where Name ='Digital (DPT)') ;
UPDATE UserRole SET RoleId = 4 where UserAccountId = ( select Id from UserAccount where UserName = 'SigningAdmin');	
	
call sp_BuildServer_InsertPOSCollection((select Id from Customer where Name = 'qa2child'));
call sp_BuildServer_InsertPOSCollection((select Id from Customer where Name = 'qa2standalone'));
call sp_BuildServer_InsertPOSCollection((select Id from Customer where Name = 'qa1child'));
call sp_BuildServer_InsertPOSCollection((select Id from Customer where Name = 'qa1standalone'));


-- Test Data for IncrementalRate for oranj

insert into Rate (Id,CustomerId,									  Name,					RateTypeId,IsLocked,IsOverride,IsDeleted,ArchiveDate,VERSION,LastModifiedGMT,LastModifiedByUserId) values 
				 (1, (select Id from Customer where Name = 'oranj'), 'Test IncrementalRate', 3, 		0, 		0, 			0, 		null, 			0,	UTC_TIMESTAMP(), 1);

insert into RateAcceptedPayment (RateId,IsCoin,IsBill,IsCreditCard,IsContactless,IsSmartCard,IsPasscard,IsCoupon,IsPromptForCoupon,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								(1,		1,		1,		1,			1,			 1,				1,		1,		 1,					0,	   UTC_TIMESTAMP(),	1);
								
insert into RateAddTime (RateId, IsAddTimeNumber, IsSpaceOrPlateNumber, IsEbP, ServiceFeeAmount, MinExtention, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						(1,		 1,					1,					1,		25,				 5,				0,		UTC_TIMESTAMP(),	1) ;
						
insert into RateIncremental (RateId, Length, Amount, MinAmount, MaxAmount, IsFlatRateEnabled,FlatRateStartTime, FlatRateEndTime, FlatRateAmount, IsUseLastIncrementValue, CCInc01, CCInc02, CCInc03, CCInc04, CCInc05, CCInc06, CCInc07, CCInc08, CCInc09, CCInc10,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
							( 1,	 1,		 100,	 25,		1000,		1,				 '18:00',			'22:00',		 500,			 1,							30,		10,		0,		 0,		  0,		0,		0,			0,		0,		0,		0,		UTC_TIMESTAMP(), 1) ;	


-- Test Data for IncrementalRate for qa1child

insert into Rate (Id,CustomerId,									  Name,					RateTypeId,IsLocked,IsOverride,IsDeleted,ArchiveDate,VERSION,LastModifiedGMT,LastModifiedByUserId) values 
				 (2, (select Id from Customer where Name = 'qa1child'), 'Test IncrementalRate', 3, 		0, 		0, 			0, 		null, 			0,	UTC_TIMESTAMP(), 1);

insert into RateAcceptedPayment (RateId,IsCoin,IsBill,IsCreditCard,IsContactless,IsSmartCard,IsPasscard,IsCoupon,IsPromptForCoupon,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								(2,		1,		1,		1,			1,			 1,				1,		1,		 1,					0,	   UTC_TIMESTAMP(),	1);
								
insert into RateAddTime (RateId, IsAddTimeNumber, IsSpaceOrPlateNumber, IsEbP, ServiceFeeAmount, MinExtention, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						(2,		 1,					1,					1,		25,				 5,				0,		UTC_TIMESTAMP(),	1) ;
						
insert into RateIncremental (RateId, Length, Amount, MinAmount, MaxAmount, IsFlatRateEnabled,FlatRateStartTime, FlatRateEndTime, FlatRateAmount, IsUseLastIncrementValue, CCInc01, CCInc02, CCInc03, CCInc04, CCInc05, CCInc06, CCInc07, CCInc08, CCInc09, CCInc10,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
							( 2,	 1,		 100,	 25,		1000,		1,				 '18:00',			'22:00',		 500,			 1,							30,		10,		0,		 0,		  0,		0,		0,			0,		0,		0,		0,		UTC_TIMESTAMP(), 1) ;	

insert into RateProfile (CustomerId,   											Name,  				PermitIssueTypeId,  IsPublished,   IsDeleted,  ArchiveDate,  VERSION,  LastModifiedGMT,  LastModifiedByUserId ) VALUES
						 ((select Id from Customer where Name = 'qa1child'), 	'Willington',		2,					0,				0,			NULL,		 1,		    UTC_TIMESTAMP(),	1) ;
							
insert into RateRateProfile (CustomerId,										RateId,																															RateProfileId,																												RateFlatTypeId,	Description,		IsRateBlendingEnabled,	IsOverride,	DisplayPriority,	AvailabilityStartDate,	AvailabilityEndDate,	AvailabilityStartTime,	AvailabilityEndTime,	DurationNumberOfDays,	DurationNumberOfHours,	DurationNumberOfMins,	IsMaximumExpiryEnabled,	RateExpiryDayTypeId,	ExpiryTime,	IsAdvancePurchaseEnabled,	IsScheduled,	IsSunday,	IsMonday,	IsTuesday,	IsWednesday,	IsThursday,	IsFriday,	IsSaturday,	IsDeleted,	VERSION,	LastModifiedGMT,	LastModifiedByUserId )  VALUES
							((select Id from Customer where Name = 'qa1child'),	(select Id from Rate where CustomerId = (select Id from Customer where Name = 'qa1child') and Name = 'Test IncrementalRate'),	(select Id from RateProfile where CustomerId = (select Id from Customer where Name = 'qa1child') and Name = 'Willington'),	NULL,			'WillingtonDesc',	1,						1,			50,					'2014-09-16 00:00:00',	'2015-09-16 00:00:00',	'00:00',				'23:59',				NULL,					NULL,				    NULL,					0,						NULL,					NULL,		1,							1,				1, 			1,			1,			1,				1,			1,			1,			0,			1,			UTC_TIMESTAMP(),	1);


						 
-- Test Data for IncrementalRate for qa1standalone

insert into Rate (Id,CustomerId,									  Name,					RateTypeId,IsLocked,IsOverride,IsDeleted,ArchiveDate,VERSION,LastModifiedGMT,LastModifiedByUserId) values 
				 (3, (select Id from Customer where Name = 'qa1standalone'), 'Test IncrementalRate', 3, 		0, 		0, 			0, 		null, 			0,	UTC_TIMESTAMP(), 1);

insert into RateAcceptedPayment (RateId,IsCoin,IsBill,IsCreditCard,IsContactless,IsSmartCard,IsPasscard,IsCoupon,IsPromptForCoupon,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								(3,		1,		1,		1,			1,			 1,				1,		1,		 1,					0,	   UTC_TIMESTAMP(),	1);
								
insert into RateAddTime (RateId, IsAddTimeNumber, IsSpaceOrPlateNumber, IsEbP, ServiceFeeAmount, MinExtention, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						(3,		 1,					1,					1,		0,				 3,				0,		UTC_TIMESTAMP(),	1) ;
						
insert into RateIncremental (RateId, Length, Amount, MinAmount, MaxAmount, IsFlatRateEnabled,FlatRateStartTime, FlatRateEndTime, FlatRateAmount, IsUseLastIncrementValue, CCInc01, CCInc02, CCInc03, CCInc04, CCInc05, CCInc06, CCInc07, CCInc08, CCInc09, CCInc10,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
							( 3,	 1,		 150,	 50,		1000,		0,				 NULL,				NULL,		 	500,			 1,							30,		10,		0,		 0,		  0,		0,		0,			0,		0,		0,		0,		UTC_TIMESTAMP(), 1) ;	


-- Insert 3 transactions to qa1child 

-- Cash Only
CALL sp_BuildServer_InsertPurchase('qa1child',2,1,'Downtown',1350,0);

-- Smart Card Only
CALL sp_BuildServer_InsertPurchase('qa1child',2,4,'Downtown',0,1350);

-- Cash and Card 
CALL sp_BuildServer_InsertPurchase('qa1child',2,6,'Downtown',1000,350);

-- General SysAdmin for automation
CALL sp_Preview_CreateSystemAdminUser(
	'Automation',
	'SysAdmin',
	'admin%40qa1admin',
	'84bd499ac692cf05f8810bd543f0aabcbaeadb04e4baf00c2b7dd48fef5e70d2a21387866cdfe9cd409603d82db53d9daf18b3722bb6a8badcd041a9090f0eeb',
	1,
	'MgSp2OtKJwt9iCOW');

UPDATE `UserAccount` SET `IsPasswordTemporary` = 0 WHERE `UserName` = 'admin%40qa1admin';

-- Added on Feb 10 2015
-- Adding the Houston Location Names to map to Flex Facilities/Properties
-- USEFUL SQL FOr Houston
-- select  concat('call sp_Preview_CreateLocation(','''','Oranj',''''),',',concat('''',Name,''''),',', concat('''','',''''),',',IsParent,',',if(ParentLocationId=37021,'''Central Business District''',if(ParentLocationId=37117,'''Route 3 Parent''','NULL')),',',1,',',NumberOfSpaces,',',
-- TargetMonthlyRevenueAmount,',','@v_NewLocationId',');' from Location where CustomerID = '' and IsParent=0 ;

/*
call sp_Preview_CreateLocation('Oranj'	,	'Central Business District'	,	''	,	1	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 3 Parent'	,	''	,	1	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'City Hall Annex'	,	''	,	0	,	NULL	,	1	,	58	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Travis'	,	''	,	0	,	NULL	,	1	,	148	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Milam'	,	''	,	0	,	NULL	,	1	,	131	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Travis W (200 - 400)'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Travis E (300 - 400)'	,	''	,	0	,	NULL	,	1	,	4	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Test Merchant'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Louisiana'	,	''	,	0	,	NULL	,	1	,	91	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Preston'	,	''	,	0	,	NULL	,	1	,	160	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Congress'	,	''	,	0	,	NULL	,	1	,	132	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Walker'	,	''	,	0	,	NULL	,	1	,	122	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Smith'	,	''	,	0	,	NULL	,	1	,	152	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Bagby'	,	''	,	0	,	NULL	,	1	,	96	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Dallas'	,	''	,	0	,	NULL	,	1	,	130	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Lamar'	,	''	,	0	,	NULL	,	1	,	85	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Brazos'	,	''	,	0	,	NULL	,	1	,	39	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'McKinney'	,	''	,	0	,	NULL	,	1	,	138	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Fannin'	,	''	,	0	,	NULL	,	1	,	197	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'San Jacinto'	,	''	,	0	,	NULL	,	1	,	211	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Capitol'	,	''	,	0	,	NULL	,	1	,	123	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Caroline'	,	''	,	0	,	NULL	,	1	,	375	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Rusk'	,	''	,	0	,	NULL	,	1	,	142	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Texas'	,	''	,	0	,	NULL	,	1	,	236	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Washington'	,	''	,	0	,	NULL	,	1	,	57	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Prairie'	,	''	,	0	,	NULL	,	1	,	175	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Crawford'	,	''	,	0	,	NULL	,	1	,	219	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Franklin'	,	''	,	0	,	NULL	,	1	,	159	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Polk'	,	''	,	0	,	NULL	,	1	,	122	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Bell'	,	''	,	0	,	NULL	,	1	,	138	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Clay'	,	''	,	0	,	NULL	,	1	,	166	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Jefferson'	,	''	,	0	,	NULL	,	1	,	61	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Leeland'	,	''	,	0	,	NULL	,	1	,	56	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Pierce'	,	''	,	0	,	NULL	,	1	,	99	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Holman'	,	''	,	0	,	NULL	,	1	,	87	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Pease'	,	''	,	0	,	NULL	,	1	,	43	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'St Joseph'	,	''	,	0	,	NULL	,	1	,	49	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Commerce'	,	''	,	0	,	NULL	,	1	,	240	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Austin'	,	''	,	0	,	NULL	,	1	,	378	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Municipal Courts'	,	''	,	0	,	NULL	,	1	,	257	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'La Branch'	,	''	,	0	,	NULL	,	1	,	327	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Jackson'	,	''	,	0	,	NULL	,	1	,	108	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Avenida De Las Americas'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Chenevert'	,	''	,	0	,	NULL	,	1	,	100	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Hamilton'	,	''	,	0	,	NULL	,	1	,	54	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Ruiz'	,	''	,	0	,	NULL	,	1	,	48	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Gray'	,	''	,	0	,	NULL	,	1	,	192	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Baldwin'	,	''	,	0	,	NULL	,	1	,	46	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Hadley'	,	''	,	0	,	NULL	,	1	,	102	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Hermann'	,	''	,	0	,	NULL	,	1	,	75	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Ewing'	,	''	,	0	,	NULL	,	1	,	43	,	150000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Cleburne'	,	''	,	0	,	NULL	,	1	,	9	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'ZStorage'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Washington Permit Lot'	,	''	,	0	,	NULL	,	1	,	350	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'UHD Lot 1'	,	''	,	0	,	NULL	,	1	,	268	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'McGowen'	,	''	,	0	,	NULL	,	1	,	4	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Webster'	,	''	,	0	,	NULL	,	1	,	79	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Oakdale'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Dennis'	,	''	,	0	,	NULL	,	1	,	8	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Calumet'	,	''	,	0	,	NULL	,	1	,	39	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Elder'	,	''	,	0	,	NULL	,	1	,	10	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Houston'	,	''	,	0	,	NULL	,	1	,	15	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Vine'	,	''	,	0	,	NULL	,	1	,	20	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Wood'	,	''	,	0	,	NULL	,	1	,	37	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Allan'	,	''	,	0	,	NULL	,	1	,	12	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Pierce Elevated'	,	''	,	0	,	NULL	,	1	,	658	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Berry'	,	''	,	0	,	NULL	,	1	,	23	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Bolsover Lot'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Presller'	,	''	,	0	,	NULL	,	1	,	9	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Bremand'	,	''	,	0	,	NULL	,	1	,	7	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Mexican Consulate Lot'	,	''	,	0	,	NULL	,	1	,	86	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Chartres Lot 1'	,	''	,	0	,	NULL	,	1	,	75	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Parking Management'	,	''	,	0	,	NULL	,	1	,	29	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Commerce Lot'	,	''	,	0	,	NULL	,	1	,	117	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Chartres Lot 2'	,	''	,	0	,	NULL	,	1	,	80	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Chartres Lot 3'	,	''	,	0	,	NULL	,	1	,	89	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Hutchins'	,	''	,	0	,	NULL	,	1	,	32	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'St. Emanuel'	,	''	,	0	,	NULL	,	1	,	65	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Braeswood Blvd'	,	''	,	0	,	NULL	,	1	,	123	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Kelvin'	,	''	,	0	,	NULL	,	1	,	15	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Dunstan'	,	''	,	0	,	NULL	,	1	,	19	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Washington PBD'	,	''	,	0	,	NULL	,	1	,	505	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Center'	,	''	,	0	,	NULL	,	1	,	19	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Riesner'	,	''	,	0	,	NULL	,	1	,	12	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Bastrop'	,	''	,	0	,	NULL	,	1	,	14	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Morning Side'	,	''	,	0	,	NULL	,	1	,	15	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Valentine'	,	''	,	0	,	NULL	,	1	,	18	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'West Gray'	,	''	,	0	,	NULL	,	1	,	28	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Williams'	,	''	,	0	,	NULL	,	1	,	9	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Baker'	,	''	,	0	,	NULL	,	1	,	8	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'UHD Lot 3'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'PEDEN'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'DRISCOLL'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'McDuffie'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 1'	,	''	,	0	,	'Central Business District'	,	1	,	375	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 2'	,	''	,	0	,	'Central Business District'	,	1	,	874	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 3'	,	''	,	0	,	'Central Business District'	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 4'	,	''	,	0	,	'Central Business District'	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 5'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 6'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 7'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 8'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 9'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 10'	,	''	,	0	,	'Central Business District'	,	1	,	976	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 11'	,	''	,	0	,	NULL	,	1	,	251	,	10000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 12 MC Lot'	,	''	,	0	,	NULL	,	1	,	190	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 13'	,	''	,	0	,	NULL	,	1	,	640	,	100000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 14'	,	''	,	0	,	NULL	,	1	,	58	,	100000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 15'	,	''	,	0	,	NULL	,	1	,	350	,	10000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 16 COML6'	,	''	,	0	,	NULL	,	1	,	117	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 17'	,	''	,	0	,	NULL	,	1	,	86	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 18'	,	''	,	0	,	NULL	,	1	,	48	,	100000	,	@v_NewLocationId	);;
call sp_Preview_CreateLocation('Oranj'	,	'Route 20'	,	''	,	0	,	NULL	,	1	,	268	,	1000000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 22'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 23'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 24'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 25'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 30 WPBD'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Unknown'	,	''	,	0	,	NULL	,	1	,	0	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1001'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	50000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1003'	,	''	,	0	,	'Route 3 Parent'	,	1	,	4	,	50000	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1101'	,	''	,	0	,	'Route 3 Parent'	,	1	,	6	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1201'	,	''	,	0	,	'Route 3 Parent'	,	1	,	4	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1301'	,	''	,	0	,	'Route 3 Parent'	,	1	,	10	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1401'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1403'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1501'	,	''	,	0	,	'Route 3 Parent'	,	1	,	6	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1601'	,	''	,	0	,	'Route 3 Parent'	,	1	,	8	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1701'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1703'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'AUS1801'	,	''	,	0	,	'Route 3 Parent'	,	1	,	4	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'BEL1400'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'BEL1401'	,	''	,	0	,	'Route 3 Parent'	,	1	,	10	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'BEL1402'	,	''	,	0	,	'Route 3 Parent'	,	1	,	3	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'BEL1501'	,	''	,	0	,	'Route 3 Parent'	,	1	,	5	,	0	,	@v_NewLocationId	);
call sp_Preview_CreateLocation('Oranj'	,	'Route 1 Radius'	,	''	,	0	,	NULL	,	1	,	6	,	10000	,	@v_NewLocationId	);
*/


/* Disabled the test data for FlexLocationProperty and FlexLocationFacility 
-- INSERT data into FlexLocationProperty

INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2002,		'Mikes Property',		'Houston',	1,				NULL,		UTC_TIMESTAMP(),	1 );

INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2003,		'Stemmanu',				'Houston',	1,				NULL,		UTC_TIMESTAMP(),	1 );

INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2006,		'Route 12 MC Lot',		'Houston',	1,				(select Id from Location where CustomerId = ( select Id from Customer where Name= 'Oranj') and Name = 'Airport'),		UTC_TIMESTAMP(),	1 );

INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2007,		'Route 16 COML6',		'Houston',	1,				NULL,		UTC_TIMESTAMP(),	1 );


INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2004,		'AUS1001',				'Houston',	1,				(select Id from Location where CustomerId = ( select Id from Customer where Name= 'Oranj') and Name = 'Airport'),		UTC_TIMESTAMP(),	1 );


INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2005,		'Ewing',				'Houston',	1,				NULL,		UTC_TIMESTAMP(),	1 );


INSERT INTO FlexLocationProperty
(CustomerId,										PRO_UID,	PRO_NAME,				PRO_CITY,	PRO_IS_ACTIVE,	LocationId,	LastModifiedGMT,	LastModifiedByUserId) VALUES
( (Select Id from Customer where Name = 'Oranj'),	2008,		'Route 20',				'Houston',	1,				NULL,		UTC_TIMESTAMP(),	1 );


-- INSERT data into FlexLocationFacility

INSERT INTO FlexLocationFacility
(CustomerId, 										FAC_UID,	FAC_CODE,		FAC_DESCRIPTION,			FAC_IS_ACTIVE,	LocationId,	LastModifiedGMT,LastModifiedByUserId) VALUES
((Select Id from Customer where Name = 'Oranj'),	2001,		'NRE DENM',		'NR DENMARK HIRSCH E',		1,			  	NULL,		UTC_TIMESTAMP(),	1 );		


INSERT INTO FlexLocationFacility
(CustomerId, 										FAC_UID,	FAC_CODE,		FAC_DESCRIPTION,			FAC_IS_ACTIVE,	LocationId,	LastModifiedGMT,LastModifiedByUserId) VALUES
((Select Id from Customer where Name = 'Oranj'),	2002,		'NRE LAUR',		'NR LAURA KOPPE RD. E',		1,			  	NULL,		UTC_TIMESTAMP(),	1 );		



INSERT INTO FlexLocationFacility
(CustomerId, 										FAC_UID,	FAC_CODE,		FAC_DESCRIPTION,			FAC_IS_ACTIVE,	LocationId,	LastModifiedGMT,LastModifiedByUserId) VALUES
((Select Id from Customer where Name = 'Oranj'),	2003,		'NRE LEE',		'NR LEE DR E',		1,			  	NULL,		UTC_TIMESTAMP(),	1 );		


INSERT INTO FlexLocationFacility
(CustomerId, 										FAC_UID,	FAC_CODE,		FAC_DESCRIPTION,			FAC_IS_ACTIVE,	LocationId,	LastModifiedGMT,LastModifiedByUserId) VALUES
((Select Id from Customer where Name = 'Oranj'),	2004,		'NRE NM',		'NR N MAIN E',		1,			  	NULL,		UTC_TIMESTAMP(),	1 );		

INSERT INTO FlexLocationFacility
(CustomerId, 										FAC_UID,	FAC_CODE,		FAC_DESCRIPTION,			FAC_IS_ACTIVE,	LocationId,	LastModifiedGMT,LastModifiedByUserId) VALUES
((Select Id from Customer where Name = 'Oranj'),	2836,		'700PIE',		'700 PIERCE LOT',			1,			  	NULL,		UTC_TIMESTAMP(),	1 );		

*/
-- INSERT DATA FOR FLEX CREDENTIALS (This hould be at the end to prevent background-processes from picking it up and start inserting data which will fail the build

INSERT INTO FlexWSUserAccount (CustomerId, 			URL, 																UserName, Password, 	VERSION,	LastModifiedGMT,	LastModifiedByUserId,	TimeZone) VALUES
((Select Id from Customer where Name = 'Oranj'), 	'https://staging-houston.t2flex.com/HOUSSA1WS/T2_Flex_Misc.asmx',	'dptws',  'pa$$w0rd', 	0,			UTC_TIMESTAMP(),	1,						'-4:00');


-- For Redbird01 For Ealvon

CALL sp_Preview_CreateAccount(
	'Redbird01',
	'Director of Elavon',
	'DPT Jenkins',
	'Redbird01',
	NULL);

call sp_InsertPointOfSale((select Id from Customer where Name = 'Redbird01'),5,1,	'500011114444',	UTC_TIMESTAMP(),1,0,0);	


update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500011114444'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500011114444'
)) ;

call sp_InsertPointOfSale((select Id from Customer where Name = 'Redbird01'),5,1,	'500011114445',	UTC_TIMESTAMP(),1,0,0);	


update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500011114445'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500011114445'
)) ;

-- For Ashok
call sp_InsertPointOfSale((select Id from Customer where Name = 'Redbird01'),5,1,	'500000180001',	UTC_TIMESTAMP(),1,0,0);	


update POSDate set CurrentSetting=1, ChangedGMT=UTC_TIMESTAMP()
where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500000180001'
))
and POSDateTypeId = 9;


-- for POSStatus

Update POSStatus set IsActivated =1, IsActivatedGMT=UTC_TIMESTAMP(),  VERSION= VERSION+1, LastModifiedGMT= UTC_TIMESTAMP()
 where PointOfSaleId  in
(
select Id from PointOfSale where SerialNumber in 
(
'500000180001'
)) ;

-- MerchantAccount 'Redbird Elavon01'

INSERT INTO `MerchantAccount`
(`Id`,`CustomerId`,`ProcessorId`,`MerchantStatusTypeId`,`Name`,`Field1`,`Field2`,`Field3`,`Field4`,`Field5`,`Field6`,`CloseQuarterOfDay`,`ReferenceCounter`,`IsValidated`,
`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES
(1,(select Id from Customer where Name = 'Redbird01'),19,1,'Redbird Elavon01','0017340009998881119888','0017340009998881119888',NULL,NULL,NULL,NULL,88,1,1,1,UTC_TIMESTAMP(),1);

-- MerchantAccount 'Redbird Elavon02'

INSERT INTO `MerchantAccount`
(`Id`,`CustomerId`,`ProcessorId`,`MerchantStatusTypeId`,`Name`,`Field1`,`Field2`,`Field3`,`Field4`,`Field5`,`Field6`,`CloseQuarterOfDay`,`ReferenceCounter`,`IsValidated`,
`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(
2,(select Id from Customer where Name = 'Redbird01'),19,1,'Redbird Elavon02','0017340009998881119887','0017340009998881119887',NULL,NULL,NULL,NULL,88,1,1,1,UTC_TIMESTAMP(),1);

-- MerchantAccount 'Ashok'
INSERT INTO `MerchantAccount`
(`Id`,`CustomerId`,`ProcessorId`,`MerchantStatusTypeId`,`Name`,`Field1`,`Field2`,`Field3`,`Field4`,`Field5`,`Field6`,`CloseQuarterOfDay`,`ReferenceCounter`,`IsValidated`,
`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(
6,(select Id from Customer where Name = 'Redbird01'),19,1,'FOR ASHOK','0017340009997772229516','09876543',5,NULL,NULL,NULL,92,1,1,1,UTC_TIMESTAMP(),1);


INSERT INTO MerchantPOS
(`Id`,`PointOfSaleId`,`MerchantAccountId`,`CardTypeId`,`IsDeleted`,`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES
(
1,
(select Id from PointOfSale where SerialNumber='500011114444'),
(select Id from MerchantAccount where Name = 'Redbird Elavon01'),
1,0,0,UTC_TIMESTAMP(),1);


INSERT INTO MerchantPOS
(`Id`,`PointOfSaleId`,`MerchantAccountId`,`CardTypeId`,`IsDeleted`,`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES
(
2,
(select Id from PointOfSale where SerialNumber='500011114445'),
(select Id from MerchantAccount where Name = 'Redbird Elavon02'),
1,0,0,UTC_TIMESTAMP(),1);

INSERT INTO MerchantPOS
(`Id`,`PointOfSaleId`,`MerchantAccountId`,`CardTypeId`,`IsDeleted`,`VERSION`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES
(
3,
(select Id from PointOfSale where SerialNumber='500000180001'),
(select Id from MerchantAccount where Name = 'FOR ASHOK'),
1,0,0,UTC_TIMESTAMP(),1);

-- Automatically sign agreement for automation customers
 INSERT INTO `CustomerAgreement`(`CustomerId`, `ServiceAgreementId`, `Name`, `Title`, `Organization`, `IsOverride`, `AgreedGMT`, `VERSION`, `LastModifiedGMT`, `LastModifiedByUserId`)
 SELECT `Id`, 1, 'automata', CONCAT('Director of ', `Name`), `Name`, 0, UTC_TIMESTAMP(), 0, UTC_TIMESTAMP(), 1 FROM `Customer` WHERE `Name` IN('qa1parent', 'qa2parent', 'qa1standalone', 'qa2standalone');
 
 -- INSERT Just enough information for EMV testing
 select @posId := Id from PointOfSale where SerialNumber='500000070001';
 INSERT INTO POSTelemetry(`PointOfSaleId`, `TelemetryTypeId`, `Value`, `IsHistory`, `TimestampGMT`, `CreatedGMT`, `ArchivedGMT`) VALUES
	(@posId, 1, '1234'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL),
	(@posId, 2, '3.3'	, 1, UTC_TIMESTAMP, UTC_TIMESTAMP, UTC_TIMESTAMP),
	(@posId, 2, '3.4'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL),
	(@posId, 3, 'Active'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL);

 select @posId := Id from PointOfSale where SerialNumber='500000070002';
 INSERT INTO POSTelemetry(`PointOfSaleId`, `TelemetryTypeId`, `Value`, `IsHistory`, `TimestampGMT`, `CreatedGMT`, `ArchivedGMT`) VALUES
	(@posId, 1, '5678'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL),
	(@posId, 2, '3.2'	, 1, UTC_TIMESTAMP, UTC_TIMESTAMP, UTC_TIMESTAMP),
	(@posId, 2, '3.3'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL),
	(@posId, 3, 'Active'	, 0, UTC_TIMESTAMP, UTC_TIMESTAMP, NULL);

-- -----------------------------------------------------
-- create parent customer for Parent-Child testings.
-- -----------------------------------------------------
CALL sp_InsertCustomer (             2,                    1,             NULL, 'SmokeTestParentRole',            NULL,        	 1, 'SmokeTest%40parent', '58ae83e00383273590f96b385ddd700802c3f07d',         1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 		             1,					1,                  1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt', null);
