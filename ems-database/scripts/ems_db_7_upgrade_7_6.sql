-- -----------------------------------------------------
-- Table `ETLAutoRecoverableLog`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLAutoRecoverableLog`;
CREATE TABLE  `ETLAutoRecoverableLog` (
  `Id` 	BIGINT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL DEFAULT 1,  
  `ETLStartTime` DATETIME DEFAULT NULL,
  `Status` TINYINT DEFAULT 0,
  `NoOfRecoverable` MEDIUMINT DEFAULT 0,
  `ETLEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_ETLAutoRecoverableLog_unq` (`ETLProcessTypeId`,`ETLStartTime`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


-- -----------------------------------------------------
-- Lookup Table EmsProperties
-- -----------------------------------------------------

UPDATE EmsProperties set Value = '180' where Name = 'CardAuthorizationBackUpMaxHoldingDays';

-- TD Merchant Services
-- TDMerchantGatewayVersion				v1
-- TDMerchantSocketTimeout				5000
-- TDMerchantConnectionTimeout			5000
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TDMerchantGatewayVersion', 'v1', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TDMerchantSocketTimeout', '5000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TDMerchantConnectionTimeout', '5000', UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Lookup Table Processor
-- -----------------------------------------------------

-- Changing First Horizon's TestUrl
UPDATE `Processor` SET `TestUrl` = 'https://gateway-bmd.merchantlink-lab.com:4443/FH-Inet/process_transaction.cgi' WHERE `Id` = 10;
-- Changing Elavon's TestUrl
UPDATE `Processor` SET `TestUrl` = 'https://certgate.viaconex.com/cgi-bin/encompass4.cgi' WHERE `Id` = 19;

-- Adding TD
INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 21, 'processor.tdmerchant', '', '', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

-- Adding Heartland
INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 22, 'processor.heartland.spplus', 'https://sslprod.secureexchange.net:54411', 'https://sslhps.test.secureexchange.net:15031', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Lookup Table ETLProcessType
-- -----------------------------------------------------

INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'AutoRecoverable', UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Lookup Table Widget
-- -----------------------------------------------------

UPDATE `Widget` SET `Name` = 'Occupancy Map by Location' WHERE `Name` = 'Occupancy Map' AND `UserAccountId` = 1 AND `WidgetListTypeId` = 1 AND `WidgetMetricTypeId` = 21 AND `WidgetTier1TypeId` = 105 AND `WidgetTier2TypeId` = 0 AND `WidgetTier3TypeId` = 0;

INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,21                ,1                ,104              ,0                ,0                ,0                 ,10               ,0                ,0       ,1          ,'Occupancy Map by Location(P)'  ,'Occupancy map by Parent Location'                                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());


-- -----------------------------------------------------
-- Stored Procedure sp_PreAuthHoldingToRetry
-- -----------------------------------------------------

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_PreAuthHoldingToRetry $$

CREATE PROCEDURE `sp_PreAuthHoldingToRetry`(OUT out_NoOfRecoverableRecords MEDIUMINT)
BEGIN
    
	-- EMS-5880 June 12
    
    -- Creation_Date: default GMT value assigned to CardRetryTransaction.CreationDate & PreAuthHolding.MovedGMT
    DECLARE Creation_Date DATETIME;
	DECLARE lv_PreAuthId BIGINT UNSIGNED ;
	
    declare NO_DATA int(4) default 0;
	declare idmaster_pos,indexm_pos,terminal_pos int(4);
	
    declare c1 cursor  for
	SELECT                  
	A.PreAuthId
	FROM    PreAuthHolding A, PointOfSale P, Customer C, ProcessorTransaction T, MerchantAccount MA
	WHERE   
	A.PointOfSaleId = P.Id 
	AND 	P.CustomerId = C.Id 	
	AND 	A.PointOfSaleId = T.PointOfSaleId 
	AND 	A.MerchantAccountId = MA.Id
	AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
    AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = A.PreAuthId
    AND     T.ProcessorTransactionTypeId = 20 
    AND     T.Amount = A.Amount
    AND     T.AuthorizationNumber LIKE '%:%'
    AND     A.Approved = 1
    AND     A.Expired = 1
    AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard')
    AND	    T.PurchasedDate < DATE_SUB(date(UTC_TIMESTAMP), INTERVAL 7 DAY)
	AND 	MA.ProcessorId <>19	
	AND 	A.MovedToRetryGMT IS NULL LIMIT 60000;
	
	
	declare continue handler for not found set NO_DATA=-1;

    -- Initialize internal variables
    SET Creation_Date = UTC_TIMESTAMP();
    
    
	set indexm_pos =0;
	set idmaster_pos =0;

	open c1;
	set idmaster_pos = (select FOUND_ROWS());

	while indexm_pos < idmaster_pos do
	fetch c1 into lv_PreAuthId ;
    -- start transaction
    -- START TRANSACTION;

    -- Insert records into CardRetryTransaction
    INSERT  CardRetryTransaction (PointOfSaleId,  PurchasedDate,  TicketNumber,  LastRetryDate,  NumRetries,  
								CardHash,  CardRetryTransactionTypeId,  CardData,  CardExpiry,  Amount,  
								CardType,  Last4DigitsOfCardNumber,  CreationDate,  BadCardHash,  IgnoreBadCard,  
								LastResponseCode,  IsRFID)
	SELECT  T.PointOfSaleId ,
            T.PurchasedDate ,
            T.TicketNumber ,
            A.PreAuthDate AS LastRetryDate ,
            0 AS NumRetries ,
            A.CardHash ,
            1 AS TypeId ,
            A.CardData ,
            A.CardExpiry ,
            T.Amount ,
            A.CardType ,
            A.Last4DigitsOfCardNumber ,
            UTC_TIMESTAMP() AS CreationDate ,
            '' AS BadCardHash ,
            0 AS IgnoreBadCard ,
            0 AS LastResponseCode ,
            A.IsRFID
    FROM    Customer C ,
			PointOfSale P ,
            ProcessorTransaction T ,
            PreAuthHolding A 
    WHERE   C.Id = P.CustomerId
	AND 	P.Id = T.PointOfSaleId
    AND     A.PointOfSaleId = T.PointOfSaleId
    AND     SUBSTR(T.AuthorizationNumber,1,INSTR(T.AuthorizationNumber,':')-1) = A.AuthorizationNumber
    AND     SUBSTR(T.AuthorizationNumber,INSTR(T.AuthorizationNumber,':')+1) = lv_PreAuthId
    AND     T.ProcessorTransactionTypeId = 20 
    AND     T.Amount = A.Amount
    AND     T.AuthorizationNumber LIKE '%:%'
    AND     A.Approved = 1
    AND     A.Expired = 1 
	AND     A.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard');
    
								
    
    -- Save number of records inserted
    -- SET Inserted = ROW_COUNT();
    
    -- Mark PreAuthHolding records as having been moved into CardRetryTransaction
    UPDATE  PreAuthHolding A SET A.MovedToRetryGMT = Creation_Date WHERE A.PreAuthId = lv_PreAuthId;
   								
set indexm_pos = indexm_pos +1;
end while;

close c1;
SET out_NoOfRecoverableRecords= idmaster_pos;
END $$

DELIMITER ;

-- End sp_PreAuthHoldingToRetry


-- -----------------------------------------------------
-- Stored Procedure sp_ETLAutoRecoverable
-- -----------------------------------------------------

-- For AutoRecoverable

DROP PROCEDURE IF EXISTS sp_ETLAutoRecoverable ;

delimiter //

CREATE PROCEDURE sp_ETLAutoRecoverable ()
BEGIN

declare NO_DATA int(4) default 0;
DECLARE lv_count int;
DECLARE lv_status TINYINT;
declare lv_ETLExecutionLogId bigint unsigned;
declare lv_CurrentTime DateTime;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

select now() into lv_CurrentTime ;

SELECT COUNT(1) INTO lv_count FROM ETLAutoRecoverableLog where ETLProcessTypeId  = 8;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLAutoRecoverableLog (ETLProcessTypeId,  	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES		(8,		 			lv_CurrentTime,      	lv_CurrentTime, 			2,	     'InitialRecord'  );
  set lv_status = 2;
  set lv_CurrentTime = DATE_ADD(lv_CurrentTime,INTERVAL 1 second);
ELSE

  SELECT Status 
  INTO lv_status
  FROM ETLAutoRecoverableLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLAutoRecoverableLog WHERE ETLProcessTypeId = 8 )  ;
  
END IF;


If (lv_status = 2) THEN		-- means previous JOb process was sucessful

    
	INSERT INTO ETLAutoRecoverableLog (ETLProcessTypeId, 		ETLStartTime, Status)
						VALUES 			(8, 		 		lv_CurrentTime, 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	
	CALL sp_PreAuthHoldingToRetry( @out_NoOfRecoverableRecords);
	
	UPDATE ETLAutoRecoverableLog
	SET 
	ETLEndTime = NOW(),
	NoOfRecoverable = @out_NoOfRecoverableRecords,
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

