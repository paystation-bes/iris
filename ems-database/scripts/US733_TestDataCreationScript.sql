
-- Need to deploy the Procedure on EMS6 DEV

-- After creating the EMS6 Database run the below
grant all on *.* to 'ltikhova'@'%' identified by 'ltikhova' with grant
 option;

flush privileges;


DROP PROCEDURE IF EXISTS sp_CreateEMS6Account;

delimiter //

CREATE PROCEDURE sp_CreateEMS6Account (IN P_CustomerName VARCHAR(40))
BEGIN

declare NO_DATA int(4) default 0;
declare lv_CustomerId, lv_RegionId,lv_PaystationId int; 	
	
declare continue handler for not found set NO_DATA=-1;

-- Insert Customer

INSERT INTO Customer (version, Name, 			AccountStatus) VALUES
					 (0,	  P_CustomerName,	'ENABLED') ;

SELECT LAST_INSERT_ID() INTO lv_CustomerId; 

-- Insert into Region

INSERT INTO Region (version, AlarmState, CustomerId, 		Name, Description, ParentRegion, IsDefault) VALUES
				   (0,		 'Clear',	 lv_CustomerId,		'Airport',	0,			 0,  		0) ;
				   
SELECT LAST_INSERT_ID() INTO lv_RegionId; 

-- Insert into MerchantAccount

INSERT INTO `MerchantAccount` (`version`,`CustomerId`,`Name`,`Field1`,`Field2`,`Field3`,`IsDeleted`,`ProcessorName`,`ReferenceCounter`,`Field4`,`Field5`,`Field6`,`IsValid`) VALUES 
							(0,			lv_CustomerId,'Paymentech','700000002018','001','digitech1',0,'Paymentech',1,'digpaytec01','0008',NULL,1);
INSERT INTO `MerchantAccount` (`version`,`CustomerId`,`Name`,`Field1`,`Field2`,`Field3`,`IsDeleted`,`ProcessorName`,`ReferenceCounter`,`Field4`,`Field5`,`Field6`,`IsValid`) VALUES 
							(0,			lv_CustomerId,'BlackBoard','abcdef0123456789abcdef','66.210.59.124','0910',1,'BlackBoard',1,'0164',NULL,NULL,0);
INSERT INTO `MerchantAccount` (`version`,`CustomerId`,`Name`,`Field1`,`Field2`,`Field3`,`IsDeleted`,`ProcessorName`,`ReferenceCounter`,`Field4`,`Field5`,`Field6`,`IsValid`) VALUES 
							(0,			lv_CustomerId,'Test.Authorize.NET','r3RuC5XP','Chase Paymentech Solutions','RmDP5KySTbAb39v6',1,'Authorize.NET',2,NULL,NULL,NULL,0);
INSERT INTO `MerchantAccount` (`version`,`CustomerId`,`Name`,`Field1`,`Field2`,`Field3`,`IsDeleted`,`ProcessorName`,`ReferenceCounter`,`Field4`,`Field5`,`Field6`,`IsValid`) VALUES 
							(0,			lv_CustomerId,'FDN','000000000001065','01005315','00010372867883130794',1,'First Data Nashville',25,'https://staging1.datawire.net/sd|https://staging2.datawire.net/sd','980051211','0005',1);



-- Insert into Paystation

INSERT INTO Paystation ( version , Name, 	CommAddress, 										ContactURL, 						CustomerId, 	IsActive, RegionId, 	TimeZone, 			LockState, 	ProvisionDate, 			MerchantAccountId, DeleteFlag, CustomCardMerchantAccountId, PaystationType, IsVerrus, QueryStallsBy, LotSettingId, EMSMigrationId, EMSPaystationURLRerouteId) VALUES
						(0,        'Level1', CONCAT('5000',LPAD(lv_CustomerId,4,'0'), '0001'),	'ashok.pamu@digitalpaytech.com',	lv_CustomerId,	1,		  lv_RegionId,	'Canada/Pacific',	'UNLOCKED',	'2014-01-01 00:00:00',	1,					0,			0,							1,				0,			0,			 0,				0,				0);

INSERT INTO Paystation ( version , Name, 	CommAddress, 										ContactURL, 						CustomerId, 	IsActive, RegionId, 	TimeZone, 			LockState, 	ProvisionDate, 			MerchantAccountId, DeleteFlag, CustomCardMerchantAccountId, PaystationType, IsVerrus, QueryStallsBy, LotSettingId, EMSMigrationId, EMSPaystationURLRerouteId) VALUES
						(0,        'Level2', CONCAT('5000',LPAD(lv_CustomerId,4,'0'), '0002'),	'ashok.pamu@digitalpaytech.com',	lv_CustomerId,	1,		  lv_RegionId,	'Canada/Pacific',	'UNLOCKED',	'2014-01-01 00:00:00',	2,					0,			0,							1,				0,			0,			 0,				0,				0);

INSERT INTO Paystation ( version , Name, 	CommAddress, 										ContactURL, 						CustomerId, 	IsActive, RegionId, 	TimeZone, 			LockState, 	ProvisionDate, 			MerchantAccountId, DeleteFlag, CustomCardMerchantAccountId, PaystationType, IsVerrus, QueryStallsBy, LotSettingId, EMSMigrationId, EMSPaystationURLRerouteId) VALUES
						(0,        'Level3', CONCAT('5000',LPAD(lv_CustomerId,4,'0'), '0003'),	'ashok.pamu@digitalpaytech.com',	lv_CustomerId,	1,		  lv_RegionId,	'Canada/Pacific',	'UNLOCKED',	'2014-01-01 00:00:00',	3,					0,			0,							1,				0,			0,			 0,				0,				0);

INSERT INTO Paystation ( version , Name, 	CommAddress, 										ContactURL, 						CustomerId, 	IsActive, RegionId, 	TimeZone, 			LockState, 	ProvisionDate, 			MerchantAccountId, DeleteFlag, CustomCardMerchantAccountId, PaystationType, IsVerrus, QueryStallsBy, LotSettingId, EMSMigrationId, EMSPaystationURLRerouteId) VALUES
						(0,        'Level4', CONCAT('5000',LPAD(lv_CustomerId,4,'0'), '0004'),	'ashok.pamu@digitalpaytech.com',	lv_CustomerId,	1,		  lv_RegionId,	'Canada/Pacific',	'UNLOCKED',	'2014-01-01 00:00:00',	4,					0,			0,							1,				0,			0,			 0,				0,				0);

INSERT INTO Paystation ( version , Name, 	CommAddress, 										ContactURL, 						CustomerId, 	IsActive, RegionId, 	TimeZone, 			LockState, 	ProvisionDate, 			MerchantAccountId, DeleteFlag, CustomCardMerchantAccountId, PaystationType, IsVerrus, QueryStallsBy, LotSettingId, EMSMigrationId, EMSPaystationURLRerouteId) VALUES
						(0,        'Level5', CONCAT('5000',LPAD(lv_CustomerId,4,'0'), '0005'),	'ashok.pamu@digitalpaytech.com',	lv_CustomerId,	1,		  lv_RegionId,	'Canada/Pacific',	'UNLOCKED',	'2014-01-01 00:00:00',	1,					0,			0,							1,				0,			0,			 0,				0,				0);

						
SELECT LAST_INSERT_ID() INTO lv_PaystationId; 


END //
DELIMITER ;


-- Need to deploy the Procedure on EMS7 DEV

-- 
-- CUSTOMER MIGRATION:				Remaining, Boarded, Scheduled, Migrated
-- RECENTLY BOARDED CUSTOMERS:		Customer,Boarded Date, Paystations
-- MIGRATED PAY STATION PROGRESS:	Remaining, Boarded, Scheduled, Migrated
-- RECENTLY MIGRATED CUSTOERS:		Customer, Migrate Date, Paystations

-- StoredProcedure Input CustomerName, MigrationProgress

DROP PROCEDURE IF EXISTS sp_SetCustomerMigrationStatusType;

delimiter //

CREATE PROCEDURE sp_SetCustomerMigrationStatusType (IN P_CustomerName VARCHAR(40), IN P_CustomerMigrationStatusTypeId TINYINT UNSIGNED, IN P_Date DATETIME)
BEGIN

declare NO_DATA int(4) default 0;
declare lv_CustomerId, lv_UserAccountId, lv_PaystationCount MEDIUMINT UNSIGNED;
declare lv_CustomerBoardedGMT datetime;
declare lv_CustomerMigrationStatusTypeId TINYINT UNSIGNED ;
declare continue handler for not found set NO_DATA=-1;

-- 0 Requested 
-- 1 Scheduled 
-- 2 Suspended
-- 3 Cancelled
-- 4 Queued
-- 5 EMS6 Disabled
-- 6 Waiting Data Migration
-- 7 Data Migration Confirmed
-- 8 Completed
-- 9 Communication Checked
-- 10 All Pay Stations Migrated
-- 90 Boarded

SELECT Id into lv_CustomerId from Customer where Name = P_CustomerName;

SELECT COUNT(distinct PaystationId) into lv_PaystationCount FROM PointOfSale where CustomerId = lv_CustomerId;

SELECT CustomerMigrationStatusTypeId into lv_CustomerMigrationStatusTypeId
FROM CustomerMigration
WHERE CustomerId = lv_CustomerId ;

-- 90 Boarded means - Customer Logged in to Iris. There is an entry in ActivityLogin. 
-- in CustomerMigration: 
--		CustomerMigrationStatusTypeId = 90
--		CustomerBoardedGMT = MIN(ActivityLogin.ActivityGMT) WHERE LoginResultTypeId = 1 (Successful Login)
--		TotalPaystationCount = SELECT COUNT(*) from PointOfSale where CustomerId = 

IF (P_CustomerMigrationStatusTypeId = 90 ) THEN -- Boarded

	-- Insert into ActivityLogin to get the CustomerBoardedGMT
	select min(Id) into lv_UserAccountId from UserAccount where CustomerId = lv_CustomerId;
	
	SELECT MIN(ActivityGMT) into lv_CustomerBoardedGMT FROM ActivityLogin where UserAccountId = ( select MIN(Id) from UserAccount where CustomerId = lv_CustomerId)
	AND LoginResultTypeId = 1 ;
	
	IF (lv_CustomerBoardedGMT IS NULL) THEN
		insert into ;( UserAccountId, LoginResultTypeId, ActivityGMT) VALUES
		( lv_UserAccountId,1,P_Date);
	
		insert into ActivityLogin( UserAccountId, LoginResultTypeId, ActivityGMT) VALUES
		( lv_UserAccountId,4,P_Date);
	END IF;

	SELECT MIN(ActivityGMT) into lv_CustomerBoardedGMT FROM ActivityLogin where UserAccountId = ( select MIN(Id) from UserAccount where CustomerId = lv_CustomerId)
	AND LoginResultTypeId = 1 ;
	
	UPDATE CustomerMigration
	SET CustomerMigrationStatusTypeId = 90,
	CustomerBoardedGMT = lv_CustomerBoardedGMT ,
	MigrationRequestedGMT = NULL,
	MigrationScheduledGMT = NULL,
	MigrationCancelGMT = NULL,
	MigrationStartGMT = NULL,
	MigrationEndGMT = NULL,
	TotalPaystationCount = lv_PaystationCount
	WHERE CustomerId = lv_CustomerId ;

END IF;
		
		
-- 0 Requested means - Customer requested to migrate to Iris. 
--     Prerequiste: Customer should be boarded (status = 90)
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 0
--		MigrationRequestedGMT = UTC_TIMESTAMP()
--		TotalPaystationCount = COUNT(PointOfSale)
		
IF (P_CustomerMigrationStatusTypeId = 0 ) AND (lv_CustomerMigrationStatusTypeId = 90) THEN -- Requested
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 0,
		MigrationRequestedGMT = P_Date,
		MigrationScheduledGMT = DATE_ADD(date(P_Date),INTERVAL 14 day), -- hr00 2 weeks after request and weekday
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;

	
-- 1 Scheduled - Customer is scheduled to migrate to Iris. 
--	 Prerequiste: Customer should request (status = 0) OR Status should  be 90
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 1
--		MigrationScheduledGMT = UTC_TIMESTAMP()
		
IF (P_CustomerMigrationStatusTypeId = 1 ) AND (lv_CustomerMigrationStatusTypeId = 0) THEN -- Scheduled
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 1,
		MigrationScheduledGMT = P_Date,
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;	

-- 2 Suspended - Migration Process suspended
--	 Prerequiste: Customer should request (status = 0) OR 	Scheduled (status = 1) OR  ...
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 2

IF (P_CustomerMigrationStatusTypeId = 2 ) AND (lv_CustomerMigrationStatusTypeId = 1) THEN -- Suspended
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 2,	
		MigrationCancelGMT = P_Date	,	
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;			


-- 3 Cancelled -- Migration Process Cancelled
--	 Prerequiste: Customer should request (status = 0) OR (Queued=4)
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 3
--		MigrationCancelGMT = UTC_TIMESTAMP()
-- no need for now

IF (P_CustomerMigrationStatusTypeId = 3 )  THEN -- Cancelled
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 3,	
		MigrationCancelGMT = P_Date,
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;	


-- 4 Queued -- Migration Process Queued
--	 Prerequiste: Customer should request (status = 0) OR (Queued=4)
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 3
--		MigrationCancelGMT = UTC_TIMESTAMP()

IF (P_CustomerMigrationStatusTypeId = 4 )  THEN -- Queued
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 4,	
		
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;	



-- 7 Data Migration Confirmed
-- Prerequiste: Queued = 4

IF (P_CustomerMigrationStatusTypeId = 7 ) AND (lv_CustomerMigrationStatusTypeId = 4) THEN -- Data Migration Confirmed
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 7,	
		MigrationStartGMT=  P_Date,
		DataMigrationConfirmedGMT = P_Date,
		MigrationEndGMT = NULL,
		TotalPaystationCount = lv_PaystationCount
		WHERE CustomerId = lv_CustomerId ;

END IF;	
		
-- 8 Completed -- Migration Process Completed		
--	 Prerequiste: Customer should request (status = 0) and (Data Migration Confirmed=7)
--	 in CustomerMigration:
--		CustomerMigrationStatusTypeId = 8
--		MigrationScheduledGMT = MigrationRequestedGMT
--		MigrationStartGMT = UTC_TIMESTAMP()
--		MigrationEndGMT = UTC_TIMESTAMP()
--		... and many more

IF (P_CustomerMigrationStatusTypeId = 8 ) AND (lv_CustomerMigrationStatusTypeId = 7) THEN -- Completed
	
		UPDATE CustomerMigration
		SET CustomerMigrationStatusTypeId = 8,	
		MigrationEndGMT = P_Date		
		WHERE CustomerId = lv_CustomerId ;

END IF;	
		
END //
DELIMITER ;


/*

-- call sp_SetCustomerMigrationStatusType('ASHOK6',90); -- boarded
-- call sp_SetCustomerMigrationStatusType('ASHOK6',0); -- requested
-- call sp_SetCustomerMigrationStatusType('ASHOK6',1); -- scheduled
-- call sp_SetCustomerMigrationStatusType('ASHOK6',2); -- Suspended
-- call sp_SetCustomerMigrationStatusType('ASHOK6',3); -- Cancelled
-- call sp_SetCustomerMigrationStatusType('ASHOK6',4); -- Queued
-- call sp_SetCustomerMigrationStatusType('ASHOK6',7); -- Data Migration Confirmed
-- call sp_SetCustomerMigrationStatusType('ASHOK6',8); -- Completed

	
'Asbury Park'
'LR Auto Park'
'Kamloops Airport'
'Cedar Rapids'
'Miami Dade'
'CR Parking'
'Chase Tower Management'
'Panhandle Parking'
'Republic Memphis'
'Townsite Marina'
'PCOM'
'Illinois IT'
'Noble Parking'
'Republic Jacksonville'
'Peninsula Airport'
'Signature Controls'
'Standard Atlanta'
'Republic Louisville'
'Roosevelt Island'
'Uptown Consortium'
'Standard Salt Lake'
'Lanier Columbia'
'Standard Soldier Field'
'City of Newton'
'Laz Harris County'
'Harborside Commons'
'UBC Vancouver'
'Metropolitan Parking'
'City of Racine'
'Marriott Fullerton'
'OKC Museum of Art'
'Red River College'
'Ampco SF - 2416'
'Standard Cleveland'
'Laz Atlanta - IP'
'Impark PG'
'Impark Kamloops'
'Impark Kelowna'
'ECPS'
'VDC Norte'
'Vista del Campo'
'South Miami'
'Central Phoenix'
'City of Nampa'
'UCSB Inactive'
'Standard Houston'
'Bowling Green'
'Geisel'
'Standard San Fran'
'Standard Kansas City'
'Richard N Best'
'PCA Ohio'
'UC Irvine'
'Diamond Maui 2'
'City of Tampa'
'1517 Pine Street'
'Central Charlotte'
'DSPS Paul'
'DSPS Herald'
'DSPS Shuswap'
'Surfside'
'UC Davis'
'Cleveland State U'
'Santa Barbara'
'WPS NA'

call sp_CreateEMS6Account(	'Asbury Park'	);
call sp_CreateEMS6Account(	'LR Auto Park'	);
call sp_CreateEMS6Account(	'Kamloops Airport'	);
call sp_CreateEMS6Account(	'Cedar Rapids'	);
call sp_CreateEMS6Account(	'Miami Dade'	);
call sp_CreateEMS6Account(	'CR Parking'	);
call sp_CreateEMS6Account(	'Chase Tower Management'	);
call sp_CreateEMS6Account(	'Panhandle Parking'	);
call sp_CreateEMS6Account(	'Republic Memphis'	);
call sp_CreateEMS6Account(	'Townsite Marina'	);
call sp_CreateEMS6Account(	'PCOM'	);
call sp_CreateEMS6Account(	'Illinois IT'	);
call sp_CreateEMS6Account(	'Noble Parking'	);
call sp_CreateEMS6Account(	'Republic Jacksonville'	);
call sp_CreateEMS6Account(	'Peninsula Airport'	);
call sp_CreateEMS6Account(	'Signature Controls'	);
call sp_CreateEMS6Account(	'Standard Atlanta'	);
call sp_CreateEMS6Account(	'Republic Louisville'	);
call sp_CreateEMS6Account(	'Roosevelt Island'	);
call sp_CreateEMS6Account(	'Uptown Consortium'	);
call sp_CreateEMS6Account(	'Standard Salt Lake'	);
call sp_CreateEMS6Account(	'Lanier Columbia'	);
call sp_CreateEMS6Account(	'Standard Soldier Field'	);
call sp_CreateEMS6Account(	'City of Newton'	);
call sp_CreateEMS6Account(	'Laz Harris County'	);
call sp_CreateEMS6Account(	'Harborside Commons'	);
call sp_CreateEMS6Account(	'UBC Vancouver'	);
call sp_CreateEMS6Account(	'Metropolitan Parking'	);
call sp_CreateEMS6Account(	'City of Racine'	);
call sp_CreateEMS6Account(	'Marriott Fullerton'	);
call sp_CreateEMS6Account(	'OKC Museum of Art'	);
call sp_CreateEMS6Account(	'Red River College'	);
call sp_CreateEMS6Account(	'Ampco SF - 2416'	);
call sp_CreateEMS6Account(	'Standard Cleveland'	);
call sp_CreateEMS6Account(	'Laz Atlanta - IP'	);
call sp_CreateEMS6Account(	'Impark PG'	);
call sp_CreateEMS6Account(	'Impark Kamloops'	);
call sp_CreateEMS6Account(	'Impark Kelowna'	);
call sp_CreateEMS6Account(	'ECPS'	);
call sp_CreateEMS6Account(	'VDC Norte'	);
call sp_CreateEMS6Account(	'Vista del Campo'	);
call sp_CreateEMS6Account(	'South Miami'	);
call sp_CreateEMS6Account(	'Central Phoenix'	);
call sp_CreateEMS6Account(	'City of Nampa'	);
call sp_CreateEMS6Account(	'UCSB Inactive'	);
call sp_CreateEMS6Account(	'Standard Houston'	);
call sp_CreateEMS6Account(	'Bowling Green'	);
call sp_CreateEMS6Account(	'Geisel'	);
call sp_CreateEMS6Account(	'Standard San Fran'	);
call sp_CreateEMS6Account(	'Standard Kansas City'	);
call sp_CreateEMS6Account(	'Richard N Best'	);
call sp_CreateEMS6Account(	'PCA Ohio'	);
call sp_CreateEMS6Account(	'UC Irvine'	);
call sp_CreateEMS6Account(	'Diamond Maui 2'	);
call sp_CreateEMS6Account(	'City of Tampa'	);
call sp_CreateEMS6Account(	'1517 Pine Street'	);
call sp_CreateEMS6Account(	'Central Charlotte'	);
call sp_CreateEMS6Account(	'DSPS Paul'	);
call sp_CreateEMS6Account(	'DSPS Herald'	);
call sp_CreateEMS6Account(	'DSPS Shuswap'	);
call sp_CreateEMS6Account(	'Surfside'	);
call sp_CreateEMS6Account(	'UC Davis'	);
call sp_CreateEMS6Account(	'Cleveland State U'	);
call sp_CreateEMS6Account(	'Santa Barbara'	);
call sp_CreateEMS6Account(	'WPS NA'	);
call sp_CreateEMS6Account(	'Ampco LA - Douglas Emmett'	);
call sp_CreateEMS6Account(	'Cal Poly SLO'	);
call sp_CreateEMS6Account(	'Tapco'	);
call sp_CreateEMS6Account(	'System Parking Louisville'	);
call sp_CreateEMS6Account(	'UC San Diego'	);
call sp_CreateEMS6Account(	'Central HoustonBrookfield'	);
call sp_CreateEMS6Account(	'DSPS Quality'	);
call sp_CreateEMS6Account(	'U of New Mexico'	);
call sp_CreateEMS6Account(	'Minneapolis Parks'	);
call sp_CreateEMS6Account(	'Jim Roderique'	);
call sp_CreateEMS6Account(	'City of Ventura'	);
call sp_CreateEMS6Account(	'Impark Saskatoon'	);
call sp_CreateEMS6Account(	'Ampco SA'	);
call sp_CreateEMS6Account(	'McMaster University'	);
call sp_CreateEMS6Account(	'DPTQATEST'	);
call sp_CreateEMS6Account(	'DSPS Kaloya'	);
call sp_CreateEMS6Account(	'Standard PA'	);
call sp_CreateEMS6Account(	'LAZ Block 7'	);
call sp_CreateEMS6Account(	'Boca Raton'	);
call sp_CreateEMS6Account(	'Lubbock Street Property'	);
call sp_CreateEMS6Account(	'Go Navy'	);
call sp_CreateEMS6Account(	'Fayetteville'	);
call sp_CreateEMS6Account(	'LAM Parking'	);
call sp_CreateEMS6Account(	'DSPS Cypress'	);
call sp_CreateEMS6Account(	'Laz New Bedford'	);
call sp_CreateEMS6Account(	'Impark St Paul'	);
call sp_CreateEMS6Account(	'Little Silver'	);
call sp_CreateEMS6Account(	'Galveston'	);
call sp_CreateEMS6Account(	'Westin Hotel'	);
call sp_CreateEMS6Account(	'Traverse City'	);
call sp_CreateEMS6Account(	'Laz Tampa'	);
call sp_CreateEMS6Account(	'Iowa City'	);
call sp_CreateEMS6Account(	'Central St. Louis'	);
call sp_CreateEMS6Account(	'Central Rhode Island'	);
call sp_CreateEMS6Account(	'LAZ CA Parks'	);
call sp_CreateEMS6Account(	'Standard Corus'	);
call sp_CreateEMS6Account(	'Madison Museum'	);
call sp_CreateEMS6Account(	'Central Columbus'	);
call sp_CreateEMS6Account(	'PCA Dallas'	);
call sp_CreateEMS6Account(	'Jeff Hunt'	);
call sp_CreateEMS6Account(	'City of Prescott'	);
call sp_CreateEMS6Account(	'Laz Chicago'	);
call sp_CreateEMS6Account(	'Diamond Bellevue'	);
call sp_CreateEMS6Account(	'Port Chester'	);
call sp_CreateEMS6Account(	'Condo Assoc'	);
call sp_CreateEMS6Account(	'Arkansas State U'	);
call sp_CreateEMS6Account(	'Central San Diego'	);
call sp_CreateEMS6Account(	'U of Virginia'	);
call sp_CreateEMS6Account(	'City of Sanibel'	);
call sp_CreateEMS6Account(	'Denver Justice'	);
call sp_CreateEMS6Account(	'Wisconsin Dells'	);
call sp_CreateEMS6Account(	'Central Virginia'	);
call sp_CreateEMS6Account(	'Central Lexington'	);
call sp_CreateEMS6Account(	'Yale'	);
call sp_CreateEMS6Account(	'Alpha Park'	);
call sp_CreateEMS6Account(	'Standard Norwood'	);
call sp_CreateEMS6Account(	'Black White Valet'	);
call sp_CreateEMS6Account(	'Standard Crown Properties'	);
call sp_CreateEMS6Account(	'Central New York'	);
call sp_CreateEMS6Account(	'Corning Gaffer District'	);
call sp_CreateEMS6Account(	'Park West'	);
call sp_CreateEMS6Account(	'Lanier Wilton Manors'	);
call sp_CreateEMS6Account(	'Central Fort Worth'	);
call sp_CreateEMS6Account(	'T2 Conference'	);
call sp_CreateEMS6Account(	'Parking Plus'	);
call sp_CreateEMS6Account(	'Seattle Mariners'	);
call sp_CreateEMS6Account(	'Golden Gate Bridge'	);
call sp_CreateEMS6Account(	'Shands Healthcare'	);
call sp_CreateEMS6Account(	'Lanier Beaufort'	);
call sp_CreateEMS6Account(	'Diamond Honolulu 2'	);
call sp_CreateEMS6Account(	'DSPS Marquise'	);
call sp_CreateEMS6Account(	'City of Lake Geneva Trial'	);
call sp_CreateEMS6Account(	'Vanderbilt U'	);
call sp_CreateEMS6Account(	'PSC Ext Test'	);
call sp_CreateEMS6Account(	'xxx Cncld City San Jose'	);
call sp_CreateEMS6Account(	'Lanier Tampa'	);
call sp_CreateEMS6Account(	'SouthernTime'	);
call sp_CreateEMS6Account(	'Observer Highway Garage'	);
call sp_CreateEMS6Account(	'City of Saskatoon'	);
call sp_CreateEMS6Account(	'City of Union City'	);
call sp_CreateEMS6Account(	'U of Arkansas'	);
call sp_CreateEMS6Account(	'City of Pasadena'	);
call sp_CreateEMS6Account(	'Potomac Parking'	);
call sp_CreateEMS6Account(	'City of Winston Salem'	);
call sp_CreateEMS6Account(	'Lanier Orlando'	);
call sp_CreateEMS6Account(	'CPC Prop Austin'	);
call sp_CreateEMS6Account(	'The Retail Connection'	);
call sp_CreateEMS6Account(	'City of Columbia'	);
call sp_CreateEMS6Account(	'Village of Larchmont'	);
call sp_CreateEMS6Account(	'Central Memphis'	);
call sp_CreateEMS6Account(	'CA Parks'	);
call sp_CreateEMS6Account(	'GLO Parking'	);
call sp_CreateEMS6Account(	'CPC Prop San Antonio'	);
call sp_CreateEMS6Account(	'City of Morro Bay'	);
call sp_CreateEMS6Account(	'Five Star LA'	);
call sp_CreateEMS6Account(	'City of Fort Collins'	);
call sp_CreateEMS6Account(	'City of Santa Cruz'	);
call sp_CreateEMS6Account(	'Eagle Parking'	);
call sp_CreateEMS6Account(	'Coppin U'	);
call sp_CreateEMS6Account(	'Downtown Investments'	);
call sp_CreateEMS6Account(	'Park America'	);
call sp_CreateEMS6Account(	'Great Parking Systems'	);
call sp_CreateEMS6Account(	'Kings College'	);
call sp_CreateEMS6Account(	'Mamaroneck'	);
call sp_CreateEMS6Account(	'Liberty Park'	);
call sp_CreateEMS6Account(	'Unit Park'	);
call sp_CreateEMS6Account(	'Kwik Parking'	);
call sp_CreateEMS6Account(	'Village of Elburn'	);
call sp_CreateEMS6Account(	'Chicago Transit'	);
call sp_CreateEMS6Account(	'Sparkle Solutions'	);
call sp_CreateEMS6Account(	'NJ Transit'	);
call sp_CreateEMS6Account(	'Mount Kisco'	);
call sp_CreateEMS6Account(	'Diamond Maui'	);
call sp_CreateEMS6Account(	'Diamond Kona'	);
call sp_CreateEMS6Account(	'Standard Hamilton'	);
call sp_CreateEMS6Account(	'U of Oregon'	);
call sp_CreateEMS6Account(	'DSPS SmithersAP'	);
call sp_CreateEMS6Account(	'Interstate Parking'	);
call sp_CreateEMS6Account(	'Croton on Hudson'	);
call sp_CreateEMS6Account(	'U of Washington'	);
call sp_CreateEMS6Account(	'Pompano Beach'	);
call sp_CreateEMS6Account(	'City of Coral Gables'	);
call sp_CreateEMS6Account(	'Sac Regional Transit'	);
call sp_CreateEMS6Account(	'DPT Sales Demo'	);
call sp_CreateEMS6Account(	'Duncan Technologies'	);
call sp_CreateEMS6Account(	'Carillion(BAD)'	);
call sp_CreateEMS6Account(	'Central Seattle'	);
call sp_CreateEMS6Account(	'Melrose Cafe'	);
call sp_CreateEMS6Account(	'Merit Fort Worth'	);
call sp_CreateEMS6Account(	'Standard NJ'	);
call sp_CreateEMS6Account(	'Sabus Enterprises'	);
call sp_CreateEMS6Account(	'Diamond Ashland'	);
call sp_CreateEMS6Account(	'Laz Golconda'	);
call sp_CreateEMS6Account(	'Central Baltimore'	);
call sp_CreateEMS6Account(	'George Mason U'	);
call sp_CreateEMS6Account(	'Auraria HEC'	);
call sp_CreateEMS6Account(	'City of HB'	);
call sp_CreateEMS6Account(	'Cordish'	);
call sp_CreateEMS6Account(	'Central SM Old'	);
call sp_CreateEMS6Account(	'U of North Dakota'	);
call sp_CreateEMS6Account(	'River Rock'	);
call sp_CreateEMS6Account(	'Lehigh University'	);
call sp_CreateEMS6Account(	'Republic Bangor'	);
call sp_CreateEMS6Account(	'Palm Beach'	);
call sp_CreateEMS6Account(	'DSPS PRAirport'	);
call sp_CreateEMS6Account(	'Miami Parking'	);
call sp_CreateEMS6Account(	'CCSF'	);
call sp_CreateEMS6Account(	'Republic Spokane'	);
call sp_CreateEMS6Account(	'Kenai Airport'	);
call sp_CreateEMS6Account(	'City of Seward'	);
call sp_CreateEMS6Account(	'Chapman U'	);
call sp_CreateEMS6Account(	'Western Wash U'	);
call sp_CreateEMS6Account(	'Consolidated'	);
call sp_CreateEMS6Account(	'Training Room'	);
call sp_CreateEMS6Account(	'City of Nashville'	);
call sp_CreateEMS6Account(	'UNLV'	);
call sp_CreateEMS6Account(	'LAZ Hartford'	);
call sp_CreateEMS6Account(	'U of Missouri'	);
call sp_CreateEMS6Account(	'Tower Valet'	);
call sp_CreateEMS6Account(	'Central Omaha'	);
call sp_CreateEMS6Account(	'Parking Concepts'	);
call sp_CreateEMS6Account(	'Ann Arbor Off Street'	);
call sp_CreateEMS6Account(	'AAA Parking'	);
call sp_CreateEMS6Account(	'Prairieville Town'	);
call sp_CreateEMS6Account(	'City of Fort Myers'	);
call sp_CreateEMS6Account(	'Pismo Beach'	);
call sp_CreateEMS6Account(	'Penticton Lakeside Resort'	);
call sp_CreateEMS6Account(	'AAA Parking Gold'	);
call sp_CreateEMS6Account(	'DSPS SCParks'	);
call sp_CreateEMS6Account(	'Impark Calgary'	);
call sp_CreateEMS6Account(	'U of N Colorado'	);
call sp_CreateEMS6Account(	'Apple Security'	);
call sp_CreateEMS6Account(	'Premium Parking'	);
call sp_CreateEMS6Account(	'Pay2Park'	);
call sp_CreateEMS6Account(	'Univ of Maryland'	);
call sp_CreateEMS6Account(	'City of Ocean City'	);
call sp_CreateEMS6Account(	'KCMO Trial'	);
call sp_CreateEMS6Account(	'NAU Parking Services'	);
call sp_CreateEMS6Account(	'Boardroom'	);
call sp_CreateEMS6Account(	'SouthernSales'	);
call sp_CreateEMS6Account(	'Lake Express'	);
call sp_CreateEMS6Account(	'Milwaukee Port'	);
call sp_CreateEMS6Account(	'DSPS RLC'	);
call sp_CreateEMS6Account(	'Town of Port McNeil'	);
call sp_CreateEMS6Account(	'Carolina Beach'	);
call sp_CreateEMS6Account(	'Impark Hamilton Old'	);
call sp_CreateEMS6Account(	'DSPS K2Parks'	);
call sp_CreateEMS6Account(	'Southern Illinois U'	);
call sp_CreateEMS6Account(	'Delta Grand Okanagan'	);
call sp_CreateEMS6Account(	'DSPS SSG Holdings'	);
call sp_CreateEMS6Account(	'Central Dallas'	);
call sp_CreateEMS6Account(	'Central Austin'	);
call sp_CreateEMS6Account(	'Capitol Parking LLC'	);
call sp_CreateEMS6Account(	'NPA Show'	);
call sp_CreateEMS6Account(	'Fresno State'	);


Boarded					
					
call sp_SetCustomerMigrationStatusType(	'Asbury Park'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'LR Auto Park'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Kamloops Airport'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Cedar Rapids'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Miami Dade'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'CR Parking'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Chase Tower Management'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Panhandle Parking'	,	90	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Memphis'	,	90	,'2015-01-02 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Townsite Marina'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'PCOM'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Illinois IT'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Noble Parking'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Jacksonville'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Peninsula Airport'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Signature Controls'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Atlanta'	,	90	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Louisville'	,	90	,'2015-01-02 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Roosevelt Island'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Uptown Consortium'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Standard Salt Lake'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Columbia'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Soldier Field'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Newton'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Harris County'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Harborside Commons'	,	90	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UBC Vancouver'	,	90	,'2015-01-02 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Metropolitan Parking'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'City of Racine'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Marriott Fullerton'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'OKC Museum of Art'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Red River College'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Ampco SF - 2416'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Cleveland'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Atlanta - IP'	,	90	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark PG'	,	90	,'2015-01-02 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Kamloops'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Impark Kelowna'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'ECPS'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'VDC Norte'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Vista del Campo'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'South Miami'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Phoenix'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Nampa'	,	90	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UCSB Inactive'	,	90	,'2015-01-02 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Houston'	,	90	,'2014-12-01 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Bowling Green'	,	90	,'2014-12-10 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Geisel'	,	90	,'2014-12-26 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard San Fran'	,	90	,'2014-12-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Kansas City'	,	90	,'2015-01-05 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Richard N Best'	,	90	,'2015-01-07 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'PCA Ohio'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Ampco LA - Douglas Emmett'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Cal Poly SLO'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Tapco'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'System Parking Louisville'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UC San Diego'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central HoustonBrookfield'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Quality'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of New Mexico'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Minneapolis Parks'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Jim Roderique'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Ventura'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Saskatoon'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Ampco SA'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'McMaster University'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DPTQATEST'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Kaloya'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard PA'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'LAZ Block 7'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Boca Raton'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lubbock Street Property'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Go Navy'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Fayetteville'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'LAM Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Cypress'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz New Bedford'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark St Paul'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Little Silver'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Galveston'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Westin Hotel'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Traverse City'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Tampa'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Iowa City'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central St. Louis'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Rhode Island'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'LAZ CA Parks'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Corus'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Madison Museum'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Columbus'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'PCA Dallas'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Jeff Hunt'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Prescott'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Chicago'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Bellevue'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Port Chester'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Condo Assoc'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Arkansas State U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central San Diego'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Virginia'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Sanibel'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Denver Justice'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Wisconsin Dells'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Virginia'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Lexington'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Yale'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Alpha Park'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Norwood'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Black White Valet'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Crown Properties'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central New York'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Corning Gaffer District'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Park West'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Wilton Manors'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Fort Worth'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'T2 Conference'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Parking Plus'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Seattle Mariners'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Golden Gate Bridge'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Shands Healthcare'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Beaufort'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Honolulu 2'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Marquise'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Lake Geneva Trial'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Vanderbilt U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'PSC Ext Test'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'xxx Cncld City San Jose'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Tampa'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'SouthernTime'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Observer Highway Garage'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Saskatoon'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Union City'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Arkansas'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Pasadena'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Potomac Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Winston Salem'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Orlando'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'CPC Prop Austin'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'The Retail Connection'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Columbia'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Village of Larchmont'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Memphis'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'CA Parks'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'GLO Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'CPC Prop San Antonio'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Morro Bay'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Five Star LA'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Fort Collins'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Santa Cruz'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Eagle Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Coppin U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Downtown Investments'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Park America'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Great Parking Systems'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Kings College'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Mamaroneck'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Liberty Park'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Unit Park'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Kwik Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Village of Elburn'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Chicago Transit'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Sparkle Solutions'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'NJ Transit'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Mount Kisco'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Maui'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Kona'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Hamilton'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Oregon'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS SmithersAP'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Interstate Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Croton on Hudson'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Washington'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Pompano Beach'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Coral Gables'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Sac Regional Transit'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DPT Sales Demo'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Duncan Technologies'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Carillion(BAD)'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Seattle'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Melrose Cafe'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Merit Fort Worth'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard NJ'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Sabus Enterprises'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Ashland'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Golconda'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Baltimore'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'George Mason U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Auraria HEC'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of HB'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Cordish'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central SM Old'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of North Dakota'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'River Rock'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lehigh University'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Bangor'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Palm Beach'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS PRAirport'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Miami Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'CCSF'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Spokane'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Kenai Airport'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Seward'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Chapman U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Western Wash U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Consolidated'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Training Room'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Nashville'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UNLV'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'LAZ Hartford'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Missouri'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Tower Valet'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Omaha'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Parking Concepts'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Ann Arbor Off Street'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'AAA Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Prairieville Town'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Fort Myers'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Pismo Beach'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Penticton Lakeside Resort'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'AAA Parking Gold'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS SCParks'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Calgary'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of N Colorado'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Apple Security'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Premium Parking'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Pay2Park'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Univ of Maryland'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Ocean City'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'KCMO Trial'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'NAU Parking Services'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Boardroom'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'SouthernSales'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lake Express'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Milwaukee Port'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS RLC'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Town of Port McNeil'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Carolina Beach'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Hamilton Old'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS K2Parks'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Southern Illinois U'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Delta Grand Okanagan'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS SSG Holdings'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Dallas'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Austin'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Capitol Parking LLC'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'NPA Show'	,	90	,'2015-01-04 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Fresno State'	,	90	,'2015-01-04 10:00:00'	);
					
Requested					
					
call sp_SetCustomerMigrationStatusType(	'Asbury Park'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'LR Auto Park'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Kamloops Airport'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Cedar Rapids'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Miami Dade'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'CR Parking'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Chase Tower Management'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Panhandle Parking'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Memphis'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Townsite Marina'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'PCOM'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Illinois IT'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Noble Parking'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Jacksonville'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Peninsula Airport'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Signature Controls'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Atlanta'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Republic Louisville'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Roosevelt Island'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Uptown Consortium'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Standard Salt Lake'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Lanier Columbia'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Soldier Field'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Newton'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Harris County'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Harborside Commons'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UBC Vancouver'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Metropolitan Parking'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'City of Racine'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Marriott Fullerton'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'OKC Museum of Art'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Red River College'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Ampco SF - 2416'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Cleveland'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Atlanta - IP'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark PG'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Kamloops'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Impark Kelowna'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'ECPS'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'VDC Norte'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Vista del Campo'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'South Miami'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Phoenix'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Nampa'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'UCSB Inactive'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Houston'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Bowling Green'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Geisel'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard San Fran'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Kansas City'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Richard N Best'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'PCA Ohio'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Ampco LA - Douglas Emmett'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Cal Poly SLO'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Tapco'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'System Parking Louisville'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'UC San Diego'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Central HoustonBrookfield'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Quality'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'U of New Mexico'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Minneapolis Parks'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Jim Roderique'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'City of Ventura'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark Saskatoon'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Ampco SA'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'McMaster University'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'DPTQATEST'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Kaloya'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard PA'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'LAZ Block 7'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Boca Raton'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Lubbock Street Property'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Go Navy'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Fayetteville'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'LAM Parking'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'DSPS Cypress'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz New Bedford'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Impark St Paul'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Little Silver'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Galveston'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Westin Hotel'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Traverse City'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Laz Tampa'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'Iowa City'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Central St. Louis'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Rhode Island'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'LAZ CA Parks'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Standard Corus'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Madison Museum'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central Columbus'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'PCA Dallas'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'Jeff Hunt'	,	0	,'2014-12-20 18:18:18'	);
call sp_SetCustomerMigrationStatusType(	'City of Prescott'	,	0	,'2014-12-30 19:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Laz Chicago'	,	0	,'2014-12-06 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Diamond Bellevue'	,	0	,'2015-01-06 09:15:00'	);
call sp_SetCustomerMigrationStatusType(	'Port Chester'	,	0	,'2015-01-08 07:18:00'	);
call sp_SetCustomerMigrationStatusType(	'Condo Assoc'	,	0	,'2015-01-05 10:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Arkansas State U'	,	0	,'2015-01-05 08:00:00'	);
call sp_SetCustomerMigrationStatusType(	'Central San Diego'	,	0	,'2015-01-07 16:00:00'	);
call sp_SetCustomerMigrationStatusType(	'U of Virginia'	,	0	,'2014-12-10 10:15:17'	);
call sp_SetCustomerMigrationStatusType(	'City of Sanibel'	,	0	,'2014-12-20 18:18:18'	);

-- This need to be executed on EMS7 if DB is rebuild

INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (1,'6.2 build 0616');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (2,'6.2 build 0620');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (3,'6.2 build 0708');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (4,'6.3 build 06208');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (5,'6.4 build 0016');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (6,'6.4 build 0026');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (7,'6.4 build 0144');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (8,'6.4 build 0154');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (16,'6.4 build 0156');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (9,'6.4 build 0164');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (10,'6.4 build 0166');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (14,'6.4 build 0168');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (13,'6.4 build 0170');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (12,'6.4 build 0174');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (11,'6.4 build 0180');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (17,'6.4 build 0220');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (18,'6.4 build 0224');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (19,'6.4 build 0228');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (20,'6.4 build 0310');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (15,'6.4 build 0314');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (21,'6.4 build 0316');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (23,'6.4 build 0320');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (24,'6.4 build 0324');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (22,'6.4 build 0330');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (25,'6.4 build 0336');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (26,'6.4 build 0346');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (28,'6.4 build 0348');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (29,'6.4 build 0352');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (30,'6.4 build 0360');
INSERT INTO `MigrationReadyPayStationVersion` (`Id`,`VersionName`) VALUES (27,'n/a');


UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Concord' WHERE `Id`='1';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='First Data Nashville' WHERE `Id`='2';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Heartland' WHERE `Id`='3';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Link2Gov' WHERE `Id`='4';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Parcxmart' WHERE `Id`='5';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Alliance' WHERE `Id`='6';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Paymentech' WHERE `Id`='7';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Paradata Gateway' WHERE `Id`='8';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Moneris' WHERE `Id`='9';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='FirstHorizon' WHERE `Id`='10';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='Authorize.NET' WHERE `Id`='11';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='BlackBoard' WHERE `Id`='13';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='TotalCard' WHERE `Id`='14';
UPDATE `Processor` SET `EMS6ProcessorNameMapping`='NuVision' WHERE `Id`='15';

-- Mapping Tables used for data ETL from EMS6 to EMS7

-- Mapping Tables used for INC Migration

CREATE TABLE `AuditMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6PaystationId` int(11) DEFAULT NULL,
  `EMS6CollectionTypeId` tinyint(4) DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EMS7POSCollectionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `AuditPOSCollectionMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` int(11) DEFAULT NULL,
  `CollectionTypeId` tinyint(4) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `CCFailLogMapping` (
  `EMS6Id` int(11) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `CardRetryTransactionMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` mediumint(9) DEFAULT NULL,
  `PurchasedDate` datetime DEFAULT NULL,
  `TicketNumber` int(11) DEFAULT NULL,
  `EMS7CardRetryTransactionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_CardRetryTransactionMapping` (`PaystationId`,`PurchasedDate`,`TicketNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `CouponMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6CouponId` varchar(20) DEFAULT NULL,
  `EMS7CouponId` int(11) DEFAULT NULL,
  `NumUses` varchar(16) DEFAULT NULL,
  `EMS6CustomerId` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_CouponMapping_EMS6CustomerId` (`EMS6CustomerId`),
  KEY `ind_CouponMapping_EMS6CouponId` (`EMS6CouponId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `CryptoKeyMapping` (
  `Type` int(11) NOT NULL DEFAULT '0',
  `KeyIndex` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` int(11) DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  PRIMARY KEY (`Type`,`KeyIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `CustomerAlertTypeMapping` (
  `EMS6Id` int(10) NOT NULL,
  `EMS7Id` int(10) NOT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `CustomerCardTypeMapping` (
  `EMS6Id` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` mediumint(9) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `CustomerMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS7CustomerId` mediumint(8) unsigned NOT NULL,
  `EMS6CustomerId` int(11) NOT NULL,
  `EMS7CustomerName` varchar(40) NOT NULL,
  `EMS6CustomerName` varchar(40) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_CustomerMapping_EMS6CustomerId` (`EMS6CustomerId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `CustomerWebServiceCalMapping` (
  `EMS6Id` varchar(45) NOT NULL DEFAULT '',
  `EMS7Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `CustomerWsTokenMapping` (
  `EMS6Id` int(10) unsigned NOT NULL DEFAULT '0',
  `EMS7Id` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `EMS7ExtensibleRateMapping` (
  `Id` int(11) NOT NULL,
  `EMS6RateId` bigint(20) DEFAULT NULL,
  `EMS7RateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `EMSParkingPermissionMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6ParkingPermission` int(11) DEFAULT NULL,
  `EMS7ParkingPermission` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `EMSRateMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6RateId` int(11) DEFAULT NULL,
  `EMS7RateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `EMSRateToUnifiedRateMapping` (
  `EMS6RateId` bigint(20) unsigned DEFAULT NULL,
  `EMS7RateId` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `EventLogNewMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` mediumint(9) DEFAULT NULL,
  `DeviceId` tinyint(4) DEFAULT NULL,
  `TypeId` int(11) DEFAULT NULL,
  `ActionId` int(11) DEFAULT NULL,
  `DateField` datetime DEFAULT NULL,
  `POSAlertId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ExtensiblePermitMapping` (
  `EMS6MobileNumber` varchar(45) NOT NULL DEFAULT '',
  `EMS7Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`EMS6MobileNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ExtensibleRateMapping` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6RateId` bigint(20) unsigned DEFAULT NULL,
  `EMS7RateId` mediumint(8) unsigned DEFAULT NULL,
  `EMS6RateTypeId` varchar(30) NOT NULL DEFAULT '',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `MerchantAccountMapping` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6MerchantAccountId` int(11) unsigned DEFAULT NULL,
  `EMS7MerchantAccountId` mediumint(8) unsigned DEFAULT NULL,
  `ProcessorName` varchar(30) NOT NULL DEFAULT '',
  `EMS6CustomerId` int(11) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_MerchantAccountMapping_EMS6MerchantAccountId` (`EMS6MerchantAccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `ModemSettingMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6Id` int(11) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ParkingPermissionMapping` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6ParkingPermissionId` int(10) unsigned DEFAULT NULL,
  `EMS7ParkingPemissionId` int(10) unsigned DEFAULT NULL,
  `Name` varchar(16) NOT NULL DEFAULT '',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `PaystationGroupRouteMapping` (
  `PaystationGroupId` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` mediumint(8) DEFAULT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaystationGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `PaystationMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS7PaystationId` mediumint(8) unsigned NOT NULL,
  `EMS6PaystationId` mediumint(8) unsigned DEFAULT NULL,
  `EMS6CustomerId` mediumint(8) unsigned DEFAULT NULL,
  `SerialNumber` varchar(20) NOT NULL,
  `Name` varchar(40) NOT NULL DEFAULT '',
  `Version` int(10) unsigned DEFAULT NULL,
  `ProvisionDate` datetime DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RegionId` mediumint(8) unsigned DEFAULT NULL,
  `LotSettingId` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_PaystationMapping_EMS6PaystationId` (`EMS6PaystationId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `PreAuthHoldingMapping` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6MerchantAccountId` int(11) unsigned DEFAULT NULL,
  `EMS7MerchantAccountId` mediumint(8) unsigned DEFAULT NULL,
  `AuthorizationNumber` varchar(30) NOT NULL DEFAULT '',
  `ProcessorTransactionId` varchar(30) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `PreAuthMapping` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6MerchantAccountId` int(11) unsigned DEFAULT NULL,
  `EMS7MerchantAccountId` mediumint(8) unsigned DEFAULT NULL,
  `AuthorizationNumber` varchar(30) NOT NULL DEFAULT '',
  `ProcessorTransactionId` varchar(30) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_PreAuthMapping_EMS6MerchantAccountId` (`EMS6MerchantAccountId`),
  KEY `ind_PreAuthMapping_EMS7MerchantAccountId` (`EMS7MerchantAccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `ProcessorPropertiesMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Processor` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ProcessorTransactionMapping` (
  `PaystationId` int(11) NOT NULL,
  `PurchasedDate` datetime NOT NULL,
  `TicketNumber` int(11) NOT NULL,
  `Approved` tinyint(4) NOT NULL,
  `TypeId` tinyint(4) NOT NULL,
  `ProcessingDate` datetime NOT NULL,
  `EMS7ProcessorTransactionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`,`Approved`,`TypeId`,`ProcessingDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `PurchaseTaxMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6Id` int(11) DEFAULT NULL,
  `EMS7Id` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `RateMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerId` int(11) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `EMS7UnifiedRateId` int(11) DEFAULT NULL,
  `PaystationId` int(11) DEFAULT NULL,
  `TicketNumber` int(11) DEFAULT NULL,
  `PurchasedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `RateToUnifiedRateMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` int(11) DEFAULT NULL,
  `PurchasedDate` datetime DEFAULT NULL,
  `TicketNumber` int(11) DEFAULT NULL,
  `EMS7UnifiedRateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RawSensorDataMapping` (
  `Id` int(11) NOT NULL,
  `ServerName` varchar(20) DEFAULT NULL,
  `PaystationCommAddress` varchar(20) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Action` varchar(45) DEFAULT NULL,
  `Information` varchar(45) DEFAULT NULL,
  `DateField` datetime DEFAULT NULL,
  `EMS7RawSensorDataId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `RawSensorDataServerMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6ServerName` varchar(20) DEFAULT NULL,
  `EMS7ServerName` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `idx_RawSensorDataServerMapping_uq` (`EMS6ServerName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RegionLocationMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS7LocationId` mediumint(8) unsigned NOT NULL,
  `EMS6RegionId` mediumint(8) unsigned DEFAULT NULL,
  `Name` varchar(255) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_RegionLocationMapping_EMS6RegionId` (`EMS6RegionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `RegionPaystationLogMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6RegionPaystationId` mediumint(9) DEFAULT NULL,
  `EMS7LocationPOSLogId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ReplenishMapping` (
  `PaystationId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Number` int(10) unsigned NOT NULL DEFAULT '0',
  `EMS7Id` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`PaystationId`,`Date`,`Number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `RestAccountMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6AccountName` varchar(45) DEFAULT NULL,
  `EMS7RestAccountId` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RestLogMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RESTAccountName` varchar(45) DEFAULT NULL,
  `EndpointName` varchar(45) DEFAULT NULL,
  `LoggingDate` varchar(45) DEFAULT NULL,
  `IsError` varchar(45) DEFAULT NULL,
  `CustomerId` varchar(45) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RestLogTotalCallMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RESTAccountName` varchar(45) DEFAULT NULL,
  `LoggingDate` datetime DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RestSessionTokenMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EMS6AccountName` varchar(45) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ReversalArchiveMapping` (
  `Id` int(11) NOT NULL,
  `MerchantAccountId` int(11) DEFAULT NULL,
  `OriginalReferenceNumber` varchar(30) DEFAULT NULL,
  `OriginalTime` varchar(30) DEFAULT NULL,
  `EMS7ReversalArchiveId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ReversalMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantAccountId` mediumint(9) DEFAULT NULL,
  `OriginalReferenceNumber` varchar(30) DEFAULT NULL,
  `OriginalTime` varchar(30) DEFAULT NULL,
  `EMS7Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `RoutePOSMapping` (
  `EMS7Id` mediumint(8) DEFAULT NULL,
  `PaystationGroupId` mediumint(8) NOT NULL,
  `PaystationId` mediumint(8) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaystationGroupId`,`PaystationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `SMSAlertMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MobileNumber` varchar(45) DEFAULT NULL,
  `EMS7Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `SMSFailedResponseMapping` (
  `PaystationId` int(11) NOT NULL DEFAULT '0',
  `PurchasedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TicketNumber` int(11) NOT NULL DEFAULT '0',
  `SMSMessageTypeId` int(11) NOT NULL DEFAULT '0',
  `EMS7SMSFailedResponseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`,`SMSMessageTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `SMSTransactionLogMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` varchar(45) DEFAULT NULL,
  `PurchasedDate` varchar(45) DEFAULT NULL,
  `TicketNumber` varchar(45) DEFAULT NULL,
  `SMSMessageTypeId` varchar(45) DEFAULT NULL,
  `EMS7TransactionLogId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ServiceAgreementMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6ServiceAgreementId` int(11) NOT NULL,
  `EMS7ServiceAgreementId` mediumint(8) NOT NULL,
  `EMS6UploadedGMT` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `ServiceStateMapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` int(11) DEFAULT NULL,
  `EMS7POSServiceStateId` int(11) DEFAULT NULL,
  `MachineNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `SettingsFileContentMapping` (
  `EMS6Id` int(11) NOT NULL,
  `EMS7Id` mediumint(8) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `SettingsFileMapping` (
  `EMS6Id` int(11) NOT NULL,
  `EMS7Id` mediumint(8) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `ThirdPartyServiceAccountMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6ThirdPartyServiceAccountId` int(11) NOT NULL,
  `EMS7ThirdPartyServiceAccountId` mediumint(8) NOT NULL,
  `EMS6UserName` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `Transaction2PushMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` int(11) DEFAULT NULL,
  `PurchasedDate` datetime DEFAULT NULL,
  `TicketNumber` int(11) DEFAULT NULL,
  `ThirdPartyServiceAccountId` int(11) DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TransactionMapping` (
  `Id` int(11) NOT NULL,
  `PaystationId` int(11) DEFAULT NULL,
  `PurchasedDate` datetime DEFAULT NULL,
  `TicketNumber` int(11) DEFAULT NULL,
  `EMS7PurchaseId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TransactionPushMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PaystationId` varchar(45) DEFAULT NULL,
  `PurchasedDate` varchar(45) DEFAULT NULL,
  `TicketNumber` varchar(45) DEFAULT NULL,
  `EMS7Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `UserAccountMapping` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `EMS6UseraccountId` int(11) NOT NULL,
  `EMS7UserAccountId` mediumint(8) NOT NULL,
  `EMS6Name` varchar(40) DEFAULT NULL,
  `EMS6CustomerId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `WsCallLogMapping` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(45) DEFAULT NULL,
  `CallDate` datetime DEFAULT NULL,
  `EMS7Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE  TABLE IF NOT EXISTS `DiscardedRecords` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `MultiKeyId` VARCHAR(255) NULL DEFAULT '0' ,
  `EMS6Id` BIGINT NULL DEFAULT '0' ,
  `TableName` VARCHAR(25) NOT NULL ,
  `Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


	
*/

