SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `PreviewRequest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewRequest` ;

CREATE  TABLE IF NOT EXISTS `PreviewRequest` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DayOfWeek` TinyInt UNSIGNED NOT NULL ,
  `MinuteOfDay` MediumInt UNSIGNED NOT NULL ,
  `PaystationId` VARCHAR(4) NOT NULL ,
  `RequestTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `RequestId` Int UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_requestdate` (`DayOfWeek` ASC, `MinuteOfDay` ASC, `PaystationId` ASC) ,
  INDEX `idx_paystation` (`PaystationId` ASC) ,
  /*CONSTRAINT `fk_paystation_id`
    FOREIGN KEY (`PaystationId` )
    REFERENCES `PreviewPaystation` (`PaystationId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,*/
  CONSTRAINT `fk_requesttype_id`
    FOREIGN KEY (`RequestTypeId` )
    REFERENCES `PreviewRequestType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PreviewPaystation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewPaystation` ;

CREATE  TABLE IF NOT EXISTS `PreviewPaystation` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `SerialNumber` VARCHAR(20) NOT NULL ,
  `CustomerId` MEDIUMINT NOT NULL ,
  `PaystationId` VARCHAR(4) NOT NULL ,
  `TimeZone` VARCHAR(20) NOT NULL ,
  `MessageNumber` MEDIUMINT DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_paystation_serialnumber` (`SerialNumber` ASC) ,
  INDEX `idx_paystation` (`PaystationId` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PreviewRequestType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewRequestType` ;

CREATE  TABLE IF NOT EXISTS `PreviewRequestType` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(32) NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;

INSERT PreviewRequestType (Id, Name)
VALUES 	(1, 'Transaction'),
		(2, 'Event'),
		(3, 'Collection');

-- -----------------------------------------------------
-- Table `PreviewEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewEvent` ;

CREATE  TABLE IF NOT EXISTS `PreviewEvent` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Type` VARCHAR(40) NOT NULL ,
  `Action` VARCHAR(40) NOT NULL ,
  `Information` VARCHAR(40) NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PreviewTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewTransaction` ;

CREATE  TABLE IF NOT EXISTS `PreviewTransaction` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  -- `PurchasedDate`
  -- `ExpiryDate`
  -- `PaystationCommAddress`
  `Number` INT UNSIGNED NOT NULL ,
  `LotNumber` VARCHAR(20) NOT NULL , 
  `LicensePlateNo` VARCHAR(15) ,
  `AddTimeNum` INT UNSIGNED NOT NULL ,
  `StallNumber` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0 ,
  `Type` VARCHAR(30) NOT NULL ,
  `ParkingTimePurchased` MEDIUMINT ,
  `OriginalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CashPaid` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardPaid` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `NumberBillsAccepted` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `NumberCoinsAccepted` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCol` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCol` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardData` VARCHAR(100) DEFAULT NULL,
  `IsRefundSlip` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `RateName` varchar(20) NOT NULL ,
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateValue` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PreviewCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewCollection` ;

CREATE  TABLE IF NOT EXISTS `PreviewCollection` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Type` VARCHAR(20), -- Coin/Bill
  `LotNumber` VARCHAR(20),
  `MachineNumber` VARCHAR(4),
  `StartDate`  MEDIUMINT UNSIGNED NOT NULL ,
  `EndDate`  MEDIUMINT UNSIGNED NOT NULL ,  
  `StartTransNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `EndTransNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,  
  `TicketCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount005` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount010` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount025` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount100` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount200` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount01` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount02` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount05` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount10` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount20` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount50` SMALLINT UNSIGNED NOT NULL DEFAULT 0 , 
  PRIMARY KEY (`Id`) ) 
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `PreviewDataInjection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewDataInjection` ;
CREATE  TABLE IF NOT EXISTS PreviewDataInjection (
	Id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
	Name varchar(40) NOT NULL,
	Value varchar(255) NOT NULL,
	LastModifiedGMT datetime NOT NULL,
PRIMARY KEY (Id));

-- insert into PreviewDataInjection values (1, 'LastRequestSent', UTC_TIMESTAMP, UTC_TIMESTAMP);
insert into PreviewDataInjection values (1, 'LastRequestSent', date_sub(DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i') ,interval 0 day), UTC_TIMESTAMP);


-- -----------------------------------------------------
-- Table `PreviewCardDataInjection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreviewCardDataInjection` ;
CREATE  TABLE IF NOT EXISTS PreviewCardDataInjection (
	Id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
	Name varchar(40) NOT NULL,
	Value datetime NOT NULL,
	LastModifiedGMT datetime NOT NULL,	
PRIMARY KEY (Id));

insert into PreviewCardDataInjection 
values (1, 'LastCardRequestSent', date_sub(DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i') ,interval 8 day), DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i'));


-- -----------------------------------------------------
-- Table `PreviewCardTransaction`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PreviewCardTransaction`;

CREATE TABLE `PreviewCardTransaction` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,  
  `Number` int(10) unsigned NOT NULL,  -- PurchaseNumber
  `LotNumber` varchar(20) NOT NULL,    -- LocationID
  `TransactionTypeId` tinyint(3) unsigned,
  `ProcessorTransactionTypeId` tinyint(3) unsigned,
  `PaymentTypeId` tinyint(3) unsigned,
  `LicensePlateNo` varchar(15) DEFAULT NULL,
  `AddTimeNum` int(10) unsigned NOT NULL,
  `StallNumber` mediumint(8) unsigned NOT NULL DEFAULT '0',  
  `ParkingTimePurchased` mediumint(9) DEFAULT NULL,
  `OriginalAmount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `CardPaid` mediumint(8) unsigned NOT NULL DEFAULT '0',  
  `RateName` varchar(20) NOT NULL , 
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;