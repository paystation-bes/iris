

DROP PROCEDURE IF EXISTS sp_AddNewTabToUserAccounts;

delimiter //

CREATE PROCEDURE sp_AddNewTabToUserAccounts(in P_TabName varchar(20)) 


 BEGIN

  declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_UserAccountId MEDIUMINT UNSIGNED;


declare c1 cursor  for
select distinct UserAccountId from Widget where UserAccountId not in (
select  UserAccountId from Tab where 
name = P_TabName
group by UserAccountId
);

declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_UserAccountId;

	CALL sp_AddUserAccountWidgetTab(lv_UserAccountId,P_TabName);
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  
END//
delimiter ;


DROP PROCEDURE IF EXISTS sp_AddUserAccountWidgetTab;

delimiter //

CREATE PROCEDURE sp_AddUserAccountWidgetTab(in P_UserAccountId MEDIUMINT UNSIGNED, in P_TabName varchar(20)) 


 BEGIN
 DECLARE lv_TabId, lv_SectionId INT UNSIGNED;
 DECLARE lv_CustomerId MEDIUMINT UNSIGNED ;
 DECLARE lv_LastPosition TINYINT Unsigned ;
 
 SELECT CustomerId into lv_CustomerId FROM UserAccount where Id = P_UserAccountId;
 SELECT MAX(OrderNumber) into lv_LastPosition FROM Tab where UserAccountId =  P_UserAccountId;
 
INSERT INTO `Tab` (`UserAccountId`, `Name`, `OrderNumber`, `VERSION`, `LastModifiedGMT`) VALUES (P_UserAccountId, P_TabName, lv_LastPosition +1, '0', '2014-04-15 00:00:00');

SET lv_TabId = LAST_INSERT_ID();

INSERT INTO Section (UserAccountId, TabId, LayoutId, OrderNumber, Version, LastModifiedGMT) VALUES
(P_UserAccountId, lv_TabId,3,1,1,UTC_TIMESTAMP());

SET lv_SectionId = LAST_INSERT_ID();


-- SELECT Id into lv_SectionId from Section where UserAccountId = P_UserAccountId and TabId = ( select Id from Tab where UserAccountId = P_UserAccountId and Name = 'Parker Metrics');

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
13,6,3,105,
0,0,3,0,0,1,'Avg Revenue by Day','Average revenue per space for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
12,5,3,105,
0,0,3,0,1,1,'Avg Price by Day','Average price per permit for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());


INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
14,5,3,0,
0,0,3,0,2,1,'Avg Duration by Day','Average duration per permit for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

END//
delimiter ;


--
DROP PROCEDURE IF EXISTS sp_AddNewDefaultDashboardTab;

delimiter //

CREATE PROCEDURE sp_AddNewDefaultDashboardTab( in P_TabName varchar(20)) 

BEGIN
 
DECLARE lv_TabId, lv_SectionId INT UNSIGNED;
Declare lv_record_cnt TINYINT UNSIGNED ;

-- check if already record exists in Tab

select count(*) into lv_record_cnt FROM Tab where UserAccountId = 1 and Name = P_TabName ;

if (lv_record_cnt = 0) THEN

INSERT INTO `Tab` (`UserAccountId`, `Name`, `OrderNumber`, `VERSION`, `LastModifiedGMT`) VALUES 
(1, P_TabName, 4, '0', UTC_TIMESTAMP());

SET lv_TabId = LAST_INSERT_ID();

INSERT INTO Section (UserAccountId, TabId, LayoutId, OrderNumber, Version, LastModifiedGMT) VALUES
(1, lv_TabId,3,1,1,UTC_TIMESTAMP());

SET lv_SectionId = LAST_INSERT_ID();

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
13,6,3,105,
0,0,3,0,0,1,'Avg Revenue/Space by Day','Average revenue per space for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
12,5,3,105,
0,0,3,0,1,1,'Avg Price/Permit by Day','Average price per permit for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());


INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
14,5,3,0,
0,0,3,0,2,1,'Avg Duration/Permit by Day','Average duration per permit for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

select 'Completed' AS INFO;

ELSE

select 'This Procedure has been already executed before' AS INFO;

END IF ;


END//
delimiter ;
