-- This is only for QA3, So that QA3 can use Production's signing server to make public key signature valid for Pay Station.
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EnableSigningProxy',              '0',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TransactionArchiveYMDH',          '2.0.30.0',                             UTC_TIMESTAMP(), 1) ;


ALTER TABLE Coupon MODIFY COLUMN `SpaceRange` VARCHAR(255);