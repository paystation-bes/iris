
DROP PROCEDURE IF EXISTS sp_MigrateSettledTransactionIntoKPI ;

delimiter //

CREATE  PROCEDURE sp_MigrateSettledTransactionIntoKPI(IN P_ETLProcessDateRangeId INT, IN P_ClusterId INT, IN P_BeginDateGMT datetime, IN P_EndDateGMT datetime)
BEGIN

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;
DECLARE lv_IsRefund TINYINT ; 


-- CURSOR VARIABLES
DECLARE v_ProcessorTxn_Id BIGINT ;
DECLARE v_ProcessorTxn_PointOfSaleId MEDIUMINT ; 
DECLARE v_ProcessorTxn_CustomerID MEDIUMINT ;
DECLARE v_ProcessorTxn_PurchaseId BIGINT ; 
DECLARE v_ProcessorTxn_ProcessorTransactionTypeId  TINYINT ;
DECLARE v_ProcessorTxn_MerchantAccountId MEDIUMINT ; 
DECLARE v_ProcessorTxn_Amount MEDIUMINT ;
DECLARE v_ProcessorTxn_ProcessingDate DATETIME ;
DECLARE v_ProcessorTxn_PropertyValue VARCHAR(40);		

DECLARE C1 CURSOR FOR
	
		SELECT
			T.Id ,
			T.PointOfSaleId , 
			B.CustomerID,
			T.PurchaseId ,
			T.ProcessorTransactionTypeId , 
			T.MerchantAccountId , 
			T.Amount ,
			T.ProcessingDate ,
			P.PropertyValue
		FROM 
			ProcessorTransaction T, 
			PointOfSale B, 
			CustomerProperty P
		WHERE
			T.PointOfSaleId = B.Id AND 
			B.CustomerId = P.CustomerId AND
			P.CustomerPropertyTypeId = 1 AND 
			P.PropertyValue IS NOT NULL AND
			T.ProcessingDate BETWEEN P_BeginDateGMT and  P_EndDateGMT ;
			-- Check Performance here
				
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO SettledTxnNotProcessed ( ProcessorTransactionId , PointOfSaleId , 				ProcessorTransactionTypeId , 				ProcessingDate , 				RecordInsertTime) VALUES
										 ( v_ProcessorTxn_Id,		v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_ProcessingDate, 	UTC_TIMESTAMP()	 );
	COMMIT ;
  END;
 

 -- SET lv_Customer_Timezone = null;
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 

UPDATE ETLExecutionLog
SET SettledAmountRecordCount  = idmaster_pos,
SettledAmountCursorOpenedTime = NOW()
WHERE Id = P_ETLProcessDateRangeId;

-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
set autocommit = 0 ;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_ProcessorTxn_Id ,
					v_ProcessorTxn_PointOfSaleId , 
					v_ProcessorTxn_CustomerID,
					v_ProcessorTxn_PurchaseId ,
					v_ProcessorTxn_ProcessorTransactionTypeId , 
					v_ProcessorTxn_MerchantAccountId , 
					v_ProcessorTxn_Amount ,
					v_ProcessorTxn_ProcessingDate ,
					v_ProcessorTxn_PropertyValue ;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				SET lv_TimeIdLocal = NULL ;
				SET lv_IsRefund = NULL ;
				
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) ;
				
				IF v_ProcessorTxn_ProcessorTransactionTypeId in (2,3, 6, 7, 10, 11, 17) THEN
					SET lv_IsRefund = 0 ;
				ELSEIF v_ProcessorTxn_ProcessorTransactionTypeId in ( 4, 12, 13, 18, 19) THEN
					SET lv_IsRefund = 1 ;
				ELSE
					SET lv_IsRefund = 2 ;
				END IF;
				
				
				INSERT INTO SettledTransaction (ProcessorTransactionId, CustomerId, 				PurchaseId, 				TimeIdLocal, 	ProcessorTransactionTypeId , 				MerchantAccountId, 					Amount, 				IsRefund)
										VALUES (v_ProcessorTxn_Id,		v_ProcessorTxn_CustomerID,	v_ProcessorTxn_PurchaseId,	lv_TimeIdLocal,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_MerchantAccountId,	v_ProcessorTxn_Amount,	lv_IsRefund) ;
	
			-- DELETE FROM POSCollectionStagging WHERE ID = v_Collection_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		IF mod(indexm_pos,3000) = 0 THEN
			COMMIT ;
		END IF ;
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;

 COMMIT ;		-- ASHOK2
set autocommit = 1 ; -- Auto Commit ON
        
END//

delimiter ;

