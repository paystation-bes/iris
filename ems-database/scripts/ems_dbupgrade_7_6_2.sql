-- -----------------------------------------------------
-- Lookup Table Processor
-- -----------------------------------------------------

-- These are only for QA & Dev
-- Changing Heartland TestUrl 
UPDATE `Processor` SET `TestUrl` = 'https://test.txns.secureexchange.net:15031' WHERE `Id` = 3;

-- Changing Heartland SP+ TestUrl
UPDATE `Processor` SET `TestUrl` = 'https://test.txns.secureexchange.net:15031' WHERE `Id` = 22;

-- -----------------------------------------------------
-- EMS Properties
-- -----------------------------------------------------

-- These are only for Dev
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId)
VALUES ('DevelopmentMode', '1', UTC_TIMESTAMP(), 1) ;
