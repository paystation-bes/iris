SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `ReportQueue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportQueue` ;

CREATE  TABLE IF NOT EXISTS `ReportQueue` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `PrimaryDateRangeBeginGMT` DATETIME NULL ,
  `PrimaryDateRangeEndGMT` DATETIME NULL ,
  `SecondaryDateRangeBeginGMT` DATETIME NULL ,
  `SecondaryDateRangeEndGMT` DATETIME NULL ,
  `ScheduledBeginGMT` DATETIME NOT NULL ,
  `QueuedGMT` DATETIME NOT NULL ,
  `ExecutionBeginGMT` DATETIME NULL ,
  `LockId` VARCHAR(20) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_reportqueue_reportstatustype` (`ReportStatusTypeId` ASC) ,
  INDEX `idx_reportqueue_customer` (`CustomerId` ASC) ,
  INDEX `idx_reportqueue_useraccount` (`UserAccountId` ASC) ,
  UNIQUE INDEX `idx_reportqueue_reportdefinition` (`ReportDefinitionId` ASC) ,
  INDEX `idx_reportqueue_scheduled` (`ScheduledBeginGMT` ASC) ,
  CONSTRAINT `fk_reportqueue_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_reportdefinition`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ReportDefinition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportDefinition` ;

CREATE  TABLE IF NOT EXISTS `ReportDefinition` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `ReportRepeatTimeOfDay` CHAR(5) NULL ,
  `ReportRepeatDays` VARCHAR(83) NULL ,
  `ReportRepeatFrequency` TINYINT UNSIGNED NULL ,
  `Title` VARCHAR(80) NULL ,
  `ReportTypeId` SMALLINT UNSIGNED NOT NULL ,
  `IsDetailsOrSummary` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsDetailsOrSummary: 0=Details,1=Summary' ,
  `PrimaryDateFilterTypeId` MEDIUMINT UNSIGNED NULL COMMENT 'Transaction Date, Procesing Date, Retry Date or Date depending on report type' ,
  `PrimaryDateRangeBeginGMT` DATETIME NULL ,
  `PrimaryDateRangeEndGMT` DATETIME NULL ,
  `SecondaryDateFilterTypeId` MEDIUMINT UNSIGNED NULL COMMENT 'Transaction Date for report requiring 2 sets of dates' ,
  `SecondaryDateRangeBeginGMT` DATETIME NULL ,
  `SecondaryDateRangeEndGMT` DATETIME NULL ,
  `PaystationSettingIdFilter` MEDIUMINT UNSIGNED NULL ,
  `SpaceNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `SpaceNumberBeginFilter` MEDIUMINT UNSIGNED NULL,
  `SpaceNumberEndFilter`MEDIUMINT UNSIGNED NULL,
  `LocationFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardNumberSpecificFilter` VARCHAR(40) NULL,
  `CardTypeId` MEDIUMINT UNSIGNED NULL ,
  `ApprovalStatusFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardMerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `LicencePlateFilter` VARCHAR(40) NULL ,
  `TicketNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `TicketNumberBeginFilter` int(10) UNSIGNED NULL,
  `TicketNumberEndFilter` int(10) UNSIGNED NULL,
  `CouponNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CouponNumberSpecificFilter` VARCHAR(40) NULL ,
  `OtherParametersTypeFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `GroupByFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `SortByFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `IsOnlyVisible` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsOnlyVisible: 0=Show also hidden, 1=Show only visible' ,
  `IsCsvOrPdf` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsCsvOrPdf: 0=Csv, 1=Pdf' ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `ScheduledToRunGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idk_reportdefinition_reporttype` (`ReportTypeId` ASC) ,
  INDEX `idk_reportdefinition_useraccount` (`UserAccountId` ASC) ,
  INDEX `idk_reportdefinition_reportrepeattype` (`ReportRepeatTypeId` ASC) ,
  INDEX `idk_reportdefinition_reportstatustype` (`ReportStatusTypeId` ASC) ,
  CONSTRAINT `fk_reportdefinition_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reporttype`
    FOREIGN KEY (`ReportTypeId` )
    REFERENCES `ReportType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportHistory` ;

CREATE  TABLE IF NOT EXISTS `ReportHistory` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportDateRangeBeginGMT` DATETIME NULL ,
  `ReportDateRangeEndGMT` DATETIME NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `ScheduledBeginGMT` DATETIME NOT NULL ,
  `QueuedGMT` DATETIME NOT NULL ,
  `ExecutionBeginGMT` DATETIME NOT NULL ,
  `ExecutionEndGMT` DATETIME NOT NULL COMMENT 'ExecutionEndGMT: when the execution of a report ends. ReportStatusTypeId will specify the status of the report when it is done execution: (3) Completed, (4) Canceled, (5) Failed.' ,
  `SqlBeginGMT` DATETIME NULL ,
  `SqlEndGMT` DATETIME NULL COMMENT 'SqlEndGMT: when the execution of a sql ends.' ,
  `FileSizeKb` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `StatusExplanation` VARCHAR(100) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_reporthistory_customer` (`CustomerId` ASC) ,
  INDEX `fk_reporthistory_useraccount` (`UserAccountId` ASC) ,
  INDEX `fk_reporthistory_reportdefinition` (`ReportDefinitionId` ASC) ,
  INDEX `fk_reporthistory_reportstatustype` (`ReportStatusTypeId` ASC) ,
  CONSTRAINT `fk_reporthistory_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportdefinition`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ReportRepository`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportRepository` ;

CREATE  TABLE IF NOT EXISTS `ReportRepository` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportHistoryId` INT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `AddedGMT` DATETIME NOT NULL ,
  `FileSizeKb` SMALLINT UNSIGNED NOT NULL ,
  `FileName` VARCHAR(60) NOT NULL ,
  `IsReadByUser` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsSoonToBeDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_reportrepository_useraccount` (`UserAccountId` ASC) ,
  INDEX `fk_reportrepository_reporthistory` (`ReportHistoryId` ASC) ,
  CONSTRAINT `fk_reportrepository_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportrepository_reporthistory`
    FOREIGN KEY (`ReportHistoryId` )
    REFERENCES `ReportHistory` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ReportContent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportContent` ;

CREATE  TABLE IF NOT EXISTS `ReportContent` (
  `ReportRepositoryId` INT UNSIGNED NOT NULL ,
  `Content` MEDIUMBLOB NOT NULL ,
  INDEX `fk_reportcontent_reportrepository` (`ReportRepositoryId` ASC) ,
  PRIMARY KEY (`ReportRepositoryId`) ,
  CONSTRAINT `fk_reportcontent_reportrepository`
    FOREIGN KEY (`ReportRepositoryId` )
    REFERENCES `ReportRepository` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ReportLocationValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportLocationValue` ;

CREATE  TABLE IF NOT EXISTS `ReportLocationValue` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ObjectId` MEDIUMINT UNSIGNED NOT NULL ,
  `ObjectType` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_reportlocationvalue_reportdefinitiontype`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;


INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('1', '3', '3', '16', '1', '0', '1', 'Paystation Summary Online', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1'); 
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('2', '3', '3', '16', '2', '1:00', '0', '1', 'Paystation Summary Daily', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('3', '3', '3', '16', '3', '4:00', '1,3,5', '2', '0', '1', 'Paystation Summary Weekly', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('4', '3', '3', '16', '4', '23:00', '15,30', '1', '0', '1', 'Paystation Summary Monthly', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('5', '3', '3', '12', '4', '12:00', '15,30', '1', '0', '1', 'Stall Report Setting', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('6', '3', '3', '12', '3', '4:00', '1,3,5', '1', '0', '1', 'Stall Report Location ', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('7', '3', '3', '12', '4', '12:00', '15,30', '1', '0', '1', 'Stall Report SpaceNumber Valid', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('8', '3', '3', '12', '4', '12:00', '15,30', '1', '0', '1', 'Stall Report SpaceNumber Expired', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('9', '3', '3', '1', '4', '12:00', '15,30', '1', '0', '1', 'Transaction All', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('10', '3', '3', '1', '4', '12:00', '15,30', '1', '0', '1', 'Transaction All Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('11', '3', '3', '2', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Cash', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('12', '3', '3', '2', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Cash Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('13', '3', '3', '3', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Credit Card', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('14', '3', '3', '3', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Credit Card Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('15', '3', '3', '4', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Refund', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('16', '3', '3', '4', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Refund Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('17', '3', '3', '5', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Processing', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('18', '3', '3', '5', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Processing Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('19', '3', '3', '6', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Retry', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('20', '3', '3', '6', '4', '12:00', '15,30', '1', '0', '1', 'Transaction CC Retry Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('21', '3', '3', '7', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Patroller Card', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('22', '3', '3', '7', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Patroller Card Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('23', '3', '3', '10', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Smart Card', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('24', '3', '3', '10', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Smart Card Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('25', '3', '3', '11', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Value Card', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('26', '3', '3', '11', '4', '12:00', '15,30', '1', '0', '1', 'Transaction Value Card Summary', '1', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('27', '3', '3', '8', '4', '12:00', '15,30', '1', '0', '1', 'Rate All', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('28', '3', '3', '8', '4', '12:00', '15,30', '1', '0', '1', 'Rate Setting', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('29', '3', '3', '8', '4', '12:00', '15,30', '1', '0', '1', 'Rate Pay Station', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('30', '3', '3', '9', '4', '12:00', '15,30', '1', '0', '1', 'Rate Summary All', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('31', '3', '3', '9', '4', '12:00', '15,30', '1', '0', '1', 'Rate Summary Regular', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('32', '3', '3', '13', '4', '12:00', '15,30', '1', '0', '1', 'Coupon Usage Summary All', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('33', '3', '3', '13', '4', '12:00', '15,30', '1', '0', '1', 'Coupon Usage Summary Specific', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('34', '3', '3', '17', '4', '12:00', '15,30', '1', '0', '1', 'Tax All', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('35', '3', '3', '17', '4', '12:00', '15,30', '1', '0', '1', 'Tax Day', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('36', '3', '3', '17', '4', '12:00', '15,30', '1', '0', '1', 'Tax Pay Station', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('37', '3', '3', '17', '4', '12:00', '15,30', '1', '0', '1', 'Tax Tax Name', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('38', '3', '3', '14', '4', '12:00', '15,30', '1', '0', '1', 'Audit All PDF', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('39', '3', '3', '14', '4', '12:00', '15,30', '1', '0', '0', 'Audit Setting CSV', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');

INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('40', '3', '3', '15', '4', '12:00', '15,30', '1', '0', '1', 'Replenish All PDF', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');
INSERT INTO `ems_db_7`.`ReportDefinition` (`Id`, `CustomerId`, `UserAccountId`, `ReportTypeId`, `ReportRepeatTypeId`, `ReportRepeatTimeOfDay`, `ReportRepeatDays`, `ReportRepeatFrequency`, `IsOnlyVisible`, `IsCsvOrPdf`, `Title`, `IsDetailsOrSummary`, `ReportStatusTypeId`, `ScheduledToRunGMT`, `VERSION`, `CreatedGMT`, `LastModifiedGMT`, `LastModifiedByUserId`) 
  VALUES ('41', '3', '3', '15', '4', '12:00', '15,30', '1', '0', '0', 'Replenish Machine CSV', '0', '1', UTC_TIMESTAMP(), '0', UTC_TIMESTAMP(), UTC_TIMESTAMP(), '1');




UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='301', `SortByFilterTypeId`='1400' WHERE `Id`='1';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='301', `SortByFilterTypeId`='1401' WHERE `Id`='2';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='304', `SortByFilterTypeId`='1402' WHERE `Id`='3';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='304', `SortByFilterTypeId`='1403' WHERE `Id`='4';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='303', `SpaceNumberFilterTypeId`='402', `OtherParametersTypeFilterTypeId`='1100', `PaystationSettingIdFilter`='1' WHERE `Id`='5';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='304', `SpaceNumberFilterTypeId`='402', `OtherParametersTypeFilterTypeId`='1100', `PaystationSettingIdFilter`='11' WHERE `Id`='6';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='304', `SpaceNumberFilterTypeId`='406', `SpaceNumberBeginFilter`='70', `SpaceNumberEndFilter`='100', `OtherParametersTypeFilterTypeId`='1101', `PaystationSettingIdFilter`='11' WHERE `Id`='7';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='304', `SpaceNumberFilterTypeId`='406', `SpaceNumberBeginFilter`='70', `SpaceNumberEndFilter`='100', `OtherParametersTypeFilterTypeId`='1102', `PaystationSettingIdFilter`='11' WHERE `Id`='8';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '100', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='9';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='10';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '102', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='11';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '103', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='12';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `ApprovalStatusFilterTypeId` = '600', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '104', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='13';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `ApprovalStatusFilterTypeId` = '600', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '105', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='14';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '106', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='15';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '107', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='16';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '108', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP(), `SecondaryDateFilterTypeId` = '101', `SecondaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 2 MONTH)), `SecondaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='17';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '109', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP(), `SecondaryDateFilterTypeId` = '101', `SecondaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 2 MONTH)), `SecondaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='18';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '110', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP(), `SecondaryDateFilterTypeId` = '101', `SecondaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 2 MONTH)), `SecondaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='19';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '500', `CardTypeId` = '0', `CardMerchantAccountId` = '0', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1010', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '111', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP(), `SecondaryDateFilterTypeId` = '101', `SecondaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 2 MONTH)), `SecondaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='20';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '112', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='21';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '113', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='22';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='23';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='24';

UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardTypeId` = '0', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `CardMerchantAccountId` = '0', `ApprovalStatusFilterTypeId` = '600', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='25';
UPDATE `ems_db_7`.`reportdefinition` SET `LocationFilterTypeId`='300', `PaystationSettingIdFilter`='0', `SpaceNumberFilterTypeId`='404', `CardNumberFilterTypeId` = '501', `CardTypeId` = '0', `CardNumberSpecificFilter` = '12345678', `CouponNumberFilterTypeId` = '900', `CardMerchantAccountId` = '0', `ApprovalStatusFilterTypeId` = '600', `SpaceNumberBeginFilter`='1', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1001', `TicketNumberFilterTypeId` = '700', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='26';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1300', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='27';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1303', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='28';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1305', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='29';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `OtherParametersTypeFilterTypeId` = '1000', `GroupByFilterTypeId`='1301', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='30';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `OtherParametersTypeFilterTypeId` = '1001', `GroupByFilterTypeId`='1301', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='31';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `CouponNumberFilterTypeId` = '901', `GroupByFilterTypeId`='1301', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='32';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `CouponNumberFilterTypeId` = '903', `GroupByFilterTypeId`='1301', `CouponNumberSpecificFilter` = '1234', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='33';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1300', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='34';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1301', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='35';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1305', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='36';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1312', `OtherParametersTypeFilterTypeId` = '1000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='37';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1300', `OtherParametersTypeFilterTypeId` = '1100', `TicketNumberFilterTypeId` = '800', `PrimaryDateFilterTypeId` = '101', `SecondaryDateFilterTypeId` = '100', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='38';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1303', `OtherParametersTypeFilterTypeId` = '1103', `TicketNumberFilterTypeId` = '804', `TicketNumberBeginFilter` = '1', `TicketNumberEndFilter` = '10000', `PrimaryDateFilterTypeId` = '101', `SecondaryDateFilterTypeId` = '100', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='39';

UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1300', `TicketNumberFilterTypeId` = '800', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='40';
UPDATE `ems_db_7`.`reportdefinition` SET `PaystationSettingIdFilter` = '0', `LocationFilterTypeId`='300', `GroupByFilterTypeId`='1311', `TicketNumberFilterTypeId` = '804', `TicketNumberBeginFilter` = '1', `TicketNumberEndFilter` = '10000', `PrimaryDateFilterTypeId` = '101', `PrimaryDateRangeBeginGMT` = (Date_Sub(UTC_TIMESTAMP(), INTERVAL 1 MONTH)), `PrimaryDateRangeEndGMT` = UTC_TIMESTAMP() WHERE `Id`='41';


INSERT INTO `ems_db_7`.`ReportLocationValue` (`Id`, `ReportDefinitionId`, `ObjectId`, `ObjectType`) VALUES 
('1', '3', '0', 'All Locations'), 
('2', '4', '6', 'com.digitalpaytech.domain.Location'), 
('3', '4', '10', 'com.digitalpaytech.domain.Location'), 
('4', '4', '5', 'com.digitalpaytech.domain.PointOfSale'), 
('5', '4', '6', 'com.digitalpaytech.domain.PointOfSale'), 
('6', '1', '0', 'All Routes'), 
('7', '2', '1', 'com.digitalpaytech.domain.Route'),
('8', '5', '1', 'com.digitalpaytech.domain.PaystationSetting'), 
('9', '6', '11', 'com.digitalpaytech.domain.Location'), 
('10', '7', '11', 'com.digitalpaytech.domain.Location'), 
('11', '8', '11', 'com.digitalpaytech.domain.Location'), 
('12', '9', '11', 'com.digitalpaytech.domain.Location'), 
('13', '10', '11', 'com.digitalpaytech.domain.Location'), 
('14', '11', '11', 'com.digitalpaytech.domain.Location'), 
('15', '12', '11', 'com.digitalpaytech.domain.Location'), 
('16', '13', '11', 'com.digitalpaytech.domain.Location'), 
('17', '14', '11', 'com.digitalpaytech.domain.Location'), 
('18', '15', '11', 'com.digitalpaytech.domain.Location'), 
('19', '16', '11', 'com.digitalpaytech.domain.Location'), 
('20', '17', '11', 'com.digitalpaytech.domain.Location'), 
('21', '18', '11', 'com.digitalpaytech.domain.Location'), 
('22', '19', '11', 'com.digitalpaytech.domain.Location'), 
('23', '20', '11', 'com.digitalpaytech.domain.Location'),
('24', '21', '11', 'com.digitalpaytech.domain.Location'), 
('25', '22', '11', 'com.digitalpaytech.domain.Location'), 
('26', '23', '11', 'com.digitalpaytech.domain.Location'), 
('27', '24', '11', 'com.digitalpaytech.domain.Location'), 
('28', '25', '11', 'com.digitalpaytech.domain.Location'), 
('29', '26', '11', 'com.digitalpaytech.domain.Location'), 
('30', '27', '11', 'com.digitalpaytech.domain.Location'), 
('31', '28', '11', 'com.digitalpaytech.domain.Location'), 
('32', '29', '11', 'com.digitalpaytech.domain.Location'), 
('33', '30', '11', 'com.digitalpaytech.domain.Location'), 
('34', '31', '11', 'com.digitalpaytech.domain.Location'), 
('35', '32', '11', 'com.digitalpaytech.domain.Location'), 
('36', '33', '11', 'com.digitalpaytech.domain.Location'),
('37', '34', '11', 'com.digitalpaytech.domain.Location'),
('38', '35', '11', 'com.digitalpaytech.domain.Location'),
('39', '36', '11', 'com.digitalpaytech.domain.Location'),
('40', '37', '11', 'com.digitalpaytech.domain.Location'),
('41', '38', '11', 'com.digitalpaytech.domain.Location'),
('42', '39', '11', 'com.digitalpaytech.domain.Location'),
('43', '40', '11', 'com.digitalpaytech.domain.Location'), 
('44', '41', '11', 'com.digitalpaytech.domain.Location'); 
  
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
