
-- Useful SQL CALL sp_FillOccupancyTimeslots('2012-06-01', 160, 2238, 159,0,0,'US/Central');


DROP PROCEDURE IF EXISTS sp_FillOccupancyTimeslots ;

delimiter //

CREATE PROCEDURE sp_FillOccupancyTimeslots (IN P_Date DATE, 
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED, 
											IN P_Interval TINYINT UNSIGNED, 
											IN P_NoOfSpaces MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25))
BEGIN

DECLARE lv_Time_Diff MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_TimeIdGMT , lv_TimeIdLocal MEDIUMINT UNSIGNED DEFAULT 0;

 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      LocationId,		CustomerId,				ETLObject, 	record_insert_time, Occ_Date,	UnifiedRateId,			NoOfSpaces, 	Customer_Timezone )  values(
      P_LocationId,		P_CustomerId,	        'TimeSlot', 	UTC_TIMESTAMP(), P_Date,	P_UnifiedRateId,		P_NoOfSpaces,	P_Customer_Timezone );
		COMMIT ; 
  END;

  
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.

SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = P_Date AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(P_Date,'GMT',P_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN

SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;

END IF;

INSERT INTO OccupancyHour 
(TimeIdGMT,TimeIdLocal, CustomerId, LocationId, UnifiedRateId, NumberOfSpaces, NoOfPermits) 
SELECT T.Id, T.Id - lv_Time_Diff, P_CustomerId, P_LocationId,P_UnifiedRateId,P_NoOfSpaces,0
FROM 
Time T, LocationOpen LO
WHERE T.Date = P_Date AND DayOfWeek = DAYOFWEEK(P_Date) AND 
LO.QuarterHourId   = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(P_Date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId;


END//

delimiter ;

-- 
/*
-- Procedure sp_MigrateOccupancyIntoKPI

-- Useful SQL: CALL sp_MigrateOccupancyIntoKPI(1,1,'2013-03-03 10:00:00', '2013-03-03 10:30:00', 160, 2263, 222, 'US/Central',1);
-- 			   CALL sp_MigrateOccupancyIntoKPI(9999,1,1,'2013-04-03 10:00:00', '2013-04-03 10:30:00', 160, 2237, 159, 'Canada/Pacific',1);

*/
-- 
DROP PROCEDURE IF EXISTS sp_MigrateOccupancyIntoKPI;

delimiter //

CREATE PROCEDURE sp_MigrateOccupancyIntoKPI(IN P_PermitId INT UNSIGNED ,
											IN P_ETLProcessDateRangeId INT UNSIGNED, 
											IN P_ClusterId INT UNSIGNED, 
											IN P_BeginPermitGMT datetime, 
											IN P_EndPermitGMT datetime,
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25),
											IN P_Permit_PermitTypeId TINYINT UNSIGNED)

BEGIN

-- DECLARE SECTION
DECLARE lv_TimeIdBeginGMT, lv_TimeIdLocal,lv_TimeIdEndGMT MEDIUMINT UNSIGNED;
DECLARE lv_cnt_records INT UNSIGNED DEFAULT 0;
DECLARE lv_GMT_date, lv_local_date DATE DEFAULT NULL;
DECLARE lv_no_of_spaces_location MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_permit_duration MEDIUMINT UNSIGNED DEFAULT NULL;
declare lv_TimeIdDayLocal mediumint unsigned DEFAULT NULL;
declare lv_TimeIdMonthLocal,lv_TimeId_Starting_QuarterHourId mediumint unsigned DEFAULT NULL;
declare lv_OccId, lv_StartingTimeID , lv_EndingTimeID BIGINT UNSIGNED DEFAULT 0;
DECLARE lv_OccupancyDay_Id, lv_OccupancyMonth_Id BIGINT UNSIGNED  DEFAULT NULL;

DECLARE lv_operation_MINUTES MEDIUMINT UNSIGNED DEFAULT NULL;
DECLARE lv_OpenQuarterHourNumber tinyint unsigned DEFAULT NULL; 
DECLARE lv_CloseQuarterHourNumber tinyint unsigned DEFAULT NULL;
DECLARE lv_IsOpen24Hours tinyint unsigned DEFAULT NULL;
DECLARE lv_location_open_datetime datetime DEFAULT NULL;
DECLARE lv_location_close_datetime datetime DEFAULT NULL;
DECLARE lv_UtilizationDay_Id BIGINT UNSIGNED DEFAULT NULL;
DECLARE lv_minutes_purchased INT UNSIGNED DEFAULT NULL;


-- Cursor Declaration Section

 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PermitId,		CustomerId,				ETLObject, 	record_insert_time,	ETLProcessDateRangeId,		ClusterId,	 BeginPermitGMT,	EndPermitGMT,	LocationId,		UnifiedRateId,	 Customer_Timezone )  values(
      P_PermitId,	P_CustomerId,	        'Permit', 	UTC_TIMESTAMP(),	P_ETLProcessDateRangeId,	P_ClusterId, P_BeginPermitGMT,	P_EndPermitGMT,	P_LocationId,	P_UnifiedRateId, P_Customer_Timezone);
		COMMIT ; 
  END;


	

-- UPDATE ETLProcessDateRange
-- SET CountOfRecords = idmaster_pos,
-- CursorOpenedTime = UTC_TIMESTAMP()
-- WHERE Id = P_ETLProcessDateRangeId;


-- Get TimeIDGMT of PermitBegin
-- This Query Performance is Good
SELECT id,Date into lv_TimeIdBeginGMT, lv_GMT_date FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= P_BeginPermitGMT);

-- Get the Local Begin TimeID of P_BeginPermitGMT 
-- This Query Performance is Good
SELECT Id, Date INTO lv_TimeIdLocal, lv_local_date FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;

-- Get the TimeIDGMT of P_EndPermitGMT 
-- This Query Performance is Good
SELECT Id INTO lv_TimeIdEndGMT FROM Time WHERE datetime = ( select min(datetime) from Time where datetime >= P_EndPermitGMT);

/*SELECT min(T.Id) INTO lv_TimeId_Starting_QuarterHourId
FROM 
Time T, LocationOpen LO
WHERE T.Date = lv_GMT_date AND DayOfWeek = DAYOFWEEK(lv_GMT_date) AND 
LO.QuarterHourId-1  = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(lv_GMT_date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId ;
*/
-- NOTE: adding -1 in the below SQL need to be removed once LocationOpen.QuarterHourId is fixed in Migration

-- This Query Performance is Fair
SELECT id INTO lv_TimeId_Starting_QuarterHourId FROM Time WHERE date = lv_GMT_date and QuarterOfDay = (select min(QuarterHourId)-1 from 
LocationOpen where LocationId = P_LocationId and DayofWeekId = DAYOFWEEK(lv_GMT_date) and IsOpen =1 );

-- This Query Performance is Good
SELECT COUNT(1) INTO lv_cnt_records FROM OccupancyHour
WHERE TimeIdGMT = lv_TimeId_Starting_QuarterHourId AND CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;


-- Get no. of spaces for Location for that day of week
-- This Query Performance is Good
-- This is used only for Testing.. Initial Migration
SELECT if(NumberOfSpaces=0,200,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = P_LocationId;

-- Get Hours of Operations from LocationDay -- Code written on June 18

SELECT OpenQuarterHourNumber, CloseQuarterHourNumber, IsOpen24Hours 
INTO lv_OpenQuarterHourNumber, lv_CloseQuarterHourNumber, lv_IsOpen24Hours
FROM LocationDay where LocationId = P_LocationId and dayofweekid = dayofweek(P_BeginPermitGMT) ;

IF lv_IsOpen24Hours = 1 THEN
	-- SET lv_operation_MINUTES = 1440 ;
	-- USed only for Testing
	SET lv_operation_MINUTES = 600 ;
ELSE
	-- uncommentSELECT dateTime INTO lv_location_open_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_OpenQuarterHourNumber ;
	-- uncommentSELECT timestampadd(minute, 15,dateTime) INTO lv_location_close_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_CloseQuarterHourNumber ;
	-- uncommentSELECT TIMESTAMPDIFF(minute,lv_location_open_datetime,lv_location_close_datetime) INTO lv_operation_MINUTES ;
	-- USed only for Testing
	SET lv_operation_MINUTES = 600 ;
END IF ;

IF lv_cnt_records = 0 THEN
	
	CALL sp_FillOccupancyTimeslots(lv_GMT_date, P_CustomerId, P_LocationId, P_UnifiedRateId, 0, lv_no_of_spaces_location, P_Customer_Timezone);

END IF;	


	-- Get time difference in minutes between P_EndPermitGMT and P_BeginPermitGMT
	-- select lv_TimeIdBeginGMT, lv_TimeIdEndGMT ;
	
	IF (lv_TimeIdEndGMT >= lv_TimeIdBeginGMT) THEN
	
	-- Get Duration of Permit (in no of buckets)
	select lv_TimeIdEndGMT - lv_TimeIdBeginGMT  into lv_permit_duration ;
	
	-- select lv_TimeIdBeginGMT, lv_permit_duration,lv_TimeIdEndGMT;
	
	-- This Query Performance is Good
	SELECT Id into lv_StartingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	-- This Query Performance is Good
	SELECT Id into lv_EndingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT+lv_permit_duration-1 AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	
	-- UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	-- WHERE TimeIdGMT between lv_TimeIdBeginGMT and (lv_TimeIdBeginGMT+lv_permit_duration-1) AND
	-- CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;
	
	-- This Query Performance is Fair
	-- UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	-- WHERE Id between lv_StartingTimeID AND  lv_EndingTimeID ;
	-- Used a Loop to update the table with Key as PK
	SET lv_OccId = lv_StartingTimeID;
	WHILE lv_OccId <= lv_EndingTimeID DO
	
		
		UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
		WHERE Id = lv_OccId ;
		
	SET lv_OccId = lv_OccId + 1 ;
	END WHILE ;
	
	-- This Query Performance is Good
	UPDATE OccupancyHour SET NoOfPurchases = NoOfPurchases + 1
	WHERE Id = lv_StartingTimeID ; 
	
	-- June 10
	-- Write Code here to fill Turnover Table
	/*INSERT INTO OccupancyHourTurnover
	(OccupancyHourId, 		DurationMins ) VALUES
	(lv_StartingTimeID , 	TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */

	
	-- Write Code here to Fill Occupancy Day and Occupancy Monthly Tables
	
	-- Rewrite the below query for betterPerformance
	-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;	
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and QuarterOfDay = 0;
	
	INSERT INTO OccupancyDay
	(TimeIdLocal,		CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces) VALUES
	(lv_TimeIdDayLocal, P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location)
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), NoOfPermits = NoOfPermits + 1; 
	
	-- P_Permit_PermitTypeId
	-- lv_no_of_Purchases;
	SELECT TIMESTAMPDIFF(minute,P_BeginPermitGMT,P_EndPermitGMT) INTO lv_minutes_purchased ;
	
	SELECT Id INTO lv_UtilizationDay_Id FROM UtilizationDay
	WHERE TimeIdLocal = lv_TimeIdDayLocal AND 
	CustomerId = P_CustomerId AND
	LocationId = P_LocationId ;
	
	IF lv_UtilizationDay_Id IS NOT NULL THEN
	
		IF (P_Permit_PermitTypeId = 1) THEN  -- Regular Permit
			UPDATE UtilizationDay SET 
			NumberOfPurchases = NumberOfPurchases + 1, 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
		ELSE								-- Add Time
			UPDATE UtilizationDay SET 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
		END IF;
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			
			CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
	
	ELSE
				
	
			INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 							NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_TimeIdDayLocal,	P_CustomerId,	P_LocationId, 	lv_no_of_spaces_location,	lv_no_of_spaces_location*lv_operation_MINUTES,	1,				  lv_minutes_purchased );
	
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			
			CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
			
	END IF ;
	
	
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyDay_Id; 
	
	/*INSERT INTO OccupancyDayTurnover
	(OccupancyDayId ,DurationMins ) VALUES
	(lv_OccupancyDay_Id, TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
	
	-- Rewrite the query for better performance
	-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	-- and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone));
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
	
	INSERT INTO OccupancyMonth
	(TimeIdLocal,			CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces) VALUES
	(lv_TimeIdMonthLocal, 	P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location)
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id) , NoOfPermits = NoOfPermits + 1; 
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyMonth_Id; 
	
	/*INSERT INTO OccupancyMonthTurnover
	(OccupancyMonthId , DurationMins ) VALUES
	(lv_OccupancyMonth_Id , TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT))
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
		
	-- Code end here 
	
	
	
	ELSE
	
		insert into ETLNotProcessed(
		PermitId,		CustomerId,			ETLObject, 	record_insert_time )  values(
		P_PermitId,	P_CustomerId,	        'Permit1', 	UTC_TIMESTAMP() );
		
		
	END IF;

END//

delimiter ;
