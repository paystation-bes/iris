﻿/*
  --------------------------------------------------------------------------------
  -- Last Modified on Sep 30. ETL will not process if No. of Sapces for that Location is 0
  
  -- Modified on 2013-09-29 
  -- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
  
  Procedure:  sp_MigrateRevenueIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: CALL sp_MigrateRevenueIntoKPI_Inc(1,1,'2012-10-01 10:00:00','2012-10-01 11:00:00');
  -------------------------------------------------------------------------------- 
*/

DROP PROCEDURE IF EXISTS sp_MigrateRevenueIntoKPI_Inc;

delimiter //

CREATE PROCEDURE sp_MigrateRevenueIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN
-- Last modified on June 11
-- DECLARE SECTION
declare lv_TimeIdGMT MEDIUMINT UNSIGNED ;
declare lv_TimeIdLocal MEDIUMINT UNSIGNED;
declare lv_Customer_Timezone char(25); -- it is char(64) 
declare lv_Purchase_CustomerId MEDIUMINT UNSIGNED ;
declare lv_Purchase_LocationId MEDIUMINT UNSIGNED ;
declare lv_Permit_LocationId MEDIUMINT UNSIGNED ;	
declare lv_Permit_SpaceNumber MEDIUMINT UNSIGNED ;	
declare lv_Purchase_PointOfSaleId MEDIUMINT UNSIGNED ;
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
declare lv_Purchase_UnifiedRateId MEDIUMINT UNSIGNED ;
declare lv_Purchase_PaystationSettingId MEDIUMINT UNSIGNED ;
declare lv_Purchase_Id BIGINT ;
declare lv_Permit_Id BIGINT ;
declare lv_Permit_PermitIssueTypeId TINYINT UNSIGNED ;
declare lv_Permit_PermitTypeId TINYINT UNSIGNED ;
declare lv_Purchase_TransactionTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PaymentTypeId TINYINT UNSIGNED ;
declare lv_ProcessorTransaction_TypeId TINYINT UNSIGNED ;		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
declare lv_Purchase_CoinCount TINYINT UNSIGNED ;				-- PPPInfo.CoinCount
declare lv_Purchase_BillCount TINYINT UNSIGNED ;				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
declare lv_Purchase_CouponId MEDIUMINT UNSIGNED ;				-- PPPInfo.IsCoupon check null/not null
declare lv_Purchase_CashPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_CardPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_IsOffline TINYINT UNSIGNED ;				-- PPPInfo.IsOffline
declare lv_Purchase_IsRefundSlip TINYINT UNSIGNED ;			-- PPPI
DECLARE lv_Purchase_ChargedAmount MEDIUMINT UNSIGNED ;
DECLARE CursorDone INT DEFAULT 0;
DECLARE RowNumber                               INT UNSIGNED DEFAULT 0;
DECLARE RowsToMigrate                           INT UNSIGNED DEFAULT 0;
declare lv_PayStation_Id MEDIUMINT UNSIGNED ;
declare lv_PaystationTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PurchaseGMT DATETIME;
declare lv_ppp_IsFreePermit TINYINT UNSIGNED ;
declare idmaster_pos,indexm_pos int default 0;
declare lv_ppp_Id BIGINT;
declare lv_PermitBeginGMT DATETIME;
declare lv_PermitExpireGMT DATETIME;
-- Payment Card columns
declare lv_PaymentCard_Amount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_CardTypeID TINYINT UNSIGNED ;
declare lv_PaymentCard_CreditCardTypeID TINYINT UNSIGNED ;
declare	lv_PaymentCard_MerchantAccountID MEDIUMINT UNSIGNED ;

declare lv_Purchase_CoinPaidAmount MEDIUMINT UNSIGNED ;
declare lv_Purchase_BillPaidAmount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_Id BIGINT;
declare lv_Permit_OriginalPermitId BIGINT;

declare lv_PermitBeginLocal  DATETIME;
declare lv_occ_BeginTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_occ_ExpireTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_PermitExpireLocal  DATETIME;
declare lv_occ_ExpireTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginMinutes  TINYINT UNSIGNED ;
declare lv_occ_ExpireMinutes  TINYINT UNSIGNED ;
declare lv_occ_DurationMinutes  MEDIUMINT UNSIGNED;

declare lv_TimeIdDayLocal mediumint unsigned;
declare lv_TimeIdMonthLocal mediumint unsigned;
declare lv_PPPTotalHour_Id bigint unsigned;
declare lv_PPPDetailHour_Id  bigint unsigned;
declare lv_PPPTotalDay_Id  bigint unsigned;
declare lv_PPPDetailDay_Id  bigint unsigned;
declare lv_PPPTotalMonth_Id  bigint unsigned;
declare lv_PPPDetailMonth_Id  bigint unsigned;
declare lv_ProcessorTransaction_Id bigint unsigned;
declare lv_no_of_spaces_location mediumint unsigned ;

-- CURSOR C1 STATEMENT
DECLARE C1 CURSOR FOR
	
		SELECT distinct
		P.CustomerId,
		P.LocationId,
		ifnull(R.LocationId,0),
		ifnull(R.SpaceNumber,0),
		P.PointOfSaleId,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.Id,
		R.Id,
		ifnull(R.PermitIssueTypeId,0),
		ifnull(R.PermitTypeId,0),		
		P.TransactionTypeId,
		P.PaymentTypeId,
		T.ProcessorTransactionTypeId, 
		P.CoinCount,
		P.BillCount,
		P.CouponId,
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.IsOffline,
		P.IsRefundSlip,
		P.PurchaseGMT,
		R.PermitBeginGMT,
		R.PermitExpireGMT,
		C.Amount,
		C.CardTypeID,
		C.CreditCardTypeID,
		C.MerchantAccountID,
		P.CoinPaidAmount,
		P.BillPaidAmount,
		C.Id,
		ifnull(R.OriginalPermitId,R.Id),
		B.PropertyValue,
		P.ChargedAmount+P.ExcessPaymentAmount,
		T.Id			-- Added on June 11 for Incremental ETL
		from StagingPurchase P
		left outer join    StagingPermit R on  P.Id = R.PurchaseId
		left outer join   StagingPaymentCard C on P.Id = C.PurchaseId
		left outer join   StagingProcessorTransaction T on P.Id = T.PurchaseId
		left join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE  -- P.CustomerId in (160) AND /* This was used for micro-migration only */
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL LIMIT 50000;

		--  AND P.PurchaseGMT between P_BeginPurchaseGMT AND P_EndPurchaseGMT
		
		
		-- specify a date range
		
 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PurchaseId,		CustomerId,				PointOfSaleId,				PurchaseGMT, 			 ETLObject, 	record_insert_time )  values(
      lv_Purchase_Id,	lv_Purchase_CustomerId,	lv_Purchase_PointOfSaleId,	lv_Purchase_PurchaseGMT, 'Purchase', 	NOW() );
	  
	  -- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
	  -- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
	  INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
	  IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
	  IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
	  IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
		
	  DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
	  DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
	  DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
	  DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
	  COMMIT ; 
	  -- set autocommit = 1 ;
  END;


 
 -- set lv_Customer_Timezone = null;
 
 -- select PropertyValue into lv_Customer_Timezone
 -- from CustomerProperty where CustomerId = P_CustomerId and CustomerPropertyTypeId=1;
 
 -- DELETE FROM ETLQueueRecordCount where ETLProcessDateRangeId = P_ETLExecutionLogId ;
 
 OPEN C1;
	set idmaster_pos = (select FOUND_ROWS());
-- select idmaster_pos;

	UPDATE ETLExecutionLog
	SET PurchaseRecordCount  = idmaster_pos,
	PPPCursorOpenedTime = NOW()
	WHERE Id = P_ETLExecutionLogId;
	
	/*INSERT INTO ETLQueueRecordCount
	(ETLProcessDateRangeId,		TotalRecords,	ETLRemaingRecords,	LastModifiedGMT) VALUES
	(P_ETLExecutionLogId,	idmaster_pos,	idmaster_pos,		NOW());*/
	
-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
-- set autocommit = 0 ;

while indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	lv_Purchase_CustomerId, -- PPPInfo.CustomerID
					lv_Purchase_LocationId, -- PPPInfo.PurchaseLocationId
					lv_Permit_LocationId,	-- PPPInfo.PermitLocationId
					lv_Permit_SpaceNumber,	-- PPPInfo.SpaceNumber	
					lv_Purchase_PointOfSaleId,	--	PPPInfo.PointOfSaleId
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
					lv_Purchase_UnifiedRateId,	-- PPPInfo.UnifiedRateId
					lv_Purchase_PaystationSettingId,	--	PPPInfo.PaystationSettingId
					lv_Purchase_Id,						-- PPPInfo.PurchaseId
					lv_Permit_Id,						-- PPPInfo.PermitId
					lv_Permit_PermitIssueTypeId,		--	PPPInfo.PermitIssueTypeId
					lv_Permit_PermitTypeId,				--	PPPInfo.PermitTypeId 
					lv_Purchase_TransactionTypeId,		-- PPPInfo.TransactionTypeId
					lv_Purchase_PaymentTypeId,			-- PPPInfo.PaymentTypeId
					lv_ProcessorTransaction_TypeId,		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
					lv_Purchase_CoinCount,				-- PPPInfo.CoinCount
					lv_Purchase_BillCount,				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
					lv_Purchase_CouponId,				-- PPPInfo.IsCoupon check null/not null
					lv_Purchase_CashPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_CardPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_IsOffline,				-- PPPInfo.IsOffline
					lv_Purchase_IsRefundSlip,			-- PPPInfo.IsRefundSlip
					lv_Purchase_PurchaseGMT,
					lv_PermitBeginGMT,
					lv_PermitExpireGMT,
					lv_PaymentCard_Amount,
					lv_PaymentCard_CardTypeID,
					lv_PaymentCard_CreditCardTypeID,
					lv_PaymentCard_MerchantAccountID,
					lv_Purchase_CoinPaidAmount,
					lv_Purchase_BillPaidAmount,
					lv_PaymentCard_Id,
					lv_Permit_OriginalPermitId,
					lv_Customer_Timezone,
					lv_Purchase_ChargedAmount,
					lv_ProcessorTransaction_Id;	
								
			
		-- STEP 1: TimeIdGMT --> Purchase.PurchaseGMT GET the ID ftom TIME Table 
		-- SELECT MAX(Id) into lv_TimeIdGMT FROM time WHERE datetime <= lv_Purchase_PurchaseGMT;
		-- select 'aaa';
		SELECT id into lv_TimeIdGMT FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= lv_Purchase_PurchaseGMT);
		
		-- SELECT MAX(Id) INTO lv_TimeIdLocal FROM time where DateTime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone);
				
		SELECT Id INTO lv_TimeIdLocal FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
					
		
		-- STEP 3: CustomerId --> Purchase.CustomerId
		-- STEP 4: PurchaseLocationId --> Purchase.LocationId
		-- STEP 5: PermitLocationId --> Permit.LocationId
		-- STEP 6: SpaceNumber --> Permit.SpaceNumber
		-- STEP 7: PointOfSaleId --> Purchase.PointOfSaleId
		
		
		-- STEP 8: PaystationId --> LOOKUP from PointofSale to PayStation
		SELECT PayStationId INTO lv_PayStation_Id from PointOfSale where Id = lv_Purchase_PointOfSaleId;
		
		
		-- STEP 9: PaystationTypeId --> paystation.PaystationTypeId
		SELECT PaystationTypeId INTO lv_PaystationTypeId FROM Paystation where Id= lv_PayStation_Id;
		
				
		set lv_ppp_IsFreePermit = 0;
		SET lv_ppp_IsFreePermit  = IF ((lv_Purchase_CashPaidAmount = 0 AND lv_Purchase_CardPaidAmount = 0),1,0);
		
		/*INSERT INTO PPPInfo
		(TimeIdGMT, 	TimeIdLocal, 	CustomerId, 			PurchaseLocationId, 	PermitLocationId, 		SpaceNumber, 			PointOfSaleId, 				PaystationId, 		PaystationTypeId, 		UnifiedRateId, 				PaystationSettingId, 			PurchaseId, 		PermitId, 		PermitIssueTypeId, 				PermitTypeId, 			TransactionTypeId, 				PaymentTypeId, 					ProcessorTransactionTypeId, 	CoinCount, 				BillCount, 				DurationMinutes, 																	IsCoupon, 										IsFreePermit, 					 IsOffline, 			IsRefundSlip, 				IsRFID, IsUploadedFromBoss) VALUES
		(lv_TimeIdGMT,	lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Permit_SpaceNumber,	lv_Purchase_PointOfSaleId,	lv_PayStation_Id,	lv_PaystationTypeId,	lv_Purchase_UnifiedRateId,	lv_Purchase_PaystationSettingId,lv_Purchase_Id,		lv_Permit_Id,	lv_Permit_PermitIssueTypeId,	lv_Permit_PermitTypeId,	lv_Purchase_TransactionTypeId,	lv_Purchase_PaymentTypeId,		lv_ProcessorTransaction_TypeId,	lv_Purchase_CoinCount,	lv_Purchase_BillCount,	ifnull((TO_SECONDS(lv_PermitExpireGMT) - TO_SECONDS(lv_PermitBeginGMT))/60,0),		if(length(lv_Purchase_CouponId)>0,1,0),			lv_ppp_IsFreePermit,			 lv_Purchase_IsOffline,	lv_Purchase_IsRefundSlip,	1,		1				);		*/
		
		
		INSERT INTO PPPTotalHour
		(TimeIdGMT,    TimeIdLocal, 	CustomerId, 			TotalAmount					) VALUES
		(lv_TimeIdGMT, lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount	)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalHour_Id  FROM PPPTotalHour;
		
		-- Insert into PPPDetailHour
		INSERT INTO PPPDetailHour
		(PPPTotalHourId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalHour_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailHour_Id FROM PPPDetailHour;
		
		
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			IF (lv_Purchase_CashPaidAmount = 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		
		-- INSERT FOR PPPTotalDay SECOND BLOCK START
		-- GET the starting Time ID from Time Table for that Day
		-- Rewrite this query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and QuarterOfDay = 0;
		
		INSERT INTO PPPTotalDay
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdDayLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalDay_Id FROM PPPTotalDay ;
		
		-- Insert into PPPDetailDay
		INSERT INTO PPPDetailDay
		(PPPTotalDayId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalDay_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailDay_Id FROM PPPDetailDay;
		
		-- start insert into PPPRevenueDay
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			IF (lv_Purchase_CashPaidAmount = 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				
				INSERT INTO PPPRevenueDay
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		-- end insert into PPPDetailDay
		-- SECOND BLOCK END
		
		-- THIRD BLOCK START
		
		-- INSERT FOR PPPTotalMonth
		-- GET the starting Time ID from Time table for that Year and Month
		-- Rewrite this Query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		--			and Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone));
		
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		 AND Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
		
		INSERT INTO PPPTotalMonth
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdMonthLocal,lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPTotalMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalMonth_Id FROM PPPTotalMonth;
		
		-- Insert into PPPDetailMonth
		INSERT INTO PPPDetailMonth
		(PPPTotalMonthId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalMonth_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPDetailMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailMonth_Id FROM PPPDetailMonth;
		
		-- start insert into PPPRevenueMonth
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			IF (lv_Purchase_CashPaidAmount = 0) THEN
			
				-- Insert into PPPRevenueMonth
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				
				INSERT INTO PPPRevenueMonth
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash)
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash)
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
-- end insert into PPPRevenueMonth
		
		-- START FOR OCCUPANCY
		-- Added on Sep 30. ETL will not process if No. of Sapces for that Location is 0
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_Permit_LocationId;

		IF (lv_Permit_Id > 0) and (lv_no_of_spaces_location > 0) THEN
		
		CALL sp_MigrateOccupancyIntoKPI_Inc( lv_Permit_Id ,
										 P_ETLExecutionLogId , 
										 1 , 
										 lv_PermitBeginGMT, 
										 lv_PermitExpireGMT ,
										 lv_Purchase_CustomerId, 
										 lv_Permit_LocationId, 
										 lv_Purchase_UnifiedRateId,
										 lv_Customer_Timezone,
										 lv_Permit_PermitTypeId);
		
		END IF;
		-- END FOR OCCUPANCY
		
		-- START FOR SettledTransaction
		-- Dont call from here 
		-- CALL sp_MigrateSettledTransactionIntoKPI_Inc(P_ETLExecutionLogId) ;
		
		-- END FOR SettledTransaction
		
		-- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
		-- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
		INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
		IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
		IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
		IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
	  
		DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
		DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
		DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
		DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
		
		
		set lv_TimeIdGMT = NULL;
		set lv_TimeIdLocal = NULL;
		set lv_Purchase_LocationId = NULL;
		set lv_Permit_LocationId = NULL;
		set lv_Purchase_CustomerId = null;
		set lv_Permit_SpaceNumber = null;
		set lv_Purchase_PointOfSaleId = null;
		set lv_PayStation_Id = null;
		set lv_PaystationTypeId = null;
		set lv_Purchase_UnifiedRateId = null;
		set lv_Purchase_PaystationSettingId = null;
		set lv_Purchase_Id = null;
		set lv_Permit_Id = null;
		set lv_Permit_PermitIssueTypeId = null;
		set lv_Permit_PermitTypeId = null;
		set lv_Purchase_TransactionTypeId = null;
		set lv_Purchase_PaymentTypeId = null;
		set lv_Purchase_CoinCount = null;
		set lv_ProcessorTransaction_TypeId = null;
		set lv_Purchase_BillCount = null;
		set lv_Purchase_CouponId= null;
		set lv_ppp_IsFreePermit = null;
		set lv_Purchase_IsOffline = null;
		set lv_Purchase_IsRefundSlip = null;
		set lv_PermitExpireGMT = null;
		set lv_PermitBeginGMT = null ;
		set lv_PaymentCard_Amount = null;
		set lv_PaymentCard_CardTypeID = null;
		set lv_PaymentCard_CreditCardTypeID = null;
		set lv_PaymentCard_MerchantAccountID = null;
		set lv_PaymentCard_Id = null;
		set lv_Permit_OriginalPermitId = null;
		set lv_Purchase_ChargedAmount = null;
		set lv_ProcessorTransaction_Id = null;
		
		
		

		IF mod(indexm_pos,1000) = 0 THEN
			COMMIT ;
		END IF ;
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
               
 COMMIT ;		-- ASHOK2	   
-- set autocommit = 1 ; -- Auto Commit ON
 -- ASHOK3 SET AUTO COMMIT ON
    
    -- SELECT NOW() AS EndMigration;
END //
delimiter ;

-- *************************************************************************************************************************************************************


-- Useful SQL CALL sp_FillOccupancyTimeslots('2012-06-01', 160, 2238, 159,0,0,'US/Central');


DROP PROCEDURE IF EXISTS sp_FillOccupancyTimeslots ;

delimiter //

CREATE PROCEDURE sp_FillOccupancyTimeslots (IN P_Date DATE, 
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED, 
											IN P_Interval TINYINT UNSIGNED, 
											IN P_NoOfSpaces MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25))
BEGIN

DECLARE lv_Time_Diff MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_TimeIdGMT , lv_TimeIdLocal MEDIUMINT UNSIGNED DEFAULT 0;

 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      LocationId,		CustomerId,				ETLObject, 	record_insert_time, Occ_Date,	UnifiedRateId,			NoOfSpaces, 	Customer_Timezone )  values(
      P_LocationId,		P_CustomerId,	        'TimeSlot', 	UTC_TIMESTAMP(), P_Date,	P_UnifiedRateId,		P_NoOfSpaces,	P_Customer_Timezone );
		COMMIT ; 
  END;

  
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.

SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = P_Date AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(P_Date,'GMT',P_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN

SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;

END IF;

select T.Id, lv_Time_Diff, CustomerId, LocationId, UnifiedRateId, NumberOfSpaces, NoOfPermits;

INSERT INTO OccupancyHour 
(TimeIdGMT,TimeIdLocal, CustomerId, LocationId, UnifiedRateId, NumberOfSpaces, NoOfPermits) 
SELECT T.Id, T.Id - lv_Time_Diff, P_CustomerId, P_LocationId,P_UnifiedRateId,P_NoOfSpaces,0
FROM 
Time T, LocationOpen LO
WHERE T.Date = P_Date AND DayOfWeek = DAYOFWEEK(P_Date) AND 
LO.QuarterHourId   = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(P_Date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId;

-- removed -1 in LO.QuarterHourId -1 

END//

delimiter ;

-- 
/*
-- Procedure sp_MigrateOccupancyIntoKPI_Inc

-- Useful SQL: CALL sp_MigrateOccupancyIntoKPI_Inc(1,1,'2013-03-03 10:00:00', '2013-03-03 10:30:00', 160, 2263, 222, 'US/Central');

*/
-- 
DROP PROCEDURE IF EXISTS sp_MigrateOccupancyIntoKPI_Inc;

delimiter //

CREATE PROCEDURE sp_MigrateOccupancyIntoKPI_Inc(IN P_PermitId INT UNSIGNED ,
											IN P_ETLExecutionLogId INT UNSIGNED, 
											IN P_ClusterId INT UNSIGNED, 
											IN P_BeginPermitGMT datetime, 
											IN P_EndPermitGMT datetime,
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25),
											IN P_Permit_PermitTypeId TINYINT UNSIGNED)
BEGIN

-- DECLARE SECTION
DECLARE lv_TimeIdBeginGMT, lv_TimeIdLocal,lv_TimeIdEndGMT MEDIUMINT UNSIGNED;
DECLARE lv_cnt_records INT UNSIGNED DEFAULT 0;
DECLARE lv_GMT_date, lv_local_date DATE DEFAULT NULL;
DECLARE lv_no_of_spaces_location MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_permit_duration MEDIUMINT UNSIGNED DEFAULT NULL;
declare lv_TimeIdDayLocal mediumint unsigned DEFAULT NULL;
declare lv_TimeIdMonthLocal,lv_TimeId_Starting_QuarterHourId mediumint unsigned DEFAULT NULL;
declare lv_OccId, lv_StartingTimeID , lv_EndingTimeID BIGINT UNSIGNED DEFAULT 0;
DECLARE lv_OccupancyDay_Id, lv_OccupancyMonth_Id BIGINT UNSIGNED  DEFAULT NULL;

DECLARE lv_operation_MINUTES MEDIUMINT UNSIGNED DEFAULT NULL;
DECLARE lv_OpenQuarterHourNumber tinyint unsigned DEFAULT NULL; 
DECLARE lv_CloseQuarterHourNumber tinyint unsigned DEFAULT NULL;
DECLARE lv_IsOpen24Hours tinyint unsigned DEFAULT NULL;
DECLARE lv_location_open_datetime datetime DEFAULT NULL;
DECLARE lv_location_close_datetime datetime DEFAULT NULL;
DECLARE lv_UtilizationDay_Id BIGINT UNSIGNED DEFAULT NULL;
DECLARE lv_minutes_purchased INT UNSIGNED DEFAULT NULL;


-- Cursor Declaration Section

 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PermitId,		CustomerId,				ETLObject, 	record_insert_time,	ETLProcessDateRangeId,		ClusterId,	 BeginPermitGMT,	EndPermitGMT,	LocationId,		UnifiedRateId,	 Customer_Timezone )  values(
      P_PermitId,	P_CustomerId,	        'Permit', 	UTC_TIMESTAMP(),	NUll,	P_ClusterId, P_BeginPermitGMT,	P_EndPermitGMT,	P_LocationId,	P_UnifiedRateId, P_Customer_Timezone);
		COMMIT ; 
  END;


	

-- UPDATE ETLProcessDateRange
-- SET CountOfRecords = idmaster_pos,
-- CursorOpenedTime = UTC_TIMESTAMP()
-- WHERE Id = P_ETLProcessDateRangeId;


-- Get TimeIDGMT of PermitBegin
-- This Query Performance is Good
SELECT id,Date into lv_TimeIdBeginGMT, lv_GMT_date FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= P_BeginPermitGMT);

-- Get the Local Begin TimeID of P_BeginPermitGMT 
-- This Query Performance is Good
SELECT Id, Date INTO lv_TimeIdLocal, lv_local_date FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;

-- Get the TimeIDGMT of P_EndPermitGMT 
-- This Query Performance is Good
SELECT Id INTO lv_TimeIdEndGMT FROM Time WHERE datetime = ( select min(datetime) from Time where datetime >= P_EndPermitGMT);

/*SELECT min(T.Id) INTO lv_TimeId_Starting_QuarterHourId
FROM 
Time T, LocationOpen LO
WHERE T.Date = lv_GMT_date AND DayOfWeek = DAYOFWEEK(lv_GMT_date) AND 
LO.QuarterHourId-1  = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(lv_GMT_date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId ;
*/
-- NOTE: adding -1 in the below SQL need to be removed once LocationOpen.QuarterHourId is fixed in Migration

-- This Query Performance is Fair
-- Added min(QuarterHourId) condition on Aug 22
SELECT id INTO lv_TimeId_Starting_QuarterHourId FROM Time WHERE date = lv_GMT_date and QuarterOfDay = (select min(QuarterHourId) from 
LocationOpen where LocationId = P_LocationId and DayofWeekId = DAYOFWEEK(lv_GMT_date) and IsOpen =1 );

-- This Query Performance is Good
SELECT COUNT(1) INTO lv_cnt_records FROM OccupancyHour
WHERE TimeIdGMT = lv_TimeId_Starting_QuarterHourId AND CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;


-- Get no. of spaces for Location for that day of week
-- This Query Performance is Good
-- This is used only for Testing.. Initial Migration
SELECT if(NumberOfSpaces=0,200,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = P_LocationId;

-- Get Hours of Operations from LocationDay -- Code written on June 18

SELECT OpenQuarterHourNumber, CloseQuarterHourNumber, IsOpen24Hours 
INTO lv_OpenQuarterHourNumber, lv_CloseQuarterHourNumber, lv_IsOpen24Hours
FROM LocationDay where LocationId = P_LocationId and dayofweekid = dayofweek(P_BeginPermitGMT) ;

IF lv_IsOpen24Hours = 1 THEN
	-- SET lv_operation_MINUTES = 1440 ;
	-- USed only for Testing
	SET lv_operation_MINUTES = 600 ;
ELSE
	-- uncommentSELECT dateTime INTO lv_location_open_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_OpenQuarterHourNumber ;
	-- uncommentSELECT timestampadd(minute, 15,dateTime) INTO lv_location_close_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_CloseQuarterHourNumber ;
	-- uncommentSELECT TIMESTAMPDIFF(minute,lv_location_open_datetime,lv_location_close_datetime) INTO lv_operation_MINUTES ;
	-- USed only for Testing
	SET lv_operation_MINUTES = 600 ;
END IF ;

IF lv_cnt_records = 0 THEN
	
	CALL sp_FillOccupancyTimeslots(lv_GMT_date, P_CustomerId, P_LocationId, P_UnifiedRateId, 0, lv_no_of_spaces_location, P_Customer_Timezone);

END IF;	


	-- Get time difference in minutes between P_EndPermitGMT and P_BeginPermitGMT
	-- select lv_TimeIdBeginGMT, lv_TimeIdEndGMT ;
	
	IF (lv_TimeIdEndGMT >= lv_TimeIdBeginGMT) THEN
	
	-- Get Duration of Permit (in no of buckets)
	select lv_TimeIdEndGMT - lv_TimeIdBeginGMT  into lv_permit_duration ;
	
	-- select lv_TimeIdBeginGMT, lv_permit_duration,lv_TimeIdEndGMT;
	
	-- This Query Performance is Good
	SELECT Id into lv_StartingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	-- This Query Performance is Good
	SELECT Id into lv_EndingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT+lv_permit_duration-1 AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	
	-- UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	-- WHERE TimeIdGMT between lv_TimeIdBeginGMT and (lv_TimeIdBeginGMT+lv_permit_duration-1) AND
	-- CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;
	
	-- This Query Performance is Fair
	 UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	 WHERE Id between lv_StartingTimeID AND  lv_EndingTimeID and CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	-- Used a Loop to update the table with Key as PK
	/*SET lv_OccId = lv_StartingTimeID;
	WHILE lv_OccId <= lv_EndingTimeID DO
	
		
		UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
		WHERE Id = lv_OccId ;
		
	SET lv_OccId = lv_OccId + 1 ;
	END WHILE ;*/
	
	-- This Query Performance is Good
	UPDATE OccupancyHour SET NoOfPurchases = NoOfPurchases + 1
	WHERE Id = lv_StartingTimeID ; 
	
	-- June 10
	-- Write Code here to fill Turnover Table
	/*INSERT INTO OccupancyHourTurnover
	(OccupancyHourId, 		DurationMins ) VALUES
	(lv_StartingTimeID , 	TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */

	
	-- Write Code here to Fill Occupancy Day and Occupancy Monthly Tables
	
	-- Rewrite the below query for betterPerformance
	-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;	
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and QuarterOfDay = 0;
	
	INSERT INTO OccupancyDay
	(TimeIdLocal,		CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces) VALUES
	(lv_TimeIdDayLocal, P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location)
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), NoOfPermits = NoOfPermits + 1; 
	
	-- P_Permit_PermitTypeId
	-- lv_no_of_Purchases;
	SELECT TIMESTAMPDIFF(minute,P_BeginPermitGMT,P_EndPermitGMT) INTO lv_minutes_purchased ;
	
	SELECT Id INTO lv_UtilizationDay_Id FROM UtilizationDay
	WHERE TimeIdLocal = lv_TimeIdDayLocal AND 
	CustomerId = P_CustomerId AND
	LocationId = P_LocationId ;
	
	IF lv_UtilizationDay_Id IS NOT NULL THEN
	
		IF (P_Permit_PermitTypeId = 1) THEN  -- Regular Permit
			UPDATE UtilizationDay SET 
			NumberOfPurchases = NumberOfPurchases + 1, 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
		ELSE								-- Add Time
			UPDATE UtilizationDay SET 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
		END IF;
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			
			CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
	
	ELSE
				
	
			INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 							NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_TimeIdDayLocal,	P_CustomerId,	P_LocationId, 	lv_no_of_spaces_location,	lv_no_of_spaces_location*lv_operation_MINUTES,	1,				  lv_minutes_purchased );
	
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			
			CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
			
	END IF ;
	
	
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyDay_Id; 
	
	/*INSERT INTO OccupancyDayTurnover
	(OccupancyDayId ,DurationMins ) VALUES
	(lv_OccupancyDay_Id, TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
	
	-- Rewrite the query for better performance
	-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	-- and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone));
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
	
	INSERT INTO OccupancyMonth
	(TimeIdLocal,			CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces) VALUES
	(lv_TimeIdMonthLocal, 	P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location)
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id) , NoOfPermits = NoOfPermits + 1; 
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyMonth_Id; 
	
	/*INSERT INTO OccupancyMonthTurnover
	(OccupancyMonthId , DurationMins ) VALUES
	(lv_OccupancyMonth_Id , TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT))
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
		
	-- Code end here 
	
	
	
	ELSE
	
		insert into ETLNotProcessed(
		PermitId,		CustomerId,			ETLObject, 	record_insert_time )  values(
		P_PermitId,	P_CustomerId,	        'Permit1', 	UTC_TIMESTAMP() );
		
		
	END IF;

END//

delimiter ;

-- **********************************************************************************************************************************************

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_MigrateSettledTransactionIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/

DROP PROCEDURE IF EXISTS sp_MigrateSettledTransactionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateSettledTransactionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- June 11 2013 

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;
DECLARE lv_IsRefund TINYINT ; 


-- CURSOR VARIABLES
DECLARE v_ProcessorTxn_Id BIGINT ;
DECLARE v_ProcessorTxn_PointOfSaleId MEDIUMINT ; 
DECLARE v_ProcessorTxn_CustomerID MEDIUMINT ;
DECLARE v_ProcessorTxn_PurchaseId BIGINT ; 
DECLARE v_ProcessorTxn_ProcessorTransactionTypeId  TINYINT ;
DECLARE v_ProcessorTxn_MerchantAccountId MEDIUMINT ; 
DECLARE v_ProcessorTxn_Amount MEDIUMINT ;
DECLARE v_ProcessorTxn_ProcessingDate DATETIME ;
DECLARE v_ProcessorTxn_PropertyValue VARCHAR(40);		

DECLARE C1 CURSOR FOR
	
		SELECT
			T.Id ,
			T.PointOfSaleId , 
			B.CustomerID,
			T.PurchaseId ,
			T.ProcessorTransactionTypeId , 
			T.MerchantAccountId , 
			T.Amount ,
			T.ProcessingDate ,
			P.PropertyValue
		FROM 
			StagingProcessorTransaction T, 
			PointOfSale B, 
			CustomerProperty P
		WHERE
			T.PointOfSaleId = B.Id AND 
			B.CustomerId = P.CustomerId AND
			P.CustomerPropertyTypeId = 1 AND 
			T.IsApproved = 1 AND
			P.PropertyValue IS NOT NULL LIMIT 50000 ;
			
			-- Check Performance here
			-- AND	T.ProcessingDate BETWEEN P_BeginDateGMT and  P_EndDateGMT 
				
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO SettledTxnNotProcessed ( ProcessorTransactionId , PointOfSaleId , 				ProcessorTransactionTypeId , 				ProcessingDate , 				RecordInsertTime) VALUES
										 ( v_ProcessorTxn_Id,		v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_ProcessingDate, 	UTC_TIMESTAMP()	 );
	  INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
	  DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;

  END;
 

 -- SET lv_Customer_Timezone = null;
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET SettledAmountRecordCount = idmaster_pos,
SettledAmountCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_ProcessorTxn_Id ,
					v_ProcessorTxn_PointOfSaleId , 
					v_ProcessorTxn_CustomerID,
					v_ProcessorTxn_PurchaseId ,
					v_ProcessorTxn_ProcessorTransactionTypeId , 
					v_ProcessorTxn_MerchantAccountId , 
					v_ProcessorTxn_Amount ,
					v_ProcessorTxn_ProcessingDate ,
					v_ProcessorTxn_PropertyValue ;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				SET lv_TimeIdLocal = NULL ;
				SET lv_IsRefund = NULL ;
				
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) ;
				
				IF v_ProcessorTxn_ProcessorTransactionTypeId in (2,3, 6, 7, 10, 11, 17) THEN
					SET lv_IsRefund = 0 ;
				ELSEIF v_ProcessorTxn_ProcessorTransactionTypeId in ( 4, 12, 13, 18, 19) THEN
					SET lv_IsRefund = 1 ;
				ELSE
					SET lv_IsRefund = 2 ;
				END IF;
				
				
				INSERT INTO SettledTransaction (ProcessorTransactionId, CustomerId, 				PurchaseId, 				TimeIdLocal, 	ProcessorTransactionTypeId , 				MerchantAccountId, 					Amount, 				IsRefund)
										VALUES (v_ProcessorTxn_Id,		v_ProcessorTxn_CustomerID,	v_ProcessorTxn_PurchaseId,	lv_TimeIdLocal,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_MerchantAccountId,	v_ProcessorTxn_Amount,	lv_IsRefund) ;
	
				
				INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
				DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;

-- ***********************************************************************


/*
 --------------------------------------------------------------------------------
  Procedure:  sp_MigrateCollectionIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/

DROP PROCEDURE IF EXISTS sp_MigrateCollectionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateCollectionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- Uploaded to Subversion
-- Last modified on June 11 2013
-- DECLARE SECTION
-- FOR ACTUAL MIGRATION THIS PROCEDURE TAKEN ONLY ENDGMT DATA AS INPUT PARAMETER

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;



-- CURSOR VARIABLES
DECLARE v_Collection_Id BIGINT;
DECLARE v_Collection_CustomerId MEDIUMINT;
DECLARE	v_Collection_PointOfSaleId MEDIUMINT;
DECLARE	v_Collection_CollectionTypeId TINYINT;
DECLARE	v_Collection_EndGMT DATETIME;
DECLARE	v_Collection_CoinTotalAmount MEDIUMINT;
DECLARE	v_Collection_BillTotalAmount MEDIUMINT;
DECLARE	v_Collection_CardTotalAmount MEDIUMINT;
DECLARE	v_CollectionTotal MEDIUMINT;
DECLARE v_Customer_Timezone VARCHAR(40);	

-- CURSOR C1 STATEMENT
-- RENAME C1 WITH c_Collection
-- There will be only INSERT operation on POSCollection

DECLARE C1 CURSOR FOR
	
		SELECT
		A.Id,
		A.CustomerId,
		A.PointOfSaleId,
		A.CollectionTypeId,
		A.EndGMT,
		A.CoinTotalAmount,
		A.BillTotalAmount,
		A.CardTotalAmount,
		A.CoinTotalAmount + A.BillTotalAmount +	A.CardTotalAmount,
		B.PropertyValue
		FROM 
		StagingPOSCollection A, CustomerProperty B
		WHERE 
		A.CustomerId = B.CustomerId AND
		B.CustomerPropertyTypeId = 1 AND
		PropertyValue IS NOT NULL  LIMIT 50000;
		
		-- AND	A.CreatedGMT between P_BeginDateGMT AND P_EndDateGMT
		-- A.EndGMT between P_BeginDateGMT AND P_EndDateGMT ; Added on April 19 2013
		-- A.EndGMT must be replace by A.CreateGMT the new attribute to be added
		-- WHERE 
		-- CustomerId = P_CustomerId AND
		-- EndGMT between P_BeginCollectionGMT AND P_EndCollectionGMT;		
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		-- BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50,
		-- AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount,
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		--	+ BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50 + 
		--	AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount ,
		
		
		
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO CollectionNotProcessed ( PosCollectionId , CustomerId , 				PointOfSaleId , 			CollectionTypeId , 				EndGMT , 				RecordInsertTime  ) VALUES
										 ( v_Collection_Id , v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId , v_Collection_EndGMT,	UTC_TIMESTAMP() );
	
	INSERT INTO ArchiveStagingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
	DELETE FROM StagingPOSCollection WHERE Id = v_Collection_Id;
	
  END;
 

 SET lv_Customer_Timezone = null;
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET CollectionRecordCount = idmaster_pos,
CollectionCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_Collection_Id,
					v_Collection_CustomerId,
					v_Collection_PointOfSaleId,
					v_Collection_CollectionTypeId,
					v_Collection_EndGMT,
					v_Collection_CoinTotalAmount,
					v_Collection_BillTotalAmount,
					v_Collection_CardTotalAmount,
					v_CollectionTotal,
					v_Customer_Timezone;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				
				SELECT Id INTO lv_TimeIdGMT FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= v_Collection_EndGMT);
						
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_Collection_EndGMT,'GMT',v_Customer_Timezone)) ;
				
				IF (SELECT COUNT(*) FROM TotalCollection 
				WHERE CustomerId = v_Collection_CustomerId AND
				PointOfSaleId = v_Collection_PointOfSaleId AND 
				EndTimeIdLocal= lv_TimeIdLocal AND
				CollectionTypeId = v_Collection_CollectionTypeId ) = 1 	THEN
					
					-- Need to check if it is duplicate record
					
					UPDATE TotalCollection
					SET
						CoinTotalAmount = CoinTotalAmount + v_Collection_CoinTotalAmount,
						BillTotalAmount = BillTotalAmount + v_Collection_BillTotalAmount,
						CardTotalAmount = CardTotalAmount + v_Collection_CardTotalAmount,
						TotalAmount = TotalAmount + v_CollectionTotal
					WHERE 
						CustomerId = v_Collection_CustomerId AND
						PointOfSaleId = v_Collection_PointOfSaleId AND
						EndTimeIdLocal= lv_TimeIdLocal AND
						CollectionTypeId = v_Collection_CollectionTypeId;
						
					-- Need a logging table to see the data ? Atleast till TEST evvironment
					-- Need to write code to make sure the same record should not be fetched again in the Cursor. 
					-- Otherwise amount will show wrong values as it will keepon updating
					
				ELSE
				
					INSERT INTO TotalCollection( POSCollectionId, 	CustomerId, 				PointOfSaleId, 				CollectionTypeId, 				EndTimeIdGMT, 	EndTimeIdLocal, CoinTotalAmount, 				BillTotalAmount, 				CardTotalAmount, 				TotalAmount) VALUES
											   ( v_Collection_Id,	v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId,	lv_TimeIdGMT,	lv_TimeIdLocal,	v_Collection_CoinTotalAmount,	v_Collection_BillTotalAmount,	v_Collection_CardTotalAmount,	v_CollectionTotal);
				
				
				END IF;
						
	
			INSERT INTO ArchiveStagingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
			DELETE FROM StagingPOSCollection WHERE Id = v_Collection_Id;
			
			-- DELETE FROM POSCollectionStaging WHERE ID = v_Collection_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;



/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inc
  
  Purpose:    For testing online Transactional data
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inc ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inc ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ClusterId  = 1;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ClusterId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ClusterId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	CALL sp_MigrateRevenueIntoKPI_Inc(lv_ETLExecutionLogId);
	
	UPDATE ETLExecutionLog
	SET ETLCompletedPurchase = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	CALL sp_MigrateSettledTransactionIntoKPI_Inc(lv_ETLExecutionLogId) ;
	
	CALL sp_MigrateCollectionIntoKPI_Inc(lv_ETLExecutionLogId);
   
   	UPDATE ETLExecutionLog
	SET ETLCompletedCollection = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;


DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthly` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthly` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_Previous_date = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check if data has been posted for previous day
	SELECT date(lv_Customer_LocalTime - INTERVAL 1 Day) INTO lv_Previous_date ;
	
	SELECT COUNT(*) INTO lv_record_cnt 
	FROM ETLMonthlyStatus WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId AND DataRunDate = lv_Previous_date ;
	
	IF (lv_record_cnt = 0) THEN
		
		-- means no data has been posted
		-- Get the data from UtilizationDay for previous day
		-- Get the begining TimeID of current month and next month
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
		
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
				
		SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth = day(LAST_DAY(lv_Previous_date)) and Quarterofday = 95;
		
		-- Think about the autoIncrement PK
		DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		
		-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	lv_Previous_date,	lv_ETLStartTime, lv_ETLEndTime );
		
		
	
	-- ELSE
	
		-- SELECT 'Already Processed for this date' AS  Trace;
	
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- 


DROP PROCEDURE IF EXISTS sp_UpdateUtilMonthly ;

delimiter //

CREATE PROCEDURE sp_UpdateUtilMonthly (
									   IN P_CustomerId MEDIUMINT UNSIGNED, 
									   IN P_LocationId MEDIUMINT UNSIGNED,
									   IN P_NumberOfPurchases MEDIUMINT UNSIGNED, 
									   IN P_TotalMinutesPurchased INT UNSIGNED,
									   IN P_BeginPermitGMT DATETIME,
									   IN P_Customer_Timezone char(25))
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_TimeIdBeginingMonthLocal,lv_cnt_UtilMonth MEDIUMINT UNSIGNED  DEFAULT NULL ;
declare continue handler for not found set NO_DATA=-1;


/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/
		-- Get the begining MonthIDLocal of the Customer
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
		and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;

		SELECT Count(*) INTO lv_cnt_UtilMonth FROM UtilizationMonth
		WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
		
		IF lv_cnt_UtilMonth = 1 THEN
			
			UPDATE UtilizationMonth SET NumberOfPurchases = NumberOfPurchases + P_NumberOfPurchases ,TotalMinutesPurchased = TotalMinutesPurchased + P_TotalMinutesPurchased
			WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
			
		-- ELSE
			-- SELECT ' IN ELSE';
		END IF ;
END//

delimiter ;

-- Process Monthly Utilization for past data

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthlyPastTxn` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthlyPastTxn` (IN P_Year SMALLINT,
														IN P_Month SMALLINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;

	SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
	
    -- Find the TimeIDLocal for Begining Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month and dayofmonth=1 and Quarterofday = 0;
	
	-- Find the TimeIDLocal for Last Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month  and IsLastDayOfMonth = 1 and Quarterofday = 95;
	
		
	-- Think about the autoIncrement PK
	DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
	INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
	SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
	FROM UtilizationDay
	WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
	GROUP by CustomerId, LocationId ; 
		
	-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
	SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
	
	INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	date(UTC_TIMESTAMP()),	lv_ETLStartTime, lv_ETLEndTime );
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

		

-- *************************************

/*


Procedure 


*/

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthly` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthly` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_Previous_date = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check if data has been posted for previous day
	SELECT date(lv_Customer_LocalTime - INTERVAL 1 Day) INTO lv_Previous_date ;
	
	SELECT COUNT(*) INTO lv_record_cnt 
	FROM ETLMonthlyStatus WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId AND DataRunDate = lv_Previous_date ;
	
	IF (lv_record_cnt = 0) THEN
		
		-- means no data has been posted
		-- Get the data from UtilizationDay for previous day
		-- Get the begining TimeID of current month and next month
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
		
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
				
		SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth = day(LAST_DAY(lv_Previous_date)) and Quarterofday = 95;
		
		-- Think about the autoIncrement PK
		DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		
		-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	lv_Previous_date,	lv_ETLStartTime, lv_ETLEndTime );
		
		
	
	-- ELSE
	
		-- SELECT 'Already Processed for this date' AS  Trace;
	
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- 


DROP PROCEDURE IF EXISTS sp_UpdateUtilMonthly ;

delimiter //

CREATE PROCEDURE sp_UpdateUtilMonthly (
									   IN P_CustomerId MEDIUMINT UNSIGNED, 
									   IN P_LocationId MEDIUMINT UNSIGNED,
									   IN P_NumberOfPurchases MEDIUMINT UNSIGNED, 
									   IN P_TotalMinutesPurchased INT UNSIGNED,
									   IN P_BeginPermitGMT DATETIME,
									   IN P_Customer_Timezone char(25))
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_TimeIdBeginingMonthLocal,lv_cnt_UtilMonth MEDIUMINT UNSIGNED  DEFAULT NULL ;
declare continue handler for not found set NO_DATA=-1;


/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/
		-- Get the begining MonthIDLocal of the Customer
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
		and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;

		SELECT Count(*) INTO lv_cnt_UtilMonth FROM UtilizationMonth
		WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
		
		IF lv_cnt_UtilMonth = 1 THEN
			
			UPDATE UtilizationMonth SET NumberOfPurchases = NumberOfPurchases + P_NumberOfPurchases ,TotalMinutesPurchased = TotalMinutesPurchased + P_TotalMinutesPurchased
			WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
			
		-- ELSE
			-- SELECT ' IN ELSE';
		END IF ;
END//

delimiter ;

-- Process Monthly Utilization for past data

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthlyPastTxn` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthlyPastTxn` (IN P_Year SMALLINT,
														IN P_Month SMALLINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;

	SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
	
    -- Find the TimeIDLocal for Begining Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month and dayofmonth=1 and Quarterofday = 0;
	
	-- Find the TimeIDLocal for Last Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month  and IsLastDayOfMonth = 1 and Quarterofday = 95;
	
		
	-- Think about the autoIncrement PK
	DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
	INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
	SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
	FROM UtilizationDay
	WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
	GROUP by CustomerId, LocationId ; 
		
	-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
	SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
	
	INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	date(UTC_TIMESTAMP()),	lv_ETLStartTime, lv_ETLEndTime );
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

		

-- ***********************************
DROP EVENT IF EXISTS EVENT_StartETL ;

CREATE EVENT EVENT_StartETL
 ON SCHEDULE EVERY 60 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inc();
					


-- ***********************************
DROP EVENT IF EXISTS EVENT_MigrateDailyUtilToMonthly ;

CREATE EVENT EVENT_MigrateDailyUtilToMonthly
 ON SCHEDULE EVERY 60 minute
 STARTS current_timestamp()
 DO
 CALL sp_MigrateDailyUtilToMonthly();
					