-- copied from GA branch 7.0 on April 4th


/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLStart_Inc
  
  Purpose:    For testing online Transactional data
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLStart_Inc ;

delimiter //

CREATE PROCEDURE sp_ETLStart_Inc ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 1;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	CALL sp_MigrateRevenueIntoKPI_Inc(lv_ETLExecutionLogId);
	
	UPDATE ETLExecutionLog
	SET ETLCompletedPurchase = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	CALL sp_MigrateSettledTransactionIntoKPI_Inc(lv_ETLExecutionLogId) ;
	
	UPDATE ETLExecutionLog
	SET ETLCompletedSettledAmount = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	CALL sp_MigrateCollectionIntoKPI_Inc(lv_ETLExecutionLogId);
   
   	UPDATE ETLExecutionLog
	SET ETLCompletedCollection = NOW()
	WHERE Id = lv_ETLExecutionLogId;
	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;




/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLMigrateDailyUtilToMonthly
  
  Purpose:    For migrating Daily Utilization to Monthly
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLMigrateDailyUtilToMonthly ;

delimiter //

CREATE PROCEDURE sp_ETLMigrateDailyUtilToMonthly ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 2;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(2,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 2 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (2, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
		
	CALL sp_MigrateDailyUtilToMonthly();
   
   	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

--  

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLMigrateHourlyOccupancyToDaily
  
  Purpose:    For migrating Hourly Occupancy to Daily
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLMigrateHourlyOccupancyToDaily ;

delimiter //

CREATE PROCEDURE sp_ETLMigrateHourlyOccupancyToDaily ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 3;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(3,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 3 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (3, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
		
	CALL sp_MigrateHourlyOccupancyToDaily();
   
   
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

-- cluster 4 


/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLGetLocationDetails
  
  Purpose:    For migrating Hourly Occupancy to Daily
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLGetLocationDetails ;

delimiter //

CREATE PROCEDURE sp_ETLGetLocationDetails ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 4;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(4,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 4 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (4, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
		
	 CALL sp_GetLocationDetails();
   
   	
	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;

-- Cluster 5

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_ETLDeletePastWidgetData
  
  Purpose:    For migrating Hourly Occupancy to Daily
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_ETLDeletePastWidgetData ;

delimiter //

CREATE PROCEDURE sp_ETLDeletePastWidgetData ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 5;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(5,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 5 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (5, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
		
	 CALL sp_DeleteWidgetData_Inc(60000,10);
   
   	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;


-- *************************************

/*


Procedure 


*/

		
-- Procedure to Reset ETLJOb incase of DBServerRestart or anyother unknow issues


DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_ETLReset` $$
CREATE PROCEDURE `sp_ETLReset` ()
BEGIN

DECLARE lv_ETLExecutionLogId BIGINT UNSIGNED ;
DECLARE lv_ETLResetInMin SMALLINT UNSIGNED ;

SELECT Value INTO lv_ETLResetInMin FROM EmsProperties WHERE Name = 'ETLResetInMin' ;

SELECT MAX(id) INTO lv_ETLExecutionLogId 
FROM ETLExecutionLog WHERE status = 1 AND timestampdiff(MINUTE,ETLStartTime,now()) >= lv_ETLResetInMin; 

IF (lv_ETLExecutionLogId > 1) THEN

	UPDATE ETLExecutionLog SET Status = 2, ETLEndTime= now(), Remarks = 'Reset'
	WHERE Id = lv_ETLExecutionLogId ;

	INSERT INTO ETLReset (ETLExecutionLogId,ResetTime) VALUES (lv_ETLExecutionLogId,now());
END IF ;



END $$

DELIMITER ;

--  new

-- Cluster 6

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_DeletePastActivePermit
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/
DROP PROCEDURE IF EXISTS sp_DeletePastActivePermit ;

delimiter //

CREATE PROCEDURE sp_DeletePastActivePermit ()
BEGIN

declare NO_DATA int(4) default 0;

declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 6;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(6,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 6 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (6, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
		
	 CALL sp_RemovePreviousActivePermit(100000);
   
   	
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;




-- end

-- FOR EMS-9622 To clear alerts for Houston

DROP PROCEDURE IF EXISTS sp_ETLClearActiveAlertsForHouston ;

delimiter //

CREATE PROCEDURE sp_ETLClearActiveAlertsForHouston ()
BEGIN

declare NO_DATA int(4) default 0;
DECLARE lv_count int;
DECLARE lv_status TINYINT;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 7;

IF (lv_count = 0 ) THEN

  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(7,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );
  set lv_status = 2;

ELSE

  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 7 )  ;
  
END IF;


If (lv_status = 2) THEN		-- means previous JOb process was sucessful

    
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (7, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
  
	-- Call this code to clear all Active Alerts for Houston	
	CALL sp_ClearActiveAlertsForCustomer(792);
	
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
	
		
end if;

END//

delimiter ;




--  EVENTS  ------------------------

-- ***********************************
DROP EVENT IF EXISTS EVENT_StartETL ;

CREATE EVENT EVENT_StartETL
 ON SCHEDULE EVERY 30 second
 STARTS current_timestamp()
 DO
 CALL sp_ETLStart_Inc();
					


-- ***********************************
DROP EVENT IF EXISTS EVENT_MigrateDailyUtilToMonthly ;

CREATE EVENT EVENT_MigrateDailyUtilToMonthly
 ON SCHEDULE EVERY 60 minute
 STARTS current_timestamp()
 DO
 CALL sp_ETLMigrateDailyUtilToMonthly();


-- ***********************************
DROP EVENT IF EXISTS EVENT_ETLReset ;

CREATE EVENT EVENT_ETLReset
 ON SCHEDULE EVERY 180 minute
 STARTS current_timestamp()
 DO
 CALL sp_ETLReset();

-- ***********************************
DROP EVENT IF EXISTS EVENT_MigrateHourlyOccupancyToDaily ;

CREATE EVENT EVENT_MigrateHourlyOccupancyToDaily
 ON SCHEDULE EVERY 60 minute
 STARTS current_timestamp()
 DO
 CALL sp_ETLMigrateHourlyOccupancyToDaily();
 
 -- ***********************************
DROP EVENT IF EXISTS EVENT_FillETLLocationDetails ;

CREATE EVENT EVENT_FillETLLocationDetails
 ON SCHEDULE EVERY 15 minute
 STARTS current_timestamp()
 DO
 CALL sp_ETLGetLocationDetails();
 
 
  -- ***********************************
DROP EVENT IF EXISTS EVENT_DeletePastWidgetData;

CREATE EVENT EVENT_DeletePastWidgetData
 ON SCHEDULE EVERY 1440 minute
 STARTS concat(DATE_ADD(current_date(),INTERVAL 1 DAY),' 02:00:00')
 DO
 CALL sp_ETLDeletePastWidgetData();
 
 -- This needs to be enabled after the EVENT_DeletePastWidgetData is run for the first time
 
 -- alter event EVENT_ETLReset disable;
 
 
 
  -- ***********************************
DROP EVENT IF EXISTS EVENT_RemovePastActivePermit;

CREATE EVENT EVENT_RemovePastActivePermit
 ON SCHEDULE EVERY 240 minute
 STARTS current_timestamp()
 DO
 CALL sp_DeletePastActivePermit();
 

 
 -- EMS-9622 To clear alerts to Houston On Monday
 
  -- ***********************************
DROP EVENT IF EXISTS EVENT_ClearAllAlertsForHoustonOnMonday;

CREATE EVENT EVENT_ClearAllAlertsForHoustonOnMonday
 ON SCHEDULE EVERY 7 DAY 
 STARTS concat(CURRENT_DATE + INTERVAL 0 - WEEKDAY(CURRENT_DATE) DAY,' 01:00:00')
 DO
 CALL sp_ETLClearActiveAlertsForHouston();
 
 
  -- EMS-9622 To clear alerts to Houston On Wednesday
 
  -- ***********************************
DROP EVENT IF EXISTS EVENT_ClearAllAlertsForHoustonOnWednesday;

CREATE EVENT EVENT_ClearAllAlertsForHoustonOnWednesday
 ON SCHEDULE EVERY 7 DAY 
 STARTS concat(CURRENT_DATE + INTERVAL 2 - WEEKDAY(CURRENT_DATE) DAY,' 01:00:00')
 DO
 CALL sp_ETLClearActiveAlertsForHouston();
 
 -- EMS-9622 To clear alerts to Houston On Friday
 
  -- ***********************************
DROP EVENT IF EXISTS EVENT_ClearAllAlertsForHoustonOnFriday;

CREATE EVENT EVENT_ClearAllAlertsForHoustonOnFriday
 ON SCHEDULE EVERY 7 DAY 
 STARTS concat(CURRENT_DATE + INTERVAL 4 - WEEKDAY(CURRENT_DATE) DAY,' 01:00:00')
 DO
 CALL sp_ETLClearActiveAlertsForHouston();
 
 -- EMS-6200 Update LastModifiedGMT in UnifiedRate
 
 DROP EVENT IF EXISTS EVENT_UpdateLastModifiedGMTForUnifiedRate;

CREATE EVENT EVENT_UpdateLastModifiedGMTForUnifiedRate
 ON SCHEDULE EVERY 15 DAY 
 STARTS '2015-07-15 04:00:00'
 DO
 CALL sp_UpdateLastModifiedDateOnUnifiedRate();
 
 -- EMS-10037 Move records from PreAuthHolding to CardRetry
 
 DROP EVENT IF EXISTS EVENT_AutomateElavonPreAuthHoldingToRetry;

CREATE EVENT EVENT_AutomateElavonPreAuthHoldingToRetry
 ON SCHEDULE EVERY 5 minute 
 STARTS current_timestamp()
 DO
 CALL sp_AutomateElavonPreAuthHoldingToRetry();

 