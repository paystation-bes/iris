-- --------------------------------------------------------------------------------------------------------
-- Procedure:   sp_LoadSettingsFileContent
--
-- Purpose:     Get SettingsFileContent zip files and put them on to a 
--				database server directory. This is not a replacement of 
--				Ashok's sp_GetSettingsFileContent, I couldn't find the
--				procedure.
--
-- Call:        CALL sp_LoadSettingsFileContent();
--
-- Note: Please change line 28 with proper destination.
-- ------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_LoadSettingsFileContent;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_LoadSettingsFileContent`()
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE sfId MEDIUMINT DEFAULT 0;
    
    DECLARE cur CURSOR FOR
		SELECT SettingsFileId
        FROM SettingsFileContent;
	
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	
    -- TODO: Set destination directory
    SET @path = 'C://Temp//BOSS//';
    
	OPEN cur;
    readLoop: LOOP
				FETCH cur INTO sfId;
                IF done = 1 THEN 
					LEAVE readLoop;
                END IF;

				SET @fileName = CONCAT(@path, 
									   'SettingsFileContent_',
									   sfId, 
                                       '.zip');

				SET @query = CONCAT("SELECT Content INTO DUMPFILE '", 
									@fileName, 
								    "' FROM SettingsFileContent sfc WHERE sfc.SettingsFileId = ",
                                    sfId);
                
                PREPARE finalQuery FROM @query;
                EXECUTE finalQuery;
                DEALLOCATE PREPARE finalQuery;
                
	END LOOP readLoop;
	CLOSE cur;

END//
DELIMITER ;
