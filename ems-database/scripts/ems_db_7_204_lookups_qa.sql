SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



-- -----------------------------------------------------
-- Table `EmsProperties`
-- -----------------------------------------------------

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumLoginAttempt',             '3',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumLoginLockUpMinutes',       '5',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ServerUrl',                       'http://172.16.5.10:8080',               UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MailServerUrl',                   'localhost',                             UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ServiceAgreementToEmailAddress',  'dpt.emsbilling@digitalpaytech.com',     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumSessionTimeoutMinutes',    '5',                                     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumWidgetDataRows',           '2000',                                  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumPastCustomerAdminNotifications', '5',                               UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumCcRetryTimes',             '10',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaximumRetryTimes',               '30',                                    UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerDefaultTimeZone',	     'US/Pacific',						      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ScheduledCardProcessingServer',	 'QAAPP01',						          UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CryptoDirectory',	             '/opt/ems/certs',	                      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EncryptionMode', 	             'CryptoServer',	                          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MinIntervalToChangeExternalKey',	 '30',	                                  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAccountName',	                 'QA Testing',	                          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAccountToken',	             '3479CUMlMvRRcs2we68hHHwrMnYVfxGoL9xRi', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceLocation',	         'https://sign.digitalpaytech.com:5005/services/SigningService', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAdminAlertToEmailAddress',	 'ems@digitalpaytech.com',                UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsAlertFromEmailAddress',	      'no-reply@digitalpaytech.com',          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('HSMDir',	                         '/opt/nfast',                            UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('AutoCryptoKeyProcessingServer',	 'VANAPP01',                              UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DisplayCardProcessingQueueStatus','1',                                     UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EmsVersion', 					 'EMS v7.0.41',						  	  UTC_TIMESTAMP(), 1) ;	
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ConcordEmsVersion', 				 'EMS v6.2.0.28',						  UTC_TIMESTAMP(), 1) ;	
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FirstDataNashvilleMCC', 			 '7523',						  		  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovAuth', 					 'ExecuteAuth.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovCharset', 				 'ISO-8859-1',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovConnectionTimeout', 		 '4000',								  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovConvenienceFee', 		 '0',								  	  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovRefund', 				 'ExecuteRefund.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovSale', 					 'ExecuteCharge.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('Link2GovSettle', 				 'ExecuteSettle.aspx',					  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ParcxmartConnectionTimeout', 	 '2000',								  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardAuthorizationBackUpMaxHoldingDays',	'90',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnectionTimeout',           '5000',                                UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumberOfBatchReencryptPeakTime',	  '2',							          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumberOfBatchReencryptOffPeakTime','45',							          UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SleepInPeakTimeInMilliSecond',	  '1000',							      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SleepInOffPeakTimeInMilliSecond',  '200',							      UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('StartPeakTime',	                  '14:00:00',							  UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EndPeakTime',	                  '07:00:00',							  UTC_TIMESTAMP(), 1) ;

-- Reporting variables`
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsPdfTransactionDetailsMaxRecords',		'50000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsCsvTransactionDetailsMaxRecords',		'50000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTransactionSummaryMaxRecords',	  	'1000',			UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsPdfAuditDetailsMaxRecords',  			'5000',			UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsCsvAuditDetailsMaxRecords',			'50000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsAuditSummaryMaxRecords',	            '200000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsMaxRecordsUsingVirtualizer',	        '57000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTaxesMaxRecords',	                    '100000',		UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSizeLimitKb',	              			'10240',				UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSizeWarningKb',	           			'9216',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsZippedLimitMb',	              		'9',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTimeLimitDays',	              		'45',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsTimeWarningDays',	              		'40',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsSystemAdminTimeLimitDays',	        '1',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsQueueTime',	              			'03:00',				UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsFileNameTimeFormat',	           		'MM dd yyyy h.mm.ss a',	UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportsQueueIdleTimeLimitMinutes',	        '30',					UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ReportBackgroundServer',	        			'VANAPP01',				UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RerouteToEms6Url', 'url/for/rereroute', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RunningTotalCalcInterval', '300', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('NumPaystationsBeExecuted ', '25', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PreAuthDelayTime', '180'        , UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardStoreForwardThreadCount', 	'1', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardStoreForwardQueueLimit', 	'10000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardSettlementThreadCount', 		'10', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CardReversalThreadCount ', 		'1', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SensorDataBatchCount', '100', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SensorProcessingThreadCount', '1', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveSensorPaystationBatchSize', '20', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ArchiveMaxDaysForSersorLogAndBatteryInfo', '90', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DisableProcUploadTranFile', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FileParserThreadCount', '1', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CsvUploadProcessingMillisPerThousand', '1000', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsRateInfoCacheSize', '500', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsSimulatedEnv', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IsSMSCallBackIPValidate', '0', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SmsCallBackIPAddress', '69.28.206.213-69.28.206.215', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MaxUploadFileSizeInBytes', '10485760', UTC_TIMESTAMP(), 1) ; 
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MappingURL', 'https://maps.digitalpaytech.com/', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IncrementalMigrationLimit', 4000000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationWaitTime', 2592000000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ETLLimit', 1500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ETLResetInMin', 120, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('IncrementalMigrationCardRetryLimit', 4000000, UTC_TIMESTAMP(), 1) ;

-- CustomerMigration Values
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationServer', 'VANAPP001', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RunDailyCustomerMigration', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationBatchSize', 10, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CMProcessDelayInHours', 48, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DataValidationWaitTimeInMins', 15, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CMHeartbeatDelayInMins', 60, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DefaultEMS6URLRerouteId', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationAlertEmail', 'no-reply@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CustomerMigrationITEmail', 'no-reply@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TransactionMaxRetryCount', 1, UTC_TIMESTAMP(), 1) ;

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RotateKeyInIris',	              '0',								      UTC_TIMESTAMP(), 1) ;

-- Mobile API
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MobileSessionTokenExpiryInterval', 60, UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('MobileSignatureVersion', '1', UTC_TIMESTAMP(), 1);

-- Update IsTest to 1 for all Processors in QA Environment

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SingleTrxCardDataStoreInDays', '30', UTC_TIMESTAMP(), 1);

-- Dublicate serialNumber alerts
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DuplicateCheckCount', '3', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DuplicateCheckEmail', 'dptcs@digitalpaytech.com', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('DPTCustomerId',  (SELECT Id FROM Customer WHERE Name='Digital (DPT)'), UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('HashAlgorithmTypeSha1Id', '1', UTC_TIMESTAMP(), 1) ;

-- Signing Service V2
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2Location', 'http://172.16.1.63:8080/services/SigningServiceV2', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2AccountName', 'SigningAdmin', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('SigningServiceV2Token', 'fWJY7K3SYzB97aEjV1DO+Q==', UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CollectionUserIntervalMins', '10', UTC_TIMESTAMP(), 1);
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PlateReportReturnSize', '1000', UTC_TIMESTAMP(), 1);

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ProdSupportEmailAddress', 'prodsupport@digitalpaytech.com', UTC_TIMESTAMP(), 1) ;

-- Authorize.Net default x_solution_id
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('AuthorizeNetSolutionId', 'A1000071', UTC_TIMESTAMP(), 1);

-- Flex
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexDataExtractionProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMapWeekInterpolation', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationWeekInterpolation', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationOffsetMinute', '15', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMaxRowCount', '1000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('FlexCitationMaxTryLimit', '20', UTC_TIMESTAMP(), 1) ;

-- Elavon viaConex
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonVersion', '4017', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRegistrationKey', 'TEMP_PORTAL_FAKE_KEY', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonApplicationID', 'TZKQ01GC', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonCloseBatchCount', '7500', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRequestTimeOut', '30', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PostAuthTimeOut', '60', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('RealTimeElavonRetryAttempts', '12', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRetryWaitingTime', '10000', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsRealTime', 2000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsRealTime', 7000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsSfSinglePhase', 2500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsSfSinglePhase', 3500, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsCloseBatch', 2000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsCloseBatch', 5000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonConnTimeoutMsDefault', 3000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReadTimeoutMsDefault', 8000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonPreAuthExpiryTimeInHours', 1, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReversalTimeoutMaxRetryTimes', 3, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonReversalMaxRetryTimes', 3, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRedisCloseBatchLockPerTxnMs', 30000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonRedisLockTxnMs', 30000, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonCloseTimeIntervalMinutes', 15, UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ElavonMaxPreAuthReversalLimit', 100, UTC_TIMESTAMP(), 1) ;


-- Case
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseDataExtractionProcessingServer', 'VANAPP01', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseAuthorization', 'Bearer ZTBjNWQwZjUxZDMwNjZkZmNmOTdiZDA4MDczMTM0MjA=', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseURL', 'http://54.197.238.92', UTC_TIMESTAMP(), 1) ;
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('CaseRequestTimeoutTime', '300000', UTC_TIMESTAMP(), 1) ;

-- Execute Patch
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ExecutePatch', '0', UTC_TIMESTAMP(), 1) ;

-- Execute Patch
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ExecutePatch', '0', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `RestProperty`
-- -----------------------------------------------------

INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'DomainName'                    , 'qa.digitalpaytech.com', UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'PermitURI'                     , '/REST/Permit'      , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'LocationURI'                   , '/REST/Location'    , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'RESTSessionTokenExpiryInterval', '30'                , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'SignatureVersion'              , '1'                 , UTC_TIMESTAMP(), 1) ;
INSERT RestProperty (Id, Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'TokenURI'                      , '/REST/Token'       , UTC_TIMESTAMP(), 1) ;

-- Processors

UPDATE Processor
SET 
IsTest = 1 ;


-- Updating IsForValueCard flags to 1 for BlackBoard,TotalCard and NuVision Processors in QA and Production enviroments

UPDATE Processor
SET 
IsForValueCard = 1
WHERE Id IN ( 13, 14, 15) ;





-- ------------------------------------------------
-- ArchiveMaxDaysForSersorLogAndBatteryInfo	90
-- ArchiveProcessingServer	VANAPP01
-- ArchiveSensorPaystationBatchSize	20
-- AutoCryptoKeyProcessingServer	VANAPP01
-- CardChargeRetryThreadCount	1
-- CardProcessingQueueStatus	1
-- CardProcessorAllianceIsTest	1
-- CardProcessorAuthorizeNetIsTest	1
-- CardProcessorConcordIsTest	1
-- CardProcessorFirstHorizonIsTest	1
-- CardProcessorMonerisIsTest	0
-- CardProcessorOptimalIsTest	1
-- CardProcessorPaymentechIsTest	1
-- CardStoreForwardThreadCount	1
-- DisableCompressionCheck	1
-- EmsAccountName	QA Testing
-- EmsAccountToken	3479CUMlMvRRcs2H9oSzHwrMnYVfxGoL9xRi
-- EmsAdminAlertToEmailAddress	alan.wong@digitalpaytech.com,martin.yeung@digitalpaytech.com,mariko.liza.tikhova@digitalpaytech.com,arash.taheri@digitalpaytech.com,daniel.agyar@digitalpaytech.com,abel.tse@digitalpaytech.com
-- EmsAlertFromEmailAddress	qa2@digitalpaytech.com
-- EmsTest	1
-- EmsVersion	EMS v6.2.1
-- EncryptionMode	Production
-- GoogleAnalyticsAccount	UA-2956139-2
-- HSMDir	/opt/nfast
-- IsSMSCallBackIPValidate	0
-- LastHeartBeat	1325780848260
-- LicenseServer	VANAPP01
-- LicenseServiceToken	TSNpuJrOCEJVPrk15kNJ0MlipbUSY8fi
-- LicenseServiceUrl	https://cerberus.digitalpaytech.com:5006/cerberus_v2/services/CerberusService
-- LicenseServiceUsername	OzfIJI0edmsQBBoIouTJMYmT8zVs9s2I
-- MailServerUrl	localhost
-- MaxAlertsPerTypeAndCustomer	10
-- MaximumLoginAttempt	10
-- MaximumLoginLockUpMinutes	5
-- MinIntervalToChangeExternalKey	0
-- ParcxmartServerConnectionTimeout	4000
-- PlateReportReturnSize	1000
-- PushDataBatchCount	100
-- PushDataClusterServerName	VANAPP01
-- PushDataMaxWaitingTime	60000
-- PushDataProcessingThreadCount	2
-- reportsAuditSummaryMaxRecords	100000
-- reportsCsvAuditDetailsMaxRecords	100000
-- reportsCsvTransactionDetailsMaxRecords	100000
-- reportsMaxRecordsUsingVitualizer	30000
-- reportsPdfAuditDetailsMaxRecords	100000
-- reportsPdfTransactionDetailsMaxRecords	100000
-- reportsTaxesMaxRecords	100000
-- ScheduledCardProcessingServer	VANAPP01
-- SensorDataBatchCount	100
-- SensorProcessingThreadCount	1
-- ServerOrganization	LAB-QA2
-- ServerUrl	http://qa2.digitalpaytech.com
-- ServiceAgreementToEmailAddress	 dpt.emsbilling@gmail.com
-- SigningServiceLocation	https://sign.digitalpaytech.com:5005/services/SigningService
-- SMSCallBackIPAddress	69.28.206.213-69.28.206.215
-- SmsRateInfoCacheSize	500
-- SmsSimulatedEnv	0
-- SSLWebServiceLists	PlateInfoService | AuditInfoService
-- XmlRpcThreadCount	100
-- HashAlgorithmTypeSha1Id	1
-- SigningServiceV2Location 	http://qa.digitalpaytech.com
-- SigningServiceV2AccountName  SigningAdmin
-- SigningServiceV2Token		fWJY7K3SYzB97aEjV1DO+Q==
-- PlateReportReturnSize	1000
-- CollectionUserIntervalMins	10
-- AuthorizeNetSolutionId	A1000071
-- ProdSupportEmailAddress		morgan.madu@digitalpaytech.com
-- FlexDataExtractionProcessingServer	VANAPP01
-- FlexCitationMapWeekInterpolation		34
-- FlexCitationWeekInterpolation	82
-- ElavonVersion	4017
-- ElavonRegistrationKey				TEMP_PORTAL_FAKE_KEY
-- ElavonApplicationID					TZKQ01GC
-- ElavonCloseBatchCount				7500
-- ElavonRequestTimeOut					30
-- PostAuthTimeOut						60
-- RealTimeElavonRetryAttempts			3
-- ElavonRetryWaitingTime				3
-- ElavonConnTimeoutMsRealTime			2000
-- ElavonReadTimeoutMsRealTime			3000
-- ElavonConnTimeoutMsSfSinglePhase		2500
-- ElavonReadTimeoutMsSfSinglePhase		3500
-- ElavonConnTimeoutMsCloseBatch		2000
-- ElavonReadTimeoutMsCloseBatch		2000
-- ElavonConnTimeoutMsDefault			3000
-- ElavonReadTimeoutMsDefault			3000
-- ElavonRedisCloseBatchLockPerTxnMs    350
-- ElavonRedisLockTxnMs                 1100
-- ElavonCloseTimeIntervalMinutes       15
-- ------------------------------------------------


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
