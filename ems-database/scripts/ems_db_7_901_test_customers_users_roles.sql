SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `Notification` 
-- -----------------------------------------------------

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('Welcome to EMS 7', 'Welcome to EMS 7.0, the next generation of our EMS product line. With a new interface, data visualization technologies, improved data storage, updated backend logic, and reporting capabilities this product will be the talk of the industry in 2013.', 'http://www.digitalpaytech.com', '2012-01-01', '2013-09-01', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('EMS 7 Meets QA', 'The EMS 7 code base is now being submitted to QA', 'http://www.digitalpaytech.com', '2012-10-01', '2012-10-31', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('EMS 7 Test Data', 'Test data for EMS 7 begins to arrive', 'http://www.digitalpaytech.com', '2012-10-15', '2013-01-01', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('EMS 7 Card Processing', 'Great progress has been made on the EMS 6 to EMS 7 data migration  ', 'http://www.digitalpaytech.com', '2012-11-01', '2012-12-31', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('EMS 7 Status Update 1-Jan-2013', 'Development milestones are being met. Expect a fabulous product.', 'http://www.digitalpaytech.com', '2013-01-01', '2013-01-15', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('Major Milestone - Customer Admin', 'Pay Station Placement, the final part for Customer Adminstration has been delivered to verification. Great job everyone!', 'http://www.digitalpaytech.com', '2013-01-22', '2013-01-31', 0, 0, UTC_TIMESTAMP(), 1);

INSERT Notification (Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES ('Updated Default Dashboard', 'Check out the new default dashboard. Looks good Rob!', 'http://www.digitalpaytech.com', '2013-05-07', '2013-05-13', 0, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `Customer`: was using INSERT statements, now using sp_InsertCustomer which completes the customer-data-object in the database
-- -----------------------------------------------------

-- Id = 2 = 'Park America Inc.'
-- Parameters          (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId,        CustomerName, TrialExpiryGMT, IsParent,   UserName,                                   Password, IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell, IsDigitalCollect, IsOnlineConfiguration,  IsFlexIntegration, IsCaseIntegration, IsDigitalAPIXChange, Timezone, UserId,     p_PasswordSalt)
CALL sp_InsertCustomer (             2,                    1,             NULL, 'Park America Inc.',           NULL,        1, 'sarasara', '58ae83e00383273590f96b385ddd700802c3f07d',                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                   1,                1,                     1,                  1,                 1,                   1, 'GMT',      1, 'SaltSaltSaltSalt');

-- Id = 3 = 'Park San Diego'
-- Parameters          (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId,     CustomerName,    TrialExpiryGMT, IsParent,   UserName,                                   Password, IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell, IsDigitalCollect, IsOnlineConfiguration,  IsFlexIntegration, IsCaseIntegration, IsDigitalAPIXChange, Timezone, UserId,     p_PasswordSalt)
CALL sp_InsertCustomer (             3,                    1,                2, 'Park San Diego',              NULL,        0, 'momomomo', '58ae83e00383273590f96b385ddd700802c3f07d',                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                   1,                1,                     1,                  1,                 1,                   1, 'US/Pacific',      1, 'SaltSaltSaltSalt');

-- Id = 4 = 'Park & Fly'
-- Parameters          (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, CustomerName,        TrialExpiryGMT, IsParent,   UserName,                                   Password, IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell, IsDigitalCollect, IsOnlineConfiguration,  IsFlexIntegration, IsCaseIntegration, IsDigitalAPIXChange, Timezone, UserId,     p_PasswordSalt)
CALL sp_InsertCustomer (             3,                    1,             NULL, 'Park & Fly',                  NULL,        0, 'dededede', '58ae83e00383273590f96b385ddd700802c3f07d',                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                   1,                1,                     1,                  1,                 1,                   1, 'GMT',      1, 'SaltSaltSaltSalt');

-- Id = 5 = 'Park Dallas'
-- Parameters          (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, CustomerName,        TrialExpiryGMT, IsParent,   UserName,                                   Password, IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell, IsDigitalCollect, IsOnlineConfiguration,  IsFlexIntegration, IsCaseIntegration, IsDigitalAPIXChange, Timezone, UserId,     p_PasswordSalt)
CALL sp_InsertCustomer (             3,                    1,                2, 'Park Dallas',                 NULL,        0, 'jilljill', '58ae83e00383273590f96b385ddd700802c3f07d',                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                   1,                1,                     1,                  1,                 1,                   1, 'US/Central',      1, 'SaltSaltSaltSalt');

-- INSERT Customer (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, Name, TrialExpiryGMT, IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- Id = 2
-- VALUES (2, 1, NULL, 'Park America Inc.', NULL, 1, 0, UTC_TIMESTAMP(), 1);

-- INSERT Customer (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, Name, TrialExpiryGMT, IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- Id = 3
-- VALUES (3, 1, 2, 'Park San Diego', NULL, 0, 0, UTC_TIMESTAMP(), 1);

-- INSERT Customer (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, Name, TrialExpiryGMT, IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- Id = 4
-- VALUES (3, 1, NULL, 'Park & Fly', NULL, 0, 0, UTC_TIMESTAMP(), 1);

-- INSERT Customer (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, Name, TrialExpiryGMT, IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- Id = 5
-- VALUES (3, 1, 2, 'Park Dallas', NULL, 0, 0, UTC_TIMESTAMP(), 1);

-- INSERT Customer (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId, Name, TrialExpiryGMT, IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- not used yet
-- VALUES (3, 1, 2, 'Park Trenton', NULL, 0, 0, UTC_TIMESTAMP(), 1);

-- Change Park & Fly to not Migrated
UPDATE Customer SET IsMigrated = 0 WHERE Name = 'Park & Fly';

-- -----------------------------------------------------
-- Table `CustomerGMTDST`: this table is not part of EMS 7, but it is used in the creation of test data
-- -----------------------------------------------------

INSERT CustomerGMTDST (CustomerId,GMTtoLocal,DSTtoLocal,LastModifiedGMT,LastModifiedByUserId)   -- Park San Diego (US/Pacific)
VALUES                (3         ,'-08:00'  ,'-07:00'  ,UTC_TIMESTAMP(),1                   );

INSERT CustomerGMTDST (CustomerId,GMTtoLocal,DSTtoLocal,LastModifiedGMT,LastModifiedByUserId)
VALUES                (5         ,'-06:00'  ,'-05:00'  ,UTC_TIMESTAMP(),1                   );  -- Park Dallas (US/Central)

-- INSERT CustomerGMTDST (CustomerId,GMTtoLocal,DSTtoLocal,LastModifiedGMT,LastModifiedByUserId)   -- Park Trenton (US/Eastern)
-- VALUES                (6         ,'-05:00'  ,'-04:00'  ,UTC_TIMESTAMP(),1                   );


-- -----------------------------------------------------
-- Table `CustomerAlertType`
--
-- Note: When creating a user defined `CustomerAlertType` record in the database the business layer (java code) will also perform inserts into the `ActivePOSAlert` table (one row for each `PointOfSale` related to the new `CustomerAlertType`)
-- -----------------------------------------------------
-- Customer.Id = 3 = `Park San Francisco`
INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           1,       NULL,    NULL, 'Last Seen Interval 2 Hours'    ,         2,        1,         0,       0, UTC_TIMESTAMP(), 1); 

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           2,       NULL,    NULL, 'Running Total Dollars 500'     ,       500,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           6,       NULL,    NULL, 'Coin Canister Count 200'       ,       200,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           7,       NULL,    NULL, 'Coin Canister Dollars 300'     ,       300,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           8,       NULL,    NULL, 'Bill Stacker Count 50'               ,        50,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,           9,       NULL,    NULL, 'Bill Stacker Dollars 250'      ,       250,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,          10,       NULL,    NULL, 'Unsettled CC Count 20'         ,        20,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         3,          11,       NULL,    NULL, 'Unsettled CC Dollars 200'      ,       200,        1,         0,       0, UTC_TIMESTAMP(), 1);

-- Customer.Id = 5 = `Park Dallas`
INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           1,       NULL,    NULL, 'Last Seen Interval 1 Hour'     ,         1,        1,         0,       0, UTC_TIMESTAMP(), 1); 

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           2,       NULL,    NULL, 'Running Total Dollars 350'     ,       350,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           6,       NULL,    NULL, 'Coin Canister Count 100'       ,       100,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           7,       NULL,    NULL, 'Coin Canister Dollars 200'     ,       200,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           8,       NULL,    NULL, 'Bill Stacker Count 40'         ,        40,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,           9,       NULL,    NULL, 'Bill Stacker Dollars 200'           ,       200,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,          10,       NULL,    NULL, 'Unsettled CC Count 10'         ,        10,        1,         0,       0, UTC_TIMESTAMP(), 1);

INSERT CustomerAlertType (CustomerId, AlertThresholdTypeId, LocationId, RouteId, Name                            , Threshold, IsActive, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                   (         5,          11,       NULL,    NULL, 'Unsettled CC Dollars 100'      ,       100,        1,         0,       0, UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Table `ForeignKeyXref`: identifies Customers and home Regions/Locations for Paystations
-- -----------------------------------------------------

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (63            ,14382       ,3247            ,3         ,12        ,1            ,1           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (63            ,14382       ,3248            ,3         ,12        ,2            ,2           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (63            ,14382       ,3252            ,3         ,12        ,3            ,3           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (398           ,1566        ,3090            ,3         ,10        ,4            ,4           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (398           ,1566        ,3089            ,3         ,10        ,5            ,5           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (316           ,1202        ,2370            ,3         ,11        ,6            ,6           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (316           ,1202        ,2371            ,3         ,11        ,7            ,7           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (601           ,3891        ,6391            ,3         ,9         ,8            ,8           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (601           ,3891        ,6401            ,3         ,9         ,9            ,9           ,UTC_TIMESTAMP(),1                   );
        
INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (316           ,1203        ,2366            ,3         ,6         ,10           ,10          ,UTC_TIMESTAMP(),1                   );
    
INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (316           ,1092        ,2367            ,3         ,7         ,11           ,11          ,UTC_TIMESTAMP(),1                   );
 
INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (113           ,369         ,723             ,3         ,8         ,12           ,12          ,UTC_TIMESTAMP(),1                   );
        
INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (113           ,369         ,724             ,3         ,8         ,13           ,13          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (3061          ,29701       ,49251           ,5         ,14        ,14           ,14          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (3061          ,29701       ,49252           ,5         ,14        ,15           ,15          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (3061          ,29701       ,49262           ,5         ,14        ,16           ,16          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (3061          ,29701       ,49272           ,5         ,14        ,17           ,17          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)
VALUES                (3061          ,29701       ,50821           ,5         ,14        ,18           ,18          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   -- Used for mapping transactions from a PS to a non-home location
VALUES                (63            ,1634        ,3252            ,3         ,13        ,3            ,3           ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   -- Used for mapping transactions from a PS to a non-home location
VALUES                (3061          ,28781       ,49251           ,5         ,3         ,14           ,14          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   -- Used for mapping transactions from a PS to a non-home location
VALUES                (3061          ,28781       ,49252           ,5         ,3         ,15           ,15          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   -- Used for mapping transactions from a PS to a non-home location
VALUES                (3061          ,28781       ,49272           ,5         ,3         ,17           ,17          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   -- Used for mapping transactions from a PS to a non-home location
VALUES                (3061          ,28781       ,50821           ,5         ,3         ,18           ,18          ,UTC_TIMESTAMP(),1                   );

-- Customer.Id = 3 = Park San Diego, Location.Id = 15 = Coronado Beach
INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   
VALUES                (3713          ,37553       ,58523           ,3         ,15        ,19           ,19          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   
VALUES                (3713          ,37553       ,58424           ,3         ,15        ,20           ,20          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   
VALUES                (3713          ,37553       ,58443           ,3         ,15        ,21           ,21          ,UTC_TIMESTAMP(),1                   );

INSERT ForeignKeyXref (CustomerIdEMS6,RegionIdEMS6,PaystationIdEMS6,CustomerId,LocationId,PointOfSaleId,PaystationId,LastModifiedGMT,LastModifiedByUserId)   
VALUES                (3713          ,37553       ,58453           ,3         ,15        ,22           ,22          ,UTC_TIMESTAMP(),1                   );

-- -----------------------------------------------------
-- Table `Location`
-- -----------------------------------------------------

-- Customer.Id = 3 = Park San Diego
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 4
VALUES          (3         ,NULL            ,'East'              ,0             ,0                   ,NULL       ,1       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );
  
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 5
VALUES          (3         ,NULL            ,'West'              ,0             ,0                   ,NULL       ,1       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );
  
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 6
VALUES          (3         ,4               ,'1201 J Street'     ,65            ,4000000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );
 
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 7
VALUES          (3         ,4               ,'983 Island Ave'    ,75            ,1800000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );
 
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 8
VALUES          (3         ,4               ,'1503 Broadway'     ,45            ,100000              ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 9
VALUES          (3         ,5               ,'Tidelands Park'    ,205           ,1100000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 10
VALUES          (3         ,5               ,'Centennial Park'   ,484           ,2400000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 11
VALUES          (3         ,5               ,'Convention Center' ,163           ,7000000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 12
VALUES          (3         ,5               ,'Embarcadero Marina',146           ,6500000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 13
VALUES          (3         ,5               ,'Marina Overflow'   ,150           ,500000              ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

-- Customer.Id = 5 = Park Dallas 
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 14
VALUES          (5         ,NULL            ,'South Dallas'      ,500           ,1300000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );

-- Customer.Id = 3 = Park San Diego
INSERT Location (CustomerId,ParentLocationId,Name                ,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId)   -- Id = 15
VALUES          (3         ,NULL            ,'Coronado Beach'    ,275           ,3000000             ,NULL       ,0       ,0           ,0        ,0      ,UTC_TIMESTAMP(),1                   );
  

/* Location `1201 J Street` */
CALL sp_InsertLocationDayAndOpen ( 6, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 5,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 6, 7,  0, 95, 1, 0, 1);

/* Location `983 Island Ave` */
CALL sp_InsertLocationDayAndOpen ( 7, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 5,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 7, 7,  0, 95, 1, 0, 1);

/* Location `1503 Broadway`: closed weekends, open 5:00 am to 8:00 pm on weekdays */
CALL sp_InsertLocationDayAndOpen ( 8, 1,  0, 0,  0, 1, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 2, 20, 79, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 3, 20, 79, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 4, 20, 79, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 5, 20, 79, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 6, 20, 79, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 8, 7,  0,  0, 0, 1, 1);

/* Location `Tidelands Park`: closed weekends, open 3:30 am to 11:00 pm on weekdays except Thursday when open at 1:00 am */
CALL sp_InsertLocationDayAndOpen ( 9, 1,  0,  0, 0, 1, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 2, 14, 91, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 3, 14, 91, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 4, 14, 91, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 5,  4, 91, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 6, 14, 91, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen ( 9, 7,  0,  0, 0, 1, 1);

/* Location `Centennial Park` */
CALL sp_InsertLocationDayAndOpen (10, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 5,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (10, 7,  0, 95, 1, 0, 1);

/* Location `Convention Center` */
CALL sp_InsertLocationDayAndOpen (11, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (11, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (11, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (11, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (11, 5,  0, 95, 1, 0, 1); 
CALL sp_InsertLocationDayAndOpen (11, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (11, 7,  0, 95, 1, 0, 1);

/* Location `Embarcadero Marina`: open at 5:00 am on weekends, open at 3:45 am week days except Wed when open at midnight, always closes at 11:30 pm */
CALL sp_InsertLocationDayAndOpen (12, 1, 20, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 2, 15, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 3, 15, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 4,  0, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 5, 15, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 6, 15, 93, 0, 0, 1);
CALL sp_InsertLocationDayAndOpen (12, 7, 20, 93, 0, 0, 1);

/* Location `Marina Overflow`, Customer.Id = 3 = `Park San Diego` */
CALL sp_InsertLocationDayAndOpen (13, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (13, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (13, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (13, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (13, 5,  0, 95, 1, 0, 1); 
CALL sp_InsertLocationDayAndOpen (13, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (13, 7,  0, 95, 1, 0, 1);

/* Location `South Dallas`, Customer.Id = 5 = `Park Dallas` */
CALL sp_InsertLocationDayAndOpen (14, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (14, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (14, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (14, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (14, 5,  0, 95, 1, 0, 1); 
CALL sp_InsertLocationDayAndOpen (14, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (14, 7,  0, 95, 1, 0, 1);

/* Location `Coronado Beach`, Customer.Id = 3 = `Park San Diego` */
CALL sp_InsertLocationDayAndOpen (15, 1,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (15, 2,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (15, 3,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (15, 4,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (15, 5,  0, 95, 1, 0, 1); 
CALL sp_InsertLocationDayAndOpen (15, 6,  0, 95, 1, 0, 1);
CALL sp_InsertLocationDayAndOpen (15, 7,  0, 95, 1, 0, 1);

-- -----------------------------------------------------
-- Tables `Paystation` and `PointOfSale`
-- -----------------------------------------------------

-- These Point Of Sale are for Customer.Id = 3 = Park San Diego
-- Parameters              p_CustomerId, p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT     , p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308003270054', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308003270055', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308003270056', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308002112467', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308002112468', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308004882765', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308004882766', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308007232893', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308007232894', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308005534285', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308003742511', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308007348928', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 3                 , 1            , '308007348929', '2008-08-08 08:08:08', 1       , 0                 , 0                  );

-- These Point Of Sale  are for Customer.Id = 5 = Park Dallas
-- Parameters              p_CustomerId, p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT     , p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
CALL sp_InsertPointOfSale (5           , 5                 , 1            , '543008810011', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (5           , 5                 , 1            , '543008810022', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (5           , 5                 , 1            , '543008810033', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (5           , 5                 , 1            , '543008810044', '2008-08-08 08:08:08', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (5           , 9                 , 1            , '900000000001', '2008-08-08 08:08:08', 1       , 0                 , 0                  ); 

-- These Point Of Sale are for Customer.Id = 3 = Park San Diego
-- Parameters              p_CustomerId, p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT     , p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
CALL sp_InsertPointOfSale (3           , 5                 , 1            , '570002400088', '2012-12-12 12:12:12', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 5                 , 1            , '570002400089', '2012-12-12 12:12:12', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 5                 , 1            , '570002400090', '2012-12-12 12:12:12', 1       , 0                 , 0                  );
CALL sp_InsertPointOfSale (3           , 5                 , 1            , '570002400091', '2012-12-12 12:12:12', 1       , 0                 , 0                  );

-- Move a point-of-sale from one customer to another
-- First: assign a new paystation (point-of-sale) to one customer
-- Parameters:             p_CustomerId, p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT     , p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
CALL sp_InsertPointOfSale (3           , 5                 , 1            , '543210000000', '2012-12-12 12:12:12', 1       , 0                 , 0                  );     
-- Second: move the paystation (point-of-sale) to another customer 
-- Parameters:            (p_CustomerId_new, p_CustomerId_old, p_SerialNumber, p_ProvisionedGMT, p_UserId)
CALL sp_MovePointOfSale   (5               , 3               , '543210000000', UTC_TIMESTAMP() , 1       );

-- -----------------------------------------------------
-- ** OLD ** Table `Paystation`
-- -----------------------------------------------------

-- These pay stations are for Customer.Id = 3 = Park San Diego
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (2, 1, '308003270054', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 1
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308003270055', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 2
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308003270056', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 3
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (2, 1, '308002112467', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 4
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308002112468', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 5
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308004882765', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 6
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (2, 1, '308004882766', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 7
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308007232893', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 8
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308007232894', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 9
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (2, 1, '308005534285', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 10
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308003742511', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 11
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308007348928', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 12
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '308007348929', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 13

-- These pay stations are for Customer.Id = 5 = Park Dallas
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '543008810011', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 14
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '543008810022', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 15
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '543008810033', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 16
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (3, 1, '543008810044', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 17

-- These pay stations are for Customer.Id = 3 = Park San Diego
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (4, 1, '570002400088', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 18
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (4, 1, '570002400089', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 19
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (4, 1, '570002400090', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 20
-- INSERT Paystation (PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES (4, 1, '570002400091', 0, 0, UTC_TIMESTAMP(), 1);  -- Id = 21

-- -----------------------------------------------------
-- ** OLD ** Table `PointOfSale`
-- -----------------------------------------------------

-- These Point Of Sale for Customer.Id = 3 = Park San Francisco
UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'EMMA 1',
        P.Latitude      = 32.703876,
        P.Longitude     = -117.164383
WHERE   P.SerialNumber  = '308003270054'
AND     L.Name          = 'Embarcadero Marina'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;


UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'EMMA 2',
        P.Latitude      = 32.704653,
        P.Longitude     = -117.164125
WHERE   P.SerialNumber  = '308003270055'
AND     L.Name          = 'Embarcadero Marina'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;


UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'EMMA 3',
        P.Latitude      = 32.703813,
        P.Longitude     = -117.163965
WHERE   P.SerialNumber  = '308003270056'
AND     L.Name          = 'Embarcadero Marina'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;


UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'CENT 1',
        P.Latitude      = 32.699895,
        P.Longitude     = -117.171654
WHERE   P.SerialNumber  = '308002112467'
AND     L.Name          = 'Centennial Park'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'CENT 2',
        P.Latitude      = 32.699886,
        P.Longitude     = -117.171684
WHERE   P.SerialNumber  = '308002112468'
AND     L.Name          = 'Centennial Park'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'CONV 1',
        P.Latitude      = 32.706725,
        P.Longitude     = -117.16242
WHERE   P.SerialNumber  = '308004882765'
AND     L.Name          = 'Convention Center'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'CONV 2',
        P.Latitude      = 32.706783,
        P.Longitude     = -117.162251
WHERE   P.SerialNumber  = '308004882766'
AND     L.Name          = 'Convention Center'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'TIDE 1',
        P.Latitude      = 32.691002,
        P.Longitude     = -117.165568
WHERE   P.SerialNumber  = '308007232893'
AND     L.Name          = 'Tidelands Park'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'TIDE 2',
        P.Latitude      = 32.690922,
        P.Longitude     = -117.165381
WHERE   P.SerialNumber  = '308007232894'
AND     L.Name          = 'Tidelands Park'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'J ST',
        P.Latitude      = 32.709311,
        P.Longitude     = -117.153611
WHERE   P.SerialNumber  = '308005534285'
AND     L.Name          = '1201 J Street'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'ISLE AVE',
        P.Latitude      = 32.710358,
        P.Longitude     = -117.155757
WHERE   P.SerialNumber  = '308003742511'
AND     L.Name          = '983 Island Ave'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;   
   
UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'BRDWY 1',
        P.Latitude      = 32.715630,
        P.Longitude     = -117.150564
WHERE   P.SerialNumber  = '308007348928'
AND     L.Name          = '1503 Broadway'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'BRDWY 2',
        P.Latitude      = 32.715640,
        P.Longitude     = -117.150574
WHERE   P.SerialNumber  = '308007348929'
AND     L.Name          = '1503 Broadway'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId; 

-- These Point Of Sale for Customer.Id = 5 = Park Dallas
UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'SOUTH 1',
        P.Latitude      = 32.779427,
        P.Longitude     = -96.760698
WHERE   P.SerialNumber  = '543008810011'
AND     L.Name          = 'South Dallas'
AND     P.CustomerId    = 5
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'SOUTH 2',
        P.Latitude      = 32.779732,
        P.Longitude     = -96.760873
WHERE   P.SerialNumber  = '543008810022'
AND     L.Name          = 'South Dallas'
AND     P.CustomerId    = 5
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'SOUTH 3',
        P.Latitude      = 32.780451,
        P.Longitude     = -96.760612
WHERE   P.SerialNumber  = '543008810033'
AND     L.Name          = 'South Dallas'
AND     P.CustomerId    = 5
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'SOUTH 4',
        P.Latitude      = 32.78062,
        P.Longitude     = -96.759751
WHERE   P.SerialNumber  = '543008810044'
AND     L.Name          = 'South Dallas'
AND     P.CustomerId    = 5
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'SOUTH API',
        P.Latitude      = 32.780302,
        P.Longitude     = -96.759140
WHERE   P.SerialNumber  = '900000000001'
AND     L.Name          = 'Unassigned'
AND     P.CustomerId    = 5
AND     P.CustomerId    = L.CustomerId; 

-- These Point Of Sale for Customer.Id = 3 = Park San Diego, Location.Id = 15 = Coronado Beach
UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'Beach 88',
        P.Latitude      = NULL,
        P.Longitude     = NULL
WHERE   P.SerialNumber  = '570002400088'
AND     L.Name          = 'Coronado Beach'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'Beach 89',
        P.Latitude      = NULL,
        P.Longitude     = NULL
WHERE   P.SerialNumber  = '570002400089'
AND     L.Name          = 'Coronado Beach'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'Beach 90',
        P.Latitude      = NULL,
        P.Longitude     = NULL
WHERE   P.SerialNumber  = '570002400090'
AND     L.Name          = 'Coronado Beach'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId; 

UPDATE  PointOfSale P, Location L
SET     P.LocationId    = L.Id, 
        P.Name          = 'Beach 91',
        P.Latitude      = NULL,
        P.Longitude     = NULL
WHERE   P.SerialNumber  = '570002400091'
AND     L.Name          = 'Coronado Beach'
AND     P.CustomerId    = 3
AND     P.CustomerId    = L.CustomerId;

-- -----------------------------------------------------
-- Calling procedure `sp_InsertPointOfSale` creates an unactivated point-of-sale. Active point-of-sale's with Id's 1 through 18. This task involves tables `POSDate` and `POSStatus`.
-- -----------------------------------------------------

-- Add transactional (historical) rows into table `POSDate`
INSERT  POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT           , CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId)
SELECT           P.Id         , T.Id         , '2008-09-10 08:09:10', 1             , 0      , UTC_TIMESTAMP(), 1                   
FROM    PointOfSale P, POSDateType T
WHERE   P.Id BETWEEN 1 AND 18   -- PointOfSale.Id 1 through 18 
AND     T.Id BETWEEN 9 AND 10   -- POSDateType.Id 9 and 10 (9 = `IsActivated`, 10 = `Is Billable Monthly (for EMS)`)
ORDER BY P.Id, T.Id;

-- Set current state in table `POSStatus` for `IsActivated`
UPDATE  POSStatus S, POSDate D
SET     S.IsActivated = D.CurrentSetting, S.IsActivatedGMT = D.ChangedGMT, S.LastModifiedGMT = D.LastModifiedGMT, S.LastModifiedByUserId = D.LastModifiedByUserId
WHERE   S.PointOfSaleId BETWEEN 1 AND 18
AND     S.PointOfSaleId = D.PointOfSaleId
AND     D.POSDateTypeId = 9
AND     D.ChangedGMT = '2008-09-10 08:09:10';

-- Set current state in table `POSStatus` for `IsBillableMonthlyForEms`
UPDATE  POSStatus S, POSDate D
SET     S.IsBillableMonthlyForEms = D.CurrentSetting, S.IsBillableMonthlyForEmsGMT = D.ChangedGMT, S.LastModifiedGMT = D.LastModifiedGMT, S.LastModifiedByUserId = D.LastModifiedByUserId
WHERE   S.PointOfSaleId BETWEEN 1 AND 18
AND     S.PointOfSaleId = D.PointOfSaleId
AND     D.POSDateTypeId = 10
AND     D.ChangedGMT = '2008-09-10 08:09:10';

-- -----------------------------------------------------
-- Table `LocationPOSLog`: Because of the UPDATE statements above many Point Of Sale's have been assigned to a different Location, so capture this change in table `LocationPOSLog`
-- -----------------------------------------------------

INSERT  LocationPOSLog (LocationId, PointOfSaleId, AssignedGMT, LastModifiedByUserId)
SELECT  P.LocationId, P.Id, DATE_ADD(L.AssignedGMT, INTERVAL 1 SECOND), L.LastModifiedByUserId
FROM    PointOfSale P, LocationPOSLog L
WHERE   P.Id = L.PointOfSaleId
AND     P.LocationId != L.LocationId;

-- -----------------------------------------------------
-- Table `POSHeartbeat` 
-- -----------------------------------------------------

UPDATE POSHeartbeat SET LastHeartbeatGMT =          UTC_TIMESTAMP()                      WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308003270054');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 15 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308003270055');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308003270056');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 45 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308002112467');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1  HOUR  ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308002112468');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 6  HOUR  ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308004882765');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 12 HOUR  ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308004882766');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1  DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308007232893');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2  DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308007232894');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 5  DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308005534285');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308003742511');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 20 DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308007348928');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 50 DAY   ) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '308007348929');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1  MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '543008810011');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 5  MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '543008810022');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '543008810033');
UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 15 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '543008810044');


-- UPDATE POSHeartbeat SET LastHeartbeatGMT = DATE_SUB(UTC_TIMESTAMP(), INTERVAL 20 MINUTE) WHERE PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNUmber = '900000000001');  '900000000001' is a Virtual PS, it shouldn't have PosHeartbeat
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (1,UTC_TIMESTAMP());
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (2,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 15 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (3,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (4,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 45 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (5,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1 HOUR));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (6,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 6 HOUR));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (7,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 12 HOUR));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (8,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1 DAY));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (9,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (10,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 5 DAY));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (11,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 DAY));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (12,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 20 DAY));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (13,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 50 DAY));

-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (14,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (15,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 5 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (16,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (17,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 15 MINUTE));
-- INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (18,DATE_SUB(UTC_TIMESTAMP(), INTERVAL 15 MINUTE));

-- -----------------------------------------------------
-- Table `POSBalance` 
-- -----------------------------------------------------

UPDATE  POSBalance                
SET     IsRecalcable               = 1,
        CashAmount                 = 18750,
        CoinCount                  = 326,
        CoinAmount                 = 7350,
        BillCount                  = 28,
        BillAmount                 = 31000,
        UnsettledCreditCardCount   = 9,
        UnsettledCreditCardAmount  = 9000,
        TotalAmount                = 66100
WHERE   PointOfSaleId NOT IN (SELECT Id FROM PointOfSale WHERE Name LIKE 'Beach%');

-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (1			 ,NULL			       ,0	      ,0				    ,0				 	      ,6000		  ,0		 ,0			 ,0		    ,0		    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (2			 ,NULL			       ,0	      ,0				    ,0				 	      ,6000		  ,0		 ,0			 ,0		    ,0	   		,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (3			 ,NULL			       ,0	      ,0				    ,0				 	      ,2000		  ,10		 ,500		 ,3		    ,3500	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (4			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (5			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (6			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (7			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (8			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (9			 ,NULL			   	   ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (10			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (11			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5	 	    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (12			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000	    ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount ,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES					  (13			 ,NULL			       ,0	      ,0				    ,0				 	      ,0		  ,20		 ,1000		 ,5		    ,5000       ,10						  ,10000				     ,16000	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
 
-- POSBalance for Point Of Sale assigned to Customer.Id = 5 = Park Dallas
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES		      (14			 ,NULL			       ,0	      ,0				    ,0				 	      ,1050	      ,200		 ,30000		 ,25        ,30500      ,7						  ,12600				     ,74150	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
 
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES		      (15			 ,NULL			       ,0	      ,0				    ,0				 	      ,1050	      ,200		 ,30000		 ,25        ,30500      ,7						  ,12600				     ,74150	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());
 
-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES		      (16			 ,NULL			       ,0	      ,0				    ,0				 	      ,1050	      ,200		 ,30000		 ,25        ,30500      ,7						  ,12600				     ,74150	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());

-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES		      (17			 ,NULL			       ,0	      ,0				    ,0				 	      ,1050	      ,200		 ,30000		 ,25        ,30500      ,7						  ,12600				     ,74150	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());

-- INSERT POSBalance (PointOfSaleId ,CollectionLockId ,ClusterId ,PrevCollectionLockId ,PrevPrevCollectionLockId ,CashAmount ,CoinCount ,CoinAmount ,BillCount ,BillAmount ,UnsettledCreditCardCount ,UnsettledCreditCardAmount ,TotalAmount,LastCashCollectionGMT ,LastCoinCollectionGMT ,LastBillCollectionGMT ,LastCardCollectionGMT ,LastRecalcGMT  ,CollectionRecalcGMT ,CollectionRecalcTransactions ,HasRecentCollection ,IsRecalcable ,LastCollectionTypeId ,NextRecalcGMT)
-- VALUES		      (18			 ,NULL			       ,0	      ,0				    ,0				 	      ,1050	      ,200		 ,30000		 ,25        ,30500      ,7						  ,12600				     ,74150	 	 ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()       ,UTC_TIMESTAMP()		 ,UTC_TIMESTAMP(),UTC_TIMESTAMP()	  ,0						    ,0				     ,0		       ,0				 	 ,UTC_TIMESTAMP());

-- -----------------------------------------------------
-- Table `Route` 
-- -----------------------------------------------------

-- Routes for Customer.Id = 3 = Park San Francisco
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 1, 'Collections 1', 0, UTC_TIMESTAMP(), 1);  -- Id = 1
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 1, 'Collections 2', 0, UTC_TIMESTAMP(), 1);  -- Id = 2
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 2, 'Maintenance 1', 0, UTC_TIMESTAMP(), 1);  -- Id = 3
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 2, 'Maintenance 2', 0, UTC_TIMESTAMP(), 1);  -- Id = 4

-- Routes for Customer.Id = 5 = Park Dallas
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 1, 'South Collections', 0, UTC_TIMESTAMP(), 1);  -- Id = 5
INSERT Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 2, 'South Maintenance', 0, UTC_TIMESTAMP(), 1);  -- Id = 6

-- -----------------------------------------------------
-- Table `RoutePOS` 
-- -----------------------------------------------------

-- RoutePOS assignments for Customer.Id = 3 = Park San Francisco
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  1);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  2);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  3);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  4);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  5);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  6);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  7);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  8);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 1,  9);

INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2,  6);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2,  7);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2,  8);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2,  9);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2, 10);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2, 11);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2, 12);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 2, 13);

INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3,  1);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3,  2);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3,  3);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3, 10);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3, 11);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3, 12);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 3, 13);

INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  4);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  5);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  6);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  7);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  8);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 4,  9);

-- RoutePOS assignments for Customer.Id = 5 = Park Dallas
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 5, 14);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 5, 15);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 5, 16);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 5, 17);

INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 6, 14);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 6, 15);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 6, 16);
INSERT RoutePOS (RouteId, PointOfSaleId) VALUES ( 6, 17);

-- ------------------------------------------------------------------
-- Table `UserAccount` and `UserDefaultDashboard` and `CustomerEmail`
-- ------------------------------------------------------------------

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 2
VALUES (2, 'sara_beck@park_america.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 3
VALUES (3, 'momo_uno@park_san_diego.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 4
VALUES (3, 'alice.chow@park_san_diego.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 5
VALUES (4, 'denise.wilshire@ParkAndFly.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 6
VALUES (4, 'andy.ellis@ParkAndFly.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 7
VALUES (5, 'jill.getty@park_dallas.com', 0, UTC_TIMESTAMP(), 1);

INSERT CustomerEmail (CustomerId, Email, VERSION, LastModifiedGMT, LastModifiedByUserId)   -- Id = 8
VALUES (5, 'fred.hsieh@park_dallas.com', 0, UTC_TIMESTAMP(), 1);

-- INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (2, 1, 1, 'sarasara', 'Sara', 'Beck', '58ae83e00383273590f96b385ddd700802c3f07d', 'salt', 1, 0, UTC_TIMESTAMP(), 1);   -- sarab

-- INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (2, 1, 0, UTC_TIMESTAMP(), 1);

-- INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (3, 1, 2, 'momomomo', 'Momo', 'Uno', '58ae83e00383273590f96b385ddd700802c3f07d', 'salt', 1, 0, UTC_TIMESTAMP(), 1);   -- momo

-- INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (3, 1, 0, UTC_TIMESTAMP(), 1);

INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (3, 1, 3, 'alicechow', 'Alice', 'Chow', '58ae83e00383273590f96b385ddd700802c3f07d', 'SaltSaltSaltSalt', 1, 0, UTC_TIMESTAMP(), 1);   -- Alice.Chow@park_san_diego.com

INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (6, 1, 0, UTC_TIMESTAMP(), 1);

-- INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (4, 1, 4, 'denisewilshire', 'Denise', 'Wilshire', '58ae83e00383273590f96b385ddd700802c3f07d', 'salt', 1, 0, UTC_TIMESTAMP(), 1);   -- Denise.Wilshire@ParkAndFly.com

-- INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (5, 1, 0, UTC_TIMESTAMP(), 1);

INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (4, 1, 5, 'andyellis', 'Andy', 'Ellis', '58ae83e00383273590f96b385ddd700802c3f07d', 'SaltSaltSaltSalt', 1, 0, UTC_TIMESTAMP(), 1);   -- Andy.Ellis@ParkAndFly.com

INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (7, 1, 0, UTC_TIMESTAMP(), 1);

-- User Accounts for Customer.Id = 5 = Park Dallas
-- INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (5, 1, 7, 'jilljill', 'Jill', 'Getty', '58ae83e00383273590f96b385ddd700802c3f07d', 'salt', 1, 0, UTC_TIMESTAMP(), 1);   -- Jill Getty

-- INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (7, 1, 0, UTC_TIMESTAMP(), 1);

INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (5, 1, 7, 'fredfred', 'Fred', 'Hsieh', '58ae83e00383273590f96b385ddd700802c3f07d', 'SaltSaltSaltSalt', 1, 0, UTC_TIMESTAMP(), 1);   -- Fred Hsieh

INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES (8, 1, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------

INSERT Role (CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 3, 1, 'Basic'       , 0, 0,    0, UTC_TIMESTAMP(), 1);  -- Id = 4 
INSERT Role (CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 3, 1, 'Employee'    , 0, 0,    0, UTC_TIMESTAMP(), 1);  -- Id = 5

-- -----------------------------------------------------
-- Table `RolePermission`
-- -----------------------------------------------------

-- Role: 'Basic' 
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  4,  201, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  4,  601, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1001, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1003, 0, UTC_TIMESTAMP(), 1);

-- Role: TEMPORARY SUPER USER ROLE FOR DEVELOPMENT 'MASTER'
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  101, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  201, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  202, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  301, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  401, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  402, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  403, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  404, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  405, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  406, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  407, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  408, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  409, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  410, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  501, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  502, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  503, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  504, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  505, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  506, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  507, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  601, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  602, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  701, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  702, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  801, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  901, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1001, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1002, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1003, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1004, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1005, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1006, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1007, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1051, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1052, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1101, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1102, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1103, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1104, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1201, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1202, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1203, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1301, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1302, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1401, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1402, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1501, 0, UTC_TIMESTAMP(), 1);
-- INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1502, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `CustomerRole`
-- -----------------------------------------------------

INSERT CustomerRole (CustomerId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 4, 0, UTC_TIMESTAMP(), 1);
INSERT CustomerRole (CustomerId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 5, 0, UTC_TIMESTAMP(), 1);

-- TEMPORARY, assigns momomomo all permissions
-- INSERT UserRole (UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 6, 0, UTC_TIMESTAMP(), 1);
-- TEMPORARY, assigns jilljill all permissions
-- INSERT UserRole (UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 6, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `UnifiedRate` 
-- -----------------------------------------------------

-- For Customer.Id = 3 = Park San Diego
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Night'           , UTC_TIMESTAMP(), 1);  -- Id = 4
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Early Bird'      , UTC_TIMESTAMP(), 1);  -- Id = 5 
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Morning'         , UTC_TIMESTAMP(), 1);  -- Id = 6 
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Afternoon'       , UTC_TIMESTAMP(), 1);  -- Id = 7 
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Evening'         , UTC_TIMESTAMP(), 1);  -- Id = 8 

-- For Customer.Id = 5 = Park Dallas                                                                                                            --  Original EMS 6 data (RateId, RateName) from Rates table:
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, '3rd Party (API)' , UTC_TIMESTAMP(), 1);  -- Id = 9    --  0       Third Party
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Aquarium'        , UTC_TIMESTAMP(), 1);  -- Id = 10   --  5       Main Art TheatreONLY
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Handicap'        , UTC_TIMESTAMP(), 1);  -- Id = 11   --  6       Handicapped
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Hourly'          , UTC_TIMESTAMP(), 1);  -- Id = 12   --  7,9,10  Hourly
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Free Day'        , UTC_TIMESTAMP(), 1);  -- Id = 13   -- 11	     Sunday/Holidays Free, 11  Sundays Free
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Reserved'        , UTC_TIMESTAMP(), 1);  -- Id = 14   -- 12       Valid For
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Wait Zone'       , UTC_TIMESTAMP(), 1);  -- Id = 15   -- 13       FREE 30 MIN MAX

-- CAUTION: There is another UnifiedRate record created below, search for this string: '$1.50 Per Hour'

-- -----------------------------------------------------
-- Table `UserRole`
-- -----------------------------------------------------

INSERT UserRole (UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 4, 0, UTC_TIMESTAMP(), 1);
INSERT UserRole (UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 5, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `RestAccount`
-- -----------------------------------------------------

INSERT RestAccount (CustomerId, PointOfSaleId, RestAccountTypeId, AccountName             , SecretKey                                     , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES             (5         , 18           , 1                , 'Park Dallas Virtual 01', 'CVPuqcqLTFnNB01RVHdZA1teDxriFlClpi5yEPeK1Lw=', 0        , UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `WebServiceEndPoint`
-- -----------------------------------------------------

INSERT WebServiceEndPoint (CustomerId, WebServiceEndPointTypeId, Token                             , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (5         , 1                       , 'tbl2fm12FZdDbUXBAaPO42hTEYYUfkvy', 0        , UTC_TIMESTAMP(), 1);

INSERT WebServiceEndPoint (CustomerId, WebServiceEndPointTypeId, Token                             , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (5         , 2                       , 'DPaF0tGx0UzFZTTbyIRh1OyXHTT2RgJZ', 0        , UTC_TIMESTAMP(), 1);

INSERT WebServiceEndPoint (CustomerId, WebServiceEndPointTypeId, Token                             , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (5         , 3                       , 'xZePnt5J2Q7jGYbmkLj2HpHhE374Zx7Y', 0        , UTC_TIMESTAMP(), 1);

INSERT WebServiceEndPoint (CustomerId, WebServiceEndPointTypeId, Token                             , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (5         , 4                       , 'PrLgo8hYwpFWotRdAvLYImDsa3jtfvPw', 0        , UTC_TIMESTAMP(), 1);

INSERT WebServiceEndPoint (CustomerId, WebServiceEndPointTypeId, Token                             , IsDeleted, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (5         , 5                       , 'jcWXzIbf9kzc7srzmI0Nb0yqpH9Rzb0b', 0        , UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `RatesXref`: maps a named rate from the EMS 6 `Rates` table to a EMS 7 test data `UnifiedRates` table, these mapped rates are for text data customer `Park Dallas`
-- -----------------------------------------------------

INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'FREE 30 MIN MAX'     , 15);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Handicapped'         , 11);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Hourly'              , 12);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Main Art TheatreONLY', 10);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Sunday/Holidays Free', 13);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Sundays Free'        , 13);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Third Party'         ,  9);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (5, 'Valid For'           , 14);
INSERT RatesXref (CustomerId,RateName,UnifiedRateId) VALUES (3, '$1.50 Per Hour'      , 16);   -- 

-- -----------------------------------------------------
-- Table `MerchantAccount`
-- -----------------------------------------------------

-- Credit Card Merchant Account
INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 25031;
VALUES                 (3         , 7          , 1                   , 'Embark Accnt 1', 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 25041;
VALUES                 (3         , 7          , 1                   , 'Embark Accnt 2', 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 25081;
VALUES                 (3         , 7          , 1                   , 'Embark Accnt 3', 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 1380;
VALUES                 (3         , 7          , 1                   , 'Broadway'      , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 751;  
VALUES                 (3         , 7          , 1                   , 'Isle and J'    , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 752;
VALUES                 (3         , 7          , 1                   , 'Convention'    , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 1362;
VALUES                 (3         , 7          , 1                   , 'Cent Accnt 2'  , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 1363;
VALUES                 (3         , 7          , 1                   , 'Cent Accnt 1'  , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 3821;
VALUES                 (3         , 7          , 1                   , 'Tide Park One' , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 3831;
VALUES                 (3         , 7          , 1                   , 'Tide Park Two' , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 32922;
VALUES                 (5         , 7          , 1                   , 'South A'       , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 32931;
VALUES                 (5         , 7          , 1                   , 'South B'       , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- EMS 6 MA.Id = 32941;
VALUES                 (5         , 7          , 1                   , 'South C'       , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

-- Value Card Merchant Account
INSERT MerchantAccount (CustomerId, ProcessorId, MerchantStatusTypeId, Name            , ReferenceCounter, IsValidated, VERSION, LastModifiedGMT, LastModifiedByUserId)  -- Id = 14;
VALUES                 (3         , 1          , 1                   , 'Concord'       , 0               , 1          , 0      , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `MerchantPOS`
-- -----------------------------------------------------

-- For Credit Cards
INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (12           , 4                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (13           , 4                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (4            , 8                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (5            , 7                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (6            , 6                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (7            , 6                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (1            , 1                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (2            , 2                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (3            , 3                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (11           , 5                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (10           , 5                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (8            , 9                , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (9            , 10               , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (14           , 11               , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (15           , 12               , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (16           , 11               , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (17           , 13               , 1         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

-- For Value Cards
INSERT MerchantPOS (PointOfSaleId, MerchantAccountId, CardTypeId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES             (20           , 14               , 3         , 0        , 0      , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Tables `ParkingPermission`, `ParkingPermissionDayOfWeek`, `ExtensibleRate`, `ExtensibleRateDayOfWeek`
-- -----------------------------------------------------

-- ParkingPermission
INSERT ParkingPermission (LocationId, ParkingPermissionTypeId, SpecialPermissionDateId, Name            , BeginHourLocal, BeginMinuteLocal, EndHourLocal, EndMinuteLocal, MaxDurationMinutes, IsLimitedOrUnlimited, IsActive, VERSION, CreatedGMT           , LastModifiedGMT      , LastModifiedByUserId)
VALUES                   (15        , 2                      , NULL                   , 'Unlimited Stay', 0             , 0               , 0           , 0             , 1440              , 2                   , 1       , 0      , '2012-03-30 22:42:50', '2012-03-30 22:42:50', 1);

-- ParkingPermissionDayOfWeek
INSERT  ParkingPermissionDayOfWeek (ParkingPermissionId, DayOfWeekId, VERSION, LastModifiedGMT, LastModifiedByUserId)
SELECT  P.Id, D.Id, 0, '2012-03-30 22:42:50', 1
FROM    ParkingPermission P, DayOfWeek D
WHERE   P.LocationId = 15 
AND     P.Name = 'Unlimited Stay';

-- For Customer.Id = 3 = Park San Diego (this is an extensible rate), this creates a record UnifiedRate.Id = 16
INSERT UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, '$1.50 Per Hour'  , UTC_TIMESTAMP(), 1);  -- Id = 16

-- ExtensibleRate
INSERT ExtensibleRate (UnifiedRateId, LocationId, ExtensibleRateTypeId, SpecialRateDateId, Name, BeginHourLocal, BeginMinuteLocal, EndHourLocal, EndMinuteLocal, RateAmount, ServiceFeeAmount, MinExtensionMinutes, IsActive, CreatedGMT, LastModifiedGMT, LastModifiedByUserId)
SELECT  Id, 15, 1, NULL, Name, 0, 0, 0, 0, 150, 25, 60, 1, '2012-03-30 22:42:50', '2012-03-30 22:42:50', 1
FROM    UnifiedRate
WHERE   CustomerId = 3
AND     Name = '$1.50 Per Hour';

-- ExtensibleRateDayOfWeek
INSERT  ExtensibleRateDayOfWeek (ExtensibleRateId, DayOfWeekId, VERSION, LastModifiedGMT, LastModifiedByUserId)
SELECT  R.Id, D.Id, 0, '2012-03-30 22:42:50', 1
FROM    ExtensibleRate R, DayOfWeek D
WHERE   R.LocationId = 15 
AND     R.Name = '$1.50 Per Hour';

-- -----------------------------------------------------
-- Table `LotSettingXref`
-- -----------------------------------------------------

INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Acme A'      , '1928'               );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Acme B'      , '1928 - WFoods'      );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Outlet 1'    , '6119-0001'          );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Outlet 2'    , '6119-0002'          );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Outlet 3'    , '6131-0001'          );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'City Center' , 'Main and Stevens'   );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Uptown AC'   , 'Plaza AC'           );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Uptown Solar', 'Plaza Solar'        );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Boardwalk'   , 'Railroad Public Lot');
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Library 1'   , 'Richmond Parkway 1' );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (3, 'Library 2'   , 'Richmond Parkway 2' );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (5, 'None'        , ''                   );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (5, 'Southwest'   , 'Emagine'            );
INSERT LotSettingXref (CustomerId, Name, NameEMS6) VALUES (5, 'Northeast'   , 'Main North'         );

-- -----------------------------------------------------
-- Table `PaystationSetting` (uses `LotSettingXref` from above)
-- -----------------------------------------------------

INSERT  PaystationSetting (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId)
SELECT                     CustomerId, Name, UTC_TIMESTAMP(), 1                   
FROM    LotSettingXref
WHERE   Name != 'None';

-- -----------------------------------------------------
-- Table `CustomerAlertEmail` 
-- -----------------------------------------------------

INSERT CustomerAlertEmail (CustomerAlertTypeId, CustomerEmailId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (1                  , 2              , 0        , 0      , UTC_TIMESTAMP(), 1                   );

INSERT CustomerAlertEmail (CustomerAlertTypeId, CustomerEmailId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (1                  , 3              , 0        , 0      , UTC_TIMESTAMP(), 1                   );

INSERT CustomerAlertEmail (CustomerAlertTypeId, CustomerEmailId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (3                  , 6              , 0        , 0      , UTC_TIMESTAMP(), 1                   );

INSERT CustomerAlertEmail (CustomerAlertTypeId, CustomerEmailId, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
VALUES                    (3                  , 7              , 0        , 0      , UTC_TIMESTAMP(), 1                   );

-- -----------------------------------------------------
-- Table `ThirdPartyServiceAccount` 
-- -----------------------------------------------------

INSERT ThirdPartyServiceAccount (CustomerId, Type, UserName, Password, Field1, IsPaystationBased, EndPointUrl) 
VALUES (1, 'Upside Wireless', 'd242a720-3d80-4601-a930-e978321d5225', 'oc4ALolLmkKyo2zc2J6IIu8l', '1', 0, '');


-- -----------------------------------------------------
-- Table `EmailAddress`
-- -----------------------------------------------------

INSERT INTO EmailAddress (Email, LastModifiedGMT) VALUES ('FN1.LN@mail.com',UTC_TIMESTAMP());
INSERT INTO EmailAddress (Email, LastModifiedGMT) VALUES ('FN2.LN@mail.com',UTC_TIMESTAMP());
INSERT INTO EmailAddress (Email, LastModifiedGMT) VALUES ('FN3.LN@mail.com',UTC_TIMESTAMP());
INSERT INTO EmailAddress (Email, LastModifiedGMT) VALUES ('FN4.LN@mail.com',UTC_TIMESTAMP());
INSERT INTO EmailAddress (Email, LastModifiedGMT) VALUES ('John.Smith@mail.com',UTC_TIMESTAMP());


-- -----------------------------------------------------
-- Table `Consumer`
-- -----------------------------------------------------

INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 3,'Consumer1FN', 'Consumer1LN',  1, UTC_TIMESTAMP(),1);
INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 3,'Consumer2FN', 'Consumer2FN',  2, UTC_TIMESTAMP(),1);
INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 5,'Consumer3FN', 'Consumer3FN',  2, UTC_TIMESTAMP(),1);
INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 5,'John', 'Smith',  NULL, UTC_TIMESTAMP(),1);
INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 5,'Scott', 'Smith',  NULL, UTC_TIMESTAMP(),1);
INSERT INTO Consumer(CustomerId, FirstName, LastName, EmailAddressId,LastModifiedGMT,LastModifiedByUserId   ) VALUES ( 5,'John', 'Smith',  5, UTC_TIMESTAMP(),1);


-- -----------------------------------------------------
-- Table `Coupon`
-- -----------------------------------------------------

INSERT INTO Coupon (ConsumerId, CustomerId, Coupon, PercentDiscount, DollarDiscountAmount, StartDateLocal,			EndDateLocal,				IsPndEnabled, IsPbsEnabled, IsDeleted, VERSION, LastModifiedGMT ,LastModifiedByUserId) VALUES
					( 1, 	3,		'3C1',	0,					1,					'2010-01-01 00:00:00',	'2015-01-01 00:00:00',		1,				1,			0,			1,		UTC_TIMESTAMP(),					1);

INSERT INTO Coupon (ConsumerId, CustomerId, Coupon, PercentDiscount, DollarDiscountAmount, StartDateLocal,			EndDateLocal,				IsPndEnabled, IsPbsEnabled, IsDeleted, VERSION, LastModifiedGMT ,LastModifiedByUserId) VALUES
					( 2,	3,		'3C2',	0,					1,					'2010-01-01 00:00:00',	'2015-01-01 00:00:00',		1,				1,			0,			1,		UTC_TIMESTAMP(),					1);
					
INSERT INTO Coupon (ConsumerId, CustomerId, Coupon, PercentDiscount, DollarDiscountAmount, StartDateLocal,			EndDateLocal,				IsPndEnabled, IsPbsEnabled, IsDeleted, VERSION, LastModifiedGMT , LastModifiedByUserId) VALUES
					( 3,	5,		'5C3',	0,					1,					'2010-01-01 00:00:00',	'2015-01-01 00:00:00',		1,				1,			0,			1,		UTC_TIMESTAMP(),					1);

INSERT INTO Coupon (ConsumerId, CustomerId, Coupon, PercentDiscount, DollarDiscountAmount, StartDateLocal,			EndDateLocal,				IsPndEnabled, IsPbsEnabled, IsDeleted, VERSION, LastModifiedGMT , LastModifiedByUserId) VALUES
					( NULL,	5,		'5C4',	0,					1,					'2010-01-01 00:00:00',	'2015-01-01 00:00:00',		1,				1,			0,			1,		UTC_TIMESTAMP(),					1);


-- -----------------------------------------------------
-- Table `CustomerCard`
-- -----------------------------------------------------

INSERT INTO CustomerCard (ConsumerId, CustomerCardTypeId,	CardNumber,	IsForNonOverlappingUse,	IsDeleted,	VERSION,	AddedGMT,	LastModifiedGMT,	LastModifiedByUserId) VALUES
						 (NULL,			3,					'12345',	0,						0,			1,			now(),		now(),				1);
						 
INSERT INTO CustomerCard (ConsumerId, CustomerCardTypeId,	CardNumber,	IsForNonOverlappingUse,	IsDeleted,	VERSION,	AddedGMT,	LastModifiedGMT,	LastModifiedByUserId) VALUES
						(1,			3,					'78654',	0,						0,			1,			now(),		now(),				1);



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;