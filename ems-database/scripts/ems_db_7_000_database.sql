DROP DATABASE IF EXISTS `@db.name@`;

-- wait 5 seconds for the clustered mysql to replicate the drop
SELECT SLEEP(10);

-- Use if 'utf8' is the desired character set for the entire database
CREATE DATABASE IF NOT EXISTS `@db.name@` CHARACTER SET utf8 COLLATE utf8_general_ci;

-- wait 5 seconds for the clustered mysql to replicate the drop
SELECT SLEEP(10);

-- Use if 'latin1' is the desired character set for the entire database
-- CREATE DATABASE IF NOT EXISTS @db.name@ CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE `@db.name@`;
