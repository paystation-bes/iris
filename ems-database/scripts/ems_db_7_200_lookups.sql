SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- -----------------------------------------------------
-- Table `AccessPoint`
-- -----------------------------------------------------

INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'digitalpaytech.com',   UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'INTERNET.COM',         UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'TELARGO.T-MOBILE.COM', UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'EPC.TMOBILE.COM',      UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'ISP.CINGULAR',         UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'WAP.CINGULAR',         UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'SP.TELUS.COM',         UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'at*netapn?',           UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'OK',                   UTC_TIMESTAMP(), 1) ;
INSERT AccessPoint (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'ERROR',                UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table AlertClassType
-- -----------------------------------------------------

INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Communication', 	UTC_TIMESTAMP(), 1); 
INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Collection', 		UTC_TIMESTAMP(), 1);
INSERT INTO AlertClassType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Pay Station Alert', UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Table AlertType
-- -----------------------------------------------------

INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
				      ( 1,	1, 				  'Last Seen Interval',			1,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 2,	2, 				  'Running Total',				1,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id,	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 3,	2, 				  'Coin Canister',				1,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 4,	2, 				  'Bill Stacker',				1,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 5,	2, 				  'Unsettled Credit Cards',		1,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 6,	3, 				  'Pay Station Alert',			0,				UTC_TIMESTAMP(), 1 );
INSERT INTO AlertType (Id, 	AlertClassTypeId, Name, 						IsUserDefined,  LastModifiedGMT, LastModifiedByUserId) VALUES
					  ( 7,	2, 				  'Last Collection Interval',	1,				UTC_TIMESTAMP(), 1 );

						
-- -----------------------------------------------------
-- Table AlertThresholdType
-- -----------------------------------------------------						
						
INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, 						LastModifiedGMT, LastModifiedByUserId) VALUES
							   (1,	1,			 'Last Seen Interval Hour', UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, 						LastModifiedGMT, LastModifiedByUserId) VALUES
							   (2,	2,			 'Running Total Dollar',   	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (6,	3,			 'Coin Canister Count',   	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (7,	3,			 'Coin Canister Dollar',  	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name, LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (8,	4,			 'Bill Stacker Count',   	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (9,	4,			 'Bill Stacker Dollar',   	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (10,	5,			 'Unsettled Credit Card Count',  UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (11,	5,			 'Unsettled Credit Card Dollar', UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, LastModifiedByUserId) VALUES
							   (12,	6,			 'Pay Station Alert',   	UTC_TIMESTAMP(),  		1 );

INSERT INTO AlertThresholdType (Id, AlertTypeId, Name,  LastModifiedGMT, 	LastModifiedByUserId) VALUES
							   (13,	7,			 'Collection Overdue',      UTC_TIMESTAMP(),  		1 );

						
-- -----------------------------------------------------
-- Table `ActivityType`
-- -----------------------------------------------------

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, NULL, 'Login',                                  NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101,  100, 'User Login',                             NULL, 'UserLogin'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102,  100, 'User Logout',                            NULL, 'UserLogout'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 103,  100, 'Login Failure',                          NULL, 'LoginFailure'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 104,  100, 'Locked Out',                             NULL, 'UserLockedOut'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 105,  100, 'User Switched In',                       NULL, 'UserSwitchedIn'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 106,  100, 'User Switched out',                      NULL, 'UserSwitchedOut'					,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 200, NULL, 'BOSS',                                   NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 201,  200, 'Configuration Upload',                   NULL, 'ConfigurationUpload'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 202,  200, 'Bad Card List Download',                 NULL, 'BadCardListDownload'				,0,UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 203,  200, 'Transaction Upload',                     NULL, 'TransactionUpload'				,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 300, NULL, 'Coupon Configuration',                   NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 301,  300, 'Coupon Bulk Export',                     NULL, 'CouponBulkExport'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 302,  300, 'Coupon Bulk Import',                     NULL, 'CouponBulkImport'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 303,  300, 'Add Coupon',                             NULL, 'AddCoupon'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 304,  300, 'Edit Coupon',                            NULL, 'EditCoupon'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 305,  300, 'Delete Coupon',                          NULL, 'DeleteCoupon'						,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 400, NULL, 'Card Management Configuration',          NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 401,  400, 'Card Bulk Export',                       NULL, 'CardBulkExport'			 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 402,  400, 'Card Bulk Import',                       NULL, 'CardBulkImport'			 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 403,  400, 'Add Permitted Record',                   NULL, 'AddPermittedRecord'		 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 404,  400, 'Edit Permitted Record',                  NULL, 'EditPermittedRecord'		 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 405,  400, 'Delete Permitted Record',                NULL, 'DeletePermittedRecord'		 	,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 406,  400, 'Add Banned Record',                      NULL, 'AddBannedRecord'			 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 407,  400, 'Edit Banned Record',                     NULL, 'EditBannedRecord'			 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 408,  400, 'Delete Banned Record',                   NULL, 'DeleteBannedRecord'		 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 409,  400, 'Add Card Type',                          NULL, 'AddCardType'				 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 410,  400, 'Edit Card Type',                         NULL, 'EditCardType'				 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 411,  400, 'Delete Card Type',                       NULL, 'DeleteCardType'			 		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 412,  400, 'Credit Card Refund',                     NULL, 'CreditCardRefund'			 		,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 500, NULL, 'Location Configuration',                 NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 501,  500, 'Add Location',                           NULL, 'AddLocation'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 502,  500, 'Edit Location',                          NULL, 'EditLocation'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 503,  500, 'Delete Location',                        NULL, 'DeleteLocation'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 504,  500, 'Assign Pay Station to Location',         NULL, 'AssignPayStationtoLocation'		,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 600, NULL, 'Pay Station Configuration',              NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 601,  600, 'Pay Station is Visible',                 NULL, 'PayStationisVisible'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 602,  600, 'Pay Station is Hidden',                  NULL, 'PayStationisHidden'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 603,  600, 'Activate Pay Station',                   NULL, 'ActivatePayStation'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 604,  600, 'Deactivate Pay Station',                 NULL, 'DeactivatePayStation'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 605,  600, 'Edit Settings',                          NULL, 'EditSettings'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 606,  600, 'Create Pay Station',                     NULL, 'CreatePayStation'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 607,  600, 'Move Pay Station',                       NULL, 'MovePayStation'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 608,  600, 'Configuration Download Status',          NULL, 'ConfigurationDownloadStatus'		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 609,  600, 'Software Updated',                       NULL, 'SoftwareUpdated'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 610,  600, 'Encryption Key Download',                NULL, 'EncryptionKeyDownload'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 611,  600, 'Test Activate Pay Station',              NULL, 'TestActivatePayStation'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 612,  600, 'Test Deactivate Pay Station',            NULL, 'TestDeactivatePayStation'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 613,  600, 'Bill Pay Station Monthly',               NULL, 'BillPayStationMonthly'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 614,  600, 'Do Not Bill Pay Station Monthly',        NULL, 'DoNotBillPayStationMonthly'		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 615,  600, 'Pay Station has Bundled Data',           NULL, 'PayStationhasBundledData'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 616,  600, 'Pay Station does Not have Bundled Data', NULL, 'PayStationdoesNothaveBundledData'	,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 700, NULL, 'Route Configuration',                    NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 701,  700, 'Add Route',                              NULL, 'AddRoute'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 702,  700, 'Edit Route',                             NULL, 'EditRoute'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 703,  700, 'Delete Route',                           NULL, 'DeleteRoute'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 704,  700, 'Assign Pay Station to Route',            NULL, 'AssignPayStationtoRoute'			,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 800, NULL, 'Alert Configuration',                    NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 801,  800, 'Add Alert',                              NULL, 'AddAlert'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 802,  800, 'Edit Alert',                             NULL, 'EditAlert'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 803,  800, 'Delete Alert',                           NULL, 'DeleteAlert'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 804,  800, 'Pay Station Alert',                      NULL, 'PayStationAlert'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 805,  800, 'User Defined Alert',                     NULL, 'UserDefinedAlert'					,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 900, NULL, 'Extend-By-Phone Configuration',          NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 901,  900, 'Add Rate',                               NULL, 'AddRate'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 902,  900, 'Edit Rate',                              NULL, 'EditRate'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 903,  900, 'Delete Rate',                            NULL, 'DeleteRate'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 904,  900, 'Add Policy',                             NULL, 'AddPolicy'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 905,  900, 'Edit Policy',                            NULL, 'EditPolicy'						,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 906,  900, 'Delete Policy',                          NULL, 'DeletePolicy'						,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1000, NULL, 'User Configuration',                     NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1001, 1000, 'Add User Account',                       NULL, 'AddUserAccount'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1002, 1000, 'Edit User Account',                      NULL, 'EditUserAccount'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1003, 1000, 'Delete User Account',                    NULL, 'DeleteUserAccount'				,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1100, NULL, 'Role Configuration',                     NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1101, 1100, 'Add Role',                               NULL, 'AddRole'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1102, 1100, 'Edit Role',                              NULL, 'EditRole'							,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1103, 1100, 'Delete Role',                            NULL, 'DeleteRole'						,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1200, NULL, 'Reporting Configuration',                NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1201, 1200, 'Add Scheduled Report',                   NULL, 'AddScheduledReport'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1202, 1200, 'Edit Scheduled Report',                  NULL, 'EditScheduledReport'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1203, 1200, 'Delete Scheduled Report',                NULL, 'DeleteScheduledReport'			,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1300, NULL, 'Company Preferences Configuration',      NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1301, 1300, 'Edit Value',                             NULL, 'EditValue'						,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1400, NULL, 'DTP Admin',                              NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1401, 1400, 'Process Next External Key',              NULL, 'ProcessNextExternalKey'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1402, 1400, 'Process Uploaded Files',                 NULL, 'ProcessUploadedFiles'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1403, 1400, 'Process Card Retry',                     NULL, 'ProcessCardRetry'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1404, 1400, 'Promote Server',                         NULL, 'PromoteServer'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1405, 1400, 'Shutdown Tomcat Server',                 NULL, 'ShutdownTomcatServer'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1406, 1400, 'Add System Notification',                NULL, 'AddSystemNotification'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1407, 1400, 'Edit System Notification',               NULL, 'EditSystemNotification'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1408, 1400, 'Delete System Notification',             NULL, 'DeleteSystemNotification'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1409, 1400, 'Archive System Notification',            NULL, 'ArchiveSystemNotification'		,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1410, 1400, 'Add Merchant Account',                   NULL, 'AddMerchantAccount'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1411, 1400, 'Edit Merchant Account',                  NULL, 'EditMerchantAccount'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1412, 1400, 'Delete Merchant Account',                NULL, 'DeleteMerchantAccount'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1413, 1400, 'Add Digital API',                        NULL, 'AddDigitalAPI'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1414, 1400, 'Edit Digital API',                       NULL, 'EditDigitalAPI'					,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1415, 1400, 'Delete Digital API',                     NULL, 'DeleteDigitalAPI'					,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1500, NULL, 'Dashboard Widget Configuration',         NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1501, 1500, 'Assign Default Dashboard',               NULL, 'AssignDefaultDashboard'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1502, 1500, 'Create Customized Dashboard',            NULL, 'CreateCustomizedDashboard'		,0, UTC_TIMESTAMP(), 1) ;

INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1600, NULL, 'Customer Agreement Configuration',       NULL, ''									,1, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1601, 1600, 'Add Customer Agreement',                 NULL, 'AddCustomerAgreement'				,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1602, 1600, 'Edit Customer Agreement',                NULL, 'EditCustomerAgreement'			,0, UTC_TIMESTAMP(), 1) ;
INSERT ActivityType (Id, ParentActivityTypeId, Name, Description, MessagePropertiesKey, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1603, 1600, 'Delete Customer Agreement',              NULL, 'DeleteCustomerAgreement'			,0, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `AuthorizationType`
-- -----------------------------------------------------

INSERT AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'None',             UTC_TIMESTAMP(), 1) ;
INSERT AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Internal List',    UTC_TIMESTAMP(), 1) ;
INSERT AuthorizationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'External Server',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CardType`
-- -----------------------------------------------------

INSERT CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',                       UTC_TIMESTAMP(), 1) ;
INSERT CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Credit Card',               UTC_TIMESTAMP(), 1) ;
INSERT CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Smart Card',                UTC_TIMESTAMP(), 1) ;
INSERT CardType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Passcard',                UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Carrier`
-- -----------------------------------------------------

INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'AT&T'           , UTC_TIMESTAMP(), 1) ;
INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'at*netop?'      , UTC_TIMESTAMP(), 1) ;
INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'CAN Rogers Wire', UTC_TIMESTAMP(), 1) ;
INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'ROGERS'         , UTC_TIMESTAMP(), 1) ;
INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'T-Mobile'       , UTC_TIMESTAMP(), 1) ;
INSERT Carrier (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Verizon'        , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `ChangeType`
-- -----------------------------------------------------

INSERT ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'N/A',    UTC_TIMESTAMP(), 1) ;
INSERT ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Insert', UTC_TIMESTAMP(), 1) ;
INSERT ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Update', UTC_TIMESTAMP(), 1) ;
INSERT ChangeType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Delete', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CollectionType`
-- -----------------------------------------------------

INSERT CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'All',  UTC_TIMESTAMP(), 1) ;
INSERT CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Bill', UTC_TIMESTAMP(), 1) ;
INSERT CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Coin', UTC_TIMESTAMP(), 1) ;
INSERT CollectionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Card', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CreditCardType`
-- -----------------------------------------------------

INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',           0,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Not Assigned',  0,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'VISA',          1,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'MasterCard',    1,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'AMEX',          1,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Discover',      1,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Diners Club',   1,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'CreditCard',    0,      UTC_TIMESTAMP(), 1) ;
INSERT CreditCardType (Id, Name, IsValid, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Other',         0,      UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Customer`: create the customer-database-object for 'Digital (DPT)'
-- -----------------------------------------------------

-- Parameters          (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId,    CustomerName, TrialExpiryGMT, IsParent, UserName     ,                                   Password, 				      IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell,  IsDigitalCollect, IsOnlineConfiguration, IsFlexIntegration, IsCaseIntegration,  IsDigitalAPIXChange,  Timezone,         UserId,     p_PasswordSalt)
CALL sp_InsertCustomer (             1,                    1,             NULL, 'Digital (DPT)',            NULL,        0, 'SystemAdmin', '58ae83e00383273590f96b385ddd700802c3f07d',                    1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 					 1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');

CALL sp_InsertCustomer (             1,                    1,             NULL, 'qa1systemAdmin',           NULL,        0, 'qaAutomation1%40systemAdmin', '58ae83e00383273590f96b385ddd700802c3f07d',    1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 	                 1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');
CALL sp_InsertCustomer (             1,                    1,             NULL, 'qa2systemAdmin',           NULL,        0, 'qaAutomation2%40systemAdmin', '58ae83e00383273590f96b385ddd700802c3f07d',    1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 	                 1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');

CALL sp_InsertCustomer (             2,                    1,             NULL, 'qa1parent',            NULL,        	 1, 'qaAutomation1%40parent', '58ae83e00383273590f96b385ddd700802c3f07d',         1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 		             1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');
CALL sp_InsertCustomer (             2,                    1,             NULL, 'qa2parent',           NULL,        	 1, 'qaAutomation2%40parent', '58ae83e00383273590f96b385ddd700802c3f07d',         1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 			         1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');
-- Adding Oranj Parent Customer
CALL sp_InsertCustomer (             2,                    1,             NULL, 'Oranj Parent',           NULL,        	 1, 'oranjparentQA', '58ae83e00383273590f96b385ddd700802c3f07d',                  1,                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                    1,                1, 			         1,					1,                  1,                    'Canada/Pacific',      1,  'SaltSaltSaltSalt');


-- -----------------------------------------------------
-- Table `CustomerPropertyType`
-- -----------------------------------------------------

INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Timezone',                        UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Query Spaces By',                 UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Max User Accounts',               UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Credit Card Offline Retry',       UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Max Offline Retry',               UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'SMS Warning Period',              UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Hidden Pay Stations Reported',     UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'GPS Latitude',                    UTC_TIMESTAMP(), 1) ;
INSERT CustomerPropertyType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'GPS Longitude',                   UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerStatusType`
-- -----------------------------------------------------

INSERT CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', UTC_TIMESTAMP(), 1) ;
INSERT CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  UTC_TIMESTAMP(), 1) ;
INSERT CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Trial',    UTC_TIMESTAMP(), 1) ;
INSERT CustomerStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Deleted',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerType`
-- -----------------------------------------------------

INSERT CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'DPT',    UTC_TIMESTAMP(), 1) ;
INSERT CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Parent', UTC_TIMESTAMP(), 1) ;
INSERT CustomerType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Child',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `DaylightSaving`
-- -----------------------------------------------------

INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2007-03-11 02:00:00','2007-11-04 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2008-03-09 02:00:00','2008-11-02 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2009-03-08 02:00:00','2009-11-01 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2010-03-14 02:00:00','2010-11-07 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2011-03-13 02:00:00','2011-11-06 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2012-03-11 02:00:00','2012-11-04 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2013-03-10 02:00:00','2013-11-03 02:00:00', UTC_TIMESTAMP(), 1);
INSERT  DaylightSaving (BeginDST, EndDST, LastModifiedGMT, LastModifiedByUserId) VALUES ('2014-03-09 02:00:00','2014-11-02 02:00:00', UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `DayOfWeek`
-- -----------------------------------------------------

INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1, 'Sunday'   , 'Sun', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 2, 'Monday'   , 'Mon', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 3, 'Tuesday'  , 'Tue', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 4, 'Wednesday', 'Wed', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 5, 'Thursday' , 'Thu', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 6, 'Friday'   , 'Fri', UTC_TIMESTAMP(), 1);
INSERT  DayOfWeek (Id, DayOfWeek, Name, NameAbbrev, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 7, 'Saturday' , 'Sat', UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `DenominationType`
-- -----------------------------------------------------

INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 1,     5, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 1,    10, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 1,    25, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1,    50, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 1,   100, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 1,   200, UTC_TIMESTAMP(), 1) ;

INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 2,   100, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 2,   200, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 2,   500, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (104, 2,  1000, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (105, 2,  2000, UTC_TIMESTAMP(), 1) ;
INSERT DenominationType (Id, IsCoinOrBill, DenominationAmount, LastModifiedGMT, LastModifiedByUserId) VALUES (106, 2,  5000, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `EMVTransactionDataMapping`
-- -----------------------------------------------------
INSERT INTO EMVTransactionDataMapping (Id, ProcessorId, Label1, Label2, Label3, Label4, Label5, Label6, Label7, CreatedGMT) VALUES (1, 99, 'TestLabel1', 'TestLabel2', 'TestLabel3', NULL, NULL, NULL, NULL, UTC_TIMESTAMP());

-- -----------------------------------------------------
-- Table `EntityType`
-- -----------------------------------------------------

INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'Customer',            UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'Customer Agreement',  UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Customer Properties', UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'Customer Services',   UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'User Account',        UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'User Role',           UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'Role',                UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Role Permission',     UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Customer Role',       UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Notification',        UTC_TIMESTAMP(), 1) ;
INSERT EntityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11, 'Activity Login',      UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `EventActionType`
-- -----------------------------------------------------

INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, '',                 UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Alerted',          UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Cleared',          UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Full',             UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Normal',           UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Low',              UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Empty',            UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'High',             UTC_TIMESTAMP(), 1) ;
INSERT EventActionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Info',             UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `EventType`
-- -----------------------------------------------------

INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1,   0, 'eventType.battery.level',                  1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2,   2, 'eventType.billacceptor.present',           0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3,   2, 'eventType.billacceptor.jam',               1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4,   2, 'eventType.billacceptor.unableToStack',     1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5,   3, 'eventType.billstacker.present',            0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6,   3, 'eventType.billstacker.level',              1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7,   3, 'eventType.billcanister.remove',            1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8,   4, 'eventType.cardreader.present',             1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9,   5, 'eventType.coinacceptor.present',           0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,  5, 'eventType.coinacceptor.jam',               1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,  6, 'eventType.coinchanger.present',            0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,  6, 'eventType.coinchanger.jam',                1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,  6, 'eventType.coinchanger.level',              1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14,  7, 'eventType.coinhopper1.present',            0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15,  7, 'eventType.coinhopper1.level',              1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 16,  8, 'eventType.coinhopper2.present',            0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 17,  8, 'eventType.coinhopper2.level',              1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 18,  9, 'eventType.door.open',                      1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 19, 10, 'eventType.doorlower.open',                 1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 20, 11, 'eventType.doorupper.open',                 1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 21, 12, 'eventType.paystation.lowerpowershutdown',  1, 1, 1, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 22, 12, 'eventType.paystation.publickeyupdate',     0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 23, 12, 'eventType.paystation.servicemode',         1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 24, 12, 'eventType.paystation.upgrade',             0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 25, 12, 'eventType.paystation.ticketfootersuccess', 0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 26, 13, 'eventType.printer.present',                1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 27, 13, 'eventType.printer.cuttererror',            1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 28, 13, 'eventType.printer.headerror',              1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 29, 13, 'eventType.printer.leverdisengaged',        1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 30, 13, 'eventType.printer.paperstatus',            1, 1, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 31, 13, 'eventType.printer.temperatureerror',       1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 32, 13, 'eventType.printer.voltageerror',           1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 33, 14, 'eventType.shockalarm.on',                  1, 1, 1, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 34, 15, 'eventType.pcm.upgrader.on',                0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 35, 15, 'eventType.pcm.wirelesssignalstrength',     0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 36, 16, 'eventType.coincanister.present',           0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 37, 16, 'eventType.coincanister.remove',            1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 38, 17, 'eventType.maintenancedoor.open',           1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 39, 18, 'eventType.cashvaultdoor.open',             1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 40, 19, 'eventType.coinescrow.present',             0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 41, 19, 'eventType.coinescrow.jam',                 1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 42, 21, 'eventType.modem.type',                     0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 43, 21, 'eventType.modem.ccid',                     0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 44, 21, 'eventType.modem.networkcarrier',           0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 45, 21, 'eventType.modem.apn',                      0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 46, 20, 'eventType.customerdefinedralert.on',       1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 47, 12, 'eventType.paystation.coinchanger.level',   1, 0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 48, 12, 'eventType.paystation.coinchanger.jam',     1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 49, 22, 'eventType.rfidcardreader.present',         0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 50, 13, 'eventType.printer.paperjam',               1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 51, 12, 'eventType.paystation.ticketnottaken',      1, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 52, 13, 'eventType.printer.ticketdidnotprint',      1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 53, 13, 'eventType.printer.printingfailure',        1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 54, 12, 'eventType.paystation.uid.changed',         0, 0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 55, 12, 'eventType.paystation.uid',                 1, 1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT EventType (Id, EventDeviceTypeId, Description, IsAlert, IsNotification, IsAutoClear, HasLevel, LastModifiedGMT, LastModifiedByUserId) VALUES ( 56, 12, 'eventType.paystation.clock.changed',       0, 0, 0, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `EventDefinition`
-- -----------------------------------------------------

INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  1,  0,  3,  4,  0, 'eventdefinition.battery.level.normal',               'Normal',                        1, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  2,  0,  3,  5,  2, 'eventdefinition.battery.level.low',                  'Low',                           1, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  3,  1,  3,  4,  0, 'eventdefinition.battery2.level.normal',              'Normal',                        1, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  4,  1,  3,  5,  2, 'eventdefinition.battery2.level.low',                 'Low',                           1, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  5,  2,  0,  1,  0, 'eventdefinition.billacceptor.present.on',            'Present',                       2, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  6,  2,  0,  2,  0, 'eventdefinition.billacceptor.present.off',           'NotPresent',                    2, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  7,  2,  1,  1,  2, 'eventdefinition.billacceptor.jam.on',                'Jam',                           3, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  8,  2,  1,  2,  0, 'eventdefinition.billacceptor.jam.off',               'JamCleared',                    3, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (  9,  2,  2,  1,  2, 'eventdefinition.billacceptor.level.full',            'UnableToStack',                 4, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,  2,  2,  2,  0, 'eventdefinition.billacceptor.level.normal',          'UnableToStackCleared',          4, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,  3,  0,  1,  0, 'eventdefinition.billstacker.present.on',             'Present',                       5, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,  3,  0,  2,  0, 'eventdefinition.billstacker.present.off',            'NotPresent',                    5, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,  3,  3,  3,  2, 'eventdefinition.billstacker.level.full',             'Full',                          6, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14,  3,  3,  4,  0, 'eventdefinition.billstacker.level.normal',           'Normal',                        6, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15,  3, 18,  1,  1, 'eventdefinition.billcanister.remove.on',             'Removed',                       7, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 16,  3, 18,  2,  0, 'eventdefinition.billcanister.remove.off',            'Inserted',                      7, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 17,  4,  0,  1,  0, 'eventdefinition.cardreader.present.on',              'Present',                       8, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 18,  4,  0,  2,  2, 'eventdefinition.cardreader.present.off',             'NotPresent',                    8, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 19,  5,  0,  1,  0, 'eventdefinition.coinacceptor.present.on',            'Present',                       9, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 20,  5,  0,  2,  0, 'eventdefinition.coinacceptor.present.off',           'NotPresent',                    9, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 21,  5,  1,  1,  2, 'eventdefinition.coinacceptor.jam.on',                'Jam',                          10, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 22,  5,  1,  2,  0, 'eventdefinition.coinacceptor.jam.off',               'JamCleared',                   10, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 23,  6,  0,  1,  0, 'eventdefinition.coinchanger.present.on',             'Present',                      11, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 24,  6,  0,  2,  0, 'eventdefinition.coinchanger.present.off',            'NotPresent',                   11, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 25,  6,  1,  1,  2, 'eventdefinition.coinchanger.jam.on',                 'Jam',                          12, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 26,  6,  1,  2,  0, 'eventdefinition.coinchanger.jam.off',                'JamCleared',                   12, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 27,  6,  3,  4,  0, 'eventdefinition.coinchanger.level.normal',           'Normal',                       13, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 28,  6,  3,  5,  1, 'eventdefinition.coinchanger.level.low',              'Low',                          13, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 29,  6,  3,  6,  2, 'eventdefinition.coinchanger.level.empty',            'Empty',                        13, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 30,  7,  0,  1,  0, 'eventdefinition.coinhopper1.present.on',             'Present',                      14, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 31,  7,  0,  2,  0, 'eventdefinition.coinhopper1.present.off',            'NotPresent',                   14, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 32,  7,  3,  4,  0, 'eventdefinition.coinhopper1.level.normal',           'EmptyCleared',                 15, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 33,  7,  3,  6,  2, 'eventdefinition.coinhopper1.level.empty',            'Empty',                        15, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 34,  8,  0,  1,  0, 'eventdefinition.coinhopper2.present.on',             'Present',                      16, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 35,  8,  0,  2,  0, 'eventdefinition.coinhopper2.present.off',            'NotPresent',                   16, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 36,  8,  3,  4,  0, 'eventdefinition.coinhopper2.level.normal',           'EmptyCleared',                 17, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 37,  8,  3,  6,  2, 'eventdefinition.coinhopper2.level.empty',            'Empty',                        17, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 38,  9,  4,  1,  1, 'eventdefinition.door.open.on',                       'DoorOpened',                   18, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 39,  9,  4,  2,  0, 'eventdefinition.door.open.off',                      'DoorClosed',                   18, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 40, 10,  4,  1,  1, 'eventdefinition.doorlower.open.on',                  'DoorOpened',                   19, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 41, 10,  4,  2,  0, 'eventdefinition.doorlower.open.off',                 'DoorClosed',                   19, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 42, 11,  4,  1,  1, 'eventdefinition.doorupper.open.on',                  'DoorOpened',                   20, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 43, 11,  4,  2,  0, 'eventdefinition.doorupper.open.off',                 'DoorClosed',                   20, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 44, 12,  5,  1,  3, 'eventdefinition.paystation.lowerpowershutdown.on',   'LowPowerShutdownOn',           21, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 45, 12,  5,  2,  0, 'eventdefinition.paystation.lowerpowershutdown.off',  'LowPowerShutdownOff',          21, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 46, 12,  6,  0,  0, 'eventdefinition.paystation.publickeyupdate.on',      'PublicKeyUpdate',              22, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 47, 12,  6,  2,  0, 'eventdefinition.paystation.publickeyupdate.off',     'PublicKeyUpdateOff',           22, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 48, 12,  7,  1,  1, 'eventdefinition.paystation.servicemode.on',          'ServiceModeEnabled',           23, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 49, 12,  7,  2,  0, 'eventdefinition.paystation.servicemode.off',         'ServiceModeDisabled',          23, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 50, 12,  8,  0,  0, 'eventdefinition.paystation.upgrade.on',              'Upgrade',                      24, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 51, 12,  9,  2,  0, 'eventdefinition.paystation.ticketfootersuccess.off', '',                             25, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 52, 13,  0,  1,  0, 'eventdefinition.printer.present.on',                 'Present',                      26, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 53, 13,  0,  2,  3, 'eventdefinition.printer.present.off',                'NotPresent',                   26, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 54, 13, 10,  1,  1, 'eventdefinition.printer.cuttererror.on',             'CutterError',                  27, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 55, 13, 10,  2,  0, 'eventdefinition.printer.cuttererror.off',            'CutterErrorCleared',           27, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 56, 13, 11,  1,  1, 'eventdefinition.printer.headerror.on',               'HeadError',                    28, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 57, 13, 11,  2,  0, 'eventdefinition.printer.headerror.off',              'HeadErrorCleared',             28, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 58, 13, 12,  1,  2, 'eventdefinition.printer.leverdisengaged.on',         'LeverDisengaged',              29, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 59, 13, 12,  2,  0, 'eventdefinition.printer.leverdisengaged.off',        'LeverDisengagedCleared',       29, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 60, 13, 13,  4,  0, 'eventdefinition.printer.paperstatus.normal',         'PaperNormal',                  30, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 61, 13, 13,  5,  2, 'eventdefinition.printer.paperstatus.low',            'PaperLow',                     30, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 62, 13, 13,  6,  3, 'eventdefinition.printer.paperstatus.out',            'PaperOut',                     30, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 63, 13, 13,  2,  0, 'eventdefinition.printer.paperstatus.cleared',        'PaperOutCleared',              30, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 64, 13, 14,  1,  1, 'eventdefinition.printer.temperatureerror.on',        'TemperatureError',             31, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 65, 13, 14,  2,  0, 'eventdefinition.printer.temperatureerror.off',       'TemperatureErrorCleared',      31, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 66, 13, 15,  1,  1, 'eventdefinition.printer.voltageerror.on',            'VoltageError',                 32, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 67, 13, 15,  2,  0, 'eventdefinition.printer.voltageerror.off',           'VoltageErrorCleared',          32, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 68, 14, 16,  1,  3, 'eventdefinition.shockalarm.on.on',                   'ShockOn',                      33, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 69, 14, 16,  2,  0, 'eventdefinition.shockalarm.on.off',                  'ShockOff',                     33, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 70, 15,  8,  0,  0, 'eventdefinition.pcm.upgrader.on',                    'Upgrade',                      34, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 71, 15, 17,  0,  0, 'eventdefinition.pcm.wirelesssignalstrength.on',      'WirelessSignalStrength',       35, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 72, 16,  0,  1,  0, 'eventdefinition.coincanister.present.on',            'Present',                      36, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 73, 16,  0,  2,  0, 'eventdefinition.coincanister.present.off',           'NotPresent',                   36, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 74, 16, 18,  1,  1, 'eventdefinition.coincanister.remove.on',             'Removed',                      37, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 75, 16, 18,  2,  0, 'eventdefinition.coincanister.remove.off',            'Inserted',                     37, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 76, 17,  4,  1,  1, 'eventdefinition.maintenancedoor.open.on',            'DoorOpened',                   38, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 77, 17,  4,  2,  0, 'eventdefinition.maintenancedoor.open.off',           'DoorClosed',                   38, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 78, 18,  4,  1,  1, 'eventdefinition.cashvaultdoor.open.on',              'DoorOpened',                   39, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 79, 18,  4,  2,  0, 'eventdefinition.cashvaultdoor.open.off',             'DoorClosed',                   39, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 80, 19,  0,  1,  0, 'eventdefinition.coinescrow.present.on',              'Present',                      40, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 81, 19,  0,  2,  0, 'eventdefinition.coinescrow.present.off',             'NotPresent',                   40, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 82, 19,  1,  1,  2, 'eventdefinition.coinescrow.jam.on',                  'Jam',                          41, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 83, 19,  1,  2,  0, 'eventdefinition.coinescrow.jam.off',                 'JamCleared',                   41, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 84, 21, 19,  0,  0, 'eventdefinition.modem.type.on',                      '',                             42, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 85, 21, 20,  0,  0, 'eventdefinition.modem.ccid.on',                      '',                             43, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 86, 21, 21,  0,  0, 'eventdefinition.modem.networkcarrier.on',            '',                             44, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 87, 21, 22,  0,  0, 'eventdefinition.modem.apn.on',                       '',                             45, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 88, 20, 23,  1,  2, 'eventdefinition.customerdefinedralert.on.on',        'CustomerDefinedAlertOn',       46, UTC_TIMESTAMP(), 1);  -- NEW FOR EMS 7
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 89, 20, 23,  2,  0, 'eventdefinition.customerdefinedralert.on.off',       'CustomerDefinedAlertOff',      46, UTC_TIMESTAMP(), 1);  -- NEW FOR EMS 7
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 90,  3,  3,  2,  0, 'eventdefinition.billstacker.level.cleared',          'FullCleared',                   6, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 91, 12,  3,  5,  1, 'eventdefinition.paystation.coinchanger.level.low',   'CoinChangerLow',               47, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 92, 12,  3,  7,  1, 'eventdefinition.paystation.coinchanger.level.high',  'CoinChangerHigh',              47, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 93, 12,  1,  1,  1, 'eventdefinition.paystation.coinchanger.jam.on',      'CoinChangerJam',               48, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 94, 12,  1,  2,  0, 'eventdefinition.paystation.coinchanger.jam.off',     'CoinChangerJamCleared',        48, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 99, 22,  0,  1,  0, 'eventdefinition.rfidcardreader.present.on',          'Present',                      49, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, 22, 0,  2,  0, 'eventdefinition.rfidcardreader.present.off',         'NotPresent',                   49, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101, 13, 1,  1,  3, 'eventdefinition.printer.paperjam.on',                'PaperJam',                     50, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102, 13, 1,  2,  0, 'eventdefinition.printer.paperjam.off',               'PaperJamCleared',              50, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 103, 12, 24,  1,  2, 'eventdefinition.paystation.ticketnottaken.on',      'TicketNotTaken',               51, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 104, 12, 24,  2,  0, 'eventdefinition.paystation.ticketnottaken.off',     'TicketNotTakenCleared',        51, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 105, 13, 25,  1,  3, 'eventdefinition.printer.ticketdidnotprint.on',      'TicketDidNotPrint',            52, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 106, 13, 25,  2,  0, 'eventdefinition.printer.ticketdidnotprint.off',     'TicketDidNotPrintCleared',     52, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 107, 13, 26,  1,  3, 'eventdefinition.printer.printingfailure.on',        'PrintingFailure',              53, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 108, 13, 26,  2,  0, 'eventdefinition.printer.printingfailure.off',       'PrintingFailureCleared',       53, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 109, 12, 27,  8,  1, 'eventdefinition.paystation.uid.changed',            'UIDChanged',                   54, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 110, 12, 27,  1,  3, 'eventdefinition.paystation.uid.on',                 'DuplicateSerialNumber',        55, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 111, 12, 27,  2,  0, 'eventdefinition.paystation.uid.off',                'DuplicateSerialNumberCleared', 55, UTC_TIMESTAMP(), 1);
INSERT EventDefinition (Id, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, Description, DescriptionNoSpace, EventTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES ( 112, 12, 28,  8,  1, 'eventdefinition.paystation.clock.changed',          'ClockChanged',                 56, UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Table `EventDeviceType`
-- -----------------------------------------------------

INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'eventdevicetype.battery',             	'Battery',			      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'eventdevicetype.battery2',              	'Battery2', 			  UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'eventdevicetype.billacceptor',          	'BillAcceptor', 	      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'eventdevicetype.billstacker',           	'BillStacker', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'eventdevicetype.cardreader',            	'CardReader', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'eventdevicetype.coinacceptor',          	'CoinAcceptor', 	      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'eventdevicetype.coinchanger',           	'CoinChanger', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'eventdevicetype.coinhopper1',          	'CoinHopper1', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'eventdevicetype.coinhopper2',          	'CoinHopper2', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'eventdevicetype.door',                   'Door', 		      	  UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'eventdevicetype.door.lower',           	'DoorLower', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'eventdevicetype.door.upper',           	'DoorUpper', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'eventdevicetype.paystation',            	'Paystation', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'eventdevicetype.printer',                'Printer', 			      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'eventdevicetype.shockalarm',            	'ShockAlarm',		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'eventdevicetype.pcm',                    'PCM', 				      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'eventdevicetype.coincanister',          	'CoinCanister', 	      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'eventdevicetype.door.maintain',          'DoorMaintenance', 		  UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'eventdevicetype.door.cashvault',        	'DoorCashVault', 		  UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'eventdevicetype.coinescrow',            	'CoinEscrow', 		      UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'eventdevicetype.customeralert', 	        'CustomerDefinedAlert',   UTC_TIMESTAMP(), 1) ;  -- CHANGED FOR EMS 7 (was Alert)
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'eventdevicetype.modem',                  'Modem', 				  UTC_TIMESTAMP(), 1) ;
INSERT EventDeviceType (Id, Name, NameNoSpace, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'eventdevicetype.rfidcardreader',       	'RfidCardReader', 	      UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `EventSeverityType`
-- -----------------------------------------------------

INSERT EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Clear',          UTC_TIMESTAMP(), 1) ;
INSERT EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Minor',          UTC_TIMESTAMP(), 1) ;
INSERT EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Major',          UTC_TIMESTAMP(), 1) ;
INSERT EventSeverityType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Critical',       UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `EventStatusType`
-- -----------------------------------------------------

INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Present',                  UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Jam',                      UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unable To Stack',          UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Level',                    UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Open',                     UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Low Power Shutdown',       UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Public Key Update',        UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Service Mode',             UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Upgrade',                  UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Ticket Footer Success',    UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Cutter Error',             UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Head Error',               UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Lever Disengaged',         UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Paper Status',             UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Temperature Error',        UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Voltage Error',            UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'On',                       UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Wireless Signal Strength', UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Remove',                   UTC_TIMESTAMP(), 1) ;
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Type',                     UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'CCID',                     UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Carrier',                  UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'APN',                      UTC_TIMESTAMP(), 1) ;  -- no instances in EMS 6 EventLogNew table
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 'Customer Defined Alert',   UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (24, 'Ticket Cup',				 UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7.1
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (25, 'Unable to Print',			 UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7.1
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (26, 'Printer Error',			 UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7.1
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (27, 'UID',   			         UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7.1
INSERT EventStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (28, 'Clock',   			     UTC_TIMESTAMP(), 1) ;  -- NEW FOR EMS 7.1

-- -----------------------------------------------------
-- Table `EventException`
-- -----------------------------------------------------

INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  7, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  8, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 19, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 19, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  7, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  8, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 19, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  6, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  7, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  8, UTC_TIMESTAMP(), 1);
INSERT EventException (PaystationTypeId, EventDeviceTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  20, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ExtensibleRateType`
-- -----------------------------------------------------

INSERT ExtensibleRateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Hourly',       UTC_TIMESTAMP(), 1) ;
INSERT ExtensibleRateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Special',      UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Table `LegacyPaymentType`
-- -----------------------------------------------------

INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A'        , 'Not Applicable'      , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Cash'       , 'Cash Only'           , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Credit Card', 'Credit Card Only'    , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Patroller'  , 'Patroller Card Only' , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Smart Card' , 'Smart Card Only'     , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cash/CC'    , 'Cash and Credit Card', UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Cash/SC'    , 'Cash and Smart Card' , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Passcard' , 'Passcard Only'     , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'No Data'    , 'Information Deleted' , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Cash/Passcard' , 'Cash and Passcard' , UTC_TIMESTAMP(), 1) ;
INSERT LegacyPaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unknown'    , 'Unknown Payment Type', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `LegacyTransactionType`
-- -----------------------------------------------------

INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown'    , 'Unknown Transaction Type'        , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'AddTime'    , 'Permit Extension'                , 1, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Regular'    , 'Permit'                          , 1, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Citation'   , 'Citation Payment'                , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'SCRecharge' , 'Smart Card Recharge'             , 0, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cancelled'  , 'Cancelled Purchase'              , 1, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Test'       , 'Test Purchase'                   , 0, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'No Data'    , 'Information Deleted'             , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Deposit'    , 'Patroller Deposit'               , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Replenish'  , 'Replenish Transaction'           , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Monthly'    , 'Monthly Permit'                  , 1, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Hopper1Add' , 'Replenish Hopper 1'              , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Hopper2Add' , 'Replenish Hopper 2'              , 0, 0, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'AddTime EbP', 'Extend by Phone Permit Extension', 1, 1, UTC_TIMESTAMP(), 1) ;
INSERT LegacyTransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Regular EbP', 'Extend by Phone Permit'          , 1, 1, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `LoginResultType`
-- -----------------------------------------------------

INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Successful Login',                   UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Invalid Credentials',                UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Invalid Credentials and Locked Out', UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'User Logout',                        UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Timeout Logout',                     UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'User Switched In',                   UTC_TIMESTAMP(), 1) ;
INSERT LoginResultType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'User Switched Out',                  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `ModemType`
-- -----------------------------------------------------

INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'No Modem',      UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Unknown',       UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'CDMA',          UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Ethernet',      UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'GSM',           UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'WiFi',          UTC_TIMESTAMP(), 1) ;
INSERT ModemType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'CellularModem', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `ParkingPermissionType`
-- -----------------------------------------------------

INSERT ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Time Limited',    UTC_TIMESTAMP(), 1) ;
INSERT ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unlimited',       UTC_TIMESTAMP(), 1) ;
INSERT ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Restricted',      UTC_TIMESTAMP(), 1) ;
INSERT ParkingPermissionType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Special',         UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `PaymentType`
-- -----------------------------------------------------

-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A', 		 'Not Applicable',					 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Cash', 			 'Cash Only',						 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'CC (Swipe)',		 'Credit Card Swipe Only',			 UTC_TIMESTAMP(), 1) ;
-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Patroller', 	 'Patroller Card Only',				 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Smart Card', 	 'Smart Card Only',					 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Cash/CC (Swipe)', 'Cash and Credit Card Swipe',		 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Cash/SC',		 'Cash and Smart Card',				 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Passcard', 		 'Passcard Only'     , 				 UTC_TIMESTAMP(), 1) ;
-- INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'No Data', 	 'Information Deleted' , 			 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Cash/Passcard',	 'Cash and Passcard' , 				 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unknown'	,		 'Unknown Payment Type', 			 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'CC (Tap)',		 'Credit Card Contactless Only',	 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Cash/CC (Tap)',	 'Cash and Credit Card Contactless', UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'CC (Chip)',		 'Credit Card Chip Only',			 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Cash/CC (Chip)',	 'Cash and Credit Card Chip',		 UTC_TIMESTAMP(), 1) ;
INSERT PaymentType (Id, Name, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'CC (External)',	 'Credit Card External',			 UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `PaystationType`
-- -----------------------------------------------------

INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  0, 'Other',       UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'V1 LUKE',     UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'SHELBY',      UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Radius LUKE', UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'FRODO', 		UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'LUKE II',     UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Test/Demo',   UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Virtual',     UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Verrus',      UTC_TIMESTAMP(), 1) ;
INSERT PaystationType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (255, 'Intellepay',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `ProcessorTransactionType`
-- -----------------------------------------------------

INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A'             , 'N/A'             , 'N/A'                               , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Auth PlaceHolder', 'PreAuth'         , 'Authorized but not yet settled'    , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Real-Time'       , 'Real-Time'       , 'Settled with no associated refunds', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Batch'           , 'Batched'         , 'Charged with no associated refunds', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Refund'          , 'Refunded'        , 'Refund of a charge or settlement'  , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Reversal'        , ''                , ''                                  , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Real-Time'       , 'Real-Time'       , 'Settled with associated refunds'   , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Batch'           , 'Batched'         , 'Charged with associated refunds'   , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Uncloseable'     , 'Uncloseable'     , 'PreAuth expired, cannot settle'    , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, '3rd Party'       , '3rd Party'       , 'Charged externally with no refunds', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Batch'           , 'Batched'         , 'Charged EMS retry with no refunds' , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Store & Fwd'     , 'Store & Fwd'     , 'Charged SF with no refunds'        , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Batch'           , 'Batched'         , 'Charged EMS retry with refunds'    , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Store & Fwd'     , 'Store & Fwd'     , 'Charged SF with refunds'           , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Encryption Error', 'Encryption Error', 'Invalid encryption'                , 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Real-Time'       , 'Extend by Phone' , 'Charged with no associated refunds', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Refund'          , 'Extend by Phone' , 'Refund of a charge or settlement'  , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Real-Time'       , 'Extend by Phone' , 'Charged with associated refunds'   , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'Recoverable'     , 'Recoverable'     , 'PreAuth expired but is recoverable', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Real-Time'       , 'Single Trx'      , 'Charged with no associated refunds', 0, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 'Refund'          , 'Single Trx'      , 'Refund of a charge'                , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 'Real-Time'       , 'Single Trx'      , 'Charged with associated refunds'   , 1, UTC_TIMESTAMP(), 1) ;
INSERT ProcessorTransactionType (Id, Name, StatusName, Description, IsRefund, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 'Cancel'          , 'Cancel'          , 'Cancelled'                         , 0, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Permission`
-- -----------------------------------------------------

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 100, NULL, 'Reports Management',                      		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 101, 100,  'Manage Reports',                      	  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 102, 100,  'Search for Transactions in Permit Lookup', 		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 200, NULL, 'Alerts Management',        		                NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 201,  200, 'View Current Alerts',           		        NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 202,  200, 'Manage User Defined Alerts',                    NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 300, NULL, 'Collections Management',                        NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 301,  300, 'View Collection Status',                        NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 400, NULL, 'Card Management',                               NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 401,  400, 'Issue Refunds',                                 NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 402,  400, 'View Banned Cards',     		                NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 403,  400, 'Manage Banned Cards'	,       	            NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 404,  400, 'Process Card Charges From BOSS',                NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 405,  400, 'Setup Credit Card Processing',                  NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 406,  400, 'View Campus Cards',                             NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 407,  400, 'Manage Campus Cards',                           NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 408,  400, 'Configure Campus Cards',          	      	    NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 500, NULL, 'Customer Account Management',                   NULL, 1, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 501,  500, 'View Passcards',                                NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 502,  500, 'Manage Passcards',                              NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 503,  500, 'Configure Passcards',                           NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 504,  500, 'View Coupons',                                  NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 505,  500, 'Manage Coupons',                                NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 506,  500, 'View Customer Accounts',                        NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 507,  500, 'Manage Customer Accounts',                      NULL, 0, UTC_TIMESTAMP(), 1) ;	
	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 600, NULL, 'Extend-By-Phone',                               NULL, 1, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 601,  600, 'View Rates & Policies',                         NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 602,  600, 'Manage Rates & Policies',                       NULL, 0, UTC_TIMESTAMP(), 1) ;	

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 700, NULL, 'User Account Management',                       NULL, 1, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 701,  700, 'View Users & Roles',                            NULL, 0, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 702,  700, 'Manage Users & Roles',                          NULL, 0, UTC_TIMESTAMP(), 1) ;	
	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 800, NULL, 'Pay Station Configuration Settings',            NULL, 1, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 801,  800, 'Upload Configurations from BOSS',               NULL, 0, UTC_TIMESTAMP(), 1) ;	
	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 900, NULL, 'System Activity',                               NULL, 1, UTC_TIMESTAMP(), 1) ;	
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES ( 901,  900, 'View System Activities',                        NULL, 0, UTC_TIMESTAMP(), 1) ;	

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1000, NULL, 'Pay Station Management',                  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1001, 1000, 'View Locations',                          		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1002, 1000, 'Manage Locations',                        		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1003, 1000, 'View Pay Stations',                             NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1004, 1000, 'Manage Pay Stations',                           NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1005, 1000, 'View Routes',                                   NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1006, 1000, 'Manage Routes',                                 NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1007, 1000, 'System Settings',                               NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1008, 1000, 'View Pay Station Settings',               		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1009, 1000, 'Manage Pay Station Settings',             		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1010, 1000, 'Manage Pay Station Placement',            		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1050, NULL, 'Remote Access',                           		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1051, 1050, 'Access Digital API',                      		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1052, 1050, 'PDA Access',                              		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1100, NULL, 'DPT Pay Station Administration',         		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1101, 1100, 'View Pay Station',                       		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1102, 1100, 'Manage Pay Station',                      		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1103, 1100, 'Move Pay Station',                       		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1104, 1100, 'Set Billing Parameters',                  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1105, 1100, 'Create Pay Stations',                     		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1106, 1100, 'Manage Pay Station Placement',            		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1200, NULL, 'DPT Customer Administration',            		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1201, 1200, 'Create New Customer',                     		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1202, 1200, 'Edit Existing Customer',                  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1203, 1200, 'View Customers',                          		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1300, NULL, 'System Notification Administration',      		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1301, 1300, 'View Notifications',                      		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1302, 1300, 'Manage Notifications',                    		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1400, NULL, 'Digital API Administration',              		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1401, 1400, 'View Licenses',                           		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1402, 1400, 'Manage Licenses',                         		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1500, NULL, 'Server Administration',                   		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1501, 1500, 'View Server Status',                      		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1502, 1500, 'Server Operations',                       		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1600, NULL, 'Dashboard Management',                    		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1601, 1600, 'View Dashboard',                   		 		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1602, 1600, 'Manage Dashboard',                   	  		NULL, 1, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1700, NULL, 'Mobile App License Admin', 				  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1701, 1700, 'View Mobile Licenses',					 		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1702, 1700, 'Manage Mobile Licenses',				  		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1800, NULL, 'Mobile Device Administration', 			  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1801, 1800, 'View Mobile Devices',					  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1802, 1800, 'Manage Mobile Devices',				      		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1900, NULL, 'Signing Server Access', 			  	  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1901, 1900, 'Generate Signature',					  		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2000, NULL, 'Online Rate Configuration', 			  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2001, 2000, 'View Online Rates',						  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2002, 2000, 'Manage Online Rates',					 		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2100, NULL, 'Customer Migration Administration', 	  		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2101, 2100, 'View Customer Migration Status',		  		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2102, 2100, 'Manage Customer Migration Status',		  		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2200, NULL, 'Flex WS Credentials',                     		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2201, 2200, 'View Flex WS Credentials',                		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2202, 2200, 'Manage Flex WS Credentials',              		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2300, NULL, 'DPT Flex WS Credentials',                 		NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2301, 2300, 'DPT View Flex WS Credentials',            		NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2302, 2300, 'DPT Manage Flex WS Credentials',          		NULL, 0, UTC_TIMESTAMP(), 1) ;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2400, NULL, 'DPT AutoCount Integration',                 	NULL, 1, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2401, 2400, 'DPT View AutoCount Integration', 	     	    NULL, 0, UTC_TIMESTAMP(), 1) ;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (2402, 2400, 'DPT Manage AutoCount Integration',	            NULL, 0, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `PermitIssueType`
-- -----------------------------------------------------

INSERT PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',              UTC_TIMESTAMP(), 1) ;
INSERT PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Pay & Display',    UTC_TIMESTAMP(), 1) ;
INSERT PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Pay by Plate',     UTC_TIMESTAMP(), 1) ;
INSERT PermitIssueType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Pay by Space',     UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `PermitType`
-- -----------------------------------------------------

INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',                     UTC_TIMESTAMP(), 1) ;
INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Regular',                 UTC_TIMESTAMP(), 1) ;
INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Addtime',                 UTC_TIMESTAMP(), 1) ;
INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Regular EbP',             UTC_TIMESTAMP(), 1) ;
INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Addtime EbP',             UTC_TIMESTAMP(), 1) ;
INSERT PermitType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Monthly',                 UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `POSDateType`
-- -----------------------------------------------------

-- These rows are `current state`: inserting one of these into table `POSDate` results in an update to table `POSStatus`
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'Is Provisioned',                     UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'Is Locked',                          UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'Is Decommissioned',                  UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'Is Deleted',                         UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'Is Visible',                         UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'Is Billable Monthly on Activation',  UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'Is Digital Connect',                 UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'Is Test Activated',                  UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'Is Activated',                       UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'Is Billable Monthly (for EMS)',      UTC_TIMESTAMP(), 1) ;

-- These rows are `informational only`: inserting one of these into table `POSDate` does NOT result in an update to table `POSStatus`
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (201, 'Was Moved To',                       UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (202, 'Was Moved Away',                     UTC_TIMESTAMP(), 1) ;
INSERT POSDateType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (203, 'Comment Added',                      UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Processor`
-- -----------------------------------------------------

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 1, 'processor.concord', 'https://prod.dw.us.fdcnet.biz/efsnet2.dll', 'https://stg.dw.us.fdcnet.biz/efsnet2.dll', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 2, 'processor.firstDataNashville', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/nocportal/SRS.do', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 3, 'processor.heartland', 'https://sslprod.secureexchange.net:54411', 'https://sslhps.test.secureexchange.net:15031', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 4, 'processor.link2gov', 'https://gate.link2gov.com/api/', 'https://ca.link2gov.com/api/', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 5, 'processor.parcxmart', 'http://www.parcxmart.net/pxtaccept/pxtaccept.dll', 'http://www.parcxmart.net/pxtaccept/pxtaccept.dll', 1, 0, 0, 0, 1, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 6, 'processor.alliance', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/staging_expresso/SRS.do', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 7, 'processor.paymentech', 'https://netconnect1.paymentech.net/NetConnect/controller,https://netconnect2.paymentech.net/NetConnect/controller', 'https://netconnectvar1.paymentech.net/NetConnect/controller,https://netconnectvar2.paymentech.net/NetConnect/controller', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 8, 'processor.paradata', '', '', 1, 1, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 9, 'processor.moneris', 'www3.moneris.com', 'esqa.moneris.com', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 10, 'processor.firstHorizon', 'https://gateway-bmd.nxt.com/FH-Inet/process_transaction.cgi', 'https://gateway-bmd.nxt.com:4443/FH-Inet/process_transaction.cgi', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 11, 'processor.authorize.net', 'https://secure2.authorize.net/gateway/transact.dll', 'https://test.authorize.net/gateway/transact.dll', 1, 1, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 12, 'processor.datawire', 'https://support.datawire.net/production_expresso/SRS.do', 'https://stagingsupport.datawire.net/staging_expresso/SRS.do', 1, 0, 0, 0, 1, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 13, 'processor.blackboard', '', '66.210.59.124,69.26.224.124', 1, 0, 0, 1, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 14, 'processor.totalcard', '', 'as400.ucen.ucsb.edu:3030', 1, 0, 0, 1, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 15, 'processor.nuvision', '', '74.92.119.157', 1, 0, 0, 1, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 16, 'processor.elavon', 'https://www.myvirtualmerchant.com/VirtualMerchant/processxml.do', 'https://demo.myvirtualmerchant.com/VirtualMerchantDemo/processxml.do', 1, 1, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 17, 'processor.cbord.csgold', '', '', 1, 0, 0, 1, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 18, 'processor.cbord.odyssey', '', '', 1, 0, 0, 1, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 19, 'processor.elavon.viaconex', 'https://webgate.viaconex.com/cgi-bin/encompass4.cgi', 'https://testgate.viaconex.com/cgi-bin/encompass4.cgi', 1, 0, 0, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

INSERT Processor (Id,Name,ProductionUrl,TestUrl,IsTest,IsGateway,IsEMV,IsForValueCard,IsExcluded,VERSION,LastModifiedGMT,LastModifiedByUserId)
VALUES ( 20, 'processor.creditcall', '', '', 1, 0, 1, 0, 0, 0, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `QuarterHour`
-- -----------------------------------------------------

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 0,  0, 0, '00:00 AM', '00:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 1,  0, 1, '00:15 AM', '00:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 2,  0, 2, '00:30 AM', '00:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 3,  0, 3, '00:45 AM', '00:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 4,  1, 0, '01:00 AM', '01:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 5,  1, 1, '01:15 AM', '01:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 6,  1, 2, '01:30 AM', '01:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 7,  1, 3, '01:45 AM', '01:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 8,  2, 0, '02:00 AM', '02:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES ( 9,  2, 1, '02:15 AM', '02:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (10,  2, 2, '02:30 AM', '02:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (11,  2, 3, '02:45 AM', '02:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (12,  3, 0, '03:00 AM', '03:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (13,  3, 1, '03:15 AM', '03:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (14,  3, 2, '03:30 AM', '03:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (15,  3, 3, '03:45 AM', '03:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (16,  4, 0, '04:00 AM', '04:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (17,  4, 1, '04:15 AM', '04:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (18,  4, 2, '04:30 AM', '04:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (19,  4, 3, '04:45 AM', '04:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (20,  5, 0, '05:00 AM', '05:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (21,  5, 1, '05:15 AM', '05:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (22,  5, 2, '05:30 AM', '05:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (23,  5, 3, '05:45 AM', '05:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (24,  6, 0, '06:00 AM', '06:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (25,  6, 1, '06:15 AM', '06:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (26,  6, 2, '06:30 AM', '06:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (27,  6, 3, '06:45 AM', '06:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (28,  7, 0, '07:00 AM', '07:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (29,  7, 1, '07:15 AM', '07:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (30,  7, 2, '07:30 AM', '07:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (31,  7, 3, '07:45 AM', '07:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (32,  8, 0, '08:00 AM', '08:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (33,  8, 1, '08:15 AM', '08:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (34,  8, 2, '08:30 AM', '08:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (35,  8, 3, '08:45 AM', '08:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (36,  9, 0, '09:00 AM', '09:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (37,  9, 1, '09:15 AM', '09:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (38,  9, 2, '09:30 AM', '09:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (39,  9, 3, '09:45 AM', '09:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (40, 10, 0, '10:00 AM', '10:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (41, 10, 1, '10:15 AM', '10:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (42, 10, 2, '10:30 AM', '10:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (43, 10, 3, '10:45 AM', '10:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (44, 11, 0, '11:00 AM', '11:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (45, 11, 1, '11:15 AM', '11:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (46, 11, 2, '11:30 AM', '11:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (47, 11, 3, '11:45 AM', '11:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (48, 12, 0, '12:00 PM', '12:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (49, 12, 1, '12:15 PM', '12:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (50, 12, 2, '12:30 PM', '12:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (51, 12, 3, '12:45 PM', '12:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (52, 13, 0, '01:00 PM', '13:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (53, 13, 1, '01:15 PM', '13:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (54, 13, 2, '01:30 PM', '13:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (55, 13, 3, '01:45 PM', '13:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (56, 14, 0, '02:00 PM', '14:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (57, 14, 1, '02:15 PM', '14:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (58, 14, 2, '02:30 PM', '14:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (59, 14, 3, '02:45 PM', '14:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (60, 15, 0, '03:00 PM', '15:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (61, 15, 1, '03:15 PM', '15:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (62, 15, 2, '03:30 PM', '15:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (63, 15, 3, '03:45 PM', '15:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (64, 16, 0, '04:00 PM', '16:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (65, 16, 1, '04:15 PM', '16:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (66, 16, 2, '04:30 PM', '16:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (67, 16, 3, '04:45 PM', '16:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (68, 17, 0, '05:00 PM', '17:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (69, 17, 1, '05:15 PM', '17:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (70, 17, 2, '05:30 PM', '17:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (71, 17, 3, '05:45 PM', '17:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (72, 18, 0, '06:00 PM', '18:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (73, 18, 1, '06:15 PM', '18:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (74, 18, 2, '06:30 PM', '18:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (75, 18, 3, '06:45 PM', '18:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (76, 19, 0, '07:00 PM', '19:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (77, 19, 1, '07:15 PM', '19:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (78, 19, 2, '07:30 PM', '19:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (79, 19, 3, '07:45 PM', '19:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (80, 20, 0, '08:00 PM', '20:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (81, 20, 1, '08:15 PM', '20:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (82, 20, 2, '08:30 PM', '20:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (83, 20, 3, '08:45 PM', '20:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (84, 21, 0, '09:00 PM', '21:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (85, 21, 1, '09:15 PM', '21:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (86, 21, 2, '09:30 PM', '21:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (87, 21, 3, '09:45 PM', '21:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (88, 22, 0, '10:00 PM', '22:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (89, 22, 1, '10:15 PM', '22:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (90, 22, 2, '10:30 PM', '22:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (91, 22, 3, '10:45 PM', '22:45', 1, UTC_TIMESTAMP());

INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (92, 23, 0, '11:00 PM', '23:00', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (93, 23, 1, '11:15 PM', '23:15', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (94, 23, 2, '11:30 PM', '23:30', 1, UTC_TIMESTAMP());
INSERT QuarterHour (Id,HourOfDay,QuarterOfHour,TimeAmPm,Time24Hour,LastModifiedByUserId,LastModifiedGMT) VALUES (95, 23, 3, '11:45 PM', '23:45', 1, UTC_TIMESTAMP());

-- -----------------------------------------------------
-- Table `RestAccountType`
-- -----------------------------------------------------

INSERT RestAccountType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Transaction Push',   UTC_TIMESTAMP(), 1) ;
INSERT RestAccountType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Coupon Push',   	   UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `RevenueType`
-- -----------------------------------------------------

INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, NULL, 1, 'Total'      , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2,    1, 2, 'Cash'       , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3,    1, 2, 'Credit Card'       , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4,    2, 3, 'Coin'       , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5,    2, 3, 'Bill'       , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6,    2, 3, 'Legacy Cash', UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7,    3, 3, 'Credit Card Swipe', UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10,   1, 2, 'OtherCards' , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11,   3, 3, 'Credit Card Tap' , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12,   3, 3, 'Credit Card Chip' , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8,    10, 3, 'Smart Card' , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9,    10, 3, 'Passcard' , UTC_TIMESTAMP(), 1) ;
INSERT RevenueType (Id, ParentRevenueTypeId, Level, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13,    3, 3, 'CC External' , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------

INSERT Role (Id,CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,(SELECT Id FROM Customer WHERE Name='Digital (DPT)'), 1, 1, 'System Admin', 1, 1, 0, UTC_TIMESTAMP(), 1) ;
INSERT Role (Id,CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2,(SELECT Id FROM Customer WHERE Name='Digital (DPT)'), 2, 1, 'Corporate Administrator', 1, 1, 0, UTC_TIMESTAMP(), 1) ;
INSERT Role (Id,CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,(SELECT Id FROM Customer WHERE Name='Digital (DPT)'), 3, 1, 'Administrator' , 1, 1, 0, UTC_TIMESTAMP(), 1) ;
INSERT Role (Id,CustomerId, CustomerTypeId, RoleStatusTypeId, Name, IsAdmin, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (4,(SELECT Id FROM Customer WHERE Name='Digital (DPT)'), 1, 1, 'Signing Admin' , 1, 1, 0, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `RolePermission`
-- -----------------------------------------------------

-- Role: 'System Admin'
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  101, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  201, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  202, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  701, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  702, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1001, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1002, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1005, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1006, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1106, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1101, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1102, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1103, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1104, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1105, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1201, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1202, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1203, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1301, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1302, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1401, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1402, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1501, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1502, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1701, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1702, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1801, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1802, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1900, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1901, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2101, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2102, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2301, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2302, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2401, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 2402, 0, UTC_TIMESTAMP(), 1);

-- Role: 'Parent Admin'
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  101, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  701, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  702, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  901, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 1007, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 1601, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 1602, 0, UTC_TIMESTAMP(), 1);

-- Role: 'Child Admin'
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  101, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  102, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  201, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  202, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  301, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  401, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  402, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  403, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  404, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  405, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  406, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  407, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  408, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  501, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  502, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  503, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  504, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  505, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  506, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  507, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  601, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  602, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  701, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  702, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  801, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  901, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1001, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1002, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1003, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1004, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1005, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1006, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1007, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1008, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1009, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1010, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1051, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1052, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1601, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1602, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1801, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1802, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 2000, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 2001, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 2002, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 2201, 0, UTC_TIMESTAMP(), 1);
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 2202, 0, UTC_TIMESTAMP(), 1);


-- Signing Admin
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 1901, 0, UTC_TIMESTAMP(), 1);

 

-- -----------------------------------------------------
-- Table `RouteType`
-- -----------------------------------------------------

INSERT RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Collections',   23, UTC_TIMESTAMP(), 1) ;
INSERT RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Maintenance',  975, UTC_TIMESTAMP(), 1) ;
INSERT RouteType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Other',       2494, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `SubscriptionType`
-- -----------------------------------------------------

INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  100, NULL, 'Standard Reports',                    1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  200, NULL, 'Alerts',                              1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  300, NULL, 'Real-Time Credit Card Processing',    1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  400, NULL, 'Batch Credit Card Processing',        1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  500, NULL, 'Real-Time Campus Card Processing',    1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  600, NULL, 'Coupons',                             1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  700, NULL, 'Passcards',                           1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  800, NULL, 'Smart Cards',                         1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES (  900, NULL, 'Extend-By-Phone',                     1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1000, NULL, 'Digital API: Read',                   1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1100, NULL, 'Digital API: Write',                  1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1200, NULL, '3rd Party Pay-By-Cell Integration',   1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1300, NULL, 'Mobile: Digital Collect',             1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1600, NULL, 'Online Pay Station Configuration',    1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1700, NULL, 'FLEX Integration',                    1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1800, NULL, 'AutoCount Integration',               1, 0, UTC_TIMESTAMP(), 1) ;
INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1900, NULL, 'Digital API: XChange',                1, 1, UTC_TIMESTAMP(), 1) ;
-- -----------------------------------------------------
-- Table `SmsMessageType`
-- -----------------------------------------------------

INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Transaction received from pay station',                                UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Expiry alert sent, option to extend',                                  UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Expiry alert sent, no option to extend',                               UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'User response received',                                               UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Invalid user response received',                                       UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Failure message sent to user',                                         UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Extension confirmation sent',                                          UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'SMS send failure',                                                     UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Credit card declined',                                                 UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Unable to process credit card',                                        UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Time auto-extended',                                                   UTC_TIMESTAMP(), 1) ;
INSERT SmsMessageType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Message from phone number not associated with active parking session', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `Timezone`
-- 
-- Q: Where is `Name` coming from?
-- -----------------------------------------------------

INSERT TimeZone (Id, Code, Name, CodeDST, NameDST, OffsetFromGMT, OffsetFromDST, LastModifiedGMT, LastModifiedByUserId)
VALUES (1, 'PST', 'US/Pacific', 'PDT', 'Pacific Daylight Time', '-08:00', '-07:00', UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `TransactionType`
-- -----------------------------------------------------

INSERT TransactionType (Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId) 
SELECT Id, Name, Description, IsRevenue, IsPurchase, LastModifiedGMT, LastModifiedByUserId
FROM   LegacyTransactionType 
WHERE  Id IN (1,2,4,5,6,12,16,17);

INSERT INTO TransactionType (Id,Name,Description,IsRevenue,IsPurchase,LastModifiedGMT,LastModifiedByUserId) VALUES
(0,'Unknown','Unknown',1,1,UTC_TIMESTAMP(),1);

-- -----------------------------------------------------
-- Table `Section`
-- -----------------------------------------------------

INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (1, 1, 1, 5, 1, 0, UTC_TIMESTAMP()) ; -- Finance
INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (2, 1, 1, 3, 2, 0, UTC_TIMESTAMP()) ;
INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (3, 1, 2, 3, 1, 0, UTC_TIMESTAMP()) ; -- Operations
INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (4, 1, 3, 1, 1, 0, UTC_TIMESTAMP()) ; -- Map
INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (5, 1, 4, 3, 1, 0, UTC_TIMESTAMP()) ; -- Parket Metrics
INSERT Section (Id, UserAccountId, TabId, LayoutId, OrderNumber, VERSION, LastModifiedGMT) VALUES (6, 1, 5, 3, 1, 0, UTC_TIMESTAMP()) ; -- Card Processing

-- -----------------------------------------------------
-- Table `Tab`
-- -----------------------------------------------------

INSERT Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (1, 1, 'Finance',       		1, 0, UTC_TIMESTAMP()) ;
INSERT Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (2, 1, 'Operations',    		2, 0, UTC_TIMESTAMP()) ;
INSERT Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (3, 1, 'Map',           		3, 0, UTC_TIMESTAMP()) ;
INSERT Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (4, 1, 'Parker Metrics',		4, 0, UTC_TIMESTAMP()) ;
INSERT Tab (Id, UserAccountId, Name, OrderNumber, VERSION, LastModifiedGMT) VALUES (5, 1, 'Card Processing',	5, 0, UTC_TIMESTAMP()) ;

-- -----------------------------------------------------
-- Table `UserDefaultDashboard` - needed for the default dash board
-- -----------------------------------------------------

-- INSERT UserAccount (CustomerId, UserStatusTypeId, UserName, CustomerEmailId, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (1, 1, 'DPT+Admin', NULL, 'root', 'admin', '58ae83e00383273590f96b385ddd700802c3f07d', 'salt', 1, 0, UTC_TIMESTAMP(), 1);   -- DPT Admin

-- INSERT UserDefaultDashboard (UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
-- VALUES (1, 1, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `UserStatusType`
-- -----------------------------------------------------

INSERT UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', UTC_TIMESTAMP(), 1) ;
INSERT UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  UTC_TIMESTAMP(), 1) ;
INSERT UserStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Deleted',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WebServiceEndPointType`
-- -----------------------------------------------------

INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'StallInfo',        0, UTC_TIMESTAMP(), 1) ;
INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'PayStationInfo',   0, UTC_TIMESTAMP(), 1) ;
INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'TransactionInfo',  0, UTC_TIMESTAMP(), 1) ;
INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'PlateInfo',        0, UTC_TIMESTAMP(), 1) ;
INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'AuditInfo',	       0, UTC_TIMESTAMP(), 1) ;
INSERT WebServiceEndPointType (Id, Name, IsPrivate, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'TransactionData',  1, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerAgreement` for 'Digital (DPT)'
-- -----------------------------------------------------
INSERT CustomerAgreement (CustomerId, ServiceAgreementId, Name, Title, Organization, AgreedGMT, IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
SELECT Id, 1, 'Mike Simmons', 'CEO', 'Digital Payment Technologies' , UTC_TIMESTAMP() , 1, 0, UTC_TIMESTAMP(), 1
FROM   Customer WHERE Name = 'Digital (DPT)'; 

-- -----------------------------------------------------
-- Table `Widget`
-- -----------------------------------------------------

-- Finance
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,1,1,6,3,105,0,0,6,0,0,1,'Total Revenue by Day','Total revenue for last 30 days grouped by day',0,0,700000,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,1,1,6,0,0,0,0,7,0,1,1,'Total Revenue Last 30 Days','The total revenue for last 30 days',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,1,1,5,0,0,0,0,7,0,1,2,'Total Revenue Last 7 Days','The total revenue for last 7 days',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,1,6,105,0,0,0,2,0,0,1,'Total Revenue by Location','Total revenue for the last 30 days grouped by location',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,1,6,105,0,0,2,1,1,0,2,'Top 5 Locations Last 30 Days','The top 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,1,6,105,0,0,0,5,0,1,1,'Total Revenue by Location','Total revenue for this month grouped by location',0,0,0,0,0,0,1,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,1,6,105,0,0,3,1,1,1,2,'Bottom 5 Locations - 30 Day','The bottom 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,4,6,3,108,0,1,5,0,2,1,'Purchases by Rate',NULL,0,0,0,0,1,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,2,1,6,107,0,0,2,1,1,2,2,'Top 5 Pay Stations - 30 Day','The top 5 pay stations based on total revenue for last 30 days',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Operations
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,5,1,105,0,0,0,5,0,0,1,'Paid Occupancy by Location',NULL,0,0,8500,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,5,4,2,105,0,0,3,0,0,2,'Paid Occupancy by Hour',NULL,0,0,8500,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,4,4,2,105,0,0,3,0,1,1,'Purchases by Hour',NULL,0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,6,6,3,0,0,0,3,0,1,2,'Space Turnover by Day',NULL,0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,9,1,106,0,0,0,5,0,2,1,'Pay Stations with Alerts',NULL,0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,3,7,3,105,0,0,0,5,0,2,2,'Utilization by Location',NULL,0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Map
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,2,4,11,1,105,0,0,0,8,0,0,1,'Map',NULL,0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Parker Metrics
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,3,5,13,6,3,105,0,0,3,0,0,1,'Avg Revenue by Day','Average revenue per space for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,3,5,12,5,3,105,0,0,3,0,1,1,'Avg Price by Day','Average price per permit for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (1,3,5,14,5,3,0,0,0,3,0,2,1,'Avg Duration by Day','Average duration per space for the last 7 days grouped by day',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Default Dashboard
-- Card Processing - Purchases
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		15,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		15,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Revenue by Method - 24h','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
-- Card Processing - Revenue
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		16,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 2,					6,		16,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Purchases by Method - 24h','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());


-- Parent - Finance 
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	1,	1,	6,	3,		103,	0,	0,	6,	0,	0,	1,	'Total Revenue by Day', 			'Total revenue for the last 30 days grouped by day',				0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	1,	1,	6,	0,		0,		0,	0,	7,	0,	1,	1,	'Total Revenue Last 30 Days',		'The total revenue for the the last 30 days ',						0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	1,	1,	5,	0,		0,		0,	0,	7,	0,	1,	2,	'Total Revenue Last 7 Days',		'The total revenue for the last 7 days',							0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	0,	2,	0,	0,	1,	'Total Revenue by Organization',	'Total revenue for the last 30 days grouped by organization',		0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	2,	1,	1,	0,	2,	'Top 5 - 30 Days',			'The top 5 organizations based on total revenue for the last 30 days',0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	3,	1,	1,	0,	3,	'Bottom 5 - 30 Days',		'The bottom 5 organizations based on total revenue for the last 30 days',0,1,0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	103,	0,		0,	0,	5,	0,	1,	1,	'Total Revenue by Organization',	'Total revenue for the last 30 days grouped by organization',		0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	105,	0,		0,	2,	1,	1,	1,	2,	'Top 5 Locations - 30 Days',			'The top 5 locations based on total revenue for the last 30 days',0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	6,	105,	0,		0,	3,	1,	1,	1,	3,	'Bottom 5 Locations - 30 Days',		'The bottom 5 locations based on total revenue for the last 30 days',0,1,0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	7,	5,		201,	0,	1,	5,	0,	2,	1,	'Total Revenue by Month',			'Total revenue for the last 12 months grouped by month',			0,	1,	0,	0,	1,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	2,	1,	4,	2,		0,		0,	0,	5,	0,	2,	2,	'Total Revenue by Hour',			'Total revenue for the last 24 hours grouped by hour',				0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());

-- Operations - Parent
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	7,	5,		201,	0,	1,	5,	0,	0,	1,	'Total Purchases by Month',			'Total purchases for the last 12 months grouped by month',			0,	1,	0,	0,	1,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	6,	201,	0,		0,	1,	5,	0,	0,	2,	'Total Purchases by Rev Type',		'Total number of purchases for the last 30 days grouped by revenue type',0,1,0,	1,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	5,	3,		201,	0,	1,	6,	0,	1,	1,	'Total Purchases by Day',			'Total purchases for the last 7 days grouped by day',				0,	1,	0,	0,	1,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	6,	103,	0,		0,	0,	5,	0,	1,	2,	'Total Purchases by Org',			'Total number of purchases the last 30 days grouped by organization',0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	4,	2,		201,	0,	1,	5,	0,	2,	1,	'Total Purchases by Hour',			'Total purchases for the last 24 hours grouped by hour',			0,	1,	0,	0,	1,	0,	0,	0,	0,	UTC_TIMESTAMP());
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	3,	4,	6,	103,	0,		0,	0,	1,	0,	2,	2,	'Total Purchases by Org',			'Total number of purchases for the last 30 days grouped by organization',0,1,0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());

-- Map - Parent
INSERT Widget 	(WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES 		(5,	1,	4,	11,	1,	103,	0,		0,	0,	8,	0,	0,	1,	'Map',								'',																	0,	1,	0,	0,	0,	0,	0,	0,	0,	UTC_TIMESTAMP());

-- Default Dashboard Parent
-- Card Processing - Purchases - Parent
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		15,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		15,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Revenue by Method - 24h','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
-- Card Processing - Revenue - Parent
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		1,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		16,					6,					3,				205,					0,				0,					5,					0,				1,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 5,					6,		16,					4,					205,			0,						0,				0,					2,					0,				2,		3,		'Purchases by Method - 24h','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());


-- Widget Master List 

-- Total Revenue                                                                                                                                                                                                                                               
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue Last 30 Days'    ,'The total revenue for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Hour'	       ,'Total revenue for the last 24 hours grouped by hour'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Day'	       ,'Total revenue for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Month'	       ,'Total revenue for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,4                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Rate'	       ,'Total revenue for the last 24 hours grouped by rate'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
-- INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,1               ,1        ,1                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue by Parent Org'   ,'Total revenue for this month grouped by parent organization'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Organization' ,'Total revenue for the last 30 days grouped by organization'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Location(P)'  ,'Total revenue for the last 30 days grouped by parent location'      ,0               ,0         ,0          ,0            ,0            ,0            ,1                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Location'     ,'Total revenue for the last 30 days grouped by location'             ,0               ,0         ,0          ,0            ,0            ,0            ,1                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Route'	       ,'Total revenue for the last 30 days grouped by route'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,202              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Trans Type'   ,'Total revenue for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Revenue by Revenue Type' ,'Total revenue for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,107              ,0                ,0                ,2                 ,1                ,1                ,0       ,1          ,'Top 5 - 30 Days'               ,'The top 5 pay stations based on total revenue for the last 30 days'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,1                 ,6                ,107              ,0                ,0                ,3                 ,1                ,1                ,0       ,1          ,'Bottom 5 - 30 Days'		       ,'The bottom 5 pay stations based on total revenue for the last 30 days'  ,0             ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Total Purchases
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Hour'	   ,'Total purchases for the last 24 hours grouped by hour'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Day'	       ,'Total purchases for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Month'	   ,'Total purchases for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
-- INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,1               ,1        ,4                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Purchases by Parent Org' ,'Total number of purchases this month grouped by parent organization',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Org'	       ,'Total number of purchases the last 30 days grouped by organization' ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Location(P)','Total number of purchases the last 30 days grouped by parent location'   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Location'   ,'Total number of purchases the last 30 days grouped by location'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Route'	   ,'Total number of purchases the last 30 days grouped by route'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rev Type'   ,'Total number of purchases the last 30 days grouped by revenue type' ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,4                 ,6                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rate'       ,'Total number of purchases the last 30 days grouped by rate'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Space Turnover
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,6                 ,6                ,3                ,0              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Turnover by Day'	   		   ,'Space turnover for the last 30 days grouped by day'         		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,6                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Turnover by Month'	      	   ,'Space turnover for the last 12 months grouped by month'     		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,6                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Turnover by Location(P)'       ,'Space turnover for the last 30 days grouped by parent location'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,6                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Turnover by Location'  		   ,'Space turnover for the last 30 days grouped by location'    		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,6                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Turnover 30 Days'        ,'The total turnover for the last 30 days'                            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());

-- Utilization
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,7                 ,6                ,3                ,105              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Utilization by Day'  		   ,'Utilization for the last 30 days grouped by day and location'		 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,7                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Utilization by Month'	   	   ,'Utilization for the last 12 months grouped by month'    			 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,7                 ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Utilization by Location(P)'	   ,'Utilization for the last 30 days grouped by parent location'    	 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,7                 ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Utilization by Location' 	   ,'Utilization for the last 30 days grouped by location'    			 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,7                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Utilization 30 Days'     ,'The total average utilization for the last 30 days'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());

-- Occupancy
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,5                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Paid Occupancy by Hour'	       ,'Paid occupancy for the last 24 hours grouped by hour'               ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,5                 ,6                ,3                ,105              ,0                ,0                 ,3                ,0                ,0       ,1          ,'Paid Occupancy by Day'  	   ,'Paid occupancy for the last 30 days grouped by day and by location' ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,5                 ,1                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Location(P)' ,'Paid occupancy for the last 30 days grouped by parent location'     ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,5                 ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Location'    ,'Paid occupancy for the last 30 days grouped by location'            ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,5                 ,4                ,108              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Paid Occupancy by Rate' 	   ,'Paid occupancy for the last 30 days grouped by rate'                ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Pay Stations with Active Alerts
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,9                 ,1                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Pay Stations with Alerts'	   ,'Total number of pay stations with active alerts'                    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,9                 ,1                ,204              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Type'	               ,'Total number of pay stations with active alerts grouped by type'	 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,9                 ,1                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Location(P)'         ,'Total number of pay stations with active alerts grouped by par location' ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,9                 ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Location'			   ,'Total number of pay stations with active alerts grouped by location',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,9                 ,1                ,106              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Alerts by Route'   			   ,'Total number of pay stations with active alerts grouped by Route'   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Total Collections
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,2                 ,2                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Collections Today' 	   ,'The total collections made today'                                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,2                 ,2                ,106              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Collections by Route'    ,'The total collections made today grouped by route'                  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,2                 ,2                ,203              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Collections by Type'     ,'The total collections made today grouped by collection type'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Total Settled
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,3                 ,3                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Settled Yesterday' 	   ,'Total dollars settled yesterday'                                    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,3                 ,3                ,109              ,0                ,0                ,0                 ,1                ,0                ,0       ,1          ,'Total Settled by Merchant Acct','Total dollars settled grouped by merchant account'                  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Map
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,11                ,1                ,105              ,0                ,0                ,0                 ,8                ,0                ,0       ,1          ,'Pay Station Map'               ,'A map pay stations grouped by location'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Average Price/Permit                                                                                                                                                                                                                                               
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Price by Day'       	   ,'Average price per permit for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Price by Month'	   		   ,'Average price per permit for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,5                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Price by Location(P)'      ,'Average price per permit for the last 7 days grouped by parent location'       ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,5                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Price by Location'         ,'Average price per permit for the last 7 days grouped by location'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Avg Price by Revenue Type'     ,'Average price per permit for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,12                ,6                ,202              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Avg Price by Trans Type'      ,'Average price per permit for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Average Revenue/Space                                                                                                                                                                                                                                               
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,13                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Revenue by Day'	           ,'Average revenue per space for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,13                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Revenue by Month'	       ,'Average revenue per space for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,13                ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Revenue by Location(P)'    ,'Average revenue per space for the last 30 days grouped by parent location'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,13                ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Revenue by Location'       ,'Average revenue per space for the last 30 days grouped by location'           ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Average Duration/Permit                                                                                                                                                                                                                                               
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,14                ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Duration by Day'           ,'Average duration per permit for the last 7 days grouped by day'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,14                ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Avg Duration by Month'         ,'Average duration per permit for the last 12 months grouped by month'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,14                ,6                ,104              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Duration by Location(P)'  ,'Average duration per permit for the last 30 days grouped by parent location'  ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          		  ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,14                ,6                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Avg Duration by Location'      ,'Average duration per permit for the last 30 days grouped by location'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (WidgetListTypeId,UserAccountId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                                   ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,14                ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,1       ,2          ,'Total Average Duration 30 Days','The total average duration for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());

-- Card Processing - Purchases
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					4,					2,			    205,					0,				0,					5,					0,				0,		1,		'Hourly Purchases by Method','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Purchases by Method','Settled Card Purchases for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					6,					105,			205,					0,				0,					5,					0,				1,		1,		'Location Purchases by Method',	'Settled Card Purchases for the last 30 days by location grouped by processing method',  0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		16,					6,					104,			205,					0,				0,					5,					0,				1,		2,		'Location(P) Purchases by Method','Settled Card Purchases for the last 30 days per parent location by processing method ',  0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Card Processing - Revenue
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					4,					2,			    205,						0,				0,					5,					0,				0,		1,		'Hourly Revenue by Method','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Revenue by Method','Settled Card Revenue for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					6,					105,			205,					0,				0,					5,					0,				1,		1,		'Location Revenue by Method',	'Settled Card Revenue for the last 30 days per location by processing method',	0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 1,					6,		15,					6,					104,			205,					0,				0,					5,					0,				1,		2,		'Location(P) Revenue by Method','Settled Card Revenue for the last 30 days per parent location by processing method',	0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Citation By Citation Type
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,17                ,4                ,2                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Hour'              ,'Citations per citation type for last 24 hours grouped by hour'      ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,17                ,5                ,3                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Day'               ,'Citations per citation type for last 7 days grouped by day'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,17                ,7                ,5                ,0                ,0                ,0                 ,6                ,0                ,0       ,1          ,'Citation by Month'             ,'Citations per citation type for last 12 months grouped by month'    ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,17                ,5                ,110              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Citations by Citation Type'    ,'Citations for last 7 days grouped by citation type    '             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         


-- Citation Map
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,18                ,4                ,110              ,0                ,0                ,0                 ,9                ,0                ,0       ,1          ,'Citation Map'                  ,'Recently issued citations displayed on a map    '                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Counted Occupancy
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,19                ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted Occupancy by Hour'	   ,'Counted occupancy for the last 24 hours grouped by hour'            ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,19                ,1                ,105              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Counted Occupancy by Location' ,'Counted occupancy grouped by location'                              ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Compare Occupancy
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,20                ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted/Paid Occ. by Hour'	   ,'Counted/paid occupancy for the last 24 hours grouped by hour'       ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,20                ,1                ,105              ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Counted/Paid Occ. by Location' ,'Counted/paid occupancy for now grouped by location'                 ,0               ,0         ,8500       ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Occupancy Map
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,1               ,1        ,21                ,1                ,105              ,0                ,0                ,0                 ,10               ,0                ,0       ,1          ,'Occupancy Map'				   ,'Occupancy map by Location'                                          ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                          

-- Parent Widget Master List
-- Total Revenue                                                                                                                                                                                                                                               
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,0                ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue Last 30 Days'    ,'The total revenue for the last 30 days'                             ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Hour'	       ,'Total revenue for the last 24 hours grouped by hour'                ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Day'	       ,'Total revenue for the last 7 days grouped by day'                   ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Revenue by Month'	       ,'Total revenue for the last 12 months grouped by month'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
-- INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,4               ,1        ,1                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Revenue by Parent Org'   ,'Total revenue for this month grouped by parent organization'        ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Organization' ,'Total revenue for the last 30 days grouped by organization'         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,202              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Revenue by Trans Type'   ,'Total revenue for the last 30 days grouped by transaction type'     ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Revenue by Revenue Type' ,'Total revenue for the last 30 days grouped by revenue type'         ,0               ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,2                 ,1                ,1                ,0       ,1          ,'Top 5 - 30 Days'               ,'The top 5 organizations based on total revenue for the last 30 days',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,1                 ,6                ,103              ,0                ,0                ,3                 ,1                ,1                ,0       ,1          ,'Bottom 5 - 30 Days'		       ,'The bottom 5 organizations based on total revenue for the last 30 days',0            ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Total Purchases
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,4                 ,4                ,2                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Hour'	   ,'Total purchases for the last 24 hours grouped by hour'              ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,4                 ,5                ,3                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Day'	       ,'Total purchases for the last 7 days grouped by day'                 ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,4                 ,7                ,5                ,0                ,0                ,0                 ,3                ,0                ,0       ,1          ,'Total Purchases by Month'	   ,'Total purchases for the last 12 months grouped by month'            ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
-- INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
-- VALUES        (1            ,4               ,1        ,4                 ,9                ,102              ,0                ,0                ,0                 ,7                ,0                ,0       ,1          ,'Total Purchases by Parent Org' ,'Total number of purchases this month grouped by parent organization',0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,4                 ,6                ,103              ,0                ,0                ,0                 ,5                ,0                ,0       ,1          ,'Total Purchases by Org'	       ,'Total number of purchases for the last 30 days grouped by organization',0            ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,4                 ,6                ,201              ,0                ,0                ,1                 ,5                ,0                ,0       ,1          ,'Total Purchases by Rev Type'   ,'Total number of purchases for the last 30 days grouped by revenue type',0            ,0         ,0          ,1            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         

-- Map
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name                            ,Description                                                          ,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES        (1            ,4               ,1        ,11                ,1                ,103              ,0                ,0                ,0                 ,8                ,0                ,0       ,1          ,'Pay Station Map'               ,'A map pay stations grouped by organization'                         ,0               ,0         ,0          ,0            ,0            ,0            ,0                ,0                ,0      ,UTC_TIMESTAMP());                         


-- Card Processing - Revenue - parent
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		16,					4,					2,				205,					0,				0,					5,					0,				0,		1,		'Hourly Purchases by Method','Settled Card Purchases for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		16,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Purchases by Method','Settled Card Purchases for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		16,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Purchases by Method','Settled Card Purchases for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		16,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Purchases by Method','Settled Card Purchases for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		16,					6,					103,			0,						0,				0,					2,					0,				1,		1,		'Purchases by Organization',	'Settled Card Purchases for the last 30 days by Organization grouped by processing method',  0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());

-- Card Processing - Purchases - parent
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		15,					4,					2,				205,					0,				0,					5,					0,				0,		1,		'Hourly Revenue by Method','Settled Card Revenue for the last 24 hours by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		15,					6,					3,				205,					0,				0,					5,					0,				0,		2,		'Daily Revenue by Method','Settled Card Revenue for the last 30 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		15,					7,					5,				205,					0,				0,					5,					0,				0,		3,		'Monthly Revenue by Method','Settled Card Revenue for the last 12 months by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,				 Description,												    ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		15,					5,					205,			0,						0,				0,					2,					0,				0,		4,		'Revenue by Method','Settled Card Revenue for the last 7 days by processing method',0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());
INSERT Widget (UserAccountId,WidgetListTypeId,SectionId,WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,CustomerId,TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES (	1,			 4,					6,		15,					6,					103,			0,						0,				0,					2,					0,				1,		1,		'Revenue by Organization',	'Settled Card Purchases for the last 30 days by Organization grouped by processing method',  0,0,0,0,0,0,0,0,0,UTC_TIMESTAMP());



-- -----------------------------------------------------
-- Table `WidgetSubsetMember`
-- -----------------------------------------------------

-- Default List Widgets
INSERT     WidgetSubsetMember (WidgetId, WidgetTierTypeId, MemberId, MemberName,VERSION, LastModifiedGmt)
SELECT     Id, 201, 3, 'Card', 0, UTC_TIMESTAMP()
FROM       Widget
WHERE (Name = 'Total Revenue by Month' 
OR Name = 'Total Purchases by Month'
OR Name = 'Total Purchases by Rev Type' 
OR Name = 'Total Purchases by Day' 
OR Name = 'Total Purchases by Hour' 
OR Name = 'Total Revenue by Revenue Type'
OR Name = 'Avg Price by Revenue Type');

INSERT     WidgetSubsetMember (WidgetId, WidgetTierTypeId, MemberId, MemberName,VERSION, LastModifiedGmt)
SELECT     Id, 201, 2, 'Cash', 0, UTC_TIMESTAMP()
FROM       Widget
WHERE (Name = 'Total Revenue by Month' OR Name = 'Total Purchases by Month' 
OR Name = 'Total Purchases by Rev Type'
OR Name = 'Total Purchases by Day' 
OR Name = 'Total Purchases by Hour' 
OR Name = 'Total Revenue by Revenue Type'
OR Name = 'Avg Price by Revenue Type');

-- -----------------------------------------------------
-- Table `WidgetChartType`
-- -----------------------------------------------------
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'List',         3882, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Pie',          1034, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Line',         8395, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Bar',          9923, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Column',       1184, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Area',         6783, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Single Value', 3850, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'Map',          4956, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (9, 'Heat Map',     4957, UTC_TIMESTAMP(), 1) ;
INSERT WidgetChartType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (10,'Occupancy Map',4958, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WidgetFilterType`
-- -----------------------------------------------------
INSERT WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'All',        2850, UTC_TIMESTAMP(), 1) ;
INSERT WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Subset',     2395, UTC_TIMESTAMP(), 1) ;
INSERT WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Top',        5949, UTC_TIMESTAMP(), 1) ;
INSERT WidgetFilterType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Bottom',     4558, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WidgetLimitType`
-- -----------------------------------------------------
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0,  0,           2295, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1,  5,           3844, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 10,           9482, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 15,           3347, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 20,           9386, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 25,           9385, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 50,           2847, UTC_TIMESTAMP(), 1) ;
INSERT WidgetLimitType (Id, RowLimit, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 100,          9387, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WidgetListType`
-- -----------------------------------------------------
INSERT WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Master List',       6830, UTC_TIMESTAMP(), 1) ;
INSERT WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Default Dashboard', 4030, UTC_TIMESTAMP(), 1) ;
INSERT WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'User Dashboard',    4069, UTC_TIMESTAMP(), 1) ;
INSERT WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Parent Master List',       5830, UTC_TIMESTAMP(), 1) ;
INSERT WidgetListType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Default Parent Dashboard', 3030, UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Table `WidgetMetricType`
-- -----------------------------------------------------
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Revenue',        7830, 'Description Revenue',        UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Collections',    4782, 'Description Collections',    UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Settled Card',   3820, 'Description Settled Card',   UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Purchases',      3959, 'Description Purchases',      UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Paid Occupancy', 4960, 'Description Paid Occupancy', UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Turnover',       3905, 'Description Turnover',       UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Utilization',    3945, 'Description Utilization',    UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Duration',       1394, 'Description Duration',       UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Active Alerts',  5896, 'Description Active Alerts',  UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Alert Status',   9947, 'Description Alert Status',   UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Map',            4823, 'Description Map',            UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Average Price per Permit',  	 4829, 'Description Average Price',  		 UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Average Revenue per Space',	 4899, 'Description Average Revenue',		 UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Average Duration per Permit', 4875, 'Description Average Duration',		 UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Card Processing - Revenue', 	 9952, 'Description Card Processing - Revenue', UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (16, 'Card Processing - Purchases', 9953, 'Description Card Processing - Purchases',  UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (17, 'Citations', 		9321, 'Description Citations',  	1700, UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (18, 'Citation Map', 	9322, 'Description Citation Map',  	1700, UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (19, 'Counted Occupancy',      9368, 'Description Counted Occupancy', 	1800, UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, SubscriptionTypeId, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 'Counted/Paid Occupancy', 9369, 'Description Counted vs Paid Occupancy',  	1800, UTC_TIMESTAMP(), 1) ;
INSERT WidgetMetricType (Id, Name, Label, Description, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 'Occupancy Map', 4953, 'Description Occupancy Map',   UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WidgetRangeType`
-- -----------------------------------------------------
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',             4583, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Now',             2958, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Today',           5930, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Yesterday',       3344, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Last 24 Hours',   8506, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Last 7 Days',     3850, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Last 30 Days',    3951, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Last 12 Months',  9939, UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'This Week',       2945, UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'This Month',      5942, UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'This Year',       3845, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Last Week',       3854, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Last Month',      2894, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Last Year',       1892, UTC_TIMESTAMP(), 1) ;
INSERT WidgetRangeType (Id, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Year to Date',    1893, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `WidgetTierType`
-- -----------------------------------------------------
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  0, 0, 'N/A',                 8406, UTC_TIMESTAMP(), 1) ;

INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 1, 'Hour',                3850, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 1, 'Day',                 3958, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 1, 'Month',               1040, UTC_TIMESTAMP(), 1) ;

INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 2, 'All Organizations',   1967, UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 2, 'Parent Organization', 9477, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 2, 'Organization',        4758, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (104, 2, 'Parent Location',     8573, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (105, 2, 'Location',            2957, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (106, 2, 'Route',               9938, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (107, 2, 'Pay Station',         7572, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (108, 2, 'Rate',                2947, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (109, 2, 'Merchant Account',    2968, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (110, 3, 'Citation Type',       9323, UTC_TIMESTAMP(), 1) ;

INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (201, 3, 'Revenue Type',        9583, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (202, 3, 'Transaction Type',    8375, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (203, 3, 'Collection Type',     8257, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (204, 3, 'Alert Type',          8258, UTC_TIMESTAMP(), 1) ;
INSERT WidgetTierType (Id, ListNumber, Name, Label, LastModifiedGMT, LastModifiedByUserId) VALUES (205, 3, 'Card Processing Method Type',     9951, UTC_TIMESTAMP(), 1) ;

-- ---------------------- These may be used in the future --------------------------
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 1, '15 Minutes',          UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 1, 'Week',                UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (204, 3, 'Permit Type',         UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (205, 3, 'Permit Issue Type',   UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (206, 3, 'Paystation Type',     UTC_TIMESTAMP(), 1) ;
-- INSERT WidgetTierType (Id, ListNumber, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (207, 3, 'Point Of Sale Type',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `RoleStatusType`
-- -----------------------------------------------------

INSERT RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Disabled', UTC_TIMESTAMP(), 1) ;
INSERT RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Enabled',  UTC_TIMESTAMP(), 1) ;
INSERT RoleStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Deleted',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `SensorInfoType`
-- -----------------------------------------------------

INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (0, 'Battery Voltage'       ,     6,	   15, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Battery 2 Voltage'     ,     6,	   15, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Controller Temperature',   -60,	   80, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Ambient Temperature'   ,   -60,	   80, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Relative Humidity'     ,     0,	  100, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Input Current'         , -9999,	99999, UTC_TIMESTAMP(), 1) ;
INSERT SensorInfoType (Id, Name, MinValue, `MaxValue`, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'System Load'           ,     0,	99999, UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `MerchantStatusType`
-- -----------------------------------------------------

INSERT MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Disabled', UTC_TIMESTAMP(), 1) ;
INSERT MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Enabled',  UTC_TIMESTAMP(), 1) ;
INSERT MerchantStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Deleted',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CardRetryTransactionType`
-- -----------------------------------------------------

INSERT CardRetryTransactionType (Id, TypeName, Description) VALUES ( 0, 'Batched',  'Auto EMS Retry') ;
INSERT CardRetryTransactionType (Id, TypeName, Description) VALUES ( 1, 'Store & Fwd', 'Store and Forward') ;

-- -----------------------------------------------------
-- Table `LicencePlate`: Id = 1, Number = 'None'
-- -----------------------------------------------------

INSERT LicencePlate (Id,Number, LastModifiedGMT) VALUES (1,'None', UTC_TIMESTAMP()) ;

-- -----------------------------------------------------
-- Table `MobileNumber`: Id = 1, Number = 'None'
-- -----------------------------------------------------

INSERT MobileNumber (Id,Number, LastModifiedGMT) VALUES (1,'None', UTC_TIMESTAMP()) ;

-- -----------------------------------------------------
-- Table `ReportRepeatType`
-- -----------------------------------------------------

INSERT ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Online', 	UTC_TIMESTAMP(), 1);
INSERT ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Daily', 	UTC_TIMESTAMP(), 1);
INSERT ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Weekly', 	UTC_TIMESTAMP(), 1);
INSERT ReportRepeatType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Monthly', UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ReportType`
-- -----------------------------------------------------

INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (1,		'Transaction - All', 					0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (2,		'Transaction - Cash', 					0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (3,		'Transaction - Credit Card', 			0, 1, 1, UTC_TIMESTAMP(), 1); 
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (4,		'Transaction - Credit Card Refund', 	0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (5,		'Transaction - Card Processing', 		0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (6,		'Transaction - Credit Card Retry', 		0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (7,		'Transaction - Patroller Card', 		0, 0, 0, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (8,		'Transaction - Rate', 					0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (9,		'Transaction - Rate Summary', 			0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (10,	'Transaction - Smart Card', 			0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (11,	'Transaction - Passcard', 			    0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (12,	'Stall Reports', 						0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (13,	'Coupon Usage Summary', 				0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (14,	'Collection Reports', 					0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (15,	'Replenish Reports', 					0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (16,	'Pay Station Summary',					0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (17,	'Tax Report', 							0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (18,	'Transaction - Delay', 					0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (19,	'Inventory Report', 					0, 1, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (20,	'Paystation Inventory Report', 			1, 0, 0, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (21,	'Maintenance Summary',					0, 0, 1, UTC_TIMESTAMP(), 1);
INSERT ReportType (Id, Name, AllowSystemAdmin, AllowParentCustomerAdmin, AllowChildCustomerAdmin, LastModifiedGMT, LastModifiedByUserId) VALUES (22,	'Collection Summary',					0, 0, 1, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ReportStatusType`
-- -----------------------------------------------------

INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'Ready', 			UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'Scheduled', 		UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'Launched', 		UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'Completed', 		UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Cancelled', 		UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Failed', 			UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Reran', 		UTC_TIMESTAMP(), 1);
INSERT ReportStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8, 'Deleted', 		UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ReportFilterType`
-- -----------------------------------------------------

INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  'Primary Date/Time', 		UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  'Secondary Date/Time', 	UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  'Location', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4,  'Space Number', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  'Card Number', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6,  'Approval Status', 		UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7,  'Ticket Number', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8,  'Report Number', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  'Coupon Code', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'Transaction', 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11, 'Report', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12, 'Retry', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13, 'Group By', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14, 'Sort By', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15, 'Coupon Type',			UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ReportFilterValue`
-- -----------------------------------------------------

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1,  100,  1,  'Date/Time', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2,  101,  1,  'Month', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3,  102,  1,  'Today', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4,  103,  1,  'Yesterday',						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5,  104,  1,  'Last 24 Hours', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6,  105,  1,  'This Week', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7,  106,  1,  'Last Week', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (8,  107,  1,  'This Month', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (9,  108,  1,  'Last Month', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10,  109,  1,  'Last 12 Months', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (11,  110,  1,  'Last 30 Days', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (12,  111,  1,  'Last 7 Days', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (13,  112,  1,  'This Year', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (14,  113,  1,  'Last Year', 						UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (15,  300,  3,  'Location',			 			UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (16,  301,  3,  'Pay Station Route', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (17,  302,  3,  'Pay Station', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (19,  304,  3,  'Location', 						UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (20, 400,  4,  'N/A', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (21, 401,  4,  'No Stall', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (22, 402,  4,  'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (23, 403,  4,  '=', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (24, 404,  4,  '>=', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (25, 405,  4,  '<=', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (26, 406,  4,  'Between', 							UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (27, 500,  5,  'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (28, 501,  5,  'Specific #', 						UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (29, 600,  6,  'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (30, 601,  6,  'Pre-Auth', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (31, 602,  6,  'Real-Time', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (32, 603,  6,  'Batched', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (33, 604,  6,  'Offline', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (34, 605,  6,  'Recoverable', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (35, 606,  6,  'Uncloseable', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (36, 607,  6,  'Encryption Error', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (103, 608, 6,  'To be Settled',	 				UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (37, 700,  7,  'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (38, 701,  7,  'Specific #', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (39, 702,  7,  'Between', 							UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (40, 800,  8,  'All',				 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (41, 801,  8,  '>=', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (42, 802,  8,  '<=', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (43, 803,  8,  'Specific #', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (44, 804,  8,  'Between',							UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (45, 900,  9,  'N/A', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (46, 901,  9,  'All Coupons', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (47, 902,  9,  'No Coupons', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (48, 903,  9,  'Specific Code', 						UTC_TIMESTAMP(), 1); 

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (49, 1000, 10, 'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (50, 1001, 10, 'Regular', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (51, 1002, 10, 'Add Time', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (52, 1003, 10, 'Monthly',							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (53, 1004, 10, 'Citation',							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (54, 1005, 10, 'SC Recharge',						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (55, 1006, 10, 'Test', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (56, 1007, 10, 'Replenish', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (57, 1008, 10, 'Extend-by-Phone', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (58, 1009, 10, 'Non-Refund', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (59, 1010, 10, 'Refund', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (60, 1011, 10, 'Deposit',							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (61, 1012, 10, 'Regular', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (62, 1013, 10, 'Cancelled', 						UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (63, 1100, 11, 'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (64, 1101, 11, 'Valid', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (65, 1102, 11, 'Expired', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (66, 1103, 11, 'Collection Report - Coin',		 	UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (67, 1104, 11, 'Collection Report - Bill',			UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (68, 1105, 11, 'Collection Report - Credit Card', 	UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (69, 1106, 11, 'Collection Report - All', 			UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (70, 1200, 12, 'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (71, 1201, 12, 'Batched', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (72, 1202, 12, 'Store Forward', 					UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (73, 1300, 13, 'None', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (74, 1301, 13, 'Day', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (75, 1302, 13, 'Month', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (77, 1304, 13, 'Location', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (78, 1305, 13, 'Pay Station', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (79, 1306, 13, 'Trans Type', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (80, 1307, 13, 'Payment Type', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (81, 1308, 13, 'Card Type', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (82, 1309, 13, 'Card Number', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (83, 1310, 13, 'Day of Week', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (84, 1311, 13, 'Machine', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (85, 1312, 13, 'Tax Name', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (86, 1313, 13, 'Pay Station Route',				UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (87, 1314, 13, 'Organization',						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (88, 1315, 13, 'Last Seen Time',					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (89, 1316, 13, 'Hour',								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (90, 1317, 13, 'Merchant Account',					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (102, 1318, 13, 'Account',							UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (91, 1400, 14, 'Pay Station Name', 				UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (92, 1401, 14, 'Voltage', 							UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (93, 1402, 14, 'Coin Count', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (94, 1403, 14, 'Bill Count', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (95, 1404, 14, 'Paper Status', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (96, 1405, 14, 'Running Total', 					UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (97, 1406, 14, 'Unsettled Credit Card Count', 		UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (98, 1407, 14, 'Unsettled Credit Card Amount',		UTC_TIMESTAMP(), 1);

INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 1500, 15, 'All', 								UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (100, 1501, 15, 'Percentage', 						UTC_TIMESTAMP(), 1);
INSERT ReportFilterValue (Id, ReportFilterValue, ReportFilterTypeId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (101, 1502, 15, 'Dollar Based',					UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Table `ReportCollectionFilterType`
-- -----------------------------------------------------

INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (1,0,0,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (2,0,0,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (3,0,0,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (4,0,0,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (5,0,0,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (6,0,0,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (7,0,0,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (8,0,0,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (9,0,0,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (10,0,0,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (11,0,0,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (12,0,0,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (13,0,0,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (14,0,0,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (15,0,0,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (16,0,0,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (17,0,0,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (18,0,0,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (19,0,0,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (20,0,0,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (21,0,0,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (22,0,0,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (23,0,0,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (24,0,0,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (25,0,0,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (26,0,0,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (27,0,0,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (28,0,0,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (29,0,0,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (30,0,0,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (31,0,0,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (32,0,0,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (33,0,0,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (34,0,0,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (35,0,0,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (36,0,0,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (37,0,0,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (38,0,0,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (39,0,0,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (40,0,0,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (41,0,0,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (42,0,0,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (43,0,0,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (44,0,0,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (45,0,0,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (46,0,0,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (47,0,0,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (48,0,0,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (49,0,0,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (50,0,0,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (51,0,0,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (52,0,0,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (53,0,0,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (54,0,0,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (55,0,0,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (56,0,0,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (57,0,0,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (58,0,0,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (59,0,0,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (60,0,0,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (61,0,0,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (62,0,0,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (63,0,0,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (64,0,1,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (65,0,1,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (66,0,1,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (67,0,1,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (68,0,1,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (69,0,1,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (70,0,1,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (71,0,1,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (72,0,1,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (73,0,1,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (74,0,1,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (75,0,1,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (76,0,1,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (77,0,1,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (78,0,1,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (79,0,1,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (80,0,1,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (81,0,1,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (82,0,1,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (83,0,1,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (84,0,1,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (85,0,1,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (86,0,1,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (87,0,1,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (88,0,1,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (89,0,1,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (90,0,1,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (91,0,1,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (92,0,1,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (93,0,1,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (94,0,1,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (95,0,1,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (96,0,1,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (97,0,1,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (98,0,1,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (99,0,1,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (100,0,1,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (101,0,1,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (102,0,1,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (103,0,1,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (104,0,1,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (105,0,1,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (106,0,1,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (107,0,1,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (108,0,1,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (109,0,1,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (110,0,1,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (111,0,1,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (112,0,1,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (113,0,1,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (114,0,1,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (115,0,1,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (116,0,1,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (117,0,1,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (118,0,1,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (119,0,1,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (120,0,1,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (121,0,1,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (122,0,1,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (123,0,1,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (124,0,1,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (125,0,1,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (126,0,1,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (127,0,1,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (128,1,0,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (129,1,0,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (130,1,0,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (131,1,0,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (132,1,0,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (133,1,0,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (134,1,0,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (135,1,0,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (136,1,0,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (137,1,0,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (138,1,0,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (139,1,0,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (140,1,0,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (141,1,0,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (142,1,0,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (143,1,0,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (144,1,0,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (145,1,0,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (146,1,0,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (147,1,0,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (148,1,0,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (149,1,0,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (150,1,0,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (151,1,0,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (152,1,0,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (153,1,0,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (154,1,0,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (155,1,0,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (156,1,0,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (157,1,0,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (158,1,0,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (159,1,0,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (160,1,0,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (161,1,0,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (162,1,0,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (163,1,0,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (164,1,0,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (165,1,0,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (166,1,0,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (167,1,0,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (168,1,0,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (169,1,0,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (170,1,0,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (171,1,0,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (172,1,0,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (173,1,0,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (174,1,0,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (175,1,0,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (176,1,0,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (177,1,0,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (178,1,0,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (179,1,0,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (180,1,0,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (181,1,0,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (182,1,0,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (183,1,0,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (184,1,0,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (185,1,0,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (186,1,0,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (187,1,0,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (188,1,0,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (189,1,0,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (190,1,0,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (191,1,0,1,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (192,1,1,0,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (193,1,1,0,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (194,1,1,0,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (195,1,1,0,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (196,1,1,0,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (197,1,1,0,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (198,1,1,0,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (199,1,1,0,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (200,1,1,0,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (201,1,1,0,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (202,1,1,0,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (203,1,1,0,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (204,1,1,0,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (205,1,1,0,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (206,1,1,0,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (207,1,1,0,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (208,1,1,0,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (209,1,1,0,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (210,1,1,0,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (211,1,1,0,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (212,1,1,0,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (213,1,1,0,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (214,1,1,0,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (215,1,1,0,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (216,1,1,0,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (217,1,1,0,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (218,1,1,0,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (219,1,1,0,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (220,1,1,0,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (221,1,1,0,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (222,1,1,0,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (223,1,1,0,1,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (224,1,1,1,0,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (225,1,1,1,0,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (226,1,1,1,0,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (227,1,1,1,0,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (228,1,1,1,0,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (229,1,1,1,0,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (230,1,1,1,0,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (231,1,1,1,0,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (232,1,1,1,0,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (233,1,1,1,0,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (234,1,1,1,0,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (235,1,1,1,0,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (236,1,1,1,0,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (237,1,1,1,0,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (238,1,1,1,0,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (239,1,1,1,0,1,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (240,1,1,1,1,0,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (241,1,1,1,1,0,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (242,1,1,1,1,0,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (243,1,1,1,1,0,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (244,1,1,1,1,0,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (245,1,1,1,1,0,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (246,1,1,1,1,0,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (247,1,1,1,1,0,1,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (248,1,1,1,1,1,0,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (249,1,1,1,1,1,0,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (250,1,1,1,1,1,0,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (251,1,1,1,1,1,0,1,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (252,1,1,1,1,1,1,0,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (253,1,1,1,1,1,1,0,1);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (254,1,1,1,1,1,1,1,0);
INSERT INTO ReportCollectionFilterType (`Id`, `IsForOverdue`, `IsForCardAmount`, `IsForCardCount`, `IsForBillAmount`, `IsForBillCount`, `IsForCoinAmount`, `IsForCoinCount`, `IsForRunningTotal`) VALUES (255,1,1,1,1,1,1,1,1);

-- -----------------------------------------------------
-- Table `ReplenishType`
-- -----------------------------------------------------

INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown'          , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'ChangerAdd'       , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'HopperAdd'        , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'ChangerTest'      , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'HopperTest'       , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'HopperRemoval'    , UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'ChangerPreAutoSet', UTC_TIMESTAMP(), 1) ;
INSERT ReplenishType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'ChangerAutoSet'   , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `LicencePlate`: Id = 1, Number = 'None'
-- -----------------------------------------------------
INSERT LicencePlate (Number, LastModifiedGMT) VALUES ('AFB114', UTC_TIMESTAMP());
INSERT LicencePlate (Number, LastModifiedGMT) VALUES ('787POP', UTC_TIMESTAMP());

-- -----------------------------------------------------
-- Table `Tax`
-- -----------------------------------------------------
-- INSERT INTO `Tax` (Id, CustomerId, Name, LastModifiedGMT) VALUES (1,5,'GST','2012-01-01 00:00:00');
-- INSERT INTO `Tax` (Id, CustomerId, Name, LastModifiedGMT) VALUES (2,5,'PST','2012-01-01 00:00:00');
-- INSERT INTO `Tax` (Id, CustomerId, Name, LastModifiedGMT) VALUES (3,5,'HST','2012-01-01 00:00:00');

-- -----------------------------------------------------
-- Table `PurchaseTax`
-- -----------------------------------------------------
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (1,1,1,500,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (2,1,2,724,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (3,2,1,500,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (4,2,2,724,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (5,3,1,500,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (6,3,2,724,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (7,4,1,500,100);
-- INSERT INTO `PurchaseTax` (Id, PurchaseId, TaxId, TaxRate, TaxAmount) VALUES (8,4,2,724,100);

-- -----------------------------------------------------
-- Table `Purchase`
-- -----------------------------------------------------
-- INSERT INTO `Purchase` (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:13:24',3,1,5,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:15:53');
-- INSERT INTO `Purchase` (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:15:34',3,17,9,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:32:33');
-- INSERT INTO `Purchase` (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:17:24',3,1,5,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:15:53');
-- INSERT INTO `Purchase` (CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber, TransactionTypeId, PaymentTypeId, LocationId, PaystationSettingId, UnifiedRateId, CouponId, OriginalAmount, ChargedAmount, ChangeDispensedAmount, ExcessPaymentAmount, CashPaidAmount, CoinPaidAmount, BillPaidAmount, CardPaidAmount, RateAmount, RateRevenueAmount, CoinCount, BillCount, IsOffline, IsRefundSlip, CreatedGMT) VALUES (5,14,'2013-01-04 07:19:34',3,17,9,14,15,12,NULL,2000,2000,0,2000,2000,0,0,2000,2000,0,0,0,0,0,'2013-01-04 15:32:33');

-- -----------------------------------------------------
-- Table `MobileNumber`
-- -----------------------------------------------------
INSERT INTO `MobileNumber` (Id, Number, LastModifiedGMT) VALUES (2,'17783899692','2013-01-03 23:39:50');
INSERT INTO `MobileNumber` (Id, Number, LastModifiedGMT) VALUES (3,'16043439333','2013-01-03 23:49:51');

-- -----------------------------------------------------
-- Table `MobileLicenseStatusType`
-- -----------------------------------------------------
INSERT INTO `MobileLicenseStatusType` (Id, Name) VALUES (1, 'AVAILABLE');
INSERT INTO `MobileLicenseStatusType` (Id, Name) VALUES (2, 'PROVISIONED');

-- -----------------------------------------------------
-- Table `MobileApplicationType`
-- -----------------------------------------------------
INSERT INTO `MobileApplicationType` (Id, Name, SubscriptionTypeId) VALUES (1, 'Digital Collect', 1300);

-- -----------------------------------------------------
-- Table `MobileAppActivityType`
-- -----------------------------------------------------
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (1, 'PROVISION');
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (2, 'LOGIN');
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (3, 'LOGOUT');
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (4, 'BLOCK');
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (5, 'UNBLOCK');
INSERT INTO `MobileAppActivityType` (Id, Name) VALUES (6, 'RELEASE');


-- -----------------------------------------------------
-- Table `SmsAlert`
-- -----------------------------------------------------
-- INSERT INTO `SmsAlert` (Id, MobileNumberId, CustomerId, PermitLocationId, PointOfSaleId, PermitBeginGMT, PermitExpireGMT, TicketNumber, AddTimeNumber, PaystationSettingName, LicencePlate, SpaceNumber, NumberOfRetries, IsAutoExtended, IsAlerted, IsLocked) VALUES (1,2,5,14,14,'2013-01-04 07:13:34','2013-01-04 07:43:34',3,0,'Northeast','AFB114',0,0,0,0,0);
-- INSERT INTO `SmsAlert` (Id, MobileNumberId, CustomerId, PermitLocationId, PointOfSaleId, PermitBeginGMT, PermitExpireGMT, TicketNumber, AddTimeNumber, PaystationSettingName, LicencePlate, SpaceNumber, NumberOfRetries, IsAutoExtended, IsAlerted, IsLocked) VALUES (2,3,5,14,14,'2013-01-04 07:15:35','2013-01-04 07:45:35',3,0,'Northeast','787POP',0,0,0,0,0);

-- -----------------------------------------------------
-- Table `ExtensiblePermit`
-- -----------------------------------------------------
-- INSERT INTO `ExtensiblePermit` (Id, OriginalPermitId, MobileNumberId, CardData, Last4Digit, PermitBeginGMT, PermitExpireGMT, IsRFID) VALUES (1,1,2,'',0,'2013-01-04 07:13:34','2013-01-04 07:43:34',0);
-- INSERT INTO `ExtensiblePermit` (Id, OriginalPermitId, MobileNumberId, CardData, Last4Digit, PermitBeginGMT, PermitExpireGMT, IsRFID) VALUES (2,2,3,'',0,'2013-01-04 07:15:35','2013-01-04 07:45:35',0);

-- -----------------------------------------------------
-- Table `Permit`
-- -----------------------------------------------------
-- INSERT INTO `Permit` (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (1,1,3,14,3,2,NULL,1,1,0,0,'2013-01-04 07:13:37','2013-01-04 07:43:37','2013-01-04 07:43:37',0);
-- INSERT INTO `Permit` (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (2,2,3,14,3,2,NULL,2,2,0,0,'2013-01-04 07:15:34','2013-01-04 07:45:37','2013-01-04 07:45:37',0);
-- INSERT INTO `Permit` (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (3,3,3,14,3,2,NULL,NULL,1,0,0,'2013-01-04 07:17:34','2013-01-04 07:47:37','2013-01-04 07:47:37',0);
-- INSERT INTO `Permit` (Id, PurchaseId, PermitNumber, LocationId, PermitTypeId, PermitIssueTypeId, OriginalPermitId, MobileNumberId, LicencePlateId, SpaceNumber, AddTimeNumber, PermitBeginGMT, PermitOriginalExpireGMT, PermitExpireGMT, NumberOfExtensions) VALUES (4,4,3,14,3,2,NULL,NULL,3,0,0,'2013-01-04 07:19:34','2013-01-04 07:49:37','2013-01-04 07:49:37',0);

-- -----------------------------------------------------
-- Table `Cluster`
-- -----------------------------------------------------

INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP01', 'vanapp01', 8080, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP02', 'vanapp02', 8080, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP03', 'vanapp01', 8081, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP04', 'vanapp02', 8081, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP05', 'vanapp01', 8085, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP06', 'vanapp02', 8086, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP07', 'vanapp03', 8087, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP08', 'vanapp03', 8088, 0, UTC_TIMESTAMP(), 1);
INSERT Cluster (Name, Hostname, LocalPort, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES ('VANAPP09', 'vanapp03', 8089, 0, UTC_TIMESTAMP(), 1);

-- -----------------------------------------------------
-- Table `ETLStatusType`
-- -----------------------------------------------------

INSERT ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Ready'          , UTC_TIMESTAMP(), 1) ;
INSERT ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Timezone Error' , UTC_TIMESTAMP(), 1) ;
INSERT ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Error'          , UTC_TIMESTAMP(), 1) ;
INSERT ETLStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Done'           , UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerMigrationStatusType`
-- -----------------------------------------------------

INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Requested', 	      			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Scheduled',       			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Suspended',             		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Cancelled',       			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Queued',               	  	UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'EMS6 Disabled',               UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 6, 'Waiting Data Migration',      UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 7, 'Data Migration Confirmed',   	UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 8, 'Completed',             		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 9, 'Communication Checked',       UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (10, 'All Pay Stations Migrated', 	UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (99, 'Not Boarded', 				UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (90, 'Boarded', 					UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerMigrationStatusType`
-- -----------------------------------------------------

INSERT CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'N/A',	       			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Blocked',       			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Warned',             		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationStatusType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Passed',       				UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerMigrationFailureType`
-- -----------------------------------------------------

INSERT CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 0, 'Unknown',       				UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Unprocessed Reversal', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Unprocessed Card',    		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationFailureType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Pre-migration Validations',  UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `CustomerMigrationValidationType`
-- -----------------------------------------------------

INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  1, 'cm.label.EMS6DBConnection',        'cm.warning.EMS6DBConnection', 			'cm.URL.EMS6DBConnection',        	1, 0, 1, 0, 1, 'checkEMS6Connection', 			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  2, 'cm.label.EMS6SlaveDBConnection',	'cm.warning.EMS6SlaveDBConnection', 	'cm.URL.EMS6SlaveDBConnection', 	1, 0, 1, 0, 1, 'checkEMS6SlaveConnection', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  3, 'cm.label.ETLUpToDate', 			'cm.warning.ETLUpToDate', 				'cm.URL.ETLUpToDate',       		1, 0, 1, 1, 1, 'checkMigrationQueueStatus', 	UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  4, 'cm.label.CardRetryRunning', 		'cm.warning.CardRetryRunning', 			'cm.URL.CardRetryRunning',      	1, 0, 1, 1, 0, 'checkCardRetry', 				UTC_TIMESTAMP(), 1) ;

INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  5, 'cm.label.PayStationValid', 		'cm.warning.PayStationValid', 			'cm.URL.PayStationValid',		    1, 1, 0, 0, 0, 'checkPayStationVersion', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  6, 'cm.label.RestAccountCheck', 		'cm.warning.RestAccountCheck', 			'cm.URL.RestAccountCheck',	    	0, 1, 0, 0, 0, 'checkRestAccounts', 			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  7, 'cm.label.PayStationCheck', 		'cm.warning.PayStationCheck', 			'cm.URL.PayStationCheck',		    1, 0, 0, 0, 0, 'checkPayStationCount', 			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  8, 'cm.label.CouponCountCheck', 		'cm.warning.CouponCountCheck', 			'cm.URL.CouponCountCheck',		    0, 0, 0, 0, 0, 'checkCouponCount', 				UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES (  9, 'cm.label.UnusedMerchantAccount', 	'cm.warning.UnusedMerchantAccount', 	'cm.URL.UnusedMerchantAccount',	    1, 0, 0, 0, 0, 'checkUnusedMerchantAccounts', 	UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 10, 'cm.label.LocationWithDash', 		'cm.warning.LocationWithDash', 			'cm.URL.LocationWithDash',		    0, 0, 0, 0, 0, 'checkLocationsWithDash', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 11, 'cm.label.LongRateNameCheck', 		'cm.warning.LongRateNameCheck', 		'cm.URL.LongRateNameCheck',		    1, 0, 0, 0, 0, 'checkLongRateNames', 			UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 12, 'cm.label.MerchantAccountCheck', 	'cm.warning.MerchantAccountCheck', 		'cm.URL.MerchantAccountCheck',	    1, 0, 0, 1, 0, 'checkMerchantAccounts', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 13, 'cm.label.ReversalCheck', 			'cm.warning.ReversalCheck', 			'cm.URL.ReversalCheck',		   	    1, 0, 0, 1, 0, 'checkReversals', 				UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 14, 'cm.label.CardTransactionCheck',	'cm.warning.CardTransactionCheck', 		'cm.URL.CardTransactionCheck',	    1, 0, 0, 1, 0, 'checkCardTransactions', 		UTC_TIMESTAMP(), 1) ;
INSERT CustomerMigrationValidationType (Id, Name, Warning, Url, IsBlocking, IsUserFacing, IsSystemWide, IsDuringMigration, IsITEmail, MethodToCall, LastModifiedGMT, LastModifiedByUserId) VALUES ( 15, 'cm.label.EnforcementModeCheck',	'cm.warning.EnforcementModeCheck', 		'cm.URL.EnforcementModeCheck',	    0, 1, 0, 0, 0, 'checkEnforcementMode', 			UTC_TIMESTAMP(), 1) ;
                                 
-- -----------------------------------------------------
-- Table `ETLLocationDetailType`
-- -----------------------------------------------------

INSERT ETLLocationDetailType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Hourly',       				UTC_TIMESTAMP(), 1) ;
INSERT ETLLocationDetailType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Daily',				 		UTC_TIMESTAMP(), 1) ;
INSERT ETLLocationDetailType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Monthly', 					UTC_TIMESTAMP(), 1) ;

-- -----------------------------------------------------
-- Table `HashAlgorithmType`
-- -----------------------------------------------------

INSERT HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'SHA-1',      		0, 'Sha1',	 3, 	0, UTC_TIMESTAMP(), 1) ;
INSERT HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'SHA-256',			0, 'Sha256', 4, 	0, UTC_TIMESTAMP(), 1) ;
INSERT HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'SHA1withRSA',		0, '', 		 0,		1, UTC_TIMESTAMP(), 1) ;
INSERT HashAlgorithmType (Id, Name, IsDeleted, ClassName, SignatureId, IsForSigning, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'SHA256withRSA',	0, '', 		 0,		1, UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Table RateType
-- -----------------------------------------------------

-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 0, 'Flat Rate', 	UTC_TIMESTAMP(), 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Daily', 	UTC_TIMESTAMP(), 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Hourly', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Incremental', 	UTC_TIMESTAMP(), 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 4, 'Monthly', 	UTC_TIMESTAMP(), 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 5, 'Parking Restriction', 	UTC_TIMESTAMP(), 1); 
-- INSERT INTO RateType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 6, 'Holiday', 	UTC_TIMESTAMP(), 1); 

-- -----------------------------------------------------
-- Table RateFlatType
-- -----------------------------------------------------

INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 0, 'Valid For', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Expires At', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateFlatType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Scheduled', 	UTC_TIMESTAMP(), 1); 

-- -----------------------------------------------------
-- Table RateHourlyRuleType
-- -----------------------------------------------------

INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Up To', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Next', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 3, 'Per', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateHourlyRuleType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 4, 'From', 	UTC_TIMESTAMP(), 1); 


-- -----------------------------------------------------
-- Table RateExpiryDayType
-- -----------------------------------------------------

INSERT INTO RateExpiryDayType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 1, 'Same Day', 	UTC_TIMESTAMP(), 1); 
INSERT INTO RateExpiryDayType (Id,Name, LastModifiedGMT , LastModifiedByUserId) VALUES ( 2, 'Next Day', 	UTC_TIMESTAMP(), 1); 


-- ---------------------------------------------------------
-- Table mutex
-- ------------------------------------
				
insert into mutex(i) values (1);

-- -----------------------------------------------------
-- Table `CardProcessMethodType`
-- -----------------------------------------------------
INSERT CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 1, 'Real-Time',  		UTC_TIMESTAMP(), 1) ;
INSERT CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 2, 'Store and Forward',  UTC_TIMESTAMP(), 1) ;
INSERT CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 3, 'Batched',  			UTC_TIMESTAMP(), 1) ;
INSERT CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 4, 'Unclosable',  			UTC_TIMESTAMP(), 1) ;
INSERT CardProcessMethodType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES ( 5, 'Recoverable',  			UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Table `CardProcessTransactionType`
-- -----------------------------------------------------

INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (1,1,2);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (2,1,17);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (3,1,21);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (4,2,11);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (5,3,3);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (6,3,10);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (7,4,8);
INSERT INTO CardProcessTransactionType (Id, CardProcessMethodTypeId,ProcessorTransactionTypeId) VALUES (8,5,20);


-- -----------------------------------------------------
-- Table `ETLProcessType`
-- -----------------------------------------------------

INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'HourlyETL', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'UtilizationDailyToMonthlyRollUp', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'OccupancyHourlyToDailyRollup', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'PopulateDailyLocationCapacityLookup', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'RemoveETLPastData', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'RemovePastActivePermitData', UTC_TIMESTAMP(), 1);
INSERT INTO ETLProcessType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'ClearActiveAlerts', UTC_TIMESTAMP(), 1);


-- -----------------------------------------------------
-- Table `FlexETLDataType` 
-- -----------------------------------------------------

INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 'CitationWidgetData', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (2, 'CitationMapData', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 'PermitData', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (4, 'CitationType', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (5, 'Facility', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (6, 'Property', UTC_TIMESTAMP(), 1) ;
INSERT INTO FlexETLDataType (Id, Name, LastModifiedGMT, LastModifiedByUserId) VALUES (7, 'Citation', UTC_TIMESTAMP(), 1) ;


-- -----------------------------------------------------
-- Table `TransactionSettlementStatusType` 
-- -----------------------------------------------------
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(0,'Pre-Authorised','PreAuth Approved',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(1,'To be Settled','Post Auth received',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(2,'Settled','Transaction sucessfully settled',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(3,'Failed to Settle','Transaction failed to settle',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(4,'Cannot Settle','Transaction cannot be settled',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(5,'Expired','Transaction Expired',UTC_TIMESTAMP(),1);
INSERT INTO TransactionSettlementStatusType (`Id`,`Name`,`Description`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(6,'Excluded','Transaction Reversed',UTC_TIMESTAMP(),1);
-- Add more status in this Lookup table

-- -----------------------------------------------------
-- Table `BatchStatusType` 
-- -----------------------------------------------------
INSERT INTO BatchStatusType (`Id`,`Name`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(1,'Open',UTC_TIMESTAMP(),1);
INSERT INTO BatchStatusType (`Id`,`Name`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(2,'Close',UTC_TIMESTAMP(),1);
INSERT INTO BatchStatusType (`Id`,`Name`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(3,'Partially Closed',UTC_TIMESTAMP(),1);
INSERT INTO BatchStatusType (`Id`,`Name`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(4,'Partially RB',UTC_TIMESTAMP(),1);
INSERT INTO BatchStatusType (`Id`,`Name`,`LastModifiedGMT`,`LastModifiedByUserId`)VALUES(5,'Failed',UTC_TIMESTAMP(),1);


-- -----------------------------------------------------
-- Table `ElavonRequestType` 
-- -----------------------------------------------------
INSERT INTO ElavonRequestType (`Id`, `Code`, `CaptureTranCode`, `RequestName`, `Description`, `LastModifiedGMT`,`LastModifiedByUserId`)VALUES(1, '000', 5, 'Credit Card.Sale', '000 = Credit Card Sale', UTC_TIMESTAMP(),1);
INSERT INTO ElavonRequestType (`Id`, `Code`, `CaptureTranCode`, `RequestName`, `Description`, `LastModifiedGMT`,`LastModifiedByUserId`)VALUES(2, '098', 6, 'Credit Card.Void', '098 = Credit Card Void', UTC_TIMESTAMP(),1);
INSERT INTO ElavonRequestType (`Id`, `Code`, `CaptureTranCode`, `RequestName`, `Description`, `LastModifiedGMT`,`LastModifiedByUserId`)VALUES(3, '099', 0, 'Credit Card.Reversal', '099 = Credit Card Reversal', UTC_TIMESTAMP(),1);
INSERT INTO ElavonRequestType (`Id`, `Code`, `CaptureTranCode`, `RequestName`, `Description`, `LastModifiedGMT`,`LastModifiedByUserId`)VALUES(4, '005', 6, 'Credit Card.Return', '005 = Credit Card Return', UTC_TIMESTAMP(),1);

INSERT INTO BatchSettlementType(Id, Description ) VALUES (1 , 'NoOfTransactions');
INSERT INTO BatchSettlementType(Id, Description ) VALUES (2 , 'TimeOfDay'); 
INSERT INTO BatchSettlementType(Id, Description ) VALUES (3 , 'Refund');


-- -----------------------------------------------------
-- Table `TelemetryType` 
-- -----------------------------------------------------
INSERT INTO `TelemetryType` (`Id`, `Name`, `Label`, `IsPrivate`, `CreatedGMT`) VALUES(1	, 'SerialNumber'		, 'label.telemetry.serialnumber'	, 0, UTC_TIMESTAMP);
INSERT INTO `TelemetryType` (`Id`, `Name`, `Label`, `IsPrivate`, `CreatedGMT`) VALUES(2	, 'FirmwareVersion'	, 'label.telemetry.firmwareversion'	, 1, UTC_TIMESTAMP);
INSERT INTO `TelemetryType` (`Id`, `Name`, `Label`, `IsPrivate`, `CreatedGMT`) VALUES(3	, 'ViolationStatus'				, 'label.telemetry.violationstatus'			, 0, UTC_TIMESTAMP);


-- -----------------------------------------------------
-- Table `GatewayProcessor` 
-- -----------------------------------------------------
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(1, 8, 'NOVA', 'NOVA', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(2, 8, 'Paymentech Tampa', 'Paymentech Tampa', UTC_TIMESTAMP()); 
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(3, 8, 'First Data Merchant Services Nashille', 'First Data Merchant Services Nashille', UTC_TIMESTAMP()); 
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(4, 8, 'First Data Merchant Services North', 'First Data Merchant Services North', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(5, 8, 'First Data Merchant Services South', 'First Data Merchant Services South', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(6, 8, 'Global East', 'Global East', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(7, 8, 'Moneris', 'Moneris', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(8, 8, 'Vital', 'Vital', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(9, 8, 'BCE Emergis', 'BCE Emergis', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(10, 11, 'Chase Paymentech Solutions', 'Chase Paymentech Solutions', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(11, 11, 'First Data Merchant Services (FDMS)', 'First Data Merchant Services (FDMS)', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(12, 11, 'Global Payments', 'Global Payments', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(13, 11, 'Nova Information Systems', 'Nova Information Systems', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(14, 11, 'Pay By Touch Payment Solutions', 'Pay By Touch Payment Solutions', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(15, 11, 'RBS Lynk', 'RBS Lynk', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(16, 11, 'TSYS Acquiring Solutions', 'TSYS Acquiring Solutions', UTC_TIMESTAMP());



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;