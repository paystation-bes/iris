

-- --------------------------------------------------------------------------------------------------------
-- Procedure:   sp_Impark
--
-- Purpose:     Extracts from an EMS 6 production database transaction counts for each individual 
--              pay station for every `Impark` customer.
--
--              We can view `Impark` as a parent company. The parent company has many child
--              companies, and each child company has many pay stations.
--
--              The transaction counts are bounded by the incoming date range parameters:
--              1.  p_BeginGMT
--              2.  p_EndGMT
--
--              The SELECT query that finds `Transaction` records searches from `p_BeginGMT` 
--              until just PRIOR to `p_EndGMT`.
--
-- Call:        To count all transactions for each Impark pay station for each Impark
--              customer for the month of January 2013 issue the following call:
--
--              CALL sp_Impark ('2013-01-01','2013-02-01');
--
-- Note:        This procedure performs date logic based on GMT time, not local time.
-- ------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_Impark;

delimiter //
CREATE PROCEDURE sp_Impark (IN p_BeginGMT DATETIME, IN p_EndGMT DATETIME)
BEGIN

    SELECT  C.Name                              AS Customer_Name, 
            P.CommAddress                       AS Paystation_SerialNumber,
            P.Name                              AS Paystation_Name,
            H.LastHeartBeatGMT                  AS Paystation_LastSeen,
            R.Name                              AS Region_Name,
            IF (L.Name IS NULL,'n/a',L.Name)    AS LotSetting_Name,
            COUNT(*)                            AS Transaction_Count
            
            INTO OUTFILE '/tmp/Impark.csv' 
            FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
            LINES TERMINATED BY '\n'  
        
    FROM    Customer C
            INNER JOIN Paystation P             ON C.Id = P.CustomerId
            INNER JOIN PaystationHeartBeat H    ON P.Id = H.PaystationId
            INNER JOIN Region R                 ON P.RegionId = R.Id 
            INNER JOIN Transaction T            ON P.Id = T.PaystationId 
            LEFT OUTER JOIN LotSetting L        ON P.LotSettingId = L.Id    -- it is possible that a pay station does not have a lot setting

    WHERE   C.Name LIKE '%Impark%'
    AND     T.PurchasedDate >= p_BeginGMT 
    AND     T.PurchasedDate <  p_EndGMT 

    GROUP BY C.Id, P.Id
    ORDER BY C.Name, P.CommAddress;

END//
delimiter ;
