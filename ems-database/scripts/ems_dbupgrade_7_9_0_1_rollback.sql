-- ROLLBACK IRIS-3852 Add Arya pay station to IrisIRIS-3890
UPDATE PaystationType SET `Name`='FRODO' WHERE Id=6; 

-- ROLLBACK IRIS-3939 Pay station modem details telemetry / IRIS-3954
ALTER TABLE `ModemSetting` DROP COLUMN `HardwareFirmware`, DROP COLUMN `HardwareModel`, DROP COLUMN `HardwareManufacturer`;