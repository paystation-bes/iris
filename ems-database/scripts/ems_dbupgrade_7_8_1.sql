-- -----------------------------------------------------
-- Table `EMVRefund`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EMVRefund`;

CREATE TABLE `EMVRefund` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `SerialNumber` VARCHAR(20) NOT NULL ,
  `ProcessedDate` datetime NOT NULL ,
  `CardTransaction` varchar(800) NOT NULL ,
  `CardEaseReference` varchar(40) NOT NULL ,
  `Amount` mediumint(8) unsigned NOT NULL,
  `ChargeToken` VARCHAR(100) ,
  `TerminalToken` VARCHAR(100) NOT NULL,
  `CardType` varchar(20) NOT NULL default 'CREDITCARD',
  `ReferenceNumber` varchar(20) NOT NULL ,
  `Last4DigitsOfCardNumber` smallint(5) unsigned NOT NULL default '0',
  `ProcessorTransactionId` VARCHAR(50) NOT NULL,
  `CardAuthorizationId` varchar(30) NOT NULL default ' ' ,
  `NumRetries` smallint(5) unsigned NOT NULL default '0',
  `LastResponse` varchar(500) ,
  `CreationDate` datetime NOT NULL,
  `LastRetryDate` datetime NOT NULL,  

  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;


-- EMV Refund retry max count 
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EMVRefundRetryMaxCount', '10', UTC_TIMESTAMP(), 1);

-- EMV Refund retry select limit
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('EMVRefundRetrySelectLimit', '100', UTC_TIMESTAMP(), 1);
 