-- Script Last Modified on March 04 2013
-- --------------------------------------------------------------------------------
-- Procedure:   sp_InsertLocationDayAndOpen
--
-- Purpose:     Inserts rows into tables LocationDay and LocationOpen with data
--              for a single Location.
--              
--              LocationDay:
--              o  The hours a Location is open
--              o  There is one record for each Location for each day of the week
--              o  A Location can have different operating hours for each day of the week 
--              o  See table DayOfWeek
--            
--              LocationOpen:
--              o  Translates the operating hours for a Location for a day into distinct quarter hour segments within the day when the Location is open
--              o  This table is used by widget queries for Occupancy, Turnover and Utilization that need to know when a Location is open within any quarter hour segment of the day
--              
-- Useful SQL:  SELECT * FROM LocationDay WHERE LocationId = X;
--              SELECT * FROM LocationOpen WHERE LocationId = X;
--              
--              SELECT  L.Id, L.Name, D.Id, D.Name, MIN(QuarterOfDay) AS MinQuarter, MAX(QuarterOfDay) AS MaxQuarter
--              FROM    Location L, PPPInfo P, Time T, DayOfWeek D
--              WHERE   L.Id = P.PurchaseLocationId
--              AND     P.TimeIdLocal = T.Id
--              AND     T.DayOfWeek = D.Id
--              GROUP BY L.Id, D.Id
--              ORDER BY L.Id, D.Id;
--
--              SHOW PROCEDURE STATUS WHERE Db = 'ems_db';
--              SHOW TRIGGERS FROM ems_db;
-- --------------------------------------------------------------------------------   
DROP PROCEDURE IF EXISTS sp_InsertLocationDayAndOpen;

delimiter //
CREATE PROCEDURE sp_InsertLocationDayAndOpen (IN p_LocationId MEDIUMINT UNSIGNED, IN p_DayOfWeekId TINYINT UNSIGNED, IN p_OpenQuarterHourId TINYINT UNSIGNED, IN p_CloseQuarterHourId TINYINT UNSIGNED, IN p_IsOpen24Hours TINYINT UNSIGNED, IN p_IsClosed TINYINT UNSIGNED, IN p_UserId MEDIUMINT UNSIGNED)
BEGIN
    -- Version: 1 - Wednesday December 12 2012 12:12 pm - Paul W

    -- INSERT LocationDay
    INSERT  LocationDay (LocationId, DayOfWeekId, OpenQuarterHourNumber, CloseQuarterHourNumber, IsOpen24Hours, IsClosed, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES  (p_LocationId, p_DayOfWeekId, p_OpenQuarterHourId, p_CloseQuarterHourId, p_IsOpen24Hours, p_IsClosed, 1, UTC_TIMESTAMP(), p_UserId);
    
    -- INSERT LocationOpen 
    -- If p_IsClosed = 1 then the Location is not open for that day 
    IF p_IsClosed = 1 THEN
    
        -- The Location is not open on this day
        INSERT  LocationOpen (LocationId, DayOfWeekId, QuarterHourId, IsOpen, LastModifiedGMT, LastModifiedByUserId)
        SELECT  p_LocationId, p_DayOfWeekId, Id, 0, UTC_TIMESTAMP(), p_UserId
        FROM    QuarterHour;
    
    -- The Location is open for atleast some portion of the day
    ELSEIF IFNULL(p_OpenQuarterHourId,-1) >= 0 AND IFNULL(p_CloseQuarterHourId,-1) >= 0 AND p_CloseQuarterHourId >= p_OpenQuarterHourId THEN
    
        -- The Location is not open before the Open time
        INSERT  LocationOpen (LocationId, DayOfWeekId, QuarterHourId, IsOpen, LastModifiedGMT, LastModifiedByUserId)
        SELECT  p_LocationId, p_DayOfWeekId, Id, 0, UTC_TIMESTAMP(), p_UserId
        FROM    QuarterHour
        WHERE   Id < p_OpenQuarterHourId;
    
        -- The Location is open between the Open and Close times (inclusive)
        INSERT  LocationOpen (LocationId, DayOfWeekId, QuarterHourId, IsOpen, LastModifiedGMT, LastModifiedByUserId)
       SELECT  p_LocationId, p_DayOfWeekId, Id, 1, UTC_TIMESTAMP(), p_UserId
        FROM    QuarterHour
        WHERE   Id >= p_OpenQuarterHourId
        AND     Id <= p_CloseQuarterHourId;
       
        -- The Location is not open after the Close time
        INSERT  LocationOpen (LocationId, DayOfWeekId, QuarterHourId, IsOpen, LastModifiedGMT, LastModifiedByUserId)
        SELECT  p_LocationId, p_DayOfWeekId, Id, 0, UTC_TIMESTAMP(), p_UserId
        FROM    QuarterHour
        WHERE   Id > p_CloseQuarterHourId;
        
    -- Otherwise, the incoming Open and Close parameters are bad
    ELSE 
        SELECT 'Error: Invalid values for parameters p_OpenQuarterHourId and p_CloseQuarterHourId.';
    END IF;
    
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_InsertCustomer
--
-- Purpose:     Populates the customer-data-object within the database for a new Customer.
--              The customer-data-object spans many tables related to a Customer.
--
-- Parameters:                         (CustomerTypeId, CustomerStatusTypeId, ParentCustomerId,    CustomerName, TrialExpiryGMT, IsParent,  UserName,                                   Password, IsStandardReports, IsAlerts, IsRealTimeCC, IsBatchCC, IsRealTimeValue, IsCoupons, IsValueCards, IsSmartCards, IsExtendByPhone, IsDigitalAPIRead, IsDigitalAPIWrite, Is3rdPartyPayByCell,     Timezone, UserId,      p_PasswordSalt)
-- Call:        CALL sp_InsertCustomer (             3,                    1,             NULL, 'Acme Parking 8',          NULL,        0, 'dadadada', '58ae83e00383273590f96b385ddd700802c3f07d',                 1,        1,            1,         1,               1,         1,            1,            1,               1,                1,                 1,                   1, 'US/Pacific',      1, 'SaltSaltSaltSalt');
--
-- Returns:     Returns via SELECT the 'Id' of the newly created 'Customer' record.
--              If any type of database error was encountered returns 0 (zero) via SELECT.
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_InsertCustomer;

delimiter //
CREATE PROCEDURE sp_InsertCustomer (IN p_CustomerTypeId TINYINT UNSIGNED, IN p_CustomerStatusTypeId TINYINT UNSIGNED, IN p_ParentCustomerId MEDIUMINT UNSIGNED, IN p_CustomerName VARCHAR(25), IN p_TrialExpiryGMT DATETIME, IN p_IsParent TINYINT(1) UNSIGNED, IN p_UserName VARCHAR(765), IN p_Password VARCHAR(128), IN p_IsStandardReports TINYINT(1), IN p_IsAlerts TINYINT(1), IN p_IsRealTimeCC TINYINT(1), IN p_IsBatchCC TINYINT(1), IN p_IsRealTimeValue TINYINT(1), IN p_IsCoupons TINYINT(1), IN p_IsValueCards TINYINT(1), IN p_IsSmartCards TINYINT(1), IN p_IsExtendByPhone TINYINT(1), IN p_IsDigitalAPIRead TINYINT(1), IN p_IsDigitalAPIWrite TINYINT(1), IN p_Is3rdPartyPayByCell TINYINT(1), IN p_IsDigitalCollect TINYINT(1), IN p_IsOnlineConfiguration TINYINT(1), IN p_IsFlexIntegration TINYINT(1) , IN p_IsCaseIntegration TINYINT(1) , IN p_IsDigitalAPIXChange TINYINT(1), IN p_Timezone VARCHAR(40), IN p_UserId MEDIUMINT UNSIGNED, IN p_PasswordSalt VARCHAR(16))
BEGIN
    -- Version: 1 - Tuesday February 19 2013  
    
    -- Local variables
    DECLARE v_CustomerId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserRoleId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    -- If the time zone specified in parameter `p_Timezone` does not exist in view `Timezone_v.Name` then a default time zone of Id = 473 (Name = 'GMT') will be assigned to the Customer 
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      -- prepare default timezone with Timezone_v.Id = 473 = 'GMT'
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';

    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     -- exit on SQL warning
    
    -- Verify the time zone specified by incoming parameter `p_Timezone` is valid (validate against the view `Timezone_v`), otherwise use a default value for Customer.TimezoneId (assigned in DECLARE statement above) 
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
    
    -- Start the atomic transaction
    START TRANSACTION;
    
    -- Insert Customer
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP());
    
    -- Get Customer.Id
    SELECT LAST_INSERT_ID() INTO v_CustomerId;
    
    -- Insert UserAccount: each new customer gets a user account with a first name of 'Administrator' (and no last name, i.e., an empty string for last name), immediately below this user will be assigned an administrator role
	
	-- Added on July 23 by Ashok (to take care of autoincrement ID)
	-- Added to force UserAccount.Id = 1 if there are no recrods in UserAccount Table. This is used as FK is many tables.
	
	IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
		INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (1,	 v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);
        SET v_UserAccountId = 1;
    ELSE
		INSERT UserAccount (CustomerId,   UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,  p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        -- Get UserAccount.Id
        SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    END IF ;

    
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    
    -- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

        -- Insert UserDefaultDashboard (every user has access to the default dashboard)
    -- WARNING: TABLE 'UserDefaultDashboard' ONLY NEEDS TO BE IN THE 'KPI DB' DATABASE BUT IT IS BEING REFERENCED HERE IN THE 'EMS DB'
    --          WHEN ALL TABLES ARE FINALLY SEGREGATED BETWEEN THE 'KPI DB' AND THE 'EMS DB' DATABASES INSERTING INTO TABLE 'UserDefaultDashboard' WILL BE PERFORMED BY A TRIGGER ON THE `UserAccount` TABLE WITHIN THE 'KPI DB'
    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);

    -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    	INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES          (v_UserAccountId, 				 3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    	-- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    	INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES              (v_CustomerId, 				  3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
    	SELECT 			  v_CustomerId,	UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
    	FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
    	
    	INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
    	FROM UserRole ur 
    	INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

    	INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM CustomerRole cr
		INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
		WHERE r.RoleStatusTypeId != 2
		AND r.CustomerTypeId = 3
		AND cr.CustomerId = p_ParentCustomerId;

		INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
		SELECT 							      ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM UserAccount ua 
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

END IF;
    
  
    -- A 'Child' customer type receives a full set of inserts (but 'Parent' and 'DPT' customer types do not)
    IF p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerCardType
        -- These are `locked` customer card types for credit cards, smart cards, and value cards
        -- These `CustomerCardType` records are used as foreign key parents for tables `CustomerCard` (lists of good/valid credit cards and smart cards) and `CustomerBadCard` (lists of bad/banned credit cards, smart cards, and value cards)
        -- Important: valid value cards (in table CustomerCard) have their own non-locked CustomerCardType record, but bad value cards used the locked 'generic' CustomerCardType record created here
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
      
        -- Insert CustomerProperty: every 'Child' customer has 7 customer properties
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);   -- Timezone
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Query Spaces By (1=Location)
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max User Accounts
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);   -- Credit Card Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);   -- SMS Warning Period
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Hidden Paystations Reported
   
        -- Insert Location: every customer has a location named 'Unassigned'
        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                    		0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
    
        -- Get Location.Id
        SELECT LAST_INSERT_ID() INTO v_LocationId;
        
        -- Insert LocationDay and LocationOpen: the 'Unassigned' location is open 24 x 7 
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
    
        -- Insert PaystationSetting: every customer has a default paystation setting named 'None' (because some pay-stations/point-of-sales do not have a paystation setting)
        -- There can only be one 'None' PaystationSetting for a Customer, therefore this SQL check
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
    
        -- Insert UnifiedRate: every customer has a default unified rate named 'Unknown' (because older versions of the 'PS App' do not send to EMS the name of the rate used when transacting a purchase)
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
    
    -- 'DPT' and 'Parent' Customers receive a small set of customer properties    
    ELSE 
        -- Insert CustomerProperty: time zone only
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   -- Timezone  
    END IF;
    
    -- 'Parent' and 'Child' customers subscribe to services (but a 'DPT' Customer does not)  
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerSubscription: set all EMS services for the customer, insert the record even if the service is not subscribed to 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId, LicenseCount, LicenseUsed) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,  0, UTC_TIMESTAMP(), p_UserId, 0, 0);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration, 0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration, 0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration, 0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange, 0, UTC_TIMESTAMP(), p_UserId);
        
		
        -- Insert CustomerAlertType: every customer gets a customer-alert-type of `Pay Station Alert`, BUT it will only be referenced as a foreign key by tables `ActivePOSAlert` and `POSAlert` IF
        --                           the customer has subscribed to `Alerts` as a subscription type (see table `CustomerSubscription`, and table.attribute `SubscriptionType.Id` = 200 = `Alerts`)
        -- This only applies to a child customer, because parent and DPT customers do not have pay stations
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'No Communication In 24 Hours',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
-- No default alert for Overdue collection
--            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
--            VALUES                   (v_CustomerId,                   13,       NULL,    NULL,       'No Collection in 7 Days',         7, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,          		  12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;
        
    END IF;
    
    -- Commit the atomic transaction;
    COMMIT;
    
    -- Return the Id of the newly created Customer record
    SELECT v_CustomerId AS CustomerId;
    
END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger:     tr_ActivityLogin_insert
--
-- Purpose:     On INSERT of a new `ActivityLogin` record the insert is cascaded into table `ActivityLog`. 
--              Table `ActivityLog` is queried by the UI (not table `ActivityLogin`).
--
--              This trigger saves the UI from having to query two tables (`ActivityLogin` and `ActivityLog`)
--              when presenting user activity.
--              
-- Note:        Technically speaking this trigger makes table `ActivityLogin` redundant. 
--              But implementing this trigger was easier than rewriting UI code. 
-- 
-- Testing:     INSERT ActivityLogin (UserAccountId, LoginResultTypeId, ActivityGMT) VALUES (3, 1, DATE_ADD(UTC_TIMESTAMP(),INTERVAL 1 SECOND));
--              INSERT ActivityLogin (UserAccountId, LoginResultTypeId, ActivityGMT) VALUES (3, 2, DATE_ADD(UTC_TIMESTAMP(),INTERVAL 2 SECOND));
--              INSERT ActivityLogin (UserAccountId, LoginResultTypeId, ActivityGMT) VALUES (3, 3, DATE_ADD(UTC_TIMESTAMP(),INTERVAL 3 SECOND));
--              INSERT ActivityLogin (UserAccountId, LoginResultTypeId, ActivityGMT) VALUES (3, 4, DATE_ADD(UTC_TIMESTAMP(),INTERVAL 4 SECOND));
--              INSERT ActivityLogin (UserAccountId, LoginResultTypeId, ActivityGMT) VALUES (3, 5, DATE_ADD(UTC_TIMESTAMP(),INTERVAL 5 SECOND));
--
--              SELECT * FROM ActivityLogin WHERE UserAccountId = 3 ORDER BY Id;
--              SELECT * FROM ActivityLog   WHERE UserAccountId = 3 ORDER BY Id;
-- ------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS tr_ActivityLogin_insert;

delimiter //
CREATE TRIGGER tr_ActivityLogin_insert AFTER INSERT ON ActivityLogin
FOR EACH ROW BEGIN
    -- Version: 1 - Wednesday December 12 2012 12:12 pm - Paul W

    -- Login
    IF NEW.LoginResultTypeId = 1 THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 101           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
    
    -- Logout
    ELSEIF NEW.LoginResultTypeId = 4 OR NEW.LoginResultTypeId = 5 THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 102           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
    
    -- Login failure
    ELSEIF NEW.LoginResultTypeId = 2 THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 103           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
    
    -- Locked Out
    ELSEIF NEW.LoginResultTypeId = 3  THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 104           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
		
    -- Switched In 
    ELSEIF NEW.LoginResultTypeId = 6  THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 105           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
		
    -- Switched Out
    ELSEIF NEW.LoginResultTypeId = 7  THEN
    
        INSERT ActivityLog (    UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId,     ActivityGMT)
        VALUES             (NEW.UserAccountId, 106           , 11          , NEW.Id  , NEW.Id     , NEW.ActivityGMT);
		
	END IF;
   
END //
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_InsertPointOfSale
--
-- Purpose:     Populates the point-of-sale-data-object within the database for a new Point Of Sale.
--              The point-of-sale-data-object spans many tables related to a Point Of Sale.
--
-- Parameters:                             p_CustomerId, p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT, p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
-- Call:        CALL sp_InsertPointOfSale (3           , 5                 , 1            , '555555555555', UTC_TIMESTAMP() , 1       , 0                 , 0                  );
--
-- Note:        IF parameter p_PaystationId_old = 0 THEN inserting a new point-of-sale
--              IF parameter p_PaystationId_old > 0 THEN a point-of-sale is being moved from one customer to another. (In this scenario this procedure is being called by procedure sp_MovePointOfSale.)
--
-- Returns:     Returns via SELECT the 'Id' of the newly created 'PointOfSale' record.
--              If any type of database error was encountered returns 0 (zero) via SELECT.
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_InsertPointOfSale;

delimiter //
CREATE PROCEDURE sp_InsertPointOfSale (IN p_CustomerId MEDIUMINT UNSIGNED, IN p_PaystationTypeId TINYINT UNSIGNED, IN p_ModemTypeId TINYINT UNSIGNED, IN p_SerialNumber VARCHAR(20), IN p_ProvisionedGMT DATETIME, IN p_UserId MEDIUMINT UNSIGNED, IN p_PaystationId_old MEDIUMINT UNSIGNED, IN p_PointOfSaleId_old MEDIUMINT UNSIGNED)                                       
BEGIN
    -- Version: 1 - Tuesday February 19 2013 - added check to ensure the customer does not already have a point-of-sale with this serial number (specified by incoming parameter p_SerialNumber)
    
    -- Local variables
    DECLARE v_PaystationId MEDIUMINT UNSIGNED DEFAULT NULL;
    DECLARE v_PointOfSaleId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_CurrentGMT DATETIME;
    
    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;     -- exit on SQL warning
    
    -- Perform an important valdiation: the customer CANNOT already be the CURRENT owner of this pay station (prevents the creation of 2 point-of-sales with the same serial number for the same customer at the same time)
    IF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0) AND (
        SELECT  COUNT(*)
        FROM    PointOfSale
        WHERE   CustomerId = p_CustomerId
        AND     SerialNumber = p_SerialNumber
        AND     Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber)
        ) = 1
    THEN
    
        -- This customer already has a point-of-sale with this serial number
        SET v_PointOfSaleId = 0;
    
    -- Validate incoming parameters `p_PaystationId_old` and `p_PointOfSaleId_old`
    ELSEIF (p_PaystationId_old = 0 AND p_PointOfSaleId_old = 0)   -- new point-of-sale (INSERT both `Paystation` and `PointOfSale`)
       OR ((SELECT COUNT(*) FROM Paystation WHERE Id = p_PaystationId_old) = 1 AND (SELECT COUNT(*) FROM PointOfSale WHERE Id = p_PointOfSaleId_old) = 1)   -- move point-of-sale (moving a point-of-sale from one customer to another (INSERT `PointOfSale` record only))
    THEN 
    
        -- Set v_CurrentGMT
        SET v_CurrentGMT = UTC_TIMESTAMP();
    
        -- Start the atomic transaction
        START TRANSACTION;
    
        -- When creating a new point-of-sale (a `PointOfSale` record) it either has: (1) it's own pay station, or (2) it uses an existing pay station that is being moved from one customer to another
        -- Only insert a new `Paystation` record when NOT moving an existing pay station from one customer to another (about 7% of pay stations end up being moved from one customer to another).
        IF p_PaystationId_old = 0 THEN 
            INSERT Paystation (  PaystationTypeId,   ModemTypeId,   SerialNumber, IsDeleted, VERSION, LastModifiedGMT , LastModifiedByUserId)
            VALUES            (p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, 0        , 0      , p_ProvisionedGMT, p_UserId            );
            -- Get PaystationId of newly created pay station record
            SELECT LAST_INSERT_ID() INTO v_PaystationId;
        ELSE
            -- When moving a pay station from one customer to another move use the Id of the existing pay station being moved
            SELECT p_PaystationId_old INTO v_PaystationId;
        END IF;
    
        -- INSERT PointOfSale
        -- The new PointOfSale gets assigned to the 'Unassigned' location, and is NOT assigned to any Settings File 
        INSERT PointOfSale (  CustomerId,   PaystationId, SettingsFileId, LocationId, Name          ,   SerialNumber,   ProvisionedGMT, Latitude, Longitude, AltitudeOrLevel, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT              p_CustomerId, v_PaystationId, NULL          , L.Id      , p_SerialNumber, p_SerialNumber, p_ProvisionedGMT, NULL    , NULL     , NULL           , 0      , v_CurrentGMT   , p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;
    
        -- Get PointOfSaleId
        SELECT LAST_INSERT_ID() INTO v_PointOfSaleId;
    
        -- INSERT LocationPOSLog: logs the history of point-of-sales assigned to a location (this is a carry over from EMS 6, a future release of EMS 7 will implement proper logging, ) 
        INSERT  LocationPOSLog (LocationId, PointOfSaleId, AssignedGMT, LastModifiedByUserId)
        SELECT  L.Id, v_PointOfSaleId, p_ProvisionedGMT, p_UserId
        FROM    Location L
        WHERE   L.CustomerId = p_CustomerId
        AND     L.Name = 'Unassigned'
        AND     L.IsUnassigned = 1;
		
		-- EMS 5520 Added LastCollectionGMT on May 21 2014
        -- INSERT POSBalance: every new `PointOfSale` record has a `POSBalance` record (1:1 relationship)
        INSERT POSBalance (  PointOfSaleId, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionGMT, LastRecalcGMT   , NextRecalcGMT   , LastCollectionTypeId)
        VALUES            (v_PointOfSaleId, p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT     , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT, 0                   );
    
        -- INSERT POSHeartbeat: every new `PointOfSale` record has a `POSHeartbeat` record (1:1 relationship)   
        INSERT POSHeartbeat (PointOfSaleId, LastHeartbeatGMT) VALUES (v_PointOfSaleId, p_ProvisionedGMT);
    
        -- INSERT POSSensorState: every new `PointOfSale` record has a `POSSensorState` record (1:1 relationship)
        INSERT POSSensorState (  PointOfSaleId, AmbientTemperature, Battery1Voltage, Battery2Voltage, ControllerTemperature, InputCurrent, RelativeHumidity, SystemLoad, WirelessSignalStrength, VERSION, LastModifiedGMT)
        VALUES                (v_PointOfSaleId, 32767             , 32767          , 32767          , 32767                , 32767       , 32767           , 32767     , NULL, 0      , v_CurrentGMT   );                                                    

        -- INSERT POSServiceState: every new `PointOfSale` record has a `POSServiceState` record (1:1 relationship)
        INSERT POSServiceState (  PointOfSaleId, BBSerialNumber, LastPaystationSettingUploadGMT, NextSettingsFileId, PrimaryVersion, SecondaryVersion, UpgradeGMT, VERSION, LastModifiedGMT)
        VALUES                 (v_PointOfSaleId, NULL          , NULL                          , NULL              , NULL          , NULL            , NULL      , 0      , v_CurrentGMT   );                                                    

        --  INSERT POSDate: apply default settings for the new point of sale, insert 10 rows into table `POSDate` (table `POSDate` is a historical logging table, while table `POSStatus` maintains the current state)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  1, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  2, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  3, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  4, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  5, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  6, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);  -- default is 1 (On)
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  7, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  8, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId,  9, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);
        INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId, 10, p_ProvisionedGMT, 0, 0, v_CurrentGMT, p_UserId);

        --  INSERT POSStatus: apply default settings for the new point of sale, insert 1 row into table `POSStatus` (this row is a mirror of the rows inserted into table `POSDate` above)
        INSERT POSStatus (  PointOfSaleId, IsProvisioned, IsLocked, IsDecommissioned, IsDeleted, IsVisible, IsBillableMonthlyOnActivation, IsDigitalConnect, IsTestActivated, IsActivated, IsBillableMonthlyForEms, IsProvisionedGMT, IsLockedGMT     , IsDecommissionedGMT, IsDeletedGMT    , IsVisibleGMT    , IsBillableMonthlyOnActivationGMT, IsDigitalConnectGMT, IsTestActivatedGMT, IsActivatedGMT  , IsBillableMonthlyForEmsGMT, VERSION, LastModifiedGMT, LastModifiedByUserId) 
        VALUES           (v_PointOfSaleId, 1            , 0       , 0               , 0        , 1        , 1                            , 0               , 0              , 0          , 0                      , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT   , p_ProvisionedGMT, p_ProvisionedGMT, p_ProvisionedGMT                , p_ProvisionedGMT   , p_ProvisionedGMT  , p_ProvisionedGMT, p_ProvisionedGMT          , 0      , v_CurrentGMT   , p_UserId            );
 
        --  INSERT POSAlertStatus: apply default settings for the new point of sale, insert 1 row into table POSStatus but do not insert rows into POSDate
        INSERT POSAlertStatus (  PointOfSaleId, CommunicationMinor, CommunicationMajor, CommunicationCritical, CollectionMinor, CollectionMajor, CollectionCritical, PayStationMinor, PayStationMajor, PayStationCritical, VERSION) 
        VALUES                (v_PointOfSaleId, 0                 , 0                 ,0                     , 0              , 0              , 0                 , 0              , 0              , 0                 , 0      );
 

        --  INSERT POSEVentCurrent: only applies to user defined customer alert type's without routes (because the point of sale is new and it has not yet been assigned to a route) 
        INSERT  POSEventCurrent (CustomerAlertTypeId,       EventTypeId,   PointOfSaleId,      EventSeverityTypeId, IsActive, AlertGMT, ClearedGMT, VERSION) 
        SELECT                   Id                 , 46 AS EventTypeId, v_PointOfSaleId, 2 AS EventSeverityTypeId, 0       , NULL    , NULL      , 0        
        FROM    CustomerAlertType 
        WHERE   CustomerId = p_CustomerId 
        AND     AlertThresholdTypeId != 12       -- user defined alert types only (not pay station alerts)
        AND     IsActive = 1             -- only for active alerts
        AND     IsDeleted = 0            -- only for undeleted alerts
        AND     RouteId IS NULL         -- this point-of-sale is new and has not yet been assigned to a route
        AND     LocationId IS NULL      -- specifying a Location for a CustomerAlertType is not yet supported in EMS 7 (this is a carry over from EMS 6 and the feature was not supported in EMS 6 either)
        ORDER BY Id;

        UPDATE POSEventCurrent
        SET IsHidden = 1
        WHERE Id IN (SELECT A.Id FROM (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId 
                                       FROM POSEventCurrent pec
									   INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
									   WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 1 AND pec.PointOfSaleId = v_PointOfSaleId) AS A
						   INNER JOIN (SELECT pec.PointOfSaleId, cat.AlertThresholdTypeId FROM POSEventCurrent pec
									   INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id)
									   WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 0) B ON (A.PointOfSaleId = B.PointOfSaleId AND A.AlertThresholdTypeId = B.AlertThresholdTypeId)
					GROUP BY A.Id);
					
        --  IF moving a pay station from customer to customer THEN deactivate the old (preexisting) point-of-sale
        IF p_PointOfSaleId_old > 0 THEN 
            -- The new point-of-sale is now provisioned by moving a paystation to it from another customer, simply add an informational record saying the point-of-sale was provisioned because of a move from another customer 
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_PointOfSaleId    , 201, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            
            -- The old point-of-sale is now decommissioned (the underlying pay station was moved to a new customer), add an informational record saying the point-of-sale was decommissioned because of a move from another customer
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old,   3, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            INSERT POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (p_PointOfSaleId_old, 202, p_ProvisionedGMT, 1, 0, v_CurrentGMT, p_UserId);
            UPDATE POSStatus SET IsDecommissioned = 1, IsDecommissionedGMT = p_ProvisionedGMT, LastModifiedGMT = v_CurrentGMT, LastModifiedByUserId = p_UserId WHERE PointOfSaleId = p_PointOfSaleId_old;
        END IF;

        -- Commit the atomic transaction;
        COMMIT;
    END IF;
    
    -- Return the Id of the newly created PointOfSale record
    -- Will return 0 (zero) if the incoming parameters did not pass validation
    SELECT v_PointOfSaleId AS PointOfSaleId;
    
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_MovePointOfSale
--
-- Purpose:     Populates the point-of-sale-data-object within the database when moving a pay station from one customer to another.
--
--
-- Parameters:                             p_CustomerId_new, p_CustomerId_old, p_SerialNumber, p_ProvisionedGMT, p_UserId)
-- Call:        CALL sp_MovePointOfSale   (5               , 3               , '555555555555', UTC_TIMESTAMP() , 1       );
--
-- Returns:     Returns via SELECT the 'Id' of the newly created 'PointOfsale' record.
--              If any type of database error was encountered returns 0 (zero) via SELECT.
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_MovePointOfSale;

delimiter //
CREATE PROCEDURE sp_MovePointOfSale (IN p_CustomerId_new MEDIUMINT UNSIGNED, IN p_CustomerId_old MEDIUMINT UNSIGNED, IN p_SerialNumber VARCHAR(20), IN p_ProvisionedGMT DATETIME, IN p_UserId MEDIUMINT UNSIGNED)                                       
BEGIN
    -- Version: 2 - Tuesday March 26 2013  - Added check #4
    
    -- Local variables
    DECLARE v_PointOfSaleId MEDIUMINT UNSIGNED;
    DECLARE v_PaystationId MEDIUMINT UNSIGNED;
    DECLARE v_ModemTypeId TINYINT UNSIGNED;
    DECLARE v_PaystationTypeId TINYINT UNSIGNED;
    
    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS PointOfSaleId; END;     -- exit on SQL warning
    
    -- The point-of-sale to be moved must pass a series of checks:
    -- 1. the existing point-of-sale must exist for the specified customer
    -- 2. the existing point-of-sale must be deactivated
    -- 3. the underlying `Paystation` record for the `PointOfSale` record must have the same `SerialNumber` as the point-of-sale being moved:
    --    this is just a precaution: `SerialNumber` for the `PointOfSale` record and its related `Paystation` record should always match (Note: `PointOfSale.SerialNumber` is DENORMALIZED from `Paystation.SerialNumber`)  
    -- 4. The point of sale being moved must be the latest (last) instance of it's serial number (because the same [pay station] serial number can be used across multiple point of sale's as time progresses)  
    IF (SELECT  COUNT(*) 
        FROM    Customer C, PointOfSale P, POSStatus S, Paystation A
        WHERE   C.Id = P.CustomerId
        AND     P.Id = S.PointOfSaleId 
        AND     P.PaystationId = A.Id
        AND     P.SerialNumber = A.SerialNumber 
        AND     P.SerialNumber = p_SerialNumber
        AND     C.Id = p_CustomerId_old
        AND     S.IsActivated = 0
        AND     P.Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber)
        ) = 1 
    THEN
        
        -- The old point-of-sale has been identified and is deactivated, get and save the Paystation.Id from the underlying pay station
        SELECT  P.Id, A.Id, A.ModemTypeId, A.PaystationTypeId INTO v_PointOfSaleId, v_PaystationId, v_ModemTypeId, v_PaystationTypeId
        FROM    Customer C, PointOfSale P, POSStatus S, Paystation A
        WHERE   C.Id = P.CustomerId
        AND     P.Id = S.PointOfSaleId 
        AND     P.PaystationId = A.Id
        AND     P.SerialNumber = A.SerialNumber 
        AND     P.SerialNumber = p_SerialNumber
        AND     C.Id = p_CustomerId_old
        AND     S.IsActivated = 0
        AND     P.Id = (SELECT MAX(Id) FROM PointOfSale WHERE SerialNumber = p_SerialNumber);
        
        -- Attempt to create a new `PointOfSale` record in the database
        -- Parameters:            (p_CustomerId    , p_PaystationTypeId, p_ModemTypeId, p_SerialNumber, p_ProvisionedGMT, p_UserId, p_PaystationId_old, p_PointOfSaleId_old)
        CALL sp_InsertPointOfSale (p_CustomerId_new, v_PaystationTypeId, v_ModemTypeId, p_SerialNumber, p_ProvisionedGMT, p_UserId, v_PaystationId    , v_PointOfSaleId    );                                     
        
    ELSE  
        -- No point-of-sale created
        SELECT 0 AS PointOfSaleId;  
        
    END IF;
END//
delimiter ;

-- --------------------------------------------------------------------------------  

-- BatteryInfo Procedure



-- --------------------------------------------------------------------------------
-- Routine DDL
-- --------------------------------------------------------------------------------
-- Used in INC
DROP PROCEDURE IF EXISTS `sp_BatteryInfo` ;

delimiter //




CREATE PROCEDURE `sp_BatteryInfo`(
IN p_Value FLOAT, IN p_PointOfSaleId INT, IN p_SensorTypeId INT, IN p_SensorDateGMT DATETIME)

-- EMS 7 lv_battery_voltage Info Stored Procedure
-- Last Modified by Ashok on Feb 20 2014 EMS 5073
--  Modified by Ashok on Mar 01 2013


BEGIN
    DECLARE lv_sLoad FLOAT DEFAULT 0;
    DECLARE lv_iCurrent FLOAT DEFAULT 0;
    DECLARE lv_battery_voltage FLOAT DEFAULT 0;
    DECLARE result INT DEFAULT 0;
	
    -- INSERT INTO SensorLog(PaystationId, TypeId, DateField, `Value`) VALUES(p_PayStationId, p_SensorTypeId, p_SensorDateGMT, p_Value);
	
	-- Get max(ID) ? or ID during that point of time from Table PointOfSale which corresponds to
	-- GET THE SAME LOGIC USED IN MIGRATION SCRIPTS
	/*SELECT
		PointOfSale_Id
		INTO p_PointOfSaleId
	FROM
		PointOfSale_v
	WHERE
		PointOfSale_Id = p_PointOfSaleId;
	*/
	
    IF (p_SensorTypeId = 0 OR p_SensorTypeId = 5 OR p_SensorTypeId = 6) THEN
        -- SELECT COUNT(PaystationId) INTO result FROM BatteryInfo WHERE PaystationId = p_PayStationId AND DateField = p_SensorDateGMT;
		SELECT 
			COUNT(PointOfSaleId) 
			INTO result 
		FROM 
			POSBatteryInfo 
		WHERE 
			PointOfSaleId = p_PointOfSaleId AND SensorGMT = p_SensorDateGMT; -- ?? both Date and Time are passed
			
        CASE p_SensorTypeId
            WHEN 0 THEN
               SET lv_battery_voltage = p_Value;
            WHEN 5 THEN
               SET lv_iCurrent = p_Value;
            WHEN 6 THEN
               SET lv_sLoad = p_Value;
        END CASE;
        IF (result <> 1) THEN
		
            --	INSERT INTO BatteryInfo(SystemLoad, InputCurrent, lv_battery_voltage, PaystationId, DateField) VALUES(lv_sLoad, lv_iCurrent, lv_battery_voltage, p_PayStationId, p_SensorDateGMT);
			-- 	INSERT INTO SAME TABLE ????
			INSERT INTO POSBatteryInfo(PointOfSaleId, 		SensorGMT, SystemLoad, InputCurrent, BatteryVoltage) VALUES
									  (p_PointOfSaleId,	p_SensorDateGMT, 	   lv_sLoad,	   lv_iCurrent,	 lv_battery_voltage);
        
        ELSEIF p_SensorTypeId = 6 THEN
			
			   --	THIS IS A BULK UPDATE !!!
			   --	THE VALUE OF p_SensorDateGMT IS DATEANDTIME ???
			   --   FREQUENCY OF INSERTS IN POSBATTERYINFO EVERY HOUR ?
			   --	PURGING OF POSbatteryInfo TABLE
			   --	REASON FOR UPDATE ??
			   --	WHERE IS THE LOGIC TO MAKE SURE THE ALREADY UPDATED RECORD IS NOT FETCHED AGAIN FOR PROCESSING ??
			   
			   --	UPDATE BatteryInfo SET SystemLoad = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   
			   UPDATE POSBatteryInfo SET SystemLoad = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
        ELSEIF p_SensorTypeId = 5 THEN
			
               --	UPDATE BatteryInfo SET InputCurrent = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   UPDATE POSBatteryInfo SET InputCurrent = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
        ELSEIF p_SensorTypeId = 0 THEN
			
               --	UPDATE BatteryInfo SET lv_battery_voltage = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   UPDATE POSBatteryInfo SET BatteryVoltage = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
         END IF;
    END IF;
	
	
   
END//

DELIMITER ;

-- --------------------------------------------------------------------------




-- Stored Procedure 
-- Purpose: To Update the ActiveAlerts Counts in POSAlertStatus for Paystation
-- Used for Incremental Migration
-- Created Date: 2013-12-06
-- Modifed Date: 2014-01-21
-- SUPP 552 Created on April 25
DROP PROCEDURE IF EXISTS sp_UpdatePaystationAlertCount;

delimiter //

CREATE  PROCEDURE `sp_UpdatePaystationAlertCount`(IN P_PointOfSaleId MEDIUMINT UNSIGNED)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

declare lv_no_of_records_ActivePOSAlert int;
declare lv_PointOfSaleId MEDIUMINT UNSIGNED ;
declare lv_EventSeverityTypeId,lv_EventDeviceTypeId tinyint unsigned;
declare lv_AlertThresholdTypeId smallint unsigned ;
declare lv_cnt_minor,lv_cnt_major, lv_cnt_critical tinyint unsigned; 
declare lv_minor_alertId,lv_major_alertId, lv_critical_alertId,lv_cnt_record_exists int unsigned;

declare lv_CommunicationMinor, lv_CommunicationMajor, lv_CommunicationCritical,lv_CollectionMinor, lv_CollectionMajor,lv_CollectionCritical tinyint unsigned;
declare lv_PaystationMinor, lv_PaystationMajor, lv_PaystationCritical tinyint unsigned;

declare c2 cursor  for
Select
apa.PointOfSaleId,  cat.AlertThresholdTypeId, apa.EventDeviceTypeId,
SUM(CASE WHEN apa.EventSeverityTypeId = 1 THEN 1 ELSE 0 END) AS Minor,
SUM(CASE WHEN apa.EventSeverityTypeId = 2 THEN 1 ELSE 0 END) AS Major,
SUM(CASE WHEN apa.EventSeverityTypeId = 3 THEN 1 ELSE 0 END) AS Critical,
MAX(CASE WHEN apa.EventSeverityTypeId = 1 THEN apa.Id ELSE NULL END) AS MinorAlertId,
MAX(CASE WHEN apa.EventSeverityTypeId = 2 THEN apa.Id ELSE NULL END) AS MajorAlertId,
MAX(CASE WHEN apa.EventSeverityTypeId = 3 THEN apa.Id ELSE NULL END) AS CriticalAlertId
from ActivePOSAlert apa
INNER JOIN CustomerAlertType cat ON (apa.CustomerAlertTypeId = cat.Id)
Where apa.PointOfSaleId = P_PointOfSaleId
AND apa.IsActive = 1
group by cat.AlertThresholdTypeId, apa.EventDeviceTypeId;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;



START TRANSACTION;

Select
apa.PointOfSaleId, 
SUM(CASE WHEN apa.EventSeverityTypeId = 1 AND cat.AlertThresholdTypeId = 1 THEN 1 ELSE 0 END) AS CommunicationMinor,
SUM(CASE WHEN apa.EventSeverityTypeId = 2 AND cat.AlertThresholdTypeId = 1 THEN 1 ELSE 0 END) AS CommunicationMajor,
SUM(CASE WHEN apa.EventSeverityTypeId = 3 AND cat.AlertThresholdTypeId = 1 THEN 1 ELSE 0 END) AS CommunicationCritical,
SUM(CASE WHEN apa.EventSeverityTypeId = 1 AND cat.AlertThresholdTypeId NOT IN (1,12) THEN 1 ELSE 0 END) AS CollectionMinor,
SUM(CASE WHEN apa.EventSeverityTypeId = 2 AND cat.AlertThresholdTypeId NOT IN (1,12) THEN 1 ELSE 0 END) AS CollectionMajor,
SUM(CASE WHEN apa.EventSeverityTypeId = 3 AND cat.AlertThresholdTypeId NOT IN (1,12) THEN 1 ELSE 0 END) AS CollectionCritical,
SUM(CASE WHEN apa.EventSeverityTypeId = 1 AND cat.AlertThresholdTypeId = 12 THEN 1 ELSE 0 END) AS PaystationMinor,
SUM(CASE WHEN apa.EventSeverityTypeId = 2 AND cat.AlertThresholdTypeId = 12 THEN 1 ELSE 0 END) AS PaystationMajor,
SUM(CASE WHEN apa.EventSeverityTypeId = 3 AND cat.AlertThresholdTypeId = 12 THEN 1 ELSE 0 END) AS PaystationCritical
INTO lv_PointOfSaleId , lv_CommunicationMinor , lv_CommunicationMajor, lv_CommunicationCritical, lv_CollectionMinor, lv_CollectionMajor,
lv_CollectionCritical , lv_PaystationMinor, lv_PaystationMajor, lv_PaystationCritical
from ActivePOSAlert apa
INNER JOIN CustomerAlertType cat ON (apa.CustomerAlertTypeId = cat.Id)
Where apa.PointOfSaleId =P_PointOfSaleId
AND apa.IsActive = 1;

UPDATE POSAlertStatus SET CommunicationMinor = lv_CommunicationMinor, CommunicationMajor=lv_CommunicationMajor,CommunicationCritical=lv_CommunicationCritical,
CollectionMinor=lv_CollectionMinor,
CollectionMajor=lv_CollectionMajor,CollectionCritical=lv_CollectionCritical,PayStationMinor=lv_PaystationMinor,PayStationMajor=lv_PaystationMajor,PayStationCritical=lv_PaystationCritical,
VERSION = VERSION+1
WHERE PointOfSaleId = P_PointOfSaleId ;
	

set indexm_pos =0;
set idmaster_pos =0;

-- Reset the values to 0
UPDATE POSAlertStatusDetail SET Minor=0,Major=0,Critical=0,MinorAlertId = null,MajorAlertId=null,CriticalAlertId=null,VERSION = VERSION+1
WHERE PointOfSaleId = P_PointOfSaleId ;

open c2;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c2 into lv_PointOfSaleId, lv_AlertThresholdTypeId , lv_EventDeviceTypeId, lv_cnt_minor,lv_cnt_major, lv_cnt_critical,lv_minor_alertId,lv_major_alertId, lv_critical_alertId;

	select count(*) into lv_cnt_record_exists from POSAlertStatusDetail 
	where PointOfSaleId = lv_PointOfSaleId and AlertThresholdTypeID = lv_AlertThresholdTypeId and EventDeviceTypeId = lv_EventDeviceTypeId ;
	
	IF (lv_cnt_record_exists=0) THEN
		
		INSERT INTO POSAlertStatusDetail (PointOfSaleId,    AlertThresholdTypeID,    EventDeviceTypeId,		Minor,			Major,		  Critical,        MinorAlertId,     MajorAlertId,    CriticalAlertId)
		VALUES  						 (lv_PointOfSaleId, lv_AlertThresholdTypeId, lv_EventDeviceTypeId,lv_cnt_minor, 	lv_cnt_major, lv_cnt_critical, lv_minor_alertId, lv_major_alertId,lv_critical_alertId) ;
	ELSE
		
		UPDATE POSAlertStatusDetail
		SET Minor = lv_cnt_minor,
		Major = lv_cnt_major,
		Critical = lv_cnt_critical,
		MinorAlertId = lv_minor_alertId,
		MajorAlertId = lv_major_alertId,
		CriticalAlertId = lv_critical_alertId,
		VERSION = VERSION +1
		WHERE
		PointOfSaleId = lv_PointOfSaleId AND
		AlertThresholdTypeID = lv_AlertThresholdTypeId AND
		EventDeviceTypeId = lv_EventDeviceTypeId ;
		
	END IF;
	
	
set indexm_pos = indexm_pos +1;
end while;

close c2;

COMMIT;

END//
delimiter ;
-- End of Stored Precedure sp_UpdatePOSAlertStatus



-- This script is used by INC migration
-- It creates Admin User Account,UserRole and UserDefaultDashboard on EMS7
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_CreateAdminUser $$
CREATE PROCEDURE sp_CreateAdminUser (in P_CustomerName Varchar(25))

BEGIN

INSERT INTO UserAccount
(
	CustomerId,
    UserStatusTypeId,
    CustomerEmailId,
    UserName,
    FirstName,
    LastName,
    Password,
    IsPasswordTemporary,
    VERSION,
    LastModifiedGMT,
    LastModifiedByUserId,
    PasswordSalt)
SELECT
    ID as CustomerId,
    1 as UserStatusTypeId,
    null as CustomerEmailId, 
    concat('admin%40', replace(Name," ", "+") ) as UserName, 
    'Administrator' as FirstName, 
    '' as LastName,
    'XXXXXX3273590f96b385ddd700802yyyyyy' as Password,
    1 as IsPasswordTemporary,
    1 as VERSION,
    UTC_TIMESTAMP() as LastModifiedGMT,
    1 as LastModifiedByUserId,
    'SaltSaltSaltSalt' as PasswordSalt
FROM Customer WHERE Name = P_CustomerName;


-- Create a Role to the UserAccount

INSERT INTO UserRole
(
UserAccountId,
RoleId,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    ID as UserAccountId, 
    3 as RoleId,  
    1 as VERSION,
    UTC_TIMESTAMP() as LastModifiedGMT,
    1 as LastModifiedByUserId
from UserAccount where UserName =  concat('admin%40', replace(P_CustomerName," ", "+") );


-- Create default dashboard for this user account

INSERT INTO UserDefaultDashboard
(
UserAccountId,
IsDefaultDashboard,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    id as UserAccountId, 
    1 as IsDefaultDashboard, 
    0 as VERSION, 
    UTC_Timestamp() as LastModifiedGMT, 
    1 as LastModifiedByUserId 
FROM UserAccount where UserName = concat('admin%40', replace(P_CustomerName," ", "+") ) ;

END $$

DELIMITER ;

-- End sp_CreateAdminUser


-- --------------------------------------------------------------------------------
-- Routine DDL
-- --------------------------------------------------------------------------------
-- Used in EMS
DROP PROCEDURE IF EXISTS `sp_ProcessBatteryInfo` ;

delimiter //




CREATE PROCEDURE `sp_ProcessBatteryInfo`(
IN p_Value FLOAT, IN p_PointOfSaleId INT, IN p_SensorTypeId INT, IN p_SensorDateGMT DATETIME)

-- EMS 7 lv_battery_voltage Info Stored Procedure
-- Last Modified by Ashok on Feb 20 2014 EMS 5073
--  Modified by Ashok on Mar 01 2013


BEGIN
    DECLARE lv_sLoad FLOAT DEFAULT 0;
    DECLARE lv_iCurrent FLOAT DEFAULT 0;
    DECLARE lv_battery_voltage FLOAT DEFAULT 0;
    DECLARE result INT DEFAULT 0;
	DECLARE lv_return_value TINYINT DEFAULT 1;
    -- INSERT INTO SensorLog(PaystationId, TypeId, DateField, `Value`) VALUES(p_PayStationId, p_SensorTypeId, p_SensorDateGMT, p_Value);
	
	-- Get max(ID) ? or ID during that point of time from Table PointOfSale which corresponds to
	-- GET THE SAME LOGIC USED IN MIGRATION SCRIPTS
	/*SELECT
		PointOfSale_Id
		INTO p_PointOfSaleId
	FROM
		PointOfSale_v
	WHERE
		PointOfSale_Id = p_PointOfSaleId;
	*/
	
    IF (p_SensorTypeId = 0 OR p_SensorTypeId = 5 OR p_SensorTypeId = 6) THEN
        -- SELECT COUNT(PaystationId) INTO result FROM BatteryInfo WHERE PaystationId = p_PayStationId AND DateField = p_SensorDateGMT;
		SELECT 
			COUNT(PointOfSaleId) 
			INTO result 
		FROM 
			POSBatteryInfo 
		WHERE 
			PointOfSaleId = p_PointOfSaleId AND SensorGMT = p_SensorDateGMT; -- ?? both Date and Time are passed
			
        CASE p_SensorTypeId
            WHEN 0 THEN
               SET lv_battery_voltage = p_Value;
            WHEN 5 THEN
               SET lv_iCurrent = p_Value;
            WHEN 6 THEN
               SET lv_sLoad = p_Value;
        END CASE;
        IF (result <> 1) THEN
		
            --	INSERT INTO BatteryInfo(SystemLoad, InputCurrent, lv_battery_voltage, PaystationId, DateField) VALUES(lv_sLoad, lv_iCurrent, lv_battery_voltage, p_PayStationId, p_SensorDateGMT);
			-- 	INSERT INTO SAME TABLE ????
			INSERT INTO POSBatteryInfo(PointOfSaleId, 		SensorGMT, SystemLoad, InputCurrent, BatteryVoltage) VALUES
									  (p_PointOfSaleId,	p_SensorDateGMT, 	   lv_sLoad,	   lv_iCurrent,	 lv_battery_voltage);
        
        ELSEIF p_SensorTypeId = 6 THEN
			
			   --	THIS IS A BULK UPDATE !!!
			   --	THE VALUE OF p_SensorDateGMT IS DATEANDTIME ???
			   --   FREQUENCY OF INSERTS IN POSBATTERYINFO EVERY HOUR ?
			   --	PURGING OF POSbatteryInfo TABLE
			   --	REASON FOR UPDATE ??
			   --	WHERE IS THE LOGIC TO MAKE SURE THE ALREADY UPDATED RECORD IS NOT FETCHED AGAIN FOR PROCESSING ??
			   
			   --	UPDATE BatteryInfo SET SystemLoad = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   
			   UPDATE POSBatteryInfo SET SystemLoad = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
        ELSEIF p_SensorTypeId = 5 THEN
			
               --	UPDATE BatteryInfo SET InputCurrent = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   UPDATE POSBatteryInfo SET InputCurrent = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
        ELSEIF p_SensorTypeId = 0 THEN
			
               --	UPDATE BatteryInfo SET lv_battery_voltage = p_Value WHERE PaystationId = p_PayStationId AND DateField=p_SensorDateGMT;
			   UPDATE POSBatteryInfo SET BatteryVoltage = p_Value WHERE PointOfSaleId = p_PointOfSaleId AND SensorGMT=p_SensorDateGMT;
			   
         END IF;
    END IF;
	
	SELECT lv_return_value ;
   
END//

DELIMITER ;


-- New Procedure Added for Performance

-- This procedure will clean up POSAlerts Records
--  Last modifed on APril 25
-- SUPP 552

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_UpdatePOSAlert $$
CREATE PROCEDURE sp_UpdatePOSAlert (in P_Sleep int)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_id INT UNSIGNED ;

declare c1 cursor  for
 select Id from PointOfSale order by Id;
 
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_id;


	UPDATE POSAlert SET IsActive = 0, ClearedGMT = UTC_TIMESTAMP(), ClearedByUserId = 1 
	WHERE PointOfSaleId = lv_id AND Id IN (SELECT Id FROM (SELECT pa.Id from POSAlert pa
             							   LEFT JOIN ActivePOSAlert apa ON (pa.Id = apa.POSAlertId)
                                           WHERE apa.Id IS NULL
                                           AND pa.IsActive = 1
                                           And pa.PointOfSaleId = lv_id) A) ;

	
	-- SUPP 552
	call sp_UpdatePaystationAlertCount(lv_id) ;
	select sleep(P_Sleep);
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

select 'Completed' As Status;
END $$

DELIMITER ;

--

-- Procedure sp_RemovePreviousActivePermit

DROP PROCEDURE IF EXISTS sp_RemovePreviousActivePermit ;

delimiter //

CREATE  PROCEDURE sp_RemovePreviousActivePermit(in P_Limit MEDIUMINT UNSIGNED)
BEGIN


DECLARE CursorDone INT DEFAULT 0;
declare idmaster_pos,indexm_pos int default 0;
DECLARE v_ActivePermitId BIGINT UNSIGNED ;

DECLARE C_ActivePermit CURSOR FOR
	
		SELECT
			Id
		FROM 
			ActivePermit
		WHERE
			PermitExpireGMT < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 14 day) and   -- Can we take this value from EmsProperties?
			SpaceNumber = 0 OR  SpaceNumber IS NULL  LIMIT P_Limit;
			
				
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
 

 -- SET lv_Customer_Timezone = null;
set indexm_pos =0;
set idmaster_pos =0;

 OPEN C_ActivePermit;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 

WHILE indexm_pos < idmaster_pos do
    
	FETCH C_ActivePermit INTO  	v_ActivePermitId ;	
								
			DELETE FROM ActivePermit 
			WHERE  
			Id = v_ActivePermitId; 
			
					
	set indexm_pos = indexm_pos +1;
end while;

close C_ActivePermit;

COMMIT ;		
        
END//

delimiter ;


-- End Procedure sp_RemovePreviousActivePermit

-- Added for 7.1

DROP PROCEDURE IF EXISTS sp_AddNewTabToUserAccounts;

delimiter //

CREATE PROCEDURE sp_AddNewTabToUserAccounts(in P_TabName varchar(20)) 


 BEGIN

  declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_UserAccountId,lv_CustomerId MEDIUMINT UNSIGNED;
 declare lv_Parent TINYINT UNSIGNED;


declare c1 cursor  for
select distinct UserAccountId from Widget where UserAccountId not in (
select  UserAccountId from Tab where 
name = P_TabName
group by UserAccountId
);

declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_UserAccountId;
	-- get CustomerId from UserAccountId
	-- We can merge these two SQL's
	select CustomerId into lv_CustomerId from UserAccount where Id = lv_UserAccountId; 
	-- Check if CustomerId isParent ?
	SELECT IsParent into lv_Parent from Customer where Id = lv_CustomerId ;
	IF (lv_Parent = 0) THEN
		CALL sp_AddUserAccountWidgetTab(lv_UserAccountId,P_TabName);
	END IF;
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  
END//
delimiter ;


DROP PROCEDURE IF EXISTS sp_AddUserAccountWidgetTab;

delimiter //

CREATE PROCEDURE sp_AddUserAccountWidgetTab(in P_UserAccountId MEDIUMINT UNSIGNED, in P_TabName varchar(20)) 


 BEGIN
 DECLARE lv_TabId, lv_SectionId INT UNSIGNED;
 DECLARE lv_CustomerId MEDIUMINT UNSIGNED ;
 DECLARE lv_LastPosition TINYINT Unsigned ;
 
 SELECT CustomerId into lv_CustomerId FROM UserAccount where Id = P_UserAccountId;
 SELECT MAX(OrderNumber) into lv_LastPosition FROM Tab where UserAccountId =  P_UserAccountId;
 
INSERT INTO `Tab` (`UserAccountId`, `Name`, `OrderNumber`, `VERSION`, `LastModifiedGMT`) VALUES (P_UserAccountId, P_TabName, lv_LastPosition +1, '0', '2014-04-15 00:00:00');

SET lv_TabId = LAST_INSERT_ID();

INSERT INTO Section (UserAccountId, TabId, LayoutId, OrderNumber, Version, LastModifiedGMT) VALUES
(P_UserAccountId, lv_TabId,3,1,1,UTC_TIMESTAMP());

SET lv_SectionId = LAST_INSERT_ID();


-- SELECT Id into lv_SectionId from Section where UserAccountId = P_UserAccountId and TabId = ( select Id from Tab where UserAccountId = P_UserAccountId and Name = 'Parker Metrics');

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
13,6,3,105,
0,0,3,0,0,1,'Avg Revenue by Day','Average revenue for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
12,5,3,105,
0,0,3,0,1,1,'Avg Price by Day','Average price for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());


INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
P_UserAccountId,
lv_SectionId,
14,5,3,0,
0,0,3,0,2,1,'Avg Duration by Day','Average duration per space for the last 7 days grouped by day',0,
lv_CustomerId,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

END//
delimiter ;


--
DROP PROCEDURE IF EXISTS sp_AddNewDefaultDashboardTab;

delimiter //

CREATE PROCEDURE sp_AddNewDefaultDashboardTab( in P_TabName varchar(20)) 

BEGIN
 
DECLARE lv_TabId, lv_SectionId INT UNSIGNED;
Declare lv_record_cnt TINYINT UNSIGNED ;

-- check if already record exists in Tab

select count(*) into lv_record_cnt FROM Tab where UserAccountId = 1 and Name = P_TabName ;

if (lv_record_cnt = 0) THEN

INSERT INTO `Tab` (`UserAccountId`, `Name`, `OrderNumber`, `VERSION`, `LastModifiedGMT`) VALUES 
(1, P_TabName, 4, '0', UTC_TIMESTAMP());

SET lv_TabId = LAST_INSERT_ID();

INSERT INTO Section (UserAccountId, TabId, LayoutId, OrderNumber, Version, LastModifiedGMT) VALUES
(1, lv_TabId,3,1,1,UTC_TIMESTAMP());

SET lv_SectionId = LAST_INSERT_ID();

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
13,6,3,105,
0,0,3,0,0,1,'Avg Revenue/Space by Day','Average revenue per space for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
12,5,3,105,
0,0,3,0,1,1,'Avg Price/Permit by Day','Average price per permit for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());


INSERT INTO Widget (WidgetListTypeId,
UserAccountId,SectionId,
WidgetMetricTypeId,WidgetRangeTypeId,WidgetTier1TypeId,WidgetTier2TypeId,
WidgetTier3TypeId,WidgetFilterTypeId,WidgetChartTypeId,WidgetLimitTypeId,ColumnId,OrderNumber,Name,Description,ParentCustomerId,
CustomerId,
TrendAmount,IsSubsetTier1,IsSubsetTier2,IsSubsetTier3,IsTrendByLocation,IsShowPaystations,VERSION,LastModifiedGMT)
VALUES
(3,
1,lv_SectionId,
14,5,3,0,
0,0,3,0,2,1,'Avg Duration/Permit by Day','Average duration per permit for the last 7 days grouped by day',0,
0,
0,0,0,0,0,0,0,UTC_TIMESTAMP());

select 'Completed' AS INFO;

ELSE

select 'This Procedure has been already executed before' AS INFO;

END IF ;


END//
delimiter ;




-- EMS-10037
-- Begin sp_AutomatePreAuthHoldingToRetry New Logic

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_AutomateElavonPreAuthHoldingToRetry $$

CREATE PROCEDURE `sp_AutomateElavonPreAuthHoldingToRetry`()
BEGIN
    
	-- EMS-10037
	-- EMS-5880 June 12
    -- Number of records inserted into table CardRetryTransaction
    DECLARE Inserted INT UNSIGNED;
    
  
    -- Creation_Date: default GMT value assigned to CardRetryTransaction.CreationDate & PreAuthHolding.MovedGMT
    DECLARE Creation_Date DATETIME;
	DECLARE lv_ProcessorTransactionId BIGINT UNSIGNED ;
	DECLARE lv_SPH_PreAuthId BIGINT UNSIGNED ;
	DECLARE lv_PH_PreAuthId BIGINT UNSIGNED ;	
    declare NO_DATA int(4) default 0;
	declare idmaster_pos,indexm_pos,terminal_pos int(4);
	
    declare c1 cursor  for
	SELECT distinct SPH.ProcessorTransactionId, SPH.PreAuthId, PH.PreAuthId
	FROM StagingPreAuthHoldingToRetry SPH 
	left outer join PreAuthHolding PH on SPH.PreAuthId = PH.PreAuthId
    inner join ProcessorTransaction PT on SPH.ProcessorTransactionId = PT.Id
    inner join MerchantAccount MA on PT.MerchantAccountId = MA.Id
    inner join Processor P on MA.ProcessorId = P.Id
	where MA.ProcessorId = 19
	LIMIT 60000 ;
	
	declare continue handler for not found set NO_DATA=-1;

    -- Initialize internal variables
    SET Inserted = 0;
    
    SET Creation_Date = UTC_TIMESTAMP();
    
    
	set indexm_pos =0;
	set idmaster_pos =0;

	open c1;
	set idmaster_pos = (select FOUND_ROWS());

	while indexm_pos < idmaster_pos do
	fetch c1 into lv_ProcessorTransactionId, lv_SPH_PreAuthId, lv_PH_PreAuthId ;
    -- start transaction
    -- START TRANSACTION;

    IF (lv_PH_PreAuthId IS NOT NULL)  THEN
	-- Insert records into CardRetryTransaction
    INSERT  CardRetryTransaction (PointOfSaleId,  PurchasedDate,  TicketNumber,  LastRetryDate,  NumRetries,  
								CardHash,  CardRetryTransactionTypeId,  CardData,  CardExpiry,  Amount,  
								CardType,  Last4DigitsOfCardNumber,  CreationDate,  BadCardHash,  IgnoreBadCard,  
								LastResponseCode,  IsRFID)
	SELECT  PT.PointOfSaleId ,
            PT.PurchasedDate ,
            PT.TicketNumber ,
            PH.PreAuthDate AS LastRetryDate ,
            0 AS NumRetries ,
            PH.CardHash ,
            1 AS TypeId ,
            PH.CardData ,
            PH.CardExpiry ,
            PT.Amount ,
            PH.CardType ,
            PH.Last4DigitsOfCardNumber ,
            UTC_TIMESTAMP() AS CreationDate ,
            '' AS BadCardHash ,
            0 AS IgnoreBadCard ,
            0 AS LastResponseCode ,
            PH.IsRFID
    FROM    ProcessorTransaction PT ,
            PreAuthHolding PH 
    WHERE   
			PT.Id = lv_ProcessorTransactionId
    AND     PT.AuthorizationNumber = PH.PreAuthId
    AND 	PT.PointOfSaleId = PH.PointOfSaleId
    AND     PT.Amount = PH.Amount
    AND     PH.Approved = 1
    AND     PH.Expired = 1
    AND     PH.CardType IN ('AMEX','Discover','Diners Club','MasterCard','VISA','CreditCard','TotalCard','JCB')
    AND	    PH.MovedToRetryGMT IS NULL;
								
    
    -- Save number of records inserted
    SET Inserted = ROW_COUNT();
    
	IF (Inserted >= 1) THEN
		-- Mark PreAuthHolding records as having been moved into CardRetryTransaction
		UPDATE  PreAuthHolding A SET A.MovedToRetryGMT = Creation_Date WHERE A.PreAuthId = lv_PH_PreAuthId;
    
		INSERT INTO ArchiveStagingPreAuthHoldingToRetry (ProcessorTransactionId, IsMovedToCardRetry, MovedGMT) VALUES
													(lv_ProcessorTransactionId, 1, 				Creation_Date);
	
		DELETE FROM StagingPreAuthHoldingToRetry where ProcessorTransactionId = lv_ProcessorTransactionId ;
	
	ELSE
		INSERT INTO ArchiveStagingPreAuthHoldingToRetry (ProcessorTransactionId, IsMovedToCardRetry, MovedGMT) VALUES
													(lv_ProcessorTransactionId, 0, 				Creation_Date);
	
		DELETE FROM StagingPreAuthHoldingToRetry where ProcessorTransactionId = lv_ProcessorTransactionId ;
		
	END IF; -- close if block for Inserted
	
    END IF;
	
	
	IF (lv_PH_PreAuthId IS NULL)  THEN -- PreAuth does not exists in PreAuthHolding
		
		INSERT INTO ArchiveStagingPreAuthHoldingToRetry (ProcessorTransactionId, IsMovedToCardRetry, MovedGMT) VALUES
													(lv_ProcessorTransactionId, 0, 				Creation_Date);
		DELETE FROM StagingPreAuthHoldingToRetry where ProcessorTransactionId = lv_ProcessorTransactionId ;
	
	END IF;
	
    							
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End sp_PreAuthHoldingToRetry




-- Stored Procedure 
-- Purpose: To Update the ActiveAlerts Counts in POSAlertStatus on the new Alert framework
-- Created Date: 2016-02-29

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_UpdatePOSAlertStatus` $$
CREATE PROCEDURE `sp_UpdatePOSAlertStatus` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

declare lv_no_of_ActiveRecords_POSEventCurrent int;
declare lv_PointOfSaleId MEDIUMINT UNSIGNED ;
declare lv_EventSeverityTypeId tinyint unsigned;
declare lv_AlertThresholdTypeId smallint unsigned ;



declare c1 cursor  for
select count(*), A.PointOfSaleId ,A.EventSeverityTypeId, B.AlertThresholdTypeId
from POSEventCurrent A, CustomerAlertType B
where A.EventTypeId = 46 and A.IsActive=1 AND A.CustomerAlertTypeId = B.Id
group by A.PointOfSaleId, A.EventSeverityTypeId, B.AlertThresholdTypeId 
union
select count(*), A.PointOfSaleId ,A.EventSeverityTypeId,'12'
from POSEventCurrent A
where A.EventTypeId <> 46 and A.IsActive=1 
group by A.PointOfSaleId, A.EventSeverityTypeId;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

-- Reset all values
	UPDATE POSAlertStatus SET 
	CommunicationMinor =0, CommunicationMajor=0, CommunicationCritical=0,CollectionMinor=0,CollectionMajor=0,
	CollectionCritical=0, PayStationMinor=0, PayStationMajor=0,PayStationCritical=0,VERSION = VERSION +1;
	
open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_no_of_ActiveRecords_POSEventCurrent, lv_PointOfSaleId, lv_EventSeverityTypeId, lv_AlertThresholdTypeId ;

	
						 
	-- CommunicationMajor
    IF (lv_AlertThresholdTypeId = 1) AND (lv_EventSeverityTypeId= 2)  THEN
	
		UPDATE POSAlertStatus
		SET CommunicationMajor = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
	-- CollectionCritical
	IF (lv_AlertThresholdTypeId not in (1,12)) AND (lv_EventSeverityTypeId= 3)  THEN
	
		UPDATE POSAlertStatus
		SET CollectionCritical = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
	-- CollectionMinor
	IF (lv_AlertThresholdTypeId not in (1,12)) AND (lv_EventSeverityTypeId= 1)  THEN
	
		UPDATE POSAlertStatus
		SET CollectionMinor = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
	-- PayStationMinor
	IF (lv_AlertThresholdTypeId = 12) AND (lv_EventSeverityTypeId= 1)  THEN
	
		UPDATE POSAlertStatus
		SET PayStationMinor = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
	-- PayStationMajor
	IF (lv_AlertThresholdTypeId = 12) AND (lv_EventSeverityTypeId= 2)  THEN
	
		UPDATE POSAlertStatus
		SET PayStationMajor = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
	-- PayStationCritical
	IF (lv_AlertThresholdTypeId = 12) AND (lv_EventSeverityTypeId= 3)  THEN
	
		UPDATE POSAlertStatus
		SET PayStationCritical = lv_no_of_ActiveRecords_POSEventCurrent 
		WHERE PointOfSaleId = lv_PointOfSaleId ;
	
	END IF;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End of Stored Precedure sp_UpdatePOSAlertStatus
