
-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateAccount`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateAccount`(
    IN p_Name VARCHAR(25),
    IN p_Title VARCHAR(25),
    IN p_Organization VARCHAR(25),
    IN p_CustomerName VARCHAR(25),
    IN p_ParentCustomerId MEDIUMINT UNSIGNED
)
BEGIN

	DECLARE v_CustomerId MEDIUMINT; -- = sprintf("%04d",(Select Id from Customer where Name = p_CustomerName));
	DECLARE v_TimeZone VARCHAR(20);
	DECLARE v_NewLocationId MEDIUMINT;
	DECLARE v_Location1Id MEDIUMINT;
	DECLARE v_Location2Id MEDIUMINT;
	DECLARE v_NewRouteId MEDIUMINT;
	DECLARE v_Route1Id MEDIUMINT;
	DECLARE v_Route2Id MEDIUMINT;
	DECLARE v_Route3Id MEDIUMINT;
	DECLARE v_Route4Id MEDIUMINT;
	-- Added on March 16 2015
	DECLARE v_ParentCustomerId MEDIUMINT;
	DECLARE v_CustAgreeRecord TINYINT;

    START TRANSACTION;

    -- add check for pay station serial number and customer existance


    --
    -- Create Customer
    --
	SET v_TimeZone = 'Canada/Pacific';
    SET v_CustomerId = 0;
    CALL sp_Preview_InsertCustomer
    ( 
          3,    -- p_CustomerTypeId
          1,    -- p_CustomerStatusTypeId
          p_ParentCustomerId, -- p_ParentCustomerId
          p_CustomerName, -- p_CustomerName
          NULL, -- p_TrialExpiryGMT
          0,    -- p_IsParent
          CONCAT('admin%40', p_CustomerName), -- p_UserName
          '58ae83e00383273590f96b385ddd700802c3f07d', -- p_Password
          1,    -- p_IsStandardReports
          1,    -- p_IsAlerts
          1,    -- p_IsRealTimeCC
          1,    -- p_IsBatchCC
          1,    -- p_IsRealTimeValue
          1,    -- p_IsCoupons
          1,    -- p_IsValueCards
          1,    -- p_IsSmartCards
          1,    -- p_IsExtendByPhone
          1,    -- p_IsDigitalAPIRead
          1,    -- p_IsDigitalAPIWrite
          1,    -- p_Is3rdPartyPayByCell
          1,    -- p_IsDigitalCollect
          1,    -- p_IsOnlineConfiguration
		  1,    -- p_IsFlexIntegration
		  1,    -- p_IsCaseIntegration
		  1,    -- p_IsDigitalAPIXChange
		  1,    -- p_IsOnlineRate
          v_TimeZone,    -- p_Timezone
          1,    -- p_UserId
          'SaltSaltSaltSalt', -- p_PasswordSalt
		  v_CustomerId
    );
	SELECT v_CustomerId;
	
	-- Code added on March 16 2015
	-- Check if this Customer has Parent.
	-- If Yes, then ParentCustomer has to sign T&C
	-- If No, then Customer has to sign T&C
	
	select ifnull(ParentCustomerId,0) into v_ParentCustomerId from Customer where Id = v_CustomerId; 
	
	select count(*) into v_CustAgreeRecord from CustomerAgreement where CustomerId = v_ParentCustomerId ;
	
	IF (v_ParentCustomerId = 0) THEN  -- Meaning this Customer has no parent
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_CustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	ELSEIF (v_ParentCustomerId > 0) and  (v_CustAgreeRecord = 0) THEN -- this customer is a child and Parent Customer has not signed T&C
	
	INSERT INTO CustomerAgreement 	(CustomerId	 , ServiceAgreementId, Name	 , Title	, Organization	, AgreedGMT		 , IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId) 
						VALUES 		(v_ParentCustomerId, '1'				 , p_Name, p_Title	, p_Organization, UTC_TIMESTAMP(), '0'		 , '0'	  , UTC_TIMESTAMP(), '1');
	
	
	END IF;
    --
    -- Locations
    --
    SET v_NewLocationId = 0;
	CALL sp_Preview_CreateLocation(p_CustomerName, 'Downtown'	,'Pay-By-Plate',	0, NULL, 1, 150, 1000000, v_NewLocationId);
	SELECT v_NewLocationId INTO v_Location1Id;
    CALL sp_Preview_CreateLocation(p_CustomerName, 'Airport'	,'Pay-By-Space',	0, NULL, 1, 300, 3000000, v_NewLocationId);
	SELECT v_NewLocationId INTO v_Location2Id;

	--
	-- Routes
	--
    SET v_NewRouteId = 0;
	CALL sp_Preview_CreateRoute(v_CustomerId, 1, v_Location1Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route1Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 2, v_Location1Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route2Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 1, v_Location2Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route3Id;
	CALL sp_Preview_CreateRoute(v_CustomerId, 2, v_Location2Id, v_NewRouteId);
	SELECT v_NewRouteId INTO v_Route4Id;


    --
    -- Pay Stations
    --
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',	CONCAT('5000',LPAD(v_CustomerId,4,'0'), '0001'), 1, v_TimeZone,49.28600334159728,-123.12086105346685, v_Route1Id, v_Route2Id);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('5000',LPAD(v_CustomerId,4,'0'), '0002'), 1, v_TimeZone,49.28348390950996,-123.11614036560059, v_Route1Id, v_Route2Id);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Downtown',    CONCAT('5000',LPAD(v_CustomerId,4,'0'), '0003'), 1, v_TimeZone,49.28085236522246,-123.12223434448242, v_Route1Id, v_Route2Id);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,4,'0'), '0004'), 1, v_TimeZone,49.19380640706086,-123.17587852478026, v_Route3Id, v_Route4Id);
	CALL sp_Preview_CreatePayStation(p_CustomerName, 'Airport',     CONCAT('5000',LPAD(v_CustomerId,4,'0'), '0005'), 1, v_TimeZone,49.19304917865785,-123.17761659622194, v_Route3Id, v_Route4Id);
		

    COMMIT;

END$$
DELIMITER ;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertCustomer`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_InsertCustomer`(
	IN p_CustomerTypeId TINYINT UNSIGNED, 
	IN p_CustomerStatusTypeId TINYINT UNSIGNED, 
	IN p_ParentCustomerId MEDIUMINT UNSIGNED, 
	IN p_CustomerName VARCHAR(25), 
	IN p_TrialExpiryGMT DATETIME, 
	IN p_IsParent TINYINT(1) UNSIGNED, 
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_IsStandardReports TINYINT(1), 
	IN p_IsAlerts TINYINT(1),
	IN p_IsRealTimeCC TINYINT(1), 
	IN p_IsBatchCC TINYINT(1), 
	IN p_IsRealTimeValue TINYINT(1),
	IN p_IsCoupons TINYINT(1), 
	IN p_IsValueCards TINYINT(1), 
	IN p_IsSmartCards TINYINT(1), 
	IN p_IsExtendByPhone TINYINT(1), 
	IN p_IsDigitalAPIRead TINYINT(1), 
	IN p_IsDigitalAPIWrite TINYINT(1),
	IN p_Is3rdPartyPayByCell TINYINT(1),
	IN p_IsDigitalCollect TINYINT(1),
	IN p_IsOnlineConfiguration TINYINT(1),
	IN p_IsFlexIntegration  TINYINT(1),
	IN p_IsCaseIntegration  TINYINT(1),
	IN p_IsDigitalAPIXChange  TINYINT(1),
	IN p_IsOnlineRate TINYINT(1),
	IN p_Timezone VARCHAR(40),
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16), 
	OUT v_CustomerId MEDIUMINT
	)
BEGIN
        
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     
    
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
        
    START TRANSACTION;
        
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP());
        
    SELECT LAST_INSERT_ID() INTO v_CustomerId;    
    
    UPDATE Customer SET UnifiId = v_CustomerId WHERE Id = v_CustomerId;
	
    IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
		INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (1,	 v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        SET v_UserAccountId = 1;
    ELSE
		INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    END IF ;
    
        
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   
        
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);

        -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    	INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES          (v_UserAccountId, 				 3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    	-- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    	INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES              (v_CustomerId, 				  3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
    	SELECT 			  v_CustomerId,	UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
    	FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
    	
    	INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
    	FROM UserRole ur 
    	INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    	
    	INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM CustomerRole cr
		INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
		WHERE r.RoleStatusTypeId != 2
		AND r.CustomerTypeId = 3
		AND cr.CustomerId = p_ParentCustomerId;

		INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
		SELECT 							      ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM UserAccount ua 
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;
    
    END IF;

    
    IF p_CustomerTypeId = 3 THEN
    
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
              
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);          
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);       
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);           
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      11,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Preferred
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      12,            '0',       0, UTC_TIMESTAMP(),             p_UserId);   -- Jurisdiction Type Limited
   
        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                    		0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
            
        SELECT LAST_INSERT_ID() INTO v_LocationId;
                
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
                    
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
            
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
        
    ELSE         
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   
    END IF;
    
    
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
            
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,    0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration,0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration,    0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration,    0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange,  0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 2000, p_IsOnlineRate,         0, UTC_TIMESTAMP(), p_UserId);
               
		
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'Communication Alert Default',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,          		  12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;        
    END IF;
    
    COMMIT;     
	
END$$
DELIMITER ;



-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateLocation`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateLocation`(
    IN p_CustomerName VARCHAR(25),
    IN p_LocationName VARCHAR(25),
    IN p_Description VARCHAR(80),
    IN p_IsParent TINYINT UNSIGNED,
    IN p_ParentLocation VARCHAR(25),
    IN p_UserId MEDIUMINT UNSIGNED,
	IN p_NumberOfSpaces MEDIUMINT UNSIGNED,
	IN p_TargetMonthlyRevenueAmount MEDIUMINT UNSIGNED,
	OUT v_NewLocationId MEDIUMINT
)
BEGIN
    DECLARE v_CustomerId           INT UNSIGNED;
    DECLARE v_ParentLocationId     INT UNSIGNED;

    SET v_CustomerId = (SELECT Id From Customer WHERE Name=p_CustomerName);
    
    IF p_ParentLocation = NULL THEN
        SET v_ParentLocationId = NULL;
    ELSE
        SET v_ParentLocationId = (SELECT l.Id From Location l WHERE CustomerId=v_CustomerId AND Name=p_ParentLocation AND IsDeleted=0);
    END IF;

    INSERT INTO `Location`
    (   
        `CustomerId`, 
        `ParentLocationId`,
        `Name`,
        `NumberOfSpaces`,
        `TargetMonthlyRevenueAmount`,
        `Description`,
        `IsParent`,
        `IsUnassigned`,
        `IsDeleted`,
        `VERSION`,
        `LastModifiedGMT`,
        `LastModifiedByUserId`
    )
    VALUES
    (
        v_CustomerId,
        v_ParentLocationId,
        p_LocationName,
        p_NumberOfSpaces,
        p_TargetMonthlyRevenueAmount,
        p_Description,
        p_IsParent,
        0,
        0,
        0,
        UTC_TIMESTAMP(),
        1
    );    
    
    IF p_IsParent = 0 THEN
        
        SET v_NewLocationId = (SELECT l.Id From Location l WHERE CustomerId=v_CustomerId AND Name=p_LocationName AND IsDeleted=0);
        
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            1, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            2, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            3, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
           v_NewLocationId, -- p_LocationId
            4, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            5, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            6, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );
        CALL sp_InsertLocationDayAndOpen
        (
            v_NewLocationId, -- p_LocationId
            7, -- p_DayOfWeekId
            0, -- p_OpenQuarterHourId
            95, -- p_CloseQuarterHourId
            1, -- p_IsOpen24Hours
            0, -- p_IsClosed
            1 -- p_UserId
        );

    END IF;

END$$
DELIMITER ;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreatePayStation`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreatePayStation`(
    IN p_CustomerName VARCHAR(25),
    IN p_LocationName VARCHAR(25),
    IN p_SerialNumber VARCHAR(20),
    IN p_UserId MEDIUMINT UNSIGNED,
	IN p_TimeZone VARCHAR(20),
    IN p_Latitude DECIMAL(18,14),
    IN p_Longitude DECIMAL(18,14),
	IN p_Route1Id MEDIUMINT,
	IN p_Route2Id MEDIUMINT
)
BEGIN

    DECLARE v_CustomerId            INT UNSIGNED;
    DECLARE v_LocationId            INT UNSIGNED;
    DECLARE v_PayStationType        INT UNSIGNED;    
	DECLARE v_PointOfSaleId			INT UNSIGNED;

    SET v_CustomerId = (SELECT Id From Customer WHERE Name=p_CustomerName);
    SET v_LocationId = (SELECT Id From Location WHERE CustomerId=v_CustomerId AND Name=p_LocationName AND IsDeleted=0);
    
    SET v_PayStationType = ASCII(p_SerialNumber) - ASCII('0');
    
    CALL sp_InsertPointOfSale
    (
        v_CustomerId, -- p_CustomerId
        v_PayStationType, -- p_PaystationTypeId (LUKE)
        1, -- p_ModemTypeId (Unknown)
        p_SerialNumber, -- p_SerialNumber
        UTC_TIMESTAMP(), -- p_ProvisionedGMT
        1, -- p_UserId
        0, -- p_PaystationId_old
        0 -- p_PointOfSaleId_old
    );
    
    SET v_PointOfSaleId = (SELECT Id FROM PointOfSale WHERE SerialNumber=p_SerialNumber);

    UPDATE PointOfSale
    SET
        `LocationId` = v_LocationId,
        `VERSION` = 1,
        `LastModifiedGMT` = UTC_TIMESTAMP(),
		`Latitude` = p_Latitude,
		`Longitude` = p_Longitude
    WHERE Id=v_PointOfSaleId;

    -- Activate the Pay Station
    UPDATE POSStatus
    SET
        `IsActivated` = 1,
        `IsActivatedGMT` = UTC_TIMESTAMP(),
        `VERSION` = 1,
        `LastModifiedGMT` = UTC_TIMESTAMP()
    WHERE PointOfSaleId=v_PointOfSaleId;

	INSERT RoutePOS (RouteId, PointOfSaleId) VALUES (p_Route1Id, v_PointOfSaleId);
	INSERT RoutePOS (RouteId, PointOfSaleId) VALUES (p_Route2Id, v_PointOfSaleId);

	Call sp_Preview_AddPaystationToAutomation(p_SerialNumber, v_CustomerId, SUBSTRING(p_SerialNumber, -4), p_TimeZone);

END$$
DELIMITER ;

-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `sp_Preview_AddPaystationToAutomation`;
DELIMITER $$

CREATE PROCEDURE `sp_Preview_AddPaystationToAutomation` (
	IN p_PaystationId VARCHAR(20),
	IN p_CustomerId MEDIUMINT,
	IN p_PaystationIdentifier VARCHAR(4),
	IN p_TimeZone VARCHAR(20))
BEGIN

	INSERT PreviewPaystation (SerialNumber, CustomerId, PaystationId, TimeZone)
	VALUES 	(P_PaystationId, p_CustomerId, p_PaystationIdentifier, p_TimeZone);

END$$
DELIMITER ;



-- -----------------------------------------------------
--  `sp_Preview_CreateRoute`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateRoute`;
DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateRoute` (
	IN p_CustomerId MEDIUMINT,
	IN p_RouteTypeId TINYINT,
	IN p_LocationId MEDIUMINT,
	OUT v_NewRouteId MEDIUMINT
)
BEGIN

	INSERT INTO Route (CustomerId, RouteTypeId, Name, VERSION, LastModifiedGMT, LastModifiedByUserId) 
	VALUES (p_CustomerId, p_RouteTypeId, CONCAT((SELECT Name FROM Location WHERE Id = p_LocationId),' ',(SELECT Name FROM RouteType WHERE Id = p_RouteTypeId)), 0, now(), 1);
	
	SELECT last_insert_id() INTO v_NewRouteId;

END$$
DELIMITER ;


-- -----------------------------------------------------
--  `sp_Preview_InsertTransaction`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertTransaction`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertTransaction`(
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20) ,
  IN p_Number INT UNSIGNED ,
  IN p_LotNumber VARCHAR(20) , 
  IN p_LicensePlateNo VARCHAR(15) ,
  IN p_AddTimeNum INT UNSIGNED ,
  IN p_StallNumber MEDIUMINT(8) UNSIGNED ,
  IN p_Type VARCHAR(30) ,
  IN p_ParkingTimePurchased MEDIUMINT ,
  IN p_OriginalAmount MEDIUMINT UNSIGNED ,
  IN p_CashPaid MEDIUMINT UNSIGNED ,
  IN p_CardPaid MEDIUMINT UNSIGNED ,
  IN p_NumberBillsAccepted MEDIUMINT UNSIGNED ,
  IN p_NumberCoinsAccepted MEDIUMINT UNSIGNED ,
  IN p_BillCol MEDIUMINT UNSIGNED ,
  IN p_CoinCol MEDIUMINT UNSIGNED ,
  IN p_CardData VARCHAR(100) ,
  IN p_IsRefundSlip TINYINT(1) UNSIGNED ,
  IN p_RateName varchar(20) ,
  IN p_RateId MEDIUMINT UNSIGNED ,
  IN p_RateValue MEDIUMINT UNSIGNED 
)
BEGIN

	DECLARE v_PreviewTransactionId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewTransaction (Number, LotNumber, LicensePlateNo, AddTimeNum, StallNumber, Type, ParkingTimePurchased, OriginalAmount, CashPaid, CardPaid, NumberBillsAccepted, NumberCoinsAccepted, BillCol, CoinCol, CardData, IsRefundSlip, RateName, RateId, RateValue)
	VALUES (p_Number, p_LotNumber, p_LicensePlateNo, p_AddTimeNum, p_StallNumber, p_Type, p_ParkingTimePurchased, p_OriginalAmount, p_CashPaid, p_CardPaid, p_NumberBillsAccepted, p_NumberCoinsAccepted, p_BillCol, p_CoinCol, p_CardData, p_IsRefundSlip, p_RateName, p_RateId, p_RateValue);

	SELECT last_insert_id() INTO v_PreviewTransactionId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Transaction' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewTransactionId);

END //
DELIMITER ;


-- -----------------------------------------------------
--  `sp_Preview_InsertEvent`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertEvent`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertEvent`(
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20) ,
  IN p_Type VARCHAR(40) ,
  IN p_Action VARCHAR(40) ,
  IN p_Information VARCHAR(40)
)
BEGIN

	DECLARE v_PreviewEventId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewEvent (Type, Action, Information)
	VALUES (p_Type, p_Action, p_Information);

	SELECT last_insert_id() INTO v_PreviewEventId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Event' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewEventId);

END //
DELIMITER ;


-- -----------------------------------------------------
--  `sp_Preview_InsertCollection`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_InsertCollection`;

DELIMITER //
CREATE PROCEDURE `sp_Preview_InsertCollection`(
  IN p_DayOfWeek MEDIUMINT UNSIGNED ,
  IN p_MinuteOfDay MEDIUMINT UNSIGNED ,
  IN p_PaystationId VARCHAR(20),

  IN p_Type VARCHAR(20), -- Coin/Bill
  IN p_LotNumber VARCHAR(20),
  IN p_MachineNumber VARCHAR(4),
  IN p_StartDate MEDIUMINT UNSIGNED ,
  IN p_EndDate MEDIUMINT UNSIGNED ,  
  IN p_StartTransNumber INT UNSIGNED ,
  IN p_EndTransNumber INT UNSIGNED ,  
  IN p_TicketCount MEDIUMINT UNSIGNED ,
  IN p_BillCount50 SMALLINT UNSIGNED ,
  IN p_BillCount20 SMALLINT UNSIGNED ,
  IN p_BillCount10 SMALLINT UNSIGNED ,
  IN p_BillCount05 SMALLINT UNSIGNED ,
  IN p_BillCount02 SMALLINT UNSIGNED ,
  IN p_BillCount01 SMALLINT UNSIGNED ,
  IN p_CoinCount200 SMALLINT UNSIGNED ,
  IN p_CoinCount100 SMALLINT UNSIGNED ,
  IN p_CoinCount025 SMALLINT UNSIGNED ,
  IN p_CoinCount010 SMALLINT UNSIGNED ,
  IN p_CoinCount005 SMALLINT UNSIGNED 
)
BEGIN

	DECLARE v_PreviewCollectionId MEDIUMINT UNSIGNED;
	DECLARE v_PreviewRequestTypeId MEDIUMINT UNSIGNED;

	INSERT PreviewCollection (Type , LotNumber ,MachineNumber ,StartDate ,EndDate , StartTransNumber ,EndTransNumber ,TicketCount ,CoinCount005 ,CoinCount010 ,CoinCount025 ,CoinCount100 ,CoinCount200 ,BillCount01 ,BillCount02 ,BillCount05 ,BillCount10 ,BillCount20 ,BillCount50 )
						VALUES (p_Type , p_LotNumber ,p_MachineNumber ,p_StartDate ,p_EndDate , p_StartTransNumber ,p_EndTransNumber ,p_TicketCount ,p_CoinCount005 ,p_CoinCount010 ,p_CoinCount025 ,p_CoinCount100 ,p_CoinCount200 ,p_BillCount01 ,p_BillCount02 ,p_BillCount05 ,p_BillCount10 ,p_BillCount20 ,p_BillCount50 );

	SELECT last_insert_id() INTO v_PreviewCollectionId;
	SELECT Id from PreviewRequestType prt WHERE Name like 'Collection' INTO v_PreviewRequestTypeId;

	INSERT PreviewRequest(DayOfWeek, MinuteOfDay, PaystationId, RequestTypeId, RequestId)
	VALUES (p_DayOfWeek, p_MinuteOfDay, p_PaystationId, v_PreviewRequestTypeId, v_PreviewCollectionId);

END //
DELIMITER ;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateAdminUser`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateAdminUser`(
	IN p_CustomerName VARCHAR(25),
	IN p_FirstName VARCHAR(25),
	IN p_LastName VARCHAR(25),
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16)
	)
BEGIN
      
	DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerRoleId MEDIUMINT UNSIGNED;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;     
    
    SELECT Id FROM Customer WHERE Name = p_CustomerName INTO v_CustomerId;    
	
	SELECT RoleId FROM CustomerRole WHERE CustomerId = v_CustomerId INTO v_CustomerRoleId;
    
    START TRANSACTION;
        
	INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,   FirstName,   LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES       (v_CustomerId,                1,            NULL, p_UserName, p_FirstName, p_LastName, p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

    SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, v_CustomerRoleId,       0, UTC_TIMESTAMP(),             p_UserId);   
                
    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);
    
    COMMIT;     
	
END$$
DELIMITER ;




-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateSystemAdminUser`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateSystemAdminUser`(
	IN p_FirstName VARCHAR(25),
	IN p_LastName VARCHAR(25),
	IN p_UserName VARCHAR(765), 
	IN p_Password VARCHAR(128), 
	IN p_UserId MEDIUMINT UNSIGNED, 
	IN p_PasswordSalt VARCHAR(16)
	)
BEGIN
      
	DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerId MEDIUMINT UNSIGNED;
    DECLARE v_CustomerRoleId MEDIUMINT UNSIGNED;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;   
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS v_UserAccountId; END;     
    
    SELECT 1 INTO v_CustomerId;    
	
	SELECT RoleId FROM CustomerRole WHERE CustomerId = v_CustomerId INTO v_CustomerRoleId;
    
    START TRANSACTION;
        
	INSERT UserAccount (CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,   FirstName,   LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES       (v_CustomerId,                1,            NULL, p_UserName, p_FirstName, p_LastName, p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

    SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, v_CustomerRoleId,       0, UTC_TIMESTAMP(),             p_UserId);   
    
    COMMIT;     
	
END$$
DELIMITER ;




-- -----------------------------------------------------
-- Procedure `sp_BuildServer_InsertPOSCollection`
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_BuildServer_InsertPOSCollection`;

DELIMITER //
CREATE PROCEDURE `sp_BuildServer_InsertPOSCollection`(
  IN p_CustomerId MEDIUMINT UNSIGNED 
)
BEGIN

	
	DECLARE lv_PointOfSaleId,v_CollectorId MEDIUMINT UNSIGNED;
	
	DECLARE v_CurrentUTCTime DATETIME ;
	
	
	DECLARE v_UTCTimeMinus15 DATETIME ;
	DECLARE v_UTCTimeMinus30 DATETIME ;
	DECLARE v_UTCTimeMinus60 DATETIME ;
	

	DECLARE v_UTCTimePlus15 DATETIME ;
	DECLARE v_UTCTimePlus30 DATETIME ;
	DECLARE v_UTCTimePlus60 DATETIME ;
	declare NO_DATA int default 0;
	declare idmaster_pos,indexm_pos int;
	declare lv_PaystationSettingId MEDIUMINT UNSIGNED;
	-- Added on Aug 26
	DECLARE lv_POSCollectionId INT UNSIGNED DEFAULT 0;
	
	declare c1 cursor  for
	select Id
	from PointOfSale
	where CustomerId = p_CustomerId;


	declare continue handler for not found set NO_DATA=-1;
	
	set indexm_pos =0;
	set idmaster_pos =0;
	
	-- SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY) into v_CurrentUTCTime;
	-- select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
	-- select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
	-- select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
	-- select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
	-- select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
	-- select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
	
	select Id into lv_PaystationSettingId from PaystationSetting where CustomerId = p_CustomerId limit 1;
	set v_CollectorId = NULL;
	open c1;
	set idmaster_pos = (select FOUND_ROWS());

	while indexm_pos < idmaster_pos do
	fetch c1 into lv_PointOfSaleId ;
	
		SELECT Id into v_CollectorId from UserAccount where UserName like '%digitalcollect%' and CustomerId = p_CustomerId limit 1;
		
		-- All
				
		IF (indexm_pos = 0) THEN
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		ELSE
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 0 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		END IF;
		
		UPDATE POSBalance SET LastCollectionGMT = v_UTCTimePlus15, LastCashCollectionGMT = v_UTCTimePlus15 WHERE PointOfSaleId = lv_PointOfSaleId;
		INSERT INTO POSCollection (CustomerId,	PointOfSaleId,		CollectionTypeId,	PaystationSettingId,	ReportNumber,	StartGMT,		  EndGMT,			StartTicketNumber,	EndTicketNumber,	NextTicketNumber,		StartTransactionNumber,EndTransactionNumber,	TicketsSold,	CoinTotalAmount,	BillTotalAmount,	CardTotalAmount,	CoinCount05,CoinCount10,CoinCount25,CoinCount100,CoinCount200,BillCount1,BillCount2,BillCount5,BillCount10,BillCount20,BillCount50,BillAmount1,BillAmount2,BillAmount5,BillAmount10,BillAmount20,BillAmount50,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,ValueCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssuedAmount,ExcessPaymentAmount,OverfillAmount,PatrollerTicketsSold,RefundIssuedAmount,ReplenishedAmount,TestDispensedChanger,TestDispensedHopper1,TestDispensedHopper2,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount,CreatedGMT) VALUES 
								(p_CustomerId,	lv_PointOfSaleId,	0,					lv_PaystationSettingId,					1,				v_UTCTimeMinus15,  v_UTCTimePlus15,	0,					0,					33,						0,						0,						6,				1000,				17000,				623,				0,0,0,0,0,0,0,0,0,0,0,300,200,1500,3000,2000,10000,123,0,0,0,225,0,275,0,0,0,0,0,200,0,0,1,0,0,0,0,0,25,100,7175,40800,75,100,0,0,0,0,0,0,0,0,0,0,v_CurrentUTCTime);
	
		-- Get POSCollection.Id
		SELECT LAST_INSERT_ID() INTO lv_POSCollectionId;
	
		IF (v_CollectorId IS NOT NULL) THEN
			
			INSERT INTO POSCollectionUser(PointOfSaleId,	CollectionTypeId,UserAccountId,	MobileStartGMT,MobileEndGMT,POSCollectionId,MobileReceivedGMT) VALUES
									 (lv_PointOfSaleId, 	0,				 v_CollectorId,	v_CurrentUTCTime,v_CurrentUTCTime,lv_POSCollectionId, v_CurrentUTCTime);
									
		END IF;
		-- Bill
		
		
		
		IF (indexm_pos = 0) THEN
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		ELSE
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 0 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		END IF;
		
		UPDATE POSBalance SET LastBillCollectionGMT = v_UTCTimePlus30 WHERE PointOfSaleId = lv_PointOfSaleId;
		INSERT INTO POSCollection (CustomerId,	PointOfSaleId,		CollectionTypeId,	PaystationSettingId,	ReportNumber,	StartGMT,		  EndGMT,			StartTicketNumber,	EndTicketNumber,	NextTicketNumber,		StartTransactionNumber,EndTransactionNumber,	TicketsSold,	CoinTotalAmount,	BillTotalAmount,	CardTotalAmount,	CoinCount05,CoinCount10,CoinCount25,CoinCount100,CoinCount200,BillCount1,BillCount2,BillCount5,BillCount10,BillCount20,BillCount50,BillAmount1,BillAmount2,BillAmount5,BillAmount10,BillAmount20,BillAmount50,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,ValueCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssuedAmount,ExcessPaymentAmount,OverfillAmount,PatrollerTicketsSold,RefundIssuedAmount,ReplenishedAmount,TestDispensedChanger,TestDispensedHopper1,TestDispensedHopper2,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount,CreatedGMT) VALUES 
								(p_CustomerId,	lv_PointOfSaleId,	1,					lv_PaystationSettingId,					1,				v_UTCTimePlus30,  v_UTCTimePlus30,	0,					0,					33,						0,						0,						6,				1000,				17000,				623,				0,0,0,0,0,0,0,0,0,0,0,300,200,1500,3000,2000,10000,123,0,0,0,225,0,275,0,0,0,0,0,200,0,0,1,0,0,0,0,0,25,100,7175,40800,75,100,0,0,0,0,0,0,0,0,0,0,v_CurrentUTCTime);
		
		-- Get POSCollection.Id
		SELECT LAST_INSERT_ID() INTO lv_POSCollectionId;
		
		IF (v_CollectorId IS NOT NULL) THEN
			
			INSERT INTO POSCollectionUser(PointOfSaleId,	CollectionTypeId,UserAccountId,	MobileStartGMT,MobileEndGMT,POSCollectionId,MobileReceivedGMT) VALUES
									 (lv_PointOfSaleId, 	1,				 v_CollectorId,	v_CurrentUTCTime,v_CurrentUTCTime,lv_POSCollectionId, v_CurrentUTCTime);
									
		END IF;
		
		-- Coin
		
		
		IF (indexm_pos = 0) THEN
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		ELSE
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 0 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		END IF;
		
		UPDATE POSBalance SET LastCoinCollectionGMT = v_UTCTimePlus60 WHERE PointOfSaleId = lv_PointOfSaleId;
		INSERT INTO POSCollection (CustomerId,	PointOfSaleId,		CollectionTypeId,	PaystationSettingId,	ReportNumber,	StartGMT,		  EndGMT,			StartTicketNumber,	EndTicketNumber,	NextTicketNumber,		StartTransactionNumber,EndTransactionNumber,	TicketsSold,	CoinTotalAmount,	BillTotalAmount,	CardTotalAmount,	CoinCount05,CoinCount10,CoinCount25,CoinCount100,CoinCount200,BillCount1,BillCount2,BillCount5,BillCount10,BillCount20,BillCount50,BillAmount1,BillAmount2,BillAmount5,BillAmount10,BillAmount20,BillAmount50,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,ValueCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssuedAmount,ExcessPaymentAmount,OverfillAmount,PatrollerTicketsSold,RefundIssuedAmount,ReplenishedAmount,TestDispensedChanger,TestDispensedHopper1,TestDispensedHopper2,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount,CreatedGMT) VALUES 
								(p_CustomerId,	lv_PointOfSaleId,	2,					lv_PaystationSettingId,					1,				v_UTCTimePlus60,  v_UTCTimePlus60,	0,					0,					33,						0,						0,						6,				1000,				17000,				623,				0,0,0,0,0,0,0,0,0,0,0,300,200,1500,3000,2000,10000,123,0,0,0,225,0,275,0,0,0,0,0,200,0,0,1,0,0,0,0,0,25,100,7175,40800,75,100,0,0,0,0,0,0,0,0,0,0,v_CurrentUTCTime);
		
		-- Get POSCollection.Id
		SELECT LAST_INSERT_ID() INTO lv_POSCollectionId;
		
		IF (v_CollectorId IS NOT NULL) THEN
			
			INSERT INTO POSCollectionUser(PointOfSaleId,	CollectionTypeId,UserAccountId,	MobileStartGMT,MobileEndGMT,POSCollectionId,MobileReceivedGMT) VALUES
									 (lv_PointOfSaleId, 	2,				 v_CollectorId,	v_CurrentUTCTime,v_CurrentUTCTime,lv_POSCollectionId, v_CurrentUTCTime);
									
		END IF;
		
		-- Card
		
		
		IF (indexm_pos = 0) THEN
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		ELSE
			SELECT DATE_SUB(UTC_TIMESTAMP(), INTERVAL 0 DAY) into v_CurrentUTCTime;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimeMinus15;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimeMinus30;
			select DATE_SUB(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimeMinus60;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 15 MINUTE) into v_UTCTimePlus15;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 30 MINUTE) into v_UTCTimePlus30;
			select DATE_ADD(v_CurrentUTCTime, INTERVAL 60 MINUTE) into v_UTCTimePlus60;
		END IF;
		
		UPDATE POSBalance SET LastCardCollectionGMT = v_UTCTimePlus60 WHERE PointOfSaleId = lv_PointOfSaleId;
		INSERT INTO POSCollection (CustomerId,	PointOfSaleId,		CollectionTypeId,	PaystationSettingId,	ReportNumber,	StartGMT,		  EndGMT,			StartTicketNumber,	EndTicketNumber,	NextTicketNumber,		StartTransactionNumber,EndTransactionNumber,	TicketsSold,	CoinTotalAmount,	BillTotalAmount,	CardTotalAmount,	CoinCount05,CoinCount10,CoinCount25,CoinCount100,CoinCount200,BillCount1,BillCount2,BillCount5,BillCount10,BillCount20,BillCount50,BillAmount1,BillAmount2,BillAmount5,BillAmount10,BillAmount20,BillAmount50,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,ValueCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssuedAmount,ExcessPaymentAmount,OverfillAmount,PatrollerTicketsSold,RefundIssuedAmount,ReplenishedAmount,TestDispensedChanger,TestDispensedHopper1,TestDispensedHopper2,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount,CreatedGMT) VALUES 
								(p_CustomerId,	lv_PointOfSaleId,	3,					lv_PaystationSettingId,					1,				v_UTCTimePlus60,  v_UTCTimePlus60,	0,					0,					33,						0,						0,						6,				1000,				17000,				623,				0,0,0,0,0,0,0,0,0,0,0,300,200,1500,3000,2000,10000,123,0,0,0,225,0,275,0,0,0,0,0,200,0,0,1,0,0,0,0,0,25,100,7175,40800,75,100,0,0,0,0,0,0,0,0,0,0,v_CurrentUTCTime);
									
		-- Get POSCollection.Id
		SELECT LAST_INSERT_ID() INTO lv_POSCollectionId;
		
		IF (v_CollectorId IS NOT NULL) THEN
			
			INSERT INTO POSCollectionUser(PointOfSaleId,	CollectionTypeId,UserAccountId,	MobileStartGMT,MobileEndGMT,POSCollectionId,MobileReceivedGMT) VALUES
									 (lv_PointOfSaleId, 	3,				 v_CollectorId,	v_CurrentUTCTime,v_CurrentUTCTime,lv_POSCollectionId, v_CurrentUTCTime);
									
		END IF;
		
	set indexm_pos = indexm_pos +1;
	end while;

	close c1;


END //
DELIMITER ;



-- --------------------------------------------------------------------------------
-- sp_Preview_CreateMoblieLicense
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_Preview_CreateMoblieLicense`;

DELIMITER $$

CREATE PROCEDURE `sp_Preview_CreateMoblieLicense`(
    IN p_CustomerName VARCHAR(25),
    IN p_ApplicationTypeId MEDIUMINT,
	IN p_LicenseCount MEDIUMINT
)
BEGIN

	DECLARE v_CustomerId MEDIUMINT;
	DECLARE v_LicenseCntr MEDIUMINT;
	DECLARE v_SubscriptionTypeId MEDIUMINT;

	SET v_LicenseCntr = p_LicenseCount;
	
	SELECT Id INTO v_CustomerId FROM Customer WHERE `Name` = p_CustomerName;
	WHILE v_LicenseCntr > 0 DO
		INSERT INTO MobileLicense(CustomerId, MobileApplicationTypeId, MobileLicenseStatusTypeId) VALUES(v_CustomerId, p_ApplicationTypeId, 1);
		SET v_LicenseCntr = v_LicenseCntr - 1;
	END WHILE;
	
	CASE p_ApplicationTypeId
		WHEN 1 THEN SET v_SubscriptionTypeId = 1300;
	END CASE;
	
	UPDATE CustomerSubscription SET LicenseCount = p_LicenseCount, LicenseUsed = 0 WHERE CustomerId = v_CustomerId AND SubscriptionTypeId = v_SubscriptionTypeId;

    COMMIT;

END$$
DELIMITER ;



-- --------------------------------------------------------------------------------
-- sp_BuildServer_InsertPurchase
-- --------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `sp_BuildServer_InsertPurchase`;

DELIMITER $$

CREATE PROCEDURE `sp_BuildServer_InsertPurchase`(
    IN p_CustomerName VARCHAR(25),    
	IN p_TransactionType tinyint(3),
	IN p_PaymentType tinyint(3),
	IN p_LocationName varchar(25),
	IN p_CashAmount MEDIUMINT,
	IN p_CardAmount MEDIUMINT
)
BEGIN

	DECLARE v_CustomerId, v_LocationId MEDIUMINT;
	DECLARE v_PointOfSaleId,v_PaystationSettingId,v_UnifiedRateId MEDIUMINT;
	
	SELECT Id INTO v_CustomerId FROM Customer WHERE `Name` = p_CustomerName;
	SELECT Id INTO v_LocationId FROM Location WHERE  CustomerId =v_CustomerId and Name = p_LocationName;
		
	SELECT Id INTO v_PointOfSaleId FROM PointOfSale WHERE CustomerId = v_CustomerId AND LocationId = v_LocationId LIMIT 1; -- AND SerialNumber = p_SerialNumber ;
	SELECT Id INTO v_PaystationSettingId FROM PaystationSetting where CustomerId = v_CustomerId limit 1;
	SELECT Id INTO v_UnifiedRateId FROM UnifiedRate where CustomerId = v_CustomerId limit 1;
	
	IF (p_PaymentType =1 ) THEN  -- CASH
	
		INSERT INTO Purchase (CustomerId ,	PointOfSaleId, PurchaseGMT,PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,
						ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT)
				VALUES   (v_CustomerId, v_PointOfSaleId, UTC_TIMESTAMP(), 1,p_TransactionType,	p_PaymentType,	v_LocationId,	v_PaystationSettingId, v_UnifiedRateId, NULL, p_CashAmount,p_CashAmount,0,
						0, 				p_CashAmount,	p_CashAmount,	0,	0,	p_CashAmount,p_CashAmount,15,0,0,0,UTC_TIMESTAMP()) ;
	END IF;
	
	IF (p_PaymentType =4 ) THEN  -- SMART CARD
	
		INSERT INTO Purchase (CustomerId ,	PointOfSaleId, PurchaseGMT,PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,
						ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,	BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,			CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT)
				VALUES   (v_CustomerId, v_PointOfSaleId, UTC_TIMESTAMP(), 2,p_TransactionType,	p_PaymentType,	v_LocationId,	v_PaystationSettingId, v_UnifiedRateId, NULL, p_CardAmount,p_CardAmount,0,
						0, 				0,	0,								0,	p_CardAmount,	p_CardAmount,p_CardAmount,									0,0,0,0,UTC_TIMESTAMP()) ;
	END IF;
	
	IF (p_PaymentType =6 ) THEN  -- CASH/SMART CARD
	
		INSERT INTO Purchase (CustomerId ,	PointOfSaleId, PurchaseGMT,PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,
						ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,	BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,			CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT)
				VALUES   (v_CustomerId, v_PointOfSaleId, UTC_TIMESTAMP(), 3,p_TransactionType,	p_PaymentType,	v_LocationId,	v_PaystationSettingId, v_UnifiedRateId, NULL, p_CashAmount+p_CardAmount,p_CashAmount+p_CardAmount,0,
						0, 				p_CashAmount,	0,								p_CashAmount,	p_CardAmount,	p_CashAmount+p_CardAmount,p_CashAmount+p_CardAmount,					0,0,0,0,UTC_TIMESTAMP()) ;
	END IF;
	
END$$
DELIMITER ;

-- --------------------------------------------------------------------------------
-- sp_InsertTestDataForCardProcessing
-- --------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_InsertTestDataForCardProcessing;

delimiter //

CREATE PROCEDURE sp_InsertTestDataForCardProcessing( P_CustomerId MEDIUMINT , P_POSID MEDIUMINT, P_ProcessorTransactionTypeId INT, P_Amount INT, P_Date Datetime)

 BEGIN

	DECLARE v_PurchaseId BIGINT UNSIGNED;
	
	INSERT INTO Purchase(CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,
							PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,
							CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,
							IsOffline,IsRefundSlip,CreatedGMT)
	VALUES (P_CustomerId, P_POSID, P_Date, 1,2,2,3,
							(select Id from PaystationSetting where CustomerId = P_CustomerId limit 1),(select Id from UnifiedRate where CustomerId = P_CustomerId limit 1),NULL,P_Amount,P_Amount,0,0,
							0,0,0,P_Amount,P_Amount,P_Amount,0,0,
							0,0,now()) ;

	SELECT LAST_INSERT_ID() INTO v_PurchaseId; 
	
	INSERT INTO ProcessorTransaction (PurchaseId,PreAuthId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,
										 MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,
										ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,
										IsRFID,CreatedGMT)
	VALUES (v_PurchaseId,NULL,P_POSID,P_Date,1,P_ProcessorTransactionTypeId,
										 (Select Id from MerchantAccount where CustomerId = P_CustomerId Limit 1),P_Amount,'MasterCard',1234,1234,P_Date,
										3,1,1,1,1,0,
										0,now()) ;

END//
delimiter ;



DROP PROCEDURE IF EXISTS sp_Preview_CreateMerchantAccount;

delimiter //
Create Procedure sp_Preview_CreateMerchantAccount()

BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_CustomerId MEDIUMINT UNSIGNED ;

DECLARE  C_Customer CURSOR FOR
SELECT Id FROM Customer
WHERE Id NOT IN
( SELECT DISTINCT CustomerID from MerchantAccount)
ORDER BY Id;

declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

open C_Customer;
	set idmaster_pos = (select FOUND_ROWS());

	while indexm_pos < idmaster_pos do
	fetch C_Customer into lv_CustomerId;
	 
	 INSERT INTO MerchantAccount (CustomerId, 		ProcessorId,MerchantStatusTypeId, Name,			Field1,   Field2,	CloseQuarterOfDay,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) VALUES
								 (lv_CustomerId,	16,			2,					 'TestPreviewAccount','Field1', 'Field2', 80,					0,				0,			0,		UTC_TIMESTAMP(),	1); 
			
	set indexm_pos = indexm_pos +1;
	end while;

close C_Customer;
	

END//
delimiter ;

 								   
--
 								   
DROP PROCEDURE IF EXISTS sp_Preview_InsertCardTransactionData;

delimiter //
Create Procedure sp_Preview_InsertCardTransactionData( P_PurchaseGMT datetime, P_MinuteOfDay MEDIUMINT)

BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos,idmaster_pos_Card,indexm_pos_Card,lv_AddTimeNum,lv_LicencePlateId int(4);
declare lv_Number int unsigned;
declare lv_LotNumber,lv_RateName  varchar(20) ;
declare lv_TransactionTypeId,lv_PaymentTypeId,lv_ProcessorTransactionTypeId TINYINT unsigned;
declare lv_LicensePlateNo varchar(15) ;
declare lv_PointOfSaleId, lv_CustomerId, lv_LocationId,lv_StallNumber,lv_ParkingTimePurchased,lv_OriginalAmount,lv_CardPaid,lv_PaystationSettingId,lv_UnifiedRateId,lv_MerchantAccountId MEDIUMINT UNSIGNED;
declare lv_PurchaseGMT datetime;
declare lv_PurchaseId,lv_ProcessorTransactionId BIGINT UNSIGNED;
declare lv_LocationName varchar(25);
declare lv_TimeZone varchar(20);
declare lv_RequestId mediumint unsigned ;
declare lv_MinuteOfDay mediumint unsigned ;
declare lv_MinuteOfDay_GMT mediumint unsigned ;

DECLARE  C_PreviewRequest CURSOR FOR
select C.Id, B.CustomerId, C.LocationId, A.MinuteOfDay ,A.RequestId, B.TimeZone  
from PreviewRequest A inner join PreviewPaystation B
on A.PaystationId = B.PaystationId 
inner join PointOfSale C on B.CustomerId = C.CustomerId and C.SerialNumber = B.SerialNumber
where A.RequestTypeId = 1 and A.DayOfWeek = dayofweek(P_PurchaseGMT) 
and A.MinuteOfDay = P_MinuteOfDay;


declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

-- Create MerchantAccount if doesnot exists

-- select timestampdiff( minute, date(convert_tz(now(),'Canada/Pacific','GMT')), convert_tz(now(),'Canada/Pacific','GMT')) INTO lv_MinuteOfDay_GMT;

CALL sp_Preview_CreateMerchantAccount();

-- select P_PurchaseGMT ;
open C_PreviewRequest;
	set idmaster_pos = (select FOUND_ROWS());

	while indexm_pos < idmaster_pos do
	fetch C_PreviewRequest into lv_PointOfSaleId, lv_CustomerId, lv_LocationId, lv_MinuteOfDay, lv_RequestId, lv_TimeZone;
	   -- select lv_RequestId ;
	   SET lv_Number = NULL;
 	   SET lv_LotNumber= NULL;
	   SET lv_TransactionTypeId = NULL;
	   set lv_ProcessorTransactionTypeId = NULL;
	   SET lv_PaymentTypeId = NULL;
	   SET lv_LicensePlateNo = NULL;
	   SET lv_AddTimeNum = NULL;
	   SET lv_StallNumber = NULL;
	   SET lv_ParkingTimePurchased = NULL;
	   SET lv_OriginalAmount= NULL;
	   SET lv_RateName = NULL;
	   
		SELECT Number, 		LotNumber,		TransactionTypeId,		ProcessorTransactionTypeId, 	PaymentTypeId,		LicensePlateNo,		AddTimeNum,		StallNumber,	ParkingTimePurchased,	OriginalAmount,RateName 
		into  lv_Number, 	lv_LotNumber,	lv_TransactionTypeId,	lv_ProcessorTransactionTypeId,	lv_PaymentTypeId,	lv_LicensePlateNo,	lv_AddTimeNum,	lv_StallNumber,	lv_ParkingTimePurchased,lv_OriginalAmount,lv_RateName  
		FROM PreviewCardTransaction WHERE  Id = lv_RequestId ;
		
		 -- Purchase
		 -- Permit
		 -- ProcessorTransaction
		 -- PaymentCard
		 SET lv_PaystationSettingId = NULL ;
		 
		 SELECT ID into lv_PaystationSettingId from PaystationSetting WHERE CustomerId = lv_CustomerId  LIMIT 1;
		 -- SELECT ID into lv_UnifiedRateId from UnifiedRate WHERE  CustomerId = lv_CustomerId  LIMIT 1;
		 -- EMS-6309 Added on Feb 04 
		 SELECT ifnull(max(ID),0) into lv_UnifiedRateId from UnifiedRate WHERE  CustomerId = lv_CustomerId and Name = lv_RateName LIMIT 1;
		 
		 IF (lv_UnifiedRateId = 0) THEN
		 
			INSERT INTO UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES
									(lv_CustomerId, lv_RateName, UTC_TIMESTAMP(), 1) ;
									
			SELECT LAST_INSERT_ID() INTO lv_UnifiedRateId; 						
		 
		 END IF;
		 
		 
		 
		 
		 INSERT INTO Purchase (CustomerId,PointOfSaleId,		PurchaseGMT,	PurchaseNumber,TransactionTypeId,		PaymentTypeId,		LocationId,		PaystationSettingId,	UnifiedRateId,	OriginalAmount,			ChargedAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CreatedGMT) VALUES
							   (lv_CustomerId, lv_PointOfSaleId,P_PurchaseGMT,lv_Number,		lv_TransactionTypeId,	lv_PaymentTypeId,	lv_LocationId,	lv_PaystationSettingId,	lv_UnifiedRateId, lv_OriginalAmount,	lv_OriginalAmount	, lv_OriginalAmount,	lv_OriginalAmount,lv_OriginalAmount,	UTC_TIMESTAMP());	
								
		 SELECT LAST_INSERT_ID() INTO lv_PurchaseId;				   
		 
		 SET lv_LicencePlateId = NULL ;
		 IF length(lv_LicensePlateNo) > 4 THEN
		 SELECT ifnull(max(ID),0) into lv_LicencePlateId FROM LicencePlate where Number = lv_LicensePlateNo ;
		 IF (lv_LicencePlateId = 0) THEN
			INSERT INTO LicencePlate (Number, LastModifiedGMT) VALUES (lv_LicensePlateNo, UTC_TIMESTAMP());
			SELECT LAST_INSERT_ID() INTO lv_LicencePlateId; 	
		 END IF;
		 END IF;
		 
		 		 
		 INSERT INTO Permit (PurchaseId,PermitNumber,LocationId,		PermitTypeId,PermitIssueTypeId,LicencePlateId, SpaceNumber,AddTimeNumber,		PermitBeginGMT,PermitOriginalExpireGMT,													PermitExpireGMT,NumberOfExtensions) VALUES
						     (lv_PurchaseId,lv_Number,	lv_LocationId,	1,			1,				   	lv_LicencePlateId,lv_StallNumber,lv_AddTimeNum,	P_PurchaseGMT, (SELECT DATE_ADD(P_PurchaseGMT,INTERVAL lv_ParkingTimePurchased MINUTE)),(SELECT DATE_ADD(P_PurchaseGMT,INTERVAL lv_ParkingTimePurchased MINUTE)),0);

		 SET lv_MerchantAccountId = NULL ;
		 SELECT Id into lv_MerchantAccountId FROM MerchantAccount where CustomerId = lv_CustomerId LIMIT 1;
		 
		 INSERT INTO ProcessorTransaction(PurchaseId,PointOfSaleId,			PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,	Amount,		CardType,	Last4DigitsOfCardNumber,ProcessingDate,ProcessorTransactionId,ReferenceNumber,IsApproved,CardHash) VALUES
										 (lv_PurchaseId,lv_PointOfSaleId, 	P_PurchaseGMT,	lv_Number,	lv_ProcessorTransactionTypeId,	lv_MerchantAccountId,	lv_OriginalAmount, 'VISA',	'1234',					P_PurchaseGMT,	'123',				'123',				1,		'123');	  				
		 
		 
		 SELECT LAST_INSERT_ID() INTO lv_ProcessorTransactionId;
		 
        INSERT INTO PaymentCard (PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,	MerchantAccountId,		Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved	) VALUES
								(lv_PurchaseId, 1,		2,				lv_ProcessorTransactionTypeId,		lv_ProcessorTransactionId,lv_MerchantAccountId,	lv_OriginalAmount, '1234',	P_PurchaseGMT, 0,			0,		1);		
							   
		 

	 
	set indexm_pos = indexm_pos +1;
	end while;

close C_PreviewRequest;
END//
delimiter ;

-- 

DROP PROCEDURE IF EXISTS sp_PreviewInjectCardData;

delimiter //
Create Procedure sp_PreviewInjectCardData()

BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos,idmaster_pos_Card,indexm_pos_Card,lv_AddTimeNum,lv_LicencePlateId int(4);
declare lv_count TINYINT ;
declare lv_GMT,lv_LastCardRequestSent,lv_PurchaseGMT datetime;
declare lv_MinuteOfDay_GMT MEDIUMINT ;
declare continue handler for not found set NO_DATA=-1;

select count(*) into lv_count from PreviewCardDataInjection where Name = 'LastCardRequestSent' ;

IF (lv_count = 0) THEN

	 INSERT INTO PreviewCardDataInjection (Name, 					Value, 											LastModifiedGMT) VALUES
										  ('LastCardRequestSent',   DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i'), DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i')); 

ELSE

 SELECT Value into lv_LastCardRequestSent FROM PreviewCardDataInjection WHERE Name = 'LastCardRequestSent' ;
 
 SELECT DATE_FORMAT(utc_timestamp(), '%Y-%m-%d %H:%i') into lv_GMT ;
 -- select lv_LastCardRequestSent as lv_LastCardRequestSent;
 -- select lv_GMT as lv_GMT;
 
 WHILE lv_LastCardRequestSent <= lv_GMT DO
	
	select lv_LastCardRequestSent + interval 0 minute into lv_PurchaseGMT ;
	
	SELECT EXTRACT(HOUR FROM lv_LastCardRequestSent)* 60 + (EXTRACT(MINUTE FROM lv_LastCardRequestSent)) into lv_MinuteOfDay_GMT;
	select lv_PurchaseGMT as lv_PurchaseGMT;
	select lv_MinuteOfDay_GMT as lv_MinuteOfDay_GMT ;
		
	CALL sp_Preview_InsertCardTransactionData(lv_PurchaseGMT, lv_MinuteOfDay_GMT) ;
	
	UPDATE PreviewCardDataInjection SET Value = lv_LastCardRequestSent + INTERVAL 1 minute, LastModifiedGMT = UTC_TIMESTAMP()
	WHERE Name = 'LastCardRequestSent' ;

 SET lv_LastCardRequestSent = lv_LastCardRequestSent + INTERVAL 1 minute;	
 end while;
 										
END IF;

END//
delimiter ;



DROP PROCEDURE IF EXISTS sp_ETLInjectCardData ;
delimiter //
CREATE PROCEDURE sp_ETLInjectCardData ()
BEGIN

declare NO_DATA int(4) default 0;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
declare lv_ETLExecutionLogId bigint unsigned;
declare continue handler for not found set NO_DATA=-1;

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLog where ETLProcessTypeId  = 9;

IF (lv_count = 0 ) THEN
  -- Initial Record 
  INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(9,		 NOW(), 				NOW() ,			NOW(),    			NOW(), 			2,	     'InitialRecord'  );
  set lv_status = 2;

ELSE
 
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLog
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLog WHERE ETLProcessTypeId = 9 )  ;

END IF;

If (lv_status = 2) THEN		-- means previous ETL process was sucessful
	INSERT INTO ETLExecutionLog (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (9, 		 NOW(), 			NOW(),				NOW(), 			1 );

	SELECT LAST_INSERT_ID() INTO lv_ETLExecutionLogId;  
	 
	 CALL sp_PreviewInjectCardData();
	 
	UPDATE ETLExecutionLog
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE Id = lv_ETLExecutionLogId;
end if;
END//
delimiter ;


 
