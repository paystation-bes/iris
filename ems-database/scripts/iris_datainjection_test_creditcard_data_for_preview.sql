 
-- Scheduler for Canned Credit Card data for Preview

 -- DROP EVENT IF EXISTS EVENT_StartETL ;
  
 call sp_Preview_CreateMerchantAccount();
 
 /*
DROP EVENT IF EXISTS EVENT_InjectPastCannedCardData;

CREATE EVENT EVENT_InjectPastCannedCardData
ON SCHEDULE AT current_timestamp() + INTERVAL 1 MINUTE
 DO
call sp_Preview_ExecuteCardDataInjectionEvents();

 
 
DROP EVENT IF EXISTS EVENT_InjectCannedCardData;

CREATE EVENT EVENT_InjectCannedCardData
 ON SCHEDULE EVERY 15 minute

 STARTS DATE_ADD(current_timestamp(),INTERVAL 15 minute)
  DO

 STARTS current_timestamp()
 DO

 CALL sp_Preview_InsertCardTransactionData((select UTC_TIMESTAMP()));
 */
 
 
 -- Scheduler to Inject Canned Past credit card data
 /*
DROP EVENT IF EXISTS EVENT_InjectPastCannedCardData;

CREATE EVENT EVENT_InjectPastCannedCardData
ON SCHEDULE AT current_timestamp() + INTERVAL 5 MINUTE
 DO
call sp_Preview_ExecuteCardDataInjectionEvents();


CREATE EVENT EVENT_StartETL
 ON SCHEDULE AT current_timestamp() + INTERVAL 50 MINUTE
 DO
 CALL sp_ETLStart_Inc();
 */
 
 
DROP EVENT IF EXISTS EVENT_InjectCannedCardData;

CREATE EVENT EVENT_InjectCannedCardData
 ON SCHEDULE EVERY 1 minute
 STARTS  DATE_FORMAT(now(), '%Y-%m-%d %H:%i') 
  DO
 CALL sp_ETLInjectCardData();
 
 
