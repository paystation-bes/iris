#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
## Sending email to ashok
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import smtplib
import os
import sys
import time
# from datetime import date
import datetime
import MySQLdb as mdb
# from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from airflow import DAG
from airflow.operators import BashOperator,MySqlOperator


conn = mdb.connect('172.30.5.65', 'ems7_vm', 'ems7_vm', 'airflow')
cur = conn.cursor()
cur.execute("select max(id),timestampdiff(minute,  max(execution_date) ,now()),concat(date(now()),' ',DATE_FORMAT(now(),'%H:%i'),':00') FROM dag_run where dag_id = 'Iris_AutoRecoverable'")
dag_run_rec = cur.fetchone()
dagId = dag_run_rec[0]
ElapsedSinceLastRun1 = dag_run_rec[1]
CurrentTime = dag_run_rec[2]
print "dagId is %s" %dagId
print "ElapsedSinceLastRun1 is %s" %ElapsedSinceLastRun1
print "CurrentTime is %s" %CurrentTime

if (ElapsedSinceLastRun1 > 2):
	print "Inside if"
	#cur.execute("UPDATE dag_run set execution_date = now() where id='%s'",(dagId))
	cur.execute("UPDATE dag_run set execution_date = %s where id=%s",(CurrentTime,dagId))
	conn.commit()
	print "ok"
	
default_args = {
'owner': 'airflow',
'depends_on_past': False,
'start_date': datetime.datetime(datetime.datetime.today().year , datetime.datetime.today().month, datetime.datetime.today().day, datetime.datetime.today().hour ,datetime.datetime.today().minute, 00 ),
'email': ['ashok.pamu@t2systems.com'],
'email_on_failure': 'ashok.pamu@t2systems.com',
'email_on_retry': 'ashok.pamu@t2systems.com',
'retries': 0,
'retry_delay': datetime.timedelta(minutes=2),
 #'queue': 'bash_queue',
 #'pool': 'backfill',
 #'priority_weight': 10,
 'end_date': datetime.datetime(2016, 3, 31),
}
datetime.datetime.today().year
dag = DAG('Iris_AutoRecoverable', default_args=default_args, schedule_interval=datetime.timedelta(minutes = 1))

t1 = MySqlOperator(mysql_conn_id='iris_dev_autorecover',sql='CALL sp_ETLAutoRecoverable()',task_id='Calling_Iris_AutoRecoverable',autocommit=True,dag=dag)

