-- 10240 to 51200
UPDATE `EmsProperties` SET `Value` = '51200' WHERE `Name` = 'ReportsSizeLimitKb';
-- 9216 to 46080
UPDATE `EmsProperties` SET `Value` = '46080' WHERE `Name` =	'ReportsSizeWarningKb';

-- -----------------------------------------------------
-- Schema Changes
-- -----------------------------------------------------

ALTER TABLE `SmsAlert` ADD COLUMN `CallbackId` VARCHAR(40)  NULL AFTER `SpaceNumber`;