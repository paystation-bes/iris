-- -----------------------------------------------------
-- Table `Processor`
-- -----------------------------------------------------
UPDATE `Processor` SET ProductionUrl = 'https://secure.authorize.net/gateway/transact.dll', 
TestUrl = 'https://test.authorize.net/gateway/transact.dll'
WHERE Name = 'processor.authorize.net' OR Name = 'processor.authorize.net.link';
