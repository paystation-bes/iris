-- Deploy this script in EMS6

DROP TABLE IF EXISTS `CustomerMigrationQueue` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationQueue` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `AlertDisabledGMT` DATETIME  NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CustomerMigrationQueue_CustomerId_uq` (`CustomerId`) )
ENGINE = InnoDB;


DROP PROCEDURE IF EXISTS sp_DisableCustomerAlerts ;

delimiter //

CREATE  PROCEDURE sp_DisableCustomerAlerts()
BEGIN


DECLARE CursorDone INT DEFAULT 0;
declare idmaster_pos,indexm_pos int default 0;

DECLARE C1 CURSOR FOR
	
		SELECT
			CustomerId
		FROM 
			CustomerMigrationQueue
		WHERE
			AlertDisabledGMT IS NULL;
			
				
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
 

 -- SET lv_Customer_Timezone = null;
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 

WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	v_CustomerId ;	
								
			UPDATE Alert 
			SET 
			IsDeleted=1,
			IsEnabled=0
			WHERE CustomerId = v_CustomerId; 
			
			UPDATE CustomerMigrationQueue
			SET 
			AlertDisabledGMT = UTC_TIMESTAMP(),
			LastModifiedByUserId = 1
			WHERE 
			CustomerId = v_CustomerId;
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;

COMMIT ;		
        
END//

delimiter ;
