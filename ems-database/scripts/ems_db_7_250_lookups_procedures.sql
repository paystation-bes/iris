/*
  --------------------------------------------------------------------------------
  Procedure:  sp_LoadTime
  
  Purpose:    Loads the Time table with 1 year of records, with 96 records per day
              at 15 minute intervals
             
  Critical:   The auto_increment Id attribute MUST be ever increasing with each 
              subsequent (later) DateTime value. Time records MUST be
              loaded in ascending order so that Id is properly assigned.
              
  Useful SQL: SELECT * FROM Time;
  -------------------------------------------------------------------------------- 
*/
    
DROP PROCEDURE IF EXISTS sp_LoadTime;

delimiter //

CREATE PROCEDURE sp_LoadTime (IN Year CHAR(4))
BEGIN

    DECLARE DateNow DATETIME;
    DECLARE DateEnd DATETIME;
    
    /* Set begin and end dates for looping */
    SET DateNow = CONCAT(Year,'-01-01 00:00:00');
    SET DateEnd = DATE_ADD(DateNow,INTERVAL 1 YEAR);
    
    /* Loop through each hour of each day */
    WHILE DateNow < DateEnd DO
    
        /* Insert into Time table (1 record for each quarter hour) */
        INSERT  Time (DateTime,Date,TimeAmPm,Year,Quarter,Month,MonthName,MonthNameAbbrev,WeekOfYear,DayOfYear,DayOfMonth,DayOfWeek,DayName,DayNameAbbrev,IsLastDayOfMonth,IsWeekend,HourOfDay,QuarterOfDay,QuarterOfHour)
        SELECT  DateNow                                                 AS DateTime ,
                DATE(DateNow)                                           AS Date ,
                H.TimeAmPm                                              AS TimeAmPm ,
                YEAR(DateNow)                                           AS Year ,
                QUARTER(DateNow)                                        AS Quarter ,
                MONTH(DateNow)                                          AS Month ,
                MONTHNAME(DateNow)                                      AS MonthName ,
                SUBSTR(MONTHNAME(DateNow),1,3)                          AS MonthNameAbbrev ,
                WEEK(DATE(DateNow),2)                                   AS WeekOfYear ,
                DAYOFYEAR(DateNow)                                      AS DayOfYear ,
                DAYOFMONTH(DateNow)                                     AS DayOfMonth ,
                DAYOFWEEK(DateNow)                                      AS DayOfWeek ,
                DAYNAME(DateNow)                                        AS DayName ,
                SUBSTR(DAYNAME(DateNow),1,3)                            AS DayNameAbbrev ,
                IF(LAST_DAY(DateNow)=DATE(DateNow),1,0)                 AS IsLastDayOfMonth ,
                IF(DAYOFWEEK(DateNow)=1 OR DAYOFWEEK(DateNow)=7,1,0)    AS IsWeekend ,
                H.HourOfDay                                             AS HourOfDay ,
                H.Id                                                    AS QuarterOfDay ,
                H.QuarterOfHour                                         AS QuarterOfHour
        FROM    QuarterHour H
        WHERE   H.HourOfDay = HOUR(DateNow)
        AND     H.QuarterOfHour*15 = MINUTE(DateNow);
        
        /* Set next quarter hour */
        SET DateNow = DATE_ADD(DateNow,INTERVAL 15 MINUTE);        
    END WHILE;
END//

delimiter ;

/* Load the Time table with 8 years of '15 minute buckets' */
/*
CALL sp_LoadTime ('2000');
CALL sp_LoadTime ('2001');
CALL sp_LoadTime ('2002');

*/
-- INSERTING DEFAULT DATA IN ETLSTATUS TABLE

-- INSERT INTO ETLStatus (ETLProcessTypeId, TableName, ETLProcessedId, ETLProcessedGMT) VALUES (1,'Purchase',0,now());



DROP PROCEDURE IF EXISTS sp_GenerateETLDateRange;

delimiter //

CREATE PROCEDURE sp_GenerateETLDateRange (IN P_ETL_OBJECT VARCHAR(20), IN P_Year INT, IN P_No_of_Days INT)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int(4);
declare lv_id TINYINT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;

DECLARE DateNow DATETIME;
DECLARE DateEnd DATETIME;
	
	
declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

SET DateNow = CONCAT(P_Year,'-01-01 00:00:00');
SET DateEnd = DATE_ADD(DateNow,INTERVAL 1 YEAR);

while DateNow < DateEnd DO

INSERT INTO `ETLProcessDateRange` (ETLProcessTypeId, ETLObject, 		BeginDt, 				EndDt, 					Status) VALUES
								  (1,         P_ETL_OBJECT, 	DateNow,	concat(date(DATE_ADD(DateNow,INTERVAL 0 second)),' ','23:59:59'), 	0);
 
SET DateNow = DATE_ADD(DateNow,INTERVAL 1 day);
 

end while;
END //
DELIMITER ;

/*
	CALL sp_GenerateETLDateRange('PPP',2012,14);
	CALL sp_GenerateETLDateRange('Collection',2012,14);
	
*/

/*

Procedure to Populate Migration Date Lookup Table with Start and End date


*/

DROP PROCEDURE IF EXISTS sp_GenerateMigrationDateRange;


delimiter //

CREATE PROCEDURE sp_GenerateMigrationDateRange (IN P_ClusterId TINYINT, IN P_ETL_OBJECT VARCHAR(50), 
IN P_StartDate Datetime, IN P_EndDate Datetime, IN P_No_of_Days INT)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int(4);
declare lv_id TINYINT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;

DECLARE DateNow DATETIME;
DECLARE DateEnd DATETIME;
	
	
declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

-- SET DateNow = CONCAT(P_Year,'-01-01 00:00:00');
SET DateNow = P_StartDate;
-- SET DateEnd = DATE_ADD(DateNow,INTERVAL 1 YEAR);
-- SET DateEnd = P_EndDate;
SET DateEnd = P_EndDate;

while DateNow < P_EndDate DO

select concat(date(DATE_ADD(DateNow,INTERVAL P_No_of_Days DAY)),' ','23:59:59') into DateEnd;
If (DateEnd > P_EndDate) then
	set DateEnd = P_EndDate;
end if;

INSERT INTO `MigrationDateLookup` (ETLProcessTypeId, TableName, 		BeginDt, 				EndDt, 					Status) VALUES
								  (P_ClusterId,         P_ETL_OBJECT, 	DateNow,		DateEnd, 	0);
 
-- select concat(date(DATE_ADD(DateNow,INTERVAL P_No_of_Days DAY)),' ','23:59:59') INTO DateNow ;--
-- SET DateNow = DateNow + INTERVAL 1 SECOND ;
SET DateNow = DateEnd + INTERVAL 1 SECOND;
-- select DateNow;
 

end while;
END //
DELIMITER ;

-- ***********************************************************************************************************************************************

DROP PROCEDURE IF EXISTS sp_GenerateETLDateRangeCustomerBeta;

delimiter //

CREATE PROCEDURE sp_GenerateETLDateRangeCustomerBeta (IN P_ClusterId TINYINT, IN P_ETL_OBJECT VARCHAR(20), 
IN P_StartDate Datetime, IN P_EndDate Datetime, IN P_No_of_Days INT)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int(4);
declare lv_id TINYINT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;

DECLARE DateNow DATETIME;
DECLARE DateEnd DATETIME;
	
	
declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

SET DateNow = P_StartDate;
-- SET DateEnd = DATE_ADD(DateNow,INTERVAL 1 YEAR);
-- SET DateEnd = P_EndDate;
SET DateEnd = P_EndDate;

while DateNow < P_EndDate DO


select concat(date(DATE_ADD(DateNow,INTERVAL P_No_of_Days DAY)),' ','23:59:59') into DateEnd;
If (DateEnd > P_EndDate) then
	set DateEnd = P_EndDate;
end if;

INSERT INTO `ETLProcessDateRange` (ETLProcessTypeId, ETLObject, 		BeginDt, 				EndDt, 					Status) VALUES
								  (P_ClusterId,         P_ETL_OBJECT, 	DateNow,	DateEnd , 	0);
 
SET DateNow = DateEnd + INTERVAL 1 SECOND;
 

end while;
END //
DELIMITER ;






-- ***********************************************************************************************************************************************
-- PROCEDURE sp_Delete_ProcessorTransaction_incrementally

DROP PROCEDURE IF EXISTS sp_Delete_ProcessorTransaction_incrementally;

delimiter //

CREATE PROCEDURE sp_Delete_ProcessorTransaction_incrementally( in p_StartDate datetime, in p_EndDate datetime, in p_Limit INT)

 BEGIN


REPEAT

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('ProcessorTransaction-Start',p_StartDate,p_EndDate,p_Limit,now());

 DELETE FROM ProcessorTransaction WHERE  ProcessingDate between p_StartDate and p_EndDate LIMIT p_Limit ;


UNTIL ROW_COUNT() = 0 END REPEAT;

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('ProcessorTransaction-End',p_StartDate,p_EndDate,p_Limit,now());

END//
delimiter ;


-- 

DROP PROCEDURE IF EXISTS sp_Delete_ProcessorTransaction_incrementally2;

delimiter //

CREATE PROCEDURE sp_Delete_ProcessorTransaction_incrementally2( in p_StartDate datetime, in p_EndDate datetime, in p_Limit INT)

 BEGIN


REPEAT

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('ProcessorTransaction2-Start',p_StartDate,p_EndDate,p_Limit,now());

 DELETE FROM ProcessorTransaction WHERE  ProcessingDate between p_StartDate and p_EndDate LIMIT p_Limit ;


UNTIL ROW_COUNT() = 0 END REPEAT;

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('ProcessorTransaction2-End',p_StartDate,p_EndDate,p_Limit,now());

END//
delimiter ;


-- -------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_Delete_PaymentCard_incrementally;

delimiter //

CREATE PROCEDURE sp_Delete_PaymentCard_incrementally( in p_StartDate datetime, in p_EndDate datetime, in p_Limit INT)

 BEGIN


REPEAT

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('PaymentCard-Start',p_StartDate,p_EndDate,p_Limit,now());

 DELETE FROM PaymentCard WHERE 
 PurchaseId in 
( select Id from Purchase where PurchaseGMT between p_StartDate and	p_EndDate)
LIMIT p_Limit ;



UNTIL ROW_COUNT() = 0 END REPEAT;

 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('PaymentCard-End',p_StartDate,p_EndDate,p_Limit,now());

END//
delimiter ;

--  

-- ***********************************************************************************************************************************************
-- This Procedure will fill the data in new column POSBalance.LastCollectionGMT


DROP PROCEDURE IF EXISTS sp_FillPOSBalanceOverdueDate;

delimiter //

CREATE PROCEDURE sp_FillPOSBalanceOverdueDate ()
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int(4);
declare lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_MostRecentCollectionDate DATETIME;


declare c1 cursor  for
select PointOfSaleId  from POSBalance order by PointOfSaleId ;


declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());
 
while indexm_pos < idmaster_pos do
fetch c1 into lv_PointOfSaleId;

	set lv_MostRecentCollectionDate = null;
	
	SELECT 
    CASE 
        WHEN 
                   LastCashCollectionGMT >= LastCoinCollectionGMT AND 
                   LastCashCollectionGMT >= LastBillCollectionGMT AND 
                   LastCashCollectionGMT >= LastCardCollectionGMT THEN LastCashCollectionGMT 
        WHEN 
                   LastCoinCollectionGMT >= LastCashCollectionGMT AND 
                   LastCoinCollectionGMT >= LastBillCollectionGMT AND 
                   LastCoinCollectionGMT >= LastCardCollectionGMT THEN LastCoinCollectionGMT 
        WHEN 
                   LastBillCollectionGMT >= LastCashCollectionGMT AND 
                   LastBillCollectionGMT >= LastCoinCollectionGMT AND 
                   LastBillCollectionGMT >= LastCardCollectionGMT THEN LastBillCollectionGMT 
		WHEN 
                   LastCardCollectionGMT >= LastCashCollectionGMT AND 
                   LastCardCollectionGMT >= LastCoinCollectionGMT AND 
                   LastCardCollectionGMT >= LastBillCollectionGMT THEN LastCardCollectionGMT 
        ELSE 
                   LastCashCollectionGMT 
		END  
		
		INTO lv_MostRecentCollectionDate 
	FROM 
         POSBalance 
	where 
         PointOfSaleId = lv_PointOfSaleId;
		 
		 
	UPDATE POSBalance SET LastCollectionGMT = lv_MostRecentCollectionDate
	WHERE PointOfSaleId = lv_PointOfSaleId;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
 
  
END//
delimiter ;

-- Procedure to Delete Purchase 
-- -------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_Delete_Purchase_incrementally;

delimiter //

CREATE PROCEDURE sp_Delete_Purchase_incrementally( in p_StartDate datetime, in p_EndDate datetime)

 BEGIN

 -- Insert into table at the begining of delete for logging purpose only
 
 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('Purchase-Start',p_StartDate,p_EndDate,NULL,now());

 -- This will delete data in Purchase based on PurchaseGMT
 
 DELETE FROM Purchase WHERE 
 PurchaseGMT between p_StartDate and p_EndDate ;

 -- Insert into table at the end of delete for logging purpose only
 
 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('PaymentCard-End',p_StartDate,p_EndDate,NULL,now());

END//
delimiter ;

--  

-- Procedure to Delete Permit 
-- -------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_Delete_Permit_incrementally;

delimiter //

CREATE PROCEDURE sp_Delete_Permit_incrementally( in p_StartDate datetime, in p_EndDate datetime)

 BEGIN

 -- Insert into table at the begining of delete for logging purpose only
 
 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('Permit-Start',p_StartDate,p_EndDate,NULL,now());

 -- This will delete data in Permit 
 
 delete PR.* from Permit PR inner join Purchase PU on PR.PurchaseId = PU.Id
 and PU.PurchaseGMT  between p_StartDate and p_EndDate;

 -- Insert into table at the end of delete for logging purpose only
 
 INSERT INTO DeleteTransactionDataincrementally (TableName,StartGMT,EndGMT,NoOfRowsDeleted,RecordInsertTime)
 values ('Permit-End',p_StartDate,p_EndDate,NULL,now());

END//
delimiter ;

--  

-- Created on Sep 22 2014 to migrate ExtensibleRates to Iris.
-- This script needs to be run for non-migrated customers ONLY
-- Permanent fix is in INC code 7.2

DROP PROCEDURE IF EXISTS sp_MigrateExtensibleRateDayOfWeek;

delimiter //

CREATE PROCEDURE sp_MigrateExtensibleRateDayOfWeek(in P_EMS6_EMSRateId BIGINT UNSIGNED, in P_EMS6_DayOfWeekId TINYINT) 


 BEGIN
 DECLARE lv_EMS7_ExtensibleRateId BIGINT UNSIGNED;
 DECLARE lv_Cnt_ExtensibleRateMapping INT ;


select count(*) into lv_Cnt_ExtensibleRateMapping from ExtensibleRateMapping where EMS6RateId = P_EMS6_EMSRateId ;

 IF (lv_Cnt_ExtensibleRateMapping = 1) THEN 	-- Valid
	
	select EMS7RateId into lv_EMS7_ExtensibleRateId from ExtensibleRateMapping where EMS6RateId = P_EMS6_EMSRateId ;
	
	delete from ExtensibleRateDayOfWeek where ExtensibleRateId = lv_EMS7_ExtensibleRateId and DayOfWeekId = P_EMS6_DayOfWeekId ;
	
	insert into ExtensibleRateDayOfWeek(ExtensibleRateId,			DayOfWeekId,		VERSION,	LastModifiedGMT,	LastModifiedByUserId) values
										(lv_EMS7_ExtensibleRateId, 	P_EMS6_DayOfWeekId,	1,	   		UTC_TIMESTAMP(),	1 );
 
 END IF;
 
IF (lv_Cnt_ExtensibleRateMapping = 0) THEN 	-- EMSRate does not exists in Iris

	SELECT 'EMSRate does not exists in Iris' ;
	
END IF;


END//
delimiter ;



-- Created on Sep 23 2014 to migrate Paystation Provisioned and Activation Dates
-- This script needs to be run for non-migrated customers ONLY
-- Need to verify data to run for Migrated Customers
-- Permanent fix is in INC code 7.2
/*
DROP PROCEDURE IF EXISTS sp_MigratePaystationActivationDateFromEMS;

delimiter //

CREATE PROCEDURE sp_MigratePaystationActivationDateFromEMS( in P_EMS6_PaystationId MEDIUMINT UNSIGNED,
															in P_EMS6_CommAddress varchar(20),
															in P_EMS6_CustomerId MEDIUMINT UNSIGNED, 
															in P_EMS6_LockState varchar(20), 
															in P_EMS6_ProvisionDate datetime,
															in P_EMS6_PaystationIsActive TINYINT) 


 BEGIN
 
 
 

END//
delimiter ;
*/ 

-- -- -- -- -- -- --

-- 1) To delete EventLogNewMapping. This code need to be deployed on Iris

-- This procedure will continue to execute till data in EventLogNewMapping is cleaned up

-- This procedure will take input as Date, Batch and Sleep interval as Parameter

-- This Procedure will call sp_DeleteEventLogNewMapping_Inc. Actual delete operation is perfomed in sp_DeleteEventLogNewMapping_Inc

-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_EventLogNewMappingForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_EventLogNewMappingForDelete_Inc(in P_ToDate DATETIME, in P_Batch INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_EventLogNewMapping INT UNSIGNED;
 declare idmaster_pos,indexm_pos INT UNSIGNED;

 
REPEAT

call sp_DeleteEventLogNewMapping_Inc(P_ToDate, P_Batch);

-- Wait time in seconds before proceding to next batch of delete

select sleep(P_Sleep) as SLEEP ;

-- Here is the loop untill count=0
select count(*) into  lv_Cnt_EventLogNewMapping
from EventLogNewMapping  where DateField <= P_ToDate ;

UNTIL lv_Cnt_EventLogNewMapping = 0 END REPEAT;

select 'Delete Completed for EventLogNewMapping' as Status ;
 
END//
delimiter ;


DROP PROCEDURE IF EXISTS sp_DeleteEventLogNewMapping_Inc;

delimiter //

CREATE PROCEDURE sp_DeleteEventLogNewMapping_Inc(in P_ToDate DATETIME, in P_Batch INT)

 BEGIN

 declare lv_EventLogNewMappingId INT;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 
declare c1 cursor  for
select Id from EventLogNewMapping  where DateField <= P_ToDate Limit P_Batch ;

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_EventLogNewMappingId;
	
	-- Actual deletion. Delete is based on Id
	DELETE FROM EventLogNewMapping where Id = lv_EventLogNewMappingId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;



END//
delimiter ;




-- Autoincrement status


DROP PROCEDURE IF EXISTS sp_GetAutoIncrementStatus;

delimiter //

CREATE PROCEDURE sp_GetAutoIncrementStatus(P_SchemaName varchar(20), P_AutoIncrement INT)

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 declare lv_Table_name varchar(100);
 declare lv_Data_Type varchar(20);
 declare lv_AUTO_INCREMENT bigint UNSIGNED;
 declare lv_COLUMN_TYPE varchar(50);
 
 declare cst_TINYINT DECIMAL(4,1) DEFAULT  127.0 ;
 declare cst_TINYINT_US DECIMAL(4,1) DEFAULT  255.0 ; 
 
 declare cst_SMALLINT DECIMAL(6,1) DEFAULT 32767.0 ;
 declare cst_SMALLINT_US DECIMAL(6,1) DEFAULT  65535.0 ; 
 
 declare cst_MEDIUMINT DECIMAL(8,1) DEFAULT  8388607.0 ;
 declare cst_MEDIUMINT_US DECIMAL(9,1) DEFAULT  16777215.0 ;
 
 declare cst_INT DECIMAL(11,1)  DEFAULT  2147483647.0 ;
 declare cst_INT_US DECIMAL(11,1) DEFAULT  4294967295.0 ;
 
 declare cst_BIGINT DECIMAL(20,1)  DEFAULT 9223372036854775807.0 ;
  declare cst_BIGINT_US DECIMAL(21,1) DEFAULT  18446744073709551615.0 ;
 
 declare lv_RemainingTinyInt DECIMAL(21,1) ;
 
 
 declare c1 cursor  for
 select A.Table_name , B.Data_Type, A.AUTO_INCREMENT, COLUMN_TYPE from information_schema.TABLES A, 
 information_schema.COLUMNS B
where 
A.TABLE_SCHEMA=B.TABLE_SCHEMA and A.TABLE_NAME=B.TABLE_NAME AND B.COLUMN_NAME = 'Id' and
A.table_schema = P_SchemaName and A.AUTO_INCREMENT is not null order by A.Table_name;

/*
DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
     select lv_Table_name ;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_Table_name , lv_Data_Type, lv_AUTO_INCREMENT, lv_COLUMN_TYPE;
	
		-- tinyint
		IF (lv_Data_Type = 'tinyint') AND (Locate('unsigned',lv_COLUMN_TYPE) > 0) THEN 
		
			select round((cst_TINYINT_US - lv_AUTO_INCREMENT)/P_AutoIncrement) INTO lv_RemainingTinyInt ;
			IF (cst_TINYINT_US - lv_AUTO_INCREMENT) <= round(cst_TINYINT_US/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		ELSEIF (lv_Data_Type = 'tinyint') THEN
		
			select round((cst_TINYINT - lv_AUTO_INCREMENT)/P_AutoIncrement) INTO lv_RemainingTinyInt ;
			IF (cst_TINYINT - lv_AUTO_INCREMENT) <= round(cst_TINYINT/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		END IF;
		
		-- mediumint
		IF (lv_Data_Type = 'mediumint') AND (Locate('unsigned',lv_COLUMN_TYPE) > 0) THEN 
		
			select (cst_MEDIUMINT_US - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_MEDIUMINT_US - lv_AUTO_INCREMENT) <= round(cst_MEDIUMINT_US/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		ELSEIF (lv_Data_Type = 'mediumint') THEN
		
			select (cst_MEDIUMINT - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_MEDIUMINT - lv_AUTO_INCREMENT) <= round(cst_MEDIUMINT/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		END IF;
		
		-- smallint
		IF (lv_Data_Type = 'smallint') AND (Locate('unsigned',lv_COLUMN_TYPE) > 0) THEN 
		
			select (cst_SMALLINT_US - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_SMALLINT_US - lv_AUTO_INCREMENT) <= round(cst_SMALLINT_US/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		ELSEIF (lv_Data_Type = 'smallint') THEN
		
			select (cst_SMALLINT - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_SMALLINT - lv_AUTO_INCREMENT) <= round(cst_SMALLINT/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		END IF;
		
		-- int
		IF (lv_Data_Type = 'int') AND (Locate('unsigned',lv_COLUMN_TYPE) > 0) THEN 
		
			select (cst_INT_US - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_INT_US - lv_AUTO_INCREMENT) <= round(cst_INT_US/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		ELSEIF (lv_Data_Type = 'int') THEN
		
			select (cst_INT - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_INT - lv_AUTO_INCREMENT) <= round(cst_INT/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		END IF;
		
		-- bigint
		IF (lv_Data_Type = 'bigint') AND (Locate('unsigned',lv_COLUMN_TYPE) > 0) THEN 
		
			select ( cst_BIGINT_US - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF ( cst_BIGINT_US  - lv_AUTO_INCREMENT) <= round(cst_BIGINT_US/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		ELSEIF (lv_Data_Type = 'bigint') THEN
		
			select (cst_BIGINT - lv_AUTO_INCREMENT)/P_AutoIncrement INTO lv_RemainingTinyInt ;
			IF (cst_BIGINT - lv_AUTO_INCREMENT) <= round(cst_BIGINT/4) THEN
				INSERT INTO AutoIncrementAlert (SchemaName, TableName, DataType, CurrentAutoIncValue, RemainingBuffer) VALUES
											    (P_SchemaName, lv_Table_name, lv_Data_Type,lv_AUTO_INCREMENT,lv_RemainingTinyInt) ;
			END IF;
		
		END IF;
		
		set lv_RemainingTinyInt = 0 ;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;
	
END//
delimiter ;



-- This procedure will delete records from Cluster ONLY
-- set sql_log_bin=0  
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_RemovePreviousTransactionsFromCluster $$
CREATE PROCEDURE sp_RemovePreviousTransactionsFromCluster (in P_FromDate datetime, in P_ToDate datetime, in P_Sleep int)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos, indexm_pos_ABS,idmaster_pos_ABS,indexm_pos_POSEVT,idmaster_pos_POSEVT int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_PurchaseId, lv_ProcessorTransactionId,lv_POSEventHistoryId BIGINT UNSIGNED ;
declare lv_FromDate, lv_ToDate datetime;
declare lv_IdElavonTransactionDetail, lv_ActiveBatchSummaryId BIGINT UNSIGNED ;
declare lv_MerchantAccountId, lv_PointOfSaleId mediumint unsigned;
declare lv_BatchNumber smallint unsigned ;



declare c1 cursor  for
 select Id from Purchase where PurchaseGMT between P_FromDate and P_ToDate order by Id;
 
 declare c2 cursor  for
 select Id from ProcessorTransaction where ProcessingDate between P_FromDate and P_ToDate and PurchaseId is null order by Id;

  declare c_ActiveBatchSummary cursor  for
 select MerchantAccountId, BatchNumber, Id from ActiveBatchSummary where CreatedGMT	between P_FromDate and P_ToDate order by Id ;
 
 
 declare c3 cursor  for
  select Id from ElavonTransactionDetail where MerchantAccountId = lv_MerchantAccountId and BatchNumber = lv_BatchNumber order by Id;
 
 declare cPointOfSaleEVT cursor for 
	select Id from PointOfSale order by Id;

declare cPOSEventHistory cursor for 
	select Id from POSEventHistory where PointOfSaleId = lv_PointOfSaleId and TimestampGMT between P_FromDate and P_ToDate order by Id ;
	
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

-- Check if input datetime passed is NOT within 1 year.

set sql_log_bin=0 ;

select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -365 DAY) into lv_FromDate;
select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -365 DAY)   into lv_ToDate;

if (P_FromDate <= lv_FromDate) and (P_ToDate <= lv_ToDate) THEN

	SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
	SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
	SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

	open c1;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c1 into lv_PurchaseId;

			DELETE from Purchase where Id = lv_PurchaseId;
			DELETE from Permit where PurchaseId = lv_PurchaseId;
			DELETE from ProcessorTransaction where PurchaseId = lv_PurchaseId;
			DELETE from PaymentCard where PurchaseId = lv_PurchaseId;
			DELETE from PurchaseTax where PurchaseId = lv_PurchaseId;
			DELETE from PurchaseCollection where PurchaseId = lv_PurchaseId;
			
			
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		-- select 'Completed' As Status;
	close c1;
	
	-- For ProcessorTransaction that doesnot have PurchaseId ( Boss Uploads)
	set indexm_pos =0;
	set idmaster_pos =0;
	open c2;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c2 into lv_ProcessorTransactionId;
		
			DELETE from ProcessorTransaction where Id = lv_ProcessorTransactionId;
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		-- select 'Removed previous data from ProcessorTransaction for BossUploads' As Status;
	close c2;
	
	-- For ElavonTransactionDetail
	
	
   set indexm_pos_ABS =0;
   set idmaster_pos_ABS =0;

   open c_ActiveBatchSummary;
	set idmaster_pos_ABS = (select FOUND_ROWS());
	while indexm_pos_ABS < idmaster_pos_ABS do
	fetch c_ActiveBatchSummary into lv_MerchantAccountId, lv_BatchNumber,lv_ActiveBatchSummaryId;

   set indexm_pos =0;
   set idmaster_pos =0;
	open c3;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c3 into lv_IdElavonTransactionDetail;
		
			DELETE from ElavonTransactionDetail where Id = lv_IdElavonTransactionDetail;
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		-- select 'Removed previous data from ElavonTransactionDetail ' As Status;
	close c3;
	

	DELETE BSD.* from BatchSettlementDetail BSD 
	inner join BatchSettlement BS on BSD.BatchSettlementId = BS.Id 
	inner join  ActiveBatchSummary ABS on BS.ActiveBatchSummaryId = ABS.Id where ABS.Id = lv_ActiveBatchSummaryId;

	DELETE BS.* from BatchSettlement BS 
	inner join  ActiveBatchSummary ABS on BS.ActiveBatchSummaryId = ABS.Id where ABS.Id = lv_ActiveBatchSummaryId;

	DELETE from ActiveBatchSummary where Id = 	lv_ActiveBatchSummaryId ;

set indexm_pos_ABS = indexm_pos_ABS +1;
end while;
close c_ActiveBatchSummary;

-- for POSEventHistory
		
   set indexm_pos_POSEVT =0;
   set idmaster_pos_POSEVT =0;

   open cPointOfSaleEVT;
	set idmaster_pos_POSEVT = (select FOUND_ROWS());
	while indexm_pos_POSEVT < idmaster_pos_POSEVT do
	fetch cPointOfSaleEVT into lv_PointOfSaleId;

   set indexm_pos =0;
   set idmaster_pos =0;
	open cPOSEventHistory;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch cPOSEventHistory into lv_POSEventHistoryId;
		
			DELETE from POSEventHistory where Id = lv_POSEventHistoryId;
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		-- select 'Removed previous data from POSEventHistory ' As Status;
	close cPOSEventHistory;
	


set indexm_pos_POSEVT = indexm_pos_POSEVT +1;
end while;
close cPointOfSaleEVT;

	SET SQL_MODE=@OLD_SQL_MODE;
	SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
	SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

ELSE
	select 'Incorrect Dates. Process aborted' As Status;
END IF;

END $$

DELIMITER ;

--



-- validate POSDate


DROP PROCEDURE IF EXISTS sp_SetActivationDateInPOSDate;

delimiter //

CREATE PROCEDURE sp_SetActivationDateInPOSDate(in P_SerialNumber VARCHAR(20), in P_LockState varchar(20), in P_ConnectionDate DateTime )

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 declare lv_PointOfSaleId  MEDIUMINT UNSIGNED;
 

DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
     select concat('ERROR', ' ', P_SerialNumber);
  END;

  SELECT Id into lv_PointOfSaleId from PointOfSale where SerialNumber = P_SerialNumber ;
  Delete from POSDate where PointOfSaleId = lv_PointOfSaleId and POSDateTypeId = 9;
  
  IF (P_LockState = 'LOCKED') THEN
	INSERT INTO POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
                      ( lv_PointOfSaleId, 9,         P_ConnectionDate, 0 ,     1,		  UTC_TIMESTAMP(), 1);
  END IF;
  
  IF (P_LockState = 'UNLOCKED') THEN
	INSERT INTO POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
                      ( lv_PointOfSaleId, 9,         P_ConnectionDate, 1 ,     1,		  UTC_TIMESTAMP(), 1);
  END IF;
  
END//
delimiter ;


-- validate POSDate in INC


DROP PROCEDURE IF EXISTS sp_InsertActivationDateInPOSDate;

delimiter //

CREATE PROCEDURE sp_InsertActivationDateInPOSDate(in P_PointOfSaleId MEDIUMINT UNSIGNED, in P_LockState varchar(20), P_ProvisionDate datetime )

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 declare lv_PointOfSaleId  MEDIUMINT UNSIGNED;
 declare lv_CurrentSetting tinyint(1) unsigned DEFAULT NULL ;
 declare lv_ChangedGMT datetime;

DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
     select concat('ERROR', ' ', P_PointOfSaleId);
  END;

 
 
	-- Check the latest Activation stetting in POSDate. If there is no change then do not perform any change
	SELECT CurrentSetting, ChangedGMT into lv_CurrentSetting, lv_ChangedGMT from POSDate
	WHERE PointOfSaleId = P_PointOfSaleId and POSDateTypeId=9 and ChangedGMT = 
	( select max(ChangedGMT) from POSDate
	WHERE PointOfSaleId = P_PointOfSaleId and POSDateTypeId=9) ;
		
	IF (lv_CurrentSetting = 0) AND (P_LockState = 'UNLOCKED') AND (lv_ChangedGMT<> P_ProvisionDate) THEN  -- Deactivated in IRIS, Activated in EMS
		
		INSERT INTO POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, 		CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
                      ( P_PointOfSaleId, 9,         		P_ProvisionDate, 1 ,     		1,		  UTC_TIMESTAMP(), 1);
		
	
	END IF ;
 
	IF (lv_CurrentSetting = 1) AND (P_LockState = 'LOCKED') AND (lv_ChangedGMT<> P_ProvisionDate) THEN  -- Activated in IRIS, DeActivated in EMS
  
		INSERT INTO POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, 		CurrentSetting, VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
                      ( P_PointOfSaleId, 9,         		P_ProvisionDate, 0 ,     		1,		  UTC_TIMESTAMP(), 1);
  
	END IF ;
	
	IF (lv_CurrentSetting IS NULL) THEN
	
		INSERT INTO POSDate (PointOfSaleId, POSDateTypeId, ChangedGMT, 		CurrentSetting, 					VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
                      ( P_PointOfSaleId, 9,         		P_ProvisionDate, IF(P_LockState='UNLOCKED',1,0) ,     		1,		  UTC_TIMESTAMP(), 1);
		
	END IF;

END//
delimiter ;





-- EMS 6330 
-- Script 1/2 (To remove form PreAuthHolding)

-- -- -- -- -- -- --
--  To delete PreAuthHolding older than 90 days for non migrated customers
--  CALL sp_PreAuthHoldingForDelete_Inc(60000, 10) ; 
--  First parameter is # of records to be deleted, Second parameter is sleep interval in seconds




-- -- -- -- -- -- --
DROP PROCEDURE IF EXISTS sp_PreAuthHoldingForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PreAuthHoldingForDelete_Inc(in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PreAuthHolding BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_Past90Date datetime; 
 
 
 select date_sub(UTC_TIMESTAMP() ,interval 90 day) into lv_Past90Date;
 
REPEAT

call sp_DeletePreAuthHolding_Inc(lv_Past90Date, P_Limit);

select sleep(P_Sleep) as SLEEP ;

select count(*) into  lv_Cnt_PreAuthHolding
from PreAuthHolding PH, PointOfSale POS, Customer C where PH.PointOfSaleId = POS.Id and POS.CustomerId = C.Id and C.IsMigrated = 0
and PH.PreAuthDate < lv_Past90Date ; 

UNTIL lv_Cnt_PreAuthHolding = 0 END REPEAT;

select 'Delete Completed for PreAuthHolding' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePreAuthHolding_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePreAuthHolding_Inc(in P_Past90Day datetime, in P_Limit INT)

 BEGIN

 declare lv_PreAuthHoldingId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
Select  PH.Id
from PreAuthHolding PH, PointOfSale POS, Customer C where PH.PointOfSaleId = POS.Id and POS.CustomerId = C.Id and C.IsMigrated = 0
and PH.PreAuthDate < P_Past90Day LIMIT P_Limit; 

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

 set autocommit = 0 ;

while indexm_pos < idmaster_pos do
fetch c1 into lv_PreAuthHoldingId;

	DELETE FROM PreAuthHolding where Id = lv_PreAuthHoldingId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;		-- ASHOK2	   
 set autocommit = 1 ; -- Auto Commit ON
 
END//
delimiter ;

-- EMS 6330 
-- Script 2/2 (To remove form PreAuth)

-- -- -- -- -- -- --
--  To delete PreAuth older than 7 days for non migrated customers
--  CALL sp_PreAuthForDelete_Inc(60000, 10);
--  First parameter is # of records to be deleted, Second parameter is sleep interval in seconds
-- -- -- -- -- -- --

DROP PROCEDURE IF EXISTS sp_PreAuthForDelete_Inc;

delimiter //

CREATE PROCEDURE sp_PreAuthForDelete_Inc(in P_Limit INT, in P_Sleep TINYINT)

 BEGIN

 declare lv_Cnt_PreAuth BIGINT UNSIGNED;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_Past7Date datetime; 
 
 
 select date_sub(UTC_TIMESTAMP() ,interval 7 day) into lv_Past7Date;
 
REPEAT

call sp_DeletePreAuth_Inc(lv_Past7Date, P_Limit);

select sleep(P_Sleep) as SLEEP ;

select count(*) into  lv_Cnt_PreAuth
from PreAuth, PointOfSale POS, Customer C where PointOfSaleId = POS.Id and POS.CustomerId = C.Id and C.IsMigrated = 0 
and PreAuthDate < lv_Past7Date ; 

UNTIL lv_Cnt_PreAuth = 0 END REPEAT;

select 'Delete Completed for PreAuth' as Status ;
 
END//
delimiter ;

DROP PROCEDURE IF EXISTS sp_DeletePreAuth_Inc;

delimiter //

CREATE PROCEDURE sp_DeletePreAuth_Inc(in P_Past7Day datetime, in P_Limit INT)

 BEGIN

 declare lv_PreAuthId BIGINT UNSIGNED;
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 
declare c1 cursor  for
Select  PA.Id
from PreAuth  PA, PointOfSale POS, Customer C where PointOfSaleId = POS.Id and POS.CustomerId = C.Id and C.IsMigrated = 0 
and PreAuthDate < P_Past7Day LIMIT P_Limit; 

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

 set autocommit = 0 ;

while indexm_pos < idmaster_pos do
fetch c1 into lv_PreAuthId;
	-- Added on Aug 10 2015 to set PreAuthId to NULL in ProcessorTransaction before deleting the record from PreAuth
	UPDATE ProcessorTransaction SET PreAuthId = NULL WHERE PreAuthId = lv_PreAuthId ;
	DELETE FROM PreAuth where Id = lv_PreAuthId ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;		-- ASHOK2	   
 set autocommit = 1 ; -- Auto Commit ON
 
END//
delimiter ;




-- Delete user logins older than 1 year

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_RemoveArchivedActivityLogin $$
CREATE PROCEDURE sp_RemoveArchivedActivityLogin (in P_FromDate datetime, in P_ToDate datetime, in P_Sleep int)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_ActivityLog, lv_ActivityLoginId INT UNSIGNED ;
declare lv_FromDateActivityLog, lv_ToDateActivityLog datetime;


declare c_ActivityLog cursor  for
 select Id from ActivityLog where  useraccountId <>1 and ActivityGMT between P_FromDate and P_ToDate order by Id;
 
 declare c_ActivityLogin cursor  for
 select Id from ActivityLogin where useraccountId <>1 and ActivityGMT between P_FromDate and P_ToDate order by Id;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

-- Check if input datetime passed is NOT within 1 year.

select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -385 DAY) into lv_FromDateActivityLog;
select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -385 DAY)   into lv_ToDateActivityLog;

if (P_FromDate <= lv_FromDateActivityLog) and (P_ToDate <= lv_ToDateActivityLog) THEN

	
	open c_ActivityLog;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c_ActivityLog into lv_ActivityLog;

			DELETE from ActivityLog where Id = lv_ActivityLog;
			
			 select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		
	close c_ActivityLog;
	
	-- For ActivityLogin
	set indexm_pos =0;
	set idmaster_pos =0;
	open c_ActivityLogin;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c_ActivityLogin into lv_ActivityLoginId;
		
			DELETE from ActivityLogin where Id = lv_ActivityLoginId;
			 select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		
	close c_ActivityLogin;
	
	
ELSE
	select 'Incorrect Dates. Process aborted' As Status;
END IF;

END $$

DELIMITER ;





-- Sprint # 15

-- This procedure will delete records from Cluster ONLY
-- set sql_log_bin=0  
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_ArchiveActivityLoginFromCluster $$
CREATE PROCEDURE sp_ArchiveActivityLoginFromCluster (in P_FromDate datetime, in P_ToDate datetime, in P_Sleep int)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_ActivityLog, lv_ActivityLoginId INT UNSIGNED ;
declare lv_FromDateActivityLog, lv_ToDateActivityLog datetime;


declare c_ActivityLog cursor  for
 select Id from ActivityLog where  useraccountId <>1 and ActivityGMT between P_FromDate and P_ToDate order by Id;
 
 declare c_ActivityLogin cursor  for
 select Id from ActivityLogin where useraccountId <>1 and ActivityGMT between P_FromDate and P_ToDate order by Id;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

set sql_log_bin=0 ;

-- Check if input datetime passed is NOT within 95 days.

select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -95 DAY) into lv_FromDateActivityLog;
select DATE_ADD(UTC_TIMESTAMP(), INTERVAL -95 DAY)   into lv_ToDateActivityLog;

if (P_FromDate <= lv_FromDateActivityLog) and (P_ToDate <= lv_ToDateActivityLog) THEN

	
	open c_ActivityLog;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c_ActivityLog into lv_ActivityLog;

			DELETE from ActivityLog where Id = lv_ActivityLog;
			
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		
	close c_ActivityLog;
	
	-- For ActivityLogin
	set indexm_pos =0;
	set idmaster_pos =0;
	open c_ActivityLogin;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c_ActivityLogin into lv_ActivityLoginId;
		
			DELETE from ActivityLogin where Id = lv_ActivityLoginId;
			-- select sleep(P_Sleep);
	
		set indexm_pos = indexm_pos +1;
		end while;
		
	close c_ActivityLogin;
	
	
ELSE
	select 'Incorrect Dates. Process aborted' As Status;
END IF;

END $$

DELIMITER ;


DROP PROCEDURE IF EXISTS sp_InsertElavonTransactionDetail;

delimiter //

CREATE PROCEDURE sp_InsertElavonTransactionDetail(in P_PointOfSaleId MEDIUMINT UNSIGNED, in P_Amount MEDIUMINT UNSIGNED, in P_BatchNumber SMALLINT UNSIGNED)

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 declare lv_MerchantAccountId, lv_LocationId,lv_CustomerId,lv_PaystationSettingId,lv_UnifiedRateId MEDIUMINT UNSIGNED ;
 declare lv_PreAuthId,lv_ElavonTransactionDetailId, lv_PurchaseId, lv_ProcessorTransactionId BIGINT UNSIGNED ;
 declare lv_PurchaseGMT datetime;
 
DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
     select concat('ERROR', ' ', P_PointOfSaleId);
  END;

  select utc_timestamp() into lv_PurchaseGMT ;
  
  SELECT MerchantAccountId into lv_MerchantAccountId from MerchantPOS WHERE PointOfSaleId  = P_PointOfSaleId ;
  select CustomerId, LocationId into lv_CustomerId, lv_LocationId from PointOfSale where Id = P_PointOfSaleId ;
  SELECT ID into lv_PaystationSettingId from PaystationSetting WHERE CustomerId = lv_CustomerId  LIMIT 1;
  
  SELECT ifnull(max(ID),0) into lv_UnifiedRateId from UnifiedRate WHERE  CustomerId = lv_CustomerId LIMIT 1;
		 
	IF (lv_UnifiedRateId = 0) THEN
		 
		INSERT INTO UnifiedRate (CustomerId, Name, LastModifiedGMT, LastModifiedByUserId) VALUES
									(lv_CustomerId, 'unknown', UTC_TIMESTAMP(), 1) ;
									
			SELECT LAST_INSERT_ID() INTO lv_UnifiedRateId; 						
		 
	END IF;
		 
	INSERT INTO PreAuth (`ResponseCode`,`ProcessorTransactionId`,`AuthorizationNumber`,`ReferenceNumber`,		`MerchantAccountId`,	`Amount`,`Last4DigitsOfCardNumber`,`CardType`,`IsApproved`,`PreAuthDate`,`CardData`,		`PointOfSaleId`,`ReferenceId`,`Expired`,`CardHash`,`ExtraData`,`PsRefId`,				`CardExpiry`,`IsRFID`) VALUES
				    (NULL,			'',						 FLOOR(RAND() * 50000),	 FLOOR(RAND() * 50000),	 lv_MerchantAccountId,	P_Amount, FLOOR(RAND() * 5000),    'VISA',     0,           UTC_TIMESTAMP(), 'CardData',	P_PointOfSaleId, NULL,			0,		'CardHash',	NULL,		FLOOR(RAND() * 5000),	'1407',			0);


	SELECT LAST_INSERT_ID() INTO lv_PreAuthId; 
  
	INSERT INTO ElavonTransactionDetail ( MerchantAccountId, 	PointOfSaleId, 		PurchasedDate, 	TicketNumber, AccountEntryMode, POSEntryCapability,   CardData, 	TransactionAmount, OriginalAuthAmount, ResponseCode, ApprovalCode, AuthorizationSource, AuthorizedAmount, AuthorizationDateTime, BatchNumber, TraceNumber, TransactionReferenceNbr, 				PS2000Data, 							MSDI, 					ElavonRequestTypeId, 					TransactionSettlementStatusTypeId, 				CreatedGMT ) VALUES
										( lv_MerchantAccountId,	P_PointOfSaleId,	null,			null,			1,				2,				  'CardData',	P_Amount,			P_Amount,		   'AA',		 'CVI334',		2,					P_Amount,		   UTC_TIMESTAMP(),		P_BatchNumber,				203162,		125223510,				'V112025981310332DD9EG',				1,	  							1,			  									1,								 			UTC_TIMESTAMP()); 
 
	SELECT LAST_INSERT_ID() INTO lv_ElavonTransactionDetailId; 

	UPDATE PreAuth SET IsApproved = 1 where Id = lv_PreAuthId ;
 
	INSERT INTO ActiveBatchSummary (MerchantAccountId, 	BatchNumber, 	 CreatedGMT ) VALUES
								 (lv_MerchantAccountId,	P_BatchNumber,	UTC_TIMESTAMP())
								ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id),NoOfTransactions = NoOfTransactions+1 ; 
  	
	-- Create PPP
	
	 INSERT INTO Purchase (CustomerId,PointOfSaleId,		PurchaseGMT,	PurchaseNumber,TransactionTypeId,		PaymentTypeId,		LocationId,		PaystationSettingId,	UnifiedRateId,	OriginalAmount,			ChargedAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CreatedGMT) VALUES
						  (lv_CustomerId, P_PointOfSaleId,	lv_PurchaseGMT,1,				2,						2,					lv_LocationId,	lv_PaystationSettingId,	lv_UnifiedRateId, P_Amount,	P_Amount	, P_Amount,	P_Amount,P_Amount,	UTC_TIMESTAMP());	
		
	SELECT LAST_INSERT_ID() INTO lv_PurchaseId;

   INSERT INTO Permit (PurchaseId,PermitNumber,LocationId,		PermitTypeId,PermitIssueTypeId,LicencePlateId, SpaceNumber,					AddTimeNumber,		PermitBeginGMT,PermitOriginalExpireGMT,													PermitExpireGMT,NumberOfExtensions) VALUES
					  (lv_PurchaseId,1,	lv_LocationId,			1,			1,				   	null,			FLOOR(RAND() * 500), 		FLOOR(RAND() * 5000),			lv_PurchaseGMT, (SELECT DATE_ADD(lv_PurchaseGMT,INTERVAL 30 MINUTE)),(SELECT DATE_ADD(lv_PurchaseGMT,INTERVAL 30 MINUTE)),0);

	INSERT INTO ProcessorTransaction(PreAuthId, PurchaseId,PointOfSaleId,			PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,	Amount,		CardType,	Last4DigitsOfCardNumber,ProcessingDate,ProcessorTransactionId,ReferenceNumber,IsApproved,CardHash) VALUES
									(lv_PreAuthId, lv_PurchaseId,P_PointOfSaleId, 	lv_PurchaseGMT,	1,	2,							lv_MerchantAccountId,	P_Amount, 'VISA',	'1234',							lv_PurchaseGMT,	'123',				'123',				0,		'123');	  				
		 
	 SELECT LAST_INSERT_ID() INTO lv_ProcessorTransactionId;

	 UPDATE ElavonTransactionDetail SET ProcessorTransactionId = lv_ProcessorTransactionId WHERE Id = lv_ElavonTransactionDetailId ;
	 
END//
delimiter ;


-- call sp_InsertElavonTransactionDetail(602, 200,66);




DROP PROCEDURE IF EXISTS sp_BatchSettlement;

delimiter //

CREATE PROCEDURE sp_BatchSettlement(in P_MerchantAccountId MEDIUMINT UNSIGNED, in P_BatchNumber smallint UNSIGNED, in P_NoOfTransactionsFailure MEDIUMINT UNSIGNED)

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos INT UNSIGNED;
 
 declare lv_ActiveBatchSummaryId BIGINT unsigned ;
 declare lv_NoOfTransactions MEDIUMINT UNSIGNED ;
 declare lv_BatchSettlementId BIGINT unsigned ;
 
DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
     select concat('ERROR', ' ', P_MerchantAccountId);
  END;

  
  
  select Id, NoOfTransactions into lv_ActiveBatchSummaryId, lv_NoOfTransactions
  from ActiveBatchSummary
  where MerchantAccountId = P_MerchantAccountId and
  BatchNumber = P_BatchNumber 
  and BatchStatusTypeId in (1,2);
  
  -- INSert into BatchSettlement
  
  INSERT INTO BatchSettlement (ActiveBatchSummaryId, ClusterId, BatchSize, 				BatchSettlementStartGMT) VALUES 
							 (lv_ActiveBatchSummaryId, 1, 		lv_NoOfTransactions, 	UTC_TIMESTAMP());
 
  SELECT LAST_INSERT_ID() INTO lv_BatchSettlementId;
  
  -- INSert into BatchSettlementDetail
    INSERT INTO BatchSettlementDetail  (BatchSettlementId, ElavonTransactionDetailId) 
	SELECT lv_BatchSettlementId, Id from ElavonTransactionDetail
	where MerchantAccountId = P_MerchantAccountId and BatchNumber = P_BatchNumber and TransactionSettlementStatusTypeId =1;
	
  UPDATE ElavonTransactionDetail
  SET TransactionSettlementStatusTypeId = 4 ,
  PreAuthId = NULL 
  WHERE Id in ( select ElavonTransactionDetailId from BatchSettlementDetail where BatchSettlementId = lv_BatchSettlementId ) ;
  
   
  UPDATE BatchSettlement SET BatchSettlementEndGMT  =  UTC_TIMESTAMP() where ID = lv_BatchSettlementId;
  
  UPDATE ActiveBatchSummary SET BatchStatusTypeId = 3, LastModifiedGMT= UTC_TIMESTAMP() where MerchantAccountId = P_MerchantAccountId and BatchNumber = P_BatchNumber ;
	 
END//
delimiter ;


-- call sp_BatchSettlement(1, 66,0);
/*
-- EMS-9705 Script to clear all Active Houston Alerts


DROP PROCEDURE IF EXISTS sp_ClearActiveAlertsForCustomer;

delimiter //

CREATE PROCEDURE `sp_ClearActiveAlertsForCustomer`(P_CustomerId MEDIUMINT UNSIGNED)
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;
declare lv_ClearGMT DATETIME;

declare c1 cursor  for
 select Id from PointOfSale where CustomerId = P_CustomerId order by Id;
 
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;

set indexm_pos =0;
set idmaster_pos =0;

-- To Clear Active Alerts from ActivePOSAlert

START TRANSACTION ;

select UTC_TIMESTAMP() into lv_ClearGMT ;

 -- This is bulk update for ActivePOSAlert
 -- As we are performing update per Customer bulk update is fine
 
 UPDATE ActivePOSAlert APA, PointOfSale  POS 
 SET 
 APA.POSAlertId = NULL,
 APA.IsActive = 0 ,
 APA.ClearedGMT = lv_ClearGMT ,
 APA.VERSION = APA.VERSION + 1
 WHERE 
 APA.PointOfSaleId = POS.ID AND
 POS.CustomerId = P_CustomerId AND
 APA.IsActive = 1 ;
 
 -- This is bulk update for POSAlert
 -- As we are performing update per Customer bulk update is fine
 
 UPDATE POSAlert POSA, PointOfSale  POS 
 SET 
 POSA.IsActive = 0 ,
 POSA.ClearedGMT = lv_ClearGMT ,
 POSA.ClearedByUserId = 1
 WHERE 
 POSA.PointOfSaleId = POS.ID AND
 POS.CustomerId = P_CustomerId AND
 POSA.IsActive = 1 ;

 -- This is to recalculate the counts
 
open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_id;

	call sp_UpdatePaystationAlertCount(lv_id) ;
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

COMMIT; 

select 'Completed' As Status;
END//
delimiter ;
*/

-- EMS-6200

DROP PROCEDURE IF EXISTS sp_UpdateLastModifiedDateOnUnifiedRate;

delimiter //

CREATE PROCEDURE `sp_UpdateLastModifiedDateOnUnifiedRate`()
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;
declare lv_PurchaseGMT DATETIME DEFAULT NULL;
declare lv_UnifiedRateId, lv_CustomerId mediumint ;

declare c1 cursor  for
 select Id,CustomerId from UnifiedRate order by Id;
 
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;



open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_UnifiedRateId, lv_CustomerId;

		SELECT MAX(PurchaseGMT) INTO lv_PurchaseGMT FROM Purchase WHERE CustomerId = lv_CustomerId AND UnifiedRateId = lv_UnifiedRateId;
		
		IF (lv_PurchaseGMT IS NOT NULL) THEN
			UPDATE UnifiedRate
			SET LastModifiedGMT = lv_PurchaseGMT
			WHERE Id = lv_UnifiedRateId ;
		END IF ;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;



-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS sp_InsertPurchase_ForWidgetsTest ;

delimiter //

CREATE PROCEDURE sp_InsertPurchase_ForWidgetsTest(

    IN p_CustomerId MEDIUMINT,    
	IN p_PointOfSaleId MEDIUMINT, 
	IN p_LocationId MEDIUMINT, 
	IN p_PaystationSettingId MEDIUMINT,
	IN P_UnifiedRateId MEDIUMINT,
	IN p_MerchantAccount MEDIUMINT,	
	IN p_FromDate DateTime,	
	IN p_PaymentTypeId tinyint unsigned,
	IN p_OriginalAmount MEDIUMINT,
	IN p_ChargedAmount MEDIUMINT, 
	IN p_CashPaidAmount MEDIUMINT,
	IN p_CoinPaidAmount MEDIUMINT,
	IN p_BillPaidAmount MEDIUMINT,
	IN p_CardPaidAmount MEDIUMINT
	
)
BEGIN

declare lv_Fromdate datetime;
declare lv_counter int default 0;
declare lv_PurchaseId,lv_ProcessorTransactionId bigint UNSIGNED ;
declare p_CashAmount MEDIUMINT ;

-- IN p_ToDate DateTime,
 START TRANSACTION ;


-- call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);

-- Case 2: Credit Card Swipe Only 
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:03:00',  2, 			  1000, 				1000, 			0, 					0,  				0, 				1000);

-- Case 2.1: Credit Card Contactless Only 
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:04:00',  11, 			  500, 				500, 			0, 					0,  				0, 				500);


-- Case 2.2: Credit Card Chip Only 
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:05:00',  13, 			  300, 				300, 			0, 					0,  				0, 				300);

-- Case 2.3: External Only 
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:06:00',  15, 			  400, 				400, 			0, 					0,  				0, 				400);


-- Case 1.2: Legacy Cash Only
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:01:00',  1, 			  200, 				200, 			200, 					0,  				0, 				0);

-- Case 1.1: Non Legacy Cash Only
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:02:00',  1, 			  600, 				600, 			0, 					400,  				200, 				0);

-- Case 4: Passcard Only
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:07:00',  7, 			  100, 				100, 			0, 					0,  				0, 				100);

-- Case 6.1: Legacy Cash + Passcard
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:16:00',  9, 			  800, 				800, 			500, 					0,  				0, 				300);


-- Case 6.2: Non Legacy Cash + Passcard
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:17:00',  9, 			  900, 				900, 			0, 					300,  				200, 				400);

-- Case 5: SmartCard Only
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:08:00',  4, 			  600, 				600, 			0, 					0,  				0, 				600);

-- Case 7.1: Legacy Cash + SmartCard
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:18:00',  6, 			  700, 				700, 			400, 					0,  				0, 				300);

-- Case 7.2: Non Legacy Cash + SmartCard
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:19:00',  6, 			  900, 				900, 			0, 					300,  				200, 				400);



-- Case 3.2: CC Swipe + Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:12:00',  5, 			  600, 				600, 			400, 					0,  				0, 				200);


-- Case 3.1: CC Swipe + Non Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:09:00',  5, 			  1000, 				1000, 			0, 					300,  				200, 				500);

-- Case 3.2.1: CC CL + Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:13:00',  12, 			  600, 				600, 			400, 					0,  				0, 				200);

-- Case 3.1.1: CC CL + Non Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:10:00',  12, 			  1000, 				1000, 			0, 					300,  				200, 				500);

-- Case 3.2.2: CC Chip + Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:14:00',  14, 			  600, 				600, 			400, 					0,  				0, 				200);

-- Case 3.1.2: CC Chip + Non Legacy Cash
--	call sp_InsertPurchase_ForWidgetsTest(p_CustomerId, p_PointOfSaleId, p_LocationId, p_PaystationSettingId, P_UnifiedRateId, p_MerchantAccount, p_FromDate,  			  p_PaymentTypeId, p_OriginalAmount, p_ChargedAmount, p_CashPaidAmount, p_CoinPaidAmount,  p_BillPaidAmount, p_CardPaidAmount);
--  call sp_InsertPurchase_ForWidgetsTest(18, 			598, 				548, 		  12, 				  12, 				6, 				  '2015-09-10 10:11:00',  14, 			  1000, 				1000, 			0, 					300,  				200, 				500);

-- CC


	SET lv_Fromdate = p_FromDate;
	-- WHILE lv_Fromdate < p_ToDate DO 
	
		-- SET p_CashAmount = substring(FLOOR(RAND()*1000),1,3) ;	
		INSERT IGNORE INTO Purchase (CustomerId ,	PointOfSaleId, PurchaseGMT,PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,
						ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT)
				VALUES   (p_CustomerId, p_PointOfSaleId, lv_Fromdate, 1,2,	p_PaymentTypeId,	p_LocationId,	p_PaystationSettingId, P_UnifiedRateId, NULL, p_OriginalAmount,p_ChargedAmount,0,
						0, 				p_CashPaidAmount,	p_CoinPaidAmount,	p_BillPaidAmount,	p_CardPaidAmount,	p_OriginalAmount,p_OriginalAmount,0,0,0,0,UTC_TIMESTAMP()) ;
	    
		SELECT LAST_INSERT_ID() INTO lv_PurchaseId; 
		INSERT IGNORE INTO PurchaseCollection (PurchaseId, PointOfSaleId, PurchaseGMT, 		CashAmount, CoinAmount, CoinCount, BillAmount,BillCount) VALUES
											  (lv_PurchaseId, p_PointOfSaleId, lv_Fromdate,  p_CashAmount,	0,    0,         0,       0);
												
		
		INSERT INTO Permit (PurchaseId,PermitNumber,LocationId,		PermitTypeId,PermitIssueTypeId,LicencePlateId, SpaceNumber,AddTimeNumber,		PermitBeginGMT,PermitOriginalExpireGMT,													PermitExpireGMT,NumberOfExtensions) VALUES
						     (lv_PurchaseId,1,	p_LocationId,	1,			3,				   	NULL,1,270000,										lv_Fromdate, (SELECT DATE_ADD(lv_Fromdate,INTERVAL 30 MINUTE)),(SELECT DATE_ADD(lv_Fromdate,INTERVAL 30 MINUTE)),0);

		IF (p_CardPaidAmount >0) THEN
		
		INSERT INTO ProcessorTransaction (PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,Amount,CardType,
											Last4DigitsOfCardNumber,ProcessingDate,ProcessorTransactionId,ReferenceNumber,IsApproved,CardHash,
										IsUploadedFromBoss,IsRFID) VALUES
										(lv_PurchaseId ,p_PointOfSaleId,lv_Fromdate,1,2,p_CardPaidAmount,'VISA',
										'1234',lv_Fromdate,'0','0',1,'CARDHASH',0,0);
	   
		SELECT LAST_INSERT_ID() INTO lv_ProcessorTransactionId; 
		
		INSERT INTO PaymentCard (PurchaseId,CardTypeId,CreditCardTypeId,
				Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved,MerchantAccountId) VALUES
				( lv_PurchaseId, 1,1,p_CardPaidAmount,'1234',lv_Fromdate, 0,0,1,p_MerchantAccount);
	   
		END IF;
	/*   set lv_counter = lv_counter +1 ;
	   IF (lv_counter = 15000) THEN
	     COMMIT ;
		 set lv_counter = 0;
	   END IF; */
	   
	   
	-- SET lv_Fromdate = DATE_ADD(lv_Fromdate,INTERVAL 30 second);        
    -- END WHILE;
COMMIT; 	
	
 	
END//

delimiter ;



--  Compute Widget Card Amount and Card Count for PPPRevenueMonth


DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth`(IN P_StartingId BIGINT UNSIGNED, IN P_EndingId BIGINT UNSIGNED)
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_PPPDetailMonthId BIGINT UNSIGNED ;
declare lv_PPPRevenueMonthId BIGINT UNSIGNED ;
declare lv_OtherCardAmount,lv_TotalAmount_CC int unsigned;
declare lv_OtherCardCount,lv_TotalCount_CC mediumint unsigned;
declare lv_counter int default 0;

declare c1 cursor  for
 select distinct PPPDetailMonthId from PPPRevenueMonth A where Id Between P_StartingId and P_EndingId 
 and NOT EXISTS ( select PPPDetailMonthId FROM ETLRevenueMonthStatus WHERE PPPDetailMonthId = A.PPPDetailMonthId and IsProcessed = 1) 
 order by Id;
 

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailMonthId;
	
	    
		
		UPDATE ETLRevenueMonthStatus
		SET IsProcessed =9
		WHERE PPPDetailMonthId = lv_PPPDetailMonthId;
	   
		

		SET lv_OtherCardAmount = NULL;
		SET lv_OtherCardCount = NULL;
		
		select sum(TotalAmount), sum(TotalCount)
		into lv_OtherCardAmount, lv_OtherCardCount
		from PPPRevenueMonth
		where PPPDetailMonthId = lv_PPPDetailMonthId AND RevenueTypeId in (8,9);
		
		IF (lv_OtherCardAmount is NOT NULL) and (lv_OtherCardCount IS NOT NULL) THEN
			-- Step 2
			UPDATE PPPRevenueMonth
			SET TotalAmount = TotalAmount - lv_OtherCardAmount,
			TotalCount = TotalCount - lv_OtherCardCount
			WHERE
			PPPDetailMonthId = lv_PPPDetailMonthId and
			RevenueTypeId = 3 ;
			
			-- Step3
			INSERT INTO PPPRevenueMonth (PPPDetailMonthId,RevenueTypeId,TotalAmount,TotalCount) VALUES
			(lv_PPPDetailMonthId, 10, lv_OtherCardAmount,lv_OtherCardCount);
			
			-- Step 4
			SELECT TotalAmount, TotalCount 
			INTO lv_TotalAmount_CC, lv_TotalCount_CC
			FROM PPPRevenueMonth
			WHERE PPPDetailMonthId = lv_PPPDetailMonthId and RevenueTypeId = 3 ;
			
			UPDATE PPPRevenueMonth
			SET TotalAmount = lv_TotalAmount_CC,
			TotalCount = lv_TotalCount_CC
			WHERE PPPDetailMonthId = lv_PPPDetailMonthId and RevenueTypeId = 7 ;
			
		END IF;
		
		UPDATE ETLRevenueMonthStatus
		SET IsProcessed =1 , ProcessedGMT = UTC_TIMESTAMP()
		WHERE PPPDetailMonthId = lv_PPPDetailMonthId;
	   
		
		set lv_counter = lv_counter +1 ;
		
	   
	   
		
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;

close c1;

END//
delimiter ;

-- DAY

--  Compute Widget Card Amount and Card Count for PPPRevenueDay


DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueDay;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueDay`(IN P_StartingId BIGINT UNSIGNED, IN P_EndingId BIGINT UNSIGNED)
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_PPPDetailDayId,lv_PPPRevenueDayId BIGINT UNSIGNED ;
declare lv_OtherCardAmount,lv_TotalAmount_CC int unsigned;
declare lv_OtherCardCount,lv_TotalCount_CC mediumint unsigned;
declare lv_counter int default 0;

declare c1 cursor  for
 select distinct PPPDetailDayId from PPPRevenueDay A where Id Between P_StartingId and P_EndingId 
 and NOT EXISTS ( select PPPDetailDayId FROM ETLRevenueDayStatus WHERE PPPDetailDayId = A.PPPDetailDayId and IsProcessed = 1)
 order by Id;
 
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailDayId;
	
		UPDATE ETLRevenueDayStatus
		SET IsProcessed =9
		WHERE PPPDetailDayId = lv_PPPDetailDayId;
		
		
		SET lv_OtherCardAmount = NULL;
		SET lv_OtherCardCount = NULL;
		
		select sum(TotalAmount), sum(TotalCount)
		into lv_OtherCardAmount, lv_OtherCardCount
		from PPPRevenueDay
		where PPPDetailDayId = lv_PPPDetailDayId AND RevenueTypeId in (8,9);
		
		IF (lv_OtherCardAmount is NOT NULL) and (lv_OtherCardCount IS NOT NULL) THEN
			-- Step 2
			UPDATE PPPRevenueDay
			SET TotalAmount = TotalAmount - lv_OtherCardAmount,
			TotalCount = TotalCount - lv_OtherCardCount
			WHERE
			PPPDetailDayId = lv_PPPDetailDayId and
			RevenueTypeId = 3 ;
			
			-- Step3
			INSERT INTO PPPRevenueDay (PPPDetailDayId,RevenueTypeId,TotalAmount,TotalCount) VALUES
			(lv_PPPDetailDayId, 10, lv_OtherCardAmount,lv_OtherCardCount);
			
			-- Step 4
			SELECT TotalAmount, TotalCount 
			INTO lv_TotalAmount_CC, lv_TotalCount_CC
			FROM PPPRevenueDay
			WHERE PPPDetailDayId = lv_PPPDetailDayId and RevenueTypeId = 3 ;
			
			UPDATE PPPRevenueDay
			SET TotalAmount = lv_TotalAmount_CC,
			TotalCount = lv_TotalCount_CC
			WHERE PPPDetailDayId = lv_PPPDetailDayId and RevenueTypeId = 7 ;
			
		END IF;
		
		UPDATE ETLRevenueDayStatus
		SET IsProcessed =1 , ProcessedGMT = UTC_TIMESTAMP()
		WHERE PPPDetailDayId = lv_PPPDetailDayId;
	   
	   
		
		set lv_counter = lv_counter +1 ;
		
	  
	   
		
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;

close c1;

END//
delimiter ;

-- HOUR

--  Compute Widget Card Amount and Card Count for PPPRevenueHour


DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueHour;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueHour`(IN P_StartingId BIGINT UNSIGNED, IN P_EndingId BIGINT UNSIGNED)
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_PPPDetailHourId,lv_PPPRevenueHourId BIGINT UNSIGNED ;
declare lv_OtherCardAmount,lv_TotalAmount_CC int unsigned;
declare lv_OtherCardCount,lv_TotalCount_CC mediumint unsigned;
declare lv_counter int default 0;

declare c1 cursor  for
 select distinct PPPDetailHourId from PPPRevenueHour A where Id Between P_StartingId and P_EndingId 
 and NOT EXISTS ( select PPPDetailHourId FROM ETLRevenueHourStatus WHERE PPPDetailHourId = A.PPPDetailHourId and IsProcessed = 1)
  order by Id;
 
 
	   
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_PPPDetailHourId;
	
	
		
		UPDATE ETLRevenueHourStatus
		SET IsProcessed =9
		WHERE PPPDetailHourId = lv_PPPDetailHourId;
	   
		
		
		SET lv_OtherCardAmount = NULL;
		SET lv_OtherCardCount = NULL;
		
		select sum(TotalAmount), sum(TotalCount)
		into lv_OtherCardAmount, lv_OtherCardCount
		from PPPRevenueHour
		where PPPDetailHourId = lv_PPPDetailHourId AND RevenueTypeId in (8,9);
		
		IF (lv_OtherCardAmount is NOT NULL) and (lv_OtherCardCount IS NOT NULL) THEN
			-- Step 2
			UPDATE PPPRevenueHour
			SET TotalAmount = TotalAmount - lv_OtherCardAmount,
			TotalCount = TotalCount - lv_OtherCardCount
			WHERE
			PPPDetailHourId = lv_PPPDetailHourId and
			RevenueTypeId = 3 ;
			
			-- Step3
			INSERT INTO PPPRevenueHour (PPPDetailHourId,RevenueTypeId,TotalAmount,TotalCount) VALUES
			(lv_PPPDetailHourId, 10, lv_OtherCardAmount,lv_OtherCardCount);
			
			-- Step 4
			SELECT TotalAmount, TotalCount 
			INTO lv_TotalAmount_CC, lv_TotalCount_CC
			FROM PPPRevenueHour
			WHERE PPPDetailHourId = lv_PPPDetailHourId and RevenueTypeId = 3 ;
			
			UPDATE PPPRevenueHour
			SET TotalAmount = lv_TotalAmount_CC,
			TotalCount = lv_TotalCount_CC
			WHERE PPPDetailHourId = lv_PPPDetailHourId and RevenueTypeId = 7 ;
			
		END IF;
		
		UPDATE ETLRevenueHourStatus
		SET IsProcessed =1 , ProcessedGMT = UTC_TIMESTAMP()
		WHERE PPPDetailHourId = lv_PPPDetailHourId;
	   
		
		set lv_counter = lv_counter +1 ;
		
	 
	   
		
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;

close c1;

END//
delimiter ;

-- sp_ReOrganiseWidgetCardAmount_PPPRevenueHour_Batches

--  Compute Existng Widget Card Amount and Card Count for PPPRevenueHour in Batches

DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueHour_Batches;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueHour_Batches`()
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_MINID, lv_MAXID, lv_StartingId, lv_EndingId BIGINT UNSIGNED ;
declare lv_Id INT UNSIGNED ;

declare c1 cursor  for
 select Id,StartingId,EndingId from  ETLRevenueHourLimit where IsProcessed=0 order by Id;
  
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessed Status on ETLRevenueHourLimit Table';	  
	  ROLLBACK;
	  
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLRevenueHourStatus ;
INSERT INTO ETLRevenueHourStatus (PPPDetailHourId)
SELECT distinct PPPDetailHourId from PPPRevenueHour  order by Id;

delete from ETLRevenueHourLimit;

select MIN(Id),MAX(Id) INTO lv_MINID, lv_MAXID from PPPRevenueHour;

while lv_MINID < lv_MAXID do
	INSERT INTO ETLRevenueHourLimit (StartingId, EndingId) VALUES (lv_MINID,lv_MINID +60000);    
set lv_MINID = lv_MINID +60001;
end while;

COMMIT;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_Id, lv_StartingId, lv_EndingId ;
	
	UPDATE ETLRevenueHourLimit SET IsProcessed = 9
	WHERE ID = lv_Id ;
	
	START TRANSACTION ;
	CALL  sp_ReOrganiseWidgetCardAmount_PPPRevenueHour ( lv_StartingId,  lv_EndingId);
	COMMIT ;

	UPDATE ETLRevenueHourLimit SET IsProcessed = 1, ProcessedGMT = UTC_TIMESTAMP()
	WHERE ID = lv_Id ;
	
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;
close c1;


END//
delimiter ;

-- Day

-- sp_ReOrganiseWidgetCardAmount_PPPRevenueDay_Batches

--  Compute Existng Widget Card Amount and Card Count for PPPRevenueDay in Batches

DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueDay_Batches;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueDay_Batches`()
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_MINID, lv_MAXID, lv_StartingId, lv_EndingId BIGINT UNSIGNED ;
declare lv_Id INT UNSIGNED ;

declare c1 cursor  for
 select Id,StartingId,EndingId from  ETLRevenueDayLimit  where IsProcessed=0  order by Id;
  
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessed Status on ETLRevenueDayLimit Table';      
	   ROLLBACK;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLRevenueDayStatus ;
INSERT INTO ETLRevenueDayStatus (PPPDetailDayId)
SELECT distinct PPPDetailDayId from PPPRevenueDay  order by Id;

delete from ETLRevenueDayLimit;

select MIN(Id),MAX(Id) INTO lv_MINID, lv_MAXID from PPPRevenueDay;

while lv_MINID < lv_MAXID do
	INSERT INTO ETLRevenueDayLimit (StartingId, EndingId) VALUES (lv_MINID,lv_MINID +60000);    
set lv_MINID = lv_MINID +60001;
end while;

COMMIT;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_Id, lv_StartingId, lv_EndingId ;
	
	UPDATE ETLRevenueDayLimit SET IsProcessed = 9
	WHERE ID = lv_Id ;
	START TRANSACTION ;
	CALL  sp_ReOrganiseWidgetCardAmount_PPPRevenueDay ( lv_StartingId,  lv_EndingId);
	COMMIT ;
	UPDATE ETLRevenueDayLimit SET IsProcessed = 1, ProcessedGMT = UTC_TIMESTAMP()
	WHERE ID = lv_Id ;
	COMMIT ;		
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;
close c1;
END//
delimiter ;

-- Month

-- sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth_Batches

--  Compute Existng Widget Card Amount and Card Count for PPPRevenueMonth in Batches

DROP PROCEDURE IF EXISTS sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth_Batches;

delimiter //

CREATE PROCEDURE `sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth_Batches`()
BEGIN

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_MINID, lv_MAXID, lv_StartingId, lv_EndingId BIGINT UNSIGNED ;
declare lv_Id INT UNSIGNED ;

declare c1 cursor  for
 select Id,StartingId,EndingId from  ETLRevenueMonthLimit  where IsProcessed=0  order by Id;
  
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessed Status on ETLRevenueMonthLimit Table';      
	   ROLLBACK;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLRevenueMonthStatus ;
INSERT INTO ETLRevenueMonthStatus (PPPDetailMonthId)
SELECT distinct PPPDetailMonthId from PPPRevenueMonth  order by Id;

delete from ETLRevenueMonthLimit;

select MIN(Id),MAX(Id) INTO lv_MINID, lv_MAXID from PPPRevenueMonth;

while lv_MINID < lv_MAXID do
	INSERT INTO ETLRevenueMonthLimit (StartingId, EndingId) VALUES (lv_MINID,lv_MINID +60000);    
set lv_MINID = lv_MINID +60001;
end while;

COMMIT;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_Id, lv_StartingId, lv_EndingId ;
	
	UPDATE ETLRevenueMonthLimit SET IsProcessed = 9
	WHERE ID = lv_Id ;
	START TRANSACTION ;
	CALL  sp_ReOrganiseWidgetCardAmount_PPPRevenueMonth ( lv_StartingId,  lv_EndingId);
	COMMIT ;
	UPDATE ETLRevenueMonthLimit SET IsProcessed = 1, ProcessedGMT = UTC_TIMESTAMP()
	WHERE ID = lv_Id ;
	COMMIT ;		
		
set indexm_pos = indexm_pos +1;
end while;

COMMIT;
close c1;
END//
delimiter ;


-- Onetime transaction data update on Master and Reporting DB
DROP PROCEDURE IF EXISTS sp_UpdateContactlessTransactions;

delimiter //

CREATE PROCEDURE `sp_UpdateContactlessTransactions`()


BEGIN

-- Execute on Master first and later on Reporting DB

-- Total Contactless transcations on Master DB: 53,231 (as on Sep 16 2015)
-- Total Contactless transcations on Reporting DB: 54,591 (as on Sep 16 2015)

-- A cursor is used to avoid any table locks
-- otherwise we can use simple SQL to update

/* Alternate approach using SQL

-- Tap only
UPDATE Purchase P , ProcessorTransaction PT
SET P.PaymentTypeId = 11 
WHERE PT.PurchaseId = P.Id
AND PT.IsRFID = 1
AND PaymentTypeId = 2;

-- Cash and Tap
UPDATE Purchase P , ProcessorTransaction PT
SET P.PaymentTypeId = 12 
WHERE PT.PurchaseId = P.Id
AND PT.IsRFID = 1
AND PaymentTypeId = 5;

*/

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;
declare lv_PurchaseGMT DATETIME DEFAULT NULL;
declare lv_PurchaseId BIGINT UNSIGNED;
declare lv_PaymentTypeId tinyint unsigned;

-- Get all Contactless Transactions
declare c1 cursor  for
select P.Id, P.PaymentTypeId
from ProcessorTransaction PT inner join Purchase P on PT.PurchaseId = P.Id
where PT.IsRFID =1 ;
 
declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      ROLLBACK;
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_PurchaseId, lv_PaymentTypeId;

		IF (lv_PaymentTypeId = 2) THEN  -- Tap Transactions
			UPDATE Purchase SET PaymentTypeId = 11 WHERE Id = lv_PurchaseId ;
		END IF;
		
		IF (lv_PaymentTypeId = 5) THEN  -- Cash and Tap Transactions
			UPDATE Purchase SET PaymentTypeId = 12 WHERE Id = lv_PurchaseId ;
		END IF;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;

-- Script to update contactless transaction in Widgets for Revenue Day 

DROP PROCEDURE IF EXISTS sp_ReComputeContactlessTransactions_RevenueDay;

delimiter //

CREATE PROCEDURE `sp_ReComputeContactlessTransactions_RevenueDay`()


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;

declare lv_CustomerId mediumint unsigned ;
declare lv_LocationId, lv_UnifiedRateId , lv_PaystationSettingId,lv_ExistingTotalCount mediumint unsigned ;
declare lv_PointOfSaleId, lv_CashPaidAmount, lv_CardPaidAmount mediumint unsigned ;
declare lv_PurchaseId,lv_PPPTotalDayId, lv_PPPDetailDayId bigint unsigned;
declare lv_PurchaseGMT datetime;
declare lv_Customer_Timezone char(25);
declare lv_TimeIdDayLocal mediumint unsigned ;
declare lv_TransactionTypeId, lv_IsCoupon tinyint unsigned;
declare lv_ExistingTotalAmount, lv_ETLPPPRevenueDayTap_LogId INT UNSIGNED;


-- Get all Contactless Transactions for Tap ONLY
declare c1 cursor  for
SELECT distinct
		P.CustomerId,
		P.LocationId,
		P.PointOfSaleId,
		P.Id,		
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.PurchaseGMT,		
		B.PropertyValue	,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.TransactionTypeId,
		if(length(P.CouponId)>0,1,0)	
		from Purchase P
		inner join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL 
      AND P.Id in 
( select distinct P.Id
from ProcessorTransaction PT inner join Purchase P on PT.PurchaseId = P.Id
where  PaymentTypeId in (11,12));
-- PT.IsRfid =1);



declare continue handler for not found set NO_DATA=-1;

 DECLARE continue HANDLER FOR SQLEXCEPTION
  BEGIN
      select lv_CustomerId ;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLPPPRevenueDayTap_Log;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
			  lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon;

	
	INSERT INTO ETLPPPRevenueDayTap_Log (CustomerId, LocationId, PointOfSaleId, PurchaseId, CashPaidAmount, CardPaidAmount, PurchaseGMT, Timezone, 
	UnifiedRateId,PaystationSettingId,  TransactionTypeId, IsCoupon) VALUES
	(lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
	lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon );
	
	SELECT LAST_INSERT_ID() INTO lv_ETLPPPRevenueDayTap_LogId ;
	
	-- Get PPPTotalDay.Id
	SET lv_TimeIdDayLocal = NULL ;
	SET lv_PPPTotalDayId = NULL ;
	SET lv_PPPDetailDayId = NULL;
	SET lv_ExistingTotalAmount = NULL;
	SET lv_ExistingTotalCount = NULL;
	
	
	SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone)) and QuarterOfDay = 0;
	
	UPDATE ETLPPPRevenueDayTap_Log SET TimeIdLocal = lv_TimeIdDayLocal
	WHERE Id = lv_ETLPPPRevenueDayTap_LogId ;
	
	SELECT Id INTO lv_PPPTotalDayId
	FROM PPPTotalDay
	WHERE CustomerId = lv_CustomerId and TimeIdLocal = lv_TimeIdDayLocal ;

	UPDATE ETLPPPRevenueDayTap_Log SET PPPTotalDayId = lv_PPPTotalDayId
	WHERE Id = lv_ETLPPPRevenueDayTap_LogId ;
	
	-- select lv_PPPTotalDayId As 'INFO1';
	-- Get PPPDetailDay.Id
	Select Id INTO lv_PPPDetailDayId
	FROM PPPDetailDay
	WHERE PPPTotalDayId = lv_PPPTotalDayId AND
	PurchaseLocationId = lv_LocationId AND
	PointOfSaleId = lv_PointOfSaleId AND
	UnifiedRateId = lv_UnifiedRateId AND
	PaystationSettingId = lv_PaystationSettingId AND
	TransactionTypeId = lv_TransactionTypeId AND
	IsCoupon = lv_IsCoupon ;
	-- select lv_PPPDetailDayId As 'INFO2';
	
	UPDATE ETLPPPRevenueDayTap_Log SET PPPDetailDayId = lv_PPPDetailDayId
	WHERE Id = lv_ETLPPPRevenueDayTap_LogId ;
	
	-- Get existing TotalAmount in PPPRevenueDay
	
	SELECT TotalAmount, TotalCount into lv_ExistingTotalAmount, lv_ExistingTotalCount
	FROM PPPRevenueDay
	WHERE PPPDetailDayId = lv_PPPDetailDayId AND
	RevenueTypeId = 7 ;
	
	UPDATE ETLPPPRevenueDayTap_Log SET ExistingTotalAmount = lv_ExistingTotalAmount, ExistingTotalCount = lv_ExistingTotalCount
	WHERE Id = lv_ETLPPPRevenueDayTap_LogId ;
	
	
	-- Get PPPRevenue.Id
	UPDATE PPPRevenueDay
	SET TotalAmount = TotalAmount - lv_CardPaidAmount,
	TotalCount = TotalCount - 1 
	WHERE PPPDetailDayId = lv_PPPDetailDayId AND
	RevenueTypeId = 7 ;
	
	INSERT INTO PPPRevenueDay
	(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
	(lv_PPPDetailDayId, 	11,				lv_CardPaidAmount)
	ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_CardPaidAmount, TotalCount = TotalCount + 1;
				
	
	
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;



-- Script to update contactless transaction in Widgets for RevenueMonth

DROP PROCEDURE IF EXISTS sp_ReComputeContactlessTransactions_RevenueMonth;

delimiter //

CREATE PROCEDURE `sp_ReComputeContactlessTransactions_RevenueMonth`()


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;

declare lv_CustomerId mediumint unsigned ;
declare lv_LocationId, lv_UnifiedRateId , lv_PaystationSettingId,lv_ExistingTotalCount mediumint unsigned ;
declare lv_PointOfSaleId, lv_CashPaidAmount, lv_CardPaidAmount mediumint unsigned ;
declare lv_PurchaseId,lv_PPPTotalMonthId, lv_PPPDetailMonthId bigint unsigned;
declare lv_PurchaseGMT datetime;
declare lv_Customer_Timezone char(25);
declare lv_TimeIdMonthLocal mediumint unsigned ;
declare lv_TransactionTypeId, lv_IsCoupon tinyint unsigned;
declare lv_ExistingTotalAmount, lv_ETLPPPRevenueMonthTap_LogId INT UNSIGNED;


-- Get all Contactless Transactions for Tap ONLY
declare c1 cursor  for
SELECT distinct
		P.CustomerId,
		P.LocationId,
		P.PointOfSaleId,
		P.Id,		
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.PurchaseGMT,		
		B.PropertyValue	,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.TransactionTypeId,
		if(length(P.CouponId)>0,1,0)	
		from Purchase P
		inner join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL 
      AND P.Id in 
( select distinct P.Id
from ProcessorTransaction PT inner join Purchase P on PT.PurchaseId = P.Id
where  PaymentTypeId in (11,12));
-- PT.IsRfid =1);


declare continue handler for not found set NO_DATA=-1;

 DECLARE continue HANDLER FOR SQLEXCEPTION
  BEGIN
      select lv_CustomerId ;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLPPPRevenueMonthTap_Log;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
			  lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon;

	
	INSERT INTO ETLPPPRevenueMonthTap_Log (CustomerId, LocationId, PointOfSaleId, PurchaseId, CashPaidAmount, CardPaidAmount, PurchaseGMT, Timezone, 
	UnifiedRateId,PaystationSettingId,  TransactionTypeId, IsCoupon) VALUES
	(lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
	lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon );
	
	SELECT LAST_INSERT_ID() INTO lv_ETLPPPRevenueMonthTap_LogId ;
	
	-- Get PPPTotalMonth.Id
	SET lv_TimeIdMonthLocal = NULL ;
	SET lv_PPPTotalMonthId = NULL ;
	SET lv_PPPDetailMonthId = NULL;
	SET lv_ExistingTotalAmount = NULL;
	SET lv_ExistingTotalCount = NULL;
	
	
	SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone))
		 AND Month = month(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
	
	UPDATE ETLPPPRevenueMonthTap_Log SET TimeIdLocal = lv_TimeIdMonthLocal
	WHERE Id = lv_ETLPPPRevenueMonthTap_LogId ;
	
	SELECT Id INTO lv_PPPTotalMonthId
	FROM PPPTotalMonth
	WHERE CustomerId = lv_CustomerId and TimeIdLocal = lv_TimeIdMonthLocal ;

	UPDATE ETLPPPRevenueMonthTap_Log SET PPPTotalMonthId = lv_PPPTotalMonthId
	WHERE Id = lv_ETLPPPRevenueMonthTap_LogId ;
	
	select lv_PPPTotalMonthId As 'INFO1';
	-- Get PPPDetailMonth.Id
	Select Id INTO lv_PPPDetailMonthId
	FROM PPPDetailMonth
	WHERE PPPTotalMonthId = lv_PPPTotalMonthId AND
	PurchaseLocationId = lv_LocationId AND
	PointOfSaleId = lv_PointOfSaleId AND
	UnifiedRateId = lv_UnifiedRateId AND
	PaystationSettingId = lv_PaystationSettingId AND
	TransactionTypeId = lv_TransactionTypeId AND
	IsCoupon = lv_IsCoupon ;
	select lv_PPPDetailMonthId As 'INFO2';
	
	UPDATE ETLPPPRevenueMonthTap_Log SET PPPDetailMonthId = lv_PPPDetailMonthId
	WHERE Id = lv_ETLPPPRevenueMonthTap_LogId ;
	
	-- Get existing TotalAmount in PPPRevenueMonth
	
	SELECT TotalAmount, TotalCount into lv_ExistingTotalAmount, lv_ExistingTotalCount
	FROM PPPRevenueMonth
	WHERE PPPDetailMonthId = lv_PPPDetailMonthId AND
	RevenueTypeId = 7 ;
	
	UPDATE ETLPPPRevenueMonthTap_Log SET ExistingTotalAmount = lv_ExistingTotalAmount, ExistingTotalCount = lv_ExistingTotalCount
	WHERE Id = lv_ETLPPPRevenueMonthTap_LogId ;
	
	
	-- Get PPPRevenue.Id
	UPDATE PPPRevenueMonth
	SET TotalAmount = TotalAmount - lv_CardPaidAmount,
	TotalCount = TotalCount - 1 
	WHERE PPPDetailMonthId = lv_PPPDetailMonthId AND
	RevenueTypeId = 7 ;
	
	INSERT INTO PPPRevenueMonth
	(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
	(lv_PPPDetailMonthId, 	11,				lv_CardPaidAmount)
	ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_CardPaidAmount, TotalCount = TotalCount + 1;
				
	
	
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;

-- Take backup of existing Revenue widget tables
DROP PROCEDURE IF EXISTS sp_BackupRevenueWidgetTables;

delimiter //

CREATE PROCEDURE `sp_BackupRevenueWidgetTables`()

BEGIN

-- Take backup of existing Revenue widget tables incase of Rollback

Create Table tmp_PPPRevenueHour Select * from PPPRevenueHour ;
Create Table tmp_PPPRevenueDay Select * from PPPRevenueDay ;
Create Table tmp_PPPRevenueMonth Select * from PPPRevenueMonth ;


END//
delimiter ;



DROP PROCEDURE IF EXISTS sp_GenerateVirtualPSDateLimit;


delimiter //

CREATE PROCEDURE sp_GenerateVirtualPSDateLimit (IN P_StartDate Datetime, IN P_EndDate Datetime, IN P_No_of_Days INT)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int(4);
declare lv_id TINYINT;


DECLARE DateNow DATETIME;
DECLARE DateEnd DATETIME;
	
	
declare continue handler for not found set NO_DATA=-1;

set indexm_pos =0;
set idmaster_pos =0;

-- SET DateNow = CONCAT(P_Year,'-01-01 00:00:00');
SET DateNow = P_StartDate;
-- SET DateEnd = DATE_ADD(DateNow,INTERVAL 1 YEAR);
-- SET DateEnd = P_EndDate;
SET DateEnd = P_EndDate;

while DateNow < P_EndDate DO

select concat(date(DATE_ADD(DateNow,INTERVAL P_No_of_Days DAY)),' ','23:59:59') into DateEnd;
If (DateEnd > P_EndDate) then
	set DateEnd = P_EndDate;
end if;

INSERT INTO `VirtualPSTransactionDateLimit` (FromDate, 		ToDate) VALUES
										    (DateNow,		DateEnd);
 
-- select concat(date(DATE_ADD(DateNow,INTERVAL P_No_of_Days DAY)),' ','23:59:59') INTO DateNow ;--
-- SET DateNow = DateNow + INTERVAL 1 SECOND ;
SET DateNow = DateEnd + INTERVAL 1 SECOND;
-- select DateNow;
 

end while;
END //
DELIMITER ;

-- Onetime update for REST Transactions
DROP PROCEDURE IF EXISTS sp_UpdatePaymentTypeRESTTransactions;

delimiter //

CREATE PROCEDURE `sp_UpdatePaymentTypeRESTTransactions`()


BEGIN

-- Execute on Master first and later on Reporting DB

-- Total REST transcations on Master DB: 4,795,663 (as on Sep 23 2015)
-- Total REST transcations on Reporting DB: 5,414,446 (as on Sep 23 2015)

-- A cursor is used to avoid any table locks
-- otherwise we can use simple SQL to update

/* Alternate approach using SQL

-- Tap only


-- Cash and Tap


*/

declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare idmaster_pos2,indexm_pos2 int(4);
declare lv_id INT UNSIGNED ;
declare lv_PurchaseGMT DATETIME DEFAULT NULL;
declare lv_PurchaseId BIGINT UNSIGNED;
declare lv_PaymentTypeId tinyint unsigned;
declare lv_FromDate, lv_ToDate DATETIME ;


declare c1 cursor  for
select FromDate, ToDate, Id
from VirtualPSTransactionDateLimit
where IsProcessedForPurchase =0 ;
 
-- Get all CC External Transactions
 declare c2 cursor  for
select PurchaseId, PaymentTypeId
from ProcessorTransaction PT INNER JOIN Purchase P on PT.PurchaseId = P.Id 
where PT.PointOfSaleId in 
(select POS.Id from PointOfSale POS inner join Paystation P on POS.PaystationId = P.Id and P.PaystationTypeId=9)
and PT.ProcessingDate between lv_FromDate and lv_ToDate ;
 
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessedForPurchase Status on VirtualPSTransactionDateLimit Table';
	  ROLLBACK;
  END;


set indexm_pos =0;
set idmaster_pos =0;

set indexm_pos2 =0;
set idmaster_pos2 =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_FromDate, lv_ToDate, lv_id;

		START TRANSACTION ;
		open c2;
		set idmaster_pos2 = (select FOUND_ROWS());
		while indexm_pos2 < idmaster_pos2 do
		fetch c2 into lv_PurchaseId, lv_PaymentTypeId;

			IF (lv_PaymentTypeId = 2) THEN  -- CC External
				UPDATE Purchase SET PaymentTypeId = 15 WHERE Id = lv_PurchaseId ;
			END IF;
		
		
		set indexm_pos2 = indexm_pos2 +1;
		end while;
		close c2;
	   COMMIT ;
	   UPDATE VirtualPSTransactionDateLimit SET IsProcessedForPurchase = 1, IsProcessedForPurchaseGMT = UTC_TIMESTAMP() WHERE Id = lv_id ;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;

END//
delimiter ;

-- CC External
-- Script to update CC External transaction in Widgets for Revenue Day 

DROP PROCEDURE IF EXISTS sp_ReComputeCCExternalTransactions_RevenueDay;

delimiter //

CREATE PROCEDURE `sp_ReComputeCCExternalTransactions_RevenueDay`( IN P_FromDate DATETIME, IN P_ToDate DATETIME)


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;

declare lv_CustomerId mediumint unsigned ;
declare lv_LocationId, lv_UnifiedRateId , lv_PaystationSettingId,lv_ExistingTotalCount mediumint unsigned ;
declare lv_PointOfSaleId, lv_CashPaidAmount, lv_CardPaidAmount mediumint unsigned ;
declare lv_PurchaseId,lv_PPPTotalDayId, lv_PPPDetailDayId bigint unsigned;
declare lv_PurchaseGMT datetime;
declare lv_Customer_Timezone char(25);
declare lv_TimeIdDayLocal mediumint unsigned ;
declare lv_TransactionTypeId, lv_IsCoupon tinyint unsigned;
declare lv_ExistingTotalAmount, lv_ETLPPPRevenueDayCCExternal_LogId INT UNSIGNED;


-- Get all CC External Transactions
declare c1 cursor  for
SELECT distinct
		P.CustomerId,
		P.LocationId,
		P.PointOfSaleId,
		P.Id,		
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.PurchaseGMT,		
		B.PropertyValue	,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.TransactionTypeId,
		if(length(P.CouponId)>0,1,0)	
		from Purchase P
		inner join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL 
      AND P.Id in 
( select distinct P.Id
from ProcessorTransaction PT inner join Purchase P on PT.PurchaseId = P.Id
where  PaymentTypeId in (15) and ProcessingDate between P_FromDate and P_ToDate);




declare continue handler for not found set NO_DATA=-1;

 DECLARE continue HANDLER FOR SQLEXCEPTION
  BEGIN
      select lv_CustomerId ;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLPPPRevenueDayCCExternal_Log;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
			  lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon;

	
	INSERT INTO ETLPPPRevenueDayCCExternal_Log (CustomerId, LocationId, PointOfSaleId, PurchaseId, CashPaidAmount, CardPaidAmount, PurchaseGMT, Timezone, 
	UnifiedRateId,PaystationSettingId,  TransactionTypeId, IsCoupon) VALUES
	(lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
	lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon );
	
	SELECT LAST_INSERT_ID() INTO lv_ETLPPPRevenueDayCCExternal_LogId ;
	
	-- Get PPPTotalDay.Id
	SET lv_TimeIdDayLocal = NULL ;
	SET lv_PPPTotalDayId = NULL ;
	SET lv_PPPDetailDayId = NULL;
	SET lv_ExistingTotalAmount = NULL;
	SET lv_ExistingTotalCount = NULL;
	
	
	SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone)) and QuarterOfDay = 0;
	
	UPDATE ETLPPPRevenueDayCCExternal_Log SET TimeIdLocal = lv_TimeIdDayLocal
	WHERE Id = lv_ETLPPPRevenueDayCCExternal_LogId ;
	
	SELECT Id INTO lv_PPPTotalDayId
	FROM PPPTotalDay
	WHERE CustomerId = lv_CustomerId and TimeIdLocal = lv_TimeIdDayLocal ;

	UPDATE ETLPPPRevenueDayCCExternal_Log SET PPPTotalDayId = lv_PPPTotalDayId
	WHERE Id = lv_ETLPPPRevenueDayCCExternal_LogId ;
	
	-- select lv_PPPTotalDayId As 'INFO1';
	-- Get PPPDetailDay.Id
	Select Id INTO lv_PPPDetailDayId
	FROM PPPDetailDay
	WHERE PPPTotalDayId = lv_PPPTotalDayId AND
	PurchaseLocationId = lv_LocationId AND
	PointOfSaleId = lv_PointOfSaleId AND
	UnifiedRateId = lv_UnifiedRateId AND
	PaystationSettingId = lv_PaystationSettingId AND
	TransactionTypeId = lv_TransactionTypeId AND
	IsCoupon = lv_IsCoupon ;
	-- select lv_PPPDetailDayId As 'INFO2';
	
	UPDATE ETLPPPRevenueDayCCExternal_Log SET PPPDetailDayId = lv_PPPDetailDayId
	WHERE Id = lv_ETLPPPRevenueDayCCExternal_LogId ;
	
	-- Get existing TotalAmount in PPPRevenueDay
	
	SELECT TotalAmount, TotalCount into lv_ExistingTotalAmount, lv_ExistingTotalCount
	FROM PPPRevenueDay
	WHERE PPPDetailDayId = lv_PPPDetailDayId AND
	RevenueTypeId = 7 ;
	
	UPDATE ETLPPPRevenueDayCCExternal_Log SET ExistingTotalAmount = lv_ExistingTotalAmount, ExistingTotalCount = lv_ExistingTotalCount
	WHERE Id = lv_ETLPPPRevenueDayCCExternal_LogId ;
	
	
	-- Get PPPRevenue.Id
	UPDATE PPPRevenueDay
	SET TotalAmount = TotalAmount - lv_CardPaidAmount,
	TotalCount = TotalCount - 1 
	WHERE PPPDetailDayId = lv_PPPDetailDayId AND
	RevenueTypeId = 7 ;
	
	-- CC External
	INSERT INTO PPPRevenueDay
	(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
	(lv_PPPDetailDayId, 	13,				lv_CardPaidAmount)
	ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_CardPaidAmount, TotalCount = TotalCount + 1;
				
	
	
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;

-- CC External RevenueMonth

-- Script to update CCExternal transaction in Widgets for RevenueMonth

DROP PROCEDURE IF EXISTS sp_ReComputeCCExternalTransactions_RevenueMonth;

delimiter //

CREATE PROCEDURE `sp_ReComputeCCExternalTransactions_RevenueMonth`(IN P_FromDate DATETIME, IN P_ToDate DATETIME)


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;

declare lv_CustomerId mediumint unsigned ;
declare lv_LocationId, lv_UnifiedRateId , lv_PaystationSettingId,lv_ExistingTotalCount mediumint unsigned ;
declare lv_PointOfSaleId, lv_CashPaidAmount, lv_CardPaidAmount mediumint unsigned ;
declare lv_PurchaseId,lv_PPPTotalMonthId, lv_PPPDetailMonthId bigint unsigned;
declare lv_PurchaseGMT datetime;
declare lv_Customer_Timezone char(25);
declare lv_TimeIdMonthLocal mediumint unsigned ;
declare lv_TransactionTypeId, lv_IsCoupon tinyint unsigned;
declare lv_ExistingTotalAmount, lv_ETLPPPRevenueMonthCCExternal_LogId INT UNSIGNED;


-- Get all CC External Transactions 
declare c1 cursor  for
SELECT distinct
		P.CustomerId,
		P.LocationId,
		P.PointOfSaleId,
		P.Id,		
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.PurchaseGMT,		
		B.PropertyValue	,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.TransactionTypeId,
		if(length(P.CouponId)>0,1,0)	
		from Purchase P
		inner join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL 
      AND P.Id in 
( select distinct P.Id
from ProcessorTransaction PT inner join Purchase P on PT.PurchaseId = P.Id
where  PaymentTypeId in (15) and ProcessingDate between P_FromDate and P_ToDate);



declare continue handler for not found set NO_DATA=-1;

 DECLARE continue HANDLER FOR SQLEXCEPTION
  BEGIN
      select lv_CustomerId ;
  END;


set indexm_pos =0;
set idmaster_pos =0;

delete from ETLPPPRevenueMonthCCExternal_Log;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
			  lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon;

	
	INSERT INTO ETLPPPRevenueMonthCCExternal_Log (CustomerId, LocationId, PointOfSaleId, PurchaseId, CashPaidAmount, CardPaidAmount, PurchaseGMT, Timezone, 
	UnifiedRateId,PaystationSettingId,  TransactionTypeId, IsCoupon) VALUES
	(lv_CustomerId,lv_LocationId,lv_PointOfSaleId,lv_PurchaseId,lv_CashPaidAmount, lv_CardPaidAmount,lv_PurchaseGMT,lv_Customer_Timezone,
	lv_UnifiedRateId , lv_PaystationSettingId, lv_TransactionTypeId, lv_IsCoupon );
	
	SELECT LAST_INSERT_ID() INTO lv_ETLPPPRevenueMonthCCExternal_LogId ;
	
	-- Get PPPTotalMonth.Id
	SET lv_TimeIdMonthLocal = NULL ;
	SET lv_PPPTotalMonthId = NULL ;
	SET lv_PPPDetailMonthId = NULL;
	SET lv_ExistingTotalAmount = NULL;
	SET lv_ExistingTotalCount = NULL;
	
	
	SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone))
		 AND Month = month(convert_tz(lv_PurchaseGMT,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
	
	UPDATE ETLPPPRevenueMonthCCExternal_Log SET TimeIdLocal = lv_TimeIdMonthLocal
	WHERE Id = lv_ETLPPPRevenueMonthCCExternal_LogId ;
	
	SELECT Id INTO lv_PPPTotalMonthId
	FROM PPPTotalMonth
	WHERE CustomerId = lv_CustomerId and TimeIdLocal = lv_TimeIdMonthLocal ;

	UPDATE ETLPPPRevenueMonthCCExternal_Log SET PPPTotalMonthId = lv_PPPTotalMonthId
	WHERE Id = lv_ETLPPPRevenueMonthCCExternal_LogId ;
	
	select lv_PPPTotalMonthId As 'INFO1';
	-- Get PPPDetailMonth.Id
	Select Id INTO lv_PPPDetailMonthId
	FROM PPPDetailMonth
	WHERE PPPTotalMonthId = lv_PPPTotalMonthId AND
	PurchaseLocationId = lv_LocationId AND
	PointOfSaleId = lv_PointOfSaleId AND
	UnifiedRateId = lv_UnifiedRateId AND
	PaystationSettingId = lv_PaystationSettingId AND
	TransactionTypeId = lv_TransactionTypeId AND
	IsCoupon = lv_IsCoupon ;
	select lv_PPPDetailMonthId As 'INFO2';
	
	UPDATE ETLPPPRevenueMonthCCExternal_Log SET PPPDetailMonthId = lv_PPPDetailMonthId
	WHERE Id = lv_ETLPPPRevenueMonthCCExternal_LogId ;
	
	-- Get existing TotalAmount in PPPRevenueMonth
	
	SELECT TotalAmount, TotalCount into lv_ExistingTotalAmount, lv_ExistingTotalCount
	FROM PPPRevenueMonth
	WHERE PPPDetailMonthId = lv_PPPDetailMonthId AND
	RevenueTypeId = 7 ;
	
	UPDATE ETLPPPRevenueMonthCCExternal_Log SET ExistingTotalAmount = lv_ExistingTotalAmount, ExistingTotalCount = lv_ExistingTotalCount
	WHERE Id = lv_ETLPPPRevenueMonthCCExternal_LogId ;
	
	
	-- Get PPPRevenue.Id
	UPDATE PPPRevenueMonth
	SET TotalAmount = TotalAmount - lv_CardPaidAmount,
	TotalCount = TotalCount - 1 
	WHERE PPPDetailMonthId = lv_PPPDetailMonthId AND
	RevenueTypeId = 7 ;
	
	-- CCExternal
	INSERT INTO PPPRevenueMonth
	(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
	(lv_PPPDetailMonthId, 	13,				lv_CardPaidAmount)
	ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_CardPaidAmount, TotalCount = TotalCount + 1;
				
	
	
		
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//
delimiter ;

-- ReCompute CCExternal Day Widgets Main
DROP PROCEDURE IF EXISTS sp_ReComputeCCExternalTransactions_RevenueDay_InBatch;

delimiter //

CREATE PROCEDURE `sp_ReComputeCCExternalTransactions_RevenueDay_InBatch`()


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;
declare lv_PurchaseGMT DATETIME DEFAULT NULL;
declare lv_PurchaseId BIGINT UNSIGNED;
declare lv_PaymentTypeId tinyint unsigned;
declare lv_FromDate, lv_ToDate DATETIME ;


declare c1 cursor  for
select FromDate, ToDate, Id
from VirtualPSTransactionDateLimit
where IsProcessedForWidgetDay =0 ;
 
 
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessedForWidgetDay Status on VirtualPSTransactionDateLimit Table';
	  ROLLBACK;
  END;


set indexm_pos =0;
set idmaster_pos =0;



open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_FromDate, lv_ToDate, lv_id;

       START TRANSACTION ;
		CALL sp_ReComputeCCExternalTransactions_RevenueDay(lv_FromDate, lv_ToDate) ;
	   COMMIT ;
	   UPDATE VirtualPSTransactionDateLimit SET IsProcessedForWidgetDay = 1, IsProcessedForWidgetDayGMT = UTC_TIMESTAMP() WHERE Id = lv_id ;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;

END//
delimiter ;


-- ReCompute CCExternal Month Widgets Main
DROP PROCEDURE IF EXISTS sp_ReComputeCCExternalTransactions_RevenueMonth_InBatch;

delimiter //

CREATE PROCEDURE `sp_ReComputeCCExternalTransactions_RevenueMonth_InBatch`()


BEGIN


declare NO_DATA int(4) default 0;
declare idmaster_pos,indexm_pos int(4);
declare lv_id INT UNSIGNED ;
declare lv_PurchaseGMT DATETIME DEFAULT NULL;
declare lv_PurchaseId BIGINT UNSIGNED;
declare lv_PaymentTypeId tinyint unsigned;
declare lv_FromDate, lv_ToDate DATETIME ;


declare c1 cursor  for
select FromDate, ToDate, Id
from VirtualPSTransactionDateLimit
where IsProcessedForWidgetMonth =0 ;
 
 
declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      Select 'Error: Check IsProcessedForWidgetMonth Status on VirtualPSTransactionDateLimit Table';
	  ROLLBACK;
  END;


set indexm_pos =0;
set idmaster_pos =0;



open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_FromDate, lv_ToDate, lv_id;

       START TRANSACTION ;
		CALL sp_ReComputeCCExternalTransactions_RevenueMonth(lv_FromDate, lv_ToDate) ;
	   COMMIT ;
	   UPDATE VirtualPSTransactionDateLimit SET IsProcessedForWidgetMonth = 1, IsProcessedForWidgetMonthGMT = UTC_TIMESTAMP() WHERE Id = lv_id ;
		
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;

END//
delimiter ;


-- EMS-9883 


DROP PROCEDURE IF EXISTS sp_MoveActivePOSAlertToPOSEventCurrent;

delimiter //

CREATE PROCEDURE sp_MoveActivePOSAlertToPOSEventCurrent()

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_CustomerAlertTypeId, lv_PointOfSaleId  mediumint unsigned;
 declare lv_POSAlertId int unsigned;
 declare lv_EventDeviceTypeId, lv_EventStatusTypeId, lv_EventActionTypeId, lv_EventSeverityTypeId,lv_IsActive,lv_HasLevel tinyint unsigned; 
 declare lv_AlertGMT, lv_ClearedGMT datetime;
 
 -- For POSEventCurrent table
 declare lv_POSEventCurrent_EventTypeId tinyint unsigned; 
 declare lv_POSEventCurrent_CustomerAlertTypeId mediumint unsigned;
 declare lv_POSEventCurrent_EventSeverityTypeId, lv_POSEventCurrent_EventActionTypeId, lv_POSEventCurrent_IsActive tinyint unsigned; 
 declare lv_POSEventCurrent_AlertGMT, lv_POSEventCurrent_ClearedGMT datetime;
 declare lv_RowExists tinyint unsigned;
 
 -- Added on April 27 2016 to exclude the Virtual Paystations
declare c1 cursor  for
select CustomerAlertTypeId,  PointOfSaleId , max(POSAlertId), EventDeviceTypeId, EventStatusTypeId, max(EventActionTypeId), max(EventSeverityTypeId), IsActive, max(AlertGMT), max(ClearedGMT)
FROM ActivePOSAlert A 
inner join PointOfSale B on A.PointOfSaleId = B.Id
inner join Paystation C on B.PaystationId = C.Id
 WHERE CustomerAlertTypeId not in (select Id from CustomerAlertType where AlertThresholdTypeId = 12) and C.PaystationTypeId <>9
group by CustomerAlertTypeId, PointOfSaleId , EventDeviceTypeId, EventStatusTypeId,   IsActive
union all
select max(CustomerAlertTypeId),  PointOfSaleId , max(POSAlertId), EventDeviceTypeId, EventStatusTypeId, max(EventActionTypeId), max(EventSeverityTypeId), IsActive, max(AlertGMT), max(ClearedGMT)
FROM ActivePOSAlert  A 
inner join PointOfSale B on A.PointOfSaleId = B.Id
inner join Paystation C on B.PaystationId = C.Id
 WHERE CustomerAlertTypeId  in (select Id from CustomerAlertType where AlertThresholdTypeId = 12) and C.PaystationTypeId <>9
group by  PointOfSaleId , EventDeviceTypeId, EventStatusTypeId,   IsActive;


declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

DELETE FROM POSEventCurrent ;

open c1;
set idmaster_pos = (select FOUND_ROWS());

 

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerAlertTypeId, lv_PointOfSaleId,lv_POSAlertId,lv_EventDeviceTypeId, lv_EventStatusTypeId, lv_EventActionTypeId, lv_EventSeverityTypeId,lv_IsActive,
			   lv_AlertGMT, lv_ClearedGMT;

	-- Reset all variable for POSEventCurrent
	SET lv_POSEventCurrent_CustomerAlertTypeId = NULL ;
	SET lv_HasLevel = 0; 
	SET lv_POSEventCurrent_EventActionTypeId = NULL ;
	
	-- Get EventTypeId:
	-- Added this block on April 27 2016 to Get the EventType
	
	SET lv_POSEventCurrent_EventTypeId = NULL ;
	
	IF lv_EventDeviceTypeId = 20 THEN
		SET lv_POSEventCurrent_EventTypeId = 46 ;
	ELSE
		SELECT EventTypeId INTO lv_POSEventCurrent_EventTypeId FROM EventDefinition WHERE 
		EventDeviceTypeId = lv_EventDeviceTypeId and EventStatusTypeId = lv_EventStatusTypeId and 
		EventActionTypeId = lv_EventActionTypeId and EventSeverityTypeId = lv_EventSeverityTypeId ;
		
		IF lv_POSEventCurrent_EventTypeId IS NULL THEN
			SELECT EventTypeId INTO lv_POSEventCurrent_EventTypeId FROM EventDefinition WHERE 
			EventDeviceTypeId = lv_EventDeviceTypeId and EventStatusTypeId = lv_EventStatusTypeId and 
			EventActionTypeId = lv_EventActionTypeId ;
			
		END IF;
		
	END IF;
	
	-- Customer Defined Alert
	IF lv_EventDeviceTypeId = 20 THEN
		SET lv_POSEventCurrent_CustomerAlertTypeId = lv_CustomerAlertTypeId ;
	ELSE
		SET lv_POSEventCurrent_CustomerAlertTypeId = NULL;
	END IF;
	
	-- For POSEventCurrent.EventActionTypeId
	SELECT HasLevel into lv_HasLevel
	FROM EventType WHERE Id = lv_POSEventCurrent_EventTypeId ;
	
	IF lv_HasLevel = 1 THEN
		SET lv_POSEventCurrent_EventActionTypeId = lv_EventActionTypeId ;
	END IF;
	
	-- For POSEventCurrent.EventSeverityTypeId
	SET lv_POSEventCurrent_EventSeverityTypeId = lv_EventSeverityTypeId ;
	
	SET lv_POSEventCurrent_IsActive = lv_IsActive;
	SET lv_POSEventCurrent_AlertGMT = lv_AlertGMT;
	SET lv_POSEventCurrent_ClearedGMT = lv_ClearedGMT;
	
	-- Code added on Feb 01 2016 (EMS-11142) / Modified on Feb 09 2016
	
		-- Select if the record already exists. If Yes then Update the existing record
		SELECT count(*) into lv_RowExists FROM POSEventCurrent WHERE PointOfSaleId = lv_PointOfSaleId AND EventTypeId=lv_POSEventCurrent_EventTypeId ;
		IF (lv_RowExists = 1) THEN
			UPDATE POSEventCurrent SET EventSeverityTypeId = lv_POSEventCurrent_EventSeverityTypeId,
			EventActionTypeId = lv_POSEventCurrent_EventActionTypeId ,
			IsActive = lv_POSEventCurrent_IsActive ,
			AlertGMT = lv_POSEventCurrent_AlertGMT ,
			ClearedGMT = lv_POSEventCurrent_ClearedGMT 
			WHERE PointOfSaleId = lv_PointOfSaleId AND 
			EventTypeId=lv_POSEventCurrent_EventTypeId ;
		
		ELSE
			INSERT INTO POSEventCurrent (PointOfSaleId, EventTypeId, 						CustomerAlertTypeId, 					EventSeverityTypeId, 					EventActionTypeId, 						IsActive, 					AlertGMT, 					ClearedGMT, 					VERSION) VALUES
								(lv_PointOfSaleId, lv_POSEventCurrent_EventTypeId,	lv_POSEventCurrent_CustomerAlertTypeId,	lv_POSEventCurrent_EventSeverityTypeId,	lv_POSEventCurrent_EventActionTypeId,	lv_POSEventCurrent_IsActive,lv_POSEventCurrent_AlertGMT,lv_POSEventCurrent_ClearedGMT,	0);		
	
		END IF;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;		-- ASHOK2	   
 
 
END//
delimiter ;

-- EMS-9882


DROP PROCEDURE IF EXISTS sp_MovePOSAlertToPOSEventHistory;

delimiter //

CREATE PROCEDURE sp_MovePOSAlertToPOSEventHistory( IN P_CustomerId MEDIUMINT)

 BEGIN

 
 declare NO_DATA int(4) default 0;
 declare idmaster_pos,indexm_pos BIGINT UNSIGNED;
 declare lv_CustomerAlertTypeId, lv_PointOfSaleId  mediumint unsigned;
 declare lv_EventDeviceTypeId, lv_EventStatusTypeId, lv_EventActionTypeId, lv_EventSeverityTypeId,lv_IsActive,lv_HasLevel tinyint unsigned; 
 declare lv_AlertGMT, lv_ClearedGMT datetime;
 declare lv_ClearedByUserId mediumint unsigned;
 declare lv_CreatedGMT datetime ;
 declare lv_CntEventTypeId TINYINT unsigned default 0; 
  
 -- For POSEventHistory table
 declare lv_POSEventHistory_EventTypeId tinyint unsigned; 
 declare lv_POSEventHistory_CustomerAlertTypeId mediumint unsigned;
 declare lv_POSEventHistory_EventSeverityTypeId, lv_POSEventHistory_EventActionTypeId, lv_POSEventHistory_IsActive tinyint unsigned; 
 declare lv_POSEventHistory_TimestampGMT, lv_POSEventHistory_CreatedGMT datetime;
 declare lv_POSEventHistory_LastModifiedByUserId mediumint unsigned;

-- April 27 2016
-- Adding the condition to exclude virtual paystation's Alert data
 
declare c1 cursor  for
select CustomerAlertTypeId, PointOfSaleId, EventDeviceTypeId, EventStatusTypeId, EventActionTypeId, EventSeverityTypeId, AlertGMT, IsActive, ClearedGMT, 
ifNULL(ClearedByUserId,1), CreatedGMT
FROM POSAlert A INNER JOIN PointOfSale B on A.PointOfSaleId = B.Id 
inner join Paystation C on B.PaystationId = C.Id
where B.CustomerId = P_CustomerId
and C.PaystationTypeId <>9; 

declare continue handler for not found set NO_DATA=-1;


set indexm_pos =0;
set idmaster_pos =0;

-- re-write this 
-- DELETE FROM POSEventHistory ;

open c1;
set idmaster_pos = (select FOUND_ROWS());

 

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerAlertTypeId, lv_PointOfSaleId,lv_EventDeviceTypeId, lv_EventStatusTypeId, lv_EventActionTypeId, lv_EventSeverityTypeId, lv_AlertGMT, lv_IsActive, lv_ClearedGMT,
lv_ClearedByUserId , lv_CreatedGMT ;

	-- Reset all variable for POSEventCurrent
	SET lv_POSEventHistory_CustomerAlertTypeId = NULL ;
	SET lv_HasLevel = 0; 
	SET lv_POSEventHistory_EventActionTypeId = NULL ;
	
	-- Get EventTypeId:
	SELECT count(EventTypeId) INTO lv_CntEventTypeId FROM EventDefinition WHERE 
	EventDeviceTypeId = lv_EventDeviceTypeId and EventStatusTypeId = lv_EventStatusTypeId and 
	EventActionTypeId = lv_EventActionTypeId and EventSeverityTypeId = lv_EventSeverityTypeId ;
	
	IF (lv_CntEventTypeId = 1) THEN
	
		SELECT EventTypeId INTO lv_POSEventHistory_EventTypeId FROM EventDefinition WHERE 
		EventDeviceTypeId = lv_EventDeviceTypeId and EventStatusTypeId = lv_EventStatusTypeId and 
		EventActionTypeId = lv_EventActionTypeId and EventSeverityTypeId = lv_EventSeverityTypeId ;
	
	END IF ;
	-- Customer Defined Alert
	IF lv_EventDeviceTypeId = 20 THEN
		SET lv_POSEventHistory_CustomerAlertTypeId = lv_CustomerAlertTypeId ;
		SET lv_POSEventHistory_EventTypeId = 46 ;
	ELSE
		SET lv_POSEventHistory_CustomerAlertTypeId = NULL;
	END IF;
	
	IF lv_EventSeverityTypeId = 0 THEN
		SET lv_POSEventHistory_TimestampGMT = lv_ClearedGMT ;
	ELSE
		SET lv_POSEventHistory_TimestampGMT = lv_AlertGMT ;
	END IF;
	
	-- For POSEventHistory.EventActionTypeId
	SELECT HasLevel into lv_HasLevel
	FROM EventType WHERE Id = lv_POSEventHistory_EventTypeId ;
	
	IF lv_HasLevel = 1 THEN
		SET lv_POSEventHistory_EventActionTypeId = lv_EventActionTypeId ;
	END IF;
	
	-- For POSEventHistory.EventSeverityTypeId
	SET lv_POSEventHistory_EventSeverityTypeId = lv_EventSeverityTypeId ;
	

	INSERT INTO POSEventHistory (PointOfSaleId, EventTypeId, 						CustomerAlertTypeId, 					EventSeverityTypeId, 					EventActionTypeId, 						IsActive, 					TimestampGMT, 					CreatedGMT, 	LastModifiedByUserId) VALUES
								(lv_PointOfSaleId, lv_POSEventHistory_EventTypeId,	lv_POSEventHistory_CustomerAlertTypeId,	lv_POSEventHistory_EventSeverityTypeId,	lv_POSEventHistory_EventActionTypeId,	lv_IsActive,				lv_POSEventHistory_TimestampGMT,lv_CreatedGMT,	lv_ClearedByUserId);		
	
set indexm_pos = indexm_pos +1;
end while;

close c1;
COMMIT ;		-- ASHOK2	   
 
 
END//
delimiter ;

-- 
-- Find missing Default data in POSEventCurrent
-- On Production there are 8 POSId's which does not have default Alert. All belong to Impark Vancouver

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_FindAndInsertDefaultAlerts $$
CREATE PROCEDURE sp_FindAndInsertDefaultAlerts ()
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,indexm_pos_i,idmaster_pos_i int(4);
declare lv_NoOfRecords int;
declare lv_POSId,lv_CustomerId, lv_CustomerAlertTypeId MEDIUMINT;

-- Added on April 27 2016 to exclude virtual paystations

declare c_FindMissingDefaultAlerts cursor  for
 select count(*), PointOfSaleId, POS.CustomerId  
 from POSEventCurrent EC inner join PointOfSale POS on EC.PointOfSaleId = POS.Id
inner join Paystation PS on POS.PaystationId = PS.Id
 inner join CustomerAlertType CAT on EC.CustomerAlertTypeId = CAT.Id
 where  POS.CustomerId = CAT.CustomerId and CAT.IsDefault =1 and CAT.AlertThresholdTypeId <> 12
and PS.PaystationTypeId <>9
 group by PointOfSaleId,POS.CustomerId
 Having count(*) < 8; 
 
 declare c_InsertMissingDefaultAlerts cursor  for
 select Id from CustomerAlertType where CustomerId = lv_CustomerId and IsDefault=1 and AlertThresholdTypeId<>12  and Id not in 
( Select  CustomerAlertTypeId from POSEventCurrent where PointOfSaleId = lv_POSId and CustomerAlertTypeId IS NOT NULL
);

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

set indexm_pos_i =0;
set idmaster_pos_i =0;

	
	open c_FindMissingDefaultAlerts;
		set idmaster_pos = (select FOUND_ROWS());

		while indexm_pos < idmaster_pos do
		fetch c_FindMissingDefaultAlerts into lv_NoOfRecords, lv_POSId, lv_CustomerId;

		
			set indexm_pos_i =0;
			set idmaster_pos_i =0;
			open c_InsertMissingDefaultAlerts;
				set idmaster_pos_i = (select FOUND_ROWS());

					while indexm_pos_i < idmaster_pos_i do
					fetch c_InsertMissingDefaultAlerts into lv_CustomerAlertTypeId;
		
					 INSERT  POSEventCurrent (CustomerAlertTypeId,       EventTypeId,   PointOfSaleId,      EventSeverityTypeId, IsActive, AlertGMT, ClearedGMT, VERSION) 
					 SELECT                   Id                 , 46 AS EventTypeId, 	lv_POSId,          2 AS EventSeverityTypeId, 0       , NULL    , NULL      , 0        
					 FROM    CustomerAlertType 
					 WHERE   CustomerId = lv_CustomerId 
					 AND     AlertThresholdTypeId != 12      
					 AND     IsActive = 1             
					 AND     IsDeleted = 0            
					 AND     RouteId IS NULL         
					 AND     LocationId IS NULL      
					 AND ID = lv_CustomerAlertTypeId;
			
	
			set indexm_pos_i = indexm_pos_i +1;
			end while;		
			close c_InsertMissingDefaultAlerts;
		
		set indexm_pos = indexm_pos +1;
		end while;
		
	close c_FindMissingDefaultAlerts;
END $$

DELIMITER ;

