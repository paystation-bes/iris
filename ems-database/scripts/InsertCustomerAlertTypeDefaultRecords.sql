


INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                    1,       NULL,    NULL,   'Communication Alert Default',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                   2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1 
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1 
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1 
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1 
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 

INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
select Id,                  12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             1 
from Customer where IsParent = 0 and CustomerStatusTypeId <> 3; 
                                    
                        
-- ActivePOSAlert

INSERT  ActivePOSAlert (CustomerAlertTypeId,   PointOfSaleId, POSAlertId,       EventDeviceTypeId,       EventStatusTypeId,      EventActionTypeId,      EventSeverityTypeId, IsActive, AlertGMT, AlertInfo, CreatedGMT) 
        SELECT                  cat.Id                 , pos.Id, NULL      , 20 AS EventDeviceTypeId, 23 AS EventStatusTypeId, 1 AS EventActionTypeId, 2 AS EventSeverityTypeId, 0       , NULL    , NULL     , UTC_TIMESTAMP()        
        FROM    CustomerAlertType cat INNER JOIN PointOfSale pos ON cat.CustomerId = pos.CustomerId
		INNER JOIN POSStatus possts ON possts.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id
		where cat.IsDefault = 1  and   possts.IsDeleted = 0 and possts.IsDecommissioned = 0  and ps.PaystationTypeId<>9     
        ORDER BY cat.Id;
        
INSERT  POSEventCurrent (CustomerAlertTypeId, EventTypeId, PointOfSaleId,      EventSeverityTypeId, IsActive, AlertGMT, ClearedGMT, VERSION) 
        SELECT           cat.Id             ,          46,        pos.Id,                        2, 0       , NULL    , NULL      , 0        
        FROM    CustomerAlertType cat INNER JOIN PointOfSale pos ON cat.CustomerId = pos.CustomerId
		INNER JOIN POSStatus possts ON possts.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id
		where cat.IsDefault = 1  and   possts.IsDeleted = 0 and possts.IsDecommissioned = 0  and ps.PaystationTypeId<>9     
        ORDER BY cat.Id;
        