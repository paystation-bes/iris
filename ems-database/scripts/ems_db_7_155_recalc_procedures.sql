-- Last Modified on March 27
-- JIRA 5225
-- Re-enabling the code to get the Coin Count and Bill Count from POSServiceState as these fields will not get updated for New PSAppVersion

-- Modified on Oct 23
-- JIRA 4529
-- Added the condition Amount > 0  while fetching UnSettledCreditCardCount in ReCalc

-- Modified on Oct 7
-- Removed Code to select/Update from POSServiceState

-- Modified on Oct 1
-- Removed RecalLagSeconds and ReCalcsPerSecond

--  Modified on Oct 1
-- JIRA: 4438 and 3924

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_CollectionSetLock
--
-- Purpose:     Locks a group of point-of-sales (pay stations) for a separate colletions balance recalculation.
--              Procedure sp_CollectionRecalcBalance performs the actual balance recalc.
--
--              A point-of-sale can only be locked if it has been flagged as 'recalcable'.
--              Flagging a point-of-sale as recalcable means attribute POSBalance.IsRecalcable has been
--              assigned a non-zero (positive) value.
--
--              Permitted values for POSBalance.IsRecalcable
--
--              Value   Meaning
--              -----   ------------------------------------------------------------------------------------------
--              0       the point-of-sale is not recalcable because there has been no activity for the point-of-sale, this is the default state, 
--                      after a recalc by procedure sp_CollectionRecalcBalance the value is reset to zero
--
--              1       there has been a Purchase for the point-of-sale, 
--                      the value is set by trigger tr_PurchaseCollection_insert
--
--              2       there has been an Audit, Bill, Coin or Card Collection Report received from the point-of-sale, 
--                      the value is set by trigger tr_POSCollection_insert
--
--              3       a stale lock has been removed from a point-of-sale 
--                      the value is set by procedure sp_CollectionReleaseLock
--
--              4       a point-of-sale has unsettled preauths
--                      the value is set by procedure sp_CollectionFlagUnsettledPreAuth
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_CollectionSetLock;

delimiter //

CREATE PROCEDURE sp_CollectionSetLock (IN Cluster_Id TINYINT UNSIGNED, IN Lock_PS SMALLINT UNSIGNED)
BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release
    
    -- Declare local variables
    DECLARE v_PointOfSaleId         INT UNSIGNED;
    DECLARE v_CollectionLockId      INT UNSIGNED DEFAULT 0;
    DECLARE v_Lockable              INT UNSIGNED DEFAULT 0;     -- there may be more lockable than locked point-of-sales
    DECLARE v_Locked                INT UNSIGNED DEFAULT 0;     -- the max number of locked point-of-sales is set by incoming parameter Lock_PS (default value = 25)
    
    -- Cursor for retrieving unlocked point-of-sales that are ready for a recalc
    DECLARE CursorDone INT DEFAULT 0;
    DECLARE LockCursor CURSOR FOR   SELECT  PointOfSaleId                                          
                                    FROM    POSBalance
                                    WHERE   IsRecalcable > 0 
                                    AND     ClusterId IS NULL
                                    AND     NextRecalcGMT < UTC_TIMESTAMP()               
                                    ORDER BY NextRecalcGMT ASC;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
    
    -- A Cluster Id must be specified    
    IF Cluster_Id > 0 THEN
    
        -- Open the cursor
        OPEN LockCursor;

        -- Loop through each row returned by cursor
        Loop_For_Cursor: LOOP

            -- Get Id of point-of-sale
            FETCH LockCursor INTO v_PointOfSaleId ;
        
            -- Are we done?
            IF CursorDone THEN  
                LEAVE Loop_For_Cursor;
            END IF;
        
            -- On first point-of-sale create a CollectionLock record
            IF v_Locked = 0 THEN
                -- Get a Collection Lock  (note: the new lock number will be returned at the end of this procedure)
                INSERT  CollectionLock (ClusterId, LockGMT, BatchSize) VALUES (Cluster_Id, UTC_TIMESTAMP(), Lock_PS);
                SET v_CollectionLockId = LAST_INSERT_ID();
            END IF;
                
            -- Lock a point-of-sale
            IF v_Locked < Lock_PS THEN 
                UPDATE  POSBalance B
                SET     B.ClusterId = Cluster_Id, 
                        B.CollectionLockId = v_CollectionLockId
                WHERE   B.PointOfSaleId = v_PointOfSaleId;
            
                -- Count number of locked point-of-sale
                SET v_Locked = v_Locked + 1;
            END IF;
            
            -- Count the total number of lockable point-of-sale
            SET v_Lockable = v_Lockable + 1;
            
        -- End of loop for cursor
        END LOOP Loop_For_Cursor;
    
        -- Close the cursor
        CLOSE LockCursor;
        
        -- Save the number of point-of-sales locked
        UPDATE  CollectionLock SET LockableCount = v_Lockable, LockedCount = v_Locked, LockGMT = UTC_TIMESTAMP() WHERE Id = v_CollectionLockId; 
    END IF;
           
    -- Return the Collection Lock Id 
    SELECT v_CollectionLockId AS CollectionLockId;
    
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_CollectionFlagUnsettledPreAuth
--
-- Purpose:     Finds point-of-sales with unsettled preauths and marks these point-of-sales
--              as recalcable for separate collections (sets POSBalance.IsRecalcable = 4).
--
-- Note:        This procedure does not require a ClusterId.
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release          
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_CollectionFlagUnsettledPreAuth;

delimiter //
CREATE PROCEDURE sp_CollectionFlagUnsettledPreAuth (IN PreAuth_Delay SMALLINT UNSIGNED)
BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release
    
    -- Declare local variables 
    DECLARE v_PointOfSaleId INT UNSIGNED;
    
    -- Cursor for retrieving point-of-sales with unsettled preauths
		
    DECLARE CursorDone INT DEFAULT 0;
    DECLARE PreauthCursor CURSOR FOR    SELECT  DISTINCT P.PointOfSaleId
                                        FROM    PreAuth P, POSBalance B 
                                        WHERE   P.PointOfSaleId = B.PointOfSaleId 
                                        AND     P.PreAuthDate > IF(B.LastCashCollectionGMT>B.LastCardCollectionGMT,B.LastCashCollectionGMT,B.LastCardCollectionGMT)     -- PreAuth is after last Collection for point-of-sale (collection type is Audit or Card) 
										AND     P.PreAuthDate < DATE_SUB(UTC_TIMESTAMP(),INTERVAL PreAuth_Delay SECOND)                                                 -- ignore most recent PreAuths because they may be in the process of being settled
                                        AND     P.IsApproved = 1                                                                                                        -- PreAuth was approved (pre-condition for unsettled preauth)
                                        AND     P.Expired = 0                                                                                                           -- PreAuth has not expired (pre-condition for unsettled preauth)
                                        AND     B.ClusterId IS NULL                                                                                                     -- PointOfSale is not already assigned to a Cluster for recalc
                                        AND     B.CollectionLockId IS NULL                                                                                              -- PointOfSale is not already locked for recalc
                                        AND     B.IsRecalcable = 0;                                                                                                     -- PointOfSale is not already flagged for recalc for some other recalc reason
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
    
    -- Open the cursor
    OPEN PreauthCursor;

    -- Loop through each row returned by cursor
    Loop_For_Cursor: LOOP

        -- Get Id of point-of-sale 
        FETCH PreauthCursor INTO v_PointOfSaleId ;
        
        -- Are we done?
        IF CursorDone THEN 
            LEAVE Loop_For_Cursor;
        END IF;
        
        -- Perform update: the point-of-sale has an unsettled preauth, make the point-of-sale recalcable 
        UPDATE  POSBalance B
        SET     B.IsRecalcable = 4
        WHERE   B.PointOfSaleId = v_PointOfSaleId;
              
    -- End of loop for cursor
    END LOOP Loop_For_Cursor;
    
    -- Close the cursor
    CLOSE PreauthCursor;
    
    -- Procedure is complete
    SELECT 0 AS ExecuteComplete;
    
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_CollectionReleaseLock
-- 
-- Purpose:     Finds point-of-sales with 'stale' separate collections (recalc) locks and releases the lock so that 
--              a new lock can be assigned to the point-of-sale in the future by sp_CollectionSetLock. 
--
--              This procedure sets POSBalance.IsRecalcable = 3.
--
--              A stale lock refers to a point-of-sale that was previously marked as recalcable (was assigned a 
--              ClusterId and a CollectionLockId) but for some reason the point-of-sale has not yet been recalced.
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release   
--
-- Source Code Iteration = 2
-- Date: 2013-02-26        Due to stale locks persisting within EMS 6 and thus preventing point-of-sales from
--                         having balance recalcs ignore the incoming parameter Cluster_Id and release all 
--                         stale locks for all clusters.  
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_CollectionReleaseLock;

delimiter //
CREATE PROCEDURE sp_CollectionReleaseLock (IN Cluster_Id TINYINT UNSIGNED)
BEGIN

    -- Source Code Iteration = 2
    -- Date: 2013-02-26        Due to stale locks persisting within EMS 6 and thus preventing point-of-sales from
    --                         having balance recalcs ignore the incoming parameter Cluster_Id and release all 
    --                         stale locks for all clusters.   
    
    -- Declare local variables 
    DECLARE v_PointOfSaleId INT UNSIGNED;
    
    -- Cursor for retrieving point-of-sales with locks that need to be released
    DECLARE CursorDone INT DEFAULT 0;
    DECLARE ReleaseCursor CURSOR FOR        SELECT  B.PointOfSaleId                                      
                                            FROM    POSBalance B, CollectionLock L
                                            WHERE   B.CollectionLockId = L.Id
                                            AND     B.IsRecalcable > 0 
                                            AND     B.ClusterId > 0
                                            AND     B.NextRecalcGMT < UTC_TIMESTAMP()
                                            AND     L.LockGMT < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 60 MINUTE); -- Important: The current stale lock is at least 60 minutes old (could be changed in the future)
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
    
    -- Open the cursor
    OPEN ReleaseCursor;

    -- Loop through each row returned by cursor
    Loop_For_Cursor: LOOP

        -- Get Id of locked point-of-sale
        FETCH ReleaseCursor INTO v_PointOfSaleId ;
        
        -- Are we done?
        IF CursorDone THEN 
            LEAVE Loop_For_Cursor;
        END IF;
        
        -- Perform update: release the existing stale lock and make the point-of-sale recalcable (will be assigned a new lock in the near future)
        UPDATE  POSBalance B
        SET     B.IsRecalcable = 3 ,
                B.ClusterId = NULL ,
                B.CollectionLockId = NULL
        WHERE   B.PointOfSaleId = v_PointOfSaleId;
              
    -- End of loop for cursor
    END LOOP Loop_For_Cursor;
    
    -- Close the cursor
    CLOSE ReleaseCursor;
   
    -- Return status
    SELECT 0 AS Status;
    
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------
-- Trigger:     tr_PurchaseCollection_insert
--
-- Purpose:     On INSERT of a PurchaseCollection record UPDATE the POSBalance record for the corresponding 
--              point-of-sale and SET IsRecalcable = 1 (but only if IsRecalcable = 0, which prevents any 
--              unnecessary updates).
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release   
-- ----------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS tr_PurchaseCollection_insert;

delimiter //
CREATE TRIGGER tr_PurchaseCollection_insert AFTER INSERT ON PurchaseCollection
FOR EACH ROW BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release   
    
    -- Perform a SELECT to see if an UPDATE is required, only perform UPDATE if IsRecalcable = 0 -- in (0,2))
	-- Added on April 21
	IF (New.PurchaseGMT) > '2012-10-01 00:00:00' THEN
	
    IF (SELECT COUNT(*) FROM POSBalance WHERE PointOfSaleId = NEW.PointOfSaleId AND IsRecalcable =0) = 1 THEN
        -- Don't issue an UPDATE that updates zero rows because this zero row UPDATE will be replicated
        UPDATE  POSBalance
        SET     IsRecalcable = 1
        WHERE   PointOfSaleId = NEW.PointOfSaleId;
    END IF;
	END IF ;
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------
-- Trigger:     tr_POSCollection_insert
--
-- Purpose:     On INSERT of a POSCollection record UPDATE the POSBalance record for the corresponding 
--              point-of-sale and SET IsRecalcable = 2 (but only if IsRecalcable = 0, which prevents any 
--              unnecessary updates).
--
--              This trigger logic must account for the arrival of out-of-sequence collections reports coming 
--              up from the paystation.
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release   
-- ----------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS tr_POSCollection_insert;

delimiter //
CREATE TRIGGER tr_POSCollection_insert AFTER INSERT ON POSCollection
FOR EACH ROW BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release 
    
    -- Local variables for lastest collection dates from matching POSBalance record
    DECLARE v_LastCashCollectionGMT DATETIME;   -- from Audit Report
    DECLARE v_LastBillCollectionGMT DATETIME;   -- from Bill Report
    DECLARE v_LastCoinCollectionGMT DATETIME;   -- from Coin Report
    DECLARE v_LastCardCollectionGMT DATETIME;   -- from Card Report
    DECLARE v_LastCollectionTypeId  TINYINT UNSIGNED;
    DECLARE v_IsRecalcable          TINYINT UNSIGNED DEFAULT 0;
    DECLARE v_MaxLastCollectionGMT  DATETIME;
	-- EMS 5525 May 20
	DECLARE v_LastCollectionGMT DATETIME;
    
	-- Added on April 21
	IF (New.StartGMT) > '2012-10-01 00:00:00' THEN
	
    -- Retrieve latest paystation collection report dates from matching POSBalance record
    SELECT  LastCashCollectionGMT, LastBillCollectionGMT, LastCoinCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId
    INTO    v_LastCashCollectionGMT, v_LastBillCollectionGMT, v_LastCoinCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId
    FROM    POSBalance B
    WHERE   B.PointOfSaleId = NEW.PointOfSaleId;
    
    -- Set v_MaxLastCollectionGMT to be the latest of all the different types of Collections Reports for the paystation
    SET v_MaxLastCollectionGMT = v_LastCashCollectionGMT;
    IF v_LastBillCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastBillCollectionGMT; END IF;
    IF v_LastCoinCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCoinCollectionGMT; END IF;
    IF v_LastCardCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCardCollectionGMT; END IF;
    
    -- Determine the collection type of the most recent (last) paystation collections report, used by sp_CollectionRecalcBalance
    IF NEW.EndGMT > v_MaxLastCollectionGMT THEN
        SET v_LastCollectionTypeId = NEW.CollectionTypeId;
    END IF;

    -- Audit report
    IF NEW.CollectionTypeId = 0 THEN
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndGMT > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndGMT > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;        
		IF NEW.EndGMT > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
		
		-- EMS 5525 May 20
        SET v_LastCollectionGMT = NEW.EndGMT;
		
		-- Code added on Sep 16
		-- This code should be in XMLRPC
		-- For now writing the code in Trigger
		UPDATE POSServiceState set CoinBagCount = 0, BillStackerCount = 0 WHERE PointOfSaleId = NEW.PointOfSaleId ;
		
		
    -- Bill report    
    ELSEIF NEW.CollectionTypeId = 1 THEN
        
        -- Reassign last collection dates
        
		-- IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;   -- TAKE THIS OUT?
		-- Added the below on Oct 1 SET v_LastCashCollectionGMT = NEW.EndGMT; 
        IF NEW.EndGMT > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; SET v_LastCashCollectionGMT = NEW.EndGMT; END IF;
		
		-- EMS 5525 May 20
        SET v_LastCollectionGMT = NEW.EndGMT;
		
		-- Code added on Sep 16
		-- This code should be in XMLRPC
		-- For now writing the code in Trigger
		UPDATE POSServiceState set BillStackerCount = 0 WHERE PointOfSaleId = NEW.PointOfSaleId ;

    -- Coin report    
    ELSEIF NEW.CollectionTypeId = 2 THEN
        
        -- Reassign last collection dates
        
		-- IF NEW.EndGMT > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;   -- TAKE THIS OUT? 
        IF NEW.EndGMT > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; SET v_LastCashCollectionGMT = NEW.EndGMT; END IF;
		-- EMS 5525 May 20
        SET v_LastCollectionGMT = NEW.EndGMT;
		-- Code added on Sep 16
		-- This code should be in XMLRPC
		-- For now writing the code in Trigger
		UPDATE POSServiceState set CoinBagCount = 0 WHERE PointOfSaleId = NEW.PointOfSaleId ;

    -- Card report    
    ELSEIF NEW.CollectionTypeId = 3 THEN       
        
        -- Reassign last collection dates
        IF NEW.EndGMT > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndGMT; SET v_IsRecalcable = 2; END IF;
		-- EMS 5525 May 20
        SET v_LastCollectionGMT = NEW.EndGMT;
		
     END IF;
    
    -- The POSBalance is recalc-able
	-- EMS 5525 May 20
    IF v_IsRecalcable = 2 THEN
        UPDATE  POSBalance
        SET     IsRecalcable          = v_IsRecalcable,
                LastCollectionTypeId  = v_LastCollectionTypeId,
                LastCashCollectionGMT = v_LastCashCollectionGMT,
                LastCoinCollectionGMT = v_LastCoinCollectionGMT,
                LastBillCollectionGMT = v_LastBillCollectionGMT,
                LastCardCollectionGMT = v_LastCardCollectionGMT,
				LastCollectionGMT     = v_LastCollectionGMT
        WHERE   PointOfSaleId         = NEW.PointOfSaleId;
		
		-- Code added on Sep 16
		/*
		IF (v_LastCollectionTypeId = 0) THEN	-- meaning CollectionType is All
			
			UPDATE  POSBalance
			SET		CashAmount = 0 ,
					CoinCount = 0 ,
					CoinAmount = 0 ,
					BillCount = 0 ,
					BillAmount = 0 ,
					UnsettledCreditCardCount = 0 ,
					UnsettledCreditCardAmount = 0 ,
					TotalAmount = 0 
			WHERE PointOfSaleId         = NEW.PointOfSaleId;
			
		ELSEIF (v_LastCollectionTypeId = 1) THEN		-- meaning Bill Collection
			
			UPDATE  POSBalance
			SET		TotalAmount = TotalAmount - BillAmount,
					CashAmount = CashAmount - BillAmount ,
					-- CashAmount = CoinAmount + BillAmount ,
					BillCount = 0 ,
					BillAmount = 0 					
			WHERE PointOfSaleId         = NEW.PointOfSaleId;
		
		ELSEIF (v_LastCollectionTypeId = 2) THEN		-- meaning Coin Collection
		
			UPDATE  POSBalance
			SET		TotalAmount = TotalAmount -  CoinAmount,
					CashAmount = CashAmount - CoinAmount ,
					-- CashAmount = CoinAmount + BillAmount ,
					CoinCount = 0 ,
					CoinAmount = 0 					
			WHERE PointOfSaleId         = NEW.PointOfSaleId;
		
		ELSE -- (v_LastCollectionTypeId = 3) meaning Card Collection
			
			UPDATE  POSBalance
			SET		TotalAmount = TotalAmount - UnsettledCreditCardAmount					 ,
					UnsettledCreditCardCount = 0 ,
					UnsettledCreditCardAmount = 0 
			WHERE PointOfSaleId         = NEW.PointOfSaleId;
		
		END IF;*/
    END IF;
    
	-- Below Insert is written by Ashok used for Widget ETL 
	INSERT INTO StagingPOSCollection (Id, CustomerId,PointOfSaleId,CollectionTypeId,EndGMT,CoinTotalAmount,BillTotalAmount,CardTotalAmount)
	VALUES (New.Id, New.CustomerId,New.PointOfSaleId,New.CollectionTypeId,New.EndGMT,New.CoinTotalAmount,New.BillTotalAmount,New.CardTotalAmount) ;

	END IF;
END//
delimiter ;

-- ----------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_CollectionDeleteLock

-- Purpose:     Deletes aged CollectionLock records that are no longer in use.
--
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release  
-- ----------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_CollectionDeleteLock;

delimiter //
CREATE PROCEDURE sp_CollectionDeleteLock()
BEGIN

    -- Source Code Iteration = 1
    -- Date: 2013-01-18        Initial release   

    -- Declare local variables
    DECLARE v_Deleted INT DEFAULT 0;
        
    -- Look for aged CollectionLock records that are no longer in use
	-- EMS-10447 Added on Sep 04 2015 Removed NOT IN Added NOT EXISTS for performance improvement
	IF (SELECT  COUNT(*)                                      
        FROM    CollectionLock L
        WHERE   L.LockGMT < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 7 DAY)                      -- The current aged lock is at least 7 days old
        AND     NOT EXISTS (SELECT CollectionLockId FROM POSBalance POSB
                             WHERE  POSB.CollectionLockId = L.Id)) > 0 THEN               -- And no `POSBalance` records reference this lock
        
        -- Aged records found, delete them
        DELETE  CL.* FROM CollectionLock CL
        WHERE   LockGMT < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 7 DAY)                        -- The current aged lock is at least 7 days old
        AND     NOT EXISTS (SELECT CollectionLockId FROM POSBalance POSB
                           WHERE  CollectionLockId = CL.Id);                          -- And no `POSBalance` records reference this lock
        
        -- Retain the number of rows deleted
        SET v_Deleted = ROW_COUNT();
    END IF;
    -- Return the number of rows deleted 
    SELECT v_Deleted;
    
END//
delimiter ;

-- -----------------------------------------------------------------------------------------------------------------------
-- Procedure:   sp_CollectionRecalcBalance
--
-- Purpose:     Performs Separate Collections point-of-sale (pay station) balance recalculations.
--              Works in tandem with stored procedure sp_CollectionSetLock which is called prior to this procedure.
--
-- Note:        All `debug` statements have been commented out. To use these debug statements they must be
--              un-commented out.
-- 
-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release 
-- -----------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS sp_CollectionRecalcBalance;

delimiter //
CREATE PROCEDURE sp_CollectionRecalcBalance (IN CL_id INT UNSIGNED, IN Num_Sec SMALLINT UNSIGNED, IN PreAuth_Delay SMALLINT UNSIGNED)
BEGIN

-- Source Code Iteration = 1
-- Date: 2013-01-18        Initial release 
    
    -- Debug flag for unit test, must be turned off for use in QA and Production environments
    DECLARE v_Debug                         TINYINT UNSIGNED DEFAULT 0;        
        
    -- Declare local variables mapping to attributes from POSBalance table
    DECLARE v_PointOfSaleId                 INT UNSIGNED;
    DECLARE v_CashAmount                    INT UNSIGNED;
    DECLARE v_CoinCount                     MEDIUMINT  UNSIGNED;
    DECLARE v_CoinAmount                    INT UNSIGNED;
    DECLARE v_BillCount                     MEDIUMINT  UNSIGNED;
    DECLARE v_BillAmount                    INT UNSIGNED;
    DECLARE v_UnsettledCreditCardCount      MEDIUMINT  UNSIGNED;
    DECLARE v_UnsettledCreditCardAmount     INT UNSIGNED;
    DECLARE v_TotalAmount                   INT UNSIGNED;
    DECLARE v_LastCashCollectionGMT         DATETIME;
    DECLARE v_LastCoinCollectionGMT         DATETIME;
    DECLARE v_LastBillCollectionGMT         DATETIME;
    DECLARE v_LastCardCollectionGMT         DATETIME;
    DECLARE v_LastCollectionTypeId          TINYINT  UNSIGNED;
    DECLARE v_CoinCount_ss                  MEDIUMINT  UNSIGNED;
    DECLARE v_BillCount_ss                  MEDIUMINT  UNSIGNED;
	DECLARE v_PointOfSaleId_List			varchar(5000) DEFAULT '0' ;
    -- Declare local variable to count number of point-of-sales recalced
    DECLARE v_Recalced                      MEDIUMINT UNSIGNED DEFAULT 0;
	DECLARE v_CancelledCashAmount, v_CancelledCoinAmount INT UNSIGNED;
	DECLARE v_CancelledCoinCount MEDIUMINT  UNSIGNED;

	declare NO_DATA int default 0;
	declare idmaster_pos,indexm_pos int;
	
    DECLARE CollectionCursor CURSOR FOR SELECT PointOfSaleId FROM POSBalance WHERE CollectionLockId = CL_Id AND IsRecalcable > 0 AND ClusterId > 0 ORDER BY NextRecalcGMT;
    
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
      
	  INSERT INTO DiscardedPOSBalance (PointOfSaleId, CollectionLockId, CreatedGMT) VALUES (v_PointOfSaleId, CL_Id, UTC_TIMESTAMP()) ;
	  UPDATE POSBalance set CollectionLockId = NULL , ClusterId = NULL WHERE PointOfSaleId = v_PointOfSaleId ;
	
	END;
	
    -- Validate incoming parameter CL_id (CollectionLock.Id): must be non-zero
    
	IF (CL_id > 0) THEN
    
        -- Set when the recalc begins, track how long (in seconds) from when the lock was set to when the recalcs began
		-- Commented RecalcLagSeconds on Oct 3.
		-- ,RecalcLagSeconds = TO_SECONDS(UTC_TIMESTAMP())-TO_SECONDS(LockGMT)
        UPDATE  CollectionLock 
        SET     RecalcBeginGMT = UTC_TIMESTAMP()                 
        WHERE Id = CL_id;
    
    END IF;
	
		set indexm_pos =0;
		set idmaster_pos =0;


		-- Open the cursor, this can take time
        OPEN CollectionCursor;
		set idmaster_pos = (select FOUND_ROWS());
		while indexm_pos < idmaster_pos do

        FETCH CollectionCursor INTO v_PointOfSaleId ;
        
            -- Has last paystation been recalced?
         
            -- Get current Separate Collections information for paystation from table POSBalance 
            SELECT  CashAmount, CoinCount, CoinAmount, BillCount, BillAmount, UnsettledCreditCardCount, UnsettledCreditCardAmount, TotalAmount, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId
            INTO    v_CashAmount, v_CoinCount, v_CoinAmount, v_BillCount, v_BillAmount, v_UnsettledCreditCardCount, v_UnsettledCreditCardAmount, v_TotalAmount, v_LastCashCollectionGMT, v_LastCoinCollectionGMT, v_LastBillCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId
            FROM    POSBalance
            WHERE   PointOfSaleId = v_PointOfSaleId;
			
			-- Added on Sep 25
			set v_PointOfSaleId_List = IF(v_PointOfSaleId_List='0', v_PointOfSaleId, concat(v_PointOfSaleId_List,',',v_PointOfSaleId)) ;
			
            -- Debug: show pre-recalc paystation balance
            -- IF v_Debug THEN SELECT  'Pre', v_PointOfSaleId, v_CashAmount, v_CoinCount, v_CoinAmount, v_BillCount, v_BillAmount, v_UnsettledCreditCardCount, v_UnsettledCreditCardAmount, v_TotalAmount, v_LastCashCollectionGMT, v_LastCoinCollectionGMT, v_LastBillCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId; END IF;
        
            -- The last report from paystation was an Audit Report
            IF v_LastCollectionTypeId = 0 THEN
        
                -- Debug: show all collection dates are the same
                -- IF v_Debug THEN SELECT v_PointOfSaleId, 'Audit report'; END IF;
        
                -- Retrieve data from PurchaseCollection table
                SELECT  IFNULL(SUM(C.CashAmount),0),
                        IFNULL(SUM(C.CoinCount),0),
                        IFNULL(SUM(C.CoinAmount),0),
                        IFNULL(SUM(C.BillCount),0),
                        IFNULL(SUM(C.BillAmount),0)
                FROM    PurchaseCollection C
                WHERE   C.PointOfSaleId = v_PointOfSaleId
                AND     C.PurchaseGMT > v_LastCashCollectionGMT
                INTO    v_CashAmount, v_CoinCount, v_CoinAmount, v_BillCount, v_BillAmount;
            
                -- Retrieve data from PreAuth table
								
                SELECT  IFNULL(COUNT(P.Id),0), 
                        IFNULL(SUM(P.Amount),0)
                FROM    PreAuth P
                WHERE   P.PointOfSaleId = v_PointOfSaleId                
				AND     P.PreAuthDate > v_LastCashCollectionGMT
                AND     P.PreAuthDate < DATE_SUB(UTC_TIMESTAMP(),INTERVAL PreAuth_Delay SECOND)
                AND     P.IsApproved = 1
                AND     P.Expired = 0 AND P.Amount > 0
                INTO    v_UnsettledCreditCardCount, v_UnsettledCreditCardAmount;
            
                -- Look for additional coin and bill counts in POSServiceState
				-- This is not required
                IF (SELECT COUNT(*) FROM POSServiceState S WHERE S.PointOfSaleId = v_PointOfSaleId AND (S.CoinBagCount > 0 OR S.BillStackerCount > 0)) > 0 THEN
                    SELECT  S.CoinBagCount, S.BillStackerCount
                    FROM    POSServiceState S
                    WHERE   v_PointOfSaleId = S.PointOfSaleId   
                    INTO    v_CoinCount_ss, v_BillCount_ss; 
                    
                    SET     v_CoinCount = v_CoinCount + v_CoinCount_ss,
                            v_BillCount = v_BillCount + v_BillCount_ss;
                END IF;
            
            -- For Separate Collections paystation (last report from paystation was NOT an Audit Report, it was a Bill, Coin or Card report)
            ELSE
        
                -- Cash (Pre PsApp 6.4.0)
                SELECT  IFNULL(SUM(C.CashAmount),0)
                FROM    PurchaseCollection C
                WHERE   C.PointOfSaleId = v_PointOfSaleId
                AND     C.PurchaseGMT > v_LastCashCollectionGMT
                INTO    v_CashAmount;
        
                -- Coin
                SELECT  IFNULL(SUM(C.CoinCount),0),
                        IFNULL(SUM(C.CoinAmount),0)
                FROM    PurchaseCollection C
                WHERE   C.PointOfSaleId = v_PointOfSaleId
                AND     C.PurchaseGMT > v_LastCoinCollectionGMT 
				INTO    v_CoinCount, v_CoinAmount;
            
                -- Bill
                SELECT  IFNULL(SUM(C.BillCount),0),
                        IFNULL(SUM(C.BillAmount),0)
                FROM    PurchaseCollection C
                WHERE   C.PointOfSaleId = v_PointOfSaleId
                AND     C.PurchaseGMT > v_LastBillCollectionGMT
				INTO    v_BillCount, v_BillAmount;            

                -- Card (from PreAuth table)
                SELECT  IFNULL(COUNT(P.Id),0), 
                        IFNULL(SUM(P.Amount),0)
                FROM    PreAuth P
                WHERE   P.PointOfSaleId = v_PointOfSaleId
                AND     P.PreAuthDate > v_LastCardCollectionGMT
                AND     P.PreAuthDate < DATE_SUB(UTC_TIMESTAMP(),INTERVAL PreAuth_Delay SECOND)
                AND     P.IsApproved = 1
                AND     P.Expired = 0 AND P.Amount > 0
                INTO    v_UnsettledCreditCardCount, v_UnsettledCreditCardAmount;
            
                -- Look for additional coin and bill counts in POSServiceState
				-- This is not required
                IF (SELECT COUNT(*) FROM POSServiceState S WHERE S.PointOfSaleId = v_PointOfSaleId AND (S.CoinBagCount > 0 OR S.BillStackerCount > 0)) > 0 THEN
                    SELECT  S.CoinBagCount, S.BillStackerCount
                    FROM    POSServiceState S
                    WHERE   v_PointOfSaleId = S.PointOfSaleId   
                    INTO    v_CoinCount_ss, v_BillCount_ss; 
                    
                    SET     v_CoinCount = v_CoinCount + v_CoinCount_ss,
                            v_BillCount = v_BillCount + v_BillCount_ss;
                END IF;
            END IF;
        
            -- Calculate v_TotalAmount
			-- Added on Oct 1
            -- IF (v_CoinAmount = 0) AND (v_BillAmount = 0) THEN
			--	SET v_CashAmount = v_CashAmount ;
			
			-- ELSE
			
			IF ((v_CoinAmount + v_BillAmount) = 0)  and (v_CashAmount <> 0) THEN
				
				SET v_CashAmount = v_CashAmount ;
			
			ELSEIF ((v_CoinAmount + v_BillAmount) > 0)  THEN
			
				SET v_CashAmount = v_CoinAmount + v_BillAmount ;
			ELSE
				SET v_CashAmount = v_CoinAmount + v_BillAmount ;
			END IF ;
			
			-- SET v_CashAmount = v_CoinAmount + v_BillAmount ;
			-- END IF ;
			
			-- SET v_TotalAmount = v_CashAmount + v_UnsettledCreditCardAmount;
			-- SET v_TotalAmount = v_CashAmount + v_CoinAmount + v_BillAmount + v_UnsettledCreditCardAmount;
			-- Added on Sep 16
			-- SET v_TotalAmount = v_CoinAmount + v_BillAmount + v_UnsettledCreditCardAmount;
        
            -- Debug: show post-recalc paystation balance
            -- IF v_Debug THEN SELECT  'Post', v_PointOfSaleId, v_CashAmount, v_CoinCount, v_CoinAmount, v_BillCount, v_BillAmount, v_UnsettledCreditCardCount, v_UnsettledCreditCardAmount, v_TotalAmount, v_LastCashCollectionGMT, v_LastCoinCollectionGMT, v_LastBillCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId; END IF;
            
			-- EMS 5802
			-- Check for any Cancelled Transactions occured after the Collection.
			-- If yes then deduct the CashAmount and CoinCount from POSBalance
			
			SELECT IFNULL(SUM(CashAmount),0), IFNULL(SUM(CoinAmount),0), IFNULL(SUM(CoinCount),0)
			INTO v_CancelledCashAmount, v_CancelledCoinAmount, v_CancelledCoinCount
			FROM PurchaseCollectionCancelled WHERE PointOfSaleId=v_PointOfSaleId AND 
			PurchaseGMT > (SELECT ifnull(MAX(EndGMT),'2014-01-01 00:00:00') FROM POSCollection WHERE PointOfSaleId=v_PointOfSaleId AND CollectionTypeId in (0,2));
		
			IF (v_CashAmount - v_CancelledCoinAmount) >= 0 THEN
				SET v_CashAmount = v_CashAmount - v_CancelledCoinAmount - v_CancelledCashAmount ;	
				-- INSERT INTO TDebug (PointOfSaleId, CashAmount, CancelledCoinAmount,CancelledCashAmount, Message, RecordInsertTime) VALUES
				--				   (v_PointOfSaleId, v_CashAmount, v_CancelledCoinAmount,v_CancelledCashAmount, 'Inside v_CashAmount', now());
			END IF;
			
			IF (v_CoinCount -  v_CancelledCoinCount)>=0 THEN
				SET v_CoinCount = v_CoinCount - v_CancelledCoinCount;
				-- INSERT INTO TDebug (PointOfSaleId, CoinCount, CancelledCoinCount, Message, RecordInsertTime) VALUES
				--				   (v_PointOfSaleId, v_CoinCount, v_CancelledCoinCount, 'Inside v_CoinCount', now());
			END IF;
			
			IF (v_CoinAmount - v_CancelledCoinAmount) >= 0 THEN
				SET v_CoinAmount = v_CoinAmount - v_CancelledCoinAmount;
				-- INSERT INTO TDebug (PointOfSaleId, CoinAmount, CancelledCoinAmount, Message, RecordInsertTime) VALUES
				--				   (v_PointOfSaleId, v_CoinAmount, v_CancelledCoinAmount, 'Inside v_CoinAmount', now());
			END IF;
			
			SET v_TotalAmount = v_CashAmount + v_UnsettledCreditCardAmount;
			
            -- UPDATE POSBalance table (one paystation at a time)
            UPDATE  POSBalance B
            SET     B.CashAmount = v_CashAmount ,
                    B.CoinCount = v_CoinCount ,
                    B.CoinAmount = v_CoinAmount ,
                    B.BillCount = v_BillCount ,
                    B.BillAmount = v_BillAmount ,
                    B.UnsettledCreditCardCount = v_UnsettledCreditCardCount ,
                    B.UnsettledCreditCardAmount = v_UnsettledCreditCardAmount ,
                    B.TotalAmount = v_TotalAmount ,
                    B.LastRecalcGMT = UTC_TIMESTAMP() ,
                    B.NextRecalcGMT = ADDDATE(UTC_TIMESTAMP(),INTERVAL Num_Sec SECOND) ,  
                    B.PrevPrevCollectionLockId = B.PrevCollectionLockId ,
                    B.PrevCollectionLockId = B.CollectionLockId , 
                    B.IsRecalcable = 0 ,                    
                    B.CollectionLockId = NULL ,
                    B.ClusterId = NULL 
            WHERE   B.PointOfSaleId = v_PointOfSaleId;
            
            -- Track the number of point-of-sales recalced for this Collection Lock
            SET v_Recalced = v_Recalced + 1;
 
        set indexm_pos = indexm_pos +1;
		end while;

        -- Close the cursor
        CLOSE CollectionCursor;

        -- Execution complete, derive recalc statistics
		-- Commented on Oct 3
		-- RecalcsPerSecond = IF(RecalcBeginGMT IS NOT NULL,v_Recalced/IF(TO_SECONDS(UTC_TIMESTAMP())-TO_SECONDS(RecalcBeginGMT)=0,1,TO_SECONDS(UTC_TIMESTAMP())-TO_SECONDS(RecalcBeginGMT)),NULL)
		IF CL_id > 0 THEN
			UPDATE  CollectionLock 
			SET     RecalcEndGMT     = UTC_TIMESTAMP(), 
					RecalcedCount    = v_Recalced                
			WHERE   Id = CL_Id;
		END IF ;
    
    -- Recalc is complete
    -- Commented on Sep 25
	-- SELECT 0 AS ExecuteComplete;
	-- Addded on Sep 25
	SELECT v_PointOfSaleId_List;
            
END//
delimiter ;

-- Code added on Sep 10 2014
-- EMS 5802

-- ----------------------------------------------------------------------------------------------------------
-- Trigger:     tr_PurchaseCollectionCancelled_insert
--
-- Purpose:     On INSERT of a PurchaseCollectionCancelled record UPDATE the POSBalance record for the corresponding 
--              point-of-sale and SET IsRecalcable = 1 (but only if IsRecalcable = 0, which prevents any 
--              unnecessary updates).
--
  
-- ----------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS tr_PurchaseCollectionCancelled_insert;

delimiter //
CREATE TRIGGER tr_PurchaseCollectionCancelled_insert AFTER INSERT ON PurchaseCollectionCancelled
FOR EACH ROW BEGIN

	
    IF (SELECT COUNT(*) FROM POSBalance WHERE PointOfSaleId = NEW.PointOfSaleId AND IsRecalcable =0) = 1 THEN
        -- Don't issue an UPDATE that updates zero rows because this zero row UPDATE will be replicated
        UPDATE  POSBalance
        SET     IsRecalcable = 1
        WHERE   PointOfSaleId = NEW.PointOfSaleId;
    END IF;
	
END//
delimiter ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
