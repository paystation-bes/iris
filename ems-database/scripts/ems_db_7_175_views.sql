-- SELECT * FROM ActivityLog_v;
-- SELECT * FROM ActivePOSAlert_v;
-- SELECT * FROM Customer_v;
-- SELECT * FROM CustomerProperty_v;
-- SELECT * FROM CustomerRole_v ORDER BY Customer_Name, CustomerType_Name;
-- SELECT * FROM CustomerSubscription_v;
-- SELECT * FROM Location_v;
-- SELECT * FROM POSAlert_v;
-- SELECT * FROM POSDate_v;
-- SELECT * FROM PointOfSale_v;
-- SELECT * FROM Role_v;
-- SELECT * FROM RolePermission_v;
-- SELECT * FROM Route_v;
-- SELECT * FROM Timezone_v;
-- SELECT * FROM UserAccount_v;
-- SELECT * FROM UserRole_v ORDER BY Customer_Name, UserAccount_UserName;
-- SELECT * FROM UserRolePermission_v;
-- SELECT * FROM Widget_v;

-- -----------------------------------------------------
-- View `ActivityLog_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `ActivityLog_v` ;

CREATE VIEW `ActivityLog_v`
AS
SELECT  A.Id, 
        U.UserName AS UserAccount_UserName,
        C.Name AS Customer_Name, 
        T.Name AS CustomerType_Name, 
        AT.Name AS ActivityType_Name,
        ET.Name AS EntityType_Name,
        A.EntityId AS Entity_Id,
        A.EntityLogId AS Entity_LogId,
        A.ActivityGMT,
        U.CustomerId,
        C.CustomerTypeId,
        A.ActivityTypeId,
        A.EntityTypeId 
FROM    ActivityLog A, UserAccount U, Customer C, CustomerType T, ActivityType AT, EntityType ET
WHERE   A.UserAccountId = U.Id
AND     U.CustomerId = C.Id
AND     C.CustomerTypeId = T.Id
AND     A.ActivityTypeId = AT.Id
AND     A.EntityTypeId = ET.Id;

-- -----------------------------------------------------
-- View `ActivePOSAlert_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `ActivePOSAlert_v` ;

CREATE VIEW `ActivePOSAlert_v`
AS
SELECT  C.Name                AS Customer_Name,
        AT.Name               AS AT_AlertType_Name, 
        AP.IsUserDefined      AS AT_IsUserDefined,
        CAT.Name              AS CAT_CustomerAlertType_Name,
        CAT.Threshold         AS CAT_Threshold,
        CAT.IsActive          AS CAT_IsActive,
        CAT.IsDeleted         AS CAT_IsDeleted,
        D.Description         AS EventDefinition_Description,
        P.Name                AS POS_Name,
        P.SerialNumber        AS POS_SerialNumber,
        A.IsActive            AS Alert_IsActive,
        A.AlertGMT            AS Alert_AlertGMT,
        A.AlertInfo           AS Alert_AlertInfo,
        A.CreatedGMT          AS Alert_CreatedGMT,
        C.Id                  AS Customer_Id,
        AT.Id                 AS AlertType_Id,
        CAT.Id                AS CustomerAlertType_Id,
        P.Id                  AS PointOfSale_Id,
        A.EventDeviceTypeId   AS EventDeviceType_Id,
        A.EventStatusTypeId   AS EventStatusType_Id,
        A.EventActionTypeId   AS EventActionType_Id,
        A.EventSeverityTypeId AS EventSeverityType_Id,
        A.Id                  AS ActivePOSAlert_Id,
        A.POSAlertId          AS POSAlert_Id
        
FROM    Customer              C,    
        AlertThresholdType    AT,   
        CustomerAlertType     CAT,  
        PointOfSale           P,   
        ActivePOSAlert        A,    
        EventDefinition       D,
		AlertType			  AP
        
WHERE   C.Id                  = CAT.CustomerId
AND     CAT.AlertThresholdTypeId       = AT.Id
AND     C.Id                  = P.CustomerId
AND     CAT.Id                = A.CustomerAlertTypeId
AND     A.PointOfSaleId       = P.Id
AND     A.EventDeviceTypeId   = D.EventDeviceTypeId
AND     A.EventStatusTypeId   = D.EventStatusTypeId
AND     A.EventActionTypeId   = D.EventActionTypeId
AND     A.EventSeverityTypeId = D.EventSeverityTypeId
AND 	AT.AlertTypeId		  = AP.Id;

-- -----------------------------------------------------
-- View `Timezone_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Timezone_v` ;

CREATE VIEW `Timezone_v`
AS
SELECT  Time_zone_id AS Id, Name 
FROM    mysql.time_zone_name 
WHERE   Time_zone_id BETWEEN 360 AND 368     -- Canada
OR      Time_zone_id = 473                   -- GMT
OR      Time_zone_id BETWEEN 571 AND 581;    -- United States

-- -----------------------------------------------------
-- View `Customer_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Customer_v` ;

CREATE VIEW `Customer_v`
AS
SELECT  C.Id, 
        C.Name AS Customer_Name, 
        COUNT(*) AS Number_Of_Locations,
        T.Name AS CustomerType_Name, 
        S.Name AS CustomerStatusType_Name, 
        Z.Name AS Customer_Timezone,
        P.PropertyValue AS CustomerProperty_Timezone,
        IF(Z.Name!=P.PropertyValue,'ERROR: timezone mismatch',IF(T.Id=3 AND C.TimezoneId=473,'WARNING: child customer data will not ETL','')) AS Timezone_Status,
        C.IsParent,
        C.ParentCustomerId, 
        C.VERSION, 
        C.LastModifiedGMT, 
        C.LastModifiedByUserId, 
        C.CustomerTypeId, 
        C.CustomerStatusTypeId
FROM    Customer C, CustomerType T, CustomerStatusType S, Timezone_v Z, CustomerProperty P, Location L
WHERE   C.CustomerTypeId = T.Id
AND     C.CustomerStatusTypeId = S.Id
AND     C.TimezoneId = Z.Id
AND     C.Id = P.CustomerId
AND     P.CustomerPropertyTypeId = 1   -- CustomerPropertyTypeId.Id = 1 = 'Timezone'
AND     C.Id = L.CustomerId
GROUP BY C.Id
ORDER BY C.Name;

-- -----------------------------------------------------
-- View `CustomerSubscription_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `CustomerSubscription_v` ;

CREATE VIEW `CustomerSubscription_v`
AS
SELECT  C.Name AS Customer_Name, 
        T.Name AS SubscriptionType_Name,
        S.IsEnabled AS CustomerSubscription_IsEnabled,
        C.Id AS Customer_Id,
        T.Id AS SubscriptionType_Id,
        S.Id AS CustomerSubscription_Id
FROM    Customer C,
        SubscriptionType T,
        CustomerSubscription S
WHERE   C.Id = S.CustomerId
AND     S.SubscriptionTypeId = T.Id
ORDER BY C.Name, T.Name;


-- -----------------------------------------------------
-- View `CustomerProperty_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `CustomerProperty_v` ;

CREATE VIEW `CustomerProperty_v`
AS
SELECT  C.Name AS Customer_Name, 
        T.Name AS CustomerPropertyType_Name,
        P.PropertyValue AS CustomerProperty_PropertyValue,
        C.Id AS Customer_Id,
        T.Id AS CustomerPropertyType_Id,
        P.Id AS CustomerProperty_Id
FROM    Customer C,
        CustomerPropertyType T,
        CustomerProperty P
WHERE   C.Id = P.CustomerId
AND     P.CustomerPropertyTypeId = T.Id
ORDER BY C.Name, T.Name;

-- -----------------------------------------------------
-- View `CustomerRole_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `CustomerRole_v` ;

CREATE VIEW `CustomerRole_v`
AS
SELECT  CR.Id,
        CR.CustomerId,
        CR.RoleId,
        S.Id AS RoleStatusTypeId,
        C.Name AS Customer_Name,
        R.Name AS Role_Name,
        S.Name AS RoleStatusType_Name,
        CR.VERSION,
        CR.LastModifiedGMT, 
        CR.LastModifiedByUserId, 
        R.IsAdmin AS Role_IsAdmin,
        R.IsLocked AS Role_IsLocked,
        T.Name AS CustomerType_Name,
        T.Id AS CustomerTypeId,
        IF(R.CustomerTypeId=1 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_DPT_Admin_Role,
        IF(R.CustomerTypeId=2 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Parent_Admin_Role,
        IF(R.CustomerTypeId=3 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Child_Admin_Role
FROM    CustomerRole CR, Customer C, Role R, CustomerType T, RoleStatusType S
WHERE   CR.CustomerId = C.Id
AND     CR.RoleId = R.Id
AND     C.CustomerTypeId = T.Id 
AND     R.CustomerTypeId = T.Id
AND     R.RoleStatusTypeId = S.Id;

-- -----------------------------------------------------
-- View `POSAlert_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `POSAlert_v` ;

CREATE VIEW `POSAlert_v`
AS
SELECT  C.Name                AS Customer_Name,
        AT.Name               AS AT_AlertType_Name, 
        AP.IsUserDefined      AS AT_IsUserDefined,
        CAT.Name              AS CAT_CustomerAlertType_Name,
        CAT.Threshold         AS CAT_Threshold,
        CAT.IsActive          AS CAT_IsActive,
        CAT.IsDeleted         AS CAT_IsDeleted,
        D.Description         AS EventDefinition_Description,
        P.Name                AS POS_Name,
        P.SerialNumber        AS POS_SerialNumber,
        A.IsActive            AS Alert_IsActive,
        A.AlertGMT            AS Alert_AlertGMT,
        A.IsSentEmail         AS Alert_IsSentEmail,
        A.SentEmailGMT        AS Alert_SentEmailGMT,
        A.ClearedGMT          AS Alert_ClearedGMT,
        A.ClearedByUserId     AS Alert_ClearedByUserId,
        A.CreatedGMT          AS Alert_CreatedGMT,
        C.Id                  AS Customer_Id,
        AT.Id                 AS AlertType_Id,
        CAT.Id                AS CustomerAlertType_Id,
        P.Id                  AS PointOfSale_Id,
        A.EventDeviceTypeId   AS EventDeviceType_Id,
        A.EventStatusTypeId   AS EventStatusType_Id,
        A.EventActionTypeId   AS EventActionType_Id,
        A.EventSeverityTypeId AS EventSeverityType_Id,
        A.Id                  AS POSAlert_Id
        
FROM    Customer              C,    
        AlertThresholdType    AT,   
        CustomerAlertType     CAT,  
        PointOfSale           P,   
        POSAlert              A,    
        EventDefinition       D,
		AlertType			  AP		
        
WHERE   C.Id                  = CAT.CustomerId
AND     CAT.AlertThresholdTypeId       = AT.Id
AND     C.Id                  = P.CustomerId
AND     CAT.Id                = A.CustomerAlertTypeId
AND     A.PointOfSaleId       = P.Id
AND     A.EventDeviceTypeId   = D.EventDeviceTypeId
AND     A.EventStatusTypeId   = D.EventStatusTypeId
AND     A.EventActionTypeId   = D.EventActionTypeId
AND     A.EventSeverityTypeId = D.EventSeverityTypeId
AND 	AT.AlertTypeId		  = AP.Id;

-- -----------------------------------------------------
-- View `POSDate_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `POSDate_v` ;

CREATE VIEW `POSDate_v`
AS
SELECT  C.Name AS Customer_Name,
        P.SerialNumber AS POS_SerialNumber,
        P.Name AS POS_Name,
        D.ChangedGMT AS POSDate_ChangedGMT,
        T.Name AS POSDateType_Name,
        IF(D.CurrentSetting=1,'1               Yes',IF(D.CurrentSetting=0,'0               No',CONCAT(D.CurrentSetting,'          Error'))) AS POSDate_CurrentSetting,
        T.Id AS POSDateType_Id,
        C.Id AS Customer_Id,
        P.Id AS PointOfSale_Id,
        Y.Id AS Paystation_Id,
        IF(P.SerialNumber=Y.SerialNumber,'same as POS',CONCAT('ERROR: ',Y.SerialNumber)) AS PS_SerialNumber
FROM    Customer C, PointOfSale P, POSDateType T, POSDate D, Paystation Y
WHERE   C.Id = P.CustomerId
AND     P.Id = D.PointOfSaleId
AND     D.POSDateTypeId = T.Id
AND     P.PaystationId = Y.Id
ORDER BY C.Name, P.SerialNumber, D.ChangedGMT, T.Id;

-- -----------------------------------------------------
-- View `PointOfSale_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `PointOfSale_v` ;

CREATE VIEW `PointOfSale_v`
AS
SELECT  C.Name AS Customer_Name, 
        L.Name AS Location_Name,
        P.SerialNumber AS POS_SerialNumber, 
        P.Name AS POS_Name, 
        T.Name AS PaystationType_Name,
        M.Name AS ModemType_Name,
        P.ProvisionedGMT AS POS_ProvisionedGMT, 
        P.Latitude AS POS_Latitude, 
        P.Longitude AS POS_Longitude, 
        S.IsBillableMonthlyOnActivation AS POSStatus_IsBillableMonthlyOnActivation,
        S.IsActivated AS POSStatus_IsActivated,
        S.IsBillableMonthlyForEms AS POSStatus_IsBillableMonthlyForEms,
        S.IsDigitalConnect AS POSStatus_IsDigitalConnect,
        S.IsDecommissioned AS POSStatus_IsDecommissioned,
        S.IsBillableMonthlyOnActivationGMT AS POSStatus_IsBillableMonthlyOnActivationGMT,
        S.IsActivatedGMT AS POSStatus_IsActivatedGMT,
        S.IsBillableMonthlyForEmsGMT AS POSStatus_IsBillableMonthlyForEmsGMT,
        S.IsDigitalConnectGMT AS POSStatus_IsDigitalConnectGMT,
        S.IsDecommissionedGMT AS POSStatus_IsDecommissionedGMT,
        P.LastModifiedGMT AS POS_LastModifiedGMT, 
        P.LastModifiedByUserId AS POS_LastModifiedByUserId, 
        P.CustomerId AS Customer_Id,
        P.LocationId AS Location_Id,
        P.Id AS PointOfSale_Id,
        Y.Id AS Paystation_Id,
        T.Id AS PaystationType_Id,
        M.Id AS ModemType_Id,
        IF(P.SerialNumber=Y.SerialNumber,'same as POS',CONCAT('ERROR: ',Y.SerialNumber)) AS PS_SerialNumber
FROM    PointOfSale P, Customer C, Location L, POSStatus S, Paystation Y, PaystationType T, ModemType M
WHERE   P.CustomerId = C.Id
AND     P.LocationId = L.Id
AND     P.Id = S.PointOfSaleId
AND     P.PaystationId = Y.Id
AND     Y.PaystationTypeId = T.Id
AND     Y.ModemTypeId = M.Id
ORDER BY C.Name, P.SerialNumber, P.ProvisionedGMT;

-- -----------------------------------------------------
-- View `Role_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Role_v` ;

CREATE VIEW `Role_v`
AS
SELECT  R.Id AS Role_Id,
        R.Name AS Role_Name,
        S.Name AS RoleStatusType_Name,
        T.Name AS CustomerType_Name, 
        R.IsAdmin AS Role_IsAdmin,
        R.IsLocked AS Role_IsLocked,
        R.VERSION,
        R.LastModifiedGMT, 
        R.LastModifiedByUserId,    
        R.CustomerTypeId,
        R.RoleStatusTypeId,
        IF(R.CustomerTypeId=1 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_DPT_Admin_Role,
        IF(R.CustomerTypeId=2 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Parent_Admin_Role,
        IF(R.CustomerTypeId=3 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Child_Admin_Role
FROM    Role R, CustomerType T, RoleStatusType S
WHERE   R.CustomerTypeId = T.Id
AND     R.RoleStatusTypeId = S.Id;

-- -----------------------------------------------------
-- View `RolePermission_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `RolePermission_v` ;

CREATE VIEW `RolePermission_v`
AS
SELECT  RP.Id,
        RP.RoleId,
        RP.PermissionId,
        R.Name AS Role_Name,
        P.Name AS Permission_Name,
        RP.VERSION,
        RP.LastModifiedGMT, 
        RP.LastModifiedByUserId,
        T.Name AS CustomerType_Name,
        T.Id AS CustomerTypeId,
        IF(R.CustomerTypeId=1 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_DPT_Admin_Role,
        IF(R.CustomerTypeId=2 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Parent_Admin_Role,
        IF(R.CustomerTypeId=3 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Child_Admin_Role
FROM    RolePermission RP, Role R, CustomerType T, Permission P
WHERE   RP.RoleId = R.Id
AND     RP.PermissionId = P.Id
AND     R.CustomerTypeId = T.Id;

-- -----------------------------------------------------
-- View `Route_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Route_v` ;

CREATE VIEW `Route_v`
AS
SELECT  C.Name AS Customer_Name,
        R.Name AS Route_Name,
        T.Name AS RouteType_Name,
        P.Name AS PointOfSale_Name,
        P.SerialNumber,
        C.Id AS Customer_Id,
        R.Id AS Route_Id,
        P.Id AS PointOfSale_Id
FROM    Customer C, Route R, PointOfSale P, RouteType T, RoutePOS M
WHERE   C.Id = R.CustomerId
AND     R.CustomerId = P.CustomerId
AND     R.RouteTypeId = T.Id
AND     R.Id = M.RouteId
AND     P.Id = M.PointOfSaleId
ORDER BY C.Name, R.Name, T.Name, P.Name;

-- -----------------------------------------------------
-- View `UserAccount_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `UserAccount_v` ;

CREATE VIEW `UserAccount_v`
AS
SELECT  U.Id, 
        U.UserName AS UserAccount_UserName,
        IFNULL(E.Email,'') AS CustomerEmail_Email,
        C.Name AS Customer_Name, 
        U.FirstName, 
        U.LastName, 
        U.Password, 
        U.PasswordSalt, 
        U.IsPasswordTemporary, 
        S.Name AS UserStatusType_Name,
        U.VERSION, 
        U.LastModifiedGMT, 
        U.LastModifiedByUserId,
        U.CustomerId,
        U.UserStatusTypeId
FROM    Customer C, UserStatusType S, UserAccount U
        LEFT JOIN CustomerEmail E ON E.Id = U.CustomerEmailId
WHERE   U.CustomerId = C.Id
AND     U.UserStatusTypeId = S.Id;

-- -----------------------------------------------------
-- View `UserRole_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `UserRole_v` ;

CREATE VIEW `UserRole_v`
AS
SELECT  UR.Id,
        UR.UserAccountId, 
        UR.RoleId,
        C.Name AS Customer_Name,
        U.UserName AS UserAccount_UserName,
        U.FirstName AS UserAccount_FirstName,
        U.LastName AS UserAccount_LastName,
        R.Name AS Role_Name,
        S.name AS RoleStatusType_Name,
        UR.VERSION,
        UR.LastModifiedGMT, 
        UR.LastModifiedByUserId, 
        R.IsAdmin AS Role_IsAdmin,
        R.IsLocked AS Role_IsLocked,
        U.CustomerId,
        R.CustomerTypeId,
        T.Name AS CustomerType_Name,
        IF(R.CustomerTypeId=1 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_DPT_Admin_Role,
        IF(R.CustomerTypeId=2 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Parent_Admin_Role,
        IF(R.CustomerTypeId=3 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Child_Admin_Role
FROM    UserRole UR, Customer C, UserAccount U, Role R, CustomerType T, RoleStatusType S
WHERE   UR.UserAccountId = U.Id
AND     U.CustomerId = C.Id
AND     UR.RoleId = R.Id
AND     C.CustomerTypeId = T.Id
AND     R.RoleStatusTypeId = S.Id;

-- -----------------------------------------------------
-- View `UserRolePermission_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `UserRolePermission_v` ;

CREATE VIEW `UserRolePermission_v`
AS
SELECT  UR.UserAccountId, 
        RP.RoleId,
        RP.PermissionId,
        UA.UserName AS UserAccount_UserName,
        R.Name AS Role_Name,
        PT.Name AS Permission_Name,
        UA.FirstName,
        UA.LastName,
        C.Name AS Customer_Name,
        CT.Name AS CustomerType_Name, 
        C.Id AS CustomerId, 
        R.CustomerTypeId,
        R.IsAdmin AS Role_IsAdmin,
        R.IsLocked AS Role_IsLocked,
        IF(R.CustomerTypeId=1 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_DPT_Admin_Role,
        IF(R.CustomerTypeId=2 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Parent_Admin_Role,
        IF(R.CustomerTypeId=3 AND R.IsAdmin=1 AND R.IsLocked=1,'Yes','-') AS Is_Child_Admin_Role
FROM    UserRole UR, UserAccount UA, Customer C, RolePermission RP, Role R, CustomerType CT, Permission PT
WHERE   UR.UserAccountId = UA.Id
AND     UA.CustomerId = C.Id
AND     UR.RoleId = R.Id
AND     RP.RoleId = R.Id
AND     RP.PermissionId = PT.Id
AND     R.CustomerTypeId = CT.Id;

-- -----------------------------------------------------
-- View `Widget_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Widget_v` ;

CREATE VIEW `Widget_v`
AS
SELECT  L.Name AS WidgetListType_Name,
        W.Name AS Widget_Name,

        T1.Name AS WS_Metric,
        T2.Name AS WS_Range,
        T3.Name AS WS_Tier1,
        T4.Name AS WS_Tier2,
        T5.Name AS WS_Tier3,
        T6.Name AS WS_Filter,
        T7.Name AS WS_Chart,
        T8.RowLimit AS WS_RowLimit,
        
        W.TrendAmount AS WS_TrendAmount,
        W.IsSubsetTier1 AS WS_IsSubsetTier1,
        W.IsSubsetTier2 AS WS_IsSubsetTier2,
        W.IsSubsetTier3 AS WS_IsSubsetTier3,
        W.IsTrendByLocation AS WS_IsTrendByLocation,
        W.IsShowPaystations AS WS_IsShowPaystations,
        W.ParentCustomerId AS WS_ParentCust_Id,
        W.CustomerId AS WS_Cust_Id,

        U.UserName AS UserAccount_UserName,
        T.Name AS Tab_Name,
        T.OrderNumber AS Tab_OrderNumber,
        S.OrderNumber AS Section_OrderNumber, 
        S.LayoutId AS Section_LayoutId,
        W.ColumnId AS Widget_ColumnId,
        W.OrderNumber AS Widget_OrderNumber,
                
        U.Id AS UserAccount_Id,
        T.Id AS Tab_Id,
        S.Id AS Section_Id,
        L.Id AS WLType_Id,
        W.Id AS Widget_Id,
        
        T1.Id AS WidgetMetricType_Id,
        T2.Id AS WidgetRangeType_Id,
        T3.Id AS WidgetTier1Type_Id,
        T4.Id AS WidgetTier2Type_Id,
        T5.Id AS WidgetTier3Type_Id,
        T6.Id AS WidgetFilterType_Id,
        T7.Id AS WidgetChartType_Id,
        T8.Id AS WidgetLimitType_Id

FROM    WidgetListType L,
        UserAccount U,
        Tab T,
        Section S,
        Widget W,
        WidgetMetricType T1,
        WidgetRangeType T2,
        WidgetTierType T3,
        WidgetTierType T4,
        WidgetTierType T5,
        WidgetFilterType T6,
        WidgetChartType T7,
        WidgetLimitType T8

WHERE   L.Id = W.WidgetListTypeId
AND     T.UserAccountId = U.Id
AND     T.Id = S.TabId
AND     S.Id = W.SectionId
AND     W.WidgetMetricTypeId = T1.Id
AND     W.WidgetRangeTypeId  = T2.Id
AND     W.WidgetTier1TypeId  = T3.Id
AND     W.WidgetTier2TypeId  = T4.Id
AND     W.WidgetTier3TypeId  = T5.Id
AND     W.WidgetFilterTypeId = T6.Id
AND     W.WidgetChartTypeId  = T7.Id
AND     W.WidgetLimitTypeId  = T8.Id

ORDER BY
        L.Id,
        U.UserName,
        T.OrderNumber,
        S.OrderNumber,
        W.ColumnId,
        W.OrderNumber;
        
-- -----------------------------------------------------
-- View `Location_v`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Location_v` ;

CREATE VIEW `Location_v`
AS
SELECT  C.Name                          AS Customer_Name,
        L.Name                          AS Location_Name,
        COUNT(*)                        AS Number_of_POS,
        SUM(S.IsBillableMonthlyForEms)  AS Number_of_POS_Billable_Monthly_for_Ems,
        L.NumberOfSpaces                AS Location_NumberOfSpaces,
        L.TargetMonthlyRevenueAmount    AS Location_TargetMonthlyRevenueAmount,
        R.Name                          AS Parent_Location_Name,
        C.Id                            AS Customer_Id,
        L.Id                            AS Location_Id,
        R.Id                            AS Parent_Location_Id
        
FROM    Location L
        INNER JOIN Customer C ON C.Id = L.CustomerId
        LEFT OUTER JOIN Location R ON R.Id = L.ParentLocationId
        INNER JOIN PointOfSale P ON P.LocationId = L.Id
        LEFT OUTER JOIN POSStatus S ON S.PointOfSaleId = P.Id
        
GROUP BY C.Id, L.Id, R.Id
ORDER BY C.Name, L.Name;


-- -----------------------------------------------------
-- Views for User Defined Alerts 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- `AlertThresholdType1Event_v` AlertThresholdType = 1
-- -----------------------------------------------------

CREATE or replace     
VIEW `AlertThresholdType1Event_v` AS
    select 
        `cat`.`Id` AS `CustomerAlertTypeId`,
        `pos`.`Id` AS `PointOfSaleId`,
        0 AS `GapToMinor`,
        -(1.0) AS `GapToMajor`,
        0 AS `GapToCritical`,
        `apa`.`Id` AS `POSEventCurrentId`
    from
       `POSEventCurrent` apa inner join `PointOfSale` pos on apa.PointOfSaleId = pos.Id
						     inner join `CustomerAlertType` cat on apa.CustomerAlertTypeId = cat.Id 
							 inner join  `POSHeartbeat` phb on pos.Id = phb.PointOfSaleId
							 inner join  `POSStatus` poss ON poss.PointOfSaleId = pos.Id
							INNER JOIN Paystation ps ON pos.PaystationId = ps.Id 
        WHERE (`cat`.`AlertThresholdTypeId` = 1)
            and (`cat`.`IsActive` = 1)
            and (`cat`.`IsDeleted` = 0)
           and  (`poss`.`IsActivated` = 1)
		   AND ps.PaystationTypeId != 9
 and (poss.IsDeleted = 0)
            and (`poss`.`IsDecommissioned` = 0)
            and (`apa`.`IsActive` = 0)
            and (`apa`.`IsHidden` = 0)
and  (`phb`.`LastHeartbeatGMT` < (utc_timestamp() - interval `cat`.`Threshold` hour));

-- -----------------------------------------------------
-- `AlertThresholdType2Event_v` AlertThresholdType = 2
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType2Event_v` ;

CREATE 
VIEW `AlertThresholdType2Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*75-bal.TotalAmount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold*100-bal.TotalAmount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=2 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.TotalAmount>cat.Threshold*75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.TotalAmount>cat.Threshold*100)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType6Event_v` AlertThresholdType = 6
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType6Event_v` ;

CREATE 
VIEW `AlertThresholdType6Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*0.75-bal.CoinCount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold-bal.CoinCount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=6 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.CoinCount>cat.Threshold*0.75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.CoinCount>cat.Threshold)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType7Event_v` AlertThresholdType = 7
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType7Event_v` ;

CREATE 
VIEW `AlertThresholdType7Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*75-bal.CoinAmount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold*100-bal.CoinAmount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=7 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.CoinAmount>cat.Threshold*75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.CoinAmount>cat.Threshold*100)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType8Event_v` AlertThresholdType = 8
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType8Event_v` ;

CREATE 
VIEW `AlertThresholdType8Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*0.75-bal.BillCount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold-bal.BillCount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=8 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.BillCount>cat.Threshold*0.75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.BillCount>cat.Threshold)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType9Event_v` AlertThresholdType = 9
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType9Event_v` ;

CREATE 
VIEW `AlertThresholdType9Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*75-bal.BillAmount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold*100-bal.BillAmount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=9 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.BillAmount>cat.Threshold*75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.BillAmount>cat.Threshold*100)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType10Event_v` AlertThresholdType = 10
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType10Event_v` ;

CREATE 
VIEW `AlertThresholdType10Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*0.75-bal.UnsettledCreditCardCount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold-bal.UnsettledCreditCardCount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=10 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.UnsettledCreditCardCount>cat.Threshold*0.75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.UnsettledCreditCardCount>cat.Threshold)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType11Event_v` AlertThresholdType = 11
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType11Event_v` ;

CREATE 
VIEW `AlertThresholdType11Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		cat.Threshold*75-bal.UnsettledCreditCardAmount AS GapToMinor, 
		0 AS GapToMajor, 
		cat.Threshold*100-bal.UnsettledCreditCardAmount AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=11 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.UnsettledCreditCardAmount>cat.Threshold*75) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.UnsettledCreditCardAmount>cat.Threshold*100)))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType13Event_v` AlertThresholdType = 13
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType13Event_v` ;

CREATE 
VIEW `AlertThresholdType13Event_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		TIMESTAMPDIFF(SECOND, DATE_SUB(UTC_TIMESTAMP(), INTERVAL (cat.Threshold-1) DAY), bal.LastCollectionGMT) AS GapToMinor, 
		0 AS GapToMajor, 
		TIMESTAMPDIFF(SECOND, DATE_SUB(UTC_TIMESTAMP(), INTERVAL cat.Threshold DAY), bal.LastCollectionGMT) AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN Paystation ps ON pos.PaystationId = ps.Id AND ps.PaystationTypeId != 9
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=13 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND ((apa.IsActive=0 AND bal.LastCollectionGMT<DATE_SUB(UTC_TIMESTAMP(), INTERVAL (cat.Threshold-1) DAY)) 
     												OR (apa.IsActive=1 AND apa.EventSeverityTypeId<3 AND bal.LastCollectionGMT<DATE_SUB(UTC_TIMESTAMP(), INTERVAL cat.Threshold DAY))))
WHERE 	poss.IsActivated=1 
AND 	poss.IsDeleted=0 
AND 	poss.IsDecommissioned=0
AND     apa.IsHidden = 0
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- Views for Collection Clears
-- -----------------------------------------------------

-- -----------------------------------------------------
-- `AlertThresholdType1EventClear_v` AlertThresholdType = 1
-- -----------------------------------------------------

 CREATE or replace  
VIEW `AlertThresholdType1EventClear_v` AS
select  `cat`.`Id` AS `CustomerAlertTypeId`,
        `pos`.`Id` AS `PointOfSaleId`,
        0 AS `GapToMinor`,
        1 AS `GapToMajor`,
        0 AS `GapToCritical`,
        `apa`.`Id` AS `POSEventCurrentId`
    from
       `POSEventCurrent` `apa`  inner join `PointOfSale`  `pos` on apa.PointOfSaleId = pos.Id
							    inner join `CustomerAlertType` cat on apa.CustomerAlertTypeId = cat.Id 
								inner join  `POSHeartbeat` phb on pos.Id = phb.PointOfSaleId
								inner join  `POSStatus` poss ON poss.PointOfSaleId = pos.Id	
		WHERE 
		`cat`.`AlertThresholdTypeId` = 1
		and `cat`.`IsActive` = 1
		and `cat`.`IsDeleted` = 0
		and `apa`.`IsActive` = 1
		and `poss`.`IsDeleted` = 0
            and `poss`.`IsDecommissioned` = 0
            and `phb`.`LastHeartbeatGMT` >= (utc_timestamp() - interval `cat`.`Threshold` hour);

-- -----------------------------------------------------
-- `AlertThresholdType2EventClear_v` AlertThresholdType = 2
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType2EventClear_v` ;

CREATE 
VIEW `AlertThresholdType2EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
      	CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*75-bal.TotalAmount END AS GapToMinor,
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*100-bal.TotalAmount END AS GapToCritical,
		apa.Id AS POSEventCurrentId

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=2 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.TotalAmount<=cat.Threshold*75 OR (apa.EventSeverityTypeId>=3 AND bal.TotalAmount<=cat.Threshold*100)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType6EventClear_v` AlertThresholdType = 6
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType6EventClear_v` ;

CREATE 
VIEW `AlertThresholdType6EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*0.75-bal.CoinCount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold-bal.CoinCount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=6 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.CoinCount<=cat.Threshold*0.75 OR (apa.EventSeverityTypeId>=3 AND bal.CoinCount<=cat.Threshold)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType7EventClear_v` AlertThresholdType = 7
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType7EventClear_v` ;

CREATE 
VIEW `AlertThresholdType7EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*75-bal.CoinAmount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*100-bal.CoinAmount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=7 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.CoinAmount<=cat.Threshold*75 OR (apa.EventSeverityTypeId>=3 AND bal.CoinAmount<=cat.Threshold*100)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType8EventClear_v` AlertThresholdType = 8
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType8EventClear_v` ;

CREATE 
VIEW `AlertThresholdType8EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*0.75-bal.BillCount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold-bal.BillCount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=8 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.BillCount<=cat.Threshold*0.75 OR (apa.EventSeverityTypeId>=3 AND bal.BillCount<=cat.Threshold)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType9EventClear_v` AlertThresholdType = 9
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType9EventClear_v` ;

CREATE 
VIEW `AlertThresholdType9EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*75-bal.BillAmount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*100-bal.BillAmount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=9 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.BillAmount<=cat.Threshold*75 OR (apa.EventSeverityTypeId>=3 AND bal.BillAmount<=cat.Threshold*100)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType10EventClear_v` AlertThresholdType = 10
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType10EventClear_v` ;

CREATE 
VIEW `AlertThresholdType10EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*0.75-bal.UnsettledCreditCardCount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold-bal.UnsettledCreditCardCount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=10 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.UnsettledCreditCardCount<=cat.Threshold*0.75 OR (apa.EventSeverityTypeId>=3 AND bal.UnsettledCreditCardCount<=cat.Threshold)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType11EventClear_v` AlertThresholdType = 11
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType11EventClear_v` ;

CREATE 
VIEW `AlertThresholdType11EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*75-bal.UnsettledCreditCardAmount END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE cat.Threshold*100-bal.UnsettledCreditCardAmount END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 

FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=11 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId
												AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                     OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 AND
																		    (bal.UnsettledCreditCardAmount<=cat.Threshold*75 OR (apa.EventSeverityTypeId>=3 AND bal.UnsettledCreditCardAmount<=cat.Threshold*100)))))
ORDER BY GapToMinor;

-- -----------------------------------------------------
-- `AlertThresholdType13EventClear_v` AlertThresholdType = 13
-- -----------------------------------------------------
DROP VIEW IF EXISTS `AlertThresholdType13EventClear_v` ;

CREATE 
VIEW `AlertThresholdType13EventClear_v`
AS
SELECT 	cat.Id AS CustomerAlertTypeId, 
		pos.Id AS PointOfSaleId, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE TIMESTAMPDIFF(SECOND, DATE_SUB(UTC_TIMESTAMP(), INTERVAL (cat.Threshold-1) DAY), bal.LastCollectionGMT) END AS GapToMinor, 
		0 AS GapToMajor, 
		CASE WHEN poss.IsActivated=0 THEN 1 ELSE TIMESTAMPDIFF(SECOND, DATE_SUB(UTC_TIMESTAMP(), INTERVAL cat.Threshold DAY), bal.LastCollectionGMT) END AS GapToCritical, 
		apa.Id AS POSEventCurrentId 


FROM 	PointOfSale pos 
		INNER JOIN POSBalance bal ON pos.Id=bal.PointOfSaleId 
		INNER JOIN POSStatus poss ON poss.PointOfSaleId = pos.Id
		INNER JOIN CustomerAlertType cat ON pos.CustomerId=cat.CustomerId AND (cat.AlertThresholdTypeId=13 AND cat.IsActive=1 AND cat.IsDeleted=0) 
		INNER JOIN POSEventCurrent apa ON (cat.Id=apa.CustomerAlertTypeId AND pos.Id=apa.PointOfSaleId 
		                                        AND apa.IsActive=1 AND (poss.IsActivated=0 
                                                                    OR (poss.IsActivated=1 AND poss.IsDeleted=0 AND poss.IsDecommissioned=0 
                                                                    AND (bal.LastCollectionGMT>DATE_SUB(UTC_TIMESTAMP(), INTERVAL (cat.Threshold-1) DAY) 
     												OR (apa.EventSeverityTypeId>=3 AND bal.LastCollectionGMT>DATE_SUB(UTC_TIMESTAMP(), INTERVAL cat.Threshold DAY))))))
ORDER BY GapToMinor;



