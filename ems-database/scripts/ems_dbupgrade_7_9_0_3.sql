INSERT INTO EmsProperties 
    SELECT MAX(Id) + 1, 'RedirectableSourceHosts', 'iris.digitalpaytech.com, ps.digitalpaytech.com', NOW(), 1 FROM EmsProperties;

INSERT INTO EmsProperties 
    SELECT MAX(Id) + 1, 'RedirectableDestHosts', 't2iris.digitalpaytech.com', NOW(), 1 FROM EmsProperties;
