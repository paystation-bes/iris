
ALTER TABLE CustomerSubscription 
ADD UNIQUE INDEX CustId_SubscriptionId_Unq (CustomerId ASC, SubscriptionTypeId ASC);

-- Update for BOSS in the cloud
ALTER TABLE Customer ADD UnifiId VARCHAR(50) NULL, ADD CONSTRAINT idx_unifi_id_unq UNIQUE(UnifiId);
UPDATE Customer SET UnifiId = Id WHERE ISNULL(UnifiId);

INSERT SubscriptionType (Id, ParentSubscriptionTypeId, Name, IsParent, IsPrivate, DisplayOrder, LastModifiedGMT, LastModifiedByUserId) VALUES (2000, NULL, 'Online Rate Configuration', 1, 0, 18, UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Id = Id;
UPDATE IGNORE CustomerSubscription SET SubscriptionTypeId = 2000 WHERE SubscriptionTypeId = 1600;

INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId)
SELECT Id, 1600, 1, 0, UTC_TIMESTAMP(), 1 FROM Customer
ON DUPLICATE KEY UPDATE CustomerId = CustomerId;

INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1011, 1000, 'Schedule Pay Station Settings Update', NULL, 0, UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Id = Id;
INSERT Permission (Id, ParentPermissionId, Name, Description, IsParent, LastModifiedGMT, LastModifiedByUserId) VALUES (1107, 1100, 'DPT View Device Groups', NULL, 0, UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Id = Id;

INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (3, 1011, 0, UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Id = Id;
INSERT RolePermission (RoleId, PermissionId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (1, 1107, 0, UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Id = Id;


-- Update for Chase Paymentech SHA-2 urls
UPDATE Processor SET TestUrl='https://netconnectvar1.chasepaymentech.com/NetConnect/controller,https://netconnectvar2.chasepaymentech.com/NetConnect/controller', ProductionUrl='https://netconnect1.chasepaymentech.com/NetConnect/controller,https://netconnect2.chasepaymentech.com/NetConnect/controller' WHERE Id=7 AND Name='processor.paymentech';

-- Update for Elavon Converge SHA-2 url
UPDATE Processor SET TestUrl='https://api.demo.convergepay.com/VirtualMerchantDemo/processxml.do', ProductionUrl='https://api.convergepay.com/VirtualMerchant/processxml.do' WHERE Id=16 AND Name='processor.elavon';

-- Configuration Group scheduled date cannot be before than current date minus 5 minutes.
INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('ConfigGroupScheduleMinutesBefore', '5', UTC_TIMESTAMP(), 1) ON DUPLICATE KEY UPDATE Name = Name;

-- Update EMSProperties.ReportsQueueIdleTimeLimitMinutes to 4 hours
UPDATE EmsProperties SET Value=240, LastModifiedGMT=UTC_TIMESTAMP(), LastModifiedByUserId=1 WHERE Name = 'ReportsQueueIdleTimeLimitMinutes';

-- Stored Procedures
DROP PROCEDURE IF EXISTS sp_InsertCustomer;

delimiter //
CREATE PROCEDURE sp_InsertCustomer (
  IN p_CustomerTypeId TINYINT UNSIGNED,
  IN p_CustomerStatusTypeId TINYINT UNSIGNED,
  IN p_ParentCustomerId MEDIUMINT UNSIGNED,
  IN p_CustomerName VARCHAR(25),
  IN p_TrialExpiryGMT DATETIME,
  IN p_IsParent TINYINT(1) UNSIGNED,
  IN p_UserName VARCHAR(765),
  IN p_Password VARCHAR(128),
  IN p_IsStandardReports TINYINT(1),
  IN p_IsAlerts TINYINT(1),
  IN p_IsRealTimeCC TINYINT(1),
  IN p_IsBatchCC TINYINT(1),
  IN p_IsRealTimeValue TINYINT(1),
  IN p_IsCoupons TINYINT(1),
  IN p_IsValueCards TINYINT(1),
  IN p_IsSmartCards TINYINT(1),
  IN p_IsExtendByPhone TINYINT(1),
  IN p_IsDigitalAPIRead TINYINT(1),
  IN p_IsDigitalAPIWrite TINYINT(1),
  IN p_Is3rdPartyPayByCell TINYINT(1),
  IN p_IsDigitalCollect TINYINT(1),
  IN p_IsOnlineConfiguration TINYINT(1),
  IN p_IsFlexIntegration TINYINT(1) ,
  IN p_IsCaseIntegration TINYINT(1) ,
  IN p_IsDigitalAPIXChange TINYINT(1),
  IN p_IsOnlineRate TINYINT(1),
  IN p_Timezone VARCHAR(40),
  IN p_UserId MEDIUMINT UNSIGNED,
  IN p_PasswordSalt VARCHAR(16)
)
BEGIN
    -- Version: 1 - Tuesday February 19 2013  
    
    -- Local variables
    DECLARE v_CustomerId MEDIUMINT UNSIGNED DEFAULT 0;
    DECLARE v_UserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserAccountId MEDIUMINT UNSIGNED;
    DECLARE v_ParentUserRoleId MEDIUMINT UNSIGNED;
    DECLARE v_LocationId MEDIUMINT UNSIGNED;
    
    -- If the time zone specified in parameter `p_Timezone` does not exist in view `Timezone_v.Name` then a default time zone of Id = 473 (Name = 'GMT') will be assigned to the Customer 
    DECLARE v_TimezoneId INT UNSIGNED DEFAULT 473;      -- prepare default timezone with Timezone_v.Id = 473 = 'GMT'
    DECLARE v_TimezoneName VARCHAR(40) DEFAULT 'GMT';

    -- Perform a ROLLBACK if there is a database issue
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;   -- exit on SQL exception
    DECLARE EXIT HANDLER FOR SQLWARNING BEGIN ROLLBACK; SELECT 0 AS CustomerId; END;     -- exit on SQL warning
    
    -- Verify the time zone specified by incoming parameter `p_Timezone` is valid (validate against the view `Timezone_v`), otherwise use a default value for Customer.TimezoneId (assigned in DECLARE statement above) 
    IF (SELECT COUNT(*) FROM Timezone_v WHERE Name = p_Timezone) = 1 THEN
        SELECT Id, Name INTO v_TimezoneId, v_TimezoneName FROM Timezone_v WHERE Name = p_Timezone;
    END IF;
    
    -- Start the atomic transaction
    START TRANSACTION;
    
    -- Insert Customer
    INSERT Customer (  CustomerTypeId,   CustomerStatusTypeId,   ParentCustomerId,   TimezoneId,           Name,   TrialExpiryGMT,   IsParent, VERSION, LastModifiedGMT, LastModifiedByUserId, IsMigrated,    MigratedDate)
    VALUES          (p_CustomerTypeId, p_CustomerStatusTypeId, p_ParentCustomerId, v_TimezoneId, p_CustomerName, p_TrialExpiryGMT, p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId,          1, UTC_TIMESTAMP());
    
    -- Get Customer.Id
    SELECT LAST_INSERT_ID() INTO v_CustomerId;
    
    UPDATE Customer SET UnifiId = v_CustomerId WHERE Id = v_CustomerId;
    
    -- Insert UserAccount: each new customer gets a user account with a first name of 'Administrator' (and no last name, i.e., an empty string for last name), immediately below this user will be assigned an administrator role
	
	-- Added on July 23 by Ashok (to take care of autoincrement ID)
	-- Added to force UserAccount.Id = 1 if there are no recrods in UserAccount Table. This is used as FK is many tables.
	
	IF ( SELECT COUNT(*) FROM UserAccount) = 0 THEN
		INSERT UserAccount (Id,  CustomerId, UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (1,	 v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);
        SET v_UserAccountId = 1;
    ELSE
		INSERT UserAccount (CustomerId,   UserStatusTypeId, CustomerEmailId,   UserName,       FirstName, LastName,   Password, IsPasswordTemporary, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
		VALUES             (v_CustomerId,                1,            NULL, p_UserName, 'Administrator',       '', p_Password,                   1,  p_IsParent,       0, UTC_TIMESTAMP(),             p_UserId, p_PasswordSalt);

        -- Get UserAccount.Id
        SELECT LAST_INSERT_ID() INTO v_UserAccountId;
    END IF ;

    
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES          (v_UserAccountId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    
    -- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES              (v_CustomerId, p_CustomerTypeId,       0, UTC_TIMESTAMP(),             p_UserId); 

        -- Insert UserDefaultDashboard (every user has access to the default dashboard)
    -- WARNING: TABLE 'UserDefaultDashboard' ONLY NEEDS TO BE IN THE 'KPI DB' DATABASE BUT IT IS BEING REFERENCED HERE IN THE 'EMS DB'
    --          WHEN ALL TABLES ARE FINALLY SEGREGATED BETWEEN THE 'KPI DB' AND THE 'EMS DB' DATABASES INSERTING INTO TABLE 'UserDefaultDashboard' WILL BE PERFORMED BY A TRIGGER ON THE `UserAccount` TABLE WITHIN THE 'KPI DB'
    INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES                      (v_UserAccountId,                  1,       0, UTC_TIMESTAMP(),             p_UserId);

    -- Assign Child Admin Role to Parent Customers
    IF p_IsParent = 1 THEN
    -- Insert UserRole: assign the new UserAccount record to the appropriate 'locked' administrator role (these 'locked' administrator roles come preconfigured with EMS 7 and cannot be modified)
    	INSERT UserRole (  UserAccountId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES          (v_UserAccountId, 				 3,       0, UTC_TIMESTAMP(),             p_UserId);   -- assign the locked Admin role to the new UserAccount 
    	-- Insert CustomerRole: assign the 'locked' administrator role to the customer (use incoming parameter 'p_CustomerTypeId')  
    	INSERT CustomerRole (  CustomerId,           RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	VALUES              (v_CustomerId, 				  3,       0, UTC_TIMESTAMP(),             p_UserId); 
    ELSEIF p_ParentCustomerId IS NOT NULL THEN
        -- This is child customer and it has a ParentCustomerId in CustomerTable
        INSERT UserAccount (CustomerId, UserStatusTypeId,   UserAccountId, CustomerEmailId,                      UserName, FirstName, LastName,  Password, IsPasswordTemporary, IsAliasUser, IsAllChilds, VERSION, LastModifiedGMT, LastModifiedByUserId,   PasswordSalt)
    	SELECT 			  v_CustomerId,	UserStatusTypeId,           ua.Id, CustomerEmailId, CONCAT(UserName,v_CustomerId), FirstName, LastName, 'NOTUSED',                   0,           1,           0,       0, UTC_TIMESTAMP(),             p_UserId,   PasswordSalt    
    	FROM UserAccount ua WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2;
    	
    	INSERT UserRole (  UserAccountId, RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                     ua2.Id,  ur.RoleId,       0, UTC_TIMESTAMP(),             p_UserId     
    	FROM UserRole ur 
    	INNER JOIN UserAccount ua ON ur.UserAccountId = ua.Id
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	INNER JOIN Role r ON ur.RoleId = r.Id AND r.CustomerTypeId = 3
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

    	INSERT CustomerRole (   CustomerId,    RoleId, VERSION, LastModifiedGMT, LastModifiedByUserId)
    	SELECT                v_CustomerId, cr.RoleId,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM CustomerRole cr
		INNER JOIN Role r ON (cr.RoleId = r.Id AND r.Id != 3)
		WHERE r.RoleStatusTypeId != 2
		AND r.CustomerTypeId = 3
		AND cr.CustomerId = p_ParentCustomerId;

		INSERT UserDefaultDashboard (  UserAccountId, IsDefaultDashboard, VERSION, LastModifiedGMT, LastModifiedByUserId)
		SELECT 							      ua2.Id,                  1,       0, UTC_TIMESTAMP(),             p_UserId
    	FROM UserAccount ua 
    	INNER JOIN UserAccount ua2 ON ua2.UserAccountId = ua.Id
    	WHERE ua.CustomerId = p_ParentCustomerId AND ua.IsAllChilds = 1 AND ua.UserStatusTypeId != 2 AND ua2.CustomerId = v_CustomerId;

END IF;
    
  
    -- A 'Child' customer type receives a full set of inserts (but 'Parent' and 'DPT' customer types do not)
    IF p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerCardType
        -- These are `locked` customer card types for credit cards, smart cards, and value cards
        -- These `CustomerCardType` records are used as foreign key parents for tables `CustomerCard` (lists of good/valid credit cards and smart cards) and `CustomerBadCard` (lists of bad/banned credit cards, smart cards, and value cards)
        -- Important: valid value cards (in table CustomerCard) have their own non-locked CustomerCardType record, but bad value cards used the locked 'generic' CustomerCardType record created here
        INSERT CustomerCardType (  CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern       , Description                                 , IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId)
        SELECT                   v_CustomerId,         Id,                   0, Name, 'No track 2 pattern', 'Created by DPT. Locked. Cannot be changed.',                0,        1,       0, UTC_TIMESTAMP(),             p_UserId           
        FROM CardType WHERE Id IN (1,2,3);
      
        -- Insert CustomerProperty: every 'Child' customer has 7 customer properties
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1, v_TimezoneName,       0, UTC_TIMESTAMP(),             p_UserId);   -- Timezone
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      2,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Query Spaces By (1=Location)
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      3,           '99',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max User Accounts
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      4,            '3',       0, UTC_TIMESTAMP(),             p_UserId);   -- Credit Card Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      5,            '5',       0, UTC_TIMESTAMP(),             p_UserId);   -- Max Offline Retry
    
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      6,           '10',       0, UTC_TIMESTAMP(),             p_UserId);   -- SMS Warning Period
        
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId,  PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      7,            '1',       0, UTC_TIMESTAMP(),             p_UserId);   -- Hidden Paystations Reported
   
        -- Insert Location: every customer has a location named 'Unassigned'
        INSERT Location (  CustomerId, ParentLocationId,         Name, NumberOfSpaces, TargetMonthlyRevenueAmount, Description, IsParent, IsUnassigned, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES          (v_CustomerId,             NULL, 'Unassigned',              0,                    		0,        NULL,        0,            1,         0,       0, UTC_TIMESTAMP(),             p_UserId);
    
        -- Get Location.Id
        SELECT LAST_INSERT_ID() INTO v_LocationId;
        
        -- Insert LocationDay and LocationOpen: the 'Unassigned' location is open 24 x 7 
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 1,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 2,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 3,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 4,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 5,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 6,  0, 95, 1, 0, p_UserId);
        CALL sp_InsertLocationDayAndOpen (v_LocationId, 7,  0, 95, 1, 0, p_UserId);
    
        -- Insert PaystationSetting: every customer has a default paystation setting named 'None' (because some pay-stations/point-of-sales do not have a paystation setting)
        -- There can only be one 'None' PaystationSetting for a Customer, therefore this SQL check
        INSERT PaystationSetting (  CustomerId, Name  , LastModifiedGMT, LastModifiedByUserId)
        VALUES                   (v_CustomerId, 'None', UTC_TIMESTAMP(), p_UserId            );
    
        -- Insert UnifiedRate: every customer has a default unified rate named 'Unknown' (because older versions of the 'PS App' do not send to EMS the name of the rate used when transacting a purchase)
        INSERT UnifiedRate (CustomerId  , Name     , LastModifiedGMT, LastModifiedByUserId)
        VALUES             (v_CustomerId, 'Unknown', UTC_TIMESTAMP(), p_UserId            );
    
    -- 'DPT' and 'Parent' Customers receive a small set of customer properties    
    ELSE 
        -- Insert CustomerProperty: time zone only
        INSERT CustomerProperty (  CustomerId, CustomerPropertyTypeId, PropertyValue, VERSION, LastModifiedGMT, LastModifiedByUserId)
        VALUES                  (v_CustomerId,                      1,    p_Timezone,       0, UTC_TIMESTAMP(), p_UserId            );   -- Timezone  
    END IF;
    
    -- 'Parent' and 'Child' customers subscribe to services (but a 'DPT' Customer does not)  
    IF p_CustomerTypeId = 2 OR p_CustomerTypeId = 3 THEN
    
        -- Insert CustomerSubscription: set all EMS services for the customer, insert the record even if the service is not subscribed to 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  100, p_IsStandardReports,   0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  200, p_IsAlerts,            0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  300, p_IsRealTimeCC,        0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  400, p_IsBatchCC,           0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  500, p_IsRealTimeValue,     0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  600, p_IsCoupons,           0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  700, p_IsValueCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  800, p_IsSmartCards,        0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId,  900, p_IsExtendByPhone,     0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1000, p_IsDigitalAPIRead,    0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1100, p_IsDigitalAPIWrite,   0, UTC_TIMESTAMP(), p_UserId); 
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1200, p_Is3rdPartyPayByCell, 0, UTC_TIMESTAMP(), p_UserId);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId, LicenseCount, LicenseUsed) VALUES (v_CustomerId, 1300, p_IsDigitalCollect,  0, UTC_TIMESTAMP(), p_UserId, 0, 0);
        INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1600, p_IsOnlineConfiguration, 0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1700, p_IsFlexIntegration,   0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1800, p_IsCaseIntegration,   0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 1900, p_IsDigitalAPIXChange, 0, UTC_TIMESTAMP(), p_UserId);
		INSERT CustomerSubscription (CustomerId, SubscriptionTypeId, IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES (v_CustomerId, 2000, p_IsOnlineRate,        0, UTC_TIMESTAMP(), p_UserId);
        
		
        -- Insert CustomerAlertType: every customer gets a customer-alert-type of `Pay Station Alert`, BUT it will only be referenced as a foreign key by tables `ActivePOSAlert` and `POSAlert` IF
        --                           the customer has subscribed to `Alerts` as a subscription type (see table `CustomerSubscription`, and table.attribute `SubscriptionType.Id` = 200 = `Alerts`)
        -- This only applies to a child customer, because parent and DPT customers do not have pay stations
        IF p_CustomerTypeId = 3 THEN
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    1,       NULL,    NULL,   'No Communication In 24 Hours',        25, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    2,       NULL,    NULL,  'Running Total Dollar Default',      1000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    6,       NULL,    NULL,   'Coin Canister Count Default',      2000, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    7,       NULL,    NULL, 'Coin Canister Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    8,       NULL,    NULL,    'Bill Stacker Count Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                    9,       NULL,    NULL,  'Bill Stacked Dollars Default',       750, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   10,       NULL,    NULL,    'Unsettled CC Count Default',       100, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   11,       NULL,    NULL,  'Unsettled CC Dollars Default',       500, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,                   12,       NULL,    NULL,     'Pay Station Alert Default',         1, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 
-- No default alert for Overdue collection
--            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
--            VALUES                   (v_CustomerId,                   13,       NULL,    NULL,       'No Collection in 7 Days',         7, 		   1,         0,         1,       0, UTC_TIMESTAMP(),             p_UserId); 

            INSERT CustomerAlertType (  CustomerId, AlertThresholdTypeId, LocationId, RouteId,                            Name, Threshold,   IsActive, IsDeleted, IsDefault, VERSION, LastModifiedGMT, LastModifiedByUserId)     
            VALUES                   (v_CustomerId,          		  12,       NULL,    NULL,             'Pay Station Alert',         1, p_IsAlerts,         0,         0,       0, UTC_TIMESTAMP(),             p_UserId); 
        END IF;
        
    END IF;
    
    -- Commit the atomic transaction;
    COMMIT;
    
    -- Return the Id of the newly created Customer record
    SELECT v_CustomerId AS CustomerId;
    
END//
delimiter ;

