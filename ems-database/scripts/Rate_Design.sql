
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `Rate`
-- -----------------------------------------------------

INSERT INTO Rate (Id, CustomerId, 									Name, 		RateTypeId, VERSION, 	LastModifiedGMT, LastModifiedByUserId) VALUES 
				(1, (Select Id from Customer where Name ='oranj') , '$12PerDay', 1 , 		1,			UTC_TIMESTAMP() , 					1) ;
				
INSERT INTO Rate (Id, CustomerId, 									Name, 				RateTypeId, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES 
				(2, (Select Id from Customer where Name ='oranj') , '$2PerHourCreditCard', 3 , 		1,		UTC_TIMESTAMP() , 					1) ;



-- -----------------------------------------------------
-- Table `RatePaymentType`
-- -----------------------------------------------------

INSERT INTO RatePaymentType (RateId, IsCoin, IsBill, IsCreditCard, IsContactless, IsSmartCard, IsPasscard, IsCoupon, IsPromptForCoupon, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
							(2,		 1,		 1,		 1,				1,				1,			1,			1,			1,				1,		UTC_TIMESTAMP(),	1) ;
							

-- -----------------------------------------------------
-- Table `RateAddTime`
-- -----------------------------------------------------

INSERT INTO RateAddTime (RateId, IsAddTimeNumber, IsSpaceOrPlateNumber, IsEbP, ServiceFeeAmount, MinExtention, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						(2,		 1,				   1,					1,		150,			 3,				1,		UTC_TIMESTAMP(),	1);
						
-- -----------------------------------------------------
-- Table `RateTypeFlat`
-- -----------------------------------------------------
						
INSERT INTO RateTypeFlat (RateId,	RateAmount,	VERSION,	LastModifiedGMT,	LastModifiedByUserId) VALUES
					     (2,		2000,		1,			UTC_TIMESTAMP(),	1);
						 
						 
-- -----------------------------------------------------
-- Table `RateTypeHourly`
-- -----------------------------------------------------

INSERT INTO RateTypeHourly( RateId, MaximumHours, HourRate01, HourRate02, HourRate03, HourRate04, HourRate05, HourRate06, HourRate07, HourRate08, HourRate09, HourRate10, HourRate11, HourRate12, HourRate13, HourRate14 ,HourRate15, HourRate16, HourRate17, HourRate18, HourRate19, HourRate20, HourRate21, HourRate22,  HourRate23, HourRate24, VERSION,LastModifiedGMT,LastModifiedByUserId ) VALUES
						  (2,		6,			 100,			200,		300,		400,		500,		600,      0,			0,			0,			0,			0,			0,			0,			0,		0,				0,			0,			0,			0,			0,			0,			0,			0,			0,			1,		UTC_TIMESTAMP(),	1) ;




-- -----------------------------------------------------
-- Table `RateTypeDaily`
-- -----------------------------------------------------

INSERT INTO RateTypeDaily (RateId, RateAmount, MaximumDays, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						 ( 2,		2000,		31,			1,		 UTC_TIMESTAMP(),	1);
						 

-- -----------------------------------------------------
-- Table `RateTypeMonthly`
-- -----------------------------------------------------

INSERT INTO RateTypeMonthly ( RateId,	RateAmount,		IsLimited,	MaximumPurchases,		DaysBeforeStart,		DaysAfterStart,	VERSION,	LastModifiedGMT,	LastModifiedByUserId) VALUES
						     ( 2,		5000,			0,			250,					1,						15,				1,			UTC_TIMESTAMP(),	1) ;
							 
-- -----------------------------------------------------
-- Table `RateTypeIncremental`
-- -----------------------------------------------------

INSERT INTO RateTypeIncremental (RateId, RateAmount, MinAmount, MaxAmount, IsFlatRateEnabled, FlatRateStartTime, FlatRateEndTime, CCInc01, CCInc02, CCInc03, CCInc04, CCInc05, CCInc06, CCInc07, CCInc08, CCInc09, CCInc10, VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
								( 2,	 100,		 500,		1000,	1,					  '10:00:00',		 '20:00:00',	  10,		0,		0,			0,		0,		0,		0,			0,		0,		0,		1,		UTC_TIMESTAMP(),	1);

-- -----------------------------------------------------
-- Table `RateTypeRestriction`
-- -----------------------------------------------------

INSERT INTO RateTypeRestriction (RateId, MessageTimeMin, Message1, Message2, Message3, Message4, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
						        (2,			5,		NULL,		NULL,	NULL,		NULL,	1,			UTC_TIMESTAMP(), 1) ;
								
-- -----------------------------------------------------
-- Table `RateProfile`
-- -----------------------------------------------------

INSERT INTO RateProfile (Id,	CustomerId,										Name,		PermitIssueTypeId,	VERSION,	LastModifiedGMT,	LastModifiedByUserId) VALUES
						(1,		(Select Id from Customer where Name ='oranj'),	'Profile1',	1,					1,			UTC_TIMESTAMP(),		1) ;
						

-- -----------------------------------------------------
-- Table `RateProfileRate`
-- -----------------------------------------------------

INSERT INTO RateProfileRate (Id,	CustomerId,									RateId,	RateProfileId,	Description,	IsRateBlendingEnabled,	IsOverride,	DisplayPriority,	AvailabilityDate,	AvailabilityStartTime,	AvailabilityEndTime,	DurationNumberOfDays,  DurationNumberOfHours, DurationNumberOfMins, IsMaximumExpiryEnabled, RateExpiryDayTypeId,	ExpiryTime, IsAdvancePurchaseEnabled, AdvancePurchaseHours, IsSunday, IsMonday, IsTuesday, IsWednesday, IsThursday, IsFriday, IsSaturday, VERSION, LastModifiedGMT, LastModifiedByUserId) VALUES
							(1,   (Select Id from Customer where Name ='oranj'), 2,		1,				'Test',			1,							1,		1,					NULL,				NULL,					NULL,					NULL,					NULL,					NULL,					1,				NULL,					NULL,		1,								NULL,			NULL,		NULL,		NULL,		NULL,		NULL,	NULL,		NULL,		1,		UTC_TIMESTAMP(),	1);
							


-- -----------------------------------------------------
-- Table `RateProfileLocation`
-- -----------------------------------------------------

INSERT INTO RateProfileLocation (Id,	CustomerId, 									RateProfileId, LocationId, 																													SpaceNumberStart, SpaceNumberEnd, IsPublished, StartGMT, 			EndGMT, 												VERSION, LastModifiedGMT, LastModifiedByUserId ) VALUES
								(1,		(Select Id from Customer where Name ='oranj'),	1,			  (select Id from PaystationSetting where CustomerId = (Select Id from Customer where Name ='oranj') and Name = 'Downtown'),		0,					0,				1,			UTC_TIMESTAMP(),	(select DATE_ADD(UTC_TIMESTAMP(), INTERVAL 1 YEAR)),	1,		 UTC_TIMESTAMP(),	1);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


