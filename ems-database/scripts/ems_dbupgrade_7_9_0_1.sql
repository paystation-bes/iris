-- IRIS-3939 Pay station modem details telemetry / IRIS-3954
ALTER TABLE `ModemSetting` ADD COLUMN `HardwareManufacturer` VARCHAR(50) NULL AFTER `VERSION`, ADD COLUMN `HardwareModel` VARCHAR(50) NULL AFTER `HardwareManufacturer`, ADD COLUMN `HardwareFirmware` VARCHAR(50) NULL AFTER `HardwareModel`;

-- IRIS-4024 [DEV] Make additional changes
UPDATE PaystationType SET `Name`='LUKE B' WHERE Id=6;