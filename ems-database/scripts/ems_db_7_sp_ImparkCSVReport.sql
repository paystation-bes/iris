
-- ***********************************************************************************************************************************************
-- PROCEDURE sp_ImparkCSVReport

DROP PROCEDURE IF EXISTS sp_ImparkCSVReport;

delimiter //

CREATE PROCEDURE sp_ImparkCSVReport( in P_BranchName VARCHAR(20))

BEGIN

IF P_BranchName = ('Advanced Parking') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/ImparkAdvancedParking.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Advanced Parking','Impark Advanced','Larco','Advanced Telus Garden')  and 
L.Id not in (29612 ,29597 ,29587 ,29617 ,29582 ,29592 ,29602 ,29567 ,29607 , 4107 )and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;




IF P_BranchName = ('BCI') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/BCI.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('BC Cancer Agency','Delta Grand Okanagan','Impark BCI','Impark Kamloops','Impark Kelowna','Impark PG','Penticton Airport')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Seattle') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Seattle.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Bellevue College','City of Bremerton','Impark Seattle')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;



IF P_BranchName = ('Toronto') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Toronto.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Colliers Intl','Impark Kensington','Impark Stockton','Impark Toronto','Larco','Toronto Airport West')  and 
L.Id not in (4107, 29567, 29572, 29577, 29582, 29587, 29607, 29612, 29617) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF;


IF P_BranchName = ('Vancouver') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Vancouver.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('BC Ferries ImPark','Impark BCIT','Impark UFV','Impark Van Lot 2298','Impark Vancouver','River Rock','Impark Kwantlen','Larco','City of Richmond')  and 
L.Id not in (4107,29572,29577,29582,29587,29592,29597,29602,29607,29617) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Edmonton') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Edmonton.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Edmonton Int Airport','Impark Edmonton','Larco')  and 
L.Id not in (4107,29567,29572,29577,29582,29592,29597,29602,29607,29612,29617) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Calgary') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Calgary.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Calgary','Impark NW Properties','Larco')  and 
L.Id not in (4107,29567,29572,29577,29587,29592,29597,29602,29607,29612,29617) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Chicago') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Chicago.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Chicago')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;

-- New Profile added - Halifax

IF P_BranchName = ('Halifax') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Halifax.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Halifax')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;



IF P_BranchName = ('Hamilton') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Hamilton.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Hamilton')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Milwaukee') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Milwaukee.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Milwaukee')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Minneapolis') THEN


(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Minneapolis.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Minneapolis','Impark St Paul')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('NYS') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/NYS.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark NY')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Montreal') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Montreal.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Montreal','Larco')  and 
L.Id not in (4107,29567,29572,29577,29582,29587,29592,29597,29602,29607,29612) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;

IF P_BranchName = ('Regina') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Regina.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Regina','Impark HFT Parkades')  and 
L.Id not in (4557,31327) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Sasaktoon') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Sasaktoon.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Saskatoon','Impark HFT Parkades')  and 
L.Id not in (4557,31332) and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;

IF P_BranchName = ('San Francisco') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/SanFrancisco.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark SF','Impark Fort Mason')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Winnipeg') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Winnipeg.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Impark Winnipeg')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Metro Parking') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/MetroParking.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Metro Parking','Richmond Hospital','Vancouver Coastal Health')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


IF P_BranchName = ('Ottawa') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/Ottawa.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('Larco')  and 
L.Id in (29607) and
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+7 DAY,"%Y-%m-%d 00:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 23:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;

IF P_BranchName = ('CCP') THEN

(SELECT 'Profile Name', 'Setting',  'Machine', 'Purchase date', 'Processing date', 'Request type', 'Card type', 'Card #','Auth #','Reference #','Merchant account',  'Orig Amount',  'Request Amount','Ticket #')
UNION ALL
(
SELECT  
C.Name as 'Profile Name', L.Name as 'Setting', POS.Name as 'Machine', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Purchase date', 
CONCAT(DATE_FORMAT(CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue),'%m/%d/%Y %H:%i:%s'),' ',tztt.Abbreviation) as 'Processing date', 
PTT.Name as 'Request type', PT.CardType as 'Card type', PT.Last4DigitsOfCardNumber as 'Card #', 
PT.AuthorizationNumber as 'Auth #', PT.ReferenceNumber as 'Reference #', MA.Name as 'Merchant account', round(P.OriginalAmount/100,2) 'Orig Amount', round(PT.Amount/100,2) as 'Request Amount',
PT.TicketNumber as 'Ticket #'  

INTO OUTFILE '/tmp/impark/CCP.csv' 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'


FROM 
ProcessorTransaction PT, 
PointOfSale POS, 
Customer C, 
MerchantAccount MA, 
Location L, 
ProcessorTransactionType PTT, 
Purchase P, 
CustomerProperty CP ,
mysql.time_zone_name tzn, 
mysql.time_zone_transition_type tztt
WHERE  
C.Id = CP.CustomerId and 
CP.CustomerPropertyTypeId = 1 and 
PT.PointOfSaleId = POS.Id and 
POS.CustomerId = C.Id and 
CP.PropertyValue = tzn.Name and 
tzn.Time_zone_id = tztt.Time_zone_id and 
tztt.transition_type_id= 1 and
C.Name in ('City Center')  and 
MA.Id = PT.MerchantAccountId and 
POS.LocationId = L.Id and 
PT.ProcessorTransactionTypeId = PTT.Id and 
PT.PurchaseId = P.Id and  

PT.ProcessorTransactionTypeId in (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) and
PT.IsApproved =1 and
CONVERT_TZ(PT.ProcessingDate,'GMT',CP.PropertyValue) 
BETWEEN 
date_format(now() - INTERVAL WEEKDAY(now())+8 DAY,"%Y-%m-%d 15:00:00") and
date_format(now() - INTERVAL WEEKDAY(now())+1 DAY,"%Y-%m-%d 14:59:59") 

ORDER BY 
C.Name, L.Name, CONVERT_TZ(PT.PurchasedDate,'GMT',CP.PropertyValue)
);

END IF ;


END//
delimiter ;