#! /bin/bash


find /tmp/impark/ -name '*.csv'  -type f -exec rm -rf {} \;
find /tmp/impark/ -name '*._zip'  -type f -exec rm -rf {} \;

mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Advanced Parking')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('BCI')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Seattle')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Toronto')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Vancouver')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Edmonton')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Calgary')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Chicago')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Halifax')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Hamilton')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Milwaukee')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Minneapolis')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('NYS')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Montreal')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Regina')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Sasaktoon')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('San Francisco')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Winnipeg')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Metro Parking')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('Ottawa')"
mysql -u root --database ems7 --execute="CALL sp_ImparkCSVReport('CCP')"


zip -jrm /tmp/impark/ImparkAdvancedParking._zip /tmp/impark/ImparkAdvancedParking.csv
zip -jrm /tmp/impark/BCI._zip /tmp/impark/BCI.csv
zip -jrm /tmp/impark/Seattle._zip /tmp/impark/Seattle.csv
zip -jrm /tmp/impark/Toronto._zip /tmp/impark/Toronto.csv
zip -jrm /tmp/impark/Vancouver._zip /tmp/impark/Vancouver.csv
zip -jrm /tmp/impark/Edmonton._zip /tmp/impark/Edmonton.csv
zip -jrm /tmp/impark/Calgary._zip /tmp/impark/Calgary.csv
zip -jrm /tmp/impark/Chicago._zip /tmp/impark/Chicago.csv
zip -jrm /tmp/impark/Halifax._zip /tmp/impark/Halifax.csv
zip -jrm /tmp/impark/Hamilton._zip /tmp/impark/Hamilton.csv
zip -jrm /tmp/impark/Milwaukee._zip /tmp/impark/Milwaukee.csv
zip -jrm /tmp/impark/Minneapolis._zip /tmp/impark/Minneapolis.csv
zip -jrm /tmp/impark/NYS._zip /tmp/impark/NYS.csv
zip -jrm /tmp/impark/Montreal._zip /tmp/impark/Montreal.csv
zip -jrm /tmp/impark/Regina._zip /tmp/impark/Regina.csv
zip -jrm /tmp/impark/Sasaktoon._zip /tmp/impark/Sasaktoon.csv
zip -jrm /tmp/impark/SanFrancisco._zip /tmp/impark/SanFrancisco.csv
zip -jrm /tmp/impark/Winnipeg._zip /tmp/impark/Winnipeg.csv
zip -jrm /tmp/impark/MetroParking._zip /tmp/impark/MetroParking.csv
zip -jrm /tmp/impark/Ottawa._zip /tmp/impark/Ottawa.csv
zip -jrm /tmp/impark/CCP._zip /tmp/impark/CCP.csv

/usr/bin/python /opt/bin/impark_email.py
