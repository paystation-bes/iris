

-- Procedure begin sp_InsertUtilizationDay 
DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_InsertUtilizationDay` $$
CREATE PROCEDURE `sp_InsertUtilizationDay` (P_CustomerId MEDIUMINT UNSIGNED, 
											P_LocationId MEDIUMINT UNSIGNED, 
											P_PermitBeginingDayTimeIdLocal MEDIUMINT UNSIGNED,
											P_PermitEndingDayTimeIdLocal MEDIUMINT UNSIGNED,
											P_TotalMinutesPurchased INT UNSIGNED,
											P_MinitesForCurrentDay INT UNSIGNED)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare FromDate,lv_LastRunDate date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month,lv_DayTimeid, lv_MinutesForCurrentDay , lv_TotalMinutesPurchased,lv_insert_RemainingminutesPurchase,lv_tmp_minutes int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;

declare c1 cursor  for
SELECT id, P_MinitesForCurrentDay , P_TotalMinutesPurchased
FROM Time
WHERE id BETWEEN P_PermitBeginingDayTimeIdLocal 
AND P_PermitEndingDayTimeIdLocal
and TimeAmPm = '00:00 AM'
;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

-- 1) Check if P_PurchaseBeginingTimeIdLocal and P_PurchaseEndingTimeIdLocal is within a day
-- 2) 

set indexm_pos =0;
set idmaster_pos =0;

set lv_insert_RemainingminutesPurchase = 0;
set lv_tmp_minutes = 0 ;
open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_DayTimeid, lv_MinutesForCurrentDay , lv_TotalMinutesPurchased ;

    -- Find the current local time of the Customer
	
	if (indexm_pos = 0) then
		if (lv_insert_RemainingminutesPurchase+lv_MinutesForCurrentDay <= lv_TotalMinutesPurchased ) then
			
			INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 		NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_DayTimeid,	P_CustomerId,	P_LocationId, 		0,							0,							1,				  lv_MinutesForCurrentDay )
			ON DUPLICATE KEY UPDATE NumberOfPurchases= NumberOfPurchases+1, TotalMinutesPurchased=TotalMinutesPurchased+lv_MinutesForCurrentDay ;
			
			set lv_insert_RemainingminutesPurchase = lv_TotalMinutesPurchased - lv_MinutesForCurrentDay ;
			set lv_tmp_minutes = lv_MinutesForCurrentDay ;
		end if;
		
	else

			if (lv_insert_RemainingminutesPurchase  > 1440 ) or (lv_insert_RemainingminutesPurchase  = 1440 ) then
				set lv_tmp_minutes = 1440 ;		
			else
				set lv_tmp_minutes = lv_insert_RemainingminutesPurchase ;
			end if;
		
		INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 		NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_DayTimeid,	P_CustomerId,	P_LocationId, 		0,							0,							1,				  lv_tmp_minutes )
			ON DUPLICATE KEY UPDATE NumberOfPurchases= NumberOfPurchases+1,TotalMinutesPurchased=TotalMinutesPurchased+lv_MinutesForCurrentDay ; 
	
		set lv_insert_RemainingminutesPurchase = lv_insert_RemainingminutesPurchase - lv_tmp_minutes;
	
	end if;

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_InsertUtilizationDay  



