DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthly` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthly` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_Previous_date = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check if data has been posted for previous day
	SELECT date(lv_Customer_LocalTime - INTERVAL 1 Day) INTO lv_Previous_date ;
	
	SELECT COUNT(*) INTO lv_record_cnt 
	FROM ETLMonthlyStatus WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId AND DataRunDate = lv_Previous_date ;
	
	IF (lv_record_cnt = 0) THEN
		
		-- means no data has been posted
		-- Get the data from UtilizationDay for previous day
		-- Get the begining TimeID of current month and next month
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
		
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
				
		SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth = day(LAST_DAY(lv_Previous_date)) and Quarterofday = 95;
		
		-- Think about the autoIncrement PK
		DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		
		-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
		SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	lv_Previous_date,	lv_ETLStartTime, lv_ETLEndTime );
		
		
	
	-- ELSE
	
		-- SELECT 'Already Processed for this date' AS  Trace;
	
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- 


DROP PROCEDURE IF EXISTS sp_UpdateUtilMonthly ;

delimiter //

CREATE PROCEDURE sp_UpdateUtilMonthly (
									   IN P_CustomerId MEDIUMINT UNSIGNED, 
									   IN P_LocationId MEDIUMINT UNSIGNED,
									   IN P_NumberOfPurchases MEDIUMINT UNSIGNED, 
									   IN P_TotalMinutesPurchased INT UNSIGNED,
									   IN P_BeginPermitGMT DATETIME,
									   IN P_Customer_Timezone char(25))
BEGIN

declare NO_DATA int(4) default 0;
declare lv_ClusterId INT;
declare lv_TimeIdBeginingMonthLocal,lv_cnt_UtilMonth MEDIUMINT UNSIGNED  DEFAULT NULL ;
declare continue handler for not found set NO_DATA=-1;


/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/
		-- Get the begining MonthIDLocal of the Customer
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
		and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;

		SELECT Count(*) INTO lv_cnt_UtilMonth FROM UtilizationMonth
		WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
		
		IF lv_cnt_UtilMonth = 1 THEN
			
			UPDATE UtilizationMonth SET NumberOfPurchases = NumberOfPurchases + P_NumberOfPurchases ,TotalMinutesPurchased = TotalMinutesPurchased + P_TotalMinutesPurchased
			WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
			
		-- ELSE
			-- SELECT ' IN ELSE';
		END IF ;
END//

delimiter ;

-- Process Monthly Utilization for past data

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthlyPastTxn` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthlyPastTxn` (IN P_Year SMALLINT,
														IN P_Month SMALLINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;



declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;

	SELECT UTC_TIMESTAMP() INTO lv_ETLStartTime ;
	
    -- Find the TimeIDLocal for Begining Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month and dayofmonth=1 and Quarterofday = 0;
	
	-- Find the TimeIDLocal for Last Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month  and IsLastDayOfMonth = 1 and Quarterofday = 95;
	
		
	-- Think about the autoIncrement PK
	DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
	INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,		MaxUtilizationMinutes,		NumberOfPurchases, 	TotalMinutesPurchased)
	SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	SUM(NumberOfSpaces),SUM(MaxUtilizationMinutes), SUM(NumberOfPurchases) , SUM(TotalMinutesPurchased)
	FROM UtilizationDay
	WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
	GROUP by CustomerId, LocationId ; 
		
	-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
	SELECT UTC_TIMESTAMP() INTO lv_ETLEndTime ;
	
	INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	date(UTC_TIMESTAMP()),	lv_ETLStartTime, lv_ETLEndTime );
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

		