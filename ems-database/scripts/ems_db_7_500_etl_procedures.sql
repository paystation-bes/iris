﻿/*
  --------------------------------------------------------------------------------
 -- Modified sp_MigrateSettledTransactionIntoKPI_Inc on Aug 27 For Card Processing Widget
 
 -- Copied the contents from GA Release on March 31 2014
 -- Made Changes to UtilizationDay and UtilizationMonth
 -- Added ETLDiscardedData as ErrorHandling
 
 -- Modified on 2014-02-18 
 -- Added logic to populate OccupancyMonth in sp_MigrateHourlyOccupancyToDaily and sp_MigrateHourlyOccupancyToDailyPastTxn
 
 -- Modified on 2014-02-15 
 -- sp_InsertUtilizationDay and sp_MigrateOccupancyIntoKPI_Inc
 -- Removed Events from this file
 -- sp_FillLocationDetails_PastTxn and sp_FillLocationDetails and sp_FillLocationDetails_PastTxn_Customer
 
 
 
  -- Modified on 2013-12-31
  -- sp_FillLocationDetails and sp_FillLocationDetails_PastTxn Increase datatype from MEDIUMINT to BIGINT
  
  -- Modified on 2013-12-17
  -- sp_FillLocationDetails and EVENT_FillETLLocationDetails
  
  -- Modified on 2013-11-18
  -- lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location
  
  -- Modified on 2013-11-12
  -- Updated OccupancyDay.NumberOfSpaces and OccupancyDay.NoOfPermits
  
  -- Modified on 2013-11-07
  -- JIRA 4624
  
  -- Modified on 2013-10-29
  -- JIRA 4556
  
  -- Modified on 2013-10-23
  -- JIRA 4303
  -- Added ExcessPaymentAmount to ChargedAmount for Revenue
  	

  -- Modified on 2013-10-18
  -- Added condition to fill Occupancy details if No. of spaces exists in Location
  -- Added Bulk Update for Occupancy with Where condition
  
  -- Modifed on 2013-10-08
  -- Moved Settled Transactions procedure after Revenue Procedure
  -- Limit the Records selected for ETL from EmsProperties
  -- Procedure sp_ETLReset() to Reset ETL Job
  
  -- Modifed on 2013-10-02
  -- Related to JIRA 4435
  
  -- Modified on 2013-09-30
  -- Relates to JIRA 4431 Added the condition T.IsApproved = 1 for SettledTransactions
  
  -- Modified on 2013-09-29 
  -- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
  
  Procedure:  sp_MigrateRevenueIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: CALL sp_MigrateRevenueIntoKPI_Inc(1,1,'2012-10-01 10:00:00','2012-10-01 11:00:00');
  -------------------------------------------------------------------------------- 
*/

DROP PROCEDURE IF EXISTS sp_MigrateRevenueIntoKPI_Inc;

delimiter //

CREATE PROCEDURE sp_MigrateRevenueIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN
-- Last modified on June 11
-- DECLARE SECTION
declare lv_TimeIdGMT MEDIUMINT UNSIGNED ;
declare lv_TimeIdLocal MEDIUMINT UNSIGNED;
declare lv_Customer_Timezone char(25); -- it is char(64) 
declare lv_Purchase_CustomerId MEDIUMINT UNSIGNED ;
declare lv_Purchase_LocationId MEDIUMINT UNSIGNED ;
declare lv_Permit_LocationId MEDIUMINT UNSIGNED ;	
declare lv_Permit_SpaceNumber MEDIUMINT UNSIGNED ;	
declare lv_Purchase_PointOfSaleId MEDIUMINT UNSIGNED ;
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
declare lv_Purchase_UnifiedRateId MEDIUMINT UNSIGNED ;
declare lv_Purchase_PaystationSettingId MEDIUMINT UNSIGNED ;
declare lv_Purchase_Id BIGINT ;
declare lv_Permit_Id BIGINT ;
declare lv_Permit_PermitIssueTypeId TINYINT UNSIGNED ;
declare lv_Permit_PermitTypeId TINYINT UNSIGNED ;
declare lv_Purchase_TransactionTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PaymentTypeId TINYINT UNSIGNED ;
declare lv_ProcessorTransaction_TypeId TINYINT UNSIGNED ;		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
declare lv_Purchase_CoinCount TINYINT UNSIGNED ;				-- PPPInfo.CoinCount
declare lv_Purchase_BillCount TINYINT UNSIGNED ;				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
declare lv_Purchase_CouponId MEDIUMINT UNSIGNED ;				-- PPPInfo.IsCoupon check null/not null
declare lv_Purchase_CashPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_CardPaidAmount MEDIUMINT UNSIGNED ;			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
declare lv_Purchase_IsOffline TINYINT UNSIGNED ;				-- PPPInfo.IsOffline
declare lv_Purchase_IsRefundSlip TINYINT UNSIGNED ;			-- PPPI
DECLARE lv_Purchase_ChargedAmount MEDIUMINT UNSIGNED ;
DECLARE CursorDone INT DEFAULT 0;
DECLARE RowNumber                               INT UNSIGNED DEFAULT 0;
DECLARE RowsToMigrate                           INT UNSIGNED DEFAULT 0;
declare lv_PayStation_Id MEDIUMINT UNSIGNED ;
declare lv_PaystationTypeId TINYINT UNSIGNED ;
declare lv_Purchase_PurchaseGMT DATETIME;
declare lv_ppp_IsFreePermit TINYINT UNSIGNED ;
declare idmaster_pos,indexm_pos int default 0;
declare lv_ppp_Id BIGINT;
declare lv_PermitBeginGMT DATETIME;
declare lv_PermitExpireGMT DATETIME;
-- Payment Card columns
declare lv_PaymentCard_Amount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_CardTypeID TINYINT UNSIGNED ;
declare lv_PaymentCard_CreditCardTypeID TINYINT UNSIGNED ;
declare	lv_PaymentCard_MerchantAccountID MEDIUMINT UNSIGNED ;

declare lv_Purchase_CoinPaidAmount MEDIUMINT UNSIGNED ;
declare lv_Purchase_BillPaidAmount MEDIUMINT UNSIGNED ;
declare lv_PaymentCard_Id BIGINT;
declare lv_Permit_OriginalPermitId BIGINT;

declare lv_PermitBeginLocal  DATETIME;
declare lv_occ_BeginTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_occ_ExpireTimeIdGMT  MEDIUMINT UNSIGNED;
declare lv_PermitExpireLocal  DATETIME;
declare lv_occ_ExpireTimeIdLocal  MEDIUMINT UNSIGNED;
declare lv_occ_BeginMinutes  TINYINT UNSIGNED ;
declare lv_occ_ExpireMinutes  TINYINT UNSIGNED ;
declare lv_occ_DurationMinutes  MEDIUMINT UNSIGNED;

declare lv_TimeIdDayLocal mediumint unsigned;
declare lv_TimeIdMonthLocal mediumint unsigned;
declare lv_PPPTotalHour_Id bigint unsigned;
declare lv_PPPDetailHour_Id  bigint unsigned;
declare lv_PPPTotalDay_Id  bigint unsigned;
declare lv_PPPDetailDay_Id  bigint unsigned;
declare lv_PPPTotalMonth_Id  bigint unsigned;
declare lv_PPPDetailMonth_Id  bigint unsigned;
declare lv_ProcessorTransaction_Id bigint unsigned;
declare lv_ETLLimit INT UNSIGNED;
declare lv_no_of_spaces_location mediumint unsigned ;

-- CURSOR C1 STATEMENT
DECLARE C1 CURSOR FOR
	
		SELECT distinct
		P.CustomerId,
		P.LocationId,
		ifnull(R.LocationId,0),
		ifnull(R.SpaceNumber,0),
		P.PointOfSaleId,
		P.UnifiedRateId,
		P.PaystationSettingId,
		P.Id,
		R.Id,
		ifnull(R.PermitIssueTypeId,0),
		ifnull(R.PermitTypeId,0),		
		P.TransactionTypeId,
		P.PaymentTypeId,
		T.ProcessorTransactionTypeId, 
		P.CoinCount,
		P.BillCount,
		P.CouponId,
		P.CashPaidAmount,
		P.CardPaidAmount,
		P.IsOffline,
		P.IsRefundSlip,
		P.PurchaseGMT,
		R.PermitBeginGMT,
		R.PermitExpireGMT,
		C.Amount,
		C.CardTypeID,
		C.CreditCardTypeID,
		C.MerchantAccountID,
		P.CoinPaidAmount,
		P.BillPaidAmount,
		C.Id,
		ifnull(R.OriginalPermitId,R.Id),
		B.PropertyValue,
		P.ChargedAmount+P.ExcessPaymentAmount,
		T.Id			-- Added on June 11 for Incremental ETL
		from StagingPurchase P
		left outer join    StagingPermit R on  P.Id = R.PurchaseId
		left outer join   StagingPaymentCard C on P.Id = C.PurchaseId
		left outer join   StagingProcessorTransaction T on P.Id = T.PurchaseId
		left join CustomerProperty B on P.CustomerId = B.CustomerId
		WHERE  -- P.CustomerId in (160) AND /* This was used for micro-migration only */
		B.CustomerPropertyTypeId = 1 AND PropertyValue IS NOT NULL LIMIT lv_ETLLimit;

		--  AND P.PurchaseGMT between P_BeginPurchaseGMT AND P_EndPurchaseGMT
		
		
		-- specify a date range
		
 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PurchaseId,		CustomerId,				PointOfSaleId,				PurchaseGMT, 			 ETLObject, 	record_insert_time )  values(
      lv_Purchase_Id,	lv_Purchase_CustomerId,	lv_Purchase_PointOfSaleId,	lv_Purchase_PurchaseGMT, 'Purchase', 	NOW() );
	  
	  -- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
	  -- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
	  INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
	  IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
	  IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
	  -- commented on Oct 28
	  -- IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
		
	  DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
	  DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
	  DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
	  -- commented on Oct 28
	  -- DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
	  COMMIT ; 
	  -- set autocommit = 1 ;
  END;


 
 -- set lv_Customer_Timezone = null;
 
 -- select PropertyValue into lv_Customer_Timezone
 -- from CustomerProperty where CustomerId = P_CustomerId and CustomerPropertyTypeId=1;
 
 -- DELETE FROM ETLQueueRecordCount where ETLProcessDateRangeId = P_ETLExecutionLogId ;
 
 SELECT Value INTO lv_ETLLimit FROM EmsProperties WHERE Name = 'ETLLimit';
 
 
 
 OPEN C1;
	set idmaster_pos = (select FOUND_ROWS());
-- select idmaster_pos;

	UPDATE ETLExecutionLog
	SET PurchaseRecordCount  = idmaster_pos,
	PPPCursorOpenedTime = NOW()
	WHERE Id = P_ETLExecutionLogId;
	
	/*INSERT INTO ETLQueueRecordCount
	(ETLProcessDateRangeId,		TotalRecords,	ETLRemaingRecords,	LastModifiedGMT) VALUES
	(P_ETLExecutionLogId,	idmaster_pos,	idmaster_pos,		NOW());*/
	
-- AUTOCOMMIT OFF FOR THE SESSION ASHOK1
-- set autocommit = 0 ;

while indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	lv_Purchase_CustomerId, -- PPPInfo.CustomerID
					lv_Purchase_LocationId, -- PPPInfo.PurchaseLocationId
					lv_Permit_LocationId,	-- PPPInfo.PermitLocationId
					lv_Permit_SpaceNumber,	-- PPPInfo.SpaceNumber	
					lv_Purchase_PointOfSaleId,	--	PPPInfo.PointOfSaleId
												--  PPPInfo.PaystationId
												-- 	PPPInfo.PaystationTypeId
					lv_Purchase_UnifiedRateId,	-- PPPInfo.UnifiedRateId
					lv_Purchase_PaystationSettingId,	--	PPPInfo.PaystationSettingId
					lv_Purchase_Id,						-- PPPInfo.PurchaseId
					lv_Permit_Id,						-- PPPInfo.PermitId
					lv_Permit_PermitIssueTypeId,		--	PPPInfo.PermitIssueTypeId
					lv_Permit_PermitTypeId,				--	PPPInfo.PermitTypeId 
					lv_Purchase_TransactionTypeId,		-- PPPInfo.TransactionTypeId
					lv_Purchase_PaymentTypeId,			-- PPPInfo.PaymentTypeId
					lv_ProcessorTransaction_TypeId,		-- PPPInfo.ProcessorTransactionTypeId	NEED TO BRING IN PROCESSORTRANSACTION TABLE
					lv_Purchase_CoinCount,				-- PPPInfo.CoinCount
					lv_Purchase_BillCount,				-- PPPInfo.BillCount
														-- PPPInfo.DurationMinutes
					lv_Purchase_CouponId,				-- PPPInfo.IsCoupon check null/not null
					lv_Purchase_CashPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_CardPaidAmount,			-- FOR PPPInfo.IsFreePermit		Check if Purchase.CashPaidAmount = 0 and CARD PAid is 0 then free permit
					lv_Purchase_IsOffline,				-- PPPInfo.IsOffline
					lv_Purchase_IsRefundSlip,			-- PPPInfo.IsRefundSlip
					lv_Purchase_PurchaseGMT,
					lv_PermitBeginGMT,
					lv_PermitExpireGMT,
					lv_PaymentCard_Amount,
					lv_PaymentCard_CardTypeID,
					lv_PaymentCard_CreditCardTypeID,
					lv_PaymentCard_MerchantAccountID,
					lv_Purchase_CoinPaidAmount,
					lv_Purchase_BillPaidAmount,
					lv_PaymentCard_Id,
					lv_Permit_OriginalPermitId,
					lv_Customer_Timezone,
					lv_Purchase_ChargedAmount,
					lv_ProcessorTransaction_Id;	
								
			
		-- STEP 1: TimeIdGMT --> Purchase.PurchaseGMT GET the ID ftom TIME Table 
		-- SELECT MAX(Id) into lv_TimeIdGMT FROM time WHERE datetime <= lv_Purchase_PurchaseGMT;
		-- select 'aaa';
		SELECT id into lv_TimeIdGMT FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= lv_Purchase_PurchaseGMT);
		
		-- SELECT MAX(Id) INTO lv_TimeIdLocal FROM time where DateTime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone);
				
		SELECT Id INTO lv_TimeIdLocal FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
					
		
		-- STEP 3: CustomerId --> Purchase.CustomerId
		-- STEP 4: PurchaseLocationId --> Purchase.LocationId
		-- STEP 5: PermitLocationId --> Permit.LocationId
		-- STEP 6: SpaceNumber --> Permit.SpaceNumber
		-- STEP 7: PointOfSaleId --> Purchase.PointOfSaleId
		
		
		-- STEP 8: PaystationId --> LOOKUP from PointofSale to PayStation
		SELECT PayStationId INTO lv_PayStation_Id from PointOfSale where Id = lv_Purchase_PointOfSaleId;
		
		
		-- STEP 9: PaystationTypeId --> paystation.PaystationTypeId
		SELECT PaystationTypeId INTO lv_PaystationTypeId FROM Paystation where Id= lv_PayStation_Id;
		
				
		set lv_ppp_IsFreePermit = 0;
		SET lv_ppp_IsFreePermit  = IF ((lv_Purchase_CashPaidAmount = 0 AND lv_Purchase_CardPaidAmount = 0),1,0);
		
		/*INSERT INTO PPPInfo
		(TimeIdGMT, 	TimeIdLocal, 	CustomerId, 			PurchaseLocationId, 	PermitLocationId, 		SpaceNumber, 			PointOfSaleId, 				PaystationId, 		PaystationTypeId, 		UnifiedRateId, 				PaystationSettingId, 			PurchaseId, 		PermitId, 		PermitIssueTypeId, 				PermitTypeId, 			TransactionTypeId, 				PaymentTypeId, 					ProcessorTransactionTypeId, 	CoinCount, 				BillCount, 				DurationMinutes, 																	IsCoupon, 										IsFreePermit, 					 IsOffline, 			IsRefundSlip, 				IsRFID, IsUploadedFromBoss) VALUES
		(lv_TimeIdGMT,	lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Permit_SpaceNumber,	lv_Purchase_PointOfSaleId,	lv_PayStation_Id,	lv_PaystationTypeId,	lv_Purchase_UnifiedRateId,	lv_Purchase_PaystationSettingId,lv_Purchase_Id,		lv_Permit_Id,	lv_Permit_PermitIssueTypeId,	lv_Permit_PermitTypeId,	lv_Purchase_TransactionTypeId,	lv_Purchase_PaymentTypeId,		lv_ProcessorTransaction_TypeId,	lv_Purchase_CoinCount,	lv_Purchase_BillCount,	ifnull((TO_SECONDS(lv_PermitExpireGMT) - TO_SECONDS(lv_PermitBeginGMT))/60,0),		if(length(lv_Purchase_CouponId)>0,1,0),			lv_ppp_IsFreePermit,			 lv_Purchase_IsOffline,	lv_Purchase_IsRefundSlip,	1,		1				);		*/
		
		
		INSERT INTO PPPTotalHour
		(TimeIdGMT,    TimeIdLocal, 	CustomerId, 			TotalAmount					) VALUES
		(lv_TimeIdGMT, lv_TimeIdLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount	)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalHour_Id  FROM PPPTotalHour;
		
		-- Insert into PPPDetailHour
		INSERT INTO PPPDetailHour
		(PPPTotalHourId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalHour_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailHour_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailHour_Id FROM PPPDetailHour;
		
		
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- Oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueHour
					(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- Case 1.2 IF (lv_Purchase_CashPaidAmount != 0) THEN -- Oct 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 2.1 Credit Card Contactless only EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 11) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		
		-- Case 2.2 Credit Card Chip only EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 13) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		
		-- Case 2.3 CC External only
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 15) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	13,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.1.1 EMS-10462 Non Legacy Cash+ Credit Card Contactless 
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- end for Case 3.1.1 
		
		
		-- Case 3.1.2 EMS-10462 Non Legacy Cash+ Credit Card Chip
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		
		END IF;
		
		-- end for Case 3.1.2
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 
		
		-- Case 3.2.1 Legacy Cash + Credit card Contactless EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF; 		
		-- end Case 3.2.1
		
		-- Case 3.2.2 Legacy Cash + Credit Card Chip EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF;			
		-- end Case 3.2.2
		
		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the change done EMS 4303
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the change done EMS 4303
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueHour
				(PPPDetailHourId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailHour_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		
		-- INSERT FOR PPPTotalDay SECOND BLOCK START
		-- GET the starting Time ID from Time Table for that Day
		-- Rewrite this query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) ;
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and QuarterOfDay = 0;
		
		INSERT INTO PPPTotalDay
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdDayLocal,	lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPTotalDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalDay_Id FROM PPPTotalDay ;
		
		-- Insert into PPPDetailDay
		INSERT INTO PPPDetailDay
		(PPPTotalDayId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalDay_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		SELECT LAST_INSERT_ID() INTO lv_PPPDetailDay_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailDay_Id FROM PPPDetailDay;
		
		-- start insert into PPPRevenueDay
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueHour
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueDay
					(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 2.1 Credit Card Contactless ONLY EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 11) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
	  	-- end Case 2.1
		
		-- Case 2.2 Credit Card Chip Only EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 13) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;	  	
		-- end Case 2.2
		
		-- Case 2.3 CC External		
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 15) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	13,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;	 
		
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.1.1 Non Legacy Case + Credit Card Contactless EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		
		END IF;		
		-- end Case 3.1.1
		
		-- Case 3.1.2 Non Legacy Cash + Credit Card Chip EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		
		END IF;			
		-- end Case 3.1.2
		
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 
		
		-- Case 3.2.1 Legacy Cash + Credit Card Contactless EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF; 	
		-- end Case 3.2.1
		
		-- Case 3.2.2 Legacy Cash + Credit Card Chip EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF; 		
		-- end Case 3.2.2
		
		
		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				

				
				INSERT INTO PPPRevenueDay
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN

				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueDay
				(PPPDetailDayId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailDay_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		-- end insert into PPPDetailDay
		-- SECOND BLOCK END
		
		-- THIRD BLOCK START
		
		-- INSERT FOR PPPTotalMonth
		-- GET the starting Time ID from Time table for that Year and Month
		-- Rewrite this Query for better performance
		-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		--			and Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone));
		
		-- This Query Performance is Good
		 SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone))
		 AND Month = month(convert_tz(lv_Purchase_PurchaseGMT,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
		
		INSERT INTO PPPTotalMonth
		(TimeIdLocal, 		CustomerId, 			TotalAmount) VALUES
		(lv_TimeIdMonthLocal,lv_Purchase_CustomerId,	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPTotalMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPTotalMonth_Id FROM PPPTotalMonth;
		
		-- Insert into PPPDetailMonth
		INSERT INTO PPPDetailMonth
		(PPPTotalMonthId, 		PurchaseLocationId,		PermitLocationId,		PointOfSaleId,				UnifiedRateId,					PaystationSettingId,				TransactionTypeId,				IsCoupon,								TotalAmount) VALUES
		(lv_PPPTotalMonth_Id,	lv_Purchase_LocationId,	lv_Permit_LocationId,	lv_Purchase_PointOfSaleId,	lv_Purchase_UnifiedRateId,		lv_Purchase_PaystationSettingId,	lv_Purchase_TransactionTypeId,	if(length(lv_Purchase_CouponId)>0,1,0),	lv_Purchase_ChargedAmount)
		ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
		
		 SELECT LAST_INSERT_ID() INTO lv_PPPDetailMonth_Id; 
		-- SELECT MAX(ID) INTO lv_PPPDetailMonth_Id FROM PPPDetailMonth;
		
		-- start insert into PPPRevenueMonth
		
		-- Case 1.1 Refer Data Population logic for PPPInfoDetail word document. (Cash Transaction)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 1) THEN
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				-- Insert into PPPRevenueMonth
				IF (lv_Purchase_CoinPaidAmount > 0) THEN				
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;					
				END IF;
				
				IF (lv_Purchase_BillPaidAmount > 0) THEN
					INSERT INTO PPPRevenueMonth
					(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
					(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
					ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
								
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
							
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_Purchase_CashPaidAmount > 0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				
				
				
				INSERT INTO PPPRevenueMonth
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_ChargedAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_ChargedAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				
			END IF;
		
		END IF;
		
		-- Case 2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 2) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
	  	
		-- Case 2.1 Credit Card Contactless Only EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 11) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		-- end Case 2.1
		
		-- Case 2.2 Credit Card Chip Only EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 13) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		-- end Case 2.2
		
		-- Case 2.3 CC External
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 15) THEN
		
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	13,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
		END IF;
		
		
		-- Case 3.1 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount);
				
			END IF;
		
		
		END IF;
		
		-- Case 3.1.1 Non Legacy Cash + Credit Card Contactless EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		
		END IF;
		-- end Case 3.1.1
		
		-- Case 3.1.2 Non-Legacy Cash + Credit Card Chip EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN oct 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CoinPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
			
			
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		
		END IF;		
		-- end Case 3.1.2
		
		-- Case 3.2 Refer Data Population logic for PPPInfoDetail word document. (Credit Card + Cash Transaction)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 5) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	7,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
				-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount + lv_Purchase_CashPaidAmount);
				
			END IF;
		
		END IF; 

		-- Case 3.2.1 Legacy Cash + Credit Card Contactless
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 12) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	11,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF; 
		-- end Case 3.2.1
		
		-- Case 3.2.2. Legacy Cash + Credit Card Chip EMS-10462
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 14) THEN
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	12,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0 ) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
			END IF;
		
		END IF; 
		
		-- end Case 3.2.2
		
		-- Case 4 Refer Data Population logic for PPPInfoDetail word document. (Value Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 7) THEN
		
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 5 Refer Data Population logic for PPPInfoDetail word document. (Smart Card Only)
		
		IF (lv_PaymentCard_Id > 0) AND (lv_Purchase_PaymentTypeId = 4) THEN
		
				IF (lv_PaymentCard_Amount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				

				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

			
			-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,	CardTypeId,	CreditCardTypeId,  MerchantAccountId,   Amount)
			-- VALUES 				 (lv_ppp_Id,	1            ,	0,			0,  				NULL, 				lv_PaymentCard_Amount);
			
		END IF;
		
		-- Case 6.1 & 6.2 Refer Data Population logic for PPPInfoDetail word document. (Cash and Value Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 9) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 6.2 Non Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CoinPaidAmount >0 ) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;

				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 6.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	9,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				

				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 June 10
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
		
		
		-- Case 7.1 & 7.2 Refer Data Population logic for PPPInfoDetail word document. ( Cash and Smart Card)
		IF (lv_Permit_Id > 0) AND (lv_Purchase_PaymentTypeId = 6) THEN 
		
			-- IF (lv_Purchase_CashPaidAmount = 0) THEN -- (Case 7.2 Non Legacy Cash)
			IF (lv_Purchase_BillPaidAmount > 0 OR lv_Purchase_CoinPaidAmount > 0) THEN
			
				IF (lv_PaymentCard_Amount > 0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_BillPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	5,				lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CoinPaidAmount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	4,				lv_Purchase_CoinPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount, TotalCount = TotalCount + 1;
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
			
				
				-- INSERT PPPInfoDetail (PPPInfoId,	RevenueTypeId,CardTypeId,	CreditCardTypeId,   MerchantAccountId,  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0,				0,					NULL,				lv_PaymentCard_Amount+lv_Purchase_CoinPaidAmount+lv_Purchase_BillPaidAmount);
				
				
				
			END IF;
			
			-- IF (lv_Purchase_CashPaidAmount != 0) THEN -- (Case 7.1 Legacy Cash) OCT 2
			IF (lv_Purchase_BillPaidAmount = 0 AND lv_Purchase_CoinPaidAmount = 0) THEN
			
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	8,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;

				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	6,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				
				IF (lv_Purchase_CashPaidAmount >0) THEN
				-- JIRA 4303 replaced lv_Purchase_CashPaidAmount with lv_Purchase_ChargedAmount
				-- EMS 5102 reverted the changes done by 4303
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	2,				lv_Purchase_CashPaidAmount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_Purchase_CashPaidAmount, TotalCount = TotalCount + 1;
				END IF;
				
				IF (lv_PaymentCard_Amount >0) THEN
				INSERT INTO PPPRevenueMonth
				(PPPDetailMonthId,		RevenueTypeId,	TotalAmount) VALUES
				(lv_PPPDetailMonth_Id, 	3,				lv_PaymentCard_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + lv_PaymentCard_Amount, TotalCount = TotalCount + 1;
				END IF;
				
				
				-- INSERT PPPInfoDetail (PPPInfoId,RevenueTypeId,CardTypeId,CreditCardTypeId,MerchantAccountId,	  Amount)
				-- VALUES 				 (lv_ppp_Id   ,1            ,0         ,0               ,NULL                ,lv_Purchase_CashPaidAmount+lv_PaymentCard_Amount);
				
			END IF;
		
		END IF;
-- end insert into PPPRevenueMonth
		
		-- START FOR OCCUPANCY
		-- Added on Sep 30. ETL will not process if No. of Sapces for that Location is 0
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_Permit_LocationId;
		
		-- EMS 7.1 Cancelled and Test Transactions are excluded from Occupancy and Utilization
		IF (lv_Permit_Id > 0) and (lv_no_of_spaces_location > 0) and ( lv_Purchase_TransactionTypeId not in (5,6) ) THEN
		
		CALL sp_MigrateOccupancyIntoKPI_Inc( lv_Permit_Id ,
										 P_ETLExecutionLogId , 
										 1 , 
										 lv_PermitBeginGMT, 
										 lv_PermitExpireGMT ,
										 lv_Purchase_CustomerId, 
										 lv_Permit_LocationId, 
										 lv_Purchase_UnifiedRateId,
										 lv_Customer_Timezone,
										 lv_Permit_PermitTypeId);
		
		END IF;
		-- END FOR OCCUPANCY
		
		
		
		-- UPDATE ETLQueueRecordCount SET  ETLRemaingRecords = ETLRemaingRecords -1, LastModifiedGMT = NOW()
		-- WHERE ETLProcessDateRangeId = P_ETLExecutionLogId ;
		
		INSERT INTO ArchiveStagingPurchase(Id,RecordInsertTime) VALUES (lv_Purchase_Id, now());
		IF lv_Permit_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPermit(Id,RecordInsertTime) VALUES (lv_Permit_Id, now()); END IF;
		IF lv_PaymentCard_Id IS NOT NULL THEN INSERT INTO ArchiveStagingPaymentCard(Id,RecordInsertTime) VALUES (lv_PaymentCard_Id, now()); END IF;
		-- commented on Oct 28
		-- IF lv_ProcessorTransaction_Id IS NOT NULL THEN INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (lv_ProcessorTransaction_Id, now()); END IF;
	  
		DELETE FROM StagingPurchase WHERE Id = lv_Purchase_Id;
		DELETE FROM StagingPermit WHERE Id = lv_Permit_Id;
		DELETE FROM StagingPaymentCard WHERE Id = lv_PaymentCard_Id;
		-- commented on Oct 28
		-- DELETE FROM StagingProcessorTransaction WHERE Id = lv_ProcessorTransaction_Id;
		
		
		
		set lv_TimeIdGMT = NULL;
		set lv_TimeIdLocal = NULL;
		set lv_Purchase_LocationId = NULL;
		set lv_Permit_LocationId = NULL;
		set lv_Purchase_CustomerId = null;
		set lv_Permit_SpaceNumber = null;
		set lv_Purchase_PointOfSaleId = null;
		set lv_PayStation_Id = null;
		set lv_PaystationTypeId = null;
		set lv_Purchase_UnifiedRateId = null;
		set lv_Purchase_PaystationSettingId = null;
		set lv_Purchase_Id = null;
		set lv_Permit_Id = null;
		set lv_Permit_PermitIssueTypeId = null;
		set lv_Permit_PermitTypeId = null;
		set lv_Purchase_TransactionTypeId = null;
		set lv_Purchase_PaymentTypeId = null;
		set lv_Purchase_CoinCount = null;
		set lv_ProcessorTransaction_TypeId = null;
		set lv_Purchase_BillCount = null;
		set lv_Purchase_CouponId= null;
		set lv_ppp_IsFreePermit = null;
		set lv_Purchase_IsOffline = null;
		set lv_Purchase_IsRefundSlip = null;
		set lv_PermitExpireGMT = null;
		set lv_PermitBeginGMT = null ;
		set lv_PaymentCard_Amount = null;
		set lv_PaymentCard_CardTypeID = null;
		set lv_PaymentCard_CreditCardTypeID = null;
		set lv_PaymentCard_MerchantAccountID = null;
		set lv_PaymentCard_Id = null;
		set lv_Permit_OriginalPermitId = null;
		set lv_Purchase_ChargedAmount = null;
		set lv_ProcessorTransaction_Id = null;
		
		
		

		IF mod(indexm_pos,1000) = 0 THEN
			COMMIT ;
		END IF ;
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
               
 COMMIT ;		-- ASHOK2	   
-- set autocommit = 1 ; -- Auto Commit ON
 -- ASHOK3 SET AUTO COMMIT ON
    
    -- SELECT NOW() AS EndMigration;
END //
delimiter ;

-- *************************************************************************************************************************************************************


-- Useful SQL CALL sp_FillOccupancyTimeslots('2012-06-01', 160, 2238, 159,0,0,'US/Central');


DROP PROCEDURE IF EXISTS sp_FillOccupancyTimeslots ;

delimiter //

CREATE PROCEDURE sp_FillOccupancyTimeslots (IN P_Date DATE, 
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED, 
											IN P_Interval TINYINT UNSIGNED, 
											IN P_NoOfSpaces MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25),
											IN P_EndDate DATE)
BEGIN

DECLARE lv_Time_Diff MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_TimeIdGMT , lv_TimeIdLocal MEDIUMINT UNSIGNED DEFAULT 0;

 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      LocationId,		CustomerId,				ETLObject, 	record_insert_time, Occ_Date,	UnifiedRateId,			NoOfSpaces, 	Customer_Timezone )  values(
      P_LocationId,		P_CustomerId,	        'TimeSlot', 	now(), P_Date,	P_UnifiedRateId,		P_NoOfSpaces,	P_Customer_Timezone );
		COMMIT ; 
  END;

  
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.

SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = P_Date AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(P_Date,'GMT',P_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN

SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;

END IF;



/*INSERT INTO OccupancyHour 
(TimeIdGMT,TimeIdLocal, CustomerId, LocationId, UnifiedRateId, NumberOfSpaces, NoOfPermits) 
SELECT T.Id, T.Id - lv_Time_Diff, P_CustomerId, P_LocationId,P_UnifiedRateId,P_NoOfSpaces,0
FROM 
Time T, LocationOpen LO
WHERE T.Date = P_Date AND DayOfWeek = DAYOFWEEK(P_Date) AND 
LO.QuarterHourId   = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(P_Date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId;
*/

-- JIRA 4624
-- Modifed on Nov 11
INSERT INTO OccupancyHour 
(TimeIdGMT,TimeIdLocal, CustomerId, LocationId, UnifiedRateId, NumberOfSpaces, NoOfPermits) 
SELECT T.Id, T.Id - lv_Time_Diff, P_CustomerId, P_LocationId,P_UnifiedRateId,P_NoOfSpaces,0
FROM 
Time T, LocationOpen LO
WHERE T.Date between P_Date AND P_EndDate 
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = T.QuarterOfDay 
AND LO.LocationId = P_LocationId;


-- removed -1 in LO.QuarterHourId -1 

END//

delimiter ;

-- 
/*
-- Procedure sp_MigrateOccupancyIntoKPI_Inc

-- Useful SQL: CALL sp_MigrateOccupancyIntoKPI_Inc(1,1,'2013-03-03 10:00:00', '2013-03-03 10:30:00', 160, 2263, 222, 'US/Central');

*/
-- 
DROP PROCEDURE IF EXISTS sp_MigrateOccupancyIntoKPI_Inc;

delimiter //

CREATE PROCEDURE sp_MigrateOccupancyIntoKPI_Inc(IN P_PermitId INT UNSIGNED ,
											IN P_ETLExecutionLogId INT UNSIGNED, 
											IN P_ETLProcessTypeId INT UNSIGNED, 
											IN P_BeginPermitGMT datetime, 
											IN P_EndPermitGMT datetime,
											IN P_CustomerId MEDIUMINT UNSIGNED, 
											IN P_LocationId MEDIUMINT UNSIGNED, 
											IN P_UnifiedRateId MEDIUMINT UNSIGNED,
											IN P_Customer_Timezone char(25),
											IN P_Permit_PermitTypeId TINYINT UNSIGNED)
BEGIN

-- DECLARE SECTION
DECLARE lv_TimeIdBeginGMT, lv_TimeIdLocal,lv_TimeIdEndGMT MEDIUMINT UNSIGNED;
DECLARE lv_cnt_records,lv_cnt_End_record INT UNSIGNED DEFAULT 0;
DECLARE lv_GMT_date, lv_local_date DATE DEFAULT NULL;
DECLARE lv_no_of_spaces_location MEDIUMINT UNSIGNED DEFAULT 0;
DECLARE lv_permit_duration MEDIUMINT UNSIGNED DEFAULT NULL;
declare lv_TimeIdDayLocal,lv_TimeIdEndLocal mediumint unsigned DEFAULT NULL;
declare lv_TimeIdMonthLocal,lv_TimeId_Starting_QuarterHourId, lv_TimeId_End_QuarterHourId mediumint unsigned DEFAULT NULL;
declare lv_OccId, lv_StartingTimeID , lv_EndingTimeID BIGINT UNSIGNED DEFAULT 0;
DECLARE lv_OccupancyDay_Id, lv_OccupancyMonth_Id BIGINT UNSIGNED  DEFAULT NULL;

DECLARE lv_operation_MINUTES MEDIUMINT UNSIGNED DEFAULT NULL;
DECLARE lv_OpenQuarterHourNumber tinyint unsigned DEFAULT NULL; 
DECLARE lv_CloseQuarterHourNumber tinyint unsigned DEFAULT NULL;
DECLARE lv_IsOpen24Hours tinyint unsigned DEFAULT NULL;
DECLARE lv_location_open_datetime datetime DEFAULT NULL;
DECLARE lv_location_close_datetime datetime DEFAULT NULL;
DECLARE lv_UtilizationDay_Id BIGINT UNSIGNED DEFAULT NULL;
DECLARE lv_minutes_purchased,lv_minutes_purchased_currentday INT UNSIGNED DEFAULT NULL;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_next_day date;
-- feb 14 2014
declare lv_PermitBeginLocalTime, lv_PermitEndLocalTime,lv_PermitBeginLocalDateTime datetime;
declare lv_PermitEndingTimeIdLocal mediumint unsigned DEFAULT NULL ;
declare lv_sum_Permits INT UNSIGNED DEFAULT 0;
-- Cursor Declaration Section

 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      insert into ETLNotProcessed(
      PermitId,		CustomerId,				ETLObject, 	record_insert_time,	ETLProcessDateRangeId,		ETLProcessTypeId,	 BeginPermitGMT,	EndPermitGMT,	LocationId,		UnifiedRateId,	 Customer_Timezone )  values(
      P_PermitId,	P_CustomerId,	        'Permit', 	now(),	NUll,	P_ETLProcessTypeId, P_BeginPermitGMT,	P_EndPermitGMT,	P_LocationId,	P_UnifiedRateId, P_Customer_Timezone);
		COMMIT ; 
  END;


	

-- UPDATE ETLProcessDateRange
-- SET CountOfRecords = idmaster_pos,
-- CursorOpenedTime = UTC_TIMESTAMP()
-- WHERE Id = P_ETLProcessDateRangeId;


-- Get TimeIDGMT of PermitBegin
-- This Query Performance is Good
SELECT id,Date into lv_TimeIdBeginGMT, lv_GMT_date FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= P_BeginPermitGMT);

-- Get the Local Begin TimeID of P_BeginPermitGMT 
-- This Query Performance is Good
SELECT Id, Date INTO lv_TimeIdLocal, lv_local_date FROM Time where DateTime = ( select max(datetime) from Time where datetime <= convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;

-- Get the TimeIDGMT of P_EndPermitGMT 
-- This Query Performance is Good
SELECT Id INTO lv_TimeIdEndGMT FROM Time WHERE datetime = ( select min(datetime) from Time where datetime >= P_EndPermitGMT);

/*SELECT min(T.Id) INTO lv_TimeId_Starting_QuarterHourId
FROM 
Time T, LocationOpen LO
WHERE T.Date = lv_GMT_date AND DayOfWeek = DAYOFWEEK(lv_GMT_date) AND 
LO.QuarterHourId-1  = T.QuarterOfDay AND LO.DayOfWeekId = DAYOFWEEK(lv_GMT_date)
AND LO.DayOfWeekId = T.DayOfWeek AND LO.LocationId = P_LocationId ;
*/
-- NOTE: adding -1 in the below SQL need to be removed once LocationOpen.QuarterHourId is fixed in Migration

-- This Query Performance is Fair
-- Added min(QuarterHourId) condition on Aug 22
SELECT id INTO lv_TimeId_Starting_QuarterHourId FROM Time WHERE date = lv_GMT_date and QuarterOfDay = (select min(QuarterHourId) from 
LocationOpen where LocationId = P_LocationId and DayofWeekId = DAYOFWEEK(lv_GMT_date) and IsOpen =1 );

-- Added on Dec 20
-- Check EndTimeId
SELECT id INTO lv_TimeId_End_QuarterHourId FROM Time WHERE date = date(P_EndPermitGMT) and QuarterOfDay = (select max(QuarterHourId) from 
LocationOpen where LocationId = P_LocationId and DayofWeekId = DAYOFWEEK(lv_GMT_date) and IsOpen =1 );


-- This Query Performance is Good
SELECT COUNT(1) INTO lv_cnt_records FROM OccupancyHour
WHERE TimeIdGMT = lv_TimeId_Starting_QuarterHourId AND CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;

-- added on Dec 20
SELECT COUNT(1) INTO lv_cnt_End_record FROM OccupancyHour
WHERE TimeIdGMT = lv_TimeId_End_QuarterHourId AND CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;


-- Get no. of spaces for Location for that day of week
-- This Query Performance is Good
-- This is used only for Testing.. Initial Migration
SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = P_LocationId;

-- Get Hours of Operations from LocationDay -- Code written on June 18

SELECT OpenQuarterHourNumber, CloseQuarterHourNumber, IsOpen24Hours 
INTO lv_OpenQuarterHourNumber, lv_CloseQuarterHourNumber, lv_IsOpen24Hours
FROM LocationDay where LocationId = P_LocationId and dayofweekid = dayofweek(P_BeginPermitGMT) ;

IF lv_IsOpen24Hours = 1 THEN
	 SET lv_operation_MINUTES = 1440 ;
	-- USed only for Testing
	-- SET lv_operation_MINUTES = 600 ;
ELSE
	-- SELECT dateTime INTO lv_location_open_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_OpenQuarterHourNumber ;
	-- SELECT timestampadd(minute, 15,dateTime) INTO lv_location_close_datetime from Time where Date = date(P_BeginPermitGMT) and QuarterOfDay = lv_CloseQuarterHourNumber ;
	-- SELECT TIMESTAMPDIFF(minute,lv_location_open_datetime,lv_location_close_datetime) INTO lv_operation_MINUTES ;
	
	SELECT COUNT(*)*15 into lv_operation_MINUTES from LocationOpen where LocationId = P_LocationId and DayofweekId = dayofweek(P_BeginPermitGMT) and IsOpen=1;
	
	-- USed only for Testing
	-- SET lv_operation_MINUTES = 600 ;
END IF ;
-- Added on Dec 20
IF (lv_cnt_records = 0) and (lv_cnt_End_record=0) THEN -- No Record in the begining
	-- JIRA 4624
	CALL sp_FillOccupancyTimeslots(lv_GMT_date, P_CustomerId, P_LocationId, P_UnifiedRateId, 0, lv_no_of_spaces_location, P_Customer_Timezone,date(P_EndPermitGMT) );

ELSEIF (lv_cnt_records > 0) and (lv_cnt_End_record=0) THEN
	
	-- Get maximumn TimeIdGMT from OccuapncyHour
	SELECT Date into lv_next_day from Time where Id = ( select max(TimeIdGMT)+1 from OccupancyHour where CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ) ;
	
	CALL sp_FillOccupancyTimeslots(lv_next_day, P_CustomerId, P_LocationId, P_UnifiedRateId, 0, lv_no_of_spaces_location, P_Customer_Timezone,date(P_EndPermitGMT) );
END IF;	


	-- Get time difference in minutes between P_EndPermitGMT and P_BeginPermitGMT
	-- select lv_TimeIdBeginGMT, lv_TimeIdEndGMT ;
	
	IF (lv_TimeIdEndGMT >= lv_TimeIdBeginGMT) THEN
	
	-- Get Duration of Permit (in no of buckets)
	select lv_TimeIdEndGMT - lv_TimeIdBeginGMT  into lv_permit_duration ;
	
	-- select lv_TimeIdBeginGMT, lv_permit_duration,lv_TimeIdEndGMT;
	
	-- This Query Performance is Good
	SELECT Id into lv_StartingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	-- This Query Performance is Good
	SELECT Id into lv_EndingTimeID from OccupancyHour where TimeIdGMT = lv_TimeIdBeginGMT+lv_permit_duration-1 AND
	CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	
	
	-- UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	-- WHERE TimeIdGMT between lv_TimeIdBeginGMT and (lv_TimeIdBeginGMT+lv_permit_duration-1) AND
	-- CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId = P_UnifiedRateId ;
	
	-- This Query Performance is Fair
	 UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
	 WHERE Id between lv_StartingTimeID AND  lv_EndingTimeID and CustomerId = P_CustomerId AND LocationId = P_LocationId AND UnifiedRateId= P_UnifiedRateId ;
	 
	-- Added on Feb 17
	
	select ROW_COUNT() into lv_sum_Permits;
	
	-- Used a Loop to update the table with Key as PK
	/*SET lv_OccId = lv_StartingTimeID;
	WHILE lv_OccId <= lv_EndingTimeID DO
	
		
		UPDATE OccupancyHour SET NoOfPermits = NoOfPermits + 1
		WHERE Id = lv_OccId ;
		
	SET lv_OccId = lv_OccId + 1 ;
	END WHILE ; */
	
	-- This Query Performance is Good
	UPDATE OccupancyHour SET NoOfPurchases = NoOfPurchases + 1
	WHERE Id = lv_StartingTimeID ; 
	
	-- June 10
	-- Write Code here to fill Turnover Table
	/*INSERT INTO OccupancyHourTurnover
	(OccupancyHourId, 		DurationMins ) VALUES
	(lv_StartingTimeID , 	TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */

	
	-- Write Code here to Fill Occupancy Day and Occupancy Monthly Tables
	
	-- Rewrite the below query for betterPerformance
	-- SELECT MIN(Id) INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) ;	
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and QuarterOfDay = 0;
	
	-- Added on Feb 14 2014
	SELECT Id INTO lv_TimeIdEndLocal FROM Time where Date = date(convert_tz(P_EndPermitGMT,'GMT',P_Customer_Timezone)) and QuarterOfDay = 0;
	
	-- Added on Nov 12, Nov 14
	
	-- Commenting this code on Nov 15
	/* INSERT INTO OccupancyDay
	(TimeIdLocal,		CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces, NoOfPermits) VALUES
	(lv_TimeIdDayLocal, P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location*lv_operation_MINUTES/15,lv_permit_duration )
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), NoOfPermits = NoOfPermits + lv_permit_duration; 
	*/
	
	-- P_Permit_PermitTypeId
	-- lv_no_of_Purchases;
	SELECT TIMESTAMPDIFF(minute,P_BeginPermitGMT,P_EndPermitGMT) INTO lv_minutes_purchased ;
	
	-- added on Feb 14 2014
	
	select TimeIdLocal into lv_PermitEndingTimeIdLocal From OccupancyHour where Id = lv_EndingTimeID;
	select date(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) into lv_PermitBeginLocalTime ;
	select convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone) into lv_PermitBeginLocalDateTime ;
	
	select date(convert_tz(P_EndPermitGMT,'GMT',P_Customer_Timezone)) into lv_PermitEndLocalTime ;
	if date(lv_PermitBeginLocalTime) = date(lv_PermitEndLocalTime) then
		set lv_minutes_purchased_currentday = lv_minutes_purchased ;
	else
		select timestampdiff(minute,lv_PermitBeginLocalDateTime,(date_add(date(lv_PermitBeginLocalDateTime),interval 1 day))) into lv_minutes_purchased_currentday;
	end if;
	
	
	SELECT Id INTO lv_UtilizationDay_Id FROM UtilizationDay
	WHERE TimeIdLocal = lv_TimeIdDayLocal AND 
	CustomerId = P_CustomerId AND
	LocationId = P_LocationId ;
	
	IF lv_UtilizationDay_Id IS NOT NULL THEN
	
		IF (P_Permit_PermitTypeId = 1) THEN  -- Regular Permit
			/*UPDATE UtilizationDay SET 
			NumberOfPurchases = NumberOfPurchases + 1, 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
			*/
			call sp_InsertUtilizationDay(P_CustomerId, P_LocationId, lv_TimeIdDayLocal, lv_PermitEndingTimeIdLocal, lv_minutes_purchased,lv_minutes_purchased_currentday,1);
		ELSE								-- Add Time
			/*UPDATE UtilizationDay SET 
			TotalMinutesPurchased = TotalMinutesPurchased + lv_minutes_purchased 
			WHERE
			Id = lv_UtilizationDay_Id ;
			*/
			-- added on Feb 14 2014
			-- EMS 7.1 IREQ-1 (add time and EBP are both new permits with their own duration) Hence at input param - IsPurchase to sp_InsertUtilizationDay is also set to 1
			-- not removing the If/ELSE condition. May be needed in future use
			call sp_InsertUtilizationDay(P_CustomerId, P_LocationId, lv_TimeIdDayLocal, lv_PermitEndingTimeIdLocal, lv_minutes_purchased,lv_minutes_purchased_currentday,1);
			
		END IF;
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			-- EMS 7.1
			-- CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
	
	ELSE
				
			-- added on Feb 14 2014
			call sp_InsertUtilizationDay(P_CustomerId, P_LocationId, lv_TimeIdDayLocal, lv_PermitEndingTimeIdLocal, lv_minutes_purchased,lv_minutes_purchased_currentday,1);
			/*INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 							NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_TimeIdDayLocal,	P_CustomerId,	P_LocationId, 	lv_no_of_spaces_location,	lv_no_of_spaces_location*lv_operation_MINUTES,	1,				  lv_minutes_purchased );
			*/
			-- To append delayed records ..
			-- Get the StartingMonthIdLocal for this Customer and check in UtilizationMonth if record exists for this Location.
			-- IF YES Update to UtilizationMonth
			-- If No Do nothing
			-- EMS 7.1
			-- CALL sp_UpdateUtilMonthly(P_CustomerId ,P_LocationId, 1, lv_minutes_purchased,P_BeginPermitGMT, P_Customer_Timezone ); 
			
	END IF ;
	
	
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyDay_Id; 
	
	/*INSERT INTO OccupancyDayTurnover
	(OccupancyDayId ,DurationMins ) VALUES
	(lv_OccupancyDay_Id, TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT) )
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
	
	-- Rewrite the query for better performance
	-- SELECT MIN(Id) INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	-- and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone));
	
	-- This Query Performance is Good
	SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
	and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
	
	-- JIRA 4650 Dec 09
	-- Get Year and month from lv_TimeIdBeginingMonthLocal
	
	
	select year, month into lv_year,lv_month FROM Time where Id = lv_TimeIdMonthLocal ;
		
	-- Get Total open Days in the month
	select count(*) into lv_total_days_open_in_month
	from Time  T, LocationDay LD
	where T.Year = lv_year and T.Month = lv_month and
	T.DayOfWeek = LD.DayOfWeekId  and
	LD.LocationId = P_LocationId and LD.IsClosed = 0 and T.QuarterOfDay = 0;
	
	-- Code added in Rollup procedure
	/*INSERT INTO OccupancyMonth
	(TimeIdLocal,			CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces) VALUES
	(lv_TimeIdMonthLocal, 	P_CustomerId,	P_LocationId,	P_UnifiedRateId,	lv_no_of_spaces_location*lv_total_days_open_in_month)
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id) , NoOfPermits = NoOfPermits + lv_sum_Permits; 
	*/
	
	-- This will elimitate calling MigrateHurlytoDaily
	-- Added on Feb 17 
	-- INSERT OccupancyDay (TimeIdLocal, 				 CustomerId,		LocationId, 	UnifiedRateId, 		NumberOfSpaces,		NoOfPermits) VALUES
	-- 					(lv_TimeIdDayLocal,			 P_CustomerId,		P_LocationId,	P_UnifiedRateId,	0,					lv_sum_Permits)	
	-- ON DUPLICATE KEY UPDATE NoOfPermits = NoOfPermits + lv_sum_Permits ;
	
	-- SELECT LAST_INSERT_ID() INTO lv_OccupancyMonth_Id; 
	
	/*INSERT INTO OccupancyMonthTurnover
	(OccupancyMonthId , DurationMins ) VALUES
	(lv_OccupancyMonth_Id , TIMESTAMPDIFF(MINUTE, P_BeginPermitGMT, P_EndPermitGMT))
	ON DUPLICATE KEY UPDATE  TotalCount = TotalCount + 1; */
		
	-- Code end here 
	
	
	
	ELSE
	
		insert into ETLNotProcessed(
		PermitId,		CustomerId,			ETLObject, 	record_insert_time )  values(
		P_PermitId,	P_CustomerId,	        'Permit1', 	now() );
		
		
	END IF;

END//

delimiter ;

-- **********************************************************************************************************************************************

/*
 --------------------------------------------------------------------------------
  Procedure:  sp_MigrateSettledTransactionIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/

DROP PROCEDURE IF EXISTS sp_MigrateSettledTransactionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateSettledTransactionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- June 11 2013 

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;
DECLARE lv_IsRefund TINYINT ; 

declare lv_CardTransactionTotalHour_Id BIGINT UNSIGNED;
declare lv_CardTransactionDetailHour_Id BIGINT UNSIGNED;
declare lv_TimeIdDayLocal MEDIUMINT;

declare lv_CardTransactionTotalDay_Id BIGINT UNSIGNED;
declare lv_CardTransactionDetailDay_Id BIGINT UNSIGNED;
declare lv_TimeIdMonthLocal MEDIUMINT;

declare lv_CardTransactionTotalMonth_Id BIGINT UNSIGNED;
declare lv_CardTransactionDetailMonth_Id BIGINT UNSIGNED;

-- CURSOR VARIABLES
DECLARE v_ProcessorTxn_Id BIGINT ;
DECLARE v_ProcessorTxn_PointOfSaleId MEDIUMINT ; 
DECLARE v_ProcessorTxn_CustomerID MEDIUMINT ;
DECLARE v_ProcessorTxn_PurchaseId BIGINT ; 
DECLARE v_ProcessorTxn_ProcessorTransactionTypeId  TINYINT ;
DECLARE v_ProcessorTxn_MerchantAccountId MEDIUMINT ; 
DECLARE v_ProcessorTxn_Amount MEDIUMINT ;
DECLARE v_ProcessorTxn_ProcessingDate DATETIME ;
DECLARE v_ProcessorTxn_PropertyValue VARCHAR(40);		
declare lv_ETLLimit INT UNSIGNED;
DECLARE v_ProcessorTxn_CardProcessTypeId TINYINT UNSIGNED;
DECLARE v_ProcessorTxn_LocationId MEDIUMINT UNSIGNED;

DECLARE C1 CURSOR FOR
	
		SELECT
			T.Id ,
			T.PointOfSaleId , 
			B.CustomerID,
			0,	
			T.ProcessorTransactionTypeId , 
			T.MerchantAccountId , 
			T.Amount ,
			T.ProcessingDate ,
			P.PropertyValue ,
			D.CardProcessMethodTypeId ,
			B.LocationId
		FROM 
			StagingProcessorTransaction T, 
			PointOfSale B, 
			CustomerProperty P,
			CardProcessTransactionType D
			
		WHERE
			T.PointOfSaleId = B.Id AND 
			B.CustomerId = P.CustomerId AND
			P.CustomerPropertyTypeId = 1 AND 
			T.IsApproved = 1 AND
			T.ProcessorTransactionTypeId = D.ProcessorTransactionTypeId AND			
			P.PropertyValue IS NOT NULL LIMIT lv_ETLLimit ;
			
			-- T.ProcessingDate between '2014-01-01' and '2014-01-10' AND
			
			-- Check Performance here
			-- EMS 4742 March 12 added 	ifnull(T.PurchaseId,1)
 -- Commented the below statement on Sep 23 2014
 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO SettledTxnNotProcessed ( ProcessorTransactionId , PointOfSaleId , 				ProcessorTransactionTypeId , 				ProcessingDate , 				RecordInsertTime) VALUES
										 ( v_ProcessorTxn_Id,		v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_ProcessingDate, 	now()	 );
	  INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
	  DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;

  END;
 

 -- SET lv_Customer_Timezone = null;
 
 SELECT Value INTO lv_ETLLimit FROM EmsProperties WHERE Name = 'ETLLimit';
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET SettledAmountRecordCount = idmaster_pos,
SettledAmountCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_ProcessorTxn_Id ,
					v_ProcessorTxn_PointOfSaleId , 
					v_ProcessorTxn_CustomerID,
					v_ProcessorTxn_PurchaseId,
					v_ProcessorTxn_ProcessorTransactionTypeId , 
					v_ProcessorTxn_MerchantAccountId , 
					v_ProcessorTxn_Amount ,
					v_ProcessorTxn_ProcessingDate ,
					v_ProcessorTxn_PropertyValue ,
					v_ProcessorTxn_CardProcessTypeId,
					v_ProcessorTxn_LocationId;	
					
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				SET lv_TimeIdLocal = NULL ;
				SET lv_IsRefund = NULL ;
				
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) ;
				
				IF v_ProcessorTxn_ProcessorTransactionTypeId in (2,3, 6, 7, 10, 11, 17, 21,23) THEN  -- Added TypeId  21 on May 21, TypeId 23 on June 03 EMS-5628
					SET lv_IsRefund = 0 ;
				ELSEIF v_ProcessorTxn_ProcessorTransactionTypeId in ( 4, 12, 13, 18, 19,22) THEN  -- Added TypeId  22 on June 03 EMS-5628
					SET lv_IsRefund = 1 ;
				ELSE
					SET lv_IsRefund = 2 ;
				END IF;
				
				
				INSERT INTO SettledTransaction (ProcessorTransactionId, CustomerId, 				PurchaseId,				  TimeIdLocal, 	ProcessorTransactionTypeId , 				MerchantAccountId, 					Amount, 				IsRefund)
										VALUES (v_ProcessorTxn_Id,		v_ProcessorTxn_CustomerID,	v_ProcessorTxn_PurchaseId, lv_TimeIdLocal,	v_ProcessorTxn_ProcessorTransactionTypeId,	v_ProcessorTxn_MerchantAccountId,	v_ProcessorTxn_Amount,	lv_IsRefund) ;
	
				-- New Code for Card Processing Widget
				-- EMS 6178 Added on Nov 27 2014
				IF (lv_IsRefund = 0) THEN
				-- Hourly 
				SELECT id into lv_TimeIdGMT FROM Time WHERE datetime = ( select max(datetime) from Time where datetime <= v_ProcessorTxn_ProcessingDate);
				
				INSERT INTO CardTransactionTotalHour
				(TimeIdGMT,    TimeIdLocal, 	CustomerId, 				CardProcessMethodTypeId,			TotalAmount					) VALUES
				(lv_TimeIdGMT, lv_TimeIdLocal,	v_ProcessorTxn_CustomerID,	v_ProcessorTxn_CardProcessTypeId, 	v_ProcessorTxn_Amount	)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionTotalHour_Id; 
				
		
				INSERT INTO CardTransactionDetailHour
				(CardTransactionTotalHourId, 		PurchaseLocationId,		PointOfSaleId,	TotalAmount) VALUES
				(lv_CardTransactionTotalHour_Id,	v_ProcessorTxn_LocationId,		v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionDetailHour_Id; 
		
		
				/*INSERT INTO CardTransactionRevenueHour
				(CardTransactionDetailHourId,		CardProcessMethodTypeId,	TotalAmount) VALUES
				(lv_CardTransactionDetailHour_Id, 	v_ProcessorTxn_CardProcessTypeId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;*/
					
					
				-- Daily
				
				SELECT Id INTO lv_TimeIdDayLocal FROM Time where Date = date(convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) and QuarterOfDay = 0;
		
				INSERT INTO `CardTransactionTotalDay`
				(TimeIdLocal, 		CustomerId, 				CardProcessMethodTypeId,			TotalAmount) VALUES
				(lv_TimeIdDayLocal,	v_ProcessorTxn_CustomerID,	v_ProcessorTxn_CardProcessTypeId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionTotalDay_Id; 
				
						
				INSERT INTO `CardTransactionDetailDay` 
				(`CardTransactionTotalDayId` , 		PurchaseLocationId,		    PointOfSaleId,					TotalAmount) VALUES
				(lv_CardTransactionTotalDay_Id,	   v_ProcessorTxn_LocationId,	v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionDetailDay_Id; 
				
				/*INSERT INTO `CardTransactionRevenueDay` 
				(`CardTransactionDetailDayId` ,		`CardProcessMethodTypeId` ,				TotalAmount) VALUES
				(lv_CardTransactionDetailDay_Id, 	v_ProcessorTxn_CardProcessTypeId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;*/
				
				-- Monthly
				
				-- This Query Performance is Good
				SELECT Id INTO lv_TimeIdMonthLocal FROM Time where Year = year(convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue))
				AND Month = month(convert_tz(v_ProcessorTxn_ProcessingDate,'GMT',v_ProcessorTxn_PropertyValue)) and dayofmonth=1 and Quarterofday = 0;
	
		
				INSERT INTO `CardTransactionTotalMonth` 
				(TimeIdLocal, 			CustomerId, 				CardProcessMethodTypeId,			TotalAmount) VALUES
				(lv_TimeIdMonthLocal,	v_ProcessorTxn_CustomerID,	v_ProcessorTxn_CardProcessTypeId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionTotalMonth_Id; 
				
						
				INSERT INTO `CardTransactionDetailMonth` 
				(`CardTransactionTotalMonthId` , 		PurchaseLocationId,		PointOfSaleId,					TotalAmount) VALUES
				(lv_CardTransactionTotalMonth_Id,	v_ProcessorTxn_LocationId,	v_ProcessorTxn_PointOfSaleId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;
		
				SELECT LAST_INSERT_ID() INTO lv_CardTransactionDetailMonth_Id; 

				END IF;
				
				/*INSERT INTO `CardTransactionRevenueMonth` 
				(`CardTransactionDetailMonthId` ,		`CardProcessMethodTypeId` ,				TotalAmount) VALUES
				(lv_CardTransactionDetailMonth_Id, 		v_ProcessorTxn_CardProcessTypeId,	v_ProcessorTxn_Amount)
				ON DUPLICATE KEY UPDATE TotalAmount = TotalAmount + v_ProcessorTxn_Amount, TotalCount = TotalCount + 1;	*/
		
				INSERT INTO ArchiveStagingProcessorTransaction(Id,RecordInsertTime) VALUES (v_ProcessorTxn_Id, now());
				DELETE FROM StagingProcessorTransaction WHERE ID = v_ProcessorTxn_Id ;
				
				
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;
-- ***********************************************************************


/*
 --------------------------------------------------------------------------------
  Procedure:  sp_MigrateCollectionIntoKPI_Inc
  
  Purpose:    
             
  Critical:   
              
  Useful SQL: 
  -------------------------------------------------------------------------------- 

*/

DROP PROCEDURE IF EXISTS sp_MigrateCollectionIntoKPI_Inc ;

delimiter //


CREATE PROCEDURE sp_MigrateCollectionIntoKPI_Inc(IN P_ETLExecutionLogId BIGINT UNSIGNED)

BEGIN

-- Uploaded to Subversion
-- Last modified on June 11 2013
-- DECLARE SECTION
-- FOR ACTUAL MIGRATION THIS PROCEDURE TAKEN ONLY ENDGMT DATA AS INPUT PARAMETER

declare lv_TimeIdGMT MEDIUMINT;
declare lv_TimeIdLocal MEDIUMINT;
declare lv_Customer_Timezone char(25); -- it is char(64) 

DECLARE CursorDone INT DEFAULT 0;

declare idmaster_pos,indexm_pos int default 0;



-- CURSOR VARIABLES
DECLARE v_Collection_Id BIGINT;
DECLARE v_Collection_CustomerId MEDIUMINT;
DECLARE	v_Collection_PointOfSaleId MEDIUMINT;
DECLARE	v_Collection_CollectionTypeId TINYINT;
DECLARE	v_Collection_EndGMT DATETIME;
DECLARE	v_Collection_CoinTotalAmount MEDIUMINT;
DECLARE	v_Collection_BillTotalAmount MEDIUMINT;
DECLARE	v_Collection_CardTotalAmount MEDIUMINT;
DECLARE	v_CollectionTotal MEDIUMINT;
DECLARE v_Customer_Timezone VARCHAR(40);	
declare lv_ETLLimit INT UNSIGNED;

-- CURSOR C1 STATEMENT
-- RENAME C1 WITH c_Collection
-- There will be only INSERT operation on POSCollection

DECLARE C1 CURSOR FOR
	
		SELECT
		A.Id,
		A.CustomerId,
		A.PointOfSaleId,
		A.CollectionTypeId,
		A.EndGMT,
		A.CoinTotalAmount,
		A.BillTotalAmount,
		A.CardTotalAmount,
		A.CoinTotalAmount + A.BillTotalAmount +	A.CardTotalAmount,
		B.PropertyValue
		FROM 
		StagingPOSCollection A, CustomerProperty B
		WHERE 
		A.CustomerId = B.CustomerId AND
		B.CustomerPropertyTypeId = 1 AND
		PropertyValue IS NOT NULL  LIMIT lv_ETLLimit;
		
		-- AND	A.CreatedGMT between P_BeginDateGMT AND P_EndDateGMT
		-- A.EndGMT between P_BeginDateGMT AND P_EndDateGMT ; Added on April 19 2013
		-- A.EndGMT must be replace by A.CreateGMT the new attribute to be added
		-- WHERE 
		-- CustomerId = P_CustomerId AND
		-- EndGMT between P_BeginCollectionGMT AND P_EndCollectionGMT;		
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		-- BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50,
		-- AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount,
		-- (A.CoinCount05*5 + A.CoinCount10*10 + A.CoinCount25*25 +  A.CoinCount100*100 + A.CoinCount200*200)/100
		--	+ BillAmount1 + BillAmount2 + BillAmount5 + BillAmount10 + BillAmount20 + BillAmount50 + 
		--	AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + ValueCardAmount + SmartCardRechargeAmount ,
		
		
		
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET CursorDone = 1;
 
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  BEGIN
      -- NEW TABLE HERE TO LOG EXCEPTION DATA
	  INSERT INTO CollectionNotProcessed ( PosCollectionId , CustomerId , 				PointOfSaleId , 			CollectionTypeId , 				EndGMT , 				RecordInsertTime  ) VALUES
										 ( v_Collection_Id , v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId , v_Collection_EndGMT,	now() );
	
	INSERT INTO ArchiveStagingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
	DELETE FROM StagingPOSCollection WHERE Id = v_Collection_Id;
	
  END;
 

 SET lv_Customer_Timezone = null;
 SELECT Value INTO lv_ETLLimit FROM EmsProperties WHERE Name = 'ETLLimit';
 
 OPEN C1;
 SET idmaster_pos = (SELECT FOUND_ROWS());
 
UPDATE ETLExecutionLog
SET CollectionRecordCount = idmaster_pos,
CollectionCursorOpenedTime = NOW()
WHERE Id = P_ETLExecutionLogId;
 
-- select idmaster_pos;
WHILE indexm_pos < idmaster_pos do
    
	FETCH C1 INTO  	
					v_Collection_Id,
					v_Collection_CustomerId,
					v_Collection_PointOfSaleId,
					v_Collection_CollectionTypeId,
					v_Collection_EndGMT,
					v_Collection_CoinTotalAmount,
					v_Collection_BillTotalAmount,
					v_Collection_CardTotalAmount,
					v_CollectionTotal,
					v_Customer_Timezone;	
					
				-- Step 1: Get the EndTimeIdGMT and EndTimeIdLocal from Time table for v_Collection_EndGMT
				-- Step 2: Check if record exist in TotalCollection for that (TimeID,CustomerId,PointOfSaleId,CollectionTypeId)
				-- Step 2.1: If Record DOES NOT EXISTS then INSERT the record in TotalCollection
				-- Step 2.2: If Record EXISTS then UPDATE the columns by summing up each attributes CoinTotalAmount,BillTotalAmount,CardTotalAmount,TotalAmount 
				--			 in TotalCollection
				
				-- Refresh Variables here....
				
				SELECT Id INTO lv_TimeIdGMT FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= v_Collection_EndGMT);
						
				SELECT Id INTO lv_TimeIdLocal FROM Time WHERE DateTime = ( SELECT MAX(datetime) FROM Time WHERE datetime <= convert_tz(v_Collection_EndGMT,'GMT',v_Customer_Timezone)) ;
				
				IF (SELECT COUNT(*) FROM TotalCollection 
				WHERE CustomerId = v_Collection_CustomerId AND
				PointOfSaleId = v_Collection_PointOfSaleId AND 
				EndTimeIdLocal= lv_TimeIdLocal AND
				CollectionTypeId = v_Collection_CollectionTypeId ) = 1 	THEN
					
					-- Need to check if it is duplicate record
					
					UPDATE TotalCollection
					SET
						CoinTotalAmount = CoinTotalAmount + v_Collection_CoinTotalAmount,
						BillTotalAmount = BillTotalAmount + v_Collection_BillTotalAmount,
						CardTotalAmount = CardTotalAmount + v_Collection_CardTotalAmount,
						TotalAmount = TotalAmount + v_CollectionTotal
					WHERE 
						CustomerId = v_Collection_CustomerId AND
						PointOfSaleId = v_Collection_PointOfSaleId AND
						EndTimeIdLocal= lv_TimeIdLocal AND
						CollectionTypeId = v_Collection_CollectionTypeId;
						
					-- Need a logging table to see the data ? Atleast till TEST evvironment
					-- Need to write code to make sure the same record should not be fetched again in the Cursor. 
					-- Otherwise amount will show wrong values as it will keepon updating
					
				ELSE
				
					INSERT INTO TotalCollection( POSCollectionId, 	CustomerId, 				PointOfSaleId, 				CollectionTypeId, 				EndTimeIdGMT, 	EndTimeIdLocal, CoinTotalAmount, 				BillTotalAmount, 				CardTotalAmount, 				TotalAmount) VALUES
											   ( v_Collection_Id,	v_Collection_CustomerId,	v_Collection_PointOfSaleId,	v_Collection_CollectionTypeId,	lv_TimeIdGMT,	lv_TimeIdLocal,	v_Collection_CoinTotalAmount,	v_Collection_BillTotalAmount,	v_Collection_CardTotalAmount,	v_CollectionTotal);
				
				
				END IF;
						
	
			INSERT INTO ArchiveStagingPOSCollection(Id,RecordInsertTime) VALUES (v_Collection_Id, now());
			
			DELETE FROM StagingPOSCollection WHERE Id = v_Collection_Id;
			
			-- DELETE FROM POSCollectionStaging WHERE ID = v_Collection_Id ;
			
			/*UPDATE ETLStatus 
			SET 
			ETLProcessedId = v_Collection_Id,
			ETLProcessedGMT = NOW()
			WHERE TableName = 'POSCollection';
			*/
		
	set indexm_pos = indexm_pos +1;
end while;

close C1;
        
END//

delimiter ;




DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthly` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthly` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Current_date, lv_Previous_month date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;


declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- Error Handler enabled on March 31 2014 as "Error Code: 1264. Out of range value for column 'MaxUtilizationMinutes' at row 195"
	  INSERT INTO ETLDiscardedData (TableName, 			ProcedureName,	CustomerId,   LocationId, 	TimeIdLocal, 					NumberOfSpaces, 
									MaxUtilizationMinutes) VALUES
								   ('UtilizationMonth', 'sp_MigrateDailyUtilToMonthly',	lv_CustomerId, lv_LocationId, lv_TimeIdBeginingMonthLocal,	lv_no_of_spaces_location*lv_total_days_open_in_month,
								   lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location) ; 
	  
  END;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_Current_date = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check if data has been posted for previous day
	-- EMS 5492
	-- SELECT date(lv_Customer_LocalTime - INTERVAL 1 Day) INTO lv_Previous_date ;
	SELECT date(lv_Customer_LocalTime) INTO lv_Current_date ; -- This is actually current Customer date
	
	
	SELECT COUNT(*) INTO lv_record_cnt 
	FROM ETLMonthlyStatus WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId AND DataRunDate = lv_Current_date ;
	
	IF (lv_record_cnt = 0) THEN
		
		-- means no data has been posted
		-- Get the data from UtilizationDay for previous day
		-- Get the begining TimeID of current month and next month
		
		SELECT now() INTO lv_ETLStartTime ;
		
		-- EMS 5492
		-- Get the CURRENT Month from Time table
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(lv_Current_date)
		and Month = month(lv_Current_date) and dayofmonth=1 and Quarterofday = 0;
				
		SELECT max(Id) INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(lv_Current_date)
		and Month = month(lv_Current_date);
		
		DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_LocationId; 
		
		select year, month into lv_year,lv_month FROM Time where Id = lv_TimeIdBeginingMonthLocal ;
		
		select count(*)*15 into lv_MaxUtilizationMinutes_month
		from Time  T, LocationOpen LO
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LO.DayOfWeekId  and LO.QuarterHourId   = T.QuarterOfDay  and
		LO.LocationId = lv_LocationId and LO.IsOpen = 1;
		
		
		-- Get Total open Days in the month
		select count(*) into lv_total_days_open_in_month
		from Time  T, LocationDay LD
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LD.DayOfWeekId  and
		LD.LocationId = lv_LocationId and LD.IsClosed = 0 and T.QuarterOfDay = 0;
		
		-- EMS 7.1
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,											MaxUtilizationMinutes,		NumberOfPurchases, 	
		TotalMinutesPurchased, ActualMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	lv_no_of_spaces_location*lv_total_days_open_in_month,	lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location, SUM(NumberOfPurchases) , 
		SUM(TotalMinutesPurchased), SUM(ActualMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		
		-- Rest the Variables here:
		set lv_Previous_month = null;
		set lv_TimeIdBeginingMonthLocal = null;
		set lv_TimeIdEndingMonthLocal = null;
		set lv_MaxUtilizationMinutes_month = null;
		set lv_total_days_open_in_month = null;
		
		-- This section is for populating previous month
		-- Get the PREVIOUS Month from Time table
				
		select DATE_FORMAT(lv_Current_date - INTERVAL 1 MONTH, '%Y-%m-01') into lv_Previous_month;
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(lv_Previous_month)
		and Month = month(lv_Previous_month) and dayofmonth=1 and Quarterofday = 0;
			
		SELECT max(Id) INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(lv_Previous_month)
		and Month = month(lv_Previous_month) ;
		
		DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		-- SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_LocationId;
		
		select year, month into lv_year,lv_month FROM Time where Id = lv_TimeIdBeginingMonthLocal ;
		
		select count(*)*15 into lv_MaxUtilizationMinutes_month
		from Time  T, LocationOpen LO
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LO.DayOfWeekId  and LO.QuarterHourId   = T.QuarterOfDay  and
		LO.LocationId = lv_LocationId and LO.IsOpen = 1;
		
		-- Get Total open Days in the month
		select count(*) into lv_total_days_open_in_month
		from Time  T, LocationDay LD
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LD.DayOfWeekId  and
		LD.LocationId = lv_LocationId and LD.IsClosed = 0 and T.QuarterOfDay = 0;
		
		-- EMS 7.1
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,											MaxUtilizationMinutes,		NumberOfPurchases, 	
		TotalMinutesPurchased, ActualMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	lv_no_of_spaces_location*lv_total_days_open_in_month,	lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location, SUM(NumberOfPurchases) , 
		SUM(TotalMinutesPurchased), SUM(ActualMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		
		SELECT now() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	lv_Current_date,	lv_ETLStartTime, lv_ETLEndTime );
		
		
	
	-- ELSE
	
		-- SELECT 'Already Processed for this date' AS  Trace;
	
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- 


DROP PROCEDURE IF EXISTS sp_UpdateUtilMonthly ;

delimiter //

CREATE PROCEDURE sp_UpdateUtilMonthly (
									   IN P_CustomerId MEDIUMINT UNSIGNED, 
									   IN P_LocationId MEDIUMINT UNSIGNED,
									   IN P_NumberOfPurchases MEDIUMINT UNSIGNED, 
									   IN P_TotalMinutesPurchased INT UNSIGNED,
									   IN P_BeginPermitGMT DATETIME,
									   IN P_Customer_Timezone char(25))
BEGIN

declare NO_DATA int(4) default 0;

declare lv_TimeIdBeginingMonthLocal,lv_cnt_UtilMonth MEDIUMINT UNSIGNED  DEFAULT NULL ;
declare continue handler for not found set NO_DATA=-1;


/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/
		-- Get the begining MonthIDLocal of the Customer
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone))
		and Month = month(convert_tz(P_BeginPermitGMT,'GMT',P_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;

		SELECT Count(*) INTO lv_cnt_UtilMonth FROM UtilizationMonth
		WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
		
		IF lv_cnt_UtilMonth = 1 THEN
			
			UPDATE UtilizationMonth SET NumberOfPurchases = NumberOfPurchases + P_NumberOfPurchases ,TotalMinutesPurchased = TotalMinutesPurchased + P_TotalMinutesPurchased
			WHERE TimeIdLocal = lv_TimeIdBeginingMonthLocal AND CustomerId = P_CustomerId AND  LocationId = P_LocationId ;
			
		-- ELSE
			-- SELECT ' IN ELSE';
		END IF ;
END//

delimiter ;

-- Process Monthly Utilization for past data

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateDailyUtilToMonthlyPastTxn` $$
CREATE PROCEDURE `sp_MigrateDailyUtilToMonthlyPastTxn` (IN P_CustomerId MEDIUMINT,
														IN P_Year SMALLINT,
														IN P_Month SMALLINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;


declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
and C.id = P_CustomerId
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
	   INSERT INTO ETLDiscardedData (TableName, 			ProcedureName,	CustomerId,   LocationId, 	TimeIdLocal, 					NumberOfSpaces, 
									MaxUtilizationMinutes) VALUES
								   ('UtilizationMonth', 	'sp_MigrateDailyUtilToMonthlyPastTxn',	lv_CustomerId, lv_LocationId, lv_TimeIdBeginingMonthLocal,	lv_no_of_spaces_location*lv_total_days_open_in_month,
								   lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location) ; 
  END;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;

	SELECT now() INTO lv_ETLStartTime ;
	
    -- Find the TimeIDLocal for Begining Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month and dayofmonth=1 and Quarterofday = 0;
	
	-- Find the TimeIDLocal for Last Date of the the Year and Month.
	SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = P_Year
		and Month = P_Month  and IsLastDayOfMonth = 1 and Quarterofday = 95;
	
		
	-- Think about the autoIncrement PK
	DELETE FROM UtilizationMonth WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
	
	
	-- Nov 14
		-- Get No.of Spaces of the Location
	
		
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_LocationId; 
		
		-- Get Year and month from lv_TimeIdBeginingMonthLocal
		
		select year, month into lv_year,lv_month FROM Time where Id = lv_TimeIdBeginingMonthLocal ;
		
		select count(*)*15 into lv_MaxUtilizationMinutes_month
		from Time  T, LocationOpen LO
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LO.DayOfWeekId  and LO.QuarterHourId   = T.QuarterOfDay  and
		LO.LocationId = lv_LocationId and LO.IsOpen = 1;
		
		
		-- Get Total open Days in the month
		select count(*) into lv_total_days_open_in_month
		from Time  T, LocationDay LD
		where T.Year = lv_year and T.Month = lv_month and
		T.DayOfWeek = LD.DayOfWeekId  and
		LD.LocationId = lv_LocationId and LD.IsClosed = 0 and T.QuarterOfDay = 0;
		
		-- EMS 7.1
		INSERT UtilizationMonth (TimeIdLocal, CustomerId, 	LocationId, 	NumberOfSpaces,											MaxUtilizationMinutes,		NumberOfPurchases, 	
		TotalMinutesPurchased, ActualMinutesPurchased)
		SELECT lv_TimeIdBeginingMonthLocal,	 lv_CustomerId, lv_LocationId,	lv_no_of_spaces_location*lv_total_days_open_in_month,	lv_MaxUtilizationMinutes_month*lv_no_of_spaces_location , SUM(NumberOfPurchases) , 
		SUM(TotalMinutesPurchased), SUM(ActualMinutesPurchased)
		FROM UtilizationDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal 
		GROUP by CustomerId, LocationId ; 
		

	

		
	-- select lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal ;
		
	SELECT now() INTO lv_ETLEndTime ;
	
	INSERT INTO ETLMonthlyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingMonthLocal,	lv_CustomerId,	lv_LocationId,	date(UTC_TIMESTAMP()),	lv_ETLStartTime, lv_ETLEndTime );
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

		
-- 

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateHourlyOccupancyToDaily` $$
CREATE PROCEDURE `sp_MigrateHourlyOccupancyToDaily` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal,lv_TimeIdBeginingMonthLocal , lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_Previous_date date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
-- and C.id = 160 and L.id=2237 
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
	  INSERT INTO ETLDiscardedData (TableName, 			ProcedureName,							CustomerId,   LocationId, 	TimeIdLocal, 					NumberOfSpaces) VALUES
								   ('OccupancyDay', 	'sp_MigrateHourlyOccupancyToDaily',	lv_CustomerId, lv_LocationId, lv_TimeIdBeginingDayLocal,	lv_location_open_hours*lv_no_of_spaces_location  ) ; 
  END;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_Previous_date = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingDayLocal = NULL ;
	SET lv_TimeIdEndLocal = NULL ;
	SET lv_TimeIdBeginingMonthLocal = NULL ;
	SET lv_TimeIdEndingMonthLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check if data has been posted for previous day
	SELECT date(lv_Customer_LocalTime - INTERVAL 1 Day) INTO lv_Previous_date ;
	
	SELECT COUNT(*) INTO lv_record_cnt 
	FROM ETLOccupancyStatus WHERE CustomerId = lv_CustomerId AND LocationId = lv_LocationId AND DataRunDate = lv_Previous_date ;
	
	IF (lv_record_cnt = 0) THEN
		
		-- means no data has been posted
		-- Get the data from UtilizationDay for previous day
		-- Get the begining TimeID of current month and next month
		
		SELECT now() INTO lv_ETLStartTime ;
		
		
		-- New logic
		
		-- Get No of spaces
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_LocationId; 
		
		-- Starting TimeId of Day
		SELECT Id INTO lv_TimeIdBeginingDayLocal FROM Time where Date = lv_Previous_date and QuarterOfDay = 0;
		
		
		-- Ending TimeId of Day
		SELECT Id into lv_TimeIdEndLocal FROM Time WHERE datetime = ( select max(datetime) from Time where Date=lv_Previous_date);
		
		
		
		-- Get Open hours
		Select count(*) into lv_location_open_hours
		from Time  T, LocationOpen LO
		where  T.Date = lv_Previous_date and
		T.DayOfWeek = LO.DayOfWeekId  and LO.QuarterHourId   = T.QuarterOfDay  and
		LO.LocationId = lv_LocationId and LO.IsOpen = 1 ;
		
		DELETE FROM OccupancyDay where CustomerId = lv_CustomerId and LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingDayLocal ;
		
		
		INSERT OccupancyDay (TimeIdLocal, 				 CustomerId,		LocationId, 	UnifiedRateId, NumberOfSpaces, 		 							NoOfPermits)
		SELECT 				lv_TimeIdBeginingDayLocal,	 lv_CustomerId, 	lv_LocationId,	UnifiedRateId, lv_location_open_hours*lv_no_of_spaces_location , sum(NoOfPermits)
		FROM OccupancyHour
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingDayLocal AND lv_TimeIdEndLocal 
		GROUP by CustomerId, LocationId, unifiedRateId ;
		
		-- New Code added on Feb 18
		
		SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth=1 and Quarterofday = 0;
		
		SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = year(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone))
		and Month = month(convert_tz(lv_Previous_date,'GMT',lv_Customer_Timezone)) and dayofmonth = day(LAST_DAY(lv_Previous_date)) and Quarterofday = 95;
		
		DELETE FROM OccupancyMonth where CustomerId = lv_CustomerId and LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		INSERT INTO OccupancyMonth	(TimeIdLocal,			CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces, NoOfPermits) 
		SELECT lv_TimeIdBeginingMonthLocal, 				lv_CustomerId,	lv_LocationId,	UnifiedRateId,	0, 				sum(NoOfPermits)
		FROM OccupancyDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal
		GROUP by CustomerId, LocationId, unifiedRateId ; 
	
		
		
		SELECT now() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLOccupancyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingDayLocal,	lv_CustomerId,	lv_LocationId,	lv_Previous_date,	lv_ETLStartTime, lv_ETLEndTime );
		
		
	
	-- ELSE
	
		-- SELECT 'Already Processed for this date' AS  Trace;
	
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- -----------------------------------------------------------------------------------------------------

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_MigrateHourlyOccupancyToDailyPastTxn` $$
CREATE PROCEDURE `sp_MigrateHourlyOccupancyToDailyPastTxn` (IN P_CustomerId MEDIUMINT, IN P_Year SMALLINT,IN P_Month SMALLINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal,lv_TimeIdBeginingMonthLocal,lv_TimeIdEndingMonthLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_OccupancyDate date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;
declare lv_LastDayOfMonth tinyint unsigned ;
declare lv_StartingDayOfMonth tinyint unsigned default 1;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
and C.id = P_CustomerId
AND P.CustomerPropertyTypeId =1 
ORDER by C.id;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
	  INSERT INTO ETLDiscardedData (TableName, 			ProcedureName,							CustomerId,   LocationId, 	TimeIdLocal, 					NumberOfSpaces) VALUES
								   ('OccupancyDayMonth', 	'sp_MigrateHourlyOccupancyToDaily',	lv_CustomerId, lv_LocationId, lv_TimeIdBeginingDayLocal,	lv_location_open_hours*lv_no_of_spaces_location  ) ; 
  END;


set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

-- Get End Date of the Month
set lv_StartingDayOfMonth = 1;
select max(DayOfMOnth) into lv_LastDayOfMonth from Time
where Year = P_Year and Month=P_Month;



	while lv_StartingDayOfMonth <= lv_LastDayOfMonth do
	
    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET lv_OccupancyDate = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_TimeIdBeginingDayLocal = NULL ;
	SET lv_TimeIdEndLocal = NULL ;
	
	
	-- Starting TimeId of Day
	SELECT Id,Date INTO lv_TimeIdBeginingDayLocal, lv_OccupancyDate FROM Time where Year = P_Year
	and Month = P_Month and dayofmonth= lv_StartingDayOfMonth and Quarterofday = 0;
	
	-- added on Feb 19
	SELECT Id INTO lv_TimeIdBeginingMonthLocal FROM Time where Year = P_Year
	and Month = P_Month and dayofmonth= 1 and Quarterofday = 0;
	
	SELECT Id INTO lv_TimeIdEndingMonthLocal FROM Time where Year = P_Year
	and Month = P_Month and IsLastDayOfMonth= 1 and Quarterofday = 0;
	
	
	SELECT now() INTO lv_ETLStartTime ;
		
		
		-- New logic
		
		-- Get No of spaces
		SELECT if(NumberOfSpaces=0,0,NumberOfSpaces) into lv_no_of_spaces_location FROM Location where Id = lv_LocationId; 
		
	
		
		
		-- Ending TimeId of Day
		SELECT Id into lv_TimeIdEndLocal FROM Time WHERE datetime = ( select max(datetime) from Time where Date=lv_OccupancyDate);
		
		
		
		-- Get Open hours
		Select count(*) into lv_location_open_hours
		from Time  T, LocationOpen LO
		where  T.Date = lv_OccupancyDate and
		T.DayOfWeek = LO.DayOfWeekId  and LO.QuarterHourId   = T.QuarterOfDay  and
		LO.LocationId = lv_LocationId and LO.IsOpen = 1 ;
		
		DELETE FROM OccupancyDay where CustomerId = lv_CustomerId and LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingDayLocal ;
		
		
		INSERT OccupancyDay (TimeIdLocal, 				 CustomerId,		LocationId, 	UnifiedRateId, NumberOfSpaces, 		 							NoOfPermits)
		SELECT 				lv_TimeIdBeginingDayLocal,	 lv_CustomerId, 	lv_LocationId,	UnifiedRateId, lv_location_open_hours*lv_no_of_spaces_location , sum(NoOfPermits)
		FROM OccupancyHour
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingDayLocal AND lv_TimeIdEndLocal 
		GROUP by CustomerId, LocationId, unifiedRateId ;
		
		
		DELETE FROM OccupancyMonth where CustomerId = lv_CustomerId and LocationId = lv_LocationId and TimeIdLocal = lv_TimeIdBeginingMonthLocal ;
		
		INSERT INTO OccupancyMonth	(TimeIdLocal,			CustomerId,		LocationId,		UnifiedRateId,		NumberOfSpaces, NoOfPermits) 
		SELECT lv_TimeIdBeginingMonthLocal, 				lv_CustomerId,	lv_LocationId,	UnifiedRateId,	0, 				sum(NoOfPermits)
		FROM OccupancyDay
		WHERE CustomerId = lv_CustomerId AND  LocationId = lv_LocationId AND TimeIdLocal BETWEEN lv_TimeIdBeginingMonthLocal AND lv_TimeIdEndingMonthLocal
		GROUP by CustomerId, LocationId, unifiedRateId ; 
		
		
		SELECT now() INTO lv_ETLEndTime ;
		
		INSERT INTO ETLOccupancyStatus (TimeIdLocal, 					CustomerId, 	LocationId, 	DataRunDate, 		StartTime, 		 EndTime) VALUES
									 (lv_TimeIdBeginingDayLocal,	lv_CustomerId,	lv_LocationId,	lv_OccupancyDate,	lv_ETLStartTime, lv_ETLEndTime );
		
	
	
	set lv_StartingDayOfMonth = lv_StartingDayOfMonth + 1 ;
	end while ;
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- -----------------------------------------------------------------------------------------------------
-- Procedure added on Dec 17

-- -----------------------------------------------------------------------------------------------------

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_FillLocationDetails` $$
CREATE PROCEDURE `sp_FillLocationDetails` (IN P_CustomerId MEDIUMINT, IN P_LocationId MEDIUMINT, IN P_FromDate DATE, IN P_ToDate DATE)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal,lv_TimeIdBeginMonth,lv_TimeIdEndMonth,lv_sum_NumberOfSpaces_Daily INT UNSIGNED ;
DECLARE lv_sum_MaxUtilizationMinutes_Daily INT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_OccupancyDate date;
-- Feb 14
declare lv_no_of_spaces_location, lv_TimeIdGMT, lv_TimeIdLocal,lv_Time_Diff,lv_sum_NumberOfSpaces_Hourly,lv_sum_MaxUtilizationMinutes_Hourly,lv_CustomerLocalTimeId  int unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;
declare lv_LastDayOfMonth tinyint unsigned ;
declare lv_StartingDayOfMonth tinyint unsigned default 1;
DECLARE DateStart DATE;
DECLARE DateEnd DATE;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId AND C.Id  = P_CustomerId AND L.Id = P_LocationId
AND P.CustomerPropertyTypeId =1 
AND L.IsUnassigned <> 1 and L.NumberOfSpaces>0 and L.IsParent=0 and L.IsDeleted = 0
AND L.id NOT IN ( SELECT LocationId from ETLLocationStatus where DataRunDate between P_FromDate and P_ToDate )
;


declare continue handler for not found set NO_DATA=-1;

 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
	  INSERT INTO ETLDiscardedData (TableName, 			ProcedureName,					CustomerId,   LocationId, 	TimeIdLocal) VALUES
								   ('ETLLocationDetail', 	'sp_FillLocationDetails',	lv_CustomerId, lv_LocationId, lv_CustomerLocalTimeId ) ; 
  END;


set indexm_pos =0;
set idmaster_pos =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

-- select systemtime
-- Added on 17 feb 2014
set DateStart  = P_FromDate ;
while DateStart <= P_ToDate DO


set lv_sum_NumberOfSpaces_Hourly  = 0;
set lv_sum_MaxUtilizationMinutes_Hourly = 0;
set lv_sum_NumberOfSpaces_Daily =0;
set lv_sum_MaxUtilizationMinutes_Daily = 0;
set lv_no_of_spaces_location = 0;

SELECT now() into lv_ETLStartTime; 

-- Get NumberOfSpaces of the Location
SELECT NumberOfSpaces into lv_no_of_spaces_location FROM Location where Id = lv_LocationId;

 
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.
SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(DateStart,'GMT',lv_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN
	SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;
END IF;


-- Fill ETLLocationDetail HOURLY ----------------
-- Feb 14 reversed TimeIDLocal and TimeIdGMT
-- check if copied to other place

SELECT id into lv_CustomerLocalTimeId 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

-- Commented the below on Aug 27 for SUPP 723 and EMS 5056
-- DELETE FROM ETLLocationDetail WHERE CustomerId = P_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,					TimeIdLocal,			NumberOfSpaces,			  			TotalNumberOfSpaces,					MaxUtilizationMinutes) 
SELECT P_CustomerId, LO.LocationId, 1, 					 T.Id + lv_Time_Diff,	   T.Id ,  lv_no_of_spaces_location*LO.IsOpen, lv_no_of_spaces_location*LO.IsOpen,	15*lv_no_of_spaces_location*LO.IsOpen
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = T.QuarterOfDay 
AND LO.LocationId = lv_LocationId;


-- Get Sum(NumberOfSpaces from Hourly)
-- Feb 14
select ifnull(sum(NumberOfSpaces),0) into lv_sum_NumberOfSpaces_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;

-- Get Sum(MaxUtilizationMinutes from Hourly)
-- Feb 14
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;



-- Fill ETLLocationDetail Daily -----------------------
DELETE FROM ETLLocationDetail WHERE CustomerId = P_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,		TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) 
SELECT P_CustomerId, lv_LocationId,		 2,						 T.Id,			T.Id,	lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Hourly,	lv_sum_MaxUtilizationMinutes_Hourly
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = 0 AND T.TimeAmPm = '00:00 AM' 
AND LO.LocationId = lv_LocationId ;



-- Get the Time Id of begining of the month
SELECT Id INTO lv_TimeIdBeginMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and dayofmonth=1 and Quarterofday = 0;

-- Get the Time Id of End of the month
SELECT Id INTO lv_TimeIdEndMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and IsLastDayOfMonth=1 and Quarterofday = 0;

-- Get Sum(NumberOfSpaces from Daily)

select ifnull(sum(TotalNumberOfSpaces),0) into lv_sum_NumberOfSpaces_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;

-- Get Sum(MaxUtilizationMinutes from Daily)
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;


-- Fill ETLLocationDetail Monthly ------------------------------------------------
DELETE FROM ETLLocationDetail where CustomerId = P_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId=3 and TimeIdGMT = lv_TimeIdBeginMonth;
INSERT INTO ETLLocationDetail 
(CustomerId , LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) VALUES
( P_CustomerId, lv_LocationId,		 3,				 lv_TimeIdBeginMonth,	lv_TimeIdBeginMonth,		lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Daily,	lv_sum_MaxUtilizationMinutes_Daily) ;



INSERT INTO ETLLocationStatus
(CustomerId, 					LocationId, 	DataRunDate, 			StartTime, EndTime) VALUES
(lv_CustomerId,		lv_LocationId,	DateStart,	lv_ETLStartTime, now() );



SET DateStart = DATE_ADD(DateStart,INTERVAL 1 day);
end while;

	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- -----------------------------------------------------------------------------------------------------



-- Procedure sp_GetLocationDetails 
DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_GetLocationDetails` $$
CREATE PROCEDURE `sp_GetLocationDetails` ()
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare FromDate,lv_LastRunDate date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
AND P.CustomerPropertyTypeId =1 
AND L.IsUnassigned <> 1 and L.NumberOfSpaces>0 and L.IsParent=0 and L.IsDeleted = 0
;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

    -- Find the current local time of the Customer
	SET lv_Customer_LocalTime = NULL ;
	SET FromDate = NULL ;
	SET lv_record_cnt = 0 ;
	SET lv_LastRunDate = NULL ;
	SET lv_TimeIdEndLocal = NULL ;
	
	select convert_tz(UTC_TIMESTAMP(),'GMT',lv_Customer_Timezone)  INTO lv_Customer_LocalTime ;
	
	-- Check last date of processed date from ETLLocationStatus
	select ifnull(Max(DataRunDate),DATE_ADD(lv_Customer_LocalTime,INTERVAL -1 day)) into lv_LastRunDate from ETLLocationStatus where CustomerId = lv_CustomerId and LocationId = lv_LocationId;
	
	IF (lv_LastRunDate <= date(lv_Customer_LocalTime)) THEN
		
		SET FromDate = DATE_ADD(lv_LastRunDate,INTERVAL 1 day);
		CALL sp_FillLocationDetails(lv_CustomerId,lv_LocationId,FromDate,date(lv_Customer_LocalTime)) ;
		
	END IF;
	

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_GetLocationDetails 


-- PROCEDURE sp_FillLocationDetails_PastTxn 
/* 
call sp_FillLocationDetails_PastTxn('2012-08-01','2012-08-31');
call sp_FillLocationDetails_PastTxn('2012-09-01','2012-09-30');
call sp_FillLocationDetails_PastTxn('2012-10-01','2012-10-31');
call sp_FillLocationDetails_PastTxn('2012-11-01','2012-11-30');
call sp_FillLocationDetails_PastTxn('2012-12-01','2012-12-31');
call sp_FillLocationDetails_PastTxn('2013-01-01','2013-01-31');
call sp_FillLocationDetails_PastTxn('2013-02-01','2013-02-28');
call sp_FillLocationDetails_PastTxn('2013-03-01','2013-03-31');
call sp_FillLocationDetails_PastTxn('2013-04-01','2013-04-30');
call sp_FillLocationDetails_PastTxn('2013-05-01','2013-05-31');
call sp_FillLocationDetails_PastTxn('2013-06-01','2013-06-30');
call sp_FillLocationDetails_PastTxn('2013-07-01','2013-07-31');
call sp_FillLocationDetails_PastTxn('2013-08-01','2013-08-31');
call sp_FillLocationDetails_PastTxn('2013-09-01','2013-09-30');
call sp_FillLocationDetails_PastTxn('2013-10-01','2013-10-31');
call sp_FillLocationDetails_PastTxn('2013-11-01','2013-11-30');
call sp_FillLocationDetails_PastTxn('2013-12-01','2013-12-31');



*/
DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_FillLocationDetails_PastTxn` $$
CREATE PROCEDURE `sp_FillLocationDetails_PastTxn` (IN P_FromDate DATE, IN P_ToDate DATE)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal,lv_TimeIdBeginMonth,lv_TimeIdEndMonth,lv_sum_NumberOfSpaces_Daily INT UNSIGNED ;
DECLARE lv_sum_MaxUtilizationMinutes_Daily INT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_OccupancyDate date;
declare lv_no_of_spaces_location, lv_TimeIdGMT, lv_TimeIdLocal,lv_Time_Diff,lv_sum_NumberOfSpaces_Hourly,lv_sum_MaxUtilizationMinutes_Hourly,lv_CustomerLocalTimeId  int unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;
declare lv_LastDayOfMonth tinyint unsigned ;
declare lv_StartingDayOfMonth tinyint unsigned default 1;
DECLARE DateStart DATE;
DECLARE DateEnd DATE;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
AND P.CustomerPropertyTypeId =1 
AND L.IsUnassigned <> 1 and L.NumberOfSpaces>0 and L.IsParent=0 and L.IsDeleted = 0
AND L.id NOT IN ( SELECT LocationId from ETLLocationStatus where DataRunDate between P_FromDate and P_ToDate )
;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

-- select systemtime
-- Added on 17 feb 2014
set DateStart  = P_FromDate ;
while DateStart <= P_ToDate DO


set lv_sum_NumberOfSpaces_Hourly  = 0;
set lv_sum_MaxUtilizationMinutes_Hourly = 0;
set lv_sum_NumberOfSpaces_Daily =0;
set lv_sum_MaxUtilizationMinutes_Daily = 0;
set lv_no_of_spaces_location = 0;

SELECT now() into lv_ETLStartTime; 

-- Get NumberOfSpaces of the Location
SELECT NumberOfSpaces into lv_no_of_spaces_location FROM Location where Id = lv_LocationId;

 
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.
SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(DateStart,'GMT',lv_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN
	SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;
END IF;

-- Fill ETLLocationDetail HOURLY ----------------
-- Feb 14 reversed TimeIDLocal and TimeIdGMT

SELECT id into lv_CustomerLocalTimeId 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

-- Commented the below on Aug 27 for SUPP 723 and EMS 5056
-- DELETE FROM ETLLocationDetail WHERE CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,					TimeIdLocal,			NumberOfSpaces,			  			TotalNumberOfSpaces,					MaxUtilizationMinutes) 
SELECT lv_CustomerId, LO.LocationId, 1, 					 T.Id + lv_Time_Diff,	   T.Id ,  lv_no_of_spaces_location*LO.IsOpen, lv_no_of_spaces_location*LO.IsOpen,	15*lv_no_of_spaces_location*LO.IsOpen
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = T.QuarterOfDay 
AND LO.LocationId = lv_LocationId;


-- Get Sum(NumberOfSpaces from Hourly)
-- Feb 14
select ifnull(sum(NumberOfSpaces),0) into lv_sum_NumberOfSpaces_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;

-- Get Sum(MaxUtilizationMinutes from Hourly)
-- Feb 14
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;

-- Fill ETLLocationDetail Daily -----------------------
DELETE FROM ETLLocationDetail WHERE CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,		TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) 
SELECT lv_CustomerId, lv_LocationId,		 2,						 T.Id,			T.Id,	lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Hourly,	lv_sum_MaxUtilizationMinutes_Hourly
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = 0 AND T.TimeAmPm = '00:00 AM' 
AND LO.LocationId = lv_LocationId ;



-- Get the Time Id of begining of the month
SELECT Id INTO lv_TimeIdBeginMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and dayofmonth=1 and Quarterofday = 0;

-- Get the Time Id of End of the month
SELECT Id INTO lv_TimeIdEndMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and IsLastDayOfMonth=1 and Quarterofday = 0;

-- Get Sum(NumberOfSpaces from Daily)

select ifnull(sum(TotalNumberOfSpaces),0) into lv_sum_NumberOfSpaces_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;

-- Get Sum(MaxUtilizationMinutes from Daily)
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;


-- Fill ETLLocationDetail Monthly ------------------------------------------------
DELETE FROM ETLLocationDetail where CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId=3 and TimeIdGMT = lv_TimeIdBeginMonth;
INSERT INTO ETLLocationDetail 
(CustomerId , LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) VALUES
( lv_CustomerId, lv_LocationId,		 3,				 lv_TimeIdBeginMonth,	lv_TimeIdBeginMonth,		lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Daily,	lv_sum_MaxUtilizationMinutes_Daily) ;


INSERT INTO ETLLocationStatus
(CustomerId, 					LocationId, 	DataRunDate, 			StartTime, EndTime) VALUES
(lv_CustomerId,		lv_LocationId,	DateStart,	lv_ETLStartTime, now() );


SET DateStart = DATE_ADD(DateStart,INTERVAL 1 day);
end while;

	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- END Procedure sp_FillLocationDetails_PastTxn



-- Added on 14 Feb 2014
-- Procedure begin sp_InsertUtilizationDay 
DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_InsertUtilizationDay` $$
CREATE PROCEDURE `sp_InsertUtilizationDay` (P_CustomerId MEDIUMINT UNSIGNED, 
											P_LocationId MEDIUMINT UNSIGNED, 
											P_PermitBeginingDayTimeIdLocal MEDIUMINT UNSIGNED,
											P_PermitEndingDayTimeIdLocal MEDIUMINT UNSIGNED,
											P_TotalMinutesPurchased INT UNSIGNED,
											P_MinitesForCurrentDay INT UNSIGNED,
											P_IsPurchase TINYINT)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal MEDIUMINT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare FromDate,lv_LastRunDate date;
declare lv_no_of_spaces_location  mediumint unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month,lv_DayTimeid, lv_MinutesForCurrentDay , lv_TotalMinutesPurchased,lv_insert_RemainingminutesPurchase,lv_tmp_minutes int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;

declare c1 cursor  for
SELECT id, P_MinitesForCurrentDay , P_TotalMinutesPurchased
FROM Time
WHERE id BETWEEN P_PermitBeginingDayTimeIdLocal 
AND P_PermitEndingDayTimeIdLocal
and TimeAmPm = '00:00 AM'
;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

-- 1) Check if P_PurchaseBeginingTimeIdLocal and P_PurchaseEndingTimeIdLocal is within a day
-- 2) 

set indexm_pos =0;
set idmaster_pos =0;

set lv_insert_RemainingminutesPurchase = 0;
set lv_tmp_minutes = 0 ;
open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_DayTimeid, lv_MinutesForCurrentDay , lv_TotalMinutesPurchased ;

    -- Find the current local time of the Customer
	
	if (indexm_pos = 0) then
		if (lv_insert_RemainingminutesPurchase+lv_MinutesForCurrentDay <= lv_TotalMinutesPurchased ) then
			
			INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 		NumberOfPurchases, TotalMinutesPurchased, ActualMinutesPurchased ) VALUES
			(lv_DayTimeid,	P_CustomerId,	P_LocationId, 		0,							0,							P_IsPurchase,		lv_MinutesForCurrentDay, P_TotalMinutesPurchased )
			ON DUPLICATE KEY UPDATE NumberOfPurchases= NumberOfPurchases+P_IsPurchase, TotalMinutesPurchased=TotalMinutesPurchased+lv_MinutesForCurrentDay, ActualMinutesPurchased=ActualMinutesPurchased+P_TotalMinutesPurchased ;
			
			set lv_insert_RemainingminutesPurchase = lv_TotalMinutesPurchased - lv_MinutesForCurrentDay ;
			set lv_tmp_minutes = lv_MinutesForCurrentDay ;
		end if;
		
	else

			if (lv_insert_RemainingminutesPurchase  > 1440 ) or (lv_insert_RemainingminutesPurchase  = 1440 ) then
				set lv_tmp_minutes = 1440 ;		
			else
				set lv_tmp_minutes = lv_insert_RemainingminutesPurchase ;
			end if;
		
		INSERT INTO UtilizationDay
			(TimeIdLocal,		CustomerId, 	LocationId, 	NumberOfSpaces, 			MaxUtilizationMinutes, 		NumberOfPurchases, TotalMinutesPurchased ) VALUES
			(lv_DayTimeid,	P_CustomerId,	P_LocationId, 		0,							0,							0,				  lv_tmp_minutes )
			ON DUPLICATE KEY UPDATE NumberOfPurchases= NumberOfPurchases+0,TotalMinutesPurchased=TotalMinutesPurchased+lv_tmp_minutes ; 
	
		set lv_insert_RemainingminutesPurchase = lv_insert_RemainingminutesPurchase - lv_tmp_minutes;
	
	end if;
	-- set lv_insert_RemainingminutesPurchase = lv_TotalMinutesPurchased - lv_MinutesForCurrentDay ;
	-- set lv_tmp_minutes = lv_MinutesForCurrentDay ;

set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_InsertUtilizationDay  

-- Start Procedure sp_FillLocationDetails_PastTxn_Customer

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_FillLocationDetails_PastTxn_Customer` $$
CREATE PROCEDURE `sp_FillLocationDetails_PastTxn_Customer` (IN P_CustomerId MEDIUMINT, IN P_FromDate DATE, IN P_ToDate DATE)
BEGIN

declare NO_DATA int default 0;
declare v1 int;
declare idmaster_pos,indexm_pos int;

DECLARE lv_Customer_LocalTime,lv_ETLStartTime,lv_ETLEndTime datetime ;
DECLARE lv_record_cnt INT ;
DECLARE lv_TimeIdBeginingDayLocal,lv_TimeIdEndLocal,lv_TimeIdBeginMonth,lv_TimeIdEndMonth,lv_sum_NumberOfSpaces_Daily INT UNSIGNED ;
DECLARE lv_sum_MaxUtilizationMinutes_Daily INT UNSIGNED ;
DECLARE lv_CustomerId, lv_LocationId MEDIUMINT UNSIGNED ;
declare lv_Customer_Timezone char(25);
declare lv_OccupancyDate date;
declare lv_no_of_spaces_location, lv_TimeIdGMT, lv_TimeIdLocal,lv_Time_Diff,lv_sum_NumberOfSpaces_Hourly,lv_sum_MaxUtilizationMinutes_Hourly,lv_CustomerLocalTimeId  int unsigned ;
declare lv_year smallint unsigned ;
declare lv_month tinyint unsigned;
declare lv_MaxUtilizationMinutes_month int unsigned ;
declare lv_total_days_open_in_month mediumint unsigned ;
declare lv_location_open_hours mediumint unsigned ;
declare lv_LastDayOfMonth tinyint unsigned ;
declare lv_StartingDayOfMonth tinyint unsigned default 1;
DECLARE DateStart DATE;
DECLARE DateEnd DATE;

declare c1 cursor  for
SELECT C.id , L.id, P.PropertyValue
FROM Customer C, Location L , CustomerProperty P
WHERE C.id = L.CustomerId AND C.id = P.CustomerId 
AND P.CustomerPropertyTypeId =1 
AND L.IsUnassigned <> 1 and L.NumberOfSpaces>0 and L.IsParent=0 and L.IsDeleted = 0 AND C.id = P_CustomerId
AND L.id NOT IN ( SELECT LocationId from ETLLocationStatus where DataRunDate between P_FromDate and P_ToDate )
;


declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;


open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_LocationId, lv_Customer_Timezone ;

-- select systemtime
-- Added on 17 feb 2014
set DateStart  = P_FromDate ;
while DateStart <= P_ToDate DO


set lv_sum_NumberOfSpaces_Hourly  = 0;
set lv_sum_MaxUtilizationMinutes_Hourly = 0;
set lv_sum_NumberOfSpaces_Daily =0;
set lv_sum_MaxUtilizationMinutes_Daily = 0;
set lv_no_of_spaces_location = 0;

SELECT now() into lv_ETLStartTime; 

-- Get NumberOfSpaces of the Location
SELECT NumberOfSpaces into lv_no_of_spaces_location FROM Location where Id = lv_LocationId;

 
-- to find the offset of GMT TimeId and Customer Local TimeId  
-- This will avoid unnecessary Function usage.
SELECT id into lv_TimeIdGMT 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

SELECT Id INTO lv_TimeIdLocal FROM Time 
where DateTime = convert_tz(DateStart,'GMT',lv_Customer_Timezone) ;

IF lv_TimeIdGMT >= lv_TimeIdLocal THEN
	SELECT lv_TimeIdGMT - lv_TimeIdLocal INTO lv_Time_Diff;
END IF;

-- Fill ETLLocationDetail HOURLY ----------------
-- Feb 14 reversed TimeIDLocal and TimeIdGMT

SELECT id into lv_CustomerLocalTimeId 
FROM Time WHERE Date = DateStart AND TimeAmPm = '00:00 AM';

-- Commented the below on Aug 27 for SUPP 723 and EMS 5056
-- DELETE FROM ETLLocationDetail WHERE CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,					TimeIdLocal,			NumberOfSpaces,			  			TotalNumberOfSpaces,					MaxUtilizationMinutes) 
SELECT lv_CustomerId, LO.LocationId, 1, 					 T.Id + lv_Time_Diff,	   T.Id ,  lv_no_of_spaces_location*LO.IsOpen, lv_no_of_spaces_location*LO.IsOpen,	15*lv_no_of_spaces_location*LO.IsOpen
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = T.QuarterOfDay 
AND LO.LocationId = lv_LocationId;


-- Get Sum(NumberOfSpaces from Hourly)
-- Feb 14
select ifnull(sum(NumberOfSpaces),0) into lv_sum_NumberOfSpaces_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;

-- Get Sum(MaxUtilizationMinutes from Hourly)
-- Feb 14
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Hourly from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 1 and TimeIdLocal between lv_CustomerLocalTimeId and lv_CustomerLocalTimeId+95;

-- Fill ETLLocationDetail Daily -----------------------
DELETE FROM ETLLocationDetail WHERE CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT=lv_TimeIdGMT;
INSERT INTO ETLLocationDetail 
(CustomerId, LocationId,		 ETLLocationDetailTypeId,		TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) 
SELECT lv_CustomerId, lv_LocationId,		 2,						 T.Id,			T.Id,	lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Hourly,	lv_sum_MaxUtilizationMinutes_Hourly
FROM 
Time T, LocationOpen LO
WHERE T.Date between DateStart and DateStart
AND T.DayOfWeek = LO.DayOfWeekId 
AND LO.QuarterHourId   = 0 AND T.TimeAmPm = '00:00 AM' 
AND LO.LocationId = lv_LocationId ;



-- Get the Time Id of begining of the month
SELECT Id INTO lv_TimeIdBeginMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and dayofmonth=1 and Quarterofday = 0;

-- Get the Time Id of End of the month
SELECT Id INTO lv_TimeIdEndMonth FROM Time where Year = year(DateStart)
and Month = month(DateStart) and IsLastDayOfMonth=1 and Quarterofday = 0;

-- Get Sum(NumberOfSpaces from Daily)

select ifnull(sum(TotalNumberOfSpaces),0) into lv_sum_NumberOfSpaces_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;

-- Get Sum(MaxUtilizationMinutes from Daily)
select ifnull(sum(MaxUtilizationMinutes),0) into lv_sum_MaxUtilizationMinutes_Daily from ETLLocationDetail
where LocationId =lv_LocationId and ETLLocationDetailTypeId = 2 and TimeIdGMT between lv_TimeIdBeginMonth and lv_TimeIdEndMonth;


-- Fill ETLLocationDetail Monthly ------------------------------------------------
DELETE FROM ETLLocationDetail where CustomerId = lv_CustomerId and LocationId = lv_LocationId and ETLLocationDetailTypeId=3 and TimeIdGMT = lv_TimeIdBeginMonth;
INSERT INTO ETLLocationDetail 
(CustomerId , LocationId,		 ETLLocationDetailTypeId,TimeIdGMT,				TimeIdLocal,				NumberOfSpaces,			  		TotalNumberOfSpaces,			MaxUtilizationMinutes) VALUES
( lv_CustomerId, lv_LocationId,		 3,				 lv_TimeIdBeginMonth,	lv_TimeIdBeginMonth,		lv_no_of_spaces_location,		lv_sum_NumberOfSpaces_Daily,	lv_sum_MaxUtilizationMinutes_Daily) ;


INSERT INTO ETLLocationStatus
(CustomerId, 					LocationId, 	DataRunDate, 			StartTime, EndTime) VALUES
(lv_CustomerId,		lv_LocationId,	DateStart,	lv_ETLStartTime, now() );


SET DateStart = DATE_ADD(DateStart,INTERVAL 1 day);
end while;

	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- END Procedure sp_FillLocationDetails_PastTxn_Customer

-- Start ETL Run for Past Data

DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_ETLStart_Inst1` $$
CREATE PROCEDURE `sp_ETLStart_Inst1` ()

BEGIN

declare NO_DATA int(4) default 0;
declare lv_ETLProcessDateRangeId  INT;
declare lv_BeginPurchaseDt datetime;
declare lv_EndPurchaseDt datetime;
DECLARE lv_count,lv_ETL_daterangeId,lv_count_ETLProcessDateRange int;
DECLARE lv_status TINYINT;
DECLARE lv_Parameter1, lv_Parameter2,ld_end_time DATETIME;
DECLARE lv_rows_effected MEDIUMINT default 0;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

SELECT COUNT(1) INTO lv_count FROM ETLExecutionLogBeta where ETLProcessTypeId  = 1;

select count(*) INTO lv_count_ETLProcessDateRange from ETLProcessDateRange where ETLProcessTypeId = 1 and status = 0 and ETLObject ='PPP' ;

IF (lv_count_ETLProcessDateRange =0) THEN

UPDATE ETLCustomer SET ETLRunTime = now()  where ETLRunTime is null;
SELECT ROW_COUNT() into lv_rows_effected ;
	if (lv_rows_effected) > 0 then
		DROP EVENT IF EXISTS EVENT_StartETL_PastData ;
		call sp_ETLRollupHourlyToDaily();
		ALTER EVENT EVENT_StartETL ENABLE;
		ALTER EVENT EVENT_MigrateDailyUtilToMonthly ENABLE;
		ALTER EVENT EVENT_ETLReset ENABLE;
		ALTER EVENT EVENT_MigrateHourlyOccupancyToDaily ENABLE;	
		ALTER EVENT EVENT_FillETLLocationDetails ENABLE;
		
		INSERT INTO ETLExecutionMessage (Message,RecordInsertTime)
		VALUES ('ETL Execution Completed',now());


	end if ;

END IF;

IF (lv_count_ETLProcessDateRange > 0) THEN

IF (lv_count = 0 )   THEN

  -- Initial Record 
  select BeginDt, EndDt, id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId 
  from ETLProcessDateRange where ETLProcessTypeId = 1 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 


  INSERT INTO ETLExecutionLogBeta (ETLProcessTypeId, Parameter1, 		Parameter2, 	ETLStartTime, 		ETLEndTime,		 Status, Remarks	)	
					    VALUES(1,		 lv_Parameter1, 	lv_Parameter2 ,	NOW(),    NOW(), 2,	     'InitialRecord'  );


  set lv_status = 2;

ELSE
  
  SELECT Status 
  INTO lv_status
  FROM ETLExecutionLogBeta
  WHERE Id = ( SELECT MAX(Id) FROM ETLExecutionLogBeta WHERE ETLProcessTypeId = 1 )  ;
  


END IF;


If (lv_status = 2) THEN		-- means previous ETL process was sucessful

   select BeginDt, EndDt, Id INTO lv_Parameter1, lv_Parameter2, lv_ETL_daterangeId
   from ETLProcessDateRange where ETLProcessTypeId = 1 and status = 0 and ETLObject ='PPP' order by Id limit 1;
 
  INSERT INTO ETLExecutionLogBeta (ETLProcessTypeId, Parameter1, 	Parameter2, 		ETLStartTime, Status)
						VALUES (1, 		 lv_Parameter1, 	lv_Parameter2,	NOW(), 1 );

  SELECT LAST_INSERT_ID() INTO lv_ETLProcessDateRangeId;  
  
	CALL sp_MigrateRevenueIntoKPI(lv_ETLProcessDateRangeId,1,lv_Parameter1, lv_Parameter2,lv_ETL_daterangeId);
	
	UPDATE ETLExecutionLogBeta
	SET ETLCompletedPurchase = NOW()
	WHERE Id = lv_ETLProcessDateRangeId;
	
	CALL sp_MigrateSettledTransactionIntoKPI(lv_ETLProcessDateRangeId,1,lv_Parameter1, lv_Parameter2);
	
	UPDATE ETLExecutionLogBeta
	SET ETLCompletedSettledAmount = NOW()
	WHERE Id = lv_ETLProcessDateRangeId;
	
    CALL sp_MigrateCollectionIntoKPI(lv_ETLProcessDateRangeId, 1,  lv_Parameter1, lv_Parameter2 );
	
	UPDATE ETLExecutionLogBeta
	SET ETLCompletedCollection = NOW()
	WHERE Id = lv_ETLProcessDateRangeId;
	
	UPDATE ETLProcessDateRange set status = 2
	WHERE Id = lv_ETL_daterangeId ;
	
	UPDATE ETLExecutionLogBeta
	SET 
	ETLEndTime = NOW(),
	status = 2
	WHERE ETLProcessTypeId = 1 and Id = lv_ETLProcessDateRangeId;
	
	
end if;
end if;

END $$
DELIMITER ;

-- Start ETL Run for Past Data


