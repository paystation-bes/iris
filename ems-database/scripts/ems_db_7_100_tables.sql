SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- LOOKUP Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `AccessPoint`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AccessPoint` ;

CREATE  TABLE IF NOT EXISTS `AccessPoint` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_accesspoint_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AlertClassType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AlertClassType` ;

CREATE  TABLE IF NOT EXISTS `AlertClassType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_alertclasstype_name_uq` (`Name` ASC) )
ENGINE = InnoDB
COMMENT = 'The classes of alert types: Communication, Collection, and PayStation Alert' ;



-- -----------------------------------------------------
-- Table `AlertType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AlertType` ;

CREATE  TABLE IF NOT EXISTS `AlertType` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `AlertClassTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(50) NOT NULL ,
  `IsUserDefined` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_alerttype_name_uq` (`Name` ASC) ,
CONSTRAINT `fk_alerttype_alertclasstype`
    FOREIGN KEY (`AlertClassTypeId` )
    REFERENCES `AlertClassType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)  
ENGINE = InnoDB
COMMENT = 'DPT provides a standardized set of alert types that customer' /* comment truncated */;

-- -----------------------------------------------------
-- Table `AlertThresholdType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AlertThresholdType` ;

CREATE  TABLE IF NOT EXISTS `AlertThresholdType` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `AlertTypeId` SMALLINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(50) NOT NULL ,  
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_alertthresholdtype_name_uq` (`Name` ASC) ,
  INDEX `idx_alertthresholdtype_alerttype` (`AlertTypeId` ASC) ,
  UNIQUE INDEX `idx_alertthresholdtype_multi_uq` (`AlertTypeId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_alertthresholdtype_alerttype`
    FOREIGN KEY (`AlertTypeId` )
    REFERENCES `AlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB ;


-- -----------------------------------------------------
-- Table `AuthorizationType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuthorizationType` ;

CREATE  TABLE IF NOT EXISTS `AuthorizationType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_authorizationtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CardType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardType` ;

CREATE  TABLE IF NOT EXISTS `CardType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `cardtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Carrier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Carrier` ;

CREATE  TABLE IF NOT EXISTS `Carrier` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_carrier_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ChangeType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ChangeType` ;

CREATE  TABLE IF NOT EXISTS `ChangeType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `changetype_name_uq` (`Name` ASC) ,
  INDEX `idx_changertype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CitationType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CitationType` ;

CREATE  TABLE IF NOT EXISTS `CitationType` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `FlexCitationTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `IsActive` TINYINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `idx_CitationType_name_uq` (`CustomerId`, `FlexCitationTypeId`, `Name` ASC) , 
  CONSTRAINT `fk_CitationType_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CollectionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CollectionType` ;

CREATE  TABLE IF NOT EXISTS `CollectionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `collectiontype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CreditCardType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CreditCardType` ;

CREATE  TABLE IF NOT EXISTS `CreditCardType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `IsValid` TINYINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_creditcardtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerPropertyType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerPropertyType` ;

CREATE  TABLE IF NOT EXISTS `CustomerPropertyType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `customerpropertytype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerStatusType` ;

CREATE  TABLE IF NOT EXISTS `CustomerStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `customerstatustype_name_uq` (`Name` ASC) ,
  INDEX `idx_customerstatustype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerType` ;

CREATE  TABLE IF NOT EXISTS `CustomerType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `customertype_name_uq` (`Name` ASC) ,
  INDEX `idx_customertype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DaylightSaving`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DaylightSaving` ;

CREATE  TABLE IF NOT EXISTS `DaylightSaving` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `BeginDST` DATETIME NOT NULL ,
  `EndDST` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `daylightsaving_begindst_uq` (`BeginDST` ASC) ,
  UNIQUE INDEX `daylightsaving_enddst_uq` (`EndDST` ASC) ,
  UNIQUE INDEX `daylightsaving_id_uq` (`Id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DayOfWeek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DayOfWeek` ;

CREATE  TABLE IF NOT EXISTS `DayOfWeek` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `DayOfWeek` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(10) NOT NULL ,
  `NameAbbrev` VARCHAR(5) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `dayofweek_dayofweek_uq` (`DayOfWeek` ASC) ,
  UNIQUE INDEX `dayofweek_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `dayofweek_nameabbrev_uq` (`NameAbbrev` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DenominationType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DenominationType` ;

CREATE  TABLE IF NOT EXISTS `DenominationType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `IsCoinOrBill` TINYINT UNSIGNED NOT NULL ,
  `DenominationAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_denominationtype_multi_uq` (`IsCoinOrBill` ASC, `DenominationAmount` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMVTransactionData`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EMVTransactionData` ;

CREATE  TABLE IF NOT EXISTS `EMVTransactionData` ( 
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `EMVTransactionDataMappingId` MEDIUMINT UNSIGNED NOT NULL,
  `Value1` VARCHAR(100) NULL,
  `Value2` VARCHAR(100) NULL,
  `Value3` VARCHAR(100) NULL,
  `Value4` VARCHAR(100) NULL,
  `Value5` VARCHAR(100) NULL,
  `Value6` VARCHAR(100) NULL,
  `Value7` VARCHAR(100) NULL,  
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_emvtransactioninfo_id` (`Id`) ,
  INDEX `idx_emvtransactioninfo_processortransaction` (`ProcessorTransactionId`) ,
  CONSTRAINT `fk_emvtransactioninfo_processortransaction`
    FOREIGN KEY (`ProcessorTransactionId` )
    REFERENCES `ProcessorTransaction` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_emvtransactioninfo_emvtransactiondatamapping`
    FOREIGN KEY (`EMVTransactionDataMappingId` )
    REFERENCES `EMVTransactionDataMapping` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	
) ENGINE=InnoDB ;


-- -----------------------------------------------------
-- Table `EMVTransactionDataMapping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EMVTransactionDataMapping` ;

CREATE  TABLE IF NOT EXISTS `EMVTransactionDataMapping` (
   `Id` MEDIUMINT UNSIGNED NOT NULL,
  `ProcessorId` MEDIUMINT UNSIGNED NOT NULL,
  `Label1` VARCHAR(50) NULL,
  `Label2` VARCHAR(50) NULL,
  `Label3` VARCHAR(50) NULL,
  `Label4` VARCHAR(50) NULL,
  `Label5` VARCHAR(50) NULL,
  `Label6` VARCHAR(50) NULL,
  `Label7` VARCHAR(50) NULL,  
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,   
  INDEX `idx_emvtransactiondatamapping_id` (`Id`) ,
  INDEX `idx_emvtransactiondatamapping_processor` (`ProcessorId`) ,  
  CONSTRAINT `fk_emvtransactiondatamapping_processor`
    FOREIGN KEY (`ProcessorId` )
    REFERENCES `Processor` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	
) ENGINE=InnoDB ;


-- -----------------------------------------------------
-- Table `EmsProperties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EmsProperties` ;

CREATE  TABLE IF NOT EXISTS `EmsProperties` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(40) NOT NULL ,
  `Value` VARCHAR(255) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_emsproperties_multi_uq` (`Name` ASC, `Value` ASC) ,
  INDEX `idx_emsproperties_user` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_emsproperties_useraccount` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_emsproperties_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EntityType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EntityType` ;

CREATE  TABLE IF NOT EXISTS `EntityType` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `entitytype_name_uq` (`Name` ASC) ,
  INDEX `idx_entitytype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EventActionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventActionType` ;

CREATE  TABLE IF NOT EXISTS `EventActionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_eventactiontype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EventDefinition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventDefinition` ;

CREATE  TABLE IF NOT EXISTS `EventDefinition` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `EventDeviceTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventActionTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventSeverityTypeId` TINYINT UNSIGNED NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `DescriptionNoSpace` VARCHAR(80) NOT NULL ,
  `EventTypeId` TINYINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_eventdefinition_eventdevicetype` (`EventDeviceTypeId` ASC) ,
  INDEX `idx_eventdefinition_eventstatustype` (`EventStatusTypeId` ASC) ,
  INDEX `idx_eventdefinition_eventactiontype` (`EventActionTypeId` ASC) ,
  INDEX `idx_eventdefinition_eventseveritytype` (`EventSeverityTypeId` ASC) ,
  UNIQUE INDEX `idx_eventdefinition_multi_uq` (`EventDeviceTypeId` ASC, `EventStatusTypeId` ASC, `EventActionTypeId` ASC) ,
  UNIQUE INDEX `idx_eventdefinition_description_uq` (`Description` ASC) ,
  CONSTRAINT `fk_eventdefinition_eventdevicetype`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventdefinition_eventstatustype`
    FOREIGN KEY (`EventStatusTypeId` )
    REFERENCES `EventStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventdefinition_eventactiontype`
    FOREIGN KEY (`EventActionTypeId` )
    REFERENCES `EventActionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventdefinition_eventseveritytype`
    FOREIGN KEY (`EventSeverityTypeId` )
    REFERENCES `EventSeverityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventdefinition_eventtype`
    FOREIGN KEY (`EventTypeId` )
    REFERENCES `EventType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The master list of events that can occur at a point of sale ' /* comment truncated */;


-- -----------------------------------------------------
-- Table `EventDeviceType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventDeviceType` ;

CREATE  TABLE IF NOT EXISTS `EventDeviceType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `NameNoSpace` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_eventdevicetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EventSeverityType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventSeverityType` ;

CREATE  TABLE IF NOT EXISTS `EventSeverityType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_eventseveritytype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EventStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventStatusType` ;

CREATE  TABLE IF NOT EXISTS `EventStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_eventstatustype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExtensibleRateType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ExtensibleRateType` ;

CREATE  TABLE IF NOT EXISTS `ExtensibleRateType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_ratetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FlexLocationProperty` Mapping Table. Maps Iris and Flex Customer and Location
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FlexLocationProperty` ;

CREATE  TABLE IF NOT EXISTS `FlexLocationProperty` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL , 
  `PRO_UID` INT UNSIGNED NOT NULL ,
  `PRO_NAME` VARCHAR(128) NOT NULL ,
  `PRO_CITY` VARCHAR(32) NOT NULL ,  
  `PRO_IS_ACTIVE` TINYINT NOT NULL ,  
  `LocationId` MEDIUMINT UNSIGNED NULL,
  `LastModifiedGMT` datetime NULL ,
  `LastModifiedByUserId`	mediumint unsigned,
  PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_FlexLocationProperty_multi_uq` (`CustomerId`, PRO_UID) ,  
CONSTRAINT `fk_FlexLocationProperty_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_FlexLocationProperty_LocationId`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	    
  )
ENGINE = InnoDB ;

 
-- -----------------------------------------------------
-- Table `FlexLocationFacility` Mapping Table. Maps Iris and Flex Customer and Location
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FlexLocationFacility` ;

CREATE  TABLE IF NOT EXISTS `FlexLocationFacility` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL , 
  `FAC_UID` INT UNSIGNED NOT NULL ,
  `FAC_CODE` VARCHAR(8) NOT NULL ,
  `FAC_DESCRIPTION` VARCHAR(32) NOT NULL ,
  `FAC_IS_ACTIVE` TINYINT NOT NULL , 
  `LocationId` MEDIUMINT UNSIGNED NULL,
  `LastModifiedGMT` datetime NULL ,
  `LastModifiedByUserId`	mediumint unsigned,
 PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_FlexLocationFacility_multi_uq` (`CustomerId`, FAC_UID) ,  
CONSTRAINT `fk_FlexLocationFacility_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_FlexLocationFacility_LocationId`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION		
  )
ENGINE = InnoDB ;

-- -----------------------------------------------------
-- Table `FlexWSUserAccount` 
-- Each Customer URL and Credentials are saved in this Table
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FlexWSUserAccount` ;

CREATE  TABLE IF NOT EXISTS `FlexWSUserAccount` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,  -- Iris CustomerId
  `URL` VARCHAR(75) NOT NULL , 		-- https://staging-houston.t2flex.com/HOUSSA1WS
  `UserName` VARCHAR(75) NOT NULL , -- dptws
  `Password` VARCHAR(75) NOT NULL , -- In plantext pa$$w0rd (need to convert to salt)
  `VERSION` INT unsigned NOT NULL ,
  `LastModifiedGMT` datetime NOT NULL ,
  `LastModifiedByUserId` mediumint unsigned	NOT NULL,
  `TimeZone` varchar(30) NOT NULL,
  `BlockingStatus`  MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_FlexWSUserAccount_CustomerId_uq` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_FlexWSUserAccount_URL_uq` (`URL`) ,
  CONSTRAINT `fk_FlexWSUserAccount_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    )
ENGINE = InnoDB ;


-- -----------------------------------------------------
-- Table `LegacyPaymentType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LegacyPaymentType` ;

CREATE  TABLE IF NOT EXISTS `LegacyPaymentType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_legacypaymenttype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_legacypaymenttype_desc_uq` (`Description` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LegacyTransactionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LegacyTransactionType` ;

CREATE  TABLE IF NOT EXISTS `LegacyTransactionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `IsRevenue` TINYINT(1) UNSIGNED NOT NULL ,
  `IsPurchase` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_legacytransactiontype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_legacytransactiontype_desc_uq` (`Description` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LoginResultType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LoginResultType` ;

CREATE  TABLE IF NOT EXISTS `LoginResultType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `loginresulttype_name_uq` (`Name` ASC) ,
  INDEX `idx_loginresulttype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModemType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModemType` ;

CREATE  TABLE IF NOT EXISTS `ModemType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_modemtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ParkingPermissionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ParkingPermissionType` ;

CREATE  TABLE IF NOT EXISTS `ParkingPermissionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_parkingpermissiontype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PaymentType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaymentType` ;

CREATE  TABLE IF NOT EXISTS `PaymentType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_paymenttype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_paymenttype_desc_uq` (`Description` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PaystationType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaystationType` ;

CREATE  TABLE IF NOT EXISTS `PaystationType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_paystationtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PermitIssueType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PermitIssueType` ;

CREATE  TABLE IF NOT EXISTS `PermitIssueType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_permitissuetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PermitType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PermitType` ;

CREATE  TABLE IF NOT EXISTS `PermitType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_permittype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `POSDateType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSDateType` ;

CREATE  TABLE IF NOT EXISTS `POSDateType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_posdatetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProcessorTransactionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ProcessorTransactionType` ;

CREATE  TABLE IF NOT EXISTS `ProcessorTransactionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `StatusName` VARCHAR(30) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `IsRefund` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `QuarantineType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `QuarantineType` ;

CREATE  TABLE IF NOT EXISTS `QuarantineType` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(40) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_quarantinetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `QuarterHour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `QuarterHour` ;

CREATE  TABLE IF NOT EXISTS `QuarterHour` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `HourOfDay` TINYINT UNSIGNED NOT NULL ,
  `QuarterOfHour` TINYINT UNSIGNED NOT NULL ,
  `TimeAmPm` CHAR(8) NOT NULL ,
  `Time24Hour` CHAR(5) NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestAccountType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestAccountType` ;

CREATE  TABLE IF NOT EXISTS `RestAccountType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_restaccounttype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RevenueType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RevenueType` ;

CREATE  TABLE IF NOT EXISTS `RevenueType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `ParentRevenueTypeId` TINYINT UNSIGNED NULL ,
  `Level` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `revenuetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RouteType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RouteType` ;

CREATE  TABLE IF NOT EXISTS `RouteType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_routetype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_routetype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SubscriptionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SubscriptionType` ;

CREATE  TABLE IF NOT EXISTS `SubscriptionType` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `ParentSubscriptionTypeId` SMALLINT UNSIGNED NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `IsParent` TINYINT(1) UNSIGNED NOT NULL ,
  `IsPrivate` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_subscriptiontype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SmsMessageType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SmsMessageType` ;

CREATE  TABLE IF NOT EXISTS `SmsMessageType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(80) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_smsmessagetype_desc_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Timezone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TimeZone` ;

CREATE  TABLE IF NOT EXISTS `TimeZone` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Code` VARCHAR(5) NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `CodeDST` VARCHAR(5) NOT NULL ,
  `NameDST` VARCHAR(30) NOT NULL ,
  `OffsetFromGMT` CHAR(6) NOT NULL ,
  `OffsetFromDST` CHAR(6) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `timezone_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `timezone_id_uq` (`Id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TransactionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TransactionType` ;

CREATE  TABLE IF NOT EXISTS `TransactionType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `IsRevenue` TINYINT(1) UNSIGNED NOT NULL ,
  `IsPurchase` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_transactiontype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_transactiontype_desc_uq` (`Description` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserStatusType` ;

CREATE  TABLE IF NOT EXISTS `UserStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `userstatustype_name_uq` (`Name` ASC) ,
  INDEX `idx_userstatustype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WebServiceEndPointType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WebServiceEndPointType` ;

CREATE  TABLE IF NOT EXISTS `WebServiceEndPointType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `IsPrivate` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_webserviceendpointtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;



























-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- PRIMARY Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `CustomerAlertEmail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerAlertEmail` ;

CREATE  TABLE IF NOT EXISTS `CustomerAlertEmail` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerAlertTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerEmailId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  INDEX `idx_CustomerAlertEmail_CustomerEmail` (`CustomerEmailId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_CustomerAlertEmail_CustomerAlertType` (`CustomerAlertTypeId` ASC) ,
  UNIQUE INDEX `idx_CustomerAlertEmail_Multi_Uq` (`CustomerAlertTypeId` ASC, `CustomerEmailId` ASC, `IsDeleted` ASC) ,
  CONSTRAINT `fk_CustomerAlertEmail_CustomerEmail`
    FOREIGN KEY (`CustomerEmailId` )
    REFERENCES `CustomerEmail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerAlertEmail_CustomerAlertType`
    FOREIGN KEY (`CustomerAlertTypeId` )
    REFERENCES `CustomerAlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The list of recipients, as identified by their email address' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CollectionLock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CollectionLock` ;

CREATE  TABLE IF NOT EXISTS `CollectionLock` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ClusterId` SMALLINT UNSIGNED NOT NULL ,
  `LockGMT` DATETIME NOT NULL ,
  `BatchSize` SMALLINT UNSIGNED NOT NULL ,
  `LockableCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `LockedCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RecalcedCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RecalcBeginGMT` DATETIME NULL ,
  `RecalcEndGMT` DATETIME NULL ,
  `RecalcLagSeconds` SMALLINT UNSIGNED NULL ,
  `RecalcsPerSecond` DECIMAL(7,2) UNSIGNED NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_collectionlock_multi_1` (`LockGMT` ASC, `ClusterId` ASC) ,
  INDEX `idx_collectionlock_cluster` (`ClusterId` ASC) ,
  INDEX `idx_collectionlock_multi_2` (`ClusterId` ASC, `LockGMT` ASC) ,
  CONSTRAINT `fk_collectionlock_cluster`
    FOREIGN KEY (`ClusterId` )
    REFERENCES `Cluster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Used to assign a lock to a group of point of sale(s) (i.e., ' /* comment truncated */;

-- -----------------------------------------------------
-- Table `Coupon`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Coupon` ;

CREATE  TABLE IF NOT EXISTS `Coupon` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ConsumerId` MEDIUMINT UNSIGNED NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `Coupon` VARCHAR(10) NOT NULL ,
  `PercentDiscount` TINYINT UNSIGNED NOT NULL ,
  `DollarDiscountAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `StartDateLocal` DATETIME NOT NULL ,
  `EndDateLocal` DATETIME NOT NULL ,
  `MaxNumberOfUses` SMALLINT UNSIGNED NULL COMMENT 'MaxNumberOfUses=NULL means unlimited number of uses' ,
  `NumberOfUsesRemaining` SMALLINT UNSIGNED NULL COMMENT 'NumberOfUsesRemaining=NULL if MaxNumberOfUses=NULL' ,
  `SpaceRange` VARCHAR(40) NULL ,
  `Description` VARCHAR(80) NULL ,
  `ValidForNumOfDay` SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Single Use Coupon, 1 = 1 Day, 0 = N/A',
  `SingleUseDate` DATETIME NULL,
  `ArchiveDate` DATETIME NULL,
  `IsPndEnabled` TINYINT(1) UNSIGNED NOT NULL ,
  `IsPbsEnabled` TINYINT(1) UNSIGNED NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `IsOffline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_coupon_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_coupon_multi_uq` (`CustomerId` ASC, `Coupon` ASC, `IsOffline` ASC, `ArchiveDate` ASC) ,
  INDEX `idx_coupon_startend` (`CustomerId` ASC, `StartDateLocal` ASC, `EndDateLocal` ASC) ,
  INDEX `idx_coupon_location` (`LocationId` ASC) ,
  CONSTRAINT `fk_coupon_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_coupon_consumerid 
	FOREIGN KEY (`ConsumerId` )
    REFERENCES `Consumer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Customer` ;

CREATE  TABLE IF NOT EXISTS `Customer` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `ParentCustomerId` MEDIUMINT UNSIGNED NULL ,
  `TimezoneId` INT UNSIGNED NOT NULL DEFAULT 473 ,
  `Name` VARCHAR(25) NOT NULL ,
  `TrialExpiryGMT` DATETIME NULL ,
  `IsParent` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsMigrated` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 , 
  `MigratedDate` DATETIME NULL , 
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customer_name_uq` (`Name` ASC) ,
  INDEX `idx_customer_self` (`ParentCustomerId` ASC) ,
  UNIQUE INDEX `idx_customer_multi_uq` (`ParentCustomerId` ASC, `Name` ASC) ,
  INDEX `idx_customer_customertype` (`CustomerTypeId` ASC) ,
  INDEX `idx_customer_customerstatustype` (`CustomerStatusTypeId` ASC) ,
  INDEX `idx_customer_by_customertype` (`CustomerTypeId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_customer_self`
    FOREIGN KEY (`ParentCustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_customertype`
    FOREIGN KEY (`CustomerTypeId` )
    REFERENCES `CustomerType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_customerstatustype`
    FOREIGN KEY (`CustomerStatusTypeId` )
    REFERENCES `CustomerStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerAgreement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerAgreement` ;

CREATE  TABLE IF NOT EXISTS `CustomerAgreement` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ServiceAgreementId` SMALLINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL DEFAULT '' ,
  `Title` VARCHAR(30) NOT NULL DEFAULT '' ,
  `Organization` VARCHAR(255) NOT NULL DEFAULT '' ,
  `AgreedGMT` DATETIME NOT NULL ,
  `IsOverride` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customeragreement_customer` (`CustomerId` ASC) ,
  INDEX `idx_customeragreement_serviceagreement` (`ServiceAgreementId` ASC) ,
  UNIQUE INDEX `customeragreement_multi_uq` (`CustomerId` ASC, `ServiceAgreementId` ASC, `AgreedGMT` ASC) ,
  INDEX `idx_customeragreement_user` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_customeragreement_useraccount` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_customeragreement_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customeragreement_serviceagreement`
    FOREIGN KEY (`ServiceAgreementId` )
    REFERENCES `ServiceAgreement` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customeragreement_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerAlertType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerAlertType` ;

CREATE  TABLE IF NOT EXISTS `CustomerAlertType` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `AlertThresholdTypeId` SMALLINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `RouteId` MEDIUMINT UNSIGNED NULL COMMENT 'RouteId=NULL means All Paystations.' ,
  `Name` VARCHAR(30) NOT NULL ,
  `Threshold` FLOAT UNSIGNED NOT NULL DEFAULT 1 ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsDefault` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_CustomerAlertType_Multi_1` (`CustomerId` ASC, `AlertThresholdTypeId` ASC, `Name` ASC) ,
  INDEX `idx_CustomerAlertType_AlertThresholdType` (`AlertThresholdTypeId` ASC) ,
  INDEX `idx_CustomerAlertType_Customer` (`CustomerId` ASC) ,
  INDEX `idx_CustomerAlertType_Location` (`LocationId` ASC) ,
  INDEX `idx_CustomerAlertType_Route` (`RouteId` ASC) ,
  INDEX `idx_CustomerAlertType_Multi_2` (`AlertThresholdTypeId` ASC, `CustomerId` ASC, `Threshold` ASC) ,
  CONSTRAINT `fk_CustomerAlertType_AlertThresholdType`
    FOREIGN KEY (`AlertThresholdTypeId` )
    REFERENCES `AlertThresholdType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerAlertType_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerAlertType_Location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerAlertType_Route`
    FOREIGN KEY (`RouteId` )
    REFERENCES `Route` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The definition of an alert type as specified by a (user for ' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CustomerBadCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerBadCard` ;

CREATE  TABLE IF NOT EXISTS `CustomerBadCard` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerCardTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `CardNumberOrHash` VARCHAR(40) NOT NULL ,
  `Last4DigitsOfCardNumber` SMALLINT UNSIGNED NULL ,
  `CardData` VARCHAR(60) NULL ,
  `CardExpiry` SMALLINT UNSIGNED NULL ,
  `Source` VARCHAR(10) NULL ,
  `Comment` VARCHAR(80) NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `AddedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customerbadcard_customercardtype` (`CustomerCardTypeId` ASC) ,
  INDEX `idx_customerbadcard_multi_1` (`CustomerCardTypeId` ASC, `CardNumberOrHash` ASC, `Last4DigitsOfCardNumber` ASC) ,
  INDEX `idx_customerbadcard_multi_2` (`CustomerCardTypeId` ASC, `Last4DigitsOfCardNumber` ASC) ,
  CONSTRAINT `fk_customerbadcard_customercardtype`
    FOREIGN KEY (`CustomerCardTypeId` )
    REFERENCES `CustomerCardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerCard` ;

CREATE  TABLE IF NOT EXISTS `CustomerCard` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ConsumerId` MEDIUMINT UNSIGNED NULL ,
  `CustomerCardTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NULL ,
  `CardNumber` VARCHAR(40) NOT NULL ,
  `GracePeriodMinutes` SMALLINT UNSIGNED NULL DEFAULT 0 ,
  `CardBeginGMT` DATETIME NULL  ,
  `CardExpireGMT` DATETIME NULL  ,
  `NumberOfUses` SMALLINT UNSIGNED NULL DEFAULT 0 ,
  `MaxNumberOfUses` SMALLINT UNSIGNED NULL DEFAULT 0 ,
  `Comment` VARCHAR(80) NULL ,
  `IsForNonOverlappingUse` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `ArchiveDate` DATETIME NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL default 0 ,  
  `VERSION` INT UNSIGNED NOT NULL ,
  `AddedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customercard_multi_uq` (`CustomerCardTypeId` ASC, `CardNumber` ASC, `IsDeleted`, `ArchiveDate` ) ,
  INDEX `idx_customercard_customercardtype` (`CustomerCardTypeId` ASC) ,
  INDEX `idx_customercard_location` (`LocationId` ASC) ,
  INDEX `idx_customercard_pointofsale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_customercard_customercardtype`
    FOREIGN KEY (`CustomerCardTypeId` )
    REFERENCES `CustomerCardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customercard_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customercard_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT fk_customercard_consumerid 
	FOREIGN KEY (`ConsumerId` )
    REFERENCES `Consumer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	)
ENGINE = InnoDB
COMMENT = 'Table CustomerCard maintains the valid cards for a customer.' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CustomerCardType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerCardType` ;

CREATE  TABLE IF NOT EXISTS `CustomerCardType` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CardTypeId` TINYINT UNSIGNED NOT NULL ,
  `ParentCustomerCardTypeId` MEDIUMINT UNSIGNED NULL DEFAULT NULL ,  
  `AuthorizationTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Track2Pattern` VARCHAR(40) NOT NULL DEFAULT '' ,
  `Description` VARCHAR(80) NULL ,
  `IsDigitAlgorithm` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsLocked` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `ArchiveDate` DATETIME NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL default 0 ,  
  `IsPrimary` TINYINT(1) UNSIGNED NOT NULL default 0 ,  
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customercardtype_customer` (`CustomerId` ASC) ,
  INDEX `idx_customercardtype_authorizationtype` (`AuthorizationTypeId` ASC) ,
  INDEX `idx_customercardtype_cardtype` (`CardTypeId` ASC) ,
  INDEX `idx_customercardtype_multi` (`CustomerId` ASC, `CardTypeId` ASC, `Name` ASC) ,
  INDEX `idx_CustomerCardType_self` (`ParentCustomerCardTypeId` ASC) ,
  UNIQUE INDEX `idx_customercardtype_multi_uq` (`CustomerId` ASC, `CardTypeId` ASC, `Track2Pattern` ASC, `IsDeleted`, `ArchiveDate` ) , 
  CONSTRAINT `fk_customercardtype_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customercardtype_authorizationtype`
    FOREIGN KEY (`AuthorizationTypeId` )
    REFERENCES `AuthorizationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customercardtype_cardtype`
    FOREIGN KEY (`CardTypeId` )
    REFERENCES `CardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customercardtype_self`
    FOREIGN KEY (`ParentCustomerCardTypeId` )
    REFERENCES `CustomerCardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Within EMS there are different types of cards used for payme' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CustomerEmail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerEmail` ;

CREATE  TABLE IF NOT EXISTS `CustomerEmail` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Email` VARCHAR(340) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customeremail_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_customeremail_multi_uq` (`CustomerId` ASC, `Email` ASC) ,
  CONSTRAINT `fk_customeremail_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The master list of email address for each Customer. Each ema' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CustomerGMTDST`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerGMTDST` ;

CREATE  TABLE IF NOT EXISTS `CustomerGMTDST` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `GMTtoLocal` CHAR(6) NOT NULL ,
  `DSTtoLocal` CHAR(6) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `daylightsaving_begindst_uq` (`GMTtoLocal` ASC) ,
  UNIQUE INDEX `daylightsaving_enddst_uq` (`DSTtoLocal` ASC) ,
  UNIQUE INDEX `daylightsaving_id_uq` (`Id` ASC) ,
  INDEX `daylightsaving_customerid` (`CustomerId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerProperty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerProperty` ;

CREATE  TABLE IF NOT EXISTS `CustomerProperty` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerPropertyTypeId` TINYINT UNSIGNED NOT NULL ,
  `PropertyValue` VARCHAR(40) NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customerproperty_customerpropertytype` (`CustomerPropertyTypeId` ASC) ,
  INDEX `idx_customerproperty_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `customerproperty_multi_uq` (`CustomerId` ASC, `CustomerPropertyTypeId` ASC) ,
  CONSTRAINT `fk_customerproperty_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customerproperty_customerpropertytype`
    FOREIGN KEY (`CustomerPropertyTypeId` )
    REFERENCES `CustomerPropertyType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerRole`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerRole` ;

CREATE  TABLE IF NOT EXISTS `CustomerRole` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `RoleId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `customerrole_multi_uq` (`CustomerId` ASC, `RoleId` ASC) ,
  INDEX `idx_customerrole_customer` (`CustomerId` ASC) ,
  INDEX `idx_customerrole_role` (`RoleId` ASC) ,
  INDEX `idx_customerrole_user` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_customerrole_useraccount` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_customerrole_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customerrole_role`
    FOREIGN KEY (`RoleId` )
    REFERENCES `Role` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customerrole_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerSubscription`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerSubscription` ;

CREATE  TABLE IF NOT EXISTS `CustomerSubscription` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `SubscriptionTypeId` SMALLINT UNSIGNED NOT NULL ,
  `IsEnabled` TINYINT(1) UNSIGNED NOT NULL ,
  `LicenseCount` MEDIUMINT UNSIGNED,
  `LicenseUsed` MEDIUMINT UNSIGNED,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_customersubscription_multi_uq` (`CustomerId` ASC, `SubscriptionTypeId` ASC) ,
  INDEX `idx_customersubscription_customer` (`CustomerId` ASC) ,
  INDEX `idx_customersubscription_subscriptiontype` (`SubscriptionTypeId` ASC) ,
  CONSTRAINT `fk_customersubscription_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customersubscription_subscriptiontype`
    FOREIGN KEY (`SubscriptionTypeId` )
    REFERENCES `SubscriptionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExtensiblePermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ExtensiblePermit` ;

CREATE  TABLE IF NOT EXISTS `ExtensiblePermit` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `OriginalPermitId` BIGINT UNSIGNED NOT NULL ,
  `MobileNumberId` INT UNSIGNED NOT NULL ,
  `CardData` VARCHAR(100) NULL ,
  `Last4Digit` SMALLINT(5) UNSIGNED NOT NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  INDEX `idx_extensiblepermit_expirygmt` (`PermitExpireGMT` ASC) ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_extensiblepermit_mobilenumber` (`MobileNumberId` ASC) ,
  UNIQUE INDEX `idx_extensiblepermit_permit` (`OriginalPermitId` ASC) ,
  CONSTRAINT `fk_extensiblepermit_mobilenumber`
    FOREIGN KEY (`MobileNumberId` )
    REFERENCES `MobileNumber` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_extensiblepermit_permit`
    FOREIGN KEY (`OriginalPermitId` )
    REFERENCES `Permit` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExtensibleRate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ExtensibleRate` ;

CREATE  TABLE IF NOT EXISTS `ExtensibleRate` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `ExtensibleRateTypeId` TINYINT UNSIGNED NOT NULL ,
  `SpecialRateDateId` MEDIUMINT UNSIGNED NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `BeginHourLocal` TINYINT UNSIGNED NOT NULL ,
  `BeginMinuteLocal` TINYINT UNSIGNED NOT NULL ,
  `EndHourLocal` TINYINT UNSIGNED NOT NULL ,
  `EndMinuteLocal` TINYINT UNSIGNED NOT NULL ,
  `RateAmount` SMALLINT UNSIGNED NOT NULL ,
  `ServiceFeeAmount` SMALLINT UNSIGNED NOT NULL ,
  `MinExtensionMinutes` SMALLINT UNSIGNED NOT NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `CreatedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_extensiblerate_begintime` (`LocationId` ASC, `BeginHourLocal` ASC, `BeginMinuteLocal` ASC) ,
  INDEX `idx_extensiblerate_endtime` (`LocationId` ASC, `EndHourLocal` ASC, `EndMinuteLocal` ASC) ,
  INDEX `idx_extensiblerate_location` (`LocationId` ASC) ,
  INDEX `idx_extensiblerate_extensibleratetype` (`ExtensibleRateTypeId` ASC) ,
  INDEX `idx_extensiblerate_specialratedate` (`SpecialRateDateId` ASC) ,
  INDEX `idx_extensiblerate_unifiedrate` (`UnifiedRateId` ASC) ,
  INDEX `idx_extensiblerate_multi` (`LocationId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_extensiblerate_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_extensiblerate_extensibleratetype`
    FOREIGN KEY (`ExtensibleRateTypeId` )
    REFERENCES `ExtensibleRateType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_extensiblerate_specialratedate`
    FOREIGN KEY (`SpecialRateDateId` )
    REFERENCES `SpecialRateDate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_extensiblerate_unifiedrate`
    FOREIGN KEY (`UnifiedRateId` )
    REFERENCES `UnifiedRate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExtensibleRateDayOfWeek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ExtensibleRateDayOfWeek` ;

CREATE  TABLE IF NOT EXISTS `ExtensibleRateDayOfWeek` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ExtensibleRateId` BIGINT UNSIGNED NOT NULL ,
  `DayOfWeekId` TINYINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_extensibleratedayofweek_extensiblerate` (`ExtensibleRateId` ASC) ,
  INDEX `idx_extensibleratedayofweek_dayofweek` (`DayOfWeekId` ASC) ,
  UNIQUE INDEX `idx_extensibleratedayofweek_multi_uq` (`ExtensibleRateId` ASC, `DayOfWeekId` ASC) ,
  CONSTRAINT `fk_extensibleratedayofweek_extensiblerate`
    FOREIGN KEY (`ExtensibleRateId` )
    REFERENCES `ExtensibleRate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_extensibleratedayofweek_dayofweek`
    FOREIGN KEY (`DayOfWeekId` )
    REFERENCES `DayOfWeek` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LicencePlate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LicencePlate` ;

CREATE  TABLE IF NOT EXISTS `LicencePlate` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Number` VARCHAR(15) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_licenceplate_number_uq` (`Number` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Location`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Location` ;

CREATE  TABLE IF NOT EXISTS `Location` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ParentLocationId` MEDIUMINT UNSIGNED NULL DEFAULT NULL ,
  `PermitIssueTypeId` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Name` VARCHAR(25) NOT NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NOT NULL ,
  `TargetMonthlyRevenueAmount` INT UNSIGNED NOT NULL ,
  `Description` VARCHAR(80) NULL ,
  `IsParent` TINYINT(1) UNSIGNED DEFAULT NULL ,
  `IsUnassigned` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_location_customer` (`CustomerId` ASC) ,
  INDEX `idx_location_multi_1` (`CustomerId` ASC, `Name` ASC) ,
  INDEX `idx_location_multi_2` (`CustomerId` ASC, `ParentLocationId` ASC, `Name` ASC) ,
  INDEX `idx_location_self` (`ParentLocationId` ASC) ,
  CONSTRAINT `fk_location_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_self`
    FOREIGN KEY (`ParentLocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Location_PermitIssueType`
    FOREIGN KEY (`PermitIssueTypeId` )
    REFERENCES `PermitIssueType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'A Location is an important concent within EMS. Table Locatio' /* comment truncated */;


-- -----------------------------------------------------
-- Table `LocationDay`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LocationDay` ;

CREATE  TABLE IF NOT EXISTS `LocationDay` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `DayOfWeekId` TINYINT UNSIGNED NOT NULL ,
  `OpenQuarterHourNumber` TINYINT UNSIGNED NOT NULL ,
  `CloseQuarterHourNumber` TINYINT UNSIGNED NOT NULL ,
  `IsOpen24Hours` TINYINT(1) UNSIGNED NOT NULL ,
  `IsClosed` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_locationday_dayofweek` (`DayOfWeekId` ASC) ,
  UNIQUE INDEX `idx_locationday_multi_uq` (`LocationId` ASC, `DayOfWeekId` ASC) ,
  INDEX `idx_locationday_location` (`LocationId` ASC) ,
  CONSTRAINT `fk_locationday_dayofweek`
    FOREIGN KEY (`DayOfWeekId` )
    REFERENCES `DayOfWeek` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locationday_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LocationGeopoint`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LocationGeopoint` ;

CREATE  TABLE IF NOT EXISTS `LocationGeopoint` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `Latitude` DECIMAL(18,14) NOT NULL ,
  `Longitude` DECIMAL(18,14) NOT NULL ,
  `AltitudeOrLevel` MEDIUMINT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_locationgeopoint_location` (`LocationId` ASC) ,
  UNIQUE INDEX `idx_locationgeopoint_multi_uq1` (`LocationId` ASC, `Latitude` ASC, `Longitude` ASC, `AltitudeOrLevel` ASC) ,
  UNIQUE INDEX `idx_locationgeopoint_multi_uq2` (`LocationId` ASC, `Id` ASC) ,
  CONSTRAINT `fk_locationgeopoint_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Table LocationGeopoint is not used for EMS 7.0. This table m' /* comment truncated */;


-- -----------------------------------------------------
-- Table `LocationOpen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LocationOpen` ;

CREATE  TABLE IF NOT EXISTS `LocationOpen` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `DayOfWeekId` TINYINT UNSIGNED NOT NULL ,
  `QuarterHourId` TINYINT UNSIGNED NOT NULL ,
  `IsOpen` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_locationopen_location` (`LocationId` ASC) ,
  INDEX `idx_locationopen_dayofweek` (`DayOfWeekId` ASC) ,
  INDEX `idx_locationopen_quarterhour` (`QuarterHourId` ASC) ,
  UNIQUE INDEX `locationopen_multi_uq` (`LocationId` ASC, `DayOfWeekId` ASC, `QuarterHourId` ASC) ,
  CONSTRAINT `fk_locationopen_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locationopen_dayofweek`
    FOREIGN KEY (`DayOfWeekId` )
    REFERENCES `DayOfWeek` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locationopen_quarterhour`
    FOREIGN KEY (`QuarterHourId` )
    REFERENCES `QuarterHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MerchantAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MerchantAccount` ;

CREATE  TABLE IF NOT EXISTS `MerchantAccount` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ProcessorId` MEDIUMINT UNSIGNED NOT NULL ,
  `MerchantStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NULL ,
  `Field1` VARCHAR(100) NULL ,
  `Field2` VARCHAR(100) NULL ,
  `Field3` VARCHAR(100) NULL ,
  `Field4` VARCHAR(100) NULL ,
  `Field5` VARCHAR(100) NULL ,
  `Field6` VARCHAR(100) NULL ,
  `CloseQuarterOfDay` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `ReferenceCounter` BIGINT UNSIGNED NOT NULL ,  
  `IsValidated` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsValidated must be set to zero when the MerchantAccoutn is Disabled or Delelted (see table MerchantStatusType).' ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_merchantaccount_processor` (`ProcessorId` ASC) ,
  INDEX `idx_merchantaccount_customer` (`CustomerId` ASC) ,
  INDEX `idx_merchantaccount_name` (`CustomerId` ASC, `Name` ASC, `IsValidated` ASC) ,
  INDEX `idx_merchantaccount_merchantstatustype` (`MerchantStatusTypeId` ASC) ,
  CONSTRAINT `fk_merchantaccount_processor`
    FOREIGN KEY (`ProcessorId` )
    REFERENCES `Processor` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_merchantaccount_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_merchantaccount_merchantstatustype`
    FOREIGN KEY (`MerchantStatusTypeId` )
    REFERENCES `MerchantStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MerchantPOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MerchantPOS` ;

CREATE  TABLE IF NOT EXISTS `MerchantPOS` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `CardTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_merchantpos_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_merchantpos_merchantaccount` (`MerchantAccountId` ASC) ,
  INDEX `idx_merchantpos_cardtype` (`CardTypeId` ASC) ,
  UNIQUE INDEX `idx_merchantpos_multi_uq` (`PointOfSaleId` ASC, `MerchantAccountId` ASC, `CardTypeId` ASC) ,
  CONSTRAINT `fk_merchantpos_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_merchantpos_merchantaccount`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_merchantpos_cardtype`
    FOREIGN KEY (`CardTypeId` )
    REFERENCES `CardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MobileNumber`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MobileNumber` ;

CREATE  TABLE IF NOT EXISTS `MobileNumber` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Number` VARCHAR(15) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_mobilenumber_number_uq` (`Number` ASC) )
ENGINE = InnoDB; 


-- -----------------------------------------------------
-- Table `ModemSetting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModemSetting` ;

CREATE  TABLE IF NOT EXISTS `ModemSetting` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CarrierId` MEDIUMINT UNSIGNED NULL ,
  `AccessPointId` TINYINT UNSIGNED NULL ,
  `ModemTypeId` TINYINT UNSIGNED NULL ,
  `CCID` VARCHAR(40) NULL ,
  `MEID` VARCHAR(14) NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_modemsetting_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_modemsetting_carrier` (`CarrierId` ASC) ,
  INDEX `idx_modemsetting_accesspoint` (`AccessPointId` ASC) ,
  INDEX `idx_modemsetting_modemtype` (`ModemTypeId` ASC) ,
  CONSTRAINT `fk_modemsetting_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_modemsetting_carrier`
    FOREIGN KEY (`CarrierId` )
    REFERENCES `Carrier` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_modemsetting_accesspoint`
    FOREIGN KEY (`AccessPointId` )
    REFERENCES `AccessPoint` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_modemsetting_modemtype`
    FOREIGN KEY (`ModemTypeId` )
    REFERENCES `ModemType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Notification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Notification` ;

CREATE  TABLE IF NOT EXISTS `Notification` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Title` VARCHAR(50) NOT NULL ,
  `Message` VARCHAR(512) NOT NULL ,
  `MessageURL` VARCHAR(512) NULL ,
  `BeginGMT` DATETIME NULL ,
  `EndGMT` DATETIME NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_notification_begingmt` (`BeginGMT` ASC) ,
  INDEX `idx_notification_endgmt` (`EndGMT` ASC) ,
  INDEX `idx_notification_useraccount` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_begingmt_endgmt` (`BeginGMT` ASC, `EndGMT` ASC) ,
  INDEX `idx_notification_title` (`Title` ASC) ,
  CONSTRAINT `fk_notification_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ParkingPermission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ParkingPermission` ;

CREATE  TABLE IF NOT EXISTS `ParkingPermission` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `ParkingPermissionTypeId` TINYINT UNSIGNED NOT NULL ,
  `SpecialPermissionDateId` MEDIUMINT UNSIGNED NULL ,
  `Name` VARCHAR(16) NOT NULL ,
  `BeginHourLocal` TINYINT UNSIGNED NOT NULL ,
  `BeginMinuteLocal` TINYINT UNSIGNED NOT NULL ,
  `EndHourLocal` TINYINT UNSIGNED NOT NULL ,
  `EndMinuteLocal` TINYINT UNSIGNED NOT NULL ,
  `MaxDurationMinutes` MEDIUMINT UNSIGNED NOT NULL ,
  `IsLimitedOrUnlimited` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Only applies to Restricted or Special parking permission types' ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_parkingpermission_begintime` (`LocationId` ASC, `BeginHourLocal` ASC, `BeginMinuteLocal` ASC) ,
  INDEX `idx_parkingpermission_endtime` (`LocationId` ASC, `EndHourLocal` ASC, `EndMinuteLocal` ASC) ,
  INDEX `idx_parkingpermission_location` (`LocationId` ASC) ,
  INDEX `idx_parkingpermission_parkingpermissiontype` (`ParkingPermissionTypeId` ASC) ,
  INDEX `idx_parkingpermission_specialpermissiondate` (`SpecialPermissionDateId` ASC) ,
  INDEX `idx_parkingpermission_multi` (`LocationId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_parkingpermission_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parkingpermission_parkingpermissiontype`
    FOREIGN KEY (`ParkingPermissionTypeId` )
    REFERENCES `ParkingPermissionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parkingpermission_specialpermissiondate`
    FOREIGN KEY (`SpecialPermissionDateId` )
    REFERENCES `SpecialPermissionDate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ParkingPermissionDayOfWeek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ParkingPermissionDayOfWeek` ;

CREATE  TABLE IF NOT EXISTS `ParkingPermissionDayOfWeek` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ParkingPermissionId` INT UNSIGNED NOT NULL ,
  `DayOfWeekId` TINYINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_parkingpermissiondayofweek_parkingpermission` (`ParkingPermissionId` ASC) ,
  INDEX `idx_parkingpermissiondayofweek_dayofweek` (`DayOfWeekId` ASC) ,
  UNIQUE INDEX `idx_parkingpermissiondayofweek_multi_uq` (`ParkingPermissionId` ASC, `DayOfWeekId` ASC) ,
  CONSTRAINT `fk_parkingpermissiondayofweek_parkingpermission`
    FOREIGN KEY (`ParkingPermissionId` )
    REFERENCES `ParkingPermission` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parkingpermissiondayofweek_dayofweek`
    FOREIGN KEY (`DayOfWeekId` )
    REFERENCES `DayOfWeek` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PaymentCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaymentCard` ;

CREATE  TABLE IF NOT EXISTS `PaymentCard` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `CardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CreditCardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerCardId` MEDIUMINT UNSIGNED NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NULL ,
  `ProcessorTransactionId` BIGINT UNSIGNED NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardLast4Digits` SMALLINT UNSIGNED NOT NULL ,
  `CardProcessedGMT` DATETIME NOT NULL ,
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL ,
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL ,
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL ,
  INDEX `idx_paymentcard_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_paymentcard_creditcardtype` (`CreditCardTypeId` ASC) ,
  INDEX `idx_paymentcard_last4digits` (`CardLast4Digits` ASC, `CreditCardTypeId` ASC, `PurchaseId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_paymentcard_cardtype` (`CardTypeId` ASC) ,
  INDEX `idx_paymentcard_customercard` (`CustomerCardId` ASC) ,
  INDEX `idx_paymentcard_processortransaction` (`ProcessorTransactionId` ASC) ,
  INDEX `idx_paymentcard_processortransactiontype` (`ProcessorTransactionTypeId` ASC) ,
  INDEX `idx_paymentcard_merchantaccount` (`MerchantAccountId` ASC) ,
  CONSTRAINT `fk_paymentcard_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_creditcardtype`
    FOREIGN KEY (`CreditCardTypeId` )
    REFERENCES `CreditCardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_cardtype`
    FOREIGN KEY (`CardTypeId` )
    REFERENCES `CardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_customercard`
    FOREIGN KEY (`CustomerCardId` )
    REFERENCES `CustomerCard` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_processortransaction`
    FOREIGN KEY (`ProcessorTransactionId` )
    REFERENCES `ProcessorTransaction` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_processortransactiontype`
    FOREIGN KEY (`ProcessorTransactionTypeId` )
    REFERENCES `ProcessorTransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcard_merchantaccount`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PaymentCash`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaymentCash` ;

CREATE  TABLE IF NOT EXISTS `PaymentCash` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `IsAcceptOrDispense` TINYINT UNSIGNED NOT NULL ,
  `DenominationTypeId` TINYINT UNSIGNED NOT NULL ,
  `Quantity` TINYINT UNSIGNED NOT NULL ,
  INDEX `idx_paymentcash_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_paymentcash_denominationtype` (`DenominationTypeId` ASC) ,
  UNIQUE INDEX `idx_paymentcash_multi_uq` (`PurchaseId` ASC, `IsAcceptOrDispense` ASC, `DenominationTypeId` ASC) ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_paymentcash_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paymentcash_denominationtype`
    FOREIGN KEY (`DenominationTypeId` )
    REFERENCES `DenominationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Paystation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Paystation` ;

CREATE  TABLE IF NOT EXISTS `Paystation` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PaystationTypeId` TINYINT UNSIGNED NOT NULL ,
  `ModemTypeId` TINYINT UNSIGNED NOT NULL ,
  `SerialNumber` VARCHAR(20) NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_paystation_serialnumber_uq` (`SerialNumber` ASC) ,
  INDEX `idx_paystation_paystationtype` (`PaystationTypeId` ASC) ,
  INDEX `idx_paystation_modemtype` (`ModemTypeId` ASC) ,
  CONSTRAINT `fk_paystation_paystationtype`
    FOREIGN KEY (`PaystationTypeId` )
    REFERENCES `PaystationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_paystation_modemtype`
    FOREIGN KEY (`ModemTypeId` )
    REFERENCES `ModemType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Permission` ;

CREATE  TABLE IF NOT EXISTS `Permission` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `ParentPermissionId` SMALLINT UNSIGNED NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `Description` VARCHAR(255) NULL ,
  `IsParent` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_permission_self` (`ParentPermissionId` ASC) ,
  UNIQUE INDEX `permission_multi_uq` (`ParentPermissionId` ASC, `Name` ASC) ,
  INDEX `idx_permission_useraccount` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_permission_self`
    FOREIGN KEY (`ParentPermissionId` )
    REFERENCES `Permission` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Permit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Permit` ;

CREATE  TABLE IF NOT EXISTS `Permit` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `PermitNumber` INT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitTypeId` TINYINT UNSIGNED NOT NULL ,
  `PermitIssueTypeId` TINYINT UNSIGNED NOT NULL ,
  `LicencePlateId` INT UNSIGNED NULL ,
  `MobileNumberId` INT UNSIGNED NULL ,
  `OriginalPermitId` BIGINT UNSIGNED NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `AddTimeNumber` INT UNSIGNED NOT NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitOriginalExpireGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `NumberOfExtensions` TINYINT UNSIGNED NOT NULL ,
  UNIQUE INDEX `idx_permit_purchase_unq` (`PurchaseId` ASC) ,
  INDEX `idx_permit_location` (`LocationId` ASC) ,
  INDEX `idx_permit_licenceplate` (`LicencePlateId` ASC) ,
  INDEX `idx_permit_mobilenumber` (`MobileNumberId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_permit_addtimenumber` (`AddTimeNumber` ASC) ,
  INDEX `idx_permit_permittype` (`PermitTypeId` ASC) ,
  INDEX `idx_permit_permitissuetype` (`PermitIssueTypeId` ASC) ,
  INDEX `idx_permit_self` (`OriginalPermitId` ASC) ,
  INDEX `ind_Permit_PermitExpireGMT`(`PermitExpireGMT`) ,
  CONSTRAINT `fk_permit_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_licenceplate`
    FOREIGN KEY (`LicencePlateId` )
    REFERENCES `LicencePlate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_mobilenumber`
    FOREIGN KEY (`MobileNumberId` )
    REFERENCES `MobileNumber` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_permittype`
    FOREIGN KEY (`PermitTypeId` )
    REFERENCES `PermitType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_permitissuetype`
    FOREIGN KEY (`PermitIssueTypeId` )
    REFERENCES `PermitIssueType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permit_self`
    FOREIGN KEY (`OriginalPermitId` )
    REFERENCES `Permit` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PointOfSale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PointOfSale` ;

CREATE  TABLE IF NOT EXISTS `PointOfSale` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationId` MEDIUMINT UNSIGNED NOT NULL ,
  `SettingsFileId` MEDIUMINT UNSIGNED NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `SerialNumber` VARCHAR(20) NOT NULL ,
  `ProvisionedGMT` DATETIME NOT NULL ,
  `Latitude` DECIMAL(18,14) NULL ,
  `Longitude` DECIMAL(18,14) NULL ,
  `AltitudeOrLevel` MEDIUMINT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  INDEX `idx_pointofsale_customer` (`CustomerId` ASC) ,
  INDEX `idx_pointofsale_paystation` (`PaystationId` ASC) ,
  UNIQUE INDEX `idx_pointofsale_paystation_uq` (`CustomerId` ASC, `PaystationId` ASC, `ProvisionedGMT` ASC) ,
  INDEX `idx_pointofsale_name` (`CustomerId` ASC, `Name` ASC, `ProvisionedGMT` ASC) ,
  INDEX `idx_pointofsale_location` (`LocationId` ASC) ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_pointofsale_serialnumber_uq2` (`SerialNumber` ASC, `CustomerId` ASC, `ProvisionedGMT` ASC) ,
  INDEX `idx_pointofsale_settingsfile` (`SettingsFileId` ASC) ,
  UNIQUE INDEX `idx_pointofsale_serialnumber_uq` (`CustomerId` ASC, `SerialNumber` ASC, `ProvisionedGMT` ASC) ,
  INDEX `idx_pointofsale_name_2` (`Name` ASC, `CustomerId` ASC, `ProvisionedGMT` ASC) ,
  CONSTRAINT `fk_pointofsale_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pointofsale_paystation`
    FOREIGN KEY (`PaystationId` )
    REFERENCES `Paystation` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pointofsale_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pointofsale_settingsfile`
    FOREIGN KEY (`SettingsFileId` )
    REFERENCES `SettingsFile` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'PointOfSale is the assignment of a Paystation to a Customer  /* comment truncated */ /*on a specific date (ProvisionedGMT).*/';


-- -----------------------------------------------------
-- Table `POSCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSCollection` ;

CREATE  TABLE IF NOT EXISTS `POSCollection` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CollectionTypeId` TINYINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `StartGMT` DATETIME NOT NULL ,
  `EndGMT` DATETIME NOT NULL ,  
  `StartTicketNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `EndTicketNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `NextTicketNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `StartTransactionNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `EndTransactionNumber` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `TicketsSold` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount05` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount10` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount25` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount100` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount200` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount1` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount2` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount5` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount10` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount20` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount50` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount1` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount2` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount5` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount10` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount20` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount50` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `AmexAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `DinersAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `DiscoverAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `MasterCardAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `VisaAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `SmartCardAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ValueCardAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `SmartCardRechargeAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `AcceptedFloatAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `AttendantDepositAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `AttendantTicketsAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `AttendantTicketsSold` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChangeIssuedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ExcessPaymentAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `OverfillAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `PatrollerTicketsSold` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RefundIssuedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ReplenishedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `TestDispensedChanger` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `TestDispensedHopper1` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `TestDispensedHopper2` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper1Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper2Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper1Current` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper2Current` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper1Dispensed` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper2Dispensed` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper1Replenished` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Hopper2Replenished` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube1Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube2Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube3Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube4Type` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube1Amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube2Amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube3Amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `Tube4Amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CreatedGMT` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_poscollection_unq` (`PointOfSaleId` , `CollectionTypeId`, `StartGMT` , `EndGMT` ) ,
  INDEX `idx_poscollection_customer` (`CustomerId` ASC) ,
  INDEX `idx_poscollection_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_poscollection_collectiontype` (`CollectionTypeId` ASC) ,
  INDEX `idx_poscollection_paystationsetting` (`PaystationSettingId` ASC) ,
  INDEX `idx_poscollection_created` (`CreatedGMT` ASC) ,
  INDEX `idx_poscollection_multi` (`PointOfSaleId` ASC, `EndGMT` DESC, `CollectionTypeId` ASC) ,
  INDEX `idx_poscollection_endgmt` (`EndGMT` ASC) ,
  CONSTRAINT `fk_poscollection_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_poscollection_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_poscollection_collectiontype`
    FOREIGN KEY (`CollectionTypeId` )
    REFERENCES `CollectionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_poscollection_paystationsetting`
    FOREIGN KEY (`PaystationSettingId` )
    REFERENCES `PaystationSetting` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `POSDate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSDate` ;

CREATE  TABLE IF NOT EXISTS `POSDate` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `POSDateTypeId` TINYINT UNSIGNED NOT NULL ,
  `ChangedGMT` DATETIME NOT NULL ,
  `CurrentSetting` TINYINT(1) UNSIGNED NOT NULL COMMENT '0=No (false), 1=Yes (true)' ,
  `Comment` VARCHAR(255) NULL ,  
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_posdate_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_posdate_posdatetype` (`POSDateTypeId` ASC) ,
  INDEX `idx_posdate_lastmodifiedbyuser` (`LastModifiedByUserId` ASC) ,
  UNIQUE INDEX `idx_posdate_multi_uq` (`PointOfSaleId` ASC, `ChangedGMT` ASC, `POSDateTypeId` ASC) ,
  CONSTRAINT `fk_posdate_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_posdate_posdatetype`
    FOREIGN KEY (`POSDateTypeId` )
    REFERENCES `POSDateType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idx_posdate_lastmodifiedbyuser`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The historical tracking of important dates for a Point Of Sa' /* comment truncated */;


-- -----------------------------------------------------
-- Table `ActivePOSAlert`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivePOSAlert` ;

CREATE  TABLE IF NOT EXISTS `ActivePOSAlert` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerAlertTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `POSAlertId` INT UNSIGNED NULL ,
  `EventDeviceTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventActionTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventSeverityTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL ,
  `AlertGMT` DATETIME NULL ,
  `ClearedGMT` DATETIME NULL ,
  `AlertInfo` VARCHAR(50) NULL COMMENT 'I think AlertInfo will be dropped in EMS 7 (will end of life in EMS 6)' ,
  `CreatedGMT` DATETIME NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_ActivePOSAlert_EventDeviceType` (`EventDeviceTypeId` ASC) ,
  INDEX `idx_ActivePOSAlert_EventStatusType` (`EventStatusTypeId` ASC) ,
  INDEX `idx_ActivePOSAlert_EventActionType` (`EventActionTypeId` ASC) ,
  INDEX `idx_ActivePOSAlert_PointOfSale` (`PointOfSaleId` ASC) ,
  INDEX `idx_ActivePOSAlert_EventSeverityType` (`EventSeverityTypeId` ASC) ,
  UNIQUE INDEX `idx_ActivePOSAlert_Multi_Uq` (`CustomerAlertTypeId` ASC, `PointOfSaleId` ASC, `EventDeviceTypeId` ASC, `EventStatusTypeId` ASC, `EventActionTypeId` ASC, `EventSeverityTypeId` ASC) ,
  INDEX `idx_ActivePOSAlert_CustomerAlertType` (`CustomerAlertTypeId` ASC) ,
  INDEX `idx_ActivePOSAlert_POSAlert` (`POSAlertId` ASC) ,
  INDEX `ind_ActivePOSAlert_mult1`(`PointOfSaleId`, `EventDeviceTypeId`, `IsActive`) ,
  CONSTRAINT `fk_ActivePOSAlert_EventDeviceType`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_EventStatusType`
    FOREIGN KEY (`EventStatusTypeId` )
    REFERENCES `EventStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_EventActionType`
    FOREIGN KEY (`EventActionTypeId` )
    REFERENCES `EventActionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_EventSeverityType`
    FOREIGN KEY (`EventSeverityTypeId` )
    REFERENCES `EventSeverityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_CustomerAlertType`
    FOREIGN KEY (`CustomerAlertTypeId` )
    REFERENCES `CustomerAlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ActivePOSAlert_POSAlert`
    FOREIGN KEY (`POSAlertId` )
    REFERENCES `POSAlert` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The active alerts for a point of sale (pay station). These a' /* comment truncated */;


-- -----------------------------------------------------
-- Table `POSStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSStatus` ;

CREATE  TABLE IF NOT EXISTS `POSStatus` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsProvisioned` TINYINT(1) UNSIGNED NOT NULL ,
  `IsLocked` TINYINT(1) UNSIGNED NOT NULL ,
  `IsDecommissioned` TINYINT(1) UNSIGNED NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `IsVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `IsBillableMonthlyOnActivation` TINYINT(1) UNSIGNED NOT NULL ,
  `IsDigitalConnect` TINYINT(1) UNSIGNED NOT NULL ,
  `IsTestActivated` TINYINT(1) UNSIGNED NOT NULL ,
  `IsActivated` TINYINT(1) UNSIGNED NOT NULL ,
  `IsBillableMonthlyForEms` TINYINT(1) UNSIGNED NOT NULL ,
  `IsProvisionedGMT` DATETIME NOT NULL ,
  `IsLockedGMT` DATETIME NOT NULL ,
  `IsDecommissionedGMT` DATETIME NOT NULL ,
  `IsDeletedGMT` DATETIME NOT NULL ,
  `IsVisibleGMT` DATETIME NOT NULL ,
  `IsBillableMonthlyOnActivationGMT` DATETIME NOT NULL ,
  `IsDigitalConnectGMT` DATETIME NOT NULL ,
  `IsTestActivatedGMT` DATETIME NOT NULL ,
  `IsActivatedGMT` DATETIME NOT NULL ,
  `IsBillableMonthlyForEmsGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `TelemetryReferenceCounter` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  INDEX `idx_posstatus_pointofsale` (`PointOfSaleId` ASC) ,
  
  PRIMARY KEY (`PointOfSaleId`) ,
  CONSTRAINT `fk_posstatus_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The current status of a Point Of Sale for each of the POS Da' /* comment truncated */;


-- -----------------------------------------------------
-- Table `POSHeartbeat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSHeartbeat` ;

CREATE  TABLE IF NOT EXISTS `POSHeartbeat` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `LastHeartbeatGMT` DATETIME NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`PointOfSaleId`) ,
  INDEX `idx_posheartbeat_pointofsale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_posheartbeat_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The date and time (in GMT) of the most recent activity from ' /* comment truncated */;


-- -----------------------------------------------------
-- Table `POSServiceState`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSServiceState` ;

CREATE  TABLE IF NOT EXISTS `POSServiceState` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `BBSerialNumber` VARCHAR(20) NULL ,
  `BillStackerCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' ,
  `BillStackerLevel` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `BillStackerSize` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
  `CoinBagCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' ,
  `CoinChangerLevel` TINYINT(4) UNSIGNED NOT NULL DEFAULT '2' ,
  `CoinHopper1DispensedCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' ,
  `CoinHopper1Level` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `CoinHopper2DispensedCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' ,
  `CoinHopper2Level` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `LastPaystationSettingUploadGMT` DATETIME NULL ,
  `PaystationSettingName` VARCHAR(20) NULL ,
  `NextSettingsFileId` MEDIUMINT UNSIGNED NULL , 
  `PrimaryVersion` VARCHAR(20) NULL ,
  `SecondaryVersion` VARCHAR(20) NULL ,
  `TotalAmountSinceLastAudit` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0' ,
  `UpgradeGMT` DATETIME NULL ,
  `IsBillAcceptor` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsBillStacker` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCardReader` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinAcceptor` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinCanister` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinChanger` TINYINT(4) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinEscrow` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinHopper1` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoinHopper2` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsPrinter` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsRfidCardReader` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsNewPaystationSetting` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsNewPublicKey` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsNewTicketFooter` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`PointOfSaleId`) ,
  INDEX `idx_POSServiceState_PointOfSale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_POSServiceState_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSServiceState_NextSettingsFileId`
    FOREIGN KEY (`NextSettingsFileId` )
    REFERENCES `SettingsFile` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The most recent information for numerous point of sale (pay ' /* comment truncated */;

-- -----------------------------------------------------
-- Table `POSSensorState`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSSensorState` ;

CREATE  TABLE IF NOT EXISTS `POSSensorState` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `Battery1Level` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `Battery1Voltage` FLOAT NULL ,
  `Battery2Level` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `Battery2Voltage` FLOAT NULL ,
  `InputCurrent` FLOAT NULL ,
  `SystemLoad` FLOAT UNSIGNED NULL ,
  `AmbientTemperature` FLOAT NULL ,
  `ControllerTemperature` FLOAT NULL ,
  `RelativeHumidity` FLOAT UNSIGNED NULL ,
  `PrinterPaperLevel` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `LastPaperLevelGMT` DATETIME NULL ,
  `PrinterStatus` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2' ,
  `LastPrinterStatusGMT` DATETIME NULL ,
  `WirelessSignalStrength` VARCHAR(40) NULL ,
  `LastWirelessSignalGMT` DATETIME NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`PointOfSaleId`) ,
  INDEX `idx_POSSensorState_PointOfSale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_POSSensorState_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The most recent information for numerous point of sale (pay ' /* comment truncated */;

-- -----------------------------------------------------
-- Table `POSAlertStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSAlertStatus` ;

CREATE  TABLE IF NOT EXISTS `POSAlertStatus` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CommunicationMinor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CommunicationMajor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CommunicationCritical` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CollectionMinor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CollectionMajor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CollectionCritical` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `PayStationMinor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `PayStationMajor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `PayStationCritical` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `VERSION` INT UNSIGNED NOT NULL ,
  INDEX `idx_POSAlertStatus_PointOfSale` (`PointOfSaleId` ASC) ,
  PRIMARY KEY (`PointOfSaleId`) ,
  CONSTRAINT `fk_POSAlertStatus_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The current status of a Point Of Sale Alerts' /* comment truncated */;

-- -----------------------------------------------------
-- Table `POSAlertStatusDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSAlertStatusDetail` ;

CREATE  TABLE IF NOT EXISTS `POSAlertStatusDetail` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `AlertThresholdTypeID` SMALLINT UNSIGNED NOT NULL,
  `EventDeviceTypeId` TINYINT UNSIGNED,
  `Minor` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `Major` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `Critical` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `MinorAlertId` INT UNSIGNED,
  `MajorAlertId` INT UNSIGNED,
  `CriticalAlertId` INT UNSIGNED,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`) ,
  INDEX `idx_POSAlertStatusDetail_AlertCategory` (`AlertThresholdTypeID`, `EventDeviceTypeId`),
  CONSTRAINT `fk_POSAlertStatusDetail_PointOfSaleId`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `POSAlertStatus` (`PointOfSaleId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlertStatusDetail_AlertThresholdType`
    FOREIGN KEY (`AlertThresholdTypeId` )
    REFERENCES `AlertThresholdType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlertStatusDetail_EventDeviceTypeId`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The current detail status (broken down to each module) of a Point Of Sale Alerts' /* comment truncated */;

-- -----------------------------------------------------
-- Table `Processor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Processor` ;

CREATE  TABLE IF NOT EXISTS `Processor` (
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `ProductionUrl` VARCHAR(250) NOT NULL ,
  `TestUrl` VARCHAR(250) NULL ,
  `IsTest` TINYINT(1) UNSIGNED NOT NULL ,
  `IsGateway` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsEMV` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsForValueCard` TINYINT(1) UNSIGNED NOT NULL ,
  `IsPaused` TINYINT UNSIGNED NOT NULL DEFAULT 0 , 
  `IsExcluded` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `EMS6ProcessorNameMapping` VARCHAR(40) NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_processor_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Purchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Purchase` ;

CREATE  TABLE IF NOT EXISTS `Purchase` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `PurchaseNumber` INT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `PaymentTypeId` TINYINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `CouponId` MEDIUMINT UNSIGNED NULL ,
  `OriginalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChargedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChangeDispensedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ExcessPaymentAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CashPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateRevenueAmount` MEDIUMINT NOT NULL DEFAULT 0 ,
  `CoinCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `IsOffline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsRefundSlip` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `TransactionGMT` DATETIME NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  INDEX `idx_purchase_transactiontype` (`TransactionTypeId` ASC) ,
  INDEX `idx_purchase_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_purchase_paymenttype` (`PaymentTypeId` ASC) ,
  INDEX `idx_purchase_location` (`LocationId` ASC) ,
  INDEX `idx_purchase_coupon` (`CouponId` ASC) ,
  INDEX `idx_purchase_paystationsetting` (`PaystationSettingId` ASC) ,
  INDEX `idx_purchase_unifiedrate` (`UnifiedRateId` ASC) ,
  INDEX `idx_purchase_PurchaseGMT` (`PurchaseGMT` ASC),
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_purchase_multi_uq` (`CustomerId` ASC, `PointOfSaleId` ASC, `PurchaseGMT` ASC, `PurchaseNumber` ASC) ,
  INDEX `idx_purchase_created` (`CreatedGMT` ASC) ,
  INDEX `idx_purchase_customer` (`CustomerId` ASC) ,
  CONSTRAINT `fk_purchase_transactiontype`
    FOREIGN KEY (`TransactionTypeId` )
    REFERENCES `TransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_paymenttype`
    FOREIGN KEY (`PaymentTypeId` )
    REFERENCES `PaymentType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_coupon`
    FOREIGN KEY (`CouponId` )
    REFERENCES `Coupon` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_paystationsetting`
    FOREIGN KEY (`PaystationSettingId` )
    REFERENCES `PaystationSetting` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_unifiedrate`
    FOREIGN KEY (`UnifiedRateId` )
    REFERENCES `UnifiedRate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PurchaseTax`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PurchaseTax` ;

CREATE  TABLE IF NOT EXISTS `PurchaseTax` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `TaxId` MEDIUMINT UNSIGNED NOT NULL ,
  `TaxRate` SMALLINT UNSIGNED NOT NULL ,
  `TaxAmount` SMALLINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_purchasetax_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_purchasetax_tax` (`TaxId` ASC) ,
  CONSTRAINT `fk_purchasetax_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchasetax_tax`
    FOREIGN KEY (`TaxId` )
    REFERENCES `Tax` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestAccount` ;

CREATE  TABLE IF NOT EXISTS `RestAccount` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `RestAccountTypeId` TINYINT UNSIGNED NOT NULL ,
  `AccountName` VARCHAR(255) NOT NULL ,
  `SecretKey` VARCHAR(50) NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `Comments` VARCHAR(100),
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_restaccount_restaccounttype` (`RestAccountTypeId` ASC) ,
  INDEX `idx_restaccount_customer` (`CustomerId` ASC) ,
  INDEX `idx_restaccount_pointofsale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_restaccount_restaccounttype`
    FOREIGN KEY (`RestAccountTypeId` )
    REFERENCES `RestAccountType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_restaccount_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_restaccount_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Role` ;

CREATE  TABLE IF NOT EXISTS `Role` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerTypeId` TINYINT UNSIGNED NOT NULL ,
  `RoleStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `IsAdmin` TINYINT(1) UNSIGNED NOT NULL ,
  `IsLocked` TINYINT(1) UNSIGNED NOT NULL ,
  `IsFromParent` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_role_customertype` (`CustomerTypeId` ASC) ,
  INDEX `idx_role_useraccount` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_role_rolestatustype` (`RoleStatusTypeId` ASC) ,
  INDEX `idx_role_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_role_multi_uq` (`CustomerId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_role_customertype`
    FOREIGN KEY (`CustomerTypeId` )
    REFERENCES `CustomerType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_rolestatustype`
    FOREIGN KEY (`RoleStatusTypeId` )
    REFERENCES `RoleStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RolePermission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RolePermission` ;

CREATE  TABLE IF NOT EXISTS `RolePermission` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `RoleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermissionId` SMALLINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_rolepermission_role` (`RoleId` ASC) ,
  UNIQUE INDEX `rolepermission_multi_uq` (`RoleId` ASC, `PermissionId` ASC) ,
  INDEX `idx_rolepermission_permission` (`PermissionId` ASC) ,
  INDEX `idx_rolepermission_user` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_rolepermission_useraccount` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_rolepermission_role`
    FOREIGN KEY (`RoleId` )
    REFERENCES `Role` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rolepermission_permission`
    FOREIGN KEY (`PermissionId` )
    REFERENCES `Permission` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rolepermission_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Route`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Route` ;

CREATE  TABLE IF NOT EXISTS `Route` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `RouteTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_route_multi_uq` (`CustomerId` ASC, `Name` ASC) ,
  INDEX `idx_route_customer` (`CustomerId` ASC) ,
  INDEX `idx_route_routetype` (`RouteTypeId` ASC) ,
  CONSTRAINT `fk_route_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_routetype`
    FOREIGN KEY (`RouteTypeId` )
    REFERENCES `RouteType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RoutePOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RoutePOS` ;

CREATE  TABLE IF NOT EXISTS `RoutePOS` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `RouteId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_routepos_route` (`RouteId` ASC) ,
  INDEX `idx_routepos_pointofsale` (`PointOfSaleId` ASC) ,
  UNIQUE INDEX `idx_routepos_multi_uq1` (`RouteId` ASC, `PointOfSaleId` ASC) ,
  UNIQUE INDEX `idx_routepos_multi_uq2` (`PointOfSaleId` ASC, `RouteId` ASC) ,
  CONSTRAINT `fk_routepos_route`
    FOREIGN KEY (`RouteId` )
    REFERENCES `Route` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_routepos_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ServiceAgreement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ServiceAgreement` ;

CREATE  TABLE IF NOT EXISTS `ServiceAgreement` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Content` MEDIUMBLOB NOT NULL ,
  `ContentVersion` INT UNSIGNED NOT NULL ,
  `UploadedGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_serviceagreement_user` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_serviceagreement_useraccount`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SmsAlert`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SmsAlert` ;

CREATE  TABLE IF NOT EXISTS `SmsAlert` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `MobileNumberId` INT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `AddTimeNumber` INT UNSIGNED NOT NULL ,
  `PaystationSettingName` VARCHAR(20) NULL ,
  `LicencePlate` VARCHAR(15) NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `NumberOfRetries` TINYINT UNSIGNED NOT NULL ,
  `IsAutoExtended` TINYINT(1) UNSIGNED NOT NULL ,
  `IsAlerted` TINYINT(1) UNSIGNED NOT NULL ,
  `IsLocked` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  INDEX `idx_smsalert_expire` (`PermitExpireGMT` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_smsalert_customer` (`CustomerId` ASC) ,
  INDEX `idx_smsalert_location` (`PermitLocationId` ASC) ,
  INDEX `idx_smsalert_pos` (`PointOfSaleId` ASC) ,
  UNIQUE INDEX `idx_smsalert_mobilenumber` (`MobileNumberId` ASC) ,
  CONSTRAINT `fk_smsalert_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smsalert_location`
    FOREIGN KEY (`PermitLocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smsalert_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smsalert_mobilenumber`
    FOREIGN KEY (`MobileNumberId` )
    REFERENCES `MobileNumber` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SmsFailedResponse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SmsFailedResponse` ;

CREATE  TABLE IF NOT EXISTS `SmsFailedResponse` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `SmsMessageTypeId` TINYINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `TrackingId` VARCHAR(255) NULL DEFAULT NULL ,
  `Number` VARCHAR(255) NULL DEFAULT NULL ,
  `ConvertedNumber` VARCHAR(255) NULL DEFAULT NULL ,
  `BlockedReason` VARCHAR(255) NULL DEFAULT NULL ,
  `IsOk` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Q: needed?	' ,
  `IsMessageEmpty` TINYINT(1) UNSIGNED NOT NULL ,
  `IsTooManyMessages` TINYINT(1) UNSIGNED NOT NULL ,
  `IsInvalidCountryCode` TINYINT(1) UNSIGNED NOT NULL ,
  `IsBlocked` TINYINT(1) UNSIGNED NOT NULL ,
  `IsBalanceZero` TINYINT(1) UNSIGNED NOT NULL ,
  `IsInvalidCarrierCode` TINYINT(1) UNSIGNED NOT NULL ,
  `IsDeferUntilOccursInThePast` TINYINT(1) UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_smsfailedresponse_pos` (`PointOfSaleId` ASC) ,
  INDEX `idx_smsfailedresponse_smsmessagetype` (`SmsMessageTypeId` ASC) ,
  CONSTRAINT `fk_smsfailedresponse_pos`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smsfailedresponse_smsmessagetype`
    FOREIGN KEY (`SmsMessageTypeId` )
    REFERENCES `SmsMessageType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SmsTransactionLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SmsTransactionLog` ;

CREATE  TABLE IF NOT EXISTS `SmsTransactionLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `SMSMessageTypeId` TINYINT UNSIGNED NOT NULL ,
  `MobileNumberId` INT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `ExpiryDateGMT` DATETIME NOT NULL ,
  `ConsumerResponse` VARCHAR(30) NULL DEFAULT NULL ,
  `TimeStampGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_smstransactionlog_smsmessagetype` (`SMSMessageTypeId` ASC) ,
  INDEX `idx_smstransactionlog_mobilenumber` (`MobileNumberId` ASC) ,
  INDEX `idx_smstransactionlog_pointofsale` (`PointOfSaleId` ASC) ,
  CONSTRAINT `fk_smstransactionlog_smsmessagetype`
    FOREIGN KEY (`SMSMessageTypeId` )
    REFERENCES `SmsMessageType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smstransactionlog_mobilenumber`
    FOREIGN KEY (`MobileNumberId` )
    REFERENCES `MobileNumber` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_smstransactionlog_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SpecialPermissionDate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SpecialPermissionDate` ;

CREATE  TABLE IF NOT EXISTS `SpecialPermissionDate` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `StartDateLocal` DATETIME NOT NULL ,
  `EndDateLocal` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SpecialRateDate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SpecialRateDate` ;

CREATE  TABLE IF NOT EXISTS `SpecialRateDate` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `StartDateLocal` DATETIME NOT NULL ,
  `EndDateLocal` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tax`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tax` ;

CREATE  TABLE IF NOT EXISTS `Tax` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_tax_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_tax_multi_uq` (`CustomerId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_tax_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UnifiedRate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UnifiedRate` ;

CREATE  TABLE IF NOT EXISTS `UnifiedRate` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_unifiedrate_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_unifiedrate_multi_uq` (`CustomerId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_unifiedrate_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserAccount` ;

CREATE  TABLE IF NOT EXISTS `UserAccount` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerEmailId` MEDIUMINT UNSIGNED NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NULL,
  `UserName` VARCHAR(765) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `FirstName` VARCHAR(75) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `LastName` VARCHAR(75) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `Password` VARCHAR(128) NOT NULL ,
  `IsPasswordTemporary` TINYINT(1) UNSIGNED NOT NULL ,
  `IsAliasUser` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `IsAllChilds` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `IsDefaultAlias` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `ArchiveDate` DATETIME NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `PasswordSalt` VARCHAR(16) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_useraccount_customer` (`CustomerId` ASC) ,
  INDEX `idx_useraccount_userstatustype` (`UserStatusTypeId` ASC) ,  
  UNIQUE INDEX `idx_useraccount_multi_uq` (`CustomerId` ASC, `UserName` ASC, `UserStatusTypeId`, `ArchiveDate` ) ,
  INDEX `idx_useraccount_customeruser` (`CustomerId` ASC, `LastName` ASC, `FirstName` ASC) ,
  INDEX `idx_useraccount_customeremail` (`CustomerEmailId` ASC) ,
  CONSTRAINT `fk_useraccount_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccount_userstatustype`
    FOREIGN KEY (`UserStatusTypeId` )
    REFERENCES `UserStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccount_customeremail`
    FOREIGN KEY (`CustomerEmailId` )
    REFERENCES `CustomerEmail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccount_useraccount1`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserDefaultDashboard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserDefaultDashboard` ;

CREATE  TABLE IF NOT EXISTS `UserDefaultDashboard` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsDefaultDashboard` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_userdefaultdashboard_useraccount` (`UserAccountId` ASC) ,
  CONSTRAINT `fk_userdefaultdashboard_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserRole`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserRole` ;

CREATE  TABLE IF NOT EXISTS `UserRole` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `RoleId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_userrole_useraccount1` (`UserAccountId` ASC) ,
  INDEX `idx_userrole_role` (`RoleId` ASC) ,
  UNIQUE INDEX `userrole_multi_uq` (`UserAccountId` ASC, `RoleId` ASC) ,
  INDEX `idx_userrole_useraccount2` (`LastModifiedByUserId` ASC) ,
  CONSTRAINT `fk_userrole_useraccount1`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userrole_role`
    FOREIGN KEY (`RoleId` )
    REFERENCES `Role` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userrole_useraccount2`
    FOREIGN KEY (`LastModifiedByUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WebServiceEndPoint`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WebServiceEndPoint` ;

CREATE  TABLE IF NOT EXISTS `WebServiceEndPoint` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `WebServiceEndPointTypeId` TINYINT UNSIGNED NOT NULL ,
  `Token` VARCHAR(45) NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `Comments` VARCHAR(100),
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_webserviceendpoint_webserviceendpointtype` (`WebServiceEndPointTypeId` ASC) ,
  INDEX `idx_webserviceendpoint_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_webserviceendpoint_token_uq` (`Token` ASC) ,
  CONSTRAINT `fk_webserviceendpoint_webserviceendpointtype`
    FOREIGN KEY (`WebServiceEndPointTypeId` )
    REFERENCES `WebServiceEndPointType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_webserviceendpoint_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserAccountRoute`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `UserAccountRoute` ;

CREATE TABLE IF NOT EXISTS `UserAccountRoute` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `RouteId` MEDIUMINT UNSIGNED NOT NULL ,
  `MobileApplicationTypeId` MEDIUMINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `fk_useracct_mobileapptype` (`UserAccountId`,`MobileApplicationTypeId`),
  CONSTRAINT `fk_useraccountroute_useraccount`
    FOREIGN KEY (`UserAccountId`)
    REFERENCES `UserAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccountroute_route`
    FOREIGN KEY (`RouteId`)
    REFERENCES `Route` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccountroute_mobileapplicationtype`
    FOREIGN KEY (`MobileApplicationTypeId`)
    REFERENCES `MobileApplicationType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MobileLicenseStatusType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MobileLicenseStatusType`;

CREATE TABLE IF NOT EXISTS `MobileLicenseStatusType`(
  `Id` SMALLINT UNSIGNED NOT NULL,
  `Name` varchar(80),  
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MobileApplicationType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MobileApplicationType`;

CREATE TABLE IF NOT EXISTS `MobileApplicationType`(
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` varchar(80),
  `SubscriptionTypeId` SMALLINT UNSIGNED,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_mobileapplicationtype_subscriptiontype`
      FOREIGN KEY (`SubscriptionTypeId`)
      REFERENCES `SubscriptionType` (`Id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
ENGINE = InnoDB;
    
-- -----------------------------------------------------
-- Table `MobileDevice`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MobileDevice`;

CREATE TABLE IF NOT EXISTS `MobileDevice`(
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Uid` VARCHAR(75) NOT NULL,
  `IsBlockedDPT` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Uid_UNIQUE` (`Uid`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMobileDevice`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CustomerMobileDevice`;

CREATE TABLE IF NOT EXISTS `CustomerMobileDevice`(
	`Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`MobileDeviceId` MEDIUMINT UNSIGNED NOT NULL,
	`CustomerId` MEDIUMINT UNSIGNED NOT NULL,
	`IsBlocked` TINYINT UNSIGNED NOT NULL,
	`Name` varchar(80),
	`Description` varchar(255),
	PRIMARY KEY (`Id`),
	CONSTRAINT `fk_customermobiledevice_mobiledevice`
		FOREIGN KEY (`MobileDeviceId`)
		REFERENCES `MobileDevice` (`Id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT `fk_customermobiledevice_customer`
		FOREIGN KEY (`CustomerId`)
		REFERENCES `Customer` (`Id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `MobileLicense`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MobileLicense`;

CREATE TABLE IF NOT EXISTS `MobileLicense`(
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL,
  `MobileApplicationTypeId` MEDIUMINT UNSIGNED NOT NULL,
  `CustomerMobileDeviceId` MEDIUMINT UNSIGNED,
  `SecretKey` varchar(50),
  `LastProvisionDate` datetime,
  `MobileLicenseStatusTypeId` SMALLINT UNSIGNED NOT NULL,  
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_mobileLicense_customer`
    FOREIGN KEY (`CustomerId`)
    REFERENCES `Customer` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileLicense_device`
    FOREIGN KEY (`CustomerMobileDeviceId`)
    REFERENCES `CustomerMobileDevice` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileLicense_mobileapplicationtype`
    FOREIGN KEY (`MobileApplicationTypeId`)
    REFERENCES `MobileApplicationType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileLicense_mobilelicensestatustype`
    FOREIGN KEY (`MobileLicenseStatusTypeId`)
    REFERENCES `MobileLicenseStatusType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MobileSessionToken`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MobileSessionToken`;

CREATE TABLE IF NOT EXISTS `MobileSessionToken`(
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `MobileLicenseId` MEDIUMINT UNSIGNED NOT NULL,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL,
  `SessionToken` varchar(32) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `ExpiryDate` datetime NOT NULL,
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  `Latitude` DECIMAL(18,14) NULL ,
  `Longitude` DECIMAL(18,14) NULL ,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_mobilesessiontoken_mobileLicense`
    FOREIGN KEY (`MobileLicenseId`)
    REFERENCES `MobileLicense` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobilesessiontoken_useraccount`
    FOREIGN KEY (`UserAccountId`)
    REFERENCES `UserAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `MobileAppHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MobileAppHistory`;

CREATE TABLE IF NOT EXISTS `MobileAppHistory`(    
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `UserAccountId` MEDIUMINT(8) UNSIGNED NULL,
  `MobileApplicationTypeId` MEDIUMINT(8) UNSIGNED NOT NULL,
  `CustomerMobileDeviceId` MEDIUMINT(8) UNSIGNED NOT NULL,
  `MobileAppActivityTypeId` MEDIUMINT(8) UNSIGNED NOT NULL,
  `IsSuccessful` TINYINT UNSIGNED NOT NULL,
  `Result` varchar(50) NULL,
  `Timestamp` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_mobileapphistory_mobileappactivitytype`
    FOREIGN KEY (`MobileAppActivityTypeId`)
    REFERENCES `MobileAppActivityType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileapphistory_customermobiledevice`
    FOREIGN KEY (`CustomerMobileDeviceId`)
    REFERENCES `CustomerMobileDevice` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileapphistory_mobileapplicationtype`
    FOREIGN KEY (`MobileApplicationTypeId`)
    REFERENCES `MobileApplicationType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mobileapphistory_useraccount`
    FOREIGN KEY (`UserAccountId`)
    REFERENCES `UserAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `MobileAppActivityType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MobileAppActivityType`;

CREATE TABLE IF NOT EXISTS `MobileAppActivityType`(
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) NOT NULL,
  PRIMARY KEY (`Id`)
)
ENGINE = InnoDB;


-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- WIDGET Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `Tab`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tab` ;

CREATE  TABLE IF NOT EXISTS `Tab` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `OrderNumber` TINYINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_tab_useraccount` (`UserAccountId` ASC) ,
  INDEX `tab_ordernumber` (`UserAccountId` ASC, `OrderNumber` ASC) ,
  INDEX `tab_name` (`UserAccountId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_tab_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Section`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Section` ;

CREATE  TABLE IF NOT EXISTS `Section` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `TabId` INT UNSIGNED NOT NULL ,
  `LayoutId` TINYINT UNSIGNED NOT NULL ,
  `OrderNumber` TINYINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_section_tab` (`TabId` ASC) ,
  INDEX `section_tabordernumber` (`TabId` ASC, `OrderNumber` ASC) ,
  INDEX `section_usertabordernumber` (`UserAccountId` ASC, `TabId` ASC, `OrderNumber` ASC) ,
  CONSTRAINT `fk_section_tab`
    FOREIGN KEY (`TabId` )
    REFERENCES `Tab` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Widget`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Widget` ;

CREATE  TABLE IF NOT EXISTS `Widget` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `WidgetListTypeId` TINYINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `SectionId` INT UNSIGNED NOT NULL ,
  `WidgetMetricTypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetRangeTypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetTier1TypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetTier2TypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetTier3TypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetFilterTypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetChartTypeId` TINYINT UNSIGNED NOT NULL ,
  `WidgetLimitTypeId` TINYINT UNSIGNED NOT NULL ,
  `ColumnId` TINYINT UNSIGNED NOT NULL ,
  `OrderNumber` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(32) NOT NULL ,
  `Description` VARCHAR(255) NULL ,
  `ParentCustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TrendAmount` BIGINT UNSIGNED NOT NULL ,
  `IsSubsetTier1` TINYINT(1) UNSIGNED NOT NULL ,
  `IsSubsetTier2` TINYINT(1) UNSIGNED NOT NULL ,
  `IsSubsetTier3` TINYINT(1) UNSIGNED NOT NULL ,
  `IsTrendByLocation` TINYINT(1) UNSIGNED NOT NULL ,
  `IsShowPaystations` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_widget_section` (`SectionId` ASC) ,
  INDEX `idx_widget_ordernumber` (`UserAccountId` ASC, `SectionId` ASC, `ColumnId` ASC, `OrderNumber` ASC) ,
  INDEX `idx_widget_widgetmetrictype` (`WidgetMetricTypeId` ASC) ,
  INDEX `idx_widget_widgetcharttype` (`WidgetChartTypeId` ASC) ,
  INDEX `idx_widget_widgetrangetype` (`WidgetRangeTypeId` ASC) ,
  INDEX `idx_widget_widgettiertype2` (`WidgetTier2TypeId` ASC) ,
  INDEX `idx_widget_widgetfiltertype` (`WidgetFilterTypeId` ASC) ,
  INDEX `idx_widget_widgetlisttype` (`WidgetListTypeId` ASC) ,
  INDEX `idx_widget_customer_user` (`CustomerId` ASC, `UserAccountId` ASC) ,
  INDEX `idx_widget_widgettiertype1` (`WidgetTier1TypeId` ASC) ,
  INDEX `idx_widget_widgettiertype3` (`WidgetTier3TypeId` ASC) ,
  INDEX `idx_widget_widgetlimittype` (`WidgetLimitTypeId` ASC) ,
  CONSTRAINT `fk_widget_section`
    FOREIGN KEY (`SectionId` )
    REFERENCES `Section` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetmetrictype`
    FOREIGN KEY (`WidgetMetricTypeId` )
    REFERENCES `WidgetMetricType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetcharttype`
    FOREIGN KEY (`WidgetChartTypeId` )
    REFERENCES `WidgetChartType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetrangetype`
    FOREIGN KEY (`WidgetRangeTypeId` )
    REFERENCES `WidgetRangeType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgettiertype1`
    FOREIGN KEY (`WidgetTier2TypeId` )
    REFERENCES `WidgetTierType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetfiltertype`
    FOREIGN KEY (`WidgetFilterTypeId` )
    REFERENCES `WidgetFilterType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetlisttype`
    FOREIGN KEY (`WidgetListTypeId` )
    REFERENCES `WidgetListType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgettiertype2`
    FOREIGN KEY (`WidgetTier1TypeId` )
    REFERENCES `WidgetTierType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgettiertype3`
    FOREIGN KEY (`WidgetTier3TypeId` )
    REFERENCES `WidgetTierType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_widgetlimittype`
    FOREIGN KEY (`WidgetLimitTypeId` )
    REFERENCES `WidgetLimitType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetChartType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetChartType` ;

CREATE  TABLE IF NOT EXISTS `WidgetChartType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetcharttype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgetcharttype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetFilterType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetFilterType` ;

CREATE  TABLE IF NOT EXISTS `WidgetFilterType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetfiltertype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgetfiltertype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetLimitType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetLimitType` ;

CREATE  TABLE IF NOT EXISTS `WidgetLimitType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `RowLimit` SMALLINT UNSIGNED NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetlimittype_limit_uq` (`RowLimit` ASC) ,
  UNIQUE INDEX `idx_widgetlimittype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetListType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetListType` ;

CREATE  TABLE IF NOT EXISTS `WidgetListType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetlisttype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgetlisttype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetMetricType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetMetricType` ;

CREATE  TABLE IF NOT EXISTS `WidgetMetricType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `SubscriptionTypeId` SMALLINT UNSIGNED NULL DEFAULT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetmetrictype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgetmetrictype_label_uq` (`Label` ASC),
  CONSTRAINT `fk_widgetMetricType_subscriptiontype`
    FOREIGN KEY (`SubscriptionTypeId` )
    REFERENCES `SubscriptionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetRangeType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetRangeType` ;

CREATE  TABLE IF NOT EXISTS `WidgetRangeType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetrangetype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgetrangetype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetSubsetMember`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetSubsetMember` ;

CREATE  TABLE IF NOT EXISTS `WidgetSubsetMember` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `WidgetId` INT UNSIGNED NOT NULL ,
  `WidgetTierTypeId` TINYINT UNSIGNED NOT NULL ,
  `MemberId` MEDIUMINT UNSIGNED NOT NULL ,
  `MemberName` VARCHAR(50) NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  INDEX `idx_widgetsubsetmember_widget` (`WidgetId` ASC) ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgetsubsetmember_multi_uq` (`WidgetId` ASC, `WidgetTierTypeId` ASC, `MemberId` ASC) ,
  INDEX `idx_widgetsubsetmember_widgettiertype` (`WidgetTierTypeId` ASC) ,
  CONSTRAINT `fk_widgetsubsetmember_widget`
    FOREIGN KEY (`WidgetId` )
    REFERENCES `Widget` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widgetsubsetmember_widgettiertype`
    FOREIGN KEY (`WidgetTierTypeId` )
    REFERENCES `WidgetTierType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WidgetTierType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WidgetTierType` ;

CREATE  TABLE IF NOT EXISTS `WidgetTierType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `ListNumber` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Label` SMALLINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_widgettiertype_name_uq` (`Name` ASC) ,
  UNIQUE INDEX `idx_widgettiertype_multi_uq` (`ListNumber` ASC, `Id` ASC) ,
  UNIQUE INDEX `idx_widgettiertype_label_uq` (`Label` ASC) )
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `Time`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Time` ;

CREATE  TABLE IF NOT EXISTS `Time` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DateTime` DATETIME NOT NULL ,
  `Date` DATE NOT NULL ,
  `TimeAmPm` CHAR(8) NOT NULL ,
  `Year` SMALLINT UNSIGNED NOT NULL ,
  `Quarter` TINYINT UNSIGNED NOT NULL ,
  `Month` TINYINT UNSIGNED NOT NULL ,
  `MonthName` VARCHAR(10) NOT NULL ,
  `MonthNameAbbrev` VARCHAR(5) NOT NULL ,
  `WeekOfYear` SMALLINT UNSIGNED NOT NULL ,
  `DayOfYear` SMALLINT UNSIGNED NOT NULL ,
  `DayOfMonth` TINYINT UNSIGNED NOT NULL ,
  `DayOfWeek` TINYINT UNSIGNED NOT NULL ,
  `DayName` VARCHAR(10) NOT NULL ,
  `DayNameAbbrev` VARCHAR(5) NOT NULL ,
  `IsLastDayOfMonth` TINYINT UNSIGNED NOT NULL ,
  `IsWeekend` TINYINT UNSIGNED NOT NULL ,
  `HourOfDay` TINYINT UNSIGNED NOT NULL ,
  `QuarterOfDay` TINYINT UNSIGNED NOT NULL ,
  `QuarterOfHour` TINYINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_time_datetime` (`DateTime` ASC) ,
  INDEX `idx_time_date` (`Date` ASC) ,
  INDEX `idx_Time_mult` (Year, Month) ,
  INDEX `idx_Time_mult1`(Year,Month,dayofmonth,Quarterofday),
  INDEX `idx_Time_mult2`(Date,Quarterofday) ,
  INDEX `idx_time_quarter` (`Quarter` ASC) ,
  INDEX `idx_time_month` (`Month` ASC) ,
  INDEX `idx_time_weekofyear` (`WeekOfYear` ASC) ,
  INDEX `idx_time_dayofyear` (`DayOfYear` ASC) ,
  INDEX `idx_time_dayofmonth` (`DayOfMonth` ASC) ,
  INDEX `idx_time_dayofweek` (`DayOfWeek` ASC) ,
  INDEX `idx_time_hourofday` (`HourOfDay` ASC) ,
  INDEX `idx_time_quarterofday` (`QuarterOfDay` ASC) ,
  INDEX `idx_time_quarterofhour` (`QuarterOfHour` ASC) )
ENGINE = InnoDB;

























-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- ACTIVITY LOG Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `ActivityType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivityType` ;

CREATE  TABLE IF NOT EXISTS `ActivityType` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `ParentActivityTypeId` SMALLINT UNSIGNED NULL DEFAULT 0 ,
  `Name` VARCHAR(40) NOT NULL ,
  `Description` VARCHAR(255) NULL ,
  `MessagePropertiesKey` VARCHAR(80) NOT NULL ,
  `IsParent` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_activitytype_self` (`ParentActivityTypeId` ASC) ,
  UNIQUE INDEX `idx_activitytype_multi_uq` (`ParentActivityTypeId` ASC, `Name` ASC) ,
  UNIQUE INDEX `idx_activitytype_name_uq` (`Name` ASC) ,
  CONSTRAINT `fk_activitytype_self`
    FOREIGN KEY (`ParentActivityTypeId` )
    REFERENCES `ActivityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ActivityLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivityLog` ;

CREATE  TABLE IF NOT EXISTS `ActivityLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ActivityTypeId` SMALLINT UNSIGNED NOT NULL ,
  `EntityTypeId` SMALLINT UNSIGNED NOT NULL ,
  `EntityId` MEDIUMINT UNSIGNED NOT NULL ,
  `EntityLogId` INT UNSIGNED NOT NULL ,
  `ActivityGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_activitylog_useraccount` (`UserAccountId` ASC) ,
  INDEX `idx_activitylog_activitytype` (`ActivityTypeId` ASC) ,
  INDEX `UserAccount` (`UserAccountId` ASC, `ActivityGMT` ASC) ,
  INDEX `idx_activitylog_entitytype` (`EntityTypeId` ASC) ,
  CONSTRAINT `fk_activitylog_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activitylog_activitytype`
    FOREIGN KEY (`ActivityTypeId` )
    REFERENCES `ActivityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activitylog_entitytype`
    FOREIGN KEY (`EntityTypeId` )
    REFERENCES `EntityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ActivityLogPOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivityLogPOS` ;

CREATE  TABLE IF NOT EXISTS `ActivityLogPOS` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserActivityId` INT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_activitylogpos_activitylog` (`UserActivityId` ASC) ,
  INDEX `idx_activitylogpos_pointofsale` (`PointOfSaleId` ASC) ,
  UNIQUE INDEX `activitylogpos_multi_uq` (`UserActivityId` ASC, `PointOfSaleId` ASC) ,
  CONSTRAINT `fk_activitylogpos_activitylog`
    FOREIGN KEY (`UserActivityId` )
    REFERENCES `ActivityLog` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activitylogpos_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ActivityLogin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivityLogin` ;

CREATE  TABLE IF NOT EXISTS `ActivityLogin` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `LoginResultTypeId` TINYINT UNSIGNED NOT NULL ,
  `ActivityGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_activitylogin_loginresulttype` (`LoginResultTypeId` ASC) ,
  INDEX `idx_activitylogin_useraccount` (`UserAccountId` ASC) ,
  INDEX `UserAccount` (`UserAccountId` ASC, `ActivityGMT` ASC, `LoginResultTypeId` ASC) ,
  CONSTRAINT `fk_activitylogin_loginresulttype`
    FOREIGN KEY (`LoginResultTypeId` )
    REFERENCES `LoginResultType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activitylogin_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerAgreement_Log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerAgreement_Log` ;

CREATE  TABLE IF NOT EXISTS `CustomerAgreement_Log` (
  `LogId` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `ChangeTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ServiceAgreementId` SMALLINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Title` VARCHAR(30) NOT NULL ,
  `Organization` VARCHAR(255) NOT NULL ,
  `AgreedGMT` DATETIME NOT NULL ,
  `IsOverride` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  UNIQUE INDEX `customeragreement_log_multi_uq` (`Id` ASC, `LogId` ASC) ,
  INDEX `idx_customeragreement_useraccount` (`LastModifiedByUserId` ASC) ,
  PRIMARY KEY (`LogId`) ,
  INDEX `idx_customeragreement_log_changetype` (`ChangeTypeId` ASC) ,
  INDEX `idx_customer` (`CustomerId` ASC) ,
  CONSTRAINT `fk_customeragreement_log_changetype`
    FOREIGN KEY (`ChangeTypeId` )
    REFERENCES `ChangeType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Notification_Log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Notification_Log` ;

CREATE  TABLE IF NOT EXISTS `Notification_Log` (
  `LogId` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `ChangeTypeId` TINYINT UNSIGNED NOT NULL ,
  `Title` VARCHAR(50) NOT NULL ,
  `Message` VARCHAR(512) NOT NULL ,
  `MessageURL` VARCHAR(512) NULL ,
  `BeginGMT` DATETIME NULL ,
  `EndGMT` DATETIME NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`LogId`) ,
  UNIQUE INDEX `notification_log_multi_uq` (`Id` ASC, `LogId` ASC) ,
  INDEX `idx_notification_log_user` (`LastModifiedByUserId` ASC) ,
  INDEX `idx_notification_log_changetype` (`ChangeTypeId` ASC) ,
  CONSTRAINT `fk_notification_log_changetype`
    FOREIGN KEY (`ChangeTypeId` )
    REFERENCES `ChangeType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserAccount_Log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserAccount_Log` ;

CREATE  TABLE IF NOT EXISTS `UserAccount_Log` (
  `LogId` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `ChangeTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerEmailId` MEDIUMINT UNSIGNED NULL ,
  `UserName` VARCHAR(765) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `FirstName` VARCHAR(75) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `LastName` VARCHAR(75) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `Password` VARCHAR(128) NOT NULL ,
  `IsPasswordTemporary` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  `PasswordSalt` VARCHAR(16) NOT NULL ,
  UNIQUE INDEX `useraccount_log_multi_uq` (`Id` ASC, `LogId` ASC) ,
  PRIMARY KEY (`LogId`) ,
  INDEX `idx_useraccount_log_changetype` (`ChangeTypeId` ASC) ,
  INDEX `idx_customer_useraccount` (`CustomerId` ASC, `Id` ASC, `LogId` ASC) ,
  CONSTRAINT `fk_useraccount_log_changetype`
    FOREIGN KEY (`ChangeTypeId` )
    REFERENCES `ChangeType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserDefaultDashboard_Log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserDefaultDashboard_Log` ;

CREATE  TABLE IF NOT EXISTS `UserDefaultDashboard_Log` (
  `LogId` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ChangeTypeId` TINYINT UNSIGNED NOT NULL ,
  `Id` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsDefaultDashboard` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`LogId`) ,
  INDEX `idx_userdefaultdashboard_log_changetype` (`ChangeTypeId` ASC) ,
  UNIQUE INDEX `userdefaultdashboard_log_multi_uq1` (`Id` ASC, `LogId` ASC) ,
  UNIQUE INDEX `userdefaultdashboard_log_multi_uq2` (`UserAccountId` ASC, `Id` ASC, `LogId` ASC) ,
  CONSTRAINT `fk_userdefaultdashboard_log_changetype`
    FOREIGN KEY (`ChangeTypeId` )
    REFERENCES `ChangeType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `SigningLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SigningLog` ;

CREATE  TABLE IF NOT EXISTS `SigningLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT(8) NOT NULL ,
  `UserAccountName` VARCHAR(765) NOT NULL ,
  `signature` TEXT NOT NULL ,
  `signDate` DATETIME NOT NULL ,
  `comments` TEXT NULL ,
  PRIMARY KEY (`Id`)  
)
ENGINE = InnoDB;


-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- OTHER Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `ForeignKeyXref`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ForeignKeyXref` ;

CREATE  TABLE IF NOT EXISTS `ForeignKeyXref` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerIdEMS6` MEDIUMINT UNSIGNED NOT NULL ,
  `RegionIdEMS6` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationIdEMS6` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationId` MEDIUMINT UNSIGNED ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_foreignkeyxref_customeridems6` (`CustomerIdEMS6` ASC) ,
  INDEX `idx_foreignkeyxref_regionidems6` (`RegionIdEMS6` ASC) ,
  INDEX `idx_foreignkeyxref_paystationidems6` (`PaystationIdEMS6` ASC) ,
  INDEX `idx_foreignkeyxref_customerid` (`CustomerId` ASC) ,
  INDEX `idx_foreignkeyxref_locationid` (`LocationId` ASC) ,
  INDEX `idx_foreignkeyxref_pointofsaleid` (`PointOfSaleId` ASC) ,
  INDEX `idx_foreignkeyxref_paystationid` (`PaystationId` ASC) )
ENGINE = InnoDB;


-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- As of September 27 2012 all new tables added to this script are appended to the end of this script
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `Cluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cluster` ;

CREATE  TABLE IF NOT EXISTS `Cluster` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(50) NOT NULL ,
  `Hostname` VARCHAR(100) NOT NULL ,
  `LocalPort` SMALLINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_cluster_name_uq` (`Name` ASC) )
ENGINE = InnoDB
COMMENT = 'The Tomcat instances that run the EMS web application on a p' /* comment truncated */;


-- -----------------------------------------------------
-- Table `POSBalance`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSBalance` ;

CREATE  TABLE IF NOT EXISTS `POSBalance` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `ClusterId` SMALLINT UNSIGNED NULL ,
  `CollectionLockId` INT UNSIGNED NULL ,
  `PrevCollectionLockId` INT UNSIGNED NULL ,
  `PrevPrevCollectionLockId` INT UNSIGNED NULL ,
  `CashAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `UnsettledCreditCardCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `UnsettledCreditCardAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `TotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `LastCashCollectionGMT` DATETIME NOT NULL ,
  `LastCoinCollectionGMT` DATETIME NOT NULL ,
  `LastBillCollectionGMT` DATETIME NOT NULL ,
  `LastCardCollectionGMT` DATETIME NOT NULL ,
  `LastCollectionGMT` DATETIME NULL ,
  `LastCollectionTypeId` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `LastRecalcGMT` DATETIME NOT NULL ,
  `IsRecalcable` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `NextRecalcGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`PointOfSaleId`) ,
  INDEX `idx_posbalance_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_posbalance_collectionlock` (`CollectionLockId` ASC) ,
  INDEX `idx_posbalance_multi_1` (`IsRecalcable` DESC, `ClusterId` ASC, `NextRecalcGMT` ASC, `PointOfSaleId` ASC) ,
  INDEX `idx_posbalance_cluster` (`ClusterId` ASC) ,
  INDEX `idx_posbalance_multi_2` (`CollectionLockId` ASC, `IsRecalcable` DESC, `ClusterId` DESC, `PointOfSaleId` ASC, `NextRecalcGMT` ASC) ,
  CONSTRAINT `fk_posbalance_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_posbalance_collectionlock`
    FOREIGN KEY (`CollectionLockId` )
    REFERENCES `CollectionLock` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_posbalance_cluster`
    FOREIGN KEY (`ClusterId` )
    REFERENCES `Cluster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SensorInfoType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SensorInfoType` ;

CREATE  TABLE IF NOT EXISTS `SensorInfoType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `MinValue` FLOAT NOT NULL ,
  `MaxValue` FLOAT NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_sensorinfo_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `POSSensorInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSSensorInfo` ;

CREATE  TABLE IF NOT EXISTS `POSSensorInfo` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `SensorInfoTypeId` TINYINT UNSIGNED NOT NULL ,
  `SensorGMT` DATETIME NOT NULL ,
  `Amount` FLOAT NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX  `idx_possensorinfo_pointofsale1`(`PointOfSaleId`),
  INDEX `idx_possensorinfo_sensorinfotype1`(`SensorInfoTypeId`) ,
  INDEX `idx_possensorinfo_multi1`(`PointOfSaleId`, `SensorInfoTypeId`, `SensorGMT`),
  CONSTRAINT `fk_possensorinfo_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_possensorinfo_sensorinfotype`
    FOREIGN KEY (`SensorInfoTypeId` )
    REFERENCES `SensorInfoType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SettingsFileContent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SettingsFileContent` ;

CREATE  TABLE IF NOT EXISTS `SettingsFileContent` (
  `SettingsFileId` MEDIUMINT UNSIGNED NOT NULL ,
  `Content` MEDIUMBLOB NOT NULL ,
  PRIMARY KEY (`SettingsFileId`) ,
  INDEX `idx_settingsfilecontent_settingsfile` (`SettingsFileId` ASC) ,
  CONSTRAINT `fk_settingsfilecontent_settingsfile`
    FOREIGN KEY (`SettingsFileId` )
    REFERENCES `SettingsFile` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PurchaseCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PurchaseCollection` ;

CREATE  TABLE IF NOT EXISTS `PurchaseCollection` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `CashAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `CoinAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `CoinCount` TINYINT UNSIGNED NOT NULL ,
  `BillAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `BillCount` TINYINT UNSIGNED NOT NULL ,
  INDEX `idx_purchasecollection_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_purchasecollection_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_purchasecollection_multi` (`PointOfSaleId` ASC, `PurchaseGMT` ASC) ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_purchasecollection_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchasecollection_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `PurchaseCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PurchaseCollectionCancelled` ;

CREATE  TABLE IF NOT EXISTS `PurchaseCollectionCancelled` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `CashAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `CoinAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `CoinCount` TINYINT UNSIGNED NOT NULL ,
  INDEX `idx_purchasecollectioncancel_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_purchasecollectioncancel_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_purchasecollectioncancel_multi` (`PointOfSaleId` ASC, `PurchaseGMT` ASC) ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_purchasecollectioncancelled_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchasecollectioncancelled_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RoleStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RoleStatusType` ;

CREATE  TABLE IF NOT EXISTS `RoleStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_userstatustype_name_uq` (`Name` ASC) ,
  INDEX `idx_userstatustype_user` (`LastModifiedByUserId` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CryptoKey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CryptoKey` ;

CREATE  TABLE IF NOT EXISTS `CryptoKey` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `KeyType` SMALLINT UNSIGNED NOT NULL ,
  `KeyIndex` SMALLINT UNSIGNED NOT NULL ,
  `ExpiryGMT` DATETIME NOT NULL ,
  `Info` TEXT NULL ,
  `Comment` VARCHAR(255) NOT NULL ,
  `Status` TINYINT(1) NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_cryptokey_multi_uq` (`KeyType` ASC, `KeyIndex` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CryptoKeyHash`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CryptoKeyHash` ;

CREATE  TABLE IF NOT EXISTS `CryptoKeyHash` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CryptoKeyId` MEDIUMINT UNSIGNED NOT NULL,
  `HashAlgorithmTypeId` TINYINT UNSIGNED NOT NULL ,
  `Hash` VARCHAR(64) NULL ,
  `NextHash` VARCHAR(64) NULL ,
  `Signature` TEXT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_cryptokeyhash_multi_uq` (`CryptoKeyId` ASC, `HashAlgorithmTypeId` ASC),
  CONSTRAINT `fk_cryptokeyhash_cryptokey`
    FOREIGN KEY (`CryptoKeyId` )
    REFERENCES `CryptoKey` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cryptokeyhash_hashalgorithmtype`
    FOREIGN KEY (`HashAlgorithmTypeId` )
    REFERENCES `HashAlgorithmType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `HashAlgorithmType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `HashAlgorithmType` ;

CREATE  TABLE IF NOT EXISTS `HashAlgorithmType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `IsDeleted` TINYINT(1) NOT NULL DEFAULT 0,
  `ClassName` VARCHAR(50) NULL,
  `SignatureId` TINYINT UNSIGNED NULL,
  `IsForSigning` TINYINT(1) NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_hashalgorithmtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;







-- -----------------------------------------------------
-- Table `MerchantStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MerchantStatusType` ;

CREATE  TABLE IF NOT EXISTS `MerchantStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_merchantstatustype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PaystationSetting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaystationSetting` ;

CREATE  TABLE IF NOT EXISTS `PaystationSetting` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_paystationsetting_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_paystationsetting_multi_uq` (`CustomerId` ASC, `Name` ASC) ,
  CONSTRAINT `fk_paystationsetting_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RatesXref`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RatesXref`;

CREATE TABLE `RatesXref` (
  `CustomerId` mediumint(8) unsigned NOT NULL,
  `RateName` varchar(20) NOT NULL,
  `UnifiedRateId` mediumint(20) unsigned NOT NULL,
  PRIMARY KEY  (`CustomerId`,`RateName`,`UnifiedRateId`)
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ProcessorTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ProcessorTransaction`;

CREATE TABLE `ProcessorTransaction` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PurchaseId` BIGINT UNSIGNED NULL ,
  `PreAuthId` BIGINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchasedDate` DATETIME NOT NULL ,  
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardType` VARCHAR(20) NOT NULL ,
  `Last4DigitsOfCardNumber` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `CardChecksum` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0',
  `ProcessingDate` DATETIME NOT NULL ,
  `ProcessorTransactionId` VARCHAR(50) NULL ,
  `AuthorizationNumber` VARCHAR(30) DEFAULT NULL ,
  `ReferenceNumber` VARCHAR(30) NOT NULL DEFAULT '',
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CardHash` VARCHAR(30) NOT NULL DEFAULT '',
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CreatedGMT` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY  (`Id`),
  UNIQUE INDEX `idx_processortransaction_multi_uq` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`,`IsApproved`,`ProcessorTransactionTypeId`,`ProcessingDate`),
  INDEX `idx_processortransaction_multi_1` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`,`Last4DigitsOfCardNumber`),
  INDEX `idx_processortransaction_multi_2` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`,`CardHash`),
  INDEX `idx_processortransaction_preauth` (`PreAuthId` ASC) ,
  INDEX `idx_processortransaction_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_processortransaction_processortransactiontype` (`ProcessorTransactionTypeId` ASC) ,
  INDEX `idx_processortransaction_merchantaccount` (`MerchantAccountId` ASC) ,
  INDEX `idx_processortransaction_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_processortransaction_createdGMT` (`CreatedGMT` ASC) ,
  INDEX `idx_processortransaction_processingdate` (`ProcessingDate` ASC) ,
  CONSTRAINT `fk_processortransaction_preauth`
    FOREIGN KEY (`PreAuthId`)
    REFERENCES `PreAuth` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processortransaction_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processortransaction_processortransactiontype`
    FOREIGN KEY (`ProcessorTransactionTypeId`)
    REFERENCES `ProcessorTransactionType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processortransaction_merchantaccount`
    FOREIGN KEY (`MerchantAccountId`)
    REFERENCES `MerchantAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processortransaction_purchase`
    FOREIGN KEY (`PurchaseId`)
    REFERENCES `Purchase` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `PreAuth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreAuth`;

CREATE TABLE `PreAuth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ResponseCode` VARCHAR(50) DEFAULT '',
  `ProcessorTransactionId` VARCHAR(50) DEFAULT '',
  `AuthorizationNumber` VARCHAR(30) DEFAULT '',
  `ReferenceNumber` VARCHAR(30) DEFAULT '',
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL,
  `Amount` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0',
  `Last4DigitsOfCardNumber` SMALLINT UNSIGNED NOT NULL DEFAULT '0',
  `CardType` VARCHAR(20) NOT NULL DEFAULT '',
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `PreAuthDate` DATETIME NOT NULL,
  `CardData` VARCHAR(100) DEFAULT NULL,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL,
  `ReferenceId` VARCHAR(60) DEFAULT NULL,
  `Expired` TINYINT DEFAULT '0',
  `CardHash` VARCHAR(30) NOT NULL DEFAULT '',
  `ExtraData` VARCHAR(1336) DEFAULT '',
  `PsRefId` VARCHAR(10) DEFAULT NULL,
  `CardExpiry` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY  (`Id`),
  INDEX `idx_preauth_pointofsale` (`PointOfSaleId` ASC),
  INDEX `idx_preauth_merchantaccount` (`MerchantAccountId` ASC),
  INDEX `PreauthByAuthNumAndDate` (`Expired`,`IsApproved`,`PointOfSaleId`,`Amount`,`AuthorizationNumber`,`PreAuthDate`),
  INDEX `PreauthExpired` (`Expired`,`PreAuthDate`),
  INDEX `PsRefId` (`PointOfSaleId`, `PsRefId`, `Expired`, `IsApproved`),
  CONSTRAINT `fk_preauth_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_preauth_merchantaccount`
    FOREIGN KEY (`MerchantAccountId`)
    REFERENCES `MerchantAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `Reversal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Reversal`;

CREATE TABLE `Reversal` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL,
  `CardData` VARCHAR(100) DEFAULT NULL,
  `ExpiryDate` VARCHAR(10) DEFAULT NULL,
  `OriginalMessageType` VARCHAR(30) NOT NULL DEFAULT '',
  `OriginalProcessingCode` VARCHAR(30) NOT NULL DEFAULT '',
  `OriginalReferenceNumber` VARCHAR(30) NOT NULL DEFAULT '',
  `OriginalTime` VARCHAR(30) NOT NULL DEFAULT '',
  `OriginalTransactionAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0',
  `OriginalProcessorTransactionId` VARCHAR(50) DEFAULT '',
  `LastResponseCode` VARCHAR(20) DEFAULT '',
  `PurchasedDate` DATETIME NOT NULL,
  `TicketNumber` INT DEFAULT '0',
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL,
  `RetryAttempts` INT NOT NULL DEFAULT '0',
  `LastRetryTime` DATETIME NOT NULL,
  `IsSucceeded` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `IsExpired` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY  (`Id`),
  UNIQUE INDEX `idx_reversal_multi_uq` (`MerchantAccountId`,`OriginalReferenceNumber`,`OriginalTime`),
  INDEX `idx_reversal_multi` (`IsSucceeded`,`IsExpired`,`LastRetryTime`),
  INDEX `idx_reversal_merchantaccount` (`MerchantAccountId`),
  CONSTRAINT `fk_reversal_merchantaccount`
    FOREIGN KEY (`MerchantAccountId`)
    REFERENCES `MerchantAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ProcessorProperty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ProcessorProperty` ;

CREATE  TABLE IF NOT EXISTS `ProcessorProperty` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ProcessorId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `Value` VARCHAR(100) NOT NULL ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_processorproperty_processor` (`ProcessorId` ASC) ,
  UNIQUE INDEX `idx_processorproperty_multi_uq` (`ProcessorId` ASC, `Name` ASC, `Value` ASC) ,
  CONSTRAINT `fk_processorproperty_processor`
    FOREIGN KEY (`ProcessorId` )
    REFERENCES `Processor` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `POSAlert`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSAlert` ;

CREATE  TABLE IF NOT EXISTS `POSAlert` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerAlertTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `EventDeviceTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventActionTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventSeverityTypeId` TINYINT UNSIGNED NOT NULL ,
  `AlertGMT` DATETIME NOT NULL ,
  `AlertInfo` VARCHAR(50) NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL ,
  `IsSentEmail` TINYINT(1) UNSIGNED NOT NULL ,
  `SentEmailGMT` DATETIME NULL ,
  `ClearedGMT` DATETIME NULL ,
  `ClearedByUserId` MEDIUMINT UNSIGNED NULL ,
  `CreatedGMT` DATETIME NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_POSAlert_EventDeviceType` (`EventDeviceTypeId` ASC) ,
  INDEX `idx_POSAlert_EventStatusType` (`EventStatusTypeId` ASC) ,
  INDEX `idx_POSAlert_EventActionType` (`EventActionTypeId` ASC) ,
  INDEX `idx_POSAlert_PointOfSale` (`PointOfSaleId` ASC) ,
  INDEX `idx_POSAlert_EventSeverityType` (`EventSeverityTypeId` ASC) ,
  INDEX `idx_POSAlert_PointOfSaleAlert` (`PointOfSaleId` ASC, `AlertGMT` DESC) ,
  INDEX `idx_POSAlert_Multi` (`CustomerAlertTypeId` ASC, `PointOfSaleId` ASC, `EventDeviceTypeId` ASC, `EventStatusTypeId` ASC, `EventActionTypeId` ASC, `EventSeverityTypeId` ASC, `IsActive` DESC, `AlertGMT` ASC) ,
  INDEX `idx_POSAlert_CustomerAlertType` (`CustomerAlertTypeId` ASC) ,
  INDEX `ind_posalert_IsSentEmail`(`IsSentEmail`),
  INDEX  `ind_POSAlert_mult1`(`PointOfSaleId`, `EventDeviceTypeId`, `EventStatusTypeId`, `IsActive`) ,
  CONSTRAINT `fk_POSAlert_EventDeviceType`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlert_EventStatusType`
    FOREIGN KEY (`EventStatusTypeId` )
    REFERENCES `EventStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlert_EventActionType`
    FOREIGN KEY (`EventActionTypeId` )
    REFERENCES `EventActionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlert_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlert_EventSeverityType`
    FOREIGN KEY (`EventSeverityTypeId` )
    REFERENCES `EventSeverityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSAlert_CustomerAlertType`
    FOREIGN KEY (`CustomerAlertTypeId` )
    REFERENCES `CustomerAlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
COMMENT = 'The history of ALL alerts (the setting AND the clearing) for' /* comment truncated */;


-- -----------------------------------------------------
-- Table `CardRetryTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardRetryTransaction`;

CREATE TABLE `CardRetryTransaction` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL ,
  `PurchasedDate` datetime NOT NULL ,
  `TicketNumber` int(10) unsigned NOT NULL ,
  `LastRetryDate` datetime NOT NULL,
  `NumRetries` smallint(5) unsigned NOT NULL default '0',
  `CardHash` varchar(30) NOT NULL default '',
  `CardRetryTransactionTypeId` tinyint(3) UNSIGNED NOT NULL,
  `CardData` varchar(100) NOT NULL default '',
  `CardExpiry` smallint(5) unsigned NOT NULL default '0',
  `Amount` mediumint(8) unsigned NOT NULL default '0',
  `CardType` varchar(20) NOT NULL default '',
  `Last4DigitsOfCardNumber` smallint(5) unsigned NOT NULL default '0',
  `CreationDate` datetime NOT NULL,
  `BadCardHash` varchar(30) NOT NULL default '',
  `IgnoreBadCard` tinyint(1) unsigned NOT NULL default '0',
  `LastResponseCode` smallint(5) unsigned NOT NULL default '0',
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `idx_cardretrytransaction_Multi_Uq` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`),
  INDEX `idx_cardretrytransaction_CardData` (`CardData`),
  INDEX `idx_cardretrytransaction_GetTransForBadCard` (`PointOfSaleId`,`NumRetries`,`BadCardHash`),
  INDEX `idx_cardretrytransaction_GetBadCards` (`PointOfSaleId`,`NumRetries`,`CardExpiry`,`IgnoreBadCard`),
  INDEX `idx_cardretrytransaction_GetTransToRetry` (`PointOfSaleId`,`NumRetries`,`CardExpiry`,`LastRetryDate`),
  INDEX `idx_cardretrytransaction_PointOfSale` (`PointOfSaleId`),
  INDEX `idx_cardretrytransaction_CardRetryTransactionType` (`CardRetryTransactionTypeId`),
  CONSTRAINT `fk_cardretrytransaction_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cardretrytransaction_CardRetryTransactionType`
    FOREIGN KEY (`CardRetryTransactionTypeId` )
    REFERENCES `CardRetryTransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ReversalArchive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReversalArchive`;

CREATE TABLE `ReversalArchive` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `MerchantAccountId` MEDIUMINT(11) UNSIGNED NOT NULL,
  `CardNumber` varchar(120) default NULL,
  `ExpiryDate` varchar(10) default NULL,
  `OriginalMessageType` varchar(30) NOT NULL default '',
  `OriginalProcessingCode` varchar(30) NOT NULL default '',
  `OriginalReferenceNumber` varchar(30) NOT NULL default '',
  `OriginalTime` varchar(30) NOT NULL default '',
  `OriginalTransactionAmount` MEDIUMINT UNSIGNED NOT NULL default '0',
  `LastResponseCode` varchar(20) default '',
  `PurchasedDate` datetime NOT NULL,
  `TicketNumber` int(11) default '0',
  `PointOfSaleId` MEDIUMINT(11) UNSIGNED NULL,
  `RetryAttempts` int(11) NOT NULL default '0',
  `LastRetryTime` datetime NOT NULL,
  `Succeeded` tinyint(1) NOT NULL default '0',
  `Expired` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`Id`),
  UNIQUE INDEX `idx_reversalarchive_legacy_pk`  (`MerchantAccountId`,`OriginalReferenceNumber`,`OriginalTime`),
  INDEX `idx_reversalarchive_pointofsale` (`PointOfSaleId`),
  INDEX `idx_reversalarchive_merchantaccount` (`MerchantAccountId`),
  CONSTRAINT `fk_reversalarchive_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reversalarchive_merchantaccount`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `PreAuthHolding`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PreAuthHolding`;

CREATE TABLE `PreAuthHolding` (
  `Id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
  `PreAuthId` BIGINT unsigned DEFAULT NULL,
  `ResponseCode` varchar(50) DEFAULT NULL,
  `ProcessorTransactionId` varchar(50) DEFAULT '',
  `AuthorizationNumber` varchar(30) DEFAULT NULL,
  `ReferenceNumber` varchar(30) DEFAULT NULL,
  `MerchantAccountId` mediumint(8) unsigned DEFAULT NULL,
  `Amount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Last4DigitsOfCardNumber` smallint(5) unsigned NOT NULL DEFAULT '0',
  `CardType` varchar(20) NOT NULL,
  `Approved` tinyint(1) NOT NULL DEFAULT '0',
  `PreAuthDate` datetime NOT NULL,
  `CardData` varchar(100) DEFAULT NULL,
  `PointOfSaleId` mediumint(8) unsigned DEFAULT NULL,
  `ReferenceId` varchar(60) DEFAULT NULL,
  `Expired` tinyint(4) DEFAULT '0',
  `CardHash` varchar(30) NOT NULL,
  `ExtraData` varchar(1336) DEFAULT NULL,
  `PsRefId` varchar(10) DEFAULT NULL,
  `IsRFID` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CardExpiry` smallint(5) unsigned NOT NULL,
  `AddedToHoldingGMT` datetime NOT NULL,
  `MovedToRetryGMT` datetime DEFAULT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`),
  KEY `idx_preauthholding_DateAdded` (`AddedToHoldingGMT`,`PointOfSaleId`),
  KEY `idx_preauthholding_pointofsale` (`PointOfSaleId`),
  KEY `idx_preauthholding_merchantaccount` (`MerchantAccountId`),
  KEY `fk_preauthholding_preauth` (`PreAuthId`),
  CONSTRAINT `fk_preauthholding_merchantaccount` FOREIGN KEY (`MerchantAccountId`) REFERENCES `MerchantAccount` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_preauthholding_pointofsale` FOREIGN KEY (`PointOfSaleId`) REFERENCES `PointOfSale` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------------------------------
-- Table `CCFailLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CCFailLog`;

CREATE TABLE `CCFailLog` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `MerchantAccountId` mediumint(8) unsigned NOT NULL,
  `TicketNumber` int(10) unsigned default 0,
  `ProcessingDate` datetime NOT NULL,
  `PurchasedDate` datetime NOT NULL,
  `CCType` tinyint(3) unsigned NOT NULL,
  `Reason` varchar(100) default NULL,
  PRIMARY KEY  (`Id`),
  INDEX `idx_ccfaillog_PSID` (`PointOfSaleId`,`MerchantAccountId`,`PurchasedDate`,`TicketNumber`),
  INDEX `idx_ccfaillog_pointofsale` (`PointOfSaleId`) ,
  INDEX `idx_ccfaillog_merchantaccount` (`MerchantAccountId`) ,
  CONSTRAINT `fk_ccfaillog_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ccfaillog_merchantaccount`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `SCRetry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SCRetry`;

CREATE TABLE `SCRetry` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `PurchasedDate` datetime NOT NULL,
  `TicketNumber` int(11) NOT NULL,
  `SCType` varchar(20) NOT NULL,
  `RetryServer` varchar(20) NOT NULL,
  `MerchantInfo` varchar(20) default NULL,
  `SettlementInfo` varchar(558) NOT NULL,
  PRIMARY KEY  (`Id`) ,
  UNIQUE INDEX `idx_scretry_legacy_pk`  (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`) ,
  INDEX `idx_scretry_pointofsale` (`PointOfSaleId`) ,
  CONSTRAINT `fk_scretry_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `CardRetryTransactionType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardRetryTransactionType`;

CREATE TABLE `CardRetryTransactionType` (
  `Id` tinyint(3) unsigned NOT NULL ,
  `TypeName` varchar(20) NOT NULL,
  `Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_cardretrytransactiontype_typename_uq` (`TypeName`) 
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ActivePermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ActivePermit` ;

CREATE  TABLE IF NOT EXISTS `ActivePermit` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `OriginalPermitId` BIGINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingName` VARCHAR(20) NOT NULL ,
  `LicencePlateNumber` VARCHAR(15)  NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED  NULL ,
  `AddTimeNumber` INT UNSIGNED  NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitOriginalExpireGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `NumberOfExtensions` TINYINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME DEFAULT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_activepermit_plate_info_by_plate` (`CustomerId` ASC, `LicencePlateNumber` ASC) ,
  INDEX `idx_activepermit_space_status` (`PointOfSaleId` ASC, `SpaceNumber` ASC, `PermitExpireGMT` ASC, `PaystationSettingName` ASC) ,
  INDEX `idx_activepermit_add_time_number_status` (`PointOfSaleId` ASC, `AddTimeNumber` ASC, `PermitExpireGMT` ASC, `PaystationSettingName` ASC) ,
  INDEX `idx_activepermit_space_status_by_purchased_date` (`PointOfSaleId` ASC, `SpaceNumber` ASC, `PermitBeginGMT` ASC, `PaystationSettingName` ASC) ,
  INDEX `idx_activepermit_customer` (`CustomerId` ASC) ,
  INDEX `idx_activepermit_location` (`PermitLocationId` ASC) ,
  INDEX `idx_activepermit_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_activepermit_permit` (`OriginalPermitId` ASC) ,
  INDEX `idx_activepermit_paystationsettingname` (`CustomerId` ASC, `PaystationSettingName` ASC, `PermitExpireGMT` ASC) ,
  INDEX `idx_activepermit_licenceplatenumber` (`CustomerId` ASC, `LicencePlateNumber` ASC, `PermitExpireGMT` ASC) ,
  INDEX `ind_ActivePermit_Loc_PlateNumber`(`PermitLocationId`, `LicencePlateNumber`),
  INDEX `ind_ActivePermit_Loc_SpaceNumber`(`PermitLocationId`, `SpaceNumber`) ,
  CONSTRAINT `fk_activepermit_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activepermit_location`
    FOREIGN KEY (`PermitLocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activepermit_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activepermit_permit`
    FOREIGN KEY (`OriginalPermitId` )
    REFERENCES `Permit` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SCType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SCType`;

CREATE TABLE `SCType` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` varchar(20) NOT NULL ,
  `ProcessorId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) , 
  UNIQUE INDEX `idx_sctype_name_uq` (`Name` ASC) ,
  INDEX `idx_sctype_processor` (`ProcessorId`) ,
  CONSTRAINT `fk_sctype_processor`
    FOREIGN KEY (`ProcessorId` )
    REFERENCES `Processor` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB ;

-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- REPORTING Tables need to be reviewed
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


-- -----------------------------------------------------
-- Table `ReportType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportType` ;

CREATE  TABLE IF NOT EXISTS `ReportType` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(40) NOT NULL ,
  `AllowSystemAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `AllowParentCustomerAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `AllowChildCustomerAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_reporttype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportRepeatType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportRepeatType` ;

CREATE  TABLE IF NOT EXISTS `ReportRepeatType` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportFilterType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportFilterType` ;

CREATE  TABLE IF NOT EXISTS `ReportFilterType` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ReportFilterValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportFilterValue` ;

CREATE  TABLE IF NOT EXISTS `ReportFilterValue` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportFilterValue` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportFilterTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_reportfiltervalue_reportfiltertype`
    FOREIGN KEY (`ReportFilterTypeId` )
    REFERENCES `ReportFilterType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ReportLocationValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportLocationValue` ;

CREATE  TABLE IF NOT EXISTS `ReportLocationValue` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ObjectId` MEDIUMINT UNSIGNED NOT NULL ,
  `ObjectType` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_reportlocationvalue_reportdefinitiontype`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportDefinition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportDefinition` ;

CREATE  TABLE IF NOT EXISTS `ReportDefinition` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `ReportRepeatTimeOfDay` CHAR(5) NULL ,
  `ReportRepeatDays` VARCHAR(83) NULL ,
  `ReportRepeatFrequency` TINYINT UNSIGNED NULL ,
  `Title` VARCHAR(80) NULL ,
  `ReportTypeId` SMALLINT UNSIGNED NOT NULL ,
  `IsDetailsOrSummary` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsDetailsOrSummary: 0=Details,1=Summary' ,
  `PrimaryDateFilterTypeId` MEDIUMINT UNSIGNED NULL COMMENT 'Transaction Date, Procesing Date, Retry Date or Date depending on report type' ,
  `PrimaryDateRangeBeginGMT` DATETIME NULL ,
  `PrimaryDateRangeEndGMT` DATETIME NULL ,
  `SecondaryDateFilterTypeId` MEDIUMINT UNSIGNED NULL COMMENT 'Transaction Date for report requiring 2 sets of dates' ,
  `SecondaryDateRangeBeginGMT` DATETIME NULL ,
  `SecondaryDateRangeEndGMT` DATETIME NULL ,
  `PaystationSettingIdFilter` MEDIUMINT UNSIGNED NULL ,
  `SpaceNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `SpaceNumberBeginFilter` MEDIUMINT UNSIGNED NULL,
  `SpaceNumberEndFilter`MEDIUMINT UNSIGNED NULL,
  `LocationFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardNumberSpecificFilter` VARCHAR(40) NULL,
  `CardTypeId` MEDIUMINT UNSIGNED NULL ,
  `ApprovalStatusFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CardMerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `LicencePlateFilter` VARCHAR(40) NULL ,
  `TicketNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `TicketNumberBeginFilter` int(10) UNSIGNED NULL,
  `TicketNumberEndFilter` int(10) UNSIGNED NULL,
  `CouponNumberFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `CouponNumberSpecificFilter` VARCHAR(40) NULL ,
  `CouponTypeFilterTypeId` int(10) UNSIGNED NULL,
  `AccountFilterTypeId` int(10) UNSIGNED NULL,
  `AccountNameFilter` VARCHAR(50) NULL,
  `EventSeverityTypeId` TINYINT UNSIGNED NULL,
  `EventDeviceTypeId` TINYINT UNSIGNED NULL ,
  `ReportCollectionFilterTypeId` SMALLINT UNSIGNED NULL,
  `OtherParametersTypeFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `GroupByFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `SortByFilterTypeId` MEDIUMINT UNSIGNED NULL ,
  `IsOnlyVisible` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsOnlyVisible: 0=Show also hidden, 1=Show only visible' ,
  `IsCsvOrPdf` TINYINT(1) UNSIGNED NOT NULL COMMENT 'IsCsvOrPdf: 0=Csv, 1=Pdf' ,
  `IsAdhoc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=Normal, 1=Adhoc' ,
  `IsSystemAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'IsSystemAdmin: 0=Customer Report, 1=CS Report' ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `ScheduledToRunGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idk_reportdefinition_reporttype` (`ReportTypeId` ASC) ,
  INDEX `idk_reportdefinition_useraccount` (`UserAccountId` ASC) ,
  INDEX `idk_reportdefinition_reportrepeattype` (`ReportRepeatTypeId` ASC) ,
  INDEX `idk_reportdefinition_reportstatustype` (`ReportStatusTypeId` ASC) ,
  CONSTRAINT `fk_reportdefinition_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reporttype`
    FOREIGN KEY (`ReportTypeId` )
    REFERENCES `ReportType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportdefinition_reportcollectionfiltertype`
    FOREIGN KEY (`ReportCollectionFilterTypeId` )
    REFERENCES `ReportCollectionFilterType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportCollectionFilterType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportCollectionFilterType` ;

CREATE  TABLE IF NOT EXISTS `ReportCollectionFilterType` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `IsForOverdue` TINYINT(1) UNSIGNED NOT NULL,
  `IsForCardAmount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForCardCount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForBillAmount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForBillCount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForCoinAmount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForCoinCount` TINYINT(1) UNSIGNED NOT NULL,
  `IsForRunningTotal` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportStatusType` ;

CREATE  TABLE IF NOT EXISTS `ReportStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_reporttype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportQueue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportQueue` ;

CREATE  TABLE IF NOT EXISTS `ReportQueue` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `PrimaryDateRangeBeginGMT` DATETIME NULL ,
  `PrimaryDateRangeEndGMT` DATETIME NULL ,
  `SecondaryDateRangeBeginGMT` DATETIME NULL ,
  `SecondaryDateRangeEndGMT` DATETIME NULL ,
  `ScheduledBeginGMT` DATETIME NOT NULL ,
  `QueuedGMT` DATETIME NOT NULL ,
  `ExecutionBeginGMT` DATETIME NULL ,
  `LockId` VARCHAR(20) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_reportqueue_reportstatustype` (`ReportStatusTypeId` ASC) ,
  INDEX `idx_reportqueue_customer` (`CustomerId` ASC) ,
  INDEX `idx_reportqueue_useraccount` (`UserAccountId` ASC) ,
  UNIQUE INDEX `idx_reportqueue_reportdefinition` (`ReportDefinitionId` ASC) ,
  INDEX `idx_reportqueue_scheduled` (`ScheduledBeginGMT` ASC) ,
  CONSTRAINT `fk_reportqueue_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportqueue_reportdefinition`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportHistory` ;

CREATE  TABLE IF NOT EXISTS `ReportHistory` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `ReportDateRangeBeginGMT` DATETIME NULL ,
  `ReportDateRangeEndGMT` DATETIME NULL ,
  `ReportRepeatTypeId` TINYINT UNSIGNED NOT NULL ,
  `ScheduledBeginGMT` DATETIME NOT NULL ,
  `QueuedGMT` DATETIME NOT NULL ,
  `ExecutionBeginGMT` DATETIME NOT NULL ,
  `ExecutionEndGMT` DATETIME NOT NULL COMMENT 'ExecutionEndGMT: when the execution of a report ends. ReportStatusTypeId will specify the status of the report when it is done execution: (3) Completed, (4) Canceled, (5) Failed.' ,
  `SqlBeginGMT` DATETIME NULL ,
  `SqlEndGMT` DATETIME NULL COMMENT 'SqlEndGMT: when the execution of a sql ends.' ,
  `FileSizeKb` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `StatusExplanation` VARCHAR(255) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_reporthistory_customer` (`CustomerId` ASC) ,
  INDEX `fk_reporthistory_useraccount` (`UserAccountId` ASC) ,
  INDEX `fk_reporthistory_reportdefinition` (`ReportDefinitionId` ASC) ,
  INDEX `fk_reporthistory_reportstatustype` (`ReportStatusTypeId` ASC) ,
  CONSTRAINT `fk_reporthistory_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportdefinition`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportrepeattype`
    FOREIGN KEY (`ReportRepeatTypeId` )
    REFERENCES `ReportRepeatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporthistory_reportstatustype`
    FOREIGN KEY (`ReportStatusTypeId` )
    REFERENCES `ReportStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportRepository`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportRepository` ;

CREATE  TABLE IF NOT EXISTS `ReportRepository` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportHistoryId` INT UNSIGNED NOT NULL ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `AddedGMT` DATETIME NOT NULL ,
  `FileSizeKb` SMALLINT UNSIGNED NOT NULL ,
  `FileName` VARCHAR(60) NOT NULL ,
  `IsReadByUser` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsSoonToBeDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_reportrepository_useraccount` (`UserAccountId` ASC) ,
  INDEX `fk_reportrepository_reporthistory` (`ReportHistoryId` ASC) ,
  CONSTRAINT `fk_reportrepository_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportrepository_reporthistory`
    FOREIGN KEY (`ReportHistoryId` )
    REFERENCES `ReportHistory` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportContent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportContent` ;

CREATE  TABLE IF NOT EXISTS `ReportContent` (
  `ReportRepositoryId` INT UNSIGNED NOT NULL ,
  `Content` MEDIUMBLOB NOT NULL ,
  INDEX `fk_reportcontent_reportrepository` (`ReportRepositoryId` ASC) ,
  PRIMARY KEY (`ReportRepositoryId`) ,
  CONSTRAINT `fk_reportcontent_reportrepository`
    FOREIGN KEY (`ReportRepositoryId` )
    REFERENCES `ReportRepository` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReportEmail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReportEmail` ;

CREATE  TABLE IF NOT EXISTS `ReportEmail` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ReportDefinitionId` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerEmailId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  INDEX `fk_reportemail_reportdefinition` (`ReportDefinitionId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_reportemail_customeremail` (`CustomerEmailId` ASC) ,
  CONSTRAINT `fk_reportemail_reportdefinition0`
    FOREIGN KEY (`ReportDefinitionId` )
    REFERENCES `ReportDefinition` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reportemail_customeremail`
    FOREIGN KEY (`CustomerEmailId` )
    REFERENCES `CustomerEmail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerMigrationStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationStatusType` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customermigrationstatustype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMigrationValidationStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationValidationStatusType` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationValidationStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CustomerMigrationValidationStatusType_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerMigrationFailureType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationFailureType` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationFailureType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customermigrationfailuretype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMigration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigration` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigration` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `MigrationAdminUserId` MEDIUMINT UNSIGNED NULL ,
  `CustomerMigrationStatusTypeId` TINYINT UNSIGNED NOT NULL ,
  `MigrationRequestedGMT` DATETIME NULL ,
  `MigrationScheduledGMT` DATETIME NULL ,  
  `CustomerMigrationValidationStatusTypeId` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `IsContacted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `CustomerBoardedGMT` DATETIME NULL ,
  `MigrationStartGMT` DATETIME NULL ,
  `MigrationEndGMT` DATETIME NULL ,
  `MigrationCancelGMT` DATETIME NULL ,
  `CustomerMigrationFailureTypeId` TINYINT UNSIGNED NULL ,
  `FailureCount` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `IsMigrationInProgress` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `StartEmailSentGMT` DATETIME NULL ,
  `EMS6CustomerLockedGMT` DATETIME NULL ,
  `DataMigrationConfirmedGMT` DATETIME NULL ,
  `EMS7CustomerEnabledGMT` DATETIME NULL ,
  `HeartbeatsResetGMT` DATETIME NULL ,
  `EMS6ForwardFlagSetGMT` DATETIME NULL ,
  `CompletionEmailSentGMT` DATETIME NULL ,
  `PaystationHeartbeatSetGMT` DATETIME NULL ,
  `CommunicationEmailSentGMT` DATETIME NULL ,
  `BackGroundJobNextTryGMT` DATETIME NULL ,
  `TotalUserLoggedMinutes` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `CommunicatedPaystationCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `TotalOnlinePaystationCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `TotalPaystationCount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `AllPayStationsCommunicatedGMT` DATETIME NULL ,
  `CreatedGMT` DATETIME NOT NULL DEFAULT '2012-01-01 00:00:00',
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customermigration_customer` (`CustomerId` ASC) ,
  INDEX `idx_customermigration_useraccount` (`MigrationAdminUserId` ASC) ,
  INDEX `idx_customermigration_customermigrationstatustype` (`CustomerMigrationStatusTypeId` ASC) ,
  INDEX `idx_customermigration_customermigrationfailuretype` (`CustomerMigrationFailureTypeId` ASC) ,
  CONSTRAINT `fk_customermigration_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customermigration_useraccount`
    FOREIGN KEY (`MigrationAdminUserId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customermigration_customermigrationstatustype` 
    FOREIGN KEY (`CustomerMigrationStatusTypeId`) 
    REFERENCES `CustomerMigrationStatusType` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customermigration_cmvalidationstatustype` 
    FOREIGN KEY (`CustomerMigrationValidationStatusTypeId`) 
    REFERENCES `CustomerMigrationValidationStatusType` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customermigration_customermigrationfailuretype` 
    FOREIGN KEY (`CustomerMigrationFailureTypeId`) 
    REFERENCES `CustomerMigrationFailureType` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMigrationEmail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationEmail` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationEmail` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerMigrationId` MEDIUMINT UNSIGNED NOT NULL,
  `CustomerEmailId` MEDIUMINT UNSIGNED NOT NULL,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_CustomerMigrationEmail_CustomerMigration`
    FOREIGN KEY (`CustomerMigrationId` )
    REFERENCES `CustomerMigration` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerMigrationEmail_CustomerEmail`
    FOREIGN KEY (`CustomerEmailId` )
    REFERENCES `CustomerEmail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerMigrationValidationType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationValidationType` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationValidationType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(50) NOT NULL ,
  `Warning` VARCHAR(50) NOT NULL ,
  `URL` VARCHAR(50) NOT NULL ,
  `IsBlocking` TINYINT(1) NOT NULL ,
  `IsUserFacing` TINYINT(1) NOT NULL ,
  `IsSystemWide` TINYINT(1) NOT NULL ,
  `IsDuringMigration` TINYINT(1) NOT NULL ,
  `IsITEmail` TINYINT(1) NOT NULL ,
  `MethodToCall` VARCHAR(50) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CustomerMigrationValidationType_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMigrationValidationStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerMigrationValidationStatus` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationValidationStatus` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerMigrationId` MEDIUMINT UNSIGNED NOT NULL,
  `CustomerMigrationValidationTypeId` TINYINT UNSIGNED NOT NULL ,
  `isPassed` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, 
  `LastFailureGMT` DATETIME NULL ,
  `LastSuccessGMT` DATETIME NULL ,
  `LastTryGMT` DATETIME NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CustomerMigrationValidationStatus_multi_uq` (`CustomerMigrationId` ASC, `CustomerMigrationValidationTypeId` ASC),
  CONSTRAINT `fk_CustomerMigrationValidationStatus_CustomerMigration`
    FOREIGN KEY (`CustomerMigrationId` )
    REFERENCES `CustomerMigration` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMValidationStatus_CMValidationType`
    FOREIGN KEY (`CustomerMigrationValidationTypeId` )
    REFERENCES `CustomerMigrationValidationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerMigrationEMS6ModifiedRecord`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CustomerMigrationEMS6ModifiedRecord` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationEMS6ModifiedRecord` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `EMS6CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TableId` INT UNSIGNED NOT NULL ,
  `TableName` VARCHAR(30) NOT NULL ,
  `ColumnName` VARCHAR(30) NOT NULL ,
  `Value` VARCHAR(30) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customermigrationems6modifiedrecord_multi_uq1` (`CustomerId`, `TableId`, `TableName`, `ColumnName`) ,
  CONSTRAINT `fk_customermigrationems6modifiedrecord_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerMigrationOnlinePOS`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CustomerMigrationOnlinePOS` ;

CREATE  TABLE IF NOT EXISTS `CustomerMigrationOnlinePOS` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_customermigrationonlinepos_multi_uq1` (`CustomerId`, `PointOfSaleId`) ,
  CONSTRAINT `fk_customermigrationonlinepos_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customermigrationonlinepos_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestLog`;

CREATE TABLE `RestLog` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `RestAccountId` mediumint(8) unsigned NOT NULL,
  `CustomerId` mediumint(8) unsigned NOT NULL,
  `EndpointName` varchar(50) DEFAULT NULL,
  `LoggingDate` datetime NOT NULL,
  `IsError` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TotalCalls` int(10) unsigned NOT NULL DEFAULT '0',
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
   UNIQUE KEY `idx_restlog_multi_uq` (`RestAccountId`,`EndpointName`,`LoggingDate`,`IsError`,`CustomerId`),
   PRIMARY KEY (`Id`),
   CONSTRAINT `fk_restlog_customer` 
     FOREIGN KEY (`CustomerId`) 
     REFERENCES `Customer` (`Id`) 
     ON DELETE NO ACTION 
     ON UPDATE NO ACTION,
   CONSTRAINT `fk_restlog_restaccount` 
     FOREIGN KEY (`RestAccountId`) 
     REFERENCES `RestAccount` (`Id`) 
     ON DELETE NO ACTION 
     ON UPDATE NO ACTION ) 
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RestLogTotalCall`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestLogTotalCall`;

CREATE TABLE `RestLogTotalCall` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `RestAccountId` mediumint(8) unsigned NOT NULL,
  `LoggingDate` datetime NOT NULL,
  `TotalCalls` int(10) unsigned NOT NULL DEFAULT '0',
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
   UNIQUE KEY `idx_restlogtotalcall_multi_uq` (`RestAccountId`,`LoggingDate`),
   PRIMARY KEY (`Id`),
   CONSTRAINT `fk_restlogtotalcall_restaccount` 
     FOREIGN KEY (`RestAccountId`) 
     REFERENCES `RestAccount` (`Id`) 
     ON DELETE NO ACTION 
     ON UPDATE NO ACTION ) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestSessionToken`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestSessionToken`;

CREATE TABLE `RestSessionToken` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `RestAccountId` mediumint(8) unsigned NOT NULL,
  `SessionToken` varchar(32) NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `ExpiryDate` datetime DEFAULT NULL,
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`),
  CONSTRAINT `fk_restsessiontoken_restaccount` 
    FOREIGN KEY (`RestAccountId`) 
    REFERENCES `RestAccount` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION ) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerWebServiceCal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerWebServiceCal`;

CREATE TABLE `CustomerWebServiceCal` (
  `Id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `CustomerId` mediumint(8) unsigned NOT NULL,
  `WebServiceEndPointTypeId` tinyint(3) unsigned NOT NULL,
  `CalInUse` int(11) DEFAULT NULL,
  `CalPurchase` int(11) DEFAULT NULL,
  `Description` varchar(80) DEFAULT NULL,
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`),
  KEY `idx_customerwebservicecal_webserviceendpointtype` (`WebServiceEndPointTypeId`),
  KEY `idx_customerwebservicecal_customer` (`CustomerId`),
  CONSTRAINT `fk_customerwebservicecal_webserviceendpointtype` 
    FOREIGN KEY (`WebServiceEndPointTypeId`) 
    REFERENCES `WebServiceEndPointType` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customerwebservicecal_customer` 
    FOREIGN KEY (`CustomerId`) 
    REFERENCES `Customer` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION ) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestProperty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RestProperty`;

CREATE TABLE `RestProperty` (
  `Id` tinyint(8) unsigned NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Value` varchar(100) NOT NULL,
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `idx_restproperty_uq` (`Name`) ) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WebServiceCallLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WebServiceCallLog`;

CREATE TABLE `WebServiceCallLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `WebServiceEndPointId` MEDIUMINT UNSIGNED NOT NULL,
  `DateLocal` DATETIME NOT NULL,
  `TotalCalls` MEDIUMINT UNSIGNED NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `idx_webservicecalllog_uq` (`WebServiceEndPointId`, `DateLocal`),
  KEY `idx_webservicecalllog_webserviceendpoint` (`WebServiceEndPointId`),
  CONSTRAINT `fk_webservicecalllog_webserviceendpoint` 
    FOREIGN KEY (`WebServiceEndPointId`) 
    REFERENCES `WebServiceEndPoint` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION ) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReplenishType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ReplenishType` ;

CREATE  TABLE IF NOT EXISTS `ReplenishType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_replenishtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Replenish`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Replenish`;

CREATE TABLE `Replenish` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `ReplenishTypeId` tinyint(3) unsigned NOT NULL,
  `ReplenishGMT` datetime NOT NULL,
  `Number` int(10) unsigned NOT NULL,
  `Tube1Type` tinyint(3) unsigned NOT NULL default '0',
  `Tube1ChangedCount` smallint(5) unsigned NOT NULL default '0',
  `Tube1CurrentCount` smallint(5) unsigned NOT NULL default '0',
  `Tube2Type` tinyint(3) unsigned NOT NULL default '0',
  `Tube2ChangedCount` smallint(5) unsigned NOT NULL default '0',
  `Tube2CurrentCount` smallint(5) unsigned NOT NULL default '0',
  `Tube3Type` tinyint(3) unsigned NOT NULL default '0',
  `Tube3ChangedCount` smallint(5) unsigned NOT NULL default '0',
  `Tube3CurrentCount` smallint(5) unsigned NOT NULL default '0',
  `Tube4Type` tinyint(3) unsigned NOT NULL default '0',
  `Tube4ChangedCount` smallint(5) unsigned NOT NULL default '0',
  `Tube4CurrentCount` smallint(5) unsigned NOT NULL default '0',
  `CoinBag005AddedCount` smallint(5) unsigned NOT NULL default '0',
  `CoinBag010AddedCount` smallint(5) unsigned NOT NULL default '0',
  `CoinBag025AddedCount` smallint(5) unsigned NOT NULL default '0',
  `CoinBag100AddedCount` smallint(5) unsigned NOT NULL default '0',
  `CoinBag200AddedCount` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_replenish_multi_uq` (`PointOfSaleId`,`ReplenishGMT`,`Number`) ,
  INDEX `idx_replenish_pointofsale` (`PointOfSaleId`) ,
  INDEX `idx_replenish_replenishtype` (`ReplenishTypeId`) ,
  CONSTRAINT `fk_replenish_pointofsale` 
    FOREIGN KEY (`PointOfSaleId`) 
    REFERENCES `PointOfSale` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION ,
  CONSTRAINT `fk_replenish_replenishtype` 
    FOREIGN KEY (`ReplenishTypeId`) 
    REFERENCES `ReplenishType` (`Id`) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `POSBatteryInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSBatteryInfo` ;

CREATE  TABLE IF NOT EXISTS `POSBatteryInfo` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `SensorGMT` DATETIME NOT NULL ,
  `SystemLoad` FLOAT NOT NULL ,
  `InputCurrent` FLOAT NOT NULL ,
  `BatteryVoltage` FLOAT NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_posbatteryinfo_pointofsale` (`PointOfSaleId` ASC) ,
  UNIQUE INDEX `idx_posbatteryinfo_multi_uq` (`PointOfSaleId` ASC, `SensorGMT` ASC) ,
  CONSTRAINT `fk_posbatteryinfo_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ImportError`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ImportError` ;

CREATE  TABLE IF NOT EXISTS `ImportError` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NULL ,
  `ImportData` TEXT NOT NULL ,
  `ImportDataHash` VARCHAR(32) NOT NULL ,
  `Comment` TEXT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_importerror_customer` (`CustomerId` ASC) ,
  INDEX `idx_importerror_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_importerror_multi_uq` (`CustomerId` ASC, `PointOfSaleId` ASC, `CreatedGMT` ASC) ,
  CONSTRAINT `fk_importerror_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_importerror_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `LocationPOSLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LocationPOSLog` ;

CREATE  TABLE IF NOT EXISTS `LocationPOSLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `AssignedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_locationposlog_location` (`LocationId` ASC) ,
  INDEX `idx_locationposlog_pointofsale` (`PointOfSaleId` ASC) ,
  UNIQUE INDEX `idx_locationposlog_multi_uq_1` (`LocationId` ASC, `PointOfSaleId` ASC, `AssignedGMT` ASC) ,
  UNIQUE INDEX `idx_locationposlog_multi_uq_2` (`PointOfSaleId` ASC, `AssignedGMT` ASC, `LocationId` ASC) ,
  CONSTRAINT `fk_locationposlog_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locationposlog_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RawSensorData`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RawSensorData`;

CREATE TABLE `RawSensorData` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ServerName` varchar(20) NOT NULL,
  `SerialNumber` varchar(20) NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Action` varchar(40) NOT NULL,
  `Information` varchar(40) NOT NULL,
  `DateField` datetime NOT NULL,
  `LastRetryTime` datetime NOT NULL,
  `RetryCount` int(10) unsigned NOT NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY  (`Id`),
  INDEX `idx_rawsensordata1_legacy_pk` (`ServerName`,`SerialNumber`,`Type`,`Action`,`Information`,`DateField`),
  INDEX `idx_rawsensordata1_SensorForServer` USING BTREE (`ServerName`,`LastRetryTime`)
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `RawSensorDataArchive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RawSensorDataArchive`;

CREATE TABLE `RawSensorDataArchive` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ServerName` varchar(20) NOT NULL,
  `SerialNumber` varchar(20) NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Action` varchar(40) NOT NULL,
  `Information` varchar(40) NOT NULL,
  `DateField` datetime NOT NULL,
  `LastRetryTime` datetime NOT NULL,
  `RetryCount` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`Id`),
  INDEX `idx_rawsensordataarchive_legacy_pk` (`ServerName`,`SerialNumber`,`Type`,`Action`,`Information`,`DateField`),
  INDEX `idx_rawsensordataarchive_SensorForServer` USING BTREE (`ServerName`,`LastRetryTime`)
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `ThirdPartyServiceAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ThirdPartyServiceAccount`;

CREATE TABLE `ThirdPartyServiceAccount` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Type` varchar(45) NOT NULL,
  `EndPointUrl` varchar(100) NOT NULL,
  `UserName` varchar(50) default NULL,
  `Password` varchar(50) default NULL,
  `Field1` varchar(50) DEFAULT NULL,
  `Field2` varchar(50) DEFAULT NULL,
  `IsPaystationBased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY  (`Id`),
  INDEX `idx_thirdpartyserviceaccount_customer` (`CustomerId`),
  CONSTRAINT `fk_thirdpartyserviceaccount_customer`
    FOREIGN KEY (`CustomerId`)
    REFERENCES `Customer` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ThirdPartyServicePOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ThirdPartyServicePOS`;

CREATE  TABLE `ThirdPartyServicePOS` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ThirdPartyServiceAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_thirdpartyservicepos_multi_uq` (`ThirdPartyServiceAccountId`, `PointOfSaleId`) ,
  INDEX `idx_thirdpartyservicepos_thirdpartyserviceaccount` (`ThirdPartyServiceAccountId`) ,
  INDEX `idx_thirdpartyservicepos_pointofsale` (`PointOfSaleId`) ,
  CONSTRAINT `fk_thirdpartyservicepos_thirdpartyserviceaccount`
    FOREIGN KEY (`ThirdPartyServiceAccountId`)
    REFERENCES `ThirdPartyServiceAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION ,
  CONSTRAINT `fk_thirdpartyservicepos_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TransactionPush`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TransactionPush`;

CREATE TABLE `TransactionPush` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` mediumint(8) unsigned NOT NULL ,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL ,
  `PurchasedDate` datetime NOT NULL ,
  `TicketNumber` int(10) unsigned NOT NULL ,
  `PaystationSettingName` varchar(20) NOT NULL default '',
  `PlateNumber` varchar(10) default '',
  `ExpiryDate` datetime NOT NULL ,
  `ChargedAmount` mediumint(8) unsigned NOT NULL default '0',
  `StallNumber` mediumint(8) unsigned NOT NULL default '0',
  `RetryCount` smallint(5) unsigned NOT NULL default '0',
  `LastRetryTime` datetime NOT NULL ,
  `IsOffline` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`Id`) ,
  UNIQUE INDEX `idx_transactionpush_multi_uq_1` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`) ,
  UNIQUE INDEX `idx_transactionpush_multi_uq_2` (`CustomerId`, `PointOfSaleId`,`PurchasedDate`,`TicketNumber`),
  INDEX `idx_transactionpush_customer` (`CustomerId`),
  INDEX `idx_transactionpush_pointofsale` (`PointOfSaleId`),
  CONSTRAINT `fk_transactionpush_customer`
    FOREIGN KEY (`CustomerId`)
    REFERENCES `Customer` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION ,
  CONSTRAINT `fk_transactionpush_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Transaction2Push`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Transaction2Push`;

CREATE  TABLE `Transaction2Push` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ThirdPartyServiceAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchasedDate` DATETIME NOT NULL ,
  `TicketNumber` INT(10) UNSIGNED NOT NULL ,
  `PaystationSettingName` VARCHAR(20) NOT NULL DEFAULT '' ,
  `PlateNumber` VARCHAR(10) NOT NULL DEFAULT '' ,
  `ExpiryDate` DATETIME NOT NULL ,
  `ChargedAmount` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0 ,
  `StallNumber` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0 ,
  `RetryCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 ,
  `LastRetryTime` DATETIME NOT NULL ,
  `IsOffline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `Field1` VARCHAR(50) NULL DEFAULT '' , 
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_transaction2push_multi_uq` (`PointOfSaleId`, `PurchasedDate`, `TicketNumber`, `ThirdPartyServiceAccountId`) ,
  INDEX `idx_transaction2push_thirdpartyserviceaccount` (`ThirdPartyServiceAccountId`) ,
  INDEX `idx_transaction2push_location` (`LocationId`) ,
  INDEX `idx_transaction2push_pointofsale` (`PointOfSaleId`) ,
  INDEX `idx_transaction2push_retrycount` (`RetryCount`) ,
  CONSTRAINT `fk_transaction2push_thirdpartyserviceaccount`
    FOREIGN KEY (`ThirdPartyServiceAccountId`)
    REFERENCES `ThirdPartyServiceAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION ,
  CONSTRAINT `fk_transaction2push_location`
    FOREIGN KEY (`LocationId`)
    REFERENCES `Location` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION ,    
  CONSTRAINT `fk_transaction2push_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ThirdPartyPOSSequence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ThirdPartyPOSSequence`;

CREATE  TABLE `ThirdPartyPOSSequence` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `SequenceNumber` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_thirdpartypossequence_pointofsale_uq` (`PointOfSaleId`) ,
  INDEX `idx_thirdpartypossequence_pointofsale` (`PointOfSaleId`) ,
  CONSTRAINT `fk_thirdpartypossequence_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ThirdPartyPushLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ThirdPartyPushLog`;

CREATE  TABLE `ThirdPartyPushLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ActionTypeId` TINYINT(3) UNSIGNED NOT NULL ,
  `DateField` datetime NOT NULL,
  `PushData` VARCHAR(1000),
  `ResponseMessage` VARCHAR(255),
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_thirdpartypushlog_multi_uq` (`ActionTypeId`, `DateField`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ThirdPartyServiceStallSequence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ThirdPartyServiceStallSequence`;

CREATE  TABLE `ThirdPartyServiceStallSequence` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `SequenceNumber` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_thirdpartyservicestallsequence_multi_uq` (`LocationId`, `SpaceNumber`) ,
  INDEX `idx_thirdpartyservicestallsequence_location` (`LocationId`) ,
  CONSTRAINT `fk_thirdpartyservicestallsequence_location`
    FOREIGN KEY (`LocationId`)
    REFERENCES `Location` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LotSettingXref`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LotSettingXref` ;

CREATE  TABLE IF NOT EXISTS `LotSettingXref` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `NameEMS6` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_lotsettingxref_multi_uq_1` (`CustomerId` ASC, `Name` ASC) ,
  INDEX `idx_lotsettingxref_multi_uq_2` (`CustomerId` ASC, `NameEMS6` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SettingsFile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SettingsFile` ;

CREATE  TABLE IF NOT EXISTS `SettingsFile` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `UniqueIdentifier` MEDIUMINT UNSIGNED NOT NULL ,
  `UploadGMT` DATETIME NOT NULL ,
  `ActivationGMT` DATETIME NOT NULL ,
  `FileName` VARCHAR(40) NULL ,
  `VERSION` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_settingsfile_customer` (`CustomerId` ASC) ,
  UNIQUE INDEX `idx_settingsfile_multi_uq` (`CustomerId` ASC, `Name` ASC, `UploadGMT` ASC) ,
  CONSTRAINT `fk_settingsfile_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ETLStatus` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLStatus` ;

CREATE  TABLE IF NOT EXISTS `ETLStatus` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ClusterId` TINYINT UNSIGNED NOT NULL DEFAULT 1 ,
  `TableName` VARCHAR(30) NOT NULL ,
  `ETLProcessedId` BIGINT NOT NULL DEFAULT 0 ,
  `ETLProcessedGMT` DATETIME NOT NULL , 
  PRIMARY KEY (`Id`)
)  
ENGINE = InnoDB;
 

-- -----------------------------------------------------
-- Table `ETLStatusType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLStatusType` ;

CREATE  TABLE IF NOT EXISTS `ETLStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_etlstatustype_name` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ETLLock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLLock` ;

CREATE  TABLE IF NOT EXISTS `ETLLock` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LockGMT` DATETIME NOT NULL ,
  `BatchSize` SMALLINT UNSIGNED NOT NULL ,
  `LockableCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `LockedCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ETLCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ETLBeginGMT` DATETIME NULL ,
  `ETLEndGMT` DATETIME NULL ,
  `ETLPerSecond` DECIMAL(7,2) UNSIGNED NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_etllock_multi` (`LockGMT` ASC, `Id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TotalCollection`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TotalCollection` ;
 
 CREATE  TABLE IF NOT EXISTS `TotalCollection` (
   `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
   `POSCollectionId` INT UNSIGNED NOT NULL, 
   `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
   `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
   `CollectionTypeId` TINYINT UNSIGNED NOT NULL ,
   `EndTimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
   `EndTimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
   `CoinTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
   `BillTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
   `CardTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
   `TotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
   PRIMARY KEY (`Id`) ,
   INDEX `idx_totalcollection_customer` (`CustomerId` ASC) ,
   INDEX `idx_totalcollection_pointofsale` (`PointOfSaleId` ASC) ,
   INDEX `idx_totalcollection_collectiontype` (`CollectionTypeId` ASC) ,
   INDEX `idx_totalcollection_multi` (`PointOfSaleId` ASC, `EndTimeIdLocal` DESC, `CollectionTypeId` ASC) ,
   CONSTRAINT `fk_totalcollection_customer`
     FOREIGN KEY (`CustomerId` )
     REFERENCES `Customer` (`Id` )
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
   CONSTRAINT `fk_totalcollection_pointofsale`
     FOREIGN KEY (`PointOfSaleId` )
     REFERENCES `PointOfSale` (`Id` )
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
   CONSTRAINT `fk_totalcollection_collectiontype`
     FOREIGN KEY (`CollectionTypeId` )
     REFERENCES `CollectionType` (`Id` )
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
   CONSTRAINT `fk_totalcollection_EndTimeIdGMT`
        FOREIGN KEY (`EndTimeIdGMT` )
        REFERENCES `Time` (`Id` )
        ON DELETE NO ACTION
     ON UPDATE NO ACTION,
   CONSTRAINT `fk_totalcollection_EndTimeIdLocal`
           FOREIGN KEY (`EndTimeIdLocal` )
           REFERENCES `Time` (`Id` )
           ON DELETE NO ACTION
     ON UPDATE NO ACTION)   
 ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PurchaseNotProcessed`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PurchaseNotProcessed`;
CREATE TABLE  `PurchaseNotProcessed` (
  `PurchaseId` BIGINT UNSIGNED NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL,
  `PurchaseGMT` DATETIME NOT NULL,
  `record_insert_time` DATETIME DEFAULT NULL,
  INDEX `idx_PurchaseNotProcessed_PurchaseId` (`PurchaseId` ASC) ,
  INDEX `idx_PurchaseNotProcessed_multi` (`PurchaseId`, `CustomerId` ASC) 
) ENGINE=InnoDB;
 
-- -----------------------------------------------------
-- Table `ETLProcessDateRange`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLProcessDateRange`;
CREATE TABLE  `ETLProcessDateRange` (
  `id` 	INT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL,
  `ETLObject` VARCHAR(20) NOT NULL,
  `BeginDt` DATETIME NOT NULL,
  `EndDt` DATETIME NOT NULL,
  `status` TINYINT DEFAULT 0,
  `ETLStartTime` DATETIME DEFAULT NULL,
  `CursorOpenedTime` DATETIME DEFAULT NULL,
  `CountOfRecords` INT DEFAULT NULL,
  `ETLCompletedTime` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- -----------------------------------------------------
-- Table `CollectionNotProcessed`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CollectionNotProcessed`;
CREATE TABLE  `CollectionNotProcessed` (
  `PosCollectionId` INT UNSIGNED NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL,
  `CollectionTypeId` TINYINT UNSIGNED NOT NULL,
  `EndGMT` DATETIME NOT NULL,
  `RecordInsertTime` DATETIME DEFAULT NULL,
  INDEX `idx_CollectionNotProcessed_POSId` (`PosCollectionId` ASC) ,
  INDEX `idx_CollectionNotProcessed_multi` (`PosCollectionId`, `CustomerId` ASC) 
) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `SettledTransaction`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `SettledTransaction`;
CREATE TABLE  `SettledTransaction` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL,
  `PurchaseId` BIGINT UNSIGNED NOT NULL,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NOT NULL,
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL,
  `Amount` MEDIUMINT UNSIGNED NOT NULL,
  `IsRefund` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idx_SettledTransaction_customer` (`CustomerId`),
  KEY `idx_SettledTransaction_ProcessorTrnTypeId` (`ProcessorTransactionTypeId`),
  KEY `idx_SettledTransaction_multi` (`CustomerId`,`TimeIdLocal`,`MerchantAccountId`),
  KEY `fk_SettledTransaction_MerchantAccount` (`MerchantAccountId`),
  KEY `fk_SettledTransaction_TimeIdLocal` (`TimeIdLocal`),
  UNIQUE INDEX `idx_SettledTransaction_ProcessorTransactionId` (`ProcessorTransactionId`),
  CONSTRAINT `fk_SettledTransaction_customer` 
	FOREIGN KEY (`CustomerId`) 
	REFERENCES `Customer` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION,
  CONSTRAINT `fk_SettledTransaction_MerchantAccount` 
	FOREIGN KEY (`MerchantAccountId`) 
	REFERENCES `MerchantAccount` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION,
  CONSTRAINT `fk_SettledTransaction_TimeIdLocal` 
	FOREIGN KEY (`TimeIdLocal`) 
	REFERENCES `Time` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION,
  CONSTRAINT `fk_SettledTransaction_txn_type` 
	FOREIGN KEY (`ProcessorTransactionTypeId`) 
	REFERENCES `ProcessorTransactionType` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION
) ENGINE=InnoDB ;


-- -----------------------------------------------------
-- Table `SettledTxnNotProcessed`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `SettledTxnNotProcessed`;
CREATE TABLE  `SettledTxnNotProcessed` (
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NOT NULL,
  `ProcessingDate` DATETIME NOT NULL,
  `RecordInsertTime` DATETIME DEFAULT NULL,
  INDEX `idx_SettledTxnNotProcessed_Id` (`ProcessorTransactionId` ASC) ,
  INDEX `idx_SettledTxnNotProcessed_multi` (`ProcessorTransactionId`, `PointOfSaleId` ASC) 
) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `ETLExecutionLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLExecutionLog`;
CREATE TABLE  `ETLExecutionLog` (
  `Id` 	BIGINT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL DEFAULT 1,
  `Parameter1` DATETIME NOT NULL,
  `Parameter2` DATETIME NOT NULL,  
  `ETLStartTime` DATETIME DEFAULT NULL,
  `Status` TINYINT DEFAULT 0,
  `PPPCursorOpenedTime` DATETIME DEFAULT NULL,
  `PurchaseRecordCount` INT DEFAULT NULL,
  `ETLCompletedPurchase` DATETIME DEFAULT NULL,  
  `SettledAmountCursorOpenedTime` DATETIME DEFAULT NULL,
  `SettledAmountRecordCount` INT DEFAULT NULL,
  `ETLCompletedSettledAmount` DATETIME DEFAULT NULL,  
  `CollectionCursorOpenedTime` DATETIME DEFAULT NULL,
  `CollectionRecordCount` INT DEFAULT NULL,
  `ETLCompletedCollection` DATETIME DEFAULT NULL,   
  `ETLEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_ETLExecutionLog_ETLProcessTypeId` (`ETLProcessTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


-- -----------------------------------------------------
-- Table `EmailAddress`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EmailAddress` ;

CREATE  TABLE IF NOT EXISTS `EmailAddress` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Email` VARCHAR(255) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_Email_uq` (Email) )
ENGINE = InnoDB; 

-- -----------------------------------------------------
-- Table `Consumer`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `Consumer` ;

CREATE  TABLE IF NOT EXISTS `Consumer` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL  ,
  `FirstName` VARCHAR(25) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `LastName` VARCHAR(25) CHARACTER SET 'latin1' COLLATE 'latin1_general_cs' NOT NULL ,
  `EmailAddressId` MEDIUMINT UNSIGNED NULL ,
  `Description` VARCHAR(80) DEFAULT NULL,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_Consumer_multi_uq` (`CustomerId`, `EmailAddressId`) ,		
  INDEX `idx_Consumer_Lastname` (`LastName` ASC) ,
  CONSTRAINT `fk_Consumer_emailaddress` 
    FOREIGN KEY (`EmailAddressId` )
    REFERENCES `EmailAddress` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT `fk_Consumer_CustomerId`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	
   )
ENGINE = InnoDB;

-- NEW KPI DESIGN

-- -----------------------------------------------------
-- Table `PPPTotalHour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalHour` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalHour_unq` (`TimeIdLocal` ,`CustomerId`) ,
  INDEX idx_PPPTotalHour_CustomerId ( CustomerId ),
  INDEX idx_PPPTotalHour_multi ( CustomerId, TimeIdGMT ),
  INDEX idx_PPPTotalHour_multi2 ( CustomerId, TimeIdLocal ) ,
  CONSTRAINT `fk_PPPTotalHour_time1`
    FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PPPTotalHour_time2`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPTotalDay`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalDay` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalDay_unq` (`TimeIdLocal`,`CustomerId`) ,
  INDEX idx_PPPTotalDay_CustomerId ( CustomerId ) ,
  CONSTRAINT `fk_PPPTotalDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPTotalMonth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PPPTotalMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPTotalMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPTotalMonth_unq` (`TimeIdLocal`,`CustomerId`) ,
  INDEX idx_PPPTotalMonth_CustomerId ( CustomerId ) ,
  INDEX idx_PPPTotalMonth_TimeIdLocal ( TimeIdLocal ),
  CONSTRAINT `fk_PPPTotalMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPDetailHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailHour` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalHourId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailHour_unq` (`PPPTotalHourId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  CONSTRAINT `fk_PPPDetailHour_PPPTotalHourId`
    FOREIGN KEY (`PPPTotalHourId` )
    REFERENCES `PPPTotalHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPDetailDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailDay` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalDayId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailDay_unq` (`PPPTotalDayId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  CONSTRAINT `fk_PPPDetailDay_PPPTotalDayId`
    FOREIGN KEY (`PPPTotalDayId` )
    REFERENCES `PPPTotalDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PPPDetailMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPDetailMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPDetailMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPTotalMonthId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitLocationId` MEDIUMINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPDetailMonth_unq` (`PPPTotalMonthId` ,`PurchaseLocationId`, `PermitLocationId`, `PointOfSaleId`, `UnifiedRateId`, `PaystationSettingId`,`TransactionTypeId`,`IsCoupon`) ,
  INDEX idx_PPPDetailMonth_TotalMonthId_PurLocId (PPPTotalMonthId, PurchaseLocationId) ,
  INDEX idx_PPPDetailMonth_PurchaseLocationId (PurchaseLocationId) ,
  INDEX idx_PPPDetailMonth_PPPTotalMonthId (PPPTotalMonthId) ,
  CONSTRAINT `fk_PPPDetailMonth_PPPTotalMonthId`
    FOREIGN KEY (`PPPTotalMonthId` )
    REFERENCES `PPPTotalMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `PPPRevenueHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueHour` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailHourId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueHour_unq` (`PPPDetailHourId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueHour_PPPDetailHourId`
    FOREIGN KEY (`PPPDetailHourId` )
    REFERENCES `PPPDetailHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPRevenueDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueDay` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailDayId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueDay_unq` (`PPPDetailDayId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueDay_PPPDetailDayId`
    FOREIGN KEY (`PPPDetailDayId` )
    REFERENCES `PPPDetailDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `PPPRevenueMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PPPRevenueMonth` ;

CREATE  TABLE IF NOT EXISTS `PPPRevenueMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PPPDetailMonthId` BIGINT UNSIGNED NOT NULL ,
  `RevenueTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_PPPRevenueMonth_unq` (`PPPDetailMonthId` ,`RevenueTypeId`) ,
  CONSTRAINT `fk_PPPRevenueMonth_PPPDetailMonthId`
    FOREIGN KEY (`PPPDetailMonthId` )
    REFERENCES `PPPDetailMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `OccupancyHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyHour` ;

CREATE  TABLE IF NOT EXISTS `OccupancyHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED DEFAULT 0  ,  
  `NoOfPurchases` MEDIUMINT UNSIGNED DEFAULT 0 ,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyHour_unq` (`TimeIdGMT` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
  INDEX `idx_OccupancyHour_multi`(`CustomerId`, `LocationId`, `UnifiedRateId`) ,
  CONSTRAINT `fk_OccupancyHour_time1`
     FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OccupancyHour_time2`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ETLNotProcessed`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLNotProcessed`;
CREATE TABLE  `ETLNotProcessed` (
  `PurchaseId` BIGINT UNSIGNED ,
  `CustomerId` MEDIUMINT UNSIGNED ,
  `LocationId` MEDIUMINT UNSIGNED ,
  `PointOfSaleId` MEDIUMINT UNSIGNED ,
  `PurchaseGMT` DATETIME ,
  `PermitId` INT UNSIGNED ,
  `ETLObject` VARCHAR(20) ,
  `ETLProcessDateRangeId` INT UNSIGNED ,
  `ETLProcessTypeId` INT UNSIGNED ,
  `BeginPermitGMT` datetime ,
  `EndPermitGMT` datetime ,
  `UnifiedRateId` MEDIUMINT UNSIGNED ,
  `Customer_Timezone` char(25) ,
  `Occ_Date` DATE ,
  `Interval` TINYINT UNSIGNED ,
  `NoOfSpaces` MEDIUMINT UNSIGNED,  
  `record_insert_time` DATETIME DEFAULT NULL  
) ENGINE=InnoDB;



-- -----------------------------------------------------
-- Table `OccupancyDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyDay` ;

CREATE  TABLE IF NOT EXISTS `OccupancyDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of the Day 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL  DEFAULT 1,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyDay_unq` (`TimeIdLocal` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
    CONSTRAINT `fk_OccupancyDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `OccupancyMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `OccupancyMonth` ;

CREATE  TABLE IF NOT EXISTS `OccupancyMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of first day of the Month 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL DEFAULT 1,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_OccupancyMonth_unq` (`TimeIdLocal` ,`CustomerId`, `LocationId`, `UnifiedRateId`) ,
    CONSTRAINT `fk_OccupancyMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `FlexETLDataType` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `FlexETLDataType` ;

CREATE  TABLE IF NOT EXISTS `FlexETLDataType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `idx_FlexETLDataType_name_uq` (`Name` ASC) )
ENGINE = InnoDB ;


-- -----------------------------------------------------
-- Table `FlexDataWSCallStatus` 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FlexDataWSCallStatus` ;

CREATE  TABLE IF NOT EXISTS `FlexDataWSCallStatus` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,  
  `FlexETLDataTypeId` TINYINT UNSIGNED NOT NULL ,   
  `RequestedFromDateGMT` DATETIME NULL ,  
  `RequestedToDateGMT` DATETIME NULL ,  
  `WSCallBeginGMT` DATETIME  NULL , 
  `WSCallEndGMT` DATETIME  NULL , 
  `LastUniqueIdentifier` INT UNSIGNED NULL , 
  `Status` TINYINT NULL ,  
  PRIMARY KEY (`Id`), 
    CONSTRAINT `fk_FlexDataWSCallStatus_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FlexDataWSCallStatus_FlexETLDataTypeId`
    FOREIGN KEY (`FlexETLDataTypeId` )
    REFERENCES `FlexETLDataType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  )
ENGINE = InnoDB ;



-- -----------------------------------------------------
-- Table `CaseETLDataType` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CaseETLDataType` ;

CREATE  TABLE IF NOT EXISTS `CaseETLDataType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `idx_CaseETLDataType_name_uq` (`Name` ASC) )
ENGINE = InnoDB ;



-- -----------------------------------------------------
-- Table `CaseDataWSCallStatus` 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CaseDataWSCallStatus` ;

CREATE  TABLE IF NOT EXISTS `CaseDataWSCallStatus` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,  
  `CaseETLDataTypeId` TINYINT UNSIGNED NOT NULL ,   
  `RequestedFromDateGMT` DATETIME NULL ,  
  `RequestedToDateGMT` DATETIME NULL ,  
  `WSCallBeginGMT` DATETIME  NULL , 
  `WSCallEndGMT` DATETIME  NULL , 
  `LastUniqueIdentifier` INT UNSIGNED NULL , 
  `Status` TINYINT NULL ,  
  PRIMARY KEY (`Id`), 
    CONSTRAINT `fk_CaseDataWSCallStatus_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CaseDataWSCallStatus_CaseETLDataTypeId`
    FOREIGN KEY (`CaseETLDataTypeId` )
    REFERENCES `CaseETLDataType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB ;



-- -----------------------------------------------------
-- Table `CitationTotalHour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CitationTotalHour` ;

CREATE  TABLE IF NOT EXISTS `CitationTotalHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CitationTypeId` MEDIUMINT UNSIGNED NOT NULL, 
  `TotalCitation` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CitationTotalHour_unq` (`CustomerId`,`TimeIdLocal`,`CitationTypeId`) ,
  CONSTRAINT `fk_CitationTotalHour_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalHour_time2`
    FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalHour_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalHour_citationtype`
    FOREIGN KEY (`CitationTypeId` )
    REFERENCES `CitationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CitationTotalDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CitationTotalDay` ;

CREATE  TABLE IF NOT EXISTS `CitationTotalDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CitationTypeId` MEDIUMINT UNSIGNED NOT NULL, 
  `TotalCitation` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CitationTotalDay_unq` (`CustomerId`,`TimeIdLocal`,`CitationTypeId`) ,
  CONSTRAINT `fk_CitationTotalDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalDay_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalDay_citationtype`
    FOREIGN KEY (`CitationTypeId` )
    REFERENCES `CitationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CitationTotalMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CitationTotalMonth` ;

CREATE  TABLE IF NOT EXISTS `CitationTotalMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CitationTypeId` MEDIUMINT UNSIGNED NOT NULL, 
  `TotalCitation` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CitationTotalMonth_unq` (`CustomerId`,`TimeIdLocal`,`CitationTypeId`) ,
  CONSTRAINT `fk_CitationTotalMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalMonth_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationTotalMonth_citationtype`
    FOREIGN KEY (`CitationTypeId` )
    REFERENCES `CitationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CitationMap`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CitationMap` ;

CREATE  TABLE IF NOT EXISTS `CitationMap` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CitationId` INT UNSIGNED NOT NULL ,
  `TicketId` VARCHAR(40) NOT NULL ,
  `CitationTypeId` MEDIUMINT UNSIGNED NOT NULL, 
  `IssueDateGMT` DATETIME NOT NULL ,
  `Latitude` DECIMAL(18,14) NULL ,
  `Longitude` DECIMAL(18,14) NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CitationMap_unq` (`CustomerId`,`CitationId`) ,
  CONSTRAINT `fk_CitationMap_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CitationMap_citationtype`
    FOREIGN KEY (`CitationTypeId` )
    REFERENCES `CitationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ETLLocationDetailType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLLocationDetailType` ;

CREATE  TABLE IF NOT EXISTS `ETLLocationDetailType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_etllocationdetailtype_name_uq` (`Name` ASC))
ENGINE = InnoDB
COMMENT = 'types: Hourly, Daily, Monthly' ;

-- -----------------------------------------------------
-- Table `ETLLocationDetail`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLLocationDetail` ;

CREATE  TABLE IF NOT EXISTS `ETLLocationDetail` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `ETLLocationDetailTypeId` TINYINT UNSIGNED NOT NULL ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `NumberOfSpaces` INT UNSIGNED NULL ,
  `TotalNumberOfSpaces` INT UNSIGNED NULL ,
  `MaxUtilizationMinutes` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `ind_ETLLocationDetail_cust` (CustomerId,LocationId,ETLLocationDetailTypeId,TimeIdGMT) ,
  INDEX `ind_ETLLocationDetail_loc` (LocationId,ETLLocationDetailTypeId,TimeIdGMT) ,
  CONSTRAINT `fk_etllocationdetail_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_etllocationdetail_etllocationdetailtype`
    FOREIGN KEY (`ETLLocationDetailTypeId` )
    REFERENCES `ETLLocationDetailType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_etllocationdetail_CustomerId`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ETLLocationStatus`
-- -----------------------------------------------------


DROP TABLE IF EXISTS `ETLLocationStatus`;

CREATE TABLE `ETLLocationStatus` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL , 
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `DataRunDate` DATE NOT NULL ,
  `StartTime` DATETIME NULL ,
  `EndTime` DATETIME NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_ETLLocationStatus_multi` (`LocationId`,`DataRunDate`)   
  ) ENGINE=InnoDB;
  
-- -----------------------------------------------------
-- Table `ETLQueueRecordCount`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLQueueRecordCount` ;

CREATE  TABLE IF NOT EXISTS `ETLQueueRecordCount` (
  `ETLProcessDateRangeId` INT UNSIGNED NOT NULL ,
  `TotalRecords` INT UNSIGNED NOT NULL , 
  `ETLRemaingRecords`  INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`ETLProcessDateRangeId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StagingPurchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingPurchase` ;

CREATE  TABLE IF NOT EXISTS `StagingPurchase` (
  `Id` BIGINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchaseGMT` DATETIME NOT NULL ,
  `PurchaseNumber` INT UNSIGNED NOT NULL ,
  `TransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `PaymentTypeId` TINYINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PaystationSettingId` MEDIUMINT UNSIGNED NOT NULL ,
  `UnifiedRateId` MEDIUMINT UNSIGNED NOT NULL ,
  `CouponId` MEDIUMINT UNSIGNED NULL ,
  `OriginalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChargedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ChangeDispensedAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `ExcessPaymentAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CashPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CoinPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardPaidAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `RateRevenueAmount` MEDIUMINT NOT NULL DEFAULT 0 ,
  `CoinCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillCount` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `IsOffline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `IsRefundSlip` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `CreatedGMT` DATETIME NOT NULL ,
  INDEX `idx_stagingpurchase_transactiontype` (`TransactionTypeId` ASC) ,
  INDEX `idx_stagingpurchase_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_stagingpurchase_paymenttype` (`PaymentTypeId` ASC) ,
  INDEX `idx_stagingpurchase_location` (`LocationId` ASC) ,
  INDEX `idx_stagingpurchase_coupon` (`CouponId` ASC) ,
  INDEX `idx_stagingpurchase_paystationsetting` (`PaystationSettingId` ASC) ,
  INDEX `idx_stagingpurchase_unifiedrate` (`UnifiedRateId` ASC) ,
  INDEX `idx_stagingpurchase_PurchaseGMT` (`PurchaseGMT` ASC),
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_stagingpurchase_multi_uq` (`CustomerId` ASC, `PointOfSaleId` ASC, `PurchaseGMT` ASC, `PurchaseNumber` ASC) ,
  INDEX `idx_stagingpurchase_created` (`CreatedGMT` ASC) ,
  INDEX `idx_stagingpurchase_customer` (`CustomerId` ASC),
  CONSTRAINT `fk_stagingpurchase_transactiontype`
    FOREIGN KEY (`TransactionTypeId` )
    REFERENCES `TransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_paymenttype`
    FOREIGN KEY (`PaymentTypeId` )
    REFERENCES `PaymentType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_coupon`
    FOREIGN KEY (`CouponId` )
    REFERENCES `Coupon` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_paystationsetting`
    FOREIGN KEY (`PaystationSettingId` )
    REFERENCES `PaystationSetting` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_unifiedrate`
    FOREIGN KEY (`UnifiedRateId` )
    REFERENCES `UnifiedRate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stagingpurchase_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;





-- -----------------------------------------------------
-- Table `StagingPermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingPermit` ;

CREATE  TABLE IF NOT EXISTS `StagingPermit` (
  `Id` BIGINT UNSIGNED NOT NULL  ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `PermitNumber` INT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `PermitTypeId` TINYINT UNSIGNED NOT NULL ,
  `PermitIssueTypeId` TINYINT UNSIGNED NOT NULL ,
  `LicencePlateId` INT UNSIGNED NULL ,
  `MobileNumberId` INT UNSIGNED NULL ,
  `OriginalPermitId` BIGINT UNSIGNED NULL ,
  `SpaceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `AddTimeNumber` INT UNSIGNED NOT NULL ,
  `PermitBeginGMT` DATETIME NOT NULL ,
  `PermitOriginalExpireGMT` DATETIME NOT NULL ,
  `PermitExpireGMT` DATETIME NOT NULL ,
  `NumberOfExtensions` TINYINT UNSIGNED NOT NULL ,
   UNIQUE INDEX `idx_Stagingpermit_purchase_unq` (`PurchaseId` ASC) ,
  INDEX `idx_Stagingpermit_location` (`LocationId` ASC) ,
  INDEX `idx_Stagingpermit_licenceplate` (`LicencePlateId` ASC) ,
  INDEX `idx_Stagingpermit_mobilenumber` (`MobileNumberId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_Stagingpermit_addtimenumber` (`AddTimeNumber` ASC) ,
  INDEX `idx_Stagingpermit_permittype` (`PermitTypeId` ASC) ,
  INDEX `idx_Stagingpermit_permitissuetype` (`PermitIssueTypeId` ASC) ,
  INDEX `idx_Stagingpermit_self` (`OriginalPermitId` ASC) ,
  CONSTRAINT `fk_Stagingpermit_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_licenceplate`
    FOREIGN KEY (`LicencePlateId` )
    REFERENCES `LicencePlate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_mobilenumber`
    FOREIGN KEY (`MobileNumberId` )
    REFERENCES `MobileNumber` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_permittype`
    FOREIGN KEY (`PermitTypeId` )
    REFERENCES `PermitType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_permitissuetype`
    FOREIGN KEY (`PermitIssueTypeId` )
    REFERENCES `PermitIssueType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpermit_self`
    FOREIGN KEY (`OriginalPermitId` )
    REFERENCES `Permit` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `StagingPaymentCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingPaymentCard` ;

CREATE  TABLE IF NOT EXISTS `StagingPaymentCard` (
  `Id` BIGINT UNSIGNED NOT NULL  ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `CardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CreditCardTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerCardId` MEDIUMINT UNSIGNED NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NULL ,
  `ProcessorTransactionId` BIGINT UNSIGNED NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardLast4Digits` SMALLINT UNSIGNED NOT NULL ,
  `CardProcessedGMT` DATETIME NOT NULL ,
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL ,
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL ,
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL ,
  INDEX `idx_Stagingpaymentcard_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_Stagingpaymentcard_creditcardtype` (`CreditCardTypeId` ASC) ,
  INDEX `idx_Stagingpaymentcard_last4digits` (`CardLast4Digits` ASC, `CreditCardTypeId` ASC, `PurchaseId` ASC) ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_Stagingpaymentcard_cardtype` (`CardTypeId` ASC) ,
  INDEX `idx_Stagingpaymentcard_customercard` (`CustomerCardId` ASC) ,
  INDEX `idx_Stagingpaymentcard_processortransaction` (`ProcessorTransactionId` ASC) ,
  INDEX `idx_Stagingpaymentcard_processortransactiontype` (`ProcessorTransactionTypeId` ASC) ,
  INDEX `idx_Stagingpaymentcard_merchantaccount` (`MerchantAccountId` ASC) ,
  CONSTRAINT `fk_Stagingpaymentcard_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_creditcardtype`
    FOREIGN KEY (`CreditCardTypeId` )
    REFERENCES `CreditCardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_cardtype`
    FOREIGN KEY (`CardTypeId` )
    REFERENCES `CardType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_customercard`
    FOREIGN KEY (`CustomerCardId` )
    REFERENCES `CustomerCard` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_processortransaction`
    FOREIGN KEY (`ProcessorTransactionId` )
    REFERENCES `ProcessorTransaction` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_processortransactiontype`
    FOREIGN KEY (`ProcessorTransactionTypeId` )
    REFERENCES `ProcessorTransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Stagingpaymentcard_merchantaccount`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StagingProcessorTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingProcessorTransaction`;

CREATE TABLE `StagingProcessorTransaction` (
  `Id` BIGINT UNSIGNED NOT NULL ,
  `PurchaseId` BIGINT UNSIGNED NULL ,
  `PreAuthId` BIGINT UNSIGNED NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchasedDate` DATETIME NOT NULL ,  
  `TicketNumber` INT UNSIGNED NOT NULL ,
  `ProcessorTransactionTypeId` TINYINT UNSIGNED NOT NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `CardType` VARCHAR(20) NOT NULL ,
  `Last4DigitsOfCardNumber` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `CardChecksum` MEDIUMINT UNSIGNED NOT NULL DEFAULT '0',
  `ProcessingDate` DATETIME NOT NULL ,
  `ProcessorTransactionId` VARCHAR(50) NULL DEFAULT '',
  `AuthorizationNumber` VARCHAR(30) DEFAULT NULL ,
  `ReferenceNumber` VARCHAR(30) NOT NULL DEFAULT '',
  `IsApproved` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CardHash` VARCHAR(30) NOT NULL DEFAULT '',
  `IsUploadedFromBoss` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `IsRFID` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `CreatedGMT` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `idx`  (`Id`),
  INDEX `idx_stgprocessortransaction_multi_1` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`,`Last4DigitsOfCardNumber`),
  INDEX `idx_stgprocessortransaction_multi_2` (`PointOfSaleId`,`PurchasedDate`,`TicketNumber`,`CardHash`),
  INDEX `idx_stgprocessortransaction_preauth` (`PreAuthId` ASC) ,
  INDEX `idx_stgprocessortransaction_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_stgprocessortransaction_processortransactiontype` (`ProcessorTransactionTypeId` ASC) ,
  INDEX `idx_stgprocessortransaction_merchantaccount` (`MerchantAccountId` ASC) ,
  INDEX `idx_stgprocessortransaction_purchase` (`PurchaseId` ASC) ,
  INDEX `idx_stgprocessortransaction_createdGMT` (`CreatedGMT` ASC) ,
  INDEX `idx_stgprocessortransaction_processingdate` (`ProcessingDate` ASC) ,
  CONSTRAINT `fk_stgprocessortransaction_preauth`
    FOREIGN KEY (`PreAuthId`)
    REFERENCES `PreAuth` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgprocessortransaction_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgprocessortransaction_processortransactiontype`
    FOREIGN KEY (`ProcessorTransactionTypeId`)
    REFERENCES `ProcessorTransactionType` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgprocessortransaction_merchantaccount`
    FOREIGN KEY (`MerchantAccountId`)
    REFERENCES `MerchantAccount` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgprocessortransaction_purchase`
    FOREIGN KEY (`PurchaseId`)
    REFERENCES `Purchase` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `StagingPOSCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingPOSCollection` ;

CREATE  TABLE IF NOT EXISTS `StagingPOSCollection` (
  `Id` BIGINT UNSIGNED NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CollectionTypeId` TINYINT UNSIGNED NOT NULL ,
  `EndGMT` DATETIME NOT NULL ,  
  `CoinTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `BillTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  `CardTotalAmount` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_stgposcollection_customer` (`CustomerId` ASC) ,
  INDEX `idx_stgposcollection_pointofsale` (`PointOfSaleId` ASC) ,
  INDEX `idx_stgposcollection_collectiontype` (`CollectionTypeId` ASC) ,
  CONSTRAINT `fk_stgposcollection_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgposcollection_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stgposcollection_collectiontype`
    FOREIGN KEY (`CollectionTypeId` )
    REFERENCES `CollectionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)  
ENGINE = InnoDB;






-- Archive Tables

-- -----------------------------------------------------
-- Table `ArchiveStagingPurchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingPurchase` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStagingPurchase` (
  `Id` BIGINT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStagingPermit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingPermit` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStagingPermit` (
  `Id` BIGINT UNSIGNED NOT NULL  ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStagingPaymentCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingPaymentCard` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStagingPaymentCard` (
  `Id` BIGINT UNSIGNED NOT NULL  ,
  `RecordInsertTime` DATETIME NOT NULL  
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStagingProcessorTransaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingProcessorTransaction`;

CREATE TABLE `ArchiveStagingProcessorTransaction` (
  `Id` BIGINT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL   
) ENGINE=InnoDB;



-- -----------------------------------------------------
-- Table `ArchiveStagingPOSCollection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingPOSCollection`;

CREATE TABLE `ArchiveStagingPOSCollection` (
  `Id` INT UNSIGNED NOT NULL ,
  `RecordInsertTime` DATETIME NOT NULL   
) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `UtilizationDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `UtilizationDay`;

CREATE TABLE `UtilizationDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of day 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NOT NULL ,
  `MaxUtilizationMinutes` INT UNSIGNED NOT NULL ,
  `NumberOfPurchases` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalMinutesPurchased` INT UNSIGNED NOT NULL ,
  `ActualMinutesPurchased` INT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `ind_UtilizationDay_unq` (TimeIdLocal, CustomerId, LocationId )
  ) ENGINE=InnoDB;
  
  
  
-- -----------------------------------------------------
-- Table `UtilizationMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `UtilizationMonth`;

CREATE TABLE `UtilizationMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of first day of the Month 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NOT NULL ,
  `MaxUtilizationMinutes` INT UNSIGNED NOT NULL ,
  `NumberOfPurchases` MEDIUMINT UNSIGNED NOT NULL ,
  `TotalMinutesPurchased` INT UNSIGNED NOT NULL ,
  `ActualMinutesPurchased` INT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `ind_UtilizationMonth_unq` (TimeIdLocal, CustomerId, LocationId )
  ) ENGINE=InnoDB;
  
    
  
-- -----------------------------------------------------
-- Table `ETLMonthlyStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLMonthlyStatus`;

CREATE TABLE `ETLMonthlyStatus` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of first day of the Month 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `DataRunDate` DATE NOT NULL ,
  `StartTime` DATETIME NULL ,
  `EndTime` DATETIME NULL ,
  PRIMARY KEY (`Id`)  ,
 INDEX `idx_ETLMonthlyStatus_multi` (`CustomerId` ,`LocationId`,`DataRunDate`) ,
 INDEX `ind_ETLMonthlyStatus`(`CustomerId`, `LocationId`, `DataRunDate`) 
  ) ENGINE=InnoDB;
  

-- -----------------------------------------------------
-- Table `LoggingMigrationModuleStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `MigrationModuleStatus`;

CREATE TABLE `MigrationModuleStatus` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ETLProcessTypeId` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30)  NOT NULL , 
  `StartTime` DATETIME ,
  `EndTime` DATETIME ,
  PRIMARY KEY (`Id`)   
  ) ENGINE=InnoDB;




-- -----------------------------------------------------
-- Table `MigrationDateLookup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MigrationDateLookup`;
CREATE TABLE  `MigrationDateLookup` (
  `Id` 	INT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL DEFAULT 1,
  `TableName` Varchar(50) NOT NULL ,
  `BeginDt` DATETIME NOT NULL,
  `EndDt` DATETIME NOT NULL,  
  `Status` TINYINT DEFAULT 0,
  `MigrationStartTime` DATETIME DEFAULT NULL,
  `MigrationEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(50) DEFAULT NULL,
  UNIQUE KEY `ind_TableName_BeginDt_unq`(`TableName`,`BeginDt`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- -----------------------------------------------------
-- Table `PaymentSmartCard`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PaymentSmartCard` ;

CREATE  TABLE IF NOT EXISTS `PaymentSmartCard` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `SmartCardType` VARCHAR(15)  NULL ,  
  `SmartCardData` VARCHAR(40) NOT NULL ,  
  INDEX `idx_paymentsmartcard_purchase` (`PurchaseId` ASC) ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_paymentsmartcard_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `MigrationExecutionLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MigrationExecutionLog`;
CREATE TABLE  `MigrationExecutionLog` (
  `Id` 	BIGINT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL DEFAULT 1,
  `MigrationStartTime` DATETIME DEFAULT NULL,
  `Status` TINYINT DEFAULT 0,
  `MigrationEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;



-- -----------------------------------------------------
-- Table `DiscardedPOSBalance`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DiscardedPOSBalance` ;

CREATE  TABLE IF NOT EXISTS `DiscardedPOSBalance` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `CollectionLockId` INT UNSIGNED NULL , 
  `CreatedGMT` DATETIME  NOT NULL ,
  PRIMARY KEY (`Id`) 
  )
ENGINE = InnoDB;

-- ----------------------------------------------------
-- Table mutex
-- ---------------------------------------------------

DROP TABLE IF EXISTS mutex ;

create table if not exists mutex(
i int not null primary key
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrationReadyPayStationVersion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MigrationReadyPayStationVersion` ;

CREATE  TABLE IF NOT EXISTS `MigrationReadyPayStationVersion` (
  `Id` SMALLINT UNSIGNED NOT NULL ,
  `VersionName` VARCHAR(30) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_migrationreadypaystationversion_versionname_uq` (`VersionName` ASC) )
ENGINE = InnoDB
COMMENT = 'Pay station versions that can be migrated to EMS7' ;


-- ----------------------------------------------------
-- Table DeleteTransactionDataincrementally
-- ---------------------------------------------------


DROP TABLE IF EXISTS `DeleteTransactionDataincrementally` ;

CREATE  TABLE IF NOT EXISTS `DeleteTransactionDataincrementally` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `TableName` Varchar(50)  NULL ,
  `StartGMT` DATETIME  NOT NULL ,
  `EndGMT` DATETIME  NOT NULL ,
  `NoOfRowsDeleted` INT UNSIGNED NULL , 
  `RecordInsertTime` DATETIME   NULL ,
  PRIMARY KEY (`Id`) 
  ) ENGINE = InnoDB ;
  


-- ----------------------------------------------------
-- Table ETLReset
-- ---------------------------------------------------


DROP TABLE IF EXISTS `ETLReset` ;

CREATE  TABLE IF NOT EXISTS `ETLReset` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ETLExecutionLogId` BIGINT UNSIGNED NOT NULL ,
  `ResetTime` DATETIME  NOT NULL ,  
  PRIMARY KEY (`Id`) 
  ) ENGINE = InnoDB ;
  

-- ----------------------------------------------------
-- Table ETLCustomer
-- ---------------------------------------------------


DROP TABLE IF EXISTS `ETLCustomer` ;

CREATE  TABLE IF NOT EXISTS `ETLCustomer` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `ETLRunTime` DATETIME  NULL ,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_ETLCustomerId_uq` (`CustomerId` ) 
  ) ENGINE = InnoDB ;
  

-- -----------------------------------------------------
-- Table `ETLExecutionLogBeta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLExecutionLogBeta`;
CREATE TABLE  `ETLExecutionLogBeta` (
  `Id` 	BIGINT NOT NULL AUTO_INCREMENT,
  `ETLProcessTypeId` TINYINT NOT NULL DEFAULT 1,
  `Parameter1` DATETIME NOT NULL,
  `Parameter2` DATETIME NOT NULL,  
  `ETLStartTime` DATETIME DEFAULT NULL,
  `Status` TINYINT DEFAULT 0,
  `PPPCursorOpenedTime` DATETIME DEFAULT NULL,
  `PurchaseRecordCount` INT DEFAULT NULL,
  `ETLCompletedPurchase` DATETIME DEFAULT NULL,  
  `SettledAmountCursorOpenedTime` DATETIME DEFAULT NULL,
  `SettledAmountRecordCount` INT DEFAULT NULL,
  `ETLCompletedSettledAmount` DATETIME DEFAULT NULL,  
  `CollectionCursorOpenedTime` DATETIME DEFAULT NULL,
  `CollectionRecordCount` INT DEFAULT NULL,
  `ETLCompletedCollection` DATETIME DEFAULT NULL,   
  `ETLEndTime` DATETIME DEFAULT NULL,
  `Remarks` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- -----------------------------------------------------
-- Table `ETLOccupancyStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLOccupancyStatus`;

CREATE TABLE `ETLOccupancyStatus` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , 
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `DataRunDate` DATE NOT NULL ,
  `StartTime` DATETIME NULL ,
  `EndTime` DATETIME NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_ETLOccupancyStatus_multi` (`CustomerId` ,`LocationId`,`DataRunDate`)   
  ) ENGINE=InnoDB;
  
-- -----------------------------------------------------
-- Table `URLReroute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `URLReroute` ;

CREATE  TABLE IF NOT EXISTS `URLReroute` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DigitalConnectIP` VARCHAR(70) NOT NULL,
  `NewManagementSysURL` VARCHAR(70) NOT NULL,
  `NewManagementSysIP` VARCHAR(70) NOT NULL,
  PRIMARY KEY  (`Id`) 
) ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `CustomerURLReroute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CustomerURLReroute` ;

CREATE  TABLE IF NOT EXISTS `CustomerURLReroute` (
  `Id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `URLRerouteId` SMALLINT UNSIGNED NOT NULL ,
  PRIMARY KEY  (`Id`) ,
  INDEX `idx_URLReroute_unq` (`CustomerId`, `URLRerouteId`) ,
  CONSTRAINT `fk_URLReroute_1`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_URLReroute_2`
    FOREIGN KEY (`URLRerouteId` )
    REFERENCES `URLReroute` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION  
) ENGINE = InnoDB;
  
  
-- -----------------------------------------------------
-- Table `ETLDiscardedData`
-- -----------------------------------------------------
 DROP TABLE IF EXISTS `ETLDiscardedData`  ;
 
CREATE TABLE `ETLDiscardedData` (
  `Id` int unsigned NOT NULL AUTO_INCREMENT,
  `TableName` varchar(30) NOT NULL,
  `ProcedureName` varchar(75) NOT NULL,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL,
  `TimeIdLocal` MEDIUMINT UNSIGNED  NULL,
  `NumberOfSpaces` varchar(50)  NULL,
  `MaxUtilizationMinutes` varchar(50)  NULL,
  `NumberOfPurchases` varchar(50)  NULL,
  `TotalMinutesPurchased` varchar(50)  NULL,
  `LastModifiedTime` timestamp NOT NULL default Current_timestamp,
  PRIMARY KEY (`Id`) 
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
  
-- -----------------------------------------------------
-- Table `PurchaseEmailAddress`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `PurchaseEmailAddress`;

CREATE TABLE `PurchaseEmailAddress` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL , 
  `EmailAddressId` MEDIUMINT UNSIGNED NOT NULL ,
  `SentEmailGMT` DATETIME NULL ,
  `IsEmailSent` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  INDEX `idx_purchaseemailaddress_purchase` (`PurchaseId` ASC) ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_purchaseemailaddress_purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchaseemailaddress_emailaddress` 
    FOREIGN KEY (`EmailAddressId` )
    REFERENCES `EmailAddress` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  ) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `POSMacAddress`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `POSMacAddress`;

CREATE TABLE `POSMacAddress` (
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `MacAddress` VARCHAR(20) NOT NULL,
  `SecretKey` VARCHAR(50) NOT NULL ,
  `AlertCount` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `VERSION` INT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  INDEX `idx_POSMacAddress_PointOfSale` (`PointOfSaleId` ASC) ,
  PRIMARY KEY (`PointOfSaleId`) ,
  CONSTRAINT `fk_posmacaddress_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `POSMacAddressLog`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `POSMacAddressLog`;

CREATE TABLE `POSMacAddressLog` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `MacAddress` VARCHAR(20) NOT NULL,
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  CONSTRAINT `fk_posmacaddresslog_pointofsale`
    FOREIGN KEY (`PointOfSaleId`)
    REFERENCES `PointOfSale` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `SingleTransactionCardData`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `SingleTransactionCardData`;

CREATE  TABLE IF NOT EXISTS `SingleTransactionCardData` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PurchaseId` BIGINT UNSIGNED NOT NULL ,
  `ProcessorTransactionId` BIGINT UNSIGNED NULL ,
  `CardData` varchar(100) NOT NULL default '',
  `CardExpiry` smallint(5) unsigned NOT NULL default '0',
  `Amount` mediumint(8) unsigned NOT NULL default '0',
  `CardType` varchar(20) NOT NULL default '',
  `PurchaseGMT` datetime NOT NULL,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_SingleTransactionCardData_Purchase` (`PurchaseId` ASC) ,
  CONSTRAINT `fk_SingleTransactionCardData_Purchase`
    FOREIGN KEY (`PurchaseId` )
    REFERENCES `Purchase` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SingleTransactionCardData_ProcessorTransaction`
    FOREIGN KEY (`ProcessorTransactionId` )
    REFERENCES `ProcessorTransaction` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



  
  -- --------------------
 -- Table: BillingReport
 -- --------------------
 
 drop table if exists BillingReport ;
 
 CREATE TABLE `BillingReport` (
  `Id` int unsigned NOT NULL AUTO_INCREMENT,
  `DateRangeBegin` DATE NOT NULL,
  `DateRangeEnd` DATE NOT NULL,
  `RunBegin` datetime NOT NULL,
  `RunEnd` datetime  NULL,
  `IsPosted` tinyint unsigned NOT NULL,
  `IsError` tinyint unsigned NOT NULL,
  PRIMARY KEY (`Id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------
 -- Table: BillingReportPaystation
 -- --------------------
 
 drop table if exists BillingReportPaystation;
 
CREATE TABLE `BillingReportPaystation` (
  `BillingReportId` int unsigned NOT NULL,
  `CustomerId` mediumint unsigned NOT NULL, -- Customer
  `CustomerName` varchar(25) NOT NULL, -- Customer
  `IsTrial` tinyint unsigned NOT NULL, -- Customer
  `TrialExpiryGMT` datetime NULL , -- Customer
  `PaystationId` mediumint unsigned NOT NULL, -- Paystation
  `PointOfSaleId` mediumint unsigned NOT NULL, -- PointOfSale
  `POSName` varchar(30) NOT NULL, -- PointOfSale
  `POSLocationId` mediumint unsigned NOT NULL, -- PointOfSale  
  `ProvisionedGMT` datetime NOT NULL, 	-- PointOfSale
  `POSLastModifiedGMT` datetime NULL, -- PointOfSale
  `SerialNumber` varchar(20) NOT NULL, -- PointOfSale
  `PaystationSettingName` varchar(20) NULL, -- POSServiceState
  `LocationName` varchar(25)  NOT NULL, -- Location
  `LastHeartbeatGMT` datetime  NULL, -- POSHeartbeat
  `BillableChangeGMT` datetime DEFAULT NULL, -- POSDate
  `ActivationChangeGMT` datetime DEFAULT NULL, -- POSStatus
  `DigitalCollectChangeGMT` datetime DEFAULT NULL, -- POSDate
  `IsActivated` tinyint unsigned NOT NULL, -- POSStatus
  `IsLocked` tinyint unsigned NOT NULL, -- POSStatus
  `IsDigitalConnect` tinyint(3) unsigned NOT NULL, -- POSStatus
  `IsBillableMonthlyOnActivation` tinyint(3) unsigned NOT NULL, -- POSStatus
  `IsBillableMonthlyForEms` tinyint(3) unsigned NOT NULL, -- POSStatus
  `IsDecommissioned` tinyint(3) unsigned NOT NULL, -- POSStatus
  `IsDeleted` tinyint(3) unsigned NOT NULL, -- Paystation
  `APN` varchar(40) DEFAULT NULL, -- APN
  `APNTransactions` int NOT NULL DEFAULT '0', -- APN
  `ServiceStandardReports` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceAlerts` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceRealTimeCC` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceBatchCC` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceCampusCard` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceCoupon` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServicePasscard` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceSmartCard` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServicePayByPhone` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceDigitalAPIRead` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceDigitalAPIWrite` tinyint unsigned NOT NULL, -- CustomerSubscription
  `ServiceExtendByCell` tinyint unsigned NOT NULL, -- CustomerSubscription
  `HasChanged` tinyint(3) unsigned NOT NULL DEFAULT '0', -- CompareLogic i
  `TransactionCount` int unsigned DEFAULT NULL, -- Purchase  
  `Type16TxnCount` mediumint(8) unsigned NOT NULL DEFAULT '0', -- Purchase 
  `Type17TxnCount` mediumint(8) unsigned NOT NULL DEFAULT '0', -- Purchase 
  PRIMARY KEY (`BillingReportId`,`PointOfSaleId`),
  CONSTRAINT `fk_BillingReportPaystation_BillingReportId` 
	FOREIGN KEY (`BillingReportId`) 
	REFERENCES `BillingReport` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------
 -- Table: BillingReportChanges
 -- --------------------
 
 drop table if exists BillingReportChanges;
 
CREATE TABLE `BillingReportChanges` (
  `BillingReportId` int unsigned NOT NULL,   
  `PaystationId` mediumint unsigned NOT NULL, -- Paystation
  `PointOfSaleId` mediumint unsigned NOT NULL, -- PointOfSale  
  `CustomerId` mediumint unsigned NOT NULL, -- Customer
  `POSLocationId` mediumint unsigned NOT NULL, -- PointOfSale  
  `SerialNumber` varchar(20) NOT NULL, -- PointOfSale
  `IsActivated` varchar(5)  NULL, -- POSStatus   
  `CustomerName` varchar(30) NOT NULL, -- Customer
  `IsTrial`  varchar(10) NULL, -- Customer
  `TrialExpiryGMT` varchar(30) NULL , -- Customer
  `ActivationChangeGMT` varchar(30) DEFAULT NULL, -- POSStatus  
  `PaystationSettingName` varchar(25) NULL, -- POSServiceState
  `Location` varchar(30)  NULL,
  `ServiceStandardReports` varchar(5) NULL, -- CustomerSubscription
  `ServiceAlerts` varchar(5) NULL, -- CustomerSubscription
  `ServiceRealTimeCC` varchar(5) NULL, -- CustomerSubscription
  `ServiceBatchCC` varchar(5) NULL, -- CustomerSubscription
  `ServiceCampusCard` varchar(5) NULL, -- CustomerSubscription
  `ServiceCoupon` varchar(5) NULL, -- CustomerSubscription
  `ServicePasscard` varchar(5) NULL, -- CustomerSubscription
  `ServiceSmartCard` varchar(5) NULL, -- CustomerSubscription
  `ServicePayByPhone` varchar(5) NULL, -- CustomerSubscription
  `ServiceDigitalAPIRead` varchar(5) NULL, -- CustomerSubscription
  `ServiceDigitalAPIWrite` varchar(5) NULL, -- CustomerSubscription
  `ServiceExtendByCell` varchar(5) NULL, -- CustomerSubscription
  PRIMARY KEY (`BillingReportId`,`SerialNumber`),
  CONSTRAINT `fk_BillingReportChanges_BillingReportId` 
	FOREIGN KEY (`BillingReportId`) 
	REFERENCES `BillingReport` (`Id`) 
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 -- --------------------
 -- Table: BillingReportDC
 -- --------------------
 
 drop table if exists BillingReportDC  ;
 
 CREATE TABLE `BillingReportDC` (
  `Id` int unsigned NOT NULL AUTO_INCREMENT,
  `SerialNumber` varchar(20) NOT NULL,
  `LastModifiedTime` timestamp NOT NULL default Current_timestamp,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_BillingReportDC_uq` (`SerialNumber`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `CollectionSummaryConfig`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CollectionSummaryConfig` ;

CREATE  TABLE IF NOT EXISTS `CollectionSummaryConfig` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `RouteVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `LocationVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `CoinCountVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `CoinTotalVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `BillCountVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `BillTotalVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `CardCountVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `CardTotalVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `RunningTotalVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_colsummcfg_useraccount` (`UserAccountId` ASC) ,
  CONSTRAINT `fk_colsummcfg_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MaintenanceSummaryConfig`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MaintenanceSummaryConfig` ;

CREATE  TABLE IF NOT EXISTS `MaintenanceSummaryConfig` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UserAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `RouteVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `LocationVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `LastSeenVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `BatteryVoltageVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `PaperStatusVisible` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_mainsummcfg_useraccount` (`UserAccountId` ASC) ,
  CONSTRAINT `fk_mainsummcfg_useraccount`
    FOREIGN KEY (`UserAccountId` )
    REFERENCES `UserAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `POSCollectionUser`
-- -----------------------------------------------------

DROP TABLE IF EXISTS POSCollectionUser ;

CREATE  TABLE IF NOT EXISTS POSCollectionUser (
  Id 					INT 		UNSIGNED 	NOT NULL AUTO_INCREMENT ,  
  PointOfSaleId 		MEDIUMINT 	UNSIGNED 	NOT NULL ,	
  CollectionTypeId 		TINYINT 	UNSIGNED 	NOT NULL ,  -- All,Bill,Coin,Card
  UserAccountId 		MEDIUMINT 	UNSIGNED  	NULL ,  -- UserAccount Id who collected
  CollectionStartGMT	DATETIME 				NULL ,	-- Start Time of POSCollection 
  CollectionEndGMT		DATETIME 				NULL ,  -- End Time of POSCollection 
  POSCollectionId		INT 		UNSIGNED  	NULL ,		-- This will be populated later after receiving Collection (POSCollection) Record from Paystation
  MobileStartGMT	DATETIME 				NULL ,	-- Start Time of Collection (It can be same as End time of Collection)
  MobileEndGMT		DATETIME 				NULL ,  -- End Time of Collection 
  MobileReceivedGMT DATETIME				NULL ,  -- Timestamp of record received from Digital Collect
  PRIMARY KEY (Id) ,
  UNIQUE INDEX idx_POSCollectionUser_unq (PointOfSaleId , POSCollectionId, CollectionTypeId, CollectionStartGMT , CollectionEndGMT) ,
  UNIQUE INDEX idx_POSCollectionUser_unq2 (PointOfSaleId , UserAccountId, CollectionTypeId, MobileStartGMT , MobileEndGMT, MobileReceivedGMT ) ,
  INDEX idx_POSCollectionUser_pointofsale (PointOfSaleId ASC) ,
  INDEX idx_POSCollectionUser_collectiontype (CollectionTypeId ASC) ,
  INDEX idx_POSCollectionUser_receivedGMT (MobileReceivedGMT ASC) ,  
  INDEX idx_POSCollectionUser_CollectionEndGMT (PointOfSaleId,CollectionTypeId,CollectionEndGMT) ,
  INDEX idx_POSCollectionUser_MobileEndGMT (PointOfSaleId,CollectionTypeId,MobileEndGMT) ,
  CONSTRAINT fk_POSCollectionUser_UserAccount
    FOREIGN KEY (UserAccountId )
    REFERENCES UserAccount (Id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_POSCollectionUser_pointofsale
    FOREIGN KEY (PointOfSaleId )
    REFERENCES PointOfSale (Id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_POSCollectionUser_collectiontype
    FOREIGN KEY (CollectionTypeId )
    REFERENCES CollectionType (Id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_POSCollectionUser_POSCollectionId
    FOREIGN KEY (POSCollectionId )
    REFERENCES POSCollection (Id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- Rate Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  

-- -----------------------------------------------------
-- Table `RateType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateType` ;

CREATE  TABLE IF NOT EXISTS `RateType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_ratetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateFlatType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateFlatType` ;

CREATE  TABLE IF NOT EXISTS `RateFlatType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_rateflattype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `Rate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Rate` ;

CREATE  TABLE IF NOT EXISTS `Rate` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `RateTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsLocked` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsOverride` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `ArchiveDate` DATETIME NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_rate_multi_uq` (`CustomerId` ASC, `Name` ASC, `RateTypeId` ASC, `IsDeleted`, `ArchiveDate` ) ,
  INDEX `idx_Rate_customer` (`CustomerId` ASC) ,
  CONSTRAINT `fk_Rate_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rate_RateType`
    FOREIGN KEY (`RateTypeId` )
    REFERENCES `RateType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateAcceptedPayment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateAcceptedPayment` ;

CREATE  TABLE IF NOT EXISTS `RateAcceptedPayment` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsCoin` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsBill` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCreditCard` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsContactless` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsSmartCard` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsPasscard` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsCoupon` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsPromptForCoupon` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RatePaymentType_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RatePaymentType_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateAddTime`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateAddTime` ;

CREATE  TABLE IF NOT EXISTS `RateAddTime` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `IsAddTimeNumber` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsSpaceOrPlateNumber` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsEbP` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `ServiceFeeAmount` MEDIUMINT UNSIGNED NULL ,
  `MinExtention` SMALLINT UNSIGNED NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateAddTime_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateAddTime_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateFlat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateFlat` ;

CREATE  TABLE IF NOT EXISTS `RateFlat` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateFlat_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateFlat_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateHourly`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateHourly` ;

CREATE  TABLE IF NOT EXISTS `RateHourly` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `MaximumHours` SMALLINT UNSIGNED NOT NULL , 
  `IsVariable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateHourly_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateHourly_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateHourlyRuleType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateHourlyRuleType` ;

CREATE  TABLE IF NOT EXISTS `RateHourlyRuleType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_ratetype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateHourlyRule
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateHourlyRule` ;

CREATE  TABLE IF NOT EXISTS `RateHourlyRule` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateHourlyRuleTypeId` TINYINT UNSIGNED NOT NULL ,
  `Hours` TINYINT NOT NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_RateHourlyRule_Rate` (`RateId` ASC) ,
  INDEX `idx_RateHourlyRule_RateHourlyRuleType` (`RateHourlyRuleTypeId` ASC) ,
  CONSTRAINT `fk_RateHourlyRule_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateHourlyRule_RateHourlyRuleType`
    FOREIGN KEY (`RateHourlyRuleTypeId` )
    REFERENCES `RateHourlyRuleType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateDaily`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateDaily` ;

CREATE  TABLE IF NOT EXISTS `RateDaily` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `MaximumDays` SMALLINT UNSIGNED NOT NULL , 
  `MonAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `TueAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `WedAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `ThuAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `FriAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `SatAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `SunAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `IsVariable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateDaily_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateDaily_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateMonthly`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateMonthly` ;

CREATE  TABLE IF NOT EXISTS `RateMonthly` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `IsLimited` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `MaximumPurchases` SMALLINT UNSIGNED NOT NULL DEFAULT '0' , 
  `DaysBeforeStart` TINYINT UNSIGNED NOT NULL DEFAULT '0' ,
  `DaysAfterStart` TINYINT UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateMonthly_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateMonthly_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateIncremental`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateIncremental` ;

CREATE  TABLE IF NOT EXISTS `RateIncremental` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `Length` TINYINT UNSIGNED NOT NULL ,
  `Amount` MEDIUMINT UNSIGNED NOT NULL ,
  `MinAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `MaxAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `IsFlatRateEnabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `FlatRateStartTime` VARCHAR(8) NULL ,
  `FlatRateEndTime` VARCHAR(8) NULL ,
  `FlatRateAmount` MEDIUMINT UNSIGNED NULL ,
  `IsUseLastIncrementValue` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `CCInc01` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc02` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc03` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc04` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc05` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc06` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc07` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc08` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc09` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `CCInc10` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateIncremental_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateIncremental_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateRestriction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateRestriction` ;

CREATE  TABLE IF NOT EXISTS `RateRestriction` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `MessageTimeMin` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  `Message1` VARCHAR(40) NULL ,
  `Message2` VARCHAR(40) NULL ,
  `Message3` VARCHAR(40) NULL ,
  `Message4` VARCHAR(40) NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateRestriction_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateRestriction_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateHoliday`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateHoliday` ;

CREATE  TABLE IF NOT EXISTS `RateHoliday` (
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`RateId`) ,
  INDEX `idx_RateHoliday_Rate` (`RateId` ASC) ,
  CONSTRAINT `fk_RateHoliday_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateProfile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateProfile` ;

CREATE  TABLE IF NOT EXISTS `RateProfile` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `PermitIssueTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsPublished` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `ArchiveDate` DATETIME NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_rate_multi_uq` (`CustomerId` ASC, `Name` ASC, `IsDeleted`, `ArchiveDate` ) ,
  INDEX `idx_RateProfile_customer` (`CustomerId` ASC) ,
  CONSTRAINT `fk_RateProfile_customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfile_PermitIssueType`
    FOREIGN KEY (`PermitIssueTypeId` )
    REFERENCES `PermitIssueType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateExpiryDayType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateExpiryDayType` ;

CREATE  TABLE IF NOT EXISTS `RateExpiryDayType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_RateExpiryDayType_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateRateProfile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateRateProfile` ;

CREATE  TABLE IF NOT EXISTS `RateRateProfile` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateProfileId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateFlatTypeId` TINYINT UNSIGNED NULL ,
  `Description` VARCHAR(40) NULL ,
  `IsRateBlendingEnabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsOverride` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `DisplayPriority` TINYINT UNSIGNED NOT NULL DEFAULT '0' ,
  `AvailabilityStartDate` DATETIME NULL ,
  `AvailabilityEndDate` DATETIME NULL ,
  `AvailabilityStartTime` VARCHAR(8) NULL ,
  `AvailabilityEndTime` VARCHAR(8) NULL ,
  `DurationNumberOfDays` SMALLINT UNSIGNED NULL ,
  `DurationNumberOfHours` SMALLINT UNSIGNED NULL ,
  `DurationNumberOfMins` SMALLINT UNSIGNED NULL ,
  `IsMaximumExpiryEnabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `RateExpiryDayTypeId` TINYINT UNSIGNED  NULL ,
  `ExpiryTime` VARCHAR(8) NULL ,
  `IsAdvancePurchaseEnabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsScheduled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsSunday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsMonday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsTuesday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsWednesday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsThursday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsFriday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsSaturday` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_RateProfileRate_Customer` (`CustomerId` ASC) ,
  INDEX `idx_RateProfileRate_Rate` (`RateId` ASC) ,
  INDEX `idx_RateProfileRate_RateProfile` (`RateProfileId` ASC) ,
  CONSTRAINT `fk_RateProfileRate_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileRate_Rate`
    FOREIGN KEY (`RateId` )
    REFERENCES `Rate` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileRate_RateProfile`
    FOREIGN KEY (`RateProfileId` )
    REFERENCES `RateProfile` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileRate_RateFlatType`
    FOREIGN KEY (`RateFlatTypeId` )
    REFERENCES `RateFlatType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileRate_RateExpiryDayType`
    FOREIGN KEY (`RateExpiryDayTypeId` )
    REFERENCES `RateExpiryDayType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RateProfileLocation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateProfileLocation` ;

CREATE  TABLE IF NOT EXISTS `RateProfileLocation` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `RateProfileId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `SpaceRange` VARCHAR(255) NULL ,
  `IsCycled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `StartGMT` DATETIME NOT NULL ,
  `EndGMT` DATETIME NULL ,
  `NextPublishGMT` DATETIME NOT NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `IsPublished` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsDeleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_RateProfileLocation_Customer` (`CustomerId` ASC) ,
  INDEX `idx_RateProfileLocation_RateProfile` (`RateProfileId` ASC) ,
  INDEX `idx_RateProfileLocation_Location` (`LocationId` ASC) ,
  CONSTRAINT `fk_RateProfileLocation_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileLocation_RateProfile`
    FOREIGN KEY (`RateProfileId` )
    REFERENCES `RateProfile` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RateProfileLocation_Location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RateGlobalSettings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RateGlobalSettings` ;

CREATE  TABLE IF NOT EXISTS `RateGlobalSettings` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `GracePeriodMins` TINYINT UNSIGNED NOT NULL ,
  `IsRoundDown` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsShowTaxOnScreen` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsShowTaxOnReceipt` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `IsIncludeTaxInRate` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `Tax1Name` VARCHAR(40) NULL ,
  `Tax1Rate` TINYINT UNSIGNED NOT NULL ,
  `Tax2Name` VARCHAR(40) NULL ,
  `Tax2Rate` TINYINT UNSIGNED NOT NULL ,
  `VERSION` INT(11) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_RateGlobalSettings_Customer` (`CustomerId` ASC) ,
  CONSTRAINT `fk_RateGlobalSettings_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CardProcessMethodType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardProcessMethodType` ;

CREATE  TABLE IF NOT EXISTS `CardProcessMethodType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `cardtype_name_uq` (`Name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CardProcessTransactionType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CardProcessTransactionType` ;

CREATE  TABLE IF NOT EXISTS `CardProcessTransactionType` (
  `Id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CardProcessMethodTypeId` TINYINT UNSIGNED NOT NULL  ,
  `ProcessorTransactionTypeId` tinyint UNSIGNED NOT NULL ,  
  PRIMARY KEY (`Id`) ,  
  CONSTRAINT `fk_CardProcessTransactionType_ProcessorTransactionTypeId`
    FOREIGN KEY (`ProcessorTransactionTypeId` )
    REFERENCES `ProcessorTransactionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_CardProcessTransactionType_CardProcessMethodTypeId`
    FOREIGN KEY (`CardProcessMethodTypeId` )
    REFERENCES `CardProcessMethodType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) 	
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `CardTransactionTotalHour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardTransactionTotalHour` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionTotalHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,  
  `CardProcessMethodTypeId` TINYINT UNSIGNED NOT NULL ,  
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionTotalHour_unq` (`TimeIdLocal` ,`CustomerId`, `CardProcessMethodTypeId`) ,
  INDEX idx_CardTransactionTotalHour_CustomerId ( CustomerId ),
  INDEX idx_CardTransactionTotalHour_multi ( CustomerId, TimeIdGMT ),
  INDEX idx_CardTransactionTotalHour_multi2 ( CustomerId, TimeIdLocal ) ,
  CONSTRAINT `fk_CardTransactionTotalHour_time1`
    FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CardTransactionTotalHour_time2`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



  
-- -----------------------------------------------------
-- Table `CardTransactionTotalDay`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardTransactionTotalDay` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionTotalDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CardProcessMethodTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionTotalDay_unq` (`TimeIdLocal`,`CustomerId`,`CardProcessMethodTypeId`) ,
  INDEX idx_CardTransactionTotalDay_CustomerId ( CustomerId ) ,
  CONSTRAINT `fk_CardTransactionTotalDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



  
  
-- -----------------------------------------------------
-- Table `CardTransactionTotalMonth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CardTransactionTotalMonth` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionTotalMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `CardProcessMethodTypeId` TINYINT UNSIGNED NOT NULL ,
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionTotalMonth_unq` (`TimeIdLocal`,`CustomerId`,`CardProcessMethodTypeId`) ,
  INDEX idx_CardTransactionTotalMonth_CustomerId ( CustomerId ) ,
  INDEX idx_CardTransactionTotalMonth_TimeIdLocal ( TimeIdLocal ),
  CONSTRAINT `fk_CardTransactionTotalMonth_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

  
-- -----------------------------------------------------
-- Table `CardTransactionDetailHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CardTransactionDetailHour` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionDetailHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CardTransactionTotalHourId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,  
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,  
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionDetailHour_unq` (`CardTransactionTotalHourId` ,`PurchaseLocationId`, `PointOfSaleId`) ,
  CONSTRAINT `fk_CardTransactionDetailHour_CardTransactionTotalHourId`
    FOREIGN KEY (`CardTransactionTotalHourId` )
    REFERENCES `CardTransactionTotalHour` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `CardTransactionDetailDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CardTransactionDetailDay` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionDetailDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CardTransactionTotalDayId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,  
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL , 
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionDetailDay_unq` (`CardTransactionTotalDayId` ,`PurchaseLocationId`, `PointOfSaleId`) ,
  CONSTRAINT `fk_CardTransactionDetailDay_CardTransactionTotalDayId`
    FOREIGN KEY (`CardTransactionTotalDayId` )
    REFERENCES `CardTransactionTotalDay` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CardTransactionDetailMonth`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CardTransactionDetailMonth` ;

CREATE  TABLE IF NOT EXISTS `CardTransactionDetailMonth` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CardTransactionTotalMonthId` BIGINT UNSIGNED NOT NULL ,
  `PurchaseLocationId` MEDIUMINT UNSIGNED NOT NULL ,  
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,   
  `TotalAmount` INT UNSIGNED NOT NULL ,
  `TotalCount` MEDIUMINT UNSIGNED NOT NULL  DEFAULT 1,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CardTransactionDetailMonth_unq` (`CardTransactionTotalMonthId` ,`PurchaseLocationId`, `PointOfSaleId`) ,
  INDEX idx_CardTransactionDetailMonth_TotalMonthId_PurLocId (CardTransactionTotalMonthId, PurchaseLocationId) ,
  INDEX idx_CardTransactionDetailMonth_PurchaseLocationId (PurchaseLocationId) ,
  INDEX idx_CardTransactionDetailMonth_CardTransactionTotalMonthId (CardTransactionTotalMonthId) ,
  CONSTRAINT `fk_CardTransactionDetailMonth_CardTransactionTotalMonthId`
    FOREIGN KEY (`CardTransactionTotalMonthId` )
    REFERENCES `CardTransactionTotalMonth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ETLExecutionMessage`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLExecutionMessage` ;

CREATE  TABLE IF NOT EXISTS `ETLExecutionMessage` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Message` VARCHAR(300) NOT NULL ,
  `RecordInsertTime` DATETIME  NULL ,  
  PRIMARY KEY (`Id`) 
  )
ENGINE = InnoDB;  

-- -----------------------------------------------------
-- Table `TDebug`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TDebug` ;

CREATE  TABLE IF NOT EXISTS `TDebug` (
  Id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  PointOfSaleId MEDIUMINT UNSIGNED NOT NULL ,
  CashAmount INT UNSIGNED NULL,
  CancelledCashAmount INT UNSIGNED NULL,
  CoinCount MEDIUMINT UNSIGNED NULL,
  CancelledCoinCount MEDIUMINT UNSIGNED NULL,
  CoinAmount INT UNSIGNED NULL,
  CancelledCoinAmount INT UNSIGNED NULL,
  Message VARCHAR(300) NOT NULL ,
  RecordInsertTime DATETIME  NULL ,  
  PRIMARY KEY (`Id`) 
  )
ENGINE = InnoDB;  



-- -----------------------------------------------------
-- Table `BillingReportDateRange`
-- -----------------------------------------------------


DROP TABLE IF EXISTS `BillingReportDateRange` ;

CREATE  TABLE IF NOT EXISTS `BillingReportDateRange` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DateRangeBegin` Date NOT NULL ,
  `DateRangeEnd` Date NOT NULL ,
  `IsPosted` TINYINT UNSIGNED NOT NULL ,  
  `ExecutionDateTime` DateTime NOT NULL ,  
  `Status` TINYINT UNSIGNED NOT NULL ,  
  PRIMARY KEY (`Id`)  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `AutoIncrementAlert`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AutoIncrementAlert` ;

CREATE  TABLE IF NOT EXISTS `AutoIncrementAlert` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `SchemaName` varchar(20) NOT NULL ,
  `TableName` varchar(100) NOT NULL ,
  `DataType` varchar(20) NOT NULL ,
  `CurrentAutoIncValue` BIGINT UNSIGNED NOT NULL ,
  `RemainingBuffer` BIGINT UNSIGNED NOT NULL ,
  `ReportRunDate` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_multi_uq` (	`SchemaName`, `TableName`,`ReportRunDate` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ETLProcessType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ETLProcessType` ;

CREATE  TABLE IF NOT EXISTS `ETLProcessType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_ETLProcessType_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-- -----------------------------------------------------
-- New Alert Processing Tables
-- -----------------------------------------------------
-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

-- -----------------------------------------------------
-- Table `EventType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EventType` ;

CREATE  TABLE IF NOT EXISTS `EventType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Description` VARCHAR(255) NOT NULL ,
  `EventDeviceTypeId` TINYINT UNSIGNED NOT NULL ,
  `IsAlert` TINYINT(1) UNSIGNED NOT NULL ,
  `IsNotification` TINYINT(1) UNSIGNED NOT NULL ,
  `IsAutoClear` TINYINT(1) UNSIGNED NOT NULL ,
  `HasLevel` TINYINT(1) UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_EventType_Description_uq` (`Description` ASC),
  CONSTRAINT `fk_EventTypes_EventDeviceType`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EventException`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EventException` ;

CREATE  TABLE IF NOT EXISTS `EventException` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PaystationTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventDeviceTypeId` TINYINT UNSIGNED NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_multi_uq` (	`PaystationTypeId`, `EventDeviceTypeId` ASC),
  CONSTRAINT `fk_EventException_PaystationType`
    FOREIGN KEY (`PaystationTypeId` )
    REFERENCES `PaystationType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_EventException_EventDeviceType`
    FOREIGN KEY (`EventDeviceTypeId` )
    REFERENCES `EventDeviceType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Pay station events to be ignored';

-- -----------------------------------------------------
-- Table `POSEventCurrent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSEventCurrent` ;

CREATE  TABLE IF NOT EXISTS `POSEventCurrent` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `EventTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerAlertTypeId` MEDIUMINT UNSIGNED NULL ,
  `EventSeverityTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventActionTypeId` TINYINT UNSIGNED NULL ,
  `AlertInfo` VARCHAR(50) NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL ,
  `IsHidden` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `AlertGMT` DATETIME NULL ,
  `ClearedGMT` DATETIME NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_multi_uq` (	`PointOfSaleId`, `EventTypeId`,`CustomerAlertTypeId` ASC),
  CONSTRAINT `fk_POSEventStatus_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventStatus_EventType`
    FOREIGN KEY (`EventTypeId` )
    REFERENCES `EventType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventStatus_CustomerAlertType`
    FOREIGN KEY (`CustomerAlertTypeId` )
    REFERENCES `CustomerAlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventStatus_EventSeverityType`
    FOREIGN KEY (`EventSeverityTypeId` )
    REFERENCES `EventSeverityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventStatus_EventActionType`
    FOREIGN KEY (`EventActionTypeId` )
    REFERENCES `EventActionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Current status of a POS Event';

-- -----------------------------------------------------
-- Table `POSEventHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `POSEventHistory` ;

CREATE  TABLE IF NOT EXISTS `POSEventHistory` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `EventTypeId` TINYINT UNSIGNED NOT NULL ,
  `CustomerAlertTypeId` MEDIUMINT UNSIGNED NULL ,
  `EventSeverityTypeId` TINYINT UNSIGNED NOT NULL ,
  `EventActionTypeId` TINYINT UNSIGNED NULL ,
  `AlertInfo` VARCHAR(50) NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL ,
  `TimestampGMT` DATETIME NULL ,
  `CreatedGMT` DATETIME NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_POSEventHistory_multi_uq` (	`PointOfSaleId`, `EventTypeId`,`CustomerAlertTypeId`,`EventActionTypeId`,`TimestampGMT`,`IsActive` ASC),
  CONSTRAINT `fk_POSEventHistory_PointOfSale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventHistory_EventType`
    FOREIGN KEY (`EventTypeId` )
    REFERENCES `EventType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventHistory_CustomerAlertType`
    FOREIGN KEY (`CustomerAlertTypeId` )
    REFERENCES `CustomerAlertType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventHistory_EventSeverityType`
    FOREIGN KEY (`EventSeverityTypeId` )
    REFERENCES `EventSeverityType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_POSEventHistory_EventActionType`
    FOREIGN KEY (`EventActionTypeId` )
    REFERENCES `EventActionType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Historical records of POS Events';

/* NOT USED FOR NOW
-- -----------------------------------------------------
-- Table `LocationLotGeopoint`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `LocationLotGeopoint` ;

CREATE  TABLE IF NOT EXISTS `LocationLotGeopoint` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,   
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,  
  `Latitude` decimal(18,14) NOT NULL ,
  `Longitude` decimal(18,14) NOT NULL ,
  `LastModifiedGMT` datetime NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_LocationLotGeopoint_multi` (LocationId,Id),
CONSTRAINT `fk_LocationLotGeopoint_LocationId`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LocationLine`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `LocationLine` ;

CREATE  TABLE IF NOT EXISTS `LocationLine` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL , 
  `LastModifiedGMT` datetime NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_LocationLine_multi` (LocationId,Id),
CONSTRAINT `fk_LocationLine_LocationId`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `LocationLineGeopoint`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `LocationLineGeopoint` ;

CREATE  TABLE IF NOT EXISTS `LocationLineGeopoint` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `LocationLineId` BIGINT UNSIGNED NOT NULL ,
  `Latitude` decimal(18,14) NOT NULL ,
  `Longitude` decimal(18,14) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_LocationLineGeopoint_LocationLineId` (LocationLineId) ,
  CONSTRAINT `fk_LocationLineGeopoint_LocationLineId`
    FOREIGN KEY (`LocationLineId` )
    REFERENCES `LocationLine` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GeopointType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `GeopointType` ;

CREATE  TABLE IF NOT EXISTS `GeopointType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_GeopointType_name_uq` (`Name` ASC) )
ENGINE = InnoDB;

*/ 

-- -----------------------------------------------------
-- Table `LocationLineGeopoint`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `LocationLineGeopoint` ;

CREATE  TABLE IF NOT EXISTS `LocationLineGeopoint` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,  
  `JSONString` VARCHAR(5000) NOT NULL ,  
  PRIMARY KEY (`Id`) ,
  INDEX `idx_LocationLineGeopoint_LocationLineId` (LocationId) ,
  CONSTRAINT `fk_LocationLineGeopoint_LocationId`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION	)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `LocationLineCoordinate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `LocationLineCoordinate` ;

CREATE  TABLE IF NOT EXISTS `LocationLineCoordinate` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `LineId` BIGINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NOT NULL ,
  `Latitude` DECIMAL(18,14) NOT NULL ,
  `Longitude` DECIMAL(18,14) NOT NULL ,  
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_LocationLineCoordinate_location` (`LocationId` ASC) ,
  INDEX `idx_LocationLineCoordinate_multi_uq1` (`LocationId` ASC, `LineId` ASC) ,  
  CONSTRAINT `fk_LocationLineCoordinate_location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `CaseCustomerAccount` Mapping Table. Maps Iris and Case Customers
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CaseCustomerAccount` ;

CREATE  TABLE IF NOT EXISTS `CaseCustomerAccount` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NULL , 
  `CaseAccountId` INT UNSIGNED NOT NULL ,
  `CaseAccountName` VARCHAR(128) NOT NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `LastUpdatedGMT` DATETIME NOT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_CaseCustomerAccount_uq` (`CustomerId`, `CaseAccountId`) ,  
CONSTRAINT `fk_CaseCustomerAccount_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CaseCustomerFacility` Mapping Table. Maps Iris Customer and Case Facility
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CaseCustomerFacility` ;

CREATE  TABLE IF NOT EXISTS `CaseCustomerFacility` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CaseCustomerAccountId` INT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NULL , 
  `FacilityId` INT UNSIGNED NOT NULL ,
  `FacilityName` VARCHAR(128) NOT NULL ,
  `IsActive` TINYINT(1) NOT NULL DEFAULT 0,
  `TimeZone` varchar(30) NULL,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_CaseLocationFacility_multi_uq` (`CustomerId`, `FacilityId`) ,  
CONSTRAINT `fk_CaseLocationFacility_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_CaseLocationFacility_CaseCustomerAccount`
    FOREIGN KEY (`CaseCustomerAccountId` )
    REFERENCES `CaseCustomerAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB ;

-- -----------------------------------------------------
-- Table `CaseLocationLot` Mapping Table. Maps Iris Location and Case Lot
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CaseLocationLot` ;

CREATE  TABLE IF NOT EXISTS `CaseLocationLot` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CaseCustomerFacilityId` INT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NULL , 
  `LocationId` MEDIUMINT UNSIGNED NULL,
  `LotId` INT UNSIGNED NOT NULL ,
  `LotName` VARCHAR(128) NOT NULL ,
  `IsActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `TotalSpaces` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `Latitude` DECIMAL(18,14) NOT NULL ,
  `Longitude` DECIMAL(18,14) NOT NULL ,
  `Occupancy` MEDIUMINT UNSIGNED NULL,
  `TimeIdGMT` MEDIUMINT NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_CaseLocationLot_multi_uq` (`CustomerId`, `LocationId`, `LotId`) ,  
CONSTRAINT `fk_CaseLocationLot_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_CaseLocationLot_Location`
    FOREIGN KEY (`LocationId` )
    REFERENCES `Location` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_CaseLocationLot_CaseCustomerFacility`
    FOREIGN KEY (`CaseCustomerFacilityId` )
    REFERENCES `CaseCustomerFacility` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB ;

-- -----------------------------------------------------
-- Table `CaseOccupancyHour`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CaseOccupancyHour` ;

CREATE  TABLE IF NOT EXISTS `CaseOccupancyHour` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdGMT` MEDIUMINT UNSIGNED NOT NULL ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL ,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED DEFAULT 0  ,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CaseOccupancyHour_unq` (`TimeIdGMT` ,`CustomerId`, `LocationId`) ,   
  CONSTRAINT `fk_CaseOccupancyHour_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CaseOccupancyHour_time2`
    FOREIGN KEY (`TimeIdGMT` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_CaseOccupancyHour_custId`
     FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)   
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CaseOccupancyDay`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `CaseOccupancyDay` ;

CREATE  TABLE IF NOT EXISTS `CaseOccupancyDay` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `TimeIdLocal` MEDIUMINT UNSIGNED NOT NULL , -- Begining of the Day 00:00 hrs
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL ,
  `LocationId` MEDIUMINT UNSIGNED NULL ,
  `NumberOfSpaces` MEDIUMINT UNSIGNED NULL ,
  `NoOfPermits` MEDIUMINT UNSIGNED NULL  DEFAULT 1,  
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_CaseOccupancyDay_unq` (`TimeIdLocal` ,`CustomerId`, `LocationId`) ,
    CONSTRAINT `fk_CaseOccupancyDay_time1`
    FOREIGN KEY (`TimeIdLocal` )
    REFERENCES `Time` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CaseOccupancyDay_custId`
     FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `TransactionSettlementStatusType` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TransactionSettlementStatusType` ;

CREATE  TABLE IF NOT EXISTS `TransactionSettlementStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,  
  `Description` VARCHAR(255) NOT NULL ,  
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BatchStatusType` 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BatchStatusType` ;

CREATE  TABLE IF NOT EXISTS `BatchStatusType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,    
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ElavonRequestType` 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ElavonRequestType` ;

CREATE  TABLE IF NOT EXISTS `ElavonRequestType` (
  `Id` TINYINT UNSIGNED NOT NULL ,
  `Code` VARCHAR(3) NOT NULL ,
  `CaptureTranCode` TINYINT UNSIGNED NULL ,
  `RequestName` VARCHAR(30) NOT NULL ,    
  `Description` VARCHAR(255) NOT NULL ,  
  `LastModifiedGMT` DATETIME NOT NULL ,
  `LastModifiedByUserId` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ElavonTransactionDetail` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ElavonTransactionDetail` ;

CREATE  TABLE IF NOT EXISTS `ElavonTransactionDetail` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,    
  `ElavonTransactionDetailId` BIGINT UNSIGNED NULL,
  `PreAuthId` BIGINT UNSIGNED NULL ,
  `ProcessorTransactionId` BIGINT UNSIGNED NULL ,  
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `PurchasedDate` DATETIME  NULL,
  `TicketNumber` INT UNSIGNED  NULL , 
  `CardData` VARCHAR(100)  NULL,
  `ServiceCode` SMALLINT  NULL,
  `POSEntryCapability` TINYINT UNSIGNED NOT  NULL,
  `AccountEntryMode` TINYINT UNSIGNED NOT  NULL,
  `TransactionAmount` MEDIUMINT UNSIGNED NOT NULL,
  `OriginalAuthAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `ResponseCode` VARCHAR(2) NOT NULL ,
  `ApprovalCode` VARCHAR(6)  NULL ,
  `AuthorizationSource` CHAR(1) NULL ,
  `AuthorizedAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `AuthorizationDateTime` Datetime NOT NULL,
  `BatchNumber` SMALLINT UNSIGNED NOT NULL ,
  `TraceNumber` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionReferenceNbr`INT UNSIGNED NOT NULL ,
  `PS2000Data` VARCHAR(22)  NULL,
  `MSDI` CHAR(1) NULL ,
  `ElavonRequestTypeId` TINYINT UNSIGNED NOT NULL ,
  `NumRetries` TINYINT UNSIGNED NOT NULL Default 0,
  `TransactionSettlementStatusTypeId` TINYINT UNSIGNED NULL,  
  `CreatedGMT` DATETIME NOT NULL,  
  `SettlementGMT` DATETIME NULL,  
  PRIMARY KEY (`Id`),   
  INDEX idx_ElavonTransactionDetail_ind1 (PreAuthId, AuthorizationDateTime) ,
  INDEX idx_ElavonTransactionDetail_ind2 (MerchantAccountId, BatchNumber) ,
  CONSTRAINT `fk_ElavonTransactionDetail_PreAuthId`
    FOREIGN KEY (`PreAuthId` )
    REFERENCES `PreAuth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ElavonTransactionDetail_TransactionSettlementStatusTypeId`
    FOREIGN KEY (`TransactionSettlementStatusTypeId` )
    REFERENCES `TransactionSettlementStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT `fk_ElavonTransactionDetail_MerchantAccountId`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_ElavonTransactionDetail_PointOfSaleId`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_ElavonTransactionDetail_ProcessorTransactionId`
    FOREIGN KEY (`ProcessorTransactionId` )
    REFERENCES `ProcessorTransaction` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_ElavonTransactionDetail_ElavonRequestTypeId`
    FOREIGN KEY (`ElavonRequestTypeId` )
    REFERENCES `ElavonRequestType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `fk_ElavonTransactionDetail_ElavonTransactionDetailId`
    FOREIGN KEY (`ElavonTransactionDetailId` )
    REFERENCES `ElavonTransactionDetail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `ElavonRequestRetry` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ElavonRequestRetry` ;

CREATE  TABLE IF NOT EXISTS `ElavonRequestRetry` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `PreAuthId` BIGINT UNSIGNED NULL ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,  
  `PurchasedDate` DATETIME NOT NULL,
  `TicketNumber` INT UNSIGNED NOT NULL , 
  `CardData` VARCHAR(100) NOT NULL,
  `OriginalAuthAmount` MEDIUMINT UNSIGNED NOT NULL ,
  `TransactionAmount` MEDIUMINT UNSIGNED NOT NULL,
  `NoOfRetry` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `IsRetrySuccessful` TINYINT NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL,  
  `LastRetryGMT` DATETIME  NULL,  
  PRIMARY KEY (`Id`), 
  INDEX idx_ElavonRequestRetry_multi (MerchantAccountId,PointOfSaleId) ,
  CONSTRAINT `fk_ElavonRequestRetry_PreAuthId`
    FOREIGN KEY (`PreAuthId` )
    REFERENCES `PreAuth` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ElavonRequestRetry_MerchantAccountId`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT `fk_ElavonRequestRetry_PointOfSaleId`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)  
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ActiveBatchSummary` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ActiveBatchSummary` ;

CREATE  TABLE IF NOT EXISTS `ActiveBatchSummary` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `MerchantAccountId` MEDIUMINT UNSIGNED NOT NULL ,
  `BatchNumber` SMALLINT UNSIGNED NOT NULL ,
  `NoOfTransactions` SMALLINT UNSIGNED NOT NULL DEFAULT 1, 
  `BatchStatusTypeId` TINYINT UNSIGNED NOT NULL DEFAULT 1, 
  `CreatedGMT` DATETIME NOT NULL,  
  `LastModifiedGMT` DATETIME  NULL ,
  PRIMARY KEY (`Id`),   
  UNIQUE INDEX ind_ActiveBatchSummary_multi (MerchantAccountId , BatchNumber),
  CONSTRAINT `fk_ActiveBatchSummary_MerchantAccountId`
    FOREIGN KEY (`MerchantAccountId` )
    REFERENCES `MerchantAccount` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT `fk_ActiveBatchSummary_BatchStatusTypeId`
    FOREIGN KEY (`BatchStatusTypeId` )
    REFERENCES `BatchStatusType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `BatchSettlement` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `BatchSettlement` ;

CREATE  TABLE IF NOT EXISTS `BatchSettlement` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,  
  `ActiveBatchSummaryId` BIGINT UNSIGNED NOT NULL ,
  `ClusterId` SMALLINT UNSIGNED NOT NULL ,
  `BatchSize` SMALLINT UNSIGNED NOT NULL ,
  `BatchSettlementStartGMT` DATETIME NOT NULL,
  `BatchSettlementEndGMT` DATETIME  NULL,
  PRIMARY KEY (`Id`), 
  CONSTRAINT `fk_BatchSettlement_ActiveBatchSummaryId`
    FOREIGN KEY (`ActiveBatchSummaryId` )
    REFERENCES `ActiveBatchSummary` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BatchSettlement_ClusterId`
    FOREIGN KEY (`ClusterId` )
    REFERENCES `Cluster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) 
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `BatchSettlementDetail` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `BatchSettlementDetail` ;

CREATE  TABLE IF NOT EXISTS `BatchSettlementDetail` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,  
  `BatchSettlementId` BIGINT UNSIGNED NOT NULL ,
  `ElavonTransactionDetailId` BIGINT UNSIGNED NOT NULL ,    
  PRIMARY KEY (`Id`), 
  CONSTRAINT `fk_BatchSettlement_ElavonTransactionDetailId`
    FOREIGN KEY (`ElavonTransactionDetailId` )
    REFERENCES `ElavonTransactionDetail` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BatchSettlement_BatchSettlementId`
    FOREIGN KEY (`BatchSettlementId` )
    REFERENCES `BatchSettlement` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StagingPreAuthHoldingToRetry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StagingPreAuthHoldingToRetry` ;

CREATE  TABLE IF NOT EXISTS `StagingPreAuthHoldingToRetry` (
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `PreAuthId` varchar(30) NULL,
  `Action` Varchar(6) NOT NULL,
  `CreatedGMT` DATETIME NOT NULL  ,
  INDEX ind_StagingPreAuthHoldingToRetry_ProcTrans (`ProcessorTransactionId`) 
)  
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BatchSettlementType` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `BatchSettlementType` ;

CREATE  TABLE IF NOT EXISTS `BatchSettlementType` (
  `Id` TINYINT UNSIGNED NOT NULL ,  
  `Description` VARCHAR(20)  NOT NULL ,
  PRIMARY KEY (`Id`)) 
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BatchSettlement` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `BatchSettlement` ;

CREATE  TABLE IF NOT EXISTS `BatchSettlement` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,  
  `ActiveBatchSummaryId` BIGINT UNSIGNED NOT NULL ,
  `BatchSettlementTypeId` TINYINT UNSIGNED NOT NULL ,
  `ClusterId` SMALLINT UNSIGNED NOT NULL ,
  `BatchSize` SMALLINT UNSIGNED NOT NULL ,
  `BatchSettlementStartGMT` DATETIME NOT NULL,
  `BatchSettlementEndGMT` DATETIME  NULL,
  PRIMARY KEY (`Id`), 
  CONSTRAINT `fk_BatchSettlement_ActiveBatchSummaryId`
    FOREIGN KEY (`ActiveBatchSummaryId` )
    REFERENCES `ActiveBatchSummary` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BatchSettlement_ClusterId`
    FOREIGN KEY (`ClusterId` )
    REFERENCES `Cluster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
   CONSTRAINT `fk_BatchSettlement_BatchSettlementTypeId`
    FOREIGN KEY (`BatchSettlementTypeId` )
    REFERENCES `BatchSettlementType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) 
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ArchiveStagingPreAuthHoldingToRetry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ArchiveStagingPreAuthHoldingToRetry` ;

CREATE  TABLE IF NOT EXISTS `ArchiveStagingPreAuthHoldingToRetry` (
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `IsMovedToCardRetry` TINYINT NOT NULL,
  `MovedGMT` DATETIME NOT NULL  ,
  PRIMARY KEY (`ProcessorTransactionId`)
)  
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TransactionFileUpload`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TransactionFileUpload` ;

CREATE  TABLE IF NOT EXISTS `TransactionFileUpload` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CustomerId` MEDIUMINT UNSIGNED NOT NULL , 
  `FileName` varchar(200) NOT NULL ,
  `CheckSum` varchar(200) NOT NULL ,
  `UploadedGMT` DATETIME NULL,  
  `NoOfUploadAttempt` INT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_TransactionFileUpload_multi_uq` (`CustomerId`, `FileName`, `CheckSum`) ,  
CONSTRAINT `fk_TransactionFileUpload_Customer`
    FOREIGN KEY (`CustomerId` )
    REFERENCES `Customer` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB ;


-- -----------------------------------------------------
-- Table `TelemetryType` 
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TelemetryType` ;

CREATE  TABLE IF NOT EXISTS `TelemetryType` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NOT NULL,
  `Label` VARCHAR(255) NOT NULL,
  `IsPrivate` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `idx_telemetrytype_name_uq` (`Name` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PosTelemetry`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `POSTelemetry` ;

CREATE  TABLE IF NOT EXISTS `POSTelemetry` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PointOfSaleId` MEDIUMINT UNSIGNED NOT NULL ,
  `TelemetryTypeId` MEDIUMINT UNSIGNED NOT NULL ,
  `Value` VARCHAR(500) NULL,
  `IsHistory` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `TimestampGMT` DATETIME NOT NULL ,
  `CreatedGMT` DATETIME NOT NULL ,
  `ArchivedGMT` DATETIME NULL ,
  `VERSION` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) ,
  INDEX `idx_postelemetry` (`PointOfSaleId` ASC, `TelemetryTypeId` ASC, `IsHistory` DESC) ,
  CONSTRAINT `fk_postelemetry_telemetrytype`
    FOREIGN KEY (`TelemetryTypeId` )
    REFERENCES `TelemetryType` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_postelemetry_pointofsale`
    FOREIGN KEY (`PointOfSaleId` )
    REFERENCES `PointOfSale` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GatewayProcessor`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `GatewayProcessor` ;

CREATE  TABLE IF NOT EXISTS `GatewayProcessor` (
  `Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProcessorId` MEDIUMINT UNSIGNED NOT NULL ,
  `Name` VARCHAR(60) NOT NULL,
  `Value` VARCHAR(100) NOT NULL,
  `CreatedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) , 
  UNIQUE INDEX `idx_gateway_processor_multi_uq` (`ProcessorId` ASC, `Name` ASC, `Value` ASC) ,
  INDEX `idx_gateway_processor_processor` (`ProcessorId`) ,
  CONSTRAINT `fk_gateway_processor_processor`
    FOREIGN KEY (`ProcessorId` )
    REFERENCES `Processor` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB ;


-- -----------------------------------------------------
-- Table `ETLRevenueHourLimit`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueHourLimit` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueHourLimit` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `StartingId` BIGINT UNSIGNED  NOT NULL ,
  `EndingId` BIGINT UNSIGNED  NOT NULL ,
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLRevenueDayLimit`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueDayLimit` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueDayLimit` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `StartingId` BIGINT UNSIGNED  NOT NULL ,
  `EndingId` BIGINT UNSIGNED  NOT NULL ,
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLRevenueMonthLimit`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueMonthLimit` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueMonthLimit` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `StartingId` BIGINT UNSIGNED  NOT NULL ,
  `EndingId` BIGINT UNSIGNED  NOT NULL ,
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLRevenueHourStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueHourStatus` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueHourStatus` (
  `PPPDetailHourId` BIGINT UNSIGNED NOT NULL  ,  
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`PPPDetailHourId`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLRevenueDayStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueDayStatus` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueDayStatus` (
  `PPPDetailDayId` BIGINT UNSIGNED NOT NULL  ,  
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`PPPDetailDayId`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLRevenueMonthStatus`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLRevenueMonthStatus` ;

CREATE  TABLE IF NOT EXISTS `ETLRevenueMonthStatus` (
  `PPPDetailMonthId` BIGINT UNSIGNED NOT NULL  ,  
  `IsProcessed` TINYINT UNSIGNED  NOT NULL DEFAULT 0,
  `ProcessedGMT` Datetime  NULL,
   PRIMARY KEY (`PPPDetailMonthId`)
   )
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `ETLPPPRevenueDayTap_Log`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLPPPRevenueDayTap_Log` ;

CREATE  TABLE IF NOT EXISTS `ETLPPPRevenueDayTap_Log` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED,
  `LocationId` MEDIUMINT UNSIGNED,
  `PointOfSaleId` MEDIUMINT UNSIGNED,
  `PurchaseId`	BIGINT UNSIGNED,
  `CashPaidAmount` MEDIUMINT UNSIGNED,
  `CardPaidAmount` MEDIUMINT UNSIGNED,
  `PurchaseGMT` DATETIME ,
  `Timezone` CHAR(25),
  `UnifiedRateId` MEDIUMINT UNSIGNED,
  `PaystationSettingId` MEDIUMINT UNSIGNED,
  `TransactionTypeId` TINYINT UNSIGNED,
  `IsCoupon` CHAR(1),  
  `TimeIdLocal` MEDIUMINT UNSIGNED,
  `PPPTotalDayId` BIGINT UNSIGNED,
  `PPPDetailDayId` BIGINT UNSIGNED,
  `ExistingTotalAmount` INT UNSIGNED,
  `ExistingTotalCount` MEDIUMINT UNSIGNED,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  



-- -----------------------------------------------------
-- Table `ETLPPPRevenueMonthTap_Log`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLPPPRevenueMonthTap_Log` ;

CREATE  TABLE IF NOT EXISTS `ETLPPPRevenueMonthTap_Log` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED,
  `LocationId` MEDIUMINT UNSIGNED,
  `PointOfSaleId` MEDIUMINT UNSIGNED,
  `PurchaseId`	BIGINT UNSIGNED,
  `CashPaidAmount` MEDIUMINT UNSIGNED,
  `CardPaidAmount` MEDIUMINT UNSIGNED,
  `PurchaseGMT` DATETIME ,
  `Timezone` CHAR(25),
  `UnifiedRateId` MEDIUMINT UNSIGNED,
  `PaystationSettingId` MEDIUMINT UNSIGNED,
  `TransactionTypeId` TINYINT UNSIGNED,
  `IsCoupon` CHAR(1),  
  `TimeIdLocal` MEDIUMINT UNSIGNED,
  `PPPTotalMonthId` BIGINT UNSIGNED,
  `PPPDetailMonthId` BIGINT UNSIGNED,
  `ExistingTotalAmount` INT UNSIGNED,
  `ExistingTotalCount` MEDIUMINT UNSIGNED,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  
  


-- -----------------------------------------------------
-- Table `VirtualPSTransactionDateLimit`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `VirtualPSTransactionDateLimit` ;

CREATE  TABLE IF NOT EXISTS `VirtualPSTransactionDateLimit` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,  
  `FromDate` Datetime NOT NULL ,
  `ToDate` Datetime  NOT NULL,
  `IsProcessedForPurchase` TINYINT  UNSIGNED NOT NULL DEFAULT 0,
  `IsProcessedForWidgetDay` TINYINT  UNSIGNED NOT NULL DEFAULT 0,
  `IsProcessedForWidgetMonth` TINYINT  UNSIGNED NOT NULL DEFAULT 0,
  `IsProcessedForPurchaseGMT` Datetime NULL ,
  `IsProcessedForWidgetDayGMT` Datetime NULL ,
  `IsProcessedForWidgetMonthGMT` Datetime NULL ,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  
  


-- -----------------------------------------------------
-- Table `ETLPPPRevenueDayCCExternal_Log`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLPPPRevenueDayCCExternal_Log` ;

CREATE  TABLE IF NOT EXISTS `ETLPPPRevenueDayCCExternal_Log` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED,
  `LocationId` MEDIUMINT UNSIGNED,
  `PointOfSaleId` MEDIUMINT UNSIGNED,
  `PurchaseId`	BIGINT UNSIGNED,
  `CashPaidAmount` MEDIUMINT UNSIGNED,
  `CardPaidAmount` MEDIUMINT UNSIGNED,
  `PurchaseGMT` DATETIME ,
  `Timezone` CHAR(25),
  `UnifiedRateId` MEDIUMINT UNSIGNED,
  `PaystationSettingId` MEDIUMINT UNSIGNED,
  `TransactionTypeId` TINYINT UNSIGNED,
  `IsCoupon` CHAR(1),  
  `TimeIdLocal` MEDIUMINT UNSIGNED,
  `PPPTotalDayId` BIGINT UNSIGNED,
  `PPPDetailDayId` BIGINT UNSIGNED,
  `ExistingTotalAmount` INT UNSIGNED,
  `ExistingTotalCount` MEDIUMINT UNSIGNED,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  



-- -----------------------------------------------------
-- Table `ETLPPPRevenueMonthCCExternal_Log`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `ETLPPPRevenueMonthCCExternal_Log` ;

CREATE  TABLE IF NOT EXISTS `ETLPPPRevenueMonthCCExternal_Log` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `CustomerId` MEDIUMINT UNSIGNED,
  `LocationId` MEDIUMINT UNSIGNED,
  `PointOfSaleId` MEDIUMINT UNSIGNED,
  `PurchaseId`	BIGINT UNSIGNED,
  `CashPaidAmount` MEDIUMINT UNSIGNED,
  `CardPaidAmount` MEDIUMINT UNSIGNED,
  `PurchaseGMT` DATETIME ,
  `Timezone` CHAR(25),
  `UnifiedRateId` MEDIUMINT UNSIGNED,
  `PaystationSettingId` MEDIUMINT UNSIGNED,
  `TransactionTypeId` TINYINT UNSIGNED,
  `IsCoupon` CHAR(1),  
  `TimeIdLocal` MEDIUMINT UNSIGNED,
  `PPPTotalMonthId` BIGINT UNSIGNED,
  `PPPDetailMonthId` BIGINT UNSIGNED,
  `ExistingTotalAmount` INT UNSIGNED,
  `ExistingTotalCount` MEDIUMINT UNSIGNED,
   PRIMARY KEY (`Id`)
   )
ENGINE = InnoDB;  
  
-- Mapping Tables from MySQLCompare



CREATE TABLE `ArchiveDateLookup` (
  `Id` int(11) NOT NULL auto_increment,
  `BeginDt` datetime NOT NULL,
  `EndDt` datetime NOT NULL,
  `Status` tinyint(4) NULL DEFAULT 0,
  `ArchiveStartTime` datetime NULL,
  `ArchiveEndTime` datetime NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `BackupPreAuth` (
  `Id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `ResponseCode` varchar(50) NULL DEFAULT '',
  `ProcessorTransactionId` varchar(50) NULL DEFAULT '',
  `AuthorizationNumber` varchar(30) NULL DEFAULT '',
  `ReferenceNumber` varchar(30) NULL DEFAULT '',
  `MerchantAccountId` mediumint(8) unsigned NOT NULL,
  `Amount` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `Last4DigitsOfCardNumber` smallint(5) unsigned NOT NULL DEFAULT 0,
  `CardType` varchar(20) NOT NULL DEFAULT '',
  `IsApproved` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `PreAuthDate` datetime NOT NULL,
  `CardData` varchar(100) NULL,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `ReferenceId` varchar(60) NULL,
  `Expired` tinyint(4) NULL DEFAULT 0,
  `CardHash` varchar(30) NOT NULL DEFAULT '',
  `ExtraData` varchar(1336) NULL DEFAULT '',
  `PsRefId` varchar(10) NULL,
  `CardExpiry` smallint(5) unsigned NOT NULL DEFAULT 0,
  `IsRFID` tinyint(1) unsigned NOT NULL DEFAULT 0
) ENGINE=InnoDB;

CREATE TABLE `CustomerAlertTypeMapping` (
  `EMS6Id` int(10) NOT NULL,
  `EMS7Id` int(10) NOT NULL,
  `ModifiedDate` datetime NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `CustomerMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS7CustomerId` mediumint(8) unsigned NOT NULL,
  `EMS6CustomerId` int(11) NOT NULL,
  `EMS7CustomerName` varchar(40) NOT NULL,
  `EMS6CustomerName` varchar(40) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_CustomerMapping_EMS6CustomerId`(`EMS6CustomerId`)
) ENGINE=InnoDB;

CREATE TABLE `DiscardedRecords` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `MultiKeyId` varchar(255) NULL DEFAULT '0',
  `EMS6Id` bigint(20) NULL DEFAULT 0,
  `TableName` varchar(25) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `EMS6AlertType` (
  `Id` int(3) unsigned NOT NULL,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `IsUserDefined` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ExtensibleRateMapping` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `EMS6RateId` bigint(20) unsigned NULL,
  `EMS7RateId` mediumint(8) unsigned NULL,
  `EMS6RateTypeId` varchar(30) NOT NULL DEFAULT '',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `FlexDataWSCallStatus_OldLog` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `CustomerId` mediumint(8) unsigned NOT NULL,
  `FlexETLDataTypeId` tinyint(3) unsigned NOT NULL,
  `RequestedFromDateGMT` datetime NULL,
  `RequestedToDateGMT` datetime NULL,
  `WSCallBeginGMT` datetime NULL,
  `WSCallEndGMT` datetime NULL,
  `LastUniqueIdentifier` int(10) unsigned NULL,
  `Status` tinyint(4) NULL
) ENGINE=InnoDB;

CREATE TABLE `MerchantAccountMapping` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `EMS6MerchantAccountId` int(11) unsigned NULL,
  `EMS7MerchantAccountId` mediumint(8) unsigned NULL,
  `ProcessorName` varchar(30) NOT NULL DEFAULT '',
  `EMS6CustomerId` int(11) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_MerchantAccountMapping_EMS6MerchantAccountId`(`EMS6MerchantAccountId`)
) ENGINE=InnoDB;

CREATE TABLE `ParkingPermissionMapping` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `EMS6ParkingPermissionId` int(10) unsigned NULL,
  `EMS7ParkingPemissionId` int(10) unsigned NULL,
  `Name` varchar(16) NOT NULL DEFAULT '',
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `PaystationGroupRouteMapping` (
  `PaystationGroupId` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` mediumint(8) NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaystationGroupId`)
) ENGINE=InnoDB;

CREATE TABLE `PaystationMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS7PaystationId` mediumint(8) unsigned NOT NULL,
  `EMS6PaystationId` mediumint(8) unsigned NULL,
  `EMS6CustomerId` mediumint(8) unsigned NULL,
  `SerialNumber` varchar(20) NOT NULL,
  `Name` varchar(40) NOT NULL DEFAULT '',
  `Version` int(10) unsigned NULL,
  `ProvisionDate` datetime NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RegionId` mediumint(8) unsigned NULL,
  `LotSettingId` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_PaystationMapping_EMS6PaystationId`(`EMS6PaystationId`)
) ENGINE=InnoDB;

CREATE TABLE `PreAuthHoldingMapping` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `EMS6MerchantAccountId` int(11) unsigned NULL,
  `EMS7MerchantAccountId` int(10) unsigned NULL,
  `AuthorizationNumber` varchar(30) NOT NULL DEFAULT '',
  `ProcessorTransactionId` varchar(30) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `PreAuthMapping` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `EMS6MerchantAccountId` int(11) unsigned NULL,
  `EMS7MerchantAccountId` int(10) unsigned NULL,
  `AuthorizationNumber` varchar(30) NOT NULL DEFAULT '',
  `ProcessorTransactionId` varchar(30) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_PreAuthMapping_EMS6MerchantAccountId`(`EMS6MerchantAccountId`),
  KEY `ind_PreAuthMapping_EMS7MerchantAccountId`(`EMS7MerchantAccountId`)
) ENGINE=InnoDB;

CREATE TABLE `RegionLocationMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS7LocationId` mediumint(8) unsigned NOT NULL,
  `EMS6RegionId` mediumint(8) unsigned NULL,
  `Name` varchar(255) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ind_RegionLocationMapping_EMS6RegionId`(`EMS6RegionId`)
) ENGINE=InnoDB;

CREATE TABLE `RoutePOSMapping` (
  `EMS7Id` mediumint(8) NULL,
  `PaystationGroupId` mediumint(8) NOT NULL,
  `PaystationId` mediumint(8) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaystationGroupId`,`PaystationId`)
) ENGINE=InnoDB;

CREATE TABLE `ServiceAgreementMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS6ServiceAgreementId` int(11) NOT NULL,
  `EMS7ServiceAgreementId` mediumint(8) NOT NULL,
  `EMS6UploadedGMT` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `SettingsFileContentMapping` (
  `EMS6Id` int(11) NOT NULL,
  `EMS7Id` mediumint(8) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `SettingsFileMapping` (
  `EMS6Id` int(11) NOT NULL,
  `EMS7Id` mediumint(8) unsigned NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `ThirdPartyServiceAccountMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS6ThirdPartyServiceAccountId` int(11) NOT NULL,
  `EMS7ThirdPartyServiceAccountId` mediumint(8) NOT NULL,
  `EMS6UserName` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `UserAccountMapping` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `EMS6UseraccountId` int(11) NOT NULL,
  `EMS7UserAccountId` mediumint(8) NOT NULL,
  `EMS6Name` varchar(40) NULL,
  `EMS6CustomerId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `teste_sandeep` (
  `1` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB;

CREATE TABLE `tmp_ServiceAgreement` (
  `Id` smallint(5) unsigned NOT NULL DEFAULT 0,
  `Content` mediumblob NOT NULL,
  `ContentVersion` int(10) unsigned NOT NULL,
  `UploadedGMT` datetime NOT NULL,
  `VERSION` int(10) unsigned NOT NULL,
  `LastModifiedGMT` datetime NOT NULL,
  `LastModifiedByUserId` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `ArchivePOSAlert` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `CustomerAlertTypeId` mediumint(8) unsigned NOT NULL,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `EventDeviceTypeId` tinyint(3) unsigned NOT NULL,
  `EventStatusTypeId` tinyint(3) unsigned NOT NULL,
  `EventActionTypeId` tinyint(3) unsigned NOT NULL,
  `EventSeverityTypeId` tinyint(3) unsigned NOT NULL,
  `AlertGMT` datetime NOT NULL,
  `AlertInfo` varchar(50) NULL,
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsSentEmail` tinyint(1) unsigned NOT NULL,
  `SentEmailGMT` datetime NULL,
  `ClearedGMT` datetime NULL,
  `ClearedByUserId` mediumint(8) unsigned NULL,
  `CreatedGMT` datetime NULL
) ENGINE=InnoDB;

CREATE TABLE `ArchivePOSAlertMay5` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `CustomerAlertTypeId` mediumint(8) unsigned NOT NULL,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `EventDeviceTypeId` tinyint(3) unsigned NOT NULL,
  `EventStatusTypeId` tinyint(3) unsigned NOT NULL,
  `EventActionTypeId` tinyint(3) unsigned NOT NULL,
  `EventSeverityTypeId` tinyint(3) unsigned NOT NULL,
  `AlertGMT` datetime NOT NULL,
  `AlertInfo` varchar(50) NULL,
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsSentEmail` tinyint(1) unsigned NOT NULL,
  `SentEmailGMT` datetime NULL,
  `ClearedGMT` datetime NULL,
  `ClearedByUserId` mediumint(8) unsigned NULL,
  `CreatedGMT` datetime NULL
) ENGINE=InnoDB;

CREATE TABLE `AuditMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6PaystationId` int(11) NULL,
  `EMS6CollectionTypeId` tinyint(4) NULL,
  `EndDate` datetime NULL,
  `StartDate` datetime NULL,
  `EMS7POSCollectionId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `AuditPOSCollectionMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` int(11) NULL,
  `CollectionTypeId` tinyint(4) NULL,
  `StartDate` datetime NULL,
  `EndDate` datetime NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `BillingReportDCOverages` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `DateRangeBegin` date NOT NULL,
  `DateRangeEnd` date NOT NULL,
  `RunBegin` datetime NOT NULL,
  `RunEnd` datetime NULL,
  `IsPosted` tinyint(3) unsigned NOT NULL,
  `IsError` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `BillingReportPaystationDCOverages` (
  `BillingReportId` int(10) unsigned NOT NULL,
  `CustomerId` mediumint(8) unsigned NOT NULL,
  `CustomerName` varchar(25) NOT NULL,
  `IsTrial` tinyint(3) unsigned NOT NULL,
  `TrialExpiryGMT` datetime NULL,
  `PaystationId` mediumint(8) unsigned NOT NULL,
  `PointOfSaleId` mediumint(8) unsigned NOT NULL,
  `POSName` varchar(30) NOT NULL,
  `POSLocationId` mediumint(8) unsigned NOT NULL,
  `ProvisionedGMT` datetime NOT NULL,
  `POSLastModifiedGMT` datetime NULL,
  `SerialNumber` varchar(20) NOT NULL,
  `PaystationSettingName` varchar(20) NULL,
  `LocationName` varchar(25) NOT NULL,
  `LastHeartbeatGMT` datetime NULL,
  `BillableChangeGMT` datetime NULL,
  `ActivationChangeGMT` datetime NULL,
  `DigitalCollectChangeGMT` datetime NULL,
  `IsActivated` tinyint(3) unsigned NOT NULL,
  `IsLocked` tinyint(3) unsigned NOT NULL,
  `IsDigitalConnect` tinyint(3) unsigned NOT NULL,
  `IsBillableMonthlyOnActivation` tinyint(3) unsigned NOT NULL,
  `IsBillableMonthlyForEms` tinyint(3) unsigned NOT NULL,
  `IsDecommissioned` tinyint(3) unsigned NOT NULL,
  `IsDeleted` tinyint(3) unsigned NOT NULL,
  `APN` varchar(40) NULL,
  `APNTransactions` int(11) NOT NULL DEFAULT '0',
  `ServiceStandardReports` tinyint(3) unsigned NOT NULL,
  `ServiceAlerts` tinyint(3) unsigned NOT NULL,
  `ServiceRealTimeCC` tinyint(3) unsigned NOT NULL,
  `ServiceBatchCC` tinyint(3) unsigned NOT NULL,
  `ServiceCampusCard` tinyint(3) unsigned NOT NULL,
  `ServiceCoupon` tinyint(3) unsigned NOT NULL,
  `ServicePasscard` tinyint(3) unsigned NOT NULL,
  `ServiceSmartCard` tinyint(3) unsigned NOT NULL,
  `ServicePayByPhone` tinyint(3) unsigned NOT NULL,
  `ServiceDigitalAPIRead` tinyint(3) unsigned NOT NULL,
  `ServiceDigitalAPIWrite` tinyint(3) unsigned NOT NULL,
  `ServiceExtendByCell` tinyint(3) unsigned NOT NULL,
  `HasChanged` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `TransactionCount` int(10) unsigned NULL,
  `Type16TxnCount` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `Type17TxnCount` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`BillingReportId`,`PointOfSaleId`),
  FOREIGN KEY (`BillingReportId`) REFERENCES `BillingReportDCOverages` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `CCFailLogMapping` (
  `EMS6Id` int(11) NULL,
  `EMS7Id` int(11) NULL,
  `ModifiedDate` datetime NULL
) ENGINE=InnoDB;

CREATE TABLE `CardRetryTransactionMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` mediumint(9) NULL,
  `PurchasedDate` datetime NULL,
  `TicketNumber` int(11) NULL,
  `EMS7CardRetryTransactionId` int(11) NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_CardRetryTransactionMapping`(`PaystationId`, `PurchasedDate`, `TicketNumber`)
) ENGINE=InnoDB;

CREATE TABLE `CleanseProcessorTransaction` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `idProcessorTransaction` bigint(20) unsigned NOT NULL,
  `PurchaseId` bigint(20) unsigned NULL,
  `MerchantAccountId` mediumint(8) unsigned NULL,
  `PointOfSaleId` mediumint(8) unsigned NULL,
  `PreAuthId` bigint(20) unsigned NULL,
  `ProcessorTransactionTypeId` tinyint(3) unsigned NULL,
  `LastModifiedTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `CouponMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6CouponId` varchar(20) NULL,
  `EMS7CouponId` int(11) NULL,
  `NumUses` varchar(16) NULL,
  `EMS6CustomerId` mediumint(9) NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_CouponMapping_EMS6CouponId`(`EMS6CouponId`),
  KEY `ind_CouponMapping_EMS6CustomerId`(`EMS6CustomerId`)
) ENGINE=InnoDB;

CREATE TABLE `CryptoKeyMapping` (
  `Type` int(11) NOT NULL DEFAULT '0',
  `KeyIndex` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` int(11) NULL,
  `DateAdded` datetime NULL,
  PRIMARY KEY (`Type`,`KeyIndex`)
) ENGINE=InnoDB;

CREATE TABLE `CustomerCardTypeMapping` (
  `EMS6Id` int(11) NOT NULL DEFAULT '0',
  `EMS7Id` mediumint(9) NULL,
  `ModifiedDate` datetime NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `CustomerWebServiceCalMapping` (
  `EMS6Id` varchar(45) NOT NULL DEFAULT '',
  `EMS7Id` varchar(45) NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `CustomerWsTokenMapping` (
  `EMS6Id` int(10) unsigned NOT NULL DEFAULT '0',
  `EMS7Id` int(10) unsigned NULL,
  `ModifiedDate` datetime NULL,
  PRIMARY KEY (`EMS6Id`)
) ENGINE=InnoDB;

CREATE TABLE `EMS7ExtensibleRateMapping` (
  `Id` int(11) NOT NULL,
  `EMS6RateId` bigint(20) NULL,
  `EMS7RateId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `EMSParkingPermissionMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6ParkingPermission` int(11) NULL,
  `EMS7ParkingPermission` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `EMSRateMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6RateId` int(11) NULL,
  `EMS7RateId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `EMSRateToUnifiedRateMapping` (
  `EMS6RateId` bigint(20) unsigned NULL,
  `EMS7RateId` int(10) unsigned NULL,
  `ModifiedDate` datetime NULL
) ENGINE=InnoDB;

CREATE TABLE `EventLogNewMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` mediumint(9) NULL,
  `DeviceId` tinyint(4) NULL,
  `TypeId` int(11) NULL,
  `ActionId` int(11) NULL,
  `DateField` datetime NULL,
  `POSAlertId` varchar(45) NULL,
  PRIMARY KEY (`Id`),
  KEY `ind_EventLogNewMapping`(`PaystationId`, `DeviceId`, `TypeId`, `ActionId`, `DateField`)
) ENGINE=InnoDB;

CREATE TABLE `ExtensiblePermitMapping` (
  `EMS6MobileNumber` varchar(45) NOT NULL DEFAULT '',
  `EMS7Id` varchar(45) NULL,
  PRIMARY KEY (`EMS6MobileNumber`)
) ENGINE=InnoDB;

CREATE TABLE `MigrationWeek` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `WeekStartDate` datetime NOT NULL,
  `WeekEndDate` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ModemSettingMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6Id` int(11) NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `POSActivationDateUpdate` (
  `Id` mediumint(8) unsigned NOT NULL auto_increment,
  `PointOfSaleID` mediumint(8) unsigned NULL,
  `SerialNumber` varchar(20) NULL,
  `EMS6ProvisionDate` datetime NULL,
  `EMS6LockState` varchar(20) NULL,
  `OriginalPOSProvisionedGMT` datetime NULL,
  `OriginalPOSStatusIsProvisionedGMT` datetime NULL,
  `OriginalPOSStatusIsActivatedGMT` datetime NULL,
  `OriginalPOSStatusIsActivated` tinyint(3) unsigned NULL,
  `ChangedPOSProvisionedGMT` datetime NULL,
  `ChangedPOSStatusProvisionedGMT` datetime NULL,
  `ChangedPOSStatusActivationGMT` datetime NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `SerialNumber_uq`(`SerialNumber`)
) ENGINE=InnoDB;

CREATE TABLE `ProcessorPropertiesMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `Processor` varchar(45) NULL,
  `Name` varchar(45) NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ProcessorTransactionMapping` (
  `PaystationId` int(11) NOT NULL,
  `PurchasedDate` datetime NOT NULL,
  `TicketNumber` int(11) NOT NULL,
  `Approved` tinyint(4) NOT NULL,
  `TypeId` tinyint(4) NOT NULL,
  `ProcessingDate` datetime NOT NULL,
  `EMS7ProcessorTransactionId` int(11) NULL,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`,`Approved`,`TypeId`,`ProcessingDate`)
) ENGINE=InnoDB;

CREATE TABLE `PurchaseTaxMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6Id` int(11) NULL,
  `EMS7Id` mediumint(9) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RateMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `CustomerId` int(11) NULL,
  `Name` varchar(45) NULL,
  `EMS7UnifiedRateId` int(11) NULL,
  `PaystationId` int(11) NULL,
  `TicketNumber` int(11) NULL,
  `PurchasedDate` datetime NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RateToUnifiedRateMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` int(11) NULL,
  `PurchasedDate` datetime NULL,
  `TicketNumber` int(11) NULL,
  `EMS7UnifiedRateId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RawSensorDataMapping` (
  `Id` int(11) NOT NULL,
  `ServerName` varchar(20) NULL,
  `PaystationCommAddress` varchar(20) NULL,
  `Type` varchar(45) NULL,
  `Action` varchar(45) NULL,
  `Information` varchar(45) NULL,
  `DateField` datetime NULL,
  `EMS7RawSensorDataId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RawSensorDataServerMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6ServerName` varchar(20) NULL,
  `EMS7ServerName` varchar(20) NULL,
  UNIQUE KEY `idx_RawSensorDataServerMapping_uq`(`EMS6ServerName`),
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RegionPaystationLogMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6RegionPaystationId` mediumint(9) NULL,
  `EMS7LocationPOSLogId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ReplenishMapping` (
  `PaystationId` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `Date` datetime NOT NULL ,
  `Number` int(10) unsigned NOT NULL DEFAULT '0',
  `EMS7Id` int(10) unsigned NULL,
  `ModifiedDate` datetime NULL,
  PRIMARY KEY (`PaystationId`,`Date`,`Number`)
) ENGINE=InnoDB;

CREATE TABLE `RestAccountMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6AccountName` varchar(45) NULL,
  `EMS7RestAccountId` mediumint(9) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RestLogMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `RESTAccountName` varchar(45) NULL,
  `EndpointName` varchar(45) NULL,
  `LoggingDate` varchar(45) NULL,
  `IsError` varchar(45) NULL,
  `CustomerId` varchar(45) NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RestLogTotalCallMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `RESTAccountName` varchar(45) NULL,
  `LoggingDate` datetime NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `RestSessionTokenMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `EMS6AccountName` varchar(45) NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ReversalArchiveMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `MerchantAccountId` int(11) NULL,
  `OriginalReferenceNumber` varchar(30) NULL,
  `OriginalTime` varchar(30) NULL,
  `EMS7ReversalArchiveId` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ReversalMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `MerchantAccountId` mediumint(9) NULL,
  `OriginalReferenceNumber` varchar(30) NULL,
  `OriginalTime` varchar(30) NULL,
  `EMS7Id` varchar(45) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `SMSAlertMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `MobileNumber` varchar(45) NULL,
  `EMS7Id` varchar(45) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `SMSFailedResponseMapping` (
  `PaystationId` int(11) NOT NULL DEFAULT '0',
  `PurchasedDate` datetime NOT NULL ,
  `TicketNumber` int(11) NOT NULL DEFAULT '0',
  `SMSMessageTypeId` int(11) NOT NULL DEFAULT '0',
  `EMS7SMSFailedResponseId` int(11) NULL,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`,`SMSMessageTypeId`)
) ENGINE=InnoDB;

CREATE TABLE `SMSTransactionLogMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` varchar(45) NULL,
  `PurchasedDate` varchar(45) NULL,
  `TicketNumber` varchar(45) NULL,
  `SMSMessageTypeId` varchar(45) NULL,
  `EMS7TransactionLogId` varchar(45) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `SensorDataLimit` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `LowerLimit` bigint(20) unsigned NOT NULL,
  `UpperLimt` bigint(20) unsigned NOT NULL,
  `IsProcessed` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `ServiceStateMapping` (
  `id` int(11) NOT NULL auto_increment,
  `PaystationId` int(11) NULL,
  `EMS7POSServiceStateId` int(11) NULL,
  `MachineNumber` varchar(20) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `Transaction2PushMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` int(11) NULL,
  `PurchasedDate` datetime NULL,
  `TicketNumber` int(11) NULL,
  `ThirdPartyServiceAccountId` int(11) NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `TransactionMapping` (
  `Id` int(11) NOT NULL,
  `PaystationId` int(11) NULL,
  `PurchasedDate` datetime NULL,
  `TicketNumber` int(11) NULL,
  `EMS7PurchaseId` varchar(45) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `TransactionPushMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `PaystationId` varchar(45) NULL,
  `PurchasedDate` varchar(45) NULL,
  `TicketNumber` varchar(45) NULL,
  `EMS7Id` varchar(45) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

CREATE TABLE `WsCallLogMapping` (
  `Id` int(11) NOT NULL auto_increment,
  `Token` varchar(45) NULL,
  `CallDate` datetime NULL,
  `EMS7Id` int(11) NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

-- End Mapping Tables

-- -----------------------------------------------------
-- Table `ElavonCCData`
-- -----------------------------------------------------


DROP TABLE IF EXISTS `ElavonCCData` ;

CREATE  TABLE IF NOT EXISTS `ElavonCCData` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `IdElavonTransactionDetail` BIGINT UNSIGNED NOT NULL ,
  `CardData` varchar(100)  NULL ,
  PRIMARY KEY (`Id`) ,  
  INDEX idx_ElavonCCData_ETDId ( IdElavonTransactionDetail ) 
  )
ENGINE = InnoDB;

  
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


