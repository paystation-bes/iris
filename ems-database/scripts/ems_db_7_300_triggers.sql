-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_CustomerAgreement_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_CustomerAgreement_insert;

delimiter //
CREATE TRIGGER tr_CustomerAgreement_insert AFTER INSERT ON CustomerAgreement
FOR EACH ROW BEGIN
    
    INSERT CustomerAgreement_Log (Id, ChangeTypeId, CustomerId, ServiceAgreementId, Name, Title, Organization, AgreedGMT, IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (NEW.Id, 1, NEW.CustomerId, NEW.ServiceAgreementId, NEW.Name, NEW.Title, NEW.Organization, NEW.AgreedGMT, NEW.IsOverride, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);
   
END //
delimiter ;


-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_CustomerAgreement_update
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_CustomerAgreement_update;

delimiter //
CREATE TRIGGER tr_CustomerAgreement_update AFTER UPDATE ON CustomerAgreement	
FOR EACH ROW BEGIN
    
    INSERT CustomerAgreement_Log (Id, ChangeTypeId, CustomerId, ServiceAgreementId, Name, Title, Organization, AgreedGMT, IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (NEW.Id, 2, NEW.CustomerId, NEW.ServiceAgreementId, NEW.Name, NEW.Title, NEW.Organization, NEW.AgreedGMT, NEW.IsOverride, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);

END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_CustomerAgreement_delete
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_CustomerAgreement_delete;

delimiter //
CREATE TRIGGER tr_CustomerAgreement_delete AFTER DELETE ON CustomerAgreement
FOR EACH ROW BEGIN
    
    INSERT CustomerAgreement_Log (Id, ChangeTypeId, CustomerId, ServiceAgreementId, Name, Title, Organization, AgreedGMT, IsOverride, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (OLD.Id, 3, OLD.CustomerId, OLD.ServiceAgreementId, OLD.Name, OLD.Title, OLD.Organization, OLD.AgreedGMT, OLD.IsOverride, OLD.VERSION, UTC_TIMESTAMP(), OLD.LastModifiedByUserId);

END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_CustomerAgreement_Log_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_CustomerAgreement_Log_insert;

delimiter //
CREATE TRIGGER tr_CustomerAgreement_Log_insert AFTER INSERT ON CustomerAgreement_Log
FOR EACH ROW BEGIN
    
    INSERT ActivityLog (UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId, ActivityGMT) 
    VALUES (NEW.LastModifiedByUserId, 1600+NEW.ChangeTypeId, 2, NEW.Id, NEW.LogId, NEW.LastModifiedGMT);
 
END //
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_Notification_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_Notification_insert;

delimiter //
CREATE TRIGGER tr_Notification_insert AFTER INSERT ON Notification
FOR EACH ROW BEGIN
    
    INSERT Notification_Log (Id, ChangeTypeId, Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId) 
    VALUES (NEW.Id, 1, NEW.Title, NEW.Message, NEW.MessageURL, NEW.BeginGMT, NEW.EndGMT, NEW.IsDeleted, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);

END //
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_Notification_update
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_Notification_update;

delimiter //
CREATE TRIGGER tr_Notification_update AFTER UPDATE ON Notification	
FOR EACH ROW BEGIN
    
    INSERT Notification_Log (Id, ChangeTypeId, Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId) 
    VALUES (NEW.Id, 2, NEW.Title, NEW.Message, NEW.MessageURL, NEW.BeginGMT, NEW.EndGMT, NEW.IsDeleted, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);

END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_Notification_delete
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_Notification_delete;

delimiter //
CREATE TRIGGER tr_Notification_delete AFTER DELETE ON Notification
FOR EACH ROW BEGIN
    
    INSERT Notification_Log (Id, ChangeTypeId, Title, Message, MessageURL, BeginGMT, EndGMT, IsDeleted, VERSION, LastModifiedGMT, LastModifiedByUserId) 
    VALUES (OLD.Id, 3, OLD.Title, OLD.Message, OLD.MessageURL, OLD.BeginGMT, OLD.EndGMT, OLD.IsDeleted, OLD.VERSION, UTC_TIMESTAMP(), OLD.LastModifiedByUserId);

END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_Notification_Log_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_Notification_Log_insert;

delimiter //
CREATE TRIGGER tr_Notification_Log_insert AFTER INSERT ON Notification_Log
FOR EACH ROW BEGIN
    
    INSERT ActivityLog (UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId, ActivityGMT) 
    VALUES (NEW.LastModifiedByUserId, 1406-1+NEW.ChangeTypeId, 10, NEW.Id, NEW.LogId, NEW.LastModifiedGMT);
 
END //
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_UserAccount_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_UserAccount_insert;

delimiter //
CREATE TRIGGER tr_UserAccount_insert AFTER INSERT ON UserAccount
FOR EACH ROW BEGIN
    
    INSERT UserAccount_Log (Id, ChangeTypeId, CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (NEW.Id, 1, NEW.CustomerId, NEW.UserStatusTypeId, NEW.CustomerEmailId, NEW.UserName, NEW.FirstName, NEW.LastName, NEW.Password, NEW.PasswordSalt, NEW.IsPasswordTemporary, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);
   
END //
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_UserAccount_update
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_UserAccount_update;

delimiter //
CREATE TRIGGER tr_UserAccount_update AFTER UPDATE ON UserAccount	
FOR EACH ROW BEGIN
    
    INSERT UserAccount_Log (Id, ChangeTypeId, CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (NEW.Id, 2, NEW.CustomerId, NEW.UserStatusTypeId, NEW.CustomerEmailId, NEW.UserName, NEW.FirstName, NEW.LastName, NEW.Password, NEW.PasswordSalt, NEW.IsPasswordTemporary, NEW.VERSION, UTC_TIMESTAMP(), NEW.LastModifiedByUserId);
 
END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_UserAccount_delete
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_UserAccount_delete;

delimiter //
CREATE TRIGGER tr_UserAccount_delete AFTER DELETE ON UserAccount
FOR EACH ROW BEGIN
    
    INSERT UserAccount_Log (Id, ChangeTypeId, CustomerId, UserStatusTypeId, CustomerEmailId, UserName, FirstName, LastName, Password, PasswordSalt, IsPasswordTemporary, VERSION, LastModifiedGMT, LastModifiedByUserId)
    VALUES (OLD.Id, 3, OLD.CustomerId, OLD.UserStatusTypeId, OLD.CustomerEmailId, OLD.UserName, OLD.FirstName, OLD.LastName, OLD.Password, OLD.PasswordSalt, OLD.IsPasswordTemporary, OLD.VERSION, UTC_TIMESTAMP(), OLD.LastModifiedByUserId);

END//
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_UserAccount_Log_insert
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_UserAccount_Log_insert;

delimiter //
CREATE TRIGGER tr_UserAccount_Log_insert AFTER INSERT ON UserAccount_Log
FOR EACH ROW BEGIN
    
    INSERT ActivityLog (UserAccountId, ActivityTypeId, EntityTypeId, EntityId, EntityLogId, ActivityGMT) 
    VALUES (NEW.LastModifiedByUserId, 1000+NEW.ChangeTypeId, 5, NEW.Id, NEW.LogId, NEW.LastModifiedGMT);
 
END //
delimiter ;

-- New Triggers


-- 1) TRIGGER on Purchase
-- Inserts data into PurchaseCollection and StagingPurchase
DROP Trigger if Exists Trg_InsertPurchase ;
DROP TRIGGER IF EXISTS tr_InsertPurchase ;

DELIMITER $$
CREATE TRIGGER tr_InsertPurchase
AFTER INSERT ON Purchase
FOR EACH ROW
BEGIN

-- commented on July 8
-- This portion of code is handled in Business Layer
-- INSERT INTO PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) 
-- values(New.Id,New.PointOfSaleId,New.PurchaseGMT,New.CashPaidAmount,New.CoinPaidAmount,New.CoinCount,New.BillPaidAmount,New.BillCount);
 
 -- Below code written by Ashok on June 11 2013
 -- Insert into StagingPurchase for Incremental ETL
  -- Added on April 21
 IF (New.PurchaseGMT) > '2012-10-01 00:00:00' THEN
INSERT INTO StagingPurchase(Id, CustomerId, PointOfSaleId, PurchaseGMT, PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,
							PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,
							CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,
							IsOffline,IsRefundSlip,CreatedGMT)
VALUES (New.Id, New.CustomerId, New.PointOfSaleId, New.PurchaseGMT, New.PurchaseNumber,New.TransactionTypeId,New.PaymentTypeId,New.LocationId,
							New.PaystationSettingId,New.UnifiedRateId,New.CouponId,New.OriginalAmount,New.ChargedAmount,New.ChangeDispensedAmount,New.ExcessPaymentAmount,
							New.CashPaidAmount,New.CoinPaidAmount,New.BillPaidAmount,New.CardPaidAmount,New.RateAmount,New.RateRevenueAmount,New.CoinCount,New.BillCount,
							New.IsOffline,New.IsRefundSlip,New.CreatedGMT) ;

END IF ;
							
END$$
DELIMITER ;


-- 2) TRIGGER on Permit
-- Inserts data into StagingPermit for Incremental ETL

DROP TRIGGER IF EXISTS  Trg_InsertPermit ;
DROP TRIGGER IF EXISTS  tr_InsertPermit ;

DELIMITER $$
CREATE TRIGGER tr_InsertPermit
AFTER INSERT ON Permit
FOR EACH ROW
BEGIN

-- Added on April 21
 IF (New.PermitBeginGMT) > '2012-10-01 00:00:00' THEN
INSERT INTO StagingPermit (Id,PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,MobileNumberId,
							OriginalPermitId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,
							NumberOfExtensions)
VALUES (New.Id,New.PurchaseId,New.PermitNumber,New.LocationId,New.PermitTypeId,New.PermitIssueTypeId,New.LicencePlateId,New.MobileNumberId,
							New.OriginalPermitId,New.SpaceNumber,New.AddTimeNumber,New.PermitBeginGMT,New.PermitOriginalExpireGMT,New.PermitExpireGMT,
							New.NumberOfExtensions) ;

END IF;
 
END$$
DELIMITER ;

-- 3) TRIGGER on PaymentCard
-- Inserts data into StagingPaymentCard for Incremental ETL

DROP TRIGGER IF EXISTS Trg_InsertPaymentCard ;
DROP TRIGGER IF EXISTS tr_InsertPaymentCard ;

DELIMITER $$
CREATE TRIGGER tr_InsertPaymentCard
AFTER INSERT ON PaymentCard
FOR EACH ROW
BEGIN

-- Added on April 21
 IF (New.CardProcessedGMT) > '2012-10-01 00:00:00' THEN
INSERT INTO StagingPaymentCard (Id, PurchaseId,CardTypeId,CreditCardTypeId,CustomerCardId,ProcessorTransactionTypeId,
								ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,
								IsApproved)
VALUES (New.Id, New.PurchaseId,New.CardTypeId,New.CreditCardTypeId,New.CustomerCardId,New.ProcessorTransactionTypeId,
								New.ProcessorTransactionId,New.MerchantAccountId,New.Amount,New.CardLast4Digits,New.CardProcessedGMT,New.IsUploadedFromBoss,New.IsRFID,
								New.IsApproved)	;							

  END IF;
END$$
DELIMITER ;



-- 4) TRIGGER on ProcessorTransaction
-- Inserts data into StagingProcessorTransaction for Incremental ETL
-- Code Modified on Sep 26

DROP TRIGGER IF EXISTS Trg_InsertProcessorTransaction ;
DROP TRIGGER IF EXISTS tr_InsertProcessorTransaction ;

DELIMITER $$
CREATE TRIGGER tr_InsertProcessorTransaction
AFTER INSERT ON ProcessorTransaction
FOR EACH ROW
BEGIN

-- Added on April 21
 IF (New.PurchasedDate) > '2012-10-01 00:00:00' THEN
IF(New.ProcessorTransactionTypeId not in (0,1,5,8,9,14,20,99)) THEN
INSERT INTO StagingProcessorTransaction (Id,PurchaseId,PreAuthId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,
										 MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,
										ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,
										IsRFID,CreatedGMT)
VALUES (New.Id,New.PurchaseId,New.PreAuthId,New.PointOfSaleId,New.PurchasedDate,New.TicketNumber,New.ProcessorTransactionTypeId,
										 New.MerchantAccountId,New.Amount,New.CardType,New.Last4DigitsOfCardNumber,New.CardChecksum,New.ProcessingDate,
										New.ProcessorTransactionId,New.AuthorizationNumber,New.ReferenceNumber,New.IsApproved,New.CardHash,New.IsUploadedFromBoss,
										New.IsRFID,New.CreatedGMT) ;
END IF;

-- EMS-10037
IF (New.ProcessorTransactionTypeId = (20)) THEN  -- Recoverable

INSERT INTO StagingPreAuthHoldingToRetry(ProcessorTransactionId, PreAuthId, 				Action, CreatedGMT) VALUES
										(New.Id,                 New.AuthorizationNumber,	'Insert',  UTC_TIMESTAMP()) ;

END IF ;
END IF;

END$$
DELIMITER ;


-- Code added on Sep 26

-- 4) TRIGGER on ProcessorTransaction
-- Updates data into StagingProcessorTransaction for Incremental ETL


DROP TRIGGER IF EXISTS tr_UpdateProcessorTransaction ;

DELIMITER $$
CREATE TRIGGER tr_UpdateProcessorTransaction
AFTER UPDATE ON ProcessorTransaction
FOR EACH ROW
BEGIN

-- Added on April 21
 IF (New.PurchasedDate) > '2012-10-01 00:00:00' THEN
IF(New.ProcessorTransactionTypeId not in (0,1,5,8,9,14,20,99)) THEN
INSERT INTO StagingProcessorTransaction (Id,PurchaseId,PreAuthId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,
										 MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,
										ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,
										IsRFID,CreatedGMT)
VALUES (New.Id,New.PurchaseId,New.PreAuthId,New.PointOfSaleId,New.PurchasedDate,New.TicketNumber,New.ProcessorTransactionTypeId,
										 New.MerchantAccountId,New.Amount,New.CardType,New.Last4DigitsOfCardNumber,New.CardChecksum,New.ProcessingDate,
										New.ProcessorTransactionId,New.AuthorizationNumber,New.ReferenceNumber,New.IsApproved,New.CardHash,New.IsUploadedFromBoss,
										New.IsRFID,New.CreatedGMT) ;

END IF;

-- EMS-10037
IF (New.ProcessorTransactionTypeId = (20)) THEN  -- Recoverable

INSERT INTO StagingPreAuthHoldingToRetry(ProcessorTransactionId,	PreAuthId,					Action, CreatedGMT) VALUES
										(New.Id, 					New.AuthorizationNumber,	'Update', UTC_TIMESTAMP()) ;

END IF ;

END IF;
END$$
DELIMITER ;





-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_CustomerProperty_update
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_CustomerProperty_update;

delimiter //
CREATE TRIGGER tr_CustomerProperty_update AFTER UPDATE ON CustomerProperty
FOR EACH ROW BEGIN
    

-- Added on March 6 2015
-- EMS 6306
-- The functionality of this trigger is to log the information when a user changes the CustomerTimeZone
-- ActivityTypeId 1301 is a child value of Company Preferences Configuration in ActivityType
-- EntityTypeId 3 relates to CustomerProperties
-- CustomerPropertyTypeId = 1 is TimeZone property
-- Infuture if we want to track any data changes in CustomerProperties we can add the code in this trigger

	IF (OLD.CustomerPropertyTypeId = 1) THEN
    
		INSERT ActivityLog 	(UserAccountId, 			ActivityTypeId, EntityTypeId, 	EntityId, 	EntityLogId, ActivityGMT) 
		VALUES 				(NEW.LastModifiedByUserId,  1301, 			3, 				OLD.Id, 	OLD.Id, 	 NEW.LastModifiedGMT);
		
	END IF;
 
END //
delimiter ;

-- ------------------------------------------------------------------------------------------------------
-- Trigger: tr_InsertElavonCCData
-- ------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS tr_InsertElavonCCData;

delimiter //

CREATE TRIGGER tr_InsertElavonCCData
AFTER INSERT ON ElavonTransactionDetail
FOR EACH ROW
BEGIN

INSERT INTO ElavonCCData(IdElavonTransactionDetail,CardData) values(New.Id, New.CardData);


END//
delimiter ;



	