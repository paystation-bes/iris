--------------------------------------------------------------------------------
-- Table `Processor`
-- Adding IsActive column which can be used to switch processor visibility in systems admin screen on/off for adding new merchant accounts
-- Updating alliance & first horizon to 0
-------------------------------------------------------------------------------- 
ALTER TABLE `Processor` ADD `IsActive` TINYINT UNSIGNED NOT NULL Default 1 AFTER `IsLink`;
UPDATE `Processor` SET `IsActive` = 0 WHERE Name IN ('processor.alliance','processor.alliance.link','processor.firstHorizon','processor.firstHorizon.link');