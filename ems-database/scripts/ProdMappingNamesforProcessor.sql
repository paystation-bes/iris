
-- This script need to be executed on PROD database before Initial Migration

-- Processor Table

Update Processor set EMS6ProcessorNameMapping = 'Concord' where Id = 1;
Update Processor set EMS6ProcessorNameMapping = 'First Data Nashville' where Id = 2;
Update Processor set EMS6ProcessorNameMapping = 'Heartland' where Id = 3;
Update Processor set EMS6ProcessorNameMapping = 'Link2Gov' where Id = 4;
Update Processor set EMS6ProcessorNameMapping = 'Parcxmart' where Id = 5;
Update Processor set EMS6ProcessorNameMapping = 'Alliance' where Id = 6;
Update Processor set EMS6ProcessorNameMapping = 'Paymentech' where Id = 7;
Update Processor set EMS6ProcessorNameMapping = 'Paradata Gateway' where Id = 8;
Update Processor set EMS6ProcessorNameMapping = 'Moneris' where Id = 9;
Update Processor set EMS6ProcessorNameMapping = 'FirstHorizon' where Id = 10;
Update Processor set EMS6ProcessorNameMapping = 'Authorize.NET' where Id = 11;
Update Processor set EMS6ProcessorNameMapping = 'BlackBoard' where Id = 13;
Update Processor set EMS6ProcessorNameMapping = 'TotalCard' where Id = 14;
Update Processor set EMS6ProcessorNameMapping = 'NuVision' where Id = 15;

