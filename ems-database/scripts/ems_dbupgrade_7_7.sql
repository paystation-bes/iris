INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('PayStationSearchResultsLimitSize', '20', UTC_TIMESTAMP(), 1) ;

ALTER TABLE BillingReportPaystation ADD IsEMV tinyint unsigned, ADD EMVCCTxnCount int unsigned, ADD TerminalId varchar(100);
ALTER TABLE BillingReportPaystationDCOverages ADD IsEMV tinyint unsigned, ADD EMVCCTxnCount int unsigned, ADD TerminalId varchar(100);

INSERT INTO EventActionType VALUES (9, "Violated", UTC_TIMESTAMP(), 1);
INSERT INTO EventActionType VALUES (10, "Tampered", UTC_TIMESTAMP(), 1);
INSERT INTO EventActionType VALUES (11, "Fault", UTC_TIMESTAMP(), 1);

INSERT INTO EventType VALUES (57, "eventType.cardReader.violationStatus", 4, 1, 1, 0, 1, UTC_TIMESTAMP(), 1); 

INSERT INTO EventStatusType VALUES (29, "Card Reader Status", UTC_TIMESTAMP(), 1);

INSERT INTO EventDefinition VALUES (113, 4, 29, 9, 3, "eventdefinition.cardReader.violationStatus.violated", "violationStatusViolated", 57, UTC_TIMESTAMP(), 1); 
INSERT INTO EventDefinition VALUES (114, 4, 29, 10, 3, "eventdefinition.cardReader.violationStatus.tampered", "violationStatusTampered", 57, UTC_TIMESTAMP(), 1); 
INSERT INTO EventDefinition VALUES (115, 4, 29, 11, 2, "eventdefinition.cardReader.violationStatus.fault", "violationStatusFault", 57, UTC_TIMESTAMP(), 1); 
INSERT INTO EventDefinition VALUES (116, 4, 29, 4, 0, "eventdefinition.cardReader.violationStatus.normal", "violationStatusOk", 57, UTC_TIMESTAMP(), 1); 

ALTER TABLE POSServiceState ADD IsNewMerchantAccount TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER IsNewTicketFooter;

ALTER TABLE POSServiceState ADD MerchantAccountChangedGMT DATETIME NULL AFTER UpgradeGMT;

ALTER TABLE POSServiceState ADD MerchantAccountRequestedGMT DATETIME NULL AFTER MerchantAccountChangedGMT;

ALTER TABLE POSServiceState ADD MerchantAccountUploadedGMT DATETIME NULL AFTER MerchantAccountRequestedGMT;

ALTER TABLE MerchantAccount ADD TerminalToken VARCHAR(100) NULL AFTER VERSION;

CREATE TABLE IF NOT EXISTS `CPSData` (
  `Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProcessorTransactionId` BIGINT UNSIGNED NOT NULL,
  `ChargeToken` varchar(60) NOT NULL,
  `Ref` varchar(60) DEFAULT NULL,
  `CVM` varchar(60) DEFAULT NULL,
  `APL` varchar(40) DEFAULT NULL,
  `AID` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_CPSData_ProcessorTransaction` (`ProcessorTransactionId`),
  CONSTRAINT `fk_CPSData_ProcessorTransaction` FOREIGN KEY (`ProcessorTransactionId`) REFERENCES `ProcessorTransaction` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(19, 20, 'Chase Paymentech', 'Chase Paymentech', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(20, 20, 'Elavon', 'Elavon', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(21, 20, 'First Data', 'First Data', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(22, 20, 'Global Payments', 'Global Payments', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(23, 20, 'Heartland', 'Heartland', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(24, 20, 'Moneris', 'Moneris', UTC_TIMESTAMP());
INSERT INTO `GatewayProcessor` (`Id`, `ProcessorId`, `Name`, `Value`, `CreatedGMT`) VALUES(25, 20, 'Vantiv', 'Vantiv', UTC_TIMESTAMP());

INSERT EmsProperties (Name, Value, LastModifiedGMT, LastModifiedByUserId) VALUES ('TDMerchantMaxSearchDays', '30', UTC_TIMESTAMP(), 1) ;

UPDATE Processor SET `ProductionUrl`='https://proxy.creditcall.t2systems.com:443', `TestUrl`='https://test.creditcall.t2systems.com:443' WHERE `Id`='20';

UPDATE EmsProperties SET `Value` = 'EMS v7.7.0' WHERE `Name` = 'EmsVersion';

-- Delete 'processor.t2emv' from EMVTransactionDataMapping, GatewayProcessor and Processor.
DELETE FROM EMVTransactionDataMapping WHERE ProcessorId = 99;
DELETE FROM GatewayProcessor WHERE ProcessorId = 99;
DELETE FROM Processor WHERE Id = 99;
ALTER TABLE ReportContent MODIFY Content LONGBLOB  NOT NULL;

DELIMITER $$

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCount $$
CREATE PROCEDURE sp_BillingReportTxnCount (in P_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_DateRangeBegin, lv_DateRangeEnd Date;
declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;
declare lv_EMVTxnCount INT UNSIGNED ;

declare c1 cursor  for
select CustomerId, PointOfSaleId, B.DateRangeBegin, B.DateRangeEnd from BillingReportPaystation A, BillingReport B WHERE 
A.BillingReportId = B.Id and A.BillingReportId = P_BillingReportId
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;

/* DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
      -- DML
  END;
*/

set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_PointOfSaleId, lv_DateRangeBegin, lv_DateRangeEnd;

	set lv_TransactionCount = 0;
	set lv_EMVTxnCount = 0 ;
	
	select count(*) into lv_TransactionCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month );
	
	
	-- -------------------------------------------------------------------
    -- Type 16: EmsPsType16TxnCount (AddTime: Extend by Phone Permit Extension)
    
    select count(*) into lv_Type16TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month )
	and TransactionTypeId = 16;
	
	
	/*
	SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 16   -- 16 is for 'AddTime: Extend By Phone AddTime Transaction' 
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
	*/
	
	            
    -- -------------------------------------------------------------------
    -- Type 17: EmsPsType17TxnCount (Regular: Extend by Phone Permit)
   
    select count(*) into lv_Type17TxnCount 
	from Purchase where CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month )
	and TransactionTypeId = 17;
	
	/*    
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 17   -- 17 is for 'Regular: Extend By Phone Regular Transaction' 
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
	*/ 
	
	-- EMS-10283
	SELECT COUNT(*) into lv_EMVTxnCount
	FROM Purchase P 
	inner join ProcessorTransaction PT on P.Id = PT.PurchaseId
	inner join MerchantAccount MA on PT.MerchantAccountId = MA.Id
	inner join Processor Pro on (MA.ProcessorId = Pro.Id and Pro.IsEMV = 1)
	where P.CustomerId = lv_CustomerId and P.PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	PurchaseGMT >= concat(date_format(LAST_DAY(lv_DateRangeEnd - interval 0 month),'%Y-%m-'),'01')
	and PurchaseGMT <= LAST_DAY(lv_DateRangeEnd - interval 0 month );
	
	UPDATE BillingReportPaystation
	SET TransactionCount = lv_TransactionCount, 
	Type16TxnCount = lv_Type16TxnCount ,
	Type17TxnCount = lv_Type17TxnCount ,
	EMVCCTxnCount = lv_EMVTxnCount
	where BillingReportId = P_BillingReportId and PointOfSaleId=lv_PointOfSaleId;
  
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END $$

DELIMITER ;

-- End Procedure sp_BillingReportTxnCount

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCountDCOverages ;

delimiter //

CREATE  PROCEDURE `sp_BillingReportTxnCountDCOverages`(in P_BillingReportId int unsigned)
BEGIN

declare NO_DATA int(4) default 0;
declare v1 int;
declare idmaster_pos,indexm_pos,terminal_pos int(4);
declare lv_LowerLimit, lv_UpperLimt BIGINT UNSIGNED ;
declare lv_CustomerId, lv_PointOfSaleId MEDIUMINT UNSIGNED;
declare lv_DateRangeBegin, lv_DateRangeEnd Date;
declare lv_TransactionCount,lv_Type16TxnCount, lv_Type17TxnCount INT UNSIGNED ;
declare lv_EMVTxnCount INT UNSIGNED ;

declare c1 cursor  for
select CustomerId, PointOfSaleId, B.DateRangeBegin, B.DateRangeEnd from BillingReportPaystationDCOverages A, BillingReportDCOverages B WHERE 
A.BillingReportId = B.Id and A.BillingReportId = P_BillingReportId
order by CustomerId;

declare continue handler for not found set NO_DATA=-1;



set indexm_pos =0;
set idmaster_pos =0;

open c1;
set idmaster_pos = (select FOUND_ROWS());

while indexm_pos < idmaster_pos do
fetch c1 into lv_CustomerId, lv_PointOfSaleId, lv_DateRangeBegin, lv_DateRangeEnd;

	set lv_TransactionCount = 0;
	set lv_EMVTxnCount = 0;
	
	-- EMS-10647
	select count(*) into lv_TransactionCount 
	from Purchase P use index (idx_purchase_multi_uq), CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day)) 
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month));
		
	
	-- EMS-10647
	select count(*) into lv_Type16TxnCount 
	from Purchase P use index (idx_purchase_multi_uq), CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day))
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month))
	and TransactionTypeId = 16;
	
	
	-- EMS-10647
	select count(*) into lv_Type17TxnCount 
	from Purchase  P use index (idx_purchase_multi_uq), CustomerProperty CP, Customer C,
	mysql.time_zone_name tzn, 
	mysql.time_zone_transition_type tztt
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day))
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month))
	and TransactionTypeId = 17;
	
	-- EMS-10283
	SELECT COUNT(*) into lv_EMVTxnCount
	FROM Purchase P 
	inner join ProcessorTransaction PT on P.Id = PT.PurchaseId
	inner join MerchantAccount MA on PT.MerchantAccountId = MA.Id
	inner join Processor Pro on (MA.ProcessorId = Pro.Id and Pro.IsEMV = 1)
	where 
	C.Id = CP.CustomerId and
	CP.CustomerPropertyTypeId = 1 and
	P.CustomerId = C.Id and 
	CP.PropertyValue = tzn.Name and 
	tzn.Time_zone_id = tztt.Time_zone_id and 
	tztt.transition_type_id= 1 and
	P.CustomerId = lv_CustomerId and P.PointOfSaleId = lv_PointOfSaleId and 
	TransactionTypeId not in (6) and
	CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) >= date(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day)) 
	and CONVERT_TZ(PurchaseGMT,'GMT',CP.PropertyValue) < date(date_add(date_sub(date_sub(lv_DateRangeEnd,interval 1 month),interval dayofmonth(lv_DateRangeEnd)-1 day),interval 1 month));
		
	
	UPDATE BillingReportPaystationDCOverages
	SET TransactionCount = lv_TransactionCount, 
	Type16TxnCount = lv_Type16TxnCount ,
	Type17TxnCount = lv_Type17TxnCount ,
	EMVCCTxnCount = lv_EMVTxnCount
	where BillingReportId = P_BillingReportId and PointOfSaleId=lv_PointOfSaleId;
  
	
	
set indexm_pos = indexm_pos +1;
end while;

close c1;

END//

delimiter ;



DROP PROCEDURE IF EXISTS sp_StartBillingReport;

delimiter //
CREATE PROCEDURE sp_StartBillingReport (INOUT task TINYINT UNSIGNED, INOUT idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    /* prvbdt & prvedt: the begin and end dates of the most recently posted (i.e., previous) Billing Report */
    DECLARE prvbdt DATE;
    DECLARE prvedt DATE;
    DECLARE prvBR  INT UNSIGNED;
    
    /* get info from the most recently posted Billing Report */
    SELECT Id, DateRangeBegin, DateRangeEnd INTO prvBR, prvbdt, prvedt
    FROM   BillingReport 
    WHERE  Id = (SELECT MAX(Id) FROM BillingReport WHERE IsPosted=1);
   
    /* error check: report end date must be greater than begin date */
    IF bdt > edt THEN SET task = 4; END IF;
                      
    /* check for very first Billing Report (no other Billing Report exists) */
    SELECT IF((SELECT COUNT(*) FROM BillingReport)=0 AND task=0,1,task) INTO task;

    /* determine if a new Billing Report is to be created (the previous Billing Report was posted (closed) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=1 AND task=0,1,task) INTO task;
     
    /* if starting a new Billing Report validate the report's begin and end dates */
    IF task=1 THEN      
        /* the begin date for the next report MUST be the day after the end date from the previous report */
        SELECT IF(ADDDATE(prvedt,INTERVAL 1 DAY)!=bdt,5,task) INTO task;
    END IF;
   
    /* existing Billing Report (existing Billing Report has yet to be posted) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=0 AND task=0,3,task) INTO task;

    /* validate dates for existing report (a continuation of the line directly above) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport) AND DateRangeBegin=bdt AND DateRangeEnd=edt)=0 AND task=3,2,task) INTO task;

    /* task has been assigned a value, examine this value */
    CASE task
        /* task=0 error, task not determined */
        WHEN 0 THEN
        BEGIN
            SELECT 'ERROR. Could not determine whether to create a new Billing Report or update an existing Billing Report.' AS ErrorMessage;
        END;

        /* task=1: create new Billing Report */
        WHEN 1 THEN
        BEGIN
            INSERT BillingReport (DateRangeBegin,DateRangeEnd,RunBegin,IsPosted,IsError) VALUES (bdt,edt,UTC_TIMESTAMP(),0,0);
            SET idBR = last_insert_id();
        END;

        /* task=2: update existing Billing Report */
        WHEN 2 THEN
        BEGIN
            SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt AND IsPosted=0 AND Id=(SELECT MAX(Id) FROM BillingReport) INTO idBR;
            UPDATE BillingReport SET RunBegin=UTC_TIMESTAMP(), RunEnd=UTC_TIMESTAMP(), IsError=0 WHERE Id=idBR;
            DELETE FROM BillingReportPaystation WHERE BillingReportId=idBR;
        END;

        /* task=3: error, invalid dates for existing report: the begin and end dates specified must match the begin and end dates for the current non-posted Billing Report */
        WHEN 3 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. Parameter dates do not match current Billing Report dates: ',bdt,' and ',edt,' do not match ',DateRangeBegin,' and ',DateRangeEnd) AS ErrorMessage
            FROM   BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport);
        END;
         
        /* task=4: error, invalid dates, end date must be greater than begin date */
        WHEN 4 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' cannot be greater than the report end date ',edt) AS ErrorMessage;
        END;
         
         /* task=5: error, the begin date of a new report must be the day after the nend date of the old report */
        WHEN 5 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' must be the next day following the previous reports end date ',prvedt) AS ErrorMessage;
        END;

        /* task>5: error */
        ELSE
        BEGIN
            SELECT 'ERROR. Could not resolve Billing Report parameters.' AS ErrorMessage;
        END;
    END CASE;
    
    /* add Paystations to Billing Report */
    IF task=1 OR task=2 THEN    
        
        /* create master list of Paystations for Billing Report  */
        INSERT INTO BillingReportPaystation 
				(BillingReportId,CustomerId,CustomerName,IsTrial, TrialExpiryGMT, PaystationId,PointOfSaleId,POSName,POSLocationId,ProvisionedGMT,POSLastModifiedGMT,SerialNumber,PaystationSettingName,LocationName,
				LastHeartbeatGMT,IsActivated,IsLocked,IsDigitalConnect,IsBillableMonthlyOnActivation,IsBillableMonthlyForEms,IsDeleted, ActivationChangeGMT,
				ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,ServicePayByPhone,
				ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned,IsEMV,TerminalId)
				Select idBR,c.Id,c.Name,c.CustomerStatusTypeId,c.TrialExpiryGMT,pst.Id,pos.Id,pos.Name,pos.LocationId,pos.ProvisionedGMT,pos.LastModifiedGMT,pos.SerialNumber,pss.PaystationSettingName,l.Name,
				phb.LastHeartbeatGMT,ps.IsActivated,ps.IsLocked, ps.IsDigitalConnect ,ps.IsBillableMonthlyOnActivation ,ps.IsBillableMonthlyForEms ,pst.IsDeleted ,ps.IsActivatedGMT ,
				csStdReports.IsEnabled ,csAlerts.IsEnabled,csCCProcess.IsEnabled,csBatchCCProcess.IsEnabled, csCampusProcess.IsEnabled, csCoupons.IsEnabled, csPasscards.IsEnabled,csSmartcards.IsEnabled,csExtendByPhone.IsEnabled,
				csDigitalRead.IsEnabled, csDigitalWrite.IsEnabled,csPayByCell.IsEnabled,ps.IsDecommissioned,pro.IsEMV,EMVMA.Field1 
				FROM PointOfSale pos 
				INNER JOIN Location l ON pos.LocationId = l.Id
				INNER JOIN Customer c ON pos.CustomerId = c.Id
				INNER JOIN POSServiceState pss ON pos.Id = pss.PointOfSaleId
				INNER JOIN POSHeartbeat phb ON pos.Id = phb.PointOfSaleId
				INNER JOIN POSStatus ps ON ps.PointOfSaleId = pos.Id
				inner join CustomerSubscription csStdReports on csStdReports.CustomerId = c.Id
				inner join CustomerSubscription csAlerts on csAlerts.CustomerId = c.Id
				inner join CustomerSubscription csCCProcess on csCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csBatchCCProcess on csBatchCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csCampusProcess on csCampusProcess.CustomerId = c.Id
				inner join CustomerSubscription csCoupons on csCoupons.CustomerId = c.Id
				inner join CustomerSubscription csPasscards on csPasscards.CustomerId = c.Id
				inner join CustomerSubscription csSmartcards on csSmartcards.CustomerId = c.Id
				inner join CustomerSubscription csExtendByPhone on csExtendByPhone.CustomerId = c.Id
				inner join CustomerSubscription csDigitalRead on csDigitalRead.CustomerId = c.Id
				inner join CustomerSubscription csDigitalWrite on csDigitalWrite.CustomerId = c.Id
				inner join CustomerSubscription csPayByCell on csPayByCell.CustomerId = c.Id
				inner join CustomerStatusType cStatus on c.CustomerStatusTypeId = cStatus.Id
				inner join Paystation pst on pst.Id = pos.PaystationId 
				LEFT JOIN MerchantPOS mpos ON (pos.Id = mpos.PointOfSaleId and mpos.IsDeleted = 0 and mpos.CardTypeId = 1)
				LEFT JOIN MerchantAccount ma ON mpos.MerchantAccountId = ma.Id
				LEFT JOIN  Processor pro ON ma.ProcessorId = pro.Id
				LEFT JOIN MerchantAccount EMVMA on (mpos.MerchantAccountId = EMVMA.Id and EMVMA.ProcessorId = pro.Id and pro.IsEMV=1) 
				where 
				csStdReports.SubscriptionTypeId = 100 and
				csAlerts.SubscriptionTypeId = 200 and
				csCCProcess.SubscriptionTypeId = 300 and
				csBatchCCProcess.SubscriptionTypeId = 400 and
				csCampusProcess.SubscriptionTypeId = 500 and
				csCoupons.SubscriptionTypeId = 600 and 
				csPasscards.SubscriptionTypeId = 700 and
				csSmartcards.SubscriptionTypeId = 800 and
				csExtendByPhone.SubscriptionTypeId = 900 and
				csDigitalRead.SubscriptionTypeId = 1000 and
				csDigitalWrite.SubscriptionTypeId = 1100 and
				csPayByCell.SubscriptionTypeId = 1200 AND 	
				UPPER(pos.SerialNumber) NOT LIKE '%X%'     
				AND     pos.SerialNumber NOT LIKE '9%'            
				AND     pos.SerialNumber NOT LIKE '8%'
				AND     UPPER(pos.SerialNumber) NOT LIKE '%VER%';
				

		-- Get Transaction Count		
		CALL sp_BillingReportTxnCount(idBR);
		
		SELECT  Id FROM BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
			
		IF (prvBR IS NOT NULL) THEN
		
			-- Find for any Changes			
			CALL sp_FindBillingReportChanges(idBR,prvBR);
			CALL sp_RecordBillingReportChanges(idBR,prvBR);
		END IF ;
		
    END IF;
 
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_StartBillingReport' AS Trace;
    END IF;
    
END//
delimiter ;


DROP PROCEDURE IF EXISTS sp_StartBillingReportDCOverages ;

delimiter //

CREATE PROCEDURE `sp_StartBillingReportDCOverages`(INOUT task TINYINT UNSIGNED, INOUT idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    
    DECLARE prvbdt DATE;
    DECLARE prvedt DATE;
    DECLARE prvBR  INT UNSIGNED;
    
    
    SELECT Id, DateRangeBegin, DateRangeEnd INTO prvBR, prvbdt, prvedt
    FROM   BillingReportDCOverages 
    WHERE  Id = (SELECT MAX(Id) FROM BillingReportDCOverages WHERE IsPosted=1);
   
    
    IF bdt > edt THEN SET task = 4; END IF;
                      
    
    SELECT IF((SELECT COUNT(*) FROM BillingReportDCOverages)=0 AND task=0,1,task) INTO task;

    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages))=1 AND task=0,1,task) INTO task;
     
    
    IF task=1 THEN      
        
        SELECT IF(ADDDATE(prvedt,INTERVAL 1 DAY)!=bdt,5,task) INTO task;
    END IF;
   
    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages))=0 AND task=0,3,task) INTO task;

    
    SELECT IF((SELECT IsPosted FROM BillingReportDCOverages WHERE Id=(SELECT MAX(Id) FROM BillingReportDCOverages) AND DateRangeBegin=bdt AND DateRangeEnd=edt)=0 AND task=3,2,task) INTO task;

    
    CASE task
        
        WHEN 0 THEN
        BEGIN
            SELECT 'ERROR. Could not determine whether to create a new Billing Report or update an existing Billing Report.' AS ErrorMessage;
        END;

        
        WHEN 1 THEN
        BEGIN
            INSERT BillingReportDCOverages (DateRangeBegin,DateRangeEnd,RunBegin,IsPosted,IsError) VALUES (bdt,edt,UTC_TIMESTAMP(),0,0);
            SET idBR = last_insert_id();
        END;

        
        WHEN 2 THEN
        BEGIN
            SELECT Id FROM BillingReportDCOverages WHERE DateRangeBegin=bdt AND DateRangeEnd=edt AND IsPosted=0 AND Id=(SELECT MAX(Id) FROM BillingReportDCOverages) INTO idBR;
            UPDATE BillingReportDCOverages SET RunBegin=UTC_TIMESTAMP(), RunEnd=UTC_TIMESTAMP(), IsError=0 WHERE Id=idBR;
            DELETE FROM BillingReportPaystationDCOverages WHERE BillingReportId=idBR;
        END;

        
        WHEN 3 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. Parameter dates do not match current Billing Report dates: ',bdt,' and ',edt,' do not match ',DateRangeBegin,' and ',DateRangeEnd) AS ErrorMessage
            FROM   BillingReportDCOverages WHERE Id = (SELECT MAX(Id) FROM BillingReportDCOverages);
        END;
         
        
        WHEN 4 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' cannot be greater than the report end date ',edt) AS ErrorMessage;
        END;
         
         
        WHEN 5 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' must be the next day following the previous reports end date ',prvedt) AS ErrorMessage;
        END;

        
        ELSE
        BEGIN
            SELECT 'ERROR. Could not resolve Billing Report parameters.' AS ErrorMessage;
        END;
    END CASE;
    
    
    IF task=1 OR task=2 THEN    
        
        
        INSERT INTO BillingReportPaystationDCOverages 
				(BillingReportId,CustomerId,CustomerName,IsTrial, TrialExpiryGMT, PaystationId,PointOfSaleId,POSName,POSLocationId,ProvisionedGMT,POSLastModifiedGMT,SerialNumber,PaystationSettingName,LocationName,
				LastHeartbeatGMT,IsActivated,IsLocked,IsDigitalConnect,IsBillableMonthlyOnActivation,IsBillableMonthlyForEms,IsDeleted, ActivationChangeGMT,
				ServiceStandardReports,ServiceAlerts,ServiceRealTimeCC,ServiceBatchCC,ServiceCampusCard,ServiceCoupon,ServicePasscard,ServiceSmartCard,ServicePayByPhone,
				ServiceDigitalAPIRead,ServiceDigitalAPIWrite,ServiceExtendByCell,IsDecommissioned,IsEMV,TerminalId)
				Select idBR,c.Id,c.Name,c.CustomerStatusTypeId,c.TrialExpiryGMT,pst.Id,pos.Id,pos.Name,pos.LocationId,pos.ProvisionedGMT,pos.LastModifiedGMT,pos.SerialNumber,pss.PaystationSettingName,l.Name,
				phb.LastHeartbeatGMT,ps.IsActivated,ps.IsLocked, ps.IsDigitalConnect ,ps.IsBillableMonthlyOnActivation ,ps.IsBillableMonthlyForEms ,pst.IsDeleted ,ps.IsActivatedGMT ,
				csStdReports.IsEnabled ,csAlerts.IsEnabled,csCCProcess.IsEnabled,csBatchCCProcess.IsEnabled, csCampusProcess.IsEnabled, csCoupons.IsEnabled, csPasscards.IsEnabled,csSmartcards.IsEnabled,csExtendByPhone.IsEnabled,
				csDigitalRead.IsEnabled, csDigitalWrite.IsEnabled,csPayByCell.IsEnabled,ps.IsDecommissioned,pro.IsEMV, EMVMA.Field1
				FROM PointOfSale pos 
				INNER JOIN Location l ON pos.LocationId = l.Id
				INNER JOIN Customer c ON pos.CustomerId = c.Id
				INNER JOIN POSServiceState pss ON pos.Id = pss.PointOfSaleId
				INNER JOIN POSHeartbeat phb ON pos.Id = phb.PointOfSaleId
				INNER JOIN POSStatus ps ON ps.PointOfSaleId = pos.Id
				inner join CustomerSubscription csStdReports on csStdReports.CustomerId = c.Id
				inner join CustomerSubscription csAlerts on csAlerts.CustomerId = c.Id
				inner join CustomerSubscription csCCProcess on csCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csBatchCCProcess on csBatchCCProcess.CustomerId = c.Id
				inner join CustomerSubscription csCampusProcess on csCampusProcess.CustomerId = c.Id
				inner join CustomerSubscription csCoupons on csCoupons.CustomerId = c.Id
				inner join CustomerSubscription csPasscards on csPasscards.CustomerId = c.Id
				inner join CustomerSubscription csSmartcards on csSmartcards.CustomerId = c.Id
				inner join CustomerSubscription csExtendByPhone on csExtendByPhone.CustomerId = c.Id
				inner join CustomerSubscription csDigitalRead on csDigitalRead.CustomerId = c.Id
				inner join CustomerSubscription csDigitalWrite on csDigitalWrite.CustomerId = c.Id
				inner join CustomerSubscription csPayByCell on csPayByCell.CustomerId = c.Id
				inner join CustomerStatusType cStatus on c.CustomerStatusTypeId = cStatus.Id
				inner join Paystation pst on pst.Id = pos.PaystationId 
				LEFT JOIN MerchantPOS mpos ON (pos.Id = mpos.PointOfSaleId and mpos.IsDeleted = 0 and mpos.CardTypeId = 1)
				LEFT JOIN MerchantAccount ma ON mpos.MerchantAccountId = ma.Id
				LEFT JOIN  Processor pro ON ma.ProcessorId = pro.Id
				LEFT JOIN MerchantAccount EMVMA on (mpos.MerchantAccountId = EMVMA.Id and EMVMA.ProcessorId = pro.Id and pro.IsEMV=1)
				 
				where 
				csStdReports.SubscriptionTypeId = 100 and
				csAlerts.SubscriptionTypeId = 200 and
				csCCProcess.SubscriptionTypeId = 300 and
				csBatchCCProcess.SubscriptionTypeId = 400 and
				csCampusProcess.SubscriptionTypeId = 500 and
				csCoupons.SubscriptionTypeId = 600 and 
				csPasscards.SubscriptionTypeId = 700 and
				csSmartcards.SubscriptionTypeId = 800 and
				csExtendByPhone.SubscriptionTypeId = 900 and
				csDigitalRead.SubscriptionTypeId = 1000 and
				csDigitalWrite.SubscriptionTypeId = 1100 and
				csPayByCell.SubscriptionTypeId = 1200 
				AND 	UPPER(pos.SerialNumber) NOT LIKE '%X%'     
				AND     pos.SerialNumber NOT LIKE '9%'            
				AND     pos.SerialNumber NOT LIKE '8%'
				AND     UPPER(pos.SerialNumber) NOT LIKE '%VER%';
				
		
		
		CALL sp_BillingReportTxnCountDCOverages(idBR);
		
		SELECT  Id FROM BillingReportDCOverages WHERE Id = (SELECT MAX(Id) FROM BillingReportDCOverages WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
			
		
		
    END IF;
 
    
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_StartBillingReportDCOverages' AS Trace;
    END IF;
    
END


