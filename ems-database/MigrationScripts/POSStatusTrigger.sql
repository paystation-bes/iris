DELIMITER $$
CREATE TRIGGER InsertPOSDate
AFTER INSERT ON POSDate
FOR EACH ROW
BEGIN
DECLARE IsProvisioned INT;
DECLARE IsLocked INT;
DECLARE IsDecommissioned INT;
DECLARE IsDeleted INT;
DECLARE IsVisible INT;
DECLARE IsBillableMonthlyOnActivation INT;
DECLARE IsDigitalConnect INT;
DECLARE IsTestActivated INT;
DECLARE IsActivated INT;
DECLARE IsBillableMonthlyForEms INT;
DECLARE IsProvisionedGMT DATE; 
DECLARE IsLockedGMT INT;
DECLARE IsDecommissionedGMT INT;
DECLARE IsDeletedGMT DATE;
DECLARE IsVisibleGMT DATE;
DECLARE IsBillableMonthlyOnActivationGMT INT;
DECLARE IsDigitalConnectGMT DATE;
DECLARE IsTestActivatedGMT DATE;
DECLARE IsActivatedGMT DATE;
DECLARE IsBillableMonthlyForEmsGMT DATE;
DECLARE LastModifiedGMT DATE;
DECLARE LastModifiedByUserId INT;
DECLARE PointOfSaleId MEDIUMINT(10);
DECLARE POSDateTypeId TINYINT(3);
DECLARE ChangedGMT DATETIME;
DECLARE CurrentSetting TINYINT(1);
DECLARE VERSION INT(10);

SET IsProvisioned = 0;
SET IsLocked = 0;
SET IsDecommissioned = 0;
SET IsDeleted = 0;
SET IsVisible = 1;
SET IsBillableMonthlyForEms = 0;
SET IsDigitalConnect=0;
SET IsTestActivated=0;
SET IsActivated =1;
SET IsBillableMonthlyForEms=0;
SET IsProvisionedGMT = now();
SET IsLockedGMT = now();
SET IsDecommissionedGMT = now();
SET IsDeletedGMT = now();
SET IsVisibleGMT = now();
SET IsBillableMonthlyOnActivationGMT = now();
SET IsDigitalConnectGMT = now();
SET IsTestActivatedGMT = now();
SET IsActivatedGMT = now();
SET IsBillableMonthlyForEmsGMT = now();
SET VERSION= 0;
SET LastModifiedGMT = now();
SET LastModifiedByUserId = 1;

SET PointOfSaleId = New.PointOfSaleId;
SET POSDateTypeId = New.POSDateTypeId;
SET ChangedGMT = New.ChangedGMT;
SET CurrentSetting = New.CurrentSetting;
SET VERSION = New.VERSION;

IF POSDateTypeId=1 THEN
SET IsProvisioned = CurrentSetting;
SET IsProvisionedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=2 THEN
SET IsLocked = CurrentSetting;
SET IsLockedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=3 THEN
SET IsDecommissioned = CurrentSetting;
SET IsDecommissionedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=4 THEN
SET IsDeleted = CurrentSetting;
SET IsDeletedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=5 THEN
SET IsVisible = 1;
SET IsVisibleGMT = ChangedGMT;
END IF;

IF POSDateTypeId=6 THEN
SET IsBillableMonthlyOnActivation=CurrentSetting;
SET IsBillableMonthlyOnActivationGMT = ChangedGMT;
END IF;

IF POSDateTypeId=7 THEN
SET IsDigitalConnect = CurrentSetting;
SET IsDigitalConnectGMT = ChangedGMT;
END IF;

IF POSDateTypeId=8 THEN
SET IsTestActivated = CurrentSetting;
SET IsTestActivatedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=9 THEN
SET IsActivated = CurrentSetting;
SET IsActivatedGMT = ChangedGMT;
END IF;

IF POSDateTypeId=10 THEN
SET IsBillableMonthlyForEms = CurrentSetting;
SET IsBillableMonthlyForEmsGMT = ChangedGMT;
END IF;

INSERT IGNORE INTO POSStatus(PointOfSaleId,IsProvisioned,IsLocked,IsDecommissioned,IsDeleted,IsVisible,IsBillableMonthlyOnActivation,\
IsDigitalConnect,IsTestActivated,IsActivated,IsBillableMonthlyForEms,IsProvisionedGMT,IsLockedGMT,IsDecommissionedGMT,IsDeletedGMT,IsVisibleGMT,\
IsBillableMonthlyOnActivationGMT,IsDigitalConnectGMT,IsTestActivatedGMT,IsActivatedGMT,IsBillableMonthlyForEmsGMT,VERSION,LastModifiedGMT,LastModifiedByUserId)\
 values(PointOfSaleId,IsProvisioned,IsLocked,IsDecommissioned,IsDeleted,IsVisible,IsBillableMonthlyOnActivation,IsDigitalConnect,IsTestActivated,\
 IsActivated,IsBillableMonthlyForEms,IsProvisionedGMT,IsLockedGMT,IsDecommissionedGMT,IsDeletedGMT,IsVisibleGMT,IsBillableMonthlyOnActivationGMT,\
 IsDigitalConnectGMT,IsTestActivatedGMT,IsActivatedGMT,IsBillableMonthlyForEmsGMT,VERSION,LastModifiedGMT,LastModifiedByUserId);

END$$
