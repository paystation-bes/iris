

SELECT '******* Creating database *******' AS INFO;

SOURCE scripts\\ems_db_7_000_database_create.sql

SELECT '******* Creating Tables *******' AS INFO;

SOURCE scripts\\ems_db_7_100_tables.sql

SELECT '******* Creating Timezones in MYSQL Schema *******' AS INFO; 

SOURCE scripts\\ems_db_7_110_tables_mysql_timezones.sql 

SELECT '******* Creating Procedures *******' AS INFO;

SOURCE scripts\\ems_db_7_150_procedures.sql

SELECT '******* Creating ReCal Procedures *******' AS INFO;

SOURCE scripts\\ems_db_7_155_recalc_procedures.sql

SELECT '******* Creating Views *******' AS INFO;

SOURCE scripts\\ems_db_7_175_views.sql

SELECT '******* Inserting Data into Looks ups *******' AS INFO;

SOURCE scripts\\ems_db_7_200_lookups.sql

SELECT '******* Inserting Data into EMS Properties *******' AS INFO;

SOURCE scripts\\ems_db_7_201_lookups_development.sql

SELECT '******* Creating Stored Procedure Time and Generate ETL DATE RANGE *******' AS INFO;

SOURCE scripts\\ems_db_7_250_lookups_procedures.sql

SELECT '******* Creating EMS 7 Triggers on Customer Agreement, Notification, User Account and ETL *******' AS INFO;

SOURCE scripts\\ems_db_7_300_triggers_onlyforBlitz.sql

SELECT '******* Creating EMS 7 ETL StoredProcedures *******' AS INFO;

SOURCE scripts\\ems_db_7_500_etl_procedures.sql

SELECT '******* Inserting Record in ServiceAgreement Table *******' AS INFO;

SOURCE scripts\\ems_db_7_902_test_service_agreement.sql

SELECT '******* Creating Mapping Tables *******' AS INFO;

SOURCE MigrationScripts\\ProdITFScripts\\DBScripts\\MappingTables.sql

SELECT '******* Updating Mapping Name for Processor for PRODUCTION *******' AS INFO;

SOURCE scripts\\ProdMappingNamesforProcessor.sql

SELECT '******* Creating Migration Lookup TXT Tables for past 3 month run *******' AS INFO;


call sp_GenerateMigrationDateRange(1,'Purchase','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'ProcessorTransaction','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'Permit','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'PaymentCard','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'POSCollection','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'PurchaseTax','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

call sp_GenerateMigrationDateRange(1,'PurchaseCollection','2013-01-01 00:00:00','2013-08-31 23:59:59',90);

SOURCE MigrationScripts\\ProdITFScripts\\DBScripts\\AssigningPythonInstancesToTransactions.sql

SELECT '******* Creating ETL Procedures for Initial Migration Data *******' AS INFO;

SOURCE scripts\\sp_MigrateRevenueIntoKPI.sql
SOURCE scripts\\sp_FillOccupancyTimeslots.sql
SOURCE scripts\\MigrateDailyUtilToMonthly.sql
SOURCE scripts\\ETLSettledTransactions.sql
SOURCE scripts\\ETL_Collection_widget.sql




SELECT '**** END ****' 

