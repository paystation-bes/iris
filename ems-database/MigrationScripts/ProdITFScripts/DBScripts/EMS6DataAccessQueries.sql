EMS6 Query
---------------

3) getEMS6LicencePlate

select distinct(PlateNumber) from Transaction where PaystationId=%s and PlateNumber<>''",(PaystationId))

EMS7--> addLicencePlateToEMS7

self.__EMS7Cursor.execute("insert ignore into LicencePlate(Number,LastModifiedGMT) values(%s,%s)",\
					(LicencePlate,LastModifiedGMT))
					
4) EMS7 --> addMobileNumberToEMS7

self.__EMS7Cursor.execute("insert ignore into MobileNumber \
					  (Number,LastModifiedGMT) values(%s,%s)",\
					  (MobileNumber,LastModifiedGMT))
					  
					  

Transaction:
Purchase:

EMS6 --> select distinct(PaystationId) from Transaction;

EMS6 -->
self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,\
			tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,\
			tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,\
			tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,\
			tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid,ep.MobileNumber \
			from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) \
			left join Rates rt using (PaystationId,PurchasedDate,TicketNumber)left join EMSExtensiblePermit ep using (PurchasedDate) \
			where tr.PaystationId=%s and tr.PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
			


EMS 7--> insert ignore into ProcessorTransaction

def getEMS6TransactionForPermit(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,\
			tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber,tr.CustomerId \
			from Transaction tr left join SMSAlert ep using (PaystationId,TicketNumber,PurchasedDate) \
			where tr.PaystationId=%s and tr.PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall()

def getEMS6TransactionForPayment(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		#Rewrite the query to lookup TransactionProcessorId for every transaction that comes up, make sure there is a single Processor transaction id returned (SubQuery) as there could be multiple processor transaction record for a single transaction, we need to find a unique processor transaction record by looking up the max(procssing date) in processor transaction table
		# Ashok Aug 12 Jira 4100. Donot consider ProcessorTransaction.IsApproved=1. Removed this condition "Approved=1 and" replaced pt.Amount to t.Cardpaid
		#Remodified the query on Aug 20 Jira 4175
		self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,\
			pt.MerchantAccountId,t.Cardpaid,pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,\
			pt.Approved,t.SmartCardData,t.SmartCardPaid,t.CustomCardData,t.CustomCardType,t.CustomerId,pt.ProcessingDate \
			from Transaction t left join ProcessorTransaction pt using (PaystationId,PurchasedDate,TicketNumber) \
			where t.PaystationId=%s and t.PurchasedDate between %s and %s and t.PaymentTypeId<>1 \
			and pt.ProcessingDate = (SELECT max(ProcessingDate) from ProcessorTransaction where \
			PaystationId= t.PaystationId  and 	PurchasedDate =t.PurchasedDate \
			and TicketNumber=t.TicketNumber)",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall() 
		
def getDistinctPaystationFromAudit(self):
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from Audit where paystationid > 49872")
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Audit")
		return self.__EMS6Cursor.fetchall()
		

def getDistinctPaystationIdForTaxes(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Taxes")
		return self.__EMS6Cursor.fetchall()

EMS7 -->
self.__EMS7Cursor.execute("insert ignore into Tax(CustomerId,Name,LastModifiedGMT) values(%s,%s,%s)",(EMS7CustomerId,TaxName,LastModifiedGMT))
self.__EMS7Cursor.execute("insert ignore into PurchaseTax(PurchaseId,TaxId,TaxRate,TaxAmount) values(%s,%s,%s,%s)",(PurchaseId,EMS7TaxId,TaxRate,TaxValue))
						print "Record Inserted in PurchaseTax"
						EMS7PurchaseTaxId = self.__EMS7Cursor.lastrowid
						self.__EMS7Cursor.execute("insert ignore into PurchaseTaxMapping(EMS6Id,EMS7Id) values(%s,%s)",(Id,EMS7PurchaseTaxId))
						self.__EMS7Connection.commit()

INDEX
__getEMS7CustomerId(EMS6CustomerId)
__getPointOfSaleId(EMS6PaystationId)
__getLocationId(EMS6RegionId)
***__getLocationIdByCustomer(EMS7CustomerId) NOT REQUIRED
***__getUnifiedRateId(EMS6PaystationId,PurchaseGMT,TicketNumber,EMS7CustomerId,RateName) NOT REQUIRED
__getEMS7CouponId
__getEMS7MerchantAccountId
__getLicencePlateId
__getPurchaseId (Already exists)
