-- Execute from Command Prompt
--
-- QA Admin Test Accounts
--
INSERT INTO UserAccount
(
	CustomerId,
    UserStatusTypeId,
    CustomerEmailId,
    UserName,
    FirstName,
    LastName,
    Password,
    IsPasswordTemporary,
    VERSION,
    LastModifiedGMT,
    LastModifiedByUserId,
    PasswordSalt)
SELECT
    ID as CustomerId,
    1 as UserStatusTypeId,
    null as CustomerEmailId, 
    concat('admin%40', replace(Name," ", "+") ) as UserName, 
    'QA' as FirstName, 
    'Admin' as LastName,
    '58ae83e00383273590f96b385ddd700802c3f07d' as Password,
    1 as IsPasswordTemporary,
    1 as VERSION,
    UTC_TIMESTAMP() as LastModifiedGMT,
    1 as LastModifiedByUserId,
    'SaltSaltSaltSalt' as PasswordSalt
FROM Customer WHERE Id >1;

INSERT INTO UserRole
(
UserAccountId,
RoleId,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    ID as UserAccountId, 
    3 as RoleId,  
    1 as VERSION,
    UTC_TIMESTAMP() as LastModifiedGMT,
    1 as LastModifiedByUserId
from UserAccount where FirstName='QA' and LastName='Admin';

INSERT INTO UserDefaultDashboard
(
UserAccountId,
IsDefaultDashboard,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    id as UserAccountId, 
    1 as IsDefaultDashboard, 
    0 as VERSION, 
    UTC_Timestamp() as LastModifiedGMT, 
    1 as LastModifiedByUserId 
FROM UserAccount where FirstName='QA' and LastName='Admin';

-- Create System Account
INSERT INTO UserAccount
    (
    CustomerId,
    UserStatusTypeId,
    CustomerEmailId,
    UserName,
    FirstName,
    LastName,
    Password,
    IsPasswordTemporary,
    VERSION,
    LastModifiedGMT,
    LastModifiedByUserId,
    PasswordSalt)
VALUES
    (
    1,
    1,
    null,
    'qa%40SystemAdmin',
    'QA',
    'Admin',
    '58ae83e00383273590f96b385ddd700802c3f07d',
    1,
    0,
    UTC_TIMESTAMP(),
    1,
    'SaltSaltSaltSalt'
    );

INSERT INTO UserRole
(
UserAccountId,
RoleId,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    ID as UserAccountId, 
    1 as RoleId,  
    1 as VERSION,
    UTC_TIMESTAMP() as LastModifiedGMT,
    1 as LastModifiedByUserId
from UserAccount where FirstName='QA' and LastName='Admin' and CustomerId = 1;

INSERT INTO UserDefaultDashboard
(
UserAccountId,
IsDefaultDashboard,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
SELECT 
    id as UserAccountId, 
    1 as IsDefaultDashboard, 
    0 as VERSION, 
    UTC_Timestamp() as LastModifiedGMT, 
    1 as LastModifiedByUserId 
FROM UserAccount where FirstName='QA' and LastName='Admin' and CustomerId = 1;

INSERT INTO CustomerRole
(CustomerId,
RoleId,
VERSION,
LastModifiedGMT,
LastModifiedByUserId)
(
SELECT 
Id,
3,
1,
UTC_Timestamp(),
1 From Customer
where Id not in (select distinct CustomerId from CustomerRole ));



-- PROD Script

Update UserAccount set Password = '58ae83e00383273590f96b385ddd700802c3f07d',
PasswordSalt = 'SaltSaltSaltSalt',
IsPasswordTemporary = 1
where Id > 1;

