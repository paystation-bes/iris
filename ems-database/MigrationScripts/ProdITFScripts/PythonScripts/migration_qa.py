#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert

ClusterId = 1;
try:

	
	EMS6Connection = mdb.connect('10.1.2.202', 'apamu', 'apamu', 'ems_db'); 
	EMS6ConnectionCerberus = mdb.connect('10.1.2.202', 'apamu', 'apamu','cerberus_db');
	EMS7Connection = mdb.connect('10.1.2.104', 'migration','migration','EMS7PreTestAug28');
	

	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccessInsert(EMS7Connection, 1), 0)

#1	

	controller.logModuleStartStatus(ClusterId,'Customers') 
	controller.migrateCustomers() #Done
	controller.logModuleEndStatus(ClusterId,'Customers') 
	
#2	
	controller.logModuleStartStatus(ClusterId,'TrialCustomer') 
	controller.migrateTrialCustomer() #Done
	controller.logModuleEndStatus(ClusterId,'TrialCustomer') 

#2.1
	controller.logModuleStartStatus(ClusterId,'UpdateCustExpiry') 
	controller.migrateCustomerExpiry() #Done
	controller.logModuleEndStatus(ClusterId,'UpdateCustExpiry') 


#3

	controller.logModuleStartStatus(ClusterId,'Locations') 
	controller.migrateLocations() #Done
	controller.logModuleEndStatus(ClusterId,'Locations') 

# 	Commented code for UnassignedLocation here. Handled in code
#4

	controller.logModuleStartStatus(ClusterId,'LotSettings') 	
	controller.migrateLotSettings() #Done
	controller.logModuleEndStatus(ClusterId,'LotSettings') 
	
# Added on Aug 22	
	controller.logModuleStartStatus(ClusterId,'LotSettingContent') 	
	controller.migrateLotSettingContent() ##Pending validation for incremental migration #Ashok: NOW
	controller.logModuleEndStatus(ClusterId,'LotSettingContent') 	

#5

	controller.logModuleStartStatus(ClusterId,'ParentRegionIdToLocation') 	
	controller.migrateParentRegionIdToLocation() #Done
	controller.logModuleEndStatus(ClusterId,'ParentRegionIdToLocation') 

#25	Copied from below. This should be before paystation as Merchant Account is necessary for MerchantPOS

	controller.logModuleStartStatus(ClusterId,'MerchantAccount') 		
	controller.migrateMerchantAccount()	#Done (InElsePart)
	controller.logModuleEndStatus(ClusterId,'MerchantAccount') 	


#6

	controller.logModuleStartStatus(ClusterId,'Paystations') 	
	controller.migratePaystations() # PointOfSale table is Migrated from within Paystation table migration  #Done
	controller.logModuleEndStatus(ClusterId,'Paystations') 

#7

	controller.logModuleStartStatus(ClusterId,'Coupons') 	
	controller.migrateCoupons() #Done
	controller.logModuleEndStatus(ClusterId,'Coupons') 

#8

	controller.logModuleStartStatus(ClusterId,'RestAccount') 	
	controller.migrateRestAccount() #Done
	controller.logModuleEndStatus(ClusterId,'RestAccount') 

#9

	controller.logModuleStartStatus(ClusterId,'ModemSettings') 	
	controller.migrateModemSettings()   #Done
	controller.logModuleEndStatus(ClusterId,'ModemSettings') 

#10

	controller.logModuleStartStatus(ClusterId,'EMSRateIntoUnifiedRate') 	
	controller.migrateEMSRateIntoUnifiedRate()  #Done
	controller.logModuleEndStatus(ClusterId,'EMSRateIntoUnifiedRate') 


#11

	controller.logModuleStartStatus(ClusterId,'CryptoKey') 	
	controller.migrateCryptoKey()	#Done
	controller.logModuleEndStatus(ClusterId,'CryptoKey') 

#12

	controller.logModuleStartStatus(ClusterId,'Rates') 	
	controller.migrateRates() #Done
	controller.logModuleEndStatus(ClusterId,'Rates') 

#13
 



	print "Completed AshokTest Migration"







   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
