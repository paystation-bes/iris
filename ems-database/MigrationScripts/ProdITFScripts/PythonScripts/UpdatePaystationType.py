import re
import MySQLdb as mdb
import sys
import time
from datetime import date
class test:
	def __getMapEMS7PaystationType(SerialNumber, PaystationType):
		EMS7PaystationTypeId=0
		length=len(str(SerialNumber))
		if (length==8 and PaystationType==0):
#		    print SerialNumber starts with 0 and has 8 characters are intellapay machine. EMS7.PaystationType.Id=255"
		    EMS7PaystationTypeId=255
		elif(re.search(r"verrus", str(SerialNumber))):
		    EMS7PaystationTypeId=10
#		    print "Verrus Paystation" 
		elif(re.search(r"^1", str(SerialNumber))): 
#		    print "LUKE IV machine"
		    EMS7PaystationTypeId=1
		elif(re.search(r"^2", str(SerialNumber))):	
#		    print "Shelby machine"
		    EMS7PaystationTypeId=2
		elif(re.search(r"^3", str(SerialNumber))):	
#		    print "Radius Luke"
		    EMS7PaystationTypeId=3
		elif(re.search(r"^5", str(SerialNumber))):
#		    print "LUKE II machine"
		    EMS7PaystationTypeId=5
		elif(re.search(r"^8", str(SerialNumber))):
#		    print "Test/Demo machine"
		    EMS7PaystationTypeId=8
		elif(re.search(r"^9", str(SerialNumber))):
#		    print "virtual machine"
		    EMS7PaystationTypeId=9
		else:
#		    print "The Paystation did not match any of the above criteria, it is being assigned as other"
		    EMS7PaystationTypeId=0
#		print "The Type Of Paystation is %s" %EMS7PaystationTypeId
		return EMS7PaystationTypeId
	
	EMS7Connection = mdb.connect('localhost', 'root','root123','ems7_ashok');
	EMS7Cursor = EMS7Connection.cursor()
	
	EMS7Cursor.execute("select Id, SerialNumber from Paystation")
	SerialNumbers = EMS7Cursor.fetchall()
	
	for SerialNumber in SerialNumbers:
		EMS7PaystationTypeId = __getMapEMS7PaystationType(SerialNumber[1],1)
		EMS7Cursor.execute("UPDATE Paystation set PaystationTypeId = %s, LastModifiedGMT = UTC_TIMESTAMP() WHERE Id = %s", (EMS7PaystationTypeId,SerialNumber[0]))
		EMS7Connection.commit()
	print "Completed"