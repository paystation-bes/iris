-- -----------------------------------------------------
-- Table `AuditMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `AuditMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6PaystationId` INT NULL DEFAULT NULL ,
  `EMS6CollectionTypeId` TINYINT NULL DEFAULT NULL ,
  `EndDate` DATETIME NULL DEFAULT NULL ,
  `StartDate` DATETIME NULL DEFAULT NULL ,
  `EMS7POSCollectionId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;

CREATE  TABLE IF NOT EXISTS `CustomerCardTypeMapping` (
  `EMS6Id` INT NULL DEFAULT NULL ,
  `EMS7Id` MEDIUMINT NULL DEFAULT NULL ,
  `ModifiedDate` DATETIME, 
   PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CryptoKeyMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CryptoKeyMapping` (
  `Type` INT NULL DEFAULT NULL ,
  `KeyIndex` INT NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  `DateAdded` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Type`,`KeyIndex`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `CustomerWsCalMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CustomerWebServiceCalMapping` (
  `EMS6Id` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerWsTokenMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CustomerWsTokenMapping` (
  `EMS6Id` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMSExtensiblePermitMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExtensiblePermitMapping` (
  `EMS6MobileNumber` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`EMS6MobileNumber`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMSParkingPermissionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EMSParkingPermissionMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6ParkingPermission` INT NULL DEFAULT NULL ,
  `EMS7ParkingPermission` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMSRateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EMSRateMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6RateId` INT NULL DEFAULT NULL ,
  `EMS7RateId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EventLogNewMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EventLogNewMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` MEDIUMINT NULL DEFAULT NULL ,
  `DeviceId` TINYINT NULL DEFAULT NULL ,
  `TypeId` INT NULL DEFAULT NULL ,
  `ActionId` INT NULL DEFAULT NULL ,
  `DateField` DATETIME NULL DEFAULT NULL ,
  `POSAlertId` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModemSettingMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ModemSettingMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6Id` INT NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestSessionTokenMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RestSessionTokenMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6AccountName` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReplenishMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ReplenishMapping` (
  `PaystationId` MEDIUMINT UNSIGNED,
  `Date` DATETIME ,
  `Number` INT UNSIGNED,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`Date`,`Number` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReversalMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ReversalMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `MerchantAccountId` MEDIUMINT NULL DEFAULT NULL ,
  `OriginalReferenceNumber` VARCHAR(30) NULL DEFAULT NULL ,
  `OriginalTime` VARCHAR(30) NULL DEFAULT NULL ,
  `EMS7Id` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SMSAlertMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SMSAlertMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `MobileNumber` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PurchaseTaxMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `PurchaseTaxMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `EMS6Id` INT NULL DEFAULT NULL ,
  `EMS7Id` MEDIUMINT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TransactionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TransactionMapping` (
  `Id` INT NOT NULL ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `EMS7PurchaseId` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CustomerMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CustomerMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS7CustomerId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `EMS6CustomerId` INT(11) NOT NULL ,
  `EMS7CustomerName` VARCHAR(40) NOT NULL ,
  `EMS6CustomerName` VARCHAR(40) NOT NULL ,
  `ModifiedDate` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PaystationMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `PaystationMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS7PaystationId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `EMS6PaystationId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `EMS6CustomerId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `SerialNumber` VARCHAR(20) NOT NULL ,
  `Name` VARCHAR(40) NOT NULL DEFAULT '' ,
  `Version` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `ProvisionDate` DATETIME NULL DEFAULT NULL ,
  `ModifiedDate` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  `RegionId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `LotSettingId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `RegionLocationMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RegionLocationMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS7LocationId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `EMS6RegionId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NOT NULL ,
  `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



CREATE  TABLE IF NOT EXISTS `SettingsFileContentMapping` (
  `EMS6Id` INT(11) NOT NULL ,
  `EMS7Id` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- -----------------------------------------------------
-- Table `SettingMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SettingsFileMapping` (
  `EMS6Id` INT(11) NOT NULL ,
  `EMS7Id` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ParkingPermissionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ParkingPermissionMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6ParkingPermissionId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `EMS7ParkingPemissionId` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `Name` VARCHAR(16) NOT NULL DEFAULT '' ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `MerchantAccountMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `MerchantAccountMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6MerchantAccountId` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `EMS7MerchantAccountId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `ProcessorName` VARCHAR(30) NOT NULL DEFAULT '' ,
  `EMS6CustomerId` INT(11) UNSIGNED NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PreAuthMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `PreAuthMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6MerchantAccountId` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `EMS7MerchantAccountId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `AuthorizationNumber` VARCHAR(30) NOT NULL DEFAULT '' ,
  `ProcessorTransactionId` VARCHAR(30) NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PreAuthHoldingMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `PreAuthHoldingMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6MerchantAccountId` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `EMS7MerchantAccountId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `AuthorizationNumber` VARCHAR(30) NOT NULL DEFAULT '' ,
  `ProcessorTransactionId` VARCHAR(30) NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ExtensibleRateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExtensibleRateMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6RateId` bigint(20) UNSIGNED NULL DEFAULT NULL ,
  `EMS7RateId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `EMS6RateTypeId` VARCHAR(30) NOT NULL DEFAULT '' ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `DiscardedRecords`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DiscardedRecords` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `MultiKeyId` VARCHAR(255) NULL DEFAULT '0' ,
  `EMS6Id` INT(15) NULL DEFAULT '0' ,
  `TableName` VARCHAR(25) NOT NULL ,
  `Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS6AlertType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EMS6AlertType` (
  `Id` INT(3) UNSIGNED NOT NULL ,
  `Name` VARCHAR(50) NOT NULL DEFAULT '' ,
  `DisplayName` VARCHAR(50) NOT NULL DEFAULT '' ,
  `IsUserDefined` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- -----------------------------------------------------
-- Table `CustomerAlertTypeMapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CustomerAlertTypeMapping` (
  `EMS6Id` INT(10) NOT NULL,
  `EMS7Id` INT(10) NOT NULL,
  `ModifiedDate` DATETIME,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- -----------------------------------------------------
-- Table `UserAccountMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `UserAccountMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6UseraccountId` INT(11) NOT NULL ,
  `EMS7UserAccountId` MEDIUMINT(8) NOT NULL ,
  `EMS6Name` VARCHAR(40) NULL DEFAULT NULL ,
  `EMS6CustomerId` INT(11) NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ServiceAgreementMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ServiceAgreementMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6ServiceAgreementId` INT(11) NOT NULL ,
  `EMS7ServiceAgreementId` MEDIUMINT(8) NOT NULL ,
  `EMS6UploadedGMT` DATETIME NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ThirdPartyServiceAccountMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ThirdPartyServiceAccountMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6ThirdPartyServiceAccountId` INT(11) NOT NULL ,
  `EMS7ThirdPartyServiceAccountId` MEDIUMINT(8) NOT NULL ,
  `EMS6UserName` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PaystationGroupRouteMapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PaystationGroupRouteMapping` (
  `PaystationGroupId` int(11) DEFAULT NULL,
  `EMS7Id` mediumint(8) DEFAULT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaystationGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `RoutePOSMapping` (
  `EMS7Id` mediumint(8),
  `PaystationGroupId` mediumint(8) NOT NULL,
  `PaystationId` mediumint(8) NOT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (PaystationGroupId,PaystationId)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=latin1;

-- -----------------------------------------------------
-- Table `CardRetryTransactionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CardRetryTransactionMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` MEDIUMINT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `EMS7CardRetryTransactionId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RegionPaystationLogMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RegionPaystationLogMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6RegionPaystationId` MEDIUMINT NULL DEFAULT NULL ,
  `EMS7LocationPOSLogId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ServiceStateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ServiceStateMapping` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `EMS7POSServiceStateId` INT NULL DEFAULT NULL ,
  `MachineNumber` VARCHAR(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestAccountMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RestAccountMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `EMS6AccountName` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7RestAccountId` MEDIUMINT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RateToUnifiedRateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RateToUnifiedRateMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `EMS7UnifiedRateId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProcessorPropertiesMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ProcessorPropertiesMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `Processor` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuditPOSCollectionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `AuditPOSCollectionMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `CollectionTypeId` TINYINT NULL DEFAULT NULL ,
  `StartDate` DATETIME NULL DEFAULT NULL ,
  `EndDate` DATETIME NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RawSensorDataMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RawSensorDataMapping` (
  `Id` INT NOT NULL ,
  `ServerName` VARCHAR(20) NULL DEFAULT NULL ,
  `PaystationCommAddress` VARCHAR(20) NULL DEFAULT NULL ,
  `Type` VARCHAR(45) NULL DEFAULT NULL ,
  `Action` VARCHAR(45) NULL DEFAULT NULL ,
  `Information` VARCHAR(45) NULL DEFAULT NULL ,
  `DateField` DATETIME NULL DEFAULT NULL ,
  `EMS7RawSensorDataId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ReversalArchiveMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ReversalArchiveMapping` (
  `Id` INT NOT NULL ,
  `MerchantAccountId` INT NULL DEFAULT NULL ,
  `OriginalReferenceNumber` VARCHAR(30) NULL DEFAULT NULL ,
  `OriginalTime` VARCHAR(30) NULL DEFAULT NULL ,
  `EMS7ReversalArchiveId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SMSTransactionLogMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SMSTransactionLogMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` VARCHAR(45) NULL DEFAULT NULL ,
  `PurchasedDate` VARCHAR(45) NULL DEFAULT NULL ,
  `TicketNumber` VARCHAR(45) NULL DEFAULT NULL ,
  `SMSMessageTypeId` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7TransactionLogId` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Transaction2PushMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `Transaction2PushMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `ThirdPartyServiceAccountId` INT NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TransactionPushMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TransactionPushMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` VARCHAR(45) NULL DEFAULT NULL ,
  `PurchasedDate` VARCHAR(45) NULL DEFAULT NULL ,
  `TicketNumber` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WsCallLogMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WsCallLogMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `Token` VARCHAR(45) NULL DEFAULT NULL ,
  `CallDate` DATETIME NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RestLogMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RestLogMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `RESTAccountName` VARCHAR(45) NULL DEFAULT NULL ,
  `EndpointName` VARCHAR(45) NULL DEFAULT NULL ,
  `LoggingDate` VARCHAR(45) NULL DEFAULT NULL ,
  `IsError` VARCHAR(45) NULL DEFAULT NULL ,
  `CustomerId` VARCHAR(45) NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProcessorTransactionMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ProcessorTransactionMapping` (
  `PaystationId` INT NOT NULL ,
  `PurchasedDate` DATETIME NOT NULL ,
  `TicketNumber` INT NOT NULL ,
  `Approved` TINYINT NOT NULL ,
  `TypeId` TINYINT NOT NULL ,
  `ProcessingDate` DATETIME NOT NULL ,
  `EMS7ProcessorTransactionId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`PaystationId`, `PurchasedDate`, `TicketNumber`, `Approved`, `TypeId`, `ProcessingDate`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CouponMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CouponMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `EMS6CouponId` VARCHAR(20) NULL ,
  `EMS7CouponId` INT NULL ,
  `NumUses` VARCHAR(16) NULL ,
  `EMS6CustomerId` MEDIUMINT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `RateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RateMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `CustomerId` INT NULL ,
  `Name` VARCHAR(45) NULL ,
  `EMS7UnifiedRateId` INT NULL ,
  `PaystationId` INT NULL ,
  `TicketNumber` INT NULL ,
  `PurchasedDate` DATETIME NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7ExtensibleRateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EMS7ExtensibleRateMapping` (
  `Id` INT NOT NULL ,
  `EMS6RateId` BIGINT(20) NULL ,
  `EMS7RateId` INT(11) NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `RestLogTotalCallMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RestLogTotalCallMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `RESTAccountName` VARCHAR(45) NULL DEFAULT NULL ,
  `LoggingDate` DATETIME NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SMSFailedResponseMapping`
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS `SMSFailedResponseMapping` (
  `PaystationId` INT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `SMSMessageTypeId` INT NULL DEFAULT NULL ,
  `EMS7SMSFailedResponseId` INT NULL DEFAULT NULL ,
  PRIMARY KEY `idx_SMSFailedRM_multi_uq` (`PaystationId`,`PurchasedDate`,`TicketNumber`,`SMSMessageTypeId`)
  )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `CCFailLogMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `CCFailLogMapping` (
  `EMS6Id` INT NULL ,
  `EMS7Id` INT NULL ,
  ModifiedDate DATETIME 
)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `EMSRateToUnifiedRateMapping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `EMSRateToUnifiedRateMapping` (
  `EMS6RateId` INT NULL ,
  `EMS7RateId` INT NULL ,
  ModifiedDate DATETIME NULL
)
ENGINE = InnoDB;