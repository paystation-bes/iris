##----------------------------------------------------------------------------------------------------------------------#
##	Class		: EMS7DataAccess.										#									    	
##	Purpose		: This class Transforms, and Loads the data into EMS7 Database.					# 
##	Important	: The calls to the procedure in the class are implicitly made by EMS6InitialMigration.py,	#
##			  and EMS6IncrementalMigration.py.								#
##			  The modules in the class are also implictly made by EMS6IncrementalMigration.py.		#
##	Author		: Vijay Ramberg.										#
##----------------------------------------------------------------------------------------------------------------------#

from datetime import date
from datetime import datetime
import MySQLdb as mdb
import time
import re
import urllib

class EMS7DataAccess:
	def __init__(self, EMS7Connection, verbose):
		self.__verbose = verbose
		self.__EMS7Connection = EMS7Connection
		self.__EMS7Cursor = EMS7Connection.cursor()

		self.__customerIdCache = {}
		self.__locationIdCache = {}
		self.__paystationIdCache = {}
		self.__plateNumberIdCache = {}
		self.__merchantIdCache = {}
		self.__extensiblerateIdCache = {}
		self.__mobilenumberIdCache = {}
		self.__lotsettingIdCache = {}
		self.__locationbycustomerIdCache = {}
		self.__carriernameIdCache = {}
		self.__modemtypeIdCache = {}
		self.__apnIdCache = {}
		self.__restaccountnameIdCache = {}
		self.__pointofsalebycustomerIdCache = {}
		self.__paystationsettingkeyIdCache = {}
		self.__parkingpermissiondayofweek = {}
		self.__couponIdCache = {}
		self.__processornameIdCache = {}
		self.__eventiseverityIdCache = {}
		self.__pointofsaleIdCache = {}
		self.__purchaseIdCache = {}
		self.__mobilenumberCache = {}
		self.__paystationGroupIdCache = {}
		self.__processortransactionIdCache = {}
		self.__customerIdfromposIdCache = {}

	def addCustomer(self, customer, Action):
		
		print "..In EMS7DataAccess class --addCustomer method....."
		
		self.__printCustomer(customer)
		ParentLocationId = None
		PaystationSettingName='None'
		UnifiedRateName='Unknown'
		AlertTypeId=12 #AlertTypeId=12 is defined as PaystationAlert in EMS7
		Threshold = 1
		IsParent = 0 
		CustomerTypeId = customer[0]
		CustomerAccountStatus = customer[1]
		Name = customer[2]
		Version = customer[3]
		LastModifiedDate = customer[4]
		LastModifiedByUser = customer[5]
		CustomerId = customer[6]
		if(Action=='Insert'):
			try:
				print " ---Inserting record in EMS7 Customer table..."
				#self.__EMS7Cursor.execute("set foreign_key_checks=0")
				self.__EMS7Cursor.execute("insert into Customer(CustomerTypeId,CustomerStatusTypeId,Name,\
						VERSION,IsParent,LastModifiedGMT,LastModifiedByUserId) \
						values(%s,%s,%s,%s,%s,%s,%s)",(CustomerTypeId,CustomerAccountStatus,\
        		Name,Version,IsParent, LastModifiedDate,LastModifiedByUser))
				EMS7CustomerId = self.__EMS7Cursor.lastrowid
				print " ---Inserting record in EMS7 CustomerMapping table..."
				self.__EMS7Cursor.execute("insert ignore into CustomerMapping ( \
							EMS7CustomerId,EMS6CustomerId,EMS7CustomerName,\
							EMS6CustomerName) values(%s,%s,%s,%s)",\
						 (EMS7CustomerId,CustomerId,Name,Name))
				# Add a PaystationAlert record for each Customer in CustomerAlertType table
				print " ---Inserting record in EMS7 CustomerAlertType table..."
				self.__EMS7Cursor.execute("insert into CustomerAlertType(CustomerId,AlertThresholdTypeId,Name,Threshold,\
							LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
							(EMS7CustomerId,AlertTypeId,'Pay Station Alert',Threshold,LastModifiedDate,LastModifiedByUser))
				
				EMS7CustomerAlertTypeId=self.__EMS7Cursor.lastrowid
				
				print " EMS curomerAlertType id is", EMS7CustomerAlertTypeId
			
				print " ---Inserting record in EMS7 CustomerAlertTypeMapping table..."
			
				self.__EMS7Cursor.execute("insert ignore into CustomerAlertTypeMapping(EMS7Id,EMS6Id,ModifiedDate) \
							values(%s,%s,%s)",(EMS7CustomerAlertTypeId,AlertTypeId,LastModifiedDate))
			
				print " ---Inserting record in EMS7 PaystationSetting table..."
			
				self.__EMS7Cursor.execute("insert into PaystationSetting(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) \
							values(%s,%s,%s,%s)",(EMS7CustomerId,PaystationSettingName,\
							LastModifiedDate,LastModifiedByUser))
			
				print " ---Inserting record in EMS7 UnifiedRate table..."
			
				self.__EMS7Cursor.execute("insert into UnifiedRate(CustomerId,Name,LastModifiedGMT,\
							LastModifiedByUserId) values(%s,%s,%s,%s)",\
							(EMS7CustomerId,UnifiedRateName,LastModifiedDate,LastModifiedByUser))
			# Add a Credit Card, Value Cards and Smartcard record for every Customer.
			
				print " ---Inserting record in EMS7 CustomerCardType table..."
				
				self.__EMS7Cursor.execute("insert into CustomerCardType(CustomerId, CardTypeId, AuthorizationTypeId, \
							Name, Track2Pattern, Description, IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, \
							LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7CustomerId,1,0,'Credit Card','','Created by DPT. Locked. Cannot be changed.',0,1,0, \
							LastModifiedDate,LastModifiedByUser))
				self.__EMS7Cursor.execute("insert into CustomerCardType(CustomerId, CardTypeId, AuthorizationTypeId, \
							Name, Track2Pattern, Description, IsDigitAlgorithm, IsLocked, VERSION, \
							LastModifiedGMT, LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7CustomerId,2,0,'Smart Card','','Created by DPT. Locked. Cannot be changed.',0,1,0,\
							LastModifiedDate,LastModifiedByUser))
				self.__EMS7Cursor.execute("insert into CustomerCardType(CustomerId, CardTypeId, AuthorizationTypeId, \
							Name, Track2Pattern, Description, IsDigitAlgorithm, IsLocked, VERSION, \
							LastModifiedGMT, LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					        (EMS7CustomerId,3,0,'Value Card','','Created by DPT. Locked. Cannot be changed.',0,1,0,\
							LastModifiedDate,LastModifiedByUser))
			
				print " ---Inserting record in EMS7 Role table..."
			
				self.__EMS7Cursor.execute("insert into Role(CustomerId,CustomerTypeId,RoleStatusTypeId,Name,IsAdmin,IsLocked,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,3,1,'Child Admin',1,1,0,LastModifiedDate,LastModifiedByUser))
			# Disabling self referencing parent location id as we get an error while trying to insert a location into the location table.
				self.__EMS7Cursor.execute("set foreign_key_checks=0")
			
				print " ---Inserting record in EMS7 Location table..."
			
				self.__EMS7Cursor.execute("insert into Location(CustomerId,ParentLocationId,Name,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,ParentLocationId,'Unassigned',0,0,'NULL',IsParent,1,0,0,LastModifiedDate,1))

			# Adding DefaultCustomerSubscription for every Customer
			
				print " ---Inserting record in EMS7 CustomerSubscription table..."
				
		 		self.__addDefaultCustomerSubscription(EMS7CustomerId)
		 		self.__EMS7Connection.commit()
			
				print " ******* Finished migrating customer record to EMS7 Database *********"
			
			#Added the except conditon on June 24 to populate invalid records to DiscardedRecrords Table
			except:
	  			IsMultiKey=0
				tableName='Customer'
				self.__InsertIntoDiscardedRecords(CustomerId,tableName,IsMultiKey)
			

		if(Action=='Update'):
			EMS7CustomerId = self.__getEMS7CustomerId(CustomerId)	
			self.__EMS7Cursor.execute("update Customer set Name='%s', VERSION=%s, \
							CustomerStatusTypeId=%s where Id=%s" \
						%(Name,Version,CustomerAccountStatus,EMS7CustomerId))
			self.__EMS7Connection.commit()

	def __addDefaultCustomerSubscription(self, EMS7CustomerId):
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
		VERSION=0
		IsEnabled=0
		count=100
		while (count<=1200):
			SubscriptionTypeId=count
			#Ashok July5 Code written to add Default SUbscriptions
			if(count!=400) or (count!=800):
				self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,\
					VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
					(EMS7CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId))	
				count=count+100
				self.__EMS7Connection.commit()
			else:
				self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,\
					VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
					(EMS7CustomerId,SubscriptionTypeId,1,VERSION,LastModifiedGMT,LastModifiedByUserId))	
				count=count+100
				self.__EMS7Connection.commit()
	
	def addTrialCustomerToEMS7(self, Customer, Action):
		self.__printTrialCustomer(Customer)
		CustomerStatusTypeId=2
		EMS6CustomerId=Customer[0]
		Status=Customer[1]
		Expiry=Customer[2]
		EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("update Customer set TrialExpiryGMT=%s, CustomerStatusTypeId=%s where Id=%s  ",\
						(Expiry,CustomerStatusTypeId,EMS7CustomerId))
		self.__EMS7Connection.commit()
		
	def __printTrialCustomer(self, Customer):
		if(self.__verbose):
			print " -- Customer is %s" %Customer[0]
			print " -- Status %s" %Customer[1]
			print " -- Expiry %s" %Customer[2]
			
	def __printCustomer(self, customer):
		if (self.__verbose):
			print " -- CustomerTypeId %s" %customer[0]
			print " -- CustomerAccountStatus %s" %customer[1]
			print " -- Name %s" %customer[2]
			print " -- Version %s" %customer[3]
			print " -- LastModifiedDate %s" %customer[4]
			print " -- LastModifiedByUser %s" %customer[5]

	def addCustomerPropertyToEMS7(self, CustomerProperty, Action):
		print "adding customer property in table"
		tableName='CustomerProperty'
		MaxUserAccounts = 99
		CCOfflineRetry = 3
		TimeZone = 'GMT' # Customer.TimzoneId=473 (which is GMT)
		SMSWarningPeriod = 10
		MaxOfflineRetry = 5
		Hidden_Paystations = 1 # This is the Customer Property Type = 7, a single record needs to be added.
		#Need to add the record for Property Type 2, and 7
		VERSION = 0
		LastModifiedGMT = date.today() 
                LastModifiedByUserId = 1
		self.__printCustomerProperties(CustomerProperty)
		EMS6CustomerId = CustomerProperty[0]
		MaxUserAccounts = CustomerProperty[1]
		CCOfflineRetry = CustomerProperty[2]
		MaxOfflineRetry = CustomerProperty[3]
		TimeZone = CustomerProperty[4]
		SMSWarningPeriod = CustomerProperty[5]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		print "EMS7 customer id for creating customer properties is", EMS7CustomerId
		#There are 5 customer property in ems7 that require a value, if it does not come 6 then we assign a default value only for the ones specified in paul stored, leave the rest
		if (EMS7CustomerId):
			if (MaxUserAccounts):
				if(Action=='Insert'):
					# "**** MaxUserAccount"
					CustomerPropertyTypeId = 3
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxUserAccounts,\
					VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				# Insert Default value.
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxUserAccounts,\
				VERSION,LastModifiedGMT,LastModifiedByUserId)

			if (MaxUserAccounts and Action=='Update'):
				CustomerPropertyTypeId = 3
                                self.__updateCustomerProperty(MaxUserAccounts,VERSION,LastModifiedGMT,\
				LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)
				self.__EMS7Connection.commit()

			if(CCOfflineRetry):
				if(Action=='Insert'):
					#  "**** CCOfflineRetry" 
					CustomerPropertyTypeId = 4
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,\
					CCOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				# Insert a Default Value.
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,CCOfflineRetry,\
				VERSION,LastModifiedGMT,LastModifiedByUserId)

			if(CCOfflineRetry and Action=='Update'):
                                CustomerPropertyTypeId = 4
                                self.__updateCustomerProperty(CCOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId,\
				EMS7CustomerId,CustomerPropertyTypeId)

			if(MaxOfflineRetry):
				if(Action=='Insert'): 
					# "**** MaxOfflineRetry" 
					CustomerPropertyTypeId = 5
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxOfflineRetry,\
					VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxOfflineRetry,\
				VERSION,LastModifiedGMT,LastModifiedByUserId)

			if(MaxOfflineRetry and Action=='Update'):
				CustomerPropertyTypeId = 5
				self.__updateCustomerProperty(MaxOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId,\
				EMS7CustomerId,CustomerPropertyTypeId)

			if(TimeZone):
				if(Action=='Insert'):
					# "**** TimeZone" 
					CustomerPropertyTypeId = 1
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,TimeZone,\
					VERSION,LastModifiedGMT,LastModifiedByUserId)
					self.__updateCustomerTimeZone(EMS7CustomerId,TimeZone)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,TimeZone,VERSION,\
				LastModifiedGMT,LastModifiedByUserId)

			if(TimeZone and Action=='Update'):
				CustomerPropertyTypeId = 1
				self.__updateCustomerProperty(TimeZone,VERSION,LastModifiedGMT,LastModifiedByUserId,\
				EMS7CustomerId,CustomerPropertyTypeId)

			if(SMSWarningPeriod):
				if(Action=='Insert'):
					# "**** SMSWarningPeriod" 
					CustomerPropertyTypeId = 6
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,\
					SMSWarningPeriod,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,SMSWarningPeriod,\
				VERSION,LastModifiedGMT,LastModifiedByUserId)
			
			if(SMSWarningPeriod and Action=='Update'):
				CustomerPropertyTypeId = 6
				# "**** SMSWarningPeriod"
				self.__updateCustomerProperty(SMSWarningPeriod,VERSION,LastModifiedGMT,\
				LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)
			# Calling a Stored Procedure to Populate Query Stalls BY the , the value for a record arrives from EMS6.Paystation.QueryStallsBY Attribute.

# This is the new property Type in EMS7
			CustomerPropertyTypeId = 7
			HiddenPaystationsReported = 1 
			self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,HiddenPaystationsReported,\
			VERSION,LastModifiedGMT,LastModifiedByUserId)
			self.__EMS7Connection.commit()

		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def __updateCustomerTimeZone(self,EMS7CustomerId,TimeZone):
		TimeZoneId=None
		self.__EMS7Cursor.execute("select Time_zone_id from mysql.time_zone_name where Name=%s",(TimeZone))
		TimeZoneId=self.__EMS7Cursor.fetchone()
		if(TimeZoneId and EMS7CustomerId):
			self.__EMS7Cursor.execute("update Customer set TimezoneId=%s where Id=%s",(TimeZoneId[0],EMS7CustomerId))
			self.__EMS7Connection.commit()
	
# Populate the Customer Property QueryStalls by with the values coming up from Paystation table
	def addQueryStallsBy(self,QueryStallsBy,EMS6CustomerId):
		QueryStallsBy = QueryStallsBy[0]
		PropertyValue = QueryStallsBy
		# The default QueryStallsBy will be defaulted to 2 if there are multiple values comming from Paystation table else, The value assigned to this property will be one coming up from Paystation.
		# select CustomerId, QueryStallsBy from Paystation group by CustomerId, QueryStallsBy order by CustomerId limit 10
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(EMS7CustomerId):
			CustomerPropertyTypeId = 2
			VERSION = 1
			LastModifiedGMT=date.today()
			LastModifiedByUserId=1
			self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId)
			print "Adding QueryStallsBy for Customer %s" %EMS7CustomerId
			self.__EMS7Connection.commit()

	def __batchInsertIntoDiscardedRecords(self,rows,IsMultiKey):
		if(IsMultiKey==0):
			self.__EMS7Cursor.executemany("insert into DiscardedRecords(EMS6Id,TableName,Date) \
							values(%s,%s,%s)", rows)
		else:
			self.__EMS7Cursor.executemany("insert into DiscardedRecords(MultiKeyId,TableName,Date) \
							values(%s,%s,%s)", rows)
		self.__EMS7Connection.commit()

	def __InsertIntoDiscardedRecords(self,EMS6Id,TableName,IsMultiKey):
		LastModifiedGMT=date.today()
		if(IsMultiKey==0):
			self.__EMS7Cursor.execute("insert into DiscardedRecords(EMS6Id,TableName,Date) \
							values(%s,%s,%s)",(EMS6Id,TableName,LastModifiedGMT))
			self.__EMS7Connection.commit()
		else:
			self.__EMS7Cursor.execute("insert into DiscardedRecords(MultiKeyId,TableName,Date) \
							values(%s,%s,%s)",(EMS6Id,TableName,LastModifiedGMT))

	def deleteCustomerPropertyFromEMS7(self, EMS6Id, Action):
		EMS7CustomerId=self.__getEMS7CustomerId(EMS6Id)
		self.__EMS7Cursor.execute("delete from CustomerProperty where CustomerId=%s",EMS7CustomerId)
		self.__EMS7Connection.commit()

	def addCustomerSubscriptionToEMS7(self, CustomerSubscription, Action):
		self.__printCustomerSubscription(CustomerSubscription)
		tableName='CustomerSubscription'
		IsEnabled = 1
                VERSION = 0
                LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
                EMS6CustomerId = CustomerSubscription[0]
                ServiceId = CustomerSubscription[1]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
                if (EMS7CustomerId):
			SubscriptionTypeId = self.__getSubscriptionTypeId(ServiceId)
			if(Action=='Insert'):
				if (SubscriptionTypeId):
					self.__CustomerSubscriptionEnable(EMS7CustomerId,SubscriptionTypeId,VERSION,LastModifiedGMT,LastModifiedByUserId)
			if(Action=='Update'):
				self.__EMS7Cursor.execute("update CustomerSubscription set SubscriptionTypeId=%s, IsEnabled=%s, VERSION=%s, \
								LastModifiedGMT=%s, LastModifiedByUserId=%s where CustomerId=%s and SubscriptionTypeId=%s",\
								(SubscriptionTypeId,IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId, EMS7CustomerId))
				self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def __printCustomerSubscription(self,CustomerSubscription):
		if(self.__verbose):
			print " -- EMS6CustomerId %s" %CustomerSubscription[0]
			print " -- ServiceId %s" %CustomerSubscription[1]

	def __getSubscriptionTypeId(self, ServiceId):
		if (ServiceId==1):
                        SubscriptionTypeId=100
                if (ServiceId==2):
                        SubscriptionTypeId=600
                if (ServiceId==3):
                        SubscriptionTypeId=200
                if(ServiceId==4):
                        SubscriptionTypeId=700
                if(ServiceId==5):
                        SubscriptionTypeId=300
                if(ServiceId==6):
                        SubscriptionTypeId=1000
                if(ServiceId==7):
                        SubscriptionTypeId=1200
                if(ServiceId==8):
                        SubscriptionTypeId=900
		return SubscriptionTypeId	

# Procedure to Enable Customer Subscription
	def __CustomerSubscriptionEnable(self,EMS7CustomerId,SubscriptionTypeId,VERSION,LastModifiedGMT,LastModifiedByUserId):
#		IsEnabled=1
		print "EMS7CustomerId %s" %EMS7CustomerId
		print "SubscriptionTypeId %s" %SubscriptionTypeId
		print " VERSION %s" %VERSION
		self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=1 where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))	
		self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=1 where CustomerId=%s and SubscriptionTypeId in (400,800)",(EMS7CustomerId))	
		
#		self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,VERSION,\
#						LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
#						(EMS7CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId))	
		self.__EMS7Connection.commit()

#Procedure to set the default value of disabled for the records that do not have a value, This procedure should always be called after the CustomerSubscription Module has been run, This module is being called in EMS6Migration.py(CustomerSubscription) Module right after the loop
#Update : The code has been disabled because it is being handled in Customer Section. The code is below is not being removed quiet yet until the new code is tested.
##	def CustomerSubscriptionDisable(self):
#		LastModifiedGMT = date.today()
 #               LastModifiedByUserId = 1
#		VERSION=0
#		IsEnabled=0
#		self.__EMS7Cursor.execute("select c.Id from CustomerSubscription cs right join \
#						Customer c on(cs.CustomerId=c.Id) where cs.CustomerID is null")
#		CustomerList=self.__EMS7Cursor.fetchall()
#		for Customer in CustomerList:
#			EMS7CustomerId=Customer[0]
#			count=100
#			while (count<=1200):
#				SubscriptionTypeId=count
#				self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,\
#					VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
#					(EMS7CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId))	
#				count=count+100
#				self.__EMS7Connection.commit() 
			
	def __getEMS7CustomerId(self, EMS6CustomerId):
		if (EMS6CustomerId in self.__customerIdCache):
			return self.__customerIdCache[EMS6CustomerId]

		EMS7CustomerId = None
		self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(EMS6CustomerId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerId = row[0]

		self.__customerIdCache[EMS6CustomerId] = EMS7CustomerId
		
		print " EMS7CustomerId %s" %EMS7CustomerId
		return EMS7CustomerId
	
 	def __getLocationId(self, RegionId):
		if (RegionId in self.__locationIdCache):
			return self.__locationIdCache[RegionId]

                #LocationId = None
                LocationId = None
		self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(RegionId))
		if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
			LocationId = row[0]

		self.__locationIdCache[RegionId] = LocationId
		return LocationId

	def __getLicencePlateId(self, PlateNumber):
		ModifiedDate = date.today()
		if (PlateNumber in self.__plateNumberIdCache):
			return self.__plateNumberIdCache[PlateNumber]
			
		LicencePlateId = None
		if (PlateNumber):
			self.__EMS7Cursor.execute("select Id from LicencePlate where Number=%s",(PlateNumber))
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				LicencePlateId = row[0]
			else:
				self.__EMS7Cursor.execute("insert into LicencePlate(Number,LastModifiedGMT) values(%s,%s)",(PlateNumber,ModifiedDate))
				row = self.__EMS7Cursor.lastrowid
				#Ashok: Commented the below line on July 10 and added the new line
				#LicencePlateId = row[0]
				LicencePlateId = self.__EMS7Cursor.lastrowid
		self.__plateNumberIdCache[PlateNumber] = LicencePlateId
		return LicencePlateId

	def __printCustomerProperties(self, CustomerProperty):
		if(self.__verbose):		
			print " -- CustomerId %s " %CustomerProperty[0]
			print " -- MaxUserAccounts %s " %CustomerProperty[1]
			print " -- CCOfflineRetry %s " %CustomerProperty[2]
			print " -- MaxOfflineRetry %s " %CustomerProperty[3]
			print " -- TimeZone %s " %CustomerProperty[4]
			print " -- SMSWarningPeriod %s " %CustomerProperty[5]

	def __insertCustomerProperty(self, EMS7CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId):
		# Ashok: Disabled FK checks
		self.__EMS7Cursor.execute("set foreign_key_checks=0")
		self.__EMS7Cursor.execute("insert ignore into CustomerProperty(CustomerId,CustomerPropertyTypeId,PropertyValue,\
						VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",\
						(EMS7CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __updateCustomerProperty(self, PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId):
		self.__EMS7Cursor.execute("update CustomerProperty set PropertyValue=%s,VERSION=%s,LastModifiedGMT=%s,\
						LastModifiedByUserId=%s where CustomerId=%s and CustomerPropertyTypeId=%s", \
						(PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId))
		self.__EMS7Connection.commit()

	def addLotSettingToEMS7(self, lotSetting, Action):
		self.__printLotSetting(lotSetting)
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		version = lotSetting[0]
		EMS6CustomerId=lotSetting[1]
		Name = lotSetting[2]
		UniqueId = lotSetting[3]
		PaystationType = lotSetting[4]
		ActivationDate = lotSetting[5]
		TimeZone = lotSetting[6]
		FileLocation = lotSetting[7]
		UploadDate = lotSetting[8]
		EMS6LotSettingId = lotSetting[9]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		print" ..in lotsetting process....."
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert ignore into SettingsFile(CustomerId,Name,UniqueIdentifier,UploadGMT,ActivationGMT,FileName,VERSION) values(%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,Name,UniqueId,UploadDate,ActivationDate,FileLocation,version))
			EMS7LotSettingId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into SettingsFileMapping(EMS6Id,EMS7Id,DateAdded) values(%s,%s,%s)",(EMS6LotSettingId,EMS7LotSettingId,LastModifiedGMT))
			if (FileLocation):
				#self.__EMS7Cursor.execute("insert into SettingsFileContent(SettingsFileId,Content) values(%s,%s)",(EMS7LotSettingId,FileLocation))
				self.__EMS7Cursor.execute("insert into SettingsFileContent(SettingsFileId,Content) SELECT Id, %s FROM SettingsFile WHERE CustomerId = %s AND Name = %s AND UploadGMT = %s", (FileLocation,EMS7CustomerId,Name,UploadDate))
				EMS7LotSettingContentId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert ignore into SettingsFileContentMapping(EMS6Id,EMS7Id,DateAdded) values(%s,%s,%s)",(EMS6LotSettingId,EMS7LotSettingContentId,date.today()))
				self.__EMS7Connection.commit()
			else:
				self.__EMS7Cursor.execute("insert into DiscardedRecords(EMS6Id,TableName,Date) values(%s,%s,%s)",(EMS6LotSettingId,'SettingsFileContent',LastModifiedGMT))
				self.__EMS7Connection.commit()
			#We are no longer adding a record into PaystationSetting table, This is temporarily being disabled.
			#This code was enabled back again after talking with Murat
			self.__EMS7Cursor.execute("insert ignore into PaystationSetting(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,Name,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			EMS7LotSettingId = self.__getEMS7LotSettingId(EMS6LotSettingId)
			self.__EMS7Cursor.execute("Update SettingsFile set CustomerId=%s,Name=%s,UniqueIdentifier=%s,UploadGMT=%s,ActivationGMT=%s,FileName=%s,VERSION=%s where Id=%s",(EMS7CustomerId,Name,UniqueId,UploadDate,ActivationDate,FileLocation,version,EMS7LotSettingId))
			
			"""check with ashok about this Added this update 17/7 ..sanjay"""
			#self.__EMS7Cursor.execute("Update PaystationSetting set Name=%s where CustomerId=%s",(Name,EMS7CustomerId))
			
			
			print "Last ro2wid is %s" %self.__EMS7Cursor.lastrowid
			self.__EMS7Connection.commit()

	def __printLotSetting(self, lotSetting):
		if (self.__verbose):
			print " -- Version %s" %lotSetting[0]
			print " -- CustomerId %s" %lotSetting[1]
			print " -- Name %s "%lotSetting[2]
			print " -- UniqueId %s " %lotSetting[3]
			print " -- PaystationType %s" %lotSetting[4]
			print " -- ActivationDate %s" %lotSetting[5]
			print " -- TimeZone %s " %lotSetting[6]
			print " -- FileLocation %s " %lotSetting[7]

	def addPaystationToEMS7(self, paystation, Action):
		tableName='Paystation'
		self.__printPaystation(paystation)
		IsDeleted = 0
		LastModifiedGMT = date.today()
		ModifiedDate = date.today()
		LastModifiedByUserId = 1
		LotSetting = None
		Location = None
		EMS6PaystationId = paystation[0]
		PaystationType = paystation[1]
		ModemTypeId = paystation[2]
		SerialNumber = paystation[3]
		Version = paystation[4]
		Name = paystation[5]
		ProvisionDate = paystation[6]
		EMS6CustomerId = paystation[7]
		RegionId = paystation[8]
		LotSetting = paystation[9]
		EMS7PaystationTypeId=self.__getMapEMS7PaystationType(SerialNumber,PaystationType)
		#Setting the IsDeleted Flag to 0 for the X'd out Paystations
		if(re.search(r"(?i)x", SerialNumber)):
		    IsDeleted=1
		if (Action=='Insert'):
			# Added this line below self.__EMS7Cursor.execute("set foreign_key_checks=0")
			# self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("insert into Paystation(PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,\
							LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
			EMS7PaystationId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			EMS7LotSettingId = self.__getEMS7LotSettingId(LotSetting)
			# Look up PaystationSettingId for that Customer where CustomerId is whatever whenever the value for EMS6.Paystation.LotSetting is 0, or NULL. make sure there is an ID for each Paystatiomn for a Lotsetting  Atttribute. This is actually LotSettingId Pointing to SettingFile table.
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
			EMS7LocationId = self.__getLocationId(RegionId)
			self.__EMS7Cursor.execute("insert into PaystationMapping(EMS7PaystationId,EMS6PaystationId,EMS6CustomerId,SerialNumber,Name,\
							ProvisionDate,ModifiedDate,Version,RegionId,LotSettingId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7PaystationId,EMS6PaystationId,EMS6CustomerId,SerialNumber,Name,ProvisionDate,ModifiedDate,Version,RegionId,LotSetting))
			self.__EMS7Connection.commit()
			
			print " before inserting in POS", EMS7CustomerId, EMS7LocationId
			
			if (EMS7CustomerId and EMS7LocationId):
				# Changes recently made to this table, changed the PaystationSettingId to LotSettingId, The value arrives from LotSetting's table
				self.__EMS7Cursor.execute("insert into PointOfSale(CustomerId,PaystationId,SettingsFileId,LocationId,Name,SerialNumber,\
					ProvisionedGMT,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					(EMS7CustomerId,EMS7PaystationId,EMS7LotSettingId,EMS7LocationId,Name,SerialNumber,ProvisionDate,\
					Version,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6CustomerId,'PointOfSale',IsMultiKey)
		if(Action == 'Update'):
			EMS7PaystationId = self.__getEMS7PaystationId(EMS6PaystationId)
			EMS7LocationId = self.__getLocationId(RegionId)
			print " EMS7PaystationId -- %s" %EMS7PaystationId
			self.__EMS7Cursor.execute("update Paystation set PaystationTypeId=%s, ModemTypeId=%s,SerialNumber=%s,IsDeleted=%s,VERSION=%s,\
				LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(PaystationType,ModemTypeId,SerialNumber,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,EMS7PaystationId))
			
			"""Added updates 17/7/2013 -- Sanjay.."""
			
			self.__EMS7Cursor.execute("update PaystationMapping set SerialNumber=%s,VERSION=%s, Name=%s, RegionId=%s,ProvisionDate=%s, LotSettingId=%s where EMS7PaystationId=%s",(SerialNumber,Version, Name,RegionId,ProvisionDate, LotSetting,EMS7PaystationId))
			self.__EMS7Cursor.execute("update PointOfSale set SerialNumber=%s,VERSION=%s, Name=%s, ProvisionedGMT=%s, LocationId=%s where PaystationId=%s",(SerialNumber,Version, Name,ProvisionDate,EMS7LocationId,EMS7PaystationId))
			
			self.__EMS7Connection.commit()

	def __getMapEMS7PaystationType(self, SerialNumber, PaystationType):
		EMS7PaystationTypeId=0
		length=len(str(SerialNumber))
		if (length==8 and PaystationType==0):
#		    print SerialNumber starts with 0 and has 8 characters are intellapay machine. EMS7.PaystationType.Id=255"
		    EMS7PaystationTypeId=255
		elif(re.search(r"verrus", str(SerialNumber))):
		    EMS7PaystationTypeId=10
#		    print "Verrus Paystation" 
		elif(re.search(r"^1", str(SerialNumber))): 
#		    print "LUKE IV machine"
		    EMS7PaystationTypeId=1
		elif(re.search(r"^2", str(SerialNumber))):	
#		    print "Shelby machine"
		    EMS7PaystationTypeId=2
		elif(re.search(r"^3", str(SerialNumber))):	
#		    print "Radius Luke"
		    EMS7PaystationTypeId=3
		elif(re.search(r"^5", str(SerialNumber))):
#		    print "LUKE II machine"
		    EMS7PaystationTypeId=5
		elif(re.search(r"^8", str(SerialNumber))):
#		    print "Test/Demo machine"
		    EMS7PaystationTypeId=8
		elif(re.search(r"^9", str(SerialNumber))):
#		    print "virtual machine"
		    EMS7PaystationTypeId=9
		else:
#		    print "The Paystation did not match any of the above criteria, it is being assigned as other"
		    EMS7PaystationTypeId=0
#		print "The Type Of Paystation is %s" %EMS7PaystationTypeId
		return EMS7PaystationTypeId
		
	def __printPaystation(self, paystation):
		if(self.__verbose):
			print " -- Paystation Id %s" %paystation[0]
			print " -- PaystationType %s " %paystation[1]
			print " -- Modem Type %s" %paystation[2]
			print " -- CommAddress %s" %paystation[3]
			print " -- Version %s" %paystation[4]
			print " -- Name %s" %paystation[5]
			print " -- ProvisionDate %s" %paystation[6]
			print " -- CustomerId %s" %paystation[7]
			print " -- RegionId %s" %paystation[8]
			print " -- LotSettingId %s" %paystation[9]

	def addLocationToEMS7(self, Location, Action):
		tableName='Region'
		ParentLocationId=None
		self.__printLocation(Location)
		IsDeleted = 0 
		LastModifiedGMT = date.today()
		ModifiedDate = date.today()
		LastModifiedByUserId = 1
		TargetMonthlyRevenueAmount = 0
		NumberOfSpaces = 0
		IsParent = 0 
		IsUnassigned = 0	
		EMS6RegionId = Location[7]
		Version = Location[0]
		AlarmState = Location[1]
		EMS6CustomerId = Location[2]
		Name = Location[3]
		Description = Location[4]
		ParentRegion = Location[5]
		#Populate Locationopen, LocationDay, Can Call the Stored procedure
#		if (ParentRegion==0):
#			ParentLocationId=None
#		elif (ParentRegion<>0 or ParentRegion<>None):
#			ParentLocationId = self.__getLocationId(ParentRegion)
			#Set the IsParent=1 if there is a ParentLocation available for the parent record
#		else:
#			ParentLocationId=None
		# Ashok: Commented the below line to make foreign_key_checks=0 Added new line
		#self.__EMS7Cursor.execute("set foreign_key_checks=1")	
		self.__EMS7Cursor.execute("set foreign_key_checks=0")
		IsDefault = Location[6]
		"""Check the mandatory columns - CustomerId, name, version as they are required in location but optional in region"""
		
		EMS7CustomerID = self.__getEMS7CustomerId(EMS6CustomerId)
		if (Action=='Insert' and Name and Name!='Unassigned'):
			self.__EMS7Cursor.execute("Insert ignore into Location(CustomerId,ParentLocationId,Name,NumberOfSpaces,TargetMonthlyRevenueAmount,\
							Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) \
							values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (EMS7CustomerID,ParentLocationId,Name,NumberOfSpaces,\
							TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
			EMS7LocationId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into RegionLocationMapping(EMS7LocationId,EMS6RegionId,Name,ModifiedDate) \
							values(%s,%s,%s,%s)",(EMS7LocationId,EMS6RegionId,Name,ModifiedDate))

			# This is the new code that was just recently added, There is no default for LocationOpen table, these are new in EMS7 therefore we will need to add a default to it
			DayCount=1
			while (DayCount<=7):
				self.__EMS7Cursor.execute("insert into LocationDay(LocationId, DayOfWeekId, OpenQuarterHourNumber, CloseQuarterHourNumber, \
								IsOpen24Hours, IsClosed, VERSION, LastModifiedGMT, LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
								(EMS7LocationId,DayCount,0,95,1,0,1,LastModifiedGMT,LastModifiedByUserId))
				LocationDayId = self.__EMS7Cursor.lastrowid
				OpenQuarterHour=1
				IsOpen=1 # dy default 1
				while (OpenQuarterHour<=95):
					self.__EMS7Cursor.execute("insert into LocationOpen(LocationId,DayOfWeekId,QuarterHourId,IsOpen,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7LocationId,DayCount,OpenQuarterHour,IsOpen,LastModifiedGMT,LastModifiedByUserId))
					OpenQuarterHour=OpenQuarterHour+1
				DayCount=DayCount+1
				self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6RegionId,tableName,IsMultiKey)

		if (Action=='Update'):
			#Aug 13 ITF
			ParentRegionId = self.__getLocationId(ParentRegion)
			EMS7LocationId = self.__getLocationId(EMS6RegionId)
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("update Location set CustomerId=%s,ParentLocationId=%s,Name='%s',NumberOfSpaces=%s,TargetMonthlyRevenueAmount=%s,\
							Description='%s',IsParent=%s,IsUnassigned=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT='%s',\
							LastModifiedByUserId=%s where Id=%s" %(EMS7CustomerID,ParentRegionId,Name,NumberOfSpaces,TargetMonthlyRevenueAmount,\
							Description,IsParent,IsUnassigned,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,EMS7LocationId))
			self.__EMS7Cursor.execute("update Location set IsParent=1 where id = %s",ParentRegionId)
			self.__EMS7Connection.commit()
							
	def addParentRegionIdToLocation(self, Location, Action):
		self.__printLocation(Location)
		EMS6RegionId = Location[7]
                Version = Location[0]
                AlarmState = Location[1]
                EMS6CustomerId = Location[2]
                Name = Location[3]
                Description = Location[4]
                ParentRegion = Location[5]
		ParentLocationId = self.__getLocationId(ParentRegion)
		LocationId = self.__getLocationId(EMS6RegionId)
#		print " -- EMS7 Parent Location Id is %s" %ParentLocationId
#		print " -- EMS7 LocationId is %s" %LocationId
		if (ParentLocationId):
			self.__EMS7Cursor.execute("update Location set ParentLocationId=%s, IsParent=1 where Id=%s",(ParentLocationId,LocationId))
			# technically a parent location should not have a any paystation, or pointof sale record,and it should nto even need location day or location open either.
			self.__EMS7Cursor.execute("update Location set IsParent=1 where Id=%s",(LocationId))
			self.__EMS7Connection.commit()
			
	def __printLocation(self,Location):
		if(self.__verbose):
			print " -- Region Id %s " %Location[7]
			print " -- Version %s " %Location[0]
			print " -- AlarmState %s " %Location[1]
			print " -- EMS6CustomerId %s " %Location[2]
			print " -- Name %s " %Location[3]
			print " -- Description %s " %Location[4]
			print " -- ParentRegion %s " %Location[5]
			print " -- IsDefault %s " %Location[6]

	def addCouponToEMS7(self, coupon, Action):
		tableName='Coupon'
		self.__printCoupon(coupon)
		IsDeleted = 1
		Version = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustID = 0	
		EMS6CustomerId = coupon[0]
		CouponId = coupon[1]
		PercentageDiscount = coupon[2]
		StartDate = coupon[3]
		EndDate = coupon[4]
		NumUses = coupon[5]
		RegionId = coupon[6]
		StallRange = coupon[7]
		Description = coupon[8]
		IsPndEnabled = coupon[9]
		IsPbsEnabled = coupon[10]
		DollarDiscount = coupon[11]
		EMS7CustID = self.__getEMS7CustomerId(EMS6CustomerId)
		if (RegionId==0):
			#All the EMS6 Records arriving as RegionId=0 are being translated into NULL in EMS7Location column in Coupon table.
			EMS7Location=None
		else:
			EMS7Location = self.__getLocationId(RegionId)
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into Coupon(CustomerId,Coupon,LocationId,PercentDiscount,StartDateLocal,EndDateLocal,\
							NumberOfUsesRemaining,SpaceRange,Description,IsPndEnabled,IsPbsEnabled,IsDeleted,VERSION,\
							LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", \
							(EMS7CustID,CouponId,EMS7Location,PercentageDiscount,StartDate,EndDate,NumUses,StallRange,Description,IsPndEnabled,\
							IsPbsEnabled,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
			EMS7CouponId=self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into CouponMapping(EMS6CouponId,EMS7CouponId,NumUses,EMS6CustomerId) \
							values(%s,%s,%s,%s)",(CouponId,EMS7CouponId,NumUses,EMS6CustomerId))
			self.__EMS7Connection.commit()

		if(Action=='Update'):
			self.__EMS7Cursor.execute("update Coupon set CustomerId=%s,LocationId=%s,Coupon=%s,PercentDiscount=%s,StartDateLocal=%s,\
							EndDateLocal=%s,NumberOfUsesRemaining=%s,SpaceRange=%s,Description=%s,IsPndEnabled=%s,\
							IsPbsEnabled=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s,DollarDiscountAmount= %s \
							where CustomerId=%s and Coupon=%s",(EMS7CustID,EMS7Location,CouponId,PercentageDiscount,\
							StartDate,EndDate,NumUses,StallRange,Description,IsPndEnabled,IsPbsEnabled,IsDeleted,Version,\
							LastModifiedGMT,LastModifiedByUserId,DollarDiscount,EMS7CustID,CouponId))
			self.__EMS7Connection.commit()
		print "...Finished coupon migration..."
	def __printCoupon(self, Coupon):
		if(self.__verbose):
			print " -- CustomerId %s" %Coupon[0]
			print " -- CouponId %s" %Coupon[1]
			print " -- Percentage Discount %s" %Coupon[2]
			print " -- StartDate %s" %Coupon[3]
			print " -- EndDate %s" %Coupon[4]
			print " -- Num uses %s" %Coupon[5]
			print " -- RegionId %s" %Coupon[6]
			print " -- StallNumber %s" %Coupon[7]
			print " -- Description %s" %Coupon[8]
			print " -- isPndEnabled %s" %Coupon[9]
			print " -- IsPbsEnabled %s" %Coupon[10]

#	This table is being pre populated
#	def addParkingPermissionTypeToEMS7(self, permissiontype):
#		self.__printParkingPermissionType(permissiontype)
#		LastModifiedGMT = date.today()
#		LastModifiedByUserId = 1
#		self.__EMS7Cursor.execute("Insert into ParkingPermissionType(Id,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)", (permissiontype[0],permissiontype[1],LastModifiedGMT,LastModifiedByUserId))
#		self.__EMS7Connection.commit()

	def __printParkingPermissionType(self, permissiontype):
		if(self.__verbose):
			print "ParkingPermisisonId %s" %permissiontype[0]
			print "ParkingPermissionName %s" %permissiontype[1]

	def addParkingPermissionToEMS7(self, ParkingPermission, Action):
		print " Adding parking permissions in EMS7"
		tableName='EMSParkingPermission'
		self.__printParkingPermission(ParkingPermission)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7LocationID = 0
		Version = 0
		SpecialParkingPermissionId='NULL'
		Name = ParkingPermission[0]
		BeginHourLocal = ParkingPermission[1]
		BeginMinuteLocal = ParkingPermission[2]	
		EndHourLocal = ParkingPermission[3]
		EndMinuteLocal = ParkingPermission[4]
		RegionId = ParkingPermission[5]
		PermissionTypeId = ParkingPermission[6]
		PermissionStatus = ParkingPermission[7] 
		IsLimitedOrUnlimited = self.__getIsLimitedOrUnlimited(PermissionStatus)
		SpecialParkingPermissionId = ParkingPermission[8]
		MaxDurationMinutes = ParkingPermission[9]
		CreationDate = ParkingPermission[10]
		IsActive = ParkingPermission[11]
		EMS6ParkingPermissionId = ParkingPermission[12]
		EMS7LocationId = self.__getLocationId(RegionId)
		if(Action=='Insert'):
			if (EMS7LocationId):
				print" migrating parking permission for location id:", EMS7LocationId 
				self.__EMS7Cursor.execute("insert into ParkingPermission(LocationId,ParkingPermissionTypeId,Name,BeginHourLocal,\
					BeginMinuteLocal,EndHourLocal,EndMinuteLocal,MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,VERSION,\
					CreatedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					(EMS7LocationId,PermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,\
					MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,Version,CreationDate,LastModifiedGMT,LastModifiedByUserId))
				EMS7ParkingPermissionId=self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into ParkingPermissionMapping(EMS6ParkingPermissionId,EMS7ParkingPemissionId,Name) \
					values(%s,%s,%s)",(EMS6ParkingPermissionId,EMS7ParkingPermissionId,Name))
				self.__EMS7Connection.commit()
			else:
				## Note that SpecialParkingPermissionId has not been imported into the ParkingPermission table because there are no rows in the table for this column in EMS6, the table EMSSpecialParkingPermission is empty as well.
				# There are no matching RegionId in the Mapping table as the Region Id is infact missing in the Region Table, but it is present in ParkingPermission Table.... These records are usually test records..these records are now being inserted as a location Id 2667 which is assigned to DPT until further clarification ...
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6ParkingPermissionId,tableName,IsMultiKey)	
				EMS7LocationID=2667
				self.__EMS7Cursor.execute("insert into ParkingPermission(LocationId,ParkingPermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,\
								EndHourLocal,EndMinuteLocal,MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,VERSION,CreatedGMT,\
								LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
								(EMS7LocationID,PermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,\
								MaxDurationMinutes,PermissionStatus,IsActive,Version,CreationDate,LastModifiedGMT,LastModifiedByUserId))
		if(Action=='Update'):
			if (EMS7LocationId):
				print " Updating parking permission for location id:", EMS7LocationId
				EMS7ParkingPermissionId=self.__getParkingPermissionId(EMS6ParkingPermissionId)
				self.__EMS7Cursor.execute("Update ParkingPermission set LocationId=%s,ParkingPermissionTypeId=%s,Name=%s,BeginHourLocal=%s,\
					BeginMinuteLocal=%s,EndHourLocal=%s,EndMinuteLocal=%s,MaxDurationMinutes=%s,IsLimitedOrUnlimited=%s,IsActive=%s,VERSION=%s,\
					CreatedGMT=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",\
					(EMS7LocationId,PermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,\
					MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,Version,CreationDate,LastModifiedGMT,LastModifiedByUserId, EMS7ParkingPermissionId))
				self.__EMS7Connection.commit()
	
	def __getIsLimitedOrUnlimited(self,PermissionStatus):
		IsLimitedOrUnlimited = None
		if (PermissionStatus=='Unlimited'):
			IsLimitedOrUnlimited = 2
		elif (PermissionStatus=='Limited'):
			IsLimitedOrUnlimited=1
		return IsLimitedOrUnlimited
	
	def __printParkingPermission(self, ParkingPermission):
		if(self.__verbose):
			print "Name %s" %ParkingPermission[0]
			print "StartTimeHourLocal %s" %ParkingPermission[1]
			print "StartTimeMinuteLocal %s" %ParkingPermission[2]
			print "EndTimeHourLocal %s" %ParkingPermission[3]
			print "EndTimeMInuteLocal %s" %ParkingPermission[4]
			print "RegionId %s" %ParkingPermission[5]
			print "PermissionTypeID %s" %ParkingPermission[6]
			print "PermissiontStatus %s" %ParkingPermission[7]
			print "SpecialParkingPermissionId %s" %ParkingPermission[8]
			print "MaxDurationMinutes %s" %ParkingPermission[9]
			print "CreationDate %s" %ParkingPermission[10]
			print "IsActive %s" %ParkingPermission[11]
			print "Id is %s" %ParkingPermission[12]

	def addParkingPermissionDayOfWeek(self, ParkingPermissionDayOfWeek):
		tableName='ParkingPermissionDayOfWeek'
		LastModifiedGMT = date.today()
		EMSPermissionId=ParkingPermissionDayOfWeek[0]
		DayOfWeek=ParkingPermissionDayOfWeek[1]
		self.__printParkingPermissionDayOfWeek(ParkingPermissionDayOfWeek)
		LastModifiedByUserId = 1
		VERSION = 1
		ParkingPermissionId=self.__getParkingPermissionId(EMSPermissionId)
		if(ParkingPermissionId):
			self.__EMS7Cursor.execute("insert into ParkingPermissionDayOfWeek(ParkingPermissionId,DayOfWeekId,VERSION,LastModifiedGMT,\
							LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(ParkingPermissionId,ParkingPermissionDayOfWeek[1],\
							VERSION,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMSPermissionId, DayOfWeek)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __getParkingPermissionId(self,ParkingPermissionDayOfWeek):
		if(ParkingPermissionDayOfWeek in self.__parkingpermissiondayofweek):
			return self.__parkingpermissiondayofweek[ParkingPermissionDayOfWeek]

		ParkingPermissionId = None
		if (ParkingPermissionDayOfWeek):
			self.__EMS7Cursor.execute("select EMS7ParkingPemissionId from ParkingPermissionMapping where EMS6ParkingPermissionId=%s",(ParkingPermissionDayOfWeek))	
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				ParkingPermissionId = row[0]

		self.__parkingpermissiondayofweek[ParkingPermissionDayOfWeek] = ParkingPermissionId

		return ParkingPermissionId

	def __printParkingPermissionDayOfWeek(self, ParkingPermissionDayOfWeek):
		if(self.__verbose):
			print " -- Parking Permission Day Of Weeek %s" %ParkingPermissionDayOfWeek[0]
			print " -- DayOfWeekId %s" %ParkingPermissionDayOfWeek[1]

	def addExtensibleRateDayOfWeekToEMS7(self, ExtensibleRate):
		self.__printExtensibleRateDayOfWeek(ExtensibleRate)
		tableName='ExtensibleRateDayOfWeek'
		IsMultiKey=None
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMSRateId=ExtensibleRate[0]
		DayOfWeek=ExtensibleRate[1]
		VERSION = 1
		ExtensibleRateId=self.__getExtensibleRateId(EMSRateId)
		if(ExtensibleRateId):
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("insert into ExtensibleRateDayOfWeek(ExtensibleRateId,DayOfWeekId,VERSION,LastModifiedGMT,\
							LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMSRateId,DayOfWeek,VERSION,\
							LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
			self.__EMS7Cursor.execute("set foreign_key_checks=1") #Added by Ashok on July 9
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMSRateId,tableName,IsMultiKey)

	def __printExtensibleRateDayOfWeek(self, ExtensibleRate):
		if(self.__verbose):
			print " -- EMSRateId %s" %ExtensibleRate[0]
			print " -- DayOfWeek %s" %ExtensibleRate[1]

	def __getExtensibleRateId(self,extensiblerate):
		if (extensiblerate in self.__extensiblerateIdCache):
			return self.__extensiblerateIdCache[extensiblerate]

		EMS7ExtensibleRateId = None
		self.__EMS7Cursor.execute("select EMS7RateId from ExtensibleRateMapping where EMS6RateId=%s",(extensiblerate))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7ExtensibleRateId = row[0]
		
		self.__extensiblerateIdCache[extensiblerate] = EMS7ExtensibleRateId

		return EMS7ExtensibleRateId

	def addExtensibleRatesToEMS7(self, rate, Action):
		print" ...Adding extensible rate to EMS 7..."
		try:
			tableName='ExtensibleRates'
			IsMultiKey=None
			LastModifiedGMT = date.today()
			LastModifiedByUserId = 1
			EMS6CustomerId = None
			UnifiedRateId = None
			self.__printExtensibleRates(rate)
			SpecialRateDateId = None
			Name = rate[0]
			RateTypeId = rate[1]
			SpecialRateId = rate[2]
			StartTimeHourLocal = rate[3]
			StartTimeMinuteLocal = rate[4]
			EndTimeHourLocal = rate[5]
			EndTimeMinuteLocal = rate[6]
			RegionId = rate[7]
			RateAmountCent = rate[8]
			ServiceFeeCent = rate[9]
			MinExtensionMinutes = rate[10]
			CreationDate = rate[11]
			IsActive = rate[12]
			EMSRateId=rate[13]
			EMS7LocationId = self.__getLocationId(RegionId)
			print "---Finished collecting variables..."
			print "location id is", EMS7LocationId
			if(Action=='Insert'):
				if (EMS7LocationId):
	#				self.__EMS7Cursor.execute("select CustomerId from Location where Id=%s", (EMS7LocationId))
	#				if (self.__EMS7Cursor.rowcount <> 0):
	#					row = self.__EMS7Cursor.fetchone()
	#					EMS7CustomerId = row[0]
					EMS7CustomerId = self.__getCustomerFromLocationId(EMS7LocationId)
					print"EMS7 Customer id is", EMS7CustomerId
					UnifiedRateId  = self.__getUnifiedRateForExtensibleRate(EMS7CustomerId,Name)
	#				self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(EMS7CustomerId,Name))
	#				if (self.__EMS7Cursor.rowcount <> 0):
	#					row = self.__EMS7Cursor.fetchone()
	#					UnifiedRateId = row[0]
	#					if(UnifiedRateId==None):
	#						self.__EMS7Cursor.execute("insert ignore into UnifiedRate(CustomerId,Name) values(%s,%s)",(EMS7CustomerId,Name))
	#						UnifiedRateId=self.__EMS7Cursor.lastrowid
	#			print "EMS7LocationId >>>>>>>>>>>>>>>>> is %s" %EMS7LocationId
	#			print "UnifiedRateId >>>>>>>>>>>>>>>>>> is %s" %UnifiedRateId
					print"Unified Rate id is", UnifiedRateId
					if (UnifiedRateId):
						self.__EMS7Cursor.execute("Insert into ExtensibleRate(UnifiedRateId,LocationId,ExtensibleRateTypeId,SpecialRateDateId,Name,\
										BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,RateAmount,ServiceFeeAmount,\
										MinExtensionMinutes,IsActive,CreatedGMT,LastModifiedGMT,LastModifiedByUserId) \
										values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(UnifiedRateId, EMS7LocationId,\
										RateTypeId,SpecialRateDateId,Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,\
										EndTimeMinuteLocal,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,IsActive,CreationDate,LastModifiedGMT,LastModifiedByUserId))
						print "Checking what is the sequence in auto invrement"
						EMS7ExtensibleRateId=self.__EMS7Cursor.lastrowid
						self.__EMS7Connection.commit()
						#EMS7ExtensibleRateId=self.__EMS7Cursor.lastrowid
						print " EMS7 Extensilble Rate Id being inserted is %s" %EMS7ExtensibleRateId
						self.__EMS7Cursor.execute("insert into ExtensibleRateMapping(EMS6RateId,EMS7RateId) \
										values(%s,%s)",(EMSRateId,EMS7ExtensibleRateId))
						self.__EMS7Connection.commit()
				else:
					IsMultiKey=0
					self.__InsertIntoDiscardedRecords(EMSRateId,tableName,IsMultiKey)

			if(Action=='Update'):
				print " We are in the Update section"
				self.__EMS7Cursor.execute("set unique_checks=0")
				EMS7CustomerId = self.__getCustomerFromLocationId(EMS7LocationId)
				print " EMS7 Customer Id %s" %EMS7CustomerId
				UnifiedRateId  = self.__getUnifiedRateForExtensibleRate(EMS7CustomerId,Name)
				print " Unified Rate Id %s" %UnifiedRateId
				self.__EMS7Cursor.execute("select EMS7RateId from ExtensibleRateMapping where EMS6RateId=%s",(EMSRateId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7ExtensibleRateId = row[0]
					print "ExtensibleRate Id %s" %EMS7ExtensibleRateId
					print "step 3"
	#				self.__EMS7Cursor.execute("update ExtensibleRate set UnifiedRateId=%s,LocationId=%s where Id=%s",(UnifiedRateId, EMS7LocationId,EMS7ExtensibleRateId))

					self.__EMS7Cursor.execute("update ExtensibleRate set UnifiedRateId=%s,LocationId=%s,ExtensibleRateTypeId=%s,SpecialRateDateId=%s,Name=%s,BeginHourLocal=%s,BeginMinuteLocal=%s,EndHourLocal=%s,EndMinuteLocal=%s,RateAmount=%s,ServiceFeeAmount=%s,MinExtensionMinutes=%s,IsActive=%s,CreatedGMT=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(UnifiedRateId, EMS7LocationId,RateTypeId,SpecialRateDateId,Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,IsActive,CreationDate,LastModifiedGMT,LastModifiedByUserId,EMS7ExtensibleRateId))
					self.__EMS7Connection.commit()
		except mdb.Error, e:
			print 'Error: ',e[0]

	def __getCustomerFromLocationId(self,EMS7LocationId):
		print "Step 1"
		EMS7CustomerId=None
		self.__EMS7Cursor.execute("select CustomerId from Location where Id=%s", (EMS7LocationId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerId = row[0]
		return EMS7CustomerId

	def __getUnifiedRateForExtensibleRate(self, EMS7CustomerId,Name):
		print "step 2"
		UnifiedRateId=None
		self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(EMS7CustomerId,Name))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			UnifiedRateId = row[0]
			print "Uni %s" %UnifiedRateId
		elif(self.__EMS7Cursor.rowcount == 0):
			self.__EMS7Cursor.execute("insert ignore into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,Name,date.today(),1))
			UnifiedRateId=self.__EMS7Cursor.lastrowid
			print " Inside Unified Rate id %s" %UnifiedRateId
		print " Unifiedrate id %s" %UnifiedRateId
		return UnifiedRateId
 
	def __printExtensibleRates(self,rate):
		if(self.__verbose):
			print " -- Name %s" %rate[0]
			print " -- rateTypeId %s" %rate[1]
			print " -- SpecialRateId %s " %rate[2]
			print " -- StartTimeHourlocal %s" %rate[3]
			print " -- StartTimeMinuteLocal %s " %rate[4]
			print " -- EndTimeHourLocal %s " %rate[5]
			print " -- EndTimeMinuteLocal %s" %rate[6]
			print " -- RegionId %s" %rate[7]
			print " -- RateAmountCent %s" %rate[8]
			print " -- ServiceFeeCent %s" %rate[9]
			print " -- MinExtensionMinutes %s" %rate[10]
			print " -- CreationDate %s" %rate[11]
			print " -- IsActive %s" %rate[12]

	def addLicencePlateToEMS7(self, LicencePlate):
		self.__printLicencePlate(LicencePlate)
		LicencePlate=LicencePlate[0]
		LastModifiedGMT = date.today()
		self.__EMS7Cursor.execute("insert ignore into LicencePlate(Number,LastModifiedGMT) values(%s,%s)",\
					(LicencePlate,LastModifiedGMT))
		self.__EMS7Connection.commit()

	def __printMobileNumber(self,MobileNumber):
		if(self.__verbose):
			print " -- MobileNumber %s" %MobileNumber

	def __printLicencePlate(self, LicencePlate):
		if(self.__verbose):
			print " -- PlateNumber %s" %LicencePlate[0]

	def addMobileNumberToEMS7(self, MobileNumber):
		self.__printMobileNumber(MobileNumber)
		MobileNumber = MobileNumber[0]
		LastModifiedGMT = date.today()	
		self.__EMS7Cursor.execute("insert ignore into MobileNumber \
					  (Number,LastModifiedGMT) values(%s,%s)",\
					  (MobileNumber,LastModifiedGMT))
		self.__EMS7Connection.commit()

	def addExtensiblePermitToEMS7(self, ExtensiblePermit, Action):
		print " In adding extensible permit to EMS 7 method..."
		self.__printExtensiblePermit(ExtensiblePermit)
		MobileNumber=ExtensiblePermit[0]
		CardData=ExtensiblePermit[1]
		Last4Digital=ExtensiblePermit[2]
		PurchasedDate=ExtensiblePermit[3]
		LatestExpiryDate=ExtensiblePermit[4]
		IsRFID=ExtensiblePermit[5]
		MobileNumberId = self.__getMobileNumberId(MobileNumber)
		if(MobileNumberId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into ExtensiblePermit(MobileNumberId,CardData,Last4Digit,PermitBeginGMT,PermitExpireGMT,IsRFID) \
								values(%s,%s,%s,%s,%s,%s)",(MobileNumberId,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID))
				EMS7ExtensiblePermitId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into ExtensiblePermitMapping(EMS6MobileNumber,EMS7Id) values(%s,%s)",(MobileNumber,EMS7ExtensiblePermitId))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				self.__EMS7Cursor.execute("select EMS7Id from ExtensiblePermitMapping where EMS6MobileNumber=%s",(MobileNumber))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7ExtensiblePermitId = row[0]
				self.__EMS7Cursor.execute("update ExtensiblePermit set CardData=%s,Last4Digit=%s,PermitBeginGMT=%s,PermitExpireGMT=%s,IsRFID=%s where Id=%s",(CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID,EMS7ExtensiblePermitId))
				self.__EMS7Connection.commit()
				

	def __printExtensiblePermit(self,ExtensiblePermit):
		if(self.__verbose):
			print " -- MobileNumber %s" %ExtensiblePermit[0]
			print " -- CardData %s " %ExtensiblePermit[1]
			print " -- Last4digit %s" %ExtensiblePermit[2]
			print " -- PurchaseDate %s" %ExtensiblePermit[3]
			print " -- LatestExpiryDate %s" %ExtensiblePermit[4]
			print " -- IsRFID %s" %ExtensiblePermit[5]

	def addSMSTransactionLogToEMS7(self, SMSTransactionLog, Action):
		print " Adding SMStransactionLog to EMS 7..."
		self.__printSMSTransactionLog(SMSTransactionLog)
		SMSMessageTypeId = SMSTransactionLog[0]
		MobileNumber = SMSTransactionLog[1]
		EMS6PaystationId = SMSTransactionLog[2]
		PurchasedDate = SMSTransactionLog[3]
		TicketNumber = SMSTransactionLog[4]
		ExpiryDate = SMSTransactionLog[5]
		UserResponse = SMSTransactionLog[6]
		TimeStamp = SMSTransactionLog[7]
		#EMS7PaystationId = self.__getEMS7PaystationId(EMS6PaystationId)
		MobileNumberId = self.__getMobileNumberId(MobileNumber)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(PointOfSaleId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into SmsTransactionLog(SMSMessageTypeId,MobileNumberId,PointOfSaleId,PurchaseGMT,TicketNumber,\
								ExpiryDateGMT,ConsumerResponse,TimeStampGMT) values(%s,%s,%s,%s,%s,%s,%s,%s)",\
								(SMSMessageTypeId,MobileNumberId,PointOfSaleId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp))
				EMS7SMSTransactionLogId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into SMSTransactionLogMapping(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,EMS7TransactionLogId) values(%s,%s,%s,%s,%s)",(EMS6PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,EMS7SMSTransactionLogId))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				self.__EMS7Cursor.execute("select distinct(EMS7TransactionLogId) from SMSTransactionLogMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(EMS6PaystationId,PurchasedDate,TicketNumber))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7TransactionLogId= row[0]
					self.__EMS7Cursor.execute("update SmsTransactionLog set SMSMessageTypeId=%s,MobileNumberId=%s,PointOfSaleId=%s,PurchaseGMT=%s,TicketNumber=%s,ExpiryDateGMT=%s,ConsumerResponse=%s,TimeStampGMT=%s where Id=%s",(SMSMessageTypeId,MobileNumberId,PointOfSaleId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp,EMS7TransactionLogId))
					self.__EMS7Connection.commit()

	def __printSMSTransactionLog(self, SMSTransactionLog):
		if(self.__verbose):
			print " -- SMSMessageTypeId %s" %SMSTransactionLog[0]
			print " -- MobileNumber %s" %SMSTransactionLog[1]
			print " -- PaystationId %s" %SMSTransactionLog[2]
			print " -- PurchasedDate %s" %SMSTransactionLog[3]
			print " -- TicketNumber %s" %SMSTransactionLog[4]
			print " -- ExpiryDate %s" %SMSTransactionLog[5]
			print " -- UserResponse %s" %SMSTransactionLog[6]
			print " -- TimeStamp %s" %SMSTransactionLog[7]

	def __getEMS7PaystationId(self, EMS6PaystationId):
		if (EMS6PaystationId in self.__paystationIdCache):
			return self.__paystationIdCache[EMS6PaystationId]

		EMS7PaystationId=None
		self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7PaystationId = row[0]
			self.__paystationIdCache[EMS6PaystationId] = EMS7PaystationId
		return EMS7PaystationId

	def __getPointOfSale(self, EMS6PaystationId):
		PointOfSaleId = None
		if(EMS6PaystationId):
			self.__EMS7Cursor.execute("select po.Id,po.CustomerId from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s",(EMS6PaystationId))
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchall()
				PointOfSaleId = row[0]
		return PointOfSaleId

	def __getMobileNumberId(self, MobileNumber):
		LastModifiedDate = date.today()
		if (MobileNumber in self.__mobilenumberIdCache):
                        return self.__mobilenumberIdCache[MobileNumber]

		MobileNumberId = None
		if(MobileNumber<>None):
			self.__EMS7Cursor.execute("select distinct(Id) from MobileNumber where Number=%s",(MobileNumber))
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				MobileNumberId = row[0]
			else:
				self.__EMS7Cursor.execute("insert into MobileNumber(Number,LastModifiedGMT) values(%s,%s)",(MobileNumber,LastModifiedDate))
				MobileNumberId = self.__EMS7Cursor.lastrowid
		
		self.__mobilenumberIdCache[MobileNumber] = MobileNumberId
		return MobileNumberId

	def __getPointOfSaleId(self, EMS6PaystationId):
		if (EMS6PaystationId in self.__pointofsaleIdCache):
			return self.__pointofsaleIdCache[EMS6PaystationId]

		PointOfSaleId = None
		if(EMS6PaystationId<>None):
			self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(EMS6PaystationId))
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				PointOfSaleId = row[0]

		self.__pointofsaleIdCache[EMS6PaystationId] = PointOfSaleId

		return PointOfSaleId			

	def addSMSAlertToEMS7(self, SMSAlert, Action):
		print " .. ....in adding SMS Alert data to EMS 7........"
		self.__printSMSAlert(SMSAlert)
		AddTimeNum = 1
		PermitBeginGMT = date.today()
		PermitExpireGMT = date.today()
		TicketNumber = 1
		MobileNumber = SMSAlert[0]
		ExpiryDate = SMSAlert[1]
		EMS6PaystationId = SMSAlert[2]
		PurchasedDate = SMSAlert[3]
		TicketNumber = SMSAlert[4]
		PlateNumber = SMSAlert[5]
		StallNumber = SMSAlert[6]
		RegionId = SMSAlert[7]
		NumOfRetry = SMSAlert[8]
		IsAlerted = SMSAlert[9]
		IsLocked = SMSAlert[10]
		IsAutoExtended = SMSAlert[11]
		self.__EMS7Cursor.execute("select po.Id,po.CustomerId from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s",EMS6PaystationId)
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PointOfSaleId = row[0]
			CustomerId = row[1]
			PermitLocationId = self.__getLocationId(RegionId)
			MobileNumberId = self.__getEMS7MobileNumberId(MobileNumber)
			print " ....Got point of sale and paystation data..."
			if(Action=='Insert'):
				print "...Creating SMS Alert Insert record..."
				self.__EMS7Cursor.execute("insert ignore into SmsAlert(CustomerId,PermitLocationId,PointOfSaleId,PermitBeginGMT,PermitExpireGMT,MobileNumberId,\
								LicencePlate,TicketNumber,AddTimeNumber,SpaceNumber,NumberOfRetries,IsAutoExtended,IsAlerted,IsLocked) \
								values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(CustomerId,PermitLocationId,PointOfSaleId,PermitBeginGMT,\
								PermitExpireGMT,MobileNumberId,PlateNumber,TicketNumber,AddTimeNum,StallNumber,NumOfRetry,IsAutoExtended,IsAlerted,IsLocked))
				SmsAlertId = self.__EMS7Cursor.lastrowid
				print " creating record in SMAAlertMapping table with SMS Alert Id", SmsAlertId
				self.__EMS7Cursor.execute("insert ignore into SMSAlertMapping(MobileNumber,EMS7Id) values(%s,%s)",(MobileNumber,SmsAlertId))
				self.__EMS7Connection.commit()	
			if(Action=='Update'):
				print ".......updating SMSAlert Record....."
				self.__EMS7Cursor.execute("select EMS7Id from SMSAlertMapping where MobileNumber=%s",(MobileNumber))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7SmsId = row[0]
					self.__EMS7Cursor.execute("update SmsAlert set CustomerId=%s,PermitLocationId=%s,PointOfSaleId=%s,PermitBeginGMT=%s,PermitExpireGMT=%s,MobileNumberId=%s,LicencePlate=%s,TicketNumber=%s,AddTimeNumber=%s,SpaceNumber=%s,NumberOfRetries=%s,IsAutoExtended=%s,IsAlerted=%s,IsLocked=%s where Id=%s",(CustomerId,PermitLocationId,PointOfSaleId,PermitBeginGMT,PermitExpireGMT,MobileNumberId,PlateNumber,TicketNumber,AddTimeNum,StallNumber,NumOfRetry,IsAutoExtended,IsAlerted,IsLocked,EMS7SmsId))
					self.__EMS7Connection.commit()

	def __printSMSAlert(self, SMSAlert):
		if(self.__verbose):
			print " -- MobileNumber %s " %SMSAlert[0]
			print " -- Expiry Date %s " %SMSAlert[1]
			print " -- PaystationId %s " %SMSAlert[2]
			print " -- Purchased Date %s " %SMSAlert[3]
			print " -- TicketNumber %s " %SMSAlert[4]
			print " -- PlateNumber %s" %SMSAlert[5]
			print " -- Stall Number %s" %SMSAlert[6]
			print " -- EMS6 RegionId %s" %SMSAlert[7]
			print " -- NumOfRetries %s" %SMSAlert[8]
			print " -- IsAlerted %s" %SMSAlert[9]
			print " -- IsLocked %s" %SMSAlert[10]
			print " -- IsAutoExtended %s" %SMSAlert[11]

	def __getEMS7MobileNumberId(self, MobileNumber):
                if (MobileNumber in self.__mobilenumberCache):
                        return self.__mobilenumberCache[MobileNumber]

		EMS7MobileNumberId = None
		self.__EMS7Cursor.execute("select Id from MobileNumber where Number=%s",(MobileNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7MobileNumberId = row[0]

		elif(self.__EMS7Cursor.rowcount==0):
			self.__EMS7Cursor.execute("insert into MobileNumber(Number,LastModifiedGMT) values(%s,%s)",(MobileNumber,date.today()))
			EMS7MobileNumberId=self.__EMS7Cursor.lastrowid

		self.__mobilenumberCache[MobileNumber] = EMS7MobileNumberId
		return EMS7MobileNumberId
		
	def addEMS6SMSFailedResponseToEMS7(self, SMSFailedResponse, Action):
		print " ...in migrating SMSFailedResponse record methos..."
		self.__printSMSFailedResponse(SMSFailedResponse)
		PaystationId = SMSFailedResponse[0]
		PurchasedDate = SMSFailedResponse[1]
		TicketNumber = SMSFailedResponse[2]
		SMSMessageTypeId = SMSFailedResponse[3]
		IsOk = SMSFailedResponse[4]
		TrackingId = SMSFailedResponse[5]
		Number = SMSFailedResponse[6]
		ConvertedNumber = SMSFailedResponse[7]
		DeferUntilOccursInThePast = SMSFailedResponse[8]
		IsMessageEmpty = SMSFailedResponse[9]
		IsTooManyMessages = SMSFailedResponse[10]
		IsInvalidCountryCode = SMSFailedResponse[11]
		IsBlocked = SMSFailedResponse[12]
		BlockedReason = SMSFailedResponse[13]
		IsBalanceZero = SMSFailedResponse[14]
		IsInvalidCarrierCode = SMSFailedResponse[15]
		EMS7PointOfSaleId=self.__getPointOfSaleId(PaystationId)
		print"..Got pointOfSaleId...", EMS7PointOfSaleId
		if(EMS7PointOfSaleId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into SmsFailedResponse(SmsMessageTypeId,PointOfSaleId,PurchaseGMT,TicketNumber,TrackingId,Number,ConvertedNumber,\
								BlockedReason,IsOk,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,IsBalanceZero,IsInvalidCarrierCode,\
								IsDeferUntilOccursInThePast) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(SMSMessageTypeId,EMS7PointOfSaleId,\
								PurchasedDate,TicketNumber,TrackingId,Number,ConvertedNumber,BlockedReason,IsOk,IsMessageEmpty,IsTooManyMessages,\
								IsInvalidCountryCode,IsBlocked,IsBalanceZero,IsInvalidCarrierCode,DeferUntilOccursInThePast))
				EMS7SMSFailedResponseId = self.__EMS7Cursor.lastrowid
				print " creating SMSFailedResponseMapping record"
				print "EMS7SMS Failed response is %s" %EMS7SMSFailedResponseId
				self.__EMS7Cursor.execute("insert into SMSFailedResponseMapping(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,EMS7SMSFailedResponseId) values(%s,%s,%s,%s,%s)",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,EMS7SMSFailedResponseId))
				print "The row id inserted into the mapping table is %s" %self.__EMS7Cursor.lastrowid
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				print " Are we in this module------ Updating SMSFailedResponse..."	
				self.__EMS7Cursor.execute("select EMS7SMSFailedResponseId from SMSFailedResponseMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7SMSFailedResponseId = row[0]
					print "EMS7FailedReponseId %s" %EMS7SMSFailedResponseId
					self.__EMS7Cursor.execute("update SmsFailedResponse set SmsMessageTypeId=%s,PointOfSaleId=%s,PurchaseGMT=%s,TicketNumber=%s,TrackingId=%s,Number=%s,ConvertedNumber=%s,BlockedReason=%s,IsOk=%s,IsMessageEmpty=%s,IsTooManyMessages=%s,IsInvalidCountryCode=%s,IsBlocked=%s,IsBalanceZero=%s,IsInvalidCarrierCode=%s,IsDeferUntilOccursInThePast=%s where Id=%s",(SMSMessageTypeId,EMS7PointOfSaleId,PurchasedDate,TicketNumber,TrackingId,Number,ConvertedNumber,BlockedReason,IsOk,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,IsBalanceZero,IsInvalidCarrierCode,DeferUntilOccursInThePast,EMS7SMSFailedResponseId))
					self.__EMS7Connection.commit()

	def __printSMSFailedResponse(self, SMSFailedReponse):
		if(self.__verbose):
			print " -- PaystationId %s " %SMSFailedReponse[0]
			print " -- PurchasedDate %s " %SMSFailedReponse[1]
			print " -- TicketNumber %s " %SMSFailedReponse[2]
			print " -- SMSMessageTypeId %s " %SMSFailedReponse[3]
			print " -- IsOk %s " %SMSFailedReponse[4]
			print " -- TrackingId %s " %SMSFailedReponse[5]
			print " -- Number %s " %SMSFailedReponse[6]
			print " -- ConvertedNumber %s " %SMSFailedReponse[7]
			print " -- DeferUntilOccursInThePast %s " %SMSFailedReponse[8]
			print " -- IsMessageEmpty %s " %SMSFailedReponse[9]
			print " -- IsTooManyMessages %s " %SMSFailedReponse[10]
			print " -- IsInvalidCountryCode %s " %SMSFailedReponse[11]
			print " -- IsBlocked %s " %SMSFailedReponse[12]
			print " -- BlockedReason %s" %SMSFailedReponse[13]
			print " -- IsBalanceZero %s " %SMSFailedReponse[14]
			print " -- IsInvalidCarrierCode %s " %SMSFailedReponse[15]
	
	def addModemSettingToEMS7(self, ModemSetting, Action):
		tableName='ModemSetting'
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		AccessPointId = None 
		CarrierId=None
		ModemTypeId=None
		CCID=None
		self.__printModemSetting(ModemSetting)
		EMS6PaystationId = ModemSetting[0]
		Type = ModemSetting[1]
		CCID = ModemSetting[2]
		CarrierName = ModemSetting[3]
		APN = ModemSetting[4]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (CCID==None):
			CCID=None
		if (CarrierName<>None):
			CarrierId = self.__getEMS7CarrierId(CarrierName)
		else:
			CarrierId=None
		if(APN<>None):
			AccessPointId = self.__getEMS7AccessPointId(APN)
			# has there been any additions to the AccessPoint in EMS6, verify before migration
		else:
			AccessPointId=None
		if(Type<>None):
			ModemTypeId = self.__getEMS7ModemType(Type)
		else:
			ModemTypeId=None

		if (PointOfSaleId):
			if(Action=='Insert'):
				print " ....We are in the insret section of modelsettings record..."
				self.__EMS7Cursor.execute("insert into ModemSetting(PointOfSaleId,CarrierId,AccessPointId,ModemTypeId, CCID,LastModifiedGMT,\
								LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,CarrierId,AccessPointId,\
								ModemTypeId,CCID,LastModifiedGMT,LastModifiedByUserId))
				# There is no need of a mapping table as the record can be looked by a PaystationId which is a Primary key
				self.__EMS7Connection.commit()
				EMS7ModemSettingId = self.__EMS7Cursor.lastrowid
				print "EMS7ModemSettingId is %s" %EMS7ModemSettingId
				self.__EMS7Cursor.execute("insert into ModemSettingMapping(EMS6Id,EMS7Id) values(%s,%s)",(EMS6PaystationId,EMS7ModemSettingId))
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6PaystationId,tableName,IsMultiKey)

			if(Action=='Update'):
				self.__EMS7Cursor.execute("update ModemSetting set PointOfSaleId=%s, CarrierId=%s, AccessPointId=%s, ModemTypeId=%s, CCID=%s, LastModifiedGMT=%s, LastModifiedbyUserId=%s where Id=(select EMS7Id from ModemSettingMapping where EMS6Id=%s)",(PointOfSaleId,CarrierId,AccessPointId,ModemTypeId,CCID,LastModifiedGMT,LastModifiedByUserId,EMS6PaystationId))
				self.__EMS7Connection.commit()

	def __printModemSetting(self, ModemSetting):
		if(self.__verbose):
			print " -- PaystationId %s " %ModemSetting[0]
			print " -- Type %s " %ModemSetting[1]
			print " -- CCID %s " %ModemSetting[2]
			print " -- Carrier %s " %ModemSetting[3]
			print " -- APN %s " %ModemSetting[4]

	def __getEMS7CarrierId(self, CarrierName):
		if(CarrierName in self.__carriernameIdCache):
			return self.__carriernameIdCache[CarrierName]

		EMS7CarrierId=None
		self.__EMS7Cursor.execute("select Id from Carrier where Name=%s",(CarrierName))	
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CarrierId = row[0]

		self.__carriernameIdCache[CarrierName] = EMS7CarrierId

		return EMS7CarrierId

	def __getEMS7AccessPointId(self, APN):
		if(APN in self.__apnIdCache):
			return self.__apnIdCache[APN]

		EMS7AccessPointId = None
		self.__EMS7Cursor.execute("select Id from AccessPoint where Name=%s",(APN))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7AccessPointId = row[0]

		self.__apnIdCache[APN]= EMS7AccessPointId

		return EMS7AccessPointId
	
	def __getEMS7ModemType(self, ModemType):
		if(ModemType in self.__modemtypeIdCache):
			return self.__modemtypeIdCache[ModemType]

		EMS7ModemTypeId=None
		self.__EMS7Cursor.execute("select Id from ModemType where Name=%s",(ModemType))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7ModemTypeId = row[0]

		self.__modemtypeIdCache[ModemType] = EMS7ModemTypeId
		return EMS7ModemTypeId		
		
	def addEMS6LotSettingContentToEMS7(self, LotSettingContent, Action):
		print " ....adding lotsettingFileContent record....."
		tableName='SettingsFileContent'
		EMS6LotSettingId = LotSettingContent[0]
		FileContent = LotSettingContent[1]
		EMS7LotSettingId = self.__getEMS7LotSettingId(EMS6LotSettingId)
		if (EMS7LotSettingId):
			if(Action=='Insert'):
				# Note: While migrating there was error that the record already exist as some of the records have already been added when LotSetting table was being migrated, In this case are we simply overriding the previous entries
				self.__EMS7Cursor.execute("insert ignore into SettingsFileContent(SettingsFileId,Content) values(%s,%s)",(EMS7LotSettingId,FileContent))
				EMS7SettingFileContentId = self.__EMS7Cursor.lastrowid 
				#in case the records are just overwritten - the last row id returns zero and file content mapping record is not inserted in the mapping table as record already exists
				print " ..adding record in settingsFileContentMapping table with EMSSettingFileContentID....", EMS7SettingFileContentId
				self.__EMS7Cursor.execute("insert into SettingsFileContentMapping(EMS6Id,EMS7Id,DateAdded) values(%s,%s,%s)",(EMS6LotSettingId,EMS7SettingFileContentId,date.today()))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6LotSettingId,tableName,IsMultiKey)
			if(Action=='Update'):
				self.__EMS7Cursor.execute("select EMS7Id from SettingsFileContentMapping where EMS6Id=%s",(EMS6LotSettingId))
				EMS7SettingsFileContentId = self.__EMS7Cursor.lastrowid
				print "EMS7SettingsFileContentId %s" %EMS7SettingsFileContentId
				self.__EMS7Cursor.execute("update SettingsFileContent set SettingsFileId=%s,Content=%s where SettingsFileId=%s",(EMS7LotSettingId,FileContent,EMS7SettingsFileContentId))
				self.__EMS7Connection.commit()

	def __getEMS7LotSettingId(self, EMS6LotSettingId):
		if (EMS6LotSettingId in self.__lotsettingIdCache):
                        return self.__lotsettingIdCache[EMS6LotSettingId]

		EMS7LotSettingId = None
		self.__EMS7Cursor.execute("select distinct(EMS7Id) from SettingsFileMapping where EMS6Id=%s", (EMS6LotSettingId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7LotSettingId = row[0]
		
		self.__lotsettingIdCache[EMS6LotSettingId] = EMS7LotSettingId

		return EMS7LotSettingId

	def addCryptoKeyToEMS7(self, curCryptoKey, Action):
		print " In adding cryptokey to EMS7 method "
		dateAdded = date.today()
		self.__printCryptoKey(curCryptoKey)
		Type = curCryptoKey[0]
		KeyIndex = curCryptoKey[1]
		Hash = curCryptoKey[2]
		Expiry = curCryptoKey[3]
		Signature = curCryptoKey[4]
		Info = curCryptoKey[5]
		NextHash = curCryptoKey[6]
		Status = curCryptoKey[7]
		CreateDate = curCryptoKey[8]
		Comment = curCryptoKey[9]
#		self.__EMS7Cursor.execute("set innodb_lock_wait_timeout=200")
		
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into CryptoKey(KeyType,KeyIndex,ExpiryGMT,Hash,NextHash,Signature,Info,Comment,Status,CreatedGMT)\
							 values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(Type,KeyIndex,Expiry,Hash,NextHash,Signature,Info,Comment,Status,CreateDate))
			EMS7Id = self.__EMS7Cursor.lastrowid
			print " adding record in cryptoKey mapping table with EM7Id", EMS7Id
			self.__EMS7Cursor.execute("insert ignore into CryptoKeyMapping(Type,KeyIndex,EMS7Id,DateAdded) values(%s,%s,%s,%s)",(Type,KeyIndex,EMS7Id,dateAdded))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select EMS7Id from CryptoKeyMapping where Type=%s and KeyIndex=%s",(Type,KeyIndex))
			if(self.__EMS7Cursor.rowcount<>0):
				EMS7Id = self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("update CryptoKey set KeyType=%s,KeyIndex=%s,ExpiryGMT=%s,Hash=%s,NextHash=%s,Signature=%s,Info=%s,Comment=%s,Status=%s,CreatedGMT=%s where Id=%s",(Type,KeyIndex,Expiry,Hash,NextHash,Signature,Info,Comment,Status,CreateDate,EMS7Id[0]))
				self.__EMS7Connection.commit()

	def __printCryptoKey(self, curCryptoKey):
		if(self.__verbose):
			print " -- Type %s " %curCryptoKey[0]
			print " -- KeyIndex %s " %curCryptoKey[1]
			print " -- Hash %s " %curCryptoKey[2]
			print " -- Expiry %s " %curCryptoKey[3]
			print " -- Signature %s " %curCryptoKey[4]
			print " -- Info %s " %curCryptoKey[5]
			print " -- NextHash %s " %curCryptoKey[6]
			print " -- Status %s " %curCryptoKey[7]
			print " -- CreateDate %s " %curCryptoKey[8]
			print " -- Comment %s " %curCryptoKey[9]

	def addRestAccountToEMS7(self, RestAccount, Action):
		tableName='RestAccount'
		self.__printRestAccount(RestAccount)
		IsDeleted = 1
		Version = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustID = 0
		AccountName = RestAccount[0]
		EMS6CustomerId = RestAccount[1]
		TypeId= RestAccount[2]
		SecretKey = RestAccount[3]
		VirtualPS = RestAccount[4]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		PointOfSaleId = self.__getPointOfSaleForVirtualMachine(VirtualPS)
		if(Action=='Insert'):
			if(PointOfSaleId and EMS7CustomerId):
				self.__EMS7Cursor.execute("insert into RestAccount(CustomerId,PointOfSaleId,RestAccountTypeId,AccountName,SecretKey,IsDeleted,\
								LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s)",\
								(EMS7CustomerId,PointOfSaleId,TypeId,AccountName,SecretKey,IsDeleted,LastModifiedGMT,LastModifiedByUserId))
				EMS7RestAccountId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into RestAccountMapping(EMS6AccountName,EMS7RestAccountId) values(%s,%s)",(AccountName,EMS7RestAccountId))
				self.__EMS7Cursor.execute("update Paystation set PaystationTypeId=9 where SerialNumber=%s",(VirtualPS))
				#Below Code added by Ashok July 5
				self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=1, LastModifiedGMT = now() where CustomerId=%s and SubscriptionTypeId = 1100",(EMS7CustomerId))
								
				self.__EMS7Connection.commit()

		if(Action=='Update'):
			print"...updating rest account now..."
			EMS7RestAccountId=None
			EMS7RestAccountId = self.__getEMS7RestAccoutId(AccountName)
			print"the secretkey is",SecretKey
			print " Ponit of saleId is",PointOfSaleId
			print "RestAccountId is", EMS7RestAccountId  
			if(PointOfSaleId and EMS7RestAccountId):
				self.__EMS7Cursor.execute("update RestAccount set CustomerId=%s,PointOfSaleId=%s,RestAccountTypeId=%s,AccountName=%s,SecretKey=%s,IsDeleted=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,PointOfSaleId,TypeId,AccountName,SecretKey,IsDeleted,LastModifiedGMT,LastModifiedByUserId,EMS7RestAccountId))
				self.__EMS7Connection.commit()

	def __getPointOfSaleForVirtualMachine(self, VirtualPS):
		PointOfSaleId=None
		self.__EMS7Cursor.execute("select Id from PointOfSale where SerialNumber=%s",(VirtualPS))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PointOfSaleId = row[0]
		return PointOfSaleId

	def __getEMS7RestAccoutId(self, AccountName):
		EMS7RestAccountId=None
		self.__EMS7Cursor.execute("select distinct(EMS7RestAccountId) from RestAccountMapping where EMS6AccountName=%s",AccountName)
		if(self.__EMS7Cursor.rowcount<>0):
			row=self.__EMS7Cursor.fetchone()
			EMS7RestAccountId = row[0]
		return EMS7RestAccountId

	def __printRestAccount(self, restAccount):
		if(self.__verbose):
			print " -- RestAccount Name %s" %restAccount[0]
			print " -- CustomerId %s" %restAccount[1]
			print " -- TypeId %s" %restAccount[2]
			print " -- Secret Key %s" %restAccount[3]
			print " -- Virtual Paystation %s" %restAccount[4]

	def addRestLogToEMS7(self, RestLog, Action):
		tableName='RESTLog'
		self.__printRestLog(RestLog)
		RestAccountName = RestLog[0]
		EndpointName = RestLog[1]
		LoggingDate = RestLog[2]
		IsError = RestLog[3]
		CustomerId = RestLog[4]
		TotalCalls = RestLog[5]
		EMS7CustomerId=None
		# The record with missing Customer id is not getting migrated
		if (CustomerId):
			EMS7CustomerId = self.__getEMS7CustomerId(CustomerId)
			EMS7RestAccountId = self.__getEMS7RestAccountId(RestAccountName)
			if(Action=='Insert'):
				if (EMS7RestAccountId):
					self.__EMS7Cursor.execute("insert ignore into RestLog(CustomerId,RestAccountId,EndpointName,LoggingDate,IsError,TotalCalls) \
									values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId, EMS7RestAccountId,EndpointName,LoggingDate,IsError,TotalCalls))
					EMS7RestLogId = self.__EMS7Cursor.lastrowid
					self.__EMS7Cursor.execute("insert ignore into RestLogMapping(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId,EMS7Id) values(%s,%s,%s,%s,%s,%s)",(RestAccountName,EndpointName,LoggingDate,IsError,CustomerId,EMS7RestLogId))	
					self.__EMS7Connection.commit()
				else:
					IsMultiKey=0
					self.__InsertIntoDiscardedRecords(EMS7CustomerId,tableName,IsMultiKey)
			if(Action=='Update'):
				if (EMS7RestAccountId):
					self.__EMS7Cursor.execute("select distinct(EMS7Id) from RestLogMapping where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s",(RestAccountName,EndpointName,LoggingDate,IsError,CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7RestAccountLogId = row[0]
						print "EMS7RestLogAccountId is %s" %EMS7RestAccountLogId
						print "What is the value of the total calles that is comming up form EMS6 adatabasei %s" %TotalCalls
						self.__EMS7Cursor.execute("update RestLog set CustomerId=%s,RestAccountId=%s,EndpointName=%s,LoggingDate=%s,IsError=%s,TotalCalls=%s where Id=%s",(EMS7CustomerId, EMS7RestAccountId,EndpointName,LoggingDate,IsError,TotalCalls, EMS7RestAccountLogId))
						print " We have finished updating"	
						self.__EMS7Connection.commit()

	def __getEMS7RestAccountId(self, RestAccountName):
		if(RestAccountName in self.__restaccountnameIdCache):
			return self.__restaccountnameIdCache[RestAccountName]

		RestAccountId = None
		self.__EMS7Cursor.execute("select Id from RestAccount where AccountName=%s",(RestAccountName))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			RestAccountId = row[0] 

		self.__restaccountnameIdCache[RestAccountName] = RestAccountId

		return RestAccountId

	def __printRestLog(self, RestLog):
		if(self.__verbose):
			print " -- CustomerId %s " %RestLog[4]
			print " -- RestAccountName %s " %RestLog[0]
			print " -- EndpointName %s " %RestLog[1]
			print " -- LoggingDate %s " %RestLog[2]
			print " -- IsError %s" %RestLog[3]
			print " -- TotalCalls %s" %RestLog[5]

	def addRestSessionToken(self, RestSessionToken, Action):
		print " ...in RestSessionToken method..."
		tableName='RESTSessionToken'
		LastModifiedByUserId = 1
		LastModifiedGMT = date.today()
		self.__printRestSessionToken(RestSessionToken)
		AccountName = RestSessionToken[0]
		SessionToken = RestSessionToken[1]
		CreationDate = RestSessionToken[2]
		ExpiryDate = RestSessionToken[3]
		EMS7RestAccountId = self.__getEMS7RestAccountId(AccountName)
		
		if(EMS7RestAccountId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into RestSessionToken(RestAccountId,SessionToken,CreationDate,ExpiryDate,LastModifiedGMT,LastModifiedByUserId) \
								values(%s,%s,%s,%s,%s,%s)",(EMS7RestAccountId,SessionToken,CreationDate,ExpiryDate,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
				EMS7RestSessionToken = self.__EMS7Cursor.lastrowid
				print " creating restSessiontokenmapping record with id:", EMS7RestSessionToken
				self.__EMS7Cursor.execute("insert into RestSessionTokenMapping(EMS6AccountName,EMS7Id) values(%s,%s)",(AccountName,EMS7RestSessionToken))
			else:
				IsMultiKey=1
				self.__InsertIntoDiscardedRecords(AccountName,tableName,IsMultiKey)
			if(Action=='Update'):
				print "We never made it to the update section in the rest account session token"
				self.__EMS7Cursor.execute("select distinct(EMS7Id) from RestSessionTokenMapping where EMS6AccountName=%s",(AccountName))
				if(self.__EMS7Cursor.rowcount<>0):
					EMS7RestSessionTokenId = self.__EMS7Cursor.fetchone()
					print "EMS7 RestSessionToken Id is %s" %EMS7RestSessionTokenId
					print "Rest Session Token comming up from the EMS6 is %s" %SessionToken
					if(EMS7RestSessionTokenId):
						self.__EMS7Cursor.execute("update RestSessionToken set RestAccountId=%s,SessionToken=%s,CreationDate=%s,ExpiryDate=%s where Id=%s",(EMS7RestAccountId,SessionToken,CreationDate,ExpiryDate,EMS7RestSessionTokenId[0]))
						self.__EMS7Connection.commit()

	def __printRestSessionToken(self, RestSessionToken):
		if(self.__verbose):
			print " -- AccountName %s " %RestSessionToken[0]
			print " -- SessionToken %s " %RestSessionToken[1]
			print " -- CreationDate %s " %RestSessionToken[2]		
			print " -- ExpiryDate %s " %RestSessionToken[3]

	def addRestLogTotalCall(self, RestLogTotalCall,Action):
		print " ..in AddRestLogToCallMethod...."
		tableName='RESTLogTotalCall'
		LastModifiedGMT=date.today()
		LastModifiedByUserId = 1
		self.__printRestLogTotalCall(RestLogTotalCall)
		AccountName = RestLogTotalCall[0]
		LoggingDate = RestLogTotalCall[1]
		TotalCalls =  RestLogTotalCall[2]
		EMS7RestAccountId = self.__getEMS7RestAccountId(AccountName)
		print " EMS7RestAccountId is ..", EMS7RestAccountId
		if(EMS7RestAccountId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert ignore into RestLogTotalCall(RestAccountId,LoggingDate,TotalCalls,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMS7RestAccountId,LoggingDate,TotalCalls,LastModifiedGMT,LastModifiedByUserId))
				EMS7RestLogTotalCall = self.__EMS7Cursor.lastrowid
				print " inserting into RestLogTotalCall mapping table with EMS7RestLogTotalCall value ", EMS7RestLogTotalCall
				self.__EMS7Cursor.execute("insert into RestLogTotalCallMapping(RESTAccountName,LoggingDate,EMS7Id) values(%s,%s,%s)",(AccountName,LoggingDate,EMS7RestLogTotalCall))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (AccountName,LoggingDate)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			if(Action=='Update'):
				print " ....updating RestLogTotalCall data...."
				self.__EMS7Cursor.execute("select distinct(EMS7Id) from RestLogTotalCallMapping where RESTAccountName=%s and LoggingDate=%s",(AccountName,LoggingDate))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7Id = row[0]
					self.__EMS7Cursor.execute("update RestLogTotalCall set RestAccountId=%s,LoggingDate=%s,TotalCalls=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7RestAccountId,LoggingDate,TotalCalls,LastModifiedGMT,LastModifiedByUserId,EMS7Id))
					self.__EMS7Connection.commit()

	def __printRestLogTotalCall(self, RestLogTotalCall):
		if(self.__verbose):
			print " -- AccountName %s " %RestLogTotalCall[0]
			print " -- LoggingDate %s " %RestLogTotalCall[1]
			print " -- TotalCalls %s " %RestLogTotalCall[2]	

#	def addCustomerWsCal(self, CustomerWsCal):
#		tableName='CustomerWsCal'
#		self.__printCustomerWsCal(CustomerWsCal)
#		LastModifiedGMT = date.today()
 #               LastModifiedByUserId = 1
#		EMS6CustomerId = CustomerWsCal[0]
#		EndPointId = CustomerWsCal[1]
#		CalInUse =  CustomerWsCal[2]
#		CalPurchase = CustomerWsCal[3]
#		Description = CustomerWsCal[4] 
#		CustomerWsCalId = CustomerWsCal[5]
#		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
#		if(EMS7CustomerId):
#			self.__EMS7Cursor.execute("insert into CustomerWebServiceCal(CustomerId,WebServiceEndPointTypeId,CalInUse,CalPurchase,Description) \
#				values(%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,CalInUse,CalPurchase,Description))
#			self.__EMS7Connection.commit()
#		else:
#			IsMultiKey=0
#			self.__InsertIntoDiscardedRecords(CustomerWsCalId,tableName,IsMultiKey)

	def __printCustomerWsCal(self, CustomerWsCal):
		if(self.__verbose):
			print " -- EMS7CustomerId %s " %CustomerWsCal[0]
			print " -- EndPointId %s " %CustomerWsCal[1]
			print " -- CalInUse %s " %CustomerWsCal[2]
			print " -- CalPurchase %s " %CustomerWsCal[3]
			print " -- Description %s " %CustomerWsCal[4]		

       # def addCustomerWsToken(self, CustomerWsToken):
	#	tableName='CustomerWsToken'
         #       self.__printCustomerWsToken(CustomerWsToken)
          #      EMS6CustomerId = CustomerWsToken[0]
           #     Token = CustomerWsToken[1]
            #    EndPointId=  CustomerWsToken[2]
             #   WsInUse = CustomerWsToken[3]
#		CustomerWsTokenId = CustomerWsToken[4]
 #               EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
  #              if(EMS7CustomerId):
   #                     self.__EMS7Cursor.execute("insert into CustomerWsToken(CustomerId,Token,EndPointId,WsInUse) values(%s,%s,%s,%s)",(EMS7CustomerId,Token,EndPointId,WsInUse))
    #                    self.__EMS7Connection.commit()
#		else:
#			IsMultiKey=0
#			self.__InsertIntoDiscardedRecords(CustomerWsTokenId,tableName,IsMultiKey)

        def __printCustomerWsToken(self, CustomerWsToken):
		if(self.__verbose):
			print " -- EMS7CustomerId %s " %CustomerWsToken[0]
			print " -- Token %s " %CustomerWsToken[1]
			print " -- EndPointId %s " %CustomerWsToken[2]
			print " -- WsInUse %s " %CustomerWsToken[3]

	def addCustomerWebServiceCalToEMS7(self, CustomerWebServiceCal, Action):
		print " .....in addCustomerWsCall method...."
		self.__printCustomerWebServiceCal(CustomerWebServiceCal)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS6CustomerId = CustomerWebServiceCal[0]
		EndPointId = CustomerWebServiceCal[1]
		CalInUse = CustomerWebServiceCal[2]
		CalPurchase = CustomerWebServiceCal[3]
		Description = CustomerWebServiceCal[4]
		EMS6CustomerWsCalId = CustomerWebServiceCal[5]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		print" EMS7CustomerId is ", EMS7CustomerId
		if(EMS7CustomerId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into CustomerWebServiceCal (CustomerId,WebServiceEndPointTypeId,CalInUse,CalPurchase,Description,LastModifiedGMT, \
					LastModifiedByUserId) value(%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,CalInUse,CalPurchase,Description,LastModifiedGMT,LastModifiedByUserId))
				EMS7CustomerWsCalId = self.__EMS7Cursor.lastrowid
				print " inserting into CustomerWSCall Mapping record with EMS7CustomerWsCalId ..", EMS7CustomerWsCalId
				if(EMS7CustomerWsCalId):
					self.__EMS7Cursor.execute("insert into CustomerWebServiceCalMapping(EMS6Id,EMS7Id) values(%s,%s)",(EMS6CustomerWsCalId,EMS7CustomerWsCalId))
				self.__EMS7Connection.commit()

			if(Action=='Update'):
				print " ..in updating customerWSCall record..."
				self.__EMS7Cursor.execute("select EMS7Id from CustomerWebServiceCalMapping where EMS6Id=%s",(EMS6CustomerWsCalId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7CustomerWebServiceCalId = row[0]
					self.__EMS7Cursor.execute("update CustomerWebServiceCal set CustomerId=%s,WebServiceEndPointTypeId=%s,CalInUse=%s,CalPurchase=%s,Description=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,EndPointId,CalInUse,CalPurchase,Description,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerWebServiceCalId))
					self.__EMS7Connection.commit()
			if(Action=='Delete'):
				print "....Are we here yet!..deleting CustomerWSCall record...."
				try:
					self.__EMS7Cursor.execute("delete from CustomerWebServiceCal where Id = (select EMS7Id from CustomerWebServiceCalMapping where EMS6Id=%s)",(EMS6CustomerWsCalId))
					self.__EMS7Connection.commit()

				except mdb.Error, e:
					print 'Error: ',e[0]

	def __printCustomerWebServiceCal(self, CustomerWebServiceCal):
		if(self.__verbose):
			print " -- EMS6CustomerId %s " %CustomerWebServiceCal[0]
			print " -- EndPointId %s "  %CustomerWebServiceCal[1]
			print " -- CalInUse %s " %CustomerWebServiceCal[2]
			print " -- CalPurchase %s"  %CustomerWebServiceCal[3]
			print " -- Description %s" %CustomerWebServiceCal[4]

#Note : the actual name of the table in EMS7 is WebServiceEndPoint that maps to EMS6.CustomerWsToken
	def addCustomerWsTokenToEMS7(self, CustomerWsToken, Action):
		self.__printCustomerWsToken(CustomerWsToken)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		IsDeleted = 0
		EMS6CustomerId = CustomerWsToken[0]
		Token = CustomerWsToken[1]
		EndPointId = CustomerWsToken[2]
		WsInUse = CustomerWsToken[3]
		EMS6CustomerWsTokenId = CustomerWsToken[4]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(EMS7CustomerId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert IGNORE into WebServiceEndPoint(CustomerId,WebServiceEndPointTypeId,Token,IsDeleted,LastModifiedGMT,LastModifiedByUserId) \
					values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,Token,IsDeleted,LastModifiedGMT,LastModifiedByUserId))
				EMS7CustomerWsTokenId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into CustomerWsTokenMapping(EMS6Id,EMS7Id,ModifiedDate) values(%s,%s,%s)",(EMS6CustomerWsTokenId,EMS7CustomerWsTokenId,LastModifiedGMT))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				self.__EMS7Cursor.execute("select EMS7Id from CustomerWsTokenMapping where EMS6Id=%s",(EMS6CustomerWsTokenId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7CustomerWsTokenId = row[0] 	
					self.__EMS7Cursor.execute("update WebServiceEndPoint set CustomerId=%s,WebServiceEndPointTypeId=%s,Token=%s,IsDeleted=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,EndPointId,Token,IsDeleted,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerWsTokenId))
					self.__EMS7Connection.commit()

	def __printCustomerWsToken(self, CustomerWsToken):
		if(self.__verbose):
			print " -- EMS7CustomerId %s" %CustomerWsToken[0]
			print " -- Token %s" %CustomerWsToken[1]
			print " -- EndPointId %s " %CustomerWsToken[2]
			print " -- WsInUse %s " %CustomerWsToken[3]

	def addWebServiceCallLog(self, WsCallLog, Action):	
		print " We are in the addWebServiceCallLog ..."
#		self.__printWebServiceCallLog(WsCallLog)
		Token = WsCallLog[0]
		CallDate = WsCallLog[1]
		CustomerId = WsCallLog[2]
		TotalCall = WsCallLog[3]
		EMS7CustomerId = self.__getEMS7CustomerId(CustomerId) 
		EMS7WebServiceEndPointId = self.__getEMS7WebServiceEndPointId(EMS7CustomerId,Token)
		print "EMS7WebServiceEndPointId %s" %EMS7WebServiceEndPointId
		if(EMS7WebServiceEndPointId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert ignore into WebServiceCallLog(WebServiceEndPointId,DateLocal,TotalCalls) values(%s,%s,%s)",(EMS7WebServiceEndPointId,CallDate,TotalCall))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				print "This is an update section so you better update"
				print "WebServiceEndPointId %s" %EMS7WebServiceEndPointId
				print "DateLocal %s" %CallDate
				print "TotalCalls %s" %TotalCall
				self.__EMS7Cursor.execute("update WebServiceCallLog set WebServiceEndPointId=%s,DateLocal=%s,TotalCalls=%s where DateLocal=%s",(EMS7WebServiceEndPointId,CallDate,TotalCall,CallDate))
				print " we are dne updateing"
				self.__EMS7Connection.commit()
			
	def __getEMS7WebServiceEndPointId(self,EMS7CustomerId,Token):
		EMS7WebServiceEndPointId = None
		self.__EMS7Cursor.execute("select Id from WebServiceEndPoint where CustomerId=%s and Token=%s",(EMS7CustomerId,Token))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7WebServiceEndPointId = row[0]
		return EMS7WebServiceEndPointId
	
	def addRatesToEMS7(self, Rates, Action):
		print" ...migrating rates table..record is", Rates
		IsMultiKey=0
		tableName='UnifiedRate'
		self.__printRates(Rates)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		PaystationId = Rates[0]
		print "Paystation Id is %s" %PaystationId
		TicketNumber = Rates[1]
		PurchasedDate = Rates[2]
		EMS6CustomerId = Rates[3]
		RateName = Rates[8]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			if (EMS7CustomerId):
				self.__EMS7Cursor.execute("insert ignore into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) \
								values(%s,%s,%s,%s)",(EMS7CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
				"""The mapping enabled -- sanjay 18July"""
				UnifiedRateId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert ignore into RateToUnifiedRateMapping(PaystationId,PurchasedDate,TicketNumber,EMS7UnifiedRateId) values(%s,%s,%s,%s)",(PaystationId,PurchasedDate,TicketNumber,UnifiedRateId))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select EMS7UnifiedRateId from RateToUnifiedRateMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))	
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				EMS7UnifiedRateId = row[0]
				self.__EMS7Cursor.execute("update UnifiedRate set CustomerId=%s, Name=%s, LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId,EMS7UnifiedRateId))
				self.__EMS7Connection.commit()

	def addEMSRatesToUnifiedRate(self, Rates, Action):
		print " .....adding EMS rate to unified rate...."
		TableName='EMSRate'
		self.__printRates(Rates)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS6EMSRateId = Rates[0]
		EMS6CustomerId = Rates[1]
		RateName = Rates[2]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(EMS7CustomerId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert ignore into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) \
								values(%s,%s,%s,%s)",(EMS7CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
				UnifiedRateId = self.__EMS7Cursor.lastrowid
				print " record creation in EMSRatetoUnifiedRate mapping with Unified rate id as", UnifiedRateId
				self.__EMS7Cursor.execute("insert into EMSRateToUnifiedRateMapping(EMS6RateId,EMS7RateId) values(%s,%s)",(EMS6EMSRateId,UnifiedRateId))
				self.__EMS7Connection.commit()
				"""The insert above creates a record with unified rate of 0. Pl check the validity of such record"""
	## This section is being diables because the row gets added when the calling module in incremental migration adds and updates the row prioir to calling this section. DO NOT ENABLE THIS MODULE !!!
	#		if(Action=='Update'):
	#			if(RateName<>''):
	#				self.__EMS7Cursor.execute("select EMS7RateId from EMSRateToUnifiedRateMapping where EMS6RateId=%s",(EMS6EMSRateId))
	#				if(self.__EMS7Cursor.rowcount<>0):
	#					row = self.__EMS7Cursor.fetchone()
	#					EMS7EMSRateId = row[0]
	#					print "This is where the Problems appears"
	#					self.__EMS7Cursor.execute("update UnifiedRate set CustomerId=%s,Name=%s where Id=%s",(EMS7CustomerId,RateName,EMS7EMSRateId))
	#					print "EMS7 Unified Rate Id is %s" %EMS7EMSRateId
	#			self.__EMS7Connection.commit()

	def __printRates(self, Rates):
		if(self.__verbose):
			print " -- CustomerId %s" %Rates[0]
			print " -- RateName %s " %Rates[1] 	

	def addReplenishToEMS7(self, Replenish, Action):
		print " in adding replenish data to EMS 7 method"
		tableName='Replenish'
		ModifiedDate = date.today()
		self.__printReplenish(Replenish)
		EMS6PaystationId = Replenish[0]
		Date = Replenish[1]
		Number = Replenish[2]
		TypeId = Replenish[3]
		Tube1Type = Replenish[4]
		Tube1ChangedCount = Replenish[5]
		Tube1CurrentCount = Replenish[6]
		Tube2Type = Replenish[7]
		Tube2ChangedCount = Replenish[8]
		Tube2CurrentCount = Replenish[9]
		Tube3Type = Replenish[10]
		Tube3ChangedCount = Replenish[11]
		Tube3CurrentCount = Replenish[12]
		Tube4Type = Replenish[13]
		Tube4ChangedCount = Replenish[14]
		Tube4CurrentCount = Replenish[15]
		CoinBag005AddedCount = Replenish[16]
		CoinBag010AddedCount = Replenish[17]
		CoinBag025AddedCount = Replenish[18]
		CoinBag100AddedCount = Replenish[19]
		CoinBag200AddedCount = Replenish[20]	
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(Action=='Insert'):
			print" inserting data with poiintOdSaleid",PointOfSaleId 
			if (PointOfSaleId):
				self.__EMS7Cursor.execute("Insert into Replenish(PointOfSaleId,ReplenishTypeId,ReplenishGMT,Number,Tube1Type,Tube1ChangedCount,\
				Tube1CurrentCount,Tube2Type,Tube2ChangedCount,Tube2CurrentCount,Tube3Type,Tube3ChangedCount,Tube3CurrentCount,Tube4Type,\
				Tube4ChangedCount,Tube4CurrentCount,CoinBag005AddedCount,CoinBag010AddedCount,CoinBag025AddedCount,CoinBag100AddedCount,CoinBag200AddedCount) \
				values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (PointOfSaleId, TypeId, Date, Number, Tube1Type, \
				Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount,Tube2CurrentCount,Tube3Type,Tube3ChangedCount, \
				Tube3CurrentCount,Tube4Type, Tube4ChangedCount,Tube4CurrentCount,CoinBag005AddedCount,CoinBag010AddedCount,CoinBag025AddedCount,CoinBag100AddedCount,CoinBag200AddedCount))
				EMS7ReplenishId = self.__EMS7Cursor.lastrowid
				print " Inserting data in RelkenishMapping table with EMS7ReplesnishId"
				print "EMS7 Replenish Id %s" %EMS7ReplenishId
				self.__EMS7Cursor.execute("insert into ReplenishMapping(PaystationId,Date,Number,EMS7Id,ModifiedDate) values(%s,%s,%s,%s,%s)",(EMS6PaystationId,Date,Number,EMS7ReplenishId,ModifiedDate))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,Date,Number)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		if(Action=='Update'):
			print " ....updating replenish table....."
			self.__EMS7Cursor.execute("select distinct(EMS7Id) from ReplenishMapping where PaystationId=%s and Date=%s and Number=%s",(EMS6PaystationId,Date,Number))
			if (self.__EMS7Cursor.rowcount<>0):
				EMS7ReplenishId = self.__EMS7Cursor.fetchone()
				print "EMS7 Replenish Id %s" %EMS7ReplenishId
				self.__EMS7Cursor.execute("update Replenish set PointOfSaleId=%s,ReplenishTypeId=%s,ReplenishGMT=%s,Number=%s,Tube1Type=%s,Tube1ChangedCount=%s,Tube1CurrentCount=%s,Tube2Type=%s,Tube2ChangedCount=%s,Tube2CurrentCount=%s,Tube3Type=%s,Tube3ChangedCount=%s,Tube3CurrentCount=%s,Tube4Type=%s,Tube4ChangedCount=%s,Tube4CurrentCount=%s,CoinBag005AddedCount=%s,CoinBag010AddedCount=%s,CoinBag025AddedCount=%s,CoinBag100AddedCount=%s,CoinBag200AddedCount=%s where Id=%s",(PointOfSaleId, TypeId, Date, Number, Tube1Type,Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount,Tube2CurrentCount,Tube3Type,Tube3ChangedCount,Tube3CurrentCount,Tube4Type, Tube4ChangedCount,Tube4CurrentCount,CoinBag005AddedCount,CoinBag010AddedCount,CoinBag025AddedCount,CoinBag100AddedCount,CoinBag200AddedCount,EMS7ReplenishId[0]))
				self.__EMS7Connection.commit()
	
	def __printReplenish(self, Replenish):
		if(self.__verbose):
			print " -- PaystationId %s " %Replenish[0]
			print " -- Date %s" %Replenish[1]
			print " -- Number %s" %Replenish[2]
			print " -- TypeId %s" %Replenish[3]
			print " -- Tube1Type %s" %Replenish[4]
			print " -- Tube1ChangedCount %s" %Replenish[5]
			print " -- Tube1CurrentCount %s" %Replenish[6]
			print " -- Tube2Type %s" %Replenish[7]
			print " -- Tube2ChangedCount %s"  %Replenish[8]
			print " -- Tube2CurrentCount %s" %Replenish[9]
			print " -- Tube3Type %s" %Replenish[10] 
			print " -- Tube3ChangedCount %s" %Replenish[11]
			print " -- Tube3CurrentCount %s" %Replenish[12]
			print " -- Tube4Type %s " %Replenish[13]
			print " -- Tube4ChangedCount %s" %Replenish[14]
			print " -- Tube4CurrentCount %s" %Replenish[15]
			print " -- CoinBag005AddedCount %s" %Replenish[16]
			print " -- CoinBag010AddedCount %s " %Replenish[17]
			print " -- CoinBag025AddedCount %s" %Replenish[18]
			print " -- CoinBag100AddedCount %s" %Replenish[19]
			print " -- CoinBag200AddedCount %s" %Replenish[20]

	def addCardRetryTransactionToEMS7(self,CardRetryTransaction, Action):
		print " .in add card retry transaction to EMS7 method...."
		if(Action=='Insert'):
			tableName='CardRetryTransaction'
			self.__printCardRetryTransaction(CardRetryTransaction)
			EMS6PaystationId = CardRetryTransaction[0]
			PurchaseDate = CardRetryTransaction[1]
			TicketNumber = CardRetryTransaction[2]
			LastRetryDate = CardRetryTransaction[3]
			NumRetries = CardRetryTransaction[4]
			CardHash = CardRetryTransaction[5]
			TypeId = CardRetryTransaction[6]
			CardData = CardRetryTransaction[7]
			CardExpiry = CardRetryTransaction[8]
			Amount = CardRetryTransaction[9]
			CardType = CardRetryTransaction[10]
			Last4DigitsOfCardNumber = CardRetryTransaction[11]
			CreationDate = CardRetryTransaction[12]
			BadCardHash = CardRetryTransaction[13]
			IgnoredBadCard = CardRetryTransaction[14]
			LastResponseCode = CardRetryTransaction[15]
			IsRFID = CardRetryTransaction[16]
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			if (PointOfSaleId):
				self.__EMS7Cursor.execute("insert into CardRetryTransaction(PointOfSaleId,PurchasedDate,TicketNumber,LastRetryDate,\
					NumRetries,CardHash,CardRetryTransactionTypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,\
					CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					(PointOfSaleId,PurchaseDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,\
					Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoredBadCard,LastResponseCode,IsRFID))
				EMS7CardRetryTransactionId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into CardRetryTransactionMapping(PaystationId,PurchasedDate,TicketNumber,EMS7CardRetryTransactionId) values(%s,%s,%s,%s)",(EMS6PaystationId,PurchaseDate,TicketNumber,EMS7CardRetryTransactionId))

				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,PurchaseDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		if(Action=='Update'):
			Split=CardRetryTransaction.split(',')
			EMS6PaystationId=Split[0]            
			PurchasedDate=Split[1]           
			TicketNumber=Split[2]            
			LastRetryDate=Split[3]           
			NumRetries=Split[4]              
			CardHash=Split[5]                
			TypeId=Split[6]                  
			CardData=Split[7]                
			CardExpiry=Split[8]              
			Amount=Split[9]                  
			CardType=Split[10]                
			Last4DigitsOfCardNumber=Split[11] 
			CreationDate=Split[12]            
			BadCardHash=Split[13]             
			IgnoreBadCard=Split[14]           
			LastResponseCode=Split[15]        
			IsRFID=Split[16]
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			EMS7CardRetryTransactionId = self.__getEMS7CardRetryTransactionId(EMS6PaystationId,PurchasedDate,TicketNumber)
			if (PointOfSaleId and EMS7CardRetryTransactionId):
				self.__EMS7Cursor.execute("update CardRetryTransaction set PointOfSaleId=%s,PurchasedDate=%s,TicketNumber=%s,LastRetryDate=%s,\
					NumRetries=%s,CardHash=%s,CardRetryTransactionTypeId=%s,CardData=%s,CardExpiry=%s,Amount=%s,CardType=%s,Last4DigitsOfCardNumber=%s,\
					CreationDate=%s,BadCardHash=%s,IgnoreBadCard=%s,LastResponseCode=%s,IsRFID=%s where Id=%s",(PointOfSaleId, PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID,EMS7CardRetryTransactionId ))
				self.__EMS7Connection.commit()

	def __getEMS7CardRetryTransactionId(self,PaystationId,PurchasedDate,TicketNumber):
		EMS7CardRetryTransactionId=None
		self.__EMS7Cursor.execute("select EMS7CardRetryTransactionId from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CardRetryTransactionId = row[0]

		return EMS7CardRetryTransactionId

	def __printCardRetryTransaction(self, CardRetryTransaction):
		if(self.__verbose):
			print " -- Paystation ID %s " %CardRetryTransaction[0]
			print " -- PurchaseDate %s " %CardRetryTransaction[1]
			print " -- TicketNumber %s " %CardRetryTransaction[2]
			print " -- LastRetryDate %s " %CardRetryTransaction[3]
			print " -- NumRetries %s " %CardRetryTransaction[4]
			print " -- CardHash %s " %CardRetryTransaction[5]
			print " -- TypeId %s " %CardRetryTransaction[6]
			print " -- CardDate %s " %CardRetryTransaction[7]
			print " -- Card Expiry %s " %CardRetryTransaction[8]
			print " -- Amount %s " %CardRetryTransaction[9]
			print " -- CardType %s " %CardRetryTransaction[10]
			print " -- Last4DigitOfCardNUmber %s " %CardRetryTransaction[11]
			print " -- CreationDate %s " %CardRetryTransaction[12]
			print " -- BadCardHash %s " %CardRetryTransaction[13]
			print " -- IgnoredBadCard %s" %CardRetryTransaction[14]
			print " -- LastResponseCode %s " %CardRetryTransaction[15]
			print " -- IsRFID %s " %CardRetryTransaction[16]
		
	def addCustomerCardTypeToEMS7(self, CustomerCardType, Action):
		print"...in adding customer card type to EMS 7 method...."
		self.__printCustomerCardType(CustomerCardType)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 0
		AuthorizationTypeId=0
		EMS6CustomerId = CustomerCardType[0]
		version = CustomerCardType[1]
		Name = CustomerCardType[2]
		Track2RegEx = CustomerCardType[3]
		CheckDigitAlg = CustomerCardType[4]
		Description = CustomerCardType[5]
		AuthorizationMethod = CustomerCardType[6]
		EMS6CustomerCardTypeId = CustomerCardType[7]
		CardTypeId = 3
		if (AuthorizationMethod=='External Server'):
			AuthorizationTypeId=2
		if (AuthorizationMethod=='EMS List'):
			AuthorizationTypeId=1
		if (AuthorizationMethod=='None'):
			AuthorizationTypeId=0
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):	
			self.__EMS7Cursor.execute("insert Ignore into CustomerCardType(CustomerId,CardTypeId,AuthorizationTypeId,Name,Track2Pattern,Description,\
				IsDigitAlgorithm,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
				(EMS7CustomerId,CardTypeId,AuthorizationTypeId,Name,Track2RegEx,Description,CheckDigitAlg,version,LastModifiedGMT,LastModifiedByUserId))
			EMS7CustomerCardTypeId=self.__EMS7Cursor.lastrowid
			print "creating customer card type mapping record with customer cart type id", EMS7CustomerCardTypeId
			self.__EMS7Cursor.execute("insert into CustomerCardTypeMapping(EMS6Id,EMS7Id,ModifiedDate) values(%s,%s,%s)",(EMS6CustomerCardTypeId,EMS7CustomerCardTypeId,LastModifiedGMT))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " ....updating the customer card type..."
			self.__EMS7Cursor.execute("select EMS7Id from CustomerCardTypeMapping where EMS6Id=%s",(EMS6CustomerCardTypeId))
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerCardTypeId = row[0]
			self.__EMS7Cursor.execute("update CustomerCardType set CustomerId=%s,CardTypeId=%s,AuthorizationTypeId=%s,Name=%s,Track2Pattern=%s,Description=%s,IsDigitAlgorithm=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,CardTypeId,AuthorizationTypeId,Name,Track2RegEx,Description,CheckDigitAlg,version,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerCardTypeId))
			self.__EMS7Connection.commit()
	
	def __printCustomerCardType(self,CustomerCardType):
		if(self.__verbose):
			print " -- Customer ID %s" %CustomerCardType[0]
			print " -- version %s " %CustomerCardType[1]
			print " -- Name %s " %CustomerCardType[2]
			print " -- Track2RegEx %s " %CustomerCardType[3]
			print " -- CheckDigitAlg %s " %CustomerCardType[4]
			print " -- Description %s " %CustomerCardType[5]
			print " -- AuthorizationMethod %s " %CustomerCardType[6]

	def addPaystationGroupToEMS7(self, PaystationGroup, Action):
		print " Adding paystaionFroup data to EMS 7"
		self.__printPaystationGroup(PaystationGroup)
		LastModifiedGMT = date.today()
		LastModifiedByUserId =1 
		version=0
		print "RouteID is set to 3- Others as a default until further clarification"
		RouteTypeId = 3
		Version=0
		EMS6PaystationGroupId = PaystationGroup[0]
		EMS6CustomerId = PaystationGroup[1]
		Name = PaystationGroup[2]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		print " got EMS7CustomerId id", EMS7CustomerId
		if(EMS7CustomerId):
			if(Action=='Insert'):
				print " ..inserting record for paystationGroup route  "
				self.__EMS7Cursor.execute("insert into Route(CustomerId,RouteTypeId,Name,VERSION,LastModifiedGMT,LastModifiedByUserId) \
					values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,RouteTypeId,Name,Version,LastModifiedGMT,LastModifiedByUserId))
				EMS7PaystationRouteId =  self.__EMS7Cursor.lastrowid
				print " inserting record in paystationGroupRouteMapping table with EMS7PaystationRouteId value", EMS7PaystationRouteId
				self.__EMS7Cursor.execute("insert into PaystationGroupRouteMapping(PaystationGroupId,EMS7Id,DateAdded) \
								values(%s,%s,%s)",(EMS6PaystationGroupId,EMS7PaystationRouteId,LastModifiedGMT))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				print " ....updateing paystationgroup record ..."
				self.__EMS7Cursor.execute("select EMS7Id from PaystationGroupRouteMapping where PaystationGroupId=%s",(EMS6PaystationGroupId))
				row = self.__EMS7Cursor.fetchone()
				EMS7PaystationRouteId = row[0]
				self.__EMS7Cursor.execute("update Route set CustomerId=%s, RouteTypeId=%s, Name=%s, VERSION=%s, LastModifiedGMT=%s, LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,RouteTypeId,Name,Version,LastModifiedGMT,LastModifiedByUserId,EMS7PaystationRouteId))	
				self.__EMS7Connection.commit()
#EMS7.POSRoute=EMS6Paystation_PaystationGroup
	def addPOSRouteToEMS7(self,PaystationGroup,Action):
		print " ...in migrating paystation_paystation_group data  method...."
		EMS6PaystationGroupId = PaystationGroup[0]
		EMS6PaystationId = PaystationGroup[1]
		VERSION = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId=1
		EMS7RouteId = self.__getEMS7RouteId(EMS6PaystationGroupId)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(PointOfSaleId and EMS7RouteId):
			if(Action=='Insert'):
				print"in insert the paystation paystation group data .. step 1"
				self.__EMS7Cursor.execute("insert ignore into RoutePOS(RouteId,PointOfSaleId,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMS7RouteId[0],PointOfSaleId,VERSION,LastModifiedGMT,LastModifiedByUserId))
				EMS7POSRouteId = self.__EMS7Cursor.lastrowid
				if(EMS7POSRouteId):
					print "in step2 - inserting in the mapping table with EMS7POSRouteId ", EMS7POSRouteId
					self.__EMS7Cursor.execute("insert into RoutePOSMapping(EMS7Id,PaystationGroupId,PaystationId,DateAdded) values(%s,%s,%s,%s)",(EMS7POSRouteId,EMS6PaystationGroupId,EMS6PaystationId,LastModifiedGMT))
					self.__EMS7Connection.commit()
			if(Action=='Update'):
				print " ...in updating paystation paystation group data..."
				print "step3"
				self.__EMS7Cursor.execute("select EMS7Id from RoutePOSMapping where PaystationId=%s and PaystationGroupId=%s",(EMS6PaystationId,EMS6PaystationGroupId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7RoutePOSId = row[0]
					self.__EMS7Cursor.execute("update RoutePOS set RouteId=%s,PointOfSaleId=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7RouteId,PointOfSaleId,VERSION,LastModifiedGMT,LastModifiedByUserId, EMS7POSRouteId))
					self.__EMS7Connection.commit()
				
	def printPOSRouteToEMS7(self, PaystationGroup):
		if(self.__verbose):
			print " -- EMS6PaystationGroupId %s" %PaystationGroup[0]
			print " -- EMS6PaystationId %s " %PaystationGroup[1]

	def __getEMS7RouteId(self, EMS6PaystationGroupId):
		if(EMS6PaystationGroupId in self.__paystationGroupIdCache):
			return self.__paystationGroupIdCache[EMS6PaystationGroupId]	
		EMS7RouteId=None
		self.__EMS7Cursor.execute("select EMS7Id from PaystationGroupRouteMapping where PaystationGroupId=%s",(EMS6PaystationGroupId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchall()
			EMS7RouteId=row[0]

		self.__paystationGroupIdCache[EMS6PaystationGroupId] = EMS7RouteId

		return EMS7RouteId
		
	def __printPaystationGroup(self, PaystationGroup):
		print " -- CustomerId %s" %PaystationGroup[0]
		print " -- Name %s" %PaystationGroup[1]

#Customer Card is Prepopulated table 
	
#	def addCollectionTypeToEMS7(self,CollectionType):
#		self.__printCollectionType(CollectionType)
#		LastModifiedGMT = date.today()
 #               LastModifiedByUserId = 0
#		Id = CollectionType[0]
#		Name = CollectionType[1]
#		IsInactive = CollectionType[2]
#		self.__EMS7Cursor.execute("insert into CollectionType(Id,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(Id,Name,LastModifiedGMT,LastModifiedByUserId))
#		self.__EMS7Connection.commit()

#	def __printCollectionType(self, CollectionType):
	#	print " -- CollectionTypeId %s " %CollectionType[0]
	#	print " -- Name %s " %CollectionType[1]

	def addValueCardToEMS7(self,ValueCard, Action):
		print " ....in adding value card to EMS7 method...."
		if(Action=='Insert'):
			print " Calling the function to insert Value card"
			LastModifiedGMT = date.today()
			LastModifiedByUserId = 1 
			IsForNonOverlappingUse = 0 
			GracePeriodMinutes = None
			CardBeginGMT = None
			CardExpireGMT = None
			NumberOfUses =None 
			MaxNumberOfUses = None
			Comment = None
			IsActive =1 
			EMS6CustomerId = ValueCard[0]
			VERSION =1 
			AddedGMT = date.today()
			EMS6RegionId = ValueCard[1]
			EMS6PaystationId = ValueCard[2]
			print " EMS6PaystatId %s" %EMS6PaystationId
			CustomCardData = ValueCard[3]
			SmartCardData = ValueCard[4]
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			print "PointOfSaleId %s" %PointOfSaleId
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
			print "EMS7CustomerId %s" %EMS7CustomerId
			EMS7LocationId = self.__getLocationId(EMS6RegionId)
			print "EMS7LocationId %s" %EMS7LocationId
			print "CustomerCard data %s" %CustomCardData
			if (CustomCardData):
				self.__printValueSmartCard(ValueCard)
				#CustomerCardData exists
				CardTypeId=3
				self.__EMS7Cursor.execute("select distinct(Id) from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				if(self.__EMS7Cursor.rowcount<>0):
					EMS7CustomerCardType=self.__EMS7Cursor.fetchone()
					EMS7CustomerCardTypeId=EMS7CustomerCardType[0]
					print " EMS7CustomerCardTypeId %s" %EMS7CustomerCardTypeId
					if(Action=='Insert'):
						if (EMS7CustomerCardTypeId):
							self.__EMS7Cursor.execute("insert ignore into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,\
							IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,\
							VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,CustomCardData,IsForNonOverlappingUse,GracePeriodMinutes,\
							CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
							self.__EMS7Connection.commit()
							print "EMS7CustomCard ID that was just inserted is %s" %self.__EMS7Cursor.lastrowid
						else:
							#CustomerTypeId is missing so create a card type
							CardTypeId=3
							self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
							self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
							EMS7CustomerCardTypeId = self.__EMS7Cursor.fetchone()
							EMS7CustomerCardTypeId=EMS7CustomerCardTypeId[0]
							self.__EMS7Cursor.execute("insert ignore into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,\
							IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,\
							VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
							(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,CustomCardData,IsForNonOverlappingUse,GracePeriodMinutes,\
							CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
							EMS7CustomCardId=self.__EMS7Cursor.lastrowid
							self.__EMS7Connection.commit()
					if(Action=='Update'):
						if( EMS7CustomerCardTypeId):
							self.__EMS7Cursor.execute("update CustomerCard set CustomerCardTypeId=%s,LocationId=%s,PointOfSaleId=%s,CardNumber=%s,IsForNonOverlappingUse=%s,GracePeriodMinutes=%s,CardBeginGMT=%s,CardExpireGMT=%s,NumberOfUses=%s,MaxNumberOfUses=%s,Comment=%s,IsActive=%s,VERSION=%s,AddedGMT=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where CustomerCardTypeId=%s and CardNumber=%s",(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,CustomCardData,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerCardTypeId,CustomCardData))
							self.__EMS7Connection.commit()

	def addSmartCardToEMS7(self,SmartCard, Action):
		print " ...in adding smart card to EMS7 method...."
		TableName='SmartCard'
	      	LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
                IsForNonOverlappingUse = 0
                GracePeriodMinutes = None
                CardBeginGMT = None
                CardExpireGMT = None
                NumberOfUses =None
                MaxNumberOfUses = None
                Comment = None
                IsActive =1
                EMS6CustomerId = SmartCard[0]
                VERSION =1
                AddedGMT = date.today()
                EMS6RegionId = SmartCard[1]
                EMS6PaystationId = SmartCard[2]
                CustomerCardData = SmartCard[3]
                SmartCardData = SmartCard[4]
                PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
                EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
                EMS7LocationId = self.__getLocationId(EMS6RegionId)
		if(Action=='Insert'):
			print " Inside the Smard card data insert section"
			if (SmartCardData):
				print " found the smard card "
				self.__printValueSmartCard(SmartCard)
				#CustomerCardData exists
				CardTypeId=2
				self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				if(self.__EMS7Cursor.rowcount<>0):
					EMS7CustomerCardType=self.__EMS7Cursor.fetchone()
					EMS7CustomerCardTypeId=EMS7CustomerCardType[0]
					if (EMS7CustomerCardTypeId):
						self.__EMS7Cursor.execute("insert ignore into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,\
						IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,\
						VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
						(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,SmartCardData,IsForNonOverlappingUse,GracePeriodMinutes,\
						CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
						self.__EMS7Connection.commit()
						print "CustomerCardId ------ %s" %self.__EMS7Cursor.lastrowid
					else:
						#CustomerTypeId is missing
						CardTypeId=2
						self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
						self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
						EMS7CustomerCardTypeId = self.__EMS7Cursor.fetchone()
						EMS7CustomerCardTypeId=EMS7CustomerCardTypeId[0]
						self.__EMS7Cursor.execute("insert ignore into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,\
						IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,\
						VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
						(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,SmartCardData,IsForNonOverlappingUse,GracePeriodMinutes,\
						CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
						self.__EMS7Connection.commit()
		if(Action=='Update'):
			CardTypeId=2
			self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				CustomerCardTypeId = row[0]
				print "Customer Card Type Id is %s" %CustomerCardTypeId
				self.__EMS7Cursor.execute("update CustomerCard set CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId where CustomerCardTypeId=%s and CardNumber=%s",(EMS7CustomerCardTypeId,SmartCardData))
				EMS7CustomCardId=self.__EMS7Cursor.fetchone()
                                                                        	
	def __printValueSmartCard(self, SmartCardValueCard):
		if(self.__verbose):
			print " -- EMS6CustomerId %s " %SmartCardValueCard[0]
			print " -- RegionId %s" %SmartCardValueCard[1]
			print " -- PaystationId %s" %SmartCardValueCard[2]
			print " -- CustomerCardData %s" %SmartCardValueCard[3]
			print " -- SmartCardData %s" %SmartCardValueCard[4]

	def addBatteryInfoToEMS7(self, BatteryInfo, Action):
		print " ......adding batteryInfo to EMS 7 database......."
		tableName='BatteryInfo'
		self.__printBatteryInfo(BatteryInfo)
		EMS6PaystationId = BatteryInfo[0]
		DateField = BatteryInfo[1]
		SystemLoad = BatteryInfo[2]
		InputCurrent = BatteryInfo[3]
		Battery = BatteryInfo[4]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(Action=='Insert'):
			print " inserting battery info with pointOfSaleId", PointOfSaleId
			if(PointOfSaleId):
				self.__EMS7Cursor.execute("insert into POSBatteryInfo(PointOfSaleId,SensorGMT,SystemLoad,InputCurrent,BatteryVoltage) \
				values(%s,%s,%s,%s,%s)",(PointOfSaleId,DateField,SystemLoad,InputCurrent,Battery))	
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,DateField)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		if(Action=='Update'):
			print " inside the update section inthe battery info table with point of sale id", PointOfSaleId
			if(PointOfSaleId):
				self.__EMS7Cursor.execute("update POSBatteryInfo set SystemLoad=%s,InputCurrent=%s,BatteryVoltage=%s where PointOfSaleId=%s and SensorGMT=%s",(SystemLoad,InputCurrent,Battery,PointOfSaleId,DateField ))
				self.__EMS7Connection.commit()

	def __printBatteryInfo(self, BatteryInfo):
		if(self.__verbose):
			print " --PaystationId %s " %BatteryInfo[0]
			print " --DateField %s " %BatteryInfo[1]
			print " --SystemLoad %s" %BatteryInfo[2]
			print " --InputCurrent %s" %BatteryInfo[3]
			print " --Battery %s" %BatteryInfo[4] 

	def DisablePurchaseKeys(self):
		self.__EMS7Cursor.execute("SET autocommit=0")
		self.__EMS7Cursor.execute("SET unique_checks=0")
		self.__EMS7Cursor.execute("SET foreign_key_checks=0")

	def EnablePurchaseKeys(self):
		self.__EMS7Cursor.execute("SET autocommit=1")
		self.__EMS7Cursor.execute("SET unique_checks=1")
		self.__EMS7Cursor.execute("SET foreign_key_checks=1")
	
	def batchAddPurchaseToEMS7(self, EMS6Transactions):
		tableName='Purchase'
		DiscardedRecords = []
		EMS7Transactions = []
		for Transaction in EMS6Transactions:
			CardPaidAmount=0
			#self.__printTransaction(Transaction)
			EMS6CustomerId = Transaction[0]
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
			EMS6PaystationId = Transaction[1]
			#print "EMS6PaystationId %s" %EMS6PaystationId
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			PurchaseGMT = Transaction[2]
			TicketNumber = Transaction[3]
			TransactionTypeId = Transaction[4]
			PaymentTypeId = Transaction[5]
			if (PaymentTypeId==3):
				PaymentTypeId=7
			##add a module : If EMS6 PaymentTypeId=4, or 6 then it means that a Smart Card was used, therefore in EMS7 Purchase.PaymentType=4, or 6. and Purchase.CardPaidAmount=EMS6.transaction.smartcardpaid.   	
			EMS6RegionId = Transaction[6]
			EMS7LocationId = self.__getLocationId(EMS6RegionId)
			if(EMS7LocationId==None):
				EMS7LocationId= self.__getLocationIdByCustomer(EMS7CustomerId)
			LotNumber = Transaction[9]
			RateName = Transaction[25]
			print "Purchase step 1"
			UnifiedRateId = self.__getUnifiedRateId(EMS6PaystationId,PurchaseGMT,TicketNumber,EMS7CustomerId,RateName)
			print "Purchase step 2"
			EMS6CouponId=Transaction[8]
			EMS7CouponId= self.__getEMS7CouponId(EMS6CouponId,EMS6CustomerId)
			print "Purchase step 3"
			OriginalAmount = Transaction[10]
			ChargedAmount = Transaction[11]
			ChangeDispensedAmount = Transaction[12]
			ExcessPaymentAmount = Transaction[13]
			CashPaidAmount = Transaction[14]
			CardPaidAmount = Transaction[15]
			CoinPaidAmount = Transaction[19]
			BillPaidAmount = Transaction[20]
			RateAmount = Transaction[23]
			RateRevenueAmount = Transaction[24]
			CoinCount = Transaction[21]
			BillCount = Transaction[22]
			IsOffline = Transaction[16]
			IsRefundSlip = Transaction[17]
			CreatedGMT = Transaction[18]
			SmartCardPaid = Transaction[31]
			Revenue=self.__calculateRevenue(PaymentTypeId,CardPaidAmount,ChargedAmount,CashPaidAmount,ChangeDispensedAmount,SmartCardPaid,TransactionTypeId,IsRefundSlip,ChangeDispensedAmount)
			if(Revenue):
				RateRevenueAmount = Revenue
			if(RateAmount==None or RateAmount=='NULL'):
				print "The value of RateAmount is %s" %RateAmount
				RateAmount=0
			if (SmartCardPaid):
				# Verify: The PaymentTypeId arriving from EMS6 matches the PaymentTypeId in EMS7, therefor there is no extra logic that needs to implemented other then checking if the smardcardpaid amount exists
				CardPaidAmount=SmartCardPaid
			#The PaystationSettingId is set to a non null value, even though a record has been inserted it may not match the CustomerId, LotNumber combination. Are we setting the Table column to allow null, or adding some unknonw none to it.
			PaystationSettingId=self.__getPaystationSettingId(LotNumber,EMS7CustomerId)
			if (PaystationSettingId==None):
				PaystationSettingId=self.__setPaystationSettingId(LotNumber,EMS7CustomerId)
			if (PurchaseGMT<>None and PointOfSaleId):
				EMS7Transactions.append((EMS7CustomerId,PointOfSaleId,PurchaseGMT,TicketNumber,TransactionTypeId,PaymentTypeId,EMS7LocationId,PaystationSettingId,UnifiedRateId,\
				EMS7CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,\
				CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT))
			else:
				MultiKeyId = (EMS6PaystationId,PurchaseGMT,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				DiscardedRecords.append((MultiKeyId,tableName,date.today()))

			if (len(EMS7Transactions) >= 10000):
				print "Number of rows %s" %len(EMS7Transactions)
				self.__EMS7Cursor.executemany("insert ignore into Purchase(CustomerId,PointOfSaleId,PurchaseGMT,PurchaseNumber,TransactionTypeId,\
					PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,\
					ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,\
					BillCount,IsOffline,IsRefundSlip,CreatedGMT) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					EMS7Transactions)
				self.__EMS7Connection.commit()
				EMS7Transactions = []

			if (len(DiscardedRecords) >= 10000):
				print "Discarded Records %s" %len(DiscardedRecords)
				self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
				DiscardedRecords = []
	#	end=time.clock()
		#print "Num of rows %s Time : %s" %(len(EMS7Transactions),end-start)
		self.__EMS7Cursor.executemany("insert ignore into Purchase(CustomerId,PointOfSaleId,PurchaseGMT,PurchaseNumber,TransactionTypeId,\
			PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,\
			ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,\
			BillCount,IsOffline,IsRefundSlip,CreatedGMT) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
			EMS7Transactions)
		self.__EMS7Connection.commit()
		print "Discarded Records %s" %len(DiscardedRecords)
		self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)

	def __calculateRevenue(self, PaymentTypeId,CardPaidAmount,ChargedAmount,CashPaidAmount,ChangeDispensedAmount,SmartCardPaidAmount,TransactionTypeId,IsRefundSlip,ChangeDispensed):
		revenue = 0 
		pcRevenue = 0 
		scRevenue = 0 
		customCardRevenue = 0
		ccCollections = 0 
		ccRevenue = 0 
		cashRevenue = 0
		ccScRechargeAmount = 0
		cashCollections = 0
		cashScRechargeAmount = 0
		cashAttendantDeposit = 0
		cashTotalRefunds = 0
		replenishAmount = 0
		cashCollections = 0
		## PaymentTypeId = Patroller and TransactionTypeId!=Test 
		if (PaymentTypeId == 3 and  TransactionTypeId!= 6): 
			pcRevenue = CardPaidAmount 
			scRevenue = SmartCardPaidAmount 
		## PaymentTypeId = Value Card or PaymentTypeId=Cash/Value
		if (PaymentTypeId == 7 or PaymentTypeId == 9): 
			customCardRevenue = CardPaidAmount 
			ccCollections = 0 
		## PaymentTypeId=Credit Card and PaymentTypeId=Cash/CC
		if (PaymentTypeId == 2 or PaymentTypeId == 5): 
			ccCollections = CardPaidAmount 
			ccScRechargeAmount = 0
		## TransactionTypeId=SCRecharge and PaymentTypeId=Credit Card 
		if (TransactionTypeId == 4 and PaymentTypeId == 2): 
			ccScRechargeAmount = ChargedAmount 
		ccRevenue = ccCollections - ccScRechargeAmount
		cashCollections = 0
		cashScRechargeAmount = 0
		cashAttendantDeposit=0
		cashTotalRefunds=0
		replenishAmount=0 
		cashCollections = CashPaidAmount
		## TransactionTypeId=SCRecharge and PaymentTypeId=Cash
		if (TransactionTypeId==4 and PaymentTypeId==1): 
			cashScRechargeAmount = ChargedAmount 
		## TransactionTypeId=Deposit
		if (TransactionTypeId==8): 
			cashAttendantDeposit = CashPaidAmount
		if (CashPaidAmount > 0 and IsRefundSlip): 
			cashTotalRefunds = CashPaidAmount + CardPaidAmount - ChargedAmount 
		else: 
			cashTotalRefunds = ChangeDispensed
		## Replenish or Hopper1Add or Hopper2Add
		if (TransactionTypeId == 11 or TransactionTypeId == 13 or TransactionTypeId == 14): 
			replenishAmount = CashPaidAmount
		cashRevenue = cashCollections - cashScRechargeAmount - cashAttendantDeposit - cashTotalRefunds - replenishAmount
		revenue = cashRevenue + ccRevenue + customCardRevenue + scRevenue + pcRevenue 
		return revenue

	def __getLocationIdByCustomer(self, EMS7CustomerId):
		if(EMS7CustomerId in self.__locationbycustomerIdCache):
			return self.__locationbycustomerIdCache[EMS7CustomerId]

		EMS7LocationId=None
		if(EMS7CustomerId<>None):
			self.__EMS7Cursor.execute("select Id from Location where CustomerId=%s and Name='Unassigned'",(EMS7CustomerId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				EMS7LocationId = row[0]

		self.__locationbycustomerIdCache[EMS7CustomerId] = EMS7LocationId			
                return EMS7LocationId

	def addPurchaseToEMS7(self, Transaction, Action):
		print "Purchase Purchase Purchase >>>"
		## Using this module for incremental migration
		tableName='Purchase'
		CardPaidAmount=0
		MobileNumber=None
		self.__printTransaction(Transaction)
		EMS6CustomerId = Transaction[0]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EMS6PaystationId = Transaction[1]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseGMT = Transaction[2]
		TicketNumber = Transaction[3]
		TransactionTypeId = Transaction[4]
		PaymentTypeId = Transaction[5]
		if (PaymentTypeId==3):
			PaymentTypeId=7
		##add a module : If EMS6 PaymentTypeId=4, or 6 then it means that a Smart Card was used, therefore in EMS7 Purchase.PaymentType=4, or 6. and Purchase.CardPaidAmount=EMS6.transaction.smartcardpaid.   	
		EMS6RegionId = Transaction[6]
		EMS7LocationId = self.__getLocationId(EMS6RegionId)
		LotNumber = Transaction[9]
		RateName = Transaction[25]
		UnifiedRateId = self.__getUnifiedRateId(EMS6PaystationId,PurchaseGMT,TicketNumber,EMS7CustomerId,RateName)
		EMS6CouponId=Transaction[8]
		EMS7CouponId=self.__getEMS7CouponId(EMS6CouponId,EMS6CustomerId)
		OriginalAmount = Transaction[10]
		ChargedAmount = Transaction[11]
		ChangeDispensedAmount = Transaction[12]
		ExcessPaymentAmount = Transaction[13]
		CashPaidAmount = Transaction[14]
		CardPaidAmount = Transaction[15]
		CoinPaidAmount = Transaction[19]
		BillPaidAmount = Transaction[20]
		RateAmount = Transaction[23]
		RateRevenueAmount = Transaction[24]
		CoinCount = Transaction[21]
		BillCount = Transaction[22]
		IsOffline = Transaction[16]
		IsRefundSlip = Transaction[17]
		CreatedGMT = Transaction[18]
		SmartCardPaid = Transaction[31]
		Revenue=self.__calculateRevenue(PaymentTypeId,CardPaidAmount,ChargedAmount,CashPaidAmount,ChangeDispensedAmount,SmartCardPaid,TransactionTypeId,IsRefundSlip,ChangeDispensedAmount)
		if(Revenue):
			RateRevenueAmount = Revenue
		if(RateAmount==None or RateAmount=='NULL'):
			RateAmount=0
		if(Action=='Insert'):
			if (SmartCardPaid):
				# Verify: The PaymentTypeId arriving from EMS6 matches the PaymentTypeId in EMS7, therefor there is no extra logic that needs to implemented other then checking if the smardcardpaid amount exists
				CardPaidAmount=SmartCardPaid
			#The PaystationSettingId is set to a non null value, even though a record has been inserted it may not match the CustomerId, LotNumber combination. Are we setting the Table column to allow null, or adding some unknonw none to it.
			PaystationSettingId=self.__getPaystationSettingId(LotNumber,EMS7CustomerId)
			if (PaystationSettingId==None):
				PaystationSettingId=self.__setPaystationSettingId(LotNumber,EMS7CustomerId)
			if (PurchaseGMT<>None and PointOfSaleId):
				self.__EMS7Cursor.execute("insert ignore into Purchase(CustomerId,PointOfSaleId,PurchaseGMT,PurchaseNumber,TransactionTypeId,\
					PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,\
					ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,\
					BillCount,IsOffline,IsRefundSlip,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					(EMS7CustomerId,PointOfSaleId,PurchaseGMT,TicketNumber,TransactionTypeId,PaymentTypeId,EMS7LocationId,PaystationSettingId,UnifiedRateId,\
					EMS7CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,\
					CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT))
				PurchaseId = self.__EMS7Cursor.lastrowid
				print "Newly inserted Purchase Id into the Purchase table is %s" %PurchaseId
				self.__EMS7Connection.commit()
	#			self.__addPurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount)
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,PurchaseGMT,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __setPaystationSettingId(self, LotNumber,EMS7CustomerId):
		LastModifiedGMT=date.today()
		self.__EMS7Cursor.execute("insert into PaystationSetting(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,LotNumber,LastModifiedGMT,1))
		self.__EMS7Connection.commit()
		EMS7PaystationSettingId = self.__EMS7Cursor.lastrowid
		return EMS7PaystationSettingId

	def addPurchaseCollection(self):
		self.__EMS7Cursor.execute("select distinct(PointOfSaleId) from Purchase")
		if(self.__EMS7Cursor.rowcount<>0):
			PointOfSaleIds = self.__EMS7Cursor.fetchall()
			for PointOfSaleId in PointOfSaleIds:
				self.__EMS7Cursor.execute("select Id,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount from Purchase where PointOfSaleId=%s",(PointOfSaleId))	
				if (self.__EMS7Cursor.rowcount<>0):	
					PurchaseCollections = self.__EMS7Cursor.fetchall() 
					for PurchaseCollection in PurchaseCollections:
						self.__EMS7Cursor.execute("insert into PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) value(%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseCollection[0],PurchaseCollection[1],PurchaseCollection[2],PurchaseCollection[3],PurchaseCollection[4],PurchaseCollection[5],PurchaseCollection[6],PurchaseCollection[7]))
						self.__EMS7Connection.commit()

	def batchAddPermitToEMS7(self, EMS6Transactions):
		# TransactionType in(4,5,6,) does not get a permit record
		# if transaction.ISRefuncslip=1m this records can be ignored from Permit, As we dont think that this is a transaction, but not permit record	
		DiscardedRecords = []
		EMS7Transactions = []

		for Transaction in EMS6Transactions:
			tableName='Permit'
		#	self.__printTransactionDataForPermit(Transaction)
			PurchaseId = None
			PermitTypeId=0
			NumberOfExtensions=0
			TransactionTypeId=None
			MobileNumber=None
			EMS7LocationId=None
			EMS6PaystationId = Transaction[0]
			TicketNumber = Transaction[1]
			RegionId = Transaction[2]
			TransactionTypeId = Transaction[3]
			PlateNumber = Transaction[4]
			SpaceNumber = Transaction[5]
			AddTimeNum = Transaction[6]
			PurchasedDate = Transaction[7]
			ExpiryDate = Transaction[8]
			EMS6MobileNumber = Transaction[9]
			if (TransactionTypeId==1):
				PermitTypeId=2
			elif(TransactionTypeId==2):
				PermitTypeId=1
			elif(TransactionTypeId==12):
				PermitTypeId=5
			elif(TransactionTypeId==16):
				PermitTypeId=4
			elif(TransactionTypeId==17):
				PermitTypeId=3
			else:
				#addCustomerPropertyToEMS7 # Ashok: This was commented and was found beside the else part (indendtation error)
				PermitTypeId=0
			if (SpaceNumber<>None):
				# Pay By Space  
				PermitIssueTypeId=3
			elif (PlateNumber<>None):
				PermitIssueTypeId=2
				# Pay By Plate 
			else:
				# Default(Pay and Display)
				PermitIssueTypeId=1
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
			LicencePlate = self.__getLicencePlateId(PlateNumber)
			MobileNumber = self.__getMobileNumberId(MobileNumber)
			EMS7LocationId = self.__getLocationId(RegionId)
			if(EMS7LocationId==None):
				EMS7LocationId=1
			if(PurchaseId and TransactionTypeId!=4 and TransactionTypeId!=5 and TransactionTypeId!=6):
				EMS7Transactions.append((PurchaseId,TicketNumber,EMS7LocationId,PermitTypeId,PermitIssueTypeId,LicencePlate,MobileNumber,SpaceNumber,AddTimeNum,PurchasedDate,ExpiryDate,ExpiryDate,NumberOfExtensions))
			else:
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				DiscardedRecords.append((MultiKeyId,tableName,date.today()))
			print " permit step 1"
			if (len(EMS7Transactions) >= 5000):
				self.__EMS7Cursor.executemany("insert ignore into Permit(PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,MobileNumberId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,NumberOfExtensions) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",EMS7Transactions)
#				self.__EMS7Connection.commit()
				EMS7Transactions = []

			if (len(DiscardedRecords) >= 5000):
				self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
				DiscardedRecords = []

		self.__EMS7Cursor.executemany("insert ignore into Permit(PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,\
						MobileNumberId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,NumberOfExtensions) \
						values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",EMS7Transactions)
#		self.__EMS7Connection.commit()

		self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)

	def addPermitToEMS7(self, Transaction, Action):
		# TransactionType in(4,5,6,) does not get a permit record
		# if transaction.ISRefuncslip=1m this records can be ignored from Permit, As we dont think that this is a transaction, but not permit record
		if(Action=='Insert'):	
			tableName='Permit'
			self.__printTransactionDataForPermit(Transaction)
			PurchaseId = None
			PermitTypeId=0
			NumberOfExtensions=0
			TransactionTypeId=None
			MobileNumber=None
			EMS7LocationId=None
			EMS6PaystationId = Transaction[0]
			TicketNumber = Transaction[1]
			RegionId = Transaction[2]
			TransactionTypeId = Transaction[3]
			print "The transaction Type id is %s" %TransactionTypeId
			PlateNumber = Transaction[4]
			SpaceNumber = Transaction[5]
			AddTimeNum = Transaction[6]
			PurchasedDate = Transaction[7]
			ExpiryDate = Transaction[8]
			EMS6MobileNumber = Transaction[9]
			if (TransactionTypeId==1):
				PermitTypeId=2
			elif(TransactionTypeId==2):
				PermitTypeId=1
			elif(TransactionTypeId==12):
				PermitTypeId=5
			elif(TransactionTypeId==16):
				PermitTypeId=4
			elif(TransactionTypeId==17):
				PermitType=3
			else:
				PermitType=0
			if (SpaceNumber<>None):
				# Pay By Space  
				PermitIssueTypeId=3
			elif (PlateNumber<>None):
				PermitIssueTypeId=2
				# Pay By Plate 
			else:
				# Default(Pay and Display)
				PermitIssueTypeId=1
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
			LicencePlate = self.__getLicencePlateId(PlateNumber)
			MobileNumber = self.__getMobileNumberId(MobileNumber)
			EMS7LocationId = self.__getLocationId(RegionId)
			if(PurchaseId and TransactionTypeId!=4 and TransactionTypeId!=5 and TransactionTypeId!=6):
				self.__EMS7Cursor.execute("insert ignore into Permit(PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,MobileNumberId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,NumberOfExtensions) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,TicketNumber,EMS7LocationId,PermitTypeId,PermitIssueTypeId,LicencePlate,MobileNumber,SpaceNumber,AddTimeNum,PurchasedDate,ExpiryDate,ExpiryDate,NumberOfExtensions))
				print "The last row id is %s" %self.__EMS7Cursor.lastrowid
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def batchAddPaymentCardToEMS7(self, EMS6Transactions):
		# If there is no corrosponding record in Processor Transaction tabl, then there is no need to insert into PaymmentCard table.
		tableName='PaymentCard'
		for Transaction in EMS6Transactions:
			DiscardedRecords = []
	                EMS7Transactions = []
			#self.__printTransactionDataForPaymentCard(Transaction)
			CardTypeId=None
			EMS6PaystationId = Transaction[0]
			TicketNumber = Transaction[1]
			PurchasedDate = Transaction[2]
			PaymentTypeId = Transaction[3]
			CreditCardType = Transaction[4]
			# Make sure that you are looking up creditcardTypeid as the CreditCardType appears as a String?
			ProcessorTransactionTypeId = Transaction[5]
			EMS6MerchantAccountId = Transaction[6]
			Amount = Transaction[7]
			Last4DigitsOfCardNumber = Transaction[8]
			ProcessingDate = Transaction[9]
			IsUploadedFromBoss = Transaction[10]
			IsRFID = Transaction[11]
			Approved = Transaction[12]
			SmartCardData = Transaction[13]
			SmartCardPaid = Transaction[14]
			CustomCardData = Transaction[15]
			CustomCardType = Transaction[16]
			EMS6CustomerId = Transaction[17]
			if(PaymentTypeId==2 or PaymentTypeId==5):
				CardTypeId=1
			elif(PaymentTypeId==0):
				CardTypeId=10
			elif(PaymentTypeId==4):
				CardTypeId=2
				# " Payment Type id SmartCard"
			elif(PaymentTypeId==6):
				#" Payment Type Id is Cash + SmartCard"
				CardTypeId=2
			elif(PaymentTypeId==7):
				#print " Payment Type id is Value Card"
				CardTypeId=3
			elif(PaymentTypeId==9):
				#print " Payment and value card"
				CardTypeId=3
			# For smartcard and value card we are inserting, or retrieving a record from CustomerCard and CustomerCardType
			# Since majority of the transaction are credit card transactions, we are only dealing 10% of transactions with value, and smart card for those types , we will look up CustoemrCardId for value cards, and smart cards. CustomerCardId will always be null , There is one customer with Customer Id =3, that has 2 records in EMS6CustomerCardType table, so do not worry about it.
#			print " -- The Credit Card Type id is %s ************************" %CreditCardType
			if(CreditCardType=='VISA'):
				CreditCardTypeId=2
			elif(CreditCardType=='MasterCard'):
				CreditCardTypeId=3
			elif(CreditCardType=='AMEX'):
				CreditCardTypeId=4
			elif(CreditCardType=='Discover'):
				CreditCardTypeId=5
			elif(CreditCardType=='Diners Club'):
				CreditCardTypeId=6
			elif(CreditCardType=='CreditCard'):
				CreditCardTypeId=7
			elif(CreditCardType=='Other'):
				CreditCardTypeId=8
			else:
				CreditCardTypeId=0
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			print " PointOfSaleID is %s" %PointOfSaleId
			PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
			print " Purchase Id is %s" %PurchaseId
			ProcessorTransactionId = self.__getProcessorTransactionId(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
			print "ProcessorTransactionId is %s" %ProcessorTransactionId
			EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
			print " Merchant Account Id is %s" %EMS7MerchantAccountId
			if(PurchaseId):
				EMS7Transactions.append((PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,ProcessingDate,IsUploadedFromBoss,IsRFID,Approved))
			else:
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				DiscardedRecords.append((MultiKeyId,tableName,date.today()))
			if (len(EMS7Transactions) >= 5000):
				print "Inserting %s Records" %len(EMS7Transactions)
				self.__EMS7Cursor.executemany("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", EMS7Transactions)
#				self.__EMS7Connection.commit()
				EMS7Transactions = []
			if (len(DiscardedRecords) >= 5000):
				self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
				DiscardeRecords = []
		#print "Inserting %s Records" %len(EMS7Transactions)
		self.__EMS7Cursor.executemany("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", EMS7Transactions)
#		self.__EMS7Connection.commit()
		self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)

	def addPaymentCardToEMS7(self, Transaction, Action):
		print "...Adding Payment Card to EMS 7..."
		# If there is no corrosponding record in Processor Transaction tabl, then there is no need to insert into PaymmentCard table.
		tableName='PaymentCard'
#		self.__printTransactionDataForPaymentCard(Transaction)
		CardTypeId=None
		EMS6PaystationId = Transaction[0]
		TicketNumber = Transaction[1]
		PurchasedDate = Transaction[2]
		PaymentTypeId = Transaction[3]
		CreditCardType = Transaction[4]
		# Make sure that you are looking up creditcardTypeid as the CreditCardType appears as a String?
		ProcessorTransactionTypeId = Transaction[5]
		EMS6MerchantAccountId = Transaction[6]
		Amount = Transaction[7]
		Last4DigitsOfCardNumber = Transaction[8]
		ProcessingDate = Transaction[9]
		IsUploadedFromBoss = Transaction[10]
		IsRFID = Transaction[11]
		Approved = Transaction[12]
		SmartCardData = Transaction[13]
		SmartCardPaid = Transaction[14]
		CustomCardData = Transaction[15]
		CustomCardType = Transaction[16]
		EMS6CustomerId = Transaction[17]
		print "PaymentTypeId is %s" %PaymentTypeId
		if(PaymentTypeId==2 or PaymentTypeId==5):
			CardTypeId=1
		elif(PaymentTypeId==0):
			CardTypeId=10
		elif(PaymentTypeId==4):
			CardTypeId=2
	#		print " Payment Type id SmartCard"
		elif(PaymentTypeId==6):
	#		print " Payment Type Id is Cash and SmartCard"
			CardTypeId=2
		elif(PaymentTypeId==7):
	#		print " Payment Type id is Value Card"
			CardTypeId=3
		elif(PaymentTypeId==9):
	#		print " Payment and value card"
			CardTypeId=3
		print "The Card Type Id is %s" %CardTypeId
		# For smartcard and value card we are inserting, or retrieving a record from CustomerCard and CustomerCardType
		# Since majority of the transaction are credit card transactions, we are only dealing 10% of transactions with value, and smart card for those types , we will look up CustoemrCardId for value cards, and smart cards. CustomerCardId will always be null , There is one customer with Customer Id =3, that has 2 records in EMS6CustomerCardType table, so do not worry about it.
		if(CreditCardType=='VISA'):
			CreditCardTypeId=2
		elif(CreditCardType=='MasterCard'):
			CreditCardTypeId=3
		elif(CreditCardType=='AMEX'):
			CreditCardTypeId=4
		elif(CreditCardType=='Discover'):
			CreditCardTypeId=5
		elif(CreditCardType=='Diners Club'):
			CreditCardTypeId=6
		elif(CreditCardType=='CreditCard'):
			CreditCardTypeId=7
		elif(CreditCardType=='Other'):
			CreditCardTypeId=8
		else:
			CreditCardTypeId=0
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
		ProcessorTransactionId = self.__getProcessorTransactionId(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		if(PurchaseId and PaymentTypeId!=1):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,\
								MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) \
								values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,\
								ProcessorTransactionId,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,ProcessingDate,IsUploadedFromBoss,IsRFID,Approved))
				EMS7PaymentCardId=self.__EMS7Cursor.lastrowid
				self.__EMS7Connection.commit()	
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			
	def __getProcessorTransactionId(self,PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate):
		ProcessorTransactionKeyId = (PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
		ProcessorTransactionKeyId = ','.join(map(str,ProcessorTransactionKeyId))

		if(ProcessorTransactionKeyId in self.__processortransactionIdCache):
			return self.__processortransactionIdCache[ProcessorTransactionKeyId]

		EMS7ProcessorTransactionId=None
		self.__EMS7Cursor.execute("select Id from ProcessorTransaction where PointOfSaleId=%s and PurchasedDate=%s and TicketNumber=%s and IsApproved=%s and ProcessorTransactionTypeId=%s and ProcessingDate=%s",(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate))
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7ProcessorTransactionId = row[0]
			self.__processortransactionIdCache[ProcessorTransactionKeyId] = EMS7ProcessorTransactionId

                return EMS7ProcessorTransactionId

	def __printTransactionDataForPaymentCard(self, Transaction):
		if(self.__verbose):
			print " -- PaystationId %s" %Transaction[0]
			print " -- TicketNumber %s" %Transaction[1]
			print " -- PurchasedDate %s" %Transaction[2]
			print " -- TypeId %s" %Transaction[3]
			print " -- CardType %s" %Transaction[4]
			print " -- TypeId %s" %Transaction[5]
			print " -- MerchantAccountId %s" %Transaction[6]
			print " -- Amount %s" %Transaction[7]
			print " -- Last4DigitsOfCardNumber %s" %Transaction[8]
			print " -- ProcessingDate %s" %Transaction[9]
			print " -- IsUploadedFromBoss %s" %Transaction[10]
			print " -- IsRFID %s" %Transaction[11]
			print " -- Approved %s" %Transaction[12]
			
	def __printTransactionDataForPermit(self, Transaction):
		if(self.__verbose):
			print " -- PaystationId %s" %Transaction[0]
			print " -- TicketNumber %s" %Transaction[1]
			print " -- RegionId %s" %Transaction[2]
			print " -- TypeId %s" %Transaction[3]
			print " -- PlateNumber %s" %Transaction[4]
			print " -- StallNumber %s" %Transaction[5]
			print " -- AddTimeNum %s" %Transaction[6]
			print " -- PurchasedDate %s" %Transaction[7]
			print " -- ExpiryDate %s" %Transaction[8]
			print " -- MobileNumber %s" %Transaction[9]

	def __getPaystationSettingId(self,LotNumber,EMS7CustomerId):
		PaystationSettingKeyId = (LotNumber, EMS7CustomerId)
		PaystationSettingKeyId = ','.join(map(str,PaystationSettingKeyId))

		if(PaystationSettingKeyId in self.__paystationsettingkeyIdCache):
			return self.__paystationsettingkeyIdCache[PaystationSettingKeyId]

		EMS7PaystationSettingId=None
		self.__EMS7Cursor.execute("select Id from PaystationSetting where CustomerId=%s and Name=%s",(EMS7CustomerId,LotNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7PaystationSettingId = row[0]
		else:
			LotNumber = 'None'
			self.__EMS7Cursor.execute("select Id from PaystationSetting where CustomerId=%s and Name=%s",(EMS7CustomerId,LotNumber))
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				EMS7PaystationSettingId = row[0]

		self.__paystationsettingkeyIdCache[PaystationSettingKeyId] = EMS7PaystationSettingId
		
                return EMS7PaystationSettingId

	def __addPurchaseCollection(self,PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount):
		tableName='PurchaseCollection'
	#	self.__printPurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount)
		if (PurchaseId):
			if (CoinPaidAmount==None):
				CoinPaidAmount='0'
			if (CoinCount==None):
				CoinCount='0'
			if (BillPaidAmount==None):
				BillPaidAmount='0'
			if (BillCount==None):
				BillCount='0'
			self.__EMS7Cursor.execute("insert into PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) values(%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(PurchaseId,tableName,IsMultiKey)

	def __printPurchaseCollection(self,PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount):
		if(self.__verbose):
			print " -- PurchaseId %s" %PurchaseId
			print " -- PointOfSaleId %s" %PointOfSaleId
			print " -- PurchaseGMT %s" %PurchaseGMT
			print " -- CashPaidAmount %s" %CashPaidAmount
			print " -- CoinPaidAmount %s" %CoinPaidAmount
			print " -- CoinCount %s" %CoinCount
			print " -- BillPaidAmount %s" %BillPaidAmount
			print " -- BillCount %s" %BillCount

	def __getUnifiedRateId(self,PaystationId,PurchasedDate,TicketNumber,CustomerId,RateName):
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		UnifiedRateId = None

		if(RateName == None or RateName == ''):
			RateName='Unknown'
		self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(CustomerId,RateName))

		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			UnifiedRateId = row[0]
		else:
	#	self.__EMS7Cursor.execute("select Id from RateToUnifiedRateMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
	#	self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(CustomerId,RateName))
	#	if (self.__EMS7Cursor.rowcount <> 0):
	#		row = self.__EMS7Cursor.fetchone()
	#		UnifiedRateId = row[0]
	#		print "Unified Rate was found %s" %UnifiedRateId

	#	if (UnifiedRateId == None):
			self.__EMS7Cursor.execute("insert ignore into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
			UnifiedRateId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(CustomerId,RateName))
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				UnifiedRateId = row[0]
				print "Unified Rate Id -----------------------%s" %UnifiedRateId
				self.__EMS7Cursor.execute("insert ignore into RateToUnifiedRateMapping(PaystationId,PurchasedDate,TicketNumber,EMS7UnifiedRateId) values(%s,%s,%s,%s)",(PaystationId,PurchasedDate,TicketNumber,UnifiedRateId))
				self.__EMS7Connection.commit()
		return UnifiedRateId

	def __printTransaction(self, Transaction):
		if(self.__verbose):
			print " --CustomerId %s" %Transaction[0]
			print " --PaystationId %s" %Transaction[1]
			print " --PurchasedDate %s" %Transaction[2]
			print " --TicketNumber %s" %Transaction[3]
			print " --TypeId %s" %Transaction[4]
			print " --PaymentTypeId %s" %Transaction[5]
			print " --RegionId %s" %Transaction[6]
			print " --RateId %s" %Transaction[7]
			print " --CouponNumber %s" %Transaction[8]
			print " --LotNumber %s" %Transaction[9]
			print " --OriginalAmount %s" %Transaction[10]
			print " --ChargedAmount %s" %Transaction[11]
			print " --ChangeDispensed %s" %Transaction[12]
			print " --ExcessPayment %s" %Transaction[13]
			print " --CashPaid %s" %Transaction[14]
			print " --CardPaid %s" %Transaction[15]
			print " --IsOffline %s" %Transaction[16]
			print " --IsRefundSlip %s" %Transaction[17]
			print " --CreationDate %s" %Transaction[18]
			print " --CoinDollars %s" %Transaction[19]
			print " --BillDollars %s" %Transaction[20]
			print " --CoinCount %s" %Transaction[21]
			print " --BillCount %s" %Transaction[22]
			print " --RateValue %s" %Transaction[23]
			print " --Revenue %s" %Transaction[24]
			print " --RateName %s" %Transaction[25]
			print " --RatesCustId %s" %Transaction[26]
			print " --StallNumber %s" %Transaction[27]
			print " --PlateNumber %s" %Transaction[28]
			print " --AddTimeNum %s" %Transaction[29]
			print " --ExpiryDate %s" %Transaction[30]
			print " --SmartCardPaid %s" %Transaction[31] 

	def __getEMS7CouponId(self, EMS6CouponId, EMS6CustomerId):
		CouponKeyId = (EMS6CouponId,EMS6CustomerId)
		CouponKeyId = ','.join(map(str,CouponKeyId))

		if(CouponKeyId in self.__couponIdCache):
			return self.__couponIdCache[CouponKeyId]

                EMS7CouponId = None
		if (EMS6CouponId <> None and EMS6CouponId <> ''):
			self.__EMS7Cursor.execute("select EMS7CouponId from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s",(EMS6CouponId,EMS6CustomerId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				EMS7CouponId = row[0]

			self.__couponIdCache[CouponKeyId] = EMS7CouponId

                return EMS7CouponId
		
	def addCustomerEmail(self, CustomerEmail, Action):
		VERSION = 0
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		self.__printCustomerEmail(CustomerEmail)
		AlertId = CustomerEmail[0]
		print ">> Alert Id %s" %CustomerEmail[0]
		EMS6CustomerId = CustomerEmail[2]
		print ">> EMS6 Customer Id %s" %EMS6CustomerId
		AlertTypeId = CustomerEmail[4]
		print " >> AlertTypeId %s " %AlertTypeId
		Email = CustomerEmail[6]
		print " Email %s" %Email
		IsDeleted = CustomerEmail[8]
		print " Is Deleted %s" %IsDeleted
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EmailList=CustomerEmail[6].split(',')
                for mail in EmailList:
			if(mail):
				if(Action=='Insert'):
					print 'here'
					self.__EMS7Cursor.execute("select Email from CustomerEmail where Email=%s",(mail))
					if (self.__EMS7Cursor.rowcount == 0):
						if(EMS7CustomerId):
							self.__EMS7Cursor.execute("insert into CustomerEmail(CustomerId,Email,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,mail,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId))
							EMS7CustomerEmailId = self.__EMS7Cursor.lastrowid	
							self.__CustomerAlertEmail(CustomerEmail, EMS7CustomerEmailId)
							self.__EMS7Connection.commit()
		#			if(Action=='Update'):
		#				self.__EMS7Cursor.execute("")

	def addPaystationAlertEmailFromPaystation(self, PaystationAlertEmail, Action):
		VERSION = 0
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		IsDeleted = 0
		self.__printPaystationAlertEmailFromPaystation(PaystationAlertEmail)
		EMS6CustomerId = PaystationAlertEmail[8]
		AlertTypeId = 12
		Email = PaystationAlertEmail[10]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			if (EMS7CustomerId):
				if (PaystationAlertEmail[10]<>None):
					EmailList=PaystationAlertEmail[10].split(',')
					for mail in EmailList:
						if(mail):
							self.__EMS7Cursor.execute("select Email from CustomerEmail where Email=%s",(mail))
							if (self.__EMS7Cursor.rowcount == 0):
								self.__EMS7Cursor.execute("insert into CustomerEmail(CustomerId,Email,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,mail,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId))
								EMS7CustomerEmailId = self.__EMS7Cursor.lastrowid
								self.__addPaystationEmailToCustomerAlertEmail(EMS7CustomerId, EMS7CustomerEmailId)
								self.__EMS7Connection.commit()

	def __printPaystationAlertEmailFromPaystation(self, PaystationAlertEmail):
		if(self.__verbose):
			print " --Paystation Id is %s" %PaystationAlertEmail[0]
			print " --CustomerId %s" %PaystationAlertEmail[8]
			print " --ContactURL %s" %PaystationAlertEmail[10]

# Add getCustomerEmailPaystation module to migrate PaystationEMS6.ContactURL. you will have to look up CustomerAlertType Id by Customer, and AlertTypeId=12 in CustomerAlertType table. read all the records from Paystation.ContactURL, Paystation.CustomerId  you will have to look up CustomerAlertType Id by Customer, and AlertTypeId=12 in CustomerAlertType table. read all the records from Paystation table and insert it into CustmerAlertEmail and CustomerEmail.table and insert it into CustmerAlertEmail and CustomerEmail.

	def __CustomerAlertEmail(self,CustomerEmail,EMS7CustomerEmailId):
		self.__printCustomerAlertEmail(CustomerEmail)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
		AlertId = CustomerEmail[0]
                EMS6CustomerId = CustomerEmail[1]
                AlertTypeId=CustomerEmail[2]
                Email = CustomerEmail[3]
                IsDeleted = CustomerEmail[4]
                EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		CustomerAlertTypeId = self.__getCustomerAlertTypeId(AlertId)
                Version=0
                self.__EMS7Cursor.execute("insert into CustomerAlertEmail(CustomerAlertTypeId,CustomerEmailId,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,EMS7CustomerEmailId,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
                self.__EMS7Connection.commit()

	def __printCustomerAlertEmail(self,CustomerEmail):
		if(self.__verbose):
			print " -- Alert Id %s" %CustomerEmail[0]
			print " -- CustomerId %s" %CustomerEmail[1]
			print " -- Alert Type %s" %CustomerEmail[2]
			print " -- Email %s" %CustomerEmail[3]
			print " -- IsDeleted %s" %CustomerEmail[4]

	def __addPaystationEmailToCustomerAlertEmail(self, EMS7CustomerId, EMS7PaystationAlertEmailId):
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustomerEmailId=EMS7PaystationAlertEmailId
		PaystationAlertTypeId=12
		CustomerAlertTypeId = self.__getCustomerAlertTypeIdForPaystationAlert(EMS7CustomerId,PaystationAlertTypeId) 
		IsDeleted = 0 
		Version=0
		self.__EMS7Cursor.execute("insert into CustomerAlertEmail(CustomerAlertTypeId,CustomerEmailId,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,EMS7CustomerEmailId,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printCustomerEmail(self,CustomerEmail):
		if(self.__verbose):
			print " -- Alert Id %s" %CustomerEmail[0]
			print " -- CustomerId %s" %CustomerEmail[1]		
			print " -- AlertTypeId %s" %CustomerEmail[2]
			print " -- Email %s" %CustomerEmail[3]
			print " -- IsDeleted %s" %CustomerEmail[4]

	def addMerchantAccount(self,MerchantAccount, Action):
		print "you are in this module"
		if(Action=='Insert'):
			tableName='MerchantAccount'
			LastModifiedGMT = date.today()
			LastModifiedByUserId = 1
			self.__printMerchantAccount(MerchantAccount)
			MerchantAccountId = MerchantAccount[0]
			version = MerchantAccount[1]
			EMS6CustomerId = MerchantAccount[2]
			Name = MerchantAccount[3]
			Field1 = MerchantAccount[4]
			Field2 = MerchantAccount[5]
			Field3 = MerchantAccount[6]
			IsDeleted = MerchantAccount[7]
			ProcessorName = MerchantAccount[8]
			ReferenceCounter = MerchantAccount[9]
			Field4 = MerchantAccount[10]
			Field5 = MerchantAccount[11]
			Field6 = MerchantAccount[12]
			IsValid = MerchantAccount[13]
			# Case 1 : if ems6.isdeleted=true then MerchantStatusTypeId=2, else if ems6.isdeleted=0 and isvalid=1 then MerchantStatusTypeId=1 else if isdeleted=0  and isvalid=0 then MerchantStatusTypeId=0, otherwise the default is disabled. Verify this logic with the data in ems6 and make sure that you do not disable a customer by mistake
			# this is big money, we need to be sure what we are doing here. !!!!!!!!!!!!!!!VERIFY THIS SECTION AGAIN!!!!!!!.
			if (IsDeleted==1):
				MerchantStatusTypeId=2
			if (IsDeleted==0 and IsValid==1):
				MerchantStatusTypeId=1
			if (IsDeleted==0 and IsValid==0):
				MerchantStatusTypeId=0
			print " Ashok: Before EMS7ProcessorId"
			EMS7ProcessorId = self.__getEMS7ProcessorId(ProcessorName)
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
			if (EMS7CustomerId and EMS7ProcessorId):
				print " Ashok: Before Insert processor id is ",EMS7ProcessorId
				self.__EMS7Cursor.execute("insert into MerchantAccount(CustomerId,ProcessorId,MerchantStatusTypeId,Name,Field1,Field2,Field3,Field4,Field5,Field6,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EMS7ProcessorId,MerchantStatusTypeId,Name,Field1,Field2,Field3,Field4,Field5,Field6,ReferenceCounter,IsValid,version,LastModifiedGMT,LastModifiedByUserId))
				EMS7MerchantAccountId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into MerchantAccountMapping(EMS6MerchantAccountId,EMS7MerchantAccountId,ProcessorName,EMS6CustomerId) values(%s,%s,%s,%s)",(MerchantAccountId,EMS7MerchantAccountId,ProcessorName,EMS6CustomerId))
				self.__EMS7Connection.commit()
			else:
				print " Ashok: In elsepart"
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(MerchantAccountId,tableName,IsMultiKey)

		if(Action=='Update'):
			MerchantStatusTypeId=0
			LastModifiedGMT = date.today()
			LastModifiedByUserId = 1
			Split=MerchantAccount.split(',')
			EMS6MerchantAccountId = Split[0]
			print " EMS6MerchantAccount %s" %EMS6MerchantAccountId
			version = Split[1]
			print " version %s" %version
			EMS6CustomerId = Split[2]
			print " EMS6CustomerId %s" %EMS6CustomerId
			Name = Split[3]
			print "Name %s" %Name
			Field1 = Split[4]
			print "Field1 %s" %Field1
			Field2 = Split[5]
			print " Field2 %s" %Field2
			Field3 = Split[6]
			print " Field3 %s" %Field3
			IsDeleted = Split[7]
			print " IsDeleted %s" %IsDeleted
			ProcessorName = Split[8]
			print " ProcessorName %s" %ProcessorName
			ReferenceCounter = Split[9]
			print " ReferenceCounter %s" %ReferenceCounter
			Field4 = Split[10]
			print "Field4 %s" %Field4
			Field5 = Split[11]
			print " Field5 %s" %Field5
			Field6 = Split[12]
			print " Field6 %s" %Field6
			IsValid = Split[13]
			print "IsValid %s" %IsValid
			if (IsDeleted==1):
				MerchantStatusTypeId=2
			if (IsDeleted==0 and IsValid==1):
				MerchantStatusTypeId=1
			if (IsDeleted==0 and IsValid==0):
				MerchantStatusTypeId=0
			print "MerchantStatusTypeId %s" %MerchantStatusTypeId
			EMS7ProcessorId = self.__getEMS7ProcessorId(ProcessorName)
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
			EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
			if (EMS7CustomerId and EMS7ProcessorId):
				print "MerchantAccountId %s" %EMS7MerchantAccountId
				self.__EMS7Cursor.execute("UPDATE MerchantAccount set CustomerId=%s,ProcessorId=%s,MerchantStatusTypeId=%s,Name=%s,Field1=%s,Field2=%s,Field3=%s,Field4=%s,Field5=%s,Field6=%s,ReferenceCounter=%s,IsValidated=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerId,EMS7ProcessorId,MerchantStatusTypeId,Name,Field1,Field2,Field3,Field4,Field5,Field6,ReferenceCounter,IsValid,version,LastModifiedGMT,LastModifiedByUserId,EMS7MerchantAccountId))
				print " EMS7MerchantAccountId is %s" %EMS7MerchantAccountId
				print " EMS7 EMS7CustomerId Is %s" %EMS7CustomerId

	def __getEMS7ProcessorId(self,ProcessorName):
		if(ProcessorName in self.__processornameIdCache):
			return self.__processornameIdCache[ProcessorName]

		EMS7ProcessorId = None
		self.__EMS7Cursor.execute("select Id from Processor where Name=%s", ProcessorName)
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7ProcessorId = row[0]

		self.__processornameIdCache[ProcessorName] = EMS7ProcessorId

                return EMS7ProcessorId

	def __printMerchantAccount(self,MerchantAccount):
		if(self.__verbose):
			print " -- Id %s" %MerchantAccount[0]
			print " -- version %s" %MerchantAccount[1]
			print " -- CustomerId %s" %MerchantAccount[2]
			print " -- Name %s" %MerchantAccount[3]
			print " -- Field1 %s" %MerchantAccount[4]
			print " -- Field2 %s" %MerchantAccount[5]	
			print " -- Field3 %s" %MerchantAccount[6]
			print " -- IsDeleted %s" %MerchantAccount[7]
			print " -- ProcessorName %s" %MerchantAccount[8]
			print " -- ReferenceCounter %s" %MerchantAccount[9]
			print " -- Field4 %s" %MerchantAccount[10]
			print " -- Field5 %s" %MerchantAccount[11]
			print " -- Field6 %s" %MerchantAccount[12]
			print " -- IsValid %s" %MerchantAccount[13]
		
	def addProcessorProperties(self, ProcessorProperties, Action):
		print " In adding processor properties method..."
		IsForValueCard = 0
		VERSION = 0
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		IsDeleted = 0
		Processor = ProcessorProperties[0]
		Name = ProcessorProperties[1]
		Value = ProcessorProperties[2]
		print " -- Processor %s " %ProcessorProperties[0]
		print " -- Name %s" %ProcessorProperties[1]
		print " -- Value %s" %ProcessorProperties[2]
		EMS7ProcessorId=self.__getEMS7ProcessorId(Processor)
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into ProcessorProperty(ProcessorId,Name,Value,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(EMS7ProcessorId,Name,Value,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId))
			EMS7ProcessorPropertyId =  self.__EMS7Cursor.lastrowid
			print " creating ProcessorPropertiesMapping record with processorPropertyIdof", EMS7ProcessorPropertyId
			self.__EMS7Cursor.execute("insert into ProcessorPropertiesMapping(Processor,Name,EMS7Id) values(%s,%s,%s)",(Processor,Name,EMS7ProcessorPropertyId))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			#self.__EMS7Cursor.execute("select EMS7Id from ProcessorPropertiesMapping where Processor=%s and Name=%s",(Processor,Name))
			self.__EMS7Cursor.execute("select EMS7Id from ProcessorPropertiesMapping where Processor=%s",(Processor))
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				EMS7ProcessorPropertyId = row[0]
				self.__EMS7Cursor.execute("update ProcessorProperty set ProcessorId=%s, Name=%s, Value=%s, IsDeleted=%s, VERSION=%s, LastModifiedGMT=%s, LastModifiedByUserId=%s where Id=%s",(EMS7ProcessorId,Name,Value,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7ProcessorPropertyId))	
				self.__EMS7Connection.commit()

	def addPreAuth(self, PreAuth, Action):
		self.__printPreAuth(PreAuth)
		ModifiedDate=date.today()
		PreAuthId = PreAuth[0]
		ResponseCode = PreAuth[1]
		ProcessorTransactionId = PreAuth[2]
		if (ProcessorTransactionId==None):
			print " Processor Transaction is null"
			ProcessorTransactionId=''
		AuthorizationNumber = PreAuth[3]
		if (AuthorizationNumber==None):
			print " The autorization number is null"
			AuthorizationNumber=''
		ReferenceNumber = PreAuth[4]
		EMS6MerchantAccountId = PreAuth[5]
		Amount = PreAuth[6]
		Last4DigitsOfCardNumber = PreAuth[7]
		CardType = PreAuth[8]
		Approved = PreAuth[9]	
		PreAuthDate = PreAuth[10]
		CardData = PreAuth[11]
		EMS6PaystationId = PreAuth[12]
		ReferenceId = PreAuth[13]
		Expired = PreAuth[14]
		CardHash = PreAuth[15]
		ExtraData = PreAuth[16]
		PsRFID = PreAuth[17]
		IsRFID = 0
		CardExpiry = 0
		print " the value of the card expiry column is ******** %s " %CardExpiry
		print " the value of ISRFID is %s" %IsRFID
		if (IsRFID==None or IsRFID=='NULL'):
			IsRFID=0
			print "The value for the ISRFID is %s" %IsRFID	
			print "The value for the IsRFID is 0"
		EMS7PointOfSaleId=self.__getPointOfSaleId(EMS6PaystationId)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		print " inserting pre-auth record with EMS 7 merchat account id: ", EMS7MerchantAccountId
		print " inserting into pre-auth table with EMS 7 POS id ", EMS7PointOfSaleId
		if(Action=='Insert'):
			if(EMS7MerchantAccountId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("insert into PreAuth(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,IsApproved,PreAuthDate,CardData,PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,CardExpiry,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,EMS7PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,CardExpiry,IsRFID))
				EMS7PreAuthId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into PreAuthMapping(EMS6MerchantAccountId, EMS7MerchantAccountId, ProcessorTransactionId, DateAdded) values(%s,%s,%s,%s)",(PreAuthId,EMS7PreAuthId,ProcessorTransactionId,ModifiedDate))
				self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " we are in the update section of the preauth table"
			if(EMS7MerchantAccountId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("select EMS7MerchantAccountId from PreAuthMapping where EMS6MerchantAccountId=%s",(PreAuthId))
				if (self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7MerchantAccountId = row[0]
					self.__EMS7Cursor.execute("SET foreign_key_checks = 0")
					self.__EMS7Cursor.execute("update PreAuth set ResponseCode=%s,ProcessorTransactionId=%s,AuthorizationNumber=%s,ReferenceNumber=%s,MerchantAccountId=%s,Amount=%s,Last4DigitsOfCardNumber=%s,CardType=%s,IsApproved=%s,PreAuthDate=%s,CardData=%s,PointOfSaleId=%s,ReferenceId=%s,Expired=%s,CardHash=%s,ExtraData=%s,PsRefId=%s,CardExpiry=%s,IsRFID=%s where Id=%s",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,EMS7PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,CardExpiry,IsRFID,EMS7MerchantAccountId))
					self.__EMS7Cursor.execute("SET foreign_key_checks = 1")
					self.__EMS7Connection.commit()
	
	def __getEMS7MerchantAccountId(self, EMS6MerchantAccountId):
		if(EMS6MerchantAccountId in self.__merchantIdCache):
			return self.__merchantIdCache[EMS6MerchantAccountId]

		EMS7MerchantAccountId=None
		if(EMS6MerchantAccountId<>None):
			self.__EMS7Cursor.execute("select EMS7MerchantAccountId from MerchantAccountMapping where EMS6MerchantAccountId=%s",(EMS6MerchantAccountId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				EMS7MerchantAccountId = row[0]

			self.__merchantIdCache[EMS6MerchantAccountId] = EMS7MerchantAccountId

		return EMS7MerchantAccountId
		
	def __printPreAuth(self, PreAuth):
		if(self.__verbose):
			print " -- Id %s" %PreAuth[0]
			print " -- ResponseCode %s" %PreAuth[1]
			print " -- ProcessorTransactionId %s" %PreAuth[2]
			print " -- AuthorizationNumber %s" %PreAuth[3]
			print " -- ReferenceNumber %s" %PreAuth[4]
			print " -- MerchantAccountId %s" %PreAuth[5]
			print " -- Amount %s" %PreAuth[6]
			print " -- Last4DigitsOfCardNumber %s" %PreAuth[7]
			print " -- CardType %s" %PreAuth[8]
			print " -- Approved %s" %PreAuth[9]
			print " -- PreAuthDate %s" %PreAuth[10]
			print " -- CardData %s" %PreAuth[11]
			print " -- PaystationId %s" %PreAuth[12]
			print " -- ReferenceId %s" %PreAuth[13]
			print " -- Expired %s" %PreAuth[14]
			print " -- CardHash %s" %PreAuth[15]
			print " -- ExtraData %s" %PreAuth[16]
			print " -- PsRFID %s" %PreAuth[17]
		#	print " -- IsRefId %s" %PreAuth[18]
		#	print " -- CardExpiry %s" %PreAuth[19]

	def addPreAuthHolding(self, PreAuthHolding, Action):
		print " Adding preauth holding record to EMS 7....."
		self.__printPreAuthHolding(PreAuthHolding)
		PreAuthHoldingId = PreAuthHolding[0]
	#	PreAuthId = PreAuthHolding[1]
		ResponseCode = PreAuthHolding[1]
		ProcessorTransactionId = PreAuthHolding[2]
		if (ProcessorTransactionId==None):
			ProcessorTransactionId=''
		AuthorizationNumber = PreAuthHolding[3]
		if (AuthorizationNumber==None):
			AuthorizationNumber=''
		ReferenceNumber = PreAuthHolding[4]
		EMS6MerchantAccountId = PreAuthHolding[5]
		Amount = PreAuthHolding[6]
		Last4DigitsOfCardNumber = PreAuthHolding[7]
		CardType = PreAuthHolding[8]
		Approved = PreAuthHolding[9]	
		PreAuthDate = PreAuthHolding[10]
		CardData = PreAuthHolding[11]
		EMS6PaystationId = PreAuthHolding[12]
		ReferenceId = PreAuthHolding[13]
		Expired = PreAuthHolding[14]
		CardHash = PreAuthHolding[15]
		ExtraData = PreAuthHolding[16]
		PsRFID = PreAuthHolding[17]
		IsRFID = PreAuthHolding[18]
		AddedGMT = PreAuthHolding[19]
		MovedGMT = PreAuthHolding[20]
		CardExpiry = PreAuthHolding[21]
		if (IsRFID==None or IsRFID=='NULL'):
			IsRFID=0
		EMS7PointOfSaleId=self.__getPointOfSaleId(EMS6PaystationId)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		EMS7PreAuthId = self.__getEMS7PreAuthId(Expired,Approved,EMS7PointOfSaleId,Amount,AuthorizationNumber,PreAuthDate)
		print " processing pre-auth holding with EMS 7 merchantAccountId",EMS7MerchantAccountId
		print " processing pre-auth holding with EMS7 preauth id : ",EMS7PreAuthId
		if(EMS7MerchantAccountId and EMS7PointOfSaleId):
			if(Action=='Insert'):
				self.__EMS7Cursor.execute("insert into PreAuthHolding(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,CardExpiry,IsRFID,AddedToHoldingGMT,MovedToRetryGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,EMS7PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,CardExpiry,IsRFID,AddedGMT,MovedGMT))
				EMS7IdPreAuthHoldingId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into PreAuthHoldingMapping(EMS6MerchantAccountId, EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId  ) values(%s,%s,%s, %s)",(EMS6MerchantAccountId,EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId))
				print " Record inserted.."
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				print " uodating pre-auth holding ..."
				self.__EMS7Cursor.execute("select EMS7Id from PreAuthHoldingMapping where EMS6Id=%s",(PreAuthHoldingId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7Id = row[0]
					
					self.__EMS7Cursor.execute("update PreAuthHolding set ResponseCode=%s,ProcessorTransactionId=%s,AuthorizationNumber=%s,ReferenceNumber=%s,MerchantAccountId=%s,Amount=%s,Last4DigitsOfCardNumber=%s,CardType=%s,Approved=%s,PreAuthDate=%s,CardData=%s,PointOfSaleId=%s,ReferenceId=%s,Expired=%s,CardHash=%s,ExtraData=%s,PsRefId=%s,CardExpiry=%s,IsRFID=%s,AddedToHoldingGMT=%s,MovedToRetryGMT=%s where Id=%s",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,EMS7PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,CardExpiry,IsRFID,AddedGMT,MovedGMT,EMS7Id))
					self.__EMS7Connection.commit()

	def __getEMS7PreAuthId(self,Expired,Approved,EMS7PointOfSaleId,Amount,AuthorizationNumber,PreAuthDate):
		PreAuthId=None
		self.__EMS7Cursor.execute("select Id from PreAuth where Expired=%s and IsApproved=%s and PointOfSaleId=%s and Amount=%s and AuthorizationNumber=%s and PreAuthDate=%s",(Expired,Approved,EMS7PointOfSaleId,Amount,AuthorizationNumber,PreAuthDate))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PreAuthId=row[0]
		return PreAuthId	

	def __printPreAuthHolding(self,PreAuth):
		if(self.__verbose):
			print " -- PreAuthHoldingId %s" %PreAuth[0]
		#	print " -- PreAuthId %s" %PreAuth[1]
			print " -- ResponseCode %s" %PreAuth[1]
			print " -- ProcessorTransactionId %s" %PreAuth[2]
			print " -- AuthorizationNumber %s" %PreAuth[3]
			print " -- ReferenceNumber %s" %PreAuth[4]
			print " -- MerchantAccountId %s" %PreAuth[5]
			print " -- Amount %s" %PreAuth[6]
			print " -- Last4DigitsOfCardNumber %s" %PreAuth[7]
			print " -- CardType %s" %PreAuth[8]
			print " -- Approved %s" %PreAuth[9]
			print " -- PreAuthDate %s" %PreAuth[10]
			print " -- CardData %s" %PreAuth[11]
			print " -- PaystationId %s" %PreAuth[12]
			print " -- ReferenceId %s" %PreAuth[13]
			print " -- Expired %s" %PreAuth[14]
			print " -- CardHash %s" %PreAuth[15]
			print " -- ExtraData %s" %PreAuth[16]
			print " -- PsRFID %s" %PreAuth[17]
			print " -- IsRefId %s" %PreAuth[18]
			print " -- AddedGMT %s" %PreAuth[19]
			print " -- MovedGMT %s" %PreAuth[20]
			print " -- CardExpiry %s" %PreAuth[21]
			
	def batchAddProcessorTransaction(self, EMS6ProcessorTransaction, Action):
		tableName='ProcessorTransaction'
		EMS7ProcessorTransaction = []
		DiscardedRecords = []
		EMS6Keys = []
		print "In batchAddProcessorTransaction"
		
		if(Action=='Insert'):
			for ProcessorTransaction in EMS6ProcessorTransaction:
	#			self.__printProcessorTransaction(ProcessorTransaction)
				TypeId = ProcessorTransaction[0]
				EMS6PaystationId = ProcessorTransaction[1]
				EMS6MerchantAccountId = ProcessorTransaction[2]
				TicketNumber = ProcessorTransaction[3]
				Amount = ProcessorTransaction[4]
				CardType = ProcessorTransaction[5]
				Last4DigitsOfSardNumber = ProcessorTransaction[6]
				CardCheckSum = ProcessorTransaction[7]
				PurchasedDate = ProcessorTransaction[8]
				ProcessingDate = ProcessorTransaction[9]
				ProcessorTransactionId = ProcessorTransaction[10]
				AuthorizationNumber = ProcessorTransaction[11]
				ReferenceNumber = ProcessorTransaction[12]
				Approved = ProcessorTransaction[13]
				CardHash = ProcessorTransaction[14]
				IsUploadFromBoos = ProcessorTransaction[15]
				IsRFID = ProcessorTransaction[16]
				PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
				EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)

				if (PointOfSaleId and EMS7MerchantAccountId):
					PurchaseId=self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)

					EMS7ProcessorTransaction.append((PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,TypeId,EMS7MerchantAccountId,Amount,CardType,Last4DigitsOfSardNumber,CardCheckSum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,Approved,CardHash,IsUploadFromBoos,IsRFID))
					EMS6Keys.append([EMS6PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate])
				else:
					MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate)
					MultiKeyId = ','.join(map(str,MultiKeyId))
					DiscardedRecords.append((MultiKeyId,tableName,date.today()))

				if (len(EMS7ProcessorTransaction) > 5000):
					self.__EMS7Cursor.executemany("insert ignore into ProcessorTransaction(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",EMS7ProcessorTransaction)
	#				EMS7ProcessorTransactionLastId = self.__EMS7Cursor.lastrowid
	#				
	#				for keys in EMS6Keys:
	#					keys.append(EMS7ProcessorTransactionLastId)
	#					EMS7ProcessorTransactionLastId = EMS7ProcessorTransactionLastId + 1
	#				print "step 2"
	#				self.__EMS7Cursor.executemany("insert into ProcessorTransactionMapping(PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate,EMS7ProcessorTransactionId) values(%s,%s,%s,%s,%s,%s,%s)",EMS6Keys)

	#				self.__EMS7Connection.commit()
	#				EMS7ProcessorTransaction = []
	#				EMS6Keys = []

				if (len(DiscardedRecords) >= 5000):
					self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
					DiscardedRecords = []
			self.__EMS7Cursor.executemany("insert ignore into ProcessorTransaction(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",EMS7ProcessorTransaction)
	#		EMS7ProcessorTransactionLastId = self.__EMS7Cursor.lastrowid
	#		print "EMS7ProcessorTransactionLastId %s" %EMS7ProcessorTransactionLastId
	#		for keys in EMS6Keys:
	#			keys.append(EMS7ProcessorTransactionLastId)
	#			EMS7ProcessorTransactionLastId = EMS7ProcessorTransactionLastId + 1
	#		print "step 4"
	#		print " EMS6Keys -- %s" %EMS6Keys
	#		self.__EMS7Cursor.executemany("insert into ProcessorTransactionMapping(PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate,EMS7ProcessorTransactionId) values(%s,%s,%s,%s,%s,%s,%s)",EMS6Keys)
	#		self.__EMS7Connection.commit()
	#		print "step 5"
			self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)

	def addProcessorTransaction(self, MultiKeyId, Action):
		print" in add processor transaction to EMS 7 method"
		Split=MultiKeyId.split(',')
		TypeId = Split[0]
		PaystationId = Split[1]
		MerchantAccountId = Split[2]
		TicketNumber = Split[3]
		Amount = Split[4]
		CardType = Split[5]
		Last4DigitsOfCardNumber = Split[6]
		CardChecksum = Split[7]
		PurchasedDate = Split[8]
		ProcessingDate = Split[9]
		ProcessorTransactionId = Split[10]
		AuthorizationNumber = Split[11]
		ReferenceNumber = Split[12]
		Approved = Split[13]
		CardHash = Split[14]
		IsUploadedFromBoss = Split[15]
		IsRFID = Split[16]
		
		PointOfSaleId = self.__getPointOfSaleId(PaystationId)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(MerchantAccountId)
		print "PaystationId is:", PaystationId
		print "PurchasedDate is:", PurchasedDate
		print "TicketNumber is:", TicketNumber
		print "Approved is:", Approved
		print "TypeId is:", TypeId
		print "ProcessingDate is:",ProcessingDate
		
		if(Action=='Insert'):
			print "inserting procesor transaction"
			tableName='ProcessorTransaction'
			
			#Added the below line by Ashok on July 10
			if (PointOfSaleId):
				PurchaseId=self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
				self.__EMS7Cursor.execute("insert into ProcessorTransaction(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,\
					Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,\
					ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,IsRFID) \
					values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",\
					(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,TypeId,EMS7MerchantAccountId,\
					Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,\
					ReferenceNumber,Approved,CardHash,IsUploadedFromBoss,IsRFID))
				self.__EMS7Connection.commit()
				
				#Code added on Aug 20
				if (Approved == 1):
					self.__EMS7Cursor.execute("select Id from PaymentCard where PurchaseId = %s",(PurchaseId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						PaymentCardId = row[0]
						self.__EMS7Cursor.execute("UPDATE PaymentCard set IsApproved = %s where Id =%s",(Approved,PaymentCardId))
						self.__EMS7Connection.commit()
						print "Record Updated for Paymentcard in ProcessorTransaction"
			else:
				IsMultiKey=1
				MultiKeyId = (PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
				
		#Aug 09 ITF
		if(Action=='Update'):
			print "Updating ProcessorTransaction"
			PreviousProcessorTransactionTypeId = Split[17]
					
			#Added the below line by Ashok on July 10

			self.__EMS7Cursor.execute("UPDATE ProcessorTransaction SET ProcessorTransactionTypeId = %s, \
						MerchantAccountId = %s,CardType = %s, Last4DigitsOfCardNumber = %s, ProcessingDate = %s, ProcessorTransactionId = %s, AuthorizationNumber = %s, ReferenceNumber = %s, CardHash = %s  WHERE PointOfSaleId=%s and \
				PurchasedDate = %s and TicketNumber = %s and ProcessorTransactionTypeId = %s", (TypeId, EMS7MerchantAccountId, CardType, Last4DigitsOfCardNumber, ProcessingDate, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, CardHash, PointOfSaleId,PurchasedDate,TicketNumber,PreviousProcessorTransactionTypeId))
			self.__EMS7Connection.commit()
			
			
			
			
	def __getPurchaseId(self,PointOfSaleId,PurchasedDate,TicketNumber):
		PurchaseKeyId = (PointOfSaleId,PurchasedDate,TicketNumber)
		PurchaseKeyId = ','.join(map(str,PurchaseKeyId))

		if(PurchaseKeyId in self.__purchaseIdCache):
			return self.__purchaseIdCache[PurchaseKeyId]
	
		EMS7PurchaseId=None
		self.__EMS7Cursor.execute("select Id from Purchase where PointOfSaleId=%s and PurchaseGMT=%s and PurchaseNumber=%s",(PointOfSaleId,PurchasedDate,TicketNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7PurchaseId = row[0]

		self.__purchaseIdCache[PurchaseKeyId] = EMS7PurchaseId
	
		return EMS7PurchaseId

	def __printProcessorTransaction(self, ProcessorTransaction):
		if(self.__verbose):
			print " -- TypeId %s" %ProcessorTransaction[0]
			print " -- PaystationId %s" %ProcessorTransaction[1]
			print " -- MerchantAccountId %s" %ProcessorTransaction[2]
			print " -- TicketNumber %s" %ProcessorTransaction[3]
			print " -- Amount %s" %ProcessorTransaction[4]
			print " -- CardType %s" %ProcessorTransaction[5]
			print " -- Last4DigitsOfSardNumber %s" %ProcessorTransaction[6]
			print " -- CardCheckSum %s" %ProcessorTransaction[7]
			print " -- PurchasedDate %s" %ProcessorTransaction[8]
			print " -- ProcessingDate %s" %ProcessorTransaction[9]
			print " -- ProcessorTransactionId %s" %ProcessorTransaction[10]
			print " -- Authorization Number %s" %ProcessorTransaction[11]
			print " -- ReferenceNumber %s" %ProcessorTransaction[12]		
			print " -- Approved %s" %ProcessorTransaction[13]
			print " -- CardHash %s" %ProcessorTransaction[14]
			print " -- IsUploadFromBoos %s" %ProcessorTransaction[15]
			print " -- IsRFID %s" %ProcessorTransaction[16]

	def addUserAccountToEMS7(self, UserAccount, Action):
		print " Inseide the Use Account section"
		tableName = 'UserAccount'
		UserRoleId=None
		self.__printUserAccount(UserAccount)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		PasswordSalt='Unassigned'
		FirstName='Unknown'
		#LastName=None # Ashok: Comment this line and added the below
		LastName=UserAccount[9]
		IsPasswordTemporary=1 # Assigned a default 1, when the customers log into EMS7 the first time the password is going to be temporary
		UserAccountStatusTypeId=1
		EMS6UserAccountId = UserAccount[0]
		Version = UserAccount[1]
		EMS6CustomerId = UserAccount[2]
		Name = UserAccount[3]
		Password = UserAccount[4]
		RoleId = UserAccount[5]
		if(RoleId==1):
			UserRoleId=1
		elif(RoleId==2):
			UserRoleId=3
		AccountStatus = UserAccount[6]
		UserName = UserAccount[7]
		UserAccountId = UserAccount[8]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if (AccountStatus=='ENABLED'):
			UserAccountStatusTypeId=1
		elif(AccountStatus=='DISABLED'):
			UserAccountStatusTypeId=0
		if(UserName):
			UserNameEncoded = urllib.quote_plus(UserName)
		else:
			UserNameEncoded=None
		#Only the Customers that have a Customer Admin Role in EMS6 are being Migrated.
		if (EMS7CustomerId and UserRoleId==3):
			print "We cannot go past the validation"
			if(Action=='Insert'):
				print " Inside the insert user account section"
				self.__EMS7Cursor.execute("insert into UserAccount(CustomerId,UserStatusTypeId,UserName,FirstName,Password,IsPasswordTemporary,VERSION,LastModifiedGMT,LastModifiedByUserId,PasswordSalt,LastName) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,UserAccountStatusTypeId,UserNameEncoded,Name,Password,IsPasswordTemporary,Version,LastModifiedGMT,LastModifiedByUserId,PasswordSalt,LastName))
				EMS7UserAccountId= self.__EMS7Cursor.lastrowid
				print" Newly inserted row into the User Account table is %s" %EMS7UserAccountId
				# This Code Below assigns CustomerRole and UserRole for only CustomerAdmin role comming up from EMS6.
				self.__EMS7Cursor.execute("insert ignore into CustomerRole(CustomerId,RoleId,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMS7CustomerId,UserRoleId,0,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Cursor.execute("insert ignore into UserRole(UserAccountId,RoleId,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMS7UserAccountId,UserRoleId,0,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Cursor.execute("insert ignore into UserDefaultDashboard(UserAccountId,IsDefaultDashboard,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMS7UserAccountId,1,0,LastModifiedGMT,LastModifiedByUserId))
				print "EMS6UserAccountId %s" %EMS6UserAccountId
				print "EMS7UserAccountId %s" %EMS7UserAccountId
				print " Name %s" %Name
				print " EMS6CustomerId %s" %EMS6CustomerId
				# Ashok: Renamed the column name values below in UserAccountMapping
				self.__EMS7Cursor.execute("insert into UserAccountMapping(EMS6UseraccountId,EMS7UserAccountId) values(%s,%s)",(EMS6UserAccountId,EMS7UserAccountId))
				
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(UserAccountId,tableName,IsMultiKey)
			if(Action=='Update'):
				self.__EMS7Cursor.execute("select EMS7UserAccountId from UserAccountMapping where EMS6UserAccountId = %s",(EMS6UserAccountId))	
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7UserAccountId = row[0]
					self.__EMS7Cursor.execute("update UserAccount set CustomerId=%s,UserStatusTypeId=%s,UserName=%s,FirstName=%s,Password=%s,IsPasswordTemporary=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s,PasswordSalt=%s where Id=%s",(EMS7CustomerId,UserAccountStatusTypeId,UserNameEncoded,Name,Password,IsPasswordTemporary,Version,LastModifiedGMT,LastModifiedByUserId,PasswordSalt, EMS7UserAccountId))
					self.__EMS7Connection.commit()


# regarding the permission, we may have the customers create the user accounts and assign permission to it
	def __printUserAccount(self,UserAccount):
		if(self.__verbose):
			print " -- UserAccountId %s" %UserAccount[0]
			print " -- Version %s" %UserAccount[1]
			print " -- CustomerId %s" %UserAccount[2]
			print " -- Name %s" %UserAccount[3]
			print " -- Password %s" %UserAccount[4]
			print " -- RoleId %s" %UserAccount[5]
			print " -- AccountStatus %s" %UserAccount[6]
			print " -- UserName %s" %UserAccount[7]
			print " -- UserAccountId %s" %UserAccount[8]

	def addPOSAlertToEMS7(self, EventLogNew, Action):
		print " in adding POSAlert to EMS 7 method"
		tableName='EventLogNew'
		MultiKeyId=''
		IsActive = 0
		IsSentEmail = 1
		EMS6PaystationId = EventLogNew[0]
		DeviceId = EventLogNew[1]
		TypeId = EventLogNew[2]
		ActionId = EventLogNew[3]
		DateField = EventLogNew[4]
		Information = EventLogNew[5]
		ClearUserAccountID = EventLogNew[6]
		ClearDateField = EventLogNew[7]
		CreatedGMT = date.today()
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__printEventLogNew(EventLogNew)
		if(Action=='Insert'):
			" inserting POSAlert data with deviceId", DeviceId
			if (DeviceId==20):
				print" Insert with Deviceid==20"
			# these are user defined alerts
				EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(TypeId)
				EventStatusTypeId=23
				EventSeverityTypeId = self.__getEventSeverityTypeId(DeviceId,EventStatusTypeId,ActionId)
				print " Adding POS Alert with customerAlertTypeId", EMS7CustomerAlertTypeId
				print " Adding POS Alert with PointOfSaleIdId", PointOfSaleId
				if (EMS7CustomerAlertTypeId and PointOfSaleId):
					self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,DeviceId,EventStatusTypeId,ActionId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
					EMS7POSAlertId = self.__EMS7Cursor.lastrowid
					print " inserting into EventLogNewMapping with EOS7POSAlertId",EMS7POSAlertId
					self.__EMS7Cursor.execute("insert into EventLogNewMapping(PaystationId,DeviceId,TypeId,ActionId,DateField,POSAlertId) values(%s,%s,%s,%s,%s,%s)",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField,EMS7POSAlertId))
					self.__EMS7Connection.commit()
				else:
					IsMultiKey=1
					MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
					MultiKeyId = ','.join(map(str,MultiKeyId))
					self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			elif (DeviceId!=20):
				print "deviceId is ! 20"
			# These are Paystation Alert, EMS6.EventNewLog.TypeId=EMS6.EventNewType.Id. EMS6.EventNewType=EMS7.EventStatusType
			# EMS7StatusTypeId = self.__getEventStatusTypeId(TypeId)
				PaystationAlertTypeId=12
				EventStatusTypeId=TypeId
				EventActionTypeId=ActionId
				EventDeviceTypeId=DeviceId
				EventSeverityTypeId=self.__getEventSeverityTypeId(EventDeviceTypeId,EventStatusTypeId,EventActionTypeId)
				EMS7CustPayId=self.__getCustomerIdFromPointOfSale(EMS6PaystationId)
				if(EMS7CustPayId):
					for CustomerId in EMS7CustPayId:
					#	print " >> CustomerId in EMS7 %s" %CustomerId
					#	print " >> TypeId %s" %TypeId
						EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeIdForPaystationAlert(CustomerId,PaystationAlertTypeId)
						if(EMS7CustomerAlertTypeId and PointOfSaleId):
							self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
							EMS7POSAlertId = self.__EMS7Cursor.lastrowid
							self.__EMS7Cursor.execute("insert into EventLogNewMapping(PaystationId,DeviceId,TypeId,ActionId,DateField,POSAlertId) values(%s,%s,%s,%s,%s,%s)",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField,EMS7POSAlertId))
							self.__EMS7Connection.commit()
						else:
							IsMultiKey=1
							MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
							MultiKeyId = ','.join(map(str,MultiKeyId))
							self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		if(Action=='Update'):
			print " Action is update"
			if (DeviceId==20):
				print " DeviceId is",DeviceId
				print "Fetching records for POSAlert with playstaionId",EMS6PaystationId
				print "Fetching records for POSAlert with deviceId",DeviceId
				print "Fetching records for POSAlert with typeId",TypeId
				print "Fetching records for POSAlert with ActionId",ActionId
				self.__EMS7Cursor.execute("select distinct(POSAlertId) from EventLogNewMapping where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",(EMS6PaystationId,DeviceId,TypeId,ActionId,DateField))
				if(self.__EMS7Cursor.rowcount<>0):
					EMS7POSAlertId = self.__EMS7Cursor.fetchone()
					print " POSAlertId is", EMS7POSAlertId
					EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(TypeId)
					EventStatusTypeId=23
					EventSeverityTypeId = self.__getEventSeverityTypeId(DeviceId,EventStatusTypeId,ActionId)
					print "updating with alertTypeId : ",EMS7CustomerAlertTypeId
					print "updating with eventseverityTypeId of", EventSeverityTypeId
					print "updating with pointOfSaleId of", PointOfSaleId
					if (EMS7CustomerAlertTypeId and PointOfSaleId):
						self.__EMS7Cursor.execute("update POSAlert set CustomerAlertTypeId=%s,PointOfSaleId=%s,EventDeviceTypeId=%s,EventStatusTypeId=%s,EventActionTypeId=%s,EventSeverityTypeId=%s,AlertGMT=%s,AlertInfo=%s,IsActive=%s,IsSentEmail=%s,ClearedGMT=%s,ClearedByUserId=%s,CreatedGMT=%s where Id=%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,DeviceId,EventStatusTypeId,ActionId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT,EMS7POSAlertId))
						self.__EMS7Connection.commit()
				else:
					IsMultiKey=1
					MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
					MultiKeyId = ','.join(map(str,MultiKeyId))
					self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			elif (DeviceId!=20):
				print " updating with devicetypeid !20..."
				# These are Paystation Alert, EMS6.EventNewLog.TypeId=EMS6.EventNewType.Id. EMS6.EventNewType=EMS7.EventStatusType
				# EMS7StatusTypeId = self.__getEventStatusTypeId(TypeId)
				PaystationAlertTypeId=12
				EventStatusTypeId=TypeId
				EventActionTypeId=ActionId
				EventDeviceTypeId=DeviceId
				EventSeverityTypeId=self.__getEventSeverityTypeId(EventDeviceTypeId,EventStatusTypeId,EventActionTypeId)
				EMS7CustPayId=self.__getCustomerIdFromPointOfSale(EMS6PaystationId)
				print " CustPayId is ", EMS7CustPayId
				if(EMS7CustPayId):
					for CustomerId in EMS7CustPayId:
						EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeIdForPaystationAlert(CustomerId,PaystationAlertTypeId)
						if(EMS7CustomerAlertTypeId and PointOfSaleId):
							self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
							self.__EMS7Connection.commit()
						else:
							IsMultiKey=1
							MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
							MultiKeyId = ','.join(map(str,MultiKeyId))
							self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)


# The addActivePOSAlertToEMS7 function adds EMS6.PaysyationAlert records as a placeholder into EMS7 ActivePOSAlert table
	def addActivePOSAlertToEMS7(self, ActivePOSAlert, Action):
		self.__printActivePOSAlert(ActivePOSAlert)
		AlertId = ActivePOSAlert[0]
		EMS6PaystationId = ActivePOSAlert[1]
		IsAlerted = ActivePOSAlert[2]
		EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(AlertId)
		print "EMS7CustomerAlertTypeId is %s" %EMS7CustomerAlertTypeId
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EventDeviceTypeId = 20
		EventStatusTypeId =23
		EventActionTypeId=1
		EventSeverityTypeId=2
		IsActive=0
		EMS7POSAlertId=None
		AlertGMT=None
		AlertInfo=None
		DateField=date.today()
		Information=None
		CreatedGMT=date.today()
		if(EMS7CustomerAlertTypeId and PointOfSaleId):
			self.__EMS7Cursor.execute("insert ignore into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,POSAlertId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EMS7POSAlertId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,DateField,Information,CreatedGMT))
			self.__EMS7Connection.commit()

#The __addActivePOSAlertForPaytatationAlert Adds the EvenlogNew records for Paystation Alert to Active POS Alert
	def addActivePOSAlertForPaytatationAlert(self):
		self.__EMS7Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId!=20 group by PointOfSaleId")
		if (self.__EMS7Cursor.rowcount <> 0):
			ActiveAlerts=self.__EMS7Cursor.fetchall()
			for ActivePOSAlert in ActiveAlerts:
				self.__printActivePOSAlertForPaystation(ActivePOSAlert)
				CustomerAlertTypeId = ActivePOSAlert[0]
				PointOfSaleId = ActivePOSAlert[1]
				EventDeviceTypeId = ActivePOSAlert[2]
				EventStatusTypeId = ActivePOSAlert[3]
				EventActionTypeId = ActivePOSAlert[4]
				EventSeverityTypeId = ActivePOSAlert[5]
				AlertGMT = ActivePOSAlert[6]
				AlertInfo = ActivePOSAlert[7]
				IsActive = ActivePOSAlert[8]
				IsSentEmail = ActivePOSAlert[9]
				SentEmailGMT = ActivePOSAlert[10]
				ClearedGMT = ActivePOSAlert[11]
				ClearedByUSerId = ActivePOSAlert[12]
				CreatedGMT = ActivePOSAlert[13]
				self.__EMS7Cursor.execute("insert ignore into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT))
				self.__EMS7Connection.commit()

# The __addActivePOSAlertForUserDefinedAlert adds the Userdefined Alerts to ActivePOSAlert table
	def addActivePOSAlertForUserDefinedAlert(self):
		self.__EMS7Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId=20 group by PointOfSaleId")
		if (self.__EMS7Cursor.rowcount<>0):
			ActiveAlerts=self.__EMS7Cursor.fetchall()
			for ActivePOSAlert in ActiveAlerts:
				self.__printActivePOSAlertForPaystation(ActivePOSAlert)
				CustomerAlertTypeId = ActivePOSAlert[0]
				PointOfSaleId = ActivePOSAlert[1]
				EventDeviceTypeId = ActivePOSAlert[2]
				EventStatusTypeId = ActivePOSAlert[3]
				EventActionTypeId = ActivePOSAlert[4]
				EventSeverityTypeId = ActivePOSAlert[5]
				AlertGMT = ActivePOSAlert[6]
				AlertInfo = ActivePOSAlert[7]
				IsActive = ActivePOSAlert[8]
				IsSentEmail = ActivePOSAlert[9]
				SentEmailGMT = ActivePOSAlert[10]
				ClearedGMT = ActivePOSAlert[11]
				ClearedByUSerId = ActivePOSAlert[12]
				CreatedGMT = ActivePOSAlert[13]
				self.__EMS7Cursor.execute("insert ignore into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT))
				self.__EMS7Connection.commit()
#				self.__EMS7Cursor.execute("insert into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUserId, CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUSerId,CreatedGMT))
#				self.EMS7Connection.commit()

	def __printActivePOSAlertForPaystation(self, ActivePOSAlert):
		if(self.__verbose):
			print " -- CustomerAlertTypeId %s" %ActivePOSAlert[0]
			print " -- PointOfSale %s" % ActivePOSAlert[1]
			print " -- EventDeviceTYpeId %s" %ActivePOSAlert[2]
			print " -- EventStatusTypeId %s" %ActivePOSAlert[3]
			print " -- EventActionTypeId %s" %ActivePOSAlert[4]
			print " -- EventSeverityTypeId %s" %ActivePOSAlert[5]
			print " -- AlertGMT %s" %ActivePOSAlert[6]
			print " -- AlertInfo %s" %ActivePOSAlert[7]
			print " -- IsActive %s" %ActivePOSAlert[8]
			print " -- IsSentEmail %s" %ActivePOSAlert[9]
			print " -- SentEmailGMT %s" %ActivePOSAlert[10]
			print " -- ClearedGMT %s" %ActivePOSAlert[11]
			print " -- ClearedByUserId %s" %ActivePOSAlert[12]
			print " -- CreatedGMT %s" %ActivePOSAlert[13]

	def __printActivePOSAlert(self, ActivePOSAlert):
		if(self.__verbose):
			print " -- AlertId %s" %ActivePOSAlert[0]
			print " -- PaystationId %s" %ActivePOSAlert[1]
			print " -- IsAlerted %s" %ActivePOSAlert[2] 

	def __getCustomerIdFromPointOfSale(self, EMS6PaystationId):
		if(EMS6PaystationId in self.__customerIdfromposIdCache):
			return self.__customerIdfromposIdCache[EMS6PaystationId]

		EMS7CustomerId=None
		self.__EMS7Cursor.execute("select CustomerId from PointOfSale where PaystationId=(select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s)",(EMS6PaystationId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchall()
			EMS7CustomerId=row[0]

		self.__customerIdfromposIdCache[EMS6PaystationId] = EMS7CustomerId
		return EMS7CustomerId
		
	def __getEventSeverityTypeId(self,DeviceId,EventStatusTypeId,ActionId):
		EventSeverityKeyId=None
		EventSeverityKeyId = (DeviceId,EventStatusTypeId,ActionId)
                EventSeverityKeyId = ','.join(map(str,EventSeverityKeyId))

                if(EventSeverityKeyId in self.__eventiseverityIdCache):
                        return self.__eventiseverityIdCache[EventSeverityKeyId]

		EventSeverityTypeId=None	
		self.__EMS7Cursor.execute("select EventSeverityTypeId from EventDefinition where EventDeviceTypeId=%s and EventStatusTypeId=%s and EventActionTypeId=%s",(DeviceId,EventStatusTypeId,ActionId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EventSeverityTypeId = row[0]
		return EventSeverityTypeId

	def __printEventLogNew(self,EventNewLog):
		if(self.__verbose):
			print " -- PaystationId %s" %EventNewLog[0]
			print " -- DeviceId %s " %EventNewLog[1]
			print " -- TypeId %s" %EventNewLog[2]
			print " -- ActionID %s" %EventNewLog[3]
			print " -- DateField %s" %EventNewLog[4]
			print " -- Information %s" %EventNewLog[5]
			print " -- ClearUserAccountID %s" %EventNewLog[6]
			print " -- ClearDateField %s" %EventNewLog[7]

	def __getCustomerAlertTypeIdForPaystationAlert(self,CustomerId, TypeId):
		EMS7CustomerAlertTypeId=None
		self.__EMS7Cursor.execute("select Id from CustomerAlertType where CustomerId=%s and AlertThresholdTypeId=%s",(CustomerId,TypeId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerAlertTypeId = row[0]
		return EMS7CustomerAlertTypeId

	def __getCustomerAlertTypeId(self,TypeId):
		EMS7CustomerAlertTypeId=None
		self.__EMS7Cursor.execute("select EMS7Id from CustomerAlertTypeMapping where EMS6Id=%s",(TypeId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerAlertTypeId = row[0]
		return EMS7CustomerAlertTypeId

	def addCustomerAlertToEMS7(self, CustomerAlert, Action):
		tableName='CustomerAlert'
		## Make sure that the alert name in EMS6 matches the AlertName is EMS7 in the table Alert Type, the following two changes were manually done on the database to have the data syncronize
#		 update AlertType set Name='Last Seen Interval' where Name='Last Seen Interval Hours';
#		 update AlertType set Name='Running Total Dollars' where Name='Running Total'; 
#		self.__updateAlertNames(self)
		RouteId = None
		Version=0
		LastModifiedGMT =date.today()
		LastModifiedByUserId=1
		self.__printCustomerAlert(CustomerAlert)
		EMS6AlertId = CustomerAlert[0]
		SpecialAlertId = CustomerAlert[0]
		print" EMS6ALERTID is %s" %EMS6AlertId
		PaystationGroupId = CustomerAlert[1]
		EMS6CustomerId = CustomerAlert[2]
		AlertName = CustomerAlert[3]
		AlertTypeId = CustomerAlert[4]
		Threshold = CustomerAlert[5]
		Email = CustomerAlert[6]
		IsEnabled = CustomerAlert[7]
		IsDeleted = CustomerAlert[8]
		RegionId = CustomerAlert[9]
		IsPaystationBased = CustomerAlert[10]
		EMS7LocationId = None
		#EMS7LocationId = self.__getLocationId(RegionId)
		EMS7CustomerId = None
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		RouteId = self.__getEMS7RouteId(PaystationGroupId)
		# The Cursor below will fetch the AlertType details within EMS6 to identify the correct EMS6Types.
		print "** EMS7CustomerId is **:",EMS7CustomerId
		#print "EMS7LocationId is:", EMS7LocationId
		
		if(EMS7CustomerId):
			
			#self.__EMS7Cursor.execute("select Id,Name,DisplayName from EMS6AlertType where Id=%s",(AlertTypeId))
			#self.__EMS7Cursor.execute("select Id,Name from AlertThresholdType where Id=%s",(AlertTypeId))
			#EMS6AlertTypes=self.__EMS7Cursor.fetchall()	
			#for AlertType in EMS6AlertTypes:
			#	print " -- Id %s" %AlertType[0]
			#	EMS6AlertTypeId = AlertType[0]
			#	print " -- Name %s" %AlertType[1]
			#	EMS6AlertName = AlertType[1]
			#	#print " -- DisplayName %s" %AlertType[2]
			#	#DisplayName = AlertType[2]
			#	DisplayName = EMS6AlertName
			#	if(EMS6AlertName=='Communication'):
			#		EMS7AlertClassTypeId=1
			#	if(EMS6AlertName=='Collection'):
			#		EMS7AlertClassTypeId=2
			#	if(EMS6AlertName=='Pay Station Alert'):
			#		EMS7AlertClassTypeId=3
			#	if(DisplayName=='Last Seen Interval Hour'):
			#		EMS7AlertTypeId=1
			#	if(DisplayName=='Running Total Dollar'):
			#		EMS7AlertTypeId=2
			#	if(DisplayName=='Coin Canister Count' or DisplayName=='Coin Canister Dollars' ):
			#		EMS7AlertTypeId=3
			#	if(DisplayName=='Bill Stacker Count' or DisplayName=='Bill Stacker Dollars' ):
			#		EMS7AlertTypeId=4
			#	if(DisplayName=='Unsettled Credit Card Count' or DisplayName=='Unsettled Credit Card Dollars' ):
			#		EMS7AlertTypeId=5
				
			#	if(DisplayName=='Last Seen Interval Hour' and EMS7AlertTypeId==1):
			#		AlertThresholdTypeId=1
			#	if(DisplayName=='Running Total Dollar' and EMS7AlertTypeId==2):
			#		AlertThresholdTypeId=2
			#	if(DisplayName=='Coin Canister Count' and EMS7AlertTypeId==3):
			#		AlertThresholdTypeId=6
			#	if(DisplayName=='Coin Canister Dollars' and EMS7AlertTypeId==3):
			#		AlertThresholdTypeId=7
			#	if(DisplayName=='Bill Stacker Count' and EMS7AlertTypeId==4):
			#		AlertThresholdTypeId=8
			#	if(DisplayName=='Bill Stacker Dollars' and EMS7AlertTypeId==4):
			#		AlertThresholdTypeId=9
			#	if(DisplayName=='Unsettled Credit Card Count' and EMS7AlertTypeId==5):
			#		AlertThresholdTypeId=10
			#	if(DisplayName=='Unsettled Credit Card Dollars' and EMS7AlertTypeId==5):
			#		AlertThresholdTypeId=11
			if(Action=='Insert'):
				print "in insert AlertTypeId is:", AlertTypeId
				#self.__EMS7Cursor.execute("insert into CustomerAlertType(CustomerId,AlertThresholdTypeId,LocationId,Name,Threshold,IsActive,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,AlertThresholdTypeId,EMS7LocationId,AlertName,Threshold,IsEnabled,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Cursor.execute("insert into CustomerAlertType(CustomerId,AlertThresholdTypeId,LocationId,Name,Threshold,IsActive,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId,RouteId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,AlertTypeId,EMS7LocationId,AlertName,Threshold,IsEnabled,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,RouteId))
				self.__EMS7Connection.commit()
				EMS7CustomerAlertTypeId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into CustomerAlertTypeMapping(EMS6Id,EMS7Id,ModifiedDate) values(%s,%s,%s)",(EMS6AlertId,EMS7CustomerAlertTypeId,LastModifiedGMT))
				self.__EMS7Connection.commit()
			if(Action=='Update'):
				if (RouteId is None):
					AlertRouteId = None
				else:
					AlertRouteId = RouteId[0]
					
				self.__EMS7Cursor.execute("select distinct(EMS7Id) from CustomerAlertTypeMapping where EMS6Id=%s",(EMS6AlertId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7CustomerAlertTypeId = row[0]
					print "EMS7CustomerAlertTypeId is:",EMS7CustomerAlertTypeId
					print "AlertRouteId is:",AlertRouteId
					self.__EMS7Cursor.execute("update CustomerAlertType set CustomerId=%s,AlertThresholdTypeId=%s,LocationId=%s,Name=%s,\
						Threshold=%s,IsActive=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s, \
						RouteId=%s where Id=%s",(EMS7CustomerId,AlertTypeId,EMS7LocationId,\
						AlertName,Threshold,IsEnabled,IsDeleted,Version,LastModifiedGMT,\
						LastModifiedByUserId,AlertRouteId,EMS7CustomerAlertTypeId))
					self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)


	def __printCustomerAlert(self, CustomerAlert):
		if(self.__verbose):
			print " -- Alert Id %s" %CustomerAlert[0]
			print " -- PaystationGroupId %s" %CustomerAlert[1]
			print " -- CustomerId %s" %CustomerAlert[2]
			print " -- AlertName %s" %CustomerAlert[3]
			print " -- AlertTypeId %s" %CustomerAlert[4]
			print " -- Threshold %s" %CustomerAlert[5]
			print " -- Email %s " %CustomerAlert[6]
			print " -- IsEnabled %s " %CustomerAlert[7]
			print " -- IsDeleted %s " %CustomerAlert[8]
			print " -- RegionId %s " %CustomerAlert[9]
			print " -- IsPaystationBased %s " %CustomerAlert[10]

	def addLocationPOSLogToEMS7(self, RegionPaystationLog, Action):
		print " adding RegionPayStaionLog to EMS 7"
		self.__printLocationPOSLog(RegionPaystationLog)
		LastModifiedByUserId = 1 
		Id=RegionPaystationLog[0]
		RegionId=RegionPaystationLog[1]
		EMS6PaystationId=RegionPaystationLog[2]
		CreationDate=RegionPaystationLog[3]
		EMS7PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EMS7LocationId = self.__getLocationId(RegionId)
		print " inserting with location id",EMS7LocationId
		print " inserting with point of sale id of ", EMS7PointOfSaleId
		if(Action=='Insert'):
			if(EMS7LocationId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("insert ignore into LocationPOSLog(LocationId,PointOfSaleId,AssignedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7LocationId,EMS7PointOfSaleId,CreationDate,LastModifiedByUserId))
				EMS7LocationPOSLogId = self.__EMS7Cursor.lastrowid			
				self.__EMS7Cursor.execute("insert ignore into RegionPaystationLogMapping(EMS6RegionPaystationId,EMS7LocationPOSLogId) values(%s,%s)",(Id,EMS7LocationPOSLogId))		
				self.__EMS7Connection.commit()
			else:
				print "LocationId does not exist for the region id %s" %RegionId

		if(Action=='Update'):
			print " ...updating EMSLocationPOSLogId..."
			EMS7LocationPOSLogId = self.__getEMS7LocationPOSLogId(Id)
			if(EMS7LocationId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("update LocationPOSLog set LocationId=%s,PointOfSaleId=%s,AssignedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7LocationId,EMS7PointOfSaleId,CreationDate,LastModifiedByUserId,EMS7LocationPOSLogId))
				self.__EMS7Connection.commit()
	
	def __getEMS7LocationPOSLogId(self,Id):
		EMS7LocationPOSLogId = None
		self.__EMS7Cursor.execute("select EMS7LocationPOSLogId from RegionPaystationLogMapping where EMS6RegionPaystationId=%s",(Id))
		if(self.__EMS7Cursor.rowcount<>0):
			EMS7LocationPOSLogId = self.__EMS7Cursor.fetchone()
		return EMS7LocationPOSLogId[0]
			
	def __printLocationPOSLog(self, RegionPaystationLog):
		if(self.__verbose):
			print " -- Id %s" %RegionPaystationLog[0]
			print " -- RegionId %s" %RegionPaystationLog[1]
			print " -- PaystationId %s" %RegionPaystationLog[2]
			print " -- CreationDate %s" %RegionPaystationLog[3]

# The Cluster table is being prepopulated, verify i dont see any records in the current model
	def addClusterToEMS7(self, Cluster, Action):
		self.__printCluster(Cluster)
		VERSION = 1
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		Id = Cluster[0]
		Name = Cluster[1]
		Hostname = Cluster[2]
		LocalPort = Cluster[3]
		self.__EMS7Cursor.execute("insert into Cluster(Id, Name,Hostname,LocalPort,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(Id, Name,Hostname,LocalPort,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printCluster(self, Cluster):
		if(self.__verbose):
			print " -- Id %s" %Cluster[0]
			print " -- Name %s" %Cluster[1]
			print " -- Hostname %s" %Cluster[2]
			print " -- LocalPort %s" %Cluster[3]

	def addCollectionLockToEMS7(self, CollectionLock, Action):
		# The new attribute in EMS7 can be assigned a value of 0
		self.__printCollectionLock(CollectionLock)
		VERSION = 1
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		EMS6ClusterId = CollectionLock[0]
		LockGMT = CollectionLock[1]
		BatchSize = CollectionLock[2]
		LockableCount = CollectionLock[3]
		LockedCount = CollectionLock[4]
		RecalcedCount = CollectionLock[5]
		IsReissuedLock = CollectionLock[6]
		RecalcMode = CollectionLock[7]
		RecalcEndGMT = CollectionLock[8]
		# NOTE !!! Alert !!! This insertion is based on the assumption that the ClusterId's match between EMS6 and EMS7 databases, This would require a manual loading on the Cluster table without an auto increment set
#		ClusterId = getClusterId()
		if (RecalcEndGMT):
			self.__EMS7Cursor.execute("insert into CollectionLock(ClusterId,LockGMT,BatchSize,LockableCount,LockedCount,RecalcedCount,RecalcEndGMT) values(%s,%s,%s,%s,%s,%s,%s)",(EMS6ClusterId,LockGMT,BatchSize,LockableCount,LockedCount,RecalcedCount,RecalcEndGMT))
			self.__EMS7Connection.commit()

	def __printCollectionLock(self, CollectionLock):
		if(self.__verbose):
			print " -- ClusterId %s" %CollectionLock[0]
			print " -- LockGMT %s" %CollectionLock[1]
			print " -- BatchSize %s" %CollectionLock[2]	
			print " -- LockableCount %s" %CollectionLock[3]
			print " -- LockedCount %s" %CollectionLock[4]
			print " -- RecalcedCount %s" %CollectionLock[5]
			print " -- IsReissuedLock %s" %CollectionLock[6]
			print " -- RecalcMode %s" %CollectionLock[7]
			print " -- RecalcEndGMT %s" %CollectionLock[8]		

	def addPOSBalanceToEMS7(self, PaystationBalance, Action):
		self.__printPOSBalance(PaystationBalance)
		EMS6PaystationId = PaystationBalance[0]
		CashDollars = PaystationBalance[1]
		CoinCount = PaystationBalance[2]
		CoinDollars = PaystationBalance[3]
		BillCount = PaystationBalance[4]
		BillDollars = PaystationBalance[5]
		UnsettledCreditCardCount = PaystationBalance[6]
		UnsettledCreditCardDollars = PaystationBalance[7]
		TotalDollars = PaystationBalance[8]
		LastCashCollectionGMT = PaystationBalance[9]
		LastCoinCollectionGMT = PaystationBalance[10]
		LastBillCollectionGMT = PaystationBalance[11]
		LastCardCollectionGMT = PaystationBalance[12]
		LastRecalcGMT = PaystationBalance[13]
		CollectionRecalcGMT = PaystationBalance[14]
		CollectionRecalcTransactions = PaystationBalance[15]
		ClusterId = PaystationBalance[16]	
		HasRecentCollection = PaystationBalance[17]
		IsRecalcable = PaystationBalance[18]		
		LastCollectionTypeId = PaystationBalance[19]
		CollectionLockId = PaystationBalance[20]
		PrevCollectionLockId = PaystationBalance[21]
		PrevPrevCollectionLockId = PaystationBalance[22]
		NextRecalcGMT = PaystationBalance[23]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (PointOfSaleId and ClusterId):
		# As Per Discussion we are not migrating working tables like POSBalance.
			self.__EMS7Cursor.execute("insert into POSBalance(PointOfSaleId, ClusterId, CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, CashAmount, CoinCount, CoinAmount, BillCount, BillAmount, UnsettledCreditCardCount, UnsettledCreditCardAmount, TotalAmount, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId, LastRecalcGMT, IsRecalcable, NextRecalcGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId, ClusterId, CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, CashDollars, CoinCount, CoinDollars, BillCount , BillDollars, UnsettledCreditCardCount, UnsettledCreditCardDollars, TotalDollars, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId, LastRecalcGMT, IsRecalcable, NextRecalcGMT))
			self.__EMS7Connection.commit()

	def __printPOSBalance(self, PaystationBalance):
		if(self.__verbose):
			print " -- PaystationId %s" %PaystationBalance[0]
			print " -- CashDollars %s" %PaystationBalance[1]
			print " -- CoinCount %s" %PaystationBalance[2]
			print " -- CoinDollars %s" %PaystationBalance[3]
			print " -- BillCount %s" %PaystationBalance[4]
			print " -- BillDollars %s" %PaystationBalance[5]
			print " -- UnsettledCreditCardCounti %s" %PaystationBalance[6]
			print " -- UnsettledCreditCardDollars %s" %PaystationBalance[7]
			print " -- TotalDollars %s" %PaystationBalance[8]
			print " -- LastCashCollectionGMT %s" %PaystationBalance[9]
			print " -- LastCoinCollectionGMT %s" %PaystationBalance[10]
			print " -- LastBillCollectionGMT %s" %PaystationBalance[11]
			print " -- LastCardCollectionGMT %s" %PaystationBalance[12]
			print " -- LastRecalcGMT %s" %PaystationBalance[13]
			print " -- CollectionRecalcGMT %s" %PaystationBalance[14]
			print " -- CollectionRecalcTransactions %s" %PaystationBalance[15]
			print " -- ClusterId %s" %PaystationBalance[16]	
			print " -- HasRecentCollection %s" %PaystationBalance[17]
			print " -- IsRecalcable %s" %PaystationBalance[18]		
			print " -- LastCollectionTypeId %s" %PaystationBalance[19]
			print " -- CollectionLockId %s" %PaystationBalance[20]
			print " -- PrevCollectionLockId %s" %PaystationBalance[21]
			print " -- PrevPrevCollectionLockId %s" %PaystationBalance[22]
			print " -- NextRecalcGMT %s" %PaystationBalance[23]

	def addPOSHeartBeatToEMS7(self, PaystationHeartBeat , Action):
		self.__printPaystationHeartBeat(PaystationHeartBeat)
		EMS6PaystationId= PaystationHeartBeat[0]
		LastHeartBeatGMT = PaystationHeartBeat[1]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("insert into POSHeartbeat(PointOfSaleId,LastHeartbeatGMT) values(%s,%s)",(PointOfSaleId,LastHeartBeatGMT))
			self.__EMS7Connection.commit()

	def __printPaystationHeartBeat(self, PaystationHeartBeat):
		if(self.__verbose):
			print " -- Paystation %s " %PaystationHeartBeat[0]
			print " -- LastHeartBeatGMT %s" %PaystationHeartBeat[1]

	def addPOSServiceStateToEMS7(self, ServiceState, Action):
		print " ...adding POSServiceState to EMS 7 method...."
		tableName='ServiceState'
		self.__printPOSServiceState(ServiceState)
		VERSION = 1
		LastModifiedGMT = date.today()
		EMS6PaystationId=ServiceState[0]
		version=ServiceState[1]
		LastHeartBeat=ServiceState[2]
		IsDoorOpen=ServiceState[3]
		IsDoorUpperOpen=ServiceState[4]
		IsDoorLowerOpen=ServiceState[5]
		IsInServiceMode=ServiceState[6]
		Battery1Voltage=ServiceState[7]
		Battery2Voltage=ServiceState[8]
		IsBillAcceptor=ServiceState[9]
		IsBillAcceptorJam=ServiceState[10]
		IsBillAcceptorUnableToStack=ServiceState[11]
		IsBillStacker=ServiceState[12]
		BillStackerSize=ServiceState[13]
		BillStackerCount=ServiceState[14]
		IsCardReader=ServiceState[15]
		IsCoinAcceptor=ServiceState[16]
		CoinBagCount=ServiceState[17]
		IsCoinHopper1=ServiceState[18]
		CoinHopper1Level=ServiceState[19]
		CoinHopper1DispensedCount=ServiceState[20]
		IsCoinHopper2=ServiceState[21]
		CoinHopper2Level=ServiceState[22]
		CoinHopper2DispensedCount=ServiceState[23]
		IsPrinter=ServiceState[24]
		IsPrinterCutterError=ServiceState[25]
		IsPrinterHeadError=ServiceState[26]
		IsPrinterLeverDisengaged=ServiceState[27]
		PrinterPaperLevel=ServiceState[28]
		IsPrinterTemperatureError=ServiceState[29]
		IsPrinterVoltageError=ServiceState[30]
		Battery1Level=ServiceState[31]
		Battery2Level=ServiceState[32]
		IsLowPowerShutdownOn=ServiceState[33]
		IsShockAlarmOn=ServiceState[34]
		WirelessSignalStrength=ServiceState[35]
		IsCoinAcceptorJam=ServiceState[36]
		BillStackerLevel=ServiceState[37]
		TotalSinceLastAudit=ServiceState[38]
		CoinChangerLevel=ServiceState[39]
		IsCoinChanger=ServiceState[40]
		IsCoinChangerJam=ServiceState[41]
		LotNumber=ServiceState[42]
		MachineNumber=ServiceState[43]
		BBSerialNumber=ServiceState[44]
		PrimaryVersion=ServiceState[45]
		SecondaryVersion=ServiceState[46]
		UpgradeDate=ServiceState[47]
		AlarmState=ServiceState[48]
		IsNewLotSetting=ServiceState[49]
		LastLotSettingUpload=ServiceState[50]
		IsNewTicketFooter=ServiceState[51]
		IsNewPublicKey=ServiceState[52]
		AmbientTemperature=ServiceState[53]
		ControllerTemperature=ServiceState[54]
		InputCurrent=ServiceState[55]
		SystemLoad=ServiceState[56]
		RelativeHumidity=ServiceState[57]
		IsCoinCanister=ServiceState[58]
		IsCoinCanisterRemoved=ServiceState[59]
		IsBillCanisterRemoved=ServiceState[60]
		IsMaintenanceDoorOpen=ServiceState[61]
		IsCashVaultDoorOpen=ServiceState[62]
		IsCoinEscrow=ServiceState[63]
		IsCoinEscrowJam=ServiceState[64]
		IsCommunicationAlerted=ServiceState[65]
		IsCollectionAlerted=ServiceState[66]
		UserDefinedAlarmState=ServiceState[67]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		# Is RFID Card reader a new column in EMS7.POSServicveState table, if not where is the value for the column arriving from in EMS7
		# Ans : Yes this is a new column in EMS7
		if(Action=='Insert'):
			print " action insert with PointOfSaleId of", PointOfSaleId
			if(PointOfSaleId):
				self.__EMS7Cursor.execute("insert into POSServiceState(PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastPaystationSettingUploadGMT, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalAmountSinceLastAudit, UpgradeGMT, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewPaystationSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastLotSettingUpload, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalSinceLastAudit, UpgradeDate, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewLotSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT))
				EMS7POSServiceStateId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into ServiceStateMapping(PaystationId,EMS7POSServiceStateId,MachineNumber) values(%s,%s,%s)",(EMS6PaystationId,MachineNumber,EMS7POSServiceStateId))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6PaystationId,tableName,IsMultiKey)

		if(Action=='Update'):
			print " Action is update"
			self.__EMS7Cursor.execute("update POSServiceState set AmbientTemperature=%s, Battery1Level=%s, Battery1Voltage=%s, Battery2Level=%s, Battery2Voltage=%s, BBSerialNumber=%s, BillStackerCount=%s, BillStackerLevel=%s, BillStackerSize=%s, CoinBagCount=%s, CoinChangerLevel=%s, CoinHopper1DispensedCount=%s, CoinHopper1Level=%s, CoinHopper2DispensedCount=%s, CoinHopper2Level=%s, ControllerTemperature=%s, InputCurrent=%s, LastPaystationSettingUploadGMT=%s, PrimaryVersion=%s, PrinterPaperLevel=%s, RelativeHumidity=%s, SecondaryVersion=%s, SystemLoad=%s, TotalAmountSinceLastAudit=%s, UpgradeGMT=%s, WirelessSignalStrength=%s, IsBillAcceptor=%s, IsBillStacker=%s, IsCardReader=%s, IsCoinAcceptor=%s, IsCoinCanister=%s, IsCoinChanger=%s, IsCoinEscrow=%s, IsCoinHopper1=%s, IsCoinHopper2=%s, IsPrinter=%s, IsNewPaystationSetting=%s, IsNewPublicKey=%s, IsNewTicketFooter=%s, VERSION=%s, LastModifiedGMT=%s where PointOfSaleId=%s",(AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastLotSettingUpload, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalSinceLastAudit, UpgradeDate, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewLotSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT,PointOfSaleId))
			self.__EMS7Connection.commit()

	def __printPOSServiceState(self, ServiceState):
		if(self.__verbose):
			print " -- PaystationId %s" %ServiceState[0]
			print " -- version %s" %ServiceState[1]
			print " -- LastHeartBeat %s" %ServiceState[2]
			print " -- IsDoorOpen %s" %ServiceState[3]
			print " -- IsDoorUpperOpen %s" %ServiceState[4]
			print " -- IsDoorLowerOpen %s" %ServiceState[5]
			print " -- IsInServiceMode %s" %ServiceState[6]
			print " -- Battery1Voltage %s" %ServiceState[7]
			print " -- Battery2Voltage %s" %ServiceState[8]
			print " -- IsBillAcceptor %s" %ServiceState[9]
			print " -- IsBillAcceptorJam %s" %ServiceState[10]
			print " -- IsBillAcceptorUnableToStack %s" %ServiceState[11]
			print " -- IsBillStacker %s" %ServiceState[12]
			print " -- BillStackerSize %s" %ServiceState[13]
			print " -- BillStackerCount %s" %ServiceState[14]
			print " -- IsCardReader %s" %ServiceState[15]
			print " -- IsCoinAcceptor %s" %ServiceState[16]
			print " -- CoinBagCount %s" %ServiceState[17]
			print " -- IsCoinHopper1 %s" %ServiceState[18]
			print " -- CoinHopper1Level %s" %ServiceState[19]
			print " -- CoinHopper1DispensedCount %s" %ServiceState[20]
			print " -- IsCoinHopper2 %s" %ServiceState[21]
			print " -- CoinHopper2Level %s" %ServiceState[22]
			print " -- CoinHopper2DispensedCount %s" %ServiceState[23]
			print " -- IsPrinter %s" %ServiceState[24]
			print " -- IsPrinterCutterError %s" %ServiceState[25]
			print " -- IsPrinterHeadError %s" %ServiceState[26]
			print " -- IsPrinterLeverDisengaged %s" %ServiceState[27]
			print " -- PrinterPaperLevel %s" %ServiceState[28]
			print " -- IsPrinterTemperatureError %s" %ServiceState[29]
			print " -- IsPrinterVoltageError %s" %ServiceState[30]
			print " -- Battery1Level %s" %ServiceState[31]
			print " -- Battery2Level %s" %ServiceState[32]
			print " -- IsLowPowerShutdownOn %s" %ServiceState[33]
			print " -- IsShockAlarmOn %s" %ServiceState[34]
			print " -- WirelessSignalStrength %s" %ServiceState[35]
			print " -- IsCoinAcceptorJam %s" %ServiceState[36]
			print " -- BillStackerLevel %s" %ServiceState[37]
			print " -- TotalSinceLastAudit %s" %ServiceState[38]
			print " -- CoinChangerLevel %s" %ServiceState[39]
			print " -- IsCoinChanger %s" %ServiceState[40]
			print " -- IsCoinChangerJam %s" %ServiceState[41]
			print " -- LotNumber %s" %ServiceState[42]
			print " -- MachineNumber %s" %ServiceState[43]
			print " -- BBSerialNumber %s" %ServiceState[44]
			print " -- PrimaryVersion %s" %ServiceState[45]
			print " -- SecondaryVersion %s" %ServiceState[46]
			print " -- UpgradeDate %s" %ServiceState[47]
			print " -- AlarmState %s" %ServiceState[48]
			print " -- IsNewLotSetting %s" %ServiceState[49]
			print " -- LastLotSettingUpload %s" %ServiceState[50]
			print " -- IsNewTicketFooter %s" %ServiceState[51]
			print " -- IsNewPublicKey %s" %ServiceState[52]
			print " -- AmbientTemperature %s" %ServiceState[53]
			print " -- ControllerTemperature %s" %ServiceState[54]
			print " -- InputCurrent %s" %ServiceState[55]
			print " -- SystemLoad %s" %ServiceState[56]
			print " -- RelativeHumidity %s" %ServiceState[57]
			print " -- IsCoinCanister %s" %ServiceState[58]
			print " -- IsCoinCanisterRemoved %s" %ServiceState[59]
			print " -- IsBillCanisterRemoved %s" %ServiceState[60]
			print " -- IsMaintenanceDoorOpen %s" %ServiceState[61]
			print " -- IsCashVaultDoorOpen %s" %ServiceState[62]
			print " -- IsCoinEscrow %s" %ServiceState[63]
			print " -- IsCoinEscrowJam %s" %ServiceState[64]
			print " -- IsCommunicationAlerted %s" %ServiceState[65]
			print " -- IsCollectionAlerted %s" %ServiceState[66]
			print " -- UserDefinedAlarmState %s" %ServiceState[67]			

	def addPOSDateBillingReportToEMS7(self, POSDateBillingReport):
		CurrentSetting=None
		VERSION=1
		LastModifiedByUserId=1
		LastModifiedGMT=date.today()
		POSDateTypeId=6
		EMS6PaystationId=POSDateBillingReport[0]
		ChangedGMT=POSDateBillingReport[1]
		Settings=POSDateBillingReport[2]
		if (Settings==1):
			CurrentSetting=0
		elif (Settings==2):
			CurrentSetting=1
		self.__printPOSBillingReport(POSDateBillingReport)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting) 
		
	def __printPOSBillingReport(self, POSDateBillingReport):
		if(self.__verbose):
			print " -- PaystationId %s" %POSDateBillingReport[0]
			print " -- ChangeGMT %s" %POSDateBillingReport[1]
			print " -- BillingStatusType %s" %POSDateBillingReport[2]
			
	def addPOSDateAuditAccessToEMS7(self, POSDateAuditAccess):
		self.__printPOSAuditAccess(POSDateAuditAccess)
		VERSION=1
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		self.__printPOSDateAuditAccess(POSDateAuditAccess)
		EMS6PaystationId= POSDateAuditAccess[0]
		POSDateTypeId = 9
		ChangedGMT=POSDateAuditAccess[1]
		CurrentSetting = POSDateAuditAccess[2] #What are different types of AuditAccess Types. It appears that there are Types other then 1, and 0 eg(29).
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)	

	def __printPOSAuditAccess(self, POSDateAudiAccesst):
		if(self.__verbose):
			print " -- PaystationId %s" %POSDateAuditAccesst[0]
			print " -- ActivityDate %s" %POSDateAuditAccess[1]
			print " -- EventTypeId %s" %POSDateAuditAccess[2]

	def addPOSDatePaystationPropertiesToEMS7(self, Paystation, Action):
		print "...in addPosDatePaystationProperties to EMS mrthod....."
		ProvisionDate=None
		Version = 1
		LastModifiedGMT=date.today()
		LastModifiedByUserId =1 
		self.__printPaystationProperties(Paystation)
		EMS6PaystationId = Paystation[0]
		version = Paystation[1]
		Name = Paystation[2]
		CommAddress = Paystation[3]
		ContactURL = Paystation[4]
		CustomerId= Paystation[5]
		IsActive = Paystation[6]
		RegionId = Paystation[7]
		TimeZone = Paystation[8]
		LockState = Paystation[9]
		Provisiondate = Paystation[10]
		MerchantAccountId = Paystation[11]
		DeleteFlag = Paystation[12]
		CustomerMerchantAccountId = Paystation[13]
		PaystationType = Paystation[14]
		IsVerrus = Paystation[15]
		QueryStallsBy = Paystation[16]
		LotSettingId = Paystation[17]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
	
		if(Action=='Insert'):
			print " ...Action is insert....."

		# POSDateType = 1 : IsProvisioned
			if (ProvisionDate):
				print " Provisiondate value is", ProvisionDate
				CurrentSetting=1
				POSDateTypeId=1
				ChangedGMT=ProvisionDate
				CurrentSetting=1  # This is a turned ON state
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
			else:
				print " No provision date value"
				CurrentSetting=0
				POSDateTypeId=1
				ChangedGMT=date.today() # note that if the provisioned date for a paystation does not exist then we are doing two things 1: setting the provisioned date to be the current date and 2: setting the CurrentSetting to be 0 which means turned off, this needs to be confirmed.
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateType = 2 : Is Locked. I am setting the default value of now for ChangedGMT parameter until confirmed, also if the locked state is Locked, The value for currentsetting is 1, otherwise it is 0.
			if(LockState=='UNLOCKED'):
				print " ...LockState is Unlocked...."
				CurrentSetting=0
				POSDateTypeId=2
				ChangedGMT=date.today()
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
			else:
				print " ...No Lockstate data found..."
				# Note : All the other states of the Paysation that are not in UNLOCKED state are all recorded as in OFF state. Please confirm.
				CurrentSetting=1
				POSDateTypeId=2
				ChangedGMT=date.today()
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateType = 3: Is Decommissoned 
			if (PointOfSaleId):
				print " point of sale id is found ", PointOfSaleId
				# If there is an x in the CommAddress then it is Decommsioned a value of 1, that is it decommiosned else it is 0, that means irt is active., or if the Delete Flag is on then it is decomissioned, 
				# if it X'd out or delete flag is turned on thehn it is isdecommioned is true
				if(re.search(r"(?i)x", CommAddress)):
					print "The Paystation has X in it"
					CurrentSetting=1
					POSDateTypeId=3
				else:
					CurrentSetting=0
					POSDateTypeId=3

			#POSDateType = 4: Is Deleted
			if (DeleteFlag==1 or re.search(r"(?i)x", CommAddress)):
				print " Delete flag is 1"
				CurrentSetting=1
				POSDateTypeId=4
				ChangedGMT=date.today()
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting) 
			else:
				print " No delete flag..."
				CurrentSetting=0
				POSDateTypeId=4
				ChangedGMT=date.today()
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)	

			#POSDateTypeId = 5: IsVisiable : This parameter is beig set to a default value for all paystation which is a value of 1
			CurrentSetting=1
			POSDateTypeId=5
			ChangedGMT=date.today()
			self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)	

			#POSDateTypeId = 6: The value arrives from cerberus, the value for the billing report is set in a procedure addBillingReporToPOSDate.

			#POSDateTypeId = 7: Is Digital connect: As per Paul we do not understand the logic therefore this is being set to 0(default value. Paul uses the list sent by Rob, digital comment paystation for a report, about a thousand paystation. 
			CurrentSetting=0
			POSDateTypeId=7
			ChangedGMT=date.today()
			self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
		
			#POSDateTypeId = 8: Is Test Activated : The value is being set a default(0) for this parameter.
			CurrentSetting = 0
			POSDateTypeId=8
			ChangedGMT=date.today()
			self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateTypeId = 9: IsActivated : The value arrives from Audit Access table  

			#POSDateTypeId = 10: Is Billable Monthly for EMS7. The value is being calculated from POSDate=6 and POSDat=9. Is 6 s true and 9 is true then 10 is true else 10 is false.   	
		if(Action=='Update'):
			print " The action is update.."
			if (ProvisionDate):
				print " In update and ProvisionDate is..", ProvisionDate
				CurrentSetting=1
				POSDateTypeId=1
				ChangedGMT=ProvisionDate
				CurrentSetting=1  # This is a turned ON state
				self.__POSDateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
			else:
				print" in update with no provosiondate..."
				CurrentSetting=0
				POSDateTypeId=1
				ChangedGMT=date.today() # note that if the provisioned date for a paystation does not exist then we are doing two things 1: setting the provisioned date to be the current date and 2: setting the CurrentSetting to be 0 which means turned off, this needs to be confirmed.
				self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateType = 2 : Is Locked. I am setting the default value of now for ChangedGMT parameter until confirmed, also if the locked state is Locked, The value for currentsetting is 1, otherwise it is 0.
			if(LockState=='UNLOCKED'):
				print" The action is update and Lockstate in UNLOCKED.."
				CurrentSetting=0
				POSDateTypeId=2
				ChangedGMT=date.today()
				self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			else:
				print" in update action but no Lockstate data"
				# Note : All the other states of the Paysation that are not in UNLOCKED state are all recorded as in OFF state. Please confirm.
				CurrentSetting=1
				POSDateTypeId=2
				ChangedGMT=date.today()
				self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateType = 3: Is Decommissoned 
			if (PointOfSaleId):
				print " In action update and pointofsaleId", PointOfSaleId
				# If there is an x in the CommAddress then it is Decommsioned a value of 1, that is it decommiosned else it is 0, that means irt is active., or if the Delete Flag is on then it is decomissioned, 
				# if it X'd out or delete flag is turned on thehn it is isdecommioned is true
				if(re.search(r"(?i)x", CommAddress)):
					print "The Paystation has X in it"
					CurrentSetting=1
					POSDateTypeId=3
					self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
				else:
					print " In action update but no pointOFSaleId"
					CurrentSetting=0
					POSDateTypeId=3
					self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateType = 4: Is Deleted
			if (DeleteFlag==1 or re.search(r"(?i)x", CommAddress)):
				print " in action update and deleteflag 1"
				CurrentSetting=1
				POSDateTypeId=4
				ChangedGMT=date.today()
				self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)
			else:
				print " in action update and no delete flag..."
				CurrentSetting=0
				POSDateTypeId=4
				ChangedGMT=date.today()
				self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateTypeId = 5: IsVisiable : This parameter is beig set to a default value for all paystation which is a value of 1
			CurrentSetting=1
			POSDateTypeId=5
			ChangedGMT=date.today()
			self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateTypeId = 6: The value arrives from cerberus, the value for the billing report is set in a procedure addBillingReporToPOSDate.

			#POSDateTypeId = 7: Is Digital connect: As per Paul we do not understand the logic therefore this is being set to 0(default value. Paul uses the list sent by Rob, digital comment paystation for a report, about a thousand paystation. 
			CurrentSetting=0
			POSDateTypeId=7
			ChangedGMT=date.today()
			self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)

			#POSDateTypeId = 8: Is Test Activated : The value is being set a default(0) for this parameter.
			CurrentSetting = 0
			POSDateTypeId=8
			ChangedGMT=date.today()
			self.__POSDateUpdateSetCurrentSetting(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting)


			print " finished the task of adding addPOSDatePaystationPropertiesToEMS7 ... coming out of the method...."
			#POSDateTypeId = 9: IsActivated : The value arrives from Audit Access table  

			#POSDateTypeId = 10: Is Billable Monthly for EMS7. The value is being calculated from POSDate=6 and POSDat=9. Is 6 s true and 9 is true then 10 is true else 10 is false.   		
	

	def __POSDateSetCurrentSetting(self, PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting):
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		Version=1
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("insert ignore into POSDate(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting,Version,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()

	def __POSDateUpdateSetCurrentSetting(self, PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting):
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		Version=1
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("update POSDate set PointOfSaleId=%s,POSDateTypeId=%s,ChangedGMT=%s,CurrentSetting=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where PointOfSaleId=%s and ChangedGMT=%s and POSDateTypeId=%s",(PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting,Version,LastModifiedGMT,LastModifiedByUserId, PointOfSaleId,ChangedGMT,POSDateTypeId))
			self.__EMS7Connection.commit()

	def __printPaystationProperties(self, Paystation):
		if(self.__verbose):
			print " -- Id %s" %Paystation[0]
			print " -- version %s" %Paystation[1]
			print " -- CommAddress %s" %Paystation[2]
			print " -- Contact URL %s" %Paystation[3]
			print " -- CustomerId %s" %Paystation[4]
			print " -- IsActive %s" %Paystation[5]
			print " -- RegionId %s" %Paystation[6]
			print " -- TimeZone %s" %Paystation[7]
			print " -- LockState %s" %Paystation[8]
			print " -- ProvisionDate %s" %Paystation[9]
			print " -- MerchantAccountId %s" %Paystation[10]
			print " -- DeleteFlag %s" %Paystation[11]
			print " -- CustomCardMerchantAccountId %s" %Paystation[12]
			print " -- Paystation Type %s" %Paystation[13]
			print " -- IsVerrus %s" %Paystation[14]
			print " -- QueryStallsBy %s" %Paystation[15]
			print " -- LotSettingId %s" %Paystation[16]

	def addBadValueCardToEMS7(self, BadValueCard, Action):
		print " ...adding bad value cards to EMS 7....."
		self.__printBadValueCard(BadValueCard)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1 
		VERSION = 1
		AddedGMT = date.today()
		Comment = None
		EMS6CustomerId = BadValueCard[0]
		AccountNumber = BadValueCard[1]
		version = BadValueCard[2]
		CardType = BadValueCard[3]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			if (EMS6CustomerId<>None):
			# This is the section value bad cards 
				CardTypeId = 3
				if (EMS7CustomerId):
					print " first creating customer card type data...."	
					print " Customer id is: ", EMS7CustomerId
					print " CardTypeId is: ",CardTypeId
					print " card type is ", CardType
					self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId,Name) values(%s,%s,%s)",(EMS7CustomerId,CardTypeId,CardType))
					self.__EMS7Connection.commit()
	#				EMS7CustomerCardTypeId = self.__EMS7Cursor.lastrowid
					self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
					EMS7CustomerCardTypeId=self.__EMS7Cursor.fetchone()
					print " now inserting customer bad card data in EMS7.."
					self.__EMS7Cursor.execute("insert ignore into CustomerBadCard(CustomerCardTypeId,CardNumberOrHash,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId[0],AccountNumber,version,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
					self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " ....now updating badcard data......"
			if (EMS6CustomerId<>None):
				print " EMS6CustomerId is:",EMS6CustomerId
			# The section below updates the CustomerBadCard table, The card is uniquely identified by CustomerId and AccountNumber 
				CardTypeId = 3
				if (EMS7CustomerId):
					self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerCardTypeId=row[0]
						self.__EMS7Cursor.execute("select Id from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,AccountNumber))
						if(self.__EMS7Cursor.rowcount<>0):
							row =  self.__EMS7Cursor.fetchone()
							EMS7CustomerBadCardId = row[0]
							self.__EMS7Cursor.execute("update CustomerBadCard set CustomerCardTypeId=%s,CardNumberOrHash=%s,VERSION=%s,AddedGMT=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(EMS7CustomerCardTypeId,AccountNumber,version,AddedGMT,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerBadCardId))
							self.__EMS7Connection.commit()
		if(Action=='Delete'):
			print " ....in deleting customer bad card data...."
			if (EMS6CustomerId<>None):
			# The section below updates the CustomerBadCard table, The card is uniquely identified by CustomerId and AccountNumber 
				CardTypeId = 3
				if (EMS7CustomerId):
					self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerCardTypeId=row[0]
						self.__EMS7Cursor.execute("select Id from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,AccountNumber))
						if(self.__EMS7Cursor.rowcount<>0):
							row =  self.__EMS7Cursor.fetchone()
							EMS7CustomerBadCardId = row[0]
							self.__EMS7Cursor.execute("delete CustomerBadCard where Id=%s",(EMS7CustomerBadCardId))
							self.__EMS7Connection.commit()
			
	def __printBadValueCard(self,BadValueCard):
		if(self.__verbose):
			print " -- EMS6CustomerId %s" %BadValueCard[0]
			print " -- AccountNumber %s" %BadValueCard[1]
			print " -- Version %s" %BadValueCard[2]
			print " -- CardType %s" %BadValueCard[3]

	def addBadCreditCardToEMS7(self, BadCreditCard, Action):
		print " Adding bad credit card to EMS 7"
		self.__printBadCreditCard(BadCreditCard)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1 
		VERSION = 1
		AddedGMT = date.today()
		Comment = None
		EMS6CustomerId = BadCreditCard[0]
		CardHash = BadCreditCard[1]
		CardData = BadCreditCard[2]
		CardExpiry = BadCreditCard[3]
		AddedDate = BadCreditCard[4]
		Comment = BadCreditCard[5]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			print " ..step 4 of inserting bad credit card..."
			if (EMS6CustomerId<>None):
			#This is the section Credit cards 
				CardTypeId = 1	
				self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
				self.__EMS7Connection.commit()
				print "The Customer Is is %s" %EMS7CustomerId
				print "The Card Type Id is %s" %CardTypeId
				print " step 5 of inserting bad credit card..."
				self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				EMS7CustomerCardTypeId=self.__EMS7Cursor.fetchone()
				print "CustomerCardType Id is %s" %EMS7CustomerCardTypeId
				self.__EMS7Cursor.execute("insert ignore into CustomerBadCard(CustomerCardTypeId,CardNumberOrHash,CardData,CardExpiry,Comment,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId[0],CardHash,CardData,CardExpiry,Comment,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
		if(Action=='Update'):
			if (EMS6CustomerId<>None):
				print "Inside the Update section of bad credit card.."
			#This is the section Credit cards 
				CardTypeId = 1	
				self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				EMS7CustomerCardTypeId=self.__EMS7Cursor.fetchone()
				print "CustomerCardType Id is %s" %EMS7CustomerCardTypeId
				self.__EMS7Cursor.execute("update CustomerBadCard set CustomerCardTypeId=%s,CardNumberOrHash=%s,CardData=%s,CardExpiry=%s,Comment=%s,VERSION=%s,AddedGMT=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId[0],CardHash,CardData,CardExpiry,Comment,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerCardTypeId[0],CardHash))
				self.__EMS7Connection.commit()

	def __printBadCreditCard(self, BadCreditCard):
		if(self.__verbose):
			print " -- EMS Customer Id %s " %BadCreditCard[0]	
			print " -- CardHash %s" %BadCreditCard[1]
			print " -- CardData %s" %BadCreditCard[2]
			print " -- CardExpiry %s" %BadCreditCard[3]
			print " -- AddedDate %s" %BadCreditCard[4]
			print " -- Comment %s" %BadCreditCard[5]

	def addBadSmartCardToEMS7(self, BadSmartCard, Action):
		print " adding bad smart cards to EMS 7 ....."
##		self.__printBadSmartCard(BadSmartCard)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1 
		VERSION = 1
		AddedGMT = date.today()
		Comment = None
		EMS6CustomerId = BadSmartCard[0]
		CardNumber = BadSmartCard[1]
		AddedDate = BadSmartCard[2]
		Comment = BadSmartCard[3]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			if (EMS6CustomerId<>None):
			# This is the section bad Smart Cards 
				CardTypeId = 2	
				print " inserting into CustomerCardType table ..."
				self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
				self.__EMS7Connection.commit()
				print " The Customer Is is %s" %EMS7CustomerId
				print " The Card Type Id is %s" %CardTypeId
				self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				EMS7CustomerCardTypeId=self.__EMS7Cursor.fetchone()
				print "inserting into customer bad card table...CustomerCardType Id is %s" %EMS7CustomerCardTypeId
				self.__EMS7Cursor.execute("insert ignore into CustomerBadCard(CustomerCardTypeId,CardNumberOrHash,AddedGMT,Comment,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId[0],CardNumber,AddedGMT,Comment,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " updating bad smart card record.."
			if (EMS6CustomerId<>None):
			# This is the section bad Smart Cards 
				CardTypeId = 2	
				self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
				EMS7CustomerCardTypeId=self.__EMS7Cursor.fetchone()
				print "CustomerCardType Id is %s" %EMS7CustomerCardTypeId
				self.__EMS7Cursor.execute("update CustomerBadCard set CustomerCardTypeId=%s,CardNumberOrHash=%s,AddedGMT=%s,Comment=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId[0],CardNumber,AddedGMT,Comment,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerCardTypeId[0],CardNumber))
				self.__EMS7Connection.commit()

	def addCCFailLogToEMS7(self, CCFailLog, Action):
		print " Inserting CC fail log to EMS 7"
		ModifiedDate=date.today()
		Id = CCFailLog[0]
		EMS6PaystationId =CCFailLog[1]
		EMS6MerchantAccountId = CCFailLog[2]
		TicketNumber = CCFailLog[3]
		ProcessingDate = CCFailLog[4]
		PurchaseDate = CCFailLog[5]
		CCType = CCFailLog[6]
		Reason = CCFailLog[7]	
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)	
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		print " merchataccountid is ", EMS7MerchantAccountId
		print " point of sale id is ", PointOfSaleId
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into CCFailLog(PointOfSaleId,MerchantAccountId,TicketNumber,ProcessingDate,PurchasedDate,CCType,Reason) values(%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,EMS7MerchantAccountId,TicketNumber,ProcessingDate,PurchaseDate,CCType,Reason))
			EMS7CCFailLogId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into CCFailLogMapping(EMS6Id,EMS7Id, ModifiedDate) values(%s,%s,%s)",(Id,EMS7CCFailLogId,ModifiedDate))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " updating CC failLog"
			self.__EMS7Cursor.execute("select distinct(EMS7Id) from CCFailLogMapping where EMS6Id=%s",(Id))
			if(self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				EMS7CCFailLogId = row[0]
				self.__EMS7Cursor.execute("update CCFailLog set PointOfSaleId=%s,MerchantAccountId=%s,TicketNumber=%s,ProcessingDate=%s,PurchasedDate=%s,CCType=%s,Reason=%s where Id=%s",(PointOfSaleId,EMS7MerchantAccountId,TicketNumber,ProcessingDate,PurchaseDate,CCType,Reason,EMS7CCFailLogId))
				self.__EMS7Connection.commit()
	
	def addServiceAgreementToEMS7(self,ServiceAgreement,Action):
		Id = ServiceAgreement[0]
		Content = ServiceAgreement[1]
		ContentVersion = ServiceAgreement[2]
		UploadedDate = ServiceAgreement[3]
		VERSION=1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		self.__EMS7Cursor.execute("insert into ServiceAgreement(Content,ContentVersion,UploadedGMT,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(Content,ContentVersion,UploadedDate,VERSION,LastModifiedGMT,LastModifiedByUserId))
		EMS7ServiceAgreementId = self.__EMS7Cursor.lastrowid
		self.__EMS7Cursor.execute("insert into ServiceAgreementMapping(EMS6ServiceAgreementId,EMS7ServiceAgreementId,EMS6UploadedGMT) values(%s,%s,%s)",(Id,EMS7ServiceAgreementId,UploadedDate))
		self.__EMS7Connection.commit()

	def addCustomerAgreementToEMS7(self, ServiceAgreedCustomer,Action):
		EMS6CustomerId = ServiceAgreedCustomer[0]
		ServiceAgreementCustomerId = ServiceAgreedCustomer[1]
		Name = ServiceAgreedCustomer[2]
		Title = ServiceAgreedCustomer[3]
		Organization = ServiceAgreedCustomer[4]
		ActivationDate = ServiceAgreedCustomer[5]
		if (ActivationDate==None):
			ActivationDate=date.today()
		IsOverride = ServiceAgreedCustomer[6]	
		VERSION = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EMS7ServiceAgreementId = self.__getEMS7ServiceAgreementId(ServiceAgreementCustomerId) 
		if (EMS7CustomerId):
			self.__EMS7Cursor.execute("insert into CustomerAgreement(CustomerId,ServiceAgreementId,Title,Organization,AgreedGMT,IsOverride,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EMS7ServiceAgreementId,Title,Organization,ActivationDate,IsOverride,VERSION,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()

	def __getEMS7ServiceAgreementId(self, ServiceAgreementCustomerId):
		self.__EMS7Cursor.execute("select EMS7ServiceAgreementId from ServiceAgreementMapping where EMS6ServiceAgreementId=%s",(ServiceAgreementCustomerId))
		EMS7ServiceAgreementCustomerId = self.__EMS7Cursor.fetchone()
		return EMS7ServiceAgreementCustomerId[0]
	
	def addRawSensorDataToEMS7(self, RawSensorData, Action):
		ServerName = RawSensorData[0]
		PaystationCommAddress = RawSensorData[1]
		Type = RawSensorData[2]
		Action = RawSensorData[3]
		Information = RawSensorData[4]
		DateField = RawSensorData[5]		
		LastRetryTime = RawSensorData[6]
		RetryCount = RawSensorData[7]
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into RawSensorData(ServerName,SerialNumber,Type,Action,Information,DateField,LastRetryTime,RetryCount) values(%s,%s,%s,%s,%s,%s,%s,%s)",(ServerName,PaystationCommAddress,Type,Action,Information,DateField,LastRetryTime,RetryCount))
			EMS7RawSensorData = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into RawSensorDataMapping(ServerName,PaystationCommAddress,Type,Action,Information,DateField,EMS7RawSensorDataId) values(%s,%s,%s,%s,%s,%s,%s)",(ServerName,PaystationCommAddress,Type,Action,Information,DateField,EMS7RawSensorData))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select distinct(EMS7RawSensorDataId) from RawSensorDataMapping where ServerName=%s and PaystationCommAddress=%s and Type=%s and Action=%s and Information=%s and DateField=%s",(ServerName,PaystationCommAddress,Type,Action,Information,DateField,EMS7RawSensorData))
			EMS7RawSensorData = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("update RawSensorData set ServerName=%s,SerialNumber=%s,Type=%s,Action=%s,Information=%s,DateField=%s,LastRetryTime=%s,RetryCount=%s where Id=%s",(ServerName,PaystationCommAddress,Type,Action,Information,DateField,LastRetryTime,RetryCount,EMS7RawSensorData))
			self.__EMS7Connection.commit()

	def addRawSensorDataArchiveToEMS7(self, RawSensorDataArchive):
		ServerName = RawSensorData[0]
		PaystationCommAddress =RawSensorData[1]
		Type = RawSensorData[2]
		Action = RawSensorData[3]
		Information = RawSensorData[4]
		DateField = RawSensorData[5]
		LastRetryTime = RawSensorData[6]
		RetryCount = RawSensorData[7]
		self.__EMS7Cursor.execute("insert into RawSensorDataArchive(ServerName,SerialNumber,Type,Action,Information,DateField,LastRetryTime,RetryCount) values(%s,%s,%s,%s,%s,%s,%s,%s)",(ServerName,PaystationCommAddress,Type,Action,Information,DateField,LastRetryTime,RetryCount))
		self.__EMS7Connection.commit()

	def addReversalToEMS7(self, Reversal, Action):
		#Id = ReversalArchive[0] ASHOK commented this line and added below line and changed index to -1
		#Id = Reversal[0] 
		#MerchantAccountId = Reversal[0] commented this line also
		EMS6MerchantAccountId = Reversal[0]
		CardNumber = Reversal[1]
		ExpiryDate = Reversal[2]
		OriginalMessageType = Reversal[3]
		OriginalProcessingCode = Reversal[4]
		OriginalReferenceNumber	= Reversal[5]
		OriginalTime = Reversal[6]
		OriginalTransactionAmount = Reversal[7]
		LastResponseCode = Reversal[8]
		PurchasedDate = Reversal[9]
		TicketNumber = Reversal[10]
		#PaystationId = Reversal[11] commented this line by Ashok
		EMS6PaystationId = Reversal[11]
		RetryAttempts = Reversal[12]
		LastRetryTime = Reversal[13]
		Succeeded = Reversal[14]
		Expired = Reversal[15]
		IsRFID = Reversal[16]
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into Reversal(MerchantAccountId,CardData,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,IsSucceeded,IsExpired) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired))
			EMS7ReversalId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into ReversalMapping(MerchantAccountId,OriginalReferenceNumber,OriginalTime,EMS7Id) values(%s,%s,%s,%s)",(EMS7MerchantAccountId,OriginalReferenceNumber,OriginalTime,EMS7ReversalId))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select distinct(EMS7Id) from ReversalMapping where MerchantAccountId=%s and OriginalReferenceNumber=%s and OriginalTime=%s",(EMS7MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			if(self.__EMS7Cursor.rowcount<>0):
				EMS7ReversalId = self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("update Reversal set MerchantAccountId=%s,CardNumber=%s,ExpiryDate=%s,OriginalMessageType=%s,OriginalProcessingCode=%s,OriginalReferenceNumber=%s,OriginalTime=%s,OriginalTransactionAmount=%s,LastResponseCode=%s,PurchasedDate=%s,TicketNumber=%s,PointOfSaleId=%s,RetryAttempts=%s,LastRetryTime=%s,Succeeded=%s,Expired=%s where Id=%s",(EMS7MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired,EMS7ReversalId))
				self.__EMS7Connection.commit()

	def addReversalArchiveToEMS7(self, ReversalArchive, Action):
		#Id = ReversalArchive[0] commented the code on July 5
		#MerchantAccountId = ReversalArchive[0]
		EMS6MerchantAccountId = ReversalArchive[0]
		CardNumber = ReversalArchive[1]
		ExpiryDate = ReversalArchive[2]
		OriginalMessageType = ReversalArchive[3]
		OriginalProcessingCode = ReversalArchive[4]
		OriginalReferenceNumber	= ReversalArchive[5]
		OriginalTime = ReversalArchive[6]
		OriginalTransactionAmount = ReversalArchive[7]
		LastResponseCode = ReversalArchive[8]
		PurchasedDate = ReversalArchive[9]
		TicketNumber = ReversalArchive[10]
		#PaystationId = ReversalArchive[11]
		EMS6PaystationId = ReversalArchive[11]
		RetryAttempts = ReversalArchive[12]
		LastRetryTime = ReversalArchive[13]
		Succeeded = ReversalArchive[14]
		Expired = ReversalArchive[15]
		IsRFID = ReversalArchive[16]
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		print PointOfSaleId
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into ReversalArchive(MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select distinct(EMS7ReversalArchiveId) from ReversalArchiveMapping where MerchantAccountId=%s and OriginalReferenceNumber=%s and OriginalTime=%s",(EMS6MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			if(self.__EMS7Cursor.rowcount<>0):
				EMS7ReversalId = self.__EMS7Cursor.fetchone()	
				self.__EMS7Cursor.execute("update ReversalArchive set MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired where Id=%s",(EMS7MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired, EMS7ReversalId))
				self.__EMS7Connection.commit()
		
	def addSCRetryToEMS7(self, SCRetry, Action):
		EMS6PaystationId = SCRetry[0]
		PurchasedDate = SCRetry[1]
		TicketNumber = SCRetry[2]
		SCType = SCRetry[3]
		RetryServer = SCRetry[4]
		MerchantInfo = SCRetry[5]
		SettlementInfo = SCRetry[6]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__EMS7Cursor.execute("insert into SCRetry(PointOfSaleId,PurchasedDate,TicketNumber,SCType,RetryServer,MerchantInfo,SettlementInfo) values(%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,PurchasedDate,TicketNumber,SCType,RetryServer,MerchantInfo,SettlementInfo))
		self.__EMS7Connection.commit()

	def addThirdPartyServiceAccountToEMS7(self, ThirdPartyServiceAccount):
		EMS6ThirdPartyServiceAccountId = ThirdPartyServiceAccount[0]
		CustomerId = ThirdPartyServiceAccount[1]
		Type = ThirdPartyServiceAccount[2]
		EndPointUrl = ThirdPartyServiceAccount[3]
		UserName = ThirdPartyServiceAccount[4]
		Password = ThirdPartyServiceAccount[5]
		Field1 = ThirdPartyServiceAccount[6]
		Field2 = ThirdPartyServiceAccount[7]
		IsPaystationBased = ThirdPartyServiceAccount[8]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("insert into ThirdPartyServiceAccount(CustomerId,Type,EndPointUrl,UserName,Password,Field1,Field2,IsPaystationBased) values(%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,Type,EndPointUrl,UserName,Password,Field1,Field2,IsPaystationBased))
		EMS7ThirdPartyServiceAccountId=self.__EMS7Cursor.lastrowid
		self.__EMS7Cursor.execute("insert into ThirdPartyServiceAccountMapping(EMS6ThirdPartyServiceAccountId,EMS7ThirdPartyServiceAccountId,EMS6UserName) values(%s,%s,%s)",(EMS6ThirdPartyServiceAccountId,EMS7ThirdPartyServiceAccountId,UserName))
		self.__EMS7Connection.commit()
		
	def addTransaction2Push(self, Transaction2Push, Action):
		PaystationId = Transaction2Push[0]
		PurchasedDate = Transaction2Push[1]
		TicketNumber = Transaction2Push[2]
		EMS6ThirdPartyServiceAccountId = Transaction2Push[3]
		LotNumber = Transaction2Push[4]
		PlateNumber = Transaction2Push[5]
		ExpiryDate = Transaction2Push[6]
		ChargedAmount = Transaction2Push[7]
		StallNumber = Transaction2Push[8]
		EMS6RegionId = Transaction2Push[9]
		RetryCount = Transaction2Push[10]
		LastRetryTime = Transaction2Push[11]
		IsOffline = Transaction2Push[12]
		Field1 = Transaction2Push[13]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EMS7LocationId = self.__getLocationId(EMS6RegionId)
		EMS7ThirdPartyServiceAccountId = getEMS7ThirdPartyServiceAccountId(EMS6ThirdPartyServiceAccountId)
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into Transaction2Push(ThirdPartyServiceAccountId,LocationId,PointOfSaleId,PurchasedDate,TicketNumber,PaystationSettingName,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline,Field1) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7ThirdPartyServiceAccountId,EMS7LocationId,PointOfSaleId,PurchasedDate,TicketNumber,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline,Field1))
			EMS7Transaction2PushId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into Transaction2PushMapping(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId,EMS7Id) values(%s,%s,%s,%s,%s)",(PaystationId,PurchasedDate,TicketNumber,EMS6ThirdPartyServiceAccountId,EMS7Transaction2PushId))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select EMS7Id from Transaction2PushMapping where PaystationId=%s and PurchasedDate=%s")
			self.__EMS7Cursor.execute("update Transaction2Push set ThirdPartyServiceAccountId=%s,LocationId=%s,PointOfSaleId=%s,PurchasedDate=%s,TicketNumber=%s,PaystationSettingName=%s,PlateNumber=%s,ExpiryDate=%s,ChargedAmount=%s,StallNumber=%s,RetryCount=%s,LastRetryTime=%s,IsOffline=%s,Field1=%s where Id=%s",(EMS7ThirdPartyServiceAccountId,EMS7LocationId,PointOfSaleId,PurchasedDate,TicketNumber,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline,Field1,EMS7Transaction2PushId))

	def addTransactionPushToEMS7(self, TransactionPush, Action):
		EMS6PaystationId = TransactionPush[0] 
		PurchasedDate = TransactionPush[1]
		TicketNumber = TransactionPush[2]
		CustomerId = TransactionPush[3]
		LotNumber = TransactionPush[4]	
		PlateNumber = TransactionPush[5]
		ExpiryDate = TransactionPush[6]
		ChargedAmount = TransactionPush[7]
		StallNumber = TransactionPush[8]
		RetryCount = TransactionPush[9]
		LastRetryTime = TransactionPush[10]
		IsOffline = TransactionPush[11]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into TransactionPush(CustomerId,PointOfSaleId,PurchasedDate,TicketNumber,PaystationSettingName,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,PointOfSaleId,PurchasedDate,TicketNumber,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline))
			self.__EMS7Connection.commit()
		if(Action=='Update'):
			self.__EMS7Cursor.execute("select distinct(EMS7Id) from TransactionPushMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(EMS6PaystationId,PurchasedDate,TicketNumber))
			if(self.__EMS7Cursor.rowcount<>0):
				EMS7TransactionPushId = self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("update TransactionPush set CustomerId=%s,PointOfSaleId=%s,PurchasedDate=%s,TicketNumber=%s,PaystationSettingName=%s,PlateNumber=%s,ExpiryDate=%s,ChargedAmount=%s,StallNumber=%s,RetryCount=%s,LastRetryTime=%s,IsOffline=%s where Id=%s",(EMS7CustomerId,PointOfSaleId,PurchasedDate,TicketNumber,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RetryCount,LastRetryTime,IsOffline,EMS7TransactionPushId))
			self.__EMS7Connnection.commit()

	def getEMS7ThirdPartyServiceAccountId(self, ThirdPartyServiceAccountId):
		EMS7ThirdPartyServiceAccountId =None
		self.__EMS7Cursor.execute("select EMS7ThirdPartyServiceAccountId from ThirdPartyServiceAccountMapping where EMS6ThirdPartyServiceAccountId=%s",(ThirdPartyServiceAccountId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchall()
			EMS7ThirdPartyServiceAccountId=row[0]
		return EMS7ThirdPartyServiceAccountId

	def addThirdPartyPOSSequenceToEMS7(self, ThirdPartyPOSSequence):
		EMS6PaystationId = ThirdPartyPOSSequence[0]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		SequenceNumber = ThirdPartyPOSSequence[1]
		self.__EMS7Cursor.execute("insert into ThirdPartyPaystationSequence (PaystationId,SequenceNumber) values(%s,%s)",(PointOfSaleId,SequenceNumber))
		self.__EMS7Connection.commit()

	def addThirdPartyPushLogToEMS7(self, ThirdPartyPushLog):
		ActionTypeId = ThirdPartyPushLog[0]
		DateField = ThirdPartyPushLog[1]
		PushData = ThirdPartyPushLog[2]
		ResponseMessage = ThirdPartyPushLog[3]
		self.__EMS7Cursor.execute("insert into ThirdPartyPushLog(ActionTypeId,DateField,PushData,ResponseMessage) values(%s,%s,%s,%s)",(ActionTypeId,DateField,PushData,ResponseMessage))
		self.__EMS7Connection.commit()

	def addThirdPartyServicePaystationToEMS7(self, ThirdPartyServicePaystation):
		EMS6PaystationId = ThirdPartyServicePaystation[0]
		EMS6ThirdPartyServiceAccountId= ThirdPartyServicePaystation[1]
		EMS7ThirdPartyServiceAccountId = getEMS7ThirdPartyServiceAccountId(EMS6ThirdPartyServiceAccountId)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__EMS7Cursor.execute("insert into ThirdPartyServicePOS(ThirdPartyServiceAccountId,PointOfSaleId) values(%s,%s)",(EMS7ThirdPartyServiceAccountId,PointOfSaleId))
		self.__EMS7Connection.commit()
	
	def addThirdPartyServiceStallSequenceToEMS7(self, ThirdPartyServiceStallSequence):
		EMS6RegionId = ThirdPartyServiceStallSequence[0]
		StallNumber = ThirdPartyServiceStallSequence[1]
		SequenceNumber = ThirdPartyServiceStallSequence[2]
		EMS7LocationId = self.__getLocationId(EMS6RegionId)
		self.__EMS7Cursor.execute("insert into ThirdPartyServiceStallSequence(LocationId,SpaceNumber,SequenceNumber) values(%s,%s,%s)",(EMS7LocationId,StallNumber,SequenceNumber))
		self.__EMS7Connection.commit()
	
	def addPOSCollectionToEMS7(self, POSCollection, Action):
		print" ....In adding POS collection to EMS method......"
		self.__printPOSCollectionToEMS7(POSCollection)
		StartTicketNumber = 0
		EndTicketNumber = 0
		CoinTotalAmount = 0
		BillTotalAmount = 0
		CardTotalAmount = 0
		EMS6PaystationId = POSCollection[0]
		CollectionTypeId = POSCollection[1]
		StartDate = POSCollection[2]
		EndDate = POSCollection[3]
		StartTransactionNumber = POSCollection[4]
		EndTransactionNumber = POSCollection[5]
		version = POSCollection[6]
		PaystationCommAddress = POSCollection[7]
		LotNumber = POSCollection[8]
		MachineNumber = POSCollection[9]
		PatrollerTicketsSold = POSCollection[10]
		TicketsSold = POSCollection[11]
		CoinTotalAmount = POSCollection[12]
		"""
		OnesAmount = POSCollection[13]*100
		TwoesAmount = POSCollection[14]*100
		FivesAmount = POSCollection[15]*100
		TensAmount = POSCollection[16]*100
		TwentiesAmount = POSCollection[17]*100
		FiftiesAmount = POSCollection[18]*100
		AmexAmount = POSCollection[19]*100
		DiscoverAmount = POSCollection[20]*100
		MasterCardAmount = POSCollection[21]*100
		OtherCardAmount = POSCollection[22]*100
		SmartCardAmount = POSCollection[23]*100
		VisaAmount = POSCollection[24]*100
		"""
		OnesAmount = POSCollection[13]
		TwoesAmount = POSCollection[14]
		FivesAmount = POSCollection[15]
		TensAmount = POSCollection[16]
		TwentiesAmount = POSCollection[17]
		FiftiesAmount = POSCollection[18]
		AmexAmount = POSCollection[19]
		DiscoverAmount = POSCollection[20]
		MasterCardAmount = POSCollection[21]
		OtherCardAmount = POSCollection[22]
		SmartCardAmount = POSCollection[23]
		VisaAmount = POSCollection[24]
		
		CitationsPaid = POSCollection[25]
		CitationAmount = POSCollection[26]*100
		ChangeIssued = POSCollection[27]*100
		RefundIssued = POSCollection[28]*100
		SmartCardRechargeAmount = POSCollection[29]*100
		ExcessPayment = POSCollection[30]*100
		EMS6CustomerId = POSCollection[31]
		ReportNumber = POSCollection[32]
		NextTicketNumber = POSCollection[33]
		AttendantTicketsSold = POSCollection[34]
		AttendantTicketsAmount = POSCollection[35]*100
		DinersAmount = POSCollection[36]*100
		AttendantDepositAmount = POSCollection[37]*100
		AcceptedFloatAmount = POSCollection[38]
		Tube1Type = POSCollection[39]
		Tube1Amount = POSCollection[40]
		Tube2Type = POSCollection[41]
		Tube2Amount = POSCollection[42]
		Tube3Type = POSCollection[43]
		Tube3Amount = POSCollection[44]
		Tube4Type = POSCollection[45]
		Tube4Amount = POSCollection[46]
		OverfillAmount = POSCollection[47]
		ReplenishedAmount = POSCollection[48]
		Hopper1Dispensed = POSCollection[49]
		Hopper2Dispensed = POSCollection[50]
		Hopper1Type = POSCollection[51]
		Hopper2Type = POSCollection[52]
		Hopper1Current = POSCollection[53]
		Hopper2Current = POSCollection[54]
		Hopper1Replenished = POSCollection[55]
		Hopper2Replenished = POSCollection[56]
		ChangerTestDispensed = POSCollection[57]
		Hopper1TestDispensed = POSCollection[58]
		Hopper2TestDispensed = POSCollection[59]
		CoinCount005 = POSCollection[60]
		CoinCount010 = POSCollection[61]
		CoinCount025 = POSCollection[62]
		CoinCount100 = POSCollection[63]
		CoinCount200 = POSCollection[64]
		BillCount01 = POSCollection[65]
		BillCount02 = POSCollection[66]
		BillCount05 = POSCollection[67]
		BillCount10 = POSCollection[68]
		BillCount20 = POSCollection[69]
		BillCount50 = POSCollection[70]

		CoinTotalAmount = (CoinCount005*5 + CoinCount010*10 + CoinCount025*25 +  CoinCount100*100 + CoinCount200*200)/100	
		BillTotalAmount = OnesAmount + TwoesAmount + FivesAmount + TensAmount + TwentiesAmount + FiftiesAmount
		CardTotalAmount = AmexAmount + DinersAmount + DiscoverAmount + MasterCardAmount + VisaAmount + SmartCardAmount + OtherCardAmount + SmartCardRechargeAmount
		
		print " -- CointTotalAmount %s" %CoinTotalAmount
		print " -- BillToTalAmount %s" %BillTotalAmount
		print " -- CardTotalAmount %s" %CardTotalAmount
		print " -- OtherCardAmount %s" %OtherCardAmount
		
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(EMS7CustomerId):
			print " step 1"
			PaystationSettingId=self.__getPaystationSettingId(LotNumber,EMS7CustomerId)
			print " PaystationSetting Id is %s" %PaystationSettingId
		print "EMS6PaystationId %s" %EMS6PaystationId
		EMS7PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		print "EMS7 PointOfSaleId %s" %EMS7PointOfSaleId
		if(Action=='Insert'):
			if(EMS7CustomerId and PaystationSettingId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("insert into POSCollection(CustomerId,PointOfSaleId,CollectionTypeId,PaystationSettingId,StartGMT,EndGMT,ReportNumber,StartTicketNumber,EndTicketNumber,NextTicketNumber,StartTransactionNumber,EndTransactionNumber,TicketsSold,CoinTotalAmount,BillTotalAmount,CardTotalAmount,CoinCount05,CoinCount10,CoinCount25,CoinCount100,CoinCount200,BillCount1,BillCount2,BillCount5,BillCount10,BillCount20,BillCount50,BillAmount1,BillAmount2,BillAmount5,BillAmount10,BillAmount20,BillAmount50,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,ValueCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssuedAmount,ExcessPaymentAmount,OverfillAmount,PatrollerTicketsSold,RefundIssuedAmount,ReplenishedAmount,TestDispensedChanger,TestDispensedHopper1,TestDispensedHopper2,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (EMS7CustomerId,EMS7PointOfSaleId,CollectionTypeId,PaystationSettingId,StartDate,EndDate,ReportNumber,StartTicketNumber,EndTicketNumber,NextTicketNumber,StartTransactionNumber,EndTransactionNumber,TicketsSold,CoinTotalAmount,BillTotalAmount,CardTotalAmount,CoinCount005,CoinCount010,CoinCount025,CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50,OnesAmount,TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,OtherCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssued,ExcessPayment,OverfillAmount,PatrollerTicketsSold,RefundIssued,ReplenishedAmount,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount))
				EMS7POSCollectionId = self.__EMS7Cursor.lastrowid
				print " The last inserted row id is %s" %EMS7POSCollectionId
				self.__EMS7Cursor.execute("insert into AuditPOSCollectionMapping(PaystationId,CollectionTypeId,StartDate,EndDate,EMS7Id) values(%s,%s,%s,%s,%s)",(EMS6PaystationId,CollectionTypeId,StartDate,EndDate,EMS7POSCollectionId))
				self.__EMS7Connection.commit()
			else:
				print "The value for a CustomerId is %s" %EMS7CustomerId
		if(Action=='Update'):
			print " inside the update section"
			if(EMS7CustomerId and PaystationSettingId and EMS7PointOfSaleId):
				self.__EMS7Cursor.execute("update POSCollection set CustomerId=%s,PointOfSaleId=%s,CollectionTypeId=%s,PaystationSettingId=%s,StartGMT=%s,EndGMT=%s,ReportNumber=%s,StartTicketNumber=%s,EndTicketNumber=%s,NextTicketNumber=%s,StartTransactionNumber=%s,EndTransactionNumber=%s,TicketsSold=%s,CoinTotalAmount=%s,BillTotalAmount=%s,CardTotalAmount=%s,CoinCount05=%s,CoinCount10=%s,CoinCount25=%s,CoinCount100=%s,CoinCount200=%s,BillCount1=%s,BillCount2=%s,BillCount5=%s,BillCount10=%s,BillCount20=%s,BillCount50=%s,BillAmount1=%s,BillAmount2=%s,BillAmount5=%s,BillAmount10=%s,BillAmount20=%s,BillAmount50=%s,AmexAmount=%s,DinersAmount=%s,DiscoverAmount=%s,MasterCardAmount=%s,VisaAmount=%s,SmartCardAmount=%s,ValueCardAmount=%s,SmartCardRechargeAmount=%s,AcceptedFloatAmount=%s,AttendantDepositAmount=%s,AttendantTicketsAmount=%s,AttendantTicketsSold=%s,ChangeIssuedAmount=%s,ExcessPaymentAmount=%s,OverfillAmount=%s,PatrollerTicketsSold=%s,RefundIssuedAmount=%s,ReplenishedAmount=%s,TestDispensedChanger=%s,TestDispensedHopper1=%s,TestDispensedHopper2=%s,Hopper1Type=%s,Hopper2Type=%s,Hopper1Current=%s,Hopper2Current=%s,Hopper1Dispensed=%s,Hopper2Dispensed=%s,Hopper1Replenished=%s,Hopper2Replenished=%s,Tube1Type=%s,Tube2Type=%s,Tube3Type=%s,Tube4Type=%s,Tube1Amount=%s,Tube2Amount=%s,Tube3Amount=%s,Tube4Amount=%s where PointOfSaleId=%s and CollectionTypeId=%s and StartGMT=%s and EndGMT=%s", (EMS7CustomerId,EMS7PointOfSaleId,CollectionTypeId,PaystationSettingId,StartDate,EndDate,ReportNumber,StartTicketNumber,EndTicketNumber,NextTicketNumber,StartTransactionNumber,EndTransactionNumber,TicketsSold,CoinTotalAmount,BillTotalAmount,CardTotalAmount,CoinCount005,CoinCount010,CoinCount025,CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50,OnesAmount,TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DinersAmount,DiscoverAmount,MasterCardAmount,VisaAmount,SmartCardAmount,OtherCardAmount,SmartCardRechargeAmount,AcceptedFloatAmount,AttendantDepositAmount,AttendantTicketsAmount,AttendantTicketsSold,ChangeIssued,ExcessPayment,OverfillAmount,PatrollerTicketsSold,RefundIssued,ReplenishedAmount,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Dispensed,Hopper2Dispensed,Hopper1Replenished,Hopper2Replenished,Tube1Type,Tube2Type,Tube3Type,Tube4Type,Tube1Amount,Tube2Amount,Tube3Amount,Tube4Amount, EMS7PointOfSaleId,CollectionTypeId,StartDate,EndDate))
				self.__EMS7Connection.commit()

	def __printPOSCollectionToEMS7(self,POSCollection):
		print " -- EMS6PaystationId %s" %POSCollection[0]
		print " -- CollectionTypeId %s" %POSCollection[1]
		print " -- StartDate %s" %POSCollection[2]
		print " -- EndDate %s" %POSCollection[3]
		print " -- StartTransactionNumber %s" %POSCollection[4]
		print " -- EndTransactionNumber %s" %POSCollection[5]
		print " -- version %s" %POSCollection[6]
		print " -- PaystationCommAddress %s" %POSCollection[7]
		print " -- LotNumber %s" %POSCollection[8]
		print " -- MachineNumber %s" %POSCollection[9]
		print " -- PatrollerTicketsSold %s" %POSCollection[10]
		print " -- TicketsSold %s" %POSCollection[11]
		print " -- OnesAmount %s" %POSCollection[12]
		print " -- TwoesAmount %s" %POSCollection[13]
		print " -- FivesAmount %s" %POSCollection[14]
		print " -- TensAmount %s" %POSCollection[15]
		print " -- TwentiesAmount %s" %POSCollection[16]
		print " -- FiftiesAmount %s" %POSCollection[17]
		print " -- AmexAmount %s" %POSCollection[18]
		print " -- DiscoverAmount %s" %POSCollection[19]
		print " -- MasterCardAmount %s" %POSCollection[20]
		print " -- OtherCardAmount %s" %POSCollection[21]
		print " -- SmartCardAmount %s" %POSCollection[22]
		print " -- VisaAmount %s" %POSCollection[23]
		print " -- CitationsPaid %s" %POSCollection[24]
		print " -- CitationAmount %s" %POSCollection[25]
		print " -- ChangeIssued %s" %POSCollection[26]
		print " -- RefundIssued %s" %POSCollection[27]
		print " -- SmartCardRechargeAmount %s" %POSCollection[28]
		print " -- ExcessPayment %s"  %POSCollection[29]
		print " -- EMS6CustomerId %s" %POSCollection[30]
		print " -- ReportNumber %s" %POSCollection[31]
		print " -- NextTicketNumber %s"  %POSCollection[32]
		print " -- AttendantTicketsSold %s" %POSCollection[33]
		print " -- AttendantTicketsAmount %s" %POSCollection[34]
		print " -- DinersAmount %s" %POSCollection[35]
		print " -- AttendantDepositAmount %s" %POSCollection[36]
		print " -- AcceptedFloatAmount %s" %POSCollection[37]
		print " -- Tube1Type %s" %POSCollection[38]
		print " -- Tube1Amount %s" %POSCollection[39]
		print " -- Tube2Type %s" %POSCollection[40]
		print " -- Tube2Amount %s" %POSCollection[41]
		print " -- Tube3Type %s" %POSCollection[42]
		print " -- Tube3Amount %s" %POSCollection[43]
		print " -- Tube4Type %s" %POSCollection[44]
		print " -- Tube4Amount %s" %POSCollection[45]
		print " -- OverfillAmount %s" %POSCollection[46]
		print " -- ReplenishedAmount %s" %POSCollection[47]
		print " -- Hopper1Dispensed %s" %POSCollection[48]
		print " -- Hopper2Dispensed %s" %POSCollection[49]
		print " -- Hopper1Type %s" %POSCollection[50]
		print " -- Hopper2Type %s" %POSCollection[51]
		print " -- Hopper1Current %s" %POSCollection[52]
		print " -- Hopper2Current %s" %POSCollection[53]
		print " -- Hopper1Replenished %s" %POSCollection[54]
		print " -- Hopper2Replenished %s " %POSCollection[55]
		print " -- ChangerTestDispensed %s" %POSCollection[56]
		print " -- Hopper1TestDispensed %s" %POSCollection[57]
		print " -- Hopper2TestDispensed %s" %POSCollection[58]
		print " -- CoinCount005 %s" %POSCollection[59]
		print " -- CoinCount010 %s" %POSCollection[60]
		print " -- CoinCount025 %s" %POSCollection[61]
		print " -- CoinCount100 %s" %POSCollection[62]
		print " -- CoinCount200 %s" %POSCollection[63]
		print " -- BillCount01 %s" %POSCollection[64]
		print " -- BillCount02 %s" %POSCollection[65]
		print " -- BillCount05 %s" %POSCollection[66]
		print " -- BillCount10 %s" %POSCollection[67]
		print " -- BillCount20 %s" %POSCollection[68]
		print " -- BillCount50 %s" %POSCollection[69]

	def addPOSStatusToEMS7(self, POSStatus):
		IsProvisioned = 0
		IsLocked = 0     
		IsDecommissioned = 0 
		IsDeleted = 0        
		IsVisible = 1       
		IsBillableMonthlyOnActivation = 0
		IsDigitalConnect = 0             
		IsTestActivated = 0             
		IsActivated = 1                 
		IsBillableMonthlyForEms = 0      
		IsProvisionedGMT = date.today()            
		IsLockedGMT = date.today()                 
		IsDecommissionedGMT = date.today()          
		IsDeletedGMT = date.today()                 
		IsVisibleGMT = date.today()                 
		IsBillableMonthlyOnActivationGMT = date.today()
		IsDigitalConnectGMT = date.today()             
		IsTestActivatedGMT = date.today()             
		IsActivatedGMT = date.today()                 
		IsBillableMonthlyForEmsGMT = date.today()     
		VERSION= 0                         
		LastModifiedGMT = date.today()                 
		LastModifiedByUserId = 1            
		print "PointOfSale %s" %POSStatus[0]
		PointOfSaleId = POSStatus[0]
		POSDateTypeId = POSStatus[1]
		ChangedGMT = POSStatus[2]
		CurrentSetting = POSStatus[3]
		VERSION = POSStatus[4]
		if(POSDateTypeId ==1):
			IsProvisioned = CurrentSetting
			IsProvisionedGMT = ChangedGMT
		if(POSDateTypeId==2):
			IsLocked = CurrentSetting
			IsLockedGMT = ChangedGMT
		if(POSDateTypeId==3):
			IsDecommissioned = CurrentSetting
			IsDecommissionedGMT = ChangedGMT
		if(POSDateTypeId==4):
			IsDeleted = CurrentSetting
			IsDeletedGMT = ChangedGMT
		if(POSDateTypeId==5):
			IsVisible = 1
			IsVisibleGMT = ChangedGMT
		if(POSDateTypeId==6):
			IsBillableMonthlyOnActivation=CurrentSetting
			IsBillableMonthlyOnActivationGMT = ChangedGMT
		if(POSDateTypeId==7):
			IsDigitalConnect = CurrentSetting
			IsDigitalConnectGMT = ChangedGMT
		if(POSDateTypeId==8):
			IsTestActivated = CurrentSetting
			IsTestActivatedGMT = ChangedGMT
		if(POSDateTypeId==9):
			IsActivated = CurrentSetting
			IsActivatedGMT = ChangedGMT
		if(POSDateTypeId==10):
			IsBillableMonthlyForEms = CurrentSetting
			IsBillableMonthlyForEmsGMT = ChangedGMT
		self.__EMS7Cursor.execute("insert ignore into POSStatus(PointOfSaleId,IsProvisioned,IsLocked,IsDecommissioned,IsDeleted,IsVisible,IsBillableMonthlyOnActivation,IsDigitalConnect,IsTestActivated,IsActivated,IsBillableMonthlyForEms,IsProvisionedGMT,IsLockedGMT,IsDecommissionedGMT,IsDeletedGMT,IsVisibleGMT,IsBillableMonthlyOnActivationGMT,IsDigitalConnectGMT,IsTestActivatedGMT,IsActivatedGMT,IsBillableMonthlyForEmsGMT,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,IsProvisioned,IsLocked,IsDecommissioned,IsDeleted,IsVisible,IsBillableMonthlyOnActivation,IsDigitalConnect,IsTestActivated,IsActivated,IsBillableMonthlyForEms,IsProvisionedGMT,IsLockedGMT,IsDecommissionedGMT,IsDeletedGMT,IsVisibleGMT,IsBillableMonthlyOnActivationGMT,IsDigitalConnectGMT,IsTestActivatedGMT,IsActivatedGMT,IsBillableMonthlyForEmsGMT,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def addPurchaseTax(self, Taxes, Action):
		print" ......in Add Purchase Tax method...."
		Id = Taxes[0]
		LastModifiedGMT = date.today()
		EMS6PaystationId = Taxes[1]
		TicketNumber = Taxes[2]
		PurchasedDate = Taxes[3]
		ProcessingDate = Taxes[4]
		EMS6CustomerId = Taxes[5]
		RegionId = Taxes[6]
		TaxName = Taxes[7]
		TaxRate = Taxes[8]
		TaxValue = Taxes[9]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		print EMS7CustomerId
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		print PointOfSaleId
		PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
		print PurchasedDate
		print TicketNumber
		if(Action=='Insert'):
			if(EMS7CustomerId):
				EMS7TaxId=None
				print " EMS7CustomerID is" , EMS7CustomerId
				print " inserting data into tax table"
				self.__EMS7Cursor.execute("insert ignore into Tax(CustomerId,Name,LastModifiedGMT) values(%s,%s,%s)",(EMS7CustomerId,TaxName,LastModifiedGMT))
				self.__EMS7Cursor.execute("select Id from Tax where CustomerId=%s and Name=%s and LastModifiedGMT=%s",(EMS7CustomerId,TaxName,LastModifiedGMT))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7TaxId=row[0]
				#	PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
					PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
					print PurchaseId
					if(PurchaseId):
						self.__EMS7Cursor.execute("insert ignore into PurchaseTax(PurchaseId,TaxId,TaxRate,TaxAmount) values(%s,%s,%s,%s)",(PurchaseId,EMS7TaxId,TaxRate,TaxValue))
						print "Record Inserted in PurchaseTax"
						EMS7PurchaseTaxId = self.__EMS7Cursor.lastrowid
						self.__EMS7Cursor.execute("insert ignore into PurchaseTaxMapping(EMS6Id,EMS7Id) values(%s,%s)",(Id,EMS7PurchaseTaxId))
						self.__EMS7Connection.commit()
		if(Action=='Update'):
			print " ......The action type is update....."
			self.__EMS7Cursor.execute("select Id from Tax where CustomerId=%s and Name=%s",(EMS7CustomerId,TaxName))
			EMS7TaxId = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("select EMS7Id from PurchaseTaxMapping where EMS6Id=%s",(Id))
			if(self.__EMS7Cursor.rowcount<>0):
				EMS7PurchaseTaxId = self.__EMS7Cursor.fetchone()
				print "Purchase Id %s" %PurchaseId
				print "EMS7TaxId %s" %EMS7TaxId
				print "TaxRate %s" %TaxRate
				print "TaxValue %s" %TaxValue
				print "EMS7PurchaseTaxId %s" %EMS7PurchaseTaxId
				print "step 3"
				params={'PurchaseId' : PurchaseId,'TaxId': EMS7TaxId,'TaxRate': TaxRate,'TaxAmount': TaxValue ,'Id': EMS7PurchaseTaxId[0]}
				self.__EMS7Cursor.execute("update PurchaseTax set PurchaseId= '%(PurchaseId)s',TaxRate ='%(TaxRate)s' ,TaxAmount= '%(TaxAmount)s' where Id='%(Id)s'" % params)
				print " ...record updated..."
			self.__EMS7Connection.commit()
			
	def addPOSHeartBeat(self, psHeartBeat, Action):
		print " in adding POSHeartbeat to EMS 7"
		EMS6PaystationId = psHeartBeat[0]
		EMS6LastHearBeatGMT= psHeartBeat[1]
		PointOfSaleId= self.__getPointOfSaleId(EMS6PaystationId)
		print " PointOfSaleId is", PointOfSaleId
		if(Action =='Insert'):
			self.__EMS7Cursor.execute("insert ignore into POSHeartbeat(PointOfSaleId, LastHeartbeatGMT) values(%s, %s)", (PointOfSaleId,EMS6LastHearBeatGMT))
		elif(Action=='Update'):
			self.__EMS7Cursor.execute("update POSHeartbeat set LastHeartbeatGMT = %s where PointOfSaleId=%s", (EMS6LastHearBeatGMT,PointOfSaleId))
		elif ( Action=='Delete'):
			self.__EMS7Cursor.execute("delete from POSHeartbeat where PointOfSaleId=%s",PointOfSaleId)
		self.__EMS7Connection.commit()
		
	def addReversal(self, reversal, Action):
		print" in Adding reversal to EMS 7"
		MerchantAccountId = reversal[0]
		CardNumber= reversal[1]
		ExpiryDate= reversal[2]
		OriginalMessageType= reversal[3]
		OriginalProcessingCode= reversal[4]
		OriginalReferenceNumber=reversal[5]
		OriginalTime=reversal[6]
		OriginalTransactionAmount= reversal[7]
		LastResponseCode= reversal[8]
		PurchasedDate=reversal[9]
		TicketNumber= reversal[10]
		PaystationId= reversal[11]
		RetryAttempts= reversal[12]
		LastRetryTime=reversal[13]
		Succeeded= reversal[14]
		Expired=reversal[15]
		IsRFID=reversal[16] 
		PointOfSaleId= self.__getPointOfSaleId(PaystationId)
		print " Point of sale id is", PointOfSaleId
		if(Action=='Insert'):
			print "In insert method"
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("INSERT INTO Reversal(MerchantAccountId,CardData,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,\
										LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,IsSucceeded,IsExpired,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\
										%s,%s,%s)", (MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,\
													LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired,IsRFID))
			EMS7ReversalId = self.__EMS7Cursor.lastrowid
			print " creating mapping record with id", EMS7ReversalId
			self.__EMS7Cursor.execute("INSERT into ReversalMapping(MerchantAccountId, OriginalReferenceNumber, OriginalTime, EMS7Id) values( %s, %s,%s,%s)", (MerchantAccountId,OriginalReferenceNumber,OriginalTime,EMS7ReversalId))
			self.__EMS7Cursor.execute("set foreign_key_checks=1")
		elif(Action=='Update'):
			self.__EMS7Cursor.execute("select EMS7Id from ReversalMapping where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime=%s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			EMS7ReversalId = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("update Reversal set MerchantAccountId = %s,OriginalReferenceNumber=%s, OriginalTime=%s, CardData=%s,ExpiryDate=%s,OriginalMessageType=%s,OriginalProcessingCode=%s, OriginalTransactionAmount=%s,\
										LastResponseCode=%s,PurchasedDate=%s,TicketNumber=%s,PointOfSaleId=%s,RetryAttempts=%s,LastRetryTime=%s,IsSucceeded=%s,IsExpired=%s,IsRFID=%s\
										where Id = %s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode, OriginalTransactionAmount, LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired,IsRFID,EMS7ReversalId))
		elif(Action=='Delete'):
			self.__EMS7Cursor.execute("select distinct(EMS7Id) from ReversalMapping where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime=%s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			EMS7ReversalId = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("delete from Reversal where Id = %s", EMS7ReversalId)
		self.__EMS7Connection.commit()
		
	def addReversalArchive(self, reversal, Action):
		print" in Adding ReversalArchive to EMS 7"
		MerchantAccountId = reversal[0]
		CardNumber= reversal[1]
		ExpiryDate= reversal[2]
		OriginalMessageType= reversal[3]
		OriginalProcessingCode= reversal[4]
		OriginalReferenceNumber=reversal[5]
		OriginalTime=reversal[6]
		OriginalTransactionAmount= reversal[7]
		LastResponseCode= reversal[8]
		PurchasedDate=reversal[9]
		TicketNumber= reversal[10]
		PaystationId= reversal[11]
		RetryAttempts= reversal[12]
		LastRetryTime=reversal[13]
		Succeeded= reversal[14]
		Expired=reversal[15]
		PointOfSaleId= self.__getPointOfSaleId(PaystationId)
		print " Point of sale id is", PointOfSaleId
		if(Action=='Insert'):
			print "In insert method"
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("INSERT INTO ReversalArchive(MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,\
										LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\
										%s,%s)", (MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,\
													LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired))
			EMS7ReversalArchiveId = self.__EMS7Cursor.lastrowid
			print " creating mapping record in ReversalArchiveMapping with id", EMS7ReversalArchiveId
			self.__EMS7Cursor.execute("INSERT into ReversalArchiveMapping(MerchantAccountId, OriginalReferenceNumber, OriginalTime, EMS7ReversalArchiveId) values( %s, %s,%s,%s)", (MerchantAccountId,OriginalReferenceNumber,OriginalTime,EMS7ReversalArchiveId))
			self.__EMS7Cursor.execute("set foreign_key_checks=1")
		elif(Action=='Update'):
			print "In Update method"
			print "MerchantAccountId is:",MerchantAccountId
			print "OriginalReferenceNumber is:",OriginalReferenceNumber
			print "OriginalTime is:",OriginalTime
			self.__EMS7Cursor.execute("select EMS7ReversalArchiveId from ReversalArchiveMapping where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime=%s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			EMS7ReversalArchiveId = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("update ReversalArchive set MerchantAccountId = %s,OriginalReferenceNumber=%s, OriginalTime=%s, CardNumber=%s,ExpiryDate=%s,OriginalMessageType=%s,OriginalProcessingCode=%s, OriginalTransactionAmount=%s,\
										LastResponseCode=%s,PurchasedDate=%s,TicketNumber=%s,PointOfSaleId=%s,RetryAttempts=%s,LastRetryTime=%s,Succeeded=%s,Expired=%s \
										where Id = %s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode, OriginalTransactionAmount, LastResponseCode,PurchasedDate,TicketNumber,PointOfSaleId,RetryAttempts,LastRetryTime,Succeeded,Expired,EMS7ReversalArchiveId[0]))
		elif(Action=='Delete'):
			self.__EMS7Cursor.execute("select distinct(EMS7ReversalArchiveId) from ReversalArchiveMapping where MerchantAccountId = %s and OriginalReferenceNumber = %s and OriginalTime=%s", (MerchantAccountId,OriginalReferenceNumber,OriginalTime))
			EMS7ReversalArchiveId = self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("delete from ReversalArchive where EMS7ReversalArchiveId = %s", EMS7ReversalArchiveId)
		self.__EMS7Connection.commit()
		
		
		