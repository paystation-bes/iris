

SELECT '******* Creating database *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_000_database_ashok.sql

SELECT '******* Creating Tables *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_100_tables.sql

/* SELECT '******* Creating Timezones in MYSQL Schema *******' AS INFO; */

/* SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_110_tables_mysql_timezones.sql */

SELECT '******* Creating Procedures *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_150_procedures.sql

SELECT '******* Creating ReCal Procedures *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_155_recalc_procedures.sql

SELECT '******* Creating Views *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_175_views.sql

SELECT '******* Inserting Data into Looks ups *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_200_lookups.sql

SELECT '******* Inserting Data into EMS Properties *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_201_lookups_development.sql

SELECT '******* Creating Stored Procedure Time and Generate ETL DATE RANGE *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_250_lookups_procedures.sql

SELECT '******* Creating EMS 7 Triggers on Customer Agreement, Notification, User Account and ETL *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_300_triggers_onlyforBlitz.sql

SELECT '******* Creating EMS 7 ETL StoredProcedures *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_500_etl_procedures.sql

SELECT '******* Inserting Record in ServiceAgreement Table *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ems_db_7_902_test_service_agreement.sql

SELECT '******* Creating Mapping Tables *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\MigrationScripts\\ITFScripts\\DBScripts\\MappingTables.sql

SELECT '******* Updating Mapping Name for Processor Carrier and APN Tables for QA *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\QAMappingNamesforProcessorCarrierAPN.sql

SELECT '******* Creating Migration Lookup TXT Tables from 2012 on wards*******' AS INFO;

call sp_GenerateMigrationDateRange(1,'Purchase','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'ProcessorTransaction','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'Permit','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'PaymentCard','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'POSCollection','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'PurchaseTax','2012-01-01 00:00:00','2013-08-16 23:59:59',30);
call sp_GenerateMigrationDateRange(1,'PurchaseCollection','2012-01-01 00:00:00','2013-08-16 23:59:59',30);

SELECT '******* Creating ETL Procedures on Initial Migration Data *******' AS INFO;

SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\sp_MigrateRevenueIntoKPI.sql
SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\sp_FillOccupancyTimeslots.sql
SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\MigrateDailyUtilToMonthly.sql
SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ETLSettledTransactions.sql
SOURCE C:\\EMSWorkspace\\ems-database\\scripts\\ETL_Collection_widget.sql

SELECT '******* Filling Time Table from 2011 to 2013 *******' AS INFO;

CALL sp_LoadTime('2011');

CALL sp_LoadTime('2012');

CALL sp_LoadTime('2013');



SELECT '**** END ****' 

