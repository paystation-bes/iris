-- Tables and Triggers to be deployed in EMS6 Database --

-- Note : add an index to Purchase table on (POS, PurchaseGMT, PurchaseNumber
-- Migration table to hold Incremental Migration Data --

-- This is new table proposed by Sanjay being tested----

DROP TRIGGER tr_InsertAudit ;

DROP TRIGGER StoreTransactionPush ;

DROP TRIGGER tr_InsertTransactionCollection ;


DROP TABLE IF EXISTS  `MigrationQueue` ;

CREATE TABLE IF NOT EXISTS  `MigrationQueue` (
  `Id` 				BIGINT 		UNSIGNED NOT NULL AUTO_INCREMENT,
  `MultiKeyId` 		VARCHAR(5000),	
  `EMS6Id` 			INT UNSIGNED NOT NULL DEFAULT 0,
  `TableName` 		VARCHAR(40) NOT NULL,
  `Action` 			VARCHAR(6) NOT NULL,
  `SelectedForMigration` TINYINT  DEFAULT 0,
  `SelectedByProcessId` INT DEFAULT 0,
  `IsProcessed` 	TINYINT  DEFAULT 0,
  `MigratedByProcessId` INT,
  `CreatedDate` 	TIMESTAMP  DEFAULT CURRENT_TIMESTAMP,
  `MigratedDate` 	DATETIME ,
  `ErrorMsg` VARCHAR(5000),
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

Alter table MigrationQueue add index idx_MigrationQueue(TableName,SelectedForMigration,SelectedByProcessId,IsProcessed);

-- --This is new table proposed by Sanjay being tested----

DROP TABLE IF  EXISTS  `MigrationProcessLog` ;

CREATE TABLE IF NOT EXISTS  `MigrationProcessLog` (
  `Id` 				BIGINT 		UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProcessType` 		VARCHAR(45),
  `StartTime` Datetime NOT NULL ,
  `EndTime` Datetime ,
  `RecordSelectedCount` Int UNSIGNED,
  `RecordProcessedCount` Int UNSIGNED,
  `Error` VARCHAR(5000),
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

----Create trigger WS Call Log as this trigger does not exist now/Sanjay/ 29th July 2012
DELIMITER $$
CREATE TRIGGER UpdateWsCallLog 
AFTER UPDATE ON WsCallLog 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Token,',',New.CallDate); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'WsCallLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertWsCallLog
AFTER INSERT ON WsCallLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.Token,',',New.CallDate); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'WsCallLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteWsCallLog
AFTER DELETE ON WsCallLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Token,',',Old.CallDate);
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'WsCallLog','Delete');
END$$
-----Create trigger for PaystationHeartBeat table.


DELIMITER $$
CREATE TRIGGER UpdatePaystationHeartBeat
AFTER UPDATE ON PaystationHeartBeat
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'PaystationHeartBeat','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystationHeartBeat
AFTER DELETE ON PaystationHeartBeat
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'PaystationHeartBeat','Delete');
END$$
-----Trigger for Reversal Table

DELIMITER $$
CREATE TRIGGER UpdateReversal 
AFTER UPDATE ON Reversal 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertReversal
AFTER INSERT ON Reversal
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteReversal
AFTER DELETE ON Reversal
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(Old.MerchantAccountId,',',Old.OriginalReferenceNumber,',',Old.OriginalTime); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Delete');
END$$
-----Trigger for ReversalArchive Table

DELIMITER $$
CREATE TRIGGER UpdateReversalArchive 
AFTER UPDATE ON ReversalArchive 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 
-- Aug 08 ITF
if (New.OriginalTime <> '') then
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Update');
end if;
END$$

DELIMITER $$
CREATE TRIGGER InsertReversalArchive
AFTER INSERT ON ReversalArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 

if (New.OriginalTime <> '') then
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Insert');
end if;

END$$

DELIMITER $$
CREATE TRIGGER DeleteReversalArchive
AFTER DELETE ON ReversalArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(Old.MerchantAccountId,',',Old.OriginalReferenceNumber,',',Old.OriginalTime); 

if (Old.OriginalTime <> '') then
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Delete');
end if;

END$$

-- Customer Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCustomer 
AFTER UPDATE ON Customer
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Customer','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCustomer
AFTER INSERT ON Customer
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Customer','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomer
AFTER DELETE ON Customer
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Customer','Delete');
END$$

-- Card Type --
DELIMITER $$
CREATE TRIGGER UpdateCardType 
AFTER UPDATE ON CardType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CardType','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCardType
AFTER INSERT ON CardType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CardType','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCardType
AFTER DELETE ON CardType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CardType','Delete');
END$$

-- Coupon Trigger --

DELIMITER $$
CREATE TRIGGER UpdateCoupon 
AFTER UPDATE ON Coupon 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Id,',',New.CustomerId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCoupon
AFTER INSERT ON Coupon
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Id,',',New.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCoupon
AFTER DELETE ON Coupon
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Id,',',Old.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Delete');
END$$

-- Region Trigger --
DELIMITER $$
CREATE TRIGGER UpdateRegion 
AFTER UPDATE ON Region
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Region','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRegion
AFTER INSERT ON Region
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Region','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRegion
AFTER DELETE ON Region
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Region','Delete');
END$$

-- Paystation Trigger --
DELIMITER $$
CREATE TRIGGER DeletePaystation
AFTER DELETE ON Paystation
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Paystation','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdatePaystation
AFTER UPDATE ON Paystation
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Paystation','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertPaystation
AFTER INSERT ON Paystation
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Paystation','Insert');
END$$

--- LotSetting Trigger --
DELIMITER $$
CREATE TRIGGER InsertLotSetting
AFTER INSERT ON LotSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSetting','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateLotSetting
AFTER UPDATE ON LotSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSetting','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteLotSetting
AFTER DELETE ON LotSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'LotSetting','Delete');
END$$

-- CustomerProperties Trigger --
DELIMITER $$
CREATE TRIGGER InsertCustomerProperties
AFTER INSERT ON CustomerProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.CustomerId,'CustomerProperties','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateCustomerProperties
AFTER UPDATE ON CustomerProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.CustomerId,'CustomerProperties','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomerProperties
AFTER DELETE ON CustomerProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.CustomerId,'CustomerProperties','Delete');
END$$

-- Customer Service Relation Trigger --
/*
DELIMITER $$
CREATE TRIGGER UpdateCustomer_Service_Relation 
AFTER UPDATE ON cerberus_db.Customer_Service_Relation 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.ServiceId); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCustomer_Service_Relation
AFTER INSERT ON cerberus_db.Customer_Service_Relation
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.CustomerId,',',New.ServiceId); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomer_Service_Relation
AFTER DELETE ON cerberus_db.Customer_Service_Relation
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.ServiceId);
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Delete');
END$$

*/

-- Alert Trigger --
DELIMITER $$
CREATE TRIGGER InsertAlert
AFTER INSERT ON Alert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Alert','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateAlert
AFTER UPDATE ON Alert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Alert','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteAlert
AFTER DELETE ON Alert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Alert','Delete');
END$$

-- Audit Trigger --
DELIMITER $$
CREATE TRIGGER UpdateAudit 
AFTER UPDATE ON Audit 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.CollectionTypeId,',',New.EndDate,',',New.StartDate); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertAudit
AFTER INSERT ON Audit
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
    DECLARE v_LastCashCollectionGMT DATETIME;
    DECLARE v_LastBillCollectionGMT DATETIME;
    DECLARE v_LastCoinCollectionGMT DATETIME;
    DECLARE v_LastCardCollectionGMT DATETIME;
    DECLARE v_LastCollectionTypeId  TINYINT UNSIGNED;
    DECLARE v_IsRecalcable          TINYINT UNSIGNED DEFAULT 0;
    DECLARE v_CollectionTypeId      TINYINT UNSIGNED;
    DECLARE v_MaxLastCollectionGMT  DATETIME;
set Id=concat(New.PaystationId,',',New.CollectionTypeId,',',New.EndDate,',',New.StartDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Insert');

-- code copied from existing functionality

    
    SELECT  LastCashCollectionGMT, LastBillCollectionGMT, LastCoinCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId
    INTO    v_LastCashCollectionGMT, v_LastBillCollectionGMT, v_LastCoinCollectionGMT, v_LastCardCollectionGMT, v_LastCollectionTypeId
    FROM    PaystationBalance B
    WHERE   B.PaystationId = NEW.PaystationId;
    
    
    SET v_MaxLastCollectionGMT = v_LastCashCollectionGMT;
    IF v_LastBillCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastBillCollectionGMT; END IF;
    IF v_LastCoinCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCoinCollectionGMT; END IF;
    IF v_LastCardCollectionGMT > v_MaxLastCollectionGMT THEN SET v_MaxLastCollectionGMT = v_LastCardCollectionGMT; END IF;
    
    
    IF NEW.EndDate > v_MaxLastCollectionGMT THEN
        SET v_CollectionTypeId = NEW.CollectionTypeId;
    END IF;

    
    IF NEW.CollectionTypeId = 0 THEN
        
        
        IF NEW.EndDate > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndDate > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndDate > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndDate > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        
    
    ELSEIF NEW.CollectionTypeId = 1 THEN
        
        
        IF NEW.EndDate > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndDate > v_LastBillCollectionGMT THEN SET v_LastBillCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;

    
    ELSEIF NEW.CollectionTypeId = 2 THEN
        
        
        IF NEW.EndDate > v_LastCashCollectionGMT THEN SET v_LastCashCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
        IF NEW.EndDate > v_LastCoinCollectionGMT THEN SET v_LastCoinCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;

    
    ELSEIF NEW.CollectionTypeId = 3 THEN       
        
        
        IF NEW.EndDate > v_LastCardCollectionGMT THEN SET v_LastCardCollectionGMT = NEW.EndDate; SET v_IsRecalcable = 2; END IF;
     END IF;
    
    
    IF v_IsRecalcable = 2 THEN
        UPDATE  PaystationBalance
        SET     IsRecalcable          = v_IsRecalcable,
                LastCollectionTypeId  = v_CollectionTypeId,
                LastCashCollectionGMT = v_LastCashCollectionGMT,
                LastCoinCollectionGMT = v_LastCoinCollectionGMT,
                LastBillCollectionGMT = v_LastBillCollectionGMT,
                LastCardCollectionGMT = v_LastCardCollectionGMT
        WHERE   PaystationId          = NEW.PaystationId;
    END IF;
    



END$$

DELIMITER $$
CREATE TRIGGER DeleteAudit
AFTER DELETE ON Audit
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.CollectionTypeId,',',Old.EndDate,',',Old.StartDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Delete');
END$$

-- Authorities Trigger --
DELIMITER $$
CREATE TRIGGER InsertAuthorities
AFTER INSERT ON Authorities
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Authorities','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateAuthorities
AFTER UPDATE ON Authorities
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Authorities','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteAuthorities
AFTER DELETE ON Authorities
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Authorities','Delete');
END$$

-- BadCard Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBadCard 
AFTER UPDATE ON BadCard 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.AccountNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBadCard
AFTER INSERT ON BadCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.AccountNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBadCard
AFTER DELETE ON BadCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.AccountNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Delete');
END$$

-- BadSmartCard Trigger--

DELIMITER $$
CREATE TRIGGER UpdateBadSmartCard 
AFTER UPDATE ON BadSmartCard 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.CardNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadSmartCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBadSmartCard
AFTER INSERT ON BadSmartCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.CardNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadSmartCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBadSmartCard
AFTER DELETE ON BadSmartCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.CardNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadSmartCard','Delete');
END$$

-- BadCreditCard Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBadCreditCard 
AFTER UPDATE ON BadCreditCard 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.CardHash); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBadCreditCard
AFTER INSERT ON BadCreditCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.CardHash);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBadCreditCard
AFTER DELETE ON BadCreditCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.CardHash);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Delete');
END$$

-- BatteryInfo Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBatteryInfo 
AFTER UPDATE ON BatteryInfo 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBatteryInfo
AFTER INSERT ON BatteryInfo
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBatteryInfo
AFTER DELETE ON BatteryInfo
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Delete');
END$$

-- CCFailLog Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCCFailLog 
AFTER UPDATE ON CCFailLog 
FOR EACH ROW 
BEGIN 
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CCFailLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCCFailLog
AFTER INSERT ON CCFailLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CCFailLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCCFailLog
AFTER DELETE ON CCFailLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CCFailLog','Delete');
END$$

-- CardRetryTransaction Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCardRetryTransaction 
AFTER UPDATE ON CardRetryTransaction 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.LastRetryDate,',',New.NumRetries,',',New.CardHash,',',New.TypeId,',',New.CardData,',',New.CardExpiry,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',', New.CreationDate,',',New.BadCardHash,',',New.IgnoreBadCard,',',New.LastResponseCode,',',New.IsRFID); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCardRetryTransaction
AFTER INSERT ON CardRetryTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCardRetryTransaction
AFTER DELETE ON CardRetryTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Delete');
END$$

-- CryptoKey Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCryptoKey 
AFTER UPDATE ON CryptoKey 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Type,',',New.KeyIndex); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCryptoKey
AFTER INSERT ON CryptoKey
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Type,',',New.KeyIndex);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCryptoKey
AFTER DELETE ON CryptoKey
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Type,',',Old.KeyIndex);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Delete');
END$$

-- CustomerPermissions Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCustomerPermissions 
AFTER UPDATE ON CustomerPermissions 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.PayByPhoneEnabled); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCustomerPermissions
AFTER INSERT ON CustomerPermissions
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.PayByPhoneEnabled);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomerPermissions
AFTER DELETE ON CustomerPermissions
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.PayByPhoneEnabled);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Delete');
END$$

--- CustomerWsCal Trigger --
DELIMITER $$
CREATE TRIGGER InsertCustomerWsCal
AFTER INSERT ON CustomerWsCal
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsCal','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateCustomerWsCal
AFTER UPDATE ON CustomerWsCal
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsCal','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomerWsCal
AFTER DELETE ON CustomerWsCal
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CustomerWsCal','Delete');
END$$

--- CustomerWsToken Trigger --

DELIMITER $$
CREATE TRIGGER InsertCustomerWsToken
AFTER INSERT ON CustomerWsToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsToken','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateCustomerWsToken
AFTER UPDATE ON CustomerWsToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsToken','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomerWsToken
AFTER DELETE ON CustomerWsToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CustomerWsToken','Delete');
END$$

/*
--- EMSExtensiblePermit Trigger --

DELIMITER $$
CREATE TRIGGER InsertEMSExtensiblePermit
AFTER INSERT ON EMSExtensiblePermit
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.MobileNumber,'EMSExtensiblePermit','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEMSExtensiblePermit
AFTER UPDATE ON EMSExtensiblePermit
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.MobileNumber,'EMSExtensiblePermit','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSExtensiblePermit
AFTER DELETE ON EMSExtensiblePermit
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.MobileNumber,'EMSExtensiblePermit','Delete');
END$$
*/
--- EMSParkingPermission Trigger --

DELIMITER $$
CREATE TRIGGER InsertEMSParkingPermission
AFTER INSERT ON EMSParkingPermission
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSParkingPermission','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEMSParkingPermission
AFTER UPDATE ON EMSParkingPermission
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSParkingPermission','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSParkingPermission
AFTER DELETE ON EMSParkingPermission
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EMSParkingPermission','Delete');
END$$

-- Triggers for EMSParkingPermissionDayOfWeek --
DELIMITER $$
CREATE TRIGGER UpdateEMSParkingPermissionDayOfWeek 
AFTER UPDATE ON EMSParkingPermissionDayOfWeek 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.EMSPermissionId,',',New.DayOfWeek); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEMSParkingPermissionDayOfWeek
AFTER INSERT ON EMSParkingPermissionDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.EMSPermissionId,',',New.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSParkingPermissionDayOfWeek
AFTER DELETE ON EMSParkingPermissionDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.EMSPermissionId,',',Old.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Delete');
END$$

--- EMSRate Trigger --
DELIMITER $$
CREATE TRIGGER InsertEMSRate
AFTER INSERT ON EMSRate
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSRate','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEMSRate
AFTER UPDATE ON EMSRate
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSRate','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSRate
AFTER DELETE ON EMSRate
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EMSRate','Delete');
END$$

-- Triggers for EMSRateDayOfWeek --

DELIMITER $$
CREATE TRIGGER UpdateEMSRateDayOfWeek 
AFTER UPDATE ON EMSRateDayOfWeek 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.EMSRateId,',',New.DayOfWeek); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEMSRateDayOfWeek
AFTER INSERT ON EMSRateDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.EMSRateId,',',New.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSRateDayOfWeek
AFTER DELETE ON EMSRateDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.EMSRateId,',',Old.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Delete');
END$$

-- Trigger EMSProperties --
DELIMITER $$
CREATE TRIGGER InsertEmsProperties
AFTER INSERT ON EmsProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'EmsProperties','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEmsProperties
AFTER UPDATE ON EmsProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'EmsProperties','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEmsProperties
AFTER DELETE ON EmsProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'EmsProperties','Delete');
END$$

-- Triggers for EventLogNew --
DELIMITER $$
CREATE TRIGGER UpdateEventLogNew 
AFTER UPDATE ON EventLogNew 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.DeviceId,',',New.TypeId,',',New.ActionId,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventLogNew
AFTER INSERT ON EventLogNew
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.DeviceId,',',New.TypeId,',',New.ActionId,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventLogNew
AFTER DELETE ON EventLogNew
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.DeviceId,',',Old.TypeId,',',Old.ActionId,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Delete');
END$$

--- Triggers to EventNewAction --
DELIMITER $$
CREATE TRIGGER InsertEventNewAction
AFTER INSERT ON EventNewAction
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewAction','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEventNewAction
AFTER UPDATE ON EventNewAction
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewAction','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventNewAction
AFTER DELETE ON EventNewAction
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewAction','Delete');
END$$

--- Triggers to RESTAccount --
DELIMITER $$
CREATE TRIGGER InsertRESTAccount
AFTER INSERT ON RESTAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTAccount','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTAccount
AFTER UPDATE ON RESTAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTAccount','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTAccount
AFTER DELETE ON RESTAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.AccountName,'RESTAccount','Delete');
END$$

-- Triggers for EventNewDefinition --
DELIMITER $$
CREATE TRIGGER UpdateEventNewDefinition 
AFTER UPDATE ON EventNewDefinition 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.DeviceId,',',New.TypeId,',',New.ActionId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventNewDefinition
AFTER INSERT ON EventNewDefinition
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.DeviceId,',',New.TypeId,',',New.ActionId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventNewDefinition
AFTER DELETE ON EventNewDefinition
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.DeviceId,',',Old.TypeId,',',Old.ActionId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventNewDevice
AFTER INSERT ON EventNewDevice
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewDevice','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEventNewDevice
AFTER UPDATE ON EventNewDevice
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewDevice','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventNewDevice
AFTER DELETE ON EventNewDevice
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewDevice','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventNewType
AFTER INSERT ON EventNewType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewType','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateEventNewType
AFTER UPDATE ON EventNewType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewType','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventNewType
AFTER DELETE ON EventNewType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewType','Delete');
END$$

--- Triggers to LotSettingFileContent --
DELIMITER $$
CREATE TRIGGER InsertLotSettingFileContent
AFTER INSERT ON LotSettingFileContent
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSettingFileContent','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateLotSettingFileContent
AFTER UPDATE ON LotSettingFileContent
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSettingFileContent','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteLotSettingFileContent
AFTER DELETE ON LotSettingFileContent
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'LotSettingFileContent','Delete');
END$$

--- Triggers to MerchantAccount --
DELIMITER $$
CREATE TRIGGER InsertMerchantAccount
AFTER INSERT ON MerchantAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'MerchantAccount','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateMerchantAccount
AFTER UPDATE ON MerchantAccount
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
-- Aug 06 ITF
set Id=concat(New.Id,',',New.version,',',ifNULL(New.CustomerId,''),',',ifnull(New.Name,''),',',ifnull(New.Field1,''),',',ifnull(New.Field2,''),',',ifnull(New.Field3,''),',',New.IsDeleted,',',New.ProcessorName,',',New.ReferenceCounter,',',ifnull(New.Field4,''),',',ifnull(New.Field5,''),',',ifnull(New.Field6,''),',',New.IsValid);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'MerchantAccount','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteMerchantAccount
AFTER DELETE ON MerchantAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'MerchantAccount','Delete');
END$$

--- Triggers to ModemSetting --
DELIMITER $$
CREATE TRIGGER InsertModemSetting
AFTER INSERT ON ModemSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ModemSetting','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateModemSetting
AFTER UPDATE ON ModemSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ModemSetting','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteModemSetting
AFTER DELETE ON ModemSetting
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'ModemSetting','Delete');
END$$

-- Triggers for PaystationAlert --
DELIMITER $$
CREATE TRIGGER UpdatePaystationAlert 
AFTER UPDATE ON PaystationAlert 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.AlertId,',',New.PaystationId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertPaystationAlert
AFTER INSERT ON PaystationAlert
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.AlertId,',',New.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystationAlert
AFTER DELETE ON PaystationAlert
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.AlertId,',',Old.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Delete');
END$$

--- Triggers to PaystationGroup --
DELIMITER $$
CREATE TRIGGER InsertPaystationGroup
AFTER INSERT ON PaystationGroup
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PaystationGroup','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdatePaystationGroup
AFTER UPDATE ON PaystationGroup
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PaystationGroup','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystationGroup
AFTER DELETE ON PaystationGroup
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PaystationGroup','Delete');
END$$


--- Paystation Group --
DELIMITER $$
CREATE TRIGGER UpdatePaystation_PaystationGroup 
AFTER UPDATE ON Paystation_PaystationGroup 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationGroupId,',',New.PaystationId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertPaystation_PaystationGroup
AFTER INSERT ON Paystation_PaystationGroup
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationGroupId,',',New.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystation_PaystationGroup
AFTER DELETE ON Paystation_PaystationGroup
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationGroupId,',',Old.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Delete');
END$$

-- Triggers to create PreAuth --
DELIMITER $$
CREATE TRIGGER InsertPreAuth
AFTER INSERT ON PreAuth
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuth','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdatePreAuth
AFTER UPDATE ON PreAuth
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuth','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeletePreAuth
AFTER DELETE ON PreAuth
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PreAuth','Delete');
END$$

--- Triggers to PreAuthHolding --
DELIMITER $$
CREATE TRIGGER InsertPreAuthHolding
AFTER INSERT ON PreAuthHolding
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuthHolding','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdatePreAuthHolding
AFTER UPDATE ON PreAuthHolding
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuthHolding','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeletePreAuthHolding
AFTER DELETE ON PreAuthHolding
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PreAuthHolding','Delete');
END$$

--- Triggers for Processor --
DELIMITER $$
CREATE TRIGGER InsertProcessor
AFTER INSERT ON Processor
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'Processor','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateProcessor
AFTER UPDATE ON Processor
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'Processor','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteProcessor
AFTER DELETE ON Processor
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'Processor','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTActionType
AFTER INSERT ON RESTActionType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RESTActionType','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTActionType
AFTER UPDATE ON RESTActionType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RESTActionType','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTActionType
AFTER DELETE ON RESTActionType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'RESTActionType','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTLog 
AFTER UPDATE ON RESTLog 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.RESTAccountName,',',New.EndpointName,',',New.LoggingDate,',',New.IsError,',',New.CustomerId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTLog
AFTER INSERT ON RESTLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.RESTAccountName,',',New.EndpointName,',',New.LoggingDate,',',New.IsError,',',New.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTLog
AFTER DELETE ON RESTLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.RESTAccountName,',',Old.EndpointName,',',Old.LoggingDate,',',Old.IsError,',',Old.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTLogTotalCall 
AFTER UPDATE ON RESTLogTotalCall 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.RESTAccountName,',',New.LoggingDate); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTLogTotalCall
AFTER INSERT ON RESTLogTotalCall
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.RESTAccountName,',',New.LoggingDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTLogTotalCall
AFTER DELETE ON RESTLogTotalCall
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.RESTAccountName,',',Old.LoggingDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTProperties
AFTER INSERT ON RESTProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'RESTProperties','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTProperties
AFTER UPDATE ON RESTProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'RESTProperties','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTProperties
AFTER DELETE ON RESTProperties
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'RESTProperties','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTSessionToken
AFTER INSERT ON RESTSessionToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTSessionToken','Insert');
END$$


DELIMITER $$
CREATE TRIGGER UpdateRESTSessionToken
AFTER UPDATE ON RESTSessionToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTSessionToken','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTSessionToken
AFTER DELETE ON RESTSessionToken
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.AccountName,'RESTSessionToken','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateRates 
AFTER UPDATE ON Rates 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRates
AFTER INSERT ON Rates
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRates
AFTER DELETE ON Rates
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertRawSensorData
AFTER INSERT ON RawSensorData
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField,',',New.LastRetryTime,',',New.RetryCount);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorData','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRawSensorDataArchive 
AFTER UPDATE ON RawSensorDataArchive 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRawSensorDataArchive
AFTER INSERT ON RawSensorDataArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRawSensorDataArchive
AFTER DELETE ON RawSensorDataArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.ServerName,',',Old.PaystationCommAddress,',',Old.Type,',',Old.Action,',',Old.Information,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertRegionPaystationLog
AFTER INSERT ON RegionPaystationLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RegionPaystationLog','Insert');
END$$


DELIMITER $$
CREATE TRIGGER UpdateRegionPaystationLog
AFTER UPDATE ON RegionPaystationLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RegionPaystationLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRegionPaystationLog
AFTER DELETE ON RegionPaystationLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'RegionPaystationLog','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateReplenish 
AFTER UPDATE ON Replenish 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.Date,',',New.Number); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertReplenish
AFTER INSERT ON Replenish
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.Date,',',New.Number);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteReplenish
AFTER DELETE ON Replenish
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.Date,',',Old.Number);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Delete');
END$$



DELIMITER $$
CREATE TRIGGER InsertRole
AFTER INSERT ON Role
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Role','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRole
AFTER UPDATE ON Role
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Role','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRole
AFTER DELETE ON Role
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Role','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateSCRetry 
AFTER UPDATE ON SCRetry 
FOR EACH ROW 
BEGIN 

DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSCRetry
AFTER INSERT ON SCRetry
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSCRetry
AFTER DELETE ON SCRetry
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSAlert
AFTER INSERT ON SMSAlert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.MobileNumber,'SMSAlert','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateSMSAlert
AFTER UPDATE ON SMSAlert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.MobileNumber,'SMSAlert','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSMSAlert
AFTER DELETE ON SMSAlert
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.MobileNumber,'SMSAlert','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateSMSFailedResponse 
AFTER UPDATE ON SMSFailedResponse 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSFailedResponse
AFTER INSERT ON SMSFailedResponse
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSMSFailedResponse
AFTER DELETE ON SMSFailedResponse
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSMessageType
AFTER INSERT ON SMSMessageType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'SMSMessageType','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateSMSMessageType
AFTER UPDATE ON SMSMessageType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'SMSMessageType','Update');
END$$


DELIMITER $$
CREATE TRIGGER DeleteSMSMessageType
AFTER DELETE ON SMSMessageType
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'SMSMessageType','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateSMSTransactionLog 
AFTER UPDATE ON SMSTransactionLog 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSTransactionLog
AFTER INSERT ON SMSTransactionLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSMSTransactionLog
AFTER DELETE ON SMSTransactionLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateServiceAgreedCustomer 
AFTER UPDATE ON ServiceAgreedCustomer 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.ServiceAgreementId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertServiceAgreedCustomer
AFTER INSERT ON ServiceAgreedCustomer
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.ServiceAgreementId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteServiceAgreedCustomer
AFTER DELETE ON ServiceAgreedCustomer
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.ServiceAgreementId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertServiceAgreement
AFTER INSERT ON ServiceAgreement
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'ServiceAgreement','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateServiceAgreement
AFTER UPDATE ON ServiceAgreement
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'ServiceAgreement','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteServiceAgreement
AFTER DELETE ON ServiceAgreement
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'ServiceAgreement','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateServiceState
AFTER UPDATE ON ServiceState
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ServiceState','Update');
END$$


DELIMITER $$
CREATE TRIGGER DeleteServiceState
AFTER DELETE ON ServiceState
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'ServiceState','Delete');
END$$

DELIMITER $$
CREATE TRIGGER InsertTaxes
AFTER INSERT ON Taxes
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Taxes','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTaxes
AFTER UPDATE ON Taxes
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Taxes','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTaxes
AFTER DELETE ON Taxes
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Taxes','Delete');
END$$


DELIMITER $$
CREATE TRIGGER UpdateTransaction
AFTER UPDATE ON Transaction
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(5000); 
set Id = concat(New.CustomerId,',',New.PaystationId,',',New.TicketNumber,',',New.LotNumber,',',New.StallNumber,',',New.TypeId,',',New.PurchasedDate,',',New.ExpiryDate,',',New.ChargedAmount,',',New.CashPaid,',',New.CardPaid,',',New.ChangeDispensed,',',New.ExcessPayment,',',New.IsRefundSlip,',',New.CustomCardData,',',New.CustomCardType,',',New.CouponNumber,',',New.CitationNumber,',',New.SmartCardData,',',New.SmartCardPaid,',',New.PaymentTypeId,',',New.CreationDate,',',New.RateId,',',New.AuthenticationCard,',',New.RegionId,',',New.AddTimeNum,',',New.OriginalAmount,',',New.IsOffline,',',New.PlateNumber); 
IF (New.CustomCardData<>'NULL' or New.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Update');
END IF;
IF(New.SmartCardData<>'NULL' or New.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Update');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Update');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Update');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransaction
AFTER INSERT ON Transaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
DECLARE CustomCardLength int(20);
DECLARE vendorId VARCHAR(50) DEFAULT '''';
DECLARE isVerrusTxn TINYINT(1) DEFAULT 0;

-- set Id = concat(New.CustomerId,',',New.PaystationId,',',New.TicketNumber,',',New.LotNumber,',',New.StallNumber,',',New.TypeId,',',New.PurchasedDate,',',New.ExpiryDate,',',New.ChargedAmount,',',New.CashPaid,',',New.CardPaid,',',New.ChangeDispensed,',',New.ExcessPayment,',',New.IsRefundSlip,',',New.CustomCardData,',',New.CustomCardType,',',New.CouponNumber,',',New.CitationNumber,',',New.SmartCardData,',',New.SmartCardPaid,',',New.PaymentTypeId,',',New.CreationDate,',',New.RateId,',',New.AuthenticationCard,',',New.RegionId,',',New.AddTimeNum,',',New.OriginalAmount,',',New.IsOffline,',',New.PlateNumber);
set Id = concat(New.CustomerId,',',New.PaystationId,',',New.TicketNumber,',',New.LotNumber,',',New.StallNumber,',',New.TypeId,',',New.PurchasedDate,',',New.ExpiryDate,',',New.ChargedAmount,',',New.CashPaid,',',New.CardPaid,',',New.ChangeDispensed,',',New.ExcessPayment,',',New.IsRefundSlip,',',New.CustomCardData,',',New.CustomCardType,',',New.CouponNumber,',',New.CitationNumber,',',ifnull(New.SmartCardData,''),',',New.SmartCardPaid,',',New.PaymentTypeId,',',New.CreationDate,',',New.RateId,',',ifnull(New.AuthenticationCard,''),',',New.RegionId,',',New.AddTimeNum,',',New.OriginalAmount,',',New.IsOffline,',',ifnull(New.PlateNumber,''));

IF (New.CustomCardData<>'NULL' or New.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Insert');
END IF;
IF(New.SmartCardData<>'NULL' or New.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Insert');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Insert');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Insert');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Insert');



-- Code copied from exsiting Functionality on Aug 6

    SELECT IsVerrus INTO isVerrusTxn FROM Paystation WHERE Id = NEW.PaystationId;
    IF isVerrusTxn = 0 AND NEW.PlateNumber IS NOT NULL AND NEW.PlateNumber <> '''' THEN
        SELECT Field2 INTO vendorId from `ThirdPartyServiceAccount` WHERE CustomerId = NEW.CustomerId AND Type = 'Verrus';
        IF vendorId IS NOT NULL AND vendorId <> '''' THEN
            INSERT IGNORE INTO `TransactionPush` VALUES (NEW.PaystationId, NEW.PurchasedDate, NEW.TicketNumber, NEW.CustomerId,
            NEW.LotNumber, NEW.PlateNumber, NEW.ExpiryDate, NEW.ChargedAmount, NEW.StallNumber, 0, NEW.PurchasedDate, NEW.IsOffline);
        END IF;
    END IF;

END$$

DELIMITER $$
CREATE TRIGGER DeleteTransaction
AFTER DELETE ON Transaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
set Id = concat(Old.CustomerId,',',Old.PaystationId,',',Old.TicketNumber,',',Old.LotNumber,',',Old.StallNumber,',',Old.TypeId,',',Old.PurchasedDate,',',Old.ExpiryDate,',',Old.ChargedAmount,',',Old.CashPaid,',',Old.CardPaid,',',Old.ChangeDispensed,',',Old.ExcessPayment,',',Old.IsRefundSlip,',',Old.CustomCardData,',',Old.CustomCardType,',',Old.CouponNumber,',',Old.CitationNumber,',',Old.SmartCardData,',',Old.SmartCardPaid,',',Old.PaymentTypeId,',',Old.CreationDate,',',Old.RateId,',',Old.AuthenticationCard,',',Old.RegionId,',',Old.AddTimeNum,',',Old.OriginalAmount,',',Old.IsOffline,',',Old.PlateNumber);
IF (Old.CustomCardData<>'NULL' or Old.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Delete');
END IF;
IF(Old.SmartCardData<>'NULL' or Old.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Delete');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Delete');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Delete');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransactionCollection 
AFTER UPDATE ON TransactionCollection 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransactionCollection
AFTER INSERT ON TransactionCollection
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Insert');

-- code copied from existing functionality on AUg 6

    IF (SELECT COUNT(*) FROM PaystationBalance WHERE PaystationId = NEW.PaystationId AND IsRecalcable = 0) = 1 THEN
        
        
        UPDATE  PaystationBalance
        SET     IsRecalcable = 1
        WHERE   PaystationId = NEW.PaystationId;
    END IF;

END$$

DELIMITER $$
CREATE TRIGGER DeleteTransactionCollection
AFTER DELETE ON TransactionCollection
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Delete');
END$$


DELIMITER $$
CREATE TRIGGER InsertCerberusAccount
AFTER INSERT ON CerberusAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusAccount','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateCerberusAccount
AFTER UPDATE ON CerberusAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusAccount','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCerberusAccount
AFTER DELETE ON CerberusAccount
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CerberusAccount','Delete');
END$$

-- ProcessorProperties Trigger --

DELIMITER $$
CREATE TRIGGER UpdateProcessorProperties 
AFTER UPDATE ON ProcessorProperties 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Processor,',',New.Name); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertProcessorProperties
AFTER INSERT ON ProcessorProperties
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Processor,',',New.Name);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteProcessorProperties
AFTER DELETE ON ProcessorProperties
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Processor,',',Old.Name);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransaction2Push 
AFTER UPDATE ON Transaction2Push 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.ThirdPartyServiceAccountId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransaction2Push
AFTER INSERT ON Transaction2Push
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.ThirdPartyServiceAccountId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransaction2Push
AFTER DELETE ON Transaction2Push
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.ThirdPartyServiceAccountId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransactionPush 
AFTER UPDATE ON TransactionPush 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransactionPush
AFTER INSERT ON TransactionPush
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransactionPush
AFTER DELETE ON TransactionPush
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Delete');
END$$


-- ProcessorTransaction Trigger --
DELIMITER $$
CREATE TRIGGER UpdateProcessorTransaction 
AFTER UPDATE ON ProcessorTransaction 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(5000); 
set Id = concat(New.TypeId,',',New.PaystationId,',',New.MerchantAccountId,',',New.TicketNumber,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',',New.CardChecksum,',',New.PurchasedDate,',',New.ProcessingDate,',',New.ProcessorTransactionId,',',New.AuthorizationNumber,',',New.ReferenceNumber,',',New.Approved,',',New.CardHash,',',New.IsUploadedFromBoss,',',New.IsRFID,',',old.TypeId,',',old.ProcessingDate); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertProcessorTransaction
AFTER INSERT ON ProcessorTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
set Id=concat(New.TypeId,',',New.PaystationId,',',New.MerchantAccountId,',',New.TicketNumber,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',',New.CardChecksum,',',New.PurchasedDate,',',New.ProcessingDate,',',New.ProcessorTransactionId,',',New.AuthorizationNumber,',',New.ReferenceNumber,',',New.Approved,',',New.CardHash,',',New.IsUploadedFromBoss,',',New.IsRFID);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteProcessorTransaction
AFTER DELETE ON ProcessorTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
set Id=concat(Old.TypeId,',',Old.PaystationId,',',Old.MerchantAccountId,',',Old.TicketNumber,',',Old.Amount,',',Old.CardType,',',Old.Last4DigitsOfCardNumber,',',Old.CardChecksum,',',Old.PurchasedDate,',',Old.ProcessingDate,',',Old.ProcessorTransactionId,',',Old.AuthorizationNumber,',',Old.ReferenceNumber,',',Old.Approved,',',Old.CardHash,',',Old.IsUploadedFromBoss,',',Old.IsRFID);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Delete');
END$$


-- ValidCustomCard Trigger --

DELIMITER $$
CREATE TRIGGER UpdateValidCustomCard
AFTER UPDATE ON ValidCustomCard
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(5000); 
set Id = concat(New.CustomerId,',',New.CardNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ValidCustomCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertValidCustomCard
AFTER INSERT ON ValidCustomCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
set Id = concat(New.CustomerId,',',New.CardNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ValidCustomCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteValidCustomCard
AFTER DELETE ON ValidCustomCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(5000);
set Id = concat(Old.CustomerId,',',Old.CardNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ValidCustomCard','Delete');
END$$
