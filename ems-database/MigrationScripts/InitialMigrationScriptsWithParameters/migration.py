#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on QA data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess

ClusterId = 1;
try:
#	EMS6Connection = mdb.connect('10.1.2.202', 'apamu', 'apamu', 'ems_db'); 
#	EMS6ConnectionCerberus = mdb.connect('10.1.2.202', 'apamu', 'apamu','cerberus_db');
#	EMS7Connection = mdb.connect('10.1.2.102', 'apamu','apamu','EMS7Blitz');
	EMS6Connection = mdb.connect('10.0.2.251', 'ltikhova', 'Pass.word', 'ems_db_6_3_12'); 
	EMS6ConnectionCerberus = mdb.connect('10.0.2.251', 'ltikhova', 'Pass.word','cerberus_db');
	EMS7Connection = mdb.connect('172.16.5.12', 'migration','migration','EMS7QA2');
	#EMS7Connection = mdb.connect('172.16.5.12', 'migration','migration','ems_ashok_micro');
		
#	EMS7Connection = mdb.connect('172.16.5.56','migration','m1gr1t3','EMS7')

	#EMS6Cursor = EMS6Connection.cursor()
	#EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
	#EMS7Cursor = EMS7Connection.cursor()

	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccess(EMS7Connection, 1), 0)

#1	

#	controller.logModuleStartStatus('Customers') 
#	controller.migrateCustomers() #Done
#	controller.logModuleEndStatus('Customers') 
	
#2	
#	controller.logModuleStartStatus('TrialCustomer') 
#	controller.migrateTrialCustomer() #Done
#	controller.logModuleEndStatus('TrialCustomer') 

#3

#	controller.logModuleStartStatus('Locations') 
#	controller.migrateLocations() #Done
#	controller.logModuleEndStatus('Locations') 
	
#4

#	controller.logModuleStartStatus('LotSettings') 	
#	controller.migrateLotSettings() #Done
#	controller.logModuleEndStatus('LotSettings') 

#5

#	controller.logModuleStartStatus('ParentRegionIdToLocation') 	
#	controller.migrateParentRegionIdToLocation() #Done
#	controller.logModuleEndStatus('ParentRegionIdToLocation') 

#6

	controller.logModuleStartStatus('Paystations') 	
	controller.migratePaystations() # PointOfSale table is Migrated from within Paystation table migration  #Done
	controller.logModuleEndStatus('Paystations') 

#7

#	controller.logModuleStartStatus('Coupons') 	
#	controller.migrateCoupons() #Done
#	controller.logModuleEndStatus('Coupons') 

#8

#	controller.logModuleStartStatus('RestAccount') 	
#	controller.migrateRestAccount() #Done
#	controller.logModuleEndStatus('RestAccount') 

#9

#	controller.logModuleStartStatus('ModemSettings') 	
#	controller.migrateModemSettings()   #Done
#	controller.logModuleEndStatus('ModemSettings') 

#10

#	controller.logModuleStartStatus('EMSRateIntoUnifiedRate') 	
#	controller.migrateEMSRateIntoUnifiedRate()  #Done
#	controller.logModuleEndStatus('EMSRateIntoUnifiedRate') 


#11

#	controller.logModuleStartStatus('CryptoKey') 	
#	controller.migrateCryptoKey()	#Done
#	controller.logModuleEndStatus('CryptoKey') 

#12

#	controller.logModuleStartStatus('Rates') 	
#	controller.migrateRates() #Done
#	controller.logModuleEndStatus('Rates') 

#13

#	controller.logModuleStartStatus('RestSessionToken') 	
#	controller.migrateRestSessionToken() #Done
#	controller.logModuleEndStatus('RestSessionToken') 

#14

#	controller.logModuleStartStatus('RatesToEMS7ExtensibleRate') 	
#	controller.migrateRatesToEMS7ExtensibleRate() #Done
#	controller.logModuleEndStatus('RatesToEMS7ExtensibleRate') 

#15

#	controller.logModuleStartStatus('CustomerSubscription') 	
#	controller.migrateCustomerSubscription() #Done
#	controller.logModuleEndStatus('CustomerSubscription') 
	
#16

#	controller.logModuleStartStatus('CustomerProperties') 	
#	controller.migrateCustomerProperties() #Done	
#	controller.logModuleEndStatus('CustomerProperties') 

#17

#	controller.logModuleStartStatus('CustomerWebServiceCal') 	
#	controller.migrateCustomerWebServiceCal() #Done
#	controller.logModuleEndStatus('CustomerWebServiceCal') 

#18

#	controller.logModuleStartStatus('CustomerWsToken') 	
#	controller.migrateCustomerWsToken() #Done
#	controller.logModuleEndStatus('CustomerWsToken') 	

#19

#	controller.logModuleStartStatus('CustomerAlert') 	
#	controller.migrateCustomerAlert() #Done ## Look for the notes to migrate this module, a db change is needed
#	controller.logModuleEndStatus('CustomerAlert') 	

#20

#	controller.logModuleStartStatus('CustomerCardType') 	
#	controller.migrateCustomerCardType() #Done
#	controller.logModuleEndStatus('CustomerCardType') 	


#21

#	controller.logModuleStartStatus('QueryStallsCustomerProperty') 	
#	controller.migrateQueryStallsCustomerProperty() # Done This Module populates the CustomerProperty table for the QueryStalls by Porperty Type from Paystation table.
#	controller.logModuleEndStatus('QueryStallsCustomerProperty') 	
	

#22	

#	controller.logModuleStartStatus('BadValueCard') 		
#	controller.migrateBadValueCard() #Done
#	controller.logModuleEndStatus('BadValueCard') 	

#23
	
#	controller.logModuleStartStatus('BadCreditCard') 		
#	controller.migrateBadCreditCard() #Done
#	controller.logModuleEndStatus('BadCreditCard') 	

#24
	
#	controller.logModuleStartStatus('BadSmartCard') 		
#	controller.migrateBadSmartCard() #Done
#	controller.logModuleEndStatus('BadSmartCard') 	

#25

#	controller.logModuleStartStatus('MerchantAccount') 		
#	controller.migrateMerchantAccount()	#Done (InElsePart)
#	controller.logModuleEndStatus('MerchantAccount') 	

#26
#	controller.logModuleStartStatus('ServiceAgreement') 		
#	controller.migrateServiceAgreement() #Done
#	controller.logModuleEndStatus('ServiceAgreement') 	

#27
#	controller.logModuleStartStatus('ServiceAgreedCustomer') 		
#	controller.migrateServiceAgreedCustomer() #Done
#	controller.logModuleEndStatus('ServiceAgreedCustomer') 		
	
#28
#	controller.logModuleStartStatus('PaystationAlertEmail') 		
#	controller.migratePaystationAlertEmail() #Done
#	controller.logModuleEndStatus('PaystationAlertEmail') 		
	
#29	
#	controller.logModuleStartStatus('SCRetry') 		
#	controller.migrateSCRetry() #Done
#	controller.logModuleEndStatus('SCRetry') 		

#30
#	controller.logModuleStartStatus('POSServiceState') 		
#	controller.migratePOSServiceState() #Done
#	controller.logModuleEndStatus('POSServiceState') 		


#31
#	controller.logModuleStartStatus('POSRoute') 		
#	controller.migratePOSRoute() #Done
#	controller.logModuleEndStatus('POSRoute') 		
	
#32
#	controller.logModuleStartStatus('ExtensibleRateDayOfWeek') 		
#	controller.migrateExtensibleRateDayOfWeek() ## Type table not getting incrementally migrated #Done
#	controller.logModuleEndStatus('ExtensibleRateDayOfWeek') 		

#33
#	controller.logModuleStartStatus('UserAccount') 		
#	controller.migrateUserAccount() #Done
#	controller.logModuleEndStatus('UserAccount') 		
		

#34

#	controller.logModuleStartStatus('ParkingPermissionDayOfWeek') 		
#	controller.migrateParkingPermissionDayOfWeek() #Done
#	controller.logModuleEndStatus('ParkingPermissionDayOfWeek') 	

#35
#	controller.logModuleStartStatus('ProcessorProperties') 		
#	controller.migrateProcessorProperties()  #Done
#	controller.logModuleEndStatus('ProcessorProperties') 	

#35.1
#	controller.migrateExtensiblePermit() #Logic error

# TRANSACTION TABLES START

#36

#	controller.logModuleStartStatus('LicencePlate') 		
#	controller.migrateLicencePlate() #  Done 
#	controller.logModuleEndStatus('LicencePlate') 	

#37
#	controller.logModuleStartStatus('MobileNumber') 		
#	controller.migrateMobileNumber() ## Pending Incremental Migration, This module will be migrted with triggers #Ashok: Done
#	controller.logModuleEndStatus('MobileNumber') 	

#38

#	controller.logModuleStartStatus('Purchase') 		
#	controller.migratePurchase(ClusterId,'Purchase') #Done	
#	controller.logModuleEndStatus('Purchase') 	

#39

#	controller.logModuleStartStatus('ProcessorTransaction') 		
#	controller.migrateProcessorTransaction(ClusterId,'ProcessorTransaction') # Changed the SQL to process 6 months of data  Done
#	controller.logModuleEndStatus('ProcessorTransaction') 	
	
#40

#	controller.logModuleStartStatus('Permit') 		
#	controller.migratePermit(ClusterId,'Permit') #Done
#	controller.logModuleEndStatus('Permit') 	

#41

#	controller.logModuleStartStatus('PaymentCard') 		
#	controller.migratePaymentCard(ClusterId,'PaymentCard') # Done
#	controller.logModuleEndStatus('PaymentCard') 	

#42

#	controller.logModuleStartStatus('POSCollection') 		
#	controller.migratePOSCollection(ClusterId,'POSCollection') #Done
#	controller.logModuleEndStatus('POSCollection') 	
	
#43

#	controller.logModuleStartStatus('PurchaseTax') 		
#	controller.migratePurchaseTax(ClusterId,'PurchaseTax') #Done
#	controller.logModuleEndStatus('PurchaseTax') 	
	
#44
#	controller.logModuleStartStatus('PurchaseCollection') 		
#	controller.migratePurchaseCollection(ClusterId,'PurchaseCollection')  #Done
#	controller.logModuleEndStatus('PurchaseCollection') 	


# TRANSACTION TABLES END

#	LOG TABLE START. 

#	controller.logModuleStartStatus('ValueCard') 		
#	controller.migrateValueCardData() #Done
#	controller.logModuleEndStatus('ValueCard') 	

#	controller.logModuleStartStatus('SmartCard') 
#	controller.migrateSmartCardData() #Done
#	controller.logModuleEndStatus('SmartCard') 	

#	controller.logModuleStartStatus('RegionPaystationLog') 
#	controller.migrateRegionPaystationLog() #Done
#	controller.logModuleEndStatus('RegionPaystationLog') 	

#	controller.logModuleStartStatus('SMSAlert') 	
#	controller.migrateSMSAlert() #Ashok: 
#	controller.logModuleEndStatus('SMSAlert') 		
	
#	controller.logModuleStartStatus('SMSTransactionLog') 		
#	controller.migrateSMSTransactionLog() #Ashok: 
#	controller.logModuleEndStatus('SMSTransactionLog') 		

#	controller.logModuleStartStatus('SMSFailedResponse') 		
#	controller.migrateSMSFailedResponse() #Ashok: 
#	controller.logModuleEndStatus('SMSFailedResponse') 		

#	controller.logModuleStartStatus('EventLogNewIntoPOSAlert') 		
#	controller.migrateEventLogNewIntoPOSAlert() # In progress CompletedwithCondition Removed the condition 
#	controller.logModuleEndStatus('EventLogNewIntoPOSAlert') 		

#	controller.logModuleStartStatus('CCFailLog') 	#COMPLETED TODAY	
#	controller.migrateCCFailLog()	
#	controller.logModuleEndStatus('CCFailLog') 		
	
#	controller.logModuleStartStatus('Reversal') 	#
#	controller.migrateReversal() #Error MerchantAccount Cannot be Null Added the Condition now
#	controller.logModuleEndStatus('Reversal') 		
	
#	controller.logModuleStartStatus('ReversalArchive') 	#		
#	controller.migrateReversalArchive()
#	controller.logModuleEndStatus('ReversalArchive') 	

#	controller.logModuleStartStatus('PreAuth') 	#		
#	controller.migratePreAuth() # 
#	controller.logModuleEndStatus('PreAuth') 	
		
#	controller.logModuleStartStatus('PreAuthHolding') 	#			
#	controller.migratePreAuthHolding() # 	
#	controller.logModuleEndStatus('PreAuthHolding') 	

#	controller.logModuleStartStatus('POSDateBilling') 	#			
#	controller.migratePOSDateBilling()
#	controller.logModuleEndStatus('POSDateBilling') 	

#	controller.logModuleStartStatus('POSStatus') 	#			
#	controller.migratePOSStatus() #Completed
#	controller.logModuleEndStatus('POSStatus') 	

#	controller.logModuleStartStatus('POSDateAuditAccess') 	#			
#	controller.migratePOSDateAuditAccess()
#	controller.logModuleEndStatus('POSDateAuditAccess') 	

#	controller.logModuleStartStatus('POSDatePaystation') 	#			
#	controller.migratePOSDatePaystation() #Completed
#	controller.logModuleEndStatus('POSDatePaystation') 	


#	controller.logModuleStartStatus('LotSettingContent') 	
#	controller.migrateLotSettingContent() ##Pending validation for incremental migration #Ashok: NOW
#	controller.logModuleEndStatus('LotSettingContent') 	

#	controller.logModuleStartStatus('Replenish') 	
#	controller.migrateReplenish()
#	controller.logModuleEndStatus('Replenish') 	

#	controller.logModuleStartStatus('CardRetryTransaction') 		
#	controller.migrateCardRetryTransaction()
#	controller.logModuleEndStatus('CardRetryTransaction') 	

#	controller.logModuleStartStatus('POSHeartBeat') 		
#	controller.migratePOSHeartBeat() #ASHOK: DID NOT EXECUTED. LATER
#	controller.logModuleEndStatus('POSHeartBeat') 	

#	controller.logModuleStartStatus('ActivePOSAlert') 		
#	controller.migrateActivePOSAlert() #CompletedwithCondition ###### but no data in destination table
#	controller.logModuleEndStatus('ActivePOSAlert') 	


		

	
#########################################	
#35 ERROR CUSTOMER ALERT TYPEID CANNOT BE NULL
#	controller.logModuleStartStatus('CustomerEmail') 		
#	controller.migrateCustomerEmail() #Error 
#	controller.logModuleEndStatus('CustomerEmail') 
	
#	controller.logModuleStartStatus('PaystationGroup') 		
#	controller.migratePaystationGroup() #NOW
#	controller.logModuleEndStatus('PaystationGroup') 

#34 ERROR
#	controller.logModuleStartStatus('ParkingPermission') 		
#	controller.migrateParkingPermission() #  FK ERROR RegionID 13931 doesnot exists
#	controller.logModuleEndStatus('ParkingPermission') 



#	THIS IS A LOG TABLE. CAN BE MIGRATED AT THE END	


#	controller.migrateWebServiceCallLog() # NO NEED TO MIGRATE
#	controller.migrateRestLog()   #NO NEED TO MIGRATE Completed 
#	controller.migrateRestLogTotalCall()  #NO NEED TO MIGRATE
#	controller.migrateBatteryInfo() #NO NEED TO MIGRATE
#	controller.migratePaystationBalance()	#ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateClusterMember() # for cluster table to work, please disable the auto increment for the EMS7.Cluster table. The auto_increment can be enabled after the initial migration has finished, as we will need to bring all the Id's as they exist in EMS6. #ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateCollectionLock() #ASHOK: DID NOT EXECUTED. LATER


	print "Completed"








   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
