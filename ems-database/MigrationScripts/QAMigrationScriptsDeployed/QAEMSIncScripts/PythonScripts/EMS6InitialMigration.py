##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6InitialMigration
##	Purpose		: This class instruments the data extraction from EMS6 database and data Transformation/Loading into EMS7 Database. The procedures in the class are called by the Main controller class, "migration.py". 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

from datetime import date
class EMS6InitialMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccessInsert, verbose):
		self.__verbose = verbose
		print " Verbose Mode Set to %s" %self.__verbose
		print " Ashok: Inside EMS6InitialMigration Class"
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccessInsert = EMS7DataAccessInsert
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def migrateCustomers(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customers --> y/n    :   ") 
			if (input=='y') :
				for curCustomer in self.__EMS6DataAccess.getCustomers():
					self.__EMS7DataAccessInsert.addCustomer(curCustomer, Action)
		else:
			print " Ashok: IN ELSE PART"
			for curCustomer in self.__EMS6DataAccess.getCustomers():
				self.__EMS7DataAccessInsert.addCustomer(curCustomer, Action)

	def migrateCustomerProperties(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import CustomerProperties --> y/n     :    ")
			if (input=='y'):
				for curCustomerProperty in self.__EMS6DataAccess.getEMS6CustomerProperties():
					self.__EMS7DataAccessInsert.addCustomerPropertyToEMS7(curCustomerProperty,Action)
		else:
			for curCustomerProperty in self.__EMS6DataAccess.getEMS6CustomerProperties():
				self.__EMS7DataAccessInsert.addCustomerPropertyToEMS7(curCustomerProperty,Action)


	def migrateQueryStallsCustomerProperty(self):
		if(self.__verbose):
			input = raw_input("Import the Query Stalls by into Customer Property  --> y/n     :")
			if (input=='y'):
				for CustomerId in self.__EMS6DataAccess.getDistinctCustomerFromPaystation():
					for curQueryStallsBy in self.__EMS6DataAccess.getEMS6QueryStallsBy(CustomerId):
						self.__EMS7DataAccessInsert.addQueryStallsBy(curQueryStallsBy,CustomerId)
		else:
			for CustomerId in self.__EMS6DataAccess.getDistinctCustomerFromPaystation():
				for curQueryStallsBy in self.__EMS6DataAccess.getEMS6QueryStallsBy(CustomerId):
					self.__EMS7DataAccessInsert.addQueryStallsBy(curQueryStallsBy,CustomerId)
			
	def migrateLotSettings(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import LotSetting --> y/n      :   ")
			if(input=='y'):
				for curLotSetting in self.__EMS6DataAccess.getEMS6LotSettings():
					self.__EMS7DataAccessInsert.addLotSettingToEMS7(curLotSetting, Action)
		else:
			for curLotSetting in self.__EMS6DataAccess.getEMS6LotSettings():
				self.__EMS7DataAccessInsert.addLotSettingToEMS7(curLotSetting, Action)

	def migratePaystations(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Paystation --> y/n      :   ")
			if(input=='y'):
				for curPaystation in self.__EMS6DataAccess.getEMS6Paystation():
					self.__EMS7DataAccessInsert.addPaystationToEMS7(curPaystation,Action)
		else:
			for curPaystation in self.__EMS6DataAccess.getEMS6Paystation():
				self.__EMS7DataAccessInsert.addPaystationToEMS7(curPaystation,Action)

	def migrateLocations(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Location --> y/n      :   ")
			if(input=='y'):
				for curLocation in self.__EMS6DataAccess.getEMS6Locations():
					self.__EMS7DataAccessInsert.addLocationToEMS7(curLocation, Action)
		else:
			for curLocation in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccessInsert.addLocationToEMS7(curLocation, Action)

	def migrateCoupons(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Coupons --> y/n     :     ")
			if(input=='y'):
				for curCoupon in self.__EMS6DataAccess.getEMS6Coupons():
					self.__EMS7DataAccessInsert.addCouponToEMS7(curCoupon, Action)
		else:
			for curCoupon in self.__EMS6DataAccess.getEMS6Coupons():
				self.__EMS7DataAccessInsert.addCouponToEMS7(curCoupon, Action)

	def migrateParkingPermissionType(self):
		if(self.__verbose):
			input = raw_input("Importing EMSParkingPermissionType --> y/n  :")
			if(input=='y'):
				for curParkingPermissionType in self.__EMS6DataAccess.getEMS6ParkingPermissionType():
					self.__EMS7DataAccessInsert.addParkingPermissionTypeToEMS7(curParkingPermissionType)
		else:
			for curParkingPermissionType in self.__EMS6DataAccess.getEMS6ParkingPermissionType():
				self.__EMS7DataAccessInsert.addParkingPermissionTypeToEMS7(curParkingPermissionType)
		
	def migrateParkingPermission(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Parking Permission --> y/n     :")
			if(input=='y'):
				for curParkingPermission in self.__EMS6DataAccess.getParkingPermission():
					self.__EMS7DataAccessInsert.addParkingPermissionToEMS7(curParkingPermission, Action)
		else:
			for curParkingPermission in self.__EMS6DataAccess.getParkingPermission():
				self.__EMS7DataAccessInsert.addParkingPermissionToEMS7(curParkingPermission, Action)

	def migrateParkingPermissionDayOfWeek(self):		
		if(self.__verbose):	
			input = raw_input("Import EMSParkingParmissionDayOfweek --> y/n     :")	
			if(input=='y'):
				for curParkingPermissionDayOfWeek in self.__EMS6DataAccess.getParkingPermissionDayOfWeek():
					self.__EMS7DataAccessInsert.addParkingPermissionDayOfWeek(curParkingPermissionDayOfWeek)
		else:
			for curParkingPermissionDayOfWeek in self.__EMS6DataAccess.getParkingPermissionDayOfWeek():
				self.__EMS7DataAccessInsert.addParkingPermissionDayOfWeek(curParkingPermissionDayOfWeek)

#	def migrateUnifiedRate(self):
#		Action='Insert'
#		if(self.__verbose):
#			input = raw_input("Import Unified Rate --> y/n ")
#			if(input=='y'):
#				for curRates in self.__EMS6DataAccess.getEMS6Rates():
#					self.__EMS7DataAccessInsert.addRatesToEMS7(curRates, Action)
#		else:
#			for curRates in self.__EMS6DataAccess.getEMS6Rates():
#				self.__EMS7DataAccessInsert.addRatesToEMS7(curRates, Action)

	def migrateEMSRateIntoUnifiedRate(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import EMS Rates into UnifiedRate table --> y/n ")
			if(input=='y'):
				for curEMSRate in self.__EMS6DataAccess.getEMS6EMSRates():
					self.__EMS7DataAccessInsert.addEMSRatesToUnifiedRate(curEMSRate, Action)
		else:
			for curEMSRate in self.__EMS6DataAccess.getEMS6EMSRates():
				self.__EMS7DataAccessInsert.addEMSRatesToUnifiedRate(curEMSRate, Action)

#Should import Unified Rate first before migrating Extensible Rate
	def migrateExtensibleRateDayOfWeek(self):
		if(self.__verbose):
			input = raw_input("Import Exensible Rate Day Of Week --> y/n     :")
			if(input=='y'):
				for curExtensibleRateDOW in self.__EMS6DataAccess.getEMS6ExtensibleRatesDayOfWeek():
					self.__EMS7DataAccessInsert.addExtensibleRateDayOfWeekToEMS7(curExtensibleRateDOW)
		else:
			for curExtensibleRateDOW in self.__EMS6DataAccess.getEMS6ExtensibleRatesDayOfWeek():
				self.__EMS7DataAccessInsert.addExtensibleRateDayOfWeekToEMS7(curExtensibleRateDOW)

	def migrateRatesToEMS7ExtensibleRate(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import EMSRate into EMS7 Extensible Rate --> y/n     :")
			if(input=='y'):
				for curExtensibleRate in self.__EMS6DataAccess.getEMS6ExtensibleRate():
					self.__EMS7DataAccessInsert.addExtensibleRatesToEMS7(curExtensibleRate, Action)
		else:
			for curExtensibleRate in self.__EMS6DataAccess.getEMS6ExtensibleRate():
				self.__EMS7DataAccessInsert.addExtensibleRatesToEMS7(curExtensibleRate, Action)

	def migrateRestAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import RESTAccount --> y/n     :     ")
			if(input=='y'):
				for curRestAccount in self.__EMS6DataAccess.getEMS6RestAccount():
					self.__EMS7DataAccessInsert.addRestAccountToEMS7(curRestAccount, Action)
		else:
			for curRestAccount in self.__EMS6DataAccess.getEMS6RestAccount():
				self.__EMS7DataAccessInsert.addRestAccountToEMS7(curRestAccount, Action)

	def migrateLicencePlate(self):
		if(self.__verbose):
			input = raw_input("import Licence Plate --> y/n    : ")
			if(input=='y'):
				for distinctPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curLicencePlate in self.__EMS6DataAccess.getEMS6LicencePlate(distinctPaystationId):
						self.__EMS7DataAccessInsert.addLicencePlateToEMS7(curLicencePlate)
		else:
			for distinctPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curLicencePlate in self.__EMS6DataAccess.getEMS6LicencePlate(distinctPaystationId):
					self.__EMS7DataAccessInsert.addLicencePlateToEMS7(curLicencePlate)

	def migrateMobileNumber(self):
		if(self.__verbose):
			input = raw_input("import MobileNumber --> y/n")
			if(input=='y'):
				for curSMSAlertMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSAlert():
					self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curSMSAlertMobileNumber)
				for curExtensiblePermitMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromEMSExtensiblePermit():
					self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curExtensiblePermitMobileNumber)
				for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSTransactionLog():
					self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curSMSTransactionLog)
				for curMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumber():
					self.__EMS7DataAccessInsert.addMobileNumbertoEMS7(curMobileNumber)
		else:
			for curSMSAlertMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSAlert():
				self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curSMSAlertMobileNumber)
			for curExtensiblePermitMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromEMSExtensiblePermit():
				self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curExtensiblePermitMobileNumber)
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSTransactionLog():
				self.__EMS7DataAccessInsert.addMobileNumberToEMS7(curSMSTransactionLog)
			#Ashok: MobileNumber Table does not exists in EMS6
			#for curMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumber():
			#	self.__EMS7DataAccessInsert.addMobileNumbertoEMS7(curMobileNumber)

	def migrateExtensiblePermit(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import Extensible Permit --> y/n        :")
			if(input=='y'):
				for curExtensiblePermit in self.__EMS6DataAccess.getEMS6ExtensiblePermit():
					self.__EMS7DataAccessInsert.addExtensiblePermitToEMS7(curExtensiblePermit, Action)
		else:
			for curExtensiblePermit in self.__EMS6DataAccess.getEMS6ExtensiblePermit():
				self.__EMS7DataAccessInsert.addExtensiblePermitToEMS7(curExtensiblePermit, Action)

	def migrateSMSTransactionLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import SMSTransactionLog --> y/n   : ")
			if(input=='y'):
				for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6SMSTransactionLog():
					self.__EMS7DataAccessInsert.addSMSTransactionLogToEMS7(curSMSTransactionLog,Action)
		else:
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6SMSTransactionLog():
				self.__EMS7DataAccessInsert.addSMSTransactionLogToEMS7(curSMSTransactionLog,Action)

	def migrateSMSAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import SMSAlert --> y/n ")
			if(input=='y'):
				for curSMSAlert in self.__EMS6DataAccess.getEMS6SMSAlert():
					self.__EMS7DataAccessInsert.addSMSAlertToEMS7(curSMSAlert,Action)
		else:
			for curSMSAlert in self.__EMS6DataAccess.getEMS6SMSAlert():
				self.__EMS7DataAccessInsert.addSMSAlertToEMS7(curSMSAlert,Action)

	def migrateSMSFailedResponse(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import SMSFailedResponse --> y/n")
			if(input=='y'):
				for curSMSFailedReponse in self.__EMS6DataAccess.getEMS6SMSFailedResponse():
					self.__EMS7DataAccessInsert.addEMS6SMSFailedResponseToEMS7(curSMSFailedReponse, Action)

		else:
			for curSMSFailedReponse in self.__EMS6DataAccess.getEMS6SMSFailedResponse():
				self.__EMS7DataAccessInsert.addEMS6SMSFailedResponseToEMS7(curSMSFailedReponse, Action)
		
	def migrateModemSettings(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import ModemSetting --> y/n ")
			if (input=='y'):
				for curModemSetting in self.__EMS6DataAccess.getEMS6ModemSettings():
					self.__EMS7DataAccessInsert.addModemSettingToEMS7(curModemSetting,Action)
		else:
			for curModemSetting in self.__EMS6DataAccess.getEMS6ModemSettings():
				self.__EMS7DataAccessInsert.addModemSettingToEMS7(curModemSetting,Action)

	def migrateLotSettingContent(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Lot Setting File Content --> y/n")
			if (input=='y'):
				for curLotSettingContent in self.__EMS6DataAccess.getEMS6LotSettingContent():
					self.__EMS7DataAccessInsert.addEMS6LotSettingContentToEMS7(curLotSettingContent,Action)
		else:
			for curLotSettingContent in self.__EMS6DataAccess.getEMS6LotSettingContent():
				self.__EMS7DataAccessInsert.addEMS6LotSettingContentToEMS7(curLotSettingContent,Action)

	def migrateCryptoKey(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import CryptoKey --> y/n      :")
			if(input=='y'):
				for curCryptoKey in self.__EMS6DataAccess.getEMS6CryptoKey():
					self.__EMS7DataAccessInsert.addCryptoKeyToEMS7(curCryptoKey,Action)
		else:
			for curCryptoKey in self.__EMS6DataAccess.getEMS6CryptoKey():
				self.__EMS7DataAccessInsert.addCryptoKeyToEMS7(curCryptoKey,Action)

	def migrateRestLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import RestLog table --> y/n  ")
			if(input=='y'):
				for curRestLog in self.__EMS6DataAccess.getEMS6RestLogs():
					self.__EMS7DataAccessInsert.addRestLogToEMS7(curRestLog, Action)
		else:
			for curRestLog in self.__EMS6DataAccess.getEMS6RestLogs():
				self.__EMS7DataAccessInsert.addRestLogToEMS7(curRestLog, Action)

	def migrateRestSessionToken(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import RestSessionToken --> y/n  ")
			if (input=='y') :
				for curRestSessionToken in self.__EMS6DataAccess.getEMS6RestSessionToken():
					self.__EMS7DataAccessInsert.addRestSessionToken(curRestSessionToken, Action)
		else:
			for curRestSessionToken in self.__EMS6DataAccess.getEMS6RestSessionToken():
				self.__EMS7DataAccessInsert.addRestSessionToken(curRestSessionToken, Action)

	def migrateRestLogTotalCall(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import RestLogTotalCall --> y/n  ")
			if (input=='y') :
				for curRestLogTotalCall in self.__EMS6DataAccess.getEMS6RestLogTotalCall():
					self.__EMS7DataAccessInsert.addRestLogTotalCall(curRestLogTotalCall,Action)
		else:
			for curRestLogTotalCall in self.__EMS6DataAccess.getEMS6RestLogTotalCall():
				self.__EMS7DataAccessInsert.addRestLogTotalCall(curRestLogTotalCall,Action)

#	def migrateCustomerWsCal(self):
#		if(self.__verbose):
#			input = raw_input(" import CustomerWsCal --> y/n  ")
#			if (input=='y') :
#				for curCustomerWsCal in self.__EMS6DataAccess.getEMS6CustomerWsCal():
#					self.__EMS7DataAccessInsert.addCustomerWsCal(curCustomerWsCal)
#		else:
#			for curCustomerWsCal in self.__EMS6DataAccess.getEMS6CustomerWsCal():
#				self.__EMS7DataAccessInsert.addCustomerWsCal(curCustomerWsCal)

	def migrateCustomerWsToken(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import CustomerWsToken --> y/n  ")
			if (input=='y') :
				for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
					self.__EMS7DataAccessInsert.addCustomerWsTokenToEMS7(curCustomerWsToken, Action)
		else:
			for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
				self.__EMS7DataAccessInsert.addCustomerWsTokenToEMS7(curCustomerWsToken, Action)

# I have left this section as is because this section is referencing the EMS6 and EMS7 Cursor within the same module. It looks like this table is not going to change much from initial migration.
	def migrateCustomerAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import Customer Alert --> y/n")		
			if(input=='y'):
				for curCustomerAlert in self.__EMS6DataAccess.getEMS6CustomerAlert():
					self.__EMS7DataAccessInsert.addCustomerAlertToEMS7(curCustomerAlert, Action)
		else:
			for curCustomerAlert in self.__EMS6DataAccess.getEMS6CustomerAlert():
				self.__EMS7DataAccessInsert.addCustomerAlertToEMS7(curCustomerAlert, Action)

	# JIRA 4757 Customer Emails not to be migrated Dec 3
	#def migratePaystationAlertEmail(self):
	#	Action='Insert'
	#	if(self.__verbose):
	#		input = raw_input("import Paystation Alert Email into CustomerAlertEmail --> y/n")
	#		if(input=='y'):
	#			for curPaystationAlertEmail in self.__EMS6DataAccess.getEMS6Paystation():
	#				self.__EMS7DataAccessInsert.addPaystationAlertEmailFromPaystation(curPaystationAlertEmail, Action)
	#	else:
	#		for curPaystationAlertEmail in self.__EMS6DataAccess.getEMS6Paystation():
	#			self.__EMS7DataAccessInsert.addPaystationAlertEmailFromPaystation(curPaystationAlertEmail, Action)

	def migrateCustomerSubscription(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customer Subscription --> y/n    :")
			if (input == 'y'):
				for curCustomerSubscription in self.__EMS6DataAccess.getEMS6CustomerSubscription():
					self.__EMS7DataAccessInsert.addCustomerSubscriptionToEMS7(curCustomerSubscription,Action)
		else:
			for curCustomerSubscription in self.__EMS6DataAccess.getEMS6CustomerSubscription():
				self.__EMS7DataAccessInsert.addCustomerSubscriptionToEMS7(curCustomerSubscription,Action)

	def migrateCustomerWebServiceCal(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customer Web Service Cal --> y/n    :")
			if (input == 'y'):
				for curCustomerWebServiceCal in self.__EMS6DataAccess.getEMS6CustomerWebServiceCal():
					self.__EMS7DataAccessInsert.addCustomerWebServiceCalToEMS7(curCustomerWebServiceCal, Action)
		else:
			for curCustomerWebServiceCal in self.__EMS6DataAccess.getEMS6CustomerWebServiceCal():
				self.__EMS7DataAccessInsert.addCustomerWebServiceCalToEMS7(curCustomerWebServiceCal, Action)

#	def migrateCustomer(self,CustomerWsToken):
#		if(self.__verbose):
#			input = raw_input("Import Customer WebService Token --> y/n    :")
#			if (input == 'y'):
#				for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
#					self.__EMS7DataAccessInsert.addCustomerWsTokenToEMS7(curCustomerWsToken)
#		else:
#			for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
#				self.__EMS7DataAccessInsert.addCustomerWsTokenToEMS7(curCustomerWsToken)

	def migrateRates(self):
		Action='Insert'
		if(self.__verbose):
			input  = raw_input("Import Rates --> y/n      :")
			if (input =='y'):
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromRates():
					for curRates in self.__EMS6DataAccess.getEMS6Rates(PaystationId):
						self.__EMS7DataAccessInsert.addRatesToEMS7(curRates,Action)
		else:
			for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromRates():
				for curRates in self.__EMS6DataAccess.getEMS6Rates(PaystationId):
					self.__EMS7DataAccessInsert.addRatesToEMS7(curRates,Action)

	def migrateReplenish(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import Replenish --> y/n   :")
			if (input == 'y'):
				for curReplenish in self.__EMS6DataAccess.getEMS6Replenish():
					self.__EMS7DataAccessInsert.addReplenishToEMS7(curReplenish, Action)
		else:
			for curReplenish in self.__EMS6DataAccess.getEMS6Replenish():
				self.__EMS7DataAccessInsert.addReplenishToEMS7(curReplenish, Action)

	def migrateCardRetryTransaction(self):
		Action='Insert'
		if(self.__verbose):
			input= raw_input("Import Card Retry Transaction --> y/n   :")
			if (input == 'y'):
				for curCardRetryTransaction in self.__EMS6DataAccess.getEMS6CardRetryTransaction():
					self.__EMS7DataAccessInsert.addCardRetryTransactionToEMS7(curCardRetryTransaction,Action)
		else:
			for curCardRetryTransaction in self.__EMS6DataAccess.getEMS6CardRetryTransaction():
				self.__EMS7DataAccessInsert.addCardRetryTransactionToEMS7(curCardRetryTransaction, Action)


	def migrateCustomerCardType(self):
		Action='Insert'
		if(self.__verbose):
			input  = raw_input(" import Customer Card Type --> y/n   :")
			if (input == 'y'):
				for curCustomerCardType in self.__EMS6DataAccess.getEMS6CardType():
					self.__EMS7DataAccessInsert.addCustomerCardTypeToEMS7(curCustomerCardType, Action)
		else:
			for curCustomerCardType in self.__EMS6DataAccess.getEMS6CardType():
				self.__EMS7DataAccessInsert.addCustomerCardTypeToEMS7(curCustomerCardType, Action)

	def migratePaystationGroup(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PaystationGroup --> y/n   :")
			if(input=='y'):
				for curPaystationGroup in self.__EMS6DataAccess.getEMS6PaystationGroup():
					self.__EMS7DataAccessInsert.addPaystationGroupToEMS7(curPaystationGroup, Action)
		else:
			for curPaystationGroup in self.__EMS6DataAccess.getEMS6PaystationGroup():
				self.__EMS7DataAccessInsert.addPaystationGroupToEMS7(curPaystationGroup, Action)

	def migratePOSRoute(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import POS Route  --> y/n :")
			if(input == 'y'):
				for curPOSRoute in self.__EMS6DataAccess.getEMS6Paystation_PaystationGroup():
					self.__EMS7DataAccessInsert.addPOSRouteToEMS7(curPOSRoute,Action)
		else:
			for curPOSRoute in self.__EMS6DataAccess.getEMS6Paystation_PaystationGroup():
				self.__EMS7DataAccessInsert.addPOSRouteToEMS7(curPOSRoute,Action)

	def migrateCollectionType(self):
		if(self.__verbose):
			input = raw_input("import CollectionType --> y/n   :")
			if(input=='y'):
				for curCollectionType in self.__EMS6DataAccess.getEMS6CollectionType():
					self.__EMS7DataAccessInsert.addCollectionTypeToEMS7(curCollectionType)
		else:
			for curCollectionType in self.__EMS6DataAccess.getEMS6CollectionType():
				self.__EMS7DataAccessInsert.addCollectionTypeToEMS7(curCollectionType)

	def migrateBatteryInfo(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import BatteryInfo --> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationForBatteryInfo():
					for curBatteryInfo in self.__EMS6DataAccess.getBatteryInfo(curPaystationId):
						self.__EMS7DataAccessInsert.addBatteryInfoToEMS7(curBatteryInfo, Action)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationForBatteryInfo():
				for curBatteryInfo in self.__EMS6DataAccess.getBatteryInfo(curPaystationId):
					self.__EMS7DataAccessInsert.addBatteryInfoToEMS7(curBatteryInfo, Action)

	def migrateValueCardData(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
					self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)

	def migrateSmartCardData(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Smart Card Data --> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curSCardData in self.__EMS6DataAccess.getEMS6SmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addSmartCardToEMS7(curSCardData, Action)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curSCardData in self.__EMS6DataAccess.getEMS6SmartCardType(curPaystationId):
					self.__EMS7DataAccessInsert.addSmartCardToEMS7(curSCardData, Action)

	def migrateProcessorTransaction(self,ClusterId,ProcessorTransaction):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import ProcessorTransaction --> y/n   :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromProcessorTransaction():
					ProcessorTransactions = self.__EMS6DataAccess.getProcessorTransaction(curPaystationId)
					self.__EMS7DataAccessInsert.batchAddProcessorTransaction(ProcessorTransactions, Action)
					#for curProcessorTransaction in self.__EMS6DataAccess.getProcessorTransaction(curPaystationId):
					#	self.__EMS7DataAccessInsert.addProcessorTransaction(curProcessorTransaction)
		else:
			print "in else part"
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,ProcessorTransaction):
				print DateRange
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromProcessorTransaction():
					ProcessorTransactions = self.__EMS6DataAccess.getProcessorTransaction(curPaystationId,DateRange)
					self.__EMS7DataAccessInsert.addProcessorTransaction(ProcessorTransactions,Action)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)
				
				#self.__EMS7DataAccessInsert.batchAddProcessorTransaction(ProcessorTransactions,Action)
				#self.__EMS7DataAccessInsert.addProcessorTransaction(curProcessorTransaction,Action)
				# Ashok: why the below two lines were commented out ? I am enabling them and added Action parameter to addProcessorTransaction below
				# Ashok Commenting the two lines as batchAdd happening above
				#for curProcessorTransaction in self.__EMS6DataAccess.getProcessorTransaction(curPaystationId):
					#self.__EMS7DataAccessInsert.addProcessorTransaction(curProcessorTransaction,Action)

	def migratePurchase(self,ClusterId,Purchase):
		self.__EMS7DataAccessInsert.DisablePurchaseKeys()
		if(self.__verbose):
		     input = raw_input ("import Purchase --> y/n    :")
		     if (input=='y'):
			     for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				     purchases = self.__EMS6DataAccess.getEMS6Transaction(curPaystationId)
				     self.__EMS7DataAccessInsert.batchAddPurchaseToEMS7(purchases)
		#                                       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
	#                                               self.__EMS7DataAccessInsert.addPurchaseToEMS7(curTransaction)
		else:
			print "In MigratePurchase"
			
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,Purchase):
				print DateRange
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					purchases = self.__EMS6DataAccess.getEMS6Transaction(curPaystationId,DateRange)
					self.__EMS7DataAccessInsert.batchAddPurchaseToEMS7(purchases)
					
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)

		self.__EMS7DataAccessInsert.EnablePurchaseKeys()
			#                                       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#                                               self.__EMS7DataAccessInsert.addPurchaseToEMS7(curTransaction)

        #        for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
         #       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
          #      self.__EMS7DataAccessInsert.addPurchaseToEMS7(curTransaction)

# 	def migratePurchase(self):
#		if(self.__verbose):
#			input = raw_input ("import Purchase --> y/n    :")
#			if (input=='y'):
#				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
#					for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#						self.__EMS7DataAccessInsert.batchAddPurchaseToEMS7(curTransaction)
#					for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#						self.__EMS7DataAccessInsert.addPurchaseToEMS7(curTransaction)
#		else:
#			print" are you here"
#			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
#				print " curPaystationId %s" %curPaystationId
#				for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#					print "curTransaction %s" %curTransaction[1]
#					print "curTransaction %s" %curTransaction[2]
#					print "curTransaction %s" %curTransaction[3]
#					self.__EMS7DataAccessInsert.batchAddPurchaseToEMS7(curTransaction)

	def migrateCustomerEmail(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import CustomerAlertEmail --> y/n   :")
			if(input=='y'):
				for curCustomerEmail in self.__EMS6DataAccess.getCustomerEmail():
					self.__EMS7DataAccessInsert.addCustomerEmail(curCustomerEmail,Action)
		else:
			for curCustomerEmail in self.__EMS6DataAccess.getCustomerEmail():
				self.__EMS7DataAccessInsert.addCustomerEmail(curCustomerEmail,Action)

# Migrate getCustomerEmailPaystation into EMS7

	def migrateProcessorProperties(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import ProcessorProperties --> y/n    :")
			if(input=='y'):
				for curProcessorProperties in self.__EMS6DataAccess.getProcessorProperties():
					self.__EMS7DataAccessInsert.addProcessorProperties(curProcessorProperties, Action)
		else:
			for curProcessorProperties in self.__EMS6DataAccess.getProcessorProperties():
				self.__EMS7DataAccessInsert.addProcessorProperties(curProcessorProperties, Action)

	def migrateMerchantAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import MerchantAccount --> y/n    :")
			if(input=='y'):
				for curMerchantAccount in self.__EMS6DataAccess.getMerchantAccount():
					self.__EMS7DataAccessInsert.addMerchantAccount(curMerchantAccount, Action)
		else:
			for curMerchantAccount in self.__EMS6DataAccess.getMerchantAccount():
				self.__EMS7DataAccessInsert.addMerchantAccount(curMerchantAccount, Action)

	def migratePreAuth(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import PreAuth --> y/n     :")
			if(input=='y'):
				for curPreAuth in self.__EMS6DataAccess.getPreAuth():
					self.__EMS7DataAccessInsert.addPreAuth(curPreAuth, Action)
		else:
			for curPreAuth in self.__EMS6DataAccess.getPreAuth():
				self.__EMS7DataAccessInsert.addPreAuth(curPreAuth, Action)

	def migratePreAuthHolding(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import PreAuthHolding --> y/n    :")
			if(input=='y'):
				for curPreAuthHolding in self.__EMS6DataAccess.getPreAuthHolding():
					self.__EMS7DataAccessInsert.addPreAuthHolding(curPreAuthHolding, Action)
		else:
			for curPreAuthHolding in self.__EMS6DataAccess.getPreAuthHolding():
				self.__EMS7DataAccessInsert.addPreAuthHolding(curPreAuthHolding, Action)

		
	def migratePermit(self,ClusterId,Permit):
		if(self.__verbose):
			input = raw_input ("import Permit --> y/n   :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					Transactions = self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId)
					self.__EMS7DataAccessInsert.batchAddPermitToEMS7(Transactions)
					#for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId):
					#	self.__EMS7DataAccessInsert.addPermitToEMS7(curTransaction)
		else:
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,Permit):
				print DateRange
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					Transactions = self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId,DateRange)
					self.__EMS7DataAccessInsert.batchAddPermitToEMS7(Transactions)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)
				#for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId):
				#	self.__EMS7DataAccessInsert.addPermitToEMS7(curTransaction)
	
	def migratePaymentCard(self,ClusterId,PaymentCard):
		Action='Insert'	
		if(self.__verbose):
			input = raw_input ("import Payment -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					Transactions = self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId)
					self.__EMS7DataAccessInsert.batchAddPaymentCardToEMS7(Transactions)
				#	for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId):
				#		self.__EMS7DataAccessInsert.addPaymentCardToEMS7(curTransaction)
		else:
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,PaymentCard):
				print DateRange
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				#Transactions = self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId)
				#self.__EMS7DataAccessInsert.batchAddPaymentCardToEMS7(Transactions)
					for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId,DateRange):
						self.__EMS7DataAccessInsert.addPaymentCardToEMS7(curTransaction,Action)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)

	def migrateUserAccount(self):	
		Action='Insert'	
		if(self.__verbose):
			input = raw_input ("import UserAccount --> y/n  :")
			if (input=='y'):
				for curUserAccount in self.__EMS6DataAccess.getEMS6UserAccount():
					self.__EMS7DataAccessInsert.addUserAccountToEMS7(curUserAccount, Action)
		else:
			for curUserAccount in self.__EMS6DataAccess.getEMS6UserAccount():
				self.__EMS7DataAccessInsert.addUserAccountToEMS7(curUserAccount, Action)

	def migrateEventLogNewIntoPOSAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import EventNew Log into POS Alert --> y/n :")
			if (input == 'y'):
				for curPaystationId in  self.__EMS6DataAccess.getDistinctPaystationIdForEventLogNew():
					for curPOSAlert in self.__EMS6DataAccess.getEventLogNew(curPaystationId):
						self.__EMS7DataAccessInsert.addPOSAlertToEMS7(curPOSAlert,Action)
		else:
			for curPaystationId in  self.__EMS6DataAccess.getDistinctPaystationIdForEventLogNew():
				for curPOSAlert in self.__EMS6DataAccess.getEventLogNew(curPaystationId):
					self.__EMS7DataAccessInsert.addPOSAlertToEMS7(curPOSAlert,Action)
		
	def migrateTrialCustomer(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Trial Customer  --> y/n :")
			if(input == 'y'):
				for curtrialcustomer in self.__EMS6DataAccess.getEMS6CerberusTrialCustomer():
					self.__EMS7DataAccessInsert.addTrialCustomerToEMS7(curtrialcustomer, Action)
		else:
			print " Ashok: Inside function migrateTrialCustomer"
			for curtrialcustomer in self.__EMS6DataAccess.getEMS6CerberusTrialCustomer():
					self.__EMS7DataAccessInsert.addTrialCustomerToEMS7(curtrialcustomer, Action)

	def migrateRegionPaystationLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Region Paystation Log --> y/n :")
			if(input == 'y'):
				for curRegionPaystationLog in self.__EMS6DataAccess.getEMS6RegionPaystationLog():
					self.__EMS7DataAccessInsert.addLocationPOSLogToEMS7(curRegionPaystationLog, Action)
		else:
			for curRegionPaystationLog in self.__EMS6DataAccess.getEMS6RegionPaystationLog():
				self.__EMS7DataAccessInsert.addLocationPOSLogToEMS7(curRegionPaystationLog, Action)

	def migrateCollectionLock(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Collection Lock --> y/n :")
			if(input == 'y'):
				for curCollectionLock in self.__EMS6DataAccess.getEMS6CollectionLock():
					self.__EMS7DataAccessInsert.addCollectionLockToEMS7(curCollectionLock, Action)
		else:
			for curCollectionLock in self.__EMS6DataAccess.getEMS6CollectionLock():
				self.__EMS7DataAccessInsert.addCollectionLockToEMS7(curCollectionLock, Action)

	def migratePaystationBalance(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PaystationBalance --> y/n  :")
			if(input == 'y'):
				for curPaystationBalance in self.__EMS6DataAccess.getEMS6PaystationBalance():
					self.__EMS7DataAccessInsert.addPOSBalanceToEMS7(curPaystationBalance, Action)
		else:
			for curPaystationBalance in self.__EMS6DataAccess.getEMS6PaystationBalance():
				self.__EMS7DataAccessInsert.addPOSBalanceToEMS7(curPaystationBalance, Action)

	
	def migrateClusterMember(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import ClusterMember  --> y/n :")
			if(input == 'y'):
				for curClusterMember in self.__EMS6DataAccess.getEMS6ClusterMember():
					self.__EMS7DataAccessInsert.addClusterToEMS7(curClusterMember, Action)
		else:
			for curClusterMember in self.__EMS6DataAccess.getEMS6ClusterMember():
				self.__EMS7DataAccessInsert.addClusterToEMS7(curClusterMember, Action)

	def migratePOSHeartBeat(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import  Paystation Heart Beat --> y/n :")
			if(input == 'y'):
				for curPOSHeartBeat in self.__EMS6DataAccess.getEMS6PaystationHeartBeat():
					self.__EMS7DataAccessInsert.addPOSHeartBeatToEMS7(curPOSHeartBeat, Action)
		else:
			for curPOSHeartBeat in self.__EMS6DataAccess.getEMS6PaystationHeartBeat():
				self.__EMS7DataAccessInsert.addPOSHeartBeatToEMS7(curPOSHeartBeat, Action)

	def migratePOSServiceState(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import  Service State --> y/n :")
			if(input == 'y'):
				for curPOSServiceState in self.__EMS6DataAccess.getEMS6ServiceState():
					self.__EMS7DataAccessInsert.addPOSServiceStateToEMS7(curPOSServiceState, Action)
		else:
			for curPOSServiceState in self.__EMS6DataAccess.getEMS6ServiceState():
				self.__EMS7DataAccessInsert.addPOSServiceStateToEMS7(curPOSServiceState, Action)

	def migrateActivePOSAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import ActivePOSAlert  --> y/n :")
			if(input == 'y'):
				for curActivePOSAlert in self.__EMS6DataAccess.getEMS6PaystationAlert():
					self.__EMS7DataAccessInsert.addActivePOSAlertToEMS7(curActivePOSAlert, Action)
				self.__EMS7DataAccessInsert.addActivePOSAlertForPaytatationAlert()
				self.__EMS7DataAccessInsert.addActivePOSAlertForUserDefinedAlert()
		else:
			for curActivePOSAlert in self.__EMS6DataAccess.getEMS6PaystationAlert():
				self.__EMS7DataAccessInsert.addActivePOSAlertToEMS7(curActivePOSAlert, Action)
			self.__EMS7DataAccessInsert.addActivePOSAlertForPaytatationAlert()
			#self.__EMS7DataAccessInsert.addActivePOSAlertForUserDefinedAlert() 

	def migratePOSDateBilling(self):
		if(self.__verbose):
			input = raw_input ("import POSDate Billing -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curBillingReport in self.__EMS6DataAccess.getEMS6BillingReport(curPaystationId):
						self.__EMS7DataAccessInsert.addPOSDateBillingReportToEMS7(curBillingReport)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curBillingReport in self.__EMS6DataAccess.getEMS6BillingReport(curPaystationId):
					self.__EMS7DataAccessInsert.addPOSDateBillingReportToEMS7(curBillingReport)


	def migratePOSDateAuditAccess(self):
		if(self.__verbose):
			input = raw_input ("import POSDate AuditAccess -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curAuditAccess in self.__EMS6DataAccess.getEMS6AuditAccess(curPaystationId):
						self.__EMS7DataAccessInsert.addPOSDateAuditAccessToEMS7(curAuditAccess)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curAuditAccess in self.__EMS6DataAccess.getEMS6AuditAccess(curPaystationId):
					self.__EMS7DataAccessInsert.addPOSDateAuditAccessToEMS7(curAuditAccess)

	def migratePOSDatePaystation(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Paystation Properties into POSDate --> y/n :")
			if (input=='y'):
				for curPaystation in self.__EMS6DataAccess.getEMS6PaystationForPOSDate():
					self.__EMS7DataAccessInsert.addPOSDatePaystationPropertiesToEMS7(curPaystation, Action)
		else:
			for curPaystation in self.__EMS6DataAccess.getEMS6PaystationForPOSDate():
				self.__EMS7DataAccessInsert.addPOSDatePaystationPropertiesToEMS7(curPaystation, Action)
	

	def migrateBadValueCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Value Card --> y/n :")
			if (input=='y'):
				for curBadValueCard in self.__EMS6DataAccess.getEMS6BadValueCard():
					self.__EMS7DataAccessInsert.addBadValueCardToEMS7(curBadValueCard, Action)
		else:
			for curBadValueCard in self.__EMS6DataAccess.getEMS6BadValueCard():
				self.__EMS7DataAccessInsert.addBadValueCardToEMS7(curBadValueCard, Action)

	def migrateBadCreditCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Credit Card --> y/n :")
			if (input=='y'):
				for curBadCreditCard in self.__EMS6DataAccess.getEMS6BadCreditCard():
					self.__EMS7DataAccessInsert.addBadCreditCardToEMS7(curBadCreditCard, Action)
		else:
			for curBadCreditCard in self.__EMS6DataAccess.getEMS6BadCreditCard():
				self.__EMS7DataAccessInsert.addBadCreditCardToEMS7(curBadCreditCard, Action)

	def migrateBadSmartCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Smart Card --> y/n :")
			if (input=='y'):
				for curBadSmartCard in self.__EMS6DataAccess.getEMS6BadSmartCard():
					self.__EMS7DataAccessInsert.addBadSmartCardToEMS7(curBadSmartCard, Action)
		else:
			for curBadSmartCard in self.__EMS6DataAccess.getEMS6BadSmartCard():
				self.__EMS7DataAccessInsert.addBadSmartCardToEMS7(curBadSmartCard, Action)

	def migrateParentRegionIdToLocation(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Add Parent Region to Location  --> y/n :")
			if (input=='y'):
				for curParentRegion in self.__EMS6DataAccess.getEMS6Locations():
					self.__EMS7DataAccessInsert.addParentRegionIdToLocation(curParentRegion, Action)
		else:
			for curParentRegion in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccessInsert.addParentRegionIdToLocation(curParentRegion, Action)

	def migrateCCFailLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import CCFailLog  --> y/n :")
			if (input=='y'):
				for curCCFailLog in self.__EMS6DataAccess.getEMS6CCFailLog():
					self.__EMS7DataAccessInsert.addCCFailLogToEMS7(curCCFailLog, Action)
		else:
			for curCCFailLog in self.__EMS6DataAccess.getEMS6CCFailLog():
				self.__EMS7DataAccessInsert.addCCFailLogToEMS7(curCCFailLog, Action)

	def migrateServiceAgreement(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ServiceAgreement  --> y/n :")
			if (input=='y'):
				for curServiceAgreement in self.__EMS6DataAccess.getEMS6ServiceAgreement():
					self.__EMS7DataAccessInsert.addServiceAgreementToEMS7(curServiceAgreement,Action)
		else:
			for curServiceAgreement in self.__EMS6DataAccess.getEMS6ServiceAgreement():
				self.__EMS7DataAccessInsert.addServiceAgreementToEMS7(curServiceAgreement,Action)

	def migrateServiceAgreedCustomer(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Service Agreed Customer --> y/n")
			if(input=='y'):
				for curServiceAgreedCustomer in self.__EMS6DataAccess.getEMS6ServiceAgreedCustomer():
					self.__EMS7DataAccessInsert.addCustomerAgreementToEMS7(curServiceAgreedCustomer, Action)
		else:
			for curServiceAgreedCustomer in self.__EMS6DataAccess.getEMS6ServiceAgreedCustomer():
				self.__EMS7DataAccessInsert.addCustomerAgreementToEMS7(curServiceAgreedCustomer, Action)

	def migrateImportError(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Error --> y/n")
			if(input=='y'):
				for curImportError in self.__EMS6DataAccess.getEMS6ImportError():
					self.__EMS7DataAccessInsert.addImportError(curImportError, Action)
		else:
			for curImportError in self.__EMS6DataAccess.getEMS6ImportError():
				self.__EMS7DataAccessInsert.addImportError(curImportError, Action)


	def migrateRawSensorData(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Raw Sensor Data --> y/n")
			if(input=='y'):
				for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
					self.__EMS7DataAccessInsert.addRawSensorDataToEMS7(curRawSensorData, Action)
		else:
			for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
				self.__EMS7DataAccessInsert.addRawSensorDataToEMS7(curRawSensorData, Action)
	

	def migrateRawSensorDataArchive(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Raw Sensor Data Archive --> y/n")
			if(input=='y'):
				for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
					self.__EMS7DataAccessInsert.addRawSensorDataArchiveToEMS7(curRawSensorData, Action)
		else:
			for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
				self.__EMS7DataAccessInsert.addRawSensorDataArchiveToEMS7(curRawSensorData, Action)

	def migrateReversal(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Reversal --> y/n")
			if(input=='y'):
				for curReversal in self.__EMS6DataAccess.getEMS6Reversal():
					self.__EMS7DataAccessInsert.addReversalToEMS7(curReversal, Action)
		else:
			for curReversal in self.__EMS6DataAccess.getEMS6Reversal():
				self.__EMS7DataAccessInsert.addReversalToEMS7(curReversal, Action)

	def migrateReversalArchive(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Reversal Archive --> y/n")
			if(input=='y'):
				for curReversalArchive in self.__EMS6DataAccess.getEMS6ReversalArchive():
					self.__EMS7DataAccessInsert.addReversalArchiveToEMS7(curReversalArchive, Action)
		else:
			for curReversalArchive in self.__EMS6DataAccess.getEMS6ReversalArchive():
				self.__EMS7DataAccessInsert.addReversalArchiveToEMS7(curReversalArchive, Action)
					
# This table is not needed as it is being used by IntellaPay
	def migrateSCRetry(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import SCRetry --> y/n")
			if(input=='y'):
				for curSCRetry in self.__EMS6DataAccess.getEMS6SCRetry():
					self.__EMS7DataAccessInsert.addSCRetryToEMS7(curSCRetry, Action)
		else:
			for curSCRetry in self.__EMS6DataAccess.getEMS6SCRetry():
				self.__EMS7DataAccessInsert.addSCRetryToEMS7(curSCRetry, Action)

	def migrateThirdPartyServiceAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdAprtyServiceAccount --> y/n")
			if(input=='y'):
				for curThirdPartyServiceAccount in self.__EMS6DataAccess.getEMS6ThirdPartyServiceAccount():
					self.__EMS7DataAccessInsert.addThirdPartyServiceAccountToEMS7(curThirdPartyServiceAccount, Action)
		else:
			for curThirdPartyServiceAccount in self.__EMS6DataAccess.getEMS6ThirdPartyServiceAccount():
				self.__EMS7DataAccessInsert.addThirdPartyServiceAccountToEMS7(curThirdPartyServiceAccount, Action)
	
	def migrateTransaction2Push(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Transaction2Push --> y/n")
			if(input=='y'):
				for curTransaction2Push in self.__EMS6DataAccess.getEMS6Transaction2Push():
					self.__EMS7DataAccessInsert.addTransaction2PushToEMS7(curTransaction2Push, Action)
		else:
			for curTransaction2Push in self.__EMS6DataAccess.getEMS6Transaction2Push():
				self.__EMS7DataAccessInsert.addTransaction2PushToEMS7(curTransaction2Push, Action)

	def migrateTransactionPush(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import TransactionPush --> y/n")
			if(input=='y'):
				for curTransactionPush in self.__EMS6DataAccess.getEMS6TransactionPush():
					self.__EMS7DataAccessInsert.addTransactionPushToEMS7(curTransactionPush, Action)
		else:
			for curTransactionPush in self.__EMS6DataAccess.getEMS6TransactionPush():
				self.__EMS7DataAccessInsert.addTransactionPushToEMS7(curTransactionPush, Action)

	def migrateThirdPartyPOSSequence(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyPOSSequence --> y/n")
			if(input=='y'):
				for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
					self.__EMS7DataAccessInsert.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)
		else:
			for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
				self.__EMS7DataAccessInsert.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)

	def migrateThirdPartyPushLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyPushLog --> y/n")
			if(input=='y'):
				for curThirdPartyPushLog in self.__EMS6DataAccess.getEMS6ThirdPartyPushLog():
					self.__EMS7DataAccessInsert.addThirdPartyPushLogToEMS7(curThirdPartyPushLog, Action)

	def migrateThirdPartyServicePaystation(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyServicePaystation --> y/n")
			if(input=='y'):
				for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
					self.__EMS7DataAccessInsert.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)

	def migrateThirdPartyServiceStallSequence(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyServiceStallSequence --> y/n")
			if(input=='y'):
				for curThirdPartyServiceStallSequence in self.__EMS6DataAccess.getEMS6ThirdPartyServiceStallSequence():
					self.__EMS7DataAccessInsert.addThirdPartyServiceStallSequenceToEMS7(curThirdPartyServiceStallSequence, Action)

	def migratePOSCollection(self,ClusterId,POSCollection):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import POSCollection --> y/n :")
			if (input=='y'):
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationFromAudit():
					for curPOSCollection in self.__EMS6DataAccess.getEMS6Audit(PaystationId):
						self.__EMS7DataAccessInsert.addPOSCollectionToEMS7(curPOSCollection, Action)
		else:
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,POSCollection):
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationFromAudit():
					for curPOSCollection in self.__EMS6DataAccess.getEMS6Audit(PaystationId,DateRange):
						self.__EMS7DataAccessInsert.addPOSCollectionToEMS7(curPOSCollection, Action)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)
				
	def migratePOSStatus(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import POSStatus --> y/n :")
			if (input=='y'):
				for status in self.__EMS6DataAccess.getPOSDateForPOSStatus():
					self.__EMS7DataAccessInsert.addPOSStatusToEMS7(status, Action)
		else:
			print "in else pos"
			for status in self.__EMS7DataAccessInsert.getPOSDateForPOSStatus():
				self.__EMS7DataAccessInsert.addPOSStatusToEMS7(status, Action)

	def migratePurchaseCollection(self,ClusterId,PurchaseCollection):
		if(self.__verbose):
			input = raw_input("import PurchaseCollection --> y/n :")
			if(input=='y'):
				self.__EMS7DataAccessInsert.addPurchaseCollection()
		else:
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,PurchaseCollection):
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				self.__EMS7DataAccessInsert.addPurchaseCollection(DateRange)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)


	def migratePurchaseTax(self,ClusterId,PurchaseTax):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PurchaseTax --> y/n :")
			if(input=='y'):
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdForTaxes():
					for curTaxes in self.__EMS6DataAccess.getTaxes(PaystationId):	
						self.__EMS7DataAccessInsert.addPurchaseTax(curTaxes, Action)
		else:
			for DateRange in self.__EMS7DataAccessInsert.getTransactionDateRange(ClusterId,PurchaseTax):
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupStartTime(DateRange)
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdForTaxes():
					for curTaxes in self.__EMS6DataAccess.getTaxes(PaystationId,DateRange):	
						self.__EMS7DataAccessInsert.addPurchaseTax(curTaxes, Action)
				self.__EMS7DataAccessInsert.UpdateMigrationDateLookupEndTime(DateRange)

	def migrateWebServiceCallLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import WebServiceCallLog --> y/n")
			if(input=='y'):
				for curTransaction2Push in self.__EMS6DataAccess.getEMS6WsCallLog():
					self.__EMS7DataAccessInsert.addWebServiceCallLog(curTransaction2Push, Action)
		else:
			for curTransaction2Push in self.__EMS6DataAccess.getEMS6WsCallLog():
				self.__EMS7DataAccessInsert.addWebServiceCallLog(curTransaction2Push, Action)

	#Added on July 19
	def logModuleStartStatus(self,ClusterId,ModuleName):
		self.__EMS7DataAccessInsert.addLogModuleStartStatus(ClusterId,ModuleName)
		
	def logModuleEndStatus(self,ClusterId,ModuleName):
		self.__EMS7DataAccessInsert.addLogModuleEndStatus(ClusterId,ModuleName)
		
	#Added on Aug 14 ITF	
	def migrateValidCustomCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Valid Custom Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)
		else:
			for curVSCardData in self.__EMS6DataAccess.getEMS6ValidCustomCard():
				self.__EMS7DataAccessInsert.addCustomerCard(curVSCardData, Action)
				
				
				
	#Added on Aug 15 ITF	
	def migratePaystationIsActive(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Valid Custom Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)
		else:
			for curVSCardData in self.__EMS6DataAccess.getEMS6PaystationForActive():
				self.__EMS7DataAccessInsert.UpdatePOSStatusIsActive(curVSCardData, Action)			


	#Added on Aug 16 ITF	
	def migrateCustomerExpiry(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Valid Custom Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)
		else:
			for curCustomerData in self.__EMS6DataAccess.getEMS6CustomerExpiryCerberus():
				self.__EMS7DataAccessInsert.UpdateCustomerExpiry(curCustomerData, Action)			
				
	def migrateUnassignedLocations(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Valid Custom Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccessInsert.addValueCardToEMS7(curVSCardData, Action)
		else:
			for curUnassignedLocations in self.__EMS7DataAccessInsert.getEMS7UnassignedLocations():
				self.__EMS7DataAccessInsert.addUnassignedLocationDetails(curUnassignedLocations, Action)			
				
				