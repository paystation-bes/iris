	def addPaymentCardToEMS7(self, Transaction, Action):
		# If there is no corrosponding record in Processor Transaction tabl, then there is no need to insert into PaymmentCard table.
		tableName='PaymentCard'
		#		self.__printTransactionDataForPaymentCard(Transaction)
		CardTypeId=None
		CustomerCardId=None
		CustomerCardTypeId =None
		Last4DigitsOfCardNumber = 0
		EMS6PaystationId = Transaction[0]
		TicketNumber = Transaction[1]
		PurchasedDate = Transaction[2]
		PaymentTypeId = Transaction[3]
		CreditCardType = Transaction[4]
		# Make sure that you are looking up creditcardTypeid as the CreditCardType appears as a String?
		ProcessorTransactionTypeId = Transaction[5]
		EMS6MerchantAccountId = Transaction[6]
		Amount = Transaction[7]
		Last4DigitsOfCardNumber = Transaction[8]
		ProcessingDate = Transaction[9]
		IsUploadedFromBoss = Transaction[10]
		IsRFID = Transaction[11]
		Approved = Transaction[12]
		SmartCardData = Transaction[13]
		SmartCardPaid = Transaction[14]
		CustomCardData = Transaction[15]
		CustomCardType = Transaction[16]
		EMS6CustomerId = Transaction[17]
		print "PaymentTypeId is %s" %PaymentTypeId
		if(PaymentTypeId==2 or PaymentTypeId==5):
			CardTypeId=1
		elif(PaymentTypeId==0):
			CardTypeId=10
		elif(PaymentTypeId==4):
			CardTypeId=2
	#		print " Payment Type id SmartCard"
		elif(PaymentTypeId==6):
	#		print " Payment Type Id is Cash and SmartCard"
			CardTypeId=2
		elif(PaymentTypeId==7):
	#		print " Payment Type id is Value Card"
			CardTypeId=3
		elif(PaymentTypeId==9):
	#		print " Payment and value card"
			CardTypeId=3
		else:
			CardTypeId=0
		print "***The Card Type Id is %s" %CardTypeId
		# For smartcard and value card we are inserting, or retrieving a record from CustomerCard and CustomerCardType
		# Since majority of the transaction are credit card transactions, we are only dealing 10% of transactions with value, and smart card for those types , we will look up CustoemrCardId for value cards, and smart cards. CustomerCardId will always be null , There is one customer with Customer Id =3, that has 2 records in EMS6CustomerCardType table, so do not worry about it.
		if(CreditCardType=='VISA'):
			CreditCardTypeId=2
		elif(CreditCardType=='MasterCard'):
			CreditCardTypeId=3
		elif(CreditCardType=='AMEX'):
			CreditCardTypeId=4
		elif(CreditCardType=='Discover'):
			CreditCardTypeId=5
		elif(CreditCardType=='Diners Club'):
			CreditCardTypeId=6
		elif(CreditCardType=='CreditCard'):
			CreditCardTypeId=7
		elif(CreditCardType=='Other'):
			CreditCardTypeId=8
		else:
			CreditCardTypeId=0
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
		ProcessorTransactionId = self.__getProcessorTransactionId(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		# Ashok Aug 12 Jira 4100 Add CustomCard data to CustomerCard and get the ID
		#Step 1) Get the Id from CustomerCardType where CustomerId,CardTypeid,Name Limit 1
		#Step 2) If not found then Get the Id from CustomerCardType where CustomerId,CardTypeid=3 and IsLocked=1
		#Step 3) Get Id from CustomerCard
		#Step 4) If not found then Add
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId = %s and CardTypeId = %s and Name = %s limit 1",(EMS7CustomerId,CardTypeId,CustomCardType))
		if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				CustomerCardTypeId = row[0]
		else:
			self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId = %s and CardTypeId = %s and isLocked =1",(EMS7CustomerId,CardTypeId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				CustomerCardTypeId = row[0]
		
		if (CustomCardData<>None) and (CustomerCardTypeId<>None):
			self.__EMS7Cursor.execute("select Id from CustomerCard where CustomerCardTypeId = %s and CardNumber = %s",(CustomerCardTypeId,CustomCardData))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				CustomerCardId = row[0]
			else:
				# Ashok Added on Aug 22 Jira 4073
				self.__EMS7Cursor.execute("insert ignore into CustomerCard (CustomerCardTypeId,CardNumber,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId,IsDeleted) VALUES \
					(%s,%s,1,UTC_TIMESTAMP(),UTC_TIMESTAMP(),1,1)",(CustomerCardTypeId,CustomCardData))
				CustomerCardId=self.__EMS7Cursor.lastrowid
				self.__EMS7Connection.commit()
				
			
		if(PurchaseId and PaymentTypeId!=1):
			if(Action=='Insert'):
				#try: #Added try except block on July 15 by Sanjay
				#if (PaymentTypeId==4):
				#	Last4DigitsOfCardNumber = 0
				#	ProcessingDate = PurchasedDate
				#	IsUploadedFromBoss = 0
				#	IsRFID = 0
				#	Approved = 1
				#	Amount = SmartCardPaid
				print "Before Insert"
				Last4DigitsOfCardNumber = 0
				ProcessingDate = PurchasedDate
				IsUploadedFromBoss = 0
				IsRFID = 0
				Approved = 1
				
				self.__EMS7Cursor.execute("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,\
								MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved,CustomerCardId) \
								values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,\
								ProcessorTransactionId,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,ProcessingDate,IsUploadedFromBoss,IsRFID,Approved,CustomerCardId))
				EMS7PaymentCardId=self.__EMS7Cursor.lastrowid
				self.__EMS7Connection.commit()	
				#except:
				#	print "in Exception"
				#	IsMultiKey=0
				#	tableName='PaymentCard'
				#	self.__InsertIntoDiscardedRecords(PurchaseId,tableName,IsMultiKey)

			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			
