	def batchAddPaymentCardToEMS7(self, EMS6Transactions):
		# If there is no corrosponding record in Processor Transaction tabl, then there is no need to insert into PaymmentCard table.
		tableName='PaymentCard'
		for Transaction in EMS6Transactions:
			DiscardedRecords = []
	                EMS7Transactions = []
			#self.__printTransactionDataForPaymentCard(Transaction)
			CardTypeId=None
			EMS6PaystationId = Transaction[0]
			TicketNumber = Transaction[1]
			PurchasedDate = Transaction[2]
			PaymentTypeId = Transaction[3]
			CreditCardType = Transaction[4]
			# Make sure that you are looking up creditcardTypeid as the CreditCardType appears as a String?
			ProcessorTransactionTypeId = Transaction[5]
			EMS6MerchantAccountId = Transaction[6]
			Amount = Transaction[7]
			Last4DigitsOfCardNumber = Transaction[8]
			ProcessingDate = Transaction[9]
			IsUploadedFromBoss = Transaction[10]
			IsRFID = Transaction[11]
			Approved = Transaction[12]
			SmartCardData = Transaction[13]
			SmartCardPaid = Transaction[14]
			CustomCardData = Transaction[15]
			CustomCardType = Transaction[16]
			EMS6CustomerId = Transaction[17]
			if(PaymentTypeId==2 or PaymentTypeId==5):
				CardTypeId=1
			elif(PaymentTypeId==0):
				CardTypeId=10
			elif(PaymentTypeId==4):
				CardTypeId=2
				# " Payment Type id SmartCard"
			elif(PaymentTypeId==6):
				#" Payment Type Id is Cash + SmartCard"
				CardTypeId=2
			elif(PaymentTypeId==7):
				#print " Payment Type id is Value Card"
				CardTypeId=3
			elif(PaymentTypeId==9):
				#print " Payment and value card"
				CardTypeId=3
			# For smartcard and value card we are inserting, or retrieving a record from CustomerCard and CustomerCardType
			# Since majority of the transaction are credit card transactions, we are only dealing 10% of transactions with value, and smart card for those types , we will look up CustoemrCardId for value cards, and smart cards. CustomerCardId will always be null , There is one customer with Customer Id =3, that has 2 records in EMS6CustomerCardType table, so do not worry about it.
#			print " -- The Credit Card Type id is %s ************************" %CreditCardType
			if(CreditCardType=='VISA'):
				CreditCardTypeId=2
			elif(CreditCardType=='MasterCard'):
				CreditCardTypeId=3
			elif(CreditCardType=='AMEX'):
				CreditCardTypeId=4
			elif(CreditCardType=='Discover'):
				CreditCardTypeId=5
			elif(CreditCardType=='Diners Club'):
				CreditCardTypeId=6
			elif(CreditCardType=='CreditCard'):
				CreditCardTypeId=7
			elif(CreditCardType=='Other'):
				CreditCardTypeId=8
			else:
				CreditCardTypeId=0
			PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
			print " PointOfSaleID is %s" %PointOfSaleId
			PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
			print " Purchase Id is %s" %PurchaseId
			ProcessorTransactionId = self.__getProcessorTransactionId(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
			print "ProcessorTransactionId is %s" %ProcessorTransactionId
			EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
			print " Merchant Account Id is %s" %EMS7MerchantAccountId
			if(PurchaseId):
				EMS7Transactions.append((PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,ProcessingDate,IsUploadedFromBoss,IsRFID,Approved))
			else:
				MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				DiscardedRecords.append((MultiKeyId,tableName,date.today()))
			if (len(EMS7Transactions) >= 5000):
				print "Inserting %s Records" %len(EMS7Transactions)
				self.__EMS7Cursor.executemany("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", EMS7Transactions)
#				self.__EMS7Connection.commit()
				EMS7Transactions = []
			if (len(DiscardedRecords) >= 5000):
				self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
				DiscardeRecords = []
		#print "Inserting %s Records" %len(EMS7Transactions)
		self.__EMS7Cursor.executemany("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", EMS7Transactions)
#		self.__EMS7Connection.commit()
		self.__batchInsertIntoDiscardedRecords(DiscardedRecords,1)
