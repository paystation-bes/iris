#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert

ClusterId = 4;
try:

	
	EMS6Connection = mdb.connect('10.1.2.202', 'apamu', 'apamu', 'ems_db'); 
	EMS6ConnectionCerberus = mdb.connect('10.1.2.202', 'apamu', 'apamu','cerberus_db');
	EMS7Connection = mdb.connect('10.1.2.99', 'migration','migration','ems7_db');
	
	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccessInsert(EMS7Connection, 1), 0)

	#38

	controller.logModuleStartStatus(ClusterId,'Purchase') 		
	controller.migratePurchase(ClusterId,'Purchase') #Done	
	controller.logModuleEndStatus(ClusterId,'Purchase') 	

#39

	controller.logModuleStartStatus(ClusterId,'ProcessorTransaction') 		
	controller.migrateProcessorTransaction(ClusterId,'ProcessorTransaction') # Changed the SQL to process 6 months of data  Done
	controller.logModuleEndStatus(ClusterId,'ProcessorTransaction') 	
	
#40

	controller.logModuleStartStatus(ClusterId,'Permit') 		
	controller.migratePermit(ClusterId,'Permit') #Done
	controller.logModuleEndStatus(ClusterId,'Permit') 	

#41

	controller.logModuleStartStatus(ClusterId,'PaymentCard') 		
	controller.migratePaymentCard(ClusterId,'PaymentCard') # Done
	controller.logModuleEndStatus(ClusterId,'PaymentCard') 	

#42

	controller.logModuleStartStatus(ClusterId,'POSCollection') 		
	controller.migratePOSCollection(ClusterId,'POSCollection') #Done
	controller.logModuleEndStatus(ClusterId,'POSCollection') 	
	
#43

	controller.logModuleStartStatus(ClusterId,'PurchaseTax') 		
	controller.migratePurchaseTax(ClusterId,'PurchaseTax') #Done
	controller.logModuleEndStatus(ClusterId,'PurchaseTax') 	
	
#44
	controller.logModuleStartStatus(ClusterId,'PurchaseCollection') 		
	controller.migratePurchaseCollection(ClusterId,'PurchaseCollection')  #Done
	controller.logModuleEndStatus(ClusterId,'PurchaseCollection') 	



#########################################	

	print "Completed Migrating Transactions Cluster 4"







   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
