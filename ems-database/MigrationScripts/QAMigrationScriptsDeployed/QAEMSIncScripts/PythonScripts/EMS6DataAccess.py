##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6DataAccess
##	Purpose		: This class extracts the data from EMS6 Database. 
##	Important	: The calls to the procedure in the class are implicitly made by EMS6InitialMigration.py.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

class EMS6DataAccess:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, verbose):
		self.__verbose = verbose

		self.__EMS6Connection = EMS6Connection
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def getCustomers(self):
		print " IN EMS6 DATAACCESS"
		# By Default all the Customers are assigned a CHILD status(CustomerType=3)
		self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer")
		return self.__EMS6Cursor.fetchall()	   
	
	def getCustomers_inc(self):
		print " IN EMS6 DATAACCESS INCREMENTAL"
		self.__EMS6Cursor.execute("SELECT * FROM MigrationQueue")
		return self.__EMS6Cursor.fetchall()	   
		
	def getEMS6CustomerProperties(self):
		# JIRA 4764
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,case when TimeZone='' then 'GMT' else TimeZone end  as TimeZone,SMSWarningPeriod \
			from CustomerProperties")
		return self.__EMS6Cursor.fetchall()
	
	#NEED TO BE CHANGED TO 1 on PROD and 21 on QA
	def getEMS6CustomerSubscription(self):
                self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from cerberus_db.Customer_Service_Relation r left join cerberus_db.Customer c on (c.Id=r.CustomerId) where c.ServerId=1")
                return self.__EMS6CursorCerberus.fetchall()

	def getEMS6LotSettings(self):
		self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,\
			l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL")
		return self.__EMS6Cursor.fetchall()

 	def getEMS6Paystation(self):
		# Added on Aug 15
		#self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,p.version,p.Name,p.ProvisionDate,p.CustomerId,p.RegionId,p.LotSettingId,p.ContactURL from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId)")
		# Added on Aug 22 If no modem then Modem Type as 1 (Unknown)
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 1 END as Type ,\
			p.CommAddress,p.version,p.Name,p.ProvisionDate,p.CustomerId,p.RegionId,p.LotSettingId,p.ContactURL, r.IsDefault,\
			p.MerchantAccountId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) left join Region r on (p.RegionId = r.Id)")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Locations(self):
                self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Name is not null")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Coupons(self):
		#Ashok: Jira 4085
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,\
			Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled,\
			Coupon.DollarDiscount,Coupon.IsOffline from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null")	
		return self.__EMS6Cursor.fetchall()

	def getEMS6ParkingPermissionType(self):
		self.__EMS6Cursor.execute("select Id,Name from EMSParkingPermissionType")
		return self.__EMS6Cursor.fetchall()

	#Ashok: Added the Where Condition
	def getParkingPermission(self):
		self.__EMS6Cursor.execute("select Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,\
			EndTimeMinuteLocal,RegionId,PermissionTypeId,PermissionStatus,SpecialParkingPermissionId,MaxDurationMinutes,\
			CreationDate,IsActive,Id from EMSParkingPermission where PermissionStatus <> ''")
		return self.__EMS6Cursor.fetchall()

	def getParkingPermissionDayOfWeek(self):
		self.__EMS6Cursor.execute("select EMSPermissionId,DayOfWeek from EMSParkingPermissionDayOfWeek")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensibleRatesDayOfWeek(self):
                self.__EMS6Cursor.execute("select EMSRateId,DayOfWeek from EMSRateDayOfWeek")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestAccount(self):
                # This SQL is for QA
				# self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null and r.CustomerId <>1432")
				# This SQL is for PROD
				self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null")
				return self.__EMS6Cursor.fetchall()		
				
		
		

	def getEMS6LicencePlate(self, PaystationId):
		self.__EMS6Cursor.execute("select distinct(PlateNumber) from Transaction where PaystationId=%s and PlateNumber<>''",(PaystationId))
		return self.__EMS6Cursor.fetchall()	

	def getEMS6MobileNumberFromSMSAlert(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from SMSAlert where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6MobileNumberFromEMSExtensiblePermit(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from EMSExtensiblePermit where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6MobileNumberFromSMSTransactionLog(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from SMSTransactionLog where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()
		
	def getEMS6MobileNumber(self):
		self.__EMS6Cursor.execute("select distinct(Number) from MobileNumber where Number<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensiblePermit(self):
		self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6SMSTransactionLog(self):
		self.__EMS6Cursor.execute("select SMSMessageTypeId,MobileNumber,PaystationId,PurchasedDate,TicketNumber,\
			ExpiryDate,UserResponse,TimeStamp from SMSTransactionLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6SMSAlert(self):
		self.__EMS6Cursor.execute("select MobileNumber,ExpiryDate,PaystationId,PurchasedDate,TicketNumber,PlateNumber,\
			StallNumber,RegionId,NumOfRetry,IsAlerted,IsLocked,IsAutoExtended from SMSAlert")
		return self.__EMS6Cursor.fetchall()

	def getEMS6SMSFailedResponse(self):		
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,IsOk,TrackingId,Number,\
			ConvertedNumber,DeferUntilOccursInThePast,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,\
			BlockedReason,IsBalanceZero,IsInvalidCarrierCode from SMSFailedResponse")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ModemSettings(self):
		self.__EMS6Cursor.execute("select PaystationId, Type, CCID, Carrier, APN from ModemSetting")
		return self.__EMS6Cursor.fetchall()

	def getEMS6LotSettingContent(self):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CryptoKey(self):
		self.__EMS6Cursor.execute("select Type,KeyIndex,Hash,Expiry,Signature,Info,NextHash,Status,CreateDate,Comment from CryptoKey")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestLogs(self):	
		self.__EMS6Cursor.execute("select RestAccountName, EndpointName, LoggingDate, IsError, CustomerId, TotalCalls from RESTLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestSessionToken(self):	
		self.__EMS6Cursor.execute("select AccountName,SessionToken,CreationDate,ExpiryDate from RESTSessionToken")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestLogTotalCall(self):	
		self.__EMS6Cursor.execute("select RESTAccountName,LoggingDate,TotalCalls from RESTLogTotalCall")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerWsCal(self):	
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal")
		return self.__EMS6Cursor.fetchall()

        def getEMS6CustomerWsToken(self):
                self.__EMS6Cursor.execute("select CustomerId,Token,EndPointId,WsInUse,Id from CustomerWsToken")
                return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerAlert(self):
		self.__EMS6Cursor.execute("select Id,PaystationGroupId,CustomerId,AlertName,AlertTypeId,Threshold,Email,\
			IsEnabled,IsDeleted,RegionId,IsPaystationBased from Alert")
		return self.__EMS6Cursor.fetchall()

## This query has been merged into the Paystation Query, Disabled on May 082013
#	def getEMS6PaystationAlertEmail(self):
#		self.__EMS6Cursor.execute("select Id,CustomerId,ContactURL from Paystation")
#		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerWebServiceCal(self):
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal")
		return self.__EMS6Cursor.fetchall()

## The Module below is extra and needs to be removed
#	def getEMS6CustomerWsToken(self):
#		self.__EMS6Cursor.execute("select CustomerId,Token, EndPointId, WsInUse from CustomerWsToken")
#		return self.__EMS6Cursor.fetchall()

	def getEMS6WsCallLog(self):
		self.__EMS6Cursor.execute("select Token,CallDate,CustomerId,TotalCall from WsCallLog")
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdFromRates(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Rates")	
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6Rates(self, PaystationId):
		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s",PaystationId)
	#	self.__EMS6Cursor.execute("select PaystationId,TicketNumber,PurchasedDate,CustomerId,RegionId,LotNumber,TypeId,RateId,RateName,RateValue,Revenue from Rates where PaystationId=%s", PaystationId)
		return self.__EMS6Cursor.fetchall()

	def getEMS6EMSRates(self):
#		self.__EMS6Cursor.execute("select distinct r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")
		## Above SQL revised after incremental migration
		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")

		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensibleRate(self):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,\
			EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,\
			IsActive,Id from EMSRate")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6Replenish(self):
		self.__EMS6Cursor.execute("select PaystationId, Date, Number, TypeId, Tube1Type, Tube1ChangedCount, Tube1CurrentCount, \
			Tube2Type, Tube2ChangedCount, Tube2CurrentCount, Tube3Type, Tube3ChangedCount, Tube3CurrentCount, Tube4Type, \
			Tube4ChangedCount, Tube4CurrentCount, CoinBag005AddedCount, CoinBag010AddedCount, CoinBag025AddedCount, \
			CoinBag100AddedCount, CoinBag200AddedCount from Replenish")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CardRetryTransaction(self):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,\
			TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,\
			LastResponseCode,IsRFID from CardRetryTransaction")
		return self.__EMS6Cursor.fetchall()

	# Ashok: version column does not exists in CardType table on QA DB. But exists on PROD. Hence enclosed with '' 
	# Ashok: Modified the SQL to fetch only valid records
	def getEMS6CardType(self):
		self.__EMS6Cursor.execute("select CustomerId,version,Name,Track2RegEx,CheckDigitAlg,Description,AuthorizationMethod,\
			Id from CardType where CustomerId in (select Id from Customer) ")
		return self.__EMS6Cursor.fetchall()
	
	# Ashok Aug 12
	def getEMS6PaystationGroup(self):
		self.__EMS6Cursor.execute("select Id,CustomerId,Name from PaystationGroup")
		return self.__EMS6Cursor.fetchall()
	
	# Ashok Aug 12
	def getEMS6Paystation_PaystationGroup(self):
		self.__EMS6Cursor.execute("select PaystationGroupId, PaystationId from Paystation_PaystationGroup")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6CollectionType(self):
		self.__EMS6Cursor.execute("select Id,Name,IsInactive from CollectionType")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ValueSmartCardType(self,PaystationId):
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData \
			from Transaction where PaystationId=%s and CustomCardData<>''",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6SmartCardType(self,PaystationId):
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData \
			from Transaction where PaystationId=%s and SmartCardData<>'' and SmartCardData!=0",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationForBatteryInfo(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from BatteryInfo")
		return self.__EMS6Cursor.fetchall()

	def getBatteryInfo(self, PaystationId):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo \
			where PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationId(self):
		# Ashok: changed the below SQL to run on QA environment
		# Infuture remove this where condition
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from Transaction where PaystationId>49872")
		#Ashok: Added this condition to migrate all paystation transaction data.
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Transaction")
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from Transaction")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Transaction(self, PaystationId, DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
#Section below commented out to merge Purchae and Permit data in one query
	#	self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, ifnull(tc.CoinDollars,0),ifnull(tc.BillDollars,0),ifnull(tc.CoinCount,0),ifnull(tc.BillCount,0),rt.RateValue,ifnull(rt.Revenue,0),rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber) where tr.PaystationId=%s and tr.PurchasedDate>='2013-05-30 00:00:01' and tr.PurchasedDate<='2013-06-30 00:00:00'",(PaystationId))
		###        Run 1            ####
		self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,\
			tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,\
			tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,\
			tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,\
			tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid,ep.MobileNumber \
			from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) \
			left join Rates rt using (PaystationId,PurchasedDate,TicketNumber)left join EMSExtensiblePermit ep using (PurchasedDate) \
			where tr.PaystationId=%s and tr.PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
			
		#### Run 2                    ####
	#	self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid,ep.MobileNumber from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber)left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate>='2012-03-02 00:00:00' and tr.PurchasedDate<='2012-06-01 00:00:00'",(PaystationId))
		### Run 3  #####
#		self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid,ep.MobileNumber from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber)left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate>='2012-06-02 00:00:00' and tr.PurchasedDate<='2012-09-01 00:00:00'",(PaystationId))
		### Run 4 ####
	#	self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid,ep.MobileNumber from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber)left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate>='2012-09-02 00:00:00' and tr.PurchasedDate<='2012-12-31 00:00:00'",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getCustomerEmail(self):
		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, \
			IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert")
#		self.__EMS6Cursor.execute("select Id, CustomerId, AlertTypeId, Email, IsDeleted from Alert")
		return self.__EMS6Cursor.fetchall()

	def getCustomerEmailPaystation(self):
		self.__EMS6Cursor.execute("select CustomerId, ContactURL from Paystation")
		return self.__EMS6Cursor.fetchall()

	def getProcessorProperties(self):
		self.__EMS6Cursor.execute("select Processor,Name,Value from ProcessorProperties")
		return self.__EMS6Cursor.fetchall()
		
	# Ashok: Added the condition CustomerId is not null 
	def getMerchantAccount(self):
		self.__EMS6Cursor.execute("select Id,version,CustomerId,Name,Field1,Field2,Field3,IsDeleted,ProcessorName,\
			ReferenceCounter,Field4,Field5,Field6,IsValid from MerchantAccount WHERE CustomerId is not null")
		return self.__EMS6Cursor.fetchall()

	def getPreAuth(self):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, \
			ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId from PreAuth")
		return self.__EMS6Cursor.fetchall()
		
	def getPreAuthHolding(self):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, \
			MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,\
			ReferenceId,Expired,CardHash,ExtraData,PsRefId,IsRFID,AddedGMT,MovedGMT,CardExpiry from PreAuthHolding")
		return self.__EMS6Cursor.fetchall()

	def getProcessorTransaction(self, PaystationId,DateRange):
		# Changed the below SQL for QA migration
		# self.__EMS6Cursor.execute("select TypeId, PaystationId, MerchantAccountId, TicketNumber, Amount, CardType, Last4DigitsOfCardNumber, CardChecksum, PurchasedDate, ProcessingDate, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, Approved, CardHash, IsUploadedFromBoss, IsRFID from ProcessorTransaction where PaystationId=%s and PurchasedDate>='2012-02-01 00:00:00' and PurchasedDate<='2012-02-05 00:00:00'",(PaystationId))
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		print "Ashok: Fetching Data from EMS6 Processor Transaction"
		self.__EMS6Cursor.execute("select TypeId, PaystationId, MerchantAccountId, TicketNumber, Amount, CardType, \
			Last4DigitsOfCardNumber, CardChecksum, PurchasedDate, ProcessingDate, ProcessorTransactionId, \
			AuthorizationNumber, ReferenceNumber, Approved, CardHash, IsUploadedFromBoss, IsRFID \
			from ProcessorTransaction where PaystationId=%s and PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdFromProcessorTransaction(self):
		#Ashok: Added this where condition to match what was present in the code to migrate Purchase
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from ProcessorTransaction where paystationid > 49872")
		self.__EMS6Cursor.execute("select distinct(PaystationId) from ProcessorTransaction")
		return self.__EMS6Cursor.fetchall()

	def getEMS6TransactionForPermit(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,\
			tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber,tr.CustomerId \
			from Transaction tr left join SMSAlert ep using (PaystationId,TicketNumber,PurchasedDate) \
			where tr.PaystationId=%s and tr.PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall()

	def getEMS6TransactionForPayment(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		#Rewrite the query to lookup TransactionProcessorId for every transaction that comes up, make sure there is a single Processor transaction id returned (SubQuery) as there could be multiple processor transaction record for a single transaction, we need to find a unique processor transaction record by looking up the max(procssing date) in processor transaction table
		# Ashok Aug 12 Jira 4100. Donot consider ProcessorTransaction.IsApproved=1. Removed this condition "Approved=1 and" replaced pt.Amount to t.Cardpaid
		#Remodified the query on Aug 20 Jira 4175
		self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,\
			pt.MerchantAccountId,t.Cardpaid,pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,\
			pt.Approved,t.SmartCardData,t.SmartCardPaid,t.CustomCardData,t.CustomCardType,t.CustomerId,pt.ProcessingDate \
			from Transaction t left join ProcessorTransaction pt using (PaystationId,PurchasedDate,TicketNumber) \
			where t.PaystationId=%s and t.PurchasedDate between %s and %s and t.PaymentTypeId<>1 \
			and pt.ProcessingDate = (SELECT max(ProcessingDate) from ProcessorTransaction where \
			PaystationId= t.PaystationId  and 	PurchasedDate =t.PurchasedDate \
			and TicketNumber=t.TicketNumber)",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall() 

	def getEMS6UserAccount(self):
		self.__EMS6Cursor.execute("select u.Id,u.version,u.CustomerId,u.Name,u.Password,u.RoleId,u.AccountStatus, \
			concat(u.Name,'@',c.Name) as UserAccount,u.Id,'LastName' from UserAccount u left join Customer c on(c.Id=u.CustomerId)")
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdForEventLogNew(self):
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from EventLogNew where PaystationId in (select Id from Paystation where CustomerId in (3634))")
		self.__EMS6Cursor.execute("select distinct(PaystationId) from EventLogNew")
		return self.__EMS6Cursor.fetchall()

	def getEventLogNew(self, PaystationId):
		# Ashok: Aug 22 Jira# 4190
		self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,\
			ifnull(ClearDateField, date_add(DateField,interval 1 second)) from EventLogNew where PaystationId=%s and \
			DateField between DATE_ADD(now(), INTERVAL -90 DAY) and now()",PaystationId)
		return self.__EMS6Cursor.fetchall()
	
#	def getEMS6CustomerAlert(self):
#		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert")
#		return self.__EMS6Cursor.fetchall()

	def getEMS6CerberusTrialCustomer(self):
		# Ashok: Commented the below SQL as schema name is different in QA migration case (ems_db_6_3_12)
		# This Query is for Production
		#self.__EMS6Cursor.execute("select EmsCustomerId,Status,Expiry from cerberus_db.Customer left join ems_db.Customer on cerberus_db.Customer.EmsCustomerId=ems_db.Customer.Id where Status=1 and ServerId=1")
		
		# This Query is for QA
		self.__EMS6Cursor.execute("select EmsCustomerId,Status,Expiry \
			from cerberus_db.Customer left join ems_db.Customer on cerberus_db.Customer.EmsCustomerId=ems_db.Customer.Id \
			where Status=1 and ServerId=1")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RegionPaystationLog(self):
		self.__EMS6Cursor.execute("select Id, RegionId, PaystationId, CreationDate from RegionPaystationLog")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6PaystationBalance(self):
		self.__EMS6Cursor.execute("select PaystationId, CashDollars, CoinCount, CoinDollars, BillCount, BillDollars, \
			UnsettledCreditCardCount, UnsettledCreditCardDollars, TotalDollars, LastCashCollectionGMT, \
			LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastRecalcGMT, CollectionRecalcGMT, \
			CollectionRecalcTransactions, ClusterId, HasRecentCollection, IsRecalcable, LastCollectionTypeId, \
			CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, NextRecalcGMT from PaystationBalance")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ClusterMember(self):
		self.__EMS6Cursor.execute("select Id,Name,Hostname,LocalPort from ClusterMember")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CollectionLock(self):
		self.__EMS6Cursor.execute("select ClusterId, LockGMT, BatchSize, LockableCount, LockedCount, RecalcedCount, \
			IsReissuedLock, RecalcMode, RecalcEndGMT from CollectionLock")
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationHeartBeat(self):
		self.__EMS6Cursor.execute("select PaystationId,LastHeartBeatGMT from PaystationHeartBeat")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ServiceState(self):
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,\
			IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,\
			IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,\
			CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,\
			CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,\
			PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,\
			IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,\
			TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,\
			PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,\
			IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,\
			IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,\
			IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState")
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationAlert(self):
		# Ashok for Testing
		#where PaystationId in (select Id from Paystation where CustomerId in (3634))
		self.__EMS6Cursor.execute("select AlertId,PaystationId,IsAlerted from PaystationAlert")
		return self.__EMS6Cursor.fetchall()

	def getActivePOSAlertForUserDefined(self):
		self.__EMS6Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,\
			EventActionTypeId,EventSeverityTypeId, max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,\
			ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId=20 group by PointOfSaleId")
		return self.__EMS6Cursor.fetchall()

	def getActivePOSAlertForPaystation(self):
		self.__EMS6Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,\
			EventActionTypeId,EventSeverityTypeId,max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,\
			ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId!=20 group by PointOfSaleId")
		return self.__EMS6Cursor.fetchall()

#Get the paystation  list from cerberus and fileter it by Customer.ServerId=1, must have that.
	def getEMS6BillingReport(self, PaystationId):
      		self.__EMS6Cursor.execute("SELECT  H.idPaystation, H.ChangeGMT, H.idBillingStatusType \
				FROM cerberus_db.BillingStatusHistory H, Paystation P, cerberus_db.Customer C WHERE P.id = %s \
				AND P.id = H.idPaystation and C.ServerId=1 GROUP BY P.id", (PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6AuditAccess(self, PaystationId):
		self.__EMS6Cursor.execute("SELECT  P.Id, A.ActivityDate AS ChangeGMT, 37-A.EventTypeId AS \
			NowActivated FROM AuditAccess A, Paystation P WHERE A.EventTypeId IN (35,36) AND A.Description \
			LIKE 'PaystationId:' AND P.id = %s AND P.id = CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED) \
			AND A.ActivityDate = (SELECT  MAX(A2.ActivityDate) FROM AuditAccess A2 WHERE A2.EventTypeId IN (35,36) AND \
			CAST(SUBSTRING(A2.Description,Locate('PaystationId: ',A2.Description)+14) AS UNSIGNED) = \
			CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED))", (PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationForPOSDate(self):
		self.__EMS6Cursor.execute("select Id,version,Name,CommAddress,ContactURL,CustomerID,IsActive,RegionId,TimeZone,\
		LockState,ProvisionDate,MerchantAccountId,DeleteFlag,CustomCardMerchantAccountID,PaystationType,\
		IsVerrus,QueryStallsBy,LotSettingId from Paystation")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadValueCard(self):
		self.__EMS6Cursor.execute("select CustomerId,AccountNumber,version,CardType from BadCard")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadCreditCard(self):
		self.__EMS6Cursor.execute("select CustomerId,CardHash,CardData,CardExpiry,AddedDate,Comment from BadCreditCard")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadSmartCard(self):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,AddedDate,Comment from BadSmartCard")
		return self.__EMS6Cursor.fetchall()

	def getDistinctCustomerFromPaystation(self):
		self.__EMS6Cursor.execute("select distinct(CustomerId) from Paystation")
		return self.__EMS6Cursor.fetchall()

	def getEMS6QueryStallsBy(self, CustomerId):
#		self.__EMS6Cursor.execute("select CustomerId,if(count(distinct QueryStallsBy) > 1, 'yes',QueryStallsBy) from Paystation where CustomerId=%s",(CustomerId))
	#	print "select CustomerId,if(count(distinct QueryStallsBy) > 1, (SELECT QueryStallsBy from Paystation where CustomerId=%s group by QueryStallsBy order by count(QueryStallsBy) Desc limit 1), QueryStallsBy) from Paystation where CustomerId=%s",(CustomerId[0],CustomerId[0])

		self.__EMS6Cursor.execute("select QueryStallsBy, count(*) as numEntries from Paystation where CustomerId=%s \
			group by QueryStallsBy ORDER BY numEntries DESC LIMIT 1",(CustomerId))
	
#		self.__EMS6Cursor.execute("select CustomerId,if(count(distinct QueryStallsBy) > 1, (SELECT QueryStallsBy from Paystation where CustomerId=%s group by QueryStallsBy order by count(QueryStallsBy) desc limit 1), QueryStallsBy) as 'QueryStallsBy' from Paystation where CustomerId=%s",(CustomerId,CustomerId));
	#	self.__EMS6Cursor.execute("select CustomerId,if(count(distinct QueryStallsBy) > 1, 'SELECT QueryStallsBy from Paystation where CustomerId=%s group by QueryStallsBy order by count(QueryStallsBy) limit 1', QueryStallsBy) from Paystation where CustomerId=%s",(CustomerId, CustomerId));
		return self.__EMS6Cursor.fetchall()
	
	# Ashok: Typo error in SQL ProcessindDate
	def getEMS6CCFailLog(self):
		self.__EMS6Cursor.execute("select Id, PaystationId, MerchantAccountId, TicketNumber, ProcessingDate, PurchasedDate, \
			CCType, Reason from CCFailLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ServiceAgreement(self):
		self.__EMS6Cursor.execute("select Id, Content, ContentVersion, UploadedDate from ServiceAgreement")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ServiceAgreedCustomer(self):
		self.__EMS6Cursor.execute("select CustomerId, ServiceAgreementId, Name, Title, Organization, ActivationDate, \
			IsOverride from ServiceAgreedCustomer")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ImportError(self):
		self.__EMS6Cursor.execute("select CustomerId,PaystationId,ImportData,ImportDataHash,Comment,CreationDate from ImportError")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RawSensorData(self):
		self.__EMS6Cursor.execute("select ServerName,PaystationCommAddress,Type,Action,Information,DateField,LastRetryTime,\
			RetryCount from RawSensorData")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RawSensorDataArchive(self):
		self.__EMS6Cursor.execute("select ServerName,PaystationCommAddress,Type,Action,Information,DateField,LastRetryTime,\
			RetryCount from RawSensorDataArchive")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Reversal(self):
		self.__EMS6Cursor.execute("select MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,OriginalProcessingCode,\
			OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,PurchasedDate,TicketNumber,\
			PaystationId,RetryAttempts,LastRetryTime,Succeeded,Expired,IsRFID from Reversal where MerchantAccountId >0 ")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6ReversalArchive(self):
		self.__EMS6Cursor.execute("select MerchantAccountId,CardNumber,ExpiryDate,OriginalMessageType,\
			OriginalProcessingCode,OriginalReferenceNumber,OriginalTime,OriginalTransactionAmount,LastResponseCode,\
			PurchasedDate,TicketNumber,PaystationId,RetryAttempts,LastRetryTime,Succeeded,Expired,IsRFID \
			from ReversalArchive Where MerchantAccountId > 0")
		return self.__EMS6Cursor.fetchall()

	def getEMS6SCRetry(self):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,SCType,RetryServer,MerchantInfo,SettlementInfo from SCRetry")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Transaction2Push(self):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId,LotNumber,PlateNumber,\
			ExpiryDate,ChargedAmount,StallNumber,RegionId,RetryCount,LastRetryTime,IsOffline,Field1 from Transaction2Push")
		return self.__EMS6Cursor.fetchall()

	def getEMS6TransactionPush(self):
		self.__EMS6Cursor.execute("select PaystationId, PurchasedDate, TicketNumber, CustomerId, LotNumber, PlateNumber, \
			ExpiryDate, ChargedAmount, StallNumber, RetryCount, LastRetryTime, IsOffline from TransactionPush")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ThirdPartyPOSSequence(self):
		self.__EMS6Cursor.execute("select PaystationId,SequenceNumber from ThirdPartyPaystationSequence")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ThirdPartyPushLog(self):
		self.__EMS6Cursor.execute("select ActionTypeId, DateField, PushData, ResponseMessage from ThirdPartyPushLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ThirdPartyServiceAccount(self):
		self.__EMS6Cursor.execute("select CustomerId,Type,EndPointUrl,UserName,Password,Field1,Field2,IsPaystationBased \
			from ThirdPartyServiceAccount")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ThirdPartyServicePaystation(self):
		self.__EMS6Cursor.execute("select PaystationId, ThirdPartyServiceAccountId from ThirdPartyServicePaystation")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ThirdPartyServiceStallSequence(self):
		self.__EMS6Cursor.execute("RegionId,StallNumber,SequenceNumber from ThirdPartyServiceStallSequence")
		return self.__EMS6Cursor.fetchall()	

	def getDistinctPaystationFromAudit(self):
		#self.__EMS6Cursor.execute("select distinct(PaystationId) from Audit where paystationid > 49872")
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Audit")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Audit(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
#		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CustomerId from Audit")
		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,\
			version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CoinAmount,OnesAmount,\
			TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DiscoverAmount,MasterCardAmount,OtherCardAmount,\
			SmartCardAmount,VisaAmount,CitationsPaid,CitationAmount,ChangeIssued,RefundIssued,SmartCardRechargeAmount,ExcessPayment,\
			CustomerId,ReportNumber,NextTicketNumber,AttendantTicketsSold,AttendantTicketsAmount,DinersAmount,AttendantDepositAmount,\
			AcceptedFloatAmount,Tube1Type,Tube1Amount,Tube2Type,Tube2Amount,Tube3Type,Tube3Amount,Tube4Type,Tube4Amount,OverfillAmount,\
			ReplenishedAmount,Hopper1Dispensed,Hopper2Dispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Replenished,\
			Hopper2Replenished,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,CoinCount005,CoinCount010,CoinCount025,\
			CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50 \
			from Audit where PaystationId=%s and StartDate between %s and %s",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdForTaxes(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Taxes")
		return self.__EMS6Cursor.fetchall()

	def getTaxes(self, PaystationId,DateRange):
		StartDate = DateRange[1]
		EndDate = DateRange[2]
		PaystationId = PaystationId[0]
		#Ashok for QA: CustomerId = 1202 and
		self.__EMS6Cursor.execute("select Id,PaystationId,TicketNumber,PurchasedDate,ProcessingDate,CustomerId,RegionId,TaxName,TaxRate,\
			TaxValue from Taxes where PaystationId=%s and PurchasedDate between %s and %s",(PaystationId,StartDate,EndDate))
		return self.__EMS6Cursor.fetchall()

	def getPOSDateForPOSStatus(self):
		self.__EMS7Cursor.execute("select PointOfSaleId,POSDateTypeId,ChangedGMT,CurrentSetting,VERSION from POSDate")
		return self.__EMS6Cursor.fetchall()

	def logIncrementalMigrationError(self, e,  migrationId):
		self.__EMS6Cursor.execute("update MigrationQueue set ErrorMsg= %s where Id = %s",(e, migrationId))
		self.__EMS6Connection.commit()
		
	#Added on Aug 14 ITF
	def getEMS6ValidCustomCard(self):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,version,StartDate,EndDate,RegionId,PaystationId,MaxValidTicketCount,\
			MaxValidTicketGracePeriod from ValidCustomCard")
		return self.__EMS6Cursor.fetchall()
	
	#Added on Aug 15
 	def getEMS6PaystationForActive(self):
		# Added on Aug 15		
		self.__EMS6Cursor.execute("select Id,isActive from Paystation")
		return self.__EMS6Cursor.fetchall()
		
		
	#Added on Aug 16
 	def getEMS6CustomerExpiryCerberus(self):
		# Added on Aug 16		
		self.__EMS6Cursor.execute("select distinct EmsCustomerId,Expiry from cerberus_db.Customer where EmsCustomerId \
			in (select Id from ems_db.Customer)")
		return self.__EMS6Cursor.fetchall()
		