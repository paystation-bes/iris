#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------
## Running Migration scripts on PROD data second time

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccessInsert import EMS7DataAccessInsert

ClusterId = 1;
try:

	
	EMS6Connection = mdb.connect('10.1.2.202', 'apamu', 'apamu', 'ems_db'); 
	EMS6ConnectionCerberus = mdb.connect('10.1.2.202', 'apamu', 'apamu','cerberus_db');
	EMS7Connection = mdb.connect('10.1.2.99', 'migration','migration','ems7_db');
	
	
	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccessInsert(EMS7Connection, 1), 0)

	
	controller.logModuleStartStatus(ClusterId,'CCFailLog') 		
	controller.migrateCCFailLog()	
	controller.logModuleEndStatus(ClusterId,'CCFailLog') 		
	
	
	controller.logModuleStartStatus(ClusterId,'SCRetry') 		
	controller.migrateSCRetry() #Done
	controller.logModuleEndStatus(ClusterId,'SCRetry') 		

# Added this module on Aug 18

	controller.logModuleStartStatus(ClusterId,'PreAuth') 		
	controller.migratePreAuth() # 
	controller.logModuleEndStatus(ClusterId,'PreAuth') 
	
# Added this module on Aug 18

	controller.logModuleStartStatus(ClusterId,'PreAuthHolding') 	
	controller.migratePreAuthHolding() # Completed
	controller.logModuleEndStatus(ClusterId,'PreAuthHolding') 

	controller.logModuleStartStatus(ClusterId,'CardRetryTransaction') 	
	controller.migrateCardRetryTransaction()
	controller.logModuleEndStatus(ClusterId,'CardRetryTransaction') 	

	
	controller.logModuleStartStatus(ClusterId,'SMSTransactionLog') 		
	controller.migrateSMSTransactionLog() #Ashok: 
	controller.logModuleEndStatus(ClusterId,'SMSTransactionLog') 		

	controller.logModuleStartStatus(ClusterId,'SMSFailedResponse') 		
	controller.migrateSMSFailedResponse() #Ashok: 
	controller.logModuleEndStatus(ClusterId,'SMSFailedResponse') 		

	controller.logModuleStartStatus(ClusterId,'Replenish') 
	controller.migrateReplenish()  
	controller.logModuleEndStatus(ClusterId,'Replenish') 		
	
	
	controller.logModuleStartStatus(ClusterId,'Reversal') 	
	controller.migrateReversal() #Error MerchantAccount Cannot be Null Added the Condition now
	controller.logModuleEndStatus(ClusterId,'Reversal') 		

	
	
	controller.logModuleStartStatus(ClusterId,'ReversalArchive') 	
	controller.migrateReversalArchive()
	controller.logModuleEndStatus(ClusterId,'ReversalArchive') 	


#########################################	
#	Donot RUN - Aug 13

#35 ERROR CUSTOMER ALERT TYPEID CANNOT BE NULL
#	controller.logModuleStartStatus(ClusterId,'CustomerEmail') 		
#	controller.migrateCustomerEmail() #Error 
#	controller.logModuleEndStatus(ClusterId,'CustomerEmail') 	

	

	
#	THIS IS A LOG TABLE. CAN BE MIGRATED AT THE END	


#	controller.migrateWebServiceCallLog() # NO NEED TO MIGRATE

#	controller.migrateActivePOSAlert() # NO because user defined alerts are not migratedCompletedwithCondition ###### but no data in destination table

#	controller.migrateRestLog()   #NO NEED TO MIGRATE Completed 
#	controller.migrateRestLogTotalCall()  #NO NEED TO MIGRATE


#	controller.migrateBatteryInfo() #NO NEED TO MIGRATE

#	controller.migratePaystationBalance()#NO NEED TO MIGRATE	#ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateClusterMember() #NO NEED TO MIGRATE # for cluster table to work, please disable the auto increment for the EMS7.Cluster table. The auto_increment can be enabled after the initial migration has finished, as we will need to bring all the Id's as they exist in EMS6. #ASHOK: DID NOT EXECUTED. LATER
#	controller.migrateCollectionLock() #NO NEED TO MIGRATE #ASHOK: DID NOT EXECUTED. LATER
#	controller.logModuleStartStatus(ClusterId,'POSDate') 			
#	controller.migratePOSDateBilling()
#	controller.logModuleEndStatus(ClusterId,'POSDate') 
#3.1)	
#	controller.logModuleStartStatus(ClusterId,'UnAsgLocations') 
#	controller.migrateUnassignedLocations() #Done
#	controller.logModuleEndStatus(ClusterId,'UnAsgLocations') 	


	print "Completed PostTransaction Modules Migration"







   


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
