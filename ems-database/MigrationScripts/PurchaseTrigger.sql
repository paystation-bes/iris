DELIMITER $$
CREATE TRIGGER InsertPurchase
AFTER INSERT ON Purchase
FOR EACH ROW
BEGIN
INSERT INTO PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) 
\ values(New.Id,New.PointOfSaleId,New.PurchaseGMT,New.CashPaidAmount,New.CoinPaidAmount,New.CoinCount,New.BillPaidAmount,New.BillCount);
END$$