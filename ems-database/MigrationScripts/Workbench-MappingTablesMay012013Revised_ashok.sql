-- -----------------------------------------------------
-- Table `EMS7`.`AuditMapping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `EMS7`.`AuditMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AuditMapping` (
  `PaystationId` INT ,
  `StartDate` DATETIME ,
  `EndDate` DATETIME ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId` ,`StartDate` ,`EndDate`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`CryptoKeyMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CryptoKeyMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CryptoKeyMapping` (
  `Type` SMALLINT UNSIGNED,
  `KeyIndex` SMALLINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Type` ,`KeyIndex`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CustomerWsCalMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CustomerWsCalMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerWsCalMapping` (
  `EMS6Id` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CustomerWsTokenMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CustomerWsTokenMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerWsTokenMapping` (
  `EMS6Id` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSExtensiblePermitMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSExtensiblePermitMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSExtensiblePermitMapping` (
  `MobileNumber` VARCHAR(15) ,		-- ASHOK1: MOBILE NO IS only PK defined and is VARCHAR 
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`MobileNumber`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSParkingPermissionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSParkingPermissionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSParkingPermissionMapping` (
  `EMS6Id` BIGINT UNSIGNED,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSRateMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSRateMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSRateMapping` (
  `EMS6Id` BIGINT UNSIGNED ,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EventLogNewMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventLogNewMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventLogNewMapping` (
  `PaystationId` MEDIUMINT UNSIGNED,
  `DeviceId` TINYINT  UNSIGNED,
  `TypeId` INT  UNSIGNED,
  `ActionId` TINYINT UNSIGNED ,
  `DateField` DATETIME ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`DeviceId`, `TypeId`, `ActionId`, `DateField`  ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ModemSettingMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ModemSettingMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ModemSettingMapping` (
  `EMS6Id` INT  UNSIGNED,		-- ASHOK2: Actual Column name in EMS6 is PaystationId
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RestSessionTokenMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestSessionTokenMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestSessionTokenMapping` (
  `AccountName` VARCHAR(32) ,	-- ASHOK3: Column name in EMS6 is AccountName of VARCHAR datatype
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`AccountName`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ReplenishMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ReplenishMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ReplenishMapping` (
  `PaystationId` MEDIUMINT UNSIGNED,
  `Date` DATETIME ,
  `Number` INT UNSIGNED,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`Date`,`Number` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ReversalMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ReversalMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ReversalMapping` (
  `MerchantAccountId` INT  ,
  `OriginalReferenceNumber` VARCHAR(30) ,
  `OriginalTime` VARCHAR(30) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`MerchantAccountId`,`OriginalReferenceNumber`, `OriginalTime` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SMSAlertMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SMSAlertMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SMSAlertMapping` (
  `MobileNumber` VARCHAR(15) ,	-- ASHOK3: Only PK is defined as Varchar
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`MobileNumber`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`PurchaseTaxMapping`
-- -----------------------------------------------------
/* ASHOK4: This table deos not exists in Ashok's computer local database
DROP TABLE IF EXISTS `EMS7`.`PurchaseTaxMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PurchaseTaxMapping` (
  `Id` INT ,
  `EMS6Id` INT NULL DEFAULT NULL ,
  `EMS7Id` MEDIUMINT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`TransactionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TransactionMapping` ;

CREATE TABLE IF NOT EXISTS `EMS7`.`TransactionMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME ,
  `TicketNumber` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CustomerMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CustomerMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,  
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`PaystationMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`PaystationMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PaystationMapping` (
  `EMS6Id` MEDIUMINT(8) UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`RegionLocationMapping`
-- -----------------------------------------------------

-- ASHOK5: Table name convention ???

DROP TABLE IF EXISTS `EMS7`.`RegionLocationMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RegionLocationMapping` (
  `EMS6Id` INT  ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`LotSettingMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`LotSettingMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`LotSettingMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`ParkingPermissionMapping`
-- -----------------------------------------------------
-- ASHOK6: Table name in Ashok's computer local database is EMSParkingPermission

DROP TABLE IF EXISTS `EMS7`.`ParkingPermissionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ParkingPermissionMapping` (
  `EMS6Id` BIGINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`MerchantAccountMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`MerchantAccountMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`MerchantAccountMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`PreAuthMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`PreAuthMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PreAuthMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`PreAuthHoldingMapping`
-- -----------------------------------------------------
-- ASHOK7: This table does not exists on Ashok's local computer database 
/*
DROP TABLE IF EXISTS `EMS7`.`PreAuthHoldingMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PreAuthHoldingMapping` (
  `Id` INT ,
  `EMS6MerchantAccountId` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `EMS7MerchantAccountId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `AuthorizationNumber` VARCHAR(30) NOT NULL DEFAULT '' ,
  `ProcessorTransactionId` VARCHAR(30) NOT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`ExtensibleRateMapping`
-- -----------------------------------------------------
-- ASHOK8: This table does not exists on Ashok's local computer database
/*
CREATE  TABLE IF NOT EXISTS `EMS7`.`ExtensibleRateMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6RateId` bigint(20) UNSIGNED NULL DEFAULT NULL ,
  `EMS7RateId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `EMS6RateTypeId` VARCHAR(30) NOT NULL DEFAULT '' ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`DiscardedRecords`
-- -----------------------------------------------------

-- ASHOK11: Did not modified this table

CREATE  TABLE IF NOT EXISTS `EMS7`.`DiscardedRecords` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `MultiKeyId` VARCHAR(255) NULL DEFAULT '0' ,
  `EMS6Id` INT(15) NULL DEFAULT '0' ,
  `TableName` VARCHAR(25) NOT NULL ,
  `Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`EMS6AlertType`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`AlertTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AlertTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`CustomerAlertTypeMapping`
-- -----------------------------------------------------

-- ASHOK12: This table does not exits on Ashok's computer local database
/*
CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerAlertTypeMapping` (
  `Id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS7CustomerAlertTypeId` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `EMS6AlertId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `EMS6CustomerId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `AlertName` VARCHAR(40) NOT NULL DEFAULT '' ,
  `ModifiedDate` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`UserAccountMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`UserAccountMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`UserAccountMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`ServiceAgreementMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ServiceAgreementMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ServiceAgreementMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`ThirdPartyServiceAccountMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ThirdPartyServiceAccountMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ThirdPartyServiceAccountMapping` (
  `EMS6Id` MEDIUMINT(8) UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `EMS7`.`PaystationGroupRouteMapping`
-- -----------------------------------------------------

-- ASHOK13: This tables doesnot exists on Ashok's computer local database
/*
CREATE  TABLE IF NOT EXISTS `EMS7`.`PaystationGroupRouteMapping` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `EMS6PaystationGroupId` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `EMS7RouteId` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL ,
  `EMS6PaystationId` MEDIUMINT(8) NULL DEFAULT NULL ,
  `DateAdded` TIMESTAMP NOT NULL DEFAULT current_timestamp() ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`CardRetryTransactionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CardRetryTransactionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CardRetryTransactionMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT UNSIGNED ,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`, `TicketNumber`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RegionPaystationLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RegionPaystationLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RegionPaystationLogMapping` (
  `EMS6Id` MEDIUMINT  ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ServiceStateMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ServiceStateMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ServiceStateMapping` (
  `EMS6Id` INT  ,	-- ASHOK 14: IN EMS6 PK is PaystationId
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RestAccountMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestAccountMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestAccountMapping` (
  `AccountName` VARCHAR(32) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`AccountName`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RateToUnifiedRateMapping`
-- -----------------------------------------------------
-- ASHOK 15: comeback later
/*
CREATE  TABLE IF NOT EXISTS `EMS7`.`RateToUnifiedRateMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `PurchasedDate` DATETIME NULL DEFAULT NULL ,
  `TicketNumber` INT NULL DEFAULT NULL ,
  `EMS7UnifiedRateId` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`ProcessorPropertiesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS  `EMS7`.`ProcessorPropertiesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ProcessorPropertiesMapping` (
  `Processor` VARCHAR(40)  ,
  `Name` VARCHAR(40)  ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Processor`,`Name`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`AuditPOSCollectionMapping`
-- -----------------------------------------------------

-- ASHOK 16: This is same as AuditMapping right ?
/*
CREATE  TABLE IF NOT EXISTS `EMS7`.`AuditPOSCollectionMapping` (
  `Id` INT NOT NULL AUTO_INCREMENT ,
  `PaystationId` INT NULL DEFAULT NULL ,
  `CollectionTypeId` TINYINT NULL DEFAULT NULL ,
  `StartDate` DATETIME NULL DEFAULT NULL ,
  `EndDate` DATETIME NULL DEFAULT NULL ,
  `EMS7Id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;
*/

-- -----------------------------------------------------
-- Table `EMS7`.`RawSensorDataMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RawSensorDataMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RawSensorDataMapping` (
  `ServerName` VARCHAR(20) ,
  `PaystationCommAddress` VARCHAR(20) ,
  `Type` VARCHAR(40)  ,
  `Action` VARCHAR(40) ,
  `Information` VARCHAR(40) ,
  `DateField` DATETIME  ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`ServerName`,`PaystationCommAddress`, `Type`, `Action`,`Information`, `DateField` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ReversalArchiveMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ReversalArchiveMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ReversalArchiveMapping` (
  `MerchantAccountId` INT  ,
  `OriginalReferenceNumber` VARCHAR(30)  ,
  `OriginalTime` VARCHAR(30)  ,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`MerchantAccountId`, `OriginalReferenceNumber`, `OriginalTime` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SMSTransactionLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SMSTransactionLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SMSTransactionLogMapping` (
  `PaystationId` MEDIUMINT UNSIGNED,
  `PurchasedDate` DATETIME ,
  `TicketNumber` INT UNSIGNED,
  `SMSMessageTypeId` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId` ,`PurchasedDate`, `TicketNumber`, `SMSMessageTypeId` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`Transaction2PushMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`Transaction2PushMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`Transaction2PushMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT  UNSIGNED,
  `ThirdPartyServiceAccountId` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`, `ThirdPartyServiceAccountId` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`TransactionPushMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TransactionPushMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TransactionPushMapping` (
  `PaystationId` MEDIUMINT UNSIGNED,
  `PurchasedDate` DATETIME ,
  `TicketNumber` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`WsCallLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`WsCallLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`WsCallLogMapping` (
  `Token` VARCHAR(45)  ,
  `CallDate` DATETIME  ,
  `EMS7Id` INT  UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Token`, `CallDate`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RestLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestLogMapping` (
  `RESTAccountName` VARCHAR(32)  ,
  `EndpointName` VARCHAR(50)  ,
  `LoggingDate` DATE ,
  `IsError` TINYINT UNSIGNED,
  `CustomerId` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`RESTAccountName`,`EndpointName`, `LoggingDate`, `IsError`, `CustomerId` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ProcessorTransactionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ProcessorTransactionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ProcessorTransactionMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT UNSIGNED ,
  `Approved` TINYINT  ,
  `TypeId` TINYINT UNSIGNED ,
  `ProcessingDate` DATETIME ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`, `PurchasedDate`, `TicketNumber`, `Approved`, `TypeId`, `ProcessingDate`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CouponMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CouponMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CouponMapping` (
  `CustomerId` MEDIUMINT UNSIGNED ,
  `EMS6Id` VARCHAR(10) ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId` ,`EMS6Id` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RatesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RatesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RatesMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME ,
  `TicketNumber` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
 PRIMARY KEY (`PaystationId`,`PurchasedDate`, `TicketNumber` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMS7ExtensibleRateMapping`
-- -----------------------------------------------------
-- ASHOK 17: This table does not exists on Ashok's local computer
/*
DROP TABLE IF EXISTS `EMS7`.`EMS7ExtensibleRateMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMS7ExtensibleRateMapping` (
  `EMS6RateId` BIGINT(20) NULL ,
  `EMS7RateId` INT(11) NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;
*/
-- USE `EMS7_DB` ;

-- -----------------------------------------------------
-- Table `EMS7`.`RestLogTotalCallMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestLogTotalCallMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestLogTotalCallMapping` (
  `RESTAccountName` VARCHAR(32) ,
  `LoggingDate` DATE ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`RESTAccountName` ,`LoggingDate` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SMSFailedResponseMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SMSFailedResponseMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SMSFailedResponseMapping` (
  `PaystationId` INT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT UNSIGNED ,
  `SMSMessageTypeId` TINYINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber`,`SMSMessageTypeId`)
  )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `EMS7`.`AlertMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`AlertMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AlertMapping` (
  `EMS6Id` INT UNSIGNED ,	
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`AuditAccessMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`AuditAccessMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AuditAccessMapping` (
  `EMS6Id` INT UNSIGNED ,	
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`AuditAccessEventTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`AuditAccessEventTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AuditAccessEventTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED ,	
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`AuthoritiesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`AuthoritiesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`AuthoritiesMapping` (
  `EMS6Id` TINYINT UNSIGNED ,	
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`BadCardMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`BadCardMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`BadCardMapping` (
  `CustomerId` INT ,
  `AccountNumber` VARCHAR(44) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`, `AccountNumber`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`BadCreditCardMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`BadCreditCardMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`BadCreditCardMapping` (
  `CustomerId` INT UNSIGNED,
  `CardHash` VARCHAR(32) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`, `CardHash`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`BadSmartCardMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`BadSmartCardMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`BadSmartCardMapping` (
  `CustomerId` INT UNSIGNED,
  `CardNumber` VARCHAR(45) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`, `CardNumber`) )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `EMS7`.`BatteryInfoMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`BatteryInfoMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`BatteryInfoMapping` (
  `PaystationId` MEDIUMINT ,
  `DateField` DATETIME ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`, `DateField`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CardRetryTransactionTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CardRetryTransactionTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CardRetryTransactionTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CardTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CardTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CardTypeMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CCFailLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CCFailLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CCFailLogMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`CerberusAccountMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CerberusAccountMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CerberusAccountMapping` (
  `EMS6Id` MEDIUMINT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `EMS7`.`ClusterMemberMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`ClusterMemberMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ClusterMemberMapping` (
  `Name` VARCHAR(50) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Name`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`CustomerPermissionsMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`CustomerPermissionsMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerPermissionsMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`CustomerPropertiesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`CustomerPropertiesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`CustomerPropertiesMapping` (
  `CustomerId` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EMSParkingPermissionDayOfWeekMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSParkingPermissionDayOfWeekMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSParkingPermissionDayOfWeekMapping` (
  `EMSPermissionId` BIGINT UNSIGNED,
  `DayOfWeek` TINYINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMSPermissionId`,`DayOfWeek` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSParkingPermissionTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSParkingPermissionTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSParkingPermissionTypeMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EMSPropertiesMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`EMSPropertiesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSPropertiesMapping` (
  `Name` VARCHAR(40) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Name`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EMSRateDayOfWeekMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSRateDayOfWeekMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSRateDayOfWeekMapping` (
  `EMSRateId` BIGINT UNSIGNED ,
  `DayOfWeek` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMSRateId`,`DayOfWeek` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSRateTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSRateTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSRateTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`EMSSpecialParkingPermissionDateMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSSpecialParkingPermissionDateMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSSpecialParkingPermissionDateMapping` (
  `EMS6Id` BIGINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EMSSpecialRateDateMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EMSSpecialRateDateMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EMSSpecialRateDateMapping` (
  `EMS6Id` BIGINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EventNewActionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventNewActionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventNewActionMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `EMS7`.`EventNewDefinitionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventNewDefinitionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventNewDefinitionMapping` (
  `DeviceId` TINYINT UNSIGNED,
  `TypeId` TINYINT UNSIGNED,
  `ActionId` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`DeviceId`,`TypeId`,`ActionId` ) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EventNewDeviceMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventNewDeviceMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventNewDeviceMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EventNewSeverityMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventNewSeverityMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventNewSeverityMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`EventNewTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`EventNewTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`EventNewTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`LicenseMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`LicenseMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`LicenseMapping` (
  `Name` VARCHAR(45),
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Name`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`LotSettingFileContentMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`LotSettingFileContentMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`LotSettingFileContentMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`PaymentTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`PaymentTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PaymentTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`Paystation_PaystationGroupMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ???

DROP TABLE IF EXISTS `EMS7`.`Paystation_PaystationGroupMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`Paystation_PaystationGroupMapping` (
  `PaystationGroupId` INT UNSIGNED,
  `PaystationId` MEDIUMINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationGroupId`, `PaystationId`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`PaystationAlertMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`PaystationAlertMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PaystationAlertMapping` (
  `AlertId` INT UNSIGNED,
  `PaystationId` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`AlertId`, `PaystationId`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`PaystationGroupMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`PaystationGroupMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`PaystationGroupMapping` (
  `EMS6Id` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`ProcessorMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`ProcessorMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ProcessorMapping` (
  `Name` VARCHAR(40) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Name`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`ProcessorTransactionTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ProcessorTransactionTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ProcessorTransactionTypeMapping` (
  `EMS6Id` TINYINT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RawSensorDataArchiveMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`RawSensorDataArchiveMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RawSensorDataArchiveMapping` (
  `ServerName` VARCHAR(20) ,
  `PaystationCommAddress` VARCHAR(20) ,
  `Type` VARCHAR(40)  ,
  `Action` VARCHAR(40) ,
  `Information` VARCHAR(40) ,
  `DateField` DATETIME  ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`ServerName`,`PaystationCommAddress`, `Type`, `Action`,`Information`, `DateField` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RegionMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RegionMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RegionMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ReplenishTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ReplenishTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ReplenishTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`RestActionTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestActionTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestActionTypeMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RestPropertiesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`RestPropertiesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RestPropertiesMapping` (
  `Name` VARCHAR(50) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Name`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`RoleMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`RoleMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`RoleMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SCRetryMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SCRetryMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SCRetryMapping` (
  `PaystationId` MEDIUMINT ,
  `PurchasedDate` DATETIME ,
  `TicketNumber` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`, `PurchasedDate`, `TicketNumber`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`SCTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SCTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SCTypeMapping` (
  `Type` VARCHAR(20) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`Type`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`SensorLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SensorLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SensorLogMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `TypeId` TINYINT UNSIGNED ,
  `DateField` DATETIME ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`TypeId`, `DateField` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SensorTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SensorTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SensorTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ServiceAgreedCustomerMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ServiceAgreedCustomerMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ServiceAgreedCustomerMapping` (
  `CustomerId` INT  ,
  `ServiceAgreementId` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`, `ServiceAgreementId` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`SMSMessageTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`SMSMessageTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`SMSMessageTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`TaxesMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TaxesMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TaxesMapping` (
  `EMS6Id` BIGINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`ThirdPartyPaystationSequenceMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ThirdPartyPaystationSequenceMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ThirdPartyPaystationSequenceMapping` (
  `PaystationId` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ThirdPartyPushLogMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ThirdPartyPushLogMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ThirdPartyPushLogMapping` (
  `ActionTypeId` TINYINT UNSIGNED ,
  `DateField` DATETIME ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`ActionTypeId`,`DateField` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ThirdPartyServicePaystationMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ThirdPartyServicePaystationMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ThirdPartyServicePaystationMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `ThirdPartyServiceAccountId` INT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`ThirdPartyServiceAccountId` ) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`ThirdPartyServiceStallSequenceMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ThirdPartyServiceStallSequenceMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ThirdPartyServiceStallSequenceMapping` (
  `RegionId` INT UNSIGNED ,
  `StallNumber` MEDIUMINT UNSIGNED ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`RegionId`,`StallNumber` ) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`TicketFooterMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TicketFooterMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TicketFooterMapping` (
  `EMS6Id` INT ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`TransactionArchiveMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`TransactionArchiveMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TransactionArchiveMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT  UNSIGNED,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`TransactionCouponMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TransactionCouponMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TransactionCouponMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `PurchasedDate` DATETIME  ,
  `TicketNumber` INT  UNSIGNED,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`PurchasedDate`,`TicketNumber` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`TransactionLotsettingsMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TransactionLotsettingsMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TransactionLotsettingsMapping` (
  `PaystationId` MEDIUMINT UNSIGNED ,
  `LotSetting` VARCHAR(20)  ,
  `EMS7Id` INT UNSIGNED ,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`PaystationId`,`LotSetting` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`TransactionTypeMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`TransactionTypeMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`TransactionTypeMapping` (
  `EMS6Id` TINYINT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`UserAccountAccessLockMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`UserAccountAccessLockMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`UserAccountAccessLockMapping` (
  `CustomerName` VARCHAR(255),
  `UserAccountName` VARCHAR(40) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`UserAccountName`,`CustomerName` ) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`UserAccountPermissionsMapping`
-- -----------------------------------------------------
-- NOT REQUIRED ??

DROP TABLE IF EXISTS `EMS7`.`UserAccountPermissionsMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`UserAccountPermissionsMapping` (
  `EMS6Id` INT,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EMS7`.`ValidCustomCardMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`ValidCustomCardMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`ValidCustomCardMapping` (
  `CustomerId` INT,
  `CardNumber` VARCHAR(37) ,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`CustomerId`,`CardNumber`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `EMS7`.`WSEndpointMapping`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `EMS7`.`WSEndpointMapping` ;

CREATE  TABLE IF NOT EXISTS `EMS7`.`WSEndpointMapping` (
  `EMS6Id` INT UNSIGNED,
  `EMS7Id` INT UNSIGNED,
  `ModifiedDate` DATETIME ,
  PRIMARY KEY (`EMS6Id`) )
ENGINE = InnoDB;
