#!/usr/bin/python
import csv
import sys, getopt

def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print 'test.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -i <inputfile> -o <outputfile>'
         sys.exit()
      if opt in ("-i", "--ifile"):
         inputfile = arg
	 PaystationList = open(inputfile, "r")
      if opt in ("-o", "--ofile"):
         outputfile = arg
	 FieldTrialReport = open(outputfile,"w")
         PaystationList.readline()
         for Paystation in PaystationList:
         	print Paystation
		s=str(Paystation)
		FieldTrialReport.write(s)
   PaystationList.close()

   print 'Input file is "', inputfile
   print 'Output file is "', outputfile

if __name__ == "__main__":
   main(sys.argv[1:])
