import abc

class TestBase(object):
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def foo(self):
		"""FOO"""
		return

	def bar(self):
		self.foo()

class TestConcrete(TestBase):
	def foo(self):
		print 'foo'

	def baz(self):
		print 'baz'

#TestBase.register(TestConcrete)

tc = TestConcrete()
tc.bar()
