from datetime import date

class EMS7DataAccess:
	def __init__(self, EMS7Connection, verbose):
		self.__verbose = verbose
		self.__EMS7Connection = EMS7Connection
		self.__EMS7Cursor = EMS7Connection.cursor()

	def addCustomer(self, customer, Action):
		self.__printCustomer(customer)
		PaystationSettingName='None'
		UnifiedRateName='Unknown'
		AlertTypeId=12 #AlertTypeId=12 is defined as PaystationAlert
		Threshold = 1
		IsParent = 0
        	CustomerTypeId = customer[0]
	        CustomerAccountStatus = customer[1]
        	Name = customer[2]
		Version = customer[3]
		LastModifiedDate = customer[4]
		LastModifiedByUser = customer[5]
		CustomerId = customer[6]
		if(Action=='Insert'):
			self.__EMS7Cursor.execute("insert into Customer(CustomerTypeId,CustomerStatusTypeId,Name,VERSION,IsParent,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(CustomerTypeId,CustomerAccountStatus,Name,Version,IsParent, LastModifiedDate,LastModifiedByUser))
			EMS7CustomerId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert ignore into CustomerMapping(EMS7CustomerId,EMS6CustomerId,EMS7CustomerName,EMS6CustomerName) values(%s,%s,%s,%s)",(EMS7CustomerId,CustomerId,Name,Name))
			# Adding a PaystationAlert record for each Customer in CustomerAlertType table
			self.__EMS7Cursor.execute("insert into CustomerAlertType(CustomerId,AlertTypeId,Name,Threshold,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,AlertTypeId,'Pay Station Alert',Threshold,LastModifiedDate,LastModifiedByUser))
			EMS7CustomerAlertTypeId=self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into CustomerAlertTypeMapping(EMS7CustomerAlertTypeId,EMS6AlertId,EMS6CustomerId,AlertName,ModifiedDate) values(%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,AlertTypeId,CustomerId,'PaystationAlert',LastModifiedDate))
			self.__EMS7Cursor.execute("insert into PaystationSetting(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,PaystationSettingName,LastModifiedDate,LastModifiedByUser))
			self.__EMS7Cursor.execute("insert into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,UnifiedRateName,LastModifiedDate,LastModifiedByUser))
#insert two rows for each Customer for Credit Card and Smartcard from EMS7.cardtype table. look for Customer Cardtype default values in stpred procedure
#Question From Paul, What did you mean by Setting the Name to be Unknown, did you mean replace created by DPT Locked with  a value of Unknown
			self.__EMS7Cursor.execute("insert into CustomerCardType(CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern, Description, IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,1,0,'Credit Card','','Created by DPT. Locked. Cannot be changed.',0,1,0,LastModifiedDate,LastModifiedByUser))
			self.__EMS7Cursor.execute("insert into CustomerCardType(CustomerId, CardTypeId, AuthorizationTypeId, Name, Track2Pattern, Description, IsDigitAlgorithm, IsLocked, VERSION, LastModifiedGMT, LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,2,0,'Smart Card','','Created by DPT. Locked. Cannot be changed.',0,1,0,LastModifiedDate,LastModifiedByUser))
			self.__EMS7Connection.commit()

		if(Action=='Update'):
			EMS7CustomerId = self.__getEMS7CustomerId(CustomerId)	
			self.__EMS7Cursor.execute("update Customer set Name='%s', VERSION=%s, CustomerStatusTypeId=%s where Id=%s" %(Name,Version,CustomerAccountStatus,EMS7CustomerId))
			self.__EMS7Connection.commit()

	def addTrialCustomerToEMS7(self, Customer, Action):
		self.__printTrialCustomer(Customer)
		CustomerStatusTypeId=2
		EMS6CustomerId=Customer[0]
		Status=Customer[1]
		Expiry=Customer[2]
		EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("update Customer set TrialExpiryGMT=%s, CustomerStatusTypeId=%s where Id=%s  ",(Expiry,CustomerStatusTypeId,EMS7CustomerId))
		self.__EMS7Connection.commit()
		
	def __printTrialCustomer(self, Customer):
		print " -- Customer is %s" %Customer[0]
		print " -- Status %s" %Customer[1]
		print " -- Expiry %s" %Customer[2]
		
	def __printCustomer(self, customer):
		print " -- CustomerTypeId %s" %customer[0]
		print " -- CustomerAccountStatus %s" %customer[1]
		print " -- Name %s" %customer[2]
		print " -- Version %s" %customer[3]
		print " -- LastModifiedDate %s" %customer[4]
		print " -- LastModifiedByUser %s" %customer[5]

	def addCustomerPropertyToEMS7(self, CustomerProperty, Action):
		tableName='CustomerProperty'
		MaxUserAccounts = 99
		CCOfflineRetry = 3
		TimeZone = 'GMT'
		SMSWarningPeriod = 10
		MaxOfflineRetry = 5
		VERSION = 0
		LastModifiedGMT = date.today() 
                LastModifiedByUserId = 1
		self.__printCustomerProperties(CustomerProperty)
		EMS6CustomerId = CustomerProperty[0]
		MaxUserAccounts = CustomerProperty[1]
		CCOfflineRetry = CustomerProperty[2]
		MaxOfflineRetry = CustomerProperty[3]
		TimeZone = CustomerProperty[4]
		SMSWarningPeriod = CustomerProperty[5]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		#There are 5 customer property in ems7 that require a value, if it does not come 6 then we assign a default value only for tghe ones specified in paul stored, leave the rest
		if (EMS7CustomerId):
			if (MaxUserAccounts):
				if(Action=='Insert'):
					# "**** MaxUserAccount"
					CustomerPropertyTypeId = 3
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxUserAccounts,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				# Insert Default value.
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxUserAccounts,VERSION,LastModifiedGMT,LastModifiedByUserId)

			if (MaxUserAccounts and Action=='Update'):
				CustomerPropertyTypeId = 3
                                self.__updateCustomerProperty(MaxUserAccounts,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)
				self.__EMS7Connection.commit()

			if(CCOfflineRetry):
				if(Action=='Insert'):
					#  "**** CCOfflineRetry" 
					CustomerPropertyTypeId = 4
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,CCOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				# Insert a Default Value.
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,CCOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId)

			if(CCOfflineRetry and Action=='Update'):
                                CustomerPropertyTypeId = 4
                                self.__updateCustomerProperty(CCOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)

			if(MaxOfflineRetry):
				if(Action=='Insert'): 
					# "**** MaxOfflineRetry" 
					CustomerPropertyTypeId = 5
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,MaxOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId)

			if(MaxOfflineRetry and Action=='Update'):
				CustomerPropertyTypeId = 5
				vself.__updateCustomerProperty(MaxOfflineRetry,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)

			if(TimeZone):
				if(Action=='Insert'):
					# "**** TimeZone" 
					CustomerPropertyTypeId = 1
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,TimeZone,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,TimeZone,VERSION,LastModifiedGMT,LastModifiedByUserId)


			if(TimeZone and Action=='Update'):
				CustomerPropertyTypeId = 1
				self.__updateCustomerProperty(TimeZone,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)

			if(SMSWarningPeriod):
				if(Action=='Insert'):
					# "**** SMSWarningPeriod" 
					CustomerPropertyTypeId = 6
					self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,SMSWarningPeriod,VERSION,LastModifiedGMT,LastModifiedByUserId)
			else:
				self.__insertCustomerProperty(EMS7CustomerId,CustomerPropertyTypeId,SMSWarningPeriod,VERSION,LastModifiedGMT,LastModifiedByUserId)
			
			if(SMSWarningPeriod and Action=='Update'):
				CustomerPropertyTypeId = 6
				# "**** SMSWarningPeriod"
				self.__updateCustomerProperty(SMSWarningPeriod,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId)
				self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def __InsertIntoDiscardedRecords(self,EMS6Id,TableName,IsMultiKey):
		LastModifiedGMT=date.today()
		if(IsMultiKey==0):
			self.__EMS7Cursor.execute("insert into DiscardedRecords(EMS6Id,TableName,Date) values(%s,%s,%s)",(EMS6Id,TableName,LastModifiedGMT))
			self.__EMS7Connection.commit()
		else:
			self.__EMS7Cursor.execute("insert into DiscardedRecords(MultiKeyId,TableName,Date) values(%s,%s,%s)",(EMS6Id,TableName,LastModifiedGMT))

	def deleteCustomerPropertyFromEMS7(self, EMS6Id, Action):
		EMS7CustomerId=self.__getEMS7CustomerId(EMS6Id)
		self.__EMS7Cursor.execute("delete from CustomerProperty where CustomerId=%s",EMS7CustomerId)
		self.__EMS7Connection.commit()

	def addCustomerSubscriptionToEMS7(self, CustomerSubscription, Action):
		self.__printCustomerSubscription(CustomerSubscription)
		tableName='CustomerSubscription'
                VERSION = 0
                LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
                EMS6CustomerId = CustomerSubscription[0]
                ServiceId = CustomerSubscription[1]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
                if (EMS7CustomerId):
			SubscriptionTypeId = self.__getSubscriptionTypeId(ServiceId)
			if(Action=='Insert'):
				if (SubscriptionTypeId):
					self.__CustomerSubscriptionEnable(EMS7CustomerId,SubscriptionTypeId,VERSION,LastModifiedGMT,LastModifiedByUserId)
			if(Action=='Update'):
				self.__EMS7Cursor.execute("update CustomerSubscription set SubscriptionTypeId=%s, IsEnabled=%s, VERSION=%s, LastModifiedGMT=%s, LastModifiedByUserId=%s where CustomerId=%s",(SubscriptionTypeId,IsEnabled, VERSION, LastModifiedGMT, LastModifiedByUserId, EMS7CustomerId))
				self.__EMS7Connection.commit()
			if(Action=='Delete'):
				self.__EMS6Cursor.execute("delete from CustomerSubscription where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))
				self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def __printCustomerSubscription(self,CustomerSubscription):
		print " -- EMS6CustomerId %s" %CustomerSubscription[0]
		print " -- ServiceId %s" %CustomerSubscription[1]

	def __getSubscriptionTypeId(self, ServiceId):
		if (ServiceId==1):
                        SubscriptionTypeId=100
                if (ServiceId==2):
                        SubscriptionTypeId=600
                if (ServiceId==3):
                        SubscriptionTypeId=200
                if(ServiceId==4):
                        SubscriptionTypeId=700
                if(ServiceId==5):
                        SubscriptionTypeId=300
                if(ServiceId==6):
                        SubscriptionTypeId=1000
                if(ServiceId==7):
                        SubscriptionTypeId=1200
                if(ServiceId==8):
                        SubscriptionTypeId=900
		return SubscriptionTypeId	

# Procedure to enable Customer Subscription
	def __CustomerSubscriptionEnable(self,EMS7CustomerId,SubscriptionTypeId,VERSION,LastModifiedGMT,LastModifiedByUserId):
		IsEnabled=1	
		self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId))	
		self.__EMS7Connection.commit()

# Procedure to set the default value of disabled for the records that do not have a value, This procedure should always be called after the CustomerSubscription Module has been run, This module is being called in EMS6Migration.py(CustomerSubscription) Module right after the loop
	def CustomerSubscriptionDisable(self):
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
		VERSION=0
		IsEnabled=0
		self.__EMS7Cursor.execute("select c.Id from CustomerSubscription cs right join Customer c on(cs.CustomerId=c.Id) where cs.CustomerID is null")
		CustomerList=self.__EMS7Cursor.fetchall()
		for Customer in CustomerList:
			EMS7CustomerId=Customer[0]
			count=100
			while (count<=1200):
				SubscriptionTypeId=count
				self.__EMS7Cursor.execute("insert into CustomerSubscription(CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,SubscriptionTypeId,IsEnabled,VERSION,LastModifiedGMT,LastModifiedByUserId))	
				count=count+100
				self.__EMS7Connection.commit()
			
	def __getEMS7CustomerId(self, EMS6CustomerId):
		EMS7CustomerId = None
		self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(EMS6CustomerId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerId = row[0]
		return EMS7CustomerId
	
 	def __getLocationId(self, RegionId):
                LocationId = None
		self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(RegionId))
		if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
			LocationId = row[0]
		return LocationId

	def __getLicencePlateId(self, PlateNumber):
		LicencePlateId = None
		if (PlateNumber):
			self.__EMS7Cursor.execute("select Id from LicencePlate where Number=%s",(PlateNumber))	
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				LicencePlateId = row[0]
		return LicencePlateId

	def __printCustomerProperties(self, CustomerProperty):
		print " -- CustomerId %s " %CustomerProperty[0]
		print " -- MaxUserAccounts %s " %CustomerProperty[1]
		print " -- CCOfflineRetry %s " %CustomerProperty[2]
		print " -- MaxOfflineRetry %s " %CustomerProperty[3]
		print " -- TimeZone %s " %CustomerProperty[4]
		print " -- SMSWarningPeriod %s " %CustomerProperty[5]

	def __insertCustomerProperty(self, EMS7CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId):
		self.__EMS7Cursor.execute("insert ignore into CustomerProperty(CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,CustomerPropertyTypeId,PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __updateCustomerProperty(self, PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId):
		self.__EMS7Cursor.execute("update CustomerProperty set PropertyValue=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where CustomerId=%s and CustomerPropertyTypeId=%s", (PropertyValue,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,CustomerPropertyTypeId))
		self.__EMS7Connection.commit()

	def addLotSettingToEMS7(self, lotSetting):
		self.__printLotSetting(lotSetting)
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		version = lotSetting[0]
		EMS6CustomerId=lotSetting[1]
		Name = lotSetting[2]
		UniqueId = lotSetting[3]
		PaystationType = lotSetting[4]
		ActivationDate = lotSetting[5]
		TimeZone = lotSetting[6]
		FileLocation = lotSetting[7]
		UploadDate = lotSetting[8]
		EMS6LotSettingId = lotSetting[9]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("insert into SettingsFile(CustomerId,Name,UniqueIdentifier,UploadGMT,ActivationGMT,FileName,VERSION) values(%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,Name,UniqueId,UploadDate,ActivationDate,FileLocation,version))
		EMS7LotSettingId = self.__EMS7Cursor.lastrowid
		self.__EMS7Cursor.execute("insert into LotSettingMapping(EMS6LotSettingId,EMS7LotSettingId,EMS6CustomerId,EMS7CustomerId) values(%s,%s,%s,%s)",(EMS6LotSettingId,EMS7LotSettingId,EMS6CustomerId,EMS7CustomerId))
		if (FileLocation):
			self.__EMS7Cursor.execute("insert into SettingsFileContent(SettingsFileId,Content) values(%s,%s)",(EMS7LotSettingId,FileLocation))
			self.__EMS7Connection.commit()
		else:
			self.__EMS7Cursor.execute("insert into DiscardedRecords(EMS6Id,TableName,Date) values(%s,%s,%s)",(EMS6LotSettingId,'SettingsFileContent',LastModifiedGMT))
                        self.__EMS7Connection.commit()
		self.__EMS7Cursor.execute("insert ignore into PaystationSetting(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,Name,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printLotSetting(self, lotSetting):
		if (self.__verbose == 1):
			print " -- Version %s" %lotSetting[0]
			print " -- CustomerId %s" %lotSetting[1]
			print " -- Name %s "%lotSetting[2]
			print " -- UniqueId %s " %lotSetting[3]
			print " -- PaystationType %s" %lotSetting[4]
			print " -- ActivationDate %s" %lotSetting[5]
			print " -- TimeZone %s " %lotSetting[6]
			print " -- FileLocation %s " %lotSetting[7]

	def addPaystationToEMS7(self, paystation, Action):
		tableName='Paystation'
		self.__printPaystation(paystation)
		IsDeleted = 1
		LastModifiedGMT = date.today()
		ModifiedDate = date.today()
		LastModifiedByUserId = 1
		LotSetting = None
		Location = None
		EMS6PaystationId = paystation[0]
		PaystationType = paystation[1] 
		ModemTypeId = paystation[2]
		SerialNumber = paystation[3]
		Version = paystation[4]
		Name = paystation[5]
		ProvisionDate = paystation[6]
		EMS6CustomerId = paystation[7]
		RegionId = paystation[8]
		LotSetting = paystation[9]
		if (Action=='Insert'):
			self.__EMS7Cursor.execute("insert into Paystation(PaystationTypeId,ModemTypeId,SerialNumber,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(PaystationType,ModemTypeId,SerialNumber,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
			EMS7PaystationId = self.__EMS7Cursor.lastrowid
			print " >> Adding a record to the PointOfSale Table"
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			EMS7LotSettingId = self.__getEMS7LotSettingId(LotSetting)
			EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)	
			EMS7LocationId = self.__getLocationId(RegionId)
			self.__EMS7Cursor.execute("insert into PaystationMapping(EMS7PaystationId,EMS6PaystationId,EMS6CustomerId,SerialNumber,Name,ProvisionDate,ModifiedDate,Version,RegionId,LotSettingId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7PaystationId,EMS6PaystationId,EMS6CustomerId,SerialNumber,Name,ProvisionDate,ModifiedDate,Version,RegionId,LotSetting))
			self.__EMS7Connection.commit()
			if (EMS7CustomerId):
				self.__EMS7Cursor.execute("insert into PointOfSale(CustomerId,PaystationId,PaystationSettingId,LocationId,Name,SerialNumber,ProvisionedGMT,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EMS7PaystationId,EMS7LotSettingId,EMS7LocationId,Name,SerialNumber,ProvisionDate,Version,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=0
				self.__InsertIntoDiscardedRecords(EMS6CustomerId,'PointOfSale',IsMultiKey)
		if (Action == 'Delete'):
			EMS6PaystationId=int(paystation[0])
			EMS7PaystationId = self.__getEMS7PaystationId(EMS6PaystationId)		
			EMS7PaystationId = EMS7PaystationId[0]
			if(EMS7PaystationId):
				#Waiting for further clarification as a Paystation is not supposed to be deleted upon any delete triggers 
#				self.__EMS7Cursor.execute("delete from Paystation where Id=%s", paystation)
				self.__EMS7Connection.commit()
		if(Action == 'Update'):
                        EMS7PaystationId = self.__getEMS7PaystationId(EMS6PaystationId)
			self.__EMS7Cursor.execute("update Paystation set PaystationTypeId=%s, ModemTypeId=%s,SerialNumber=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(PaystationType,ModemTypeId,SerialNumber,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,EMS7PaystationId[0]))
			self.__EMS7Connection.commit()

	def __printPaystation(self, paystation):
		print " -- Paystation Id %s" %paystation[0]
		print " -- PaystationType %s " %paystation[1]
		print " -- Modem Type %s" %paystation[2]
		print " -- CommAddress %s" %paystation[3]
		print " -- Version %s" %paystation[4]
		print " -- Name %s" %paystation[5]
		print " -- ProvisionDate %s" %paystation[6]
		print " -- CustomerId %s" %paystation[7]
		print " -- RegionId %s" %paystation[8]
		print " -- LotSettingId %s" %paystation[9]

	def addLocationToEMS7(self, Location):
		self.__printLocation(Location)
		IsDeleted = 1
		LastModifiedGMT = date.today()
		ModifiedDate = date.today()
		LastModifiedByUserId = 1
		TargetMonthlyRevenueAmount = 0
		NumberOfSpaces = 0
		IsParent = 0
		ParentLocationId=None
		IsUnassigned = 0	
		EMS6RegionId = Location[7]
		Version = Location[0]
		AlarmState = Location[1]
		EMS6CustomerId = Location[2]
		Name = Location[3]
		Description = Location[4]
		ParentRegion = Location[5]
		if (ParentRegion==0):
			ParentLocationId=None
		elif (ParentRegion<>0 or ParentRegion<>None):
			ParentLocationId = self.__getLocationId(ParentRegion)
		else:
			ParentLocationId=None
		IsDefault = Location[6]
		EMS7CustomerID = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("Insert into Location(CustomerId,ParentLocationId,Name,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (EMS7CustomerID,ParentLocationId,Name,NumberOfSpaces,TargetMonthlyRevenueAmount,Description,IsParent,IsUnassigned,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
		EMS7LocationId = self.__EMS7Cursor.lastrowid
		self.__EMS7Cursor.execute("insert into RegionLocationMapping(EMS7LocationId,EMS6RegionId,Name,ModifiedDate) values(%s,%s,%s,%s)",(EMS7LocationId,EMS6RegionId,Name,ModifiedDate))
		self.__EMS7Connection.commit()

	def __printLocation(self,Location):
		print " -- Region Id %s " %Location[7]
		print " -- Version %s " %Location[0]
		print " -- AlarmState %s " %Location[1]
		print " -- EMS6CustomerId %s " %Location[2]
		print " -- Name %s " %Location[3]
		print " -- Description %s " %Location[4]
		print " -- ParentRegion %s " %Location[5]
		print " -- IsDefault %s " %Location[6]

	def addCouponToEMS7(self, coupon):
		self.__printCoupon(coupon)
		IsDeleted = 1
		Version = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustID = 0	
		EMS6CustomerId = coupon[0]
		CouponId = coupon[1]
		PercentageDiscount = coupon[2]
		StartDate = coupon[3]
		EndDate = coupon[4]
		NumUses = coupon[5]
		RegionId = coupon[6]
		StallRange = coupon[7]
		Description = coupon[8]
		IsPndEnabled = coupon[9]
		IsPbsEnabled = coupon[10]
		EMS7CustID = self.__getEMS7CustomerId(EMS6CustomerId)
		EMS7Location = self.__getLocationId(RegionId)
		self.__EMS7Cursor.execute("insert into Coupon(CustomerId,Coupon,LocationId,PercentDiscount,StartDateLocal,EndDateLocal,NumberOfUsesRemaining,SpaceRange,Description,IsPndEnabled,IsPbsEnabled,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (EMS7CustID,CouponId,EMS7Location,PercentageDiscount,StartDate,EndDate,NumUses,StallRange,Description,IsPndEnabled,IsPbsEnabled,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
		EMS7CouponId=self.__EMS7Cursor.lastrowid
		self.__EMS7Cursor.execute("insert into CouponMapping(EMS6CouponId,EMS7CouponId,NumUses,EMS6CustomerId) values(%s,%s,%s,%s)",(CouponId,EMS7CouponId,NumUses,EMS6CustomerId))
		self.__EMS7Connection.commit()
	
	def __printCoupon(self, Coupon):
		print " -- CustomerId %s" %Coupon[0]
		print " -- CouponId %s" %Coupon[1]
		print " -- Percentage Discount %s" %Coupon[2]
		print " -- StartDate %s" %Coupon[3]
		print " -- EndDate %s" %Coupon[4]
		print " -- Num uses %s" %Coupon[5]
		print " -- RegionId %s" %Coupon[6]
		print " -- StallNumber %s" %Coupon[7]
		print " -- Description %s" %Coupon[8]
		print " -- isPndEnabled %s" %Coupon[9]
		print " -- IsPbsEnabled %s" %Coupon[10]

	def addParkingPermissionTypeToEMS7(self, permissiontype):
		self.__printParkingPermissionType(permissiontype)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		self.__EMS7Cursor.execute("Insert into ParkingPermissionType(Id,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)", (permissiontype[0],permissiontype[1],LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printParkingPermissionType(self, permissiontype):
		print "ParkingPermisisonId %s" %permissiontype[0]
		print "ParkingPermissionName %s" %permissiontype[1]

	def addParkingPermissionToEMS7(self, ParkingPermission):
		tableName='ParkingPermission'
		self.__printParkingPermission(ParkingPermission)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7LocationID = 0
		Version = 0
		SpecialParkingPermissionId='NULL'
		Name = ParkingPermission[0]
		BeginHourLocal = ParkingPermission[1]
		BeginMinuteLocal = ParkingPermission[2]	
		EndHourLocal = ParkingPermission[3]
		EndMinuteLocal = ParkingPermission[4]
		RegionId = ParkingPermission[5]
		PermissionTypeId = ParkingPermission[6]
		PermissionStatus = ParkingPermission[7]
		SpecialParkingPermissionId = ParkingPermission[8]
		MaxDurationMinutes = ParkingPermission[9]
		CreationDate = ParkingPermission[10]
		IsActive = ParkingPermission[11]
		EMS6ParkingPermissionId = ParkingPermission[12]
		EMS7LocationId = self.__getLocationId(RegionId)
		if (EMS7LocationId):
			self.__EMS7Cursor.execute("insert into ParkingPermission(LocationId,ParkingPermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,VERSION,CreatedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7LocationId,PermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,MaxDurationMinutes,PermissionStatus,IsActive,Version,CreationDate,LastModifiedGMT,LastModifiedByUserId))
			EMS7ParkingPermissionId=self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into ParkingPermissionMapping(EMS6ParkingPermissionId,EMS7ParkingPemissionId,Name) values(%s,%s,%s)",(EMS6ParkingPermissionId,EMS7ParkingPermissionId,Name))
			self.__EMS7Connection.commit()
		else:
			## Note that SpecialParkingPermissionId has not been imported into the ParkingPermission table because there are no rows in the table for this column in EMS6, the table EMSSpecialParkingPermission is empty as well.
			# There are no matching RegionId in the Mapping table as the Region Id is infact missing in the Region Table, but it is present in ParkingPermission Table.... These records are usually test records..these records are now being inserted as a location Id 2667 which is assigned to DPT until further clarification ...
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6ParkingPermissionId,tableName,IsMultiKey)	
			EMS7LocationID=2667
			self.__EMS7Cursor.execute("insert into ParkingPermission(LocationId,ParkingPermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,MaxDurationMinutes,IsLimitedOrUnlimited,IsActive,VERSION,CreatedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7LocationID,PermissionTypeId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,MaxDurationMinutes,PermissionStatus,IsActive,Version,CreationDate,LastModifiedGMT,LastModifiedByUserId))
	
	def __printParkingPermission(self, ParkingPermission):
		print "Name %s" %ParkingPermission[0]
		print "StartTimeHourLocal %s" %ParkingPermission[1]
		print "StartTimeMinuteLocal %s" %ParkingPermission[2]
		print "EndTimeHourLocal %s" %ParkingPermission[3]
		print "EndTimeMInuteLocal %s" %ParkingPermission[4]
		print "RegionId %s" %ParkingPermission[5]
		print "PermissionTypeID %s" %ParkingPermission[6]
		print "PermissiontStatus %s" %ParkingPermission[7]
		print "SpecialParkingPermissionId %s" %ParkingPermission[8]
		print "MaxDurationMinutes %s" %ParkingPermission[9]
		print "CreationDate %s" %ParkingPermission[10]
		print "IsActive %s" %ParkingPermission[11]
		print "Id is %s" %ParkingPermission[12]

	def addParkingPermissionDayOfWeek(self, ParkingPermissionDayOfWeek):
		tableName='ParkingPermissionDayOfWeek'
		LastModifiedGMT = date.today()
		EMSPermissionId=ParkingPermissionDayOfWeek[0]
		DayOfWeek=ParkingPermissionDayOfWeek[1]
		self.__printParkingPermissionDayOfWeek(ParkingPermissionDayOfWeek)
		LastModifiedByUserId = 1
		VERSION = 1
		ParkingPermissionId=self.__getParkingPermissionId(EMSPermissionId)
		if(ParkingPermissionId):
			self.__EMS7Cursor.execute("insert into ParkingPermissionDayOfWeek(ParkingPermissionId,DayOfWeekId,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(ParkingPermissionId,ParkingPermissionDayOfWeek[1],VERSION,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()

		else:
			IsMultiKey=1
			MultiKeyId = (EMSPermissionId, DayOfWeek)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __getParkingPermissionId(self,ParkingPermissionDayOfWeek):
		ParkingPermissionId = None
		if (ParkingPermissionDayOfWeek):
			self.__EMS7Cursor.execute("select EMS7ParkingPemissionId from ParkingPermissionMapping where EMS6ParkingPermissionId=%s",(ParkingPermissionDayOfWeek))	
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				ParkingPermissionId = row[0]
		return ParkingPermissionId

	def __printParkingPermissionDayOfWeek(self, ParkingPermissionDayOfWeek):
		print " -- Parking Permission Day Of Weeek %s" %ParkingPermissionDayOfWeek[0]
		print " -- DayOfWeekId %s" %ParkingPermissionDayOfWeek[1]

	def addExtensibleRateDayOfWeekToEMS7(self, ExtensibleRate):
		self.__printExtensibleRateDayOfWeek(ExtensibleRate)
		tableName='ExtensibleRateDayOfWeek'
		IsMultiKey=None
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMSRateId=ExtensibleRate[0]
		DayOfWeek=ExtensibleRate[1]
		VERSION = 1
		ExtensibleRateId=self.__getExtensibleRateId(EMSRateId)
		if(ExtensibleRateId):
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("insert into ExtensibleRateDayOfWeek(ExtensibleRateId,DayOfWeekId,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s)",(EMSRateId,DayOfWeek,VERSION,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMSRateId,tableName,IsMultiKey)

	def __printExtensibleRateDayOfWeek(self, ExtensibleRate):
		print " -- EMSRateId %s" %ExtensibleRate[0]
		print " -- DayOfWeek %s" %ExtensibleRate[1]

	def __getExtensibleRateId(self,extensiblerate):
		EMS7ExtensibleRateId = None
		self.__EMS7Cursor.execute("select EMS7EMSRateId from ExtensibleRateMapping where EMS6EMSRateId=%s",(extensiblerate))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7ExtensibleRateId = row[0]
		return EMS7ExtensibleRateId

	def addExtensibleRatesToEMS7(self, rate):
		tableName='ExtensibleRates'
		IsMultiKey=None
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
		EMS6CustomerId = None
		UnifiedRateId = None
		self.__printExtensibleRates(rate)
		SpecialRateDateId = None
	 	Name = rate[0]
		RateTypeId = rate[1]
		SpecialRateId = rate[2]
		StartTimeHourLocal = rate[3]
		StartTimeMinuteLocal = rate[4]
		EndTimeHourLocal = rate[5]
		EndTimeMinuteLocal = rate[6]
		RegionId = rate[7]
		RateAmountCent = rate[8]
		ServiceFeeCent = rate[9]
		MinExtensionMinutes = rate[10]
		CreationDate = rate[11]
		IsActive = rate[12]
		EMSRateId=rate[13]
		EMS7LocationId = self.__getLocationId(RegionId)
		if (EMS7LocationId):
			self.__EMS7Cursor.execute("select CustomerId from Location where Id=%s", (EMS7LocationId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				EMS7CustomerId = row[0]
		if (EMS7LocationId):
			self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name=%s",(EMS7CustomerId,Name))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				UnifiedRateId = row[0]
		if (EMS7LocationId and UnifiedRateId):
			self.__EMS7Cursor.execute("Insert into ExtensibleRate(UnifiedRateId,LocationId,ExtensibleRateTypeId,SpecialRateDateId,Name,BeginHourLocal,BeginMinuteLocal,EndHourLocal,EndMinuteLocal,RateAmount,ServiceFeeAmount,MinExtensionMinutes,IsActive,CreatedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(UnifiedRateId, EMS7LocationId,RateTypeId,SpecialRateDateId,Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,IsActive,CreationDate,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
			EMS7ExtensibleRateId=self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into ExtensibleRateMapping(EMS6EMSRateId,EMS7EMSRateId,EMS6RateTypeId,DateAdded) values(%s,%s,%s,%s)",(EMSRateId,EMS7ExtensibleRateId,RateTypeId,LastModifiedGMT))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMSRateId,tableName,IsMultiKey)

	def __printExtensibleRates(self,rate):
		print " -- Name %s" %rate[0]
		print " -- rateTypeId %s" %rate[1]
		print " -- SpecialRateId %s " %rate[2]
		print " -- StartTimeHourlocal %s" %rate[3]
		print " -- StartTimeMinuteLocal %s " %rate[4]
		print " -- EndTimeHourLocal %s " %rate[5]
		print " -- EndTimeMinuteLocal %s" %rate[6]
		print " -- RegionId %s" %rate[7]
		print " -- RateAmountCent %s" %rate[8]
		print " -- ServiceFeeCent %s" %rate[9]
		print " -- MinExtensionMinutes %s" %rate[10]
		print " -- CreationDate %s" %rate[11]
		print " -- IsActive %s" %rate[12]

	def addLicencePlateToEMS7(self, LicencePlate):
		self.__printLicencePlate(LicencePlate)
		LicencePlate=LicencePlate[0]
		LastModifiedGMT = date.today()
		self.__EMS7Cursor.execute("insert ignore into LicencePlate(Number,LastModifiedGMT) values(%s,%s)",(LicencePlate,LastModifiedGMT))
		self.__EMS7Connection.commit()

	def __printMobileNumber(self,MobileNumber):
		print " -- MobileNumber %s" %MobileNumber

	def __printLicencePlate(self, LicencePlate):
		print " -- PlateNumber %s" %LicencePlate[0]

	def addMobileNumberToEMS7(self, MobileNumber):
		self.__printMobileNumber(MobileNumber)
		MobileNumber = MobileNumber[0]
		LastModifiedGMT = date.today()	
		self.__EMS7Cursor.execute("insert ignore into MobileNumber(Number,LastModifiedGMT) values(%s,%s)",(MobileNumber,LastModifiedGMT))
		self.__EMS7Connection.commit()

	def addExtensiblePermitToEMS7(self, ExtensiblePermit):
		self.__printExtensiblePermit(ExtensiblePermit)
		MobileNumber=ExtensiblePermit[0]
		CardData=ExtensiblePermit[1]
		Last4Digital=ExtensiblePermit[2]
		PurchasedDate=ExtensiblePermit[3]
		LatestExpiryDate=ExtensiblePermit[4]
		IsRFID=ExtensiblePermit[5]
		self.__EMS7Cursor.execute("select Id from MobileNumber where Number=%s", (MobileNumber))
		MobileNumberId=self.__EMS7Cursor.fetchone()
		self.__EMS7Cursor.execute("insert into ExtensiblePermit(MobileNumberId,CardData,Last4Digit,PermitBeginGMT,PermitExpireGMT,IsRFID) values(%s,%s,%s,%s,%s,%s)",(MobileNumberId[0],CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID))
		self.__EMS7Connection.commit()

	def __printExtensiblePermit(self,ExtensiblePermit):
		print " -- MobileNumber %s" %ExtensiblePermit[0]
		print " -- CardData %s " %ExtensiblePermit[1]
		print " -- Last4digit %s" %ExtensiblePermit[2]
		print " -- PurchaseDate %s" %ExtensiblePermit[3]
		print " -- LatestExpiryDate %s" %ExtensiblePermit[4]
		print " -- IsRFID %s" %ExtensiblePermit[5]

	def addSMSTransactionLogToEMS7(self, SMSTransactionLog):
		SMSMessageTypeId = SMSTransactionLog[0]
		MobileNumber = SMSTransactionLog[1]
		EMS6PaystationId = SMSTransactionLog[2]
		PurchasedDate = SMSTransactionLog[3]
		TicketNumber = SMSTransactionLog[4]
		ExpiryDate = SMSTransactionLog[5]
		UserResponse = SMSTransactionLog[6]
		TimeStamp = SMSTransactionLog[7]
		
		EMS7PaystationId = self.__getEMS7PaystationId(EMS6PaystationId)
		MobileNumberId = self.__getMobileNumberId(MobileNumber);
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)

		self.__EMS7Cursor.execute("insert into SmsTransactionLog(SMSMessageTypeId,MobileNumberId,PointOfSaleId,PurchaseGMT,TicketNumber,ExpiryDateGMT,ConsumerResponse,TimeStampGMT) values(%s,%s,%s,%s,%s,%s,%s,%s)",(SMSMessageTypeId,MobileNumberId,PointOfSaleId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp))
		self.__EMS7Connection.commit()

	def __printSMSTransactionLog(self, SMSTransactionLog):
		print " -- SMSMessageTypeId %s" %SMSTransactionLog[0]
		print " -- MobileNumber %s" %SMSTransactionLog[1]
		print " -- PaystationId %s" %SMSTransactionLog[2]
		print " -- PurchasedDate %s" %SMSTransactionLog[3]
		print " -- TicketNumber %s" %SMSTransactionLog[4]
		print " -- ExpiryDate %s" %SMSTransactionLog[5]
		print " -- UserResponse %s" %SMSTransactionLog[6]
		print " -- TimeStamp %s" %SMSTransactionLog[7]

	def __getEMS7PaystationId(self, EMS6PaystationId):
		EMS7PaystationId=None
		self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
		if(self.__EMS7Cursor.rowcount<>0):
			return self.__EMS7Cursor.fetchone()

	def __getPointOfSale(self, EMS6PaystationId):
		self.__EMS7Cursor.execute("select po.Id,po.CustomerId from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s",(EMS6PaystationId))	
		if (self.__EMS7Cursor.rowcount<>0):
			return self.__EMS7Cursor.fetchone()

	def __getMobileNumberId(self, MobileNumber):
		MobileNumberId = None
		if(MobileNumber<>None):
			self.__EMS7Cursor.execute("select distinct(Id) from MobileNumber where Number=%s",(MobileNumber))
			if (self.__EMS7Cursor.rowcount<>0):
				row = self.__EMS7Cursor.fetchone()
				MobileNumberId = row[0]
		return MobileNumberId

	def __getPointOfSaleId(self, EMS6PaystationId):
		PointOfSaleId = None
		self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(EMS6PaystationId))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			PointOfSaleId = row[0]
		return PointOfSaleId			

	def addSMSAlertToEMS7(self, SMSAlert):
		self.__printSMSAlert(SMSAlert)
		AddTimeNum = 1
		PermitBeginGMT = date.today()
		PermitExpireGMT = date.today()
		TicketNumber = 1
		MobileNumber = SMSAlert[0]
		ExpiryDate = SMSAlert[1]
		EMS6PaystationId = SMSAlert[2]
		PurchasedDate = SMSAlert[3]
		TicketNumber = SMSAlert[4]
		PlateNumber = SMSAlert[5]
		StallNumber = SMSAlert[6]
		RegionId = SMSAlert[7]
		NumOfRetry = SMSAlert[8]
		IsAlerted = SMSAlert[9]
		IsLocked = SMSAlert[10]
		IsAutoExtended = SMSAlert[11]

		(PointOfSaleId, CustomerId) = self.__getPointOfSale(EMS6PaystationId)
		PermitLocationId = self.__getLocationId(RegionId)

		MobileNumberId = self.__getEMS7MobileNumberId(MobileNumber)
		self.__EMS7Cursor.execute("insert into SmsAlert(CustomerId,PermitLocationId,PointOfSaleId,PermitBeginGMT,PermitExpireGMT,MobileNumberId,LicencePlate,TicketNumber,AddTimeNumber,SpaceNumber,NumberOfRetries,IsAutoExtended,IsAlerted,IsLocked) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(CustomerId,PermitLocationId,PointOfSaleId,PermitBeginGMT,PermitExpireGMT,MobileNumberId,PlateNumber,TicketNumber,AddTimeNum,StallNumber,NumOfRetry,IsAutoExtended,IsAlerted,IsLocked))
		self.__EMS7Connection.commit()	

	def __printSMSAlert(self, SMSAlert):
		print " -- MobileNumber %s " %SMSAlert[0]
		print " -- Expiry Date %s " %SMSAlert[1]
		print " -- PaystationId %s " %SMSAlert[2]
		print " -- Purchased Date %s " %SMSAlert[3]
		print " -- TicketNumber %s " %SMSAlert[4]
		print " -- PlateNumber %s" %SMSAlert[5]
		print " -- Stall Number %s" %SMSAlert[6]
		print " -- EMS6 RegionId %s" %SMSAlert[7]
		print " -- NumOfRetries %s" %SMSAlert[8]
		print " -- IsAlerted %s" %SMSAlert[9]
		print " -- IsLocked %s" %SMSAlert[10]
		print " -- IsAutoExtended %s" %SMSAlert[11]

	def __getEMS7MobileNumberId(self, MobileNumber):
		EMS7MobileNumberId = None
		self.__EMS7Cursor.execute("select Id from MobileNumber where Number=%s",(MobileNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7MobileNumberId = row[0]
		return EMS7MobileNumberId
		
	def addEMS6SMSFailedResponseToEMS7(self, SMSFailedResponse):
		self.__printSMSFailedResponse(SMSFailedResponse)
		PaystationId = SMSFailedResponse[0]
		PurchasedDate = SMSFailedResponse[1]
		TicketNumber = SMSFailedResponse[2]
		SMSMessageTypeId = SMSFailedResponse[3]
		IsOk = SMSFailedResponse[4]
		TrackingId = SMSFailedResponse[5]
		Number = SMSFailedResponse[6]
		ConvertedNumber = SMSFailedResponse[7]
		DeferUntilOccursInThePast = SMSFailedResponse[8]
		IsMessageEmpty = SMSFailedResponse[9]
		IsTooManyMessages = SMSFailedResponse[10]
		IsInvalidCountryCode = SMSFailedResponse[11]
		IsBlocked = SMSFailedResponse[12]
		BlockedReason = SMSFailedResponse[13]
		IsBalanceZero = SMSFailedResponse[14]
		IsInvalidCarrierCode = SMSFailedResponse[15]

		EMS7PointOfSaleId=self.__getPointOfSaleId(PaystationId)
			
		self.__EMS7Cursor.execute("insert into SmsFailedResponse(SmsMessageTypeId,PointOfSaleId,PurchaseGMT,TicketNumber,TrackingId,Number,ConvertedNumber,BlockedReason,IsOk,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,IsBalanceZero,IsInvalidCarrierCode,IsDeferUntilOccursInThePast) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(SMSMessageTypeId,EMS7PointOfSaleId,PurchasedDate,TicketNumber,TrackingId,Number,ConvertedNumber,BlockedReason,IsOk,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,IsBalanceZero,IsInvalidCarrierCode,DeferUntilOccursInThePast))	
		self.__EMS7Connection.commit()

	def __printSMSFailedResponse(self, SMSFailedReponse):
		print " -- PaystationId %s " %SMSFailedReponse[0]
		print " -- PurchasedDate %s " %SMSFailedReponse[1]
		print " -- TicketNumber %s " %SMSFailedReponse[2]
		print " -- SMSMessageTypeId %s " %SMSFailedReponse[3]
		print " -- IsOk %s " %SMSFailedReponse[4]
		print " -- TrackingId %s " %SMSFailedReponse[5]
		print " -- Number %s " %SMSFailedReponse[6]
		print " -- ConvertedNumber %s " %SMSFailedReponse[7]
		print " -- DeferUntilOccursInThePast %s " %SMSFailedReponse[8]
		print " -- IsMessageEmpty %s " %SMSFailedReponse[9]
		print " -- IsTooManyMessages %s " %SMSFailedReponse[10]
		print " -- IsInvalidCountryCode %s " %SMSFailedReponse[11]
		print " -- IsBlocked %s " %SMSFailedReponse[12]
		print " -- BlockedReason %s" %SMSFailedReponse[13]
		print " -- IsBalanceZero %s " %SMSFailedReponse[14]
		print " -- IsInvalidCarrierCode %s " %SMSFailedReponse[15]
	
	def addModemSettingToEMS7(self, ModemSetting):
		tableName='ModemSetting'
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		AccessPointId = 1
		CarrierId = 0	
		CCID = 'Unknown'
		self.__printModemSetting(ModemSetting)
		EMS6PaystationId = ModemSetting[0]
		Type = ModemSetting[1]
		CCID = ModemSetting[2]
		CarrierName = ModemSetting[3]
		APN = ModemSetting[4]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		CarrierId = self.__getEMS7CarrierId(CarrierName)
		AccessPointId = self.__getEMS7AccessPointId(APN)
		ModemTypeId = self.__getEMS7ModemType(Type)
		if (PointOfSaleId):
			print "Corrosponding EMS7 PointOfSale id is %s " %PointOfSaleId
		#	self.__EMS7Cursor.execute("set foreign_key_checks=0")
			self.__EMS7Cursor.execute("insert into ModemSetting(PointOfSaleId,CarrierId,AccessPointId,ModemTypeId, CCID,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,CarrierId,AccessPointId,ModemTypeId,CCID,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6PaystationId,tableName,IsMultiKey)

	def __printModemSetting(self, ModemSetting):
		print " -- PaystationId %s " %ModemSetting[0]
		print " -- Type %s " %ModemSetting[1]
		print " -- CCID %s " %ModemSetting[2]
		print " -- Carrier %s " %ModemSetting[3]
		print " -- APN %s " %ModemSetting[4]

	def __getEMS7CarrierId(self, CarrierName):
		EMS7CarrierId=None
		self.__EMS7Cursor.execute("select Id from Carrier where Name=%s",(CarrierName))	
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CarrierId = row[0]
		return EMS7CarrierId

	def __getEMS7AccessPointId(self, APN):
		EMS7AccessPointId = None
		self.__EMS7Cursor.execute("select Id from AccessPoint where Name=%s",(APN))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7AccessPointId = row[0]
		return EMS7AccessPointId
	
	def __getEMS7ModemType(self, ModemType):
		EMS7ModemTypeId=None
		self.__EMS7Cursor.execute("select Id from ModemType where Name=%s",(ModemType))
		if (self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			EMS7ModemTypeId = row[0]
		return EMS7ModemTypeId		
		
	def addEMS6LotSettingContentToEMS7(self, LotSettingContent):
		tableName='LotSettingFileContent'
		print " -- LotSettingContent %s " %LotSettingContent[0]
		EMS6LotSettingId = LotSettingContent[0]
		FileContent = LotSettingContent[1]
		EMS7LotSettingId = self.__getEMS7LotSettingId(EMS6LotSettingId)
		if (EMS7LotSettingId and EMS7LotSettingId<>0):
			self.__EMS7Cursor.execute("insert into LotSettingContent(LotSettingId,Content) values(%s,%s)",(EMS7LotSettingId,FileContent))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6LotSettingId,tableName,IsMultiKey)

	def __getEMS7LotSettingId(self, EMS6LotSettingId):
		EMS7LotSettingId = None
		self.__EMS7Cursor.execute("select distinct(EMS7LotSettingId) from LotSettingMapping where EMS6LotSettingId=%s", (EMS6LotSettingId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7LotSettingId = row[0]
		return EMS7LotSettingId

	def addCryptoKeyToEMS7(self, curCryptoKey):
		self.__printCryptoKey(curCryptoKey)
		Type = curCryptoKey[0]
		KeyIndex = curCryptoKey[1]
		Hash = curCryptoKey[2]
		Expiry = curCryptoKey[3]
		Signature = curCryptoKey[4]
		Info = curCryptoKey[5]
		NextHash = curCryptoKey[6]
		Status = curCryptoKey[7]
		CreateDate = curCryptoKey[8]
		Comment = curCryptoKey[9]
		self.__EMS7Cursor.execute("set innodb_lock_wait_timeout=200")
			
		self.__EMS7Cursor.execute("insert into CryptoKey(KeyType,KeyIndex,ExpiryGMT,Hash,NextHash,Signature,Info,Comment,Status,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(Type,KeyIndex,Expiry,Hash,NextHash,Signature,Info,Comment,Status,CreateDate))
		self.__EMS7Connection.commit()

	def __printCryptoKey(self, curCryptoKey):
		print " -- Type %s " %curCryptoKey[0]
		print " -- KeyIndex %s " %curCryptoKey[1]
		print " -- Hash %s " %curCryptoKey[2]
		print " -- Expiry %s " %curCryptoKey[3]
		print " -- Signature %s " %curCryptoKey[4]
		print " -- Info %s " %curCryptoKey[5]
		print " -- NextHash %s " %curCryptoKey[6]
		print " -- Status %s " %curCryptoKey[7]
		print " -- CreateDate %s " %curCryptoKey[8]
		print " -- Comment %s " %curCryptoKey[9]

	def addRestAccountToEMS7(self, RestAccount):
		tableName='RestAccount'
		self.__printRestAccount(RestAccount)
		IsDeleted = 1
		Version = 1
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS7CustID = 0
		AccountName = RestAccount[0]
		EMS6CustomerId = RestAccount[1]
		TypeId= RestAccount[2]
		SecretKey = RestAccount[3]
		VirtualPS = RestAccount[4]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		PointOfSaleId = self.__EMS7Cursor.execute("select Id from PointOfSale where SerialNumber=%s",(VirtualPS))
		self.__EMS7Cursor.execute("insert ignore into RestAccount(CustomerId,PointOfSaleId,RestAccountTypeId,AccountName,SecretKey,IsDeleted,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,PointOfSaleId,TypeId,AccountName,SecretKey,IsDeleted,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Cursor.execute("update Paystation set PaystationTypeId=9 where SerialNumber=%s",(VirtualPS))
		self.__EMS7Connection.commit()

	def __printRestAccount(self, restAccount):
		print " -- RestAccount Name %s" %restAccount[0]
		print " -- CustomerId %s" %restAccount[1]
		print " -- TypeId %s" %restAccount[2]
		print " -- Secret Key %s" %restAccount[3]
		print " -- Virtual Paystation %s" %restAccount[4]

	def addRestLogToEMS7(self, RestLog):
		tableName='RESTLog'
		self.__printRestLog(RestLog)
		RestAccountName = RestLog[0]
		EndpointName = RestLog[1]
		LoggingDate = RestLog[2]
		IsError = RestLog[3]
		CustomerId = RestLog[4]
		TotalCalls = RestLog[5]
		# The record with missing Customer id is not getting migrated
		if (CustomerId):
			EMS7CustomerId = self.__getEMS7CustomerId(CustomerId)
			EMS7RestAccountId = self.__getEMS7RestAccountId(RestAccountName)
			if (EMS7RestAccountId):
				self.__EMS7Cursor.execute("insert into RestLog(CustomerId,RestAccountId,EndpointName,LoggingDate,IsError,TotalCalls) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId, EMS7RestAccountId,EndpointName,LoggingDate,IsError,TotalCalls))	
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS7CustomerId,tableName,IsMultiKey)

	def __getEMS7RestAccountId(self, RestAccountName):
		self.__EMS7Cursor.execute("select Id from RestAccount where AccountName=%s",(RestAccountName))
		if(self.__EMS7Cursor.rowcount<>0):
			row = self.__EMS7Cursor.fetchone()
			return row[0] 

	def __printRestLog(self, RestLog):
		print " -- CustomerId %s " %RestLog[4]
		print " -- RestAccountName %s " %RestLog[0]
		print " -- EndpointName %s " %RestLog[1]
		print " -- LoggingDate %s " %RestLog[2]
		print " -- IsError %s" %RestLog[3]
		print " -- TotalCalls %s" %RestLog[5]

	def addRestSessionToken(self, RestSessionToken):
		tableName='RESTSessionToken'
		self.__printRestSessionToken(RestSessionToken)
		AccountName = RestSessionToken[0]
		SessionToken = RestSessionToken[1]
		CreationDate = RestSessionToken[2]
		ExpiryDate = RestSessionToken[3]
		EMS7RestAccountId = self.__getEMS7RestAccountId(AccountName)
		if(EMS7RestAccountId):
			self.__EMS7Cursor.execute("insert into RestSessionToken(RestAccountId,SessionToken,CreationDate,ExpiryDate) values(%s,%s,%s,%s)",(EMS7RestAccountId,SessionToken,CreationDate,ExpiryDate))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			self.__InsertIntoDiscardedRecords(AccountName,tableName,IsMultiKey)

	def __printRestSessionToken(self, RestSessionToken):
		print " -- AccountName %s " %RestSessionToken[0]
		print " -- SessionToken %s " %RestSessionToken[1]
		print " -- CreationDate %s " %RestSessionToken[2]		
		print " -- ExpiryDate %s " %RestSessionToken[3]

	def addRestLogTotalCall(self, RestLogTotalCall):
		tableName='RESTLogTotalCall'
		self.__printRestLogTotalCall(RestLogTotalCall)
		AccountName = RestLogTotalCall[0]
		LoggingDate = RestLogTotalCall[1]
		TotalCalls =  RestLogTotalCall[2]
		EMS7RestAccountId = self.__getEMS7RestAccountId(AccountName)
		if(EMS7RestAccountId):
			self.__EMS7Cursor.execute("insert into RestLogTotalCall(RestAccountId,LoggingDate,TotalCalls) values(%s,%s,%s)",(EMS7RestAccountId,LoggingDate,TotalCalls))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (AccountName,LoggingDate)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __printRestLogTotalCall(self, RestLogTotalCall):
		print " -- AccountName %s " %RestLogTotalCall[0]
		print " -- LoggingDate %s " %RestLogTotalCall[1]
		print " -- TotalCalls %s " %RestLogTotalCall[2]	

	def addCustomerWsCal(self, CustomerWsCal):
		tableName='CustomerWsCal'
		self.__printCustomerWsCal(CustomerWsCal)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
		EMS6CustomerId = CustomerWsCal[0]
		EndPointId = CustomerWsCal[1]
		CalInUse =  CustomerWsCal[2]
		CalPurchase = CustomerWsCal[3]
		Description = CustomerWsCal[4] 
		CustomerWsCalId = CustomerWsCal[5]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if(EMS7CustomerId):
			self.__EMS7Cursor.execute("insert into CustomerWebServiceCal(CustomerId,WebServiceEndPointTypeId,CalInUse,CalPurchase,Description) values(%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,CalInUse,CalPurchase,Description))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(CustomerWsCalId,tableName,IsMultiKey)

	def __printCustomerWsCal(self, CustomerWsCal):
		print " -- EMS7CustomerId %s " %CustomerWsCal[0]
		print " -- EndPointId %s " %CustomerWsCal[1]
		print " -- CalInUse %s " %CustomerWsCal[2]
		print " -- CalPurchase %s " %CustomerWsCal[3]
		print " -- Description %s " %CustomerWsCal[4]		

        def addCustomerWsToken(self, CustomerWsToken):
		tableName='CustomerWsToken'
                self.__printCustomerWsToken(CustomerWsToken)
                EMS6CustomerId = CustomerWsToken[0]
                Token = CustomerWsToken[1]
                EndPointId=  CustomerWsToken[2]
                WsInUse = CustomerWsToken[3]
		CustomerWsTokenId = CustomerWsToken[4]
                EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
                if(EMS7CustomerId):
                        self.__EMS7Cursor.execute("insert into CustomerWsToken(CustomerId,Token,EndPointId,WsInUse) values(%s,%s,%s,%s)",(EMS7CustomerId,Token,EndPointId,WsInUse))
                        self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(CustomerWsTokenId,tableName,IsMultiKey)

        def __printCustomerWsToken(self, CustomerWsToken):
                print " -- EMS7CustomerId %s " %CustomerWsToken[0]
                print " -- Token %s " %CustomerWsToken[1]
                print " -- EndPointId %s " %CustomerWsToken[2]
                print " -- WsInUse %s " %CustomerWsToken[3]

	def addCustomerWebServiceCalToEMS7(self, CustomerWebServiceCal):
		self.__printCustomerWebServiceCal(CustomerWebServiceCal)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS6CustomerId = CustomerWebServiceCal[0]
		EndPointId = CustomerWebServiceCal[1]
		CalInUse = CustomerWebServiceCal[2]
		CalPurchase = CustomerWebServiceCal[3]
		Description = CustomerWebServiceCal[4]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("insert into CustomerWebServiceCal (CustomerId,WebServiceEndPointTypeId,CalInUse,CalPurchase,Description,LastModifiedGMT,LastModifiedByUserId) value(%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,CalInUse,CalPurchase,Description,LastModifiedGMT,LastModifiedByUserId))

	def __printCustomerWebServiceCal(self, CustomerWebServiceCal):
		print " -- EMS6CustomerId %s " %CustomerWebServiceCal[0]
		print " -- EndPointId %s "  %CustomerWebServiceCal[1]
		print " -- CalInUse %s " %CustomerWebServiceCal[2]
		print " -- CalPurchase %s"  %CustomerWebServiceCal[3]
		print " -- Description %s" %CustomerWebServiceCal[4]

#Note : the actual name of the table in EMS7 is WebServiceEndPoint that maps to EMS6.CustomerWsToken
	def addCustomerWsTokenToEMS7(self, CustomerWsToken):
		self.__printCustomerWsToken(CustomerWsToken)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		IsDeleted = 0
		EMS6CustomerId = CustomerWsToken[0]
		Token = CustomerWsToken[1]
		EndPointId = CustomerWsToken[2]
		WsInUse = CustomerWsToken[3]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("insert IGNORE into WebServiceEndPoint(CustomerId,WebServiceEndPointTypeId,Token,IsDeleted,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EndPointId,Token,IsDeleted,LastModifiedGMT,LastModifiedByUserId))

	def __printCustomerWsToken(self, CustomerWsToken):
		print " -- EMS7CustomerId %s" %CustomerWsToken[0]
		print " -- Token %s" %CustomerWsToken[1]
		print " -- EndPointId %s " %CustomerWsToken[2]
		print " -- WsInUse %s " %CustomerWsToken[3]

	def addRatesToEMS7(self, Rates):
		IsMultiKey=0
		tableName='UnifiedRate'
		self.__printRates(Rates)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS6CustomerId = Rates[0]
		RateName = Rates[1]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if (EMS7CustomerId):
			self.__EMS7Cursor.execute("insert into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def addEMSRatesToEMS7(self, Rates):
		self.__printRates(Rates)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1
                EMS6CustomerId = Rates[0]
                RateName = Rates[1]
                EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
                self.__EMS7Cursor.execute("insert IGNORE into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
                self.__EMS7Connection.commit()

	def __printRates(self, Rates):
		print " -- CustomerId %s" %Rates[0]
		print " -- RateName %s " %Rates[1] 	

	def addReplenishToEMS7(self, Replenish):
		tableName='Replenish'
		self.__printReplenish(Replenish)
		EMS6PaystationId = Replenish[0]
		Date = Replenish[1]
		Number = Replenish[2]
		TypeId = Replenish[3]
		Tube1Type = Replenish[4]
		Tube1ChangedCount = Replenish[5]
		Tube1CurrentCount = Replenish[6]
		Tube2Type = Replenish[7]
		Tube2ChangedCount = Replenish[8]
		Tube2CurrentCount = Replenish[9]
		Tube3Type = Replenish[10]
		Tube3ChangedCount = Replenish[11]
		Tube3CurrentCount = Replenish[12]
		Tube4Type = Replenish[13]
		Tube4ChangedCount = Replenish[14]
		Tube4CurrentCount = Replenish[15]
		CoinBag005AddedCount = Replenish[16]
		CoinBag010AddedCount = Replenish[17]
		CoinBag025AddedCount = Replenish[18]
		CoinBag100AddedCount = Replenish[19]
		CoinBag200AddedCount = Replenish[20]	
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (PointOfSaleId):
			self.__EMS7Cursor.execute("Insert into Replenish(PointOfSaleId,ReplenishTypeId,ReplenishGMT,Number,Tube1Type,Tube1ChangedCount,Tube1CurrentCount,Tube2Type,Tube2ChangedCount,Tube2CurrentCount,Tube3Type,Tube3ChangedCount,Tube3CurrentCount,Tube4Type,Tube4ChangedCount,Tube4CurrentCount,CoinBag005AddedCount,CoinBag010AddedCount,CoinBag025AddedCount,CoinBag100AddedCount,CoinBag200AddedCount) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (PointOfSaleId, TypeId, Date, Number, Tube1Type, Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount,Tube2CurrentCount,Tube3Type,Tube3ChangedCount, Tube3CurrentCount,Tube4Type, Tube4ChangedCount,Tube4CurrentCount,CoinBag005AddedCount,CoinBag010AddedCount,CoinBag025AddedCount,CoinBag100AddedCount,CoinBag200AddedCount))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,Date,Number)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
	


	def __printReplenish(self, Replenish):
		print " -- PaystationId %s " %Replenish[0]
		print " -- Date %s" %Replenish[1]
		print " -- Number %s" %Replenish[2]
		print " -- TypeId %s" %Replenish[3]
		print " -- Tube1Type %s" %Replenish[4]
		print " -- Tube1ChangedCount %s" %Replenish[5]
		print " -- Tube1CurrentCount %s" %Replenish[6]
		print " -- Tube2Type %s" %Replenish[7]
		print " -- Tube2ChangedCount %s"  %Replenish[8]
		print " -- Tube2CurrentCount %s" %Replenish[9]
		print " -- Tube3Type %s" %Replenish[10] 
		print " -- Tube3ChangedCount %s" %Replenish[11]
		print " -- Tube3CurrentCount %s" %Replenish[12]
		print " -- Tube4Type %s " %Replenish[13]
		print " -- Tube4ChangedCount %s" %Replenish[14]
		print " -- Tube4CurrentCount %s" %Replenish[15]
		print " -- CoinBag005AddedCount %s" %Replenish[16]
		print " -- CoinBag010AddedCount %s " %Replenish[17]
		print " -- CoinBag025AddedCount %s" %Replenish[18]
		print " -- CoinBag100AddedCount %s" %Replenish[19]
		print " -- CoinBag200AddedCount %s" %Replenish[20]

	def addCardRetryTransactionToEMS7(self,CardRetryTransaction):
		tableName='CardRetryTransaction'
		self.__printCardRetryTransaction(CardRetryTransaction)
		EMS6PaystationId = CardRetryTransaction[0]
		PurchaseDate = CardRetryTransaction[1]
		TicketNumber = CardRetryTransaction[2]
		LastRetryDate = CardRetryTransaction[3]
		NumRetries = CardRetryTransaction[4]
		CardHash = CardRetryTransaction[5]
		TypeId = CardRetryTransaction[6]
		CardData = CardRetryTransaction[7]
		CardExpiry = CardRetryTransaction[8]
		Amount = CardRetryTransaction[9]
		CardType = CardRetryTransaction[10]
		Last4DigitsOfCardNumber = CardRetryTransaction[11]
		CreationDate = CardRetryTransaction[12]
		BadCardHash = CardRetryTransaction[13]
		IgnoredBadCard = CardRetryTransaction[14]
		LastResponseCode = CardRetryTransaction[15]
		IsRFID = CardRetryTransaction[16]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (PointOfSaleId):
			self.__EMS7Cursor.execute("insert into CardRetryTransaction(PointOfSaleId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,CardRetryTransactionTypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId,PurchaseDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoredBadCard,LastResponseCode,IsRFID))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,PurchaseDate,TicketNumber)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __printCardRetryTransaction(self, CardRetryTransaction):
		print " -- Paystation ID %s " %CardRetryTransaction[0]
		print " -- PurchaseDate %s " %CardRetryTransaction[1]
		print " -- TicketNumber %s " %CardRetryTransaction[2]
		print " -- LastRetryDate %s " %CardRetryTransaction[3]
		print " -- NumRetries %s " %CardRetryTransaction[4]
		print " -- CardHash %s " %CardRetryTransaction[5]
		print " -- TypeId %s " %CardRetryTransaction[6]
		print " -- CardDate %s " %CardRetryTransaction[7]
		print " -- Card Expiry %s " %CardRetryTransaction[8]
		print " -- Amount %s " %CardRetryTransaction[9]
		print " -- CardType %s " %CardRetryTransaction[10]
		print " -- Last4DigitOfCardNUmber %s " %CardRetryTransaction[11]
		print " -- CreationDate %s " %CardRetryTransaction[12]
		print " -- BadCardHash %s " %CardRetryTransaction[13]
		print " -- IgnoredBadCard %s" %CardRetryTransaction[14]
		print " -- LastResponseCode %s " %CardRetryTransaction[15]
		print " -- IsRFID %s " %CardRetryTransaction[16]
	
	def addCustomerCardTypeToEMS7(self, CustomerCardType):
		self.__printCustomerCardType(CustomerCardType)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 0
		IsForBadCard=0
		AuthorizationTypeId=0
		EMS6CustomerId = CustomerCardType[0]
		version = CustomerCardType[1]
		Name = CustomerCardType[2]
		Track2RegEx = CustomerCardType[3]
		CheckDigitAlg = CustomerCardType[4]
		Description = CustomerCardType[5]
		AuthorizationMethod = CustomerCardType[6]
		CardTypeId = 3
		if (AuthorizationMethod=='External Server'):
			AuthorizationTypeId=2
		if (AuthorizationMethod=='EMS List'):
			AuthorizationTypeId=1
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)	
		self.__EMS7Cursor.execute("insert Ignore into CustomerCardType(CustomerId,CardTypeId,AuthorizationTypeId,Name,Track2Pattern,Description,IsDigitAlgorithm,IsForBadCard,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,CardTypeId,AuthorizationTypeId,Name,Track2RegEx,Description,CheckDigitAlg,IsForBadCard,version,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()
	
	def __printCustomerCardType(self,CustomerCardType):
		print " -- Customer ID %s" %CustomerCardType[0]
		print " -- version %s " %CustomerCardType[1]
		print " -- Name %s " %CustomerCardType[2]
		print " -- Track2RegEx %s " %CustomerCardType[3]
		print " -- CheckDigitAlg %s " %CustomerCardType[4]
		print " -- Description %s " %CustomerCardType[5]
		print " -- AuthorizationMethod %s " %CustomerCardType[6]

	def addPaystationGroupToEMS7(self, PaystationGroup):
		self.__printPaystationGroup(PaystationGroup)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 0
		version=0
		print "RouteID is set to 3- Others as a default unless further clarification"
		RouteTypeId = 3
		Version=0
		EMS6CustomerId = PaystationGroup[0]
		Name = PaystationGroup[1]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		self.__EMS7Cursor.execute("insert into Route(CustomerId,RouteTypeId,Name,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,RouteTypeId,Name,Version,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()	

	def __printPaystationGroup(self, PaystationGroup):
		print " -- CustomerId %s" %PaystationGroup[0]
		print " -- Name %s" %PaystationGroup[1]
	
	def addCollectionTypeToEMS7(self,CollectionType):
		self.__printCollectionType(CollectionType)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 0
		Id = CollectionType[0]
		Name = CollectionType[1]
		IsInactive = CollectionType[2]
		self.__EMS7Cursor.execute("insert into CollectionType(Id,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(Id,Name,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printCollectionType(self, CollectionType):
		print " -- CollectionTypeId %s " %CollectionType[0]
		print " -- Name %s " %CollectionType[1]

	def addValueCardSmartCardToEMS7(self,SmartCardValueCard):
		self.__printValueSmartCard(SmartCardValueCard)
		LastModifiedGMT = date.today()
                LastModifiedByUserId = 1 
		IsForNonOverlappingUse = None 
		GracePeriodMinutes = None
		CardBeginGMT = None
		CardExpireGMT = None
		NumberOfUses =None 
		MaxNumberOfUses = None
		Comment = None
		IsActive =None 
		EMS6CustomerId = SmartCardValueCard[0]
		VERSION =None 
		AddedGMT = None
		EMS6RegionId = SmartCardValueCard[1]
		EMS6PaystationId = SmartCardValueCard[2]
		CustomCardData = SmartCardValueCard[3]
		SmartCardData = SmartCardValueCard[4]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EMS7LocationId = self.__getLocationId(EMS6RegionId)
		if (SmartCardData<>None):
		#" SmartCardData exists
			CardTypeId = 2	
			self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
			EMS7CustomerCardTypeId = self.__EMS7Cursor.lastrowid

			self.__EMS7Cursor.execute("insert into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,SmartCardData,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		if (CustomCardData<>None):
			#CustomerCardData exists
			self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=3",(EMS7CustomerId))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				EMS7CustomerCardTypeId = row[0]
				self.__EMS7Cursor.execute("insert ignore into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,SmartCardData,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
			if (self.__EMS7Cursor.rowcount == 0):
				# CustomerTypeId is missing
				CardTypeId=3
				self.__EMS7Cursor.execute("insert ignore into CustomerCardType(CustomerId,CardTypeId) values(%s,%s) ",(EMS7CustomerId,CardTypeId))
				EMS7CustomerCardTypeId = self.__EMS7Cursor.lastrowid
				self.__EMS7Cursor.execute("insert into CustomerCard(CustomerCardTypeId,LocationId,PointOfSaleId,CardNumber,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerCardTypeId,EMS7LocationId,PointOfSaleId,SmartCardData,IsForNonOverlappingUse,GracePeriodMinutes,CardBeginGMT,CardExpireGMT,NumberOfUses,MaxNumberOfUses,Comment,IsActive,VERSION,AddedGMT,LastModifiedGMT,LastModifiedByUserId))
				self.__EMS7Connection.commit()
	
	def __printValueSmartCard(self, SmartCardValueCard):
		print " -- EMS6CustomerId %s " %SmartCardValueCard[0]
		print " -- RegionId %s" %SmartCardValueCard[1]
		print " -- PaystationId %s" %SmartCardValueCard[2]
		print " -- CustomerCardData %s" %SmartCardValueCard[3]
		print " -- SmartCardData %s" %SmartCardValueCard[4]

	def addBatteryInfoToEMS7(self, BatteryInfo):
		tableName='BatteryInfo'
		self.__printBatteryInfo(BatteryInfo)
		EMS6PaystationId = BatteryInfo[0]
		DateField = BatteryInfo[1]
		SystemLoad = BatteryInfo[2]
		InputCurrent = BatteryInfo[3]
		Battery = BatteryInfo[4]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("insert into POSBatteryInfo(PointOfSaleId,SensorGMT,SystemLoad,InputCurrent,BatteryVoltage) values(%s,%s,%s,%s,%s)",(PointOfSaleId,DateField,SystemLoad,InputCurrent,Battery))	
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,DateField)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __printBatteryInfo(self, BatteryInfo):
		print " --PaystationId %s " %BatteryInfo[0]
		print " --DateField %s " %BatteryInfo[1]
		print " --SystemLoad %s" %BatteryInfo[2]
		print " --InputCurrent %s" %BatteryInfo[3]
		print " --Battery %s" %BatteryInfo[4] 

	def addPurchaseToEMS7(self, Transaction):
		tableName='Purchase'
		self.__printTransaction(Transaction)
		EMS6CustomerId = Transaction[0]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EMS6PaystationId = Transaction[1]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseGMT = Transaction[2]
		TicketNumber = Transaction[3]
		TransactionTypeId = Transaction[4]
		PaymentTypeId = Transaction[5]
		EMS6RegionId = Transaction[6]
		EMS7LocationId = self.__getLocationId(EMS6RegionId)
		LotNumber = Transaction[9]
		RateName = Transaction[25]
		UnifiedRateId = self.getUnifiedRateId(EMS7CustomerId,RateName)
		EMS6CouponId=Transaction[8]
		EMS7CouponId= self.__getEMS7CouponId(EMS6CouponId,EMS6CustomerId)
		OriginalAmount = Transaction[10]
		ChargedAmount = Transaction[11]
		ChangeDispensedAmount = Transaction[12]
		ExcessPaymentAmount = Transaction[13]
		CashPaidAmount = Transaction[14]
		CardPaidAmount = Transaction[15]
		CoinPaidAmount = Transaction[19]
		BillPaidAmount = Transaction[20]
		RateAmount = Transaction[23]
		RateRevenueAmount = Transaction[24]
		CoinCount = Transaction[21]
		BillCount = Transaction[22]
		IsOffline = Transaction[16]
		IsRefundSlip = Transaction[17]
		CreatedGMT = Transaction[18]
		PaystationSettingId=self.__getPaystationSettingId(LotNumber,EMS7CustomerId)
		if( PurchasedGMT<>None):
			self.__EMS7Cursor.execute("insert ignore into Purchase(CustomerId,PointOfSaleId,PurchaseGMT,PurchaseNumber,TransactionTypeId,PaymentTypeId,LocationId,PaystationSettingId,UnifiedRateId,CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,PointOfSaleId,PurchaseGMT,TicketNumber,TransactionTypeId,PaymentTypeId,EMS7LocationId,PaystationSettingId,UnifiedRateId,EMS7CouponId,OriginalAmount,ChargedAmount,ChangeDispensedAmount,ExcessPaymentAmount,CashPaidAmount,CoinPaidAmount,BillPaidAmount,CardPaidAmount,RateAmount,RateRevenueAmount,CoinCount,BillCount,IsOffline,IsRefundSlip,CreatedGMT))
			PurchaseId = self.__EMS7Cursor.lastrowid
			self.__EMS7Connection.commit()
			self.__addPurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount)
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,PurchaseGMT,TicketNumber)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def addPermitToEMS7(self, Transaction):
		tableName='Permit'
		self.__printTransactionDataForPermit(Transaction)
		PurchaseId = None
		PermitTypeId=0
		NumberOfExtensions=0
		TransactionTypeId=None
		MobileNumber=None
		EMS7LocationId=None
		EMS6PaystationId = Transaction[0]
		TicketNumber = Transaction[1]
		RegionId = Transaction[2]
		TypeId = Transaction[3]
		PlateNumber = Transaction[4]
		SpaceNumber = Transaction[5]
		AddTimeNum = Transaction[6]
		PurchasedDate = Transaction[7]
		ExpiryDate = Transaction[8]
		EMS6MobileNumber = Transaction[9]
		if (TransactionTypeId==1):
			PermitTypeId=2
		elif(TransactionTypeId==2):
			PermitTypeId=1
		elif(TransactionTypeId==12):
			PermitTypeId=5
		elif(TransactionTypeId==16):
			PermitTypeId=4
		elif(TransactionTypeId==17):
			PermitType=3
		else:
			PermitType=0
		if (SpaceNumber<>None):
			# Pay By Space  
			PermitIssueTypeId=3
		elif (PlateNumber<>None):
			PermitIssueTypeId=2
			# Pay By Plate 
		else:
			# Default(Pay and Display)
			PermitIssueTypeId=1
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
		LicencePlate = self.__getLicencePlateId(PlateNumber)
		MobileNumber = self.__getMobileNumberId(MobileNumber)
		EMS7LocationId = self.__getLocationId(RegionId)
		if(PurchaseId):
			self.__EMS7Cursor.execute("insert ignore into Permit(PurchaseId,PermitNumber,LocationId,PermitTypeId,PermitIssueTypeId,LicencePlateId,MobileNumberId,SpaceNumber,AddTimeNumber,PermitBeginGMT,PermitOriginalExpireGMT,PermitExpireGMT,NumberOfExtensions) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,TicketNumber,EMS7LocationId,PermitTypeId,PermitIssueTypeId,LicencePlate,MobileNumber,SpaceNumber,AddTimeNum,PurchasedDate,ExpiryDate,ExpiryDate,NumberOfExtensions))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def addPaymentCardToEMS7(self, Transaction):
		tableName='PaymentCard'
		self.__printTransactionDataForPaymentCard(Transaction)
		CardTypeId=None
		EMS6PaystationId = Transaction[0]
		TicketNumber = Transaction[1]
		PurchasedDate = Transaction[2]
		PaymentTypeId = Transaction[3]
		if(PaymentTypeId==2 or PaymentTypeId==5):
			CardTypeId=1
		elif(PaymentTypeId==0):
			CardTypeId=10 # for all the transaction.PaymentType=0
		CreditCardType = Transaction[4]
		ProcessorTransactionTypeId = Transaction[5]
		EMS6MerchantAccountId = Transaction[6]
		Amount = Transaction[7]
		Last4DigitsOfCardNumber = Transaction[8]
		ProcessingDate = Transaction[9]
		IsUploadedFromBoss = Transaction[10]
		IsRFID = Transaction[11]
		Approved = Transaction[12]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		PurchaseId = self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
		ProcessorTransactionId = self.__getProcessorTransactionId(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		if(PurchaseId):
			self.__EMS7Cursor.execute("insert into PaymentCard(PurchaseId,CardTypeId,CreditCardTypeId,ProcessorTransactionTypeId,ProcessorTransactionId,MerchantAccountId,Amount,CardLast4Digits,CardProcessedGMT,IsUploadedFromBoss,IsRFID,IsApproved) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,CardTypeId,CreditCardType,ProcessorTransactionTypeId,ProcessorTransactionId,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,ProcessingDate,IsUploadedFromBoss,IsRFID,Approved))
			self.__EMS7Connection.commit()	
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
			
	def __getProcessorTransactionId(self,PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate):	
		ProcessorTransactionId=None
		self.__EMS7Cursor.execute("select Id from ProcessorTransaction where PointOfSaleId=%s and PurchasedDate=%s and TicketNumber=%s and IsApproved=%s and ProcessorTransactionTypeId=%s and ProcessingDate=%s",(PointOfSaleId,PurchasedDate,TicketNumber,Approved,ProcessorTransactionTypeId,ProcessingDate))
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7ProcessorTransactionId = row[0]
                return EMS7ProcessorTransactionId

	def __printTransactionDataForPaymentCard(self, Transaction):
		print " -- PaystationId %s" %Transaction[0]
		print " -- TicketNumber %s" %Transaction[1]
		print " -- PurchasedDate %s" %Transaction[2]
		print " -- TypeId %s" %Transaction[3]
		print " -- CardType %s" %Transaction[4]
		print " -- TypeId %s" %Transaction[5]
		print " -- MerchantAccountId %s" %Transaction[6]
		print " -- Amount %s" %Transaction[7]
		print " -- Last4DigitsOfCardNumber %s" %Transaction[8]
		print " -- ProcessingDate %s" %Transaction[9]
		print " -- IsUploadedFromBoss %s" %Transaction[10]
		print " -- IsRFID %s" %Transaction[11]
		print " -- Approved %s" %Transaction[12]
		
	def __printTransactionDataForPermit(self, Transaction):
		print " -- PaystationId %s" %Transaction[0]
		print " -- TicketNumber %s" %Transaction[1]
		print " -- RegionId %s" %Transaction[2]
		print " -- TypeId %s" %Transaction[3]
		print " -- PlateNumber %s" %Transaction[4]
		print " -- StallNumber %s" %Transaction[5]
		print " -- AddTimeNum %s" %Transaction[6]
		print " -- PurchasedDate %s" %Transaction[7]
		print " -- ExpiryDate %s" %Transaction[8]
		print " -- MobileNumber %s" %Transaction[9]

	def __getPaystationSettingId(self,LotNumber,EMS7CustomerId):
		self.__EMS7Cursor.execute("select Id from PaystationSetting where CustomerId=%s and Name=%s",(EMS7CustomerId,LotNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7PaystationSettingId = row[0]
                return EMS7PaystationSettingId

	def __addPurchaseCollection(self,PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount):
		tableName='PurchaseCollection'
		self.__printPurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount)
		if (PurchaseId):
			if (CoinPaidAmount==None):
				CoinPaidAmount='0'
			if (CoinCount==None):
				CoinCount='0'
			if (BillPaidAmount==None):
				BillPaidAmount='0'
			if (BillCount==None):
				BillCount='0'
			self.__EMS7Cursor.execute("insert into PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) values(%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(PurchaseId,tableName,IsMultiKey)

	def __printPurchaseCollection(self,PurchaseId,PointOfSaleId,PurchaseGMT,CashPaidAmount,CoinPaidAmount,CoinCount,BillPaidAmount,BillCount):
		print " -- PurchaseId %s" %PurchaseId
		print " -- PointOfSaleId %s" %PointOfSaleId
		print " -- PurchaseGMT %s" %PurchaseGMT
		print " -- CashPaidAmount %s" %CashPaidAmount
		print " -- CoinPaidAmount %s" %CoinPaidAmount
		print " -- CoinCount %s" %CoinCount
		print " -- BillPaidAmount %s" %BillPaidAmount
		print " -- BillCount %s" %BillCount

	def getUnifiedRateId(self,CustomerId,RateName):
		LastModifiedGMT = date.today()
		LastModifiedByUserId=1
		UnifiedRateId=None
		if(RateName):
			self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and RateName=%s",(CustomerId,RateName))
			if (self.__EMS7Cursor.rowcount <> 0):
				row = self.__EMS7Cursor.fetchone()
				UnifiedRateId = row[0]
		elif(RateName==None):
			RateName='Unknown'
			self.__EMS7Cursor.execute("select Id from UnifiedRate where CustomerId=%s and Name='Unknown' limit 1",(CustomerId))
			UnifiedRateId=self.__EMS7Cursor.fetchone()
			if (UnifiedRateId==0):
				self.__EMS7Cursor.execute("insert into UnifiedRate(CustomerId,Name,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(CustomerId,RateName,LastModifiedGMT,LastModifiedByUserId))
				UnifiedRateId=self.__EMS7Cursor.lastrowid
		return UnifiedRateId[0]

	def __printTransaction(self, Transaction):
		print " --CustomerId %s" %Transaction[0]
		print " --PaystationId %s" %Transaction[1]
		print " --PurchasedDate %s" %Transaction[2]
		print " --TicketNumber %s" %Transaction[3]
		print " --TypeId %s" %Transaction[4]
		print " --PaymentTypeId %s" %Transaction[5]
		print " --RegionId %s" %Transaction[6]
		print " --RateId %s" %Transaction[7]
		print " --CouponNumber %s" %Transaction[8]
		print " --LotNumber %s" %Transaction[9]
		print " --OriginalAmount %s" %Transaction[10]
		print " --ChargedAmount %s" %Transaction[11]
		print " --ChangeDispensed %s" %Transaction[12]
		print " --ExcessPayment %s" %Transaction[13]
		print " --CashPaid %s" %Transaction[14]
		print " --CardPaid %s" %Transaction[15]
		print " --IsOffline %s" %Transaction[16]
		print " --IsRefundSlip %s" %Transaction[17]
		print " --CreationDate %s" %Transaction[18]
		print " --CoinDollars %s" %Transaction[19]
		print " --BillDollars %s" %Transaction[20]
		print " --CoinCount %s" %Transaction[21]
		print " --BillCount %s" %Transaction[22]
		print " --RateValue %s" %Transaction[23]
		print " --Revenue %s" %Transaction[24]
		print " --RateName %s" %Transaction[25]
		print " --RatesCustId %s" %Transaction[26] 

	def __getEMS7CouponId(self, EMS6CouponId, EMS6CustomerId):
                EMS7CouponId = None
                self.__EMS7Cursor.execute("select EMS7CouponId from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s",(EMS6CouponId,EMS6CustomerId))
                if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7CouponId = row[0]
                return EMS7CouponId
		
	def addCustomerEmail(self, CustomerEmail):
		VERSION = 0
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		self.__printCustomerEmail(CustomerEmail)
		EMS6CustomerId = CustomerEmail[0]
		AlertTypeId = CustomerEmail[1]
		Email = CustomerEmail[2]
		IsDeleted = CustomerEmail[3]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		EmailList=CustomerEmail[2].split(',')
                for mail in EmailList:
			if(mail):
				self.__EMS7Cursor.execute("select Email from CustomerEmail where Email=%s",(mail))
				if (self.__EMS7Cursor.rowcount == 0):
					self.__EMS7Cursor.execute("insert into CustomerEmail(CustomerId,Email,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,mail,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId))
					EMS7CustomerEmailId = self.__EMS7Cursor.lastrowid	
					self.__CustomerAlertEmail(CustomerEmail, EMS7CustomerEmailId)
					self.__EMS7Connection.commit()


	def __CustomerAlertEmail(self,CustomerEmail,EMS7CustomerEmailId):
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		EMS6CustomerId = CustomerEmail[0]
		CustomerAlertTypeId=CustomerEmail[1]
		Email = CustomerEmail[2]
		IsDeleted = CustomerEmail[3]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		Version=0
		self.__EMS7Cursor.execute("insert into CustomerAlertEmail(CustomerAlertTypeId,CustomerEmailId,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s)",(CustomerAlertTypeId,EMS7CustomerEmailId,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()
			
	def __printCustomerEmail(self,CustomerEmail):
		print " -- CustomerId %s" %CustomerEmail[0]		
		print " -- AlertTypeId %s" %CustomerEmail[1]
		print " -- Email %s" %CustomerEmail[2]
		print " -- IsDeleted %s" %CustomerEmail[3]
		EmailList=CustomerEmail[2].split(',')

	def addMerchantAccount(self,MerchantAccount):
		tableName='MerchantAccount'
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		MerchantStatusTypeId =1 #This needs to be confirmed with pauk as to where is the value comming from	
		self.__printMerchantAccount(MerchantAccount)
		MerchantAccountId = MerchantAccount[0]
		version = MerchantAccount[1]
		EMS6CustomerId = MerchantAccount[2]
		Name = MerchantAccount[3]
		Field1 = MerchantAccount[4]
		Field2 = MerchantAccount[5]
		Field3 = MerchantAccount[6]
		IsDeleted = MerchantAccount[7]
		ProcessorName = MerchantAccount[8]
		ReferenceCounter = MerchantAccount[9]
		Field4 = MerchantAccount[10]
		Field5 = MerchantAccount[11]
		Field6 = MerchantAccount[12]
		IsValid = MerchantAccount[13]
		EMS7ProcessorId = self.__getEMS7ProcessorId(ProcessorName)
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if (EMS7CustomerId):
			self.__EMS7Cursor.execute("insert into MerchantAccount(CustomerId,ProcessorId,MerchantStatusTypeId,Name,Field1,Field2,Field3,Field4,Field5,Field6,ReferenceCounter,IsValidated,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EMS7ProcessorId,MerchantStatusTypeId,Name,Field1,Field2,Field3,Field4,Field5,Field6,ReferenceCounter,IsValid,version,LastModifiedGMT,LastModifiedByUserId))
			EMS7MerchantAccountId = self.__EMS7Cursor.lastrowid
			self.__EMS7Cursor.execute("insert into MerchantAccountMapping(EMS6MerchantAccountId,EMS7MerchantAccountId,ProcessorName,EMS6CustomerId) values(%s,%s,%s,%s)",(MerchantAccountId,EMS7MerchantAccountId,ProcessorName,EMS6CustomerId))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)

	def __getEMS7ProcessorId(self,ProcessorName):
		EMS7ProcessorId = None
		self.__EMS7Cursor.execute("select Id from Processor where Name=%s", ProcessorName)
		if (self.__EMS7Cursor.rowcount <> 0):
                        row = self.__EMS7Cursor.fetchone()
                        EMS7ProcessorId = row[0]
                return EMS7ProcessorId

	def __printMerchantAccount(self,MerchantAccount):
		print " -- Id %s" %MerchantAccount[0]
		print " -- version %s" %MerchantAccount[1]
		print " -- CustomerId %s" %MerchantAccount[2]
		print " -- Name %s" %MerchantAccount[3]
		print " -- Field1 %s" %MerchantAccount[4]
		print " -- Field2 %s" %MerchantAccount[5]	
		print " -- Field3 %s" %MerchantAccount[6]
		print " -- IsDeleted %s" %MerchantAccount[7]
		print " -- ProcessorName %s" %MerchantAccount[8]
		print " -- ReferenceCounter %s" %MerchantAccount[9]
		print " -- Field4 %s" %MerchantAccount[10]
		print " -- Field5 %s" %MerchantAccount[11]
		print " -- Field6 %s" %MerchantAccount[12]
		print " -- IsValid %s" %MerchantAccount[13]
	
	def addProcessorProperties(self, ProcessorProperties):
		IsForValueCard = 0
		VERSION = 0
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		IsDeleted = 0
		Processor = ProcessorProperties[0]
		Name = ProcessorProperties[1]
		Value = ProcessorProperties[2]
		print " -- Processor %s " %ProcessorProperties[0]
		print " -- Name %s" %ProcessorProperties[1]
		print " -- Value %s" %ProcessorProperties[2]
		EMS7ProcessorId=self.__getEMS7ProcessorId(Processor)
		self.__EMS7Cursor.execute("insert into ProcessorProperty(ProcessorId,Name,Value,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(EMS7ProcessorId,Name,Value,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()	

	def addPreAuth(self, PreAuth):
		self.__printPreAuth(PreAuth)
		ResponseCode = PreAuth[1]
		ProcessorTransactionId = PreAuth[2]
		AuthorizationNumber = PreAuth[3]
		ReferenceNumber = PreAuth[4]
		EMS6MerchantAccountId = PreAuth[5]
		Amount = PreAuth[6]
		Last4DigitsOfCardNumber = PreAuth[7]
		CardType = PreAuth[8]
		Approved = PreAuth[9]	
		PreAuthDate = PreAuth[10]
		CardData = PreAuth[11]
		EMS6PaystationId = PreAuth[12]
		ReferenceId = PreAuth[13]
		Expired = PreAuth[14]
		CardHash = PreAuth[15]
		ExtraData = PreAuth[13]
		PsRFID = PreAuth[14]
		IsRFID = PreAuth[15]
		CardExpiry = PreAuth[16]
		EMS7PointOfSaleId=self.__getPointOfSaleId(PaystationId)
		EMS7MerchantAccountId = self.__getEMS7MerchantAccountId(EMS6MerchantAccountId)
		self.__EMS7Cursor.execute("insert into PreAuth(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,IsApproved,PreAuthDate,CardData,PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,CardExpiry,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,EMS7MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,EMS7PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,IsRFID,CardExpiry))
		self.__EMS7Cursor.execute("insert into PreAuthMapping(EMS6MerchantAccountId,EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId) values(%s,%s,%s,%s)",(EMS6MerchantAccountId,EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId))
		self.__EMS7Connection.commit()
	
	def __getEMS7MerchantAccountId(self, EMS6MerchantAccountId):
		EMS7MerchantId = None
		self.__EMS7Cursor.execute("select EMS7MerchantAccountId from MerchantAccountMapping where EMS6MerchantAccountId=%s",(EMS6MerchantAccountId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7MerchantAccountId = row[0]
		return EMS7MerchantAccountId
	
	def __printPreAuth(self, PreAuth):
		print " -- Id %s" %PreAuth[0]
		print " -- ResponseCode %s" %PreAuth[1]
		print " -- ProcessorTransactionId %s" %PreAuth[2]
		print " -- AuthorizationNumber %s" %PreAuth[3]
		print " -- ReferenceNumber %s" %PreAuth[4]
		print " -- MerchantAccountId %s" %PreAuth[5]
		print " -- Amount %s" %PreAuth[6]
		print " -- Last4DigitsOfCardNumber %s" %PreAuth[7]
		print " -- CardType %s" %PreAuth[8]
		print " -- PaystationId %s" %PreAuth[9]
		print " -- ReferenceId %s" %PreAuth[10]
		print " -- Expired %s" %PreAuth[11]
		print " -- CardHash %s" %PreAuth[12]
		print " -- ExtraData %s" %PreAuth[13]
		print " -- PsRFID %s" %PreAuth[14]
		print " -- CardExpiry %s" %PreAuth[15]

	def addPreAuthHolding(self, PreAuthiHolding):
		self.__printPreAuth(PreAuth)
		ResponseCode = PreAuth[1]
		ProcessorTransactionId = PreAuth[2]
		AuthorizationNumber = PreAuth[3]
		ReferenceNumber = PreAuth[4]
		MerchantAccountId = PreAuth[5]
		Amount = PreAuth[6]
		Last4DigitsOfCardNumber = PreAuth[7]
		CardType = PreAuth[8]
		Approved = PreAuth[9]	
		PreAuthDate = PreAuth[10]
		CardData = PreAuth[11]
		PaystationId = PreAuth[12]
		ReferenceId = PreAuth[13]
		Expired = PreAuth[14]
		CardHash = PreAuth[15]
		ExtraData = PreAuth[13]
		PsRFID = PreAuth[14]
		IsRFID = PreAuth[15]
		CardExpiry = PreAuth[16]
		self.__EMS7Cursor.execute("insert into PreAuth(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,IsApproved,PreAuthDate,CardData,PointOfSaleId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,CardExpiry,IsRFID) values(i%s,5s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ResponseCode,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRFID,IsRFID,CardExpiry))
		self.__EMS7Cursor.execute("insert into PreAuthMappingHolding(EMS6MerchantAccountId,EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId) values(%s,%s,%s,%s)",(EMS6MerchantAccountId,EMS7MerchantAccountId,AuthorizationNumber,ProcessorTransactionId))
		self.__EMS7Connection.commit()

	def addProcessorTransaction(self, ProcessorTransaction):
		tableName='ProcessorTransaction'
		self.__printProcessorTransaction(ProcessorTransaction)
		TypeId = ProcessorTransaction[0]
		EMS6PaystationId = ProcessorTransaction[1]
		MerchantAccountId = ProcessorTransaction[2]
		TicketNumber = ProcessorTransaction[3]
		Amount = ProcessorTransaction[4]
		CardType = ProcessorTransaction[5]
		Last4DigitsOfSardNumber = ProcessorTransaction[6]
		CardCheckSum = ProcessorTransaction[7]
		PurchasedDate = ProcessorTransaction[8]
		ProcessingDate = ProcessorTransaction[9]
		ProcessorTransactionId = ProcessorTransaction[10]
		AuthorizationNumber = ProcessorTransaction[11]
		ReferenceNumber = ProcessorTransaction[12]
		Approved = ProcessorTransaction[13]
		CardHash = ProcessorTransaction[14]
		IsUploadFromBoos = ProcessorTransaction[15]
		IsRFID = ProcessorTransaction[16]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (PointOfSaleId and MerchantAccountId):
			PurchaseId=self.__getPurchaseId(PointOfSaleId,PurchasedDate,TicketNumber)
			print self.__EMS7Cursor.execute("insert into ProcessorTransaction(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,ProcessorTransactionTypeId,MerchantAccountId,Amount,CardType,Last4DigitsOfCardNumber,CardChecksum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,IsApproved,CardHash,IsUploadedFromBoss,IsRFID) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PurchaseId,PointOfSaleId,PurchasedDate,TicketNumber,TypeId,MerchantAccountId,Amount,CardType,Last4DigitsOfSardNumber,CardCheckSum,ProcessingDate,ProcessorTransactionId,AuthorizationNumber,ReferenceNumber,Approved,CardHash,IsUploadFromBoos,IsRFID))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=1
			MultiKeyId = (EMS6PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate)
			MultiKeyId = ','.join(map(str,MultiKeyId))
			self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)

	def __getPurchaseId(self,PointOfSaleId,PurchasedDate,TicketNumber):
		EMS7PurchaseId=None
		self.__EMS7Cursor.execute("select Id from Purchase where PointOfSaleId=%s and PurchaseGMT=%s and PurchaseNumber=%s",(PointOfSaleId,PurchasedDate,TicketNumber))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7PurchaseId = row[0]
		return EMS7PurchaseId

	def __printProcessorTransaction(self, ProcessorTransaction):
		print " -- TypeId %s" %ProcessorTransaction[0]
		print " -- PaystationId %s" %ProcessorTransaction[1]
		print " -- MerchantAccountId %s" %ProcessorTransaction[2]
		print " -- TicketNumber %s" %ProcessorTransaction[3]
		print " -- Amount %s" %ProcessorTransaction[4]
		print " -- CardType %s" %ProcessorTransaction[5]
		print " -- Last4DigitsOfSardNumber %s" %ProcessorTransaction[6]
		print " -- CardCheckSum %s" %ProcessorTransaction[7]
		print " -- PurchasedDate %s" %ProcessorTransaction[8]
		print " -- ProcessingDate %s" %ProcessorTransaction[9]
		print " -- ProcessorTransactionId %s" %ProcessorTransaction[10]
		print " -- Authorization Number %s" %ProcessorTransaction[11]
		print " -- ReferenceNumber %s" %ProcessorTransaction[12]		
		print " -- Approved %s" %ProcessorTransaction[13]
		print " -- CardHash %s" %ProcessorTransaction[14]
		print " -- IsUploadFromBoos %s" %ProcessorTransaction[15]
		print " -- IsRFID %s" %ProcessorTransaction[16]

	def addUserAccountToEMS7(self, UserAccount):
		tableName = 'UserAccount'
		self.__printUserAccount(UserAccount)
		LastModifiedGMT = date.today()
		LastModifiedByUserId = 1
		PasswordSalt='Unassigned'
		FirstName='Unknown'
		IsPasswordTemporary=0 # Assigned a default 0, to be confirmed later
		UserAccountStatusTypeId=2
		Version = UserAccount[0]
		EMS6CustomerId = UserAccount[1]
		Name = UserAccount[2]
		Password = UserAccount[3]
		RoleId = UserAccount[4]
		AccountStatus = UserAccount[5]
		UserName = UserAccount[6]
		UserAccountId = UserAccount[7]
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		if (AccountStatus=='ENABLED'):
			UserAccountStatusTypeId=1
		elif(AccountStatus=='DISABLED'):
			UserAccountStatusTypeId=0
		if (EMS7CustomerId):
			self.__EMS7Cursor.execute("insert into UserAccount(CustomerId,UserStatusTypeId,UserName,FirstName,Password,IsPasswordTemporary,VERSION,LastModifiedGMT,LastModifiedByUserId,PasswordSalt) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,UserAccountStatusTypeId,UserName,Name,Password,IsPasswordTemporary,Version,LastModifiedGMT,LastModifiedByUserId,PasswordSalt))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(UserAccountId,tableName,IsMultiKey)

	def __printUserAccount(self,UserAccount):
		print " -- Version %s" %UserAccount[0]
		print " -- CustomerId %s" %UserAccount[1]
		print " -- Name %s" %UserAccount[2]
		print " -- Password %s" %UserAccount[3]
		print " -- RoleId %s" %UserAccount[4]
		print " -- AccountStatus %s" %UserAccount[5]
		print " -- UserName %s" %UserAccount[6]
		print " -- UserAccountId %s" %UserAccount[7]

#Populating POSAlert from EMS6.EventLogNew table"	
#ActivePOSAlert table populate, If the severity is non 0 then it is an active alert, and it's the last alert for that Paystation, and you would get the last DateField in eventlognew to get the last record
#EMS6 Paystation alerts table specifies a paystation assigjned to a custmer defined alert. The defaults below apply to records comming in from event new log table and the records comming in from paystation alert table. the paystation alert table only records user defined alert records.
#mysql> desc ActivePOSAlert;
#+---------------------+-----------------------+------+-----+---------+----------------+
#| Field               | Type                  | Null | Key | Default | Extra          |
#+---------------------+-----------------------+------+-----+---------+----------------+
#| Id                  | int(10) unsigned      | NO   | PRI | NULL    | auto_increment |
#| CustomerAlertTypeId | mediumint(8) unsigned | NO   | MUL | NULL    |                |
#| PointOfSaleId       | mediumint(8) unsigned | NO   | MUL | NULL    |                |
#| POSAlertId          | int(10) unsigned      | YES  | MUL | NULL    |                |
#| EventDeviceTypeId   | tinyint(3) unsigned   | NO   | MUL | NULL    |                 | 20
#| EventStatusTypeId   | tinyint(3) unsigned   | NO   | MUL | NULL    |                | 23
#| EventActionTypeId   | tinyint(3) unsigned   | NO   | MUL | NULL    |                | Defaults=1
#| EventSeverityTypeId | tinyint(3) unsigned   | NO   | MUL | NULL    |                |2
#| IsActive            | tinyint(1) unsigned   | NO   |     | NULL    |                | PaystationAlert.isalerted
#| AlertGMT            | datetime              | YES  |     | NULL    |                | DateField
#| AlertInfo           | varchar(50)           | YES  |     | NULL    |                | Info
#| CreatedGMT          | datetime              | YES  |     | NULL    |                   New in EMS7
#---------------------+-----------------------+------+-----+---------+----------------+
# everything from EMS6.PaystationAlert table comes over into activePOSAlert, you have to provide specialvaluesfor devicetype,action, and look up severity. EventActionTypeId defaults=1, and EventSeverityTypeId default =2

	def addPOSAlertToEMS7(self, EventLogNew):
		tableName='EventLogNew'
		MultiKeyId=''
		IsActive = 0
		IsSentEmail = 1
		EMS6PaystationId = EventLogNew[0]
		DeviceId = EventLogNew[1]
		TypeId = EventLogNew[2]
		ActionId = EventLogNew[3]
		DateField = EventLogNew[4]
		Information = EventLogNew[5]
		ClearUserAccountID = EventLogNew[6]
		ClearDateField = EventLogNew[7]
		CreatedGMT = date.today()
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		self.__printEventLogNew(EventLogNew)
		if (DeviceId==20):
		# these are user defined alerts
			EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(TypeId)
			EventStatusTypeId=23
			EventSeverityTypeId = self.__getEventSeverityTypeId(DeviceId,EventStatusTypeId,ActionId)
			if (EMS7CustomerAlertTypeId):
				self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,DeviceId,EventStatusTypeId,ActionId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
				self.__EMS7Connection.commit()
				EMS7POSAlertId = self.__EMS7Cursor.lastrowid
				# This section is for only ActievPOSAlert
				if(EventSeverityTypeId<>0):
					#If the EventSeverityType<>0 then it is an active alert, the record will be enetered in ActivePOSAlert table
					EventActionTypeId=1
					EventSeverityTypeId=2 # why are setting the SeverityTypeId to be Major for all the records? Because user defined alerts are major
					#One additional Logic to identify if it's the last record, only the last active alert needs to be added to activepos alert 
					IsActive=1
					self.__EMS7Cursor.execute("insert ignore into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,POSAlertId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EMS7POSAlertId,DeviceId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,DateField,Information,CreatedGMT))
				self.__EMS7Connection.commit()
			else:
				IsMultiKey=1
				MultiKeyId = (EMS6PaystationId,DeviceId,TypeId,ActionId,DateField)
				MultiKeyId = ','.join(map(str,MultiKeyId))
				self.__InsertIntoDiscardedRecords(MultiKeyId,tableName,IsMultiKey)
		elif (DeviceId!=20):
		# These are Paystation Alert, EMS6.EventNewLog.TypeId=EMS6.EventNewType.Id. EMS6.EventNewType=EMS7.EventStatusType
		#	EMS7StatusTypeId = self.__getEventStatusTypeId(TypeId)
			PaystationAlertTypeId=12
			EventStatusTypeId=TypeId
			EventActionTypeId=ActionId
			EventDeviceTypeId=DeviceId
			EventSeverityTypeId=self.__getEventSeverityTypeId(EventDeviceTypeId,EventStatusTypeId,EventActionTypeId)
			EMS7CustPayId=self.__getCustomerIdFromPointOfSale(EMS6PaystationId)
			for CustomerId in EMS7CustPayId:
			#	print " >> CustomerId in EMS7 %s" %CustomerId
			#	print " >> TypeId %s" %TypeId
				EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeIdForPaystationAlert(CustomerId,PaystationAlertTypeId)
				if(EMS7CustomerAlertTypeId):
					self.__EMS7Cursor.execute("insert into POSAlert(CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,AlertGMT,AlertInfo,IsActive,IsSentEmail,ClearedGMT,ClearedByUserId,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,DateField,Information,IsActive,IsSentEmail,ClearDateField,ClearUserAccountID,CreatedGMT))
					self.__EMS7Connection.commit()
					EMS7POSAlertId = self.__EMS7Cursor.lastrowid
                                # This section is for only ActievPOSAlert
					if(EventSeverityTypeId<>0):
						#If the EventSeverityType<>0 then it is an active alert, the record will be enetered in ActivePOSAlert table
						#One additional Logic to identify if it's the last record, only the last active alert needs to be added to activepos
						self.__EMS7Cursor.execute("insert into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,POSAlertId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EMS7POSAlertId,DeviceId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,DateField,Information,CreatedGMT))
					self.__EMS7Connection.commit()

	def addActivePOSAlert(self, ActivePOSAlert):
		self.__printActivePOSAlert(ActivePOSAlert)
		AlertId = ActivePOSAlert[0]
		PaystationId = ActivePOSAlert[1]
		IsAlerted = ActivePOSAlert[2]
		EMS7CustomerAlertTypeId=self.__getCustomerAlertTypeId(AlertId)
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EventDeviceTypeId = 20
		EventStatusTypeId =23
		EventActionTypeId=1
		EventSeverityTypeId=2
		IsActive=0
		POSAlertId=None
		AlertGMT=None
		AlertInfo=None
		CreatedGMT=date.today()
		# How am I getting the value for POSALertId, by looking up Paystation and CustomerAlertType in POSAlert
		# Am i joining PaystationAlert with EventLogNew table to get the values for datefield, AlertGMT and info field for these paystation.
		#These are the default values for the user defined alert that never get changed, and assume the default values for a placeholder record for only user defined alerts
		self.__EMS7Cursor.execute("insert into ActivePOSAlert(CustomerAlertTypeId,PointOfSaleId,POSAlertId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,AlertGMT,AlertInfo,CreatedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,PointOfSaleId,EMS7POSAlertId,DeviceId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,IsActive,DateField,Information,CreatedGMT))
		self.__EMS7Connection.commit()

	def getPOSAlertIdFromEMS7(self, PaystationId, AlertId):
		self.__EMS7Cursor.execute("select Id from POSAlert where PointOfSaleId=%s and CustomerAlertTypeId=%s ")

	def __printActivePOSAlert(self, ActivePOSAlert):
		print " -- AlertId %s" %ActivePOSAlert[0]
		print " -- PaystationId %s" %ActivePOSAlert[1]
		print " -- IsAlerted %s" %ActivePOSAlert[2] 

	def __getCustomerIdFromPointOfSale(self, EMS6PaystationId):
		EMS7CustomerId=None
		self.__EMS7Cursor.execute("select CustomerId from PointOfSale where PaystationId=(select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s)",(EMS6PaystationId))
		if(self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchall()
			EMS7CustomerId=row[0]
		return EMS7CustomerId
		
	def __getEventSeverityTypeId(self,DeviceId,EventStatusTypeId,ActionId):
		EventSeverityTypeId=None	
		self.__EMS7Cursor.execute("select EventSeverityTypeId from EventDefinition where EventDeviceTypeId=%s and EventStatusTypeId=%s and EventActionTypeId=%s",(DeviceId,EventStatusTypeId,ActionId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EventSeverityTypeId = row[0]
		return EventSeverityTypeId

	def __printEventLogNew(self,EventNewLog):
		print " -- PaystationId %s" %EventNewLog[0]
		print " -- DeviceId %s " %EventNewLog[1]
		print " -- TypeId %s" %EventNewLog[2]
		print " -- ActionID %s" %EventNewLog[3]
		print " -- DateField %s" %EventNewLog[4]
		print " -- Information %s" %EventNewLog[5]
		print " -- ClearUserAccountID %s" %EventNewLog[6]
		print " -- ClearDateField %s" %EventNewLog[7]

	def __getCustomerAlertTypeIdForPaystationAlert(self,CustomerId, TypeId):
		EMS7CustomerAlertTypeId=None
		self.__EMS7Cursor.execute("select Id from CustomerAlertType where CustomerId=%s and AlertTypeId=%s",(CustomerId,TypeId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerAlertTypeId = row[0]
		return EMS7CustomerAlertTypeId

	def __getCustomerAlertTypeId(self,TypeId):
		EMS7CustomerAlertTypeId=None
		self.__EMS7Cursor.execute("select EMS7CustomerAlertTypeId from CustomerAlertMapping where EMS6AlertId=%s",(TypeId))
		if (self.__EMS7Cursor.rowcount <> 0):
			row = self.__EMS7Cursor.fetchone()
			EMS7CustomerAlertTypeId = row[0]
		return EMS7CustomerAlertTypeId

	def addCustomerAlertToEMS7(self, CustomerAlert):
		tableName='CustomerAlert'
		Version=0
		LastModifiedGMT =date.today()
		LastModifiedByUserId=1
		self.__printCustomerAlert(CustomerAlert)
		EMS6AlertId = CustomerAlert[0]
		PaystationGroupId = CustomerAlert[1]
		EMS6CustomerId = CustomerAlert[2]
		AlertName = CustomerAlert[3]
		AlertTypeId = CustomerAlert[4]
		Threshold = CustomerAlert[5]
		Email = CustomerAlert[6]
		IsEnabled = CustomerAlert[7]
		IsDeleted = CustomerAlert[8]
		RegionId = CustomerAlert[9]
		IsPaystationBased = CustomerAlert[10]
		EMS7LocationId = None
		EMS7LocationId = self.__getLocationId(RegionId)
		EMS7CustomerId = None
		EMS7CustomerId = self.__getEMS7CustomerId(EMS6CustomerId)
		# The Cursor below will fetch the AlertType details within EMS6 to identify the correct EMS6Types.
		if(EMS7CustomerId):
			self.__EMS7Cursor.execute("select Id,Name,DisplayName from EMS6AlertType where Id=%s",(AlertTypeId))
			EMS6AlertTypes=self.__EMS7Cursor.fetchall()	
			for AlertType in EMS6AlertTypes:
			#	print " -- Id %s" %AlertType[0]
			#	print " -- Name %s" %AlertType[1]
			#	print " -- DisplayName %s" %AlertType[2]
				self.__EMS7Cursor.execute("select Id,Name,AlertClassTypeId = (select distinct(Id) from AlertClassType where Name=%s) from AlertType where Name=%s",(AlertType[1],AlertType[2]))
				EMS7AlertTypes=self.__EMS7Cursor.fetchall()
				for EMS7AlertType in EMS7AlertTypes:
			#		print " >> EMS7 Alert ID %s" %EMS7AlertType[0]
			#		print " >> EMS7 Name %s" %EMS7AlertType[1] 
			#		print " >> EMS7 AlertClassTypedId %s " %EMS7AlertType[2]
			#		print " >> EMS7 CustomerId is %s " %EMS7CustomerId
			#		print " >> Adding a record"
					self.__EMS7Cursor.execute("insert into CustomerAlertType(CustomerId,AlertTypeId,LocationId,Name,Threshold,IsActive,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(EMS7CustomerId,EMS7AlertType[0],EMS7LocationId,AlertName,Threshold,IsEnabled,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId))
					EMS7CustomerAlertTypeId = self.__EMS7Cursor.lastrowid 
					self.__EMS7Cursor.execute("insert into CustomerAlertMapping(EMS7CustomerAlertTypeId,EMS6AlertId,EMS6CustomerId,AlertName,ModifiedDate) values(%s,%s,%s,%s,%s)",(EMS7CustomerAlertTypeId,EMS6AlertId,EMS6CustomerId,AlertName,LastModifiedGMT))
			self.__EMS7Connection.commit()
		else:
			IsMultiKey=0
			self.__InsertIntoDiscardedRecords(EMS6CustomerId,tableName,IsMultiKey)
			
	def __printCustomerAlert(self, CustomerAlert):
		print " -- Alert Id %s" %CustomerAlert[0]
		print " -- PaystationGroupId %s" %CustomerAlert[1]
		print " -- CustomerId %s" %CustomerAlert[2]
		print " -- AlertName %s" %CustomerAlert[3]
		print " -- AlertTypeId %s" %CustomerAlert[4]
		print " -- Threshold %s" %CustomerAlert[5]
		print " -- Email %s " %CustomerAlert[6]
		print " -- IsEnabled %s " %CustomerAlert[7]
		print " -- IsDeleted %s " %CustomerAlert[8]
		print " -- RegionId %s " %CustomerAlert[9]
		print " -- IsPaystationBased %s " %CustomerAlert[10]

	def addLocationPOSLogToEMS7(self, RegionPaystationLog, Action):
		self.__printLocationPOSLog(RegionPaystationLog)
		LastModifiedByUserId = 1 
		Id=RegionPaystationLog[0]
		RegionId=RegionPaystationLog[1]
		EMS6PaystationId=RegionPaystationLog[2]
		CreationDate=RegionPaystationLog[3]
		EMS7PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		EMS7LocationId = self.__getLocationId(RegionId)
		if(EMS7LocationId and EMS7PointOfSaleId):
			self.__EMS7Cursor.execute("insert ignore into LocationPOSLog(LocationId,PointOfSaleId,AssignedGMT,LastModifiedByUserId) values(%s,%s,%s,%s)",(EMS7LocationId,EMS7PointOfSaleId,CreationDate,LastModifiedByUserId))
			self.__EMS7Connection.commit()
		else:
			print " LocationId does not exist for the region id %s" %RegionId

	def __printLocationPOSLog(self, RegionPaystationLog):
		print " -- Id %s" %RegionPaystationLog[0]
		print " -- RegionId %s" %RegionPaystationLog[1]
		print " -- PaystationId %s" %RegionPaystationLog[2]
		print " -- CreationDate %s" %RegionPaystationLog[3]

	def addClusterToEMS7(self, Cluster, Action):
		self.__printCluster(Cluster)
		VERSION = 1
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		Id = Cluster[0]
		Name = Cluster[1]
		Hostname = Cluster[2]
		LocalPort = Cluster[3]
		self.__EMS7Cursor.execute("insert into Cluster(Id, Name,Hostname,LocalPort,VERSION,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(Id, Name,Hostname,LocalPort,VERSION,LastModifiedGMT,LastModifiedByUserId))
		self.__EMS7Connection.commit()

	def __printCluster(self, Cluster):
		print " -- Id %s" %Cluster[0]
		print " -- Name %s" %Cluster[1]
		print " -- Hostname %s" %Cluster[2]
		print " -- LocalPort %s" %Cluster[3]

	def addCollectionLockToEMS7(self, CollectionLock, Action):
		self.__printCollectionLock(CollectionLock)
		VERSION = 1
		LastModifiedGMT=date.today()
		LastModifiedByUserId=1
		EMS6ClusterId = CollectionLock[0]
		LockGMT = CollectionLock[1]
		BatchSize = CollectionLock[2]
		LockableCount = CollectionLock[3]
		LockedCount = CollectionLock[4]
		RecalcedCount = CollectionLock[5]
		IsReissuedLock = CollectionLock[6]
		RecalcMode = CollectionLock[7]
		RecalcEndGMT = CollectionLock[8]
		# NOTE !!! Alert !!! This insertion is based on the assumption that the ClusterId's match between EMS6 and EMS7 databases, This would require a manual loading on teh Cluster table without an auto increment set
#		ClusterId = getClusterId()
		if (RecalcEndGMT):
			self.__EMS7Cursor.execute("insert into CollectionLock(ClusterId,LockGMT,BatchSize,LockableCount,LockedCount,RecalcedCount,RecalcEndGMT) values(%s,%s,%s,%s,%s,%s,%s)",(EMS6ClusterId,LockGMT,BatchSize,LockableCount,LockedCount,RecalcedCount,RecalcEndGMT))
			self.__EMS7Connection.commit()

	def __printCollectionLock(self, CollectionLock):
		print " -- ClusterId %s" %CollectionLock[0]
		print " -- LockGMT %s" %CollectionLock[1]
		print " -- BatchSize %s" %CollectionLock[2]	
		print " -- LockableCount %s" %CollectionLock[3]
		print " -- LockedCount %s" %CollectionLock[4]
		print " -- RecalcedCount %s" %CollectionLock[5]
		print " -- IsReissuedLock %s" %CollectionLock[6]
		print " -- RecalcMode %s" %CollectionLock[7]
		print " -- RecalcEndGMT %s" %CollectionLock[8]		

	def addPOSBalanceToEMS7(self, PaystationBalance, Action):
		self.__printPOSBalance(PaystationBalance)
		EMS6PaystationId = PaystationBalance[0]
		CashDollars = PaystationBalance[1]
		CoinCount = PaystationBalance[2]
		CoinDollars = PaystationBalance[3]
		BillCount = PaystationBalance[4]
		BillDollars = PaystationBalance[5]
		UnsettledCreditCardCount = PaystationBalance[6]
		UnsettledCreditCardDollars = PaystationBalance[7]
		TotalDollars = PaystationBalance[8]
		LastCashCollectionGMT = PaystationBalance[9]
		LastCoinCollectionGMT = PaystationBalance[10]
		LastBillCollectionGMT = PaystationBalance[11]
		LastCardCollectionGMT = PaystationBalance[12]
		LastRecalcGMT = PaystationBalance[13]
		CollectionRecalcGMT = PaystationBalance[14]
		CollectionRecalcTransactions = PaystationBalance[15]
		ClusterId = PaystationBalance[16]	
		HasRecentCollection = PaystationBalance[17]
		IsRecalcable = PaystationBalance[18]		
		LastCollectionTypeId = PaystationBalance[19]
		CollectionLockId = PaystationBalance[20]
		PrevCollectionLockId = PaystationBalance[21]
		PrevPrevCollectionLockId = PaystationBalance[22]
		NextRecalcGMT = PaystationBalance[23]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if (PointOfSaleId):
	#		self.__EMS7Cursor.execute("insert into POSBalance(PointOfSaleId, ClusterId, CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, CashAmount, CoinCount, CoinAmount, BillCount, BillAmount, UnsettledCreditCardCount, UnsettledCreditCardAmount, TotalAmount, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId, LastRecalcGMT, IsRecalcable, NextRecalcGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId, ClusterId, CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, CashDollars, CoinCount, CoinDollars, BillCount , BillDollars, UnsettledCreditCardCount, UnsettledCreditCardDollars, TotalDollars, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastCollectionTypeId, LastRecalcGMT, IsRecalcable, NextRecalcGMT))
			self.__EMS7Connection.commit()

	def __printPOSBalance(self, PaystationBalance):
		print " -- PaystationId %s" %PaystationBalance[0]
		print " -- CashDollars %s" %PaystationBalance[1]
		print " -- CoinCount %s" %PaystationBalance[2]
		print " -- CoinDollars %s" %PaystationBalance[3]
		print " -- BillCount %s" %PaystationBalance[4]
		print " -- BillDollars %s" %PaystationBalance[5]
		print " -- UnsettledCreditCardCounti %s" %PaystationBalance[6]
		print " -- UnsettledCreditCardDollars %s" %PaystationBalance[7]
		print " -- TotalDollars %s" %PaystationBalance[8]
		print " -- LastCashCollectionGMT %s" %PaystationBalance[9]
		print " -- LastCoinCollectionGMT %s" %PaystationBalance[10]
		print " -- LastBillCollectionGMT %s" %PaystationBalance[11]
		print " -- LastCardCollectionGMT %s" %PaystationBalance[12]
		print " -- LastRecalcGMT %s" %PaystationBalance[13]
		print " -- CollectionRecalcGMT %s" %PaystationBalance[14]
		print " -- CollectionRecalcTransactions %s" %PaystationBalance[15]
		print " -- ClusterId %s" %PaystationBalance[16]	
		print " -- HasRecentCollection %s" %PaystationBalance[17]
		print " -- IsRecalcable %s" %PaystationBalance[18]		
		print " -- LastCollectionTypeId %s" %PaystationBalance[19]
		print " -- CollectionLockId %s" %PaystationBalance[20]
		print " -- PrevCollectionLockId %s" %PaystationBalance[21]
		print " -- PrevPrevCollectionLockId %s" %PaystationBalance[22]
		print " -- NextRecalcGMT %s" %PaystationBalance[23]

	def addPOSHeartBeatToEMS7(self, PaystationHeartBeat , Action):
		self.__printPaystationHeartBeat(PaystationHeartBeat)
		EMS6PaystationId= PaystationHeartBeat[0]
		LastHeartBeatGMT = PaystationHeartBeat[1]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("insert into POSHeartbeat(PointOfSaleId,LastHeartbeatGMT) values(%s,%s)",(PointOfSaleId,LastHeartBeatGMT))
			self.__EMS7Connection.commit()

	def __printPaystationHeartBeat(self, PaystationHeartBeat):
		print " -- Paystation %s " %PaystationHeartBeat[0]
		print " -- LastHeartBeatGMT %s" %PaystationHeartBeat[1]

	def addPOSServiceStateToEMS7(self, ServiceState, Action):
		self.__printPOSServiceState(ServiceState)
		VERSION = 1
		LastModifiedGMT = date.today()
		EMS6PaystationId=ServiceState[0]
		version=ServiceState[1]
		LastHeartBeat=ServiceState[2]
		IsDoorOpen=ServiceState[3]
		IsDoorUpperOpen=ServiceState[4]
		IsDoorLowerOpen=ServiceState[5]
		IsInServiceMode=ServiceState[6]
		Battery1Voltage=ServiceState[7]
		Battery2Voltage=ServiceState[8]
		IsBillAcceptor=ServiceState[9]
		IsBillAcceptorJam=ServiceState[10]
		IsBillAcceptorUnableToStack=ServiceState[11]
		IsBillStacker=ServiceState[12]
		BillStackerSize=ServiceState[13]
		BillStackerCount=ServiceState[14]
		IsCardReader=ServiceState[15]
		IsCoinAcceptor=ServiceState[16]
		CoinBagCount=ServiceState[17]
		IsCoinHopper1=ServiceState[18]
		CoinHopper1Level=ServiceState[19]
		CoinHopper1DispensedCount=ServiceState[20]
		IsCoinHopper2=ServiceState[21]
		CoinHopper2Level=ServiceState[22]
		CoinHopper2DispensedCount=ServiceState[23]
		IsPrinter=ServiceState[24]
		IsPrinterCutterError=ServiceState[25]
		IsPrinterHeadError=ServiceState[26]
		IsPrinterLeverDisengaged=ServiceState[27]
		PrinterPaperLevel=ServiceState[28]
		IsPrinterTemperatureError=ServiceState[29]
		IsPrinterVoltageError=ServiceState[30]
		Battery1Level=ServiceState[31]
		Battery2Level=ServiceState[32]
		IsLowPowerShutdownOn=ServiceState[33]
		IsShockAlarmOn=ServiceState[34]
		WirelessSignalStrength=ServiceState[35]
		IsCoinAcceptorJam=ServiceState[36]
		BillStackerLevel=ServiceState[37]
		TotalSinceLastAudit=ServiceState[38]
		CoinChangerLevel=ServiceState[39]
		IsCoinChanger=ServiceState[40]
		IsCoinChangerJam=ServiceState[41]
		LotNumber=ServiceState[42]
		MachineNumber=ServiceState[43]
		BBSerialNumber=ServiceState[44]
		PrimaryVersion=ServiceState[45]
		SecondaryVersion=ServiceState[46]
		UpgradeDate=ServiceState[47]
		AlarmState=ServiceState[48]
		IsNewLotSetting=ServiceState[49]
		LastLotSettingUpload=ServiceState[50]
		IsNewTicketFooter=ServiceState[51]
		IsNewPublicKey=ServiceState[52]
		AmbientTemperature=ServiceState[53]
		ControllerTemperature=ServiceState[54]
		InputCurrent=ServiceState[55]
		SystemLoad=ServiceState[56]
		RelativeHumidity=ServiceState[57]
		IsCoinCanister=ServiceState[58]
		IsCoinCanisterRemoved=ServiceState[59]
		IsBillCanisterRemoved=ServiceState[60]
		IsMaintenanceDoorOpen=ServiceState[61]
		IsCashVaultDoorOpen=ServiceState[62]
		IsCoinEscrow=ServiceState[63]
		IsCoinEscrowJam=ServiceState[64]
		IsCommunicationAlerted=ServiceState[65]
		IsCollectionAlerted=ServiceState[66]
		UserDefinedAlarmState=ServiceState[67]
		PointOfSaleId = self.__getPointOfSaleId(EMS6PaystationId)
		# Is RFID Card reader a new column in EMS7.POSServicveState table, if not where is the value for the column arriving from in EMS7
		if(PointOfSaleId):
			self.__EMS7Cursor.execute("insert into POSServiceState(PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastPaystationSettingUploadGMT, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalAmountSinceLastAudit, UpgradeGMT, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewPaystationSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(PointOfSaleId, AmbientTemperature, Battery1Level, Battery1Voltage, Battery2Level, Battery2Voltage, BBSerialNumber, BillStackerCount, BillStackerLevel, BillStackerSize, CoinBagCount, CoinChangerLevel, CoinHopper1DispensedCount, CoinHopper1Level, CoinHopper2DispensedCount, CoinHopper2Level, ControllerTemperature, InputCurrent, LastLotSettingUpload, PrimaryVersion, PrinterPaperLevel, RelativeHumidity, SecondaryVersion, SystemLoad, TotalSinceLastAudit, UpgradeDate, WirelessSignalStrength, IsBillAcceptor, IsBillStacker, IsCardReader, IsCoinAcceptor, IsCoinCanister, IsCoinChanger, IsCoinEscrow, IsCoinHopper1, IsCoinHopper2, IsPrinter, IsNewLotSetting, IsNewPublicKey, IsNewTicketFooter, VERSION, LastModifiedGMT))
		self.__EMS7Connection.commit()

	def __printPOSServiceState(self, ServiceState):
		print " -- PaystationId %s" %ServiceState[0]
		print " -- version %s" %ServiceState[1]
		print " -- LastHeartBeat %s" %ServiceState[2]
		print " -- IsDoorOpen %s" %ServiceState[3]
		print " -- IsDoorUpperOpen %s" %ServiceState[4]
		print " -- IsDoorLowerOpen %s" %ServiceState[5]
		print " -- IsInServiceMode %s" %ServiceState[6]
		print " -- Battery1Voltage %s" %ServiceState[7]
		print " -- Battery2Voltage %s" %ServiceState[8]
		print " -- IsBillAcceptor %s" %ServiceState[9]
		print " -- IsBillAcceptorJam %s" %ServiceState[10]
		print " -- IsBillAcceptorUnableToStack %s" %ServiceState[11]
		print " -- IsBillStacker %s" %ServiceState[12]
		print " -- BillStackerSize %s" %ServiceState[13]
		print " -- BillStackerCount %s" %ServiceState[14]
		print " -- IsCardReader %s" %ServiceState[15]
		print " -- IsCoinAcceptor %s" %ServiceState[16]
		print " -- CoinBagCount %s" %ServiceState[17]
		print " -- IsCoinHopper1 %s" %ServiceState[18]
		print " -- CoinHopper1Level %s" %ServiceState[19]
		print " -- CoinHopper1DispensedCount %s" %ServiceState[20]
		print " -- IsCoinHopper2 %s" %ServiceState[21]
		print " -- CoinHopper2Level %s" %ServiceState[22]
		print " -- CoinHopper2DispensedCount %s" %ServiceState[23]
		print " -- IsPrinter %s" %ServiceState[24]
		print " -- IsPrinterCutterError %s" %ServiceState[25]
		print " -- IsPrinterHeadError %s" %ServiceState[26]
		print " -- IsPrinterLeverDisengaged %s" %ServiceState[27]
		print " -- PrinterPaperLevel %s" %ServiceState[28]
		print " -- IsPrinterTemperatureError %s" %ServiceState[29]
		print " -- IsPrinterVoltageError %s" %ServiceState[30]
		print " -- Battery1Level %s" %ServiceState[31]
		print " -- Battery2Level %s" %ServiceState[32]
		print " -- IsLowPowerShutdownOn %s" %ServiceState[33]
		print " -- IsShockAlarmOn %s" %ServiceState[34]
		print " -- WirelessSignalStrength %s" %ServiceState[35]
		print " -- IsCoinAcceptorJam %s" %ServiceState[36]
		print " -- BillStackerLevel %s" %ServiceState[37]
		print " -- TotalSinceLastAudit %s" %ServiceState[38]
		print " -- CoinChangerLevel %s" %ServiceState[39]
		print " -- IsCoinChanger %s" %ServiceState[40]
		print " -- IsCoinChangerJam %s" %ServiceState[41]
		print " -- LotNumber %s" %ServiceState[42]
		print " -- MachineNumber %s" %ServiceState[43]
		print " -- BBSerialNumber %s" %ServiceState[44]
		print " -- PrimaryVersion %s" %ServiceState[45]
		print " -- SecondaryVersion %s" %ServiceState[46]
		print " -- UpgradeDate %s" %ServiceState[47]
		print " -- AlarmState %s" %ServiceState[48]
		print " -- IsNewLotSetting %s" %ServiceState[49]
		print " -- LastLotSettingUpload %s" %ServiceState[50]
		print " -- IsNewTicketFooter %s" %ServiceState[51]
		print " -- IsNewPublicKey %s" %ServiceState[52]
		print " -- AmbientTemperature %s" %ServiceState[53]
		print " -- ControllerTemperature %s" %ServiceState[54]
		print " -- InputCurrent %s" %ServiceState[55]
		print " -- SystemLoad %s" %ServiceState[56]
		print " -- RelativeHumidity %s" %ServiceState[57]
		print " -- IsCoinCanister %s" %ServiceState[58]
		print " -- IsCoinCanisterRemoved %s" %ServiceState[59]
		print " -- IsBillCanisterRemoved %s" %ServiceState[60]
		print " -- IsMaintenanceDoorOpen %s" %ServiceState[61]
		print " -- IsCashVaultDoorOpen %s" %ServiceState[62]
		print " -- IsCoinEscrow %s" %ServiceState[63]
		print " -- IsCoinEscrowJam %s" %ServiceState[64]
		print " -- IsCommunicationAlerted %s" %ServiceState[65]
		print " -- IsCollectionAlerted %s" %ServiceState[66]
		print " -- UserDefinedAlarmState %s" %ServiceState[67]			



