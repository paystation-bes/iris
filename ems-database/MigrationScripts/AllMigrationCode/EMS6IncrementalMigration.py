#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess

class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccess, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}

	def __migrateCustomers(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id from Customer where Id=%s", EMS6Id)
				customer=self.__EMS6Cursor.fetchall()
				for cust in customer:
					self.__EMS7DataAccess.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)		

			elif (Action=='Insert' and IsProcessed==0):
				self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
				customer=self.__EMS6Cursor.fetchall()
				for cust in customer:
					self.__EMS7DataAccess.addCustomer(cust,Action)
					self.__updateMigrationQueue(MigrationId)

			elif (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
				EMS7CustomerId=self.__EMS7Cursor.fetchone()
				self.__EMS7Cursor.execute("delete from Customer where Id=%s",EMS7CustomerId)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print " -- MySQL Error %s" %e			

	def __migrateCustomerProperties(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				CustomerProperties = self.__getCustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				CustomerProperties = self.__getCustomerProperties(EMS6Id)
				for custProperties in CustomerProperties:
					self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7DataAccess.deleteCustomerPropertyFromEMS7(EMS6Id, Action)	
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print " --MySQL Error %s" %e

	def __getCustomerProperties(self,EMS6Id):
		CustomerProperty=None
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,TimeZone,SMSWarningPeriod from CustomerProperties where CustomerId=%s", EMS6Id)
		if (self.__EMS6Cursor.rowcount<>0):
	                CustomerProperty=self.__EMS6Cursor.fetchall()
			return CustomerProperty

	def __migrateCustomerSubscription(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			Split=MultiKeyId.split(',')
			SubscriptionTypeId=None
			CustomerId = Split[0]
			ServiceId= Split[1]
			if (ServiceId=="1"):
				SubscriptionTypeId=100
			if (ServiceId=="2"):
				SubscriptionTypeId=600
			if (ServiceId=="3"):
				SubscriptionTypeId=200
			if(ServiceId=="4"):
				SubscriptionTypeId=700
			if(ServiceId=="5"):
				SubscriptionTypeId=300
			if(ServiceId=="6"):
				SubscriptionTypeId=1000
			if(ServiceId=="7"):
				SubscriptionTypeId=1200
			if(ServiceId=="8"):
				SubscriptionTypeId=900
			if(Action=='Update' and IsProcessed==0):
				CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Insert' and IsProcessed==0):
				CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
				for custCSR in CustomerServiceRelation:
					self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
					self.__updateMigrationQueue(MigrationId)
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS6CursorCerberus.execute("select distinct(EmsCustomerId) from cerberus_db.Customer where Id=%s",(CustomerId))
				if(self.__EMS6CursorCerberus.rowcount<>0):
					EMS6CustomerId=self.__EMS6CursorCerberus.fetchone()
				EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
				self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=0 where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print " --MySQL Error %s" %e

	def __getCustomerServiceRelation(self,CustomerId,ServiceId):
		CustomerServiceRelation=None
		self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId) where c.Id=%s and r.ServiceId=%s",(CustomerId,ServiceId))
		if(self.__EMS6CursorCerberus.rowcount<>0):
			CustomerServiceRelation=self.__EMS6CursorCerberus.fetchall()
		return CustomerServiceRelation

	def __migratePaystations(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				IsDeleted=0
				LastModifiedGMT=date.today()
				LastModifiedByUserId=1
				EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
						self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
				if(EMS6Paystations):
					for paystation in EMS6Paystations:
						self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
						self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(paystation,Action)

					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6PaystationId(self,EMS6Id):
		EMS6Rows = None
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId,RegionId,LotSettingId,p.ContactURL from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rows = self.__EMS6Cursor.fetchall()
		return EMS6Rows

	def __migrateLocations(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
		#	print " -- Action %s" %Action
		#	print " -- EMS6Id %s" %EMS6Id
		#	print " -- IsProcessed %s" %IsProcessed
		#	print " -- MigrationId %s" %MigrationId
			if (Action=='Update' and IsProcessed==0):
				EMS6Regions=self.__getEMS6Region(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				EMS6Regions=self.__getEMS6Region(EMS6Id)
				IsProcessed = 1
				for EMS6Region in EMS6Regions:
					self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
					self.__EMS7Cursor.execute("delete from Location where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6Region(self, EMS6Id):
		EMS6Region=None
		self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Region = self.__EMS6Cursor.fetchall()
		return EMS6Region	

	def __migrateLocationPOSLog(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			if (Action=='Update' and IsProcessed==0):
				EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
				IsProcessed = 1
				for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
					self.__EMS7DataAccess.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()
					self.__EMS7Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
				IsProcessed = 1
				for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
					self.__EMS7DataAccess.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS6Connection.commit()
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from LocationPOSLog where Id=(select distinct(EMS7LocationPOSLogId) from RegionPaystationLogMapping where EMS6RegionPaystationId=%s)",(EMS6Id))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6RegionPaystationLog(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id, RegionId, PaystationId, CreationDate from RegionPaystationLog where Id=%s", EMS6Id)
		return self.__EMS6Cursor.fetchall()

#	def __printLocation(self, Location):
#		print " -- EMS6 RegionId %s" %Location[0]
#		print " -- Version %s" %Location[1]
#		print " -- AlarmStats %s" %Location[2]
#		print " -- CustomerId %s" %Location[3]
#		print " -- Name %s" %Location[4]
#		print " -- Description %s" %Location[5]
#		print " -- ParentRegion %s" %Location[6]
#		print " -- IsDefault %s" %Location[7]
#		print " -- EMS7CustomerId %s" %Location[8]
#		print " -- EMS7LocationId %s" %Location[9]

	def __migrateCoupons(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
		#	print "Coupon %s" %Split[0]
			Coupon = Split[0]
		#	print "CustmerId %s" %Split[1]
			CustomerId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
				IsProcessed = 1
				for EMS6Coupon in EMS6Coupons:
					self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
                	print "-- Error %s" %e

	def __getEMS6Coupon(self, CustomerId, CouponId):
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null and CustomerId=%s and Coupon.Id=%s",(CustomerId,CouponId))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSetting(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			#	print " -- Action %s" %Action
			 #       print " -- IsProcessed %s" %IsProcessed
			  #      print " -- MigrationId %s" %MigrationId
			   #    	print " -- EMS6Id %s" %EMS6Id
				if(Action=='Insert' and IsProcessed==0):
					EMS6LotSettings=self.__getLotSetting(EMS6Id)
					for LotSetting in EMS6LotSettings:
						self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting, Action)
						self.__updateMigrationQueue(MigrationId)
				if(Action=='Update' and IsProcessed==0):
					EMS6LotSettings=self.__getLotSetting(EMS6Id)
					for LotSetting in EMS6LotSettings:
						self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting, Action)
						self.__updateMigrationQueue(MigrationId)
				if(Action=='Delete' and IsProcessed==0):
					try:
						self.__EMS7Cursor.execute("delete from SettingsFile where Id=(select EMS7LotSettingId from LotSettingMapping where EMS6LotSettingId=%s)",EMS6Id)
						self.__EMS7Connection.commit()
					except mdb.Error, e:   
						 print "-- MySQL Error %s" %e
					
	#               	self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s" %(MigrationId))
	#	                self.__EMS6Connection.commit()
		except mdb.Error, e:
                	print "-- Error %s" %e

	def __getLotSetting(self,EMS6Id):
		LotSetting = None
		self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL and l.Id=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			LotSetting = self.__EMS6Cursor.fetchall()
		return LotSetting
		
	def __updateMigrationQueue(self,MigrationId):
			self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s",(MigrationId))
			self.__EMS6Cursor.execute("delete from MigrationQueue where IsProcessed=1 and Id=%s",(MigrationId))
			EMS6Connection.commit()

        def __getEMS7CustomerId(self,Id):
                self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
                row=self.__EMS7Cursor.fetchone()
                EMS7CustomerId = row[0]
                return EMS7CustomerId

        def __getEMS7LocationId(self,EMS6RegionId):
                LocationId = None
                self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(EMS6RegionId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        LocationId = row[0]
                        return LocationId

        def __getEMS7PaystationId(self,EMS6PaystationId):
                PaystationId=None
                self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        PaystationId =row[0]
                        return PaystationId

	def __migrateAlert(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				alerts=self.__getEMS6Alerts(EMS6Id)
				for alert in alerts:
					self.__EMS7DataAccess.addCustomerAlertToEMS7(alert,Action)
					self.__EMS7DataAccess.addCustomerEmail(alert, Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				alerts=self.__getEMS6Alerts(EMS6Id)
				for alert in alerts:
					self.__EMS7DataAccess.addCustomerAlertToEMS7(alert,Action)
					self.__EMS7DataAccess.addCustomerEmail(alert, Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=(select distinct(EMS7Id) from CustomerAlertTypeMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6Alerts(self,EMS6Id):
		Alerts=None
		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert where Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			Alerts = self.__EMS6Cursor.fetchall()	
		return Alerts	

		#This code will be enabled if the Alerts are being deleted
#                elif (Action=='Delete' and IsProcessed==0):
 #                       self.__EMS7Cursor.execute("select distinct(EMS7AlertId) from CustomerAlertTypeMapping where EMS6AlertId=%s", EMS6Id)
  #                      EMS7CustomerId=self.__EMS7Cursor.fetchone()
#			self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=%s",EMS7CustomerId)
#			self.__EMS7Connection.commit()
#	                self.__updateMigrationQueue(MigrationId)

	def __migrateBadValueCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			AccountNumber= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
				IsProcessed = 1
				for BadValueCards in EMS6BadValueCards:
					self.__EMS7DataAccess.addBadValueCardToEMS7(BadValueCards, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
				IsProcessed = 1
				for BadValueCards in EMS6BadValueCards:
					self.__EMS7DataAccess.addBadValueCardToEMS7(BadValueCards, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				if (CustomerId<>None):
				# The section below updates the CustomerBadCard table, The card is uniquely identified by CustomerId and AccountNumber 
					CardTypeId = 3
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
						if (EMS7CustomerId):
							self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
							if(self.__EMS7Cursor.rowcount<>0):
								row = self.__EMS7Cursor.fetchone()
								EMS7CustomerCardTypeId=row[0]
								self.__EMS7Cursor.execute("select Id from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,AccountNumber))
								if(self.__EMS7Cursor.rowcount<>0):
									row =  self.__EMS7Cursor.fetchone()
									EMS7CustomerBadCardId = row[0]
									self.__EMS7Cursor.execute("delete from CustomerBadCard where Id=%s",(EMS7CustomerBadCardId))
									self.__updateMigrationQueue(MigrationId)
									self.__EMS7Connection.commit()
		except mdb.Error, e:
			print "-- Error %s" %e

#		if (Action=='Delete'):
#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
#			EMS7Connection.commit()

	def __migrateBadCreditCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			CardHash = Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6BadCreditCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
				IsProcessed = 1
				for BadCreditCard in EMS6BadCreditCards:
					self.__EMS7DataAccess.addBadCreditCardToEMS7(BadCreditCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				EMS6BadCreditCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
				IsProcessed = 1
				for BadCreditCard in EMS6BadCreditCards:
					self.__EMS7DataAccess.addBadCreditCardToEMS7(BadCreditCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete'):
				if(CustomerId):
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
					#This is the section Credit cards 
						CardTypeId = 1	
						self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
						if(self.__EMS7Cursor.rowcount<>0):
							row = self.__EMS7Cursor.fetchone()
							EMS7CustomerCardTypeId=row[0]
							self.__EMS7Cursor.execute("delete from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,CardHash))
							self.__EMS7Connection.commit()
							self.__updateMigrationQueue(MigrationId)
		except mdb.Error, e:
			print "-- Error %s" %e


	def __migrateBadSmartCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			CardNumber= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BadSmartCards=self.__getEMS6BadSmartCard(CustomerId,CardNumber)
				IsProcessed = 1
				for BadSmartCard in EMS6BadSmartCards:
					self.__EMS7DataAccess.addBadSmartCardToEMS7(BadSmartCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6BadSmartCards=self.__getEMS6BadSmartCard(CustomerId,CardNumber)
				IsProcessed = 1
				for BadSmartCard in EMS6BadSmartCards:
					self.__EMS7DataAccess.addBadSmartCardToEMS7(BadSmartCard, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete'):
				if (CustomerId<>None):
					self.__EMS7Cursor.execute("select EMS7CustomerId from CustomerMapping where EMS6CustomerId=%s",(CustomerId))
					if(self.__EMS7Cursor.rowcount<>0):
						row = self.__EMS7Cursor.fetchone()
						EMS7CustomerId = row[0]
					# This is the section bad Smart Cards 
						CardTypeId = 2
						self.__EMS7Cursor.execute("select Id from CustomerCardType where CustomerId=%s and CardTypeId=%s",(EMS7CustomerId,CardTypeId))
						if(self.__EMS7Cursor.rowcount<>0):
							row = self.__EMS7Cursor.fetchone()
							EMS7CustomerCardTypeId=row[0]
							self.__EMS7Cursor.execute("delete from CustomerBadCard where CustomerCardTypeId=%s and CardNumberOrHash=%s",(EMS7CustomerCardTypeId,CardNumber))
							self.__EMS7Connection.commit()
	#		if (Action=='Delete'):
	#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
	#			EMS7Connection.commit()
		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6BadCreditCard(self, CustomerId, CardHash):
		self.__EMS6Cursor.execute("select CustomerId,CardHash,CardData,CardExpiry,AddedDate,Comment from BadCreditCard where CustomerId=%s and CardHash=%s",(CustomerId,CardHash))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadValueCard(self, CustomerId, AccountNumber):
		self.__EMS6Cursor.execute("select CustomerId,AccountNumber,version,CardType from BadCard where CustomerId=%s and AccountNumber=%s",(CustomerId,AccountNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadSmartCard(self, CustomerId, CardNumber):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,AddedDate,Comment from BadSmartCard where CustomerId=%s and CardNumber=%s",(CustomerId, CardNumber))
		return self.__EMS6Cursor.fetchall()

	def __migrateBatteryInfo(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DateField= Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccess.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
				IsProcessed = 1
				for BatteryInfo in EMS6BatteryInfos:
					self.__EMS7DataAccess.addBatteryInfoToEMS7(BatteryInfo, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId = row[0]
					self.__EMS7Cursor.execute("delete from POSBatteryInfo where PointOfSaleId=%s and SensorGMT=%s",(PointOfSaleId,DateField))
					self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6BatteryInfo(self,PaystationId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo where PaystationId=%s and DateField=%s",(PaystationId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateCardRetryTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId=Split[0]           
			print "PaystationId %s" %PaystationId 
			PurchasedDate=Split[1]           
			print "PurchasedDate %s" %PurchasedDate
			TicketNumber=Split[2]            
			print "TicketNumber %s" %TicketNumber

			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				self.__EMS7DataAccess.addCardRetryTransactionToEMS7(MultiKeyId,Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
				print EMS6CardRetryTransactions[0]
				if(EMS6CardRetryTransactions):
					for EMS6CardRetryTransaction in EMS6CardRetryTransactions:
						self.__EMS7DataAccess.addCardRetryTransactionToEMS7(EMS6CardRetryTransaction,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
	#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
	#			if(EMS6CardRetryTransactions):
				self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6CardRetryTransactions(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID from CardRetryTransaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			row = self.__EMS6Cursor.fetchall()
		return row

	def __migrateEMSExtensiblePermit(self, Action, MultiKeyId, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s",(MultiKeyId))
				ExtensiblePermits = self.__EMS6Cursor.fetchall()
				IsProcessed = 1
				for ExtensiblePermit in ExtensiblePermits:
					self.__EMS7DataAccess.addExtensiblePermitToEMS7(ExtensiblePermit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Insert' and IsProcessed==0):
				self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s", MultiKeyId)
				ExtensiblePermits = self.__EMS6Cursor.fetchall()
				IsProcessed = 1
				for ExtensiblePermit in ExtensiblePermits:
					self.__EMS7DataAccess.addExtensiblePermitToEMS7(ExtensiblePermit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		except mdb.Error, e:
			print " --Error %s" %e

	def __migrateCryptoKey(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			Type = Split[0]
			KeyIndex = Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
				IsProcessed = 1
				for CryptoKey in EMS6CryptoKeys:
					self.__EMS7DataAccess.addCryptoKeyToEMS7(CryptoKey, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
				IsProcessed = 1
				for CryptoKey in EMS6CryptoKeys:
					self.__EMS7DataAccess.addCryptoKeyToEMS7(CryptoKey, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CryptoKey where Id=(select EMS7Id from CryptoKeyMapping where Type=%s and KeyIndex=%s)",(Type,KeyIndex))	
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except mdb.Error, e:
			print " --Error %s" %e

	def __getEMS6CryptoKey(self,Type,KeyIndex):
		self.__EMS6Cursor.execute("select Type,KeyIndex,Hash,Expiry,Signature,Info,NextHash,Status,CreateDate,Comment from CryptoKey where Type=%s and KeyIndex=%s",(Type,KeyIndex))
		return self.__EMS6Cursor.fetchall()

#EMS6.CustomerWsCal= EMS7.CustomerWebServiceCal
	def __migrateCustomerWsCal(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				CustomerWsCals=self.__getEMS6CustomerWsCal(EMS6Id)
				IsProcessed = 1
				for CustomerWsCal in CustomerWsCals:
					self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Insert' and IsProcessed==0):
				CustomerWsCals=self.__getEMS6CustomerWsCal(EMS6Id)
				IsProcessed = 1
				for CustomerWsCal in CustomerWsCals:
					self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CustomerWebServiceCal where Id = (select EMS7Id from CustomerWebServiceCalMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except mdb.Error, e:
			print " --Error: %s" %e

	def __getEMS6CustomerWsCal(self,EMS6Id):	
		CustomerWsCal=None
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal where Id=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			CustomerWsCal = self.__EMS6Cursor.fetchall()
		return CustomerWsCal		
			
## EMS6.CustomerWsToken = EMS7.WebServiceEndPoint
	def __migrateCustomerWsToken(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- EMS6Id %s" %EMS6Id
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				CustomerWsToken=self.__getEMS6CustomerWsToken(EMS6Id)
				IsProcessed = 1
				for CustomerWsToken in CustomerWsToken:
					self.__EMS7DataAccess.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				CustomerWsTokens=self.__getEMS6CustomerWsToken(EMS6Id)
				IsProcessed = 1
				for CustomerWsToken in CustomerWsTokens:
					self.__EMS7DataAccess.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from WebServiceEndPoint where Id = (select distinct(EMS7Id) from CustomerWsTokenMapping where EMS6Id=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except mdb.Error, e:
			print "-- Error %s" %e

	def __getEMS6CustomerWsToken(self, EMS6Id):
                self.__EMS6Cursor.execute("select CustomerId,Token,EndPointId,WsInUse,Id from CustomerWsToken where Id=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

 	def __migrateEMSParkingPermission(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RegionId = Split[0]
			StartTimeHourLocal = Split[1]
			StartTimeMinuteLocal = Split[2]

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
				IsProcessed = 1
				for EMSParkingPermission in EMS6EMSParkingPermissions:
					self.__EMS7DataAccess.addParkingPermissionToEMS7(EMSParkingPermission, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
			     EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
			     IsProcessed = 1
			     for EMSParkingPermission in EMS6EMSParkingPermissions:
				self.__EMS7DataAccess.addParkingPermissionToEMS7(self, EMSParkingPermission, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except mdb.Error, e:
			print "-- Error %s " %e

	def __getParkingPermission(self,RegionId,StartTimeHourLocal,StartTimeMinuteLocal):
		self.__EMS6Cursor.execute("select Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,PermissionTypeId,PermissionStatus,SpecialParkingPermissionId,MaxDurationMinutes,CreationDate,IsActive,Id from EMSParkingPermission where RegionId=%s and StartTimeHourLocal=%s and StartTimeMinuteLocal=%s",(RegionId,StartTimeHourLocal,StartTimeMinuteLocal))
		return self.__EMS6Cursor.fetchall()

## This Module only adds EMSRate to Unified Rate, The module next to it does the incremental migration of the EMSRates into Extensiblerate.
	def __migrateEMSRateIntoUnifiedRate(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			self.__migrateEMS7ExtensibleRate(Action,IsProcessed,MigrationId,EMS6Id)

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6EMSRates(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6EMSRates(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addEMSRatesToUnifiedRate(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select distinct(EMS7RateId) from EMSRateToUnifiedRateMapping where EMS6RateId=%s and EMS7RateId<>0)",EMS6Id)
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
			print "-- Error %s" %e
		
	def __getEMS6EMSRates(self,EMS6Id):
#		self.__EMS6Cursor.execute("select distinct r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")
		## Above SQL revised after incremental migration
		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
#		self.__EMS6Cursor.execute("select er.Id, r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id and er.Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMS7ExtensibleRate(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6EMSRates=self.__getEMS6ExtensibleRate(EMS6Id)
				IsProcessed = 1
				for EMSRate in EMS6EMSRates:
					self.__EMS7DataAccess.addExtensibleRatesToEMS7(EMSRate, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ExtensibleRate where Id=(select distinct(EMS7RateId) from ExtensibleRateMapping where EMS6RateId=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
		except mdb.Error, e:
			print "-- Errror %s" %e

	def __getEMS6ExtensibleRate(self,EMS6Id):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,IsActive,Id from EMSRate where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateSMSTransactionLog(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			SMSMessageTypeId = Split[3]
			
			if (Action=='Update' and IsProcessed==0):
				EMS6SMSTransactionLogs = self.__getEMS6SMSTransactionLog(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				IsProcessed = 1
				for SMSTransactionLog in EMS6SMSTransactionLogs:
					self.__EMS7DataAccess.addSMSTransactionLogToEMS7(SMSTransactionLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			
			if (Action=='Insert' and IsProcessed==0):
				EMS6SMSTransactionLogs = self.__getEMS6SMSTransactionLog(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				IsProcessed = 1
				for SMSTransactionLog in EMS6SMSTransactionLogs:
					self.__EMS7DataAccess.addSMSTransactionLogToEMS7(SMSTransactionLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				IsProcessed=1
				self.__EMS7Cursor.execute("delete from SmsTransactionLog where Id=(select Id from SMSTransactionLogMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s)",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		except mdb.Error, e:
			print " --Error %s" %e

	def __getEMS6SMSTransactionLog(self, PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId):
		self.__EMS6Cursor.execute("select SMSMessageTypeId,MobileNumber,PaystationId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp from SMSTransactionLog where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
		return self.__EMS6Cursor.fetchall()

	def __migrateEventLogNew(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			DeviceId = Split[1]
			TypeId = Split[2]
			ActionId = Split[3]
			DateField = Split[4]

			if (Action=='Update' and IsProcessed==0):
				EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccess.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
				IsProcessed = 1
				for EventLogNew in EMS6EventLogNews:
					self.__EMS7DataAccess.addPOSAlertToEMS7(EventLogNew, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
		except mdb.Error, e:
			print "-- Errror %s" %e
		
	def __getEventLogNew(self, PaystationId,DeviceId,TypeId,ActionId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,ClearDateField from EventLogNew where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",(PaystationId,DeviceId,TypeId,ActionId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSettingFileContent(self,Action,EMS6Id,IsProcessed,MigrationId):
        	if (Action=='Update' and IsProcessed==0):
			LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
			for LotSettingFileContent in LotSettingFileContents:
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
				self.__updateMigrationQueue(MigrationId)	
	
                elif (Action=='Insert' and IsProcessed==0):
			LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
			for LotSettingFileContent in LotSettingFileContents:
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
				self.__updateMigrationQueue(MigrationId)		

	def __getEMS6LotSettingFileContent(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __migrateMerchantAccount(self,Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- EMS6Id %s" %EMS6Id
		       
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				self.__EMS7DataAccess.addMerchantAccount(MultiKeyId, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6MerchantAccounts=self.__getEMS6MerchantAccount(EMS6Id)
				IsProcessed = 1
				for EMS6Merchant in EMS6MerchantAccounts:
					self.__EMS7DataAccess.addMerchantAccount(EMS6Merchant, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from MerchantAccount where Id=(select distinct(EMS7MerchantAccountId) from MerchantAccountMapping where EMS6MerchantAccountId=%s)",(EMS6Id))
				EMS7Connection.commit()

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6MerchantAccoun(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id,version,CustomerId,Name,Field1,Field2,Field3,IsDeleted,ProcessorName,ReferenceCounter,Field4,Field5,Field6,IsValid from MerchantAccount where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateServiceState(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- EMS6Id %s" %EMS6Id
		try:
			if (Action=='Update' and IsProcessed==0):
				EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
				IsProcessed = 1
				for EMS6ServiceState in EMS6ServiceStates:
					self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from POSServiceState where PointOfSaleId=(select po.Id from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s)",(EMS6Id))
				EMS7Connection.commit()

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6ServiceState(self,EMS6Id):
		ServiceState=None
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			ServiceState=self.__EMS6Cursor.fetchall()
		return ServiceState

	def __migrateRestAccount(self, Action, MultiKeyId, IsProcessed, MigrationId):
		try:
			print " -- Action %s" %Action
			print " -- MultiKeyId %s" %MultiKeyId
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId

			if (Action=='Update' and IsProcessed==0):
				RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)

			elif(Action=='Insert' and IsProcessed==0):
				RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
				IsProcessed = 1
				for RestAccount in RestAccounts:
					self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			elif(Action=='Delete' and IsProcessed==0):
				IsProcessed = 1
	#			self.__EMS7Cursor.execute("delete from RestAccount where Id=(select EMS7RestAccountId from RestAccountMapping where EMS6AccountName=%s",(MultiKeyId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6RestAccount(self, MultiKeyId):
		self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null and r.AccountName=%s",(MultiKeyId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRates(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
	#		print " -- MultiKeyId %s" %EMS6Id
			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			print " PaystationId is %s" %PaystationId
			PurchasedDate = Split[1]
			print " Purchasedate is %s" %PurchasedDate
			TicketNumber = Split[2]
			print " TicketNumeber %s" %TicketNumber
		       
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
				for Rates in EMS6Rates:
					self.__EMS7DataAccess.addRatesToEMS7(Rates, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
				for rates in EMS6Rates:
					self.__EMS7DataAccess.addRatesToEMS7(rates, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				IsProcessed = 1
				self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select EMS7UnifiedRateId from RateToUnifiedRateMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
                except mdb.Error, e:
                        print " --Error %s " %e

#	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
#		EMS6Rates=None	
#		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		if(self.__EMS6Cursor.rowcount<>0):
#			EMS6Rates=self.__EMS6Cursor.fetchall()
	#	return EMS6Rates
#		else:
#			print " no row was found"
#		return EMS6Rates

	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
		EMS6Rates=None
		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		self.__EMS6Cursor.execute("select PaystationId,TicketNumber,PurchasedDate,CustomerId,RegionId,LotNumber,TypeId,RateId,RateName,RateValue,Revenue from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rates=self.__EMS6Cursor.fetchall()
		return EMS6Rates

	def __migratePaystationGroup(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
		       
			if (Action=='Update' and IsProcessed==0):
				EMS6PaystationGroups = self.__getEMS6PaystationGroup(EMS6Id)
				IsProcessed = 1
				for PaystationGroup in EMS6PaystationGroups:
					self.__EMS7DataAccess.addPaystationGroupToEMS7(PaystationGroup, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6PaystationGroups = self.__getEMS6PaystationGroup(EMS6Id)
				IsProcessed = 1
				for PaystationGroup in EMS6PaystationGroups:
					self.__EMS7DataAccess.addPaystationGroupToEMS7(PaystationGroup, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from Route where Id=(select distinct(EMS7RouteId) from PaystationGroupRouteMapping where EMS6PaystationGroupId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except mdb.Error, e:
               		print " --Error %s" %e

	def __getEMS6PaystationGroup(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,CustomerId,Name from PaystationGroup where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateProcessorProperties(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			Processor = Split[0]
			Name = Split[1]
			
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
				for ProcessorProperties in EMS6ProcessorProperties:
					self.__EMS7DataAccess.addProcessorProperties(ProcessorProperties, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
				for ProcessorProperties in EMS6ProcessorProperties:
					self.__EMS7DataAccess.addProcessorProperties(ProcessorProperties, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				IsProcessed = 1
				self.__EMS7Cursor.execute("delete from ProcessorProperty where Id=(select EMS7Id from ProcessorPropertiesMapping where Processor=%s and Name=%s)",(Processor,Name))
				self.__updateMigrationQueue(MigrationId)
				EMS7Connection.commit()
		except mdb.Error, e:
               		print " --Error %s" %e

	def __getEMS6ProcessorProperties(self, Processor,Name):
		self.__EMS6Cursor.execute("select Processor,Name,Value from ProcessorProperties where Processor=%s and Name=%s",(Processor,Name))
		return self.__EMS6Cursor.fetchall()


	def __migrateSmsAlert(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				SMS=self.__getEMS6SMSAlert(EMS6Id)
				IsProcessed = 1
				for EMSSMS in SMS:
					self.__EMS7DataAccess.addSMSAlertToEMS7(EMSSMS, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				SMS=self.__getEMS6SMSAlert(EMS6Id)
				IsProcessed = 1
				for EMSSMS in SMS:
					self.__EMS7DataAccess.addSMSAlertToEMS7(EMSSMS, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from SmsAlert where Id=(select EMS7Id from SMSAlertMapping where MobileNumber=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

		except mdb.Error, e:
	       		print " --Error %s" %e

	def __getEMS6SMSAlert(self,EMS6Id):
                self.__EMS6Cursor.execute("select MobileNumber,ExpiryDate,PaystationId,PurchasedDate,TicketNumber,PlateNumber,StallNumber,RegionId,NumOfRetry,IsAlerted,IsLocked,IsAutoExtended from SMSAlert where MobileNumber=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

	def __migrateSMSFailedResponse(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
	#		print " -- MultiKeyId %s" %EMS6Id

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			SMSMessageTypeId = Split[3]
		       
			if (Action=='Update' and IsProcessed==0):
				IsProcessed = 1
				EMS6FailedResponses=self.__getEMS6SMSFailedResponse(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				for EMS6FailedResponse in EMS6FailedResponses:
					self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(EMS6FailedResponse, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6FailedResponses=self.__getEMS6SMSFailedResponse(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)
				for EMS6FailedResponse in EMS6FailedResponses:
					self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(EMS6FailedResponse, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				IsProcessed = 1
				self.__EMS7Cursor.execute("select EMS7SMSFailedResponseId from SMSFailedResponseMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId)) 
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					EMS7FailedResponseId = row[0]
					self.__EMS7Cursor.execute("delete from SmsFailedResponse where Id=(select EMS7SMSFailedResponseId from SMSFailedResponseMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s)",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6SMSFailedResponse(self,PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId):	
		SMSFailedReponse = None	
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,IsOk,TrackingId,Number,ConvertedNumber,DeferUntilOccursInThePast,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,BlockedReason,IsBalanceZero,IsInvalidCarrierCode from SMSFailedResponse where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and SMSMessageTypeId=%s",(PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId))
		if(self.__EMS6Cursor.rowcount<>0):
			SMSFailedReponse = self.__EMS6Cursor.fetchall()
		return SMSFailedReponse

	def __migrateModemSetting(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccess.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				ModemSettings=self.__getEMS6ModemSettings(EMS6Id)
				IsProcessed = 1
				for ModemSetting in ModemSettings:
					self.__EMS7DataAccess.addModemSettingToEMS7(ModemSetting, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from ModemSetting where Id=(select EMS7Id from ModemSettingMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6ModemSettings(self, EMS6Id):
		EMS6ModemSetting=None
		self.__EMS6Cursor.execute("select PaystationId, Type, CCID, Carrier, APN from ModemSetting where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6ModemSetting = self.__EMS6Cursor.fetchall()
		return EMS6ModemSetting 

#	def __getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
#		EMS6Rates=None	
#		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
#		if(self.__EMS6Cursor.rowcount<>0):
#			EMS6Rates=self.__EMS6Cursor.fetchall()
	#	return EMS6Rates
#		else:
#			print " no row was found"
#		return EMS6Rates

	def __migrateUserAccount(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id

			if (Action=='Update' and IsProcessed==0):
				UserAccounts=self.__getEMS6UserAccount(EMS6Id)
				IsProcessed = 1
				for UserAccount in UserAccounts:
					self.__EMS7DataAccess.addUserAccountToEMS7(UserAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				UserAccounts=self.__getEMS6UserAccount(EMS6Id)
				IsProcessed = 1
				for UserAccount in UserAccounts:
					self.__EMS7DataAccess.addUserAccountToEMS7(UserAccount, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from UserAccount where Id=(select distinct(EMS7Id) from UserAccountMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6UserAccount(self, EMS6Id):
		self.__EMS6Cursor.execute("select u.Id,u.version,u.CustomerId,u.Name,u.Password,u.RoleId,u.AccountStatus, concat(u.Name,'@',c.Name) as UserAccount,u.Id from UserAccount u left join Customer c on(c.Id=u.CustomerId) where u.Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateAudit(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			CollectionTypeId = Split[1]
			EndDate = Split[2]
			StartDate = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
				IsProcessed = 1
				for Audit in EMS6Audit:
					self.__EMS7DataAccess.addPOSCollectionToEMS7(Audit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
				IsProcessed = 1
				for Audit in EMS6Audit:
					self.__EMS7DataAccess.addPOSCollectionToEMS7(Audit, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select Id from PointOfSale where PaystationId=(select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s)",(PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId = row[0]	
				self.__EMS7Cursor.execute("delete from POSCollection where PointOfSaleId=%s and CollectionTypeId=%s and StartGMT=%s and EndGMT=%s",(PointOfSaleId,CollectionTypeId,StartDate,EndDate))
				self.__EMS7Connection.commit() 
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6Audit(self, PaystationId,CollectionTypeId,EndDate,StartDate):
#		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CustomerId from Audit")
		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CoinAmount,OnesAmount,TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DiscoverAmount,MasterCardAmount,OtherCardAmount,SmartCardAmount,VisaAmount,CitationsPaid,CitationAmount,ChangeIssued,RefundIssued,SmartCardRechargeAmount,ExcessPayment,CustomerId,ReportNumber,NextTicketNumber,AttendantTicketsSold,AttendantTicketsAmount,DinersAmount,AttendantDepositAmount,AcceptedFloatAmount,Tube1Type,Tube1Amount,Tube2Type,Tube2Amount,Tube3Type,Tube3Amount,Tube4Type,Tube4Amount,OverfillAmount,ReplenishedAmount,Hopper1Dispensed,Hopper2Dispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Replenished,Hopper2Replenished,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,CoinCount005,CoinCount010,CoinCount025,CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50 from Audit where PaystationId=%s and CollectionTypeId=%s and EndDate=%s and StartDate=%s",(PaystationId,CollectionTypeId,EndDate,StartDate))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransaction2Push(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			ThirdPartyServiceAccountId = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
				IsProcessed = 1
				for Transaction2Push in EMS6Transaction2Push:
					self.__EMS7DataAccess.addTransaction2Push(self, Transaction2Push, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
				IsProcessed = 1
				for Transaction2Push in EMS6Transaction2Push:
					self.__EMS7DataAccess.addTransaction2Push(self, Transaction2Push, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6Transaction2Push(self, PaystationId, PurchasedDate, TicketNumber, ThirdPartyServiceAccountId):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RegionId,RetryCount,LastRetryTime,IsOffline,Field1 from Transaction2Push where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and ThirdPartyServiceAccountId=%s",(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransactionPush(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			PaystationId = Split[0]
			PurchasedDate = Split[1]
			TicketNumber = Split[2]
			ThirdPartyServiceAccountId = Split[3]

			if (Action=='Update' and IsProcessed==0):
				EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
				IsProcessed = 1
				for TransactionPush in EMS6TransactionPush:
					self.__EMS7DataAccess.addTransactionPush(self, TransactionPush, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
				IsProcessed = 1
				for TransactionPush in EMS6TransactionPush:
					self.__EMS7DataAccess.addTransactionPush(self, TransactionPush, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from TransactionPush where Id=(select EMS7Id from TransactionPushMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
				self.__EMS7Connection.commit()
		except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6TransactionPush(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId, PurchasedDate, TicketNumber, CustomerId, LotNumber, PlateNumber, ExpiryDate, ChargedAmount, StallNumber, RetryCount, LastRetryTime, IsOffline from TransactionPush where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

#	def __migrateCustomerWsCal(self,Action,IsProcessed,MigrationId,EMS6Id):
 #               print " -- Action %s" %Action
  #              print " -- IsProcessed %s" %IsProcessed
   #             print " -- MigrationId %s" %MigrationId
    #            print " -- EMS6Id %s" %EMS6Id

     #           if (Action=='Update' and IsProcessed==0):
     #                   CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
      #                  IsProcessed = 1
       #                 for WebService in CustomerWsCal:
        #                        self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
         #                       self.__updateMigrationQueue(MigrationId)
          #                      self.__EMS7Connection.commit()

           #     if (Action=='Insert' and IsProcessed==0):
            #            CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
#                        IsProcessed = 1
 #                       for WebService in CustomerWsCal:
  #                              self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
   #                             self.__updateMigrationQueue(MigrationId)
    #                            self.__EMS7Connection.commit()

     #           if(Action=='Delete' and IsProcessed==0):
      #                  self.__EMS7Cursor.execute("delete from CustomerWsCalMapping where Id=(select EMS7Id from CustomerWsCalMapping where EMS6Id=%s",(EMS6Id))
#                        self.__EMS7Connection.commit()
 #                       self.__updateMigrationQueue(MigrationId)

	def __getEMS6CustomerWebServiceCal(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description from CustomerWsCal where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestSessionToken(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %MultiKeyId

			if (Action=='Update' and IsProcessed==0):
				RestSessionToken=self.__getEMS6RestSessionToken(MultiKeyId)
				IsProcessed = 1
				for RestSession in RestSessionToken:
					self.__EMS7DataAccess.addRestSessionToken(RestSession, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				RestSessionToken=self.__getEMS6RestSessionToken(MultiKeyId)
				IsProcessed = 1
				for RestSession in RestSessionToken:
					self.__EMS7DataAccess.addRestSessionToken(RestSession, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RestSessionToken where Id=(select EMS7Id from RestSessionTokenMapping where EMS6AccountName=%s)",(MultiKeyId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
		except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6RestSessionToken(self, EMS6Id):	
		self.__EMS6Cursor.execute("select AccountName,SessionToken,CreationDate,ExpiryDate from RESTSessionToken where AccountName=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLog(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKey Id %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RESTAccountName = Split[0]
			EndpointName = Split[1]
			LoggingDate = Split[2]
			IsError = Split[3]
			CustomerId = Split[4]

			if (Action=='Update' and IsProcessed==0):
				RestLogs=self.__getEMS6RestLogs(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
				IsProcessed = 1
				for RestLog in RestLogs:
					self.__EMS7DataAccess.addRestLogToEMS7(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				RestLogs=self.__getEMS6RestLogs(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
				IsProcessed = 1
				for RestLog in RestLogs:
					self.__EMS7DataAccess.addRestLogToEMS7(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RestLog where Id=(select EMS7Id from RestLogMapping where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s)",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6RestLogs(self,RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId):
		self.__EMS6Cursor.execute("select RestAccountName, EndpointName, LoggingDate, IsError, CustomerId, TotalCalls from RESTLog where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLogTotalCall(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKey Id %s" %MultiKeyId

			Split=MultiKeyId.split(',')
			RESTAccountName = Split[0]
			LoggingDate = Split[1]

			if (Action=='Update' and IsProcessed==0):
				RestLogTotalCall=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
				IsProcessed = 1
				for RestLog in RestLogTotalCall:
					self.__EMS7DataAccess.addRestLogTotalCall(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				RestLogTotalCalls=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
				IsProcessed = 1
				for RestLog in RestLogTotalCalls:
					self.__EMS7DataAccess.addRestLogTotalCall(RestLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RestLogTotalCall where Id=(select distinct(EMS7Id) from RestLogTotalCallMapping where RESTAccountName=%s and LoggingDate=%s)",(RESTAccountName,LoggingDate))
				self.__EMS7Connection.commit()
                except mdb.Error, e:
                        print " --Error %s" %e

	def __migrateReplenish(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split = MultiKeyId.split(',')
			PaystationId = Split[0]
			Date = Split[1]
			Number = Split[2]
			if (Action=='Update' and IsProcessed==0):
				EMS6Replenish=self.__getEMS6Replenish(PaystationId,Date,Number)
				IsProcessed = 1
				for Replenish in EMS6Replenish:
					self.__EMS7DataAccess.addReplenishToEMS7(Replenish, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6Replenish=self.__getEMS6Replenish(PaystationId,Date,Number)
				IsProcessed = 1
				for Replenish in EMS6Replenish:
					self.__EMS7DataAccess.addReplenishToEMS7(Replenish, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Delete'):
				self.__EMS7Cursor.execute("delete from Replenish where Id=(select distinct(EMS7Id) from ReplenishMapping where PaystationId=%s and Date=%s and Number=%s)",(PaystationId,Date,Number))
				EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6Replenish(self, PaystationId,Date,Number):
		self.__EMS6Cursor.execute("select PaystationId, Date, Number, TypeId, Tube1Type, Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount, Tube2CurrentCount, Tube3Type, Tube3ChangedCount, Tube3CurrentCount, Tube4Type, Tube4ChangedCount, Tube4CurrentCount, CoinBag005AddedCount, CoinBag010AddedCount, CoinBag025AddedCount, CoinBag100AddedCount, CoinBag200AddedCount from Replenish where PaystationId=%s and Date=%s and Number=%s",(PaystationId,Date,Number))
		return self.__EMS6Cursor.fetchall()

	def __migrateCustomerCardType(self,Action,IsProcessed,MigrationId,EMS6Id):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- EMS6Id %s" %EMS6Id
			
			if (Action=='Update' and IsProcessed==0):
				CustomerCardTypes=self.__getEMS6CustomerCardType(EMS6Id)
				IsProcessed = 1
				for CustomerCardType in CustomerCardTypes:
					self.__EMS7DataAccess.addCustomerCardTypeToEMS7(CustomerCardType, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Insert' and IsProcessed==0):
				CustomerCardTypes=self.__getEMS6CustomerCardType(EMS6Id)
				IsProcessed = 1
				for CustomerCardType in CustomerCardTypes:
					self.__EMS7DataAccess.addCustomerCardTypeToEMS7(CustomerCardType, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from CustomerCardType where Id=(select EMS7Id from CustomerCardTypeMapping where EMS6Id=%s)",(EMS6Id))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6CustomerCardType(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,version,Name,Track2RegEx,CheckDigitAlg,Description,AuthorizationMethod,Id from CardType where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6RestLogTotalCall(self,RESTAccountName,LoggingDate):	
		self.__EMS6Cursor.execute("select RESTAccountName,LoggingDate,TotalCalls from RESTLogTotalCall where RESTAccountName=%s and LoggingDate=%s",(RESTAccountName,LoggingDate))
		return self.__EMS6Cursor.fetchall()

	def __migratePaystation_PaystationGroup(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			Split=MultiKeyId.split(',')
			PaystationGroupId = Split[0]
			PaystationId= Split[1]
			if (Action=='Update' and IsProcessed==0):
				PaystationGroups=self.__getEMS6Paystation_PaystationGroup(PaystationGroupId,PaystationId)
				for Paystation in PaystationGroups:
					self.__EMS7DataAccess.addPOSRouteToEMS7(Paystation,Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				PaystationGroups=self.__getEMS6Paystation_PaystationGroup(PaystationGroupId,PaystationId)
				for Paystation in PaystationGroups:
					self.__EMS7DataAccess.addPOSRouteToEMS7(Paystation,Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("delete from RoutePOS where Id=(select distinct(EMS7Id) from RoutePOSMapping where PaystationGroupId=%s and PaystationId=%s)",(PaystationGroupId,PaystationId))
				self.__EMS7Connection.commit()
				self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e
	
	def __getEMS6Paystation_PaystationGroup(self, PaystationGroupId,PaystationId):
		self.__EMS6Cursor.execute("select PaystationGroupId, PaystationId from Paystation_PaystationGroup where PaystationId=%s and PaystationGroupId=%s",(PaystationId,PaystationGroupId))
		if(self.__EMS6Cursor.rowcount<>0):
			return self.__EMS6Cursor.fetchall()

	def __migrateTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId, TableName):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- Display MultiKeyId now %s" %MultiKeyId
			print " -- TableName %s" %TableName

			Split=MultiKeyId.split(',')
			CustomerId = Split[0]
			PaystationId = Split[1]
			TicketNumber = Split[2]
			LotNumber = Split[3]
			StallNumber = Split[4]
			TypeId = Split[5]
			PurchasedDate = Split[6]
			ExpiryDate = Split[7]
			ChargedAmount = Split[8]
			CashPaid = Split[9]
			CardPaid = Split[10]
			ChangeDispensed = Split[11]
			ExcessPayment = Split[12]
			IsRefundSlip = Split[13]
			CustomCardData = Split[14]
			CustomCardType = Split[15]
			CouponNumber = Split[16]
			CitationNumber = Split[17]
			SmartCardData = Split[18]
			SmartCardPaid = Split[19]
			PaymentTypeId = Split[20]
			CreationDate = Split[21]
			RateId = Split[22]
			AuthenticationCard = Split[23]
			RegionId = Split[24]
			AddTimeNum = Split[25]
			OriginalAmount = Split[26]
			IsOffline = Split[27]
			PlateNumber = Split[28]

			# This section populates the array for addValueCardToEMS7
				
			if (Action=='Update' and IsProcessed==0):
				print "Update section has been disabled on purpose, following no updates being received in transactional records"
				## For smart card and value card we never send an update so the section below is being disabled
	#			if(TableName == 'CustomerCard' or TableName =='SmartCard'):
	#				print "CustomerId %s" %CustomerId
	#				print "Regionid %s" %RegionId
	#				print "PaystationId %s" %PaystationId
	#				print "CustomCardData %s" %CustomCardData
	#				print "SmartCardData %s" %SmartCardData
	#				ValueCardToEMS7 = [CustomerId,RegionId,PaystationId,CustomCardData,SmartCardData]
	#				self.__EMS7DataAccess.addValueCardToEMS7(ValueCardToEMS7,Action)
	#				self.__updateMigrationQueue(MigrationId)
	#				self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				if(TableName=='CustomerCard'):
	#			Adding a module to Add CustomerValueCard to the Transaction table
					if(CustomCardData):
						ValueCards = self.__getEMS6ValueSmartCardType(PaystationId, PurchasedDate,TicketNumber)
						if(ValueCards):
							for valueCard in ValueCards:
								self.__EMS7DataAccess.addValueCardToEMS7(valueCard,Action)
								self.__updateMigrationQueue(MigrationId)
								self.__EMS7Connection.commit()
				if(TableName=='SmartCard'):
					if(SmartCardData):
						SmartCards = self.__getEMS6SmartCardType(PaystationId,PurchasedDate,TicketNumber)
						for smartcard in SmartCards:
							print "Customer Id %s" %smartcard[0]
							print "ResgionID %s" %smartcard[1]
							print "PaystationId %s" %smartcard[2]
							print "SmartCard %s" %smartcard[3]
							self.__EMS7DataAccess.addSmartCardToEMS7(smartcard, Action)
							self.__updateMigrationQueue(MigrationId)
							self.__EMS7Connection.commit()
				if(TableName=='Purchase'):
					print " Inside the Purchase Section >>>>>>>>"
					print " PaystationId %s" %PaystationId
					print " PurchasedDate %s" %PurchasedDate
					print " TicketNumber %s" %TicketNumber
					Purchases = self.__getEMS6TransactionForPurchase(PaystationId,PurchasedDate,TicketNumber)
					for purchase in Purchases:
						self.__EMS7DataAccess.addPurchaseToEMS7(purchase,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()
				if(TableName=='Permit'):
					print " Inside the Permit table  >>>>>"
					Permits = self.__getEMS6TransactionForPermit(PaystationId,PurchasedDate,TicketNumber)
					for permit in Permits:
						self.__EMS7DataAccess.addPermitToEMS7(permit,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()
				if(TableName=='PaymentCard'):
					print " Inside the PaymentCard table  >>>>"
					print " PaystationId %s" %PaystationId
					print " PurchasedDate %s" %PurchasedDate
					print " TicketNumber %s" %TicketNumber
					PaymentCards = self.__getEMS6TransactionForPayment(PaystationId,PurchasedDate,TicketNumber)
					for payment in PaymentCards:
						self.__EMS7DataAccess.addPaymentCardToEMS7(payment,Action)
						self.__updateMigrationQueue(MigrationId)
						self.__EMS7Connection.commit()

			if(Action=='Delete' and IsProcessed==0):
				# There are is no update and delete being done to transactional data containing smart card and value card
				print "Section disabled following no deletes for transaction record"
	#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
	#			if(EMS6CardRetryTransactions):
			#	self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
			#	EMS7Connection.commit()
			#	self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6TransactionForPayment(self, PaystationId,PurchasedDate,TicketNumber):
		#Rewrite the query to lookup TransactionProcessorId for every transaction that comes up, make sure there is a single Processor transaction id returned (SubQuery) as there could be multiple processor transaction record for a single transaction, we need to find a unique processor transaction record by looking up the max(procssing date) in processor transaction table
		self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,pt.MerchantAccountId,pt.Amount,pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,pt.Approved,t.SmartCardData,t.SmartCardPaid,t.CustomCardData,t.CustomCardType,t.CustomerId,max(pt.ProcessingDate) from trust t left join ProcessorTransaction pt using (PaystationId,PurchasedDate,TicketNumber) where t.PaystationId=%s and Approved=1 and t.PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall() 

	def __getEMS6TransactionForPermit(self, PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber from trust tr left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6TransactionForPurchase(self, PaystationId,PurchasedDate,TicketNumber):
#Section below commented out to merge Purchae and Permit data in one query
		self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate,tr.SmartCardPaid from trust tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber) where tr.PaystationId=%s and tr.PurchasedDate=%s and tr.TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6ValueSmartCardType(self,PaystationId, PurchasedDate,TicketNumber):
		print "PaystationId %s" %PaystationId
		print "PurchasedDate %s" %PurchasedDate
		print "TicketNumber %s" %TicketNumber
		ValueCards = None
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from trust where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and CustomCardData<>''",(PaystationId, PurchasedDate, TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			ValueCards = self.__EMS6Cursor.fetchall()	
		return ValueCards

	def __getEMS6SmartCardType(self,PaystationId,PurchasedDate,TicketNumber):
		print " PaystationId %s" %PaystationId
		print " PurchaseDate %s" %PurchasedDate
		print " TicketNumber %s" %TicketNumber

		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from trust where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __migratePreAuth(self, Action, EMS6Id, IsProcessed, MigrationId):
                if (Action=='Update' and IsProcessed==0):
			EMS6PreAuths=self.__getPreAuth(EMS6Id)
			if(EMS6PreAuths):
				for preauth in EMS6PreAuths:
					self.__EMS7DataAccess.addPreAuth(preauth, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Insert' and IsProcessed==0):
			EMS6PreAuths=self.__getPreAuth(EMS6Id)
			if(EMS6PreAuths):
				for preauth in EMS6PreAuths:
					self.__EMS7DataAccess.addPreAuth(preauth, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Delete' and IsProcessed==0):
			try:
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from PreAuth where Id=(select distinct(EMS7Id) from PreAuthMapping where EMS6Id=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			except mdb.Error, e:
				print "-- Error %s" %e

	def __getPreAuth(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId from PreAuth where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migratePreAuthHolding(self, Action, EMS6Id, IsProcessed, MigrationId):
                if (Action=='Update' and IsProcessed==0):
			EMS6PreAuths=self.__getPreAuthHolding(EMS6Id)
			if(EMS6PreAuths):
				for preauth in EMS6PreAuths:
					self.__EMS7DataAccess.addPreAuthHolding(preauth, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Insert' and IsProcessed==0):
			EMS6PreAuths=self.__getPreAuthHolding(EMS6Id)
			if(EMS6PreAuths):
				for preauth in EMS6PreAuths:
					self.__EMS7DataAccess.addPreAuthHolding(preauth, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Delete' and IsProcessed==0):
			try:
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from PreAuthHolding where Id=(select distinct(EMS7Id) from PreAuthHoldingMapping where EMS6Id=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			except mdb.Error, e:
				print "-- Error %s" %e

	def __getPreAuthHolding(self, EMS6Id):
                self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,IsRFID,AddedGMT,MovedGMT,CardExpiry from PreAuthHolding where Id=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

	def __migrateProcessorTransaction(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			Split=MultiKeyId.split(',')
			TypeId = Split[0]
			PaystationId = Split[1]
			MerchantAccountId = Split[2]
			TicketNumber = Split[3]
			Amount = Split[4]
			CardType = Split[5]
			Last4DigitsOfCardNumber = Split[6]
			CardChecksum = Split[7]
			PurchasedDate = Split[8]
			ProcessingDate = Split[9]
			ProcessorTransactionId = Split[10]
			AuthorizationNumber = Split[11]
			ReferenceNumber = Split[12]
			Approved = Split[13]
			CardHash = Split[14]
			IsUploadedFromBoss = Split[15]
			IsRFID = Split[16]
			if (Action=='Update' and IsProcessed==0):
				ProcessorTransactions = self.__getProcessorTransaction(PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate)
				for processorTransaction in ProcessorTransactions:
					self.__EMS7DataAccess.addProcessorTransaction(processorTransaction,Action)
					self.__updateMigrationQueue(MigrationId)		

			if (Action=='Insert' and IsProcessed==0):
				Split = MultiKeyId.split(',')
				self.__EMS7DataAccess.addProcessorTransaction(Split,Action)
				self.__updateMigrationQueue(MigrationId)		

			if (Action=='Delete' and IsProcessed==0):
				self.__EMS7Cursor.execute("select p.Id from PaystationMapping pm,PointOfSale p where pm.EMS7PaystationId=p.PaystationId and pm.EMS6PaystationId=%s",(EMS6PaystationId))
				if(self.__EMS7Cursor.rowcount<>0):
					row = self.__EMS7Cursor.fetchone()
					PointOfSaleId= row[0]
					if(PointOfSaleId):
						self.__EMS7Cursor.execute("delete from ProcessorTransaction where PointOfSaleId=%s,PurchasedDate=%s,TicketNumber=%s,IsApproved=%s,ProcessorTransactionTypeId=%s,ProcessingDate=%s)",(PointOfSaleId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate))
						self.__EMS7Connection.commit()
						self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getProcessorTransaction(self, PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate):
		self.__EMS6Cursor.execute("select TypeId, PaystationId, MerchantAccountId, TicketNumber, Amount, CardType, Last4DigitsOfCardNumber, CardChecksum, PurchasedDate, ProcessingDate, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, Approved, CardHash, IsUploadedFromBoss, IsRFID from ProcessorTransaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and Approved=%s and TypeId=%s and ProcessingDate=%s ",(PaystationId,PurchasedDate,TicketNumber,Approved,TypeId,ProcessingDate))
		return self.__EMS6Cursor.fetchall()

	def __migratePaystationforPOSDate(self, Action, EMS6Id, IsProcessed, MigrationId):
                if (Action=='Update' and IsProcessed==0):
			EMS6Paystations=self.__getEMS6PaystationForPOSDate(EMS6Id)
			if(EMS6Paystations):
				self.__EMS7DataAccess.addPOSDatePaystationPropertiesToEMS7(EMS6Paystations, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Insert' and IsProcessed==0):
                        EMS6Paystations=self.__getEMS6PaystationForPOSDate(EMS6Id)
			if(EMS6Paystations):
				self.__EMS7DataAccess.addPOSDatePaystationPropertiesToEMS7(EMS6Paystations, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Delete' and IsProcessed==0):
			try:
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

			except mdb.Error, e:
				print "-- Error %s" %e

	def __getEMS6PaystationForPOSDate(self, EMS6Id):
       		self.__EMS6Cursor.execute("select Id,version,Name,CommAddress,ContactURL,CustomerID,IsActive,RegionId,TimeZone,LockState,ProvisionDate,MerchantAccountId,DeleteFlag,CustomCardMerchantAccountID,PaystationType,IsVerrus,QueryStallsBy,LotSettingId from Paystation where Id=%s",EMS6Id)
		row = self.__EMS6Cursor.fetchone()
                return row 

	def __migrateTaxes(self,Action,EMS6Id,IsProcessed,MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				Taxes = self.__getTaxes(EMS6Id)
				for Tax in Taxes:
					self.__EMS7DataAccess.addPurchaseTax(Tax,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				Taxes = self.__getTaxes(EMS6Id)
				for Tax in Taxes:
					self.__EMS7DataAccess.addPurchaseTax(Tax,Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				self.__updateMigrationQueue(MigrationId)
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getTaxes(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,PaystationId,TicketNumber,PurchasedDate,ProcessingDate,CustomerId,RegionId,TaxName,TaxRate,TaxValue from Taxes where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateWsCallLogs(self,Action,IsProcessed,MigrationId,MultiKeyId):
		try:
			print " -- Action %s" %Action
			print " -- IsProcessed %s" %IsProcessed
			print " -- MigrationId %s" %MigrationId
			print " -- MultiKeyId %s" %MultiKeyId
			Split=MultiKeyId.split(',')
			Token = Split[0]
			CallDate  = Split[1]
			if (Action=='Update' and IsProcessed==0):
				EMS6WsCallLogs=self.__getEMS6WsCallLog(Token,CallDate)
				IsProcessed = 1
				for EMS6WsCallLog in EMS6WsCallLogs:
					self.__EMS7DataAccess.addWebServiceCallLog(EMS6WsCallLog, Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
			if (Action=='Insert' and IsProcessed==0):
				EMS6WsCallLogs=self.__getEMS6WsCallLog(Token,CallDate)
				IsProcessed = 1
				for EMS6WsCallLog in EMS6WsCallLogs:
					self.__EMS7DataAccess.addWebServiceCallLog(EMS6WsCallLog, Action)
				#        self.__updateMigrationQueue(MigrationId)
				 #       self.__EMS7Connection.commit()
	#                if (Action=='Delete'):
	 #                       self.__EMS7Cursor.execute("delete from WsCallLog where Id=(select distinct(EMS7WsCallLogId) from WsCallLogMapping where EMS6WsCallLogId=%s and EMS6CustomerId=%s)",(WsCallLog,CustomerId))
	  #                      EMS7Connection.commit()
	   #                     self.__updateMigrationQueue(MigrationId)

                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6WsCallLog(self, Token,CallDate):
		self.__EMS6Cursor.execute("select Token,CallDate,CustomerId,TotalCall from WsCallLog where Token=%s and CallDate=%s",(Token,CallDate))
		return self.__EMS6Cursor.fetchall()

	def __migrateCCFailLog(self, Action, EMS6Id, IsProcessed, MigrationId):
		try:
			if (Action=='Update' and IsProcessed==0):
				IsDeleted=0
				LastModifiedGMT=date.today()
				LastModifiedByUserId=1
				EMS6CCFailLogs=self.__getEMS6CCFailLog(EMS6Id)
				if(EMS6CCFailLogs):
					for CCFailLog in EMS6CCFailLogs:
						self.__EMS7DataAccess.addCCFailLogToEMS7(CCFailLog, Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Insert' and IsProcessed==0):
				IsProcessed = 1
				EMS6CCFailLogs=self.__getEMS6CCFailLog(EMS6Id)
				if(EMS6CCFailLogs):
					for CCFailLog in EMS6CCFailLogs:
						self.__EMS7DataAccess.addCCFailLogToEMS7(CCFailLog, Action)
					self.__updateMigrationQueue(MigrationId)

			if (Action=='Delete' and IsProcessed==0):
				if(EMS6Id):
					self.__EMS7Cursor.execute("delete from CCFailLog where Id=(select distinct(EMS7Id) from CCFailLogMapping where EMS6Id=%s)",(EMS6Id))
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()
                except mdb.Error, e:
                        print " --Error %s" %e

	def __getEMS6CCFailLog(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,PaystationId,MerchantAccountId,TicketNumber,ProcessingDate,PurchasedDate,CCType,Reason from CCFailLog where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def IncrementalMigration(self):
		self.__EMS6Cursor.execute("select * FROM MigrationQueue")
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
	#	input = raw_input("Start Migration  ........................... y/n ?   :   ")

	#	if (input=='y') :
		for migration in MigrationQueue:
			MigrationId = migration[0]
			MultiKeyId = migration[1]
			EMS6Id = migration[2]
			TableName = migration[3]
			Action = migration[4]
			IsProcessed = migration[5]
			Date = migration[6]
			''' Begin Transaction '''
			if(IsProcessed==0):
				if (TableName=='Customer'):
					self.__migrateCustomers(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='CustomerProperties'):
					self.__migrateCustomerProperties(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='Customer_Service_Relation'):
					self.__migrateCustomerSubscription(Action,IsProcessed,MigrationId,MultiKeyId)
				if (TableName=='Paystation'):
					self.__migratePaystations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Region'):
					self.__migrateLocations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Coupon'):
					self.__migrateCoupons(Action,IsProcessed,MigrationId,MultiKeyId)
				if (TableName=='LotSetting'):
					self.__migrateLotSetting(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='RegionPaystationLog'):
					self.__migrateLocationPOSLog(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='ParkingPermission'):
					self.__migrateEMSParkingPermission(Action,IsProcessed,MigrationId,MultiKeyId)
				## Test for RestAccount is pending
				if(TableName=='RESTAccount'):
					self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
				if(TableName=='BadCard'):
					self.__migrateBadValueCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BadSmartCard'):
					self.__migrateBadSmartCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BadCreditCard'):
					self.__migrateBadCreditCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BatteryInfo'):
					self.__migrateBatteryInfo(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='CardRetryTransaction'):
					self.__migrateCardRetryTransaction(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='MerchantAccount'):
					self.__migrateMerchantAccount(Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id)
				if(TableName=='ServiceState'):
					self.__migrateServiceState(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='RESTAccount'):
					self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
				if(TableName=='Rates'):
					self.__migrateRates(Action,IsProcessed,MigrationId,MultiKeyId)
				# The section below does incremental migration for EMSRates INTO UnifiedRates, and EMSRates INTO ExtensibleRates
				if(TableName=='EMSRate'):
					self.__migrateEMSRateIntoUnifiedRate(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='SMSTransactionLog'):
					self.__migrateSMSTransactionLog(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='SMSAlert'):
					self.__migrateSmsAlert(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='SMSFailedResponse'):
					self.__migrateSMSFailedResponse(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='ModemSetting'):
					self.__migrateModemSetting(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='LotSettingFileContent'):
					self.__migrateLotSettingFileContent(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='CryptoKey'):
					self.__migrateCryptoKey(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='RESTLog'):
					self.__migrateRestLog(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='RESTSessionToken'):
					self.__migrateRestSessionToken(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='RESTLogTotalCall'):
					self.__migrateRestLogTotalCall(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='Alert'):
					self.__migrateAlert(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='CustomerWsCal'):
					self.__migrateCustomerWsCal(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='CustomerWsToken'):
					self.__migrateCustomerWsToken(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='Replenish'):
					self.__migrateReplenish(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='CardType'):
					self.__migrateCustomerCardType(Action,IsProcessed,MigrationId,EMS6Id)
#				if(TableName=='PaystationGroup'):  This modeule still needs testing
#					self.__migratePaystationGroup(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='Paystation_PaystationGroup'):
					self.__migratePaystation_PaystationGroup(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='CustomerCard' or TableName=='SmartCard' or TableName=='Purchase' or TableName=='Permit' or TableName=='PaymentCard'):
					self.__migrateTransaction(Action, IsProcessed, MigrationId, MultiKeyId,TableName)
				if(TableName=='ProcessorProperties'):
					self.__migrateProcessorProperties(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='ProcessorTransaction'):
					self.__migrateProcessorTransaction(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='EMSExtensiblePermit'):
					self.__migrateEMSExtensiblePermit(Action, MultiKeyId, IsProcessed, MigrationId)
				if(TableName=='UserAccount'):
					self.__migrateUserAccount(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='EventLogNew'):
					self.__migrateEventLogNew(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='PaystationForPOSDate'):
					self.__migratePaystationforPOSDate(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='Audit'):
					self.__migrateAudit(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='Taxes'):
					self.__migrateTaxes(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='WsCallLog'):
					self.__migrateWsCallLogs(Action,IsProcessed,MigrationId,MultiKeyId)
				if(TableName=='PreAuth'):
					self.__migratePreAuth(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='PreAuthHolding'):
					self.__migratePreAuthHolding(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='CCFailLog'):
					self.__migrateCCFailLog(Action, EMS6Id, IsProcessed, MigrationId)

try:
	EMS6Connection = mdb.connect('172.16.1.141', 'root', 'testitnow', 'ems_db');
	EMS6ConnectionCerberus = mdb.connect('172.16.1.141', 'root', 'testitnow','cerberus_db');
	EMS7Connection = mdb.connect('172.16.1.143', 'root','testitnow','EMS7')

	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), 1)
	startMigration.IncrementalMigration()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
    if EMS6Connection:
        EMS6Connection.close()
    if EMS7Connection:
        EMS7Connection.close()
