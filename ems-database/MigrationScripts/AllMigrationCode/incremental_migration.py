#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys
import time
from datetime import date

try:
        ''' Initialize EMS6, and EMS7 Connection '''
        EMS6Connection = mdb.connect('127.0.0.1', 'vijay','test', 'ems_db');
        EMS7Connection = mdb.connect('172.16.1.153','EMS7','test','EMS7');

        EMS6Cursor = EMS6Connection.cursor()
        EMS7Cursor = EMS7Connection.cursor()

        ''' Import Customer Data '''
        EMS6Cursor.execute("select * FROM MigrationQueue")
        MigrationQueue=EMS6Cursor.fetchall()
        today = date.today()
	input = raw_input("Start Migration  ........................... y/n ?   :   ")

        if (input=='y') :
                for migration in MigrationQueue:
                        MigrationId = migration[0]
			EMS6Id = migration[1]
                        TableName = migration[2]
                        Action = migration[3]
                        Date = migration[4]
                        IsProcessed = migration[5]
                        print "The action is %s " %Action
                        ''' Begin Transaction '''
			
			if (TableName=='Customer'):
				self.__MigrateCustomer(self, Action)

	def __migrateCustomers(self, Action):						
		IsProcessed = 1
		IsParent = 0	
		if (Action=='Update'):
			Transaction=EMS7Connection.begin()
			EMS6Cursor.execute("select Id,version,Name,IF(AccountStatus='ENABLED',1,0) from Customer where Id=%s", EMS6Id)
			EMS6Customers=EMS6Cursor.fetchall()
			for EMS6Customer in EMS6Customers:
				today = date.today()
				IsProcessed = 1
				Id = EMS6Customer[0]
				print "The EMS6 Customer Id is %s" %Id
				version = EMS6Customer[1]
				print "The version is %s" %version
				Name = EMS6Customer[2]
				print "The Name is %s" %Name
				AccountStatus = EMS6Customer[3]
				print "The AccountStatus is %s "  %AccountStatus
				EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
				EMS7CustomerId=EMS7Cursor.fetchone()
				print "The Corrosponding EMS7 Customer ID is %s" %EMS7CustomerId[0]
				EMS7Cursor.execute("update Customer set Name='%s', VERSION=%s, CustomerStatusTypeId=%s where Id=%s" %(Name,version,AccountStatus,EMS7CustomerId[0]))
				EMS6Cursor.execute("update MigrationQueue set IsProcessed=%s where EMS6Id=%s" %(IsProcessed,Id))
				EMS6Connection.commit()	
				EMS7Connection.commit()

		elif (Action=='Insert'):
			EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
			customer=EMS6Cursor.fetchall()
			for cust in customer:
				CustomerTypeId = cust[0]
				CustomerAccountStatus = cust[1]
				Name = cust[2]
				Version = cust[3]
				LastModifiedDate = cust[4]
				LastModifiedByUser = cust[5]
				CustomerId = cust[6]
				Transaction=EMS7Connection.begin()
				EMS7Cursor.execute("insert into Customer(CustomerTypeId,CustomerStatusTypeId,Name,VERSION,IsParent,LastModifiedGMT,LastModifiedByUserId) values(%s,%s,%s,%s,%s,%s,%s)",(CustomerTypeId,CustomerAccountStatus,Name,Version,IsParent,LastModifiedDate,LastModifiedByUser))
				EMS7CustomerId=EMS7Cursor.lastrowid
				EMS7Cursor.execute("insert ignore into CustomerMapping(EMS7CustomerId,EMS6CustomerId,EMS7CustomerName,EMS6CustomerName) values(%s,%s,%s,%s)",(EMS7CustomerId,CustomerId,Name,Name))
				EMS6Cursor.execute("update MigrationQueue set IsProcessed=%s where EMS6Id=%s" %(IsProcessed,EMS6Id))
				EMS7Connection.commit()
				EMS6Connection.commit()

		elif (Action=='Delete'):
			EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
			EMS7CustomerId=EMS7Cursor.fetchone()
			print "The Corrosponding EMS7 Customer ID is %s" %EMS7CustomerId[0]
			EMS7Cursor.execute("delete from Customer where Id=%s",(EMS7CustomerId[0]))
			EMS6Cursor.execute("update MigrationQueue set IsProcessed=%s where EMS6Id=%s" %(IsProcessed,EMS6Id))
			EMS7Connection.commit()
			EMS6Connection.commit()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
    if EMS6Connection:
        EMS6Connection.close()
    if EMS7Connection:
        EMS7Connection.close()

