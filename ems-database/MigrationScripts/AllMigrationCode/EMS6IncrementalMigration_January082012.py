#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6Migration import EMS6Migration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess

class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccess, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def __migrateCustomers(self,Action,EMS6Id,IsProcessed,MigrationId):
                if (Action=='Update' and IsProcessed==0):
                        Transaction=EMS7Connection.begin()
                        self.__EMS6Cursor.execute("select Id,version,Name,IF(AccountStatus='ENABLED',1,0) from Customer where Id=%s", EMS6Id)
                        EMS6Customers=self.__EMS6Cursor.fetchall()
                        for EMS6Customer in EMS6Customers:
                                today = date.today()
                                IsProcessed = 1
                                Id = EMS6Customer[0]
                                version = EMS6Customer[1]
                                Name = EMS6Customer[2]
                                AccountStatus = EMS6Customer[3]
                                self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
                                EMS7CustomerId=self.__EMS7Cursor.fetchone()
                                self.__EMS7Cursor.execute("update Customer set Name='%s', VERSION=%s, CustomerStatusTypeId=%s where Id=%s" %(Name,version,AccountStatus,EMS7CustomerId[0]))
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                elif (Action=='Insert' and IsProcessed==0):
                        self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
                        IsProcessed = 1
                        IsParent = 0
                        customer=self.__EMS6Cursor.fetchall()
                        for cust in customer:
				print "calling the EMS7DataAccess to add the Customer"
				self.__EMS7DataAccess.addCustomer(cust)
           	                self.__updateMigrationQueue(MigrationId)

                elif (Action=='Delete' and IsProcessed==0):
                        self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
                        IsProcessed = 1
                        EMS7CustomerId=self.__EMS7Cursor.fetchone()
                        self.__EMS7Cursor.execute("delete from Customer where Id=%s",(EMS7CustomerId[0]))
                        self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()

	def __migrateCustomerProperties(self,Action,EMS6Id,IsProcessed,MigrationId):
		if (Action=='Update' and IsProcessed==0):
			CustomerProperties = self.__getCustomerProperties(EMS6Id)
			for custProperties in CustomerProperties:
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
				self.__updateMigrationQueue(MigrationId)

		if (Action=='Insert' and IsProcessed==0):
			CustomerProperties = self.__getCustomerProperties(EMS6Id)
			for custProperties in CustomerProperties:
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
				self.__updateMigrationQueue(MigrationId)

	def __getCustomerProperties(self,EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,TimeZone,SMSWarningPeriod from CustomerProperties where CustomerId=%s", EMS6Id)
		if ( self.__EMS6Cursor.rowcount<>0):
	                CustomerProperties=self.__EMS6Cursor.fetchall()
			return CustomerProperties

	def __migrateCustomerSubscription(self,Action,IsProcessed,MigrationId,MultiKeyId):
		Split=MultiKeyId.split(',')
                print "CustomerId %s" %Split[0]
                CustomerId = Split[0]
                print "ServiceId %s" %Split[1]
                ServiceId= Split[1]
		if(Action=='Update' and IsProcessed==0):
			CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
			for custCSR in CustomerServiceRelation:
				self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
				self.__updateMigrationQueue(MigrationId)

		if(Action=='Insert' and IsProcessed==0):
			CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
			for custCSR in CustomerServiceRelation:
                                self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
                                self.__updateMigrationQueue(MigrationId)

	def __getCustomerServiceRelation(self,CustomerId,ServiceId):
		self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId) where c.Id=%s and r.ServiceId=%s",(CustomerId,ServiceId))
		if(self.__EMS6CursorCerberus.rowcount<>0):
			CustomerServiceRelation=self.__EMS6CursorCerberus.fetchall()
			return CustomerServiceRelation

	def __migratePaystations(self, Action, EMS6Id, IsProcessed, MigrationId):
                if (Action=='Update' and IsProcessed==0):
                        IsDeleted=0
                        LastModifiedGMT=date.today()
                        LastModifiedByUserId=1
                        print "Update Paystation Module !!"
                        print "EMS6 Paystation Id is %s " %EMS6Id
                        self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,version from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
                        EMS6Paystations = self.__EMS6Cursor.fetchall()
                        for paystation in EMS6Paystations:
                                PaystationId = paystation[0]
                                print " -- EMS6PaystationId %s " %PaystationId
                                PaystationType = paystation[1]
                                ModemTypeId = paystation[2]
                                SerialNumber = paystation[3]
                                Version = paystation[4]
                                EMS7PaystationId = self.__getEMS7PaystationId(PaystationId)
                                print " -- EMS7PaystationId %s" %EMS7PaystationId
                                self.__EMS7Cursor.execute("update Paystation set PaystationTypeId=%s, ModemTypeId=%s,SerialNumber=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s",(PaystationType,ModemTypeId,SerialNumber,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,EMS7PaystationId))
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
                                self.__EMS6Connection.commit()

                elif (Action=='Insert' and IsProcessed==0):
                        print "Inserting into a Paystation table "
                        self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId,RegionId,LotSettingId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
                        IsProcessed = 1
                        EMS6Paystations=self.__EMS6Cursor.fetchall()
                        for paystation in EMS6Paystations:
				self.__EMS7DataAccess.addPaystationToEMS7(paystation)
				self.__updateMigrationQueue(MigrationId)

                elif (Action=='Delete' and IsProcessed==0):
			print " we are trying to go and delete again"
			print " The Paystation Id being deleted is %s " %EMS6Id
                        self.__EMS7Cursor.execute("set foreign_key_checks=0;delete from Paystation where Id=%s", EMS6Id)
                        self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()

	def __migrateLocations(self, Action, EMS6Id, IsProcessed, MigrationId):
                print " -- Incremental migration for Location -- "
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId
		if (Action=='Update' and IsProcessed==0):
                        print "Incremental migration for Region"
                        self.__EMS6Cursor.execute("select * from Region where Id=%s", EMS6Id)
                        EMS6Regions=self.__EMS6Cursor.fetchall()
                        IsProcessed = 1
                        for EMS6Region in EMS6Regions:
                                IsDeleted = 1
                                LastModifiedGMT = date.today()
                                ModifiedDate = date.today()
                                LastModifiedByUserId = 1
                                TargetMonthlyRevenue = 0
                                NumberOfSpaces = 0
                                IsParent = 0
                                IsUnassigned = 0
                                Version=0
                                EMS6RegionId = EMS6Region[0]
                                print " EMS6 Region Id %s" %EMS6RegionId
                                version = EMS6Region[1]
                                print " The version is %s" %version
                                AlarmState = EMS6Region[2]
                                print " The Alarm state is %s" %AlarmState
                                EMS6CustomerId = EMS6Region[3]
                                print " The Customer Id is %s" %EMS6CustomerId
                                Name = EMS6Region[4]
                                print " The Name is %s" %Name
                                Description = EMS6Region[5]
                                print " The Decription is %s" %Description
                                ParentRegion = EMS6Region[6]
                                print " The parent region is %s" %ParentRegion
                                IsDefault = EMS6Region[7]
                                print " The Isdefault is %s" %IsDefault
                                EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
                                print " The EMS7 Customer Id is %s" %EMS7CustomerId
                                EMS7LocationId=self.__getEMS7LocationId(EMS6RegionId)
                                print " The EMS7LocationID is %s" %EMS7LocationId
                                self.__EMS7Cursor.execute("update Location set CustomerId=%s,ParentLocationId=%s,Name='%s',NumberOfSpaces=%s,TargetMonthlyRevenue=%s,Description='%s',IsParent=%s,IsUnassigned=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT='%s',LastModifiedByUserId=%s where Id=%s" %(EMS7CustomerId,ParentRegion,Name,NumberOfSpaces,TargetMonthlyRevenue,Description,IsParent,IsUnassigned,IsDeleted,Version,LastModifiedGMT,LastModifiedByUserId,EMS7LocationId))
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
                        print "Insert into a Location table"
                        self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region, Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Id=%s", EMS6Id)
			EMS6Regions=self.__EMS6Cursor.fetchall()
                        for region in EMS6Regions:
				self.__EMS7DataAccess.addLocationToEMS7(region)
				self.__updateMigrationQueue(MigrationId)

                elif(Action=='Delete'):
                        self.__EMS7Cursor.execute("select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s", (EMS6Id))
                        EMS7LocationID = self.__EMS7Cursor.fetchone()
                        self.__EMS7Cursor.execute("delete from Location where Id=%s",(EMS7LocationID))
                        self.__EMS7Connection.commit()

	def __migrateCoupons(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId
		Split=MultiKeyId.split(',')
		print "Coupon %s" %Split[0]
		Coupon = Split[0]
		print "CustmerId %s" %Split[1]
		CustomerId= Split[1]
                if (Action=='Update' and IsProcessed==0):
                        Transaction=EMS7Connection.begin()
                        print "Incremental migration for Coupon"
                        self.__EMS6Cursor.execute("select * from Coupon where Id=%s", EMS6Id)
                        EMS6Coupons=self.__EMS6Cursor.fetchall()
                        IsProcessed = 1
                        for EMS6Coupon in EMS6Coupons:
                                today = date.today()
                                IsProcessed = 1
                                IsDeleted = 0
                                VERSION = 0
                                LastModifiedGMT = date.today()
                                LastModifiedByUserId = 1
                                EMS6CustomerId = EMS6Coupon[0]
                                print " EMS6 Customer Id is %s" %EMS6CustomerId
                                Coupon = EMS6Coupon[1]
                                print " EMS6 Coupon Id is %s" %Coupon
                                PercentDiscount = EMS6Coupon[2]
                                print " EMS6 Percent Discount %s " %PercentDiscount
                                StartDate = EMS6Coupon[3]
                                print " EMS6 Start Date %s" %StartDate
                                EndDate = EMS6Coupon[4]
                                print " EMS6 End Date %s " %EndDate
                                NumUses = EMS6Coupon[5]
                                print "NumUses %s" %NumUses
                                RegionId = EMS6Coupon[6]
                                print " EMS6 Region id is %s " %RegionId
                                StallRange = EMS6Coupon[7]
                                print " Stall Range %s" %StallRange
                                Description = EMS6Coupon[8]
                                print " Description %s" %Description
                                IsPndEnabled = EMS6Coupon[9]
                                print " IsPndEnabled %s" %IsPndEnabled
                                IsPbsEnabled = EMS6Coupon[10]
                                print "IsPbsEnabled %s" %IsPbsEnabled
                                EMS7CustomerId=getEMS7CustomerId(EMS6CustomerId)
                                EMS7LocationId=getEMS7LocationId(RegionId)
                                EMS7Cursor.execute("select EMS7CouponId from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s", (Coupon,CustomerId))
                                EMS7CouponId=EMS7Cursor.fetchone()
                                print "EMS7 Location Id is %s" %EMS7LocationId
                                EMS7CustomerId=getEMS7CustomerId(EMS6CustomerId)
                                print "EMS7 Customer Id is %s" %EMS7CustomerId
                                EMS7Cursor.execute("update Coupon set CustomerId=%s,LocationId=%s,Coupon=%s,PercentDiscount=%s,StartDateLocal=%s,EndDateLocal=%s,NumberOfUsesRemaining=%s,SpaceRange=%s,Description=%s,IsPndEnabled=%s,IsPbsEnabled=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where CustomerId=%s and Coupon=%s",(EMS7CustomerId,EMS7LocationId,Coupon,PercentDiscount,StartDate,EndDate,NumUses,StallRange,Description,IsPndEnabled,IsPbsEnabled,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CustomerId,Coupon))

                # when a new row gets inserted in this table how do i uniquely identify a row in the source table as it has a primary key on two columns
                if (Action=='Insert'):
			print "nothing inside"	
                       # EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null and ")

                       # EMS7Cursor.execute("update Coupon set CustomerId=%s,LocationId=%s,Coupon=%s,PercentDiscount=%s,StartDateLocal=%s,EndDateLocal=%s,NumberOfUsesRemaining=%s,SpaceRange=%s,Description=%s,IsPndEnabled=%s,IsPbsEnabled=%s,IsDeleted=%s,VERSION=%s,LastModifiedGMT=%s,LastModifiedByUserId=%s where Id=%s" %(EMS7CustomerId,EMS7LocationId, Coupon, PercentDiscount, StartDate, EndDate, NumUses, StallRange, Description, IsPndEnabled, IsPbsEnabled,IsDeleted,VERSION,LastModifiedGMT,LastModifiedByUserId,EMS7CouponId))
                        #EMS6Cursor.execute("update MigrationQueue set IsProcessed=%s where EMS6Id=%s" %(IsProcessed,Id))
                        #EMS6Connection.commit()
   		if (Action=='Delete'):
                        print "Delete Coupon Section is empty"

#                       EMS6Cursor.execute("" (EMS6CustomerId))
#                       EMS6Cursor.execute()

	def __migrateLotSetting(self,Action,EMS6Id,IsProcessed,MigrationId):
			print " Migrating LotSetting tables"
			print " -- Action %s" %Action
	                print " -- IsProcessed %s" %IsProcessed
        	        print " -- MigrationId %s" %MigrationId
        	       	print " -- EMS6Id %s" %EMS6Id
			if(Action=='Insert' and IsProcessed==0):
				print "Insert into LotSetting table"
				self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL and l.Id=%s",(EMS6Id))
				EMS6LotSettings=self.__EMS6Cursor.fetchall()
				for LotSetting in EMS6LotSettings:
					print "Inside the cursor to add lot setting to the incremental migration"
					self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting)
					print "why is the migration queue not getting updated"
					self.__updateMigrationQueue(MigrationId)
#               	self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s" %(MigrationId))
#	                self.__EMS6Connection.commit()


	def __updateMigrationQueue(self,MigrationId):
			self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s",(MigrationId))
			EMS6Connection.commit()


        def __getEMS7CustomerId(self,Id):
                self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
                row=self.__EMS7Cursor.fetchone()
                EMS7CustomerId = row[0]
                return EMS7CustomerId

        def __getEMS7LocationId(self,EMS6RegionId):
                LocationId = None
                self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(EMS6RegionId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        LocationId = row[0]
                        return LocationId

        def __getEMS7PaystationId(self,EMS6PaystationId):
                PaystationId=None
                self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        PaystationId =row[0]
                        return PaystationId

	def IncrementalMigration(self):
		self.__EMS6Cursor.execute("select * FROM MigrationQueue")
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		input = raw_input("Start Migration  ........................... y/n ?   :   ")

		if (input=='y') :
			for migration in MigrationQueue:
				MigrationId = migration[0]
				MultiKeyId = migration[1]
				EMS6Id = migration[2]
				TableName = migration[3]
				Action = migration[4]
				IsProcessed = migration[5]
				Date = migration[6]
				''' Begin Transaction '''
				if (TableName=='Customer'):
					self.__migrateCustomers(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Coupon'):
					self.__migrateCoupons(Action,IsProcessed,MigrationId,MultiKeyId)
				if (TableName=='Region'):
					self.__migrateLocations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Paystation'):
					self.__migratePaystations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='LotSetting'):
					self.__migrateLotSetting(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='CustomerProperties'):
					self.__migrateCustomerProperties(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='Customer_Service_Relation'):
					self.__migrateCustomerSubscription(Action,IsProcessed,MigrationId,MultiKeyId)
try:

	EMS6Connection = mdb.connect('127.0.0.1', 'vijay', 'test', 'ems_db');
	EMS6ConnectionCerberus = mdb.connect('127.0.0.1', 'vijay', 'test','cerberus_db');
	EMS7Connection = mdb.connect('172.16.1.123','vijay','test','EMS7')

	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), 1)
	startMigration.IncrementalMigration()


except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
    if EMS6Connection:
        EMS6Connection.close()
    if EMS7Connection:
        EMS7Connection.close()
