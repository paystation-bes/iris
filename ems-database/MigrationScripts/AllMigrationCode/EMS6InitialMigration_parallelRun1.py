##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6InitialMigration
##	Purpose		: This class instruments the data extraction from EMS6 database and data Transformation/Loading into EMS7 Database. The procedures in the class are called by the Main controller class, "migration.py". 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

from datetime import date
class EMS6InitialMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccess, verbose):
		self.__verbose = verbose
		print " Verbose Mode Set to %s" %self.__verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def migrateCustomers(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customers --> y/n    :   ") 
			if (input=='y') :
				for curCustomer in self.__EMS6DataAccess.getCustomers():
					self.__EMS7DataAccess.addCustomer(curCustomer, Action)
		else:
			for curCustomer in self.__EMS6DataAccess.getCustomers():
				self.__EMS7DataAccess.addCustomer(curCustomer, Action)

	def migrateCustomerProperties(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import CustomerProperties --> y/n     :    ")
			if (input=='y'):
				for curCustomerProperty in self.__EMS6DataAccess.getEMS6CustomerProperties():
					self.__EMS7DataAccess.addCustomerPropertyToEMS7(curCustomerProperty,Action)
		else:
			for curCustomerProperty in self.__EMS6DataAccess.getEMS6CustomerProperties():
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(curCustomerProperty,Action)


	def migrateQueryStallsCustomerProperty(self):
		if(self.__verbose):
			input = raw_input("Import the Query Stalls by into Customer Property  --> y/n     :")
			if (input=='y'):
				for CustomerId in self.__EMS6DataAccess.getDistinctCustomerFromPaystation():
					for curQueryStallsBy in self.__EMS6DataAccess.getEMS6QueryStallsBy(CustomerId):
						self.__EMS7DataAccess.addQueryStallsBy(curQueryStallsBy,CustomerId)
		else:
			for CustomerId in self.__EMS6DataAccess.getDistinctCustomerFromPaystation():
				for curQueryStallsBy in self.__EMS6DataAccess.getEMS6QueryStallsBy(CustomerId):
					self.__EMS7DataAccess.addQueryStallsBy(curQueryStallsBy,CustomerId)
			
	def migrateLotSettings(self):
		if(self.__verbose):
			input = raw_input("Import LotSetting --> y/n      :   ")
			if(input=='y'):
				for curLotSetting in self.__EMS6DataAccess.getEMS6LotSettings():
					self.__EMS7DataAccess.addLotSettingToEMS7(curLotSetting)
		else:
			for curLotSetting in self.__EMS6DataAccess.getEMS6LotSettings():
				self.__EMS7DataAccess.addLotSettingToEMS7(curLotSetting)

	def migratePaystations(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Paystation --> y/n      :   ")
			if(input=='y'):
				for curPaystation in self.__EMS6DataAccess.getEMS6Paystation():
					self.__EMS7DataAccess.addPaystationToEMS7(curPaystation,Action)
		else:
			for curPaystation in self.__EMS6DataAccess.getEMS6Paystation():
				self.__EMS7DataAccess.addPaystationToEMS7(curPaystation,Action)

	def migrateLocations(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Location --> y/n      :   ")
			if(input=='y'):
				for curLocation in self.__EMS6DataAccess.getEMS6Locations():
					self.__EMS7DataAccess.addLocationToEMS7(curLocation, Action)
		else:
			for curLocation in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccess.addLocationToEMS7(curLocation, Action)

	def migrateCoupons(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Coupons --> y/n     :     ")
			if(input=='y'):
				for curCoupon in self.__EMS6DataAccess.getEMS6Coupons():
					self.__EMS7DataAccess.addCouponToEMS7(curCoupon, Action)
		else:
			for curCoupon in self.__EMS6DataAccess.getEMS6Coupons():
				self.__EMS7DataAccess.addCouponToEMS7(curCoupon, Action)

	def migrateParkingPermissionType(self):
		if(self.__verbose):
			input = raw_input("Importing EMSParkingPermissionType --> y/n  :")
			if(input=='y'):
				for curParkingPermissionType in self.__EMS6DataAccess.getEMS6ParkingPermissionType():
					self.__EMS7DataAccess.addParkingPermissionTypeToEMS7(curParkingPermissionType)
		else:
			for curParkingPermissionType in self.__EMS6DataAccess.getEMS6ParkingPermissionType():
				self.__EMS7DataAccess.addParkingPermissionTypeToEMS7(curParkingPermissionType)
		
	def migrateParkingPermission(self):
		if(self.__verbose):
			input = raw_input("Import Parking Permission --> y/n     :")
			if(input=='y'):
				for curParkingPermission in self.__EMS6DataAccess.getParkingPermission():
					self.__EMS7DataAccess.addParkingPermissionToEMS7(curParkingPermission)
		else:
			for curParkingPermission in self.__EMS6DataAccess.getParkingPermission():
				self.__EMS7DataAccess.addParkingPermissionToEMS7(curParkingPermission)

	def migrateParkingPermissionDayOfWeek(self):		
		if(self.__verbose):	
			input = raw_input("Import EMSParkingParmissionDayOfweek --> y/n     :")	
			if(input=='y'):
				for curParkingPermissionDayOfWeek in self.__EMS6DataAccess.getParkingPermissionDayOfWeek():
					self.__EMS7DataAccess.addParkingPermissionDayOfWeek(curParkingPermissionDayOfWeek)
		else:
			for curParkingPermissionDayOfWeek in self.__EMS6DataAccess.getParkingPermissionDayOfWeek():
				self.__EMS7DataAccess.addParkingPermissionDayOfWeek(curParkingPermissionDayOfWeek)

	def migrateUnifiedRate(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Unified Rate --> y/n ")
			if(input=='y'):
				for curRates in self.__EMS6DataAccess.getEMS6Rates():
					self.__EMS7DataAccess.addRatesToEMS7(curRates, Action)
		else:
			for curRates in self.__EMS6DataAccess.getEMS6Rates():
				self.__EMS7DataAccess.addRatesToEMS7(curRates, Action)

	def migrateEMSRateIntoUnifiedRate(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import EMS Rates into UnifiedRate table --> y/n ")
			if(input=='y'):
				for curEMSRate in self.__EMS6DataAccess.getEMS6EMSRates():
					self.__EMS7DataAccess.addEMSRatesToEMS7(curEMSRate, Action)
		else:
			for curEMSRate in self.__EMS6DataAccess.getEMS6EMSRates():
				self.__EMS7DataAccess.addEMSRatesToEMS7(curEMSRate, Action)

#Should import Unified Rate first before migrating Extensible Rate
	def migrateExtensibleRateDayOfWeek(self):
		if(self.__verbose):
			input = raw_input("Import Exensible Rate Day Of Week --> y/n     :")
			if(input=='y'):
				for curExtensibleRateDOW in self.__EMS6DataAccess.getEMS6ExtensibleRatesDayOfWeek():
					self.__EMS7DataAccess.addExtensibleRateDayOfWeekToEMS7(curExtensibleRateDOW)
		else:
			for curExtensibleRateDOW in self.__EMS6DataAccess.getEMS6ExtensibleRatesDayOfWeek():
				self.__EMS7DataAccess.addExtensibleRateDayOfWeekToEMS7(curExtensibleRateDOW)

	def migrateRatesToEMS7ExtensibleRate(self):
		if(self.__verbose):
			input = raw_input("Import EMSRate into EMS7 Extensible Rate --> y/n     :")
			if(input=='y'):
				for curExtensibleRate in self.__EMS6DataAccess.getEMS6ExtensibleRate():
					self.__EMS7DataAccess.addExtensibleRatesToEMS7(curExtensibleRate)
		else:
			for curExtensibleRate in self.__EMS6DataAccess.getEMS6ExtensibleRate():
				self.__EMS7DataAccess.addExtensibleRatesToEMS7(curExtensibleRate)

	def migrateRestAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import RESTAccount --> y/n     :     ")
			if(input=='y'):
				for curRestAccount in self.__EMS6DataAccess.getEMS6RestAccount():
					self.__EMS7DataAccess.addRestAccountToEMS7(curRestAccount, Action)
		else:
			for curRestAccount in self.__EMS6DataAccess.getEMS6RestAccount():
				self.__EMS7DataAccess.addRestAccountToEMS7(curRestAccount, Action)

	def migrateLicencePlate(self):
		if(self.__verbose):
			input = raw_input("import Licence Plate --> y/n    : ")
			if(input=='y'):
				for distinctPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curLicencePlate in self.__EMS6DataAccess.getEMS6LicencePlate(distinctPaystationId):
						self.__EMS7DataAccess.addLicencePlateToEMS7(curLicencePlate)
		else:
			for distinctPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curLicencePlate in self.__EMS6DataAccess.getEMS6LicencePlate(distinctPaystationId):
					self.__EMS7DataAccess.addLicencePlateToEMS7(curLicencePlate)

	def migrateMobileNumber(self):
		if(self.__verbose):
			input = raw_input("import MobileNumber --> y/n")
			if(input=='y'):
				for curSMSAlertMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSAlert():
					self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSAlertMobileNumber)
				for curExtensiblePermitMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromEMSExtensiblePermit():
					self.__EMS7DataAccess.addMobileNumberToEMS7(curExtensiblePermitMobileNumber)
				for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSTransactionLog():
					self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSTransactionLog)
				for curMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumber():
					self.__EMS7DataAccess.addMobileNumbertoEMS7(curMobileNumber)
		else:
			for curSMSAlertMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSAlert():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSAlertMobileNumber)
			for curExtensiblePermitMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromEMSExtensiblePermit():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curExtensiblePermitMobileNumber)
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSTransactionLog():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSTransactionLog)
			for curMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumber():
				self.__EMS7DataAccess.addMobileNumbertoEMS7(curMobileNumber)

	def migrateExtensiblePermit(self):
		if(self.__verbose):
			input = raw_input("import Extensible Permit --> y/n        :")
			if(input=='y'):
				for curExtensiblePermit in self.__EMS6DataAccess.getEMS6ExtensiblePermit():
					self.__EMS7DataAccess.addExtensiblePermitToEMS7(curExtensiblePermit)
		else:
			for curExtensiblePermit in self.__EMS6DataAccess.getEMS6ExtensiblePermit():
				self.__EMS7DataAccess.addExtensiblePermitToEMS7(curExtensiblePermit)

	def migrateSMSTransactionLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import SMSTransactionLog --> y/n   : ")
			if(input=='y'):
				for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6SMSTransactionLog():
					self.__EMS7DataAccess.addSMSTransactionLogToEMS7(curSMSTransactionLog,Action)
		else:
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6SMSTransactionLog():
				self.__EMS7DataAccess.addSMSTransactionLogToEMS7(curSMSTransactionLog,Action)

	def migrateSMSAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import SMSAlert --> y/n ")
			if(input=='y'):
				for curSMSAlert in self.__EMS6DataAccess.getEMS6SMSAlert():
					self.__EMS7DataAccess.addSMSAlertToEMS7(curSMSAlert,Action)
		else:
			for curSMSAlert in self.__EMS6DataAccess.getEMS6SMSAlert():
				self.__EMS7DataAccess.addSMSAlertToEMS7(curSMSAlert,Action)

	def migrateSMSFailedResponse(self):
		if(self.__verbose):
			input = raw_input("import SMSFailedResponse --> y/n")
			if(input=='y'):
				for curSMSFailedReponse in self.__EMS6DataAccess.getEMS6SMSFailedResponse():
					self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(curSMSFailedReponse)
		else:
			for curSMSFailedReponse in self.__EMS6DataAccess.getEMS6SMSFailedResponse():
				self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(curSMSFailedReponse)
		
	def migrateModemSettings(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import ModemSetting --> y/n ")
			if (input=='y'):
				for curModemSetting in self.__EMS6DataAccess.getEMS6ModemSettings():
					self.__EMS7DataAccess.addModemSettingToEMS7(curModemSetting,Action)
		else:
			for curModemSetting in self.__EMS6DataAccess.getEMS6ModemSettings():
				self.__EMS7DataAccess.addModemSettingToEMS7(curModemSetting,Action)

	def migrateLotSettingContent(self):
		if(self.__verbose):
			input = raw_input("Import Lot Setting File Content --> y/n")
			if (input=='y'):
				for curLotSettingContent in self.__EMS6DataAccess.getEMS6LotSettingContent():
					self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(curLotSettingContent)
		else:
			for curLotSettingContent in self.__EMS6DataAccess.getEMS6LotSettingContent():
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(curLotSettingContent)

	def migrateCryptoKey(self):
		if(self.__verbose):
			input = raw_input("Import CryptoKey --> y/n      :")
			if(input=='y'):
				for curCryptoKey in self.__EMS6DataAccess.getEMS6CryptoKey():
					self.__EMS7DataAccess.addCryptoKeyToEMS7(curCryptoKey)
		else:
			for curCryptoKey in self.__EMS6DataAccess.getEMS6CryptoKey():
				self.__EMS7DataAccess.addCryptoKeyToEMS7(curCryptoKey)

	def migrateRestLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import RestLog table --> y/n  ")
			if(input=='y'):
				for curRestLog in self.__EMS6DataAccess.getEMS6RestLogs():
					self.__EMS7DataAccess.addRestLogToEMS7(curRestLog, Action)
		else:
			for curRestLog in self.__EMS6DataAccess.getEMS6RestLogs():
				self.__EMS7DataAccess.addRestLogToEMS7(curRestLog, Action)

	def migrateRestSessionToken(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import RestSessionToken --> y/n  ")
			if (input=='y') :
				for curRestSessionToken in self.__EMS6DataAccess.getEMS6RestSessionToken():
					self.__EMS7DataAccess.addRestSessionToken(curRestSessionToken, Action)
		else:
			for curRestSessionToken in self.__EMS6DataAccess.getEMS6RestSessionToken():
				self.__EMS7DataAccess.addRestSessionToken(curRestSessionToken, Action)

	def migrateRestLogTotalCall(self):
		if(self.__verbose):
			input = raw_input(" import RestLogTotalCall --> y/n  ")
			if (input=='y') :
				for curRestLogTotalCall in self.__EMS6DataAccess.getEMS6RestLogTotalCall():
					self.__EMS7DataAccess.addRestLogTotalCall(curRestLogTotalCall)
		else:
			for curRestLogTotalCall in self.__EMS6DataAccess.getEMS6RestLogTotalCall():
				self.__EMS7DataAccess.addRestLogTotalCall(curRestLogTotalCall)

#	def migrateCustomerWsCal(self):
#		if(self.__verbose):
#			input = raw_input(" import CustomerWsCal --> y/n  ")
#			if (input=='y') :
#				for curCustomerWsCal in self.__EMS6DataAccess.getEMS6CustomerWsCal():
#					self.__EMS7DataAccess.addCustomerWsCal(curCustomerWsCal)
#		else:
#			for curCustomerWsCal in self.__EMS6DataAccess.getEMS6CustomerWsCal():
#				self.__EMS7DataAccess.addCustomerWsCal(curCustomerWsCal)

	def migrateCustomerWsToken(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import CustomerWsToken --> y/n  ")
			if (input=='y') :
				for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
					self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken, Action)
		else:
			for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
				self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken, Action)

# I have left this section as is because this section is referencing the EMS6 and EMS7 Cursor within the same module. It looks like this table is not going to change much from initial migration.
	def migrateCustomerAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import Customer Alert --> y/n")		
			if(input=='y'):
				for curCustomerAlert in self.__EMS6DataAccess.getEMS6CustomerAlert():
					self.__EMS7DataAccess.addCustomerAlertToEMS7(curCustomerAlert, Action)
		else:
			for curCustomerAlert in self.__EMS6DataAccess.getEMS6CustomerAlert():
				self.__EMS7DataAccess.addCustomerAlertToEMS7(curCustomerAlert, Action)

	def migratePaystationAlertEmail(self):
		if(self.__verbose):
			input = raw_input("import Paystation Alert Email into CustomerAlertEmail --> y/n")
			if(input=='y'):
				for curPaystationAlertEmail in self.__EMS6DataAccess.getEMS6PaystationAlertEmail():
					self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(curPaystationAlertEmail)
		else:
			for curPaystationAlertEmail in self.__EMS6DataAccess.getEMS6PaystationAlertEmail():
				self.__EMS7DataAccess.addPaystationAlertEmailFromPaystation(curPaystationAlertEmail)

	def migrateCustomerSubscription(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customer Subscription --> y/n    :")
			if (input == 'y'):
				for curCustomerSubscription in self.__EMS6DataAccess.getEMS6CustomerSubscription():
					self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(curCustomerSubscription,Action)
		else:
			for curCustomerSubscription in self.__EMS6DataAccess.getEMS6CustomerSubscription():
				self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(curCustomerSubscription,Action)

	def migrateCustomerWebServiceCal(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import Customer Web Service Cal --> y/n    :")
			if (input == 'y'):
				for curCustomerWebServiceCal in self.__EMS6DataAccess.getEMS6CustomerWebServiceCal():
					self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(curCustomerWebServiceCal, Action)
		else:
			for curCustomerWebServiceCal in self.__EMS6DataAccess.getEMS6CustomerWebServiceCal():
				self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(curCustomerWebServiceCal, Action)

#	def migrateCustomer(self,CustomerWsToken):
#		if(self.__verbose):
#			input = raw_input("Import Customer WebService Token --> y/n    :")
#			if (input == 'y'):
#				for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
#					self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken)
#		else:
#			for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
#				self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken)

	def migrateRates(self):
		Action='Insert'
		if(self.__verbose):
			input  = raw_input("Import Rates --> y/n      :")
			if (input =='y'):
				for curRates in self.__EMS6DataAccess.getEMS6Rates():
					self.__EMS7DataAccess.addRatesToEMS7(curRates,Action)
		else:
			for curRates in self.__EMS6DataAccess.getEMS6Rates():
				self.__EMS7DataAccess.addRatesToEMS7(curRates,Action)

	def migrateReplenish(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input(" import Replenish --> y/n   :")
			if (input == 'y'):
				for curReplenish in self.__EMS6DataAccess.getEMS6Replenish():
					self.__EMS7DataAccess.addReplenishToEMS7(curReplenish, Action)
		else:
			for curReplenish in self.__EMS6DataAccess.getEMS6Replenish():
				self.__EMS7DataAccess.addReplenishToEMS7(curReplenish, Action)

	def migrateCardRetryTransaction(self):
		Action='Insert'
		if(self.__verbose):
			input= raw_input("Import Card Retry Transaction --> y/n   :")
			if (input == 'y'):
				for curCardRetryTransaction in self.__EMS6DataAccess.getEMS6CardRetryTransaction():
					self.__EMS7DataAccess.addCardRetryTransactionToEMS7(curCardRetryTransaction,Action)
		else:
			for curCardRetryTransaction in self.__EMS6DataAccess.getEMS6CardRetryTransaction():
				self.__EMS7DataAccess.addCardRetryTransactionToEMS7(curCardRetryTransaction, Action)


	def migrateCustomerCardType(self):
		if(self.__verbose):
			input  = raw_input(" import Customer Card Type --> y/n   :")
			if (input == 'y'):
				for curCustomerCardType in self.__EMS6DataAccess.getEMS6CardType():
					self.__EMS7DataAccess.addCustomerCardTypeToEMS7(curCustomerCardType)
		else:
			for curCustomerCardType in self.__EMS6DataAccess.getEMS6CardType():
				self.__EMS7DataAccess.addCustomerCardTypeToEMS7(curCustomerCardType)


	def migratePaystationGroup(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PaystationGroup --> y/n   :")
			if(input=='y'):
				for curPaystationGroup in self.__EMS6DataAccess.getEMS6PaystationGroup():
					self.__EMS7DataAccess.addPaystationGroupToEMS7(curPaystationGroup, Action)
		else:
			for curPaystationGroup in self.__EMS6DataAccess.getEMS6PaystationGroup():
				self.__EMS7DataAccess.addPaystationGroupToEMS7(curPaystationGroup, Action)

	def migratePOSRoute(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import POS Route  --> y/n :")
			if(input == 'y'):
				for curPOSRoute in self.__EMS6DataAccess.getEMS6Paystation_PaystationGroup():
					self.__EMS7DataAccess.addPOSRouteToEMS7(curPOSRoute,Action)
		else:
			for curPOSRoute in self.__EMS6DataAccess.getEMS6Paystation_PaystationGroup():
				self.__EMS7DataAccess.addPOSRouteToEMS7(curPOSRoute,Action)


	def migrateCollectionType(self):
		if(self.__verbose):
			input = raw_input("import CollectionType --> y/n   :")
			if(input=='y'):
				for curCollectionType in self.__EMS6DataAccess.getEMS6CollectionType():
					self.__EMS7DataAccess.addCollectionTypeToEMS7(curCollectionType)
		else:
			for curCollectionType in self.__EMS6DataAccess.getEMS6CollectionType():
				self.__EMS7DataAccess.addCollectionTypeToEMS7(curCollectionType)

	def migrateBatteryInfo(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("Import BatteryInfo --> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationForBatteryInfo():
					for curBatteryInfo in self.__EMS6DataAccess.getBatteryInfo(curPaystationId):
						self.__EMS7DataAccess.addBatteryInfoToEMS7(curBatteryInfo, Action)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationForBatteryInfo():
				for curBatteryInfo in self.__EMS6DataAccess.getBatteryInfo(curPaystationId):
					self.__EMS7DataAccess.addBatteryInfoToEMS7(curBatteryInfo, Action)

	def migrateValueCardData(self):
		if(self.__verbose):
			input = raw_input ("import Value Card Data--> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
						self.__EMS7DataAccess.addValueCardToEMS7(curVSCardData)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType(curPaystationId):
					self.__EMS7DataAccess.addValueCardToEMS7(curVSCardData)

	def migrateSmartCardData(self):
		if(self.__verbose):
			input = raw_input ("import Smart Card Data --> y/n    :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curSCardData in self.__EMS6DataAccess.getEMS6SmartCardType(curPaystationId):
						self.__EMS7DataAccess.addSmartCardToEMS7(curSCardData)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curSCardData in self.__EMS6DataAccess.getEMS6SmartCardType(curPaystationId):
					self.__EMS7DataAccess.addSmartCardToEMS7(curSCardData)

	def migratePurchase(self):
		self.__EMS7DataAccess.DisablePurchaseKeys()
		if(self.__verbose):
		     input = raw_input ("import Purchase --> y/n    :")
		     if (input=='y'):
			     for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				     purchases = self.__EMS6DataAccess.getEMS6Transaction(curPaystationId)
				     self.__EMS7DataAccess.batchAddPurchaseToEMS7(purchases)
		#                                       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
	#                                               self.__EMS7DataAccess.addPurchaseToEMS7(curTransaction)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				purchases = self.__EMS6DataAccess.getEMS6Transaction(curPaystationId)
				self.__EMS7DataAccess.batchAddPurchaseToEMS7(purchases)

		self.__EMS7DataAccess.EnablePurchaseKeys()
			#                                       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#                                               self.__EMS7DataAccess.addPurchaseToEMS7(curTransaction)

        #        for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
         #       for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
          #      self.__EMS7DataAccess.addPurchaseToEMS7(curTransaction)

# 	def migratePurchase(self):
#		if(self.__verbose):
#			input = raw_input ("import Purchase --> y/n    :")
#			if (input=='y'):
#				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
#					for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#						self.__EMS7DataAccess.batchAddPurchaseToEMS7(curTransaction)
#					for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#						self.__EMS7DataAccess.addPurchaseToEMS7(curTransaction)
#		else:
#			print" are you here"
#			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
#				print " curPaystationId %s" %curPaystationId
#				for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
#					print "curTransaction %s" %curTransaction[1]
#					print "curTransaction %s" %curTransaction[2]
#					print "curTransaction %s" %curTransaction[3]
#					self.__EMS7DataAccess.batchAddPurchaseToEMS7(curTransaction)

	def migrateCustomerEmail(self):
		if(self.__verbose):
			input = raw_input ("import CustomerAlertEmail --> y/n   :")
			if(input=='y'):
				for curCustomerEmail in self.__EMS6DataAccess.getCustomerEmail():
					self.__EMS7DataAccess.addCustomerEmail(curCustomerEmail)
		else:
			for curCustomerEmail in self.__EMS6DataAccess.getCustomerEmail():
				self.__EMS7DataAccess.addCustomerEmail(curCustomerEmail)

# Migrate getCustomerEmailPaystation into EMS7

	def migrateProcessorProperties(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import ProcessorProperties --> y/n    :")
			if(input=='y'):
				for curProcessorProperties in self.__EMS6DataAccess.getProcessorProperties():
					self.__EMS7DataAccess.addProcessorProperties(curProcessorProperties, Action)
		else:
			for curProcessorProperties in self.__EMS6DataAccess.getProcessorProperties():
				self.__EMS7DataAccess.addProcessorProperties(curProcessorProperties, Action)

	def migrateMerchantAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import MerchantAccount --> y/n    :")
			if(input=='y'):
				for curMerchantAccount in self.__EMS6DataAccess.getMerchantAccount():
					self.__EMS7DataAccess.addMerchantAccount(curMerchantAccount, Action)
		else:
			for curMerchantAccount in self.__EMS6DataAccess.getMerchantAccount():
				self.__EMS7DataAccess.addMerchantAccount(curMerchantAccount, Action)

	def migratePreAuth(self):
		if(self.__verbose):
			input = raw_input ("import PreAuth --> y/n     :")
			if(input=='y'):
				for curPreAuth in self.__EMS6DataAccess.getPreAuth():
					self.__EMS7DataAccess.addPreAuth(curPreAuth)
		else:
			for curPreAuth in self.__EMS6DataAccess.getPreAuth():
				self.__EMS7DataAccess.addPreAuth(curPreAuth)

	def migratePreAuthHolding(self):
		if(self.__verbose):
			input = raw_input ("import PreAuthHolding --> y/n    :")
			if(input=='y'):
				for curPreAuthHolding in self.__EMS6DataAccess.getPreAuthHolding():
					self.__EMS7DataAccess.addPreAuthHolding(curPreAuthHolding)
		else:
			for curPreAuthHolding in self.__EMS6DataAccess.getPreAuthHolding():
				self.__EMS7DataAccess.addPreAuthHolding(curPreAuthHolding)


	def migrateProcessorTransaction(self):
		if(self.__verbose):
			input = raw_input ("import ProcessorTransaction --> y/n   :")
			if(input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromProcessorTransaction():
					ProcessorTransactions = self.__EMS6DataAccess.getProcessorTransaction(curPaystationId)
					self.__EMS7DataAccess.batchAddProcessorTransaction(ProcessorTransactions)
					#for curProcessorTransaction in self.__EMS6DataAccess.getProcessorTransaction(curPaystationId):
					#	self.__EMS7DataAccess.addProcessorTransaction(curProcessorTransaction)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromProcessorTransaction():
				ProcessorTransactions = self.__EMS6DataAccess.getProcessorTransaction(curPaystationId)
				self.__EMS7DataAccess.batchAddProcessorTransaction(ProcessorTransactions)
				#for curProcessorTransaction in self.__EMS6DataAccess.getProcessorTransaction(curPaystationId):
				#	self.__EMS7DataAccess.addProcessorTransaction(curProcessorTransaction)
		
	def migratePermit(self):
		if(self.__verbose):
			input = raw_input ("import Permit --> y/n   :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					Transactions = self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId)
					self.__EMS7DataAccess.batchAddPermitToEMS7(Transactions)
					#for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId):
					#	self.__EMS7DataAccess.addPermitToEMS7(curTransaction)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				Transactions = self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId)
				self.__EMS7DataAccess.batchAddPermitToEMS7(Transactions)
				#for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId):
				#	self.__EMS7DataAccess.addPermitToEMS7(curTransaction)
	
	def migratePaymentCard(self):
		if(self.__verbose):
			input = raw_input ("import Payment -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					Transactions = self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId)
					self.__EMS7DataAccess.batchAddPaymentCardToEMS7(Transactions)
				#	for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId):
				#		self.__EMS7DataAccess.addPaymentCardToEMS7(curTransaction)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				Transactions = self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId)
				self.__EMS7DataAccess.batchAddPaymentCardToEMS7(Transactions)
#				for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId):
#					self.__EMS7DataAccess.addPaymentCardToEMS7(curTransaction)

	def migrateUserAccount(self):	
		Action='Insert'	
		if(self.__verbose):
			input = raw_input ("import UserAccount --> y/n  :")
			if (input=='y'):
				for curUserAccount in self.__EMS6DataAccess.getEMS6UserAccount():
					self.__EMS7DataAccess.addUserAccountToEMS7(curUserAccount, Action)
		else:
			for curUserAccount in self.__EMS6DataAccess.getEMS6UserAccount():
				self.__EMS7DataAccess.addUserAccountToEMS7(curUserAccount, Action)

	def migrateEventLogNewIntoPOSAlert(self):
		if(self.__verbose):
			input = raw_input ("import EventNew Log into POS Alert --> y/n :")
			if (input == 'y'):
				for curPaystationId in  self.__EMS6DataAccess.getDistinctPaystationIdForEventLogNew():
					for curPOSAlert in self.__EMS6DataAccess.getEventLogNew(curPaystationId):
						self.__EMS7DataAccess.addPOSAlertToEMS7(curPOSAlert)
		else:
			for curPaystationId in  self.__EMS6DataAccess.getDistinctPaystationIdForEventLogNew():
				for curPOSAlert in self.__EMS6DataAccess.getEventLogNew(curPaystationId):
					self.__EMS7DataAccess.addPOSAlertToEMS7(curPOSAlert)
		
	def migrateTrialCustomer(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Trial Customer  --> y/n :")
			if(input == 'y'):
				for curtrialcustomer in self.__EMS6DataAccess.getEMS6CerberusTrialCustomer():
					self.__EMS7DataAccess.addTrialCustomerToEMS7(curtrialcustomer, Action)
		else:
			for curtrialcustomer in self.__EMS6DataAccess.getEMS6CerberusTrialCustomer():
					self.__EMS7DataAccess.addTrialCustomerToEMS7(curtrialcustomer, Action)

	def migrateRegionPaystationLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Region Paystation Log --> y/n :")
			if(input == 'y'):
				for curRegionPaystationLog in self.__EMS6DataAccess.getEMS6RegionPaystationLog():
					self.__EMS7DataAccess.addLocationPOSLogToEMS7(curRegionPaystationLog, Action)
		else:
			for curRegionPaystationLog in self.__EMS6DataAccess.getEMS6RegionPaystationLog():
				self.__EMS7DataAccess.addLocationPOSLogToEMS7(curRegionPaystationLog, Action)

	def migrateCollectionLock(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Collection Lock --> y/n :")
			if(input == 'y'):
				for curCollectionLock in self.__EMS6DataAccess.getEMS6CollectionLock():
					self.__EMS7DataAccess.addCollectionLockToEMS7(curCollectionLock, Action)
		else:
			for curCollectionLock in self.__EMS6DataAccess.getEMS6CollectionLock():
				self.__EMS7DataAccess.addCollectionLockToEMS7(curCollectionLock, Action)

	def migratePaystationBalance(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PaystationBalance --> y/n  :")
			if(input == 'y'):
				for curPaystationBalance in self.__EMS6DataAccess.getEMS6PaystationBalance():
					self.__EMS7DataAccess.addPOSBalanceToEMS7(curPaystationBalance, Action)
		else:
			for curPaystationBalance in self.__EMS6DataAccess.getEMS6PaystationBalance():
				self.__EMS7DataAccess.addPOSBalanceToEMS7(curPaystationBalance, Action)

	
	def migrateClusterMember(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import ClusterMember  --> y/n :")
			if(input == 'y'):
				for curClusterMember in self.__EMS6DataAccess.getEMS6ClusterMember():
					self.__EMS7DataAccess.addClusterToEMS7(curClusterMember, Action)
		else:
			for curClusterMember in self.__EMS6DataAccess.getEMS6ClusterMember():
				self.__EMS7DataAccess.addClusterToEMS7(curClusterMember, Action)

	def migratePOSHeartBeat(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import  Paystation Heart Beat --> y/n :")
			if(input == 'y'):
				for curPOSHeartBeat in self.__EMS6DataAccess.getEMS6PaystationHeartBeat():
					self.__EMS7DataAccess.addPOSHeartBeatToEMS7(curPOSHeartBeat, Action)
		else:
			for curPOSHeartBeat in self.__EMS6DataAccess.getEMS6PaystationHeartBeat():
				self.__EMS7DataAccess.addPOSHeartBeatToEMS7(curPOSHeartBeat, Action)

	def migratePOSServiceState(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import  Service State --> y/n :")
			if(input == 'y'):
				for curPOSServiceState in self.__EMS6DataAccess.getEMS6ServiceState():
					self.__EMS7DataAccess.addPOSServiceStateToEMS7(curPOSServiceState, Action)
		else:
			for curPOSServiceState in self.__EMS6DataAccess.getEMS6ServiceState():
				self.__EMS7DataAccess.addPOSServiceStateToEMS7(curPOSServiceState, Action)

	def migrateActivePOSAlert(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import ActivePOSAlert  --> y/n :")
			if(input == 'y'):
				for curActivePOSAlert in self.__EMS6DataAccess.getEMS6PaystationAlert():
					self.__EMS7DataAccess.addActivePOSAlertToEMS7(curActivePOSAlert, Action)
				self.__EMS7DataAccess.addActivePOSAlertForPaytatationAlert()
				self.__EMS7DataAccess.addActivePOSAlertForUserDefinedAlert()
		else:
			for curActivePOSAlert in self.__EMS6DataAccess.getEMS6PaystationAlert():
				self.__EMS7DataAccess.addActivePOSAlertToEMS7(curActivePOSAlert, Action)
			self.__EMS7DataAccess.addActivePOSAlertForPaytatationAlert()
			self.__EMS7DataAccess.addActivePOSAlertForUserDefinedAlert()

	def migratePOSDateBilling(self):
		if(self.__verbose):
			input = raw_input ("import POSDate Billing -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curBillingReport in self.__EMS6DataAccess.getEMS6BillingReport(curPaystationId):
						self.__EMS7DataAccess.addPOSDateBillingReportToEMS7(curBillingReport)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curBillingReport in self.__EMS6DataAccess.getEMS6BillingReport(curPaystationId):
					self.__EMS7DataAccess.addPOSDateBillingReportToEMS7(curBillingReport)


	def migratePOSDateAuditAccess(self):
		if(self.__verbose):
			input = raw_input ("import POSDate AuditAccess -->  y/n :")
			if (input=='y'):
				for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
					for curAuditAccess in self.__EMS6DataAccess.getEMS6AuditAccess(curPaystationId):
						self.__EMS7DataAccess.addPOSDateAuditAccessToEMS7(curAuditAccess)
		else:
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curAuditAccess in self.__EMS6DataAccess.getEMS6AuditAccess(curPaystationId):
					self.__EMS7DataAccess.addPOSDateAuditAccessToEMS7(curAuditAccess)

	def migratePOSDatePaystation(self):
		if(self.__verbose):
			input = raw_input ("import Paystation Properties into POSDate --> y/n :")
			if (input=='y'):
				for curPaystation in self.__EMS6DataAccess.getEMS6PaystationForPOSDate():
					self.__EMS7DataAccess.addPOSDatePaystationPropertiesToEMS7(curPaystation)
		else:
			for curPaystation in self.__EMS6DataAccess.getEMS6PaystationForPOSDate():
				self.__EMS7DataAccess.addPOSDatePaystationPropertiesToEMS7(curPaystation)
	

	def migrateBadValueCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Value Card --> y/n :")
			if (input=='y'):
				for curBadValueCard in self.__EMS6DataAccess.getEMS6BadValueCard():
					self.__EMS7DataAccess.addBadValueCardToEMS7(curBadValueCard, Action)
		else:
			for curBadValueCard in self.__EMS6DataAccess.getEMS6BadValueCard():
				self.__EMS7DataAccess.addBadValueCardToEMS7(curBadValueCard, Action)

	def migrateBadCreditCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Credit Card --> y/n :")
			if (input=='y'):
				for curBadCreditCard in self.__EMS6DataAccess.getEMS6BadCreditCard():
					self.__EMS7DataAccess.addBadCreditCardToEMS7(curBadCreditCard, Action)
		else:
			for curBadCreditCard in self.__EMS6DataAccess.getEMS6BadCreditCard():
				self.__EMS7DataAccess.addBadCreditCardToEMS7(curBadCreditCard, Action)

	def migrateBadSmartCard(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("import Bad Smart Card --> y/n :")
			if (input=='y'):
				for curBadSmartCard in self.__EMS6DataAccess.getEMS6BadSmartCard():
					self.__EMS7DataAccess.addBadSmartCardToEMS7(curBadSmartCard, Action)
		else:
			for curBadSmartCard in self.__EMS6DataAccess.getEMS6BadSmartCard():
				self.__EMS7DataAccess.addBadSmartCardToEMS7(curBadSmartCard, Action)

	def migrateParentRegionIdToLocation(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Add Parent Region to Location  --> y/n :")
			if (input=='y'):
				for curParentRegion in self.__EMS6DataAccess.getEMS6Locations():
					self.__EMS7DataAccess.addParentRegionIdToLocation(curParentRegion, Action)
		else:
			for curParentRegion in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccess.addParentRegionIdToLocation(curParentRegion, Action)

	def migrateCCFailLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import CCFailLog  --> y/n :")
			if (input=='y'):
				for curCCFailLog in self.__EMS6DataAccess.getEMS6CFailLog():
					self.__EMS7DataAccess.addCCFailLogToEMS7(curCCFailLog, Action)
		else:
			for curCCFailLog in self.__EMS6DataAccess.getEMS6CFailLog():
				self.__EMS7DataAccess.addCCFailLogToEMS7(curCCFailLog, Action)

	def migrateServiceAgreement(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ServiceAgreement  --> y/n :")
			if (input=='y'):
				for curServiceAgreement in self.__EMS6DataAccess.getEMS6ServiceAgreement():
					self.__EMS7DataAccess.addServiceAgreementToEMS7(curServiceAgreement,Action)
		else:
			for curServiceAgreement in self.__EMS6DataAccess.getEMS6ServiceAgreement():
				self.__EMS7DataAccess.addServiceAgreementToEMS7(curServiceAgreement,Action)

	def migrateServiceAgreedCustomer(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Service Agreed Customer --> y/n")
			if(input=='y'):
				for curServiceAgreedCustomer in self.__EMS6DataAccess.getEMS6ServiceAgreedCustomer():
					self.__EMS7DataAccess.addCustomerAgreementToEMS7(curServiceAgreedCustomer, Action)
		else:
			for curServiceAgreedCustomer in self.__EMS6DataAccess.getEMS6ServiceAgreedCustomer():
				self.__EMS7DataAccess.addCustomerAgreementToEMS7(curServiceAgreedCustomer, Action)


	def migrateRawSensorData(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Raw Sensor Data --> y/n")
			if(input=='y'):
				for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
					self.__EMS7DataAccess.addRawSensorDataToEMS7(curRawSensorData, Action)
		else:
			for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
				self.__EMS7DataAccess.addRawSensorDataToEMS7(curRawSensorData, Action)
	

	def migrateRawSensorDataArchive(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input (" Import Raw Sensor Data Archive --> y/n")
			if(input=='y'):
				for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
					self.__EMS7DataAccess.addRawSensorDataArchiveToEMS7(curRawSensorData, Action)
		else:
			for curRawSensorData in self.__EMS6DataAccess.getEMS6RawSensorData():
				self.__EMS7DataAccess.addRawSensorDataArchiveToEMS7(curRawSensorData, Action)

	def migrateReversal(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Reversal --> y/n")
			if(input=='y'):
				for curReversal in self.__EMS6DataAccess.getEMS6Reversal():
					self.__EMS7DataAccess.addReversalToEMS7(curReversal, Action)
		else:
			for curReversal in self.__EMS6DataAccess.getEMS6Reversal():
				self.__EMS7DataAccess.addReversalToEMS7(curReversal, Action)

	def migrateReversalArchive(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Reversal Archive --> y/n")
			if(input=='y'):
				for curReversalArchive in self.__EMS6DataAccess.getEMS6ReversalArchive():
					self.__EMS7DataAccess.addReversalArchiveToEMS7(curReversalArchive, Action)

# This table is not needed as it is being used by IntellaPay
	def migrateSCRetry(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import SCRetry --> y/n")
			if(input=='y'):
				for curSCRetry in self.__EMS6DataAccess.getEMS6SCRetry():
					self.__EMS7DataAccess.addSCRetryToEMS7(curSCRetry, Action)
		else:
			for curSCRetry in self.__EMS6DataAccess.getEMS6SCRetry():
				self.__EMS7DataAccess.addSCRetryToEMS7(curSCRetry, Action)

	def migrateThirdPartyServiceAccount(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdAprtyServiceAccount --> y/n")
			if(input=='y'):
				for curThirdPartyServiceAccount in self.__EMS6DataAccess.getEMS6ThirdPartyServiceAccount():
					self.__EMS7DataAccess.addThirdAprtyServiceAccountToEMS7(curThirdPartyServiceAccount, Action)
		else:
			for curThirdPartyServiceAccount in self.__EMS6DataAccess.getEMS6ThirdPartyServiceAccount():
				self.__EMS7DataAccess.addThirdAprtyServiceAccountToEMS7(curThirdPartyServiceAccount, Action)
	
	def migrateTransaction2Push(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import Transaction2Push --> y/n")
			if(input=='y'):
				for curTransaction2Push in self.__EMS6DataAccess.getEMS6Transaction2Push():
					self.__EMS7DataAccess.addTransaction2PushToEMS7(curTransaction2Push, Action)
		else:
			for curTransaction2Push in self.__EMS6DataAccess.getEMS6Transaction2Push():
				self.__EMS7DataAccess.addTransaction2PushToEMS7(curTransaction2Push, Action)

	def migrateTransactionPush(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import TransactionPush --> y/n")
			if(input=='y'):
				for curTransactionPush in self.__EMS6DataAccess.getEMS6TransactionPush():
					self.__EMS7DataAccess.addTransactionPushToEMS7(curTransactionPush, Action)
		else:
			for curTransactionPush in self.__EMS6DataAccess.getEMS6TransactionPush():
				self.__EMS7DataAccess.addTransactionPushToEMS7(curTransactionPush, Action)

	def migrateThirdPartyPOSSequence(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyPOSSequence --> y/n")
			if(input=='y'):
				for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
					self.__EMS7DataAccess.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)
		else:
			for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
				self.__EMS7DataAccess.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)

	def migrateThirdPartyPushLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyPushLog --> y/n")
			if(input=='y'):
				for curThirdPartyPushLog in self.__EMS6DataAccess.getEMS6ThirdPartyPushLog():
					self.__EMS7DataAccess.addThirdPartyPushLogToEMS7(curThirdPartyPushLog, Action)

	def migrateThirdPartyServicePaystation(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyServicePaystation --> y/n")
			if(input=='y'):
				for curThirdPartyPOSSequence in self.__EMS6DataAccess.getEMS6ThirdPartyPOSSequence():
					self.__EMS7DataAccess.addThirdPartyPOSSequenceToEMS7(curThirdPartyPOSSequence, Action)

	def migrateThirdPartyServiceStallSequence(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import ThirdPartyServiceStallSequence --> y/n")
			if(input=='y'):
				for curThirdPartyServiceStallSequence in self.__EMS6DataAccess.getEMS6ThirdPartyServiceStallSequence():
					self.__EMS7DataAccess.addThirdPartyServiceStallSequenceToEMS7(curThirdPartyServiceStallSequence, Action)

	def migratePOSCollection(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import POSCollection --> y/n :")
			if (input=='y'):
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationFromAudit():
					for curPOSCollection in self.__EMS6DataAccess.getEMS6Audit(PaystationId):
						self.__EMS7DataAccess.addPOSCollectionToEMS7(curPOSCollection, Action)
		else:
			for PaystationId in self.__EMS6DataAccess.getDistinctPaystationFromAudit():
				for curPOSCollection in self.__EMS6DataAccess.getEMS6Audit(PaystationId):
					self.__EMS7DataAccess.addPOSCollectionToEMS7(curPOSCollection, Action)
	def migratePOSStatus(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import POSStatus --> y/n :")
			if (input=='y'):
				self.__EMS7DataAccess.addPOSStatusToEMS7()
		else:
			self.__EMS7DataAccess.addPOSStatusToEMS7()

	def migratePurchaseCollection(self):
		if(self.__verbose):
			input = raw_input("import PurchaseCollection --> y/n :")
			if(input=='y'):
				self.__EMS7DataAccess.addPurchaseCollection()
		else:
			self.__EMS7DataAccess.addPurchaseCollection()

	def migratePurchaseTax(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input("import PurchaseTax --> y/n :")
			if(input=='y'):
				for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdForTaxes():
					for curTaxes in self.__EMS6DataAccess.getTaxes(PaystationId):	
						self.__EMS7DataAccess.addPurchaseTax(curTaxes, Action)
		else:
			for PaystationId in self.__EMS6DataAccess.getDistinctPaystationIdForTaxes():
				for curTaxes in self.__EMS6DataAccess.getTaxes(PaystationId):	
					self.__EMS7DataAccess.addPurchaseTax(curTaxes, Action)

	def migrateWebServiceCallLog(self):
		Action='Insert'
		if(self.__verbose):
			input = raw_input ("Import WebServiceCallLog --> y/n")
			if(input=='y'):
				for curTransaction2Push in self.__EMS6DataAccess.getEMS6WsCallLog():
					self.__EMS7DataAccess.addWebServiceCallLog(curTransaction2Push, Action)
		else:
			for curTransaction2Push in self.__EMS6DataAccess.getEMS6WsCallLog():
				self.__EMS7DataAccess.addWebServiceCallLog(curTransaction2Push, Action)

