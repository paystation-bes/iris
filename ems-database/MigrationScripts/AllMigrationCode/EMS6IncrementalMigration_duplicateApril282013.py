#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6IncrementalMigration
##	Purpose		: This class incrementally migrates the data from EMS6 into EMS7. 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess

class EMS6IncrementalMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccess, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
		self.__merchantIdCache = {}

	def __migrateCustomers(self,Action,EMS6Id,IsProcessed,MigrationId):
                if (Action=='Update' and IsProcessed==0):
                        self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id from Customer where Id=%s", EMS6Id)
                        customer=self.__EMS6Cursor.fetchall()
			for cust in customer:
				self.__EMS7DataAccess.addCustomer(cust,Action)
				self.__updateMigrationQueue(MigrationId)		

                elif (Action=='Insert' and IsProcessed==0):
                        self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer where Id=%s", EMS6Id)
                        customer=self.__EMS6Cursor.fetchall()
                        for cust in customer:
				self.__EMS7DataAccess.addCustomer(cust,Action)
           	                self.__updateMigrationQueue(MigrationId)

                elif (Action=='Delete' and IsProcessed==0):
                        self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", EMS6Id)
                        EMS7CustomerId=self.__EMS7Cursor.fetchone()
			self.__EMS7Cursor.execute("delete from Customer where Id=%s",EMS7CustomerId)
			self.__EMS7Connection.commit()
	                self.__updateMigrationQueue(MigrationId)

	def __migrateCustomerProperties(self,Action,EMS6Id,IsProcessed,MigrationId):
		if (Action=='Update' and IsProcessed==0):
			CustomerProperties = self.__getCustomerProperties(EMS6Id)
			for custProperties in CustomerProperties:
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
				self.__updateMigrationQueue(MigrationId)

		if (Action=='Insert' and IsProcessed==0):
			CustomerProperties = self.__getCustomerProperties(EMS6Id)
			for custProperties in CustomerProperties:
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(custProperties,Action)
				self.__updateMigrationQueue(MigrationId)

		if (Action=='Delete' and IsProcessed==0):
			self.__EMS7DataAccess.deleteCustomerPropertyFromEMS7(EMS6Id, Action)	
			self.__updateMigrationQueue(MigrationId)

	def __getCustomerProperties(self,EMS6Id):
		CustomerProperty=None
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,TimeZone,SMSWarningPeriod from CustomerProperties where CustomerId=%s", EMS6Id)
		if (self.__EMS6Cursor.rowcount<>0):
	                CustomerProperty=self.__EMS6Cursor.fetchall()
			return CustomerProperty

	def __migrateCustomerSubscription(self,Action,IsProcessed,MigrationId,MultiKeyId):
		Split=MultiKeyId.split(',')
		SubscriptionTypeId=None
                CustomerId = Split[0]
                ServiceId= Split[1]
		if (ServiceId=="1"):
			SubscriptionTypeId=100
		if (ServiceId=="2"):
			SubscriptionTypeId=600
		if (ServiceId=="3"):
			SubscriptionTypeId=200
		if(ServiceId=="4"):
			SubscriptionTypeId=700
		if(ServiceId=="5"):
			SubscriptionTypeId=300
		if(ServiceId=="6"):
			SubscriptionTypeId=1000
		if(ServiceId=="7"):
			SubscriptionTypeId=1200
		if(ServiceId=="8"):
			SubscriptionTypeId=900
		if(Action=='Update' and IsProcessed==0):
			CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
			for custCSR in CustomerServiceRelation:
				self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
				self.__updateMigrationQueue(MigrationId)
		if(Action=='Insert' and IsProcessed==0):
			CustomerServiceRelation=self.__getCustomerServiceRelation(CustomerId,ServiceId)
			for custCSR in CustomerServiceRelation:
                                self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(custCSR, Action)
                                self.__updateMigrationQueue(MigrationId)
		if(Action=='Delete' and IsProcessed==0):
			self.__EMS6CursorCerberus.execute("select distinct(EmsCustomerId) from cerberus_db.Customer where Id=%s",(CustomerId))
			if(self.__EMS6CursorCerberus.rowcount<>0):
				EMS6CustomerId=self.__EMS6CursorCerberus.fetchone()
			EMS7CustomerId=self.__getEMS7CustomerId(EMS6CustomerId)
			self.__EMS7Cursor.execute("update CustomerSubscription set IsEnabled=0 where CustomerId=%s and SubscriptionTypeId=%s",(EMS7CustomerId,SubscriptionTypeId))
			self.__EMS7Connection.commit()
			self.__updateMigrationQueue(MigrationId)

	def __getCustomerServiceRelation(self,CustomerId,ServiceId):
		CustomerServiceRelation=None
		self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId) where c.Id=%s and r.ServiceId=%s",(CustomerId,ServiceId))
		if(self.__EMS6CursorCerberus.rowcount<>0):
			CustomerServiceRelation=self.__EMS6CursorCerberus.fetchall()
		return CustomerServiceRelation

	def __migratePaystations(self, Action, EMS6Id, IsProcessed, MigrationId):
                if (Action=='Update' and IsProcessed==0):
                        IsDeleted=0
                        LastModifiedGMT=date.today()
                        LastModifiedByUserId=1
			EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
			if(EMS6Paystations):
				for paystation in EMS6Paystations:
					self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Insert' and IsProcessed==0):
                        IsProcessed = 1
                        EMS6Paystations=self.__getEMS6PaystationId(EMS6Id)
			if(EMS6Paystations):
				for paystation in EMS6Paystations:
					self.__EMS7DataAccess.addPaystationToEMS7(paystation, Action)
				self.__updateMigrationQueue(MigrationId)

                if (Action=='Delete' and IsProcessed==0):
			if(EMS6Id):
				self.__EMS7Cursor.execute("set foreign_key_checks=0")
				self.__EMS7Cursor.execute("delete from Paystation where Id=(select distinct(EMS7PaystationId) from PaystationMapping where EMS6PaystationId=%s)",(EMS6Id))
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

	def __getEMS6PaystationId(self,EMS6Id):
		EMS6Rows = None
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId,RegionId,LotSettingId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId) where p.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rows = self.__EMS6Cursor.fetchall()
		return EMS6Rows

	def __migrateLocations(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId
		if (Action=='Update' and IsProcessed==0):
                        EMS6Regions=self.__getEMS6Region(EMS6Id)
                        IsProcessed = 1
                        for EMS6Region in EMS6Regions:
				self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
			EMS6Regions=self.__getEMS6Region(EMS6Id)
                        IsProcessed = 1
                        for EMS6Region in EMS6Regions:
				self.__EMS7DataAccess.addLocationToEMS7(EMS6Region, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                elif(Action=='Delete' and IsProcessed==0):
			self.__EMS7Cursor.execute("set foreign_key_checks=0")
                        self.__EMS7Cursor.execute("delete from Location where Id=(select distinct(EMS7LocationId) from RegionLocationMapping where EMS6RegionId=%s)",(EMS6Id))
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()

	def __getEMS6Region(self, EMS6Id):
		EMS6Region=None
		self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null and Region.Id=%s", EMS6Id)
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Region = self.__EMS6Cursor.fetchall()
		return EMS6Region	

	def __migrateLocationPOSLog(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId
		if (Action=='Update' and IsProcessed==0):
                        print "Incremental migration for LocationPOSLog"
                        EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
                        IsProcessed = 1
                        for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
				self.__EMS7DataAccess.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
			EMS6RegionPaystationLogs=self.__getEMS6RegionPaystationLog(EMS6Id)
                        IsProcessed = 1
                        for EMS6RegionPaystationLog in EMS6RegionPaystationLogs:
				self.__EMS7DataAccess.addLocationPOSLogToEMS7(EMS6RegionPaystationLog, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

                elif(Action=='Delete' and IsProcessed==0):
			self.__EMS7Cursor.execute("delete from LocationPOSLog where Id=(select distinct(EMS7LocationPOSLogId) from RegionPaystationLogMapping where EMS6RegionPaystationId=%s)",(EMS6Id))
			EMS7Connection.commit()
			self.__updateMigrationQueue(MigrationId)

	def __getEMS6RegionPaystationLog(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id, RegionId, PaystationId, CreationDate from RegionPaystationLog where Id=%s", EMS6Id)
		return self.__EMS6Cursor.fetchall()

	def __printLocation(self, Location):
		print " -- EMS6 RegionId %s" %Location[0]
		print " -- Version %s" %Location[1]
		print " -- AlarmStats %s" %Location[2]
		print " -- CustomerId %s" %Location[3]
		print " -- Name %s" %Location[4]
		print " -- Description %s" %Location[5]
		print " -- ParentRegion %s" %Location[6]
		print " -- IsDefault %s" %Location[7]
		print " -- EMS7CustomerId %s" %Location[8]
		print " -- EMS7LocationId %s" %Location[9]

	def __migrateCoupons(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId
		Split=MultiKeyId.split(',')
		print "Coupon %s" %Split[0]
		Coupon = Split[0]
		print "CustmerId %s" %Split[1]
		CustomerId= Split[1]
                if (Action=='Update' and IsProcessed==0):
                        EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
                        IsProcessed = 1
                        for EMS6Coupon in EMS6Coupons:
				self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
			EMS6Coupons=self.__getEMS6Coupon(CustomerId,Coupon)
                        IsProcessed = 1
			for EMS6Coupon in EMS6Coupons:
				self.__EMS7DataAccess.addCouponToEMS7(EMS6Coupon, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		if (Action=='Delete'):
			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
			EMS7Connection.commit()
			self.__updateMigrationQueue(MigrationId)

	def __getEMS6Coupon(self, CustomerId, CouponId):
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null and CustomerId=%s and Coupon.Id=%s",(CustomerId,CouponId))
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSetting(self,Action,EMS6Id,IsProcessed,MigrationId):
			print " Migrating LotSetting tables"
			print " -- Action %s" %Action
	                print " -- IsProcessed %s" %IsProcessed
        	        print " -- MigrationId %s" %MigrationId
        	       	print " -- EMS6Id %s" %EMS6Id
			if(Action=='Insert' and IsProcessed==0):
				print "Insert into LotSetting table"
				self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL and l.Id=%s",(EMS6Id))
				EMS6LotSettings=self.__EMS6Cursor.fetchall()
				for LotSetting in EMS6LotSettings:
					print "Inside the cursor to add lot setting to the incremental migration"
					self.__EMS7DataAccess.addLotSettingToEMS7(LotSetting)
					print "why is the migration queue not getting updated"
					self.__updateMigrationQueue(MigrationId)
#               	self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s" %(MigrationId))
#	                self.__EMS6Connection.commit()

	def __updateMigrationQueue(self,MigrationId):
			self.__EMS6Cursor.execute("update MigrationQueue set IsProcessed=1 where Id=%s",(MigrationId))
			EMS6Connection.commit()

        def __getEMS7CustomerId(self,Id):
                self.__EMS7Cursor.execute("select distinct(EMS7CustomerId) from CustomerMapping where EMS6CustomerId=%s", Id)
                row=self.__EMS7Cursor.fetchone()
                EMS7CustomerId = row[0]
                return EMS7CustomerId

        def __getEMS7LocationId(self,EMS6RegionId):
                LocationId = None
                self.__EMS7Cursor.execute("select EMS7LocationId from RegionLocationMapping where EMS6RegionId=%s",(EMS6RegionId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        LocationId = row[0]
                        return LocationId

        def __getEMS7PaystationId(self,EMS6PaystationId):
                PaystationId=None
                self.__EMS7Cursor.execute("select EMS7PaystationId from PaystationMapping where EMS6PaystationId=%s",(EMS6PaystationId))
                if (self.__EMS7Cursor.rowcount<>0):
                        row = self.__EMS7Cursor.fetchone()
                        PaystationId =row[0]
                        return PaystationId

	def __migrateAlert(self,Action,EMS6Id,IsProcessed,MigrationId):
		if (Action=='Update' and IsProcessed==0):
                        self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert where Id=%s", EMS6Id)
                        alerts=self.__EMS6Cursor.fetchall()
			for alert in alerts:
				self.__EMS7DataAccess.addCustomerAlertToEMS7(alert,Action)
				self.__updateMigrationQueue(MigrationId)		

		if (Action=='Insert' and IsProcessed==0):
                        self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert where Id=%s", EMS6Id)
                        alerts=self.__EMS6Cursor.fetchall()
                        for alert in alerts:
				self.__EMS7DataAccess.addCustomerAlertToEMS7(alert,Action)
           	                self.__updateMigrationQueue(MigrationId)

		if (Action=='Delete' and IsProcessed==0):
			self.__EMS6Cursor.execute("delete from CustomerAlertType where Id=(select EMS7CustomerAlertTypeId from CustomerAlertTypeMapping where EMS6AlertId=%s",(EMS6Id))
			self.__EMS7Connection.commit()

		#This code will be enabled if the Alerts are being deleted
#                elif (Action=='Delete' and IsProcessed==0):
 #                       self.__EMS7Cursor.execute("select distinct(EMS7AlertId) from CustomerAlertTypeMapping where EMS6AlertId=%s", EMS6Id)
  #                      EMS7CustomerId=self.__EMS7Cursor.fetchone()
#			self.__EMS7Cursor.execute("delete from CustomerAlertType where Id=%s",EMS7CustomerId)
#			self.__EMS7Connection.commit()
#	                self.__updateMigrationQueue(MigrationId)

	def __migrateBadValueCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		CustomerId = Split[0]
		AccountNumber= Split[1]
		
                if (Action=='Update' and IsProcessed==0):
                        EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
                        IsProcessed = 1
                        for BadValueCard in EMS6BadValueCards:
				self.__EMS7DataAccess.addBadValueCardToEMS7(self, BadValueCard, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		
                if (Action=='Insert' and IsProcessed==0):
			EMS6BadValueCards=self.__getEMS6BadValueCard(CustomerId,AccountNumber)
                        IsProcessed = 1
			for BadValueCards in EMS6BadValueCards:
				self.__EMS7DataAccess.addBadValueCardToEMS7(self, BadValueCard, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

#		if (Action=='Delete'):
#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
#			EMS7Connection.commit()

	def __migrateBadCreditCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		CustomerId = Split[0]
		AccountNumber= Split[1]
		
                if (Action=='Update' and IsProcessed==0):
                        EMS6BaCreditCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
                        IsProcessed = 1
                        for BadCreditCard in EMS6BadCreditCards:
				self.__EMS7DataAccess.addBadCreditCardToEMS7(self, BadCreditCard, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		
                if (Action=='Insert' and IsProcessed==0):
			EMS6BadValueCards=self.__getEMS6BadCreditCard(CustomerId,CardHash)
                        IsProcessed = 1
			for BadCreditCard in EMS6BadCreditCards:
				self.__EMS7DataAccess.addBadCreditCardToEMS7(self, BadCreditCard, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

#		if (Action=='Delete'):
#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
#			EMS7Connection.commit()

	def __migrateBadSmartCard(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		CustomerId = Split[0]
		CardNumber= Split[1]
		
                if (Action=='Update' and IsProcessed==0):
                        EMS6BadSmartCards=self.__getEMS6BadSmartCard(CustomerId,CardNumber)
                        IsProcessed = 1
                        for BadSmartCard in EMS6BadSmartCards:
				self.__EMS7DataAccess.addBadSmartCardToEMS7(self, BadSmartCard, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		
                if (Action=='Insert' and IsProcessed==0):
			EMS6BadValueCards=self.__getEMS6BadSmartCard(CustomerId,CardHash)
                        IsProcessed = 1
			for BadSmartCard in EMS6BadSmartCards:
				self.__EMS7DataAccess.addBadSmartCardToEMS7(self, BadSmartCard, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

#		if (Action=='Delete'):
#			EMS7CouponId=self.__EMS7Cursor.execute("delete from Coupon where Id=(select distinct(EMS7CouponId) from CouponMapping where EMS6CouponId=%s and EMS6CustomerId=%s)",(Coupon,CustomerId))
#			EMS7Connection.commit()

	def __getEMS6BadCreditCard(CustomerId, CardHash):
		self.__EMS6Cursor.execute("select CustomerId,CardHash,CardData,CardExpiry,AddedDate,Comment from BadCard where CustomerId=%s and CardHash=%s",(CustomerId,CardHash))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadValueCard(CustomerId, AccountNumber):
		self.__EMS6Cursor.execute("select CustomerId,AccountNumber,version,CardType from BadCard where CustomerId=%s and AccountNumber=%s",(CustomerId,AccountNumber))
		return self.__EMS6Cursor.fetchall()

	def __getEMS6BadSmartCard(CustomerId, CardNumber):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,AddedDate,Comment from BadSmartCard where CustomerId=%s and CardNumber=%s",(CustomerId, CardNumber))
		return self.__EMS6Cursor.fetchall()

	def __migrateBatteryInfo(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		PaystationId = Split[0]
		DateField= Split[1]
		
                if (Action=='Update' and IsProcessed==0):
                        EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
                        IsProcessed = 1
                        for BatteryInfo in EMS6BatteryInfos:
				self.__EMS7DataAccess.addBatteryInfoToEMS7(self, BatteryInfo, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		
                if (Action=='Insert' and IsProcessed==0):
			EMS6BatteryInfos=self.__getEMS6BatteryInfo(PaystationId,DateField)
                        IsProcessed = 1
			for BatteryInfo in EMS6BatteryInfos:
				self.__EMS7DataAccess.addBatteryInfoToEMS7(self, BatteryInfo, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

	def __migrateCardRetryTransaction(self, Action, IsProcessed, MigrationId, MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		PaystationId=Split[0]           
		print "PaystationId %s" %PaystationId 
		PurchasedDate=Split[1]           
		print "PurchasedDate %s" %PurchasedDate
		TicketNumber=Split[2]            
		print "TicketNumber %s" %TicketNumber

                if (Action=='Update' and IsProcessed==0):
                        IsProcessed = 1
			self.__EMS7DataAccess.addCardRetryTransactionToEMS7(MultiKeyId,Action)
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()
                
		if (Action=='Insert' and IsProcessed==0):
			IsProcessed = 1
			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
			print EMS6CardRetryTransactions[0]
			if(EMS6CardRetryTransactions):
				for EMS6CardRetryTransaction in EMS6CardRetryTransactions:
					self.__EMS7DataAccess.addCardRetryTransactionToEMS7(EMS6CardRetryTransaction,Action)
					self.__updateMigrationQueue(MigrationId)
					self.__EMS7Connection.commit()

		if(Action=='Delete' and IsProcessed==0):
#			EMS6CardRetryTransactions = self.__getEMS6CardRetryTransactions(PaystationId,PurchasedDate,TicketNumber)
#			if(EMS6CardRetryTransactions):
			self.__EMS7Cursor.execute("delete from CardRetryTransaction where Id=(select distinct(EMS7CardRetryTransactionId) from CardRetryTransactionMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
			EMS7Connection.commit()
			self.__updateMigrationQueue(MigrationId)

	def __getEMS6CardRetryTransactions(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID from CardRetryTransaction where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			row = self.__EMS6Cursor.fetchall()
		return row

	def __getEMS6BatteryInfo(PaystationId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo where PaystationId=%s and DateField=%s",(PaystationId,DateField))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMSExtensiblePermit(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId

		if (Action=='Update' and IsProcessed==0):
                        print "Incremental migration for EMSExtensiblePermit"
			self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s",(EMS6Id))
			ExtensiblePermits = self.__EMS6Cursor.fetchall()
                        IsProcessed = 1
                        for ExtensiblePermit in ExtensiblePermits:
				self.__EMS7DataAccess.addEMSExtensiblePermitToEMS7(ExtensiblePermit, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
                        self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit where MobileNumber=%s", EMS6Id)
			ExtensiblePermits = self.__EMS6Cursor.fetchall()
                        IsProcessed = 1
                        for ExtensiblePermit in ExtensiblePermits:
				self.__EMS7DataAccess.addEMSExtensiblePermitToEMS7(ExtensiblePermit, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

	def __migrateCryptoKey(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

		Split=MultiKeyId.split(',')
		Type = Split[0]
		KeyIndex = Split[1]
		
                if (Action=='Update' and IsProcessed==0):
                        EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
                        IsProcessed = 1
                        for CryptoKey in EMS6CryptoKeys:
				self.__EMS7DataAccess.addCryptoKeyToEMS7(self, CryptoKey, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		
                if (Action=='Insert' and IsProcessed==0):
		        EMS6CryptoKeys=self.__getEMS6CryptoKey(Type,KeyIndex)
                        IsProcessed = 1
                        for CryptoKey in EMS6CryptoKeys:
				self.__EMS7DataAccess.addCryptoKeyToEMS7(self, CryptoKey, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

	def getEMS6CryptoKey(Type,KeyIndex):
		self.__EMS6Cursor.execute("select Type,KeyIndex,Hash,Expiry,Signature,Info,NextHash,Status,CreateDate,Comment from CryptoKey where Type=%s and KeyIndex=%s",(Type,KeyIndex))
		return self.__EMS6Cursor.fetchall()

#EMS6.CustomerWsCal= EMS7.CustomerWebServiceCal
	def __migrateCustomerWsCal(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId

		if (Action=='Update' and IsProcessed==0):
			CustomerWsCals=self.__getEMS6CustomerWsCal(Type,KeyIndex)
                        IsProcessed = 1
                        for CustomerWsCal in CustomerWsCals:
				self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
			CustomerWsCals=self.__getEMS6CustomerWsCal(Type,KeyIndex)
                        IsProcessed = 1
                        for CustomerWsCal in CustomerWsCals:
				self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(CustomerWsCal, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

	def __getEMS6CustomerWsCal(self,EMS6Id):	
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

## EMS6.CustomerWsToken = EMS7.WebServiceEndPoint
	def __migrateCustomerWsToken(self, Action, EMS6Id, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- EMS6Id %s" %EMS6Id
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId

		if (Action=='Update' and IsProcessed==0):
			CustomerWsToken=self.__getEMS6CustomerWsToken(EMS6Id)
                        IsProcessed = 1
                        for CustomerWsToken in CustomerWsToken:
				self.__EMS7DataAccess.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

		elif(Action=='Insert' and IsProcessed==0):
			CustomerWsTokens=self.__getEMS6CustomerWsToken(EMS6Id)
                        IsProcessed = 1
                        for CustomerWsToken in CustomerWsTokens:
				self.__EMS7DataAccess.addCustomerWsTokenToEMS7(CustomerWsToken, Action)
				self.__updateMigrationQueue(MigrationId)
                                self.__EMS6Connection.commit()
                                self.__EMS7Connection.commit()

	def __getEMS6CustomerWsToken(self, EMS6Id):
                self.__EMS6Cursor.execute("select CustomerId,Token,EndPointId,WsInUse,Id from CustomerWsToken where Id=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

 	def __migrateEMSParkingPermission(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                RegionId = Split[0]
                StartTimeHourLocal = Split[1]
		StartTimeMinuteLocal = Split[2]

                if (Action=='Update' and IsProcessed==0):
                        EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
                        IsProcessed = 1
                        for EMSParkingPermission in EMS6EMSParkingPermissions:
                                self.__EMS7DataAccess.addParkingPermissionToEMS7(self, EMSParkingPermission, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
		     EMS6EMSParkingPermissions=self.__getParkingPermission(RegionId,StartTimeHourLocal,StartTimeMinuteLocal)
		     IsProcessed = 1
		     for EMSParkingPermission in EMS6EMSParkingPermissions:
			self.__EMS7DataAccess.addParkingPermissionToEMS7(self, EMSParkingPermission, Action)
			self.__updateMigrationQueue(MigrationId)
			self.__EMS7Connection.commit()

	def __getParkingPermission(self,RegionId,StartTimeHourLocal,StartTimeMinuteLocal):
		self.__EMS6Cursor.execute("select Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,PermissionTypeId,PermissionStatus,SpecialParkingPermissionId,MaxDurationMinutes,CreationDate,IsActive,Id from EMSParkingPermission where RegionId=%s and StartTimeHourLocal=%s and StartTimeMinuteLocal=%s",(RegionId,StartTimeHourLocal,StartTimeMinuteLocal))
		return self.__EMS6Cursor.fetchall()

	def __migrateEMSRate(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " -- Action %s" %Action
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId
		print " -- EMS6Id %s" %EMS6Id

		if (Action=='Update' and IsProcessed==0):
                        EMS6EMSRates=self.__getEMS6EMSRate(EMS6Id)
                        IsProcessed = 1
                        for EMSRate in EMS6EMSRates:
                                self.__EMS7DataAccess.addExtensibleRatesToEMS7(self, EMSRate, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        EMS6EMSRates=self.__getEMS6EMSRate(EMS6Id)
                        IsProcessed = 1
                        for EMSRate in EMS6EMSRates:
                                self.__EMS7DataAccess.addExtensibleRatesToEMS7(self, EMSRate, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
		if(Action=='Delete' and Isprocessed==0):
			self.__EMS7Cursor.execute("delete from ExtensibleRate where Id=(select EMS7RateId from EMSRateMapping where EMS6RateId=%s",(EMS6Id))
			self.__EMS7Connection.commit()

	def __getEMS6EMSRate(self,EMS6Id):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,IsActive,Id from EMSRate where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateEventLogNew(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                PaystationId = Split[0]
                DeviceId = Split[1]
                TypeId = Split[2]
		ActionId = Split[3]
		DateField = Split[4]

                if (Action=='Update' and IsProcessed==0):
                        EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
                        IsProcessed = 1
                        for EventLogNew in EMS6EventLogNews:
                                self.__EMS7DataAccess.addPOSAlertToEMS7(self, EventLogNew, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
			EMS6EventLogNews=self.__getEventLogNew(PaystationId,DeviceId,TypeId,ActionId,DateField)
                        IsProcessed = 1
                        for EventLogNew in EMS6EventLogNews:
                                self.__EMS7DataAccess.addPOSAlertToEMS7(self, EventLogNew, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

	def __getEventLogNew(self, PaystationId,DeviceId,TypeId,ActionId,DateField):
		self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,ClearDateField from EventLogNew where PaystationId=%s and DeviceId=%s and TypeId=%s and ActionId=%s and DateField=%s",PaystationId,DeviceId,TypeId,ActionId,DateField)
		return self.__EMS6Cursor.fetchall()

	def __migrateLotSettingFileContent(self,Action,EMS6Id,IsProcessed,MigrationId):
        	if (Action=='Update' and IsProcessed==0):
			LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
			for LotSettingFileContent in LotSettingFileContents:
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
				self.__updateMigrationQueue(MigrationId)	
	
                elif (Action=='Insert' and IsProcessed==0):
			LotSettingFileContents=self.__getEMS6LotSettingFileContent(EMS6Id)
			for LotSettingFileContent in LotSettingFileContents:
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(LotSettingFileContent, Action)
				self.__updateMigrationQueue(MigrationId)		

	def getEMS6LotSettingContent(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()
	
	def __migrateMerchantAccount(self,Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId
		print " -- EMS6Id %s" %EMS6Id
               
		if (Action=='Update' and IsProcessed==0):
                        IsProcessed = 1
			self.__EMS7DataAccess.addMerchantAccount(MultiKeyId, Action)
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
			EMS6MerchantAccounts=self.__getEMS6MerchantAccount(EMS6Id)
                        IsProcessed = 1
			for EMS6Merchant in EMS6MerchantAccounts:
				self.__EMS7DataAccess.addMerchantAccount(EMS6Merchant, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		if (Action=='Delete'):
			self.__EMS7Cursor.execute("delete from MerchantAccount where Id=(select distinct(EMS7MerchantAccountId) from MerchantAccountMapping where EMS6MerchantAccountId=%s)",(EMS6Id))
			EMS7Connection.commit()

	def __getEMS6MerchantAccount(self,EMS6Id):
		self.__EMS6Cursor.execute("select Id,version,CustomerId,Name,Field1,Field2,Field3,IsDeleted,ProcessorName,ReferenceCounter,Field4,Field5,Field6,IsValid from MerchantAccount where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateServiceState(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                #print " -- MultiKeyId %s" %MultiKeyId
                print " -- EMS6Id %s" %EMS6Id

                if (Action=='Update' and IsProcessed==0):
			EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
                        IsProcessed = 1
			for EMS6ServiceState in EMS6ServiceStates:
				self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
                        EMS6ServiceStates=self.__getEMS6ServiceState(EMS6Id)
                        IsProcessed = 1
                        for EMS6ServiceState in EMS6ServiceStates:
                                self.__EMS7DataAccess.addPOSServiceStateToEMS7(EMS6ServiceState, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
                if (Action=='Delete'):
			self.__EMS7Cursor.execute("delete from POSServiceState where PointOfSaleId=(select po.Id from PointOfSale po,PaystationMapping pm where po.PaystationId=pm.EMS7PaystationId and EMS6PaystationId=%s)",(EMS6Id))
                        EMS7Connection.commit()

	def __getEMS6ServiceState(self,EMS6Id):
		ServiceState=None
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState where PaystationId=%s",(EMS6Id))
		if(self.__EMS6Cursor.rowcount<>0):
			ServiceState=self.__EMS6Cursor.fetchall()
		return ServiceState

	def __migrateRestAccount(self, Action, MultiKeyId, IsProcessed, MigrationId):
		print " -- Action %s" %Action
		print " -- MultiKeyId %s" %MultiKeyId
		print " -- IsProcessed %s" %IsProcessed
		print " -- MigrationId %s" %MigrationId

		if (Action=='Update' and IsProcessed==0):
			RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
                        IsProcessed = 1
                        for RestAccount in RestAccounts:
				self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
				self.__updateMigrationQueue(MigrationId)

		elif(Action=='Insert' and IsProcessed==0):
			RestAccounts=self.__getEMS6RestAccount(MultiKeyId)
                        IsProcessed = 1
                        for RestAccount in RestAccounts:
				self.__EMS7DataAccess.addRestAccountToEMS7(RestAccount, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()

		elif(Action=='Delete' and IsProcessed==0):
                        IsProcessed = 1
			self.__EMS7Cursor.execute("delete from RestAccount where Id=(select EMS7RestAccountId from RestAccountMapping where EMS6AccountName=%s",(MultiKeyId))
			self.__updateMigrationQueue(MigrationId)
			self.__EMS7Connection.commit()

	def __getEMS6RestAccount(self, EMS6Id):
                self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null and r.AccountName=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRates(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId
		print " -- EMS6Id %s" %EMS6Id

                Split=MultiKeyId.split(',')
                PaystationId = Split[0]
                PurchasedDate = Split[1]
                TicketNumber = Split[2]
               
		if (Action=='Update' and IsProcessed==0):
                        IsProcessed = 1
			EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
			self.__EMS7DataAccess.addRatesToEMS7(EMS6Rates, Action)
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
			EMS6Rates=self.__getEMS6Rates(PaystationId,PurchasedDate,TicketNumber)
                        IsProcessed = 1
			self.__EMS7DataAccess.addRatesToEMS7(EMS6Rates, Action)
			self.__updateMigrationQueue(MigrationId)
			self.__EMS7Connection.commit()
		if (Action=='Delete'):
			IsProcessed = 1
			self.__EMS7Cursor.execute("delete from UnifiedRate where Id=(select EMS7UnifiedRateId from RateToUnifiedRateMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s)",(PaystationId,PurchasedDate,TicketNumber))
			self.__updateMigrationQueue(MigrationId)
			EMS7Connection.commit()

	def getEMS6Rates(self, PaystationId,PurchasedDate,TicketNumber):
		EMS6Rates=None	
		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		if(self.__EMS6Cursor.rowcount<>0):
			EMS6Rate=self.__EMS6Cursor.fetchall()
		return EMS6Rates

	def migratePaystationGroup(self,Action,IsProcessed,MigrationId,EMS6Id):
		print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
		print " -- EMS6Id %s" %EMS6Id
               
		if (Action=='Update' and IsProcessed==0):
			EMS6PaystationGroups = self.__getEMS6PaystationGroup(self,EMS6Id)
                        IsProcessed = 1
			for PaystationGroup in EMS6PaystationGroups:
				self.__EMS7DataAccess.addPaystationGroupToEMS7(PaystationGroup, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		if (Action=='Insert' and IsProcessed==0):
			EMS6PaystationGroups = self.__getEMS6PaystationGroup(self,EMS6Id)
                        IsProcessed = 1
			for PaystationGroup in EMS6PaystationGroups:
				self.__EMS7DataAccess.addPaystationGroupToEMS7(PaystationGroup, Action)
				self.__updateMigrationQueue(MigrationId)
				self.__EMS7Connection.commit()
		if (Action=='Delete'):
			self.__EMS7Cursor.execute("delete from PaystationGroup where Id=(select distinct(EMS7RouteId) from PaystationGroupRouteMapping where EMS6PaystationGroupId=%s)",(EMS6Id))
			EMS7Connection.commit()

	def __getEMS6PaystationGroup(self, EMS6Id):
		self.__EMS6Cursor.execute("select Id,CustomerId,Name from PaystationGroup where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateProcessorProperties(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                Processor = Split[0]
                Name = Split[1]
               
		if (Action=='Update' and IsProcessed==0):
                        IsProcessed = 1
			EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
			self.__EMS7DataAccess.addProcessorProperties(EMS6ProcessorProperties, Action)
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
                        IsProcessed = 1
			EMS6ProcessorProperties=self.__getEMS6ProcessorProperties(Processor,Name)
			self.__EMS7DataAccess.addProcessorProperties(EMS6ProcessorProperties, Action)
			self.__updateMigrationQueue(MigrationId)
                        self.__EMS7Connection.commit()
		if (Action=='Delete'):
			IsProcessed = 1
			self.__EMS7Cursor.execute("delete from ProcessorProperty where Id=(select EMS7Id from ProcessorPropertiesMapping where Processor=%s and Name=%s)",(Processor,Name))
			self.__updateMigrationQueue(MigrationId)
			EMS7Connection.commit()

	def __getProcessorProperties(self, Processor,Name):
		self.__EMS6Cursor.execute("select Processor,Name,Value from ProcessorProperties where Processor=%s and Name=%s",(Processor,Name))
		return self.__EMS6Cursor.fetchall()


	def __migrateSmsAlert(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- EMS6Id %s" %EMS6Id

                if (Action=='Update' and IsProcessed==0):
                        SMS=self.__getEMS6SMSAlert(EMS6Id)
                        IsProcessed = 1
                        for EMSSMS in SMS:
                                self.__EMS7DataAccess.addSMSAlertToEMS7(self, EMSSMS, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        SMS=self.__getEMS6SMSAlert(EMS6Id)
                        IsProcessed = 1
                        for EMSSMS in SMS:
                                self.__EMS7DataAccess.addSMSAlertToEMS7(self, EMSSMS, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if(Action=='Delete' and Isprocessed==0):
                        self.__EMS7Cursor.execute("delete from SmsAlert where Id=(select EMS7Id from SMSAlertMapping where MobileNumber=%s",(MobileNumber))
                        self.__EMS7Connection.commit()
                        self.__updateMigrationQueue(MigrationId)

	def __getEMS6SMSAlert(self,EMS6Id):
                self.__EMS6Cursor.execute("select MobileNumber,ExpiryDate,PaystationId,PurchasedDate,TicketNumber,PlateNumber,StallNumber,RegionId,NumOfRetry,IsAlerted,IsLocked,IsAutoExtended from SMSAlert where MobileNumber=%s",(EMS6Id))
                return self.__EMS6Cursor.fetchall()

	def __migrateUserAccount(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- EMS6Id %s" %EMS6Id

                if (Action=='Update' and IsProcessed==0):
                        UserAccounts=self.__getEMS6UserAccount(EMS6Id)
                        IsProcessed = 1
                        for UserAccount in UserAccounts:
                                self.__EMS7DataAccess.addUserAccountToEMS7(self, UserAccount, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
                if (Action=='Insert' and IsProcessed==0):
                        UserAccounts=self.__getEMS6UserAccount(EMS6Id)
                        IsProcessed = 1
                        for UserAccount in UserAccounts:
                                self.__EMS7DataAccess.addUserAccountToEMS7(self, UserAccount, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()
                if(Action=='Delete' and Isprocessed==0):
                        self.__EMS7Cursor.execute("delete from UserAccount where Id=(select EMS7UserAccountId from UserAccountMapping where EMS6UserAccountId=%s",(EMS6Id))
                        self.__EMS7Connection.commit()
                        self.__updateMigrationQueue(MigrationId)

	def __migrateAudit(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                PaystationId = Split[0]
                CollectionTypeId = Split[1]
                EndDate = Split[2]
		StartDate = Split[3]

                if (Action=='Update' and IsProcessed==0):
                        EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
                        IsProcessed = 1
                        for Audit in EMS6Audit:
                                self.__EMS7DataAccess.addPOSCollectionToEMS7(self, Audit, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        EMS6Audit=self.__getEMS6Audit(PaystationId,CollectionTypeId,EndDate,StartDate)
                        IsProcessed = 1
                        for Audit in EMS6Audit:
                                self.__EMS7DataAccess.addPOSCollectionToEMS7(self, Audit, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

	def getEMS6Audit(self, PaystationId,CollectionTypeId,EndDate,StartDate):
#		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CustomerId from Audit")
		self.__EMS6Cursor.execute("select PaystationId,CollectionTypeId,StartDate,EndDate,StartTransactionNumber,EndTransactionNumber,version,PaystationCommAddress,LotNumber,MachineNumber,PatrollerTicketsSold,TicketsSold,CoinAmount,OnesAmount,TwoesAmount,FivesAmount,TensAmount,TwentiesAmount,FiftiesAmount,AmexAmount,DiscoverAmount,MasterCardAmount,OtherCardAmount,SmartCardAmount,VisaAmount,CitationsPaid,CitationAmount,ChangeIssued,RefundIssued,SmartCardRechargeAmount,ExcessPayment,CustomerId,ReportNumber,NextTicketNumber,AttendantTicketsSold,AttendantTicketsAmount,DinersAmount,AttendantDepositAmount,AcceptedFloatAmount,Tube1Type,Tube1Amount,Tube2Type,Tube2Amount,Tube3Type,Tube3Amount,Tube4Type,Tube4Amount,OverfillAmount,ReplenishedAmount,Hopper1Dispensed,Hopper2Dispensed,Hopper1Type,Hopper2Type,Hopper1Current,Hopper2Current,Hopper1Replenished,Hopper2Replenished,ChangerTestDispensed,Hopper1TestDispensed,Hopper2TestDispensed,CoinCount005,CoinCount010,CoinCount025,CoinCount100,CoinCount200,BillCount01,BillCount02,BillCount05,BillCount10,BillCount20,BillCount50 from Audit where PaystationId=%s and CollectionTypeId=%s and EnsDate=%s and StartDate=%s",(PaystationId,CollectionTypeId,EndDate,StartDate))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransaction2Push(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                PaystationId = Split[0]
		PurchasedDate = Split[1]
		TicketNumber = Split[2]
		ThirdPartyServiceAccountId = Split[3]

                if (Action=='Update' and IsProcessed==0):
                        EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
                        IsProcessed = 1
                        for Transaction2Push in EMS6Transaction2Push:
                                self.__EMS7DataAccess.addTransaction2Push(self, Transaction2Push, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        EMS6Transaction2Push=self.__getEMS6Transaction2Push(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId)
                        IsProcessed = 1
                        for Transaction2Push in EMS6Transaction2Push:
                                self.__EMS7DataAccess.addTransaction2Push(self, Transaction2Push, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

	def __getEMS6Transaction2Push(self, PaystationId, PurchasedDate, TicketNumber, ThirdPartyServiceAccountId):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId,LotNumber,PlateNumber,ExpiryDate,ChargedAmount,StallNumber,RegionId,RetryCount,LastRetryTime,IsOffline,Field1 from Transaction2Push where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s and ThirdPartyServiceAccountId=%s",(PaystationId,PurchasedDate,TicketNumber,ThirdPartyServiceAccountId))
		return self.__EMS6Cursor.fetchall()

	def __migrateTransactionPush(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKeyId %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                PaystationId = Split[0]
		PurchasedDate = Split[1]
		TicketNumber = Split[2]
		ThirdPartyServiceAccountId = Split[3]

                if (Action=='Update' and IsProcessed==0):
                        EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
                        IsProcessed = 1
                        for TransactionPush in EMS6TransactionPush:
                                self.__EMS7DataAccess.addTransactionPush(self, TransactionPush, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        EMS6TransactionPush=self.__getEMS6TransactionPush(PaystationId,PurchasedDate,TicketNumber)
                        IsProcessed = 1
                        for TransactionPush in EMS6TransactionPush:
                                self.__EMS7DataAccess.addTransactionPush(self, TransactionPush, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

		if(Action=='Delete' and IsProcessed==0):
			self.__EMS7Cursor.execute("delete from TransactionPush where Id=(select EMS7Id from TransactionPushMapping where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
			self.__EMS7Connection.commit()

	def __getEMS6TransactionPush(self,PaystationId,PurchasedDate,TicketNumber):
		self.__EMS6Cursor.execute("select PaystationId, PurchasedDate, TicketNumber, CustomerId, LotNumber, PlateNumber, ExpiryDate, ChargedAmount, StallNumber, RetryCount, LastRetryTime, IsOffline from TransactionPush where PaystationId=%s and PurchasedDate=%s and TicketNumber=%s",(PaystationId,PurchasedDate,TicketNumber))
		return self.__EMS6Cursor.fetchall()

	def __migrateCustomerWsCal(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- EMS6Id %s" %EMS6Id

                if (Action=='Update' and IsProcessed==0):
                        CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
                        IsProcessed = 1
                        for WebService in CustomerWsCal:
                                self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        CustomerWsCal=self.__getEMS6CustomerWebServiceCal(EMS6Id)
                        IsProcessed = 1
                        for WebService in CustomerWsCal:
                                self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(self, WebService, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if(Action=='Delete' and Isprocessed==0):
                        self.__EMS7Cursor.execute("delete from CustomerWsCalMapping where Id=(select EMS7Id from CustomerWsCalMapping where EMS6Id=%s",(EMS6Id))
                        self.__EMS7Connection.commit()
                        self.__updateMigrationQueue(MigrationId)

	def __getEMS6CustomerWebServiceCal(self, EMS6Id):
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description from CustomerWsCal where Id=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestSessionToken(self,Action,IsProcessed,MigrationId,EMS6Id):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- EMS6Id %s" %EMS6Id

                if (Action=='Update' and IsProcessed==0):
                        RestSessionToken=self.__getEMS6RestSessionToken(EMS6Id)
                        IsProcessed = 1
                        for RestSession in RestSessionToken:
                                self.__EMS7DataAccess.addRestSessionToken(self, RestSession, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        RestSessionToken=self.__getEMS6RestSessionToken(EMS6Id)
                        IsProcessed = 1
                        for RestSession in RestSessionToken:
                                self.__EMS7DataAccess.addRestSessionToken(self, RestSession, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if(Action=='Delete' and Isprocessed==0):
                        self.__EMS7Cursor.execute("delete from RestSessionToken where Id=(select EMS7Id from RestSessionTokenMapping where AccountName=%s",(EMS6AccountName))
                        self.__EMS7Connection.commit()
                        self.__updateMigrationQueue(MigrationId)

	def __getEMS6RestSessionToken(self, EMS6Id):	
		self.__EMS6Cursor.execute("select AccountName,SessionToken,CreationDate,ExpiryDate from RESTSessionToken where AccountName=%s",(EMS6Id))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLog(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKey Id %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                RESTAccountName = Split[0]
		EndpointName = Split[1]
		LoggingDate = Split[2]
		IsError = Split[3]
		CustomerId = Split[4]

                if (Action=='Update' and IsProcessed==0):
                        RestLogs=self.__getEMS6RestLog(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
                        IsProcessed = 1
                        for RestLog in RestLogs:
                                self.__EMS7DataAccess.addRestLogToEMS7(self, RestLog, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        RestLogs=self.__getEMS6RestLog(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId)
                        IsProcessed = 1
                        for RestLog in RestLogs:
                                self.__EMS7DataAccess.addRestLogToEMS7(self, RestLog, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if(Action=='Delete' and Isprocessed==0):
                        self.__EMS7Cursor.execute("delete from RestLog where Id=(select EMS7Id from RestLogMapping where RESTAccountName=%s and EndpointName=%s and LoggingDate=%s and IsError=%s and CustomerId=%s",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
                        self.__EMS7Connection.commit()
                        self.__updateMigrationQueue(MigrationId)

	def getEMS6RestLogs(self,RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId):	
		self.__EMS6Cursor.execute("select RestAccountName, EndpointName, LoggingDate, IsError, CustomerId, TotalCalls from RESTLog where RESTAccountName=%s,EndpointName=%s,LoggingDate=%s,IsError=%s,CustomerId=%s",(RESTAccountName,EndpointName,LoggingDate,IsError,CustomerId))
		return self.__EMS6Cursor.fetchall()

	def __migrateRestLogTotalCall(self,Action,IsProcessed,MigrationId,MultiKeyId):
                print " -- Action %s" %Action
                print " -- IsProcessed %s" %IsProcessed
                print " -- MigrationId %s" %MigrationId
                print " -- MultiKey Id %s" %MultiKeyId

                Split=MultiKeyId.split(',')
                RESTAccountName = Split[0]
		LoggingDate = Split[1]

                if (Action=='Update' and IsProcessed==0):
                        RestLogTotalCall=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
                        IsProcessed = 1
                        for RestLog in RestLogs:
                                self.__EMS7DataAccess.addRestLogTotalCall(self, RestLog, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

                if (Action=='Insert' and IsProcessed==0):
                        RestLogTotalCall=self.__getEMS6RestLogTotalCall(RESTAccountName,LoggingDate)
                        IsProcessed = 1
                        for RestLog in RestLogs:
                                self.__EMS7DataAccess.addRestLogTotalCall(self, RestLog, Action)
                                self.__updateMigrationQueue(MigrationId)
                                self.__EMS7Connection.commit()

	def __getEMS6RestLogTotalCall(self,RESTAccountName,LoggingDate):	
		self.__EMS6Cursor.execute("select RESTAccountName,LoggingDate,TotalCalls from RESTLogTotalCall where RESTAccountName=%s and LoggingDate=%s",(RESTAccountName,LoggingDate))
		return self.__EMS6Cursor.fetchall()

	def IncrementalMigration(self):
		self.__EMS6Cursor.execute("select * FROM MigrationQueue")
		MigrationQueue=self.__EMS6Cursor.fetchall()
		today = date.today()
		input = raw_input("Start Migration  ........................... y/n ?   :   ")

		if (input=='y') :
			for migration in MigrationQueue:
				MigrationId = migration[0]
				MultiKeyId = migration[1]
				EMS6Id = migration[2]
				TableName = migration[3]
				Action = migration[4]
				IsProcessed = migration[5]
				Date = migration[6]
				''' Begin Transaction '''
				if (TableName=='Customer'):
					self.__migrateCustomers(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='CustomerProperties'):
					self.__migrateCustomerProperties(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='Customer_Service_Relation'):
					self.__migrateCustomerSubscription(Action,IsProcessed,MigrationId,MultiKeyId)
				if (TableName=='Paystation'):
					self.__migratePaystations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Region'):
					self.__migrateLocations(Action,EMS6Id,IsProcessed,MigrationId)
				if (TableName=='Coupon'):
					self.__migrateCoupons(Action,IsProcessed,MigrationId,MultiKeyId)
				if (TableName=='LotSetting'):
					self.__migrateLotSetting(Action,EMS6Id,IsProcessed,MigrationId)
				if(TableName=='BadCard'):
					self.__migrateBadValueCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BadSmartCard'):
					self.__migrateBadSmartCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BadCreditCard'):
					self.__migrateBadCreditCard(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='BatteryInfo'):
					self.__migrateBatteryInfo(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='CardRetryTransaction'):
					self.__migrateCardRetryTransaction(Action, IsProcessed, MigrationId, MultiKeyId)
				if(TableName=='MerchantAccount'):
					self.__migrateMerchantAccount(Action,IsProcessed,MigrationId,MultiKeyId,EMS6Id)
				if(TableName=='RegionPaystationLog'):
					self.__migrateLocationPOSLog(Action, EMS6Id, IsProcessed, MigrationId)
				if(TableName=='ServiceState'):
					self.__migrateServiceState(Action,IsProcessed,MigrationId,EMS6Id)
				if(TableName=='RESTAccount'):
					self.__migrateRestAccount(Action, MultiKeyId, IsProcessed, MigrationId)
				if(TableName=='Rates'):
					self.__migrateRates(Action,IsProcessed,MigrationId,MultiKeyId)
try:

	EMS6Connection = mdb.connect('172.16.1.141', 'root', 'testitnow', 'ems_db');
	EMS6ConnectionCerberus = mdb.connect('172.16.1.141', 'root', 'testitnow','cerberus_db');
	EMS7Connection = mdb.connect('172.16.1.143', 'root','testitnow','EMS7')

	startMigration=EMS6IncrementalMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus ,1), EMS7DataAccess(EMS7Connection, 1), 1)
	startMigration.IncrementalMigration()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)

finally:
    if EMS6Connection:
        EMS6Connection.close()
    if EMS7Connection:
        EMS7Connection.close()
