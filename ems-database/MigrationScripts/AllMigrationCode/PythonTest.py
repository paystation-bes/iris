#!/usr/bin/python
import re

SerialNumber = "X0227777"
PaystationType=0
length = len(SerialNumber)	
if (re.search(r"^0", SerialNumber) and length==8 and PaystationType==0):
    print SerialNumber, "starts with 0 and has 8 characters, This is an intellapay machine. This is mapping to EMS7.PaystationType.Id=255"
    EMS7PaystationTypeId=255
elif(re.search(r"verrus", SerialNumber)):
    EMS7PaystationTypeId=10
    print "This is a Verrus Paystation" 
elif(re.search(r"^1", SerialNumber)): 
    print " It is a LUKE machine"
    EMS7PaystationTypeId=1
elif(re.search(r"^2", SerialNumber)):	
    print "It is a shelby machine"
    EMS7PaystationTypeId=2
elif(re.search(r"^3", SerialNumber)):	
    print " It is a Radius Luke"
    EMS7PaystationTypeId=3
elif(re.search(r"^5", SerialNumber)):
    print "It is a LUKE II machine"
    EMS7PaystationTypeId=5
elif(re.search(r"^8", SerialNumber)):
    print "It is a Test/Demo machine"
    EMS7PaystationTypeId=8
elif(re.search(r"^9", SerialNumber)):
    print "It is a virtual machine"
    EMS7PaystationTypeId=9
elif(re.search(r"(?i)x", SerialNumber)):
    print "The Paystation has X in it"
else:
    print "The Paystation did not match any of the above criteria, it is being assigned to other"
    EMS7PaystationTypeId=255
