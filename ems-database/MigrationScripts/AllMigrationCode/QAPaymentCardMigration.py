#!/usr/bin/python
# -*- coding: utf-8 -*-

##-------------------------------------------------------------------------------------------------------------------
##	Class		: migration.py 
##	Purpose		: This is the main controller file that issues a call to the Modules in the EMS6InitialMigration class. 
##	Important	: The migration of the specific tables in the class can be controlled in this file by disabling the calls.
##	Author		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

import MySQLdb as mdb
import sys
import time
from datetime import date
from EMS6InitialMigration import EMS6InitialMigration
from EMS6DataAccess import EMS6DataAccess
from EMS7DataAccess import EMS7DataAccess

try:
	EMS6Connection = mdb.connect('172.16.1.141', 'root', 'testitnow', 'ems_db'); 
	EMS6ConnectionCerberus = mdb.connect('172.16.1.141', 'root', 'testitnow','cerberus_db');
#	EMS7Connection = mdb.connect('172.16.1.143', 'root','testitnow','EMS7');
	EMS7Connection = mdb.connect('172.16.5.56','migration','m1gr1t3','EMS7')

	#EMS6Cursor = EMS6Connection.cursor()
	#EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()
	#EMS7Cursor = EMS7Connection.cursor()

	controller = EMS6InitialMigration(EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess(EMS6Connection, EMS6ConnectionCerberus, 1), EMS7DataAccess(EMS7Connection, 1), 0)
#	controller.migrateCustomers()
#	controller.migrateTrialCustomer()
#	controller.migrateLotSettings()
#	controller.migrateLocations()
#	controller.migrateParentRegionIdToLocation()
#	controller.migratePaystations() # PointOfSale table is Migrated from within Paystation table migration 
#	controller.migrateRegionPaystationLog()
#	controller.migratePaystationBalance()	
	#controller.migrateClusterMember() # for cluster table to work, please disable the auto increment for the EMS7.Cluster table. The auto_increment can be enabled after the initial migration has finished, as we will need to bring all the Id's as they exist in EMS6.
#	controller.migrateCollectionLock()
#	controller.migratePOSHeartBeat()
#	controller.migratePOSServiceState()
#	controller.migrateCoupons()
#	controller.migrateParkingPermission()
#	controller.migrateParkingPermissionDayOfWeek()
#	controller.migrateRestAccount()
#	controller.migrateLicencePlate()
#	controller.migrateUnifiedRate()
#	controller.migrateEMSRateIntoUnifiedRate()
#	controller.migrateRatesToEMS7ExtensibleRate()
#	controller.migrateExtensibleRateDayOfWeek()
#	controller.migrateMobileNumber()
#	controller.migrateSMSTransactionLog()
#	controller.migrateSMSAlert()
#	controller.migrateSMSFailedResponse()
#	controller.migrateModemSettings()  
#	controller.migrateLotSettingContent()
#	controller.migrateCryptoKey()	
#	controller.migrateRestLog()     
#	controller.migrateRestSessionToken() 
#	controller.migrateCustomerProperties()
#	controller.migrateQueryStallsCustomerProperty() # This Module populates the CustomerProperty table for the QueryStalls by Porperty Type from Paystation table.
#	controller.migrateRestLogTotalCall() 
#	controller.migrateCustomerWsCal()
#	controller.migrateCustomerWsToken()
#	controller.migrateCustomerAlert() ## Look for the notes to migrate this module, a db change is needed
#	controller.migrateCustomerSubscription()
#	controller.migrateCustomerWebServiceCal()
#	controller.migrateCustomerWsToken()
#	controller.migrateRates()
#	controller.migrateReplenish()
#	controller.migrateCardRetryTransaction()
#	controller.migrateCustomerCardType()
#	controller.migratePaystationGroup()
#	controller.migratePOSRoute()
	#controller.migrateCollectionType() #This table is not being migrated as data is being pre poluated
#	controller.migrateValueCardData()
#	controller.migrateSmartCardData()
#	controller.migrateBatteryInfo()
#	controller.migratePurchase()

## The Migration stopped right while migrating the table below, All the data Above this statement has been migrated
## The Moduel below to be migrated next


#	controller.migrateCustomerEmail()
#	controller.migratePaystationAlertEmail()
#	controller.migrateMerchantAccount()
#	controller.migrateProcessorProperties()
#	controller.migratePreAuth()
#	controller.migratePreAuthHolding()
#	controller.migrateProcessorTransaction()
#	controller.migratePermit()
#	controller.migrateExtensiblePermit()
	controller.migratePaymentCard()
#	controller.migrateUserAccount()
#	controller.migrateEventLogNewIntoPOSAlert()
#	controller.migrateActivePOSAlert()
#	controller.migratePOSDate()


## The module below is not getting migrating

#	controller.migratePOSDateBilling()
#	controller.migratePOSDateAuditAccess()
#	controller.migratePOSDatePaystation()
#	controller.migrateBadValueCard()
#	controller.migrateBadCreditCard()
#	controller.migrateBadSmartCard()


### The Modules below needs to enabled:
#	controller.migrateCCFailLog()
#	controller.migrateServiceAgreement()
#	controller.migrateServiceAgreedCustomer()
#	controller.migratePOSCollection()
#	controller.migratePOSStatus()

except mdb.Error, e:
    print "Error %d: %s" % (e.args[0],e.args[1])
    sys.exit(1)
