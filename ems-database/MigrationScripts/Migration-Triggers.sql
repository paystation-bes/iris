-- Tables and Triggers to be deployed in EMS6 Database --

Note : add an index to Purchase table on (POS, PurchaseGMT, PurchaseNumber
-- Migration table to hold Incremental Migration Data --

CREATE TABLE `MigrationQueue` (
  `Id` 				BIGINT 		UNSIGNED NOT NULL AUTO_INCREMENT,
  `MultiKeyId` 		VARCHAR(255),	
  `EMS6Id` 			INT UNSIGNED NOT NULL DEFAULT 0,
  `TableName` 		VARCHAR(40) NOT NULL,
  `Action` 			VARCHAR(6) NOT NULL,
  `IsProcessed` 	TINYINT NOT NULL DEFAULT 0,
  `CreatedDate` 	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MigratedDate` 	DATETIME ,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Customer Trigger --

CREATE TRIGGER UpdateCustomer 
AFTER UPDATE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Customer','Update');

CREATE TRIGGER InsertCustomer
AFTER INSERT ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Customer','Insert');

CREATE TRIGGER DeleteCustomer
AFTER DELETE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Customer','Delete');

-- Card Type --

CREATE TRIGGER UpdateCardType 
AFTER UPDATE ON CardType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CardType','Update');

CREATE TRIGGER InsertCardType
AFTER INSERT ON CardType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CardType','Insert');

CREATE TRIGGER DeleteCardType
AFTER DELETE ON CardType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CardType','Delete');

-- Coupon Trigger --

DELIMITER $$
CREATE TRIGGER UpdateCoupon 
AFTER UPDATE ON Coupon 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Id,',',New.CustomerId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCoupon
AFTER INSERT ON Coupon
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Id,',',New.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCoupon
AFTER DELETE ON Coupon
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Id,',',Old.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Coupon','Delete');
END$$

-- Region Trigger --

CREATE TRIGGER UpdateRegion 
AFTER UPDATE ON Region
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Region','Update');

CREATE TRIGGER InsertRegion
AFTER INSERT ON Region
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Region','Insert');

CREATE TRIGGER DeleteRegion
AFTER DELETE ON Region
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Region','Delete');

-- Paystation Trigger --

CREATE TRIGGER DeletePaystation
AFTER DELETE ON Paystation
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Paystation','Delete');

CREATE TRIGGER UpdatePaystation
AFTER UPDATE ON Paystation
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Paystation','Update');

CREATE TRIGGER InsertPaystation
AFTER INSERT ON Paystation
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Paystation','Insert');

--- LotSetting Trigger --

CREATE TRIGGER InsertLotSetting
AFTER INSERT ON LotSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSetting','Insert');

CREATE TRIGGER UpdateLotSetting
AFTER UPDATE ON LotSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSetting','Update');

CREATE TRIGGER DeleteLotSetting
AFTER DELETE ON LotSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'LotSetting','Delete');

-- CustomerProperties Trigger --

CREATE TRIGGER InsertCustomerProperties
AFTER INSERT ON CustomerProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.CustomerId,'CustomerProperties','Insert');

CREATE TRIGGER UpdateCustomerProperties
AFTER UPDATE ON CustomerProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.CustomerId,'CustomerProperties','Update');

CREATE TRIGGER DeleteCustomerProperties
AFTER DELETE ON CustomerProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.CustomerId,'CustomerProperties','Delete');

-- Customer Service Relation Trigger --

DELIMITER $$
CREATE TRIGGER UpdateCustomer_Service_Relation 
AFTER UPDATE ON Customer_Service_Relation 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.ServiceId); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCustomer_Service_Relation
AFTER INSERT ON Customer_Service_Relation
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.CustomerId,',',New.ServiceId); 
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomer_Service_Relation
AFTER DELETE ON Customer_Service_Relation
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.ServiceId);
INSERT INTO ems_db.MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Customer_Service_Relation','Delete');
END$$

-- Trial Customer cerberus_db.Customer Trigger --

CREATE TRIGGER InsertCerberusCustomer
AFTER INSERT ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusCustomer','Insert');

CREATE TRIGGER UpdateCerberusCustomer
AFTER UPDATE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusCustomer','Update');

CREATE TRIGGER DeleteCerberusCustomer
AFTER DELETE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CerberusCustomer','Delete');

-- Trigger for Cerberus Customer --

CREATE TRIGGER InsertCerberusCustomer
AFTER INSERT ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusCustomer','Insert');

CREATE TRIGGER UpdateCerberusCustomer
AFTER UPDATE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusCustomer','Update');

CREATE TRIGGER DeleteCerberusCustomer
AFTER DELETE ON Customer
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CerberusCustomer','Delete');

-- Alert Trigger --
CREATE TRIGGER InsertAlert
AFTER INSERT ON Alert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Alert','Insert');

CREATE TRIGGER UpdateAlert
AFTER UPDATE ON Alert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Alert','Update');

CREATE TRIGGER DeleteAlert
AFTER DELETE ON Alert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Alert','Delete');

-- Audit Trigger --
DELIMITER $$
CREATE TRIGGER UpdateAudit 
AFTER UPDATE ON Audit 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.CollectionTypeId,',',New.EndDate,',',New.StartDate); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertAudit
AFTER INSERT ON Audit
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.CollectionTypeId,',',New.EndDate,',',New.StartDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteAudit
AFTER DELETE ON Audit
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.CollectionTypeId,',',Old.EndDate,',',Old.StartDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Audit','Delete');
END$$

-- Authorities Trigger --
CREATE TRIGGER InsertAuthorities
AFTER INSERT ON Authorities
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Authorities','Insert');

CREATE TRIGGER UpdateAuthorities
AFTER UPDATE ON Authorities
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Authorities','Update');

CREATE TRIGGER DeleteAuthorities
AFTER DELETE ON Authorities
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Authorities','Delete');

-- BadCard Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBadCard 
AFTER UPDATE ON BadCard 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.AccountNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBadCard
AFTER INSERT ON BadCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.AccountNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBadCard
AFTER DELETE ON BadCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.AccountNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCard','Delete');
END$$

-- BadCreditCard Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBadCreditCard 
AFTER UPDATE ON BadCreditCard 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.CardHash); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBadCreditCard
AFTER INSERT ON BadCreditCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.CardHash);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBadCreditCard
AFTER DELETE ON BadCreditCard
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.CardHash);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BadCreditCard','Delete');
END$$

-- BatteryInfo Trigger--
DELIMITER $$
CREATE TRIGGER UpdateBatteryInfo 
AFTER UPDATE ON BatteryInfo 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertBatteryInfo
AFTER INSERT ON BatteryInfo
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteBatteryInfo
AFTER DELETE ON BatteryInfo
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'BatteryInfo','Delete');
END$$

-- CCFailLog Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCCFailLog 
AFTER UPDATE ON CCFailLog 
FOR EACH ROW 
BEGIN 
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CCFailLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCCFailLog
AFTER INSERT ON CCFailLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CCFailLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCCFailLog
AFTER DELETE ON CCFailLog
FOR EACH ROW
BEGIN
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CCFailLog','Delete');
END$$

-- CardRetryTransaction Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCardRetryTransaction 
AFTER UPDATE ON CardRetryTransaction 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.LastRetryDate,',',New.NumRetries,',',New.CardHash,',',New.TypeId,',',New.CardData,',',New.CardExpiry,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',', New.CreationDate,',',New.BadCardHash,',',New.IgnoreBadCard,',',New.LastResponseCode,',',New.IsRFID); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCardRetryTransaction
AFTER INSERT ON CardRetryTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCardRetryTransaction
AFTER DELETE ON CardRetryTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CardRetryTransaction','Delete');
END$$

-- CryptoKey Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCryptoKey 
AFTER UPDATE ON CryptoKey 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Type,',',New.KeyIndex); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCryptoKey
AFTER INSERT ON CryptoKey
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Type,',',New.KeyIndex);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCryptoKey
AFTER DELETE ON CryptoKey
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Type,',',Old.KeyIndex);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CryptoKey','Delete');
END$$

-- CustomerPermissions Trigger --
DELIMITER $$
CREATE TRIGGER UpdateCustomerPermissions 
AFTER UPDATE ON CustomerPermissions 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.PayByPhoneEnabled); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertCustomerPermissions
AFTER INSERT ON CustomerPermissions
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.PayByPhoneEnabled);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteCustomerPermissions
AFTER DELETE ON CustomerPermissions
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.PayByPhoneEnabled);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerPermissions','Delete');
END$$

--- CustomerWsCal Trigger --

CREATE TRIGGER InsertCustomerWsCal
AFTER INSERT ON CustomerWsCal
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsCal','Insert');

CREATE TRIGGER UpdateCustomerWsCal
AFTER UPDATE ON CustomerWsCal
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsCal','Update');

CREATE TRIGGER DeleteCustomerWsCal
AFTER DELETE ON CustomerWsCal
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CustomerWsCal','Delete');

--- CustomerWsToken Trigger --

CREATE TRIGGER InsertCustomerWsToken
AFTER INSERT ON CustomerWsToken
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsToken','Insert');

CREATE TRIGGER UpdateCustomerWsToken
AFTER UPDATE ON CustomerWsToken
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CustomerWsToken','Update');

CREATE TRIGGER DeleteCustomerWsToken
AFTER DELETE ON CustomerWsToken
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CustomerWsToken','Delete');

--- EMSExtensiblePermit Trigger --

CREATE TRIGGER InsertEMSExtensiblePermit
AFTER INSERT ON EMSExtensiblePermit
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.MobileNumber,'EMSExtensiblePermit','Insert');

CREATE TRIGGER UpdateEMSExtensiblePermit
AFTER UPDATE ON EMSExtensiblePermit
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.MobileNumber,'EMSExtensiblePermit','Update');

CREATE TRIGGER DeleteEMSExtensiblePermit
AFTER DELETE ON EMSExtensiblePermit
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.MobileNumber,'EMSExtensiblePermit','Delete');

--- EMSParkingPermission Trigger --

CREATE TRIGGER InsertEMSParkingPermission
AFTER INSERT ON EMSParkingPermission
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSParkingPermission','Insert');

CREATE TRIGGER UpdateEMSParkingPermission
AFTER UPDATE ON EMSParkingPermission
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSParkingPermission','Update');

CREATE TRIGGER DeleteEMSParkingPermission
AFTER DELETE ON EMSParkingPermission
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EMSParkingPermission','Delete');

-- Triggers for EMSParkingPermissionDayOfWeek --
DELIMITER $$
CREATE TRIGGER UpdateEMSParkingPermissionDayOfWeek 
AFTER UPDATE ON EMSParkingPermissionDayOfWeek 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.EMSPermissionId,',',New.DayOfWeek); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEMSParkingPermissionDayOfWeek
AFTER INSERT ON EMSParkingPermissionDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.EMSPermissionId,',',New.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSParkingPermissionDayOfWeek
AFTER DELETE ON EMSParkingPermissionDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.EMSPermissionId,',',Old.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSParkingPermissionDayOfWeek','Delete');
END$$

--- EMSRate Trigger --

CREATE TRIGGER InsertEMSRate
AFTER INSERT ON EMSRate
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSRate','Insert');

CREATE TRIGGER UpdateEMSRate
AFTER UPDATE ON EMSRate
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EMSRate','Update');

CREATE TRIGGER DeleteEMSRate
AFTER DELETE ON EMSRate
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EMSRate','Delete');

-- Triggers for EMSRateDayOfWeek --

DELIMITER $$
CREATE TRIGGER UpdateEMSRateDayOfWeek 
AFTER UPDATE ON EMSRateDayOfWeek 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.EMSRateId,',',New.DayOfWeek); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEMSRateDayOfWeek
AFTER INSERT ON EMSRateDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.EMSRateId,',',New.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEMSRateDayOfWeek
AFTER DELETE ON EMSRateDayOfWeek
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.EMSRateId,',',Old.DayOfWeek);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EMSRateDayOfWeek','Delete');
END$$

-- Trigger EMSProperties --
CREATE TRIGGER InsertEmsProperties
AFTER INSERT ON EmsProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'EmsProperties','Insert');

CREATE TRIGGER UpdateEmsProperties
AFTER UPDATE ON EmsProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'EmsProperties','Update');

CREATE TRIGGER DeleteEmsProperties
AFTER DELETE ON EmsProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'EmsProperties','Delete');

-- Triggers for EventLogNew --
DELIMITER $$
CREATE TRIGGER UpdateEventLogNew 
AFTER UPDATE ON EventLogNew 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.DeviceId,',',New.TypeId,',',New.ActionId,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventLogNew
AFTER INSERT ON EventLogNew
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.DeviceId,',',New.TypeId,',',New.ActionId,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventLogNew
AFTER DELETE ON EventLogNew
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.DeviceId,',',Old.TypeId,',',Old.ActionId,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventLogNew','Delete');
END$$

--- Triggers to EventNewAction --

CREATE TRIGGER InsertEventNewAction
AFTER INSERT ON EventNewAction
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewAction','Insert');

CREATE TRIGGER UpdateEventNewAction
AFTER UPDATE ON EventNewAction
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewAction','Update');

CREATE TRIGGER DeleteEventNewAction
AFTER DELETE ON EventNewAction
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewAction','Delete');

--- Triggers to RESTAccount --

CREATE TRIGGER InsertRESTAccount
AFTER INSERT ON RESTAccount
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTAccount','Insert');

CREATE TRIGGER UpdateRESTAccount
AFTER UPDATE ON RESTAccount
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTAccount','Update');

CREATE TRIGGER DeleteRESTAccount
AFTER DELETE ON RESTAccount
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.AccountName,'RESTAccount','Delete');

-- Triggers for EventNewDefinition --
DELIMITER $$
CREATE TRIGGER UpdateEventNewDefinition 
AFTER UPDATE ON EventNewDefinition 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.DeviceId,',',New.TypeId,',',New.ActionId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertEventNewDefinition
AFTER INSERT ON EventNewDefinition
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.DeviceId,',',New.TypeId,',',New.ActionId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteEventNewDefinition
AFTER DELETE ON EventNewDefinition
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.DeviceId,',',Old.TypeId,',',Old.ActionId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'EventNewDefinition','Delete');
END$$

CREATE TRIGGER InsertEventNewDevice
AFTER INSERT ON EventNewDevice
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewDevice','Insert');

CREATE TRIGGER UpdateEventNewDevice
AFTER UPDATE ON EventNewDevice
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewDevice','Update');

CREATE TRIGGER DeleteEventNewDevice
AFTER DELETE ON EventNewDevice
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewDevice','Delete');

CREATE TRIGGER InsertEventNewType
AFTER INSERT ON EventNewType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewType','Insert');

CREATE TRIGGER UpdateEventNewType
AFTER UPDATE ON EventNewType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'EventNewType','Update');

CREATE TRIGGER DeleteEventNewType
AFTER DELETE ON EventNewType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'EventNewType','Delete');

--- Triggers to LotSettingFileContent --

CREATE TRIGGER InsertLotSettingFileContent
AFTER INSERT ON LotSettingFileContent
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSettingFileContent','Insert');

CREATE TRIGGER UpdateLotSettingFileContent
AFTER UPDATE ON LotSettingFileContent
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'LotSettingFileContent','Update');

CREATE TRIGGER DeleteLotSettingFileContent
AFTER DELETE ON LotSettingFileContent
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'LotSettingFileContent','Delete');

--- Triggers to MerchantAccount --
DELIMITER $$
CREATE TRIGGER InsertMerchantAccount
AFTER INSERT ON MerchantAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'MerchantAccount','Insert');
END$$

DELIMITER $$
CREATE TRIGGER UpdateMerchantAccount
AFTER UPDATE ON MerchantAccount
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Id,',',New.version,',',New.CustomerId,',',New.Name,',',New.Field1,',',New.Field2,',',New.Field3,',',New.IsDeleted,',',New.ProcessorName,',',New.ReferenceCounter,',',New.Field4,',',New.Field5,',',New.Field6,',',New.IsValid);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'MerchantAccount','Update');
END$$

DELIMITER $$
CREATE TRIGGER DeleteMerchantAccount
AFTER DELETE ON MerchantAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'MerchantAccount','Delete');
END$$

--- Triggers to ModemSetting --

CREATE TRIGGER InsertModemSetting
AFTER INSERT ON ModemSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ModemSetting','Insert');

CREATE TRIGGER UpdateModemSetting
AFTER UPDATE ON ModemSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ModemSetting','Update');

CREATE TRIGGER DeleteModemSetting
AFTER DELETE ON ModemSetting
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'ModemSetting','Delete');

-- Triggers for PaystationAlert --
DELIMITER $$
CREATE TRIGGER UpdatePaystationAlert 
AFTER UPDATE ON PaystationAlert 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.AlertId,',',New.PaystationId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertPaystationAlert
AFTER INSERT ON PaystationAlert
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.AlertId,',',New.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystationAlert
AFTER DELETE ON PaystationAlert
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.AlertId,',',Old.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaystationAlert','Delete');
END$$

--- Triggers to PaystationGroup --

CREATE TRIGGER InsertPaystationGroup
AFTER INSERT ON PaystationGroup
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PaystationGroup','Insert');

CREATE TRIGGER UpdatePaystationGroup
AFTER UPDATE ON PaystationGroup
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PaystationGroup','Update');

CREATE TRIGGER DeletePaystationGroup
AFTER DELETE ON PaystationGroup
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PaystationGroup','Delete');

--- Triggers to PaystationHeartBeat --

CREATE TRIGGER InsertPaystationHeartBeat
AFTER INSERT ON PaystationHeartBeat
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'PaystationHeartBeat','Insert');

CREATE TRIGGER UpdatePaystationHeartBeat
AFTER UPDATE ON PaystationHeartBeat
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'PaystationHeartBeat','Update');

CREATE TRIGGER DeletePaystationHeartBeat
AFTER DELETE ON PaystationHeartBeat
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'PaystationHeartBeat','Delete');

--- Paystation Group --
DELIMITER $$
CREATE TRIGGER UpdatePaystation_PaystationGroup 
AFTER UPDATE ON Paystation_PaystationGroup 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationGroupId,',',New.PaystationId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertPaystation_PaystationGroup
AFTER INSERT ON Paystation_PaystationGroup
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationGroupId,',',New.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeletePaystation_PaystationGroup
AFTER DELETE ON Paystation_PaystationGroup
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationGroupId,',',Old.PaystationId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Paystation_PaystationGroup','Delete');
END$$

-- Triggers to create PreAuth --
CREATE TRIGGER InsertPreAuth
AFTER INSERT ON PreAuth
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuth','Insert');

CREATE TRIGGER UpdatePreAuth
AFTER UPDATE ON PreAuth
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuth','Update');

CREATE TRIGGER DeletePreAuth
AFTER DELETE ON PreAuth
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PreAuth','Delete');

--- Triggers to PreAuthHolding --

CREATE TRIGGER InsertPreAuthHolding
AFTER INSERT ON PreAuthHolding
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuthHolding','Insert');

CREATE TRIGGER UpdatePreAuthHolding
AFTER UPDATE ON PreAuthHolding
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'PreAuthHolding','Update');

CREATE TRIGGER DeletePreAuthHolding
AFTER DELETE ON PreAuthHolding
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'PreAuthHolding','Delete');

--- Triggers for Processor --

CREATE TRIGGER InsertProcessor
AFTER INSERT ON Processor
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'Processor','Insert');

CREATE TRIGGER UpdateProcessor
AFTER UPDATE ON Processor
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'Processor','Update');

CREATE TRIGGER DeleteProcessor
AFTER DELETE ON Processor
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'Processor','Delete');

CREATE TRIGGER InsertRESTActionType
AFTER INSERT ON RESTActionType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RESTActionType','Insert');

CREATE TRIGGER UpdateRESTActionType
AFTER UPDATE ON RESTActionType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RESTActionType','Update');

CREATE TRIGGER DeleteRESTActionType
AFTER DELETE ON RESTActionType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'RESTActionType','Delete');

DELIMITER $$
CREATE TRIGGER UpdateRESTLog 
AFTER UPDATE ON RESTLog 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.RESTAccountName,',',New.EndpointName,',',New.LoggingDate,',',New.IsError,',',New.CustomerId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTLog
AFTER INSERT ON RESTLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.RESTAccountName,',',New.EndpointName,',',New.LoggingDate,',',New.IsError,',',New.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTLog
AFTER DELETE ON RESTLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.RESTAccountName,',',Old.EndpointName,',',Old.LoggingDate,',',Old.IsError,',',Old.CustomerId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLog','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRESTLogTotalCall 
AFTER UPDATE ON RESTLogTotalCall 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.RESTAccountName,',',New.LoggingDate); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRESTLogTotalCall
AFTER INSERT ON RESTLogTotalCall
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.RESTAccountName,',',New.LoggingDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRESTLogTotalCall
AFTER DELETE ON RESTLogTotalCall
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.RESTAccountName,',',Old.LoggingDate);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RESTLogTotalCall','Delete');
END$$

CREATE TRIGGER InsertRESTProperties
AFTER INSERT ON RESTProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'RESTProperties','Insert');

CREATE TRIGGER UpdateRESTProperties
AFTER UPDATE ON RESTProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Name,'RESTProperties','Update');

CREATE TRIGGER DeleteRESTProperties
AFTER DELETE ON RESTProperties
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Name,'RESTProperties','Delete');

CREATE TRIGGER InsertRESTSessionToken
AFTER INSERT ON RESTSessionToken
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTSessionToken','Insert');

CREATE TRIGGER UpdateRESTSessionToken
AFTER UPDATE ON RESTSessionToken
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(New.AccountName,'RESTSessionToken','Update');

CREATE TRIGGER DeleteRESTSessionToken
AFTER DELETE ON RESTSessionToken
FOR EACH ROW
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Old.AccountName,'RESTSessionToken','Delete');

DELIMITER $$
CREATE TRIGGER UpdateRates 
AFTER UPDATE ON Rates 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRates
AFTER INSERT ON Rates
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRates
AFTER DELETE ON Rates
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Rates','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRawSensorData 
AFTER UPDATE ON RawSensorData 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorData','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRawSensorData
AFTER INSERT ON RawSensorData
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorData','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRawSensorData
AFTER DELETE ON RawSensorData
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.ServerName,',',Old.PaystationCommAddress,',',Old.Type,',',Old.Action,',',Old.Information,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorData','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateRawSensorDataArchive 
AFTER UPDATE ON RawSensorDataArchive 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertRawSensorDataArchive
AFTER INSERT ON RawSensorDataArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.ServerName,',',New.PaystationCommAddress,',',New.Type,',',New.Action,',',New.Information,',',New.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteRawSensorDataArchive
AFTER DELETE ON RawSensorDataArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.ServerName,',',Old.PaystationCommAddress,',',Old.Type,',',Old.Action,',',Old.Information,',',Old.DateField);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'RawSensorDataArchive','Delete');
END$$

CREATE TRIGGER InsertRegionPaystationLog
AFTER INSERT ON RegionPaystationLog
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RegionPaystationLog','Insert');

CREATE TRIGGER UpdateRegionPaystationLog
AFTER UPDATE ON RegionPaystationLog
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'RegionPaystationLog','Update');

CREATE TRIGGER DeleteRegionPaystationLog
AFTER DELETE ON RegionPaystationLog
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'RegionPaystationLog','Delete');

DELIMITER $$
CREATE TRIGGER UpdateReplenish 
AFTER UPDATE ON Replenish 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.Date,',',New.Number); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertReplenish
AFTER INSERT ON Replenish
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.Date,',',New.Number);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteReplenish
AFTER DELETE ON Replenish
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.Date,',',Old.Number);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Replenish','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateReversal 
AFTER UPDATE ON Reversal 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertReversal
AFTER INSERT ON Reversal
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteReversal
AFTER DELETE ON Reversal
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.MerchantAccountId,',',Old.OriginalReferenceNumber,',',Old.OriginalTime);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Reversal','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateReversalArchive 
AFTER UPDATE ON ReversalArchive 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertReversalArchive
AFTER INSERT ON ReversalArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.MerchantAccountId,',',New.OriginalReferenceNumber,',',New.OriginalTime);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteReversalArchive
AFTER DELETE ON ReversalArchive
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.MerchantAccountId,',',Old.OriginalReferenceNumber,',',Old.OriginalTime);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ReversalArchive','Delete');
END$$

CREATE TRIGGER InsertRole
AFTER INSERT ON Role
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Role','Insert');

CREATE TRIGGER UpdateRole
AFTER UPDATE ON Role
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Role','Update');

CREATE TRIGGER DeleteRole
AFTER DELETE ON Role
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Role','Delete');

DELIMITER $$
CREATE TRIGGER UpdateSCRetry 
AFTER UPDATE ON SCRetry 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSCRetry
AFTER INSERT ON SCRetry
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSCRetry
AFTER DELETE ON SCRetry
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SCRetry','Delete');
END$$


CREATE TRIGGER InsertSMSAlert
AFTER INSERT ON SMSAlert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.MobileNumber,'SMSAlert','Insert');

CREATE TRIGGER UpdateSMSAlert
AFTER UPDATE ON SMSAlert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.MobileNumber,'SMSAlert','Update');

CREATE TRIGGER DeleteSMSAlert
AFTER DELETE ON SMSAlert
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.MobileNumber,'SMSAlert','Delete');

DELIMITER $$
CREATE TRIGGER UpdateSMSFailedResponse 
AFTER UPDATE ON SMSFailedResponse 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSFailedResponse
AFTER INSERT ON SMSFailedResponse
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSMSFailedResponse
AFTER DELETE ON SMSFailedResponse
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSFailedResponse','Delete');
END$$


CREATE TRIGGER InsertSMSMessageType
AFTER INSERT ON SMSMessageType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'SMSMessageType','Insert');

CREATE TRIGGER UpdateSMSMessageType
AFTER UPDATE ON SMSMessageType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'SMSMessageType','Update');

CREATE TRIGGER DeleteSMSMessageType
AFTER DELETE ON SMSMessageType
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'SMSMessageType','Delete');

DELIMITER $$
CREATE TRIGGER UpdateSMSTransactionLog 
AFTER UPDATE ON SMSTransactionLog 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertSMSTransactionLog
AFTER INSERT ON SMSTransactionLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteSMSTransactionLog
AFTER DELETE ON SMSTransactionLog
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.SMSMessageTypeId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SMSTransactionLog','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateServiceAgreedCustomer 
AFTER UPDATE ON ServiceAgreedCustomer 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.ServiceAgreementId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertServiceAgreedCustomer
AFTER INSERT ON ServiceAgreedCustomer
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.CustomerId,',',New.ServiceAgreementId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteServiceAgreedCustomer
AFTER DELETE ON ServiceAgreedCustomer
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.CustomerId,',',Old.ServiceAgreementId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ServiceAgreedCustomer','Delete');
END$$

CREATE TRIGGER InsertServiceAgreement
AFTER INSERT ON ServiceAgreement
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'ServiceAgreement','Insert');

CREATE TRIGGER UpdateServiceAgreement
AFTER UPDATE ON ServiceAgreement
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'ServiceAgreement','Update');

CREATE TRIGGER DeleteServiceAgreement
AFTER DELETE ON ServiceAgreement
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'ServiceAgreement','Delete');

CREATE TRIGGER InsertServiceState
AFTER INSERT ON ServiceState
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ServiceState','Insert');

CREATE TRIGGER UpdateServiceState
AFTER UPDATE ON ServiceState
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.PaystationId,'ServiceState','Update');

CREATE TRIGGER DeleteServiceState
AFTER DELETE ON ServiceState
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.PaystationId,'ServiceState','Delete');

CREATE TRIGGER InsertTaxes
AFTER INSERT ON Taxes
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Taxes','Insert');

CREATE TRIGGER UpdateTaxes
AFTER UPDATE ON Taxes
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'Taxes','Update');

CREATE TRIGGER DeleteTaxes
AFTER DELETE ON Taxes
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'Taxes','Delete');

DELIMITER $$
CREATE TRIGGER UpdateTransaction
AFTER UPDATE ON Transaction
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.CustomerId,',',New.PaystationId,',',New.TicketNumber,',',New.LotNumber,',',New.StallNumber,',',New.TypeId,',',New.PurchasedDate,',',New.ExpiryDate,',',New.ChargedAmount,',',New.CashPaid,',',New.CardPaid,',',New.ChangeDispensed,',',New.ExcessPayment,',',New.IsRefundSlip,',',New.CustomCardData,',',New.CustomCardType,',',New.CouponNumber,',',New.CitationNumber,',',New.SmartCardData,',',New.SmartCardPaid,',',New.PaymentTypeId,',',New.CreationDate,',',New.RateId,',',New.AuthenticationCard,',',New.RegionId,',',New.AddTimeNum,',',New.OriginalAmount,',',New.IsOffline,',',New.PlateNumber); 
IF (New.CustomCardData<>'NULL' or New.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Update');
END IF;
IF(New.SmartCardData<>'NULL' or New.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Update');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Update');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Update');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransaction
AFTER INSERT ON Transaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
DECLARE CustomCardLength int(20);
set Id = concat(New.CustomerId,',',New.PaystationId,',',New.TicketNumber,',',New.LotNumber,',',New.StallNumber,',',New.TypeId,',',New.PurchasedDate,',',New.ExpiryDate,',',New.ChargedAmount,',',New.CashPaid,',',New.CardPaid,',',New.ChangeDispensed,',',New.ExcessPayment,',',New.IsRefundSlip,',',New.CustomCardData,',',New.CustomCardType,',',New.CouponNumber,',',New.CitationNumber,',',New.SmartCardData,',',New.SmartCardPaid,',',New.PaymentTypeId,',',New.CreationDate,',',New.RateId,',',New.AuthenticationCard,',',New.RegionId,',',New.AddTimeNum,',',New.OriginalAmount,',',New.IsOffline,',',New.PlateNumber);
IF (New.CustomCardData<>'NULL' or New.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Insert');
END IF;
IF(New.SmartCardData<>'NULL' or New.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Insert');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Insert');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Insert');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransaction
AFTER DELETE ON Transaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(Old.CustomerId,',',Old.PaystationId,',',Old.TicketNumber,',',Old.LotNumber,',',Old.StallNumber,',',Old.TypeId,',',Old.PurchasedDate,',',Old.ExpiryDate,',',Old.ChargedAmount,',',Old.CashPaid,',',Old.CardPaid,',',Old.ChangeDispensed,',',Old.ExcessPayment,',',Old.IsRefundSlip,',',Old.CustomCardData,',',Old.CustomCardType,',',Old.CouponNumber,',',Old.CitationNumber,',',Old.SmartCardData,',',Old.SmartCardPaid,',',Old.PaymentTypeId,',',Old.CreationDate,',',Old.RateId,',',Old.AuthenticationCard,',',Old.RegionId,',',Old.AddTimeNum,',',Old.OriginalAmount,',',Old.IsOffline,',',Old.PlateNumber);
IF (Old.CustomCardData<>'NULL' or Old.CustomCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'CustomerCard','Delete');
END IF;
IF(Old.SmartCardData<>'NULL' or Old.SmartCardData<>0) THEN
	INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'SmartCard','Delete');
END IF;
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Purchase','Delete');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Permit','Delete');
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'PaymentCard','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransactionCollection 
AFTER UPDATE ON TransactionCollection 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransactionCollection
AFTER INSERT ON TransactionCollection
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransactionCollection
AFTER DELETE ON TransactionCollection
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id = concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionCollection','Delete');
END$$

CREATE TRIGGER InsertUserAccount
AFTER INSERT ON UserAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'UserAccount','Insert');

CREATE TRIGGER UpdateUserAccount
AFTER UPDATE ON UserAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'UserAccount','Update');

CREATE TRIGGER DeleteUserAccount
AFTER DELETE ON UserAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'UserAccount','Delete');

CREATE TRIGGER InsertAuditAccess
AFTER INSERT ON AuditAccess
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'AuditAccess','Insert');

CREATE TRIGGER UpdateAuditAccess
AFTER UPDATE ON AuditAccess
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'AuditAccess','Update');

CREATE TRIGGER DeleteAuditAccess
AFTER DELETE ON AuditAccess
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'AuditAccess','Delete');

CREATE TRIGGER InsertCerberusAccount
AFTER INSERT ON CerberusAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusAccount','Insert');

CREATE TRIGGER UpdateCerberusAccount
AFTER UPDATE ON CerberusAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(New.Id,'CerberusAccount','Update');

CREATE TRIGGER DeleteCerberusAccount
AFTER DELETE ON CerberusAccount
FOR EACH ROW
INSERT INTO MigrationQueue(EMS6Id,TableName,Action) values(Old.Id,'CerberusAccount','Delete');


-- ProcessorProperties Trigger --

DELIMITER $$
CREATE TRIGGER UpdateProcessorProperties 
AFTER UPDATE ON ProcessorProperties 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.Processor,',',New.Name); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertProcessorProperties
AFTER INSERT ON ProcessorProperties
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.Processor,',',New.Name);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteProcessorProperties
AFTER DELETE ON ProcessorProperties
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.Processor,',',Old.Name);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorProperties','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransaction2Push 
AFTER UPDATE ON Transaction2Push 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.ThirdPartyServiceAccountId); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransaction2Push
AFTER INSERT ON Transaction2Push
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber,',',New.ThirdPartyServiceAccountId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransaction2Push
AFTER DELETE ON Transaction2Push
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber,',',Old.ThirdPartyServiceAccountId);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'Transaction2Push','Delete');
END$$

DELIMITER $$
CREATE TRIGGER UpdateTransactionPush 
AFTER UPDATE ON TransactionPush 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertTransactionPush
AFTER INSERT ON TransactionPush
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.PaystationId,',',New.PurchasedDate,',',New.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteTransactionPush
AFTER DELETE ON TransactionPush
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.PaystationId,',',Old.PurchasedDate,',',Old.TicketNumber);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'TransactionPush','Delete');
END$$

-- Trigger to insert a record into PurchaseCollection for incremental migration --
DELIMITER $$
CREATE TRIGGER InsertPurchase
AFTER INSERT ON Purchase
FOR EACH ROW
BEGIN
INSERT INTO PurchaseCollection(PurchaseId,PointOfSaleId,PurchaseGMT,CashAmount,CoinAmount,CoinCount,BillAmount,BillCount) values(New.Id,New.PointOfSaleId,New.PurchaseGMT,New.CashPaidAmount,New.CoinPaidAmount,New.CoinCount,New.BillPaidAmount,New.BillCount);
END$$

-- ProcessorTransaction Trigger --
DELIMITER $$
CREATE TRIGGER UpdateProcessorTransaction 
AFTER UPDATE ON ProcessorTransaction 
FOR EACH ROW 
BEGIN 
DECLARE Id VARCHAR(255); 
set Id = concat(New.TypeId,',',New.PaystationId,',',New.MerchantAccountId,',',New.TicketNumber,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',',New.CardChecksum,',',New.PurchasedDate,',',New.ProcessingDate,',',New.ProcessorTransactionId,',',New.AuthorizationNumber,',',New.ReferenceNumber,',',New.Approved,',',New.CardHash,',',New.IsUploadedFromBoss,',',New.IsRFID); 
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Update');
END$$

DELIMITER $$
CREATE TRIGGER InsertProcessorTransaction
AFTER INSERT ON ProcessorTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(New.TypeId,',',New.PaystationId,',',New.MerchantAccountId,',',New.TicketNumber,',',New.Amount,',',New.CardType,',',New.Last4DigitsOfCardNumber,',',New.CardChecksum,',',New.PurchasedDate,',',New.ProcessingDate,',',New.ProcessorTransactionId,',',New.AuthorizationNumber,',',New.ReferenceNumber,',',New.Approved,',',New.CardHash,',',New.IsUploadedFromBoss,',',New.IsRFID);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Insert');
END$$

DELIMITER $$
CREATE TRIGGER DeleteProcessorTransaction
AFTER DELETE ON ProcessorTransaction
FOR EACH ROW
BEGIN
DECLARE Id varchar(255);
set Id=concat(Old.TypeId,',',Old.PaystationId,',',Old.MerchantAccountId,',',Old.TicketNumber,',',Old.Amount,',',Old.CardType,',',Old.Last4DigitsOfCardNumber,',',Old.CardChecksum,',',Old.PurchasedDate,',',Old.ProcessingDate,',',Old.ProcessorTransactionId,',',Old.AuthorizationNumber,',',Old.ReferenceNumber,',',Old.Approved,',',Old.CardHash,',',Old.IsUploadedFromBoss,',',Old.IsRFID);
INSERT INTO MigrationQueue(MultiKeyId,TableName,Action) values(Id,'ProcessorTransaction','Delete');
END$$
