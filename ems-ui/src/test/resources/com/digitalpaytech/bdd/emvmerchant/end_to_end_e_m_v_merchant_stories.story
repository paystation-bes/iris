Narrative:
In order to test the emv merchant framework 
As a developer
I want to ensure that queue process and events are handled correctly

Scenario: Successfully add an emv merchant account to pay station 
& have pay station notify iris successfully
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a merchant account where the  
name is TestCreditCall
merchant account status is enabled
customer is Mackenzie Parking
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
terminal token is abcdefg
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
location is Downtown 
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is not linux
credit card merchant account is TestCreditCall
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 1 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCreditCall
When iris receives an incoming transaction message where the
serial number is 500000070001
ticket number is 1
lot number is Student Parking
license plate number is ABC123
stall number is 1
type is Regular
purchase date is 2016:03:01:22:10:36:GMT
parking ticket purchased is 60
original amount is 200
charged amount is 2.00
coupon number is left blank
cash paid is 0.00
card paid is 2.00
card authorization id is 12345678
authorization number is 12345678
is not refund slip
expiry time is 2016:03:01:23:10:36:GMT
rate name is early bird
rate id is 1
rate value is 400
tax type is NA
Then pay station with message number 1 will contain EMVMerchantAccountNotification
When iris receives an incoming emv merchant account message with message number 2 where the
serial number is 500000070001
Then pay station with message number 2 will respond MerchantID tag with a value of 1
And there exists a message number 2 with MerchantInfo tag where the merchantProcessor is Elavon
And there exists a message number 2 with MerchantInfo tag where the transactionKey is TRANSACTION
And there exists a message number 2 with MerchantInfo tag where the terminalId is 11223344
And there exists a message number 2 with MerchantInfo tag where the merchantId is merchantID
And there exists a message number 2 with MerchantInfo tag where the currency is CAD
And there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account uploaded gmt is null
When iris receives an incoming emv merchant account success message with message number 3 where the
serial number is 500000070001
Then pay station with message number 3 will contain Acknowledge