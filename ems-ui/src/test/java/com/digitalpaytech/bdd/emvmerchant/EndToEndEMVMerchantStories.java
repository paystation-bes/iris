package com.digitalpaytech.bdd.emvmerchant;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminpaystationlistdetailcontroller.TestSavePOS;
import com.digitalpaytech.bdd.rpc.ps2.PaystationCommunicationSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.rpc.ps2.EMVMerchantAccountHandler;
import com.digitalpaytech.rpc.ps2.EMVMerchantAccountSuccessHandler;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class EndToEndEMVMerchantStories extends AbstractStories {
    
    public EndToEndEMVMerchantStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        this.testHandlers.registerTestHandler("paystationsave", TestSavePOS.class);
        TestContext
                .getInstance()
                .getObjectParser()
                .register(new Class[] { EMVMerchantAccountHandler.class, EMVMerchantAccountSuccessHandler.class, Customer.class, PointOfSale.class,
                    CardType.class, MerchantAccount.class, SysAdminPayStationEditForm.class, TransactionDto.class, PaystationSetting.class,
                    Purchase.class, CardRetryTransaction.class, CcFailLog.class, ProcessorTransaction.class, Permit.class,
                    PosServiceState.class, });
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new PaystationCommunicationSteps(testHandlers),
                new ControllerBaseSteps(this.testHandlers));
    }
    
}
