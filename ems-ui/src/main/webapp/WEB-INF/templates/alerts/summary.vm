#* velocity document *#
##
#parse( "/WEB-INF/templates/include/setup.vm" )
##
#set($helpLink = "#springMessage('help.link.AlertCentre')")##
##
#set($pageSec = "alerts")##
##
<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gt IE 8)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->


##
<head>
	<title>#springMessage('label.siteName') - #springMessage('heading.alerts') : #springMessage('label.collections.alert.payStationSummary')</title>
#parse( "${dirPrefix}include/head.vm" )
##
<link rel="stylesheet" type="text/css" media="all" href="${styleLocal}collections_alerts_cntr.css${staticQuerry}">
<link rel="stylesheet" href="${styleLocal}leaflet.css${staticQuerry}" />
<script src="${scriptLocal}leaflet.js${staticQuerry}"></script>
<!--[if lte IE 8]><link rel="stylesheet" href="${styleLocal}MarkerCluster.Default.ie.css${staticQuerry}" /><![endif]-->
<script src="${scriptLocal}leaflet.markercluster-src.js${staticQuerry}"></script>
##
</head>
##
<body id="settings">
##
#parse( "${dirPrefix}include/generalHead.vm" )
##
<section id="content">
##
#parse( "${dirPrefix}include/mainNav.vm" )
##
	<section id="mainContent">
##
#set($curLocation = "$!{params.locationID}")##
#set($curRoute = "$!{params.routeID}")##
#set($crntSection = "#springMessage('heading.alerts')")
#set($crntPage = "#springMessage('heading.alerts')")
#set($sectionTitle = "#springMessage('heading.alerts')")
##
		<h1>${sectionTitle}</h1>
		<section id="secondNav">
			<section id="secondNavArea">
				<nav>
					<div class="tabSection">
						<a href="/secure/alerts/index.html?" id="activeListTab" class="tab" title="#springMessage('button.alerts.currentAlerts')">#springMessage('button.alerts.currentAlerts')</a>
					</div>
					<div class="tabSection">
						<a href="/secure/alerts/index.html?pgActn=recentList" id="recentListTab" class="tab" title="#springMessage('button.alerts.resolvedAlerts')">#springMessage('button.alerts.resolvedAlerts')</a>
					</div>
					<div class="tabSection current">
						<a href="#" class="tab" title="#springMessage('label.collections.alert.payStationSummary')">#springMessage('label.collections.alert.payStationSummary')</a>
					</div>
				</nav>
			</section>
		</section>
		<section class="filterHeader searchHeader horizontal">
			<section class="title">
				<img title="filter" src="${imgLocal}icn_FilterLrg.png" width="22" height="20">## #springMessage('label.allCaps.filter'):
			</section>
			#springBind("alertsForm.*")##
			<form name="posSummaryFilterForm" id="posSummaryFilterForm" action="#" method="post">
				#springBind("alertsForm.postToken")##
				<input type="hidden" id="postToken" name="${status.expression}" value="${alertsForm.initToken}"/>
				<ul class="filterForm">
					<li class="locationRouteField">
						<section class="filterTitle">
							<label for="filterLocationRoute">#springMessage('label.route')/#springMessage('label.location')</label>
						</section>
						
						<section class="autoComplete">
						#springBind("alertsForm.wrappedObject.randomId")##
						<input id="filterLocationRouteVal" type="hidden" name="${status.expression}" value="$!{status.value}"/>
						<input id="filterLocationRoute" type="text" autocomplete="off" class="autoText flatRight dateType stndrdChrctr" />
						<a href="#" id="filterLocationRouteExpand" class="linkButtonIcn selectMenu flatLeft" title="#springMessage('button.showOptions')"><img width="18" height="1" title="arrow" alt="arrow" src="${imgLocal}spacer.gif" border="0"></a>
					
						#set($localCount = 0)
						<span id="searchLocationData" style="display: none;">
							[{"value": "all", "label": "#springMessage('label.settings.locations.allPayStations')"}##
#*							*##foreach($route in $routeFilter)##
#*							*#,{ "value": "$!{route.randomId}", "label": "$!esc.javascript($!{route.name})", "category": "#springMessage('label.route')" }##
#*							*##end
#*							*##foreach($location in $locationFilter)##
#*							*##if($location.isParent == false)##
#*							*#, {##
#*							*##if($location.parentName && $location.parentName != "") "value": "${location.randomId}", "label": "$!esc.javascript($!{location.name})", "desc": "$!esc.javascript($!{location.parentName})", "category": "#springMessage('label.location')" }##
#*							*##else "value": "$!{location.randomId}", "label": "$!esc.javascript($!{location.name})", "category": "#springMessage('label.location')" }##
#*							*##end##
#*							*##end##
#*							*##end]
						</span>
						</section>
					</li>
					<li>
						#springBind("alertsForm.wrappedObject.severityRandomId")##
						<input id="filterSeverity" type="hidden" name="${status.expression}" value="$!{status.value}"/>
						<section class="filterTitle">
							<label for="filterSeverity">#springMessage('label.settings.alerts.severity')</label>
							<section id="filterSeverityLabel" class="logValue"></section>
						</section>
						<section class="filterMenu">
							<ul id="severityList">
								<li id="-1" class="clickable">#springMessage('label.all')</li>
							#foreach($severity in $severityFilter)##
								<li id="$severity.randomId" class="clickable">$severity.name</li>
							#end##
							</ul>
						</section>
					</li>
				</ul>
			</form>
			</section>

		<section id="Maintenance" class="layout1 layout sectionContent">
			<section class="column first">
				<section class="groupBox">
					<header>
						<h2>#springMessage('label.alert.payStationAlertSummary')</h2>
					</header>
					<ul id="posSummaryListHeader" class="scrollingListHeader alertSummary">
						<li>
							<div class="col1 clickable current sort" name="sort_pointOfSaleName" sort="pointOfSaleName:DESC" colTitle="#springMessage('label.payStations')" tooltip="#springMessage('label.payStations') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.payStations') <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
							<div class="col2 clickable sort" name="sort_routeName" sort="routeName:ASC" colTitle="#springMessage('label.settings.routes.Routes')" tooltip="#springMessage('label.settings.routes.Routes') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.settings.routes.Routes')  <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
							<div class="col3 clickable sort" name="sort_locationName" sort="locationName:ASC" colTitle="#springMessage('label.locations')" tooltip="#springMessage('label.locations') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.locations') <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
							<div class="col4 clickable sort" name="sort_lastSeen" sort="lastSeen:ASC" colTitle="#springMessage('label.lastSeen')" tooltip="#springMessage('label.lastSeen') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.lastSeen') <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
							<div class="col5 clickable sort" name="sort_batteryVolt" sort="batteryVolt:ASC" colTitle="#springMessage('label.alert.paystationListSummary.batteryVoltage')" tooltip="#springMessage('label.alert.paystationListSummary.batteryVoltage') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.alert.paystationListSummary.batteryVoltage') <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
							<div class="col6 clickable sort" name="sort_paperStatus" sort="paperStatus:ASC" colTitle="#springMessage('label.alert.paystationListSummary.paperStatus')" tooltip="#springMessage('label.alert.paystationListSummary.paperStatus') : #springMessage('label.sorting.ascendingMsg')"><p>#springMessage('label.alert.paystationListSummary.paperStatus') <img src="${imgLocal}icn_ArwDown.png" class="sortIcn" alt="sort" /></p></div>
						</li>
					</ul>
					<ul id="posSummaryList" class="setMaxHeight scrollingList alertSummary listItems">
						<li class="loadItem" class="hide">#parse("${dirPrefix}include/loading.vm")</li>
					</ul>
					<span id="posSummaryListTemplate" class="hide">
						<li>
						<section class="[%= (typeof payStationSeverity !== 'undefined')? psAlerts[payStationSeverity].listClass : psAlerts[0].listClass %]">
							<div class='col1'><p>#if($authtools.allow('customer.payStations'))<a href="/secure/settings/locations/payStationList.html?pgActn=display&psID=[%= posRandomId %]">#end[%= posName %]#if($authtools.allow('customer.payStations'))</a>#end</p></div>
							<div class='col2'>
							[% if(typeof routeName !== 'undefined' && routeName !== ''){ if(routeName === '#springMessage('label.multiple')'){ %] <p><strong style="color: #004B70;" tooltip="#springMessage('label.collections.payStationAlertMultipleRoutes')">[%= routeName %]</strong></p>
							[% }else{ %] #if($authtools.allow('routes'))
								<p><a href="/secure/settings/routes/index.html?pgActn=display&itemValue=[%= routeRandomId %]">#end[%= routeName %]#if($authtools.allow('routes'))</a></p>#end 
							[% } }else{ %] <p>#springMessage('label.collections.alert.noRouteAssigned')</p> [% } %]
							</div>
							<div class='col3'><p>#if($authtools.allow('locations'))<a href="/secure/settings/locations/locationDetails.html?&pgActn=display&filterValue=loc_[%= locationRandomId %]">#end[%= locationName %]#if($authtools.allow('locations'))</a>#end</p></div>
							<div class='col4'><p class="[%= (typeof lastSeenSeverity !== 'undefined')? psAlerts[lastSeenSeverity].listClass : psAlerts[0].listClass %]">[%= lastSeen %]</p></div>
							<div class='col5'><p class="[%= (typeof batteryLevel !== 'undefined')? psAlerts[batteryLevel].listClass : psAlerts[0].listClass %]">[%= (batteryVoltage === 32767)? 'N/A' : batteryVoltage+'V' %]</p></div>
							<div class='col6'><p class="[%= (typeof paperLevel !== 'undefined')? psAlerts[paperLevel].listClass : psAlerts[0].listClass %]">[%= psAlerts[paperLevel].pprTitle %]</p></div>
						</section>
						</li>
					</span>
				</section>
			</section>
		</section>
	</section>
	<hr>
</section>##
#parse( "${dirPrefix}include/footer.vm")##
##
#parse("${dirPrefix}include/alertMsg.vm")##
## 
<script type="text/javascript" language="javascript">#parse("${dirPrefix}include/sessionActivityVar.vm")##
#parse( "${dirPrefix}include/standardJSMessages.vm" )##
##
noPaystationsMsg = "#springMessage('alert.noPayStationAlertsMsg')";##
##
$(document).ready(function(){ standard(); sessionActvty(); posSummary(); soh(["posSummaryList"]); });</script>
<script type="text/javascript" language="javascript" src="${scriptLocal}jquery.ui.paginatetab.js${staticQuerry}"></script>
<script type="text/javascript" language="javascript" src="${scriptLocal}collections_alerts_cntr.js${staticQuerry}"></script>
##
</body>
</html>