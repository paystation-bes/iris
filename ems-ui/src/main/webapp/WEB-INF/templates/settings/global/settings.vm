#* velocity document *#
##
#parse( "/WEB-INF/templates/include/setup.vm" )
##
#set($helpLink = "#springMessage('help.link.Settings')")##
##
#set($pageSec = "settings")##
##
<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gt IE 8)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->


##
<head>
	<title>#springMessage('label.siteName') - #springMessage('heading.settings') - #springMessage('heading.settings.global') : #springMessage('link.nav.settings.global.settings')</title>
#parse( "${dirPrefix}include/head.vm" )
##
<link rel="stylesheet" type="text/css" media="all" href="${styleLocal}settings.css${staticQuerry}">
##
</head>
##
<body id="settings">
##
#parse( "${dirPrefix}include/generalHead.vm" )
##
<section id="content">
##
#parse( "${dirPrefix}include/mainNav.vm" )
##
	<section id="mainContent">
##
#set($crntSection = "#springMessage('link.nav.settings.global')")
#set($crntPage = "#springMessage('link.nav.settings.global.settings')")
#set($sectionTitle = "#springMessage('heading.settings')")
#parse( "${dirPrefix}settings/include/navSetup.vm" )
#parse( "${dirPrefix}settings/include/subNavSetup.vm" )
#parse( "${dirPrefix}include/sectionNav.vm" )
#parse( "${dirPrefix}include/subSectionNav.vm" )
## Global > Settings
	#if($authtools.allow('customer.settings'))##
		<section class="layout4 layout sectionContent">
			<section class="column first">
				## #globalPreferences 
				<section id="globalPreferences" class="groupBox">
					<header><h2>#springMessage('label.settings.globalPreferences')</h2></header>
					<section class="groupBoxBody">
						<h3>#springMessage('label.settings.globalPref.registrationInfo')</h3>
						<dl class="details">
							<dt class="detailLabel">#springMessage('label.settings.globalPref.regInfo.legalName'):</dt>
							<dd class="detailValue">${GlobalConfiguration.legalName}</dd>
							<dt class="detailLabel">#springMessage('label.settings.globalPref.regInfo.activationDate'):</dt>
							<dd class="detailValue">$date.format("MMM. dd, yyyy", $GlobalConfiguration.activationDate)</dd>
						</dl>
						
						<hr class="divider" /> 
						
						#springBind("globalPreferencesEditForm.*")##
						<form id="timeZoneEditForm" name="timeZoneEditForm" method="post" enctype="multipart/form-data" action="">

						<input type="hidden" name="sessionToken" value="$!{sessionToken}"/>
							#springBind("globalPreferencesEditForm.postToken")##
							<input type="hidden" name="postToken" id="postToken" value="$!{globalPreferencesEditForm.initToken}"/>
							
							#parse("${dirPrefix}settings/include/timeZoneForm.vm")
							#if(!${sec.principal.isParent})##
								#parse("${dirPrefix}settings/include/querySpaceByForm.vm")
							#end##
						</form>

						#if(!${sec.principal.isParent})##
							#parse("${dirPrefix}settings/include/jurisdictionTypeForm.vm")
						#end##

#if($authtools.allow('customer.flexCredentials'))##
						<hr class="divider" />
						<h3>
							#springMessage('label.settings.globalPref.flexCredentials')##
						#if($authtools.allow('customer.flexCredentials.manage'))##
							<a id="btnEditFlexCredentials" href="#" class="linkButtonIcn edit right" title="#springMessage('button.edit')"><img width="19" height="1" border="0" title="#springMessage('button.edit')" alt="#springMessage('button.edit')" src="${imgLocal}spacer.gif" border="0"></a>
						#end
						</h3>
						<section id="flexCredentialsFormArea" class="hide">
						#parse("${dirPrefix}include/flexCredentialForm.vm")	
						</section>
						#set($testCredentialID = "flexTestButton")
						#parse("${dirPrefix}include/testFlex.vm")
#end

#if($authtools.allow('customer.caseSubscription'))##
						<hr class="divider" />
						<h3 style="font-weight: normal;">#springMessage('label.settings.globalPref.caseCredentials')</h3>
						#parse("${dirPrefix}settings/include/caseCustomerDetails.vm")##				
#end
						<hr class="divider" />
						
						<h3>#springMessage('label.settings.globalPref.serviceSubscriptions')</h3>
						<ul class="listStyle details">							
					#foreach($service in $GlobalConfiguration.subscribedServices)##
						#if($service.isEnabled == true)##
							<li tooltip="$service.subscriptionType.name #springMessage('label.is') #springMessage('label.activated')" class="activatedService toolTip">##
						#else##
							<li tooltip="$service.subscriptionType.name #springMessage('label.is') #springMessage('label.available')" class="availableService toolTip">##
						#end##
								$service.subscriptionType.name
						#if($service.licenseCount && !$sec.principal.isParent)
								<span class="right toolTip" tooltip="#springArgMessage('label.settings.globalPref.licensesUsedFor', ["${service.licenseUsed}", "${service.licenseCount}", "${service.subscriptionType.name}"])">${service.licenseUsed}/${service.licenseCount}</span>
						#end
							</li>
					#end##
						</ul>
					</section>
				</section>## /#globalPreferences
			</section>
			<section class="column second">
	#else##
		<section class="layout5 layout sectionContent">
			<section class="column first">
	#end##
		#if($authtools.allow('customer.digitalAPI'))##
				## <!-- #webServices -->
				<section id="webServices" class="groupBox">
					<header><h2>#springMessage('label.settings.globalPref.webServices')</h2></header>
					<section class="groupBoxBody">
#if($authtools.allow('customer.digitalAPI.read'))##
						<h3>#springMessage('label.settings.globalPref.webServiceEndpoint')</h3>
						<table class="tabular">
							<thead>
								<tr class="notclickable">
									<th><span>#springMessage('label.settings.globalPref.endPointType')</span></th>
									<th><span>#springMessage('label.settings.globalPref.token')</span></th>
								</tr>
							</thead>
							<tbody class="details">
							#set($webServiceStatus = false)
							#foreach($endPoint in $GlobalConfiguration.webServiceEndPointList)  #set($webServiceStatus = true)
								<tr #if($endPoint.comments && $endPoint.comments != "")comment="$endPoint.comments"#end >
									<td>$endPoint.type</td>
									<td class="fixedWidthFont">$endPoint.key</td>
								</tr>
							#end
							#if($webServiceStatus != true)
								<tr>
									<td class="detailLabel" align="center" colspan="2">#springMessage('label.settings.globalPref.noWebServices')</td>
								</tr>
							#end
							</tbody>
						</table>
	#if($authtools.allow('customer.digitalAPI.write') || $authtools.allow('customer.digitalAPI.XChange'))##
						<hr class="space" />
	#end##

#end##

#if($authtools.allow('customer.digitalAPI.write'))##
						<h3>#springMessage('label.settings.globalPref.restApiAccount')</h3>
						<table class="tabular">
							<thead>
								<tr class="notclickable">
									<th><span>#springMessage('label.settings.globalPref.accountType')</span></th>
									<th><span>#springMessage('label.settings.globalPref.accountName')</span></th>
									<th><span>#springMessage('label.settings.globalPref.key')</span></th>
									<th><span>#springMessage('label.settings.globalPref.virtualPS')</span></th>
								</tr>
							</thead>
							<tbody class="details">
							#set($restAPIStatus = false)
							#foreach($restApi in $GlobalConfiguration.restAccountList) #set($restAPIStatus = true)
								<tr #if($restApi.comments && $restApi.comments != "")comment="$restApi.comments"#end >
									<td>${restApi.type}</td>
									<td>$restApi.name</td>
									<td class="fixedWidthFont">$restApi.key</td>
									<td>$restApi.virtualPS</td>
								</tr>
							#end
							#if($restAPIStatus != true)
								<tr>
									<td class="detailLabel" align="center" colspan="4">#springMessage('label.settings.globalPref.noAccount')</td>
								</tr>
							#end
							</tbody>
						</table>
	#if($authtools.allow('customer.digitalAPI.XChange'))##
						<hr class="space" />
	#end						
	
#end##

#if($authtools.allow('customer.digitalAPI.XChange'))##
						<h3>#springMessage('label.settings.globalPref.webServiceEndpoint.private')</h3>
						<table class="tabular">
							<thead>
								<tr class="notclickable">
									<th><span>#springMessage('label.settings.globalPref.endPointType')</span></th>
									<th><span>#springMessage('label.settings.globalPref.token')</span></th>
								</tr>
							</thead>
							<tbody class="details">
							#set($webServiceStatus = false)
							#foreach($endPoint in $GlobalConfiguration.privateWebServiceEndPointList)  #set($webServiceStatus = true)
								<tr #if($endPoint.comments && $endPoint.comments != "")comment="$endPoint.comments"#end >
									<td>$endPoint.type</td>
									<td class="fixedWidthFont">$endPoint.key</td>
								</tr>
							#end
							#if($webServiceStatus != true)
								<tr>
									<td class="detailLabel" align="center" colspan="2">#springMessage('label.settings.globalPref.noWebServices')</td>
								</tr>
							#end
							</tbody>
						</table>
#end##
					</section>
				</section>## /#webServices
		#end##
			</section>
		</section>
	</section>
	<hr>
</section>
##
#parse( "${dirPrefix}include/footer.vm" )
##
#parse("${dirPrefix}include/alertMsg.vm")##
## 
<script type="text/javascript" language="javascript">##
#parse( "${dirPrefix}include/standardJSMessages.vm" )##
var noTimeZoneAlert = "#springMessage('alert.settings.globalPref.setTimeZone')";##
var timezoneSavedMsg = "#springArgMessage('alert.form.savedMsg', ["#springMessage('label.settings.globalPref.selected.timeZone')"])";##, $TimeZoneTitle)";##
var invalidTimeZoneMsg = "#springMessage('alert.settings.globalPref.timeZoneInvalid')";##
var querySpacesBySavedMsg = "#springArgMessage('alert.form.savedMsg', ["#springMessage('label.settings.globalPref.selected.querySpacesBy')"])";##
var jurisdictionBySavedMsg = "#springArgMessage('alert.form.savedMsg', ["#springMessage('label.settings.globalPref.parkingJurisdiction')"])";##
var flexCredentialFormTitle = "#springMessage('label.settings.globalPref.flexCredentials')";##
var preferredParkingSavedMsg = "#springMessage('label.settings.globalPref.preferredParking.savedMsg')";##
var testingMsg = "<section class='flexTestIcon'></section>#springMessage('label.testing')";##
var successMsg = "<section class='flexTestIcon success'></section>#springMessage('label.settings.globalPref.successFlexTest')";##
var failMsg = "<section class='flexTestIcon failed'></section>#springMessage('label.settings.globalPref.failFlexTest')";##

var LOC = {
	"saveFlexCredentials": "/secure/settings/global/saveFlexCredentials.html",
	"testNewFlexCredentials": "/secure/settings/global/testNewFlexCredentials.html",
	"testFlexCredentials": "/secure/settings/global/testFlexCredentials.html"
};
var LABEL = {
	"saveSuccess": "#springMessage('alert.systemAdmin.workspace.licenses.flexCredentials.save.success')"
}
#parse("${dirPrefix}include/sessionActivityVar.vm")##
$(document).ready(function(){ $('#selectTimeZone, #selectQuerySpacesBy').inputfit(); standard(); settings(); preferredParkingUploadSetup();	 sessionActvty();##
#if($authtools.allow('customer.flexCredentials'))##
flexCredentials();##
#end##
});##
</script>
<script type="text/javascript" language="javascript" src="${scriptLocal}settings.js${staticQuerry}"></script>##
<script type="text/javascript" language="javascript" src="${scriptLocal}flexCredentials.js${staticQuerry}"></script>##
<script type="text/javascript" language="javascript" src="${scriptLocal}jquery.ui.dptupload.js${staticQuerry}"></script>##
##
</body>
</html>
