<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:sec="http://www.springframework.org/schema/security" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:oauth="http://www.springframework.org/schema/security/oauth2"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
          http://www.springframework.org/schema/security
          http://www.springframework.org/schema/security/spring-security-3.2.xsd
          http://www.springframework.org/schema/security/oauth2
          http://www.springframework.org/schema/security/spring-security-oauth2.xsd">

	<!-- Spring security annotation -->
	<sec:global-method-security pre-post-annotations="enabled" access-decision-manager-ref="accessDecisionManager" />
	
	<sec:http pattern="/secure/**"
			  entry-point-ref="loginUrlAuthenticationEntryPoint"
			  auto-config="false"
			  use-expressions="true"
			  disable-url-rewriting="true">
		<sec:intercept-url pattern="/secure/*" access="isAuthenticated()" />
		<sec:intercept-url pattern="/**" access="isAuthenticated() and (not principal.isSystemAdmin()) and (not principal.isLoginViaPDA())" />
		
		<sec:logout success-handler-ref="webLogoutSuccessHandler"
					invalidate-session="true"
					delete-cookies="JSESSIONID" />
		
		<sec:access-denied-handler ref="accessDeniedHandler"/>
		
		<sec:custom-filter position="CONCURRENT_SESSION_FILTER" ref="concurrentSessionFilter" />
		<sec:custom-filter position="FORM_LOGIN_FILTER" ref="authenticationProcessingFilter" />
		
		<sec:session-management invalid-session-url="/" session-fixation-protection="newSession" />
		<sec:session-management session-authentication-strategy-ref="concurrentSessionController" />
	</sec:http>
	<sec:http pattern="/systemAdmin/**"
			  entry-point-ref="loginUrlAuthenticationEntryPoint"
			  auto-config="false"
			  use-expressions="true"
			  disable-url-rewriting="true">
		<sec:intercept-url pattern="/**" access="isAuthenticated() and principal.isSystemAdmin() and (not principal.isLoginViaPDA())" />
		
		<sec:logout success-handler-ref="webLogoutSuccessHandler"
					invalidate-session="true"
					delete-cookies="JSESSIONID" />
		
		<sec:access-denied-handler ref="accessDeniedHandler"/>
		
		<sec:custom-filter position="CONCURRENT_SESSION_FILTER" ref="concurrentSessionFilter" />
		<sec:custom-filter position="FORM_LOGIN_FILTER" ref="authenticationProcessingFilter" />
		
		<sec:session-management invalid-session-url="/secure/index.html" session-fixation-protection="newSession" />
		<sec:session-management session-authentication-strategy-ref="concurrentSessionController" />
	</sec:http>
	<sec:http pattern="/pda/**"
			  entry-point-ref="pdaLoginUrlAuthenticationEntryPoint"
			  auto-config="false"
			  use-expressions="true"
			  disable-url-rewriting="true">
		<sec:intercept-url pattern="/pda/index.html" access="permitAll" />
		<sec:intercept-url pattern="/pda/j_spring_security_check" access="permitAll" />
		<sec:intercept-url pattern="/pda/**" access="isAuthenticated() and principal.hasPdaAccess() and (not principal.isSystemAdmin())" />
		
		<sec:logout success-handler-ref="pdaWebLogoutSuccessHandler"
					invalidate-session="true"
					delete-cookies="JSESSIONID" />
		
		<sec:access-denied-handler ref="pdaAccessDeniedHandler"/>
		
		<sec:custom-filter position="CONCURRENT_SESSION_FILTER" ref="pdaConcurrentSessionFilter" />
		<sec:custom-filter position="FORM_LOGIN_FILTER" ref="pdaAuthenticationProcessingFilter" />
		
		<sec:session-management invalid-session-url="/pda/" session-fixation-protection="newSession" />
		<sec:session-management session-authentication-strategy-ref="concurrentSessionController" />
	</sec:http>

	<sec:http pattern="/mobile/**"
	          entry-point-ref="mobileLoginUrlAuthenticationEntryPoint"
	          auto-config="false"
	          use-expressions="true"
	          disable-url-rewriting="true"
	          >
		<sec:custom-filter position="BASIC_AUTH_FILTER" ref="mobileAuthenticationProcessingFilter" />
	</sec:http>
	
	<bean id="linkUserShellService" class="com.digitalpaytech.auth.service.LinkUserDetailsService" />

	<bean id="oauthPreAuthenticatedProvider" class="org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider">
		<property name="preAuthenticatedUserDetailsService" ref="linkUserShellService" />
	</bean>
	
	<sec:authentication-manager alias="authenticationManager">
		<sec:authentication-provider ref="daoAuthenticationProvider" />
		<sec:authentication-provider ref="oauthPreAuthenticatedProvider" />
	</sec:authentication-manager>
	
	<!-- PDA Site -->
	<bean id="pdaConcurrentSessionFilter" class="org.springframework.security.web.session.ConcurrentSessionFilter">
		<property name="sessionRegistry" ref="sessionRegistry" />
		<property name="expiredUrl" value="/pda/" />
	</bean>
	
	<bean id="pdaWebLogoutSuccessHandler" class="com.digitalpaytech.auth.WebLogoutSuccessHandler">
		<property name="successURL" value="/pda/" />
	</bean>
	
	<bean id="pdaLogoutFilter" class="com.digitalpaytech.auth.WebLogoutFilter">
		<constructor-arg value="/pda/" />
		<!-- URL redirected to after  logout -->
		<constructor-arg>
			<list>
				<bean class="org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler" />
			</list>
		</constructor-arg>
		<property name="filterProcessesUrl" value="/pda/j_spring_security_logout" />
	</bean>
	
	<bean id="pdaLoginUrlAuthenticationEntryPoint"
		class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
		<property name="loginFormUrl" value="/pda/" />
		<property name="forceHttps" value="false" />
	</bean>
	
	<bean id="pdaAuthenticationProcessingFilter"
		class="com.digitalpaytech.auth.WebAuthenticationProcessingFilter">
		<property name="loginPage" value="/pda/" />
		<property name="filterProcessesUrl" value="/pda/j_spring_security_check" />
		<property name="authenticationFailureHandler">
			<bean class="com.digitalpaytech.auth.WebAuthenticationFailureHandler">
				<property name="defaultFailureUrl" value="/pda/index.html?error=-2"/>
				<property name="failureUrlMap">
					<props>
						<prop key="org.springframework.security.authentication.BadCredentialsException">/pda/index.html?error=-5</prop>
						<prop key="org.springframework.security.authentication.CredentialsExpiredException">/pda/index.html?error=-5</prop>
						<prop key="org.springframework.security.authentication.LockedException">/pda/index.html?error=-5</prop>
						<prop key="org.springframework.security.authentication.DisabledException">/pda/index.html?error=-5</prop>
					</props>
				</property>
			</bean>
		</property>
 		<property name="authenticationSuccessHandler">
 			<bean class="com.digitalpaytech.auth.WebAuthenticationSuccessHandler">
				<property name="defaultTargetUrl" value="/secure/terms.html"/>
				<property name="alwaysUseDefaultTargetUrl" value="false" />
			</bean>
 		</property>
 		<property name="sessionAuthenticationStrategy" ref="concurrentSessionController"/>
		<property name="authenticationManager" ref="authenticationManager" />
		<property name="pda" value="true" />
	</bean>
	
	<bean id="pdaAccessDeniedHandler" class="com.digitalpaytech.auth.IrisAccessDeniedHandler">
		<property name="errorPage" value="/pda/index.html?error=-5" />
	</bean>
	
	<bean id="pdaExceptionTranslationFilter" class="org.springframework.security.web.access.ExceptionTranslationFilter">
		<property name="authenticationEntryPoint" ref="pdaLoginUrlAuthenticationEntryPoint" />
		<property name="accessDeniedHandler" ref="pdaAccessDeniedHandler" />
	</bean>
	
	<!-- Main Site -->
	<bean id="concurrentSessionFilter" class="org.springframework.security.web.session.ConcurrentSessionFilter">
		<property name="sessionRegistry" ref="sessionRegistry" />
		<property name="expiredUrl" value="/" />
	</bean>
	
	<bean id="webLogoutSuccessHandler" class="com.digitalpaytech.auth.WebLogoutSuccessHandler">
		<property name="successURL" value="/" />
	</bean>
	
	<bean id="logoutFilter" class="com.digitalpaytech.auth.WebLogoutFilter">
		<constructor-arg value="/" />
		<!-- URL redirected to after  logout -->
		<constructor-arg>
			<list>
				<bean class="org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler" />
			</list>
		</constructor-arg>
		<property name="filterProcessesUrl" value="/j_spring_security_logout" />
	</bean>
	
	<bean id="loginUrlAuthenticationEntryPoint"
		class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
		<property name="loginFormUrl" value="/" />
		<property name="forceHttps" value="false" />
	</bean>
	
	<bean id="authenticationProcessingFilter"
		class="com.digitalpaytech.auth.WebAuthenticationProcessingFilter">
		<property name="loginPage" value="/" />
		<property name="filterProcessesUrl" value="/j_spring_security_check" />
		<property name="authenticationFailureHandler" ref="failureHandler"/>
 		<property name="authenticationSuccessHandler" ref="successHandler"/>
  		<property name="sessionAuthenticationStrategy" ref="concurrentSessionController"/>
		<property name="authenticationManager" ref="authenticationManager"/>
	</bean>
	
	<bean id="successHandler" class="com.digitalpaytech.auth.WebAuthenticationSuccessHandler">
		<property name="defaultTargetUrl" value="/secure/terms.html"/>
		<property name="alwaysUseDefaultTargetUrl" value="false" />
	</bean>
	<bean id="failureHandler" class="com.digitalpaytech.auth.WebAuthenticationFailureHandler">
		<property name="defaultFailureUrl" value="/index.html?error=-2"/>
		<property name="failureUrlMap">
			<props>
				<prop key="org.springframework.security.authentication.BadCredentialsException">/index.html?error=-5</prop>
				<prop key="org.springframework.security.authentication.CredentialsExpiredException">/index.html?error=-5</prop>
				<prop key="org.springframework.security.authentication.LockedException">/index.html?error=-5</prop>
				<prop key="org.springframework.security.authentication.DisabledException">/index.html?error=-5</prop>
			</props>
		</property>
		
	</bean>
	
	<bean id="accessDeniedHandler" class="com.digitalpaytech.auth.IrisAccessDeniedHandler">
		<property name="errorPage" value="/index.html?error=-5" />
	</bean>
	
	<bean id="exceptionTranslationFilter" class="org.springframework.security.web.access.ExceptionTranslationFilter">
		<property name="authenticationEntryPoint" ref="loginUrlAuthenticationEntryPoint" />
		<property name="accessDeniedHandler" ref="accessDeniedHandler" />
	</bean>
	
	<!-- Mobile -->
	<bean id="mobileAuthenticationProcessingFilter" class="com.digitalpaytech.auth.MobileAuthenticationProcessingFilter">
		<property name="filterProcessesUrl" value="/" />
		<property name="authenticationManager" ref="authenticationManager" />
		<property name="authenticationFailureHandler">
			<bean class="com.digitalpaytech.auth.WebAuthenticationFailureHandler">
				<property name = "defaultFailureUrl" value="/mobile/fail/"/>
				<property name = "useForward" value="true"/>
			</bean>
		</property>
	</bean>
	<bean id="mobileLoginUrlAuthenticationEntryPoint"
		class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
		<property name="loginFormUrl" value="/mobile/" />
		<property name="useForward" value="false" />
		<property name="forceHttps" value="false" />
	</bean>
		
	<!-- Shared -->
	<bean id="channelProcessingFilter"
		class="org.springframework.security.web.access.channel.ChannelProcessingFilter">
		<property name="channelDecisionManager" ref="channelDecisionManager" />
		<property name="securityMetadataSource">
			<sec:filter-security-metadata-source request-matcher="ant">
				<sec:intercept-url pattern="/**" access="REQUIRES_SECURE_CHANNEL" />
			</sec:filter-security-metadata-source>
		</property>
	</bean>

	<bean id="channelDecisionManager" class="org.springframework.security.web.access.channel.ChannelDecisionManagerImpl">
		<property name="channelProcessors">
			<list>
				<ref bean="secureChannelProcessor" />
				<ref bean="insecureChannelProcessor" />
			</list>
		</property>
	</bean>

	<bean id="secureChannelProcessor" class="org.springframework.security.web.access.channel.SecureChannelProcessor" />
	<bean id="insecureChannelProcessor" class="org.springframework.security.web.access.channel.InsecureChannelProcessor" />
	
	<bean id="sessionRegistry" class="org.springframework.security.core.session.SessionRegistryImpl"/>
	
	<bean id="concurrentSessionController" class="org.springframework.security.web.authentication.session.ConcurrentSessionControlStrategy">
		<property name="maximumSessions" value="1" />
		<constructor-arg name="sessionRegistry" ref="sessionRegistry" />
	</bean>
	
	<bean id="securityContextPersistenceFilter"
		class="org.springframework.security.web.context.SecurityContextPersistenceFilter">
		<property name="securityContextRepository">
			<bean class="org.springframework.security.web.context.HttpSessionSecurityContextRepository">
				<property name="allowSessionCreation" value="true" />
			</bean>
		</property>
	</bean>
	
	<bean id="daoAuthenticationProvider"
		class="com.digitalpaytech.auth.WebAuthenticationProvider">
		<property name="userDetailsService" ref="userDetailsService" />
		<property name="passwordEncoder" ref="passwordEncoder" />
		<property name="saltSource" ref="saltSource" />
		<property name="saltSourceEms6" ref="saltSourceEms6" />
		<property name="hideUserNotFoundExceptions" value="false" />
	</bean>

	<bean id="passwordEncoder"
		class="org.springframework.security.authentication.encoding.ShaPasswordEncoder">
		<constructor-arg value="512"/>
	</bean>

	<bean id="saltSource"
		class="org.springframework.security.authentication.dao.ReflectionSaltSource">
		<property name="userPropertyToUse" value="saltSource" />
	</bean>
	
	<!--  EMS 6 Salt -->
	<bean id="saltSourceEms6"
		class="org.springframework.security.authentication.dao.SystemWideSaltSource">
		<property name="systemWideSalt">
			<value>IAmThe$EmsSaltSource!</value>
		</property>
	</bean>	
	
	<!--	 Authentication using JDBC Dao -->
	<bean id="userDetailsService" class="com.digitalpaytech.auth.WebJdbcDaoImpl">
		<property name="dataSource" ref="dataSource" />
	</bean>
	
	<!-- Start authorization config -->
	<bean id="accessDecisionManager" class="org.springframework.security.access.vote.UnanimousBased">
		<property name="decisionVoters">
			<list>
				<ref bean="roleVoter" />
			</list>
		</property>
	</bean>

	<bean id="roleVoter" class="org.springframework.security.access.vote.RoleVoter">
		<property name="rolePrefix">
			<value></value>
		</property>
	</bean>
	<!-- End authorization config -->
	
	<bean id="applicationSecurityListener" class="com.digitalpaytech.auth.ApplicationSecurityListener" />
	
	<!-- OAuth -->
	<bean id="objectMapper" class="com.fasterxml.jackson.databind.ObjectMapper" />
	
	<!-- OAuth Migration -->
	<sec:http pattern="/oauth_migration/**"
			  entry-point-ref="oauthMigrationLoginUrlAuthenticationEntryPoint"
			  auto-config="false"
			  use-expressions="true"
			  disable-url-rewriting="true">
		<sec:intercept-url pattern="/oauth_migration/**" access="isAuthenticated()" />
		
		<sec:logout success-handler-ref="webLogoutSuccessHandler"
					invalidate-session="true"
					delete-cookies="JSESSIONID" />
		
		<sec:access-denied-handler ref="accessDeniedHandler"/>
		
		<sec:custom-filter position="CONCURRENT_SESSION_FILTER" ref="concurrentSessionFilter" />
		<sec:custom-filter position="FORM_LOGIN_FILTER" ref="oauthMigrationFilter" />
		
		<sec:session-management invalid-session-url="/" session-fixation-protection="newSession" />
		<sec:session-management session-authentication-strategy-ref="concurrentSessionController" />
	</sec:http>
	
	<bean id="oauthMigrationLoginUrlAuthenticationEntryPoint"
		class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
		<property name="loginFormUrl" value="/oauth/authorize" />
		<property name="forceHttps" value="false" />
	</bean>
	
	<bean id="oauthMigrationFilter"
		class="com.digitalpaytech.auth.OAuthMigrationFilter">
		<property name="loginPage" value="/oauth/authorize" />
		<property name="filterProcessesUrl" value="/oauth_migration/j_spring_security_check" />
		<property name="authenticationFailureHandler" ref="failureHandler"/>
 		<property name="authenticationSuccessHandler" ref="successHandler"/>
		<property name="authenticationManager" ref="authenticationManager"/>
		<property name="sessionAuthenticationStrategy" ref="concurrentSessionController" />
	</bean>
	
	<!-- Filters -->
	<sec:http pattern="/oauth/**"
			  entry-point-ref="loginUrlAuthenticationEntryPoint"
			  auto-config="false"
			  use-expressions="true"
			  disable-url-rewriting="true">
		<sec:intercept-url pattern="/oauth/*" access="isAuthenticated()" />
		
		<sec:logout success-handler-ref="webLogoutSuccessHandler"
					invalidate-session="true"
					delete-cookies="JSESSIONID" />
		
		<sec:access-denied-handler ref="accessDeniedHandler"/>
		
		<sec:custom-filter position="CONCURRENT_SESSION_FILTER" ref="concurrentSessionFilter" />
		<sec:custom-filter position="FORM_LOGIN_FILTER" ref="oauthProcessingFilter" />
		
		<sec:session-management invalid-session-url="/" session-fixation-protection="newSession" />
		<sec:session-management session-authentication-strategy-ref="concurrentSessionController" />
	</sec:http>
	
	<bean id="tokenServices" class="org.springframework.security.oauth2.provider.token.DefaultTokenServices">
        <property name="tokenStore" ref="tokenStore"/>
        <property name="supportRefreshToken" value="false"/>
        <property name="accessTokenValiditySeconds" value="300000"/>
        <property name="clientDetailsService" ref="clientDetails"/>
    </bean>
    
    <bean id="tokenStore" class="org.springframework.security.oauth2.provider.token.store.jwk.JwkTokenStore">
    	<constructor-arg value="${oauth.server}/jwk" />
    </bean>
    
    <oauth:client-details-service id="clientDetails">
        <!-- Web Application clients -->
        <oauth:client client-id="${oauth.clientId}"
                      secret="${oauth.secret}"
                      authorities="ROLE_OAUTH_CLIENT"
                      authorized-grant-types="implicit"
                      resource-ids="${oauth.resourceIds}"
                      scope="openid"/>
	</oauth:client-details-service>
	
	<bean id="oauthSuccessHandler" class="com.digitalpaytech.auth.WebAuthenticationSuccessHandler">
		<property name="defaultTargetUrl" value="/oauth_migration/authorize.html"/>
		<property name="alwaysUseDefaultTargetUrl" value="false" />
	</bean>
	
	<bean id="oauthProcessingFilter"
		class="com.digitalpaytech.auth.OAuthProcessingFilter">
		<property name="filterProcessesUrl" value="/j_spring_security_check" />
		<property name="authenticationFailureHandler" ref="failureHandler"/>
 		<property name="authenticationSuccessHandler" ref="oauthSuccessHandler"/>
		<property name="authenticationManager" ref="authenticationManager"/>
		<property name="sessionAuthenticationStrategy" ref="concurrentSessionController" />
		<property name="tokenService" ref="tokenServices" />
	</bean>
	
	<bean id="deviceConfigProperties" class="org.springframework.beans.factory.config.PropertiesFactoryBean">
        <property name="locations">
            <list>
                <value>classpath*:device.config.properties</value>
            </list>
        </property>
	</bean>	

	<bean id="OAuthController"
		class="com.digitalpaytech.auth.OAuthController">
	</bean>

</beans>
