<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
	xmlns:task="http://www.springframework.org/schema/task"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
       		http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
       		http://www.springframework.org/schema/context
		  	http://www.springframework.org/schema/context/spring-context-4.1.xsd
		  	http://www.springframework.org/schema/task
        	http://www.springframework.org/schema/task/spring-task-4.1.xsd">

	<task:annotation-driven executor="asyncExecutor" />
	
	<task:executor id="asyncExecutor" pool-size="15" />
	
    <!-- Kafka -->
	<bean id="OTAkafkaExecutor"
		class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
		<property name="corePoolSize" value="1" />
		<property name="maxPoolSize" value="1" />
		<property name="queueCapacity" value="25" />
	</bean>
	
	<!-- Kafka Consumer Threadpool -->
	<bean id="kafkaConsumerExecutor"
		class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
		<property name="corePoolSize" value="1" />
		<property name="maxPoolSize" value="1" />
		<property name="queueCapacity" value="60" />
	</bean>
	
	<!-- Kafka ALert Priority Consumer Threadpool -->
	<bean id="kafkaHighPriorityAlertConsumerExecutor"
		class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
		<property name="corePoolSize" value="1" />
		<property name="maxPoolSize" value="1" />
		<property name="queueCapacity" value="60" />
	</bean>
	
	
	<!-- Kafka Producer Threadpool -->
	<bean id="kafkaProducerExecutor"
        class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
        <property name="corePoolSize" value="5" />
        <property name="maxPoolSize" value="6" />
        <property name="queueCapacity" value="100" />
    </bean>
    
	
	<bean id="actionServicesConfiguration" class="org.springframework.core.io.ClassPathResource" scope="prototype">
		<constructor-arg index="0" value="actionServices.xml"/>
	</bean>
	
	<bean id="actionServicesScheduler" class="org.springframework.scheduling.quartz.SchedulerFactoryBean" scope="singleton">
		<property name="schedulerName" value="ActionServicesScheduler" />
		<property name="waitForJobsToCompleteOnShutdown" value="true" />
		<property name="quartzProperties">
			<props>
				<prop key="org.quartz.threadPool.threadCount">21</prop>
				<prop key="org.quartz.jobStore.misfireThreshold">5000</prop>
				<prop key="org.quartz.scheduler.skipUpdateCheck">true</prop>
			</props>
		</property>
	</bean>
	
	<bean id="actionServicesStopperScheduler" class="org.springframework.scheduling.quartz.SchedulerFactoryBean" scope="singleton">
		<property name="schedulerName" value="ActionServicesStopperScheduler" />
		<property name="waitForJobsToCompleteOnShutdown" value="true" />
		<property name="quartzProperties">
			<props>
				<prop key="org.quartz.threadPool.threadCount">7</prop>
				<prop key="org.quartz.jobStore.misfireThreshold">30000</prop>
				<prop key="org.quartz.scheduler.skipUpdateCheck">true</prop>
			</props>
		</property>
	</bean>

    <!-- Remove low power shut down and shock alarms older than 24 hrs -->
    <bean id="removeUnwantedAlarmsJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="unwantedAlarmsRemovalTask" />
        <property name="targetMethod" value="removeUnwantedAlarms" />
    </bean>
    <bean id="removeUnwantedAlarmsJobTrigger"
        class="org.springframework.scheduling.quartz.SimpleTriggerFactoryBean">
        <property name="jobDetail" ref="removeUnwantedAlarmsJobDetail" />
        <property name="startDelay" value="60000" />
        <property name="repeatInterval" value="1800000" />
    </bean>
	
	<!-- Reverse outstanding failed Alliance transactions -->
	<bean id="cardReversalJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="outstandingCardTransactionCleanUpTask" />
		<property name="targetMethod" value="processOutstandingTransactionReversals" />
	</bean>
	<bean id="cardReversalJobTrigger"
		class="org.springframework.scheduling.quartz.SimpleTriggerFactoryBean">
		<property name="jobDetail" ref="cardReversalJobDetail" />
		<property name="startDelay" value="900000" />
		<property name="repeatInterval" value="300000" />
	</bean>

	<!-- Settle outstanding unsettled transactions -->
	<bean id="cardSettlementJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="outstandingCardTransactionCleanUpTask" />
		<property name="targetMethod"
			value="processOutstandingTransactionSettlementsAndCleanupCardData" />
	</bean>
	<bean id="cardSettlementJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cardSettlementJobDetail" />
		<property name="cronExpression" value="0 50 6 * * ? *" />
	</bean>

    <!-- Retry capture charge transactions every 15 minutes -->
    <bean id="cardCaptureChargeJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="outstandingCardTransactionCleanUpTask" />
        <property name="targetMethod" value="processCaptureChargeResults" />
    </bean>
    <bean id="cardCaptureChargeJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="cardCaptureChargeJobDetail" />
        <property name="cronExpression" value="0 0/15 * * * ? *" />
    </bean>

	<!-- Clean up the PreAuth backup data -->
	<bean id="cardAuthorizationBackupCleanupJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="outstandingCardTransactionCleanUpTask" />
		<property name="targetMethod" value="processCardAuthorizationBackupCleanup" />
	</bean>
	<bean id="cardAuthorizationBackupCleanupJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cardAuthorizationBackupCleanupJobDetail" />
		<property name="cronExpression" value="0 40 6 * * ? *" />
	</bean>

	<!-- Clean up singletransactionCardData data -->
	<bean id="cleanupSingleTransactionCardDataJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="oldDataCleanUpTask" />
		<property name="targetMethod" value="cleanupSingleTransactionCardData" />
	</bean>
	<bean id="cleanupSingleTransactionCardDataJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cleanupSingleTransactionCardDataJobDetail" />
		<property name="cronExpression" value="0 0 8 * * ? *" />
	</bean>

	<!-- Archive data -->
	<bean id="archiveOldReversalJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="oldDataArchiveTask" />
		<property name="targetMethod" value="archiveOldReversalData" />
	</bean>
	<bean id="archiveOldReversalJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="archiveOldReversalJobDetail" />
		<property name="cronExpression" value="0 30 6 * * ? *" />
	</bean>
	<bean id="archivePosSensorInfoJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="oldDataArchiveTask" />
        <property name="targetMethod" value="archiveOldPosSensorInfoData" />
    </bean>
    <bean id="archiveDeletePosSensorInfoJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="archivePosSensorInfoJobDetail"/>
        <property name="cronExpression" value="0 0 7 ? * 1 *" />
    </bean>
    <bean id="archivePosBatteryInfoJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="oldDataArchiveTask" />
        <property name="targetMethod" value="archiveOldPosBatteryInfoData" />
    </bean>
    <bean id="archiveDeletePosBatteryInfoJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="archivePosBatteryInfoJobDetail"/>
        <property name="cronExpression" value="0 30 7 ? * 1 *" />
    </bean>

	<!-- Run charge retry process -->
	<bean id="cardChargeRetryJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="nightlyTransactionRetryProcessingTask" />
		<property name="targetMethod" value="processNightlyTransactionRetries" />
	</bean>
	<bean id="cardChargeRetryJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cardChargeRetryJobDetail" />
		<property name="cronExpression" value="0 0 7 * * ? *" />
	</bean>
	
	<!-- Run Data Patching process -->
	<bean id="dataPatchingJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="dataPatchingTask" />
		<property name="targetMethod" value="process" />
	</bean>
	<bean id="dataPatchingJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="dataPatchingJobDetail" />
		<property name="cronExpression" value="0 0 0/1 * * ? *" />
	</bean>
	
	<!-- CryptoKey rotation -->
	<bean id="cryptoKeyCheckAndProcessJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="autoCryptoKeyProcessingTask" />
		<property name="targetMethod" value="checkAndProcessCryptoKey" />
	</bean>
	<bean id="cryptoKeyCheckAndProcessJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cryptoKeyCheckAndProcessJobDetail" />
		<property name="cronExpression" value="0 0 8 * * ? *" /> 
	</bean>
	
	<bean id="cryptoKeyCheckAndProcessSigningServerKey"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="autoCryptoKeyProcessingTask" />
		<property name="targetMethod" value="checkAndUpdateSigningKeyStore" />
	</bean>
	<bean id="cryptoKeyCheckAndProcessSigningServerKeyTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="cryptoKeyCheckAndProcessSigningServerKey" />
		<!-- <property name="cronExpression" value="0 0 8 * * ? *" /> -->
		<property name="cronExpression" value= "0 0 22 1/1 * ? *" />
	</bean>
	
	<!-- Load the track data and put them in the store forward queue after tomcat restarts
	     SimpleTriggerFactoryBean in Spring 3.1.1 doesn't support repeatCount at this time, 
	     so loadTrackDataJobTriggerModified will be added and it could be removed once the
	     framework supports repeatCount. -->
    <bean id="loadTrackDataJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="trackDataLoadingTask" />
        <property name="targetMethod" value="loadTrackDataAndAddToQueue" />
        <property name="concurrent" value="false" />
        
    </bean>
    <bean id="loadTrackDataJobTrigger"
        class="org.springframework.scheduling.quartz.SimpleTriggerFactoryBean">
        <property name="jobDetail" ref="loadTrackDataJobDetail" />
        <property name="startDelay" value="600000" />
        <property name="repeatInterval" value="10000" />
    </bean>
	<bean id="loadTrackDataJobTriggerModified"
		class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject" ref="loadTrackDataJobTrigger" />
		<property name="targetMethod" value="setRepeatCount" />
		<property name="arguments">
			<list>
				<value>0</value>
			</list>
		</property>
	</bean>
    
	<!-- call sp_CollectionFlagUnsettledPreAuth every 10 minutes for separate collection recalculation. 
        If time interval is other than 10 minutes, please change second parameter value in cronExpression. (e.g. 0/20: every 20 minutes)-->
    <bean id="collectionFlagUnsettledPreAuthJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="runningTotalJobService" />
        <property name="targetMethod" value="processCollectionFlagUnsettledPreAuth" />
    </bean>
    <bean id="collectionFlagUnsettledPreAuthJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="collectionFlagUnsettledPreAuthJobDetail"/>
        <property name="cronExpression" value="25 1/10 * * * ? *" />
    </bean>
    
    <!-- call sp_CollectionReleaseLock every 10 minutes for separate collection recalculation. 
        If time interval is other than 10 minutes, please change second parameter value in cronExpression. (e.g. 0/20: every 20 minutes)-->
    <bean id="collectionReleaseLockJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="runningTotalJobService" />
        <property name="targetMethod" value="processCollectionReleaseLock" />
    </bean>
    <bean id="collectionReleaseLockJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="collectionReleaseLockJobDetail"/>
        <property name="cronExpression" value="5 3/10 * * * ? *" />
    </bean>

    <!-- call sp_CollectionDeleteLock every 6AM for separate collection delete collection lock. -->
    <bean id="deleteCollectionLockJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="runningTotalJobService" />
        <property name="targetMethod" value="processDeleteCollectionLock" />
    </bean>
    <bean id="deleteCollectionLockJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="deleteCollectionLockJobDetail"/>
        <property name="cronExpression" value="0 0 6 * * ? *" />
    </bean>
    
	<bean id="syncFlexDataJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="synchronizeT2FlexDataTask" />
		<property name="targetMethod" value="syncT2Data" />
	</bean>

	<bean id="syncFlexDataJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="syncFlexDataJobDetail" />
		<property name="cronExpression" value="0 0/1 * * * ? *" />
	</bean>

    <bean id="syncFlexWidgetCitationRawJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="syncFlexWidgetDataTask" />
        <property name="targetMethod" value="getCitationRawData" />
        <property name="concurrent" value="false" />
    </bean>

    <bean id="syncFlexWidgetCitationRawJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="syncFlexWidgetCitationRawJobDetail" />
        <property name="cronExpression" value="0 * * * * ? *" />
    </bean>
	
	<bean id="elavonCloseBatchJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="elavonViaConexCloseBatchService" />
        <property name="targetMethod" value="batchProcessingManager" />
    </bean>

    <bean id="elavonCloseBatchJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="elavonCloseBatchJobDetail" />
        <property name="cronExpression" value="0 0/3 * * * ? *" />
    </bean>
    
    <!-- Process reversals on the expired Elavon pre-auth transactions - it runs every hour -->
    <bean id="elavonPreAuthReversalJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="elavonViaConexPreAuthReversalService" />
        <property name="targetMethod" value="reversalProcessingManager" />
    </bean>
    
    <bean id="elavonPreAuthReversalJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="elavonPreAuthReversalJobDetail" />
        <property name="cronExpression" value="0 0/3 * * * ? *" />
    </bean>

    <bean id="emvRefundRetryJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="emvRefundRetryService" />
        <property name="targetMethod" value="refundRetry" />
    </bean>
    <bean id="emvRefundRetryJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="emvRefundRetryJobDetail" />
        <property name="cronExpression" value="0 0 3 * * ? *" />
    </bean>
    
    <bean id="cpsReversalRetryJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="cpsReversalRetryService" />
        <property name="targetMethod" value="reversalRetry" />
    </bean>
    <bean id="cpsReversalJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="cpsReversalRetryJobDetail" />
        <property name="cronExpression" value="0 45 5 * * ? *" />
    </bean>    

    <!-- Notify delayed PosEvents -->
    <bean id="posEventDelayedJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="delayedEventNotificationService" />
        <property name="targetMethod" value="processDelayedAlerts" />
        <property name="concurrent" value="false" />
    </bean>
    <bean id="posEventDelayedJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="posEventDelayedJobDetail" />
        <property name="cronExpression" value="0 0/1 * 1/1 * ? *" />
    </bean>
    
     <!-- Notify delayed PosEvents -->
    <bean id="merchantAccountMigrationJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="merchantAccountMigrationDelayService" />
        <property name="targetMethod" value="processMerchantAccountMigration" />
        <property name="concurrent" value="false" />
    </bean>
    <bean id="merchantAccountMigrationJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="merchantAccountMigrationJobDetail" />
        <property name="cronExpression" value="0 0/1 * 1/1 * ? *" />
    </bean>
    
     <!-- Notify Linux Bad Card List -->
    <bean id="linuxBadCardListJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="linuxBadCardListNotificationTask" />
        <property name="targetMethod" value="checkBadCardList" />
    </bean>
    <bean id="linuxBadCardListJobTrigger"
        class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail" ref="linuxBadCardListJobDetail" />
        <property name="cronExpression" value="0 45 3 * * ? *" />
    </bean>
    
    <!--  Sample data injection tool -->
    <!-- @data.injection.uncomment.start@
	<bean id="dataInjectionJobTrigger"
		class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
		<property name="jobDetail" ref="dataInjectionJobDetail" />
		<property name="cronExpression" value="0 * * * * ? *" />
	</bean>
	<bean id="dataInjectionJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="dataInjectionProcessingTask" />
		<property name="targetMethod" value="sendRequests" />
	</bean>
	@data.injection.uncomment.end@-->
	
	<bean id="schedulerFactory"
		class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
		<property name="quartzProperties">
			<props>
				<prop key="org.quartz.threadPool.threadCount">10</prop>
				<prop key="org.quartz.jobStore.misfireThreshold">1000</prop>
				<prop key="org.quartz.scheduler.skipUpdateCheck">true</prop>
			</props>
		</property>
		<property name="triggers">
			<list>
				<ref bean="removeUnwantedAlarmsJobTrigger" />
				<ref bean="cardReversalJobTrigger" />
				<ref bean="cardSettlementJobTrigger" />
                <ref bean="cardCaptureChargeJobTrigger" />
				<ref bean="cardAuthorizationBackupCleanupJobTrigger" />
				<ref bean="cleanupSingleTransactionCardDataJobTrigger" />
				<ref bean="archiveOldReversalJobTrigger" />
				<ref bean="cardChargeRetryJobTrigger" />
				<ref bean="cryptoKeyCheckAndProcessJobTrigger" />
				<ref bean="loadTrackDataJobTrigger" />
				<ref bean="cryptoKeyCheckAndProcessSigningServerKeyTrigger"/>
				<ref bean="collectionFlagUnsettledPreAuthJobTrigger" />
				<ref bean="collectionReleaseLockJobTrigger" />
				<ref bean="deleteCollectionLockJobTrigger" />
				<ref bean="syncFlexDataJobTrigger" />
                <ref bean="syncFlexWidgetCitationRawJobTrigger" />
				<ref bean="elavonCloseBatchJobTrigger" />
				<ref bean="elavonPreAuthReversalJobTrigger" />
				<ref bean="emvRefundRetryJobTrigger" />
                <ref bean="cpsReversalJobTrigger" />
				<ref bean="dataPatchingJobTrigger" />
				<ref bean="posEventDelayedJobTrigger" />
				<ref bean="merchantAccountMigrationJobTrigger" />
                <ref bean="linuxBadCardListJobTrigger" />
                <!-- @data.injection.uncomment.start@
                <ref bean="dataInjectionJobTrigger" />
                @data.injection.uncomment.end@-->                
			</list>
		</property>
	</bean>
</beans>
