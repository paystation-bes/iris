<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:sec="http://www.springframework.org/schema/security" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:p="http://www.springframework.org/schema/p" xmlns:cache="http://www.springframework.org/schema/cache"
	xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
          http://www.springframework.org/schema/security
          http://www.springframework.org/schema/security/spring-security-4.1.xsd
		  http://www.springframework.org/schema/cache 
		  http://www.springframework.org/schema/cache/spring-cache-4.1.xsd
		  http://www.springframework.org/schema/aop 
		  http://www.springframework.org/schema/aop/spring-aop-4.1.xsd
          http://www.springframework.org/schema/tx 
          http://www.springframework.org/schema/tx/spring-tx-4.1.xsd
       	  http://www.springframework.org/schema/context
		  http://www.springframework.org/schema/context/spring-context-4.1.xsd">

	<context:annotation-config />
	<context:component-scan base-package="com.digitalpaytech.client.spring,com.digitalpaytech.dao,com.digitalpaytech.service,com.digitalpaytech.cardprocessing,com.digitalpaytech.util,com.digitalpaytech.helper,com.digitalpaytech.rpc,com.digitalpaytech.scheduling,com.digitalpaytech.dto.merchant" />
	<tx:annotation-driven transaction-manager="transactionManager" />

	<!-- ========================= message.properties DEFINITIONS ========================= -->
	<bean id="messageSource"
		class="org.springframework.context.support.ResourceBundleMessageSource"
		p:basename="message" />

	<!-- ========================= database config files DEFINITIONS ========================= -->
	<bean id="propertyConfigurer"
		class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
		<property name="locations">
			<list>
				<value>classpath:jdbc.properties</value>
				<value>classpath:jdbc1.properties</value>
				<value>classpath:jdbc_reportQuery.properties</value>
				<value>classpath:jdbc_archiveReportQuery.properties</value>
				<value>classpath:jdbcMigrationIRIS.properties</value>
				<value>classpath:jdbcMigrationEMS6.properties</value>
				<value>classpath:jdbcMigrationEMS6Slave.properties</value>
				<value>classpath:cluster.properties</value>
				<value>classpath:reporting.properties</value>
				<value>classpath:mapping.properties</value>
				<value>classpath:redis.properties</value>
				<value>classpath:oauth.properties</value>
				<value>classpath:retry.properties</value>
				<value>classpath:licenseplateservice.properties</value>
				<value>classpath:test.credit.cards.properties</value>
                <value>classpath:kafka.consumer.task.properties</value>
                <value>classpath:currency.codes.numbers.properties</value>
			</list>
		</property>
	</bean>

	<!-- ========================= DATABASE DEFINITIONS ========================= -->
	<!-- Local Apache Commons DBCP DataSource that refers to a combined database -->
	<!-- (see dataAccessContext-jta.xml for an alternative) -->
	<!-- The placeholders are resolved from jdbc.properties through -->
	<!-- the PropertyPlaceholderConfigurer in applicationContext.xml -->
	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="${jdbc.url}" />
		<property name="user" value="${jdbc.username}" />
		<property name="password" value="${jdbc.password}" />
		<property name="autoCommitOnClose" value="true" />
		<property name="checkoutTimeout" value="${cpool.checkoutTimeout}" />
		<property name="initialPoolSize" value="${cpool.minPoolSize}" />
		<property name="minPoolSize" value="${cpool.minPoolSize}" />
		<property name="maxPoolSize" value="${cpool.maxPoolSize}" />
		<property name="maxIdleTime" value="${cpool.maxIdleTime}" />
		<property name="acquireIncrement" value="${cpool.acquireIncrement}" />
		<property name="maxIdleTimeExcessConnections" value="${cpool.maxIdleTimeExcessConnections}" />
		<property name="numHelperThreads" value="10" />
		<property name="maxAdministrativeTaskTime" value="0" />
		<property name="idleConnectionTestPeriod" value="899" />
		<property name="testConnectionOnCheckin" value="true" />
		<property name="preferredTestQuery" value="/* ping */ SELECT 1" />
	</bean>
	<bean id="sessionFactory"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="dataSource" />
		</property>
		<property name="entityInterceptor">
			<bean class="com.digitalpaytech.util.SQLInterceptor" />
		</property>
		<property name="packagesToScan" value="com.digitalpaytech.domain,com.digitalpaytech.scheduling.datainjection.domain" />
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQL5Dialect</prop>
				<prop key="hibernate.show_sql"> ${hibernate.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpool.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpool.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpool.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpool.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>

	<bean id="defaultLobHandler" class="org.springframework.jdbc.support.lob.DefaultLobHandler" />

	<!-- New connection pool for communication / collection alert. -->
	<bean id="dataSource1" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="${jdbc1.url}" />
		<property name="user" value="${jdbc1.username}" />
		<property name="password" value="${jdbc1.password}" />
		<property name="autoCommitOnClose" value="true" />
		<property name="checkoutTimeout" value="${cpool1.checkoutTimeout}" />
		<property name="initialPoolSize" value="${cpool1.minPoolSize}" />
		<property name="minPoolSize" value="${cpool1.minPoolSize}" />
		<property name="maxPoolSize" value="${cpool1.maxPoolSize}" />
		<property name="maxIdleTime" value="${cpool1.maxIdleTime}" />
		<property name="acquireIncrement" value="${cpool1.acquireIncrement}" />
		<property name="maxIdleTimeExcessConnections" value="${cpool1.maxIdleTimeExcessConnections}" />
		<property name="numHelperThreads" value="10" />
		<property name="maxAdministrativeTaskTime" value="0" />
		<property name="idleConnectionTestPeriod" value="899" />
		<property name="testConnectionOnCheckin" value="true" />
		<property name="preferredTestQuery" value="/* ping */ SELECT 1" />
	</bean>
	<bean id="sessionFactory1"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="dataSource1" />
		</property>
		<property name="mappingLocations">
			<value>
				classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
			</value>
		</property>
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show_sql"> ${hibernate1.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpool1.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpool1.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpool1.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpool1.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>

	<!-- New connection pool for report queries. -->
	<bean id="reportQueryDataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="${jdbc_reportQuery.url}" />
		<property name="user" value="${jdbc_reportQuery.username}" />
		<property name="password" value="${jdbc_reportQuery.password}" />
		<property name="autoCommitOnClose" value="true" />
		<property name="checkoutTimeout" value="${cpool_reportQuery.checkoutTimeout}" />
		<property name="initialPoolSize" value="${cpool_reportQuery.minPoolSize}" />
		<property name="minPoolSize" value="${cpool_reportQuery.minPoolSize}" />
		<property name="maxPoolSize" value="${cpool_reportQuery.maxPoolSize}" />
		<property name="maxIdleTime" value="${cpool_reportQuery.maxIdleTime}" />
		<property name="acquireIncrement" value="${cpool_reportQuery.acquireIncrement}" />
		<property name="maxIdleTimeExcessConnections"
			value="${cpool_reportQuery.maxIdleTimeExcessConnections}" />
		<property name="numHelperThreads" value="10" />
		<property name="maxAdministrativeTaskTime" value="0" />
		<property name="idleConnectionTestPeriod" value="899" />
		<property name="testConnectionOnCheckin" value="true" />
		<property name="preferredTestQuery" value="/* ping */ SELECT 1" />
	</bean>
	<bean id="reportQuerySessionFactory"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="reportQueryDataSource" />
		</property>
		<property name="mappingLocations">
			<value>
				classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
			</value>
		</property>
		<property name="packagesToScan" value="com.digitalpaytech.domain" />
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show_sql"> ${hibernate_reportQuery.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpool_reportQuery.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpool_reportQuery.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpool_reportQuery.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpool_reportQuery.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>
	
	<bean id="archiveReportProperties" class="com.digitalpaytech.report.support.dto.ArchiveReportProperties">
		<property name="months" value="${jdbc_archiveReportQuery.months}" />
		<property name="bufferDays" value="${jdbc_archiveReportQuery.bufferDays}" />
	</bean>
	
	<!-- New connection pool for report queries where data is older than two years -->
    <bean id="archiveReportQueryDataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
        destroy-method="close">
        <property name="driverClass" value="com.mysql.jdbc.Driver" />
        <property name="jdbcUrl" value="${jdbc_archiveReportQuery.url}" />
        <property name="user" value="${jdbc_archiveReportQuery.username}" />
        <property name="password" value="${jdbc_archiveReportQuery.password}" />
        <property name="autoCommitOnClose" value="true" />
        <property name="checkoutTimeout" value="${cpool_archiveReportQuery.checkoutTimeout}" />
        <property name="initialPoolSize" value="${cpool_archiveReportQuery.minPoolSize}" />
        <property name="minPoolSize" value="${cpool_archiveReportQuery.minPoolSize}" />
        <property name="maxPoolSize" value="${cpool_archiveReportQuery.maxPoolSize}" />
        <property name="maxIdleTime" value="${cpool_archiveReportQuery.maxIdleTime}" />
        <property name="acquireIncrement" value="${cpool_archiveReportQuery.acquireIncrement}" />
        <property name="maxIdleTimeExcessConnections"
            value="${cpool_archiveReportQuery.maxIdleTimeExcessConnections}" />
        <property name="numHelperThreads" value="10" />
        <property name="maxAdministrativeTaskTime" value="0" />
        <property name="idleConnectionTestPeriod" value="899" />
        <property name="testConnectionOnCheckin" value="true" />
        <property name="preferredTestQuery" value="/* ping */ SELECT 1" />
    </bean>
    <bean id="archiveReportQuerySessionFactory"
        class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
        <property name="dataSource">
            <ref bean="archiveReportQueryDataSource" />
        </property>
        <property name="mappingLocations">
            <value>
                classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
            </value>
        </property>
        <property name="packagesToScan" value="com.digitalpaytech.domain" />
        <property name="hibernateProperties">
            <props>
                <!-- Echo all executed SQL to stdout -->
                <!-- set hibernate.show_sql = true/false in jdbc.properties -->
                <prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
                <prop key="hibernate.show_sql"> ${hibernate_archiveReportQuery.show_sql} </prop>
                <prop key="hibernate.cache.use_query_cache">true</prop>
                <prop key="hibernate.cache.use_second_level_cache">true</prop>
                
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

                <prop key="hibernate.c3p0.aquire_increment"> ${cpool_archiveReportQuery.acquireIncrement} </prop>
                <prop key="hibernate.c3p0.timeout"> ${cpool_archiveReportQuery.maxIdleTime} </prop>
                <prop key="hibernate.c3p0.max_size"> ${cpool_archiveReportQuery.maxPoolSize} </prop>
                <prop key="hibernate.c3p0.min_size"> ${cpool_archiveReportQuery.minPoolSize} </prop>
                <prop key="hibernate.c3p0.max_statements">0</prop>
            </props>
        </property>
    </bean>

	<!-- New connection pool for Customer Migration to IRIS DB -->
	<bean id="dataSourceMigrationIRIS" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="${jdbcMigrationIRIS.url}" />
		<property name="user" value="${jdbcMigrationIRIS.username}" />
		<property name="password" value="${jdbcMigrationIRIS.password}" />
		<property name="autoCommitOnClose" value="true" />
		<property name="checkoutTimeout" value="${cpoolMigrationIRIS.checkoutTimeout}" />
		<property name="initialPoolSize" value="${cpoolMigrationIRIS.minPoolSize}" />
		<property name="minPoolSize" value="${cpoolMigrationIRIS.minPoolSize}" />
		<property name="maxPoolSize" value="${cpoolMigrationIRIS.maxPoolSize}" />
		<property name="maxIdleTime" value="${cpoolMigrationIRIS.maxIdleTime}" />
		<property name="acquireIncrement" value="${cpoolMigrationIRIS.acquireIncrement}" />
		<property name="maxIdleTimeExcessConnections"
			value="${cpoolMigrationIRIS.maxIdleTimeExcessConnections}" />
		<property name="numHelperThreads" value="10" />
		<property name="maxAdministrativeTaskTime" value="0" />
		<property name="idleConnectionTestPeriod" value="899" />
		<property name="testConnectionOnCheckin" value="true" />
		<property name="preferredTestQuery" value="/* ping */ SELECT 1" />
	</bean>
	<bean id="sessionFactoryMigrationIRIS"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="dataSourceMigrationIRIS" />
		</property>
		<property name="mappingLocations">
			<value>
				classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
			</value>
		</property>
		<property name="packagesToScan" value="com.digitalpaytech.domain" />
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show_sql"> ${hibernateMigrationIRIS.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpoolMigrationIRIS.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpoolMigrationIRIS.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpoolMigrationIRIS.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpoolMigrationIRIS.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>


	<!-- New connection pool for Customer Migration to EMS6 DB -->
	<bean id="dataSourceMigrationEMS6" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close">
		<property name="driverClass" value="com.mysql.jdbc.Driver" />
		<property name="jdbcUrl" value="${jdbcMigrationEMS6.url}" />
		<property name="user" value="${jdbcMigrationEMS6.username}" />
		<property name="password" value="${jdbcMigrationEMS6.password}" />
		<property name="autoCommitOnClose" value="true" />
		<property name="checkoutTimeout" value="${cpoolMigrationEMS6.checkoutTimeout}" />
		<property name="initialPoolSize" value="${cpoolMigrationEMS6.minPoolSize}" />
		<property name="minPoolSize" value="${cpoolMigrationEMS6.minPoolSize}" />
		<property name="maxPoolSize" value="${cpoolMigrationEMS6.maxPoolSize}" />
		<property name="maxIdleTime" value="${cpoolMigrationEMS6.maxIdleTime}" />
		<property name="acquireIncrement" value="${cpoolMigrationEMS6.acquireIncrement}" />
		<property name="maxIdleTimeExcessConnections"
			value="${cpoolMigrationEMS6.maxIdleTimeExcessConnections}" />
		<property name="numHelperThreads" value="10" />
		<property name="maxAdministrativeTaskTime" value="0" />
		<property name="idleConnectionTestPeriod" value="899" />
		<property name="testConnectionOnCheckin" value="true" />
		<property name="preferredTestQuery" value="/* ping */ SELECT 1" />
	</bean>
	<bean id="sessionFactoryMigrationEMS6"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="dataSourceMigrationEMS6" />
		</property>
		<property name="mappingLocations">
			<value>
				classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
			</value>
		</property>
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show_sql"> ${hibernateMigrationEMS6.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpoolMigrationEMS6.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpoolMigrationEMS6.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpoolMigrationEMS6.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpoolMigrationEMS6.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>
	
	<!-- New connection pool for Customer Migration to EMS6 DB Slave-->
    <bean id="dataSourceMigrationEMS6Slave" class="com.mchange.v2.c3p0.ComboPooledDataSource"
        destroy-method="close">
        <property name="driverClass" value="com.mysql.jdbc.Driver" />
        <property name="jdbcUrl" value="${jdbcMigrationEMS6Slave.url}" />
        <property name="user" value="${jdbcMigrationEMS6Slave.username}" />
        <property name="password" value="${jdbcMigrationEMS6Slave.password}" />
        <property name="autoCommitOnClose" value="true" />
        <property name="checkoutTimeout" value="${cpoolMigrationEMS6Slave.checkoutTimeout}" />
        <property name="initialPoolSize" value="${cpoolMigrationEMS6Slave.minPoolSize}" />
        <property name="minPoolSize" value="${cpoolMigrationEMS6Slave.minPoolSize}" />
        <property name="maxPoolSize" value="${cpoolMigrationEMS6Slave.maxPoolSize}" />
        <property name="maxIdleTime" value="${cpoolMigrationEMS6Slave.maxIdleTime}" />
        <property name="acquireIncrement" value="${cpoolMigrationEMS6Slave.acquireIncrement}" />
        <property name="maxIdleTimeExcessConnections" value="${cpoolMigrationEMS6Slave.maxIdleTimeExcessConnections}" />
        <property name="numHelperThreads" value="10" />
        <property name="maxAdministrativeTaskTime" value="0" />
        <property name="idleConnectionTestPeriod" value="899" />
        <property name="testConnectionOnCheckin" value="true" />
        <property name="preferredTestQuery" value="/* ping */ SELECT 1" />
    </bean>
	<bean id="sessionFactoryMigrationEMS6Slave"
		class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
		<property name="dataSource">
			<ref bean="dataSourceMigrationEMS6Slave" />
		</property>
		<property name="mappingLocations">
			<value>
				classpath*:com/digitalpaytech/dao/hibernate/maps/*.hbm.xml
			</value>
		</property>
		<property name="hibernateProperties">
			<props>
				<!-- Echo all executed SQL to stdout -->
				<!-- set hibernate.show_sql = true/false in jdbc.properties -->
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show_sql"> ${hibernateMigrationEMS6Slave.show_sql} </prop>
				<prop key="hibernate.cache.use_query_cache">true</prop>
				<prop key="hibernate.cache.use_second_level_cache">true</prop>
				
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.redis.SingletonRedisRegionFactory</prop>
                <prop key="hibernate.cache.region_prefix">hibernate:</prop>
                <prop key="hibernate.cache.provider_configuration_file_resource_path">hibernate-redis.properties</prop>

				<prop key="hibernate.c3p0.aquire_increment"> ${cpoolMigrationEMS6Slave.acquireIncrement} </prop>
				<prop key="hibernate.c3p0.timeout"> ${cpoolMigrationEMS6Slave.maxIdleTime} </prop>
				<prop key="hibernate.c3p0.max_size"> ${cpoolMigrationEMS6Slave.maxPoolSize} </prop>
				<prop key="hibernate.c3p0.min_size"> ${cpoolMigrationEMS6Slave.minPoolSize} </prop>
				<prop key="hibernate.c3p0.max_statements">0</prop>
			</props>
		</property>
        <!--
		<property name="eventListeners">
			<map>
				<entry key="merge">
					<bean
						class="org.springframework.orm.hibernate4.support.IdTransferringMergeEventListener" />
				</entry>
			</map>
		</property>
		<property name="lobHandler" ref="defaultLobHandler" />
        -->
	</bean>
	
	<!-- Pool for Redis connection -->
    <bean id="cacheConfig" class="com.digitalpaytech.util.cache.CacheConfiguration">
        <property name="host" value="${redis.host}" />
        <property name="port" value="${redis.port}" />
        <property name="permitDatabaseIndex" value="${redis.db.permit}" />
    </bean>
    
	<bean id="jedis" class="redis.clients.jedis.JedisPool">
		<constructor-arg index="0" type="org.apache.commons.pool2.impl.GenericObjectPoolConfig">
			<bean class="org.apache.commons.pool2.impl.GenericObjectPoolConfig">
				<property name="maxTotal" value="${redis.maxTotal}" />
				<property name="minIdle" value="${redis.minIdle}" />
				<property name="maxIdle" value="${redis.maxIdle}" />
			</bean>
		</constructor-arg>
		<constructor-arg index="1" value="${redis.host}" type="java.lang.String" />
		<constructor-arg index="2" value="${redis.port}" type="int" />
        <constructor-arg index="3" value="${redis.timeout}" type="int" />
        <constructor-arg index="4" value="${redis.password}" type="java.lang.String" />
	</bean>
	
	<bean id="jedisCache" class="redis.clients.jedis.JedisPool">
        <constructor-arg index="0" type="org.apache.commons.pool2.impl.GenericObjectPoolConfig">
            <bean class="org.apache.commons.pool2.impl.GenericObjectPoolConfig">
                <property name="maxTotal" value="${redis.maxTotal}" />
                <property name="minIdle" value="${redis.minIdle}" />
                <property name="maxIdle" value="${redis.maxIdle}" />
            </bean>
        </constructor-arg>
        <constructor-arg index="1" value="${redis.cache.host}" type="java.lang.String" />
        <constructor-arg index="2" value="${redis.cache.port}" type="int" />
        <constructor-arg index="3" value="${redis.timeout}" type="int" />
        <constructor-arg index="4" value="${redis.password}" type="java.lang.String" />
	</bean>
	
	<bean id="jedisConnectionFactory" class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory" >
		<constructor-arg index="0" type="redis.clients.jedis.JedisPoolConfig">
			<bean class="redis.clients.jedis.JedisPoolConfig">
			   <property name="maxTotal" value="${redis.maxTotal.elavon}" />
               <property name="minIdle" value="${redis.minIdle.elavon}" />
               <property name="maxIdle" value="${redis.maxIdle.elavon}" />
	           <!-- 
			    the pool will attempt to validate each object before it is borrowed from the pool since it makes sure that 
			    the Jedis instance allocated from the pool is sound and good. 
			    It helps solve the issues of connection timeout. The default is false.
			    -->
			    <property name="testOnBorrow" value="true" />
			</bean>	
		</constructor-arg>
        <property name="hostName" value="${redis.host.elavon}" /> 
        <property name="port" value="${redis.port.elavon}" />
        <property name="database" value="${redis.db.elavon}" />
        <property name="password" value="${redis.password}" /> 
	</bean>
	
	<!-- ========================= Start of AOP Transaction manager ========================= -->
	<bean id="transactionManager"
		class="org.springframework.orm.hibernate4.HibernateTransactionManager">
		<property name="sessionFactory" ref="sessionFactory" />
	</bean>
	<bean id="transactionManagerMigrationIRIS"
		class="org.springframework.orm.hibernate4.HibernateTransactionManager">
		<property name="sessionFactory" ref="sessionFactoryMigrationIRIS" />
	</bean>
	<bean id="transactionManagerMigrationEMS6"
		class="org.springframework.orm.hibernate4.HibernateTransactionManager">
		<property name="sessionFactory" ref="sessionFactoryMigrationEMS6" />
	</bean>
	<bean id="transactionManagerMigrationEMS6Slave"
		class="org.springframework.orm.hibernate4.HibernateTransactionManager">
		<property name="sessionFactory" ref="sessionFactoryMigrationEMS6Slave" />
	</bean>


	<!-- Add exception translation to a template-less Hibernate DAO. -->
	<bean
		class="org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor" />
	
	<!-- General ObjectCache -->
	<bean id="ObjectCache" class="com.digitalpaytech.util.cache.RedisObjectCache" scope="singleton">
		<constructor-arg>
			<bean class="com.digitalpaytech.util.transport.KryoTransporter">
				<constructor-arg type="int" value="5" />
			</bean>
		</constructor-arg>
		<constructor-arg ref="jedisCache"/>
		<constructor-arg type="int" name="dbIndex" value="${redis.db.cache}"/>
		<constructor-arg type="int" name="expirySecs" value="900"/>
	</bean>
	
	<bean id="jsonMarshaller" class="com.digitalpaytech.util.json.JSON"/>
</beans>