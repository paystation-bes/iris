// JavaScript Document
var	posMarkerMap = [],
	posMarkers = new Array(),
	currentListObj = "",
	posDisplayList = "",
	scrollTest = false,
	scrollPage = "",
	listType = "",
	marker2Show = "",
	toShowInfo = false,
	listSwitch = false;
	prevTab = "";
	
var	pageTabs = new Array();
	pageTabs["collection"] = ["activeList", "recentList", "userList"],
	pageTabs["alert"] = ["activeList", "recentList"];


// ----------------------------------------------------------------------------------------------------

function markerTemplateFn(posList) {
	var result = null;
	if(currentListObj !== "userList"){
		var	latlongPOS = "lat='' long='' ",
			itemID = "",
			selectedClass = "",
			randomID = (posList.posRandomId)? posList.posRandomId : posList.randomId,
			dateStrng = (posList.resolvedDate)? posList.resolvedDate : posList.lastSeen;
	
		if(posList.collectionRandomId){ itemID = " itemID='"+posList.collectionRandomId+"' "; if(currentListObj === "recentList"){ randomID = posList.collectionRandomId; }} 
		else if(posList.alertRandomId){ itemID = " itemID='"+posList.alertRandomId+"' "; if(currentListObj === "recentList"){ randomID = posList.alertRandomId; }}
							
		if(posList.numberOfAlerts > 1){ 
			var alertModule = multipleLbl;
			var alrtTitle = posList.numberOfAlerts + " " + additionalAlertMsg }
		else { 
			var alertModule = (posList.module)? posList.module : posList.alertMessage;
			var alrtTitle = (posList.module)? posList.module : posList.alertMessage;}
			
		if($("#selectedPayStation").is(":visible") && $("#selectedPayStation").attr("serial") == posList.serial){ selectedClass = "selected"; }
	
		result =
			"<li id='pos_"+ randomID +"' "+
			"serial='"+ posList.serial +"' "+
			latlongPOS+itemID+
			"class='clickable "+ selectedClass +"'><section class='"+psAlerts[posList.severity].listClass+" clickable'>"+
			"<h3>"+posList.pointOfSaleName+"</h3>"+
			"<span class='routeName'>"+posList.alertDate+"</span>"+
			"<span class='resolvedDate'>"+dateStrng+"</span>"+
			"<span class='collTypeMsg "+psAlerts[posList.severity].listClass+"' title='"+alrtTitle+"'>"+alertModule+"</span>"+
			"<span class='collectionTypeMsg'>"+posList.revenueType+"</span>"+
			"<span class='alertTypeMsg "+psAlerts[posList.severity].listClass+"' title='"+alrtTitle+"'>"+alertModule+"</span>"+
			"<span class='alertHistoryTypeMsg'>"+posList.module+"</span></section></li>"; }
	else{
		var usrList = posList;
		var	latlongUsr = "lat='' long='' ",
			itemID = "",
			selectedClass = "",
			statusClass = "",
			routeName = "&nbsp;",
			randomID = usrList.randomId,
			dateStrng = usrList.lastCommunicated;
		
		if(usrList.latitude){ latlongUsr = "lat='"+usrList.latitude+"' long='"+usrList.longitude+"' "; }
			
		if($("#selectedPayStation").is(":visible") && $("#selectedPayStation").attr("serial") == posList.serial){ selectedClass = "selected"; }
		
		var statusClass = deviceInfo[usrList.statusId]['class'];
		var statusMsg = (usrList.statusId === 1 && (usrList.assignedRouteName && usrList.assignedRouteName !== ""))? usrList.assignedRouteName : usrList.status;
		
		result = 
			"<li id='usr_"+ randomID +"' "+
			"userName='"+ usrList.userName +"' "+
			latlongUsr+
			"class='clickable "+ selectedClass +"'><section class='"+statusClass+" clickable'>"+
			"<h3>"+usrList.firstName+" "+usrList.lastName+"</h3>"+
			"<span class='lastSeen'>"+usrList.lastCommunicated+"</span>"+
			"<span class='usrStatusTypeMsg "+statusClass+"' title='"+alrtTitle+"'>"+statusMsg+"</span></section></li>"; }
	
	return result;
}
	
function markerListContent(posList) //Creates List Items
{
	if(currentListObj !== "userList"){
		var	latlongPOS = "lat='' long='' ",
			itemID = "",
			selectedClass = "",
			randomID = (posList.posRandomId)? posList.posRandomId : posList.randomId,
			dateStrng = (posList.resolvedDate)? posList.resolvedDate : posList.lastSeen;
	
		if(posList.collectionRandomId){ itemID = " itemID='"+posList.collectionRandomId+"' "; if(currentListObj === "recentList"){ randomID = posList.collectionRandomId; }} 
		else if(posList.alertRandomId){ itemID = " itemID='"+posList.alertRandomId+"' "; if(currentListObj === "recentList"){ randomID = posList.alertRandomId; }}
							
		if(posList.numberOfAlerts > 1){ 
			var alertModule = multipleLbl;
			var alrtTitle = posList.numberOfAlerts + " " + additionalAlertMsg }
		else { 
			var alertModule = (posList.module)? posList.module : posList.alertMessage;
			var alrtTitle = (posList.module)? posList.module : posList.alertMessage;}
			
		if($("#selectedPayStation").is(":visible") && $("#selectedPayStation").attr("serial") == posList.serial){ selectedClass = "selected"; }
	
		posDisplayList = posDisplayList + 
			"<li id='pos_"+ randomID +"' "+
			"serial='"+ posList.serial +"' "+
			latlongPOS+itemID+
			"class='clickable "+ selectedClass +"'><section class='"+psAlerts[posList.severity].listClass+" clickable'>"+
			"<h3>"+posList.pointOfSaleName+"</h3>"+
			"<span class='routeName'>"+posList.alertDate+"</span>"+
			"<span class='resolvedDate'>"+dateStrng+"</span>"+
			"<span class='collTypeMsg "+psAlerts[posList.severity].listClass+"' title='"+alrtTitle+"'>"+alertModule+"</span>"+
			"<span class='collectionTypeMsg'>"+posList.revenueType+"</span>"+
			"<span class='alertTypeMsg "+psAlerts[posList.severity].listClass+"' title='"+alrtTitle+"'>"+alertModule+"</span>"+
			"<span class='alertHistoryTypeMsg'>"+posList.module+"</span></section></li>"; }
	else{
		var usrList = posList;
		var	latlongUsr = "lat='' long='' ",
			itemID = "",
			selectedClass = "",
			statusClass = "",
			routeName = "&nbsp;",
			randomID = usrList.randomId,
			dateStrng = usrList.lastCommunicated;
		
		if(usrList.latitude){ latlongUsr = "lat='"+usrList.latitude+"' long='"+usrList.longitude+"' "; }
			
		if($("#selectedPayStation").is(":visible") && $("#selectedPayStation").attr("serial") == posList.serial){ selectedClass = "selected"; }
		
		var statusClass = deviceInfo[usrList.statusId]['class'];
		var statusMsg = (usrList.statusId === 1 && (usrList.assignedRouteName && usrList.assignedRouteName !== ""))? usrList.assignedRouteName : usrList.status;
		
		posDisplayList = posDisplayList + 
			"<li id='usr_"+ randomID +"' "+
			"userName='"+ usrList.userName +"' "+
			latlongUsr+
			"class='clickable "+ selectedClass +"'><section class='"+statusClass+" clickable'>"+
			"<h3>"+usrList.firstName+" "+usrList.lastName+"</h3>"+
			"<span class='lastSeen'>"+usrList.lastCommunicated+"</span>"+
			"<span class='usrStatusTypeMsg "+statusClass+"' title='"+alrtTitle+"'>"+statusMsg+"</span></section></li>"; }
}

// ----------------------------------------------------------------------------------------------------

function markerClick(curMarker, markerGroup)
{
	var posCollSN = curMarker.options.alt;
	var posRandomID = posMarkerMap[posCollSN];

	if(currentListObj === "activeList"){ 
		if(markerGroup === "activeList"){ selectListItem(curMarker, posRandomID); }
		else if(markerGroup === "userList"){ getPOSAlertInfo(posRandomID, "new", curMarker, "", "light", markerGroup); }}
	else if(currentListObj === "recentList"){ 
		if(markerGroup === "activeList"){ getPOSAlertInfo(posRandomID, "new", curMarker, "", "light", markerGroup); }
		else if(markerGroup === "recentList"){ findCollections(curMarker, posRandomID); }
		else if(markerGroup === "userList"){ getPOSAlertInfo(posRandomID, "new", curMarker, "", "light", markerGroup); }}
	else if(currentListObj === "userList"){ 
		if(markerGroup === "activeList"){ getPOSAlertInfo(posRandomID, "new", curMarker, "", "light", markerGroup); }
		else if(markerGroup === "recentList"){ findCollections(curMarker, posRandomID, "light"); }
		else if(markerGroup === "userList"){ selectListItem(curMarker, posRandomID); }}
}

// ----------------------------------------------------------------------------------------------------

function showCollectionPopup(collectionData, curMarker, method){
	var posCollectionInfo = collectionData.collectionsList;
	var alertDetailRoute = posCollectionInfo.route;
	if(alertDetailRoute instanceof Array === false){ alertDetailRoute = [alertDetailRoute]; }
	for(x = 0; x < alertDetailRoute.length; x++){ 
		if(x==0){ routeNames = alertDetailRoute[x]; } 
		else { routeNames = routeNames + ", " + alertDetailRoute[x]; } }

	var collections =  posCollectionInfo.collections;
	if(collections instanceof Array === false){ collections = [collections]; }
	var collMadeDetails = '<dt class="detailLabel">'+collectionTodayTitle+':</dt><dd class="detailValue">';
	for(x = 0; x < collections.length; x++){ 
		if(method === "light"){ var objStart = '<strong>'; var objEnd = '</strong>'; }
		else{ var objStart = '<a href="#" onclick="return selectEventItem(\''+collections[x].collectionRandomId+'\');" class="collDetailLink">'; var objEnd = '</a>'; }
		if(x > 0){ collMadeDetails = collMadeDetails + "<hr>"; }
		collMadeDetails = collMadeDetails + objStart + collections[x].revenueType + objEnd +'<strong class="right">$'+ collections[x].amount +'</strong>';
		if(collections[x].collectionUserName && collections[x].collectionUserName !== ""){		
			collMadeDetails =	collMadeDetails + '<br><section class="left note">'+ collectedByMsg +
								':</section> <strong class="note collectionUser" tooltip="'+ collectedByMsg + ": " + collections[x].collectionUserName +'">'+ 
								collections[x].collectionUserName +'</strong>'; }}
	
	collMadeDetails = collMadeDetails + '</dd>';
	
	if(detailPermission){permissionLink = '<a href="'+detailsUrl+'payStationList.html?pgActn=display&psID='+posCollectionInfo.randomId+'&tabID=reports&'+'" class="linkButtonFtr textButton psDetailBtn">'+detailsMsg+'</a>';}

	
	curMarker.setZIndexOffset(1400);
	curMarker.unbindPopup().bindPopup(
		'<section class="mapPopUp">' +
		'<h1>'+posCollectionInfo.pointOfSaleName+'</h1>' +
		'	<section class="thumbNailButton">' +
		'		<img src="'+imgLocalDir+"paystation_"+payStationIcn[posCollectionInfo.payStationType].img[0]+'Sml.jpg" width="50" height="100" border="0" /><br /><br />' + permissionLink +'</section>' +
		'	<dl class="psDetails">' +
		'		<dt class="detailLabel">'+ lastSeenMsg +': </dt><dd class="detailValue">'+ posCollectionInfo.lastSeen +'</dd>'+
		'		<dt class="detailLabel">'+ routeTitleMsg +': </dt><dd class="detailValue">'+ routeNames +'</dd>' + collMadeDetails +
		'	</dl>' +
		'</section>'
	).openPopup();
}

// ----------------------------------------------------------------------------------------------------

function showResolvedAlertPopup(alertData, curMarker, method){
	var	posResolvedAlertInfo = alertData.payStationInfo,
		alertDetailRoute = posResolvedAlertInfo.route,
		resolvedAlertDetails = '', objStart = '', objEnd = '', clearedAlertUser = '', permissionLink = '';
		
	if(alertDetailRoute instanceof Array === false){ alertDetailRoute = [alertDetailRoute]; }
	for(x = 0; x < alertDetailRoute.length; x++){ 
		if(x==0){ routeNames = alertDetailRoute[x]; } 
		else { routeNames = routeNames + ", " + alertDetailRoute[x]; } }

	var resolvedAlert =  posResolvedAlertInfo.resolvedAlerts;
	if(resolvedAlert instanceof Array === false){ resolvedAlert = [resolvedAlert]; }
	if(detailPermission){permissionLink = '<a href="'+detailsUrl+'payStationList.html?pgActn=display&psID='+resolvedAlert[0].posRandomId+'&tabID=alerts&'+'" class="linkButtonFtr textButton psDetailBtn">'+detailsMsg+'</a>';}
/*	
	if(method === "light"){ objStart = '<strong>'; objEnd = '</strong>'; }
	else{ objStart = '<a href="#" onclick="return selectEventItem(\''+resolvedAlert[0].alertRandomId+'\');" class="collDetailLink">'; objEnd = '</a>'; }
*/	clearedAlertUser = (resolvedAlert[0].isClearedByUser)? resolvedAlert[0].lastModifiedByUserName : clearedAutoTitle;	
	resolvedAlertDetails =	'<dt class="detailLabel">Resolved Alert:</dt><dd class="detailValue">' +
							'<strong>'+ resolvedAlert[0].alertMessage +'</strong></dd>' +
							'<dt class="detailLabel">'+ alertDateTitle +':</dt><dd class="detailValue">' +
							resolvedAlert[0].alertDate +'</dd>' +
							'<dt class="detailLabel">'+ resolvedDateTitle +':</dt><dd class="detailValue">' +
							resolvedAlert[0].resolvedDate +'</dd>' +
							'<dt class="detailLabel">'+ resolvedTimeTitle +':</dt><dd class="detailValue">' +
							resolvedAlert[0].duration +'</dd>' +
							'<dt class="detailLabel">'+ clearedByTitle +':</dt><dd class="detailValue">' +
							clearedAlertUser +'</dd>';
	
	curMarker.setZIndexOffset(1400);
	curMarker.unbindPopup().bindPopup(
		'<section class="mapPopUp">' +
		'<h1>'+posResolvedAlertInfo.name+'</h1>' +
		'	<section class="thumbNailButton">' +
		'		<img src="'+imgLocalDir+"paystation_"+payStationIcn[posResolvedAlertInfo.payStationType].img[0]+'Sml.jpg" width="50" height="100" border="0"/><br/><br/>' + permissionLink + '</section>' +
		'	<dl class="psDetails">' +
		'		<dt class="detailLabel">'+ lastSeenMsg +': </dt><dd class="detailValue">'+ posResolvedAlertInfo.lastSeen +'</dd>'+
		'		<dt class="detailLabel">'+ routeTitleMsg +': </dt><dd class="detailValue">'+ routeNames +'</dd>' + resolvedAlertDetails +
		'	</dl>' +
		'</section>'
	).openPopup();
}

// ----------------------------------------------------------------------------------------------------

function findCollections(curMarker, posRandomID, method)
{
	var clearedPosEventUrl = [];
		clearedPosEventUrl['collection'] = "/secure/collections/payStationCollectionsMade.html"
		clearedPosEventUrl['alert'] = "/secure/alerts/resolvedAlertMap.html"
	
	$.ajax({
		url: clearedPosEventUrl[listType],
		dataType: "json",
		data: {pointOfSaleId: posRandomID},
		success: function(data) {
			if(listType === 'collection'){ showCollectionPopup(data, curMarker, method); }
			else if(listType === 'alert'){ showResolvedAlertPopup(data, curMarker, method); } },
		error: function(){ alertDialog(systemErrorMsg); toShowInfo = false; }
	});
}

// ----------------------------------------------------------------------------------------------------

function selectListItem(curMarker, posRandomID) //Selects list Item when Map Marker is clicked.
{
	var $listObj = (currentListObj === "activeList")? $("#pos_"+posRandomID) : $("#usr_"+posRandomID);
	if($listObj.length){ $listObj.trigger('click'); }
	else{
		locatePaginatetab().paginatetab("reloadPageWithItem", { "id": posRandomID }, function() {
			var $listObj = (currentListObj === "activeList")? $("#pos_"+posRandomID) : $("#usr_"+posRandomID);
			$listObj.trigger('click');
		}); }
}

// ----------------------------------------------------------------------------------------------------

var	collTypeClass = new Array();
	collTypeClass[0] = "collection-All";
	collTypeClass[1] = "collection-Bill";
	collTypeClass[2] = "collection-Coin";
	collTypeClass[3] = "collection-Card";

//Inserts Details into Detail Box and Map Marker InfoBox
function insertPayStationDetails(posAlertDetails, curMarker, posItemID, posID, method, listStatus) 
{
	if(posAlertDetails && posAlertDetails != "") {
		if(listStatus === "userList"){
			var userDetails = posAlertDetails;
			var userFullName = userDetails.firstName+' '+userDetails.lastName;
			var assignedRoute = (userDetails.assignedRouteName && userDetails.assignedRouteName !== "")? "<strong>"+userDetails.assignedRouteName+"</strong> ["+userDetails.status+"]" : userDetails.status;
			var deviceNameID = (userDetails.deviceName && userDetails.deviceName !== "")? userDetails.deviceName : unnamedDeviceTitle;
			
			if(method !== "light"){
				$("#selectedUsrDetails").find("h2").html(userFullName);				
				$("#selectedPayStation").attr("serial", userDetails.userName);
				$("#detailStatus").html(assignedRoute);
				$("#deviceLastSeen").html(userDetails.lastCommunicated);
				if(userDetails.statusId !== 3){ 
					$(".deviceInfo, .loggedInSince").show();
					$("#detailDeviceName").html(deviceNameID);
					$("#detailDeviceID").html(userDetails.deviceUid);
					$("#detailLoggedInSince").html(userDetails.loggedInSince); }
				else { $(".deviceInfo, .loggedInSince").hide();}
				
				if(detailUserPermission){ $("#userDetailBtn").attr("href", userDetailsUrl+userDetails.userRandomId+'&'+document.location.search.substring(1)).show(); } }
			
			var permissionLink = "";
			if(detailUserPermission){
				var tempQuerryStrng = scrubQueryString(document.location.search.substring(1), "pgActn");
				permissionLink = '<br /><a href="'+userDetailsUrl+userDetails.userRandomId+'&'+tempQuerryStrng+'" class="linkButtonFtr textButton psDetailBtn right" style="margin: 2px;">'+detailsMsg+'</a>';}
			var routeAssigned = (userDetails.assignedRouteName && userDetails.assignedRouteName !== "")?'<dt class="detailLabel">'+ routeTitleMsg +': </dt><dd class="detailValue">'+ userDetails.assignedRouteName +'</dd>': "" ;
			
			showMarkerPopUp = function(){
				curMarker.setZIndexOffset(1400);
				curMarker.unbindPopup().bindPopup(
					'<section class="mapPopUp">' +
					'<h1>'+userFullName+'</h1>' +
					'	</section>' +
					'	<dl class="psDetails">' +
					'		<dt class="detailLabel">'+ lastSeenMsg +': </dt><dd class="detailValue">'+ userDetails.lastCommunicated +'</dd>'+
					routeAssigned +
					'	</dl>' +
					permissionLink +
					'</section>' 
				).openPopup(); }
					
			if(curMarker){
				var visibleOne = markerGroups[currentListObj].getVisibleParent(curMarker);
				if(visibleOne && visibleOne.options.alt !== curMarker.options.alt){ 
					visibleOne.zoomToBounds()
					var testZoomComp = setInterval(function(){
						var visibleMarker = markerGroups[currentListObj].getVisibleParent(curMarker); 
						if(visibleMarker.options.alt === curMarker.options.alt){ showMarkerPopUp(); testZoomComp = window.clearInterval(testZoomComp); } }, 500); }
				else{ 
					var	mapBounds = mapInPage.getBounds();
					var markerVisible = mapBounds.contains(curMarker.getLatLng());
					if(!markerVisible){ mapInPage.panTo(curMarker.getLatLng(), {animate: true});  showMarkerPopUp(); }
					else{ showMarkerPopUp(); } } }}
		else {
			if(($("li.selected").length && !posID)){ var posID = $("li.selected").attr("id").replace("pos_", ""); }
			var psType = posAlertDetails.payStationType;
			if(method !== "light"){
				if(listStatus !== "userList"){ $("#slctdPOSImg").attr("src", imgLocalDir+"paystation_"+payStationIcn[psType].img[0]+"Lrg.jpg"); }
				var routeNames = "";
				$("#selectedPOSdetails").find("h2").html(posAlertDetails.name);
				var alertDetailRoute = (posAlertDetails.route)? posAlertDetails.route : "";
				if(alertDetailRoute !== "" && alertDetailRoute instanceof Array === false){ alertDetailRoute = [alertDetailRoute]; }
                if(alertDetailRoute.length > 0){
                    for(x = 0; x < alertDetailRoute.length; x++)
                    { if(x==0){ routeNames = alertDetailRoute[x]; } else { routeNames = routeNames + ", " + alertDetailRoute[x]; } }}
                else{ routeNames = unassignedMsg; }
				
				$("#selectedPayStation").attr("serial", posAlertDetails.serial);
				if(routeNames === unassignedMsg){ $("#routeNames").html(""); }else{ $("#routeNames").html(routeNames); }
				$("#lastSeen").html(lastSeenMsg + ": " + posAlertDetails.lastSeen); }
				
			if(listType == "collection"){
				if(currentListObj === "recentList"){ $("#lastCollected").html("").hide(); }
				else{
					$("#lastCollected").html( lastCollectedMsg + ": <span class="+ psAlerts[posAlertDetails.lastCollectionSeverity].listClass +">" + posAlertDetails.lastCollection  + "</span>").show(); }
				var collAlertDetails = "";
				var permissionLink = "";
				
				if(method !== "light"){
					if(posItemID){ 
						$("#collDetailBtn").attr("collectionID", posItemID).show();
						$("#collectionDetailTable").attr("class", collTypeClass[posAlertDetails.collectionType]); }
					else { 
						$("#collDetailBtn").hide();
						$("#collectionDetailTable").attr("class", ""); }
						
					if(posAlertDetails.collectionUserName &&  posAlertDetails.collectionUserName !== ""){ 
						$("#collectedBy").html(collectedByMsg +": "+ posAlertDetails.collectionUserName).show(); }
					else{ $("#collectedBy").html("").hide(); }
					
					$("#coinCount").html(posAlertDetails.coinCount).attr("class", "col2").addClass(psAlerts[posAlertDetails.coinCountSeverity].listClass);
					$("#coinDollar").html("$"+posAlertDetails.coinAmount).attr("class", "col3").addClass(psAlerts[posAlertDetails.coinAmountSeverity].listClass);
					
					$("#billCount").html(posAlertDetails.billCount).attr("class", "col2").addClass(psAlerts[posAlertDetails.billCountSeverity].listClass);
					$("#billDollar").html("$"+posAlertDetails.billAmount).attr("class", "col3").addClass(psAlerts[posAlertDetails.billAmountSeverity].listClass);
					
					if(currentListObj == "activeList") {
						$("#ccCount").html(posAlertDetails.creditCount).attr("class", "col2").addClass(psAlerts[posAlertDetails.creditCountSeverity].listClass); }
					else { $("#ccCount").html("").attr("class", "col2"); }
					
					$("#ccDollar").html("$"+posAlertDetails.creditAmount).attr("class", "col3").addClass(psAlerts[posAlertDetails.creditAmountSeverity].listClass);
					
					$("#rtDollarLabel").html(rtDollarLabel[currentListObj]);
					$("#rtDollar").html("$"+posAlertDetails.runningTotal).attr("class", "col3").addClass(psAlerts[posAlertDetails.runningTotalSeverity].listClass);}
					
					if(posAlertDetails.coinCountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ coinCountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.coinCountSeverity].listClass +'"><strong>'+ 
						posAlertDetails.coinCount +'</strong></dd>'; }
					if(posAlertDetails.coinAmountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ coinAmountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.coinAmountSeverity].listClass +'"><strong>$'+ 
						posAlertDetails.coinAmount +'</strong></dd>'; }
					
					if(posAlertDetails.billCountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ billCountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.billCountSeverity].listClass +'"><strong>'+ 
						posAlertDetails.billCount +'</strong></dd>'; }
					if(posAlertDetails.billAmountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ billAmountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.billAmountSeverity].listClass +'"><strong>$'+ 
						posAlertDetails.billAmount +'</strong></dd>'; }
					
					if(posAlertDetails.creditCountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ cardCountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.creditCountSeverity].listClass +'"><strong>'+ 
						posAlertDetails.creditCount +'</strong></dd>'; }
					if(posAlertDetails.creditAmountSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ cardAmountMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.creditAmountSeverity].listClass +'"><strong>$'+ 
						posAlertDetails.creditAmount +'</strong></dd>';}
					
					if(posAlertDetails.runningTotalSeverity > 0) { collAlertDetails = collAlertDetails + 
						'<dt class="detailLabel"><strong>'+ runningTotalMsg +':</strong> </dt><dd class="detailValue '+ 
						psAlerts[posAlertDetails.runningTotalSeverity].listClass +'"><strong>$'+ 
						posAlertDetails.runningTotal +'</strong></dd>'; }
					
					if(detailPermission){
						var tempQuerryStrng = scrubQueryString(document.location.search.substring(1), "pgActn");
						permissionLink = '<a href="'+detailsUrl+'payStationList.html?pgActn=display&psID='+posID+'&tabID=reportsTransactions&'+tempQuerryStrng+'" class="linkButtonFtr textButton psDetailBtn">'+detailsMsg+'</a>'; }
				
				if(currentListObj === "recentList" && curMarker){
					showMarkerPopUp = function(){
						var posCollSN = curMarker.options.alt;
						var posRandomID = posMarkerMap[posCollSN];
						findCollections(curMarker, posRandomID) }}
				else if(curMarker) {
					showMarkerPopUp = function(){
						curMarker.setZIndexOffset(1400);
						curMarker.unbindPopup().bindPopup(
							'<section class="mapPopUp">' +
							'<h1>'+posAlertDetails.name+'</h1>' +
							'	<section class="thumbNailButton">' +
							'		<img src="'+imgLocalDir+"paystation_"+payStationIcn[psType].img[0]+'Sml.jpg" width="50" height="100" border="0" /><br /><br />' +
							permissionLink +
							'	</section>' +
							'	<dl class="psDetails">' +
							'		<dt class="detailLabel">'+ lastSeenMsg +': </dt><dd class="detailValue">'+ posAlertDetails.lastSeen +'</dd>'+
							'		<dt class="detailLabel">'+ lastCollectedMsg +': </dt>'+
							'<dd class="detailValue '+ psAlerts[posAlertDetails.lastCollectionSeverity].listClass + '">'+ posAlertDetails.lastCollection +'</dd>' +
							'		<dt class="detailLabel">'+ routeTitleMsg +': </dt><dd class="detailValue">'+ routeNames +'</dd>' +
							collAlertDetails +
							'	</dl>' +
							'</section>' 
						).openPopup(); }}
					
					if(curMarker){
						var visibleOne = markerGroups[currentListObj].getVisibleParent(curMarker);
						if(visibleOne && visibleOne.options.alt !== curMarker.options.alt){ 
							visibleOne.zoomToBounds()
							var testZoomComp = setInterval(function(){
								var visibleMarker = markerGroups[currentListObj].getVisibleParent(curMarker); 
								if(visibleMarker.options.alt === curMarker.options.alt){ showMarkerPopUp(); testZoomComp = window.clearInterval(testZoomComp); } }, 500); }
						else{ 
							var	mapBounds = mapInPage.getBounds();
							var markerVisible = mapBounds.contains(curMarker.getLatLng());
							if(!markerVisible){ mapInPage.panTo(curMarker.getLatLng(), {animate: true});  showMarkerPopUp(); }
							else{ showMarkerPopUp(); } } } }
			else if(listType == "alert"){
				$("#lastCollected").html("").hide();
				var	mapAlertDetails = "",
					alertDetails = "",
					permissionLink = "",
					clearLink = "";
					
				if(currentListObj === "activeList" || listStatus === "activeList") {
					var activeAlerts = posAlertDetails.activeAlerts;
					if(activeAlerts instanceof Array === false){ activeAlerts = [activeAlerts]; }
					for(x=0; x < activeAlerts.length; x++){
						mapAlertDetails = 	mapAlertDetails+'<span id="mapAlert_'+activeAlerts[x].alertRandomId+
										'" class="'+psAlerts[activeAlerts[x].severity].listClass+'">'+ 
										activeAlerts[x].module+'<br></span>'; 
						clearLink = (activeAlerts[x].isClearable && clearAlerts)? '<a href="#" id="clear_'+activeAlerts[x].alertRandomId+'" class="removeFile settingDelete menuListButton" title="'+resolveLbl+'">&nbsp;</a>':'';
						alertDetails = 	alertDetails+'<li module="'+ activeAlerts[x].module +'" id="clearItem_'+activeAlerts[x].alertRandomId+
										'"><section class="'+psAlerts[activeAlerts[x].severity].listClass+
										'">'+activeAlerts[x].alertMessage+clearLink+'</section></li>'; }
					if(method !== "light"){
						$("#alertDetailTable").html(alertDetails);
						if(detailPermission){
							var tempQuerryStrng = scrubQueryString(document.location.search.substring(1), "pgActn");
							permissionLink = '<a href="'+detailsUrl+'payStationList.html?pgActn=display&psID='+posID+'&tabID=alerts&'+tempQuerryStrng+'" class="linkButtonFtr textButton psDetailBtn">'+detailsMsg+'</a>';} }
					
					showMarkerPopUp = function(){
						curMarker.setZIndexOffset(1400);
						curMarker.unbindPopup().bindPopup(
							'<section class="mapPopUp">' +
							'<h1>'+posAlertDetails.name+'</h1>' +
							'	<section class="thumbNailButton">' +
							'	<img src="'+imgLocalDir+"paystation_"+payStationIcn[psType].img[0]+'Sml.jpg" width="50" height="100" border="0" /><br /><br />' +
							permissionLink +
							'	</section>' +
							'	<dl class="psDetails">' +
							'		<dt class="detailLabel">'+ lastSeenMsg +': </dt><dd class="detailValue">'+ posAlertDetails.lastSeen +'</dd>'+
							'		<dt class="detailLabel">'+ routeTitleMsg +': </dt><dd class="detailValue">'+ routeNames +'</dd>' +
							'		<dt class="detailLabel">Module: </dt><dd class="detailValue">'+ mapAlertDetails +'</dd>' +
							'	</dl>' +
							'</section>'
						).openPopup(); }
					
					if(curMarker){
						var visibleOne = markerGroups[currentListObj].getVisibleParent(curMarker);
						if(visibleOne && visibleOne.options.alt !== curMarker.options.alt){ 
							visibleOne.zoomToBounds()
							var testZoomComp = setInterval(function(){
								var visibleMarker = markerGroups[currentListObj].getVisibleParent(curMarker); 
								if(visibleMarker.options.alt === curMarker.options.alt){ showMarkerPopUp(); testZoomComp = window.clearInterval(testZoomComp); } }, 500); }
						else{ 
							var	mapBounds = mapInPage.getBounds();
							var markerVisible = mapBounds.contains(curMarker.getLatLng());
							if(!markerVisible){ mapInPage.panTo(curMarker.getLatLng(), {animate: true});  showMarkerPopUp(); }
							else{ showMarkerPopUp(); } } } }
				else {
					$("#lastSeen").html("<strong>"+posAlertDetails.activeAlerts.alertMessage+"</strong>");
					alertDetails = alertDetails + '<li><dt class="detailLabel"><p>'+ alertDateTitle +':</p></dt><dd class="detailValue">'+ posAlertDetails.activeAlerts.alertDate +'</dd></li>';
					alertDetails = alertDetails + '<li><dt class="detailLabel"><p>'+ resolvedDateTitle +':</p></dt><dd class="detailValue">'+ posAlertDetails.activeAlerts.resolvedDate +'</dd></li>';
					alertDetails = alertDetails + '<li><dt class="detailLabel"><p>'+ resolvedTimeTitle +':</p></dt><dd class="detailValue">'+ posAlertDetails.activeAlerts.duration +'</dd></li>';
					alertDetails = alertDetails + '<li><dt class="detailLabel"><p>'+ clearedByTitle +':</p></dt>';
					if(posAlertDetails.activeAlerts.isClearedByUser){ alertDetails = alertDetails + '<dd class="detailValue">'+ posAlertDetails.activeAlerts.lastModifiedByUserName +'</dd></li>'; }
					else{ alertDetails = alertDetails + '<dd class="detailValue">'+ clearedAutoTitle +'</dd></li>'; }
					
					$("#alertDetailTable").html(alertDetails);}}
		else {
			$("#selectedPOSdetails").find("h2").html("");
			$("#routeNames").html("");
			$("#lastSeen").html("");
	
			if(listType == "collection"){
				$("#coinCount").html("").attr("class", "col2");
				$("#coinDollar").html("").attr("class", "col3");
				
				$("#billCount").html("").attr("class", "col2");
				$("#billDollar").html("").attr("class", "col3");
				
				$("#ccCount").html("").attr("class", "col2");
				$("#ccDollar").html("").attr("class", "col3");
				
				$("#rtDollar").html("").attr("class", "col3"); }
			else if(listType == "alert"){ $("#alertDetailTable").html(""); }
		}}
}}

// ----------------------------------------------------------------------------------------------------

function getPOSAlertInfo(objID, eventType, curMarker, posItemID, method, listStatus) //Collects Details on Point of Sale Alert Info and hide/displays detail Box and Marker on Map
{
	var heightDiff = $("#selectedPayStation").outerHeight(true) - $("#selectedPayStation").height(),
	objBaseHeight = (currentListObj !== "userList")? 208 : 100,
	objHeight = 0,
	listHeight = $("#"+currentListObj).height()-heightDiff,
	contentHeightDiff = 0,
	$selectedDisplayArea = (currentListObj === "userList")? $("#selectedUser"): $("#selectedPayStation"),
	$otherListDisplayArea = (currentListObj === "userList")? $("#selectedPayStation") : $("#selectedUser"),
	$crntDetailObj = (currentListObj !== "userList")? $("#selectedPOSdetails") : $("#selectedUsrDetails"),
	$otherDetailObj = (currentListObj !== "userList")? $("#selectedUsrDetails") : $("#selectedPOSdetails"),
	crntDetailObjID = (currentListObj !== "userList")? "selectedPOSdetails" : "selectedUsrDetails",
	otherDetailObjID = (currentListObj !== "userList")? "selectedUsrDetails" : "selectedPOSdetails";
	
	if(method !== "light"){ listStatus = currentListObj };

	if(eventType == "clear")
	{
		$selectedDisplayArea = ($("#selectedUser").is(":visible"))? $("#selectedUser"): $("#selectedPayStation");
		var	ogSelectedDetailOuterHeight = $selectedDisplayArea.outerHeight(true),
			ogSelectedDetailHeight = $selectedDisplayArea.height(),
			currentListHeight = $(".mapItemList").height(); // USE mapItemList class instead of currentListObj for tab Switch.
		$selectedDisplayArea.animate({height: "0px", marginTop: -(ogSelectedDetailOuterHeight-ogSelectedDetailHeight)}, { duration: 800, 
			step: function(now, fx){ if(fx.prop === "height"){$(".mapItemList").height(currentListHeight+(ogSelectedDetailOuterHeight-now))}; },
			complete: function(){
				$selectedDisplayArea.hide().css("margin-top", "0px");
				insertPayStationDetails(); 
				$crntDetailObj.width(100);
				toShowInfo = false;
				soh("",[currentListObj]); }})
			.attr("serial", "");
	} else {
		objDetailUrl = new Array();
		objDetailUrl["collection"] = new Array();
		objDetailUrl["collection"]["activeList"] = ["/secure/collections/payStationCollectionsNeeded.html", {pointOfSaleId: objID}];
		objDetailUrl["collection"]["recentList"] = ["/secure/collections/payStationRecentCollections.html", {collectionId: posItemID}];
		objDetailUrl["collection"]["userList"] = ["/secure/collections/viewCollectionUserDetail.html", {randomId: objID}];
		objDetailUrl["alert"] = new Array();
		objDetailUrl["alert"]["activeList"] = ["/secure/alerts/payStationActiveAlerts.html", {pointOfSaleId: objID}];
		objDetailUrl["alert"]["recentList"] = ["/secure/alerts/resolvedAlert.html", {posAlertId: posItemID}];
		
		$.ajax({
			url: objDetailUrl[listType][listStatus][0]+"?"+document.location.search.substring(1),
			dataType: "json",
			data: objDetailUrl[listType][listStatus][1],
			success: function(data) {								
				if(listStatus === "userList"){
					var 	$itemObj = $("#usr_"+objID),
						itemInList = ($itemObj.length)? true : false ;
						posAlertDetails = data.user;
					if(!curMarker || curMarker.options.alt !== posAlertDetails.userName){ curMarker = false; } }
				else {
					var 	$itemObj = $("#pos_"+objID),
						itemInList = ($itemObj.length)? true : false ;
						posAlertDetails = data.payStationInfo;
					if(!curMarker || curMarker.options.alt !== posAlertDetails.serial){ curMarker = false; } }

				if(eventType == "new")
				{
					insertPayStationDetails(posAlertDetails, curMarker, posItemID, objID, method, listStatus);
					if(method !== "light") {						
						if(itemInList){ $itemObj.addClass("selected"); }
						$selectedDisplayArea.height(0).show();
						sow([crntDetailObjID]);
						var objContentHeight = $crntDetailObj.height();
						if(objContentHeight > objBaseHeight){ objHeight = objContentHeight+8; }
						else{ objHeight = objBaseHeight; }
						
						var $listBox = $("#" + listStatus);
						var targetListHeight = listHeight - objHeight;
						var targetListScrollTop = calculateNewScrollTop($listBox, $listBox.find("li.selected:first"), $listBox.offset().top + objHeight, targetListHeight);
						
						$selectedDisplayArea.animate({ "height": objHeight+"px" }, {
							"duration": 800
						});
						$listBox.animate({ "height": targetListHeight + "px", "scrollTop": targetListScrollTop }, {
							"duration": 800
						});
					}
				}
				else if(eventType == "update")
				{
					if(listStatus === "userList"){
						var 	$itemObj = $("#usr_"+objID); }
					else {
						var 	$itemObj = $("#pos_"+objID); }
					
					$(".mapItemList li.selected").removeClass("selected"); $itemObj.addClass("selected");
					if(listStatus !== "userList"){ $("#selectedPOSimg").fadeOut(400); }
					$crntDetailObj.fadeOut(400,function(){
						var objHeight = $selectedDisplayArea.height(),
						ogCntntHeight = $crntDetailObj.height()>objBaseHeight? $crntDetailObj.height() : objBaseHeight,
						ogListHeight = $("#"+currentListObj).height(),
						cntntHeightDiff = 0;
						
						insertPayStationDetails(posAlertDetails, curMarker, posItemID, objID, method, listStatus);
						
						var newCntntHeight = $crntDetailObj.height();
						if(ogCntntHeight != newCntntHeight){
							if(ogCntntHeight > newCntntHeight && newCntntHeight >= objBaseHeight){ 
								cntntHeightDiff = ogCntntHeight - newCntntHeight;
								objHeight = objHeight - cntntHeightDiff; } 
							else if(ogCntntHeight < newCntntHeight){ 
								cntntHeightDiff = newCntntHeight - ogCntntHeight;
								objHeight = objHeight + cntntHeightDiff; } 
							else {
								cntntHeightDiff = ogCntntHeight - objBaseHeight;
								objHeight = objBaseHeight; }
							
							var $listBox = $("#" + currentListObj);
							var targetListScrollTop = calculateNewScrollTop($listBox, $listBox.find("li.selected:first"), $listBox.offset().top, $listBox.height());
							
							$selectedDisplayArea.animate({ "height": objHeight + "px" }, {
								"duration": 800,
								"complete": function() { toShowInfo = false; }
							});
							$listBox.animate({ "scrollTop": targetListScrollTop }, {"duration": 800});
						}
						if(listStatus !== "userList"){
							$("#selectedPOSdetails, #selectedPOSimg").fadeIn(400, function(){ toShowInfo = false; }); }
						else{ $crntDetailObj.fadeIn(400, function(){ toShowInfo = false; }); }
					});
				}
				toShowInfo = false; },
			error: function(){ alertDialog(systemErrorMsg); toShowInfo = false; }
		});
	}
}

// ----------------------------------------------------------------------------------------------------

function mapMarkers(preloadAll, listStatus) //Creates Map Marker
{
	var mapedListUrl = new Array();
	mapedListUrl["collection"] = new Array();
	mapedListUrl["collection"]["activeList"] = "/secure/collections/viewCollectionAlertsMap.html";
	mapedListUrl["collection"]["recentList"] = "/secure/collections/viewCollectionsMadeMap.html";
	mapedListUrl["collection"]["userList"] = "/secure/collections/viewCollectionUserMap.html";
	mapedListUrl["alert"] = new Array();
	mapedListUrl["alert"]["activeList"] = "/secure/alerts/viewPayStationAlertsMap.html";
	mapedListUrl["alert"]["recentList"] = "/secure/alerts/viewPayStationClearedAlertsMap.html";

	posMarkers = new Array();
	posMarkers["activeList"] = new Array();
	posMarkers["recentList"] = new Array();
	posMarkers["userList"] = new Array();

	var params = $("#listFilter").serialize();

	var ajaxPOSAlertItemList = GetHttpObject({"showLoader": false});
	ajaxPOSAlertItemList.onreadystatechange = function(){
		if (ajaxPOSAlertItemList.readyState==4 && ajaxPOSAlertItemList.status == 200){
			var results = true;
			var data = JSON.parse(ajaxPOSAlertItemList.responseText);
			if(!data.errorStatus){
				var posItemListJson;
				if(listStatus !== "userList"){ posItemListJson = data.list[0].alertEntry; }
				else{ posItemListJson = data.UserList[0].user; }

				if(posItemListJson && posItemListJson != "") {
					if(posItemListJson instanceof Array === false){ posItemListJson = [posItemListJson]; }
					if(posItemListJson.length === 0){ results = false; }

					for(x = 0; x < posItemListJson.length; x++){ 
						var posList = posItemListJson[x];
						var	latlongPOS = "lat='' long='' ",
						itemID = "",
						randomID = (posList.posRandomId)? posList.posRandomId : posList.randomId,
						dateStrng = (posList.resolvedDate)? posList.resolvedDate : posList.lastSeen;
						
				//alertThresholdTypeId, lastSeen, revenueType
				//alertDate, alertRandomId, module, resolvedDate
						if(listStatus != "userList"){
							if(!posMarkerMap[posList.serial] && posMarkerMap[posList.serial] !== ""){ posMarkerMap[posList.serial] = randomID; }
						
							if(posList.latitude){
							//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
							posMarkers[listStatus].push(new mapMarker(
								posList.latitude, 
								posList.longitude, 
								posList.pointOfSaleName,
								posList.payStationType, 
								randomID,
								posList.severity,
								dateStrng,'','','','',
								posList.location,
								posList.serial ));
								
								latlongPOS = "lat='"+ posList.latitude +"' long='"+ posList.longitude +"' "; }} 
						else{
							var usrList = posList;
							if(!posMarkerMap[usrList.userName] && posMarkerMap[usrList.userName] !== ""){ posMarkerMap[usrList.userName] = randomID; }
							
							if(usrList.latitude){
							//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
							posMarkers[listStatus].push(new mapMarker(
								usrList.latitude, 
								usrList.longitude, 
								usrList.firstName+' '+usrList.lastName,
								'', 
								randomID,
								usrList.statusId,'','','','','','',
								usrList.userName )); 
								
								latlongPOS = "lat='"+ usrList.latitude +"' long='"+ usrList.longitude +"' "; }}}
								
					if(listType == "collection"){ initMapCollections("map", posMarkers[listStatus], listStatus, preloadAll); }
					else{ initMapAlerts("map", posMarkers[listStatus], listStatus, preloadAll); } }
				else { results = false; } }
			else{ results = false; }
			if(results === false){ 
				if(listType == "collection"){ initMapCollections("map", "", listStatus, preloadAll); }
				else{ initMapAlerts("map", "", listStatus, preloadAll); } } }
		else if(ajaxPOSAlertItemList.readyState==4){ 
			if(listType == "collection"){ initMapCollections("map", "", listStatus, preloadAll); }
			else{ initMapAlerts("map", "", listStatus, preloadAll); }
			if(listType === "alert" && currentListObj !== "recentList"){ alertDialog(mapDataErrorMsg); }} };
	ajaxPOSAlertItemList.open("POST",mapedListUrl[listType][listStatus],true);
	ajaxPOSAlertItemList.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxPOSAlertItemList.send(params);
}

// ----------------------------------------------------------------------------------------------------

var URL_PAGINATETAB = new Array();
URL_PAGINATETAB["collection"] = new Array();
URL_PAGINATETAB["collection"]["activeList"] = ["/secure/collections/viewCollectionAlerts.html"];
URL_PAGINATETAB["collection"]["recentList"] = ["/secure/collections/viewCollectionsMade.html"];
URL_PAGINATETAB["collection"]["userList"] = ["/secure/collections/viewCollectionUserList.html"];
URL_PAGINATETAB["alert"] = new Array();
URL_PAGINATETAB["alert"]["activeList"] = ["/secure/alerts/viewPayStationAlertsDetail.html"];
URL_PAGINATETAB["alert"]["recentList"] = ["/secure/alerts/filterActiveAlerts.html"];

function getCurrentListObj() {
	if(!currentListObj) {
		if($("#activeList").is(":visible")) {
			currentListObj = "activeList";
			$("#alertList").val("true");
		}
		else {
			currentListObj = "recentList";
			$("#alertList").val("false");
		}
	}
	else {
		if(currentListObj == "activeList") {
			$("#alertList").val("true");
		}
		else {
			$("#alertList").val("false");
		}
	}
	
	return $("#" + currentListObj);
}

function extractListResponse(rawResponse, processingCtx) {
	var result = null;
	
	var data = JSON.parse(rawResponse);
	
	var dataContainer = null;
	if(currentListObj === "userList") {
		dataContainer = (listType === "collection") ? data.userAccountList : null;
	}
	else if(currentListObj === "activeList"){
		dataContainer = data.alertsList;
	}
	else if(currentListObj === "recentList") {
		dataContainer = (listType === "collection") ? data.collectionsList : data.alertsList;
	}
	
	if(!dataContainer) {
		result = "";
	}
	else {
		result = dataContainer.elements;
		processingCtx.page = dataContainer.page;
		processingCtx.dataKey = dataContainer.dataKey;
	}
	
	return (!result) ? [] : ((Array.isArray(result)) ? result : [ result ]);
}

function locatePaginatetab() {
	/* For userList, what returned from this function is paginateTab with only 1 page.
	 * This is because business-side does not do pagination on purpose. The reason behind this is the sort-order of this list.
	 * Currently, the list is sorted by last communicate time which will change everytime the user do things on their device.
	 * This will cause the list to be inconsistent because the order that each user appeared on the list always changing.
	 * 
	 * Since, the sort-order right now make sense and there are very unlikely for a company to have more than 100 digital collect users,
	 * The pagination is disabled to prevent incosistency. If we have to do pagination on this list
	 * , we have to come up with the solution to make the order of user in the list stay the same between requests.
	 */
	var $currentListObj = getCurrentListObj();
	$currentListObj.paginatetab({
		"url": URL_PAGINATETAB[listType][currentListObj],
		"formSelector": "#listFilter",
		"rowTemplate": markerTemplateFn,
		"emptyMessage": (currentListObj === "userList") ? noUserMsg : ((currentListObj === "recentList") ? noRecentMsg : noPaystationsMsg),
		"itemLocatorURL": URL_PAGINATETAB[listType][currentListObj],
		"itemLocatorParamTemplate": "wrappedObject.targetId=[%= id %]",
		"responseExtractor": extractListResponse,
		"pagingAndOrdering": function(page, sortParams, dataKey) {
			var result = [];
			if(page) {
				result.push("wrappedObject.pageNumber=" + page);
			}
			if(dataKey) {
				result.push("wrappedObject.dataKey=" + dataKey);
			}
			
			return result.join("&");
		}
	});
	
	return $currentListObj;
}

// ----------------------------------------------------------------------------------------------------

function updateList(callback) //Determins Current list and sets "isFiltered" style and calls filterPOSList()
{
	var preloadAll = false;
	if($("#selectedPayStation").is(":visible")){ $("li.selected").trigger('click'); }
	if($("#activeList").is(":visible") && currentListObj === ""){ currentListObj = "activeList"; preloadAll = true; } 
	if(!$("#"+currentListObj).hasClass("isFiltered")){$("#"+currentListObj).addClass("isFiltered")}
	if(currentListObj === "userList"){ 
		$("#showUserCheck").hide(); 
		if(prevTab === "activeList"){ 
			$("#activeCheck").find("a").addClass("checked"); $("#activeCheck").find("input").val(true);
			$("#resolvedCheck").find("a").removeClass("checked"); $("#resolvedCheck").find("input").val(false); }
		else if(prevTab === "recentList"){ 
			$("#activeCheck").find("a").removeClass("checked"); $("#activeCheck").find("input").val(false);
			$("#resolvedCheck").find("a").addClass("checked"); $("#resolvedCheck").find("input").val(true); }
		else{ preloadAll = true; }
		$("#showPayStationCheck").show();
	}
	else{ 
		$("#showPayStationCheck").hide();
		$("#showUserCheck").show();
	}
	
	locatePaginatetab().paginatetab("reloadData", callback);
	mapMarkers(preloadAll, currentListObj);
}

// ----------------------------------------------------------------------------------------------------

function selectEventItem(itemID){
	var $itemToSelect = $('#pos_'+itemID);
	if($itemToSelect.length > 0) {
		$itemToSelect.trigger('click');
	}
	else {
		locatePaginatetab().paginatetab("reloadPageWithItem", { "id": itemID }, function() {
			$('#pos_'+itemID).trigger('click');
		});
	}
	
	return false;
}

// ----------------------------------------------------------------------------------------------------

function hideFilter($filterBox)
{
	if($("#selectedPayStation, #selectedUser").is(':animated')){ setTimeout(function(){ hideFilter($filterBox); }, 500); return false; }
	else{
		var ogHeight = $filterBox.height();
		var currentListHeight = $(".mapItemList").height();
		$filterBox.animate({height: "0px"}, {duration: 800, 
		step: function(now, fx){ if(fx.prop === "height"){$(".mapItemList").height(currentListHeight+(ogHeight-now))}; },
		complete: function(){
			$filterBox.hide().css("margin-top", "0px");
			soh("",[currentListObj]); }}) } }

// ----------------------------------------------------------------------------------------------------

function showFilter($filterBox){
	if(!$filterBox.is(":visible")){ 
		if($("#selectedPayStation, #selectedUser").is(':animated')){ setTimeout(function(){ showFilter($filterBox); }, 500); return false; }
		else{
			var listHeight = $(".mapItemList").height();
			$filterBox.height(0).show();
			$filterBox.animate({height: "65px"},{
				duration: 800, 
				step: function(now, fx){ $(".mapItemList").height(listHeight-now); },
				complete: function(){ soh("",[currentListObj]); }}); } } }

// ----------------------------------------------------------------------------------------------------

function switchCollTab($parentDiv, listObjct, newListObj){
	$parentDiv.siblings("div.tabSection").find("a").addClass("disabled");
	$("#secondNav").find("div.current").removeClass("current");
	$parentDiv.addClass("current");

	$parentDiv.siblings("div.tabSection").find("a").addClass("disabled");
	prevTab = $("#secondNav").find("div.current > a").attr("id").replace("Tab", "");
	$("#secondNav").find("div.current").removeClass("current");
	$parentDiv.addClass("current");

	locatePaginatetab().paginatetab("clearData", { 
		"showLoadingPanel": true, 
		"callback": function(){ 
			currentListObj = newListObj;
	
			$("ul.mapItemList").attr("id", currentListObj);
			if($("#selectedPayStation, #selectedUser").is(":visible")){ getPOSAlertInfo("", "clear"); }
			
			if(mapInPage){ markerGroups[prevTab].eachLayer( function(marker){ marker.setZIndexOffset(0); marker.closePopup().unbindPopup(); } ); }
			
			$("#payStationListArea").find("header > h2").html(listObjct[currentListObj]);
			if(currentListObj === "activeList"){ $("#saveActiveList").fadeIn(); } else { $("#saveActiveList").fadeOut("fast"); } 
	
			$("#locationRouteAC")
				.autoselector("reset", true)
				.trigger('blur').siblings("label").show();
	
			var $filterBox = $("#payStationListArea").find(".filterHeader");
			if(currentListObj === "userList"){ hideFilter($filterBox); }
			else{ showFilter($filterBox); }
	
		//clear Filter Form	
			$("#coinCheckHidden").val("false");
			$("#coinCheck").removeClass("checked");
			$("#billsCheckHidden").val("false");
			$("#billsCheck").removeClass("checked");
			$("#ccardCheckHidden").val("false");
			$("#ccardCheck").removeClass("checked");
			$("#totalCheckHidden").val("false");
			$("#totalCheck").removeClass("checked");
			$("#totalAllLabel").html(totalAllLabel[currentListObj][0]);
			$("#totalAllLabel").parent("label").attr("title", totalAllLabel[currentListObj][1]);
			$("#overdueCheckHidden").val("false");
			$("#overdueCheck").removeClass("checked");
	
			if(currentListObj === "activeList" || currentListObj === "recentList"){
				if(mapInPage){ 
					mapInPage.removeLayer(markerGroups["activeList"]); 
					mapInPage.removeLayer(markerGroups["recentList"]);
					if($("#showUserCheck").find("input").val() === "false"){ mapInPage.removeLayer(markerGroups["userList"]); } 
					else{ mapInPage.addLayer(markerGroups["userList"]); } } }
			
			updateList(function(){ $parentDiv.siblings("div.tabSection").find("a").removeClass("disabled"); }); } }); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popCollAction(tempEvent, listObjct){
	var	$parentDiv = $("#"+tempEvent+"Tab").parent("div.tabSection"),
		newListObj = tempEvent;
	switchCollTab($parentDiv, listObjct, newListObj); }
///////////////////////////////////////////////////

// ----------------------------------------------------------------------------------------------------

function collections(collectionList) //Main Function for Collection Center Page
{
	var listObjct = new Array();
	listObjct['activeList'] = collectNeededTitle;
	listObjct['recentList'] = recentCollectTitle;
	listObjct['userList'] = userCollectTitle;
	
	listType = "collection";

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempEvent = "activeList";
		if(stateObj !== null){
			tempEvent = stateObj.action; }
		popCollAction(tempEvent, listObjct); }
	
	if(!popStateEvent && (collectionList !== undefined && collectionList !== '')){ popCollAction(collectionList, listObjct); }
	else{ 
		currentListObj = $("#secondNav").find("div.current > a").attr("id").replace("Tab", "");
		updateList(); }
///////////////////////////////////////////////////
	
	$("#secondNav").find("a").on('click', function(event){
		if(!$(this).hasClass("disabled")){
			if($(this).attr("id") !== "posSummaryList"){ event.preventDefault();
				var $parentDiv = $(this).parent("div.tabSection");
				if(!$parentDiv.hasClass("current")){ 
					var	newListObj = $(this).attr("id").replace("Tab", ""),
						stateObj = { action: newListObj };
					crntPageAction = stateObj.action;
					history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action);
					switchCollTab($parentDiv, listObjct, stateObj.action); } } }});

	$("#secondNav").find("a").on('click', function(event){
		if(!$(this).hasClass("disabled")){
			if($(this).attr("id") !== "posSummaryList"){ event.preventDefault(); 
				var $parentDiv = $(this).parent("div.tabSection");
				if(!$parentDiv.hasClass("current")){
					var	newListObj = $(this).attr("id").replace("Tab", "");
						stateObj = { action: newListObj };
					crntPageAction = stateObj.action;
					history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action);
					switchAlertTab($parentDiv, listObjct, stateObj.action); } } } });
	
	$(document.body).on("click", ".mapItemList li", function(event){ event.preventDefault();
		if(!$(this).hasClass("notclickable") && !toShowInfo){
			var	markerMatch = false,
				posCollID = $(this).attr("id").split("_")[1],
				posCollSN = (currentListObj !== "userList")? $(this).attr("serial") : $(this).attr("username"),
				posCollection = $(this).attr("itemID"),
				posCollLat = $(this).attr("lat"),
				posCollLong = $(this).attr("long");

			 toShowInfo = true;

			if((posCollLat !== "" && posCollLat !== "undefined" )&& (posCollLong !== "" && posCollLong !== "undefined" )){ 
				mapInPage.panTo(new L.LatLng(posCollLat, posCollLong)); } 
			else { curMarker = false; }
			
			markerGroups[currentListObj].eachLayer( function(marker){ 
				if(marker.options.alt == posCollSN /*&& currentListObj != "recentList"*/){ curMarker = marker; markerMatch = true;  } 
				else { marker.setZIndexOffset(0); marker.closePopup().unbindPopup(); }} );
			
			if($(this).hasClass("selected")){ 
				//hide selected paystation details
				$(this).removeClass("selected");
				getPOSAlertInfo(posCollID, "clear");
				if(markerMatch != false){ curMarker.closePopup().unbindPopup(); } } 
			else {
				var status = "";
				if($(".mapItemList li.selected").length > 0){
					status = "update";
					$(".mapItemList li.selected").removeClass("selected"); $(this).addClass("selected"); } 
				else { 
					status = "new";
					$(this).addClass("selected"); }
				getPOSAlertInfo(posCollID, status, curMarker, posCollection); }}
	});
	
	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("ul#collectionFilterForm").length){ updateList(); } });
	
	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("#showUserCheck").length){ 
		if($(this).siblings("input").val() === "false"){ mapInPage.removeLayer(markerGroups["userList"]); } 
		else{ mapInPage.addLayer(markerGroups["userList"]); } }});

	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("#activeCheck").length){ 
		if($(this).siblings("input").val() === "false"){ mapInPage.removeLayer(markerGroups["activeList"]); } 
		else{ mapInPage.addLayer(markerGroups["activeList"]); } }});
	
	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("#resolvedCheck").length){ 
		if($(this).siblings("input").val() === "false"){ mapInPage.removeLayer(markerGroups["recentList"]); } 
		else{ mapInPage.addLayer(markerGroups["recentList"]); } }});	
	
	$(document.body).on("click", "#saveActiveList", function(event) {
		event.preventDefault();
		exportAlerts("/secure/alerts/collectionAlertsDetailReport.html", $("#listFilter"), event);
	});
	
//	locatePaginatetab().paginatetab("reloadData");
	
	$("#collDetailBtn").on('click', function(event){ event.preventDefault();
		var ajaxCollectionReport = GetHttpObject();
		ajaxCollectionReport.onreadystatechange = function()
		{
			if (ajaxCollectionReport.readyState==4 && ajaxCollectionReport.status == 200){
				if($("#collectionDetailView.ui-dialog-content").length){$("#collectionDetailView.ui-dialog-content").dialog("destroy");}
				$("#collectionDetailView").html(ajaxCollectionReport.responseText);
				var collectionViewBtn = {};
				collectionViewBtn.btn1 = { text: closeMsg, click: function(event, ui) { $(this).scrollTop(0); $(this).dialog( "close" ); } };
				$("#collectionDetailView").dialog({
					title: collectionReportTitle,
					modal: true,
					resizable: false,
					closeOnEscape: false,
					maxHeight:  $(window).innerHeight()-150,
					width: 475,
					hide: "fade",
					buttons: collectionViewBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }
				}); 
				btnStatus(closeMsg);	
			} else if(ajaxCollectionReport.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxCollectionReport.open("GET","/secure/settings/locations/payStationList/payStationDetails/collectionReport.html?collectionId="+$(this).attr("collectionID") +"&"+ document.location.search.substring(1),true);
		ajaxCollectionReport.send();
	});
}

// ----------------------------------------------------------------------------------------------------

function switchAlertTab($parentDiv, listObjct, newListObj){ 
	$parentDiv.siblings("div.tabSection").find("a").addClass("disabled");
	$("#secondNav").find("div.current").removeClass("current");
	$parentDiv.addClass("current");
				
	locatePaginatetab().paginatetab("clearData", {
		"showLoadingPanel": true,
		"callback": function() {
			currentListObj = newListObj;
				
			$("ul.mapItemList").attr("id", currentListObj);
			if($("#selectedPayStation").is(":visible")){ getPOSAlertInfo("", "clear"); }
	
			$("#payStationListArea").find("header > h2").html(listObjct[currentListObj]);
			if(currentListObj === "activeList"){ $("#saveActiveList").fadeIn(); } else { $("#saveActiveList").fadeOut("fast"); }
	
			//clear Filter Form
			$("#locationRouteAC")
				.autoselector("reset", true)
				.trigger('blur').siblings("label").show();
			
			$("#moduleAC")
				.autoselector("reset", true)
				.siblings("label").show();
			
			$("#severityAC")
				.autoselector("reset", true)
				.siblings("label").show();
			
			if(currentListObj === "activeList" || currentListObj === "recentList"){
				if(mapInPage){ mapInPage.removeLayer(markerGroups["activeList"]); mapInPage.removeLayer(markerGroups["recentList"]); }
				updateList(function() { $parentDiv.siblings("div.tabSection").find("a").removeClass("disabled"); }); } } }); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popAlertAction(tempEvent, listObjct){
	var	$parentDiv = $("#"+tempEvent+"Tab").parent("div.tabSection"),
		newListObj = tempEvent;
	switchAlertTab($parentDiv, listObjct, newListObj); }
///////////////////////////////////////////////////

// ----------------------------------------------------------------------------------------------------
	
function alerts(alertList) //Main Function for Maintanence Center Page
{
	var listObjct = new Array();
	listObjct['activeList'] = currentAlertTitle;
	listObjct['recentList'] = resolvedAlertTitle;
	listObjct['userList'] = alertUserTitle;

	listType = "alert";

	//LOCATION ROUTE AUTO COMPLETE STANDARD 
	$("#moduleAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": true,
		"valueContainerSelector": "#moduleAC\\-id",
		"data": moduleOptions })
	.on("itemSelected", function(event, selectedItem) {
		updateList();
	});
	
	$("#severityAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": true,
		"valueContainerSelector": "#severityAC\\-id",
		"data": severityOption })
	.on("itemSelected", function(event, selectedItem) {
		updateList();
	});

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempEvent = "activeList";
		if(stateObj !== null){
			tempEvent = stateObj.action; }
		popAlertAction(tempEvent, listObjct); }
	
	if(!popStateEvent && (alertList !== undefined && alertList !== '')){ popAlertAction(alertList, listObjct); }
	else{ 
		currentListObj = $("#secondNav").find("div.current > a").attr("id").replace("Tab", "");	
		updateList(); }
///////////////////////////////////////////////////
	
	
	$("#secondNav").find("a").on('click', function(event){
		if(!$(this).hasClass("disabled")){
			if($(this).attr("id") !== "posSummaryList"){ event.preventDefault();
				var $parentDiv = $(this).parent("div.tabSection");
				if(!$parentDiv.hasClass("current")){ 
					var	newListObj = $(this).attr("id").replace("Tab", ""),
						stateObj = { action: newListObj };
					crntPageAction = stateObj.action;
					history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action);
					switchAlertTab($parentDiv, listObjct, stateObj.action); } } }});
	
	$(document.body).on("click", ".mapItemList li", function(event){ event.preventDefault();
		if(!$(this).hasClass("notclickable") && !toShowInfo){
			var	markerMatch = false,
				posAlrtID = $(this).attr("id").split("_")[1],
				posAlrtSN = $(this).attr("serial"),
				posAlert = $(this).attr("itemID"),
				posAlrtLat = $(this).attr("lat"),
				posAlrtLong = $(this).attr("long");
			
			toShowInfo = true;
	
			if((posAlrtLat != "" && posAlrtLong != "") && currentListObj != "recentList"){ mapInPage.panTo(new L.LatLng(posAlrtLat, posAlrtLong)); } 
			else { curMarker = false; }
			
			markerGroups[currentListObj].eachLayer( function(marker){ 
				if(marker.options.alt == posAlrtSN && currentListObj != "recentList"){ curMarker = marker; markerMatch = true;  } 
				else { marker.setZIndexOffset(0); marker.closePopup().unbindPopup(); }} );
			
			if($(this).hasClass("selected")){ 
				//hide selected paystation details
				$(this).removeClass("selected");
				getPOSAlertInfo(posAlrtID, "clear");
				if(markerMatch != false){ curMarker.closePopup().unbindPopup(); } } 
			else {
				var status = "";
				if($(".mapItemList li.selected").length > 0){ 
					status = "update";
					$(".mapItemList li.selected").removeClass("selected"); $(this).addClass("selected"); } 
				else { 
					status = "new";
					$(this).addClass("selected"); }
				getPOSAlertInfo(posAlrtID, status, curMarker, posAlert); }}
	});
	
	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("ul#alertFilterForm").length){ updateList(); } });
	
	$(document.body).on("click", ".removeFile", function(event){ event.preventDefault();
		var clearedAlertID = $(this).attr("id").split("_")[1];
		$.ajax({
			url: "/secure/alerts/clearAlert.html"+"?"+document.location.search.substring(1),
			data: {activePosAlertId: clearedAlertID},
			success: function(data) { 
					$("#clearItem_"+clearedAlertID).slideUp( function(){ 
						$(this).remove(); 
						$("#mapAlert_"+clearedAlertID).fadeOut("fast"); 
						var alertCount = $(".removeFile").length;
						if(alertCount > 1) { 
							alertCount = alertCount - 1; 
							$(".selected").find(".alertTypeMsg").html(multipleLbl); }
						else if(alertCount === 1) { $(".selected").find(".alertTypeMsg").html($("#alertDetailTable").children("li:first").attr("module")); }
						else { 
							curObj = $(".selected");
							curObj.trigger('click').slideUp( function(){ $(this).remove(); markerGroups[currentListObj].removeLayer(curMarker); }); } }); },
			error: function(){ alertDialog(systemErrorMsg); }
		});
	 });
	
	$(document.body).on("click", "#saveActiveList", function(event) {
		event.preventDefault();
		exportAlerts("/secure/alerts/payStationAlertsDetailReport.html", $("#listFilter"), event);
	});
}

function exportAlerts(url, form, event) {
	event.preventDefault();
	
	var params = $(form).serialize();
	var request = GetHttpObject();
	request.responseContext = {};
	
	request.onprocesscompleted = function(responseFalse, displayedMessages) {
		if(!responseFalse) {
			var obj = parseJSON(request.responseContext.responseText);
			if(obj.report && obj.report.reportHistoryId) {
				downloadFile("/secure/reporting/viewAdhocReport.html?" + scrubQueryString(document.location.search.substring(1), "all") + "&historyId=" + obj.report.reportHistoryId);
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
		else if(!displayedMessages) {
			alertDialog(systemErrorMsg);
		}
	};
	
	request.open("POST", url, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send(params);
}

// ----------------------------------------------------------------------------------------------------

function extractDataKey(activityMsgs, context) {
	return context.dataKey;
}

function extractPOSList(posList, context) {
	if($("#Collections").length){ 
		var buffer = JSON.parse(posList).collectionsList; }
	else if($("#Maintenance").length){
		var buffer = JSON.parse(posList).alertList; }

	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	return result;
}

function getPOSSummaryList(listURL)
{
	$("#posSummaryList")
		.paginatetab({
			"url": listURL,
			"formSelector": "#posSummaryFilterForm",
			"postTokenSelector": "#postToken",
			"rowTemplate": unescape($("#posSummaryListTemplate").html()),
			"emptyMsgTemplate": unescape("<li class='alignCenter notclickable'><span class='textLine'>"+noPaystationsMsg+"</span></li>"),
			"responseExtractor": extractPOSList,
			"dataKeyExtractor": extractDataKey
		})
		.paginatetab("setupSortingMenu", "#posSummaryListHeader")
		.paginatetab("reloadData");
}

function setSevrityDefault(loadList)
{
	var $selLi = $("#severityList").find("li:first");
	$("#filterSeverity").val($selLi.attr("id"));
	$("#filterSeverityLabel").html($selLi.html());
	if(loadList){ $("#posSummaryList").paginatetab("reloadData"); } }

function posSummary(){
	if($("#Collections").length){ 
		getPOSSummaryList("/secure/collections/posSummaryList.html"); } 
	else if($("#Maintenance").length){
		getPOSSummaryList("/secure/alerts/posSummaryList.html"); } 
			
	$("#filterLocationRoute").autoselector({
		"isComboBox": true, 
		"defaultValue": "all",
		"shouldCategorize": true,
		"selectedCSS": "autoselector-selected",
		"data": JSON.parse($("#searchLocationData").text()) })
	.on("itemSelected", function(event, ui) {
		$("#posSummaryList").paginatetab("reloadData");
	});
						
	$(".filterHeader").on("mouseenter mouseleave", ".filterForm > li", function(event){
		var	$menu = $(this).children(".filterMenu"),
			$title =  $(this).find(".filterTitle");
		if(event.type === "mouseenter"){
			var stringCount = 0;
			var xHeight = parseInt($menu.find("li").css("font-size"));
			var ogWidth = $menu.width();
			$menu.find("li").each(function(){ tempCount = $(this).html().length; if(tempCount > stringCount){ stringCount = tempCount} });
			var listWidth = ((stringCount * (xHeight / 1.125)) > ogWidth)? stringCount * (xHeight/1.125) : ogWidth ; 
			$menu.width(listWidth).fadeIn(1, function(){$title.css("z-index", 1055);});  }
		else if(event.type === "mouseleave"){ $title.css("z-index", 40); $menu.width("100%").hide(); }
	});
	
	$(".filterHeader").on("click", ".filterMenu > ul > li", function(event){
		var $this = $(this);
		if(!$this.hasClass("selected"))
		{
			$this.siblings("li").removeClass("selected");
			$this.addClass("selected");
			$this.parents(".filterMenu").siblings(".filterTitle").children(".logValue").html("<a href='#' title='Undo Filter' class='undo'>" + $this.html() + "</a>");
			$("#filterSeverity").val($this.attr("id"));
		}
		$("#posSummaryList").paginatetab("reloadData");
	});
	
	if($("#filterSeverity").val().length == 0) { setSevrityDefault(false); }
	
	$("#filterSeverityLabel").on("click", ".undo", function(event){ event.preventDefault(); setSevrityDefault(true); }); }
