function reloadPage() {
	window.setTimeout(function() {
		window.location.reload();
	}, 1200);
}

function bannedCard() {
	msgResolver.mapAttribute("cardNumber", "#formBannedCardNumber");
	msgResolver.mapAttribute("cardType", "#formBannedCardType");
	msgResolver.mapAttribute("cardExpiry", "#formBannedCardExpiry");
	msgResolver.mapAttribute("comment", "#formBannedCardComment");
	
	msgResolver.mapAttribute("file", "#formBannedCardFile");
	
	var $filterCardType = $("#filterCardType");
	// Initialize Filter Menu
	$(".filterHeader").on("mouseenter mouseleave", ".filterForm > li", function(event){
		var	$menu = $(this).children(".filterMenu"),
			$title =  $(this).find(".filterTitle");
		if(event.type === "mouseenter"){
			var stringCount = 0;
			var xHeight = parseInt($menu.find("li").css("font-size"));
			var ogWidth = $menu.width();
			$menu.find("li").each(function(){ tempCount = $(this).html().length; if(tempCount > stringCount){ stringCount = tempCount} });
			var listWidth = ((stringCount * (xHeight / 1.125)) > ogWidth)? stringCount * (xHeight/1.125) : ogWidth ; 
			$menu.width(listWidth).fadeIn(1, function(){$title.css("z-index", 1055);});  }
		else if(event.type === "mouseleave"){ $title.css("z-index", 40); $menu.width("100%").hide(); }
	});
	
	$(".filterHeader").on("click", ".filterMenu > ul > li", function(event){
		var $this = $(this);
		if(!$this.hasClass("selected"))
		{
			$this.siblings("li").removeClass("selected");
			$this.addClass("selected");
			$this.parents(".filterMenu").siblings(".filterTitle").children(".logValue").html("<a href='#' title='Undo Filter' class='undo'>" + $this.html() + "</a>");
			
			$filterCardType.val($this.attr("id"));
		}
	});
	
	if($filterCardType.val().length == 0) {
		var $selLi = $("#cardTypesList").find("li:first");
		
		$filterCardType.val($selLi.attr("id"));
		$("#filterCardTypeLabel").html($selLi.html());
	}
	
	/* Extract CardType's JSON */
	var $formBannedCardType = $("#formBannedCardType")
		.autoselector({
			"isComboBox": true,
			"data": "#formBannedCardTypeData"
		})
		.on("itemSelected", function(event, selectedItem) {
			updateCardTypeVisibility(selectedItem.extra);
		});
	
	$("#formBannedCardTypeData").remove();
	
	$("#bannedCardListContainer")
		.paginatetab({
			"url": LOC.searchBannedCard,
			"formSelector": "#bannedCardSearchForm",
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": $("#bannedCardTemplate").html(),
			"responseExtractor": extractBannedCardsList,
			"triggerSelector": "#bannedCardSearchForm .search"
		})
		.paginatetab("setupSortingMenu", "#bannedCardListContainerHeader");;
	
	$("#bannedCardSearchForm").on('submit', function(event){ event.preventDefault(); $("#bannedCardSearchForm").find(".search").trigger('click'); })
	
	/* Create BannedCard's form dialog */
	$("#bannedCardDialog")
		.dialog({
			"title": CM_LABEL.addFormHeader,
			"modal": true,
			"resizable": false,
			"closeOnEscape": false,
			"width": 300,
			"height": 340,
			"hide": "fade",
			"zIndex": 3000,
			"autoOpen": false,
			"stack": true,
			"buttons": {
				"save": {
					"text": saveBtn,
					"click": saveBannedCard
				},
				"cancel": {
					"text": cancelBtn,
					"click": function(event) { event.preventDefault(); cancelConfirm($(this)); } 
				}
			},
			"dragStop": function(){ checkDialogPlacement($(this)); }
		})
		.on("dialogopen", function() {
			updateCardTypeVisibility($formBannedCardType.autoselector("getSelectedObject").extra);
		});
	
	btnStatus(saveBtn);
	
	/* Create ImportBannedCard's form dialog */
	var $importForm = $("#bannedCardUploadForm")
		.dptupload({
			"postTokenSelector": "#uploadPostToken",
			"url": "/secure/cardManagement/importBannedCard.html?" + document.location.search.substring(1)
		})
		.on("uploadSuccess", function(event, responseContext) {
			if(!responseContext.displayedMessages) {
				noteDialog(CM_LABEL.importSuccess);
				reloadPage();
			}
			else {
				window.location.reload();
			}
		});
	
	var $importDialog = $("#bannedCardImportDialog").dialog({
		"title": CM_LABEL.importFormHeader,
		"modal": true,
		"resizable": false,
		"closeOnEscape": false,
		"width": 350,
		"height": 490,
		"hide": "fade",
		"zIndex": 3000,
		"autoOpen": false,
		"stack": true,
		"buttons": {
			"save": {
				"text": CM_LABEL.uploadBtn,
				"click": function(event) {
					$importForm
							.dptupload("option", "triggerSelector",  (event) ? event.target : null)
							.dptupload("upload");
				}
			},
			"cancel": {
				"text": cancelBtn,
				"click": function(event){ event.preventDefault(); cancelConfirm($(this)); }
			}
		},
		"dragStop": function(){ checkDialogPlacement($(this)); }
	});
	
	btnStatus(CM_LABEL.uploadBtn);
	
	$("#formUploadStatus").autoselector({
		"isComboBox": true,
		"defaultValue": "Merge",
		"data": "#formUploadStatusData"
	});
	
	scrollListWidth(["bannedCardListContainer"]);
	
	/* Prepare menu actions */
	$("#menu_bannedCard")
		.on("click", ".add", function(event) {
			event.preventDefault();
			showBannedCardDialog();
			return false;
		})
		.on("click", ".import", function(event) {
			event.preventDefault();
			showImportDialog();			
			return false;
		})
		.on("click", ".export", function(event) {
			event.preventDefault();
			exportBannedCards();			
			return false;
		});
	
	$("#bannedCardListContainer")
		.on("click", ".delete", function(event) {
			event.preventDefault();
			deleteBannedCard($(this).attr("id").substring(6));			
			return false;
		});
	
	$("#bannedCardSearchForm .search").on("click", function(event) {
		event.preventDefault();
		searchBannedCard(event);		
		return false;
	});
	
	// Display error import messages
	var errorData = $("#_importErrors").text().trim();
	if(errorData.length > 0) {
		errorData = JSON.parse(errorData);
		if(errorData && errorData.errorStatus.errorStatus) {
			$importDialog.dialog("open");
			msgResolver.displaySerMsg(errorData.errorStatus.errorStatus);
		}
	}
	
	// Perform Search
	searchBannedCard();
	
	$("#filterCardNumber").on("keyup", function(event) {
		if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
	});
	
	$("#searchClearBtn").on('click', function(event){ event.preventDefault(); $("#filterCardNumber").val(""); searchBannedCard(); $(this).hide(); });
}

function extractBannedCardsList(responseText) {
	var result = null;
	
	var responseJSON = JSON.parse(responseText);
	if(responseJSON && responseJSON.bannedCardList) {
		result = responseJSON.bannedCardList.bannedCardInfo;
	}
	
	return result;
}

function searchBannedCard(event) {
	$("#bannedCardListContainer").paginatetab("reloadData");
	
	return false;
}

function saveBannedCard(event) {
	var params = $("#bannedCardForm").serialize();
	var request = GetHttpObject({
		"postTokenSelector": "#editPostToken",
		"triggerSelector": (event) ? event.target : null
	});
	request.onreadystatechange = function(responseFalse) {
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
					noteDialog(saveCardSuccess, "timeOut", 2500);
					$("#bannedCardDialog").dialog("close");
					$("#bannedCardListContainer").paginatetab("reloadData");
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("POST", LOC.saveBannedCard, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send(params);	
	return false;
}

function deleteBannedCard(randomId) {
	confirmDialog({
		title: alertTitle,
		message: deleteCardConfirm,
		okCallback: function(event) {
			var request = GetHttpObject();
			request.onreadystatechange = function(responseFalse) {
				if((request.readyState === 4) && (request.status === 200)) {
					if(responseFalse) {
						alertDialog(deleteCardFail);
					}
					else {
						noteDialog(deleteCardSuccess, "timeOut", 800);
						searchBannedCard();
					}
				}
				else if(request.readyState === 4) {
					alertDialog(systemErrorMsg);
				}
			};
			
			request.open("GET", LOC.deleteBannedCard +"?customerBadCardID=" + randomId +"&"+ document.location.search.substring(1), true);
			request.send();
		}
	});
}

function showBannedCardDialog() {
	$("#bannedCardForm").find("#actionFlag").val("0");
	$("#bannedCardForm :input").filter(":not(input[type=hidden])").val("");
	$("#formBannedCardType").autoselector("reset");
	$("#bannedCardDialog").dialog("open");
	testPopFormEdit($("#bannedCardForm"), $("#bannedCardDialog"));
	$("#formBannedCardNumber").trigger('focus');
}

function showImportDialog() {
	var $bannedCardUploadForm = $("#bannedCardUploadForm");
	$bannedCardUploadForm.find(":input").filter(":not(input[type=hidden])").val("");
	var fileInputs = $bannedCardUploadForm.find("input[type=file]");
	var $file, i = fileInputs.length;
	while(--i >= 0) {
		$file = $(fileInputs[i]);
		$file.replaceWith($file.clone());
	}
	
	$("#formUploadStatus").autoselector("refreshValue");
	$("#bannedCardImportDialog").dialog("open").find("*").removeClass("error");
	testPopFormEdit($("#bannedCardUploadForm"), $("#bannedCardImportDialog"));
}

function exportBannedCards() {
	window.location = LOC.exportBannedCard + "?" + scrubQueryString(document.location.search.substring(1), "all");
}

function updateCardTypeVisibility(type) {
	var $cn = $("#formBannedCardNumber");
	if(type == "1") {
		$cn.removeClass("alphaNumChrctr").addClass("intNmbrChrctr").attr("maxlength", "16").attr("size", "16");
		$("#secCardExpiry").show();
	}
	else {
		if(type == "2") {
			$cn.removeClass("alphaNumChrctr").addClass("intNmbrChrctr").attr("maxlength", "20").attr("size", "20");
		}
		else {
			$cn.removeClass("alphaNumChrctr").addClass("intNmbrChrctr").attr("maxlength", "37").attr("size", "37");
		}
		
		$("#secCardExpiry").hide();
	}
}

function cardRefund() {
	msgResolver.mapAttribute("filterCardNumber", "#searchCardNumber");
	msgResolver.mapAttribute("filterExpiry", "#searchCardExpiry");
	msgResolver.mapAttribute("filterDateType", "#searchTxDateType");
	msgResolver.mapAttribute("filterStartDate", "#searchTxDateStart");
	msgResolver.mapAttribute("filterStartTime", "#searchTxTimeStart");
	msgResolver.mapAttribute("filterEndDate", "#searchTxDateEnd");
	msgResolver.mapAttribute("filterEndTime", "#searchTxTimeEnd");
	msgResolver.mapAttribute("filterAN", "#searchAuthorizationNumber");
	msgResolver.mapAttribute("filterMA", "#searchMerchantAccount");
	msgResolver.mapAttribute("refundCardNumber", "#formRefundCardNumber");
	msgResolver.mapAttribute("refundCardExpiry", "#formRefundCardExpiry");
	msgResolver.mapAttribute("refundAmount", "#formRefundAmount");
	
	var today = new Date();
	
	/* Create Refund Detail form dialog */
	$("#refundDetailDialog")
		.dialog({
			"title": CM_LABEL.detailFormHeader,
			"modal": true,
			"resizable": false,
			"closeOnEscape": false,
			"width": 460,
			"hide": "fade",
			"zIndex": 3000,
			"autoOpen": false,
			"stack": true,
			"buttons": {
				"save": {
					"text": CM_LABEL.refundBtn,
					"click": processRefund
				},
				"cancel": {
					"text": cancelBtn,
					"click": function(event) { event.preventDefault(); cancelConfirm($(this)); return false; } 
				}
			},
			"dragStop": function(){ checkDialogPlacement($(this)); }
		})
		.on("dialogopen", function() {
			$("input#formRefundCardNumber").trigger('focus'); //formRefundCardNumber
		});
	
	btnStatus(CM_LABEL.refundBtn);
	
	$("#transReceipt")
		.on("click", ".refund", function(event) {
			event.preventDefault();
			showRefundDetail($(this).attr("ptrID"), $(this).attr("panneeded"));
			return false;
		});
}

function extractTransactionList(responseText) {
	return JSON.parse(responseText)["sorted-set"][0].CardRefundListInfo;
}

function searchCardTransaction(event) {
	$("#txListContainer").ajaxtab("loadData", {}, true);
	
	$("#searchCardNumber").val("");
	$("#searchCardExpiry").val("");
	/*
	$searchForm.find("input[type=text]").val("");
	$("#searchTxDateType").autoselector("refreshValue");
	$("#searchMerchantAccount").autoselector("refreshValue");
	*/
	
	return false;
}

function showRefundDetail(randomId, panNeeded) {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if((request.readyState === 4) && (request.status === 200)) {
			$("#refundDetailDialog")
				.html(request.responseText)
				.dialog("open");
			testPopFormEdit($("#refundDetailForm"), $("#refundDetailDialog"));
			
		}
		else if(request.readyState === 4) {
			alertDialog(systemErrorMsg);
		}
	};
	
	request.open("GET", LOC.loadRefundDetail +"?ptID=" + randomId +"&panNeeded="+ panNeeded +"&"+ document.location.search.substring(1), true);
	request.send();
}

function processRefund(event) {

		var params = mergeSerializedForms($("#cardRefundSarchForm").serialize(), $("#refundDetailForm").serialize());
		
		var request = GetHttpObject({
			"triggerSelector": (event) ? event.target : null,
			"postTokenSelector": "#rfndPostToken"
		});
		
		request.onreadystatechange = function(responseFalse) { 
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
						setTimeout(function(){ $(".second, .third").fadeOut(function(){ showTransaction(request.responseText, "updateDisplay"); }); }, 500);
					noteDialog(refundSuccess, "timeOut", 2500);
					$("#refundDetailDialog").dialog("close");
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("POST", LOC.processRefund, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send(params);
	return false;
}
