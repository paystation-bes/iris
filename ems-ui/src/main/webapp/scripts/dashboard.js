// JavaScript Document

// FOR NEW WIDGET TYPES MAKE SURE YOU RECORD THEIR "metricTypeId" TO THE APPROPRIATE VALUE FORMAT - DOLLAR, COUNT OR PERCENTAGE 
// SEARCH FOR "metricTypeId EffectedArea" TO FIND ALL EFFECTED AREAS


//LIST OF METRIC TYPES AND THERE ID'S FOR REFERENCE WHEN SCRIPTS ARE BLOCKING FEATURES FOR WIDGET TYPES
/*
"Active Alerts" 5896
"Alert Status" 9947
"Average Duration per Permit" 4875
"Average Price per Permit" 4829
"Average Revenue per Space" 4899
"Card Processing - Purchases" 9953
"Card Processing - Revenue" 9952
"Collections" 4782
"Duration" 1394
"Map" 4823
"Paid Occupancy" 4960
"Purchases" 3959
"Revenue" 7830
"Settled Card" 3820
"Turnover" 3905
"Utilization" 3945
"Citation Map" 9322
"Citations" 9321
"Counted Occupancy" 9368
"Counted/Paid Occupancy" 9369
"Occupancy Map" 4953
*/

var	columnBox = 	'<section class="columnBox"></section>',

	tabWidth = 	0,
	tabNavArea = 	0,
	offsetNav = 	0,
	lastTabOffsetPosition = 0,
	pastLength = 	0,
	ddMenuHover = 	false,
	tabCount = 	4,
	editStatus =	false,

	metricLabel = '',
	layoutStylLink = "",
	newLayout = "",
	newLayoutID = "",
	curLayout = "",
	layoutPrntSection = "",
	layoutPrntSctnContent = "",
	curTabID = "",
	curSectionID = "",

	tabInputObj = "",
	tabInputVal = "",
	tabLink = "",
	tabLinkText = "",
	valLength = "",
	//metricTypeId EffectedArea // metricTypeId should be in array for metric value format
	dollarVal = {1:'', 2:'', 3:'', 12:'', 13:'', 15:''}, //Revenue //Collections //Settled Cards //Average Price //Average Revenue // Card Processing - Revenue
	numberVal = {4:'', 6:'', 8:'', 9:'', 16:'', 17:'', 18:''}, //Purchases //Card Processing - Purchase //Citations
	timeVal = {14:''}, //Duration //Average Duration
	percentageVal = {5:'', 7:'', 19:'', 20:'', 21:''}, //Paid Occupancy //Counted Occupancy //Counted/Paid Occupancy
	timeMetric = [],
	wdgtDataArray = [],
	optionBasedObj = [], //Array of tier values objects
	optionBasedTier = []; //Array of tier values that have multiple select options for widget settings (WidgetTierType)

	timeMetric[14] = "h";
		
	optionBasedTier[0] = 2947; //Rate
	optionBasedTier[1] = 4758; //Organization
	optionBasedTier[2] = 2957; //Location
	optionBasedTier[3] = 9938; //Route
	optionBasedTier[4] = 8375; //Transaction Type
	optionBasedTier[5] = 9583; //Revenue
	optionBasedTier[6] = 8257; //Collection Type
	optionBasedTier[7] = 2968; //Merchant Account
	optionBasedTier[8] = 8258; //Alert Type
	optionBasedTier[9] = 8573; //Parent Location
	optionBasedTier[10] = 9951; //Card Processed
	optionBasedTier[11] = 9323; //Citation Type

Highcharts.setOptions({
	global: {
		useUTC: false
	}
});

//--------------------------------------------------------------------------------------

function mapLocationOccupancyObj(name, jsonData, occupancyLevel, occupancyCount, autoCount)
{
	this.localName=name;
	this.jsonData=jsonData;
	this.occpncyLevel=occupancyLevel;
	this.occpncyCount=occupancyCount;
	this.autoCount=autoCount;
}

//--------------------------------------------------------------------------------------

function saveDashboard(url, clickObj) // Save Dashboard
{
	var ajaxSaveMode = GetHttpObject();
	ajaxSaveMode.onreadystatechange = function()
	{
		if (ajaxSaveMode.readyState==4 && ajaxSaveMode.status == 200)
		{
			if(ajaxSaveMode.responseText == "true"){ 
				if(clickObj){clickObj.trigger('click');}
				else if(url){window.location = url;}
				else{
					var curTabID = $(".current > .tab").attr("id").replace("tab","").replace("-link", "");
					location.href = location.pathname; } } 
			else { alertDialog(saveModeFail); }
		} else if(ajaxSaveMode.readyState==4){ alertDialog(saveModeFail); }
	};
	ajaxSaveMode.open("GET","/secure/dashboard/saveDashboard.html",true);
	ajaxSaveMode.send();
}

//--------------------------------------------------------------------------------------

function getNewTabID(endStatus)
{
	
	$(".tabSection.current").removeClass("current");
	var ajaxTabID = GetHttpObject();
	ajaxTabID.onreadystatechange = function()
	{	
		if(ajaxTabID.readyState==4 && ajaxTabID.status == 200){
			if(ajaxTabID.responseText != "false"){ 				addTab(ajaxTabID.responseText, endStatus);
			} 
			else{ 
				alertDialog(editFail); 
			}
		} else if(ajaxTabID.readyState==4)
		{
			alertDialog(editFail);
		}
	};
	ajaxTabID.open("GET","/secure/dashboard/newTab.html?"+document.location.search.substring(1),true);
	ajaxTabID.send();
}

//--------------------------------------------------------------------------------------

function loadTab(tabID, $this){  // Loads Dashboard for the Tab that is set to Current
	if(tabID !== ""){
		$this = ($this)? $this : $("#tab"+tabID+"-link");
		$("#messageResponseAlert").hide();
		$(".dashboardTab").hide();
		$(".tabSection.current").removeClass("current");
		$this.parent(".tabSection").addClass("current"); }
	else{ $this = $(".current > .tab");  } 
	var crntTab = $this.attr("href");

	if(!$(crntTab).hasClass("dashboardTab")){
		var ajaxDashboard = GetHttpObject();
		ajaxDashboard.onreadystatechange = function()
		{	
			if (ajaxDashboard.readyState==4 && ajaxDashboard.status == 200){
				$("#dashboard").append(ajaxDashboard.responseText);
				$(crntTab).fadeIn();
				enableSectionMove();
			} else if (ajaxDashboard.readyState==4){
				$("#messageResponseAlert").fadeIn();
			}
			if(editStatus == true){editSort(false);}
		};
		ajaxDashboard.open("GET","/secure/dashboard/tab.html?id="+tabID +"&"+ document.location.search.substring(1),true);
		ajaxDashboard.send(); } 
	else {
		var ajaxTabUpdate = GetHttpObject();
		ajaxTabUpdate.open("GET","/secure/dashboard/setCurrentTab.html?tabID="+tabID+"&"+document.location.search.substring(1),true);
		ajaxTabUpdate.send();
		$(crntTab).fadeIn(); }
	
	chartRefresh("tabSwitch");
	dashboardNav();
	enableSectionMove(); }

//--------------------------------------------------------------------------------------

function addTab(tabID, endStatus)  // ADDS NEW TAB TO DASHBOARD NAV
{
	
	var ajaxTab = GetHttpObject();
	ajaxTab.onreadystatechange = function()
	{	
		if (ajaxTab.readyState==4 && ajaxTab.status == 200){
			$("#dashboardNavArea > nav").append(ajaxTab.responseText);
			dashboardNav();
			if($(".dashboardTab").length > 0){$(".dashboardTab").hide();}
			if(endStatus ==  "blur"){$(".current.new > input").trigger('blur');}else{$(".current.new > input").trigger('focus');}
			loadTab(tabID);
		} else if(ajaxTab.readyState==4)
		{
			alertDialog(editFail);
		}
	};
	ajaxTab.open("GET","/secure/dashboard/addTab.html?id="+tabID+"&"+document.location.search.substring(1),true);
	ajaxTab.send();
}							

//--------------------------------------------------------------------------------------

function editTab() {
	if(tabInputVal != tabLinkText){		
		if(valLength == 0){ 
			tabInputVal = untitledTitle;
			tabInputObj.val(untitledTitle);
			tabLink.html(untitledTitle); }
		var params = "tabID="+tabLink.attr("href").replace("#tab", "")+"&tabName="+tabInputVal+"&"+document.location.search.substring(1);
		var ajaxTabName = GetHttpObject();
		ajaxTabName.onreadystatechange = function(){
			if (ajaxTabName.readyState==4 && ajaxTabName.status==200){
				if(ajaxTabName.responseText == "true"){
					tabLink.html(tabInputVal);
					tabInputObj.hide();
					tabLink.show();
					dashboardNav(); }
				else{
					tabInputObj.val(tabLinkText);
					tabInputObj.hide();
					tabLink.show();
					dashboardNav();
					alertDialog(editFail); } } 
			else if(ajaxTabName.readyState==4) {
				tabInputObj.val(tabLinkText);
				tabInputObj.hide();
				tabLink.show();
				dashboardNav();
				alertDialog(editFail); } };
		ajaxTabName.open("Get","/secure/dashboard/editTab.html?"+params,true);
		ajaxTabName.send(); }
	else {
		tabInputObj.hide();
		tabLink.show();
		dashboardNav(); } }

//--------------------------------------------------------------------------------------

function dashboardNav() // Manages the size and Dimension of the Dashboard Nav and the poistion of the nav if scrolling is enabled due to size.
{
	tabWidth = 0;
	tabNavArea = $("#dashboardNav").innerWidth();
	tabNavArea = ($("#dashboardNav").parents("#mainContent").hasClass("edit"))? tabNavArea - 60 : tabNavArea - 34;

	if($("#dashboardNavArea > nav > .current").length === 1){
		var crntTab = $("#dashboardNavArea > nav > .current");
		var crntTabWidth = crntTab.outerWidth();
		var crntTabPosition = crntTab.position();
		var crntTabPositionLeft = Math.round(crntTabPosition.left);
		var lastTab = $("#dashboardNavArea > nav > .tabSection").last();
		var lastTabWidth = lastTab.outerWidth();
		var lastTabPosition = lastTab.position();
		var lastTabPositionLeft = Math.round(lastTabPosition.left);
		var navPosition = $("#dashboardNavArea > nav").position();
		var navPositionLeft =  Math.round(navPosition.left);
		var offsetPosition = tabNavArea - crntTabPositionLeft - crntTabWidth;
		lastTabOffsetPosition = tabNavArea - lastTabPositionLeft - lastTabWidth;
		var navTabPosSum = navPositionLeft + crntTabPositionLeft;
		var navPositionOffset = offsetPosition - navPositionLeft;

		$(".tabSection").each(function() { tabWidth = tabWidth + $(this).outerWidth(); });

		if(tabWidth > tabNavArea){
			if(navTabPosSum >= 0 && navPositionOffset < 0){
				 offsetNav = offsetPosition;
				 offsetNav = (offsetNav > 0)? 0 : offsetNav; }
			else if(navTabPosSum < 0){
				offsetNav = navPositionLeft - navTabPosSum;	}
			else if(lastTabOffsetPosition > navPositionLeft){
				offsetNav = lastTabOffsetPosition;
				offsetNav = (offsetNav > 0)? 0 : offsetNav;	} 
			else {
				offsetNav = navPositionLeft; }

			$(".moveLeft, .moveRight").fadeIn();

			if(offsetNav < 0){
				$(".moveLeft").addClass("active"); } 
			else {
				$(".moveLeft").removeClass("active"); } 
			$("#dashboardNavArea > nav").css("left", offsetNav+"px");
			$("#dashboardNavArea").width(tabNavArea);
			if(offsetNav > lastTabOffsetPosition){
				$(".moveRight").addClass("active"); } 
			else {
				$(".moveRight").removeClass("active"); } } 
		else {
			$(".moveLeft, .moveRight").hide();
			$("#dashboardNavArea > nav").css("left", "0px");
			$("#dashboardNavArea").width(tabWidth); } } }

//--------------------------------------------------------------------------------------

function enableSectionMove() // Sets wether the Tab Section Move Buttons are available to the user or not
{
	$(".moveUp.disabled, .moveDown.disabled").removeClass("disabled");
	if($(".dashboardTab:visible").find(".layout").length > 1)
	{
		var layoutCount = $(".dashboardTab:visible").find(".layout").length-1;
		$(".dashboardTab:visible").find(".layout").each(function(index) {
			if(index == 0)
			{
				$(this).find(".moveUp").addClass("disabled");
			} else if(index == layoutCount)
			{
				$(this).find(".moveDown").addClass("disabled");
			}
		});
	} else {
		$(".dashboardTab:visible").find(".moveUp, .moveDown").addClass("disabled");
	}
}

//--------------------------------------------------------------------------------------

function layoutChng(mergeStatus)
{
	
	var ajaxLytChng = GetHttpObject();
	ajaxLytChng.onreadystatechange = function()
	{
		if (ajaxLytChng.readyState==4 && ajaxLytChng.status == 200){
			if(ajaxLytChng.responseText == "true")
			{
				layoutStylLink.addClass("on").siblings(".on").removeClass("on");
				layoutPrntSection.removeClass(curLayout).addClass(newLayout);
				chartRefresh("layoutChange");
			}else{
				if(mergeStatus != null){layoutPrntSection.html(layoutPrntSctnContent)};
				alertDialog(editFail);
				layoutPrntSctnContent = null;
			}
		} else if(ajaxLytChng.readyState==4){
			if(mergeStatus != null){layoutPrntSection.html(layoutPrntSctnContent)};
			alertDialog(editFail);
			layoutPrntSctnContent = null;
		}
	};
	ajaxLytChng.open("GET","/secure/dashboard/editSection.html?layoutID="+newLayoutID+"&sectionID="+layoutPrntSection.attr("id").replace("section_","")+"&tabID="+curTabID.replace("tab", "")+"&"+document.location.search.substring(1),true);
	ajaxLytChng.send();
}

//--------------------------------------------------------------------------------------

function clearColumn(colCount)
{
	
	var columnClass = [".third", ".second", ".first"];
	var columnID = [2, 1, 0];
	var colIDs = "";
	for(x = 0; x < colCount; x++){colIDs = colIDs + "colID[]=" + columnID[x] + "&";}
	var ajaxClearCol = GetHttpObject();
	ajaxClearCol.onreadystatechange = function()
	{
		if (ajaxClearCol.readyState==4 && ajaxClearCol.status == 200){
			if(ajaxClearCol.responseText == "true" && colCount <= 3)
			{
				for(x = 0; x < colCount; x++)
				{
					layoutPrntSection.children(columnClass[x]).children(".widgetObject").each(function() {
						layoutPrntSection.children(columnClass[x]).children($(this)).remove();
						layoutPrntSection.children(columnClass[x]).append(columnBox);
					});
				}
				layoutChng();
			} else {
				alertDialog(editFail);
				layoutPrntSctnContent = null;
			}
		} else if(ajaxClearCol.readyState==4){
			alertDialog(editFail);
			layoutPrntSctnContent = null;
		}
	};
	ajaxClearCol.open("GET","/secure/dashboard/clearColumn.html?"+colIDs+"sectionID="+layoutPrntSection.attr("id").replace("section_","")+"&tabID="+curTabID.replace("tab", "")+"&"+document.location.search.substring(1),true);
	ajaxClearCol.send();
}

//--------------------------------------------------------------------------------------

function sectionOrder(moveObj, moveState)
{						
	var thisSectionID = moveObj.parents(".layout").attr("id").replace("section_", "");
	var curTab = moveObj.parents(".dashboardTab");
	var curIndex = null;
	var sectionArray = new Array();
	$(curTab).children(".layout").each(function(index){
		var curSecArray = $(this).attr("id").split("_");
		if(thisSectionID == curSecArray[1] && curIndex == null){curIndex = index};
		sectionArray[index] = curSecArray[0]+"[]="+curSecArray[1];
	});
	
	if(moveState == "up"){sectionArray.move(curIndex, -1);}
	else if(moveState == "down"){sectionArray.move(curIndex, 1);}
	
	var sectionQuerry = null;
	for(x=0;x<sectionArray.length;x++)
	{
		if(sectionQuerry == null){sectionQuerry = sectionArray[x];}
		else{sectionQuerry = sectionQuerry+"&"+sectionArray[x];}
	}

	var params = "tabID="+curTab.attr("id").replace("tab", "")+"&"+sectionQuerry+"&"+document.location.search.substring(1);
	var ajaxSecOrder = GetHttpObject();
	ajaxSecOrder.onreadystatechange = function()
	{
		if (ajaxSecOrder.readyState==4 && ajaxSecOrder.status == 200)
		{
			if(ajaxSecOrder.responseText == "true")
			{
				if(moveState == "up")
				{
					moveObj.parents(".layout").insertBefore(moveObj.parents(".layout").prev(".layout"));
					enableSectionMove();
					var parentPosition = moveObj.parents(".layout").offset();
					window.scrollTo(0, parentPosition.top);
				}
				else if(moveState == "down")
				{
					moveObj.parents(".layout").insertAfter(moveObj.parents(".layout").next(".layout"));
					enableSectionMove();
					var parentPosition = moveObj.parents(".layout").offset();
					window.scrollTo(0, parentPosition.top);					
				}
			} else {
				alertDialog(editFail);	
			}
		} else if(ajaxSecOrder.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxSecOrder.open("POST","/secure/dashboard/sectionOrder.html",true);
	ajaxSecOrder.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxSecOrder.setRequestHeader("Content-length", params.length);
	ajaxSecOrder.setRequestHeader("Connection", "close");
	ajaxSecOrder.send(params);
}

//--------------------------------------------------------------------------------------

function deleteSection(sectionID, parentSection)
{
	
	var ajaxDltSection = GetHttpObject();
	ajaxDltSection.onreadystatechange = function()
	{
		var curTabObj = "#"+curTabID;
		if (ajaxDltSection.readyState==4 && ajaxDltSection.status == 200){
			if(ajaxDltSection.responseText == "true") {
				parentSection.remove();
				if($(curTabObj).find(".layout").length == 0)
				{
					$(curTabObj).find(".addSection").trigger('click');
				}
			} else {
				alertDialog(editFail);
			}
		} else if(ajaxDltSection.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxDltSection.open("GET","/secure/dashboard/deleteSection.html?sectionID="+sectionID+"&tabID="+curTabID.replace("tab", "")+"&"+document.location.search.substring(1),true);
	ajaxDltSection.send();
}

//--------------------------------------------------------------------------------------

function widgetOrder(fullQuerry, lytChng)
{
	var params = fullQuerry+"&"+document.location.search.substring(1);
	var ajaxLytOrder = GetHttpObject({"sessionReset": false});
	ajaxLytOrder.onreadystatechange = function()
	{
		if(lytChng == true)
		{
			if (ajaxLytOrder.readyState==4 && ajaxLytOrder.status == 200)
			{
				if(ajaxLytOrder.responseText == "true"){ layoutChng(true); }
				else
				{
					layoutPrntSection.html(layoutPrntSctnContent);
					alertDialog(editFail);
					layoutPrntSctnContent = null;
				}
			} else if(ajaxLytOrder.readyState==4){
				layoutPrntSection.html(layoutPrntSctnContent);
				alertDialog(editFail);
				layoutPrntSctnContent = null;
			}
		} else {
			if (ajaxLytOrder.readyState==4 && ajaxLytOrder.status == 200)
			{
				if(ajaxLytOrder.responseText == "false")
				{
					$(".column").sortable("cancel");
					alertDialog(editFail);
				}
			} else if(ajaxLytOrder.readyState==4){
				$(".column").sortable("cancel");
				alertDialog(editFail);
			}	
		}
	};
	ajaxLytOrder.open("POST","/secure/dashboard/widgetOrder.html",true);
	ajaxLytOrder.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxLytOrder.send(params);
}
											
//--------------------------------------------------------------------------------------

function loadWidgetList(layoutSection, curSectionID, curTabID, placementLoc)
{
	var ajaxWidgetList = GetHttpObject();
	ajaxWidgetList.onreadystatechange = function()
	{
		if (ajaxWidgetList.readyState==4 && ajaxWidgetList.status == 200){
			if(ajaxWidgetList.responseText != "false")
			{
				createWidgetList(ajaxWidgetList);
				showWidgetList(layoutSection, curSectionID, curTabID, placementLoc);
			}else{
				alertDialog(editFail);
			}
		} else if(ajaxWidgetList.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxWidgetList.open("GET","/secure/dashboard/widgetMasterList.html?"+document.location.search.substring(1),true);
	ajaxWidgetList.send();
}

//--------------------------------------------------------------------------------------

var metricMap = [widgetMetricTitleActiveAlerts, widgetMetricTitleAverageDuration, widgetMetricTitleAveragePrice, widgetMetricTitleAverageRevenue, widgetMetricTitleCardProcessingPurchases, widgetMetricTitleCardProcessingRevenue, widgetMetricTitleSettledCards, widgetMetricTitleCollections, widgetMetricTitleMap, widgetMetricTitleOccupancy, widgetMetricTitlePurchases, widgetMetricTitleRevenue, widgetMetricTitleTurnover, widgetMetricTitleUtilization, widgetMetricTitleCitations, widgetMetricTitlePhysicalOccupancy, widgetMetricTitleCompareOccupancy, widgetMetricTitleOccupancyMap];

function createWidgetList(widgetListData)
{
	var	listData =	JSON.parse(widgetListData.responseText),
		$listHTML = $('<section>');
		$listHTML.addClass("widgetListContainer");
		$listHTML.append('<a href="#" class="linkButton textButton showAllWdgts">'+ showAllBtn +'</a>');

	
	var catNames = listData.widgetMasterList.categoryNames;
	if(!catNames) {
		catNames = metricMap;
	}
	for(x = 0; x < listData.widgetMasterList.metricTypes.length; x++) {
		if(listData.widgetMasterList.metricTypes[x].length){
			$listHTML.append('<h3 id="metric'+x+'">'+catNames[x]+'<div class="expand"><img src="'+imgLocalDir+'spacer.gif" border="0" width="18" height="1"></div></h3>'); 
			var $metricList = $('<ul id="metricList'+ x +'" class="widgetSelection">');
			for(var y = 0; y < listData.widgetMasterList.metricTypes[x].length; y++) {			
				$metricList.append('<li id="'+ listData.widgetMasterList.metricTypes[x][y].id +'">' +
					'<header>' + listData.widgetMasterList.metricTypes[x][y].name + '</header>' +
					'<article>' + listData.widgetMasterList.metricTypes[x][y].description + '</article></li>'); }
			$listHTML.append($metricList); } }
	$listHTML.append('<form id="wdgtListForm"><input type="hidden" id="theWdgtSelected" />');
	
	$("#widgetList").html($listHTML);
}

//--------------------------------------------------------------------------------------

function showWidgetList(layoutSection, curSectionID, curTabID, placementLoc)
{
	var showWidgetButtons = {};
	showWidgetButtons.btn1 = {
		text :	 addWidgetMsg,
		click : 	function(event, ui) {
					var isActive = false;
					$('button').each(function(){ if($(this).html().match(addWidgetMsg) != null){ isActive = $(this).hasClass('active'); } });
					
					if(isActive)
					{
						var selected = $("#theWdgtSelected").val().split(',');
						for(var x = 0; x < selected.length; x++) { addWidget(layoutSection, placementLoc, selected[x]); }
						$("#widgetList").find("ul").hide();
						$("#widgetList").find(".contract").removeClass(("contract"));
						$("#widgetList").find("h3").removeClass(("selected"));
						if($(".showAllWdgts").html().match("Hide") != null) { var newButtonLbl = $(this).html().replace("Hide", "Show"); $(this).html(newButtonLbl); }
						$(this).scrollTop(0); $(this).dialog( "close" );
					}
				}
	};
	showWidgetButtons.btn2 = {
		text : cancelMsg,
		click :	function() {
					$(".ui-selected").each(function(index) { $(this).removeClass('ui-selected click-selected'); });
					$("#theWdgtSelected").val('');
					$("#widgetList").find("ul").hide();
					$("#widgetList").find(".contract").removeClass(("contract"));
					$("#widgetList").find("h3").removeClass(("selected"));
					if($(".showAllWdgts").html().match("Hide") != null) { var newButtonLbl = $(this).html().replace("Hide", "Show"); $(this).html(newButtonLbl); }
					$(this).scrollTop(0); $(this).dialog( "close" );
				}
	};
	if($("#widgetList.ui-dialog-content").length){$("#widgetList.ui-dialog-content").dialog("destroy");}
	$("#widgetList").dialog({
		title: addWidgetTitle,
		modal: true,
		resizable: false,
		closeOnEscape: false,
		width: 480,
		maxHeight:  $(window).innerHeight()-150,
		hide: "fade",
		buttons:	showWidgetButtons,
		dragStop: function(){ checkDialogPlacement($(this)); }
	});
	
	$(".ui-dialog-buttonset").children().each(function(){ if($(this).html() == cancelMsg){ $(this).addClass("btnCancel"); }});
	
	$("#widgetList").find("h3:first").trigger('click');
}

//--------------------------------------------------------------------------------------

function addWidget(layoutSection, placementLoc, widgetTypeID)
{
	
	var ajaxAddWidget = GetHttpObject();
	ajaxAddWidget.onreadystatechange = function()
	{
		if (ajaxAddWidget.readyState==4 && ajaxAddWidget.status == 200){
			if(ajaxAddWidget.responseText != "false")
			{
				$(ajaxAddWidget.responseText).insertBefore(placementLoc);
				$(".ui-selected").removeClass('ui-selected click-selected');
				$("#theWdgtSelected").val('');
				
				var tabQuerry = "tabID=" + curTabID;
				var sectionQuerry = null;
				$("#tab"+curTabID).children(".layout").each(function(){
					var sectionID = $(this).attr("id").split("_");
					if(sectionQuerry == null){sectionQuerry = sectionID[0]+"[]="+sectionID[1];}
					else{sectionQuerry = sectionQuerry+"&"+sectionID[0]+"[]="+sectionID[1];}
				});
				var widgetQuerry = null;
				$(".column:visible").has(".widgetObject").each(function(){
					if(widgetQuerry == null){widgetQuerry = $(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
					else{widgetQuerry = widgetQuerry+"&"+$(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
				});
				var fullQuerry = tabQuerry  + "&" + sectionQuerry  + "&" + widgetQuerry;
				widgetOrder(fullQuerry);
			}else{
				alertDialog(editFail);
			}
		} else if(ajaxAddWidget.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxAddWidget.open("GET","/secure/dashboard/addWidget.html?sectionID="+curSectionID+"&tabID="+curTabID+"&widgetTypeID="+widgetTypeID+"&"+document.location.search.substring(1),true);
	ajaxAddWidget.send();
}

//--------------------------------------------------------------------------------------

//CHART GENERAL VARIABLES
var metricTypes = [/*
0	*/"",/* 
1	*/"Revenue",/* 
2	*/"Collections",/* 
3	*/"Settled Card",/* 
4	*/"Purchases", /*
5	*/"Occupancy",/* Paid Occupancy
6	*/"Turnover", /*
7	*/"Utilization",/* 
8	*/"Duration", /*
9	*/"Active Alerts", /*
10	*/"Alert Status", /*
11	*/"Map", /*
12	*/"Average Price per Permit", /*
13	*/"Average Revenue per Space", /*
14	*/"Average Duration per Permit",/*
15	*/"Card Processing - Revenue",/*
16	*/"Card Processing - Purchases",/*
17	*/"Citations", /*
18	*/"Citation Map",/* 
19	*/"Occupancy", /* Counted Occupancy
20	*/"Occupancy", /* Counted/Paid Occupancy
21	*/"Occupancy Map"];
// metricTypeId EffectedArea // Area should match number of metricTypeId's
var metricIcon = [/*
0	*/"",/* 
1	*/"icnLrg-revenue",/* 
2	*/"icnLrg-collections",/* 
3	*/"icnLrg-revenue",/* 
4	*/"icnLrg-revenue", /*
5	*/"icnLrg-parking",/* 
6	*/"icnLrg-parking", /*
7	*/"icnLrg-parking",/* 
8	*/"icnLrg-parking", /*
9	*/"icnLrg-alert", /*
10	*/"icnLrg-alert", /*
11	*/"", /*
12	*/"icnLrg-revenue", /*
13	*/"icnLrg-revenue", /*
14	*/"icnLrg-duration",/*
15	*/"icnLrg-revenue",/*
16	*/"icnLrg-parking",/*
17	*/"icnLrg-parking", /*
18	*/"",/* 
19	*/"icnLrg-parking", /*
20	*/"icnLrg-parking", /*
21	*/""];
var chartTypes = ["", "List", "pie", "line", "bar", "column", "area", "Single Value", "Map", "Heat Map", "Occupancy Map"];
var rangeTypes = ["N/A", "Now", "Today", "Yesterday", "Last 24 Hours", "Last 7 Days", "Last 30 Days", "Last 12 Months", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];

//--------------------------------------------------------------------------------------

function dollarToolTip(tempChartOptions, data)//SETS TOOLTIPS AND VALUE DISPLAY FOR DOLLAR VALUES
{
	tempChartOptions.yAxis.labels.formatter = function() { return (this.value < 0)? "($"+Highcharts.numberFormat(Math.abs(this.value), 2)+")" : "$"+Highcharts.numberFormat(this.value, 2);}
	if(data.widgetData.chartType == 2){
		tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ this.point.name +'</strong><br/>'+  '($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ this.point.name +'</strong><br/>'+'$'+Highcharts.numberFormat(this.y, 2); }; } 
	else if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
		if(!data.widgetData.seriesList.length && data.widgetData.chartType != 2){
			tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ this.x +'</strong><br/>'+'($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ this.x +'</strong><br/>'+'$'+Highcharts.numberFormat(this.y, 2); };
		} else {
			tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ this.series.name +'</strong><br/>'+ this.x +' - '+'($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ this.series.name +'</strong><br/>'+'$'+Highcharts.numberFormat(this.y, 2); }; }} 
	else {
		if(!data.widgetData.seriesList.length){
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ Highcharts.dateFormat('%I:%M %P', this.x) +'</strong><br/>' + '($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ Highcharts.dateFormat('%I:%M %P', this.x) +'</strong><br/>' + '$'+Highcharts.numberFormat(this.y, 2); };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ Highcharts.dateFormat('%a, %b %d', this.x) +'</strong><br/>'+'($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ Highcharts.dateFormat('%a, %b %d', this.x) +'</strong><br/>'+'$'+Highcharts.numberFormat(this.y, 2); };
			}
		} else {
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%I:%M %P', this.x) +' - ($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%I:%M %P', this.x) +' - $'+Highcharts.numberFormat(this.y, 2); }
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return (this.y < 0)? '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%a, %b %d', this.x) +' - ' + '($'+Highcharts.numberFormat(Math.abs(this.y), 2)+')' : '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%a, %b %d', this.x) +' - $'+Highcharts.numberFormat(this.y, 2); }; }}}
}

//--------------------------------------------------------------------------------------

function countToolTip(tempChartOptions, data)//SETS TOOLTIPS AND VALUE DISPLAY FOR COUNT VALUES
{
	tempChartOptions.yAxis.labels.formatter = function() { return this.value;};
	if(data.widgetData.chartType == 2){
		tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.point.name +'</strong><br/>'+ this.y; }; }
	else if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
		if(!data.widgetData.seriesList.length && data.widgetData.chartType != 2){
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.x +'</strong><br/>'+ this.y; };
		} else {
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ this.x +' - '+ this.y; }; }}
	else {
		if(!data.widgetData.seriesList.length){
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%I:%M %P', this.x) +'</strong><br/>'+ this.y; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%a, %b %d', this.x) +'</strong><br/>'+ this.y; };
			}
		} else {
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%I:%M %P', this.x) +' - '+ this.y; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%a, %b %d', this.x) +' - '+ this.y; }; }}}
}

//--------------------------------------------------------------------------------------

function timeToolTip(tempChartOptions, data)//SETS TOOLTIPS AND VALUE DISPLAY FOR TIME BASED VALUES
{
	var tempMetricMarker = timeMetric[data.widgetData.metricTypeId];
	tempChartOptions.yAxis.labels.formatter = function() { return this.value + tempMetricMarker;};
	if(data.widgetData.chartType == 2){
		tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.point.name +'</strong><br/>'+ this.y + tempMetricMarker; }; }
	else if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
		if(!data.widgetData.seriesList.length && data.widgetData.chartType != 2){
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.x +'</strong><br/>'+ this.y + tempMetricMarker; };
		} else {
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ this.x +' - '+ this.y + tempMetricMarker; }; }}
	else {
		if(!data.widgetData.seriesList.length){
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%I:%M %P', this.x) +'</strong><br/>'+ this.y + tempMetricMarker; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%a, %b %d', this.x) +'</strong><br/>'+ this.y + tempMetricMarker; };
			}
		} else {
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%I:%M %P', this.x) +' - '+ this.y + tempMetricMarker; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%a, %b %d', this.x) +' - '+ this.y + tempMetricMarker; }; }}}
}


//--------------------------------------------------------------------------------------

function percentageToolTip(tempChartOptions, data)//SETS TOOLTIPS AND VALUE DISPLAY FOR PERCENTAGE VALUES
{
	tempChartOptions.yAxis.labels.formatter = function() { return this.value+"%";};
	if(data.widgetData.chartType == 2){
		tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.point.name +'</strong><br/>'+ this.y +"%"; }; } 
	else if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
		if(!data.widgetData.seriesList.length && data.widgetData.chartType != 2){
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.x +'</strong><br/>'+ this.y+"%"; };
		} else {
			tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ this.x +' - '+ this.y+"%"; }; }} 
	else {
		if(!data.widgetData.seriesList.length){
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%I:%M %P', this.x) +'</strong><br/>'+ this.y+"%"; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ Highcharts.dateFormat('%a, %b %d', this.x) +'</strong><br/>'+ this.y+"%"; };
			}
		} else {
			if(data.widgetData.timeInterval == hourMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%I:%M %P', this.x) +' - '+ this.y+"%"; };
			}else if(data.widgetData.timeInterval == dayMS){
				tempChartOptions.tooltip.formatter = function() { return '<strong>'+ this.series.name +'</strong><br/>'+ Highcharts.dateFormat('%a, %b %d', this.x) +' - '+ this.y+"%"; }; }}}
}

//--------------------------------------------------------------------------------------

function wdgtSorting(sortData, sortOps) {
	var	json_object = sortData.listData,
		key_to_sort_by = sortOps[0], 
		dir = sortOps[1];

	function sortByKeyAsc(a, b) {
		var x = a[key_to_sort_by];
		var y = b[key_to_sort_by];
		var xIsNum = isNaN(x);
		var yIsNum = isNaN(y);
		if(xIsNum !== yIsNum){ return((xIsNum) ? 1 : -1 ); }//X and Y are different types return number less than character if x is # return -1 if y is # return 1
		else{
			if(!xIsNum && typeof x !== 'number'){ x = parseInt(x); }
			if(!xIsNum && typeof y !== 'number'){ y = parseInt(y); }
			x = (typeof x !== 'number')? x.toLowerCase() : x;
			y = (typeof y !== 'number')? y.toLowerCase() : y;
			return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
	}
	function sortByKeyDesc(a, b) {
		var x = a[key_to_sort_by];
		var y = b[key_to_sort_by];
		var xIsNum = isNaN(x);
		var yIsNum = isNaN(y);
		if(xIsNum !== yIsNum){ return((xIsNum) ? -1 : 1 ); }//X and Y are different types return number greater than character if x is # return 1 if y is # return -1
		else{
			if(!xIsNum && typeof x !== 'number'){ x = parseInt(x); }
			if(!xIsNum && typeof y !== 'number'){ y = parseInt(y); }
			x = (typeof x !== 'number')? x.toLowerCase() : x;
			y = (typeof y !== 'number')? y.toLowerCase() : y;
			return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
	}
	
	if(dir === "desc"){ return json_object.sort(sortByKeyDesc); } else { return json_object.sort(sortByKeyAsc); }
}

//--------------------------------------------------------------------------------------

function wdgtListDataPoint(value, colNum, colWidth, valueType, byTime, Interval){
	if(colNum === 1){
		var ogValue = value;
		if(byTime){ value = Highcharts.dateFormat(listTimeFormat[Interval], ogValue, true); }
		return('<div class="col'+colNum+'" style="width: '+colWidth+';" title="' + value + '"><p>' + value + '</p></div>');}
	else {
		if(valueType in dollarVal){ 
			var $valDisplay = (value < 0)? '($' + Highcharts.numberFormat(Math.abs(value), 2) +')' : '$' + Highcharts.numberFormat(value, 2);
			return('<div class="col'+colNum+'" style="width: '+colWidth+';" title="' + $valDisplay + '"><p>' + $valDisplay + '</p></div>');} 
		else if(valueType in numberVal){ 
			return('<div class="col'+colNum+'" style="width: '+colWidth+';" title="' + value + '"><p>' + value + '</p></div>');} 
		else if(valueType in timeVal){ 			
			return('<div class="col'+colNum+'" style="width: '+colWidth+';" title="' + value + '"><p>' + value + timeMetric[valueType] +'</p></div>');} 
		else if(valueType in percentageVal){ 
			return('<div class="col'+colNum+'" style="width: '+colWidth+';" title="' + Highcharts.numberFormat(value, 1, '.') + '%"><p>' + Highcharts.numberFormat(value, 1, '.') + '%</p></div>');} } }

//--------------------------------------------------------------------------------------

var posMapIDs = {};
function getPosInfo(eventMarker) // Gets Popup Details for POS in Map Widget
{
	var serialNumber = eventMarker.options.alt;
	if(curMarker && curMarker.options.alt === serialNumber){ curMarker.closePopup(); }
	else{
		if(curMarker){ curMarker.closePopup().unbindPopup(); }
		curMarker = eventMarker;
		var	ajaxMapWidgetInfo = GetHttpObject();
			ajaxMapWidgetInfo.onreadystatechange = function(){
				if (ajaxMapWidgetInfo.readyState==4 && ajaxMapWidgetInfo.status == 200){
					if(ajaxMapWidgetInfo.responseText !== "false"){
						var	posDetails = JSON.parse(ajaxMapWidgetInfo.responseText);
							posDetails = posDetails.mapEntry;
						if(posDetails.runningTotal.isLegacyCash){
							var runningTotalDisplay = '<dt class="detailLabel">Cash Total: </dt><dd class="detailValue">$'+ Highcharts.numberFormat(posDetails.runningTotal.cashAmount,2) +'</dd>'+
													'<dt class="detailLabel">Card Total: </dt><dd class="detailValue">$'+  Highcharts.numberFormat((posDetails.runningTotal.cardAmount) ? posDetails.runningTotal.cardAmount:0,2) +'</dd>';
						}
						else if(!posDetails.runningTotal.coinAmount){
							var runTotal = Highcharts.numberFormat(parseFloat(posDetails.runningTotal.cardAmount) + parseFloat(posDetails.runningTotal.cashAmount, 2));
							var runningTotalDisplay = '<dt class="detailLabel">Running Total: </dt><dd class="detailValue">$'+ runTotal +'</dd>';}
						else{
							var runningTotalDisplay = 	'<dt class="detailLabel">Coin Total: </dt><dd class="detailValue">$'+  Highcharts.numberFormat(posDetails.runningTotal.coinAmount,2) +'</dd>'+
													'<dt class="detailLabel">Bill Total: </dt><dd class="detailValue">$'+  Highcharts.numberFormat(posDetails.runningTotal.billAmount,2) +'</dd>'+
													'<dt class="detailLabel">Card Total: </dt><dd class="detailValue">$'+  Highcharts.numberFormat(posDetails.runningTotal.cardAmount,2) +'</dd>';}
													
						if(!isCustomerParent && canViewPayStations){ var detailsBtn = '<a href="'+detailsUrl+'payStationList.html?itemID='+posDetails.randomId+'&pgActn=display" class="linkButtonFtr textButton psDetailBtn">Details</a>';}
						else { var detailsBtn = ''; }
						

						if(!isCustomerParent && canViewPayStations){ var detailsBtn = '<a href="'+detailsUrl+'payStationList.html?itemID='+posDetails.randomId+'&pgActn=display" class="linkButtonFtr textButton psDetailBtn">Details</a>';}
						else { var detailsBtn = ''; }

						curMarker.bindPopup(
							'<section class="mapPopUp">' +
							'<h1>'+posDetails.name+'</h1>' +
							'	<section class="thumbNailButton">' +
							'		<img src="'+imgLocalDir+'paystation_'+payStationIcn[posDetails.payStationType].img[0]+'Sml.jpg" width="50" height="100" border="0" /><br /><br />' + detailsBtn +
							'	</section>' +
					
							'	<dl class="psDetails">' +
							'		<dt class="detailLabel">Serial Number: </dt><dd class="detailValue">'+ posDetails.serialNumber +'</dd>'+
							'		<dt class="detailLabel">Location: </dt><dd class="detailValue">'+ posDetails.locationName +'</dd>' +
							'		<dt class="detailLabel">Last Seen: </dt><dd class="detailValue">'+ posDetails.lastSeen +'</dd>' + runningTotalDisplay +
							'		<dt class="detailLabel">Last Collection: </dt><dd class="detailValue">'+ posDetails.lastCollection +'</dd>' +
							'		<dt class="detailLabel">Battery Voltage: </dt><dd class="detailValue '+ psAlerts[posDetails.batteryStatus].listClass +'">'
									+ validatePOSValue(posDetails.batteryVoltage) +'</dd>' +
							'		<dt class="detailLabel">Alert Status: </dt><dd class="detailValue '+ psAlerts[posDetails.severity].listClass +'">'+ 
									psAlerts[posDetails.severity].title +'</dd>' +
							'		<dt class="detailLabel">Paper Status: </dt><dd class="detailValue '+ psAlerts[posDetails.paperStatus].listClass +'">'+ 
									psAlerts[posDetails.paperStatus].pprTitle +'</dd>' +
							'	</dl>' +
							'</section>'
						).openPopup(); } }
				else if(ajaxMapWidgetInfo.readyState==4){
					alertDialog(systemErrorMsg); } };//Unable to load details
			ajaxMapWidgetInfo.open("GET","/secure/map/payStationDetail.html?psID="+posMapIDs[serialNumber]+"&pgActn=display&"+document.location.search.substring(),true);
			ajaxMapWidgetInfo.send(); }
}

//--------------------------------------------------------------------------------------

function displayChart(wdgtID, source)//DISPLAYS CHARTS AND LISTS FOR WIDGETS
{
	var tempChart = 	null,
		dataSource = 	$("#widgetID_"+wdgtID).find(".widgetData"),
		data = 			JSON.parse(dataSource.html()),
		dataStatus = 	dataSource.attr("status"),
		widgetName = 	data.widgetData.widgetName,
		singleSeriesSingleData = false,
		chartSwitch = 	false,
		byTime = 		false,
		timeInterval =	false,
		resizable = 	true,
		$chrtObj = (source === 'xpnd')? $("#xpndContent") : $("#content_"+wdgtID),
		wdgtLgndWidth = $chrtObj.innerWidth()-40;

	if(wdgtLgndWidth < 250){ var wdgtLgndItemWidth = wdgtLgndWidth; }
	else if(wdgtLgndWidth < 750){ var wdgtLgndItemWidth = parseInt(wdgtLgndWidth/2); }
	else { var wdgtLgndItemWidth = parseInt(wdgtLgndWidth/3); }

//	data.widgetData.timeInterval = false;

//DISPLAY NO DATA RETURNED MESSAGE WHEN NOTHING RETUREND TO CHART OR DISPLAY
$chrtObj.html('');

if(dataStatus === '249' && data.widgetData.isEmpty === true){
	$chrtObj.removeClass("listGraph").addClass("chartGraph");
	if(source !== 'xpnd'){ $chrtObj.siblings("header").children(".wdgtName").html(widgetName); }
	$chrtObj.html("<section class='noWdgtData noData'><span>"+ widgetNoDataMsg +"</span></section>") }
else{
	var	customerTime = -customerOffset,
		userTime = new Date().getTimezoneOffset();
	if(customerTime !== userTime){ var widgetStartTime = (userTime - customerTime)*60000 + data.widgetData.startTime; }
	else{ widgetStartTime = data.widgetData.startTime; }

//	widgetStartTime = data.widgetData.startTime-(420*60000);

	$chrtObj.siblings("header").children(".wdgtName").html(data.widgetData.widgetName);
// LIST TABLE WIDGET
	if(data.widgetData.chartType == 1)
	{
		var wdgtTblObj = (source === 'xpnd')? 'xpndWdgt-'+wdgtID : 'wdgt-'+wdgtID ;
		wdgtDataArray['wdgt'+wdgtID]={};
		wdgtDataArray['wdgt'+wdgtID].wdgtByTime = {};
		wdgtDataArray['wdgt'+wdgtID].wdgtInterval = {};
		wdgtDataArray['wdgt'+wdgtID].listData = [];
		listDataArray=[];
		var $listHeader = $('<ul class="scrollingListHeader" id="'+wdgtTblObj+'Header" metricType="'+data.widgetData.metricTypeId+'">');
		var $listHeaderItem = $("<li>");
		var $dataList = $('<ul class="scrollingList widgetList" id="'+wdgtTblObj+'" class="setMaxHeight">');
		var colWidth = 100/data.widgetData.listHeaders.length+"%";
		var minWidth = 80*data.widgetData.listHeaders.length+"px";
		$listHeader.css("min-width", minWidth);
		$dataList.css("min-width", minWidth);
		for(x = 0; x < data.widgetData.listHeaders.length; x++){
			var colNum = x+1;
			var sortColStatus = data.widgetData.listHeadersSortBy[x];
			var sortOrder = data.widgetData.sortByLabel.split(":");
			
			if(sortColStatus){ 
				var sortDir = (sortOrder[0] === "asc")?"desc":"asc";
				var sortMsg = (sortOrder[0] === "asc")?sortDescendingMsg:sortAscendingMsg;
				var sortOptions = 'sort="obj'+x+':'+sortDir+'" tooltip="'+data.widgetData.listHeaders[x]+' : '+ sortMsg +'" class="col'+colNum+' sort clickable current"'; 
				var sortImg = (sortOrder[0] === "asc")?"ArwDown":"ArwUp"; }
			else{ 
				var sortOptions = 'sort="obj'+x+':asc" tooltip="'+data.widgetData.listHeaders[x]+' : '+ sortAscendingMsg +'" class="col'+colNum+' sort clickable"';
				var sortImg = "ArwDown"; }
			
			$listHeaderItem.append('<div '+ sortOptions +' style="width: '+colWidth+';"><p title="'+data.widgetData.listHeaders[x]+'">' + 
			data.widgetData.listHeaders[x] + ' <img alt="" class="sortIcn" src="'+imgLocalDir+'icn_'+sortImg+'.png"></p></div>'); }
		$listHeader.append($listHeaderItem);
		
		var listData = data.widgetData.listData;

		if(data.widgetData.startTime !== 0){ byTime = true; }	
		
		if(data.widgetData.timeInterval === dayMS){ timeInterval = "day"; }
		else if(data.widgetData.timeInterval === hourMS){ timeInterval = "hour"; }
		else if(data.widgetData.timeInterval === "Month"){ timeInterval = "month"; byTime = true; }

		wdgtDataArray['wdgt'+wdgtID].wdgtByTime = byTime;		
		wdgtDataArray['wdgt'+wdgtID].wdgtInterval = timeInterval;
		
		if(listData instanceof Array === false || listData.length === 1){ 
			var $listDataItem = $("<li>");
			var itemData = {};
			for(var y = 0; y < listData.row.length; y++){
				var colNum = y+1, objItem = "obj"+y;
				if(y === 0 && byTime){
					value = (timeInterval === "month")? value.replace(" ", " 01 ") : listData.row[y];
					value = (timeInterval === "hour")? value.substr(0,value.length-2) : value;
					value = Date.parse(value); }
				else{ value = listData.row[y] }
				itemData[objItem] = value;
				$listDataItem.append(wdgtListDataPoint(value, colNum, colWidth, data.widgetData.metricTypeId, byTime, timeInterval)); }
			$dataList.append($listDataItem)
			listDataArray.push(itemData);
			wdgtDataArray['wdgt'+wdgtID].listData = listDataArray; }
		else{
			for(x = 0; x < listData.length; x++){
				var $listDataItem = $("<li>");
				var itemData = {};
				for(var y = 0; y < listData[x].row.length; y++){
					var colNum = y+1, objItem = "obj"+y;
					if(y === 0 && byTime){
						value = (timeInterval === "month")? listData[x].row[y].replace(" ", " 01 ") : listData[x].row[y];
						value = (timeInterval === "hour")? value.substr(0,value.length-2) : value;
						value = Date.parse(new Date(value)); }
					else{ value = listData[x].row[y] }
					itemData[objItem] = value;
					$listDataItem.append(wdgtListDataPoint(value, colNum, colWidth, data.widgetData.metricTypeId, byTime, timeInterval)); }
				$dataList.append($listDataItem);
				listDataArray.push(itemData); }
			wdgtDataArray['wdgt'+wdgtID].listData = listDataArray; }	
				
		$chrtObj.removeClass("chartGraph").addClass("listGraph");
		$chrtObj.html($listHeader).append($dataList);
		
		$chrtObj.parents(".widgetObject").children(".ddMenu").children(".menuOptions").children(".svPDF").hide();

		var objOffset = 200;
		heightLimit = $(window).height()-objOffset-20;
		$('#'+wdgtTblObj).css("max-height", heightLimit+"px");
		
		scrollListWidth([wdgtTblObj]);
	}
//SINGLE VALUE  // metricTypeId EffectedArea
 	else if(data.widgetData.chartType == 7)
	{ 
		var	tempTypeId = data.widgetData.metricTypeId,
			widgetValue;
		if(tempTypeId in dollarVal){ 
			widgetValue = (data.widgetData.singleValue < 0)? '($' + Highcharts.numberFormat(Math.abs(data.widgetData.singleValue), 2) + ')' : '$' + Highcharts.numberFormat(data.widgetData.singleValue, 2); } 
		else if(tempTypeId in numberVal){ widgetValue = data.widgetData.singleValue; } 
		else if(tempTypeId in timeVal){ widgetValue = data.widgetData.singleValue + '<span class="characterCaseBreak">' +timeMetric[tempTypeId] + '</span>'; } 
		else if(tempTypeId in percentageVal){ widgetValue = Highcharts.numberFormat(data.widgetData.singleValue, 1, '.') + '%'; }
		
		var singleValueHTML = 	'<section class="snglValueIcon"><img src="'+imgLocalDir + metricIcon[data.widgetData.metricTypeId] +'.jpg" width="125" height="125" border="0" /></section>' +
							'<section class="snglValue"><h1 class="valueDisplay">'+ widgetValue +'</h1></section>';
		
		$chrtObj.html(singleValueHTML);
		$chrtObj.parents(".widgetObject").children(".ddMenu").children(".menuOptions").children(".svPDF").remove();
		$chrtObj.parents(".widgetObject").children(".ddMenu").children(".menuOptions").children(".xpndWdgt").remove();
	}
// CHART TYPES
	else if(data.widgetData.chartType > 1 && data.widgetData.chartType < 7)
	{		
		$chrtObj.removeClass("listGraph").addClass("chartGraph");
		if($chrtObj.is(":hidden")){$chrtObj.addClass("resizeChartGraph");}
		
	//Test Single Series Data's Display Type to confirm if it will display if not switch to one that will
		if(data.widgetData.chartType === 3 || data.widgetData.chartType === 6 ){
			if(data.widgetData.seriesList instanceof Array === false){
				if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){ 
					if(data.widgetData.seriesList.seriesData instanceof Array === false){ data.widgetData.chartType = 5; chartSwitch = true;} } } }

		var tempChartOptions = {
			chart: { renderTo: $chrtObj.attr("id"), zoomType: 'x', resetZoomButton: { theme: { fill: '#eef5f8', stroke: '#dadcde', 
			style:{ color: '#92989d' }, r: 4, states: { hover: { fill: '#e5f0f4', stroke: '#aecede', style: { color: '#005782' } } } } }, type: chartTypes[data.widgetData.chartType] },
			title: { text: null, margin: 0 },
			xAxis: { dateTimeLabelFormats: timeFormat, labels : {overflow: 'justify'}, tickmarkPlacement: 'on' },
			yAxis: { dateTimeLabelFormats: timeFormat, labels : {overflow: 'justify'}, title: { text: null }, labels: { }, startOnTick: false },
			tooltip: { borderRadius: 6 },
			legend: { width: wdgtLgndWidth, x: -3, borderWidth: 1, borderRadius: 6, backgroundColor: '#f9fcfd', borderColor: '#e8e9ea', itemWidth: wdgtLgndItemWidth, itemMarginBottom: 3, itemMarginTop: 6, symbolRadius: 2, padding: 12, itemStyle: { "fontWeight": "normal", "color": "#005782"}, itemHoverStyle: {color: "#da7923"} },
			plotOptions: {
				line: { 	lineWidth: 2, states: { hover: { lineWidth: 4 } },
						marker: { enabled: false, symbol: 'circle', states: { hover: { enabled: true, fillColor: '#F88B2B'} } },
						events: { mouseOver: function(){ this.graph.attr("stroke", '#F88B2B'); this.group.toFront(); }, mouseOut: function(){ this.graph.attr("stroke", this.color); } } },
				area: {	lineWidth: 1, fillOpacity: 0.3, states: { hover: { lineWidth: 4 } }, shadow: false,
						marker: { enabled: false, symbol: 'circle', states: { hover: { enabled: true, fillColor: '#F88B2B'} } },
						events: { mouseOver: function(){ this.graph.attr("stroke", '#F88B2B'); this.group.toFront(); }, mouseOut: function(){ this.graph.attr("stroke", this.color); } } },
				column: {	states: { hover: { color: '#F88B2B' } }, borderColor: '#e8e9ea' },
				bar: {	states: { hover: { color: '#F88B2B' } }, borderColor: '#e8e9ea' },
				pie: { 	allowPointSelect: true, cursor: 'pointer', color: '#F88B2B', 
						dataLabels: { enabled: true, color: '#555F66', connectorColor: '#92989D', distance: 25, crop: false, 
						backgroundColor: '#F4F5F6', borderColor: '#DADCDE', borderWidth: 1, borderRadius: 4,
						formatter: function() { return '<strong>'+ this.point.name +'</strong>: <br />'+ Highcharts.numberFormat(this.percentage, 1, '.') +' %'; } } }
			},
			credits: { enabled: false },	
			navigation: { buttonOptions: { enabled: false } },
			series:[]
		};

//SETS STACKING FOR BAR, COLUMN AND AREA CHART TYPES
		if(data.widgetData.chartType >= 4 && data.widgetData.chartType <= 6){ tempChartOptions.plotOptions.series = {stacking: 'normal'}; }

//SETS CROSSHAIRS FOR LINE CHART
		if(data.widgetData.chartType === 3){ tempChartOptions.tooltip.crosshairs = [{ width: 1, color: "#CFD2D4" }, { width: 1, color: "#CFD2D4" }]; }

//CREATE SERIES DATA
		if(data.widgetData.seriesList instanceof Array === false){
			var seriesList = [data.widgetData.seriesList];
		} else { seriesList = data.widgetData.seriesList; }
			
		var tier1Names = data.widgetData.tier1Names;
		if(!tier1Names) { tier1Names = [""]; }
		if(tier1Names instanceof Array === false){ tier1Names = [tier1Names] }
		if(seriesList.length === 1){
			var seriesListLength = 1;
			var seriesName = seriesList[0].seriesName;			
			var tier1Data = seriesList[0].seriesData;
			if(!tier1Data) {
				tier1Data = [0];
			}
			if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
				if(tier1Data instanceof Array === false){ singleSeriesSingleData = true; tier1Data = [tier1Data] }
				if(data.widgetData.chartType == 2){
					var pieData = new Array();
					for(var x=0; x < tier1Names.length; x++){
						var tempPieObj = new Array();
						tempPieObj[0] = tier1Names[x];
						tempPieObj[1] = tier1Data[x];
						pieData[x] = tempPieObj; }				
					tempChartOptions.series.push({ data : pieData});
					}
				else{
					tempChartOptions.series.push({ name : seriesName, data : tier1Data}) } } 
			else {
				if(data.widgetData.timeInterval === dayMS){
					var tempTier1Data = new Array();
					
					for(var x = 0; x < tier1Data.length; x++)
					{
						tempTier1Data.push([Date.parse(data.widgetData.tier1Names[x]), tier1Data[x]]);
					}
					tempChartOptions.series.push({
						name : seriesName,
						data : tempTier1Data }); }
				else{				
					tempChartOptions.series.push({
						name : seriesName,
						data : tier1Data,
						pointStart: widgetStartTime,
						pointInterval: data.widgetData.timeInterval }); 
				} 
			}
			
			if( data.widgetData.chartType == 2 )
			{
				if( typeof pieData != "undefined" ) {
					tempChartOptions.legend.enabled = true;
					if( pieData.length >= 14 ) { tempChartOptions.colors = colorList[14]; }
					else { tempChartOptions.colors = colorList[pieData.length]; } 
				}
			}
			else { tempChartOptions.colors = colorList[1]; } 
		}
		//else{ if(!targets){tempChartOptions.legend.enabled = false;} tempChartOptions.colors = colorSngl;}
		else {
			var seriesListLength = seriesList.length;
			if(seriesListLength >= 14){ tempChartOptions.colors = colorList[14]; }
			else{ tempChartOptions.colors = colorList[seriesListLength]; }
			
			for(x = 0; x < seriesListLength; x++)
			{
				var seriesData = seriesList[x].seriesData;
				if(!seriesData) {
					seriesData = [0];
				}
				
				if(seriesData instanceof Array === false){ singleSeriesSingleData = true; seriesData = [seriesData]; }
				if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
					if(!seriesList[x].stackName){
						tempChartOptions.series.push({
							name: seriesList[x].seriesName, 
							data :seriesData }); } 
					else {
						tempChartOptions.series.push({
							name: seriesList[x].seriesName, 
							data : seriesData,
							stack : seriesList[x].stackName }); } } 
				else {
					
					if(data.widgetData.timeInterval === dayMS){
						var tempTier1Data = new Array();
						
						for(xx = 0; xx < seriesData.length; xx++)
						{
							tempTier1Data.push([Date.parse(data.widgetData.tier1Names[xx]), seriesData[xx]]);
						}
						if(!seriesList[x].stackName){
							tempChartOptions.series.push({
								name: seriesList[x].seriesName, 
								data : tempTier1Data });
						} else {
							tempChartOptions.series.push({
								name: seriesList[x].seriesName, 
								data : tempTier1Data,
								stack : seriesList[x].stackName }); } }
					else{
						if(!seriesList[x].stackName){
							tempChartOptions.series.push({
								name: seriesList[x].seriesName, 
								data : seriesData,
								pointStart: widgetStartTime,
								pointInterval: data.widgetData.timeInterval });
						} else {
							tempChartOptions.series.push({
								name: seriesList[x].seriesName, 
								data : seriesData,
								stack : seriesList[x].stackName,
								pointStart: widgetStartTime,
								pointInterval: data.widgetData.timeInterval }); } } } } }
//SETS TARGET VALUE
		if(data.widgetData.trendValue && data.widgetData.trendValue instanceof Array === true){ 
			tempChartOptions.series.push({ 
				name: "Target " + metricTypes[data.widgetData.metricTypeId], 
				data : data.widgetData.trendValue, type: 'spline', color: '#DA7923', zIndex: 800, lineWidth: 0 }); }
		else if(data.widgetData.trendValue && data.widgetData.trendValue != "null"){
			if(singleSeriesSingleData == true){ var seriesLength = 1; }
			else if(seriesList.length && seriesList[0].seriesData.length){ var seriesLength = seriesList[0].seriesData.length;}
			else { var seriesLength = seriesListLength;}

			var TargetLine = new Array();
			for(var y = 0; y < seriesLength; y++){ 
				if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){TargetLine[y] = [y, data.widgetData.trendValue]}
				else{TargetLine[y] = data.widgetData.trendValue} }
			if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
				if(seriesLength == 1){
					tempChartOptions.series.push({ 
						name: "Target " + metricTypes[data.widgetData.metricTypeId], 
						data : TargetLine, type: 'spline', color: '#DA7923', width: 2, zIndex: 800, lineWidth: 0 }); }
				else{
					tempChartOptions.series.push({ 
						name: "Target " + metricTypes[data.widgetData.metricTypeId], 
						data : TargetLine, type: 'line', color: '#DA7923', width: 2, zIndex: 800, dashStyle: 'longdash'}); } }
			else{
				if(data.widgetData.timeInterval === dayMS){
						var tempTargetLineData = new Array();
						
						for(x = 0; x < TargetLine.length; x++)
						{
							tempTargetLineData.push([Date.parse(data.widgetData.tier1Names[x]), TargetLine[x]]);
						}
						tempChartOptions.series.push({ 
						name: "Target " + metricTypes[data.widgetData.metricTypeId], 
						data : tempTargetLineData, type: 'line', color: '#DA7923', width: 2, zIndex: 800, dashStyle: 'longdash'}); }
					else{
						tempChartOptions.series.push({ 
						name: "Target " + metricTypes[data.widgetData.metricTypeId], 
						data : TargetLine, type: 'line', color: '#DA7923', width: 2, zIndex: 800, dashStyle: 'longdash', 
						pointStart: widgetStartTime, pointInterval: data.widgetData.timeInterval}); } } }
//SET xAxis VALUES
		if(!data.widgetData.timeInterval || data.widgetData.timeInterval === "Month"){
			tempChartOptions.xAxis.categories = tier1Names;
			if(data.widgetData.chartType != 4){ tempChartOptions.xAxis.labels = { rotation: -65, align: 'right', style: { fontSize: '1em' } };};
		} else {
			tempChartOptions.xAxis.type = 'datetime'; 
			tempChartOptions.xAxis.tickInterval = data.widgetData.timeInterval;
			if(data.widgetData.chartType != 4){ tempChartOptions.xAxis.labels = { rotation: -65, align: 'right', style: { fontSize: '1em' } };};
		}
//SET yAxis VALUES
		if(data.widgetData.metricTypeId === 4 || data.widgetData.metricTypeId === 9){ tempChartOptions.yAxis.allowDecimals = false; }
	
//SETS VALUE DISPLAY ON METRIC TYPE  // metricTypeId EffectedArea
		if(data.widgetData.metricTypeId in dollarVal){ dollarToolTip(tempChartOptions, data);} 
		else if(data.widgetData.metricTypeId in numberVal){ countToolTip(tempChartOptions, data);} 
		else if(data.widgetData.metricTypeId in timeVal){ timeToolTip(tempChartOptions, data);} 
		else if(data.widgetData.metricTypeId in percentageVal){ percentageToolTip(tempChartOptions, data);}
		
//TRIGGERS CHART
		tempChart = new Highcharts.Chart(tempChartOptions, 
			function(tempChart){
				if(seriesListLength){					
					$(tempChart.series).each(function(i, serie){
						if(data.widgetData.chartType == 3 || data.widgetData.chartType == 6){
							$(serie.legendItem.element).on('hover',
								function(){ serie.graph.attr("stroke", '#F88B2B'); serie.group.toFront(); }, 
								function(){ serie.graph.attr("stroke", serie.color); });}
						
						if(data.widgetData.chartType == 4 || data.widgetData.chartType == 5){
							serie.legendItem
								.on('mouseover', function(){$(serie.data).each(function(j, point){ point.graphic.attr('fill', '#F88B2B') }) })
								.on('mouseout', function(){ $(serie.data).each(function(j, point){ point.graphic.attr('fill', serie.color) }) }) } }); } });
		
		// the button handler
		$chrtObj.parents(".widgetObject").children(".ddMenu").children(".menuOptions").children(".svPDF").on('click',
			function(event) { event.preventDefault(); 
			tempChart.exportChart({ type: "application/pdf", filename: escapeFileName(data.widgetData.widgetName) }, { title: { text: data.widgetData.widgetName, margin: 12 } } ); });
//DISPLAY CHART SWITCH MESSAGE			
		if(chartSwitch === true){ $chrtObj.prepend(chartSwitchMsgObj); }
//DISPLAY NO DATA RETURNED MESSAGE
		if(dataStatus == 249){ $chrtObj.prepend("<section class='noWdgtData'><span>"+ widgetNoDataMsg +"</span></section>") };
	}
// MAP WIDGET
	else if (data.widgetData.chartType == 8)
	{
		$chrtObj.removeClass("chartGraph listGraph");
		
		if(source === 'xpnd'){ var mapHeight = $("#xpndContent").innerHeight(); }
		else if($chrtObj.innerWidth() > 800 && $chrtObj.innerWidth() < 1200){var mapHeight = $chrtObj.innerWidth()/2;}
		else if($chrtObj.innerWidth() > 1200){var mapHeight = 600;}
		else{var mapHeight = 400;}
		
		var mapObj = (source === 'xpnd')? 'xpndMap_'+wdgtID : 'map_'+wdgtID;
		
		$chrtObj.html("<section id='"+mapObj+"' class='map' style='height: "+mapHeight+"px;'></section>");
		
		var payStationMarkers = new Array();
		if(data.widgetData.widgetMapInfo.mapEntries){
			var mapEntries = data.widgetData.widgetMapInfo.mapEntries;
			if(mapEntries instanceof Array === false){ mapEntries = [mapEntries]; }
			for(x = 0; x < mapEntries.length; x++)
			{ //mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
				payStationMarkers.push(new mapMarker(
					mapEntries[x].latitude, 
					mapEntries[x].longitude, 
					mapEntries[x].name, 
					mapEntries[x].payStationType,
					mapEntries[x].randomId, 
					mapEntries[x].alertIcon,'','','','','','',
					mapEntries[x].serialNumber )); } }
		
		initMapWidget(mapObj, payStationMarkers);
		
		$chrtObj.parents(".widgetObject").find(".svPDF, .svCSV").remove();
		
		var	$mapDetails = $("<section id='statusDate' class='mapOverlay'></section>");
			$mapDetails.append("last updated: " + data.widgetData.widgetMapInfo.lastUpdate);
			$chrtObj.append($mapDetails);

		var	$mapPin = $("<a href='/secure/settings/locations/payStationPlacement.html?"+document.location.search.substring(1)+"' class='linkButtonIcn Ftr pin' title='"+mapPlacementTitle+"'></a>");
			$mapPin.append("<img width='18' height='1' border='0' src='"+imgLocalDir+"spacer.gif' alt='"+mapPlacementTitle+"' title='"+mapPlacementTitle+"'>");
		if(data.widgetData.widgetMapInfo.missingPayStation && mapPin){ $chrtObj.append($mapPin) } else { $chrtObj.find(".pin").hide(); }
	}
// HEAT MAP WIDGET
	else if (data.widgetData.chartType == 9) {
		if(source === 'xpnd'){ var mapHeight = $("#xpndContent").innerHeight(); }
		else if($chrtObj.innerWidth() > 800 && $chrtObj.innerWidth() < 1200){var mapHeight = $chrtObj.innerWidth()/2;}
		else if($chrtObj.innerWidth() > 1200){var mapHeight = 600;}
		else{var mapHeight = 400;}
		
		var mapObj = (source === 'xpnd')? 'xpndMap_'+wdgtID : 'map_'+wdgtID,
			showHeatObj = (source === 'xpnd')? 'xpndShowHeat_'+wdgtID : 'showHeat_'+wdgtID;
		
		$chrtObj.removeClass("chartGraph listGraph");
		$chrtObj.html("<section id='"+mapObj+"' class='map' style='height: "+mapHeight+"px;'></section>");
		
		var payStationMarkers = new Array();
		if(data.widgetData.widgetMapInfo.mapEntries){
			var mapEntries = data.widgetData.widgetMapInfo.mapEntries;
			if(mapEntries instanceof Array === false){ mapEntries = [mapEntries]; }
			for(x = 0; x < mapEntries.length; x++)
			{ //mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
				payStationMarkers.push(new mapMarker(
					mapEntries[x].latitude, 
					mapEntries[x].longitude, 
					mapEntries[x].name, '','','',
					mapEntries[x].lastSeen,'','','','','',
					mapEntries[x].serialNumber )); } }
		
		initHeatMapWidget(mapObj, payStationMarkers);
		
		$chrtObj.parents(".widgetObject").find(".svPDF, .svCSV").remove();
		
		var	$mapDetails = $("<section id='statusDate' class='mapOverlay'></section>");
			$mapDetails.append("last updated: " + data.widgetData.widgetMapInfo.lastUpdate);
			$chrtObj.append($mapDetails);

		if(payStationMarkers.length > 0){
			var	$heatMapToggle = $("<section>").attr("id", "heatMapOverlay").addClass("mapOverlay");
				$heatMapToggle.append(
					"<input type='hidden' name='showHeat' id='"+showHeatObj+"Hidden' value='true'>"+
					"<label class='checkboxLabel switch' for='"+showHeatObj+"Hidden'><a href='#' id='"+showHeatObj+"' class='checkBox checked'>&nbsp;</a> Show Heat Map</label>");
			//'<section style="position: absolute; top: 50px; right: 24px;" >HEAT MAP TOGGLE</section>';
				$chrtObj.append($heatMapToggle) }
		else if(data.widgetData.isEmpty){
			$chrtObj.append("<section class='noWdgtData noHeatMap'><span>"+ widgetNoDataMsg +"</span></section>"); } }
// OCCUPANCY MAP WIDGET
	else if (data.widgetData.chartType === 10) {
		var parkingAreaMarkers = "noData",
			mapObj = (source === 'xpnd')? 'xpndMap_'+wdgtID : 'map_'+wdgtID;
		
		if(source === 'xpnd'){ var mapHeight = $("#xpndContent").innerHeight(); }
		else if($chrtObj.innerWidth() > 800 && $chrtObj.innerWidth() < 1200){var mapHeight = $chrtObj.innerWidth()/2;}
		else if($chrtObj.innerWidth() > 1200){var mapHeight = 600;}
		else{var mapHeight = 400;}
			
		$chrtObj.removeClass("chartGraph listGraph");
		$chrtObj.html("<section id='"+mapObj+"' class='map' style='height: "+mapHeight+"px;'></section>");
			
			if(data.widgetData.widgetMapInfo.geoLocationEntries){
				var parkingAreaEntries = data.widgetData.widgetMapInfo.geoLocationEntries,
					locationOccupancy, localName, localJsonData, localLevel, localCount, 
					autoCount = false; //False if no value is coming through for Counted Occupancy / AutoCount
				parkingAreaEntries = (parkingAreaEntries instanceof Array === false)? [parkingAreaEntries] : parkingAreaEntries;
				parkingAreaMarkers = [];
	
				for(x = 0; x < parkingAreaEntries.length; x++){
					localName = parkingAreaEntries[x].locationName;
					localJsonData = JSON.parse(parkingAreaEntries[x].jsonString);

					if(parkingAreaEntries[x].physicalOccupancy){
						localCount = parkingAreaEntries[x].physicalOccupancy;
						autoCount = true; }
					else { 
						localCount = parkingAreaEntries[x].paidOccupancy;
						autoCount = false; }
					if(localCount < 70){ localLevel = 3; } else if(localCount < 90){ localLevel = 2; } else{ localLevel = 1; }
					
					locationOccupancy = new mapLocationOccupancyObj(localName, localJsonData, localLevel, localCount, autoCount);
					parkingAreaMarkers.push(locationOccupancy); }}
				
			initOccupancyMapWidget(mapObj, parkingAreaMarkers);
			
			$chrtObj.parents(".widgetObject").find(".svPDF, .svCSV").hide();
			
			var	$mapDetails = $("<section id='statusDate' class='mapOverlay'></section>");
				$mapDetails.append("last updated: " + data.widgetData.widgetMapInfo.lastUpdate);
				$chrtObj.append($mapDetails);
			if(parkingAreaMarkers === "noData"){
				$chrtObj.append("<section class='noWdgtData noHeatMap'><span>"+ widgetNoDataMsg +"</span></section>"); } }
	
	dashboardNav();
} }

//--------------------------------------------------------------------------------------

function loadChart(wdgtID)
{
	var ajaxChartDisplay = GetHttpObject({"showLoader": false});
	ajaxChartDisplay.onreadystatechange = function()
	{
		if(ajaxChartDisplay.readyState==1){
			if(!$("#content_"+wdgtID).find(".loading").length){  $("#content_"+wdgtID).html(loadObj); } }
		else if(ajaxChartDisplay.readyState==4 && ajaxChartDisplay.getResponseHeader("CustomStatusCode") == 209){
			$('#content_'+wdgtID).addClass("chartGraph");
			$("#content_"+wdgtID).html("<section class='noWdgtData noData'><span>"+ widgetDataMsg +"</span></section>");
			$("#widgetID_"+wdgtID).children(".widget").children("header").children(".menu").addClass("editOnly"); }//- Too much data (over 2000 rows from the database)		
		else if(ajaxChartDisplay.readyState==4 && (ajaxChartDisplay.status == 200 || ajaxChartDisplay.getResponseHeader("CustomStatusCode") == 249)){
			$("#widgetID_"+wdgtID).find(".widgetData").html(ajaxChartDisplay.responseText).attr("status", ajaxChartDisplay.getResponseHeader("CustomStatusCode"));
			displayChart(wdgtID); } 
		else if(ajaxChartDisplay.readyState==4){
			$('#content_'+wdgtID).addClass("chartGraph");
			$("#content_"+wdgtID).html("<section class='noWdgtData noData'><span>"+ widgetNoResultMsg +"</span></section>");
			$("#widgetID_"+wdgtID).children(".widget").children("header").children(".menu").addClass("editOnly"); } };
	ajaxChartDisplay.open("GET","/secure/dashboard/widget.html?widgetID="+wdgtID+"&"+document.location.search.substring(1),true);
	ajaxChartDisplay.send();
}

//--------------------------------------------------------------------------------------

function chartRefresh(status)
{
	$(".chartGraph:visible").each(function(index) {
		if(status == "tabSwitch"){ if($(this).hasClass("resizeChartGraph") == true){ displayChart($(this).attr("id").replace("content_", "")); $(this).removeClass("resizeChartGraph"); } }
		else if(status == "layoutChange"){ displayChart($(this).attr("id").replace("content_", "")); }
	});
}

//--------------------------------------------------------------------------------------

function modifyWdgtSttngForm(settingDetails, statusStep, wdgtDialogSettings) // PUTS WIDGET SETTINGS INTO FORM ON INITIAL AND UPDATE REQUESTS FOR SETTINGS
{
	if(statusStep == "new"){if($("#wdgtSettingDialog.ui-dialog-content").length){$("#wdgtSettingDialog.ui-dialog-content").dialog("destroy");}}
	
	var data = JSON.parse(settingDetails.responseText);
	var filterSelect = null;
	if(data.widgetSettingsOptions.targetValue){ var targetValue = data.widgetSettingsOptions.targetValue }else{var targetValue = ""};
	var filterList = false;
	
	optionBasedObj[0] = data.widgetSettingsOptions.rateTypes;
	optionBasedObj[1] = data.widgetSettingsOptions.organizationTypes;
	optionBasedObj[2] = data.widgetSettingsOptions.locationTypes; 
	optionBasedObj[3] = data.widgetSettingsOptions.routeTypes;
	optionBasedObj[4] = data.widgetSettingsOptions.transactionTypes;
	optionBasedObj[5] = data.widgetSettingsOptions.revenueTypes;
	optionBasedObj[6] = data.widgetSettingsOptions.collectionTypes;
	optionBasedObj[7] = data.widgetSettingsOptions.merchantTypes;
	optionBasedObj[8] = data.widgetSettingsOptions.alertTypes;
	optionBasedObj[9] = data.widgetSettingsOptions.parentLocationTypes;
	optionBasedObj[10] = data.widgetSettingsOptions.cardProcessMethodTypes;
	optionBasedObj[11] = data.widgetSettingsOptions.citatTypes;

	var filterCount = new Array(5, 10, 15, 20, 25);	

	$("#tier1Attr, #tier2Attr, #tier3Attr").val("");
	
	$("#wdgtSettings").find("#widgetName").val(data.widgetSettingsOptions.widgetName)
	
	for(x = 0; x < data.widgetSettingsOptions.metricTypes.length; x++){ 
		if(data.widgetSettingsOptions.metricTypes[x].status == "selected"){ 
			$("#wdgtSettings").find("#metricType").html(data.widgetSettingsOptions.metricTypes[x].name);
			metricLabel = data.widgetSettingsOptions.metricTypes[x].label;
		} 
	}
	
	for(var x = 0; x < data.widgetSettingsOptions.filterTypes.length; x++){ if(data.widgetSettingsOptions.filterTypes[x].status == "selected"){ filterSelect = x; } }
	
	//FILTER
	if(filterSelect != null && filterSelect != 0)
	{
		filterList = true;
		$("#filterArea, #tier1MenuArea").show();
		$(".tier1By").hide();
		$("#wdgtSettings").find("#filterType").html(data.widgetSettingsOptions.filterTypes[filterSelect].name);
		$("#filter").val(data.widgetSettingsOptions.filterTypes[filterSelect].randomId);
		var filterOptions = [];
		var filterSelected = "";
		for(x = 0; x < filterCount.length; x++){
			if(filterCount[x] == data.widgetSettingsOptions.length){ filterSelected = filterCount[x]; }
			filterOptions.push({ "label": filterCount[x], "value": filterCount[x] });
		}
		$("#filterLength").autoselector({"isComboBox": true, "data": filterOptions})
			.autoselector("setSelectedValue", filterSelected, true)
			.on("itemSelected", function(event, ui) { updateWidgetSettings($(this).attr("id")) }); 
	} else { $("#filterArea, #tier1MenuArea").hide(); $(".tier1By, #tier1SnglVal").show(); }
	
//TIER1
	var tier1Options = [];
	var tier1Selected = '';
	var tier1SelectedValueID = '';
	var tier1DefaultID = '';
	var tier1Value = false;
	for(var x = 0; x < data.widgetSettingsOptions.tier1Types.length; x++)
	{
		var optionStatus = data.widgetSettingsOptions.tier1Types[x].status;
		var optionName = data.widgetSettingsOptions.tier1Types[x].name;
		var optionID = data.widgetSettingsOptions.tier1Types[x].randomId;
		var optionLabel = data.widgetSettingsOptions.tier1Types[x].label;
		if(optionStatus != "disabled" && optionStatus != "inactive") {
			if(optionStatus == "selected"){
				if(x > 0){ tier1Value = true; tier1Selected = optionLabel; tier1Name = optionName; }
				tier1SelectedValueID = optionID; }
			tier1Options.push({ "label": optionName, "value": optionID }); } }	
	
	if(tier1Options.length === 1){ 
		$("#tier1MenuArea").hide();
		if(tier1Options[0].label === "N/A"){ $(".tier1By").hide(); }
		else{ $("#tier1SnglVal").html(tier1Options[0].label); }
		$("#tier1MenuVal").val(tier1Options[0].value);
		$("#tier2Switch").show(); }
	else{ 
		if(tier1Value == false){ $("#tier1Msg, .tier1By").hide(); }
		else if(tier1Value == true){ $("#tier2Switch").show(); $("#tier1SnglVal").html(tier1Name);}
		if(statusStep === "update"){ $("#tier1Menu").autoselector({"data": tier1Options}); }
		else{
			$("#tier1Menu").autoselector({
				"isComboBox": true,
				"defaultValue": tier1DefaultID,
				"data": tier1Options
			}).autoselector("setSelectedValue", tier1SelectedValueID, true).on("itemSelected", function(event, ui) { updateWidgetSettings($(this).attr("id")) }); }}
	
	//TIER1 OPTIONS
	var t1OptFullList = "";
	var t1OptselectedList = "";
	var showOptions = false;
	var t1SelectedCount = 0;
	
	if(tier1Value == true && (filterSelect == null || filterSelect == 0)) {
		if(tier1Selected == 9583) { 
			t1SelectedCount = revenueTypeFormatting($("#tier1Ops ul.fullList"), $("#tier1Ops ul.selectedList"), $("#tier1Ops  #tier1OpsAll"), $("#tier1Attr"), $("#tier1Ops ul.fullList, #tier1Ops ul.selectedList"), $("#tier2Switch"));
			showOptions = (t1SelectedCount > 0)? true : false ; }
		else { // ALL OTHER TIER OPTIONS
			$("#tier2Switch").removeClass("noTierOptions");
			for(x = 0; x < optionBasedTier.length; x++){
				if(tier1Selected == optionBasedTier[x]){ 
					if(optionBasedObj[x] instanceof Array === false){ optionBasedObj[x] = [optionBasedObj[x]]; }
					showOptions = true;
					var allSelected = true;
					for(var y = 0; y < optionBasedObj[x].length; y++){
						if(optionBasedObj[x][y]){
							var optionStatus = optionBasedObj[x][y].status;
							var optionName = optionBasedObj[x][y].name;
							var optionID = optionBasedObj[x][y].randomId;
							
							if(optionStatus != "selected" && allSelected == true){ allSelected = false; }
							if(optionStatus == "selected"){  
								t1SelectedCount++; 
								if($("#tier1Attr").val() == ""){ $("#tier1Attr").val(optionID); }else{ $("#tier1Attr").val($("#tier1Attr").val()+","+optionID); }}
							t1OptFullList += '<li id="full-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';
							t1OptselectedList += '<li id="select-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';}
						else{ 
							allSelected = false;
							t1OptFullList += '<li class="notclickable alignCenter"><span class="textLine">'+noItemsExist+'</span></li>'; }}
					$("#tier1Ops ul.fullList").html(t1OptFullList);
					$("#tier1Ops ul.selectedList").html(t1OptselectedList);
					$("#tier1Ops  #tier1OpsAll").parents("label").show();
					if(allSelected == true){$("#tier1Ops  #tier1OpsAll").addClass("checked")} 
					else {$("#tier1Ops  #tier1OpsAll").removeClass("checked")}
					if(tier1Selected === 4758) {// ORGANIZATION TYPE
						if(t1SelectedCount !== 1){ $("#tier2Switch").hide(); }
						$("#tier1Ops ul.fullList, #tier1Ops ul.selectedList").addClass("orgList"); }
					else {
						$("#tier1Ops ul.fullList, #tier1Ops ul.selectedList").removeClass("orgList"); } } } }
		if(showOptions == true){ $("#tier1Ops").slideDown(); } }
	else {
		$("#tier1Ops").slideUp(function(){
			$("#tier1Ops ul.fullList, #tier1Ops ul.selectedList").html("");
			$("#tier1Ops  #tier1OpsAll").removeClass("checked");
			$("#tier1Attr").val("");}); }
	
//DURATION
	var	rangeOptions = [],
		rangeID = '',
		rangeDefaultID = '';
	for(var x = 0; x < data.widgetSettingsOptions.rangeTypes.length; x++){
		var optionStatus = data.widgetSettingsOptions.rangeTypes[x].status;
		var optionName = data.widgetSettingsOptions.rangeTypes[x].name;
		var optionID = data.widgetSettingsOptions.rangeTypes[x].randomId;
		
		if(optionStatus != "disabled" && optionStatus != "inactive") {
			if(optionStatus == "selected"){ rangeID = optionID; }
			rangeOptions.push({ "label": optionName, "value": optionID });		
		}
	} 
	if(rangeOptions.length === 1){ 
		$("#rangeOptMenu").hide();
		$("#rangeSnglVal").html("<strong class='left'>"+rangeOptions[0].label+"</strong>").show(); 
		$("#rangeMenuVal").val(rangeOptions[0].value); }
	else{
		$("#rangeSnglVal").hide();
		$("#rangeOptMenu").show();
		if(statusStep === "update"){ $("#rangeMenu").autoselector({"data": rangeOptions}); }
		else{
			$("#rangeMenu").autoselector({
				"isComboBox": true,
				"defaultValue": rangeDefaultID,
				"data": rangeOptions
			}).autoselector("setSelectedValue", rangeID, true).on("itemSelected", function(event, ui) { updateWidgetSettings($(this).attr("id")) }); }
	}
//TIER2
	var tier2Options = [];
	var tier2Selected = '';
	var tier2SelectedValueID = '';
	var tier2Value = false;
	var optionCount = 0;
	var tier2DefaultID = '';
		
	for(var x = 0; x < data.widgetSettingsOptions.tier2Types.length; x++) {
		var optionStatus = data.widgetSettingsOptions.tier2Types[x].status;
		var optionName = data.widgetSettingsOptions.tier2Types[x].name;
		var optionID = data.widgetSettingsOptions.tier2Types[x].randomId;
		var optionLabel = data.widgetSettingsOptions.tier2Types[x].label;
		if((optionStatus != "disabled" && optionStatus != "inactive") || optionName == "N/A") {
			optionCount++;
			if(optionStatus == "selected"){ 
				if(x > 0){ tier2Value = true; tier2Selected = optionLabel; }
				tier2SelectedValueID = optionID; }
			if(optionName == "N/A"){ optionName = selectMenuChooseMsg; tier2DefaultID = optionID; }
			tier2Options.push({ "label": optionName, "value": optionID }); } }
	if(optionCount <= 1 || filterList === true){$("#tier2Switch").hide().addClass("noTierOptions");}
	if(tier2Value == true && statusStep != "update"){$("#tier2Switch").find(".tierCheck").trigger('click');}
	if(statusStep === "update"){ $("#tier2Menu").autoselector({"data": tier2Options}); }
	else{
		$("#tier2Menu").autoselector({
			"isComboBox": true,
			"defaultValue": tier2DefaultID,
			"data": tier2Options
		}).autoselector("setSelectedValue", tier2SelectedValueID, true).on("itemSelected", function(event, ui) { 
			var	objID = $(this).attr("id"),
				objVal = $(this).val();
			if(objVal === selectMenuChooseMsg && ($("#tier3Switch").is(":visible") && $("#tier3Switch").find(".checkBox").hasClass("checked"))){ 
				$("#tier3Switch").find(".tierCheck").trigger('click'); updateWidgetSettings(objID); } else { updateWidgetSettings(objID); } }); }
	
	//TIER2 OPTIONS
	var t2OptFullList = "";
	var t2OptselectedList = "";
	var showOptions = false;
	var selectedCount = 0;

	if(tier2Value == true) {
		if(tier2Selected == 9583) { // REVENUE TYPE
			showOptions = revenueTypeFormatting($("#tier2Ops ul.fullList"), $("#tier2Ops ul.selectedList"), $("#tier2Ops  #tier2OpsAll"), $("#tier2Attr"), $("#tier2Ops ul.fullList, #tier2Ops ul.selectedList"), $("#tier3Switch")); }
		else { // ALL OTHER TIER OPTIONS
			$("#tier3Switch").removeClass("noTierOptions");
			for(x = 0; x < optionBasedTier.length; x++){	
				if(tier2Selected == optionBasedTier[x]){ 
					if(optionBasedObj[x] instanceof Array === false){ optionBasedObj[x] = [optionBasedObj[x]]; }
					showOptions = true;
					var allSelected = true;
						for(var y = 0; y < optionBasedObj[x].length; y++){
							if(optionBasedObj[x][y]){
								var optionStatus = optionBasedObj[x][y].status;
								var optionName = optionBasedObj[x][y].name;
								var optionID = optionBasedObj[x][y].randomId;
								var orgID = optionBasedObj[x][y].orgId;
								
								if((tier1Selected != 4758 || tier2Selected === 8375 || tier2Selected === 9951) || (tier1Selected === 4758 && orgID === $("#tier1Attr").val())) {
									if(optionStatus != "selected" && allSelected == true){ allSelected = false; }
									if(optionStatus == "selected"){  
										selectedCount++; 

										if($("#tier2Attr").val() == ""){ $("#tier2Attr").val(optionID); }
										else{ $("#tier2Attr").val($("#tier2Attr").val()+","+optionID); }}
									t2OptFullList += '<li id="full-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';
									t2OptselectedList += '<li id="select-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>'; }}
					else{
						allSelected = false;
						t2OptFullList += '<li class="notclickable alignCenter"><span class="textLine">'+noItemsExist+'</span></li>'; } }
					
					$("#tier2Ops ul.fullList").html(t2OptFullList);
					$("#tier2Ops ul.selectedList").html(t2OptselectedList);
					$("#tier2Ops #tier2OpsAll").parents("label").show();
					if(tier2Selected === 2957 && metricLabel === 9369) { // LOCATION TYPE FOR COMPARED OCCUPANCY
						allSelected = false; 
						$("#tier2Ops  #tier2OpsAll").removeClass("checked")
						$("#tier2Ops  #tier2OpsAll").parent("label").addClass("disabled");
						$("#tier2Ops ul.fullList, #tier2Ops ul.selectedList").addClass("snglValList"); }
					else{ 
						$("#tier2Ops  #tier2OpsAll").parent("label").removeClass("disabled");
						$("#tier2Ops ul.fullList, #tier2Ops ul.selectedList").removeClass("snglValList"); }
					if(allSelected == true){$("#tier2Ops  #tier2OpsAll").addClass("checked")} 
					else {$("#tier2Ops  #tier2OpsAll").removeClass("checked")}
					if(tier2Selected === 4758) {// ORGANIZATION TYPE	
						if(selectedCount !== 1){ $("#tier3Switch").hide(); } else { $("#tier3Switch").slideDown(); }
						$("#tier2Ops ul.fullList, #tier2Ops ul.selectedList").addClass("orgList"); }
					else {
						if(selectedCount === 1){$("#tier3Switch").slideDown();}
						$("#tier2Ops ul.fullList, #tier2Ops ul.selectedList").removeClass("orgList"); } } } }
		if(showOptions && !$("#tier2Ops").is(":visible")){ $("#tier2Ops").slideDown();  } }
	else {
		$("#tier2Ops").slideUp(function(){
			$("#tier2Ops ul.fullList, #tier2Ops ul.selectedList").html("");
			$("#tier2Ops #tier2OpsAll").removeClass("checked");
			$("#tier2Attr").val("");}); }
		
	
//TIER3
	var tier3Options = [];
	var tier3Selected = '';
	var tier3SelectedValueID = '';
	var tier3Value = false;
	var optionCount = 0;
	var inactiveCount = 0;
	var tier3DefaultID = '';

	for(var x = 0; x < data.widgetSettingsOptions.tier3Types.length; x++) {
		var optionStatus = data.widgetSettingsOptions.tier3Types[x].status;
		var optionName = data.widgetSettingsOptions.tier3Types[x].name;
		var optionID = data.widgetSettingsOptions.tier3Types[x].randomId;
		var optionLabel = data.widgetSettingsOptions.tier3Types[x].label;
		if(optionStatus != "disabled" || optionName == "N/A") {
			optionCount++;
			if(optionStatus == "selected"){ 
				if(x > 0){ tier3Value = true; tier3Selected = optionLabel; }
				tier3SelectedValueID = optionID; }
			if(optionName == "N/A"){ optionName = selectMenuChooseMsg; tier3DefaultID = optionID; }
			tier3Options.push({ "label": optionName, "value": optionID }); } }
	if(optionCount <= 1){$("#tier3Switch").hide().addClass("noTierOptions");} else { $("#tier3Switch").removeClass("noTierOptions"); }
	if(tier3Value == true && statusStep != "update"){$("#tier3Switch").find(".tierCheck").trigger('click');}
	if(statusStep === "update"){ $("#tier3Menu").autoselector({"data": tier3Options}); }
	else{
		$("#tier3Menu").autoselector({
			"isComboBox": true,
			"defaultValue": tier3DefaultID,
			"data": tier3Options
		}).autoselector("setSelectedValue", tier3SelectedValueID, true).on("itemSelected", function(event, ui) { updateWidgetSettings($(this).attr("id"));  }); }

	//TIER3 OPTIONS
	var t3OptFullList = "";
	var t3OptselectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	
	if(tier3Value == true){
		if(tier3Selected == 9583){ // REVENUE TYPE
			showOptions = revenueTypeFormatting($("#tier3Ops ul.fullList"), $("#tier3Ops ul.selectedList"), $("#tier3Ops  #tier3OpsAll"), $("#tier3Attr"), $("#tier3Ops ul.fullList, #tier3Ops ul.selectedList"), false); }
		else { // ALL OTHER TIER OPTIONS
			for(x = 0; x < optionBasedTier.length; x++){
				if(tier3Selected == optionBasedTier[x]){ 
					if(optionBasedObj[x] instanceof Array === false){ optionBasedObj[x] = [optionBasedObj[x]]; }
					showOptions = true;
					var allSelected = true;
						for(var y = 0; y < optionBasedObj[x].length; y++){
							if(optionBasedObj[x][y]){
								var optionStatus = optionBasedObj[x][y].status;
								var optionName = optionBasedObj[x][y].name;
								var optionID = optionBasedObj[x][y].randomId;
								var orgID = optionBasedObj[x][y].orgId;
								
								if((tier2Selected != 4758 || tier3Selected === 8375 || tier3Selected === 9951) || (tier2Selected === 4758 && orgID === $("#tier2Attr").val())) {
									if(optionStatus != "selected" && allSelected == true){ allSelected = false; }
									if(optionStatus == "selected"){  
										selectedCount++; 
										if($("#tier3Attr").val() == ""){ $("#tier3Attr").val(optionID); }
										else{ $("#tier3Attr").val($("#tier3Attr").val()+","+optionID); }}
									t3OptFullList += '<li id="full-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';
									t3OptselectedList += '<li id="select-'+ optionID +'" class="'+ optionStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>'; }}
					else{
						allSelected = false;
						t2OptFullList += '<li class="notclickable alignCenter"><span class="textLine">'+noItemsExist+'</span></li>';}}
								
					$("#tier3Ops ul.fullList").html(t3OptFullList);
					$("#tier3Ops ul.selectedList").html(t3OptselectedList);
					$("#tier3Ops  #tier3OpsAll").parents("label").show();
					if(allSelected == true){$("#tier3Ops  #tier3OpsAll").addClass("checked")} 
					else {$("#tier3Ops  #tier3OpsAll").removeClass("checked")} } } } 
		if(showOptions && !$("#tier3Ops").is(":visible")){ $("#tier3Ops").slideDown(); } }
	else {
		$("#tier3Ops").slideUp(function(){ 
			$("#tier3Ops ul.fullList, #tier3Ops ul.selectedList").html("");
			$("#tier3Ops  #tier3OpsAll").removeClass("checked");
			$("#tier3Attr").val("");}); }

	//TARGET
	$("#targetValueHidden").val(targetValue);
	if(metricLabel === 7830 && tier1Selected === 2957){$("#monthlyRev").show();} else {$("#monthlyRev").hide();}
	if(metricLabel === 5896 || metricLabel === 9321 || metricLabel === 4953){ $("#targetSwitch").hide(); }

	if(metricLabel === 7830 || metricLabel === 4782){
		$("#targetPre").html("$").show(); 
		$("#targetPost").html("").hide(); 
		$("#targetValueDisplay").attr("size", "8").css("text-align", "left");}
	else if(metricLabel === 4960 || metricLabel === 3945 || metricLabel === 9368 || metricLabel === 9369){
		$("#targetPre").html("").hide(); 
		$("#targetPost").html("%").show(); 
		$("#targetValueDisplay").attr("size", "4").css("text-align", "right");}
	else{
		$("#targetPre, #targetPost").html("").hide(); 
		$("#targetValueDisplay").attr("size", "8").css("text-align", "left");}
	
	if(targetValue != false)
	{ 
		if(statusStep !== "update"){ $("#targetSwitch").find(".targetCheck").trigger('click'); }
		if(targetValue === true && statusStep !== "update"){ $("#useMonthlyRevTarget").trigger('click'); }
		else if(targetValue != true){ $("#targetValueDisplay").val(targetValue); }
	}	

	//DISPLAY
	var displayHTML = '';
	var displayValue = false;
	var anyDisplayActive = false;
	var listOnlyDisplay = false;
	$("#chartType").val("");
	for(var x = 0; x < data.widgetSettingsOptions.displayTypes.length; x++)
	{
		var optionStatus = data.widgetSettingsOptions.displayTypes[x].status;
		var optionName = data.widgetSettingsOptions.displayTypes[x].name;
		var optionID = data.widgetSettingsOptions.displayTypes[x].randomId;
		var optionLabel = data.widgetSettingsOptions.displayTypes[x].label;
		
		if(optionStatus == "selected"){$("#chartType").val(optionID);}
		
		if(optionStatus == "active" && optionLabel != "3850"){ anyDisplayActive = true; }

		if((optionStatus == "selected" && optionLabel == "3850") || (anyDisplayActive == false && optionStatus == "active")) { 
			$("#chartDesignSV").show(); $("#targetSwitch, #target").hide(); }
		else if(optionStatus == "selected" && optionLabel == "3882"){
			$("#targetSwitch, #target").hide(); }
		else if((optionStatus == "selected" && optionLabel == "3882") && (filterSelect != null && filterSelect != 0)) { 
			$("#targetSwitch, #target").hide(); $("#chartDesignFL").show(); listOnlyDisplay = true;}
		else if(optionStatus == "selected" && (optionLabel === 4956 || optionLabel === 4957 || optionLabel === 4958)) {
			if(tier1Selected === 4758){ $("#chartDesignMap, #tier1SnglVal, .tier1By").show(); }
			else if(tier1Selected === 9323 && optionLabel === 4957){
				$(".chartDesignBox, #targetSwitch, #target").hide();
				$("#chartDesignHeatMap, #tier1SnglVal, .tier1By").show(); }
			else if(optionLabel === 4958){
				$(".chartDesignBox, #targetSwitch, #target").hide();
				$("#chartDesignOccupancyMap").show();}
			else{ 
				$(".chartDesignBox, #targetSwitch, #target, .rangeArea").hide();
				$("#chartDesignMap, #tier1MenuArea, .tier1By").show(); 
				$("#tier1SnglVal").hide(); }}
		else if(optionStatus == "selected" && optionLabel == "1034") { 
			 $("#targetSwitch, #target").hide(); }
		else if(optionLabel == "3850" && (optionStatus == "inactive" || optionStatus == "disabled")){
			if($("#chartDesignSV").is(":visible") ){ $("#chartDesignSV").hide(); } }

		if(optionLabel !== 3850 && optionLabel !== 4956 && optionLabel !== 4957 && optionLabel !== 4958)
		{ 
			displayHTML += '<li><section id="'+optionID+'" class="' +optionStatus+ ' lbl'+ optionLabel +'"';
			displayHTML += '><img src="'+imgLocalDir+'chartThumb_'+optionName+'.png" height="33" width="42" border="0">'+optionName+'</section></li>'; 
		}
	}
	$("#wdgtSettings").find("#chartDesign").html(displayHTML);
	if(t1SelectedCount === 1){$(".lbl1034, .lbl8395, .lbl6783").addClass("disabled");}
	if(metricLabel === 4960){$(".lbl1034").addClass("disabled");}
	if(listOnlyDisplay === true){ $("#wdgtSettings").find("#chartDesign > li > section").addClass("disabled"); } 
	
	if(statusStep == "new"){
		$("#wdgtSettingDialog").css('max-height', $(window).innerHeight()-150+"px"); 
		$("#wdgtSettingDialog").dialog(wdgtDialogSettings); btnStatus(applyChangesMsg); }
}

//--------------------------------------------------------------------------------------

function getWidgetSettingDetails(wdgtID, wdgtDialogSettings) // GETS WIDGET SETTINGS FOR INITIAL CALL FOR THE SETTINGS
{
	
	var ajaxGetSettingsDtls = GetHttpObject();
	ajaxGetSettingsDtls.onreadystatechange = function()
	{
		if (ajaxGetSettingsDtls.readyState==4 && ajaxGetSettingsDtls.status == 200){
			modifyWdgtSttngForm(ajaxGetSettingsDtls, "new", wdgtDialogSettings);
			testPopFormEdit($("#wdgtSettings"), $("#wdgtSettingDialog"));
		} else if(ajaxGetSettingsDtls.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxGetSettingsDtls.open("GET","/secure/dashboard/updateWidgetSettings.html?widgetID="+wdgtID+"&"+document.location.search.substring(1),true);
	ajaxGetSettingsDtls.send();
}

//--------------------------------------------------------------------------------------

function updateWidgetSettings(changeObjID) // UPDATES WIDGET SETTINGS WHEN USERS MAKE A CHANGE IN THE WIDGET SETTINGS FORM
{
	var widgetFormContent = $("#"+changeObjID).parents("form").serialize();
	var params = widgetFormContent;
	var ajaxWdgtSttngUpdate = GetHttpObject();
	ajaxWdgtSttngUpdate.onreadystatechange = function()
	{
		if (ajaxWdgtSttngUpdate.readyState==4 && ajaxWdgtSttngUpdate.status == 200){
			if(ajaxWdgtSttngUpdate != false){ modifyWdgtSttngForm(ajaxWdgtSttngUpdate, "update"); } else { alertDialog(editFail); }
		} else if(ajaxWdgtSttngUpdate.readyState==4){
			alertDialog(editFail);
		}
	};
	ajaxWdgtSttngUpdate.open("POST","/secure/dashboard/updateWidgetSettings.html",true);
	ajaxWdgtSttngUpdate.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxWdgtSttngUpdate.setRequestHeader("Content-length", params.length);
	ajaxWdgtSttngUpdate.setRequestHeader("Connection", "close");
	ajaxWdgtSttngUpdate.send(params);
}

//--------------------------------------------------------------------------------------

function formErrorObjects(id, message)
{
	this.id=id;
	this.message=message;
}

var	formErrors = new Array();
	formErrors["ws101"] = new formErrorObjects("#widgetName", widgetSettingNameMissingMsg);
	formErrors["ws102"] = new formErrorObjects("#widgetName", widgetSettingNameCharacterMsg);
	formErrors["ws103"] = new formErrorObjects("#widgetName", widgetSettingNameLengthMsg);
	formErrors["ws201"] = new formErrorObjects("#filterLength", widgetSettingsFilterLngtMsg);
	formErrors["ws301"] = new formErrorObjects("#rangeMenu", widgetSettingsRangeMsg);
	formErrors["ws401"] = new formErrorObjects("#tier1Ops", widgetSettingsTier1Options);
	formErrors["ws501"] = new formErrorObjects("#tier2Ops", widgetSettingsTier2Options);
	formErrors["ws601"] = new formErrorObjects("#tier3Ops", widgetSettingsTier3Options);
	formErrors["ws701"] = new formErrorObjects("#targetValueDisplay", widgetSettingsTargetMissingMsg);
	formErrors["ws702"] = new formErrorObjects("#targetValueDisplay", widgetSettingsTargetCharacterMsg);
	formErrors["ws801"] = new formErrorObjects("#chartDesign", widgetSettingsDisplayMissingMsg);

function wdgtSettingValidate()
{
	var returnStatus = true;
	var alertMsg = '';
	var alertCode = new Array();
//Widget Name	
	if($("#widgetName").val() == ''){returnStatus = false; alertCode.push("ws101");} 
	else if(characterTypes.spclCharacter.test($("#widgetName").val()) == true){returnStatus = false; alertCode.push("ws102");}
	else if($("#widgetName").val().length > 30){returnStatus = false; alertCode.push("ws103");}
//Filter field
	if($("#filterArea").is(":visible")){if($("#filterLength").val() == 0){returnStatus = false; alertCode.push("ws201");}}
//Widget Duration
	if($("#rangeMenu").val() == 0){returnStatus = false; alertCode.push("ws301");}
//Tier 1 Options
	if($("#tier1Ops").is(":visible")){if($("#tier1Attr").val() == ''){returnStatus = false; alertCode.push("ws401");}}
//Tier 2 Options
	if($("#tier2Ops").is(":visible")){if($("#tier2Attr").val() == ''){returnStatus = false; alertCode.push("ws501");}}
//Tier 3 Options
	if($("#tier3Ops").is(":visible")){if($("#tier3Attr").val() == ''){returnStatus = false; alertCode.push("ws601");}}
//Target
	if($("#target").is(":visible") && ($("#targetValueHidden").val() != "true" && $("#targetValueHidden").val() != "false"))
	{	if($("#targetValueHidden").val() == ''){returnStatus = false; alertCode.push("ws701");}
		else if(characterTypes.nonDblNumeric.test($("#targetValueHidden").val()) == true){returnStatus = false; alertCode.push("ws702");}	}
//Display
	if($("#chartType").val() == ''){returnStatus = false; alertCode.push("ws801");}
	
	
	for(var x = 0; x < alertCode.length; x++)
	{	var testStr = /[tier]\d[Ops]/; 
		if(testStr.test(formErrors[alertCode[x]].id)){ $(formErrors[alertCode[x]].id).children("ul").addClass("error") }
		else { $(formErrors[alertCode[x]].id).addClass("error"); }
		alertMsg += "<li>" + formErrors[alertCode[x]].message + "</li>";
	}
		
	if(alertCode.length > 0)
	{
		var cancelButton = {};
		cancelButton.btn1 = {
			text : "Okay",
			click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); resetSessionActvty(); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+ widgetSettingErrorMsg +" <ul>"+alertMsg+"</ul></article><a href='#' class='close' title='Close'>&nbsp;</a></section>").dialog(alertNoticeOptions).on( "dialogclose", function(event) {$("input.error").first().trigger('focus');} );
	}
	
	return returnStatus;
}

//--------------------------------------------------------------------------------------

var timerID = null;

//--------------------------------------------------------------------------------------
	
function moveNavLeft(speed) // moves nav left
{
	var objPosition = $("#dashboardNavArea > nav").position();
	if(objPosition.left < 0) 
	{	
		if((objPosition.left + speed) > 0) 
		{
			$("#dashboardNavArea > nav").css("left", "0px");
			$(".moveLeft").removeClass("active");
		} else {
			var tempPosition = objPosition.left + speed;
			$("#dashboardNavArea > nav").css("left", tempPosition+"px");
			$(".moveRight").addClass("active");
		}
		speed = speed+1;
		timerID = setTimeout("moveNavLeft("+ speed +")", 25);
	} else {
		moveStop();
		$(".moveLeft").removeClass("active");
	}
}

//--------------------------------------------------------------------------------------

function moveNavRight(speed) // moves nav right
{	
	var objPosition = $("#dashboardNavArea > nav").position();
	if(objPosition.left > lastTabOffsetPosition) 
	{
		if((objPosition.left - speed) < lastTabOffsetPosition) 
		{
			$("#dashboardNavArea > nav").css("left", lastTabOffsetPosition+"px");
			$(".moveRight").removeClass("active");
		} else {
			var tempPosition = objPosition.left - speed;
			$("#dashboardNavArea > nav").css("left", tempPosition+"px");
			$(".moveLeft").addClass("active");
		}
		speed = speed+1;
		timerID = setTimeout("moveNavRight("+ speed +")", 25);
	} else {
		moveStop();
		$(".moveRight").removeClass("active");
	}
}

//--------------------------------------------------------------------------------------

function moveStop()  // Nav Stop Scrolling
{
	if (timerID != null) 
	{
		clearTimeout(timerID);
		timerID = null;
	}
}

//--------------------------------------------------------------------------------------

function cancelDashBoard(action){
	var ajaxCancel = GetHttpObject();
	ajaxCancel.onreadystatechange = function() { if (ajaxCancel.readyState==4){
		if(action === "reload"){
			var curTabID = $(".current > .tab").attr("id").replace("tab","").replace("-link", "");
			location.href = location.pathname+"?itemID="+curTabID;}
		else{ action(); }}};
	ajaxCancel.open("GET","/secure/dashboard/cancelDashboard.html?"+document.location.search.substring(1),true);
	ajaxCancel.send(); }
					
//--------------------------------------------------------------------------------------

function setEditMode(){
	var ajaxEditMode = GetHttpObject();
		ajaxEditMode.onreadystatechange = function(){
			if (ajaxEditMode.readyState==4 && ajaxEditMode.status == 200){
				if(ajaxEditMode.responseText == "true"){
					editStatus = true;
					$("a.edit").fadeOut(200, function(){ $("a.save, a.cancel, a.resetDashboard").fadeIn(200); });
					$("#mainContent").addClass("edit");
					$("#content").css("background-color", "#f8f8f9");
					$(".tabDelete").show();
					$(".widget").children("header").children(".menu").show();
					editSort(false);
					editSortTabs(false);
					dashboardNav();
					enableSectionMove(); } 
				else { alertDialog(editModeFail); } } 
			else if(ajaxEditMode.readyState==4){
				alertDialog(editModeFail); } };
		ajaxEditMode.open("GET","/secure/dashboard/editDashboard.html?"+document.location.search.substring(1),true);
		ajaxEditMode.send(); }

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popDashAction(tempItemID, tempEvent){
	if((editStatus && tempEvent === "edit") || (!editStatus && tempEvent === "display")){ loadTab(tempItemID); }
	else if(tempEvent === "display" && editStatus){ //moving out of edit
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+alertEditMode+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ 
				buttons: [{
					text : saveChangesMsg,
					click : function() { 
							saveDashboard();
							$(this).scrollTop(0); $(this).dialog( "close" ); }
				},{
					text : cancelChangesBtn,
					click : function() {
							$(this).scrollTop(0); $(this).dialog( "close" );
							cancelDashBoard("reload"); }
				},{
					text : returnEditMsg,
					click : function() { $(this).scrollTop(0); $(this).dialog( "close" );
							var curTabID = $(".current > .tab").attr("id").replace("tab","").replace("-link", "");
							stateObj = { itemObject: curTabID, action: "display" };
							crntPageAction = "display";
							history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject);
							loadTab(curTabID); }
				}]
			});
			btnStatus(saveChangesMsg); }
	else if(tempEvent === "edit" && !editStatus){ loadTab(tempItemID); setEditMode(); }
	else { loadTab(dfltTabID); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function dashboard(tabID, pgActn) // Main Function for the Dashboard to be called on Page load
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popDashAction(tempItemID, tempEvent); }

if(!popStateEvent && (tabID !== undefined && tabID !== '')){ popDashAction(tabID, pgActn); }
else{ loadTab(dfltTabID); }
///////////////////////////////////////////////////
		
	//--------------------------------------------------------------------------------------

	$("#dashboardPage").on("click", "nav#main a, header#titleBar a, footer a, a.pin, a.psDetailBtn", function(event){ // tracks if a click is made outside of the dashboard when in edit mode.
		if($("#mainContent").hasClass("edit") && $(this).is(".save, .cancel, .edit, .resetDashboard") === false)
		{
			var linkObject = $(this);
			event.preventDefault();

			if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
			$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+alertEditMode+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ 
				buttons: [{
					text : saveChangesMsg,
					click : function() {
							saveDashboard(linkObject.attr("href"));
							$(this).scrollTop(0); $(this).dialog( "close" );
						   }
				},{
					text : cancelChangesBtn,
					click : function() {
							$(this).scrollTop(0); $(this).dialog( "close" );
							if(linkObject.attr("target") && linkObject.attr("target") === "_blank")
							{ cancelDashBoard("reload"); window.open(linkObject.attr("href")); }
							else{ var loadLink  = function(){ window.location = linkObject.attr("href"); }; cancelDashBoard(loadLink); }}
				},{
					text : returnEditMsg,
					click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
				}]
			});
			btnStatus(saveChangesMsg);
		}
	});
	
	$("#dashboardPage").on("click", "#custSwitchBtn", function(event){ // tracks if a click is made outside of the dashboard when in edit mode.
		if($("#mainContent").hasClass("edit") && $(this).is(".save, .cancel, .edit, .resetDashboard") == false)
		{
			var linkObject = $(this);
			event.preventDefault();
			pausePropagation = true;

			if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
			$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+alertEditMode+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ 
				buttons: [{
					text : saveChangesMsg,
					click : function() {
							pausePropagation = false; $("#mainContent").removeClass("edit");
							saveDashboard('',linkObject); $(this).scrollTop(0); $(this).dialog("close"); }
				},{
					text : cancelChangesBtn,
					click : function() {
							pausePropagation = false; $("#mainContent").removeClass("edit"); $(this).scrollTop(0); 
							var objectClick  = function(){ linkObject.trigger('click'); }; cancelDashBoard(objectClick); $(this).dialog("close"); }
				},{
					text : returnEditMsg,
					click : function() { $(this).scrollTop(0); $(this).dialog("close"); }
				}]
			});
			btnStatus(saveChangesMsg);
		}
	});
	
	//--------------------------------------------------------------------------------------
	//  MAIN BUTTONS
	//--------------------------------------------------------------------------------------

	$(".edit").on('click', function(event){ // sets into Edit mode
		event.preventDefault();
		tabID = $(".current > .tab").attr("id").replace("tab","").replace("-link", "");
		stateObj = { itemObject: tabID, action: "display" };
		crntPageAction = "display";
		history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject);
		setEditMode(); });
	
	$(".save").on('click', function(event){ // Save Changes
		event.preventDefault();
		if($("#mainContent.edit").find(".error").length){ alertDialog(outstandingDashBoardErrorMsg, "", "", $(".error").first()) }
		else{ saveDashboard(); }
	});
	
	$("a.cancel").on('click', function(event){ // Cancel Changes
		event.preventDefault();
		var cancelButtons = {};
		cancelButtons.btn1 = {
			text : cancelNowMsg,
			click : function() { cancelDashBoard("reload"); }
		};
		cancelButtons.btn2 = {
			text : goBackMsg,
			click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); resetSessionActvty(); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+cancelDashBrdConfirmMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: cancelButtons });
		btnStatus(cancelNowMsg);
	});
	
	$("a.resetDashboard").on('click', function(event){ // Reset Dashboard to default
		event.preventDefault();
		var cancelButtons = {};
		cancelButtons.btn1 = {
			text : resetNowBtnMsg,
			click : function() { 
					var ajaxCancel = GetHttpObject();
					ajaxCancel.onreadystatechange = function() { 
						if(ajaxCancel.readyState==4){ if(ajaxCancel.responseText === "true"){location.reload(true);} else{ alertDialog(systemErrorMsg); }} };
					ajaxCancel.open("GET","/secure/dashboard/resetDashboard.html?"+document.location.search.substring(1),true);
					ajaxCancel.send();
				}
		};
		cancelButtons.btn2 = {
			text : goBackMsg,
			click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); resetSessionActvty(); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+resetDashBrdConfirmMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: cancelButtons });
		btnStatus(resetNowBtnMsg);
	});


	//--------------------------------------------------------------------------------------
	//  TAB FEATRUES
	//--------------------------------------------------------------------------------------

	$("#dashboardPage").on("click", ".tab", function(event){ event.preventDefault(); // switches tabs and dashboard
		if($(this).parent(".tabSection").hasClass("current") != true)
		{
			if($("#mainContent.edit").length && $(".edit #dashboardNav #dashboardNavArea .current .tabInput").is(":visible")){ 
				$(".edit #dashboardNav #dashboardNavArea .current .tabInput").trigger('blur'); }
			if(blurSent === true) {
				tabID = $(this).attr("id").replace("tab","").replace("-link", "");
				var pgAction = ($("#mainContent").hasClass("edit"))? "edit" : "display";
				var stateObj = { itemObject: tabID, action: "display" };
				crntPageAction = "display";
				if(!$(this).parent(".tabSection").hasClass("new")){ history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject); }
				loadTab(tabID, $(this));
			}
		}
	});
	
	$("#dashboardPage").on("click", ".edit #dashboardNav #dashboardNavArea .current .tab", function(event){ event.preventDefault(); 
// switch tab to input field for current tab in edit mode
		var tabWidth = $(this).outerWidth(true)+2+"px";
		$(this).siblings(".tabInput").css("width", tabWidth);
		$(this).hide();
		$(this).siblings(".tabInput").show();
		$(this).siblings(".tabInput").trigger('focus').select();
		dashboardNav();
		enableSectionMove();
	});

	var blurSent = true;	
	
	setupCharBlocker(".tabInput", characterTypes.stndrdChrctr, function() { return stndrdChrctrsOnlyMsg; });
	
	$("#dashboardPage").on("keypress", ".edit #dashboardNav #dashboardNavArea .current .tabInput", function(e) // controls for the tab when the text is being edited
	{
		var curObj = $(this);
/*		if(e.charCode && characterTypes.spclCharacter.test(String.fromCharCode(e.charCode)) == true)
		{
			e.preventDefault();
			alertDialog(tabAlpaNumericMsg);
		} else {
*/			blurSent = false;
			if(e.keyCode == 13 || e.keyCode == 9) // enter blurs to cause submission of text
			{ $(this).trigger('blur'); }
			else if(e.keyCode == 8 || e.keyCode == 46) // backspace or delete
			{
				var thisLength = $(this).val().length;
				pastLength =(pastLength < thisLength)? thisLength : pastLength;
				if(thisLength >= 1 && thisLength < pastLength)
				{
					var inputWidth = $(this).innerWidth()-6+"px";
					$(this).css("width", inputWidth);
					dashboardNav();
					pastLength = thisLength;
				}
			}
			else if(e.keyCode >= 37 && e.keyCode <= 40) // arrow keys no reaction needed.
			{ }
			else {
				if($(this).val().length < 20)
				{
					var thisWidth = ($(this).val().length+1)*8;
					var inputWidth = $(this).innerWidth();
					if(thisWidth >= inputWidth-8)
					{
						var inputWidth = $(this).innerWidth()+6+"px";
						$(this).css("width", inputWidth);
						dashboardNav();
					}
				}
			}
		//}
	}).on("focus", ".edit #dashboardNav #dashboardNavArea .current .tabInput",	function(){
		$(this).siblings(".tabDelete").hide().parent(".tabSection").css("padding-right", "29px");
	}).on("blur", ".edit #dashboardNav #dashboardNavArea .current .tabInput", function(event){ 
		tabInputObj = $(this);
		tabInputVal = $(this).val();
		tabLink = $(this).siblings(".tab");
		tabLinkText = $(this).siblings(".tab").html();
		valLength = tabInputVal.length;
		
		if(!$("#messageResponseAlertBox").is(":visible")){
			if(characterTypes.stndrdChrctr.test(tabInputVal) !== true) {
				if(valLength == 0 && tabInputObj.parent(".tabSection").hasClass("new")){
					tabInputObj.val(untitledTitle);
					tabLink.html(untitledTitle);
					editTab(); } 
				else if(tabInputVal != tabLinkText) {
					editTab(); } 
				else if(tabInputVal == tabLinkText) {
					tabInputObj.hide();
					tabLink.show();
					dashboardNav(); }
				blurSent = true;
				tabInputObj.removeClass("warning");
				tabInputObj.siblings(".tabDelete").show().parent(".tabSection").css("padding-right", "34px");
				dashboardNav(); }
			else { tabInputObj.trigger('focus'); tabInputObj.addClass("warning"); alertDialog(stndrdChrctrsOnlyMsg); blurSent = false; } }
	});
	
	$("#dashboardPage").on("click", ".tabDelete", function(event)// Delete Tab in Edit mode
	{
		event.preventDefault();
		
		
		var dltTab = $(this).parent(".tabSection");
		var dltTabDashbrd = $(this).siblings(".tab").attr("href");
		var dltTabID = dltTab.attr("id").replace("tabID_", "");
		
		var tabDeleteButtons = {};
		tabDeleteButtons.btn1 = {
			text : 	deleteTabMsg,
			click : 	function() {
						$(this).scrollTop(0); $(this).dialog( "close" );
						var ajaxTabDlt = GetHttpObject();
						ajaxTabDlt.onreadystatechange = function()
						{	
							if (ajaxTabDlt.readyState==4 && ajaxTabDlt.status==200)
							{
								if(ajaxTabDlt.responseText == "true")
								{
									if($(dltTab).hasClass("current"))
									{
										$(dltTab).next(".tabSection").addClass("current");
										if(!$(dltTab).siblings(".tabSection").hasClass("current"))
										{
											if($(dltTab).siblings(".tabSection").length > 0)
											{
												$(dltTab).prev(".tabSection").addClass("current");
											} else {
												$(dltTab).siblings(".tabSection").eq(0).addClass("current");
											}
										}
										$(dltTab).remove();
										$(dltTabDashbrd).remove();
										if($(".tabSection").length == 0)
										{
											getNewTabID("blur");
										} else {
											loadTab($(".tabSection.current").attr("id").replace("tabID_", ""));
										}
									} else {
										$(dltTab).remove();
										$(dltTabDashbrd).remove();
									}
									dashboardNav();
								}
							} else if(ajaxTabDlt.readyState==4)
							{
								alertDialog(editFail);
							}
						};
						ajaxTabDlt.open("Get","/secure/dashboard/deleteTab.html?id="+dltTabID+"&"+document.location.search.substring(1),true);
						ajaxTabDlt.send();
					}
		};
		tabDeleteButtons.btn2 = {
			text : cancelMsg,
			click : 	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+deleteTabMsg+"</strong></h4><article>"+tabDeleteMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: tabDeleteButtons });
		btnStatus(deleteTabMsg);
	});
	
	var clickDelay = 200, clickCount = 0, clickTimer = null; // global variables set to block errors on rapid clicking on add tab

	$("#dashboardPage").on("click", ".addTab", function(event) // Add Tab, Dashboard and Section
		{
			event.preventDefault();
			clickCount++;  //count clicks
			if(clickCount == 1) { 
				clickTimer = setTimeout(function() {
					getNewTabID();
					clickCount = 0;
				}, clickDelay);
			} else{
				clearTimeout(clickTimer);    //prevent single-click action
				clickCount = 0;
			}
		}
	);

	$("#dashboardNav > .moveLeft").on('mousedown', function(){ // Action to move nav Left
		if($(this).hasClass("active")){ moveNavLeft(3); }
	}).on('mouseup',function(){
		moveStop();
	}).on('click', function(event){event.preventDefault();});
	
	$("#dashboardNav > .moveRight").on('mousedown', function(){ // Action to move nav Left
		if($(this).hasClass("active")){ moveNavRight(3); }
	}).on('mouseup',function(){
		moveStop();
	}).on('click', function(event){event.preventDefault();});
	
	//--------------------------------------------------------------------------------------
	//  SECTIONS
	//--------------------------------------------------------------------------------------
	
	$("#dashboardPage").on("click", ".addSection", function(event) // Adds a section to the dashboard
	{
		event.preventDefault();
		
		
		var tabID = $(this).parents(".dashboardTab").attr("id");
		tabID = tabID.replace("tab","");
		var parentObject = $(this).parent();
		
		var ajaxSection = GetHttpObject();
		ajaxSection.onreadystatechange = function()
		{	
			if(ajaxSection.readyState==4 && ajaxSection.status == 200){
				parentObject.before(ajaxSection.responseText);
				editSort(false);
				enableSectionMove();
			} else if(ajaxSection.readyState==4)
			{
				alertDialog(editFail);
			}
		};
		ajaxSection.open("GET","/secure/dashboard/addSection.html?id="+tabID+"&"+document.location.search.substring(1),true);
		ajaxSection.send();
	});
	
	$("#dashboardPage").on("click", ".colOptn", function(event) // switches Column layout design for the sections
	{
		event.preventDefault();
		layoutStylLink = $(this);
		newLayout = $(this).attr("name");
		newLayoutID = newLayout.replace("layout", "")
		curLayout = $(this).parent().children(".on").attr("name");
		layoutPrntSection = $(this).parents(".layout");
		curTabID = $(this).parents(".dashboardTab").attr("id");
		
		if((newLayout == "layout1" && newLayout != curLayout) || (curLayout == "layout3" && curLayout != newLayout))
		{
			var containsWdgt = null;
			if(curLayout == "layout3"){layoutPrntSection.children(".third").children(".widgetObject").each(function(index){containsWdgt = index;});}
			if (newLayout == "layout1" && containsWdgt == null){layoutPrntSection.children(".second").children(".widgetObject").each(function(index){containsWdgt = index;});}
			if(containsWdgt != null)
			{
				layoutPrntSctnContent = layoutPrntSection.html();
				
				if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
				$("#messageResponseAlertBox")
				.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+columnMerger+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ 
					buttons: [{
						text : mergeWidgetsMsg,
						click : function() {
								$(this).scrollTop(0); $(this).dialog( "close" );
								layoutPrntSection.children(".third").children(".widgetObject").each(function() {
									layoutPrntSection.children(".second").append($(this));
									layoutPrntSection.children(".third").children($(this)).remove();
									layoutPrntSection.children(".third").append(columnBox);
								});
								if(newLayout == "layout1")
								{
									layoutPrntSection.children(".second").children(".widgetObject").each(function() {
										layoutPrntSection.children(".first").append($(this));
										layoutPrntSection.children(".second").children($(this)).remove();
										layoutPrntSection.children(".second").append(columnBox);
									});
								}
								
								var tabQuerry = "tabID=" + curTabID.replace("tab", "");
								
								var sectionQuerry = null;
								$("#"+curTabID).children(".layout").each(function(){
									var sectionID = $(this).attr("id").split("_");
									if(sectionQuerry == null){sectionQuerry = sectionID[0]+"[]="+sectionID[1];}
									else{sectionQuerry = sectionQuerry+"&"+sectionID[0]+"[]="+sectionID[1];}
								});
								
								var widgetQuerry = null;
								$(".column:visible").has(".widgetObject").each(function(){
									if(widgetQuerry == null){widgetQuerry = $(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
									else{widgetQuerry = widgetQuerry+"&"+$(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
								});
								
								var fullQuerry = tabQuerry  + "&" + sectionQuerry  + "&" + widgetQuerry;
								
								widgetOrder(fullQuerry, true);
							   }
					},{
						text : deleteWidgetsMsg,
						click : function() {
								$(this).scrollTop(0); $(this).dialog( "close" );
								if(newLayout == "layout1"){ clearColumn(2); }
								else { clearColumn(1); }
							   }
					},{
						text : cancelMsg,
						click : function(){ $(this).scrollTop(0); $(this).dialog( "close" ); }
					}]
				});
				btnStatus(mergeWidgetsMsg, deleteWidgetsMsg);
				
			} else { layoutChng(containsWdgt); }
		} else { layoutChng(containsWdgt);}
	});
	
	$("#dashboardPage").on("click", ".deleteSection", function(event) // deletes sections from a dashboard
	{
		event.preventDefault();
		var parentSection = $(this).parents(".layout");
		var parentSectionID = parentSection.attr("id").replace("section_", "");
		curTabID = $(this).parents(".dashboardTab").attr("id");
		if(parentSection.find(".widgetObject").length > 0)
		{
			var deleteSectionButtons = {};
			deleteSectionButtons.btn1 = {
				text : deleteSectionBtn,
				click : 	function() { deleteSection(parentSectionID, parentSection); $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
			deleteSectionButtons.btn2 = {
				text : cancelMsg,
				click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
			
			if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
			$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+deleteSectionBtn+"</strong></h4><article>"+deleteSectionMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteSectionButtons });
			btnStatus(deleteSectionBtn);
		} else {
			deleteSection(parentSectionID, parentSection);
		}
		enableSectionMove();
	});
	
	$("#dashboardPage").on("click", ".moveUp", function(event) // action to move a section up
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){ sectionOrder($(this), "up"); }
	});
	
	$("#dashboardPage").on("click", ".moveDown", function(event) // action to move a section down
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){ sectionOrder($(this), "down"); }
	});

	$("#dashboardPage").on("click", ".addWidget", function(event) // action to open the Widget List dialog box
	{
		event.preventDefault();
		var	layoutSection = $(this).parents('.layout'),
			placementLoc = $(this).parents('.addWidgetHolder');
		curSectionID = layoutSection.attr("id").replace("section_", "");
		curTabID = layoutSection.parent(".dashboardTab").attr("id").replace("tab", "");
		
		if($("#widgetList").html() == "") { loadWidgetList(layoutSection, curSectionID, curTabID, placementLoc); } //line: *472*
		else { showWidgetList(layoutSection, curSectionID, curTabID, placementLoc); } //line: 
	});	
	
	$("#widgetList").on("click", "li", function(){ 
		$("#theWdgtSelected").val($(this).attr("id"));
		$("#widgetList").find("li.ui-selected").removeClass("ui-selected");
		$(this).addClass("ui-selected");
		if($("#theWdgtSelected").val() != '') {
			$(".ui-dialog-buttonset").children().each(function(){ if($(this).html().match(addWidgetMsg) != null){ $(this).addClass('active'); } }); }
		else {
			$(".ui-dialog-buttonset").children().each(function(){ if($(this).html().match(addWidgetMsg) != null) { $(this).removeClass('active'); } }); }
	});
	
	$("#widgetList").on("click", "h3", 
		function(event)
		{
			var widgetListObj = "#"+$(this).attr("id").replace("metric", "metricList");
			$(this).toggleClass("selected");
			$(this).find(".expand").toggleClass("contract");
			$(widgetListObj).slideToggle(function(){checkDialogPlacement($("#widgetList"));});
		}
	).on("click", ".showAllWdgts",
		function(event){
			event.preventDefault();
			if($(this).html().match("Show") != null)
			{
				var newButtonLbl = $(this).html().replace("Show", "Hide");
				$(this).html(newButtonLbl);
				$(this).parents("#widgetList").find("h3").each(function(index, element) {
					if($(this).hasClass("selected") == false)
					{
						$(this).trigger('click');
					}
				});
			} else if($(this).html().match("Hide") != null)
			{	
				var newButtonLbl = $(this).html().replace("Hide", "Show");
				$(this).html(newButtonLbl);
				$(this).parents("#widgetList").find("h3").each(function(index, element) {
					if($(this).hasClass("selected") == true)
					{
						$(this).trigger('click');
					}
				});
			}
		}
	);

	//--------------------------------------------------------------------------------------
	//  WIDGETS
	//--------------------------------------------------------------------------------------

	$("#dashboardPage").on("click", ".sort", function(event) { event.preventDefault(); // Sorting for List Widgets
		Object.size = function(obj) {
		    var size = 0, key;
		    for (key in obj) { if (obj.hasOwnProperty(key)) size++; }
		    return size; };
		
		var	wdgtOBjStrng = ($(this).parents(".widgetContent").attr("id") === "xpndContent")? "xpndWdgt-" : "wdgt-" ;
			sortOptions = $(this).attr("sort").split(":"),
			sortWdgtObj = "wdgt"+$(this).parents("ul").attr("id").replace(wdgtOBjStrng, "").replace("Header", ""),
			$dataList = $("#"+$(this).parents("ul").attr("id").replace("Header", "")),
			metricType = parseInt($(this).parents("ul").attr("metricType")), 
			tooltipMsg = $(this).attr("tooltip").split(":"), 
			colCount = Object.size(wdgtDataArray[sortWdgtObj].listData[0]),
			colWidth =  100/colCount+"%";

		if(!$(this).hasClass("current")){
			$(this).siblings("div").each(function() {
				if($(this).hasClass("current")){ currentCol = $(this); } });
			var	currentColSortMsg = currentCol.attr("sort").split(":"),
				currentColToolTipMsg = currentCol.attr("tooltip").split(":");
			currentCol.removeClass("current")
				.attr("sort", currentColSortMsg[0]+":asc")
				.attr("tooltip", currentColToolTipMsg[0] +" : "+ sortAscendingMsg)
				.find("img[src='"+imgLocalDir+"icn_ArwUp.png']")
				.attr("src", imgLocalDir+"icn_ArwDown.png");	
			$(this).addClass("current"); }
		
		if(sortOptions[1] === "asc"){ 
			if($(".ui-tooltip:visible").length > 0){$(".ui-tooltip:visible").html(tooltipMsg[0] +" : "+ sortDescendingMsg); }
			$(this).attr("sort", sortOptions[0]+":desc")
				.attr("tooltip", tooltipMsg[0] +" : "+ sortDescendingMsg)
				.find("img[src='"+imgLocalDir+"icn_ArwUp.png']")
				.attr("src", imgLocalDir+"icn_ArwDown.png"); } 
		else { 
			if($(".ui-tooltip:visible").length > 0){$(".ui-tooltip:visible").html(tooltipMsg[0] +" : "+ sortAscendingMsg); }
			$(this).attr("sort", sortOptions[0]+":asc")
				.attr("tooltip", tooltipMsg[0] +" : "+ sortAscendingMsg)
				.find("img[src='"+imgLocalDir+"icn_ArwDown.png']")
				.attr("src", imgLocalDir+"icn_ArwUp.png"); }
		var sortedData = wdgtSorting(wdgtDataArray[sortWdgtObj], sortOptions);		
		$dataList.html("");
		for(var x = 0; x < sortedData.length; x++){
			var $listDataItem = $("<li>");
			for(var y = 0; y < colCount; y++){
				var colNum = y+1, objItem = "obj"+y;	
				$listDataItem.append(wdgtListDataPoint(sortedData[x][objItem], colNum, colWidth, metricType, wdgtDataArray[sortWdgtObj].wdgtByTime, wdgtDataArray[sortWdgtObj].wdgtInterval)); }
			$dataList.append($listDataItem) }	});

	$("#dashboardPage").on("click", ".checkboxLabel", function(event){ 
		if($(this).parents("#heatMapOverlay").length){ 
			var wdgtID = $(this).parents(".widgetContent").attr("id").split("_")[1];	
				if($("#showHeat_"+wdgtID+"Hidden").val() === "false"){
					maps["map_"+wdgtID].removeLayer(markerGroups["markers_"+wdgtID]);
					maps["map_"+wdgtID].addLayer(markerGroups["heatmapLayer_"+wdgtID]); } 
				else{ 
					maps["map_"+wdgtID].removeLayer(markerGroups["heatmapLayer_"+wdgtID]);
					maps["map_"+wdgtID].addLayer(markerGroups["markers_"+wdgtID]); } }});

	$("#dashboardPage").on("click", ".svCSV", function(event) { event.preventDefault(); // Save Widget As CSV
		var widgetID = $(this).parents(".ddMenu").attr("id").split("_")[1];
		$.ajax({
			url: "/secure/dashboard/widgetCSV.html?widgetID="+widgetID+"&"+document.location.search.substring(1),
			success: function(data) { window.location = "/secure/dashboard/widgetCSV.html?widgetID="+widgetID+"&"+document.location.search.substring(1); },
			error: function(data){ alertDialog(systemErrorMsg); } }); });

	$("#dashboardPage").on("click", ".print", function(event) { event.preventDefault(); // Print Widget
		var	printGraph = $(this).parents(".widgetObject").find(".widgetContent").hasClass("chartGraph"),
			columnStyle = "dashBoardPrintOnly",
			listGraph = $(this).parents(".widgetObject").find(".widgetContent").hasClass("listGraph");
		if(listGraph){
			var tempWidth = $(this).parents(".widgetObject").find(".scrollingListHeader").outerWidth(true)+22
			$(this).parents(".column").css("width", tempWidth+"px"); }
		if(printGraph){ columnStyle = "dashBoardPrintGraphOnly"; }
		$("body").addClass("dashBoardPrintOnly");
		$(this).parents(".layout").addClass("dashBoardPrintOnly");
		$(this).parents(".column").addClass(columnStyle);
		$(this).parents(".widgetObject").addClass("dashBoardPrintOnly");
		window.print();
		$(this).parents(".widgetObject").removeClass("dashBoardPrintOnly");
		if(listGraph){ $(this).parents(".column").css("width", ''); }
		$(this).parents(".column").removeClass(columnStyle);
		$(this).parents(".layout").removeClass("dashBoardPrintOnly");
		$("body").removeClass("dashBoardPrintOnly"); });


	$("#dashboardPage").on("click", ".deleteWdgt", function(event) { event.preventDefault(); // Delete Widgets
		
		var tabID = $(this).parents(".dashboardTab").attr("id").replace("tab", "");
		var sectionID = $(this).parents(".layout").attr("id").replace("section_", "");
		var colID = $(this).parents(".column").attr("id").replace("sec"+sectionID+"col", "");
		var colObj = $(this).parents(".column");
		var wdgtID = $(this).parents(".widgetObject").attr("id").replace("widgetID_", "");
		var wdgtObj = $(this).parents(".widgetObject");
		var prevAdBtn = wdgtObj.prev();
		while(!prevAdBtn.hasClass("addWidgetHolder")){ prevAdBtn = prevAdBtn.prev(); if(prevAdBtn.length === 0){ break; } }
		
		var deleteWidgetButtons = {};
		deleteWidgetButtons.btn1 = {
			text : deleteWidgetMsg,
			click : 	function() {
				$(this).scrollTop(0); $(this).dialog( "close" );
				var ajaxDltWdgt = GetHttpObject();
				ajaxDltWdgt.onreadystatechange = function()
				{
					if (ajaxDltWdgt.readyState==4 && ajaxDltWdgt.status == 200){
						if(ajaxDltWdgt.responseText == "true")
						{
							snglSlideFadeTransition("hide", wdgtObj, function(){ $(this).remove(); }, "", "slow");
							snglSlideFadeTransition("hide", prevAdBtn, function(){ $(this).remove(); }, "", "slow");
						} else {
							alertDialog(editFail);
						}
					} else if(ajaxDltWdgt.readyState==4){
						alertDialog(editFail);
					}
				};
				ajaxDltWdgt.open("GET","/secure/dashboard/deleteWidget.html?tabID="+tabID+"&sectionID="+sectionID+"&colID="+colID+"&widgetID="+wdgtID+"&"+document.location.search.substring(1),true);
				ajaxDltWdgt.send();

			}
		};
		deleteWidgetButtons.btn2 = {
			text : cancelMsg,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+deleteWidgetMsg+"</strong></h4><article>"+deleteWdgtMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteWidgetButtons });
			btnStatus(deleteWidgetMsg);
	});
	
	//--------------------------------------------------------------------------------------
	//  WIDGETS SETTINGS
	//--------------------------------------------------------------------------------------

	$("#dashboardPage").on("click", ".settings", function(event) // brings up widget settings
	{	
		event.preventDefault();
		
		var menu = $(this).parents('.ddMenu');
		var wdgtID = $(this).parents('.widgetObject').attr("id").replace("widgetID_", "");
		var wdgtObjectID = $(this).parents('.widgetObject').attr("id");
		var wdgtFormSettingsID = "#wdgtSettings";
		menu.toggle("fast");
		$("#opBtn_"+wdgtID).toggleClass('on');
		
		var widgetSettingsButtons = {};
		widgetSettingsButtons.btn1 = {
			text :	 applyChangesMsg,
			click : 	function(event, ui) {
						var widgetFormContent = $(wdgtFormSettingsID).serialize();
						var params = widgetFormContent;
						var ajaxWdgtSttngApply = GetHttpObject();
						var dialogObj = $(this);
						ajaxWdgtSttngApply.onreadystatechange = function()
						{
							if (ajaxWdgtSttngApply.readyState==4 && ajaxWdgtSttngApply.status == 200){
								if(ajaxWdgtSttngApply.responseText == "true"){ dialogObj.dialog("close"); loadChart(wdgtID); }
							} else if(ajaxWdgtSttngApply.readyState==4){
								alertDialog(editFail);
							}
						};
						ajaxWdgtSttngApply.open("POST","/secure/dashboard/applyWidgetSettings.html",true);
						ajaxWdgtSttngApply.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
						ajaxWdgtSttngApply.setRequestHeader("Content-length", params.length);
						ajaxWdgtSttngApply.setRequestHeader("Connection", "close");
						ajaxWdgtSttngApply.send(params);
					}
		};
		widgetSettingsButtons.btn2 = {
			text : cancelMsg,
			click :	function(event) { event.preventDefault(); cancelConfirm($(this), null, function() {}); }
		};
		var wdgtSettings = 	{ 
							title: widgetSettingsMsg,
							modal: true,
							classes: { "ui-dialog": "dialogSettings" },
							resizable: false,
							closeOnEscape: false,
							position: {my: "center center-100"},
							width: 600,
							height: "auto",
							maxHeight:  $(window).innerHeight()-150,
							buttons:	widgetSettingsButtons,
							dragStop: function(){ checkDialogPlacement($(this)); }
						};		
		var ajaxWdgtSetting = GetHttpObject();
		ajaxWdgtSetting.onreadystatechange = function()
		{
			if (ajaxWdgtSetting.readyState==4 && ajaxWdgtSetting.status == 200){
				$("#wdgtSettingDialog").html(ajaxWdgtSetting.responseText);
				getWidgetSettingDetails(wdgtID, wdgtSettings);
			} else if(ajaxWdgtSetting.readyState==4){
				alertDialog(editFail);
			}
		};
		ajaxWdgtSetting.open("GET","/secure/dashboard/showWidgetSettings.html?widgetID="+wdgtID+"&"+document.location.search.substring(1),true);
		ajaxWdgtSetting.send();
	});
	
	$("#dashboardPage").on("click", ".tierCheck", function(event) // SHOWS/HIDES TIER OPTIONS 
	{
		event.preventDefault();
		var parentID = $(this).parent(".switch").attr("id");		
		var tierOpts = $("#" + parentID.replace("Switch", "Area"));
		var tierMenu = $("#" + parentID.replace("Switch", "Menu"));
		var tierAttr = $("#" + parentID.replace("Switch", "Attr"));
		var checkBoxObj = $(this).children(".checkBox");
		var checkboxField = $("#"+checkBoxObj.attr("id")+"Hidden");
		if(checkBoxObj.hasClass("checked") == true){
			tierOpts.hide();
			tierMenu.autoselector("reset", true);
			tierAttr.val("");
			tierOpts.find(".tierOps").hide();
			tierOpts.nextUntil("#targetSwitch").each(function(index){
				if($(this).is(":visible")){
					$(this).hide();
					if($(this).hasClass("tierSwitch")){
						$(this).find(".checkBox").removeClass("checked");
						$(this).find("input").val("false"); }
					if($(this).hasClass("tierArea")){ 
						$("#"+$(this).attr("id").replace("Area", "Menu")).autoselector("reset", true);
						$("#"+$(this).attr("id").replace("Area", "Attr")).val("");
						$(this).find(".tierOps").hide();} } });
			updateWidgetSettings(tierMenu.attr("id"));
		} else {tierOpts.fadeIn();}
	});
	
	$("#dashboardPage").on("click", ".tierOps .fullListHeader .checkboxLabel", function(event) // SELECT ALL FEATURE, SELECTS OR DESELECTS ALL IN FULL LIST 
	{
		event.preventDefault();
		event.stopImmediatePropagation();
		var parentTier = $(this).parents(".tierOps");
		var checkBoxObj = $(this).children(".checkBox");
		var inputObj = parentTier.children("input");
		if(!$(this).hasClass("disabled")){
			checkBoxObj.toggleClass("checked");
			
			if(checkBoxObj.hasClass("checked") == true)
			{
				parentTier.find("ul.fullList > li").each(function(index)
				{ 
					var currObjID = $(this).attr("id"), currID = currObjID.split("-");
					if(inputObj.val() == ""){ inputObj.val(currID[1]); }else{ if(inputObj.val().search(currID[1]) < 0){ inputObj.val(inputObj.val()+","+currID[1]).trigger('change'); } }
					$(this).addClass("selected"); 
					parentTier.children("input").val(parentTier.children("input").val()+""); 
				});
				parentTier.find("ul").removeClass("error");			
				parentTier.find("ul.selectedList > li").each(function(index){ $(this).fadeIn("slow").addClass("selected"); });
				if($("#tier1Ops ul.selectedList li.selected").length === 1){ $(".lbl1034, .lbl8395, .lbl6783").addClass("disabled"); 
					if($(".lbl1034").attr("id") == $("#chartType").val()){$("#chartType").val('')}
					if($(".lbl8395").attr("id") == $("#chartType").val()){$("#chartType").val('')}
					if($(".lbl6783").attr("id") == $("#chartType").val()){$("#chartType").val('')}
				} 
				else if($("#tier1Ops ul.selectedList li.selected").length != 1 && !$("#tier3Check").hasClass("checked")) {
					var $currChartType;
					if($("#tier2Check").hasClass("checked")){
						$currChartType = $(".lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
					} 
					else {
						$currChartType = $(".lbl1034, .lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
					}
					
					if($currChartType.length > 0) {
						$("#chartType").val($currChartType.attr("id"));
					}
				}
	
			} else {
				parentTier.find("ul.fullList > li").each(function(index){ $(this).removeClass("selected"); });
				parentTier.find("ul.selectedList > li").each(function(index){ $(this).fadeOut("fast").removeClass("selected"); });
				inputObj.val("").trigger('change');
				if($("#tier1Ops ul.selectedList li.selected").length != 1 && !$("#tier3Check").hasClass("checked")) {
					var $currChartType;
					if($("#tier2Check").hasClass("checked")) {
						$currChartType = $(".lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
					}
					else {
						$currChartType = $(".lbl1034, .lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
					}
					
					if($currChartType.length > 0) {
						$("#chartType").val($currChartType.attr("id"));
					}
				}
			}
			if($("#tier1Ops ul.selectedList.orgList li.selected").length == 1 && !$("#tier2Switch").hasClass("noTierOptions")){ $("#tier2Switch").slideDown(); } 
			else if($("#tier1Ops ul.orgList").length){if($("#tier2Switch").find("input").val() === "true"){$("#tier2Switch").find(".tierCheck").trigger('click');} $("#tier2Switch").slideUp(); }
			
			if(($("#tier2Ops ul.selectedList li.selected").length == 1 && $("#tier2Ops").is(":visible") ) && !$("#tier3Switch").hasClass("noTierOptions")){ $("#tier3Switch").slideDown(); } 
			else { if($("#tier3Switch").find("input").val() === "true"){ $("#tier3Switch").find(".tierCheck").trigger('click'); }  $("#tier3Switch").slideUp(); }
		}});
	
	$("#dashboardPage").on("click", ".tierOps ul.fullList  li, .tierOps ul.selectedList li", function(event) // LIST ITEM SELECTION FEATURE
	{
		event.preventDefault();
		if(!$(this).hasClass("notclickable")){
			var	$that = $(this),
				fullList = /full/i,
				selectedList = /select/i,
				currObjID = $(this).attr("id"),
				currID = currObjID.split("-"),
				parentList = $(this).parent("ul"),
				inputObj = $(this).parents(".tierOps").children("input"),
				levelObj = false,
				parentCnt = 0,
				$parentItem = "";
			
			if($(this).hasClass("level2")){levelObj = "level2";}else if($(this).hasClass("level3")){levelObj = "level3";}
				
			if(parentList.hasClass("fullList") && !$(this).hasClass("selected")){
				if((parentList.hasClass("snglValList") && inputObj.val() == "") || !parentList.hasClass("snglValList")){
					var optionsAvailble = false;
					if(inputObj.val() == ""){ inputObj.val(currID[1]).trigger('change'); }else{ inputObj.val(inputObj.val()+","+currID[1]).trigger('change'); }
					var newObjID = currObjID.replace(fullList, "#select");
					$("#"+currObjID).addClass("selected");
					parentList.children("li").each(function(index){ if($(this).hasClass("selected") == false && optionsAvailble == false){ optionsAvailble = true; } });
					if(optionsAvailble == false){ parentList.siblings(".fullListHeader").find(".checkBox").addClass("checked") }
					
					if(levelObj != false){
						if(levelObj == "level2"){ 
							$that.nextUntil(".level2").each(function(index){ $(this).addClass("selected"); }); }
						else if(levelObj == "level3"){
							$that.prevAll(".level2").each(function(index){ if(parentCnt < 1){$(this).addClass("inactive notclickable");} parentCnt++; }); }}
						
					$(newObjID).addClass("selected").fadeIn("slow");
					$(newObjID).parent("ul").removeClass("error");
					parentList.removeClass("error");
					if($("#tier1Ops ul.selectedList li.selected").length == 1){ $(".lbl1034, .lbl8395, .lbl6783").addClass("disabled"); 
						if($(".lbl1034").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}
						if($(".lbl8395").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}
						if($(".lbl6783").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}}
					else if($("#tier1Ops ul.selectedList li.selected").length != 1 && !$("#tier3Check").hasClass("checked")) {
						var $currChartType;
						if($("#tier2Check").hasClass("checked")){
							$currChartType = $(".lbl8395, .lbl6783").removeClass("disabled").filter(".selected"); }
						else{
							$currChartType = $(".lbl1034, .lbl8395, .lbl6783").removeClass("disabled").filter(".selected"); }
						
						if($currChartType.length > 0) {
							$("#chartType").val($currChartType.attr("id")).trigger('change'); }}
								
					if($("#tier1Ops ul.selectedList.orgList li.selected").length == 1 && !$("#tier2Switch").hasClass("noTierOptions")){ $("#tier2Switch").slideDown(); } 
					else if($("#tier1Ops ul.orgList").length){if($("#tier2Switch").find("input").val() === "true"){$("#tier2Switch").find(".tierCheck").trigger('click');} $("#tier2Switch").slideUp();}
					
					if(($("#tier2Ops ul.selectedList li.selected").length == 1 && $("#tier2Ops").is(":visible") ) && !$("#tier3Switch").hasClass("noTierOptions")){ $("#tier3Switch").slideDown(); } 
					else { if($("#tier3Switch").find("input").val() === "true"){ $("#tier3Switch").find(".tierCheck").trigger('click'); }  $("#tier3Switch").slideUp(); }}
				else{ alertDialog(widgetSettingsSnglValuWarningMsg); }}
			else if(parentList.hasClass("selectedList")){
				inputObj.val(inputObj.val().replace(currID[1], "").replace(",,", ","));
				if(inputObj.val().charAt(0)==","){ inputObj.val(inputObj.val().substring(1)).trigger('change'); }
				if(inputObj.val().charAt(inputObj.val().length-1)==","){inputObj.val(inputObj.val().substring(0, inputObj.val().length-1)).trigger('change');}
				var newObjID = currObjID.replace(selectedList, "#full");
				var fullListObj = parentList.siblings(".fullList");
				var levelSelected = false;
				if(parentList.siblings(".fullListHeader").find(".checkBox").hasClass("checked")){ parentList.siblings(".fullListHeader").find(".checkBox").removeClass("checked") }

				$(newObjID).removeClass("selected inactive notclickable");
				$("#"+currObjID).removeClass("selected").slideUp(function(){ 
					//$(this);
				
					if(levelObj != false)
					{
						if(levelObj == "level2"){ 
							$(newObjID).nextUntil(".level2", "li").removeClass("selected inactive notclickable"); } 
						else if(levelObj == "level3"){
							$(newObjID).prevAll("li.level2").each(function(){ if($parentItem === ""){ $parentItem = $(this); } });
							$parentItem.nextUntil("li.level2").each(function(){ if(parentCnt < 1 && $(this).hasClass("selected")){ parentCnt++; } });
							if(parentCnt < 1){ $parentItem.removeClass("inactive notclickable"); } }
					}
					if($("#tier1Ops ul.selectedList li.selected").length == 1){ $(".lbl1034, .lbl8395, .lbl6783").addClass("disabled"); 
						if($(".lbl1034").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}
						if($(".lbl8395").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}
						if($(".lbl6783").attr("id") == $("#chartType").val()){$("#chartType").val('').trigger('change');}
					} 
					else if($("#tier1Ops ul.selectedList li.selected").length != 1 && !$("#tier3Check").hasClass("checked")) {
						var $currChartType;
						if($("#tier2Check").hasClass("checked")){
							$currChartType = $(".lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
						} 
						else {
							$currChartType = $(".lbl1034, .lbl8395, .lbl6783").removeClass("disabled").filter(".selected");
						}
						
						if($currChartType.length > 0) {
							$("#chartType").val($currChartType.attr("id")).trigger('change');;
						}
					}
					
				});
			}
			if($("#tier1Ops ul.selectedList.orgList li.selected").length == 1 && !$("#tier2Switch").hasClass("noTierOptions")){ $("#tier2Switch").slideDown(); } 
			else if($("#tier1Ops ul.orgList").length){if($("#tier2Switch").find("input").val() === "true"){
				$("#tier2Switch").find(".tierCheck").trigger('click');} $("#tier2Switch").slideUp(); }
			
			if(($("#tier2Ops ul.selectedList li.selected").length == 1 && $("#tier2Ops").is(":visible") ) && !$("#tier3Switch").hasClass("noTierOptions")){ 
				$("#tier3Switch").slideDown(); } 
			else { if($("#tier3Switch").find("input").val() === "true"){ $("#tier3Switch").find(".tierCheck").trigger('click'); }  $("#tier3Switch").slideUp(); } }
	});

	$("#dashboardPage").on("click", ".tierOps .selectedListHeader #clearSelected", function(event) // SELECT ALL FEATURE, SELECTS OR DESELECTS ALL IN FULL LIST 
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){
			var parentTier = $(this).parents(".tierOps");
			var inputObj = parentTier.children("input");
			
			parentTier.find("ul.fullList > li").each(function(index, element){ $(this).removeClass("selected inactive notclickable"); $(this).fadeIn("fast") });
			parentTier.find("ul.selectedList > li").each(function(index){ $(this).fadeOut("fast").removeClass("selected"); });
			parentTier.find(".fullListHeader").find(".checkBox").removeClass("checked");
			inputObj.val("").trigger('change');
		}
	});
	
	$("#dashboardPage").on("click", ".targetCheck", function(event) // SHOWS/HIDES TARGET VALUE AREA
	{
		event.preventDefault();
		var checkBoxObj = $(this).children(".checkBox");
		if(checkBoxObj.hasClass("checked") == true)
		{
			$("#target").hide();
			if($("#useMonthlyRevTarget").children(".checkBox").hasClass("checked") == true){ $("#useMonthlyRevTarget").trigger('click'); }
			$("#targetValueDisplay").removeClass("error");
			$("#targetValueDisplay").val("").trigger('change');
			$("#targetValueHidden").val("false").trigger('change');
			updateWidgetSettings("targetValueHidden");
		}else {$("#target").fadeIn();}
	});
	
	$("#dashboardPage").on("click", "#useMonthlyRevTarget", function(event) // SELECTS TARGET REVENUE BY LOCATION ACTIONS
	{
		event.preventDefault(); event.stopImmediatePropagation();
		var checkBoxObj = $(this).children(".checkBox");
		checkBoxObj.toggleClass("checked");
		if(checkBoxObj.hasClass("checked")) {
			$("#targetValueDisplay").val("").attr("disabled", "disabled").addClass("inactive").removeClass("error").trigger('change');
			$("#targetValueHidden").val("true").trigger('change');
			$("#targetLabel, #targetPre, #targetPost").addClass("inactive"); } 
		else {
			$("#targetValueDisplay").attr("disabled", false).removeClass("inactive", "error"); 
			$("#targetValueHidden").val("false").trigger('change');
			$("#targetLabel, #targetPre, #targetPost").removeClass("inactive"); }
		//updateWidgetSettings("targetValueHidden");
	});
	
	$("#dashboardPage").on("change", "#targetValueDisplay", function(){ // COPIES TARGET VALUE TO HIDDEN INPUT VALUE
		if(!$("#useMonthlyRevTarget").children(".checkBox").hasClass("checked")){
			if($(this).val() == "") {
				$("#targetValueHidden").val("false").trigger('change');
			}
			else {
				$("#targetValueHidden").val($(this).val()).trigger('change');
			}
		}
		//updateWidgetSettings("targetValueHidden");
	});
	
	$("#dashboardPage").on("click", "#chartDesign li section", function(event) // SELECTS CHART DESIGN OPTION
	{
		event.preventDefault();
		if(!$(this).hasClass("inactive") && !$(this).hasClass("disabled")){
			if($(this).hasClass("selected") == true)
			{
				$(this).removeClass("selected");
				if(metricLabel !== 5896 && metricLabel !== 9321){ $("#targetSwitch").slideDown("slow") };
				$("#chartType").val('').trigger('change');
			}else{
				$(this).parentsUntil("#chartDesign").siblings().find(".selected").removeClass("selected");
				$(this).addClass("selected");
				$(this).parents("#chartDesign").removeClass("error");
				$("#chartType").val($(this).attr("id")).trigger('change');
				if(metricLabel !== 5896 && metricLabel !== 9321){ 
					if($(this).hasClass("lbl1034") || $(this).hasClass("lbl3882")){ 
						if($("#target").is(":visible")){ $(".targetCheck").trigger('click'); } $("#targetSwitch").slideUp("slow"); }
					else { $("#targetSwitch").slideDown("slow"); } }
			}
		}
	});
}


	//--------------------------------------------------------------------------------------
	//  REVENUE TYPE OPTION LIST FORMATTING
	//--------------------------------------------------------------------------------------
	
function revenueTypeFormatting($fullList, $selectedList, $tierOpsAll, $tierAttr, $combinedList, $nxtTierSwitch){
	var	tierSelectedCount = 0,
    	tierFullOpList = '',
        tierSelectedOpList = '';
	if($nxtTierSwitch){ $nxtTierSwitch.addClass("noTierOptions"); }
	$combinedList.removeClass("orgList");
	if(optionBasedObj[5].length > 0){
		showOptions = true;
		for(var y = 0; y < optionBasedObj[5].length; y++){
			if(optionBasedObj[5][y]){
                var optionStatus = optionBasedObj[5][y].status,
                	optionName = optionBasedObj[5][y].name,
                	optionID = optionBasedObj[5][y].randomId,
                	optionLevel = 'level'+optionBasedObj[5][y].level;
                if(optionLevel == "level2"){
                    if(optionStatus == "selected"){ 
                        tierSelectedCount++; 
                        if($tierAttr.val() == ""){ $tierAttr.val(optionID); }
                        else{ $tierAttr.val($tierAttr.val()+","+optionID); } }
                    tierFullOpList += '<li id="full-'+ optionID +'" class="'+ optionStatus +' '+ optionLevel +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';
                    tierSelectedOpList += '<li id="select-'+ optionID +'" class="'+ optionStatus +' '+ optionLevel +'"><a href="#" class="checkBox">&nbsp;</a> '+ optionName +'</li>';
                    
                    for(var q = 0; q < optionBasedObj[5].length; q++){
                        var	chOptionStatus = optionBasedObj[5][q].status,
                        	chOptionName = optionBasedObj[5][q].name,
                        	chOptionID = optionBasedObj[5][q].randomId,
                        	chParentID = optionBasedObj[5][q].parentRandomId,
                        	chOptionLevel = 'level'+optionBasedObj[5][q].level;
                        
                        if(chOptionLevel == "level3" && chParentID == optionID){
                            if(chOptionStatus == "selected"){
								tierSelectedCount++;  
                                if($tierAttr.val() == ""){ $tierAttr.val(chOptionID); }
                                else{ $tierAttr.val($tierAttr.val()+","+chOptionID); } 
                                slctdLevel = "level3";}
                            tierFullOpList += '<li id="full-'+ chOptionID +'" class="'+ chOptionStatus +' '+ chOptionLevel +'"><a href="#" class="checkBox">&nbsp;</a> '+ chOptionName +'</li>';
                            tierSelectedOpList += '<li id="select-'+ chOptionID +'" class="'+ chOptionStatus +' '+ chOptionLevel +'"><a href="#" class="checkBox">&nbsp;</a> '+ chOptionName +'</li>'; } } } }
        else{ 
            allSelected = false;
            tierFullOpList += '<li class="notclickable alignCenter"><span class="textLine">'+noItemsExist+'</span></li>'; } }
    $fullList.html(tierFullOpList);
    $selectedList.html(tierSelectedOpList);
    $tierOpsAll.parents("label").hide();
    
    $fullList.children("li").each(
        function(index){
            if($(this).hasClass("level2") && !$(this).hasClass("selected")){ 
                $(this).nextUntil(".level2").each(function(index){ $(this).removeClass("inactive notclickable"); }); }
            else if($(this).hasClass("level2") && $(this).hasClass("selected")){ 
                $(this).nextUntil(".level2").each(function(index){ $(this).addClass("selected"); }); }
            else if($(this).hasClass("level3") && $(this).hasClass("selected")){
                var parentCnt = 0;
                $(this).prevAll(".level2").each(function(index){ if(parentCnt < 1){$(this).addClass("inactive notclickable");} parentCnt++; }); } }); }
	return showOptions; }

	//--------------------------------------------------------------------------------------
	//  SORT SCRIPTS
	//--------------------------------------------------------------------------------------

function editSort(inactive) // function allows widgets to be moved 
{
	var	startWidth = "",
		endWidth = "",
		$thisObj = "",
		$prevAddBtn = "";
		
	$(".column").sortable({
		disabled: inactive,
		appendTo:	".dashboardTab",
		connectWith: ".column",
		cursor: "move",
		distance: 30,
		handle: "header",
		items: ".widgetObject",
		cancel: ".widgetContent",
		opacity: oplSelected,
		revert: 200,
		tolerance: "pointer",
		scrollSensitivity: 150,
		scrollSpeed: 30,
/*		over:	function(){ $(this).children(".empty").hide(); },
		out:		function(){ $(this).children(".empty").show(); },
*/		start:	function(event, ui){
					$thisObj = ui.item;
					$prevAddBtn = $thisObj.prev();
					startWidth = $thisObj.width();
					while(!$prevAddBtn.hasClass("addWidgetHolder")){ $prevAddBtn = $prevAddBtn.prev(); if($prevAddBtn.length === 0){ break; } }
					$prevAddBtn.fadeOut();				
				},
/*		receive: 	function(){	},*/
		beforeStop: function(){ 
					var prevObj = ($thisObj.prev().hasClass("columnBox"))? $thisObj.prev().prev() : $thisObj.prev() ;
						if(prevObj.hasClass("addWidgetHolder") && prevObj.is(":visible")){
							$prevAddBtn.insertAfter($thisObj).show(); }
						else{ $prevAddBtn.insertBefore($thisObj).show(); }
					},
		stop: 	function(event, ui){
					endWidth = $thisObj.width();
					var redraw = false;
					if($thisObj.find(".chartGraph").hasClass("chartGraph") && endWidth != startWidth)
					{
						displayChart($thisObj.attr("id").replace("widgetID_", ""));
					}
					if(redraw == true){ }
					
					var curTabID = $(".dashboardTab:visible").attr("id");
					var tabQuerry = "tabID=" + curTabID.replace("tab", "");
					var sectionQuerry = null;
					$("#"+curTabID).children(".layout").each(function(){
						var sectionID = $(this).attr("id").split("_");
						if(sectionQuerry == null){sectionQuerry = sectionID[0]+"[]="+sectionID[1];}
						else{sectionQuerry = sectionQuerry+"&"+sectionID[0]+"[]="+sectionID[1];}
					});
					var widgetQuerry = null;
					$(".column:visible").has(".widgetObject").each(function(){
						if(widgetQuerry == null){widgetQuerry = $(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
						else{widgetQuerry = widgetQuerry+"&"+$(this).sortable("serialize", {key: $(this).attr("id")+"[]"});}
					});
					var fullQuerry = tabQuerry  + "&" + sectionQuerry  + "&" + widgetQuerry;
					widgetOrder(fullQuerry);
				}
	});
}

function editSortTabs(inactive) // function allows tabs to be moved 
{
	if(inactive == true){
		$(".tab, .tabSection").attr('title', $(this).html())}
	else if (inactive == false){
		$(".tab, .tabSection").attr('title', 'Click and drag to sort')
		$(".tabSection.current > .tab").attr('title', 'Click to edit name') }
	
	$("#dashboardNavArea").sortable({
		disabled: inactive,
		axis: "x",
		appendTo:	"#dashboardNavArea",
		cursor: "move",
		items: ".tabSection",
		cancel: ".tabDelete, .tabInput",
		distance: 10,
		opacity: oplSelected,
		revert: 200,
		tolerance: "pointer",
		stop:	function(){
					$(".tabInput").trigger('blur')
					var params = $("#dashboardNavArea").sortable("serialize")+"&"+document.location.search.substring(1);
					var ajaxTabOrdr = GetHttpObject();
					ajaxTabOrdr.onreadystatechange = function()
					{	
						if (ajaxTabOrdr.readyState==4 && ajaxTabOrdr.status==200){
							if(ajaxTabOrdr.responseText == "false")
							{
								$("#dashboardNavArea").sortable("cancel");
								alertDialog(editFail);
							}
						} else if(ajaxTabOrdr.readyState==4)
						{
							$("#dashboardNavArea").sortable("cancel");
							alertDialog(editFail);
						}
					};
					ajaxTabOrdr.open("POST","/secure/dashboard/tabOrder.html",true);
					ajaxTabOrdr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					ajaxTabOrdr.setRequestHeader("Content-length", params.length);
					ajaxTabOrdr.setRequestHeader("Connection", "close");
					ajaxTabOrdr.send(params); } });
}
