// JavaScript Document
var 	mapInPage, mapArea, ajaxRequest, plotlist, markers = new L.layerGroup(), requiredMarkers = new L.layerGroup(), recentMarkers = new L.layerGroup(), userMarkers = new L.layerGroup(), markerGroups=[], plotlayers=[], psIcon = {}, userIcn = {}, lotIcn = {}, minZoomLvl = 4, maxZoomLvl = 20, bounds = "", bindSouth = "", bindNorth = "", bindWest = "", bindEast = "", heatMapData=[], maps=[],
	osmUrl= mapURL+'osm/{z}/{x}/{y}.png',
	detailsUrl = ($(".sysAdminWorkSpace").length)? "/systemAdmin/workspace/payStations/" : "/secure/settings/locations/",
	userDetailsUrl = "/secure/settings/users/userAccount.html?filterValue=",
	ocupancyLvlClr = ["#046595", "#CC0000", "#F88B2B", "#3bb23e"],
	heatMapConfig = {
		"radius": 24,
		"maxOpacity": .8,
		"minOpacity": .05, 
		"scaleRadius": false, 
		"useLocalExtrema": false,
		"latField": "lat",
		"lngField": "lng",
		"valueField": "count",
		"blur": .75,
		"gradient": {
			".2": "#3d89af",
			".45": "#005782",
			".65": "#FEB474",
			".95": "#F88B2B" } },
		southWest = new L.LatLng("35","-150"),
		northEast = new L.LatLng("65","-50"),
		ogBounds = (!ogBounds || ogBounds === "")? new L.LatLngBounds(southWest, northEast) : ogBounds;

for(var y = 0; y < payStationIds.length; y++){ 	
	for(var z = 0; z <= 3; z++){
		var i = payStationIds[y];
		psIcon[payStationIcn[i].psID+"-"+z] =  L.icon({
			iconUrl: imgLocalDir+'mapIcn_'+payStationIcn[i].img[z]+'.png',
			iconSize: payStationIcn[i].icnSize,
			iconAnchor: payStationIcn[i].iconAnchor,
			popupAnchor: payStationIcn[i].popupAnchor,
			shadowUrl: imgLocalDir+'mapIcn_'+payStationIcn[i].shdwImg+'.png',
			shadowSize: payStationIcn[i].shdwSize,
			shadowAnchor: payStationIcn[i].shdwAnchor }); } }
			
for(var y = 0; y < mapUserIcn.length; y++){ 	
	userIcn[y] =  L.icon({
		iconUrl: imgLocalDir+'mapIcn_'+mapUserIcn[y].img+'.png',
		iconSize: mapUserIcn[y].icnSize,
		iconAnchor: mapUserIcn[y].iconAnchor,
		popupAnchor: mapUserIcn[y].popupAnchor,
		shadowUrl: imgLocalDir+'mapIcn_'+mapUserIcn[y].shdwImg+'.png',
		shadowSize: mapUserIcn[y].shdwSize,
		shadowAnchor: mapUserIcn[y].shdwAnchor }); }
		
for(var y = 0; y < mapLotIcn.length; y++){ 	
	lotIcn[y] =  L.icon({
		iconUrl: imgLocalDir+'mapIcn_'+mapLotIcn[y].img+'.png',
		iconSize: mapLotIcn[y].icnSize,
		iconAnchor: mapLotIcn[y].iconAnchor,
		popupAnchor: mapLotIcn[y].popupAnchor,
		shadowUrl: imgLocalDir+'mapIcn_'+mapLotIcn[y].shdwImg+'.png',
		shadowSize: mapLotIcn[y].shdwSize,
		shadowAnchor: mapLotIcn[y].shdwAnchor }); }

function initMapWidget(objectID, markerList)
{
	mapInPage = new L.Map(objectID);
	
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapInPage.addLayer(osm);
	if(markerList){ if(markerList instanceof Array === false){ markerList = [markerList]; } }
	else{ markerList = []; }
	if(markerList.length > 0){
		var bindSouth = ''; //latitude small
		var bindNorth = ''; //latitude large
		var bindWest = ''; //longitude small
		var bindEast = ''; //longitude large
		
		markers = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });

		for(x=0; x < markerList.length; x++){
			posMapIDs[markerList[x].serialNumber] = markerList[x].id;
			var posStatus = (markerList[x].status === '')?0:markerList[x].status; 
			var psIconID = markerList[x].type+"-"+posStatus;
			var	markerLat = parseFloat(markerList[x].lat),
				markerLong = parseFloat(markerList[x].long);
			if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
			if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
			if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
			if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}
			
			markers.addLayer(new L.marker([markerLat, markerLong], {icon: psIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true})
				.on("click", function(event){ getPosInfo(event.target); })); }
		mapInPage.addLayer(markers);
		southWest = new L.LatLng(bindSouth, bindWest);
		northEast = new L.LatLng(bindNorth, bindEast);
		bounds = new L.LatLngBounds(southWest, northEast); }
	else{ bounds = ogBounds; }
	mapInPage.fitBounds(bounds, {padding: [50,50]});
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
}

function initHeatMapWidget(objectID, markerList){ // HEAT MAP WIDGET MAP
	mapUID = objectID.split("_")[1];
	maps[objectID] = new L.Map(objectID);
	
	var heapMapBuffer = [];
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	maps[objectID].addLayer(osm);
	if(markerList){ if(markerList instanceof Array === false){ markerList = [markerList]; } }
	else{ markerList = []; }
	if(markerList.length > 0){
		var bindSouth = '';	//latitude small
		var bindNorth = '';	//latitude large
		var bindWest = '';	//longitude small
		var bindEast = '';	//longitude large
		
		markerGroups["markers_"+mapUID] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });

		for(x=0; x < markerList.length; x++){
			posMapIDs[markerList[x].serialNumber] = markerList[x].id;
			var posStatus = (markerList[x].status === '')?0:markerList[x].status; 
			var psIconID = markerList[x].type+"-"+posStatus;
			var	markerLat = parseFloat(markerList[x].lat),
				markerLong = parseFloat(markerList[x].long);
			if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
			if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
			if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
			if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}
			markerGroups["markers_"+mapUID].addLayer(new L.marker([markerLat, markerLong], {icon: userIcn[4], title: markerList[x].comment, alt: markerList[x].ticket, riseOnHover: true})
				.bindPopup('<section class="mapPopUp">' +
						'<h1>'+markerList[x].name+'</h1>' +
						'	<dl class="psDetails">' +
						'		<dt class="detailLabel">Citation Number: </dt><dd class="detailValue">'+ markerList[x].serialNumber +'</dd>'+
						'		<dt class="detailLabel">Date Issue: </dt><dd class="detailValue">'+ markerList[x].lastSeen +'</dd>' +
						'	</dl>' +
						'</section>')); 
			heapMapBuffer.push({lat: markerLat, lng:markerLong, count: 1}); }
			
		var heatMapDataObj = { max: markerList.length*.01, data: heapMapBuffer };
		markerGroups["heatmapLayer_"+mapUID] = new HeatmapOverlay(heatMapConfig);
		maps[objectID].removeLayer(markerGroups["heatmapLayer_"+mapUID]);
		maps[objectID].addLayer(markerGroups["heatmapLayer_"+mapUID]);

		southWest = new L.LatLng(bindSouth, bindWest);
		northEast = new L.LatLng(bindNorth, bindEast);
		bounds = new L.LatLngBounds(southWest, northEast); }
	else{ bounds = ogBounds; }
	maps[objectID].fitBounds(bounds, {padding: [50,50]});

	if(markerList.length > 0){ markerGroups["heatmapLayer_"+mapUID].setData(heatMapDataObj); }

	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
//	$("#heatMapOverlay").hide();
}

function initOccupancyMapWidget(objectID, parkingAreaMarkers){
	mapInPage = new L.Map(objectID);
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl}),
		bounds = ogBounds;
	mapInPage.addLayer(osm);

	if(parkingAreaMarkers && ( parkingAreaMarkers !== "emptyMap" && parkingAreaMarkers !== "noData")){
		for(x = 0; x < parkingAreaMarkers.length; x++){
			var localName = parkingAreaMarkers[x].localName,
				mapJsonData = parkingAreaMarkers[x].jsonData,
				occupancyLevel = parkingAreaMarkers[x].occpncyLevel,
				occupancyCount = parkingAreaMarkers[x].occpncyCount,
				autoCount = parkingAreaMarkers[x].autoCount,
				popUpContent = "",
				jsonLayer = L.geoJson("",{ pointToLayer: function (feature, latlng) { return L.marker(latlng, {icon: lotIcn[occupancyLevel], riseOnHover: true}); },
							onEachFeature: function(feature, layer){
								if(autoCount){ popUpContent = "<strong>"+localName+"</strong><br>Current Occupancy: "+Math.round(occupancyCount)+"%"; }
								else{ popUpContent = "<strong>"+localName+"</strong><br><em>Current Paid Occupancy: "+Math.round(occupancyCount)+"%</em>"; }
								layer.bindPopup(popUpContent); }}),
				styles = { color: ocupancyLvlClr[occupancyLevel], weight: 6, opacity: 0.9 };
			$("#mappingOptList").find("li").removeClass("disabled");

			jsonLayer.addData(mapJsonData);
			jsonLayer.setStyle(styles);
			if(x === 0){ bounds = jsonLayer.getBounds(); } else { bounds = bounds.extend(jsonLayer.getBounds()); }
			mapInPage.addLayer(jsonLayer); }}

	mapInPage.fitBounds(bounds, {padding: [50,50]});

	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />'); }

function initMapStatic(markerList, iconType, parkingAreaMarkers, occupancyLevel)
{	// set up the map
	if(!occupancyLevel || occupancyLevel === ""){ occupancyLevel = 0; }
	var $mapHolder = $("#mapHolder"),
		$mapObj = $("<section id='map' class='headerMap'></section>"),
		jsonLayer = L.geoJson("",{ pointToLayer: function (feature, latlng) { return L.marker(latlng, {icon: lotIcn[occupancyLevel], riseOnHover: true, clickable: false}); }});
	mapInPage = false;
	
	$mapHolder.html($mapObj);
	mapInPage = new L.Map("map", {
			dragging: false,
			touchZoom: false,
			scrollWheelZoom: false,
			doubleClickZoom: false,
			boxZoom: false,
			zoomControl: false });
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapInPage.addLayer(osm); 
	
	var mapIcon = (iconType && iconType === "user")? userIcn : psIcon;

	if(markerList && ( markerList !== "emptyMap" && markerList !== "noData")){
		if(markerList instanceof Array === false){ markerList = [markerList]; }
		var bindSouth = ''; //latitude small
		var bindNorth = ''; //latitude large
		var bindWest = ''; //longitude small
		var bindEast = ''; //longitude large
		
		markers = new L.layerGroup(); //STATIC MAP

		for(x=0; x < markerList.length; x++){
			var posStatus = (markerList[x].status === '')?0:markerList[x].status;
			var psIconID = (iconType === "user")? markerList[x].type : markerList[x].type+"-"+posStatus;
			var	markerLat = parseFloat(markerList[x].lat),
				markerLong = parseFloat(markerList[x].long);
			if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
			if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
			if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
			if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}
			markers.addLayer(new L.marker([markerLat, markerLong], {icon: mapIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true, clickable: false})); }
		mapInPage.addLayer(markers);
		southWest = new L.LatLng(bindSouth, bindWest);
		northEast = new L.LatLng(bindNorth, bindEast);
		bounds = new L.LatLngBounds(southWest, northEast); }
	else{ bounds = ogBounds; }
	
	if(parkingAreaMarkers && ( parkingAreaMarkers !== "emptyMap" && parkingAreaMarkers !== "noData")){
		var staticStyles = { color: ocupancyLvlClr[occupancyLevel], weight: 6, opacity: 0.9, clickable: false }
		$("#mappingOptList").find("li").removeClass("disabled");	
		jsonLayer.addData(parkingAreaMarkers);
		jsonLayer.setStyle(staticStyles);
		if(bounds === ogBounds){ bounds = jsonLayer.getBounds(); }
		else{ bounds = bounds.extend(jsonLayer.getBounds()); }
		mapInPage.addLayer(jsonLayer); }	
		
	mapInPage.fitBounds(bounds, {padding: [50,50]});
	if(markerList === "noData" && parkingAreaMarkers === "noData"){ $mapObj.append("<section class='noMap'><section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><article>"+ noMapDataMsg +"</article></section></section>"); }
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
}

function initMapPlacement(objectID, markerList)
{
	if(!mapInPage) { mapInPage = new L.Map(objectID); } else if(markers) { markers.clearLayers(); } // set up the map
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapInPage.addLayer(osm);
	if(markerList){ if(markerList instanceof Array === false){ markerList = [markerList]; } }
	else{ markerList = []; }
	if(markerList.length > 0){
		var bindSouth = ''; //latitude small
		var bindNorth = ''; //latitude large
		var bindWest = ''; //longitude small
		var bindEast = ''; //longitude large
		
		markers = new L.layerGroup();

		for(x=0; x < markerList.length; x++){
			var posStatus = (markerList[x].status === '')?0:markerList[x].status;			
			var psIconID = markerList[x].type+"-"+posStatus;
			var	markerLat = parseFloat(markerList[x].lat),
				markerLong = parseFloat(markerList[x].long);
			if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
			if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
			if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
			if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}

			markers.addLayer(new L.marker([markerLat, markerLong], {icon: psIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true})
				.bindPopup('<section class="mapPopUp">' +
					'<h1>'+markerList[x].name+'</h1>' +
					'	<section class="thumbNailButton">' +
					'		<img src="'+imgLocalDir+'paystation_'+payStationIcn[markerList[x].type].img[0]+'Sml.jpg" width="50" height="100" border="0" />' +
					'	</section>' +
					'	<dl class="psDetails">' +
					'		<dt class="detailLabel">Serial Number: </dt><dd class="detailValue">'+ markerList[x].serialNumber +'</dd>'+
					'		<dt class="detailLabel">Location: </dt><dd class="detailValue">'+ markerList[x].location +'</dd>' +
					'		<dt class="detailLabel">Placement: </dt><dd class="detailValue">'+ markerList[x].lat +',<br>'+ markerList[x].long +'</dd>' +
					'	</dl>' +
					'</section>')
				.on('dragstart', function(event){ this.closePopup(); })); }
		mapInPage.addLayer(markers);
		southWest = new L.LatLng(bindSouth, bindWest);
		northEast = new L.LatLng(bindNorth, bindEast);
		bounds = new L.LatLngBounds(southWest, northEast); }
	else{ bounds = ogBounds; }

	mapInPage.fitBounds(bounds, {padding: [50,50]})
		.on('mouseup', function(e) { if(markerListDrag){
			$(".pin2map").removeClass("pin2map").addClass("pined2map");
			var position = (curMarker)? curMarker.getLatLng() : e.latlng;
			$("#pinedLat").val(position.lat);
			$("#pinedLong").val(position.lng);
			var curPayStation = payStation[$(".pined2map").attr("id").substring(1)],
			markerDetails =	'<section class="mapPopUp">' +
							'<h1>'+curPayStation.name+'</h1>' +
							'	<section class="thumbNailButton">' +
							'		<img src="'+imgLocalDir+'paystation_'+payStationIcn[curPayStation.psType].img[0]+'Sml.jpg" width="50" height="100" border="0" />' +
							'	</section>' +
							'	<dl class="psDetails">' +
							'		<dt class="detailLabel">Serial Number: </dt><dd class="detailValue">'+ curPayStation.psSN +'</dd>'+
							'		<dt class="detailLabel">Location: </dt><dd class="detailValue">'+ curPayStation.location +'</dd>' +
							'		<dt class="detailLabel">Placement: </dt><dd class="detailValue">'+ $("#pinedLat").val() +',<br>'+ $("#pinedLong").val() +'</dd>' +
							'	</dl>' +
							'</section>';
			
			if((markers && !curMarker) || (curMarker && curMarker.options.alt != curPayStation.psSN)){
				markers.eachLayer( function(marker){ if(marker.options.alt == curPayStation.psSN){ curMarker = marker; } } ); }
			if(!curMarker){
				var marker =	new L.marker(position, {icon: psIcon[curPayStation.psType+"-0"], title: curPayStation.psSN, draggable: true}).bindPopup(markerDetails)
							.on('dragstart', function(event){ this.closePopup(); markerListDrag = true; }).on('dragend', function(event){ markerListDrag = false; }).addTo(mapInPage);
				markers.addLayer(marker);
				curMarker = marker; } 
			else{
				curMarker.setLatLng(position)
				.closePopup()
				.unbindPopup()
				.bindPopup(markerDetails)
				.on('dragstart', function(event){ this.closePopup(); markerListDrag = true; })
				.on('dragend', function(event){ markerListDrag = false; })
				.openPopup()
				.dragging.enable(); } }});
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
}

function initMapParkingAreaEdit(objectID, posMarkers, parkingAreaMarkers){
	bounds = ogBounds;
	var	$mapHolder = $("#mapParkingHolder"),
		$mapObj = $("<section id='"+objectID+"' class='mapHolder setHeight' style='width: 100%; height: 400px; margin-top: -8px;'></section>"),
		bindSouth = '', //latitude small
		bindNorth = '', //latitude large
		bindWest = '', //longitude small
		bindEast = '',
		staticMarkers = new L.layerGroup(),
		tempItems = new L.FeatureGroup(),
		drawnItems = new L.FeatureGroup(),
		jsonLayer = L.geoJson("",{ pointToLayer: function (feature, latlng) { return L.marker(latlng, {icon: lotIcn[0], riseOnHover: true }); }}),
		editMode = false,
		polyLineActive = false,
		polyLineStarted = false,
		markerActive = false,
		editActive = false,
		editMade = false,
		deleteActive = false,
		deleteMade = false,
		tempItemDrawn = false,
		mapDeleted = false;

	mapArea = false;
	$mapHolder.html($mapObj);

	if(!mapArea) { mapArea = new L.Map(objectID); } else if(markers) { markers.clearLayers(); } // set up the map
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapArea.addLayer(osm);

//PAY STATION MARKERS - STATIC ON MAP
	if(posMarkers && ( posMarkers !== "emptyMap" && posMarkers !== "noData")){	
		if(posMarkers instanceof Array === false){ posMarkers = [posMarkers]; }
		for(x=0; x < posMarkers.length; x++){
			var posStatus = (posMarkers[x].status === '')?0:posMarkers[x].status;
			var psIconID = posMarkers[x].type+"-"+posStatus;
			var	markerLat = parseFloat(posMarkers[x].lat),
				markerLong = parseFloat(posMarkers[x].long);
			if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
			if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
			if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
			if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}
			staticMarkers.addLayer(new L.marker([markerLat, markerLong], {icon: psIcon[psIconID], title: posMarkers[x].name, alt: posMarkers[x].serialNumber, riseOnHover: true, clickable: false})); }
		mapArea.addLayer(staticMarkers);
		southWest = new L.LatLng(bindSouth, bindWest);
		northEast = new L.LatLng(bindNorth, bindEast);
		bounds = new L.LatLngBounds(southWest, northEast); }

//STREET PARKING AREAS - EDITABLE
	if(parkingAreaMarkers && parkingAreaMarkers !== "noData"){
		var styles = { color: '#003E5E', weight: 6, opacity: 0.9 }
		$("#mappingOptList").find("li").removeClass("disabled");	
		jsonLayer.addData(parkingAreaMarkers);
		jsonLayer.setStyle(styles);
		drawnItems = jsonLayer;
		if(bounds === ogBounds){ bounds = drawnItems.getBounds(); }
		else{ bounds = bounds.extend(drawnItems.getBounds()); } }
	else{ $("#mappingOptList").find("li").each(function(index, element) {
		if($(this).attr("id") !== "streetParking" && $(this).attr("id") !== "parkingLot"){ $(this).addClass("disabled"); }}); }

	mapArea.addLayer(drawnItems);

	var drawControl = new L.Control.Draw({
		draw: {
			polyline: { metric: false, guidelineDistance: 10, repeatMode: true, shapeOptions: { color: '#003E5E', weight: 6, opacity: 0.9 } }, 
			polygon: false,
			circle: false,
			rectangle: false,
			marker: {repeatMode: true, icon: lotIcn[0] } },
		edit: {
			featureGroup: drawnItems,
			edit: { selectedPathOptions: {
				maintainColor: true,
				opacity: 0.3 }},
			remove: true } });
			
		mapArea.on('click', function(e){
			if(!$("#mainContent").hasClass("edit") && editMode){ $("#mainContent").addClass("edit"); } 
			if(polyLineActive && !polyLineStarted){
				polyLineStarted = true;
				$("#streetParking-Revert").removeClass("inactive"); } });
		
		mapArea.on('mouseup', function(e){ 
			if(editActive){
				if(!$("#mainContent").hasClass("edit") && editMode){ $("#mainContent").addClass("edit"); }
				editMade = true;
				$("#editParking-Save").removeClass("inactive").addClass("Ftr"); }
			else if(deleteActive){
				if(!$("#mainContent").hasClass("edit") && editMode){ $("#mainContent").addClass("edit"); }
				deleteMade = true;
				$("#removeParking-Save").removeClass("inactive").addClass("Ftr"); }});
			
		mapArea.on('draw:created', function (e) { 
			var layer = e.layer;
			if(polyLineActive){	
				polyLineStarted = false;
				$("#streetParking-Revert").addClass("inactive");
				$("#streetParking-Save").removeClass("inactive").addClass("Ftr"); }
			else if(markerActive){
				if(!$("#mainContent").hasClass("edit") && editMode){ $("#mainContent").addClass("edit"); }
				$("#parkingLot-Save").removeClass("inactive").addClass("Ftr");}
			tempItemDrawn = true;
			tempItems.addLayer(layer);
			drawnItems.addLayer(layer); });
		
		var	polyLine = new L.Draw.Polyline(mapArea, drawControl.options.draw.polyline),
			marker = new L.Draw.Marker(mapArea, drawControl.options.draw.marker),
			editMap = new L.EditToolbar.Edit(mapArea, {featureGroup: drawControl.options.edit.featureGroup,
					selectedPathOptions: drawControl.options.edit.edit.selectedPathOptions}),
			deleteParking = new L.EditToolbar.Delete(mapArea, {featureGroup: drawControl.options.edit.featureGroup}),
			endMapDraw = function (typeLine, action){ 
				if(typeLine === "polyline"){
					$("#streetParking-Save, #streetParking-Revert").addClass("inactive").removeClass("Ftr");
					$("#streetParking").removeClass("selected");
					polyLineActive = false;
					tempItemDrawn = false;
					if(action === "cancel"){
						$("#streetParking").siblings("li").removeClass("inactive");
						tempItems.eachLayer(function (layer) { if(mapArea.hasLayer(layer)){ mapArea.removeLayer(layer); drawnItems.removeLayer(layer); }; }); }
					else if(action === "save"){
						$("#streetParking").siblings("li").removeClass("inactive disabled");
						mapDeleted = false;
						$("#mapingComplete").removeClass("inactive"); }
					tempItems.clearLayers();
					polyLine.disable(); }
				else if(typeLine === "marker"){ 
					$("#parkingLot-Save").addClass("inactive").removeClass("Ftr");
					$("#parkingLot").removeClass("selected");
					markerActive = false;
					tempItemDrawn = false;
					if(action === "cancel"){
						$("#parkingLot").siblings("li").removeClass("inactive");
						tempItems.eachLayer(function (layer) { if(mapArea.hasLayer(layer)){ mapArea.removeLayer(layer); drawnItems.removeLayer(layer); }; }); }
					else if(action === "save"){
						$("#parkingLot").siblings("li").removeClass("inactive disabled");
						mapDeleted = false;
						$("#mapingComplete").removeClass("inactive"); }
					tempItems.clearLayers();
					marker.disable(); }
				else if(typeLine === "edit"){
					$("#editParking-Save").addClass("inactive").removeClass("Ftr");
					$("#editParking").removeClass("selected");
					$("#editParking").siblings("li").removeClass("inactive");
					editActive = false;
					editMade = false;
					if(action === "cancel"){
						editMap.revertLayers(); }
					else if(action === "save"){
						editMap.save();
						mapDeleted = false;
						$("#mapingComplete").removeClass("inactive"); }
					editMap.disable(); }
				else if(typeLine === "delete"){
					$("#removeParking-Save").addClass("inactive").removeClass("Ftr");
					$("#removeParking").removeClass("selected");
					$("#removeParking").siblings("li").removeClass("inactive");
					editActive = false;
					editMade = false;
					if(action === "cancel"){
						deleteParking.revertLayers(); }
					else if(action === "save"){
						deleteParking.save();
						$("#mapingComplete").removeClass("inactive"); }
					deleteParking.disable(); }
/*				else if(typeLine === "clearMap"){
					if(!$("#mainContent").hasClass("edit") && editMode){ $("#mainContent").addClass("edit"); }
					drawnItems.clearLayers();
					mapDeleted = true;
					$("#mapingComplete").removeClass("inactive"); }
*/	
			editMode = false; };
		
	mapArea.fitBounds(bounds, {padding: [50,50]});
		
		$("#streetParking-Draw").on("click", function(event){ event.preventDefault(); 
			if(!$(this).parent("li").hasClass("inactive")){
				$(this).parent("li").addClass("selected");
				$(this).parent("li").siblings("li").addClass("inactive");
				editMode = true;
				polyLineActive = true;
				polyLineStarted = false;
				polyLine.enable(); }});
	
		$("#streetParking-Revert").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){ polyLine.deleteLastVertex(this); } });

		$("#streetParking-Cancel").on("click", function(event){ event.preventDefault();
			var tempAction = function(){ return endMapDraw("polyline", "cancel"); };
			if(polyLineStarted || tempItemDrawn){ 
				confirmDialog({
					title: alertTitle,
					message: cancelStreetMapMsg,
					okLabel: btnRemoveMarkers,
					okCallback: tempAction,
					cancelLabel: btnBackToStreet }); }
			else{ tempAction(); } });
				
		$("#streetParking-Save").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){
				if(polyLineStarted){ alertDialog(finishStreetMapMsg); }
				else{ endMapDraw("polyline", "save"); }} });

		$("#parkingLot-Draw").on("click", function(event){ event.preventDefault(); 
			if(!$(this).parent("li").hasClass("inactive")){
				$(this).parent("li").addClass("selected");
				$(this).parent("li").siblings("li").addClass("inactive");
				editMode = true;
				markerActive = true;
				marker.enable(); }});

		$("#parkingLot-Cancel").on("click", function(event){ event.preventDefault(); 
			var tempAction = function(){ return endMapDraw("marker", "cancel"); };
			if(tempItemDrawn){ 
				confirmDialog({
					title: alertTitle,
					message: cancelLotMsg,
					okLabel: btnRemoveMarkers,
					okCallback: tempAction,
					cancelLabel: btnBackToLot }); }
			else{ tempAction(); } });
		
		$("#parkingLot-Save").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){
				endMapDraw("marker", "save"); }});
		
		$("#editParking-Edit").on("click", function(event){ event.preventDefault(); 
			if(!$(this).parent("li").hasClass("inactive")){
				$(this).parent("li").addClass("selected");
				$(this).parent("li").siblings("li").addClass("inactive");
				editMode = true;
				editActive = true;
				editMap.enable(); }});
		
		$("#editParking-Cancel").on("click", function(event){ event.preventDefault();
			var tempAction = function(){ return endMapDraw("edit", "cancel"); };
			if(editMade){ 
				confirmDialog({
					title: alertTitle,
					message: cancelEditMsg,
					okLabel: btnCancelChanges,
					okCallback: tempAction,
					cancelLabel: btnBackToEdit }); }
			else{ tempAction(); } });

		$("#editParking-Save").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){
				endMapDraw("edit", "save"); }});
			
		$("#removeParking-Edit").on("click", function(event){ event.preventDefault(); 
			if(!$(this).parent("li").hasClass("inactive")){
				$(this).parent("li").addClass("selected");
				$(this).parent("li").siblings("li").addClass("inactive");
				editMode = true;
				deleteActive = true;
				deleteParking.enable(); }});
	
		$("#removeParking-Cancel").on("click", function(event){ event.preventDefault(); 
			var tempAction = function(){ return endMapDraw("delete", "cancel"); };
			if(deleteMade){ 
				confirmDialog({
					title: alertTitle,
					message: cancelEditMsg,
					okLabel: btnCancelChanges,
					okCallback: tempAction,
					cancelLabel: btnBackToRemove }); }
			else{ tempAction(); } });

		$("#removeParking-Save").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){
				endMapDraw("delete", "save"); }});
				
/*		$("#clearMap-Request").on("click", function(event){ event.preventDefault(); 
			if(!$(this).hasClass("inactive")){
				var tempAction = function(){ return endMapDraw("clearMap"); };
				confirmDialog({
					title: alertTitle,
					message: "Are you sure you want to delete all of the maped parking area?",
					okLabel: "Clear Map",
					okCallback: tempAction,
					cancelLabel: "Don't Clear Map" }); }});
*/		
		$("#mapingComplete").on("click", function(event){ event.preventDefault(); event.stopPropagation();
			if(!$(this).hasClass("inactive")){
				var x = 0, tempGeoData = [], tempLatLngs;
				if(mapDeleted){tempGeoData = "clearMap"}
				else{
					for(var key in drawnItems._layers){ 
						if(drawnItems._layers.hasOwnProperty(key)){
							tempLatLngs = drawnItems._layers[key].toGeoJSON();
							tempGeoData.push(tempLatLngs); }}
					if(key === undefined){ tempGeoData = "clearMap" }}
				postGeoParkingData(tempGeoData); }});
			
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />'); }

function initMapCollections(objectID, markerList, listStatus, preLoad) 
{
	if(!mapInPage) { // set up the map
		mapInPage = new L.Map(objectID);
		markerGroups["activeList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });
		markerGroups["recentList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });
		markerGroups["userList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 }); 
		bounds = "",
		bindSouth = "",	//latitude small
		bindNorth = "",	//latitude large
		bindWest = "",		//longitude small
		bindEast = "";	}	//longitude large
	else if(markerGroups[listStatus]){ 
		markerGroups[listStatus].clearLayers(); 
		markerGroups[listStatus] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 }); }
		
	// create the tile layer with correct attribution
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapInPage.addLayer(osm);
	
	var mapIcon = (listStatus === "userList")? userIcn : psIcon ;

	if(markerList && markerList.length > 0){
		if(markerList instanceof Array === false){ markerList = [markerList]; }

		for(x=0; x < markerList.length; x++){
			if(markerList[x].lat){
				var posStatus = (!markerList[x].status || markerList[x].status === '')?0:markerList[x].status;		
				var psIconID = (listStatus === "userList")? posStatus : markerList[x].type+"-"+posStatus;
				var	markerLat = parseFloat(markerList[x].lat),
					markerLong = parseFloat(markerList[x].long);
					if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
					if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
					if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
					if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}

				markerGroups[listStatus].addLayer(new L.marker([markerLat, markerLong], { icon: mapIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true })
					.on("click", function(event){ markerClick(event.target, listStatus); })); }}

		//if(listStatus === currentListObj || listStatus === "userList"){ 
			mapInPage.addLayer(markerGroups[listStatus]);

			if(bindSouth !== ""){ 
				southWest = new L.LatLng(bindSouth, bindWest);
				northEast = new L.LatLng(bindNorth, bindEast);
				bounds = new L.LatLngBounds(southWest, northEast); }}//}
			
	if(bounds === ""){ 
		bounds = ogBounds; }
	if((listStatus === currentListObj && !preLoad) || (listStatus === "userList")){ mapInPage.fitBounds(bounds, {padding: [50,50]}); }
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
	
	if(preLoad === true){ for(x=0; x < pageTabs[listType].length; x++){ 
		if(pageTabs[listType][x] !== currentListObj){ mapMarkers(false, pageTabs[listType][x]); }}}
}

function initMapAlerts(objectID, markerList, listStatus, preLoad) 
{
	if(!mapInPage) { // set up the map
		mapInPage = new L.Map(objectID, {});
		markerGroups["activeList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });
		markerGroups["recentList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });
		markerGroups["userList"] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });
		bounds = "",
		bindSouth = "",	//latitude small
		bindNorth = "",	//latitude large
		bindWest = "",		//longitude small
		bindEast = "";	}	//longitude large
	else if(markerGroups[listStatus]){ 
		markerGroups[listStatus].clearLayers(); 
		markerGroups[listStatus] = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 }); }

// create the tile layer with correct attribution
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});    
    mapInPage.addLayer(osm);

	if(markerList && markerList.length > 0){
		if(markerList instanceof Array === false){ markerList = [markerList]; }

		for(x=0; x < markerList.length; x++){
			if(markerList[x].lat){
				var posStatus = (!markerList[x].status || markerList[x].status === '')?0:markerList[x].status;
				var psIconID = markerList[x].type+"-"+posStatus;
				var	markerLat = parseFloat(markerList[x].lat),
					markerLong = parseFloat(markerList[x].long);
					if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
					if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
					if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
					if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}

				markerGroups[listStatus].addLayer(new L.marker([markerLat, markerLong], { icon: psIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true })
					.on("click", function(event){ markerClick(event.target, listStatus); })); }}

		mapInPage.addLayer(markerGroups[listStatus]);
		if(bindSouth !== ""){ 
			southWest = new L.LatLng(bindSouth, bindWest);
			northEast = new L.LatLng(bindNorth, bindEast);
			bounds = new L.LatLngBounds(southWest, northEast); }}
	
	if(bounds === ""){ bounds = ogBounds; }
	mapInPage.fitBounds(bounds, {padding: [50,50]});

	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
}

function initMapSettings(objectID, markerList)
{ 
	if(!mapInPage) { mapInPage = new L.Map(objectID); } else if(markers) { markers.clearLayers(); } // set up the map
	
	// create the tile layer with correct attribution
	var osm = new L.TileLayer(osmUrl, {minZoom: minZoomLvl, maxZoom: maxZoomLvl});
	mapInPage.addLayer(osm);
	
	if(markerList){ if(markerList instanceof Array === false){ markerList = [markerList]; } }
	else{ markerList = []; }
	if(markerList.length > 0){
		var bindSouth = ''; //latitude small
		var bindNorth = ''; //latitude large
		var bindWest = ''; //longitude small
		var bindEast = ''; //longitude large
		
		markers = new L.MarkerClusterGroup({ showCoverageOnHover: false, disableClusteringAtZoom: 14 });

		for(x=0; x < markerList.length; x++){
			if(markerList[x].lat){
				var posStatus = (markerList[x].status === '')?0:markerList[x].status; 			
				var psIconID = markerList[x].type+"-"+posStatus;
				var	markerLat = parseFloat(markerList[x].lat),
					markerLong = parseFloat(markerList[x].long);
				if(bindSouth == '' || bindSouth > markerLat){bindSouth = markerLat;}
				if(bindNorth == '' || bindNorth < markerLat){bindNorth = markerLat;}
				if(bindWest == '' || bindWest > markerLong){bindWest = markerLong;}
				if(bindEast == '' || bindEast < markerLong){bindEast = markerLong;}
				
				markers.addLayer(new L.marker([markerLat, markerLong], { icon: psIcon[psIconID], title: markerList[x].name, alt: markerList[x].serialNumber, riseOnHover: true })
					.bindPopup(
						'<section class="mapPopUp">' +
						'<h1>'+markerList[x].name+'</h1>' +
						'	<section class="thumbNailButton">' +
						'		<img src="'+imgLocalDir+'paystation_'+payStationIcn[markerList[x].type].img[0]+'Sml.jpg" width="50" height="100" border="0" /><br /><br />' +
						'	</section>' +
					
						'	<dl class="psDetails">' +
						'		<dt class="detailLabel">Location: </dt><dd class="detailValue">'+ markerList[x].location +'</dd>' +
						'		<dt class="detailLabel">Setting: </dt><dd class="detailValue '+ markerList[x].batteryVoltage +'">'+ markerList[x].lastSeen +'</dd>'+
						'		<dt class="detailLabel">Download Date: </dt><dd class="detailValue">'+ markerList[x].paperStatus +'</dd>' +
						'	</dl>' +
						'</section>' )); }}
		mapInPage.addLayer(markers);
		if(bindSouth !== ""){ 
			southWest = new L.LatLng(bindSouth, bindWest);
			northEast = new L.LatLng(bindNorth, bindEast);
			bounds = new L.LatLngBounds(southWest, northEast); }}
	else{ bounds = ogBounds; }
	mapInPage.fitBounds(bounds, {padding: [50,50]});
	$(".leaflet-control-zoom-in, .leaflet-control-zoom-out").html('<img src="'+imgLocalDir+'spacer.gif" width="18" height="1" border="0" />');
}