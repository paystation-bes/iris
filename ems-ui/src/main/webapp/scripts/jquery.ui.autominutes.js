(function($, undefined) {
	var timeRangesHash = {};
	
	$.widget("ui.autominutes", $.ui.autoselector, {
		"options": {
			"militaryMode": true,
			"nextDayMode": false,
			"allows2400": false,
			"intervalMins": 15,
			"minLimit": 0,
			"maxLimit": 1439,
			"isComboBox": true,
			"isSeconds": false,
			"defaultValue": null,
			"data": function() {
				var range = timeRangesHash[this.rangeId];
				if(typeof range === "undefined") {
					range = [];
					
					var mins = this.minLimit;
					var lastOption = null;
					if(this.nextDayMode || this.allows2400) {
						if(this.maxLimit >= 1439) {
							lastOption = {
								"label": this._formatTime(this.militaryMode, (this.allows2400) ? 24 : 0, 0),
								"value": this._formatTime(true, (this.allows2400) ? 24 : 0, 0)
							};
							
							if(this.minLimit <= 0) {
								mins += this.intervalMins;
							}
						}
					}
					
					for( ; mins <= this.maxLimit; mins += this.intervalMins) {
						var hrs = Math.floor(mins / 60);
						var pureMins = mins - (hrs * 60);
						range.push({
							"label": this._formatTime(this.militaryMode, hrs, pureMins),
							"value": this._formatTime(true, hrs, pureMins)
						});
					}
					
					if(lastOption != null) {
						range.push(lastOption);
					}
					
					timeRangesHash[this.rangeId] = range;
				}
				
				return range;
			},
			"invalidSelectionMessage": function() {
				return (commonMessages && commonMessages.invalidTimeSelection) ? commonMessages.invalidTimeSelection : "Invalid Time !";
			},
		},
		"_create": function() {
			this.rangeId = null;
			this._updateRangeId();
			
			$.ui.autoselector.prototype._create.apply(this, arguments);
		},
		"parse": function(label) {
			var result = null,
				mins = (!this.options.isSeconds)? this._parseTime(label) : 0,
				sec = (this.options.isSeconds)? this._parseTime(label) : 0;
			if(mins != null && sec != null) {
				result = this._formatTime(true, 0, mins, sec);
			}

			return result;
		},
		"format": function(value) {
			var result = null,
				mins = (!this.options.isSeconds)? this._parseTime(value) : 0,
				sec = (this.options.isSeconds)? this._parseTime(value) : 0;
			if(mins != null && sec != null) {
				result = this._formatTime(this.options.militaryMode, 0, mins, sec);
			}
			
			return result;
		},
		"resetTime": function() {
			var defaultVal = this.options.defaultValue;
			this.setTime(defaultVal);
		},
		"getTime": function() {
			return this.elementValContainer.val();
		},
		"getTimeInMinutes": function() {
			return this._parseTime(this.elementValContainer.val());
		},
		"isNextDayMode": function() {
			return this.options.nextDayMode || this.options.allows2400;
		},
		"setTime": function(timeStr) {
			if(typeof timeStr === "number") {
				var buffer = this._splitTime(timeStr);
				this.element.val(this._formatTime(this.options.militaryMode, buffer[0], buffer[1])).trigger('change');
				this.elementValContainer.val(this._formatTime(true, buffer[0], buffer[1])).trigger('change');
			}
			else {
				timeStr = this._convertToStr(timeStr);
				this.element.val(this.formatTime(timeStr)).trigger('change');
				this.elementValContainer.val(this._convertToStr(timeStr)).trigger('change');
			}
		},
		"_splitTime": function(minutes) {
			var result = [];
			result[1] = minutes % 60;
			result[0] = Math.ceil((minutes - result[1]) / 60);
			
			return result;
		},
		"_convertToStr": function(timeStr) {
			var result = "";
			if((typeof timeStr !== "undefined") && (timeStr !== null)) {
				if((typeof timeStr !== "string") && (typeof timeStr.getHours === "function")) {
					result = this._formatTime(true, timeStr.getHours(), timeStr.getMinutes());
				}
				else {
					result = timeStr;
				}
			}
			
			return result;
		},
		/* Override */
		"findByValue": function(value) {
			var result = null;
			if(value && value.length > 0) {
				result = { "value": value, "label": this.formatTime(value) };
			}
			
			return result;
		},
		"formatTime": function(timeStr) {
			var result = "";
			
			timeStr = this._convertToStr(timeStr);
			if(timeStr.length > 0) {
				var time = timeStr.split(":");
				if(isNaN(time[0]) || isNaN(time[1])) {
					throw "Time string should be in the format HH:MM! [current is \"" + timeStr + "\"";
				}
				else {
					time[0] = parseInt(time[0], 10);
					time[1] = parseInt(time[1], 10);
					if(time.length > 2){ time[2] = parseInt(time[2], 10); }
				}
				
				var is2400 = this.options.allows2400 && (time[0] === 24) && (time[1] === 0);
				if((time[0] < 0) || (time[0] > 23 && !is2400) || (time[1] < 0) || (time[1] > 59) || ( time.length > 2 && (time[2] < 0) || (time[2] > 59))) {
					throw "Invalid time: " + timeStr;
				}
				
				result = this._formatTime(this.options.militaryMode, time[0], time[1], time[2]);
			}
			
			return result;
		},
		"_formatTime": function(militaryMode, hrs, mins, secs) {
			var useSecs = (this.options.isSeconds && secs != null)? true : false,
				result,
				transHrs, 
				indctr,
				actualSecs,
				actualMins,
				actualHrs;
				
				
			if(this.options.allows2400 && (hrs === 24)) {
				actualMins = 0;
				actualHrs = 24; }
			else if(mins > 60 || secs > 60) { 
				if(useSecs) {
					actualSecs = secs % 60;
					actualMins = (mins + ((secs - actualSecs)/60)) % 60;
					actualHrs = (hrs + ((secs - actualSecs - ((actualMins - mins) * 60))/3600)) % 24; }
				else {
					actualHrs = Math.floor((mins/60)/60);
					actualMins = (mins/60)-(actualHrs*60); } }
			else {
				actualHrs = hrs;
				actualMins = mins;
				actualSecs = secs; }
			
			if(militaryMode) {
				result = ((actualHrs > 9) ? "" : "0") + actualHrs + ":" + ((actualMins > 9) ? "" : "0") + actualMins;
				if(useSecs) { result = result + ":" + ((actualSecs > 9) ? "" : "0") + actualSecs; } }
			else {
				if(actualHrs < 12) {
					indctr = "AM";
					if(actualHrs === 0) {
						transHrs = 12; }
					else {
						transHrs = actualHrs; } }
				else {
					indctr = "PM";
					if(actualHrs === 12) {
						transHrs = 12; }
					else {
						if(actualHrs === 24){ transHrs = actualHrs - 12; indctr = "AM"; }
						else{ transHrs = actualHrs - 12; } } }
				var displayHrs = ((transHrs > 9) ? "" : "0") + transHrs,
					displayMins = ":" + ((actualMins > 9) ? "" : "0") + actualMins,
					displaySecs = (useSecs)? ":" + ((actualSecs > 9) ? "" : "0") + actualSecs : "" ;
					
				result = displayHrs + displayMins + displaySecs + " " + indctr; }
			
			return result; },
		"_parseTime": function(timeStr) {
			var result = null;
			
			var inputType = typeof timeStr;
			if(inputType === "number") {
				result = timeStr;
			}
			else if(inputType === "string") {
				timeStr = timeStr.trim();
				if(timeStr.length > 0) {
					var matches = timeStr.match(/^([0-9]+)?\s*(:\s*[0-9]+)?\s*(:\s*[0-9]+)?\s*(([ap])\.?m?\.?)?$/i);
					if(matches) {
						var hour = matches[1];
						if(hour) {
							hour = parseInt(hour, 10);
						}
						else {
							hour = 0;
						}
						
						var min = matches[2];
						if(min) {
							min = parseInt(min.substring(1), 10);
						}
						else {
							min = 0;
						}

						var sec = (matches[3])? matches[3] : false;
						if(sec) {
							sec = parseInt(sec.substring(1), 10);
						}
						else {
							sec = 0;
						}
						
						var is2400 = this.options.allows2400 && (hour === 24) && (min === 0);
						var correctTime = (sec >= 0) && (sec < 60) && (min >= 0) && (min < 60) && (hour >= 0) && ((hour < 24) || is2400);
						if(correctTime) {
							if(typeof matches[5] === "undefined") { matches[5] = (hour > 12)? "p" : "a" ; }
							var indctr = matches[5];
							
							if(typeof indctr !== undefined) {
								indctr = indctr.toLowerCase();
								if(indctr === "p") {
									hour = hour % 12;
									if(hour < 12) {
										hour = hour + 12;
									}
								}
								else if(indctr === "a") {
									hour = hour % 12;
									if(hour === 12) {
										hour = 0;
									}
								}
								else {
									correctTime = false;
								}
							}
						}
						
						if(correctTime) {
							result = ((hour * 60) * 60) + (min * 60) + sec;
						}
					}
				}
			}
			
			return result;
		},
		"_setOption": function(key, value) {
			if(key == "data") {
				throw "autominutes doesn't support override of \"data\" function !";
			}
			else {
				$.ui.autoselector.prototype._setOption.apply(this, arguments);
				this._updateRangeId();
			}
		},
		"_updateRangeId": function() {
			var nextDayMode = this.options.nextDayMode;
			var allows2400 = this.options.allows2400;
			var militaryMode = this.options.militaryMode;
			var intervalMins = this.options.intervalMins;
			
			var minLimit = this._parseTime(this.options.minLimit);
			var maxLimit = this._parseTime(this.options.maxLimit);
			if((minLimit == null) || (minLimit < 0)) {
				minLimit = 0;
			}
			else {
				if(minLimit >= 1440) {
					minLimit = minLimit % 1440;
				}
				
				minLimit = Math.ceil(minLimit / intervalMins) * intervalMins;
			}
			
			if((maxLimit == null) || (maxLimit < 0)) {
				maxLimit = 1439;
			}
			else if(maxLimit >= 1440) {
				maxLimit = maxLimit % 1440;
			}
			
			var rangeId = militaryMode + "|" + nextDayMode + "," + allows2400 + ">" + intervalMins + ":[" + minLimit + "," + maxLimit + "]";
			if((this.rangeId === null) || (this.rangeId !== rangeId)) {
				this.rangeId = rangeId;
				
				this.nextDayMode = nextDayMode;
				this.allows2400 = allows2400;
				this.militaryMode = militaryMode;
				this.intervalMins = intervalMins;
				this.minLimit = minLimit;
				this.maxLimit = maxLimit;
				
				this.updateData();
			}
		}
	});
}(jQuery));
