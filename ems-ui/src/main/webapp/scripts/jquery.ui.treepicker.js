/*
 * The terms which will be used in this widget...
 *  - "NodeObj" is javascript object represent each node in this tree (except the root node which cannot, of course, be modified)
 *  - "RawObj" is raw data which will be used to create the "nodeObj"
 */
(function($, undefined) {
	$.widget("ui.treepicker", {
		"options": {
			"fullListCSS": "ui-treepicker-list-full",
			"selectedListCSS": "ui-treepicker-list-selected",
			"rootSelectorCSS": "ui-treepicker-root-selector",
			
			"levelDisplayThreshold": 1,
			"nodesDisplayThreshold": 100,
			"showSelectedHierarchy": true,
			
			"parentAttribute": "parent",
			"childAttribute": "children",
			"selectedAttributeName": "isSelected",
			"partialSelectedAttributeName": "isPartialSelected",
			
			"allowSelectionToggle": true,
			
			"propagateSelectionUp": true,
			"propagateSelectionDown": true,
			"propagateUnselectionUp": true,
			"propagateUnselectionDown": true,
			
			"levelIndent": 17,
			"spacerImagePath": imgLocalDir + "spacer.gif",
			
			"nodeValue": function(nodeObj) {
				return nodeObj;
			},
			"drawNode": function(node, $nodeContainer, conf) {
				if(conf.showSelector) {
					$nodeContainer.append($("<a>").attr("href", "#").addClass("treepicker-selector").text("[]"));
				}
				
				$nodeContainer.append(node);
				
				if(conf.showExpander) {
					$nodeContainer.append($("<a>").attr("href", "#").addClass("treepicker-expander").text("+"));
				}
			}
		},
		"_create": function() {
			var that = this;
			
			this.id = this.element.attr("id");
			while((!this.id) || ($("#" + this.id).length > 1)) {
				this.id = [ "_treepicker_", (new Date()).getTime(), "_", Math.floor(Math.random() * 100000) ].join("");
			}
			
			this.element.addClass("ui-treepicker");
			
			this.fullList = this.element.find("ul." + this.options.fullListCSS);
			if(this.fullList.length <= 0) {
				this.fullList = $("<ul>").addClass(this.options.fullListCSS).appendTo(this.element);
			}
			
			this.selectedList = this.element.find("ul." + this.options.selectedListCSS);
			if(this.selectedList.length <= 0) {
				this.selectedList = $("<ul>").addClass(this.options.selectedListCSS).appendTo(this.element);
			}
			
			this.rootSelector = this.element.find("." + this.options.rootSelectorCSS);
			
			this.parentAttrName = this.options.parentAttribute;
			this.childAttrName = this.options.childAttribute;
			this.selectedAttrName = this.options.selectedAttributeName;
			this.partialSelectedAttrName = this.options.partialSelectedAttributeName;
			this.levelIndent = this.options.levelIndent;
			if((typeof this.levelIndent === "undefined") || (this.levelIndent === null) || (isNaN(this.levelIndent))) {
				this.levelIndent = 0;
			}
			
			this.rootNode = {
				"isRoot": true,
				"_nodeId": "node"
			};
			
			this.rootNode[this.childAttrName] = [];
			
			/* Register Events... */
			this.element
				.on("click", "." + this.options.rootSelectorCSS, function(event) {
					event.preventDefault();
					
					if(that.isRootSelected()) {
						that.resetSelection();
					}
					else {
						that.select(null);
					}
					
					return false;
				});
			
			this.fullList
				.on("click", ".treepicker-expander", function(event) {
					event.preventDefault();
					
					var $listElem = $(event.target);
					if(!$listElem.is("li")) {
						$listElem = $listElem.parents("li:first");
					}
					
					if($listElem.length > 0) {
						var node = that.getNode($listElem);
						if($listElem.children("span").hasClass("treepicker-expanded")) {
							that.collapse(node, { "maxLevel": 1, "selectedList": false });
							that.element.trigger("nodeCollapsed", [ node ]);
						}
						else {
							that.expand(node, { "maxLevel": 1, "selectedList": false });
							that.element.trigger("nodeExpanded", [ node ]);
						}
					}
					
					return false;
				})
				.on("click", ".treepicker-selector", function(event) {
					event.preventDefault();
					
					var $listElem = $(event.target);
					if(!$listElem.is("li")) {
						$listElem = $listElem.parents("li:first");
					}
					
					if($listElem.length > 0) {
						var node = that.getNode($listElem);
						if(node[that.selectedAttrName]) {
							if(that.options.allowSelectionToggle === true) {
								that.unselect(node, true);
							}
						}
						else {
							that.select(node, true);
						}
					}
					
					return false;
				});
			
			this.selectedList
				.on("click", ".treepicker-expander", function(event) {
					event.preventDefault();
					
					var $listElem = $(event.target);
					if(!$listElem.is("li")) {
						$listElem = $listElem.parents("li:first");
					}
					
					if($listElem.length > 0) {
						var node = that.getNode($listElem, true);
						if($listElem.children("span").hasClass("treepicker-expanded")) {
							that.collapse(node, { "selectedList": true });
							that.element.trigger("nodeCollapsed", [ node ]);
						}
						else {
							that.expand(node, { "maxLevel": 1, "selectedList": true });
							that.element.trigger("nodeExpanded", [ node ]);
						}
					}
					
					return false;
				})
				.on("click", ".treepicker-selector", function(event) {
					event.preventDefault();
					
					var $listElem = $(event.target);
					if(!$listElem.is("li")) {
						$listElem = $listElem.parents("li:first");
					}
					
					if($listElem.length > 0) {
						var node = that.getNode($listElem, true);
						that.unselect(node, true);
					}
					
					return false;
				});
			
			/* Reuseable Closures */
			this._partialSelectNode = function(node, parentNode, conf) {
				node[that.partialSelectedAttrName] = true;
				
				if((!conf) || (conf.deferRender != true)) {
					var $nodeElements = that.getNodeElement(node).children("span");
					$nodeElements.addClass("treepicker-selected-partial").removeClass("treepicker-unselected");
					if(!node[that.selectedAttrName]) {
						$nodeElements.removeClass("treepicker-selected");
					}
				}
			};
			
			this._selectNode = function(node, parentNode, breadcrumbIgnored, conf) {
				node[that.partialSelectedAttrName] = false;
				node[that.selectedAttrName] = true;
				
				if((!conf) || (conf.deferRender != true)) {
					var $nodeElements = that.getNodeElement(node).children("span");
					$nodeElements.removeClass("treepicker-unselected").removeClass("treepicker-selected-partial").addClass("treepicker-selected");
				}
			};
			
			this._partialUnselectNode = function(node, parentNode, conf) {
				if(node[that.selectedAttrName]) {
					delete node[that.selectedAttrName];
					node[that.partialSelectedAttrName] = true;
				}
				
				if((!conf) || (conf.deferRender != true)) {
					var $nodeElements = that.getNodeElement(node).children("span");
					$nodeElements.removeClass("treepicker-unselected").removeClass("treepicker-selected").addClass("treepicker-selected-partial");
				}
			};
			
			this._unselectNode = function(node, parentNode, breadcrumbIgnored, conf) {
				delete node[that.selectedAttrName];
				delete node[that.partialSelectedAttrName];
				
				if((!conf) || (conf.deferRender != true)) {
					var $nodeElements = that.getNodeElement(node).children("span");
					$nodeElements.removeClass("treepicker-selected").removeClass("treepicker-selected-partial").addClass("treepicker-unselected");
				}
			};
		},
		/**
		 * callback's signature: callback(node, parentNode, travelData)
		 * 		- node: current visited node's object.
		 * 		- parentNode: parent object of the "node" (will be null if it's root node).
		 * 		- travelData: additional hash (Object) which the callback can put anything into it (will be automatically initialize if not specified).
		 * 		  This hash is always the same object to make it possible to passing the data between callback invocations.
		 * 
		 * *** Please note that the all callbacks [b]will be[/b] invoked for the startNode because root is not there ! ***
		 * 
		 * @param currentNode
		 * @param callback
		 * @param travelData
		 */
		"travelUp": function(currentNode, callback, travelData, completedCallback) {
			if(typeof callback === "function") {
				if(!travelData) {
					travelData = {};
				}
				
				var continueProcess = true;
				var parentNode, node = ((!currentNode) || (currentNode.isRoot)) ? null : currentNode;
				while(continueProcess && (node !== null)) {
					parentNode = node[this.parentAttrName];
					if((!parentNode) || (parentNode.isRoot)) {
						parentNode = null;
					}
					
					continueProcess = callback(node, parentNode, travelData);
					if(typeof continueProcess === "undefined") {
						continueProcess = true;
					}
					
					node = parentNode;
				}
			}
			
			if(typeof completedCallback === "function") {
				completedCallback();
			}
		},
		/**
		 * Breadth-First Search tree traversal. You can provide a "callback" function which will be invoke each time a node is visited.
		 * 
		 * Note that the "breadcrumb" for callbacks of this function is not actually computed during traversal. But it is computed using "_siblingOrder" attribute of the node.
		 * This should not be troubles for other function rather than "_prepareNode" which is the one that create "_siblingOrder". And of course, there are overhead for doing this.
		 * 
		 * *** Please note that the all callbacks will not be invoked for the startNode because root is not there ! ***
		 * 
		 * @param startNode
		 * @param callback callback function that get executed when the node is visited.
		 * 		callback's signature: callback(node, parentNode, breadcrumb, travelData)
		 * 		- node: current visited node's object.
		 * 		- parentNode: parent object of the "node" (will be null if it's root node).
		 * 		- breadcrumb: an array containing every parent node index that get traversed until the "node".
		 * 				For example, [ 0, 3, 2 ] means the current "node" is at depth "2" (3rd level) and is at child index 2 (3rd child) of the "parentNode".
		 * 		- travelData: additional hash (Object) which the callback can put anything into it (will be automatically initialize if not specified).
		 * 		  This hash is always the same object to make it possible to passing the data between callback invocations.
		 * @param travelData
		 */
		"travelBFS": function(startNode, callback, travelData, completedCallback) {
			if(typeof callback === "function") {
				if(!travelData) {
					travelData = {};
				}
				
				// If startNode is null, use root as start node.
				if((!startNode) || (startNode.isRoot)) {
					startNode = [ this.rootNode ];
				}
				else if(!Array.isArray(startNode)) {
					startNode = [ startNode ];
				}
				
				var procRoot, procChilds, idx = -1, len = startNode.length;
				while(++idx < len) {
					procRoot = startNode[idx];
					procChilds = procRoot[this.childAttrName];
					
					var procDataQueue = [];
					if(procChilds && (procChilds.length > 0)) {
						// Push parent node and its' breadcrumb into the queue for procesing
						procDataQueue.push({
							"parentNode": procRoot,
							"parentBreadcrumb": this._createBreadcrumb(procRoot)
						});
					}
					
					// Process nodes in queue
					while(procDataQueue.length > 0) {
						var procData = procDataQueue.shift();
						var parentNode = procData.parentNode;
						var breadcrumb = procData.parentBreadcrumb;
						var level = breadcrumb.length;
						
						var childsList = parentNode[this.childAttrName];
						var childsIdx = -1, childsLen = (!childsList) ? 0 : childsList.length;
						while(++childsIdx < childsLen) {
							var node = childsList[childsIdx];
							breadcrumb[level] = childsIdx;
							
							var continueToChild = callback(node, (parentNode.isRoot) ? null : parentNode, breadcrumb, travelData);
							if(typeof continueToChild === "undefined") {
								continueToChild = true;
							}
							
							var grandChildsList = node[this.childAttrName];
							if(continueToChild && grandChildsList && (grandChildsList.length > 0)) {
								procDataQueue.push({
									"parentNode": node,
									"parentBreadcrumb": breadcrumb.slice(0)
								});
							}
						}
					}
				}
			}
			
			if(typeof completedCallback === "function") {
				completedCallback();
			}
		},
		"_createBreadcrumb": function(startNode) {
			var breadcrumb = [];
			if((startNode) && (!startNode.isRoot)) {
				this.travelUp(startNode, function(node, parentNode, travelData) {
					breadcrumb.unshift(node._siblingOrder);
				});
			}
			
			return breadcrumb;
		},
		/**
		 * *** Please note that the all callbacks will not be invoked for the startNode because root is not there ! ***
		 * 
		 * @param startNode
		 * @param downCallback callback function that get executed when the node is visited while traveling down to its' children.
		 * 		callback's signature: callback(node, parentNode, breadcrumb, travelData)
		 * 		- node: current visited node's object.
		 * 		- parentNode: parent object of the "node" (will be null if it's root node).
		 * 		- breadcrumb: an array containing every parent node index that get traversed until the "node".
		 * 				For example, [ 0, 3, 2 ] means the current "node" is at depth "2" (3rd level) and is at child index 2 (3rd child) of the "parentNode".
		 * 		- travelData: additional hash (Object) which the callback can put anything into it (will be automatically initialize if not specified).
		 * 		  This hash is always the same object to make it possible to passing the data between callback invocations.
		 * @param upCallback callback function that get executed when the node is visited while traveling back up to its' parent
		 * 		callback's signature: callback(node, parentNode, breadcrumb, travelData)
		 * 		- node: current visited node's object.
		 * 		- parentNode: parent object of the "node" (will be null if it's root node).
		 * 		- breadcrumb: an array containing every parent node index that get traversed until the "node".
		 * 				For example, [ 0, 3, 2 ] means the current "node" is at depth "2" (3rd level) and is at child index 2 (3rd child) of the "parentNode".
		 * 		- travelData: additional hash (Object) which the callback can put anything into it (will be automatically initialize if not specified).
		 * 		  This hash is always the same object to make it possible to passing the data between callback invocations.
		 * @param travelData
		 */
		"travelDFS": function(startNode, downCallback, upCallback, travelData, completedCallback) {
			var hasDownCallback = (typeof downCallback === "function");
			var hasUpCallback = (typeof upCallback === "function");
			if(hasDownCallback || hasUpCallback) {
				if(!travelData) {
					travelData = {};
				}
				
				if(startNode === null) {
					startNode = [ this.rootNode ];
				}
				else if(!Array.isArray(startNode)) {
					startNode = [ startNode ];
				}
				
				var idx = -1, len = startNode.length;
				while(++idx < len) {
					var procRoot = startNode[idx];
					var breadcrumb = [], procDataStack = [];
					
					var procChilds = procRoot[this.childAttrName];
					if(procChilds && (procChilds.length > 0)) {
						breadcrumb = this._createBreadcrumb(procRoot);
						breadcrumb.push(-1);
						procDataStack.push({
							"parentNode": procRoot
						});
					}
					
					while(procDataStack.length > 0) {
						var procData = procDataStack[procDataStack.length - 1];
						var parentNode = procData.parentNode;
						var nodesList = parentNode[this.childAttrName];
						
						var level = breadcrumb.length - 1;
						var order = breadcrumb[level] = breadcrumb[level] + 1;
						if(order >= nodesList.length) {
							procDataStack.pop();
							breadcrumb.pop();
							
							if(procDataStack.length > 0) {
								if(hasUpCallback) {
									var grandParentNode = procDataStack[level - 1].parentNode;
									upCallback(parentNode, (grandParentNode.isRoot) ? null : grandParentNode, breadcrumb, travelData);
								}
							}
						}
						else {
							var node = nodesList[order];
							if(hasDownCallback) {
								continueTraversal = downCallback(node, (parentNode.isRoot) ? null : parentNode, breadcrumb, travelData);
								if(typeof continueTraversal === "undefined") {
									continueTraversal = true;
								}
							}
							
							var childsList = node[this.childAttrName];
							if(continueTraversal && childsList && (childsList.length > 0)) {
								breadcrumb.push(-1);
								procDataStack.push({
									"parentNode": node
								});
							}
							else {
								if(hasUpCallback) {
									upCallback(node, (parentNode.isRoot) ? null : parentNode, breadcrumb, travelData);
								}
							}
						}
					}
				}
			}
			
			if(typeof completedCallback === "function") {
				completedCallback();
			}
		},
		/**
		 * Turn anonymous selector which could be node, array of node into selector function.
		 * If the specified selector is already a function, this simply return that selector.
		 * 
		 * @param selector
		 * @returns
		 */
		"_createSelector": function(selector) {
			var selectorFn = null;
			if((typeof selector !== "undefined") && (selector !== null)) {
				if(typeof selector === "function") {
					selectorFn = selector;
				}
				else {
					var nodesHash = null;
					if(!Array.isArray(selector)) {
						nodesHash = {};
						nodesHash[selector._nodeId] = selector;
					}
					else if(selector.length > 0) {
						nodesHash = {};
						
						var targetNode, idx = -1, len = selector.length;
						while(++idx < len) {
							targetNode = selector[idx];
							nodesHash[targetNode._nodeId] = targetNode;
						}
					}
					
					if(nodesHash === null) {
						selectorFn = function(node) {
							return true;
						};
					}
					else {
						selectorFn = function(node) {
							var result = false;
							if(nodesHash[node._nodeId]) {
								result = true;
							}
							
							return result;
						};
					}
				}
			}
			
			return selectorFn;
		},
		/**
		 * Retrieve the node object using node's html element rendered on screen.
		 * 
		 * @param element
		 * @param isSelectedList
		 * @returns
		 */
		"getNode": function(element, isSelectedList) {
			var result = null;
			
			var elemName;
			var $listElement = $(element);
			if($listElement.length > 0) {
				elemName = $listElement.attr("name");
				if(elemName !== null) {
					result = this.searchByBreadcrumb(this.rootNode, elemName.substring(5).split("_"));
				}
			}
			
			return result;
		},
		/**
		 * Retrieve node's html element rendered on screen from node object.
		 * 
		 * @param node
		 * @param isSelectedList
		 * @returns
		 */
		"getNodeElement": function(node, isSelectedList) {
			var $element = $();
			
			var isRoot = (!node) || node.isRoot;
			var isBothList = (typeof isSelectedList === "undefined") || (isSelectedList === null);
			
			if(isBothList || (isSelectedList == false)) {
				$element = $element.add((isRoot) ? this.fullList : $("#" + node._elementIdFull));
			}
			
			if(isBothList || (isSelectedList == true)) {
				$element = $element.add((isRoot) ? this.selectedList : $("#" + node._elementIdSelected));
			}
			
			return $element;
		},
		/**
		 * 
		 * 
		 * @param value
		 * @returns
		 */
		"_createSearchByValueFunction": function(value) {
			var resultFn = null;
			
			var nodeValueFn = this.options.nodeValue;
			if(typeof nodeValueFn !== "function") {
				throw "IllegalConfiguration: Could not resolve node value with out 'nodeValue' function!";
			}
			
			var rootValue = this.options.rootValue;
			var searchHash = {};
			
			if(!Array.isArray(value)) {
				value = [ value ];
			}
			
			{
				var idx = -1, len = value.length;
				while(++idx < len) {
					searchHash[value[idx]] = true;
				}
			}
			
			resultFn = function(node) {
				var result = false;
				
				var nodeValue;
				if(node.isRoot) {
					nodeValue = rootValue;
				}
				else {
					nodeValue = nodeValueFn(node);
				}
				
				if((typeof nodeValue !== "unspecified") && (searchHash[nodeValue])) {
					result = true;
				}
				
				return result;
			};
			
			return resultFn;
		},
		"_findUpdatedNodes": function(selector, select, propagateUp, propagateDown) {
			var that = this;
			var updatedNodes = {};
			if(selector === null) {
				// Select root only, no need to check for the actual updated nodes
				updatedNodes[this.rootNode._nodeId] = this.rootNode;
			}
			else {
				selector = this._createSelector(selector);
				
				var findEndOfBubble = function(node, parentNode) {
					// Searching for the actual selected nodes. If all siblings are selected, the actual selected node is the parent node.
					var duplicateUpdates = [];
					
					if(parentNode == null) {
						parentNode = that.rootNode;
					}
					
					var childsList = parentNode[that.childAttrName];
					var childNode, childIdx = -1, childsLen = childsList.length;
					var allTheSame = true;
					while((allTheSame) && (++childIdx < childsLen)) {
						childNode = childsList[childIdx];
						
						var isSelected = childNode[that.selectedAttrName];
						var isPartialSelected = childNode[that.partialSelectedAttrName];
						allTheSame = allTheSame && ((childNode === node) || (select && isSelected) || (!select && (!isSelected && !isPartialSelected)));
						
						if(typeof updatedNodes[childNode._nodeId] !== "undefined") {
							duplicateUpdates.push(childNode._nodeId);
						}
					}
					
					if(allTheSame) {
						if(parentNode.isRoot) {
							updatedNodes[parentNode._nodeId] = parentNode;
						}
					}
					else {
						updatedNodes[node._nodeId] = node;
						
						childsIdx = -1, childsLen = duplicateUpdates.length;
						while(++childsIdx < childsLen) {
							delete updatedNodes[duplicateUpdates[childsIdx]];
						}
					}
					
					return allTheSame;
				};
				
				var rootSelected = (selector(this.rootNode) === true);
				if(rootSelected) {
					updatedNodes[this.rootNode._nodeId] = this.rootNode;
				}
				
				if((!rootSelected) && (propagateDown)) {
					// Propagate Selection
					this.travelBFS(null, function(node, parentNode) {
						// Parent always get processed before its childrens. So there are no worries about select child under the selected parent !
						var continueTravel = (node._disabled !== true) && (select && !node[that.selectedAttrName]) || ((!select) && (node[that.selectedAttrName] || node[that.partialSelectedAttrName]));
						if(continueTravel) {
							if(selector(node) === true) {
								if(propagateUp) {
									that.travelUp(node, findEndOfBubble);
								}
								else {
									updatedNodes[node._nodeId] = node;
								}
								
								continueTravel = !propagateDown;
							}
						}
						
						return continueTravel;
					});
				}
			}
			
			return updatedNodes;
		},
		"redraw": function() {
			var ref = this._startExecution();
			
			this.fullList.empty();
			this.expand(this.rootNode, { "selectedList": false, "maxLevel": 1 });
			
			this.selectedList.empty();
			this.expand(this.rootNode, { "selectedList": true, "maxLevel": 1 });
			
			if(this.isRootSelected()) {
				this.rootSelector.removeClass("treepicker-unselected").addClass("treepicker-selected");
			}
			else {
				this.rootSelector.removeClass("treepicker-selected").addClass("treepicker-unselected");
			}
			
			this._endExecution(ref);
		},
		/**
		 * Select nodes using their values.
		 * 
		 * @param value
		 */
		"selectByValues": function(value, conf) {
			this.select(this._createSearchByValueFunction(value), conf);
		},
		"select": function(selector, conf) {
			var ref = this._startExecution();
			
			var propagateUp = this.options.propagateSelectionUp;
			var propagateDown = this.options.propagateSelectionDown;
			
			var updatedNodes = this._findUpdatedNodes(selector, true, propagateUp, propagateDown);
			
			// Apply selction to the nodes...
			var node;
			for(var nodeId in updatedNodes) {
				node = updatedNodes[nodeId];
				
				this.collapse(node);
				// Go up the tree structure to set childSelectedFlag
				this.travelUp(node, this._partialSelectNode, conf);
				this._selectNode(node, node[this.parentAttrName], null, conf);
				if(propagateDown) {
					// Propagate selection down the tree structure
					this.travelBFS(node, this._selectNode, conf);
				}
				
				var expandConf = {
					"selectedList": true
				};
				
				if((!conf) || (conf.deferRender != true)) {
					if(node.isRoot) {
						this.collapse(null, expandConf);
						expandConf["maxLevel"] = 1;
						this.expand(null, expandConf);
						
						this.element.trigger("nodeSelected", [ null ]);
					}
					else {
						this.collapse(node, expandConf);
						this.expandTo(node, expandConf);
						
						this.element.trigger("nodeSelected", [ node ]);
					}
				}
				
				this.element.trigger("nodeSelected", [ ((node.isRoot) ? null : node) ]);
			}
			
			if((!conf) || (conf.deferRender != true)) {
				if(this.isRootSelected()) {
					this.rootSelector.removeClass("treepicker-unselected").addClass("treepicker-selected");
				}
				else {
					this.rootSelector.removeClass("treepicker-selected").addClass("treepicker-unselected");
				}
			}
			
			this._endExecution(ref);
		},
		"unselectByValues": function(value, conf) {
			this.unselect(this._createSearchByValueFunction(value), conf);
		},
		"unselect": function(selector, conf) {
			var ref = this._startExecution();
			
			var propagateUp = this.options.propagateUnselectionUp;
			var propagateDown = this.options.propagateUnselectionDown;
			
			var updatedNodes = this._findUpdatedNodes(selector, false, propagateUp, propagateDown);
			
			// Apply selction to the nodes...
			var node;
			for(var nodeId in updatedNodes) {
				node = updatedNodes[nodeId];
				
				if(propagateUp) {
					this.travelUp(node, this._partialUnselectNode, conf);
					
					delete this.rootNode[this.selectedAttrName];
					this.rootNode[this.partialSelectedAttrName] = true;
				}
				
				this._unselectNode(node, node[this.parentAttrName], null, conf);
				if(propagateDown) {
					// Propagate selection down the tree structure
					this.travelBFS(node, this._unselectNode, conf);
				}
				
				if((!conf) || (conf.deferRender != true)) {
					if(node.isRoot) {
						this.selectedList.html("&nbsp;");
						
						var that = this;
						window.setTimeout(function() { that.selectedList.empty(); }, 1);
					}
					else {
						this.getNodeElement(node, true).remove();
					}
				}
				
				this.element.trigger("nodeUnselected", [ ((node.isRoot) ? null : node) ]);
			}
			
			if((!conf) || (conf.deferRender != true)) {
				if(this.isRootSelected()) {
					this.rootSelector.removeClass("treepicker-unselected").addClass("treepicker-selected");
				}
				else {
					this.rootSelector.removeClass("treepicker-selected").addClass("treepicker-unselected");
				}
			}
			
			this._endExecution(ref);
		},
		"resetSelection": function(conf) {
			this.unselect(null, conf);
		},
		"searchByBreadcrumb": function(root, breadcrumb) {
			if(!root) {
				root = this.rootNode;
			}
			
			var node = null;
			
			var current = root;
			var currentChild = current.children;
			
			var bcIdx = 0, bcLen = breadcrumb.length;
			while((currentChild) && (currentChild.length > 0) && (bcIdx < bcLen)) {
				current = currentChild[breadcrumb[bcIdx]];
				currentChild = current[this.childAttrName];
				++bcIdx;
			}
			
			if(bcIdx >= bcLen) {
				node = current;
			}
			
			return node;
		},
		/**
		 * Add new nodes to a parent node. If the parent node is not specified, these new nodes will be added to root node.
		 * 
		 * @param parentNode
		 * @param childNode
		 */
		"add": function(parentNode, childNode, conf) {
			if(childNode) {
				var ref = this._startExecution();
				
				// Preparing container node and breadcrumb
				var containerNode, breadcrumb;
				if(parentNode && (!parentNode.isRoot)) {
					containerNode = parentNode;
					breadcrumb = this._createBreadcrumb(parentNode);
				}
				else {
					parentNode = null;
					containerNode = this.rootNode;
					breadcrumb = [];
				}
				
				if(!Array.isArray(childNode)) {
					childNode = [ childNode ];
				}
				
				var drawConf = {};
				var currChilds = containerNode[this.childAttrName];
				var newIdx = currChilds.length;
				var insertToLv = breadcrumb.length;
				
				var newNode, idx = 0, len = childNode.length;
				
				var that = this;
				
				// Extract selected nodes for selection after the add-node-process completed
				var selectedNodes = [];
				var prepareNode = function(_node, _parentNode, _breadcrumb, travelData) {
					if(_node[that.selectedAttrName] === true) {
						selectedNodes.push(_node);
						delete _node[that.selectedAttrName];
					}
					
					return that._prepareNode(_node, _parentNode, _breadcrumb, travelData);
				};
				
				// Prepare first level new nodes and travel to all its' childrens
				var drawBuffer = [];
				while(idx < len) {
					newNode = childNode[idx];
					
					breadcrumb[insertToLv] = newIdx;
					currChilds[newIdx] = newNode;
					
					prepareNode(newNode, parentNode, breadcrumb, drawConf);
					this.travelDFS(newNode, prepareNode);
					if(((!conf) || (conf.deferRender != true)) && containerNode.isRoot && (!newNode._disabled)) {
						this._drawNode(newNode, null, breadcrumb, drawConf);
						this._s_completeNodeView(newNode, drawConf);
						if(newNode._view) {
							drawBuffer.push(newNode._view);
							delete newNode._view;
						}
					}
					
					++idx;
					++newIdx;
				}
				
				if(drawBuffer.length > 0) {
					this.fullList.append(drawBuffer.join(""));
				}
				
				this._endExecution(ref);
				
				// Perform nodes selection
				if(selectedNodes.length > 0) {
					this.select(selectedNodes, conf);
				}
			}
		},
		/**
		 * Remove nodes using "selector" which could be a node, an array of nodes, or a function return "true" to indicate that the node should be removed.
		 * 
		 * @param selector a node object, an array of node objects, or function with the following signature,
		 * "boolean callback(nodeObject)" (return "true" means that the node should be removed).
		 */
		"remove": function(selector) {
			selector = this._createSelector(selector);
			if(selector === null) {
				// Remove everything !
				this.selectedList.empty();
				this.fullList.empty();
				this.rootNode[this.childAttrName] = [];
			}
			else {
				var that = this;
				this.travelBFS(null, function(node, parentNode) {
					if(parentNode === null) {
						parentNode = that.rootNode;
					}
					
					var continueTravel = true;
					if(selector(node) === true) {
						continueTravel = false;
						
						that.getNodeElement(node).remove();
						
						var siblingList = parentNode[that.childAttrName];
						// Detach node from parent node
						delete node[that.parentAttrName];
						siblingList.splice(order, 1);
						
						// Update sibling order
						var siblingIdx = order - 1, siblingLen = siblingList.length;
						while(++siblingIdx < siblingLen) {
							siblingList[siblingIdx]._siblingOrder = siblingIdx;
						}
					}
					
					return continueTravel;
				});
			}
		},
		"clear": function() {
			this.remove(null);
		},
		/**
		 * Expand the specified node. If the specified node is null, this function will redraw all nodes
		 * 
		 * @param node
		 * @param conf
		 */
		"expand": function(node, conf) {
			var ref = this._startExecution();
			
			if(!node) {
				node = this.rootNode;
			}
			
			var buffer = [];
			var childsList = node[this.childAttrName];
			if(childsList) {
				var that = this;
				var drawNode = function(node, parentNode, breadcrumb, drawConf) {
					return that._drawNode(node, parentNode, breadcrumb, drawConf);
				};
				
				var attachToParent = function(node, parentNode, breadcrumb, drawConf) {
					return that._attachToParent(node, parentNode, breadcrumb, drawConf);
				};
				
				this.travelDFS(node, drawNode, attachToParent, conf);
				
				var childNode, childsIdx = -1, childsLen = childsList.length;
				while(++childsIdx < childsLen) {
					childNode = childsList[childsIdx];
					if(childNode._view) {
						buffer.push(childNode._view);
						delete childNode._view;
					}
				}
			}
			
			if(buffer.length > 0) {
				var $container = this.getNodeElement(node, conf.selectedList);
				if(node.isRoot) {
					$container.append(buffer.join(""));
				}
				else {
					$container.children("ul").append(buffer.join(""));
					$container.children("span").addClass("treepicker-expanded");
				}
			}
			
			this._endExecution(ref);
		},
		/*
		 * callback's signature: callback(node, parentNode, travelData)
		 * 		- node: current visited node's object.
		 * 		- parentNode: parent object of the "node" (will be null if it's root node).
		 * 		- travelData: additional hash (Object) which the callback can put anything into it (will be automatically initialize if not specified).
		 * 		  This hash is always the same object to make it possible to passing the data between callback invocations.
		 */
		"expandTo": function(targetNode, conf) {
			if(targetNode && (targetNode._disabled !== true)) {
				var ref = this._startExecution();
				
				// Fake the breadcrumb for "travelUp" traversal
				var targetBreadcrumb = this._createBreadcrumb(targetNode);
				
				var that = this;
				var drawAndAttach = function(node, parentNode, drawConf) {
					targetBreadcrumb.pop(); // Still fake !
					
					var continueTravel = true;
					if((parentNode === null) || (parentNode.isRoot)) {
						parentNode = that.rootNode;
						
						that._s_completeNodeView(node, conf);
						that.getNodeElement(parentNode, conf.selectedList).append(node._view);
						delete node._view;
						
						continueTravel = false;
					}
					else {
						var $parentElem = that.getNodeElement(parentNode, conf.selectedList);
						if($parentElem.length > 0) {
							continueTravel = false;
							
							that._s_completeNodeView(node, conf);
							$parentElem.children("ul").append(node._view);
							$parentElem.addClass("treepicker-expanded");
							delete node._view;
						}
						else {
							// Draw Parent on the fly
							that._drawNode(parentNode, parentNode[that.parentAttrName], targetBreadcrumb, drawConf);
							that._attachToParent(node, parentNode, targetBreadcrumb, drawConf);
						}
					}
					
					// Order the expanded node
					if(!continueTravel) {						
						that._correctNodeOrder(node, parentNode, conf);
					}
					
					return continueTravel;
				};
				
				this._drawNode(targetNode, targetNode[this.parentAttrName], targetBreadcrumb, conf);
				this.travelUp(targetNode, drawAndAttach, conf);
				
				this._endExecution(ref);
			}
		},
		"collapse": function(node, conf) {
			if((!node) || (node._disabled !== true)) {
				var ref = this._startExecution();
				
				if(!conf) {
					conf = {};
				}
				
				if(node && (!node.isRoot)) {
					this._detachChild(node, conf);
				}
				else {
					var childsList = this.rootNode[this.childAttrName], idx = -1, len = childsList.length;
					while(++idx < len) {
						this._detachChild(childsList[idx], conf);
					}
				}
				
				this._endExecution(ref);
			}
		},
		"_createIndentSpan": function(breadcrumb) {
			var width = 0;
			if(breadcrumb.length > 0) {
				width = (breadcrumb.length - 1) * this.levelIndent;
			}
			
			return (width <= 0) ? "" : "<img src='" + this.options.spacerImagePath + "' border='0' style='height: 1px; width: " + width + "px;'/>";
		},
		"_drawNode": function(node, parentNode, breadcrumb, conf) {
			var continueProcess = (node._disabled !== true) && ((node._active !== false) || (conf.selectedList === true));
			if(continueProcess) {
				continueProcess = (typeof conf.drawFilter !== "function") || (conf.drawFilter(node));
			}

			if(continueProcess) {
				if((conf.selectedList !== true) || (node[this.selectedAttrName] === true) || (node[this.partialSelectedAttrName])) {
					var $nodeElement = this.getNodeElement(node, conf.selectedList);
					if($nodeElement.length > 0) {
						node._view = $nodeElement;
					}
					else {
						node._view = [];
						node._viewContainerClasses = [ "treepicker-node" ];
						node._viewChilds = [];
						
						node._view[0] = "<li id='";
						node._view[1] = (conf.selectedList == true) ? node._elementIdSelected : node._elementIdFull;
						node._view[2] = "' name='";
						node._view[3] = node._nodeId;
						node._view[4] = "'>";
						
						node._view[5] = "<span class='";
						node._view[6] = "";
						node._view[7] = "'>";
						
						if(this.levelIndent > 0) {
							node._view.push(this._createIndentSpan(breadcrumb));
						}
						
						conf.breadcrumb = breadcrumb;
						conf.showExpander = false;
						conf.showSelector = true;
						
						var nodeChilds = node[this.childAttrName];
						var hasChilds = false;
						if(nodeChilds) {
							var idx = nodeChilds.length;
							while((!hasChilds) && (--idx >= 0)) {
								if(nodeChilds[idx]._disabled !== true) {
									hasChilds = true;
								}
							}
						}
						
						if(hasChilds && (!conf.hideToggler)) {
							node._viewContainerClasses.push("treepicker-parent");
							conf.showExpander = true;
						}

						if(node[this.selectedAttrName]) {
							node._viewContainerClasses.push("treepicker-selected");
						}
						else if(node[this.partialSelectedAttrName]) {
							node._viewContainerClasses.push("treepicker-selected-partial");
						}
						else {
							node._viewContainerClasses.push("treepicker-unselected");
						}
						
						this.options.drawNode(node, node._view, node._viewContainerClasses, conf);
						
						node._view.push("</span>");
						node._view.push("<ul>");
					}
				}

				if(conf.maxLevel && (breadcrumb.length >= conf.maxLevel)) {
					continueProcess = false;
				}
			}
			
			return continueProcess;
		},
		"_correctNodeOrder": function(node, parentNode, conf) {
			var youngerIdx = node._siblingOrder;
			var siblingsList = parentNode[this.childAttrName];
			
			var $younger = null;
			while((($younger === null) || ($younger.length <= 0)) && (++youngerIdx < siblingsList.length)) {
				$younger = this.getNodeElement(siblingsList[youngerIdx], conf.selectedList);
			}
			
			if(($younger !== null) && ($younger.length > 0)) {
				this.getNodeElement(node, conf.selectedList).insertBefore($younger);
			}
		},
		"_attachToParent": function(node, parentNode, breadcrumb, conf) {
			this._s_completeNodeView(node, conf);
			
			var continueProcess = (node._disabled !== true) && ((node._active !== false) || (conf.selectedList === true));
			if(continueProcess) {
				continueProcess = (typeof conf.drawFilter !== "function") || (conf.drawFilter(node));
			}
			
			if(continueProcess) {
				if((parentNode && parentNode._viewChilds) && ((conf.selectedList !== true) || (node[this.selectedAttrName] === true) || (node[this.partialSelectedAttrName]))) {
					parentNode._viewChilds.push(node._view);
					delete node._view;
				}
			}

			return true;
		},
		"_s_completeNodeView": function(node, conf) {
			if(node._view) {
				if(!Array.isArray(node._view)) {
					node._view.remove();
					node._view = $("<div>").append(node._view).html();
				}
				else {
					if(node._viewChilds && (node._viewChilds.length > 0)) {
						node._view.push(node._viewChilds.join(""));
						node._viewContainerClasses.push("treepicker-expanded");
					}
					
					node._view.push("</ul>");
					node._view.push("</li>");
					node._view[6] = node._viewContainerClasses.join(" ");
					
					node._view = node._view.join("");
					
					delete node._viewContainerClasses;
					delete node._viewChilds;
				}
			}
		},
		"_detachChild": function(node, conf) {
			var $node = this.getNodeElement(node, conf.selectedList);
			if(node.isRoot) {
				$node.html("");
			}
			else {
				$node.children("ul").html("");
				$node.children("span").removeClass("treepicker-expanded");
			}

			return true;
		},
		"_prepareNode": function(node, parentNode, breadcrumb) {
			var brotherIdx = breadcrumb[breadcrumb.length - 1];
			node._siblingOrder = brotherIdx;
			if(parentNode) {
				node[this.parentAttrName] = parentNode;
				
				var siblingsList = parentNode[this.childAttrName];
				var nodeId = null;
				if(brotherIdx < siblingsList.length) {
					nodeId = siblingsList[brotherIdx]._siblingOrder;
				}
				
				if(!nodeId) {
					nodeId = brotherIdx;
				}
				
				node._nodeId = parentNode._nodeId + "_" + nodeId;
			}
			else {
				// First level condition
				var nodeId = this.rootNode[this.childAttrName][brotherIdx]._siblingOrder;
				if(!nodeId) {
					nodeId = brotherIdx;
				}
				
				node._nodeId = "node_" + nodeId;
			}
			
			node._elementIdFull = this.id + "_full_" + node._nodeId;
			node._elementIdSelected = this.id + "_selected_" + node._nodeId;

			return true;
		},
		"maxDisplayLevel": function() {
			var result = -1;
			var nodesCount = 0;
			var deepestLevel = -1;

			var maxNodesCount = this.options.nodesDisplayThreshold;
			if(maxNodesCount <= 0) {
				result = this.options.levelDisplayThreshold;
			}
			else {
				this.travelBFS(null, function(node, parentNode, level) {
					++nodesCount;
					if(nodesCount <= maxNodesCount) {
						deepestLevel = Math.max(deepestLevel, level);
					}
					else {
						if((result === -1) && (deepestLevel === level)) {
							--deepestLevel;
							result = deepestLevel;
						}
						
						return false;
					}
				});
				
				if(result === -1) {
					result = deepestLevel;
				}
				
				++result;
				result = Math.max(result, this.options.levelDisplayThreshold);
			}

			if(result <= 0) {
				result = 1;
			}
			
			return result;
		},
		"filter": function(filterFn) {
			var activeNodes = [];
			this.travelDFS(null, function(node, parentNode, breadcrumb) {
				node._active = false;
				if(filterFn(node)) {
					activeNodes.push(node);
				}
			});

			var node, idx = -1, len = activeNodes.length;
			while(++idx < len) {
				node = activeNodes[idx];
				this.travelUp(node, function(node) {
					node._active = true;
				});
			}

			this.fullList.empty();
			this.expand(null, { "maxLevel": 1, "selectedList": false });
			
			this.element.trigger("treeFiltered");
		},
		"getSelectedNodes": function() {
			var result = [];
			
			var propagate = this.options.propagateSelectionUp || this.options.propagateUnselectionDown;
			if(!propagate) {
				this.travelBFS(null, function(node) {
					var selected = false;
					if((node._disabled !== true) && (node[selectedAttrName] === true)) {
						result.push(node);
						selected = true;
					}

					return !(propagate && selected);
				});
			}

			return result;
		},
		"getSelectedValues": function() {
			var result = [];
			var valueRetriever = this.options.nodeValue;
			var selectedAttrName = this.selectedAttrName;
			
			var rootSelected = (this.rootNode[selectedAttrName] === true);
			if(rootSelected) {
				if(typeof this.options.rootValue !== "undefined") {
					result.push(this.options.rootValue);
				}
				else {
					var childList = this.rootNode[this.childAttrName];
					var idx = -1, len = childList.length;
					while(++idx < len) {
						result.push(valueRetriever(childList[idx]));
					}
				}
			}
			
			var propagate = this.options.propagateSelectionUp || this.options.propagateUnselectionDown;
			if(!propagate || !rootSelected) {
				this.travelBFS(null, function(node) {
					var selected = false;
					if((node._disabled !== true) && (node[selectedAttrName] === true)) {
						result.push(valueRetriever(node));
						selected = true;
					}

					return !(propagate && selected);
				});
			}

			return result;
		},
		"isRootSelected": function() {
			return (this.rootNode[this.selectedAttrName] === true);
		},
		"getRootValue": function() {
			return this.options.rootValue;
		},
		"disableNodes": function(selector, conf) {
			selector = this._createSelector(selector);
			var that = this;
			this.travelDFS(null,
				function(node, parentNode, breadcrumb, travelData) {
					if(selector(node, travelData)) {
						node._disabled = true;
						delete node[that.selectedAttributeName];
					}
					else {
						delete node._disabled;
					}
				},
				null,
				conf
			);
			
			if((!conf) || (conf.deferRender != true)) {
				this.redraw();
			}
		},
		"_startExecution": function() {
			// This is apparenlty surely not work because javascript already took all of the available processing time.
			return new RequestMonitor({
				"delays": [ 1500, 3500, 30000 ]
			});
		},
		"_endExecution": function(reqMntr) {
			if(reqMntr) {
				reqMntr.reset();
			}
		}
	});
}(jQuery));