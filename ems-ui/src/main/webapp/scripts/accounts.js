function reloadPage(event, context) {
	var actionConf = (context) ? context.actionConf : null,
		cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["itemID", "pgActn"]),
		newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry;
	
	if((actionConf !== null && typeof actionConf.name !== "undefined") && ((actionConf.name === "delete") || (actionConf.name === "deleteAll"))) { 
		window.setTimeout(function() {
			window.location = newLocation;
		}, 1200); }
	else {
		window.setTimeout(function() {
			window.location.reload();
		}, 1200); }
}

var blockedActions = {
	"add": true,
	"edit": true,
	"import": true
};

var accountsImportActionStatus = {
		"status" : false
};

function popAccountAction($this, accntID, action, stateObj){
	if (stateObj === null || (accntID === "new" && action === "display")) {
		if (accountsImportActionStatus.status) {
			accountsImportActionStatus.status = false;
			$this.crudpanel("hideActivePanelUpload", true);
		} else {
			$this.crudpanel("hideActivePanel", true);
		}
	} else {
		$this.crudpanel("showPanel", action, {
			"id" : accntID
		});
	}
}

/* Scripts for Cosumer Accounts Section */
function cosumerAccounts(accntID, pageEvent) {
	/* Map Messages */
	msgResolver.mapAllAttributes({
		"firstName": "#formFirstName",
		"lastName": "#formLastName",
		"emailAddress": "#formEmail",
		"description": "#formDescription"
	});
	
	/* Setup Shrinkable */
	$(".layout1")
		.shrinkable();
	
	/* Set up Consumer's CRUD Panel */
	var $consumerPanel = $("#consumersPanel")
		.crudpanel({
			"viewURL": "/secure/accounts/viewConsumerAccountDetails.html?consumerId=[%= id %]",
			"viewRender": prepareConsumerDetails,
			"formRender": prepareConsumerForm,
			"saveURL": "/secure/accounts/saveConsumerAccounts.html",
			"deleteURL": "/secure/accounts/deleteConsumer.html?consumerId=[%= id %]",
			"itemName": CONSUMER_LABEL.consumerAccount,
			"idExtractor": retrieveId,
			"postTokenSelector": "#postToken",
			"itemSelector": "#consumersListContainer li *:not(a)"
		})
		.on("beforeShowPanel", function(event, actionName) {
			if(blockedActions[actionName]) {
				testFormEdit($("#consumerForm"));
			}
		})
		.on("showPanel", function(event, actionName) {
			$("#formFirstName").trigger('focus');
		})
		.on("detachedPanel", function(event) {
			$("#mainContent").removeClass("edit");
		});

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popAccountAction($("#consumersPanel"), tempItemID, tempEvent, stateObj); }
 
if(!popStateEvent && (accntID !== undefined && accntID !== '')){ popAccountAction($("#consumersPanel"), accntID, pageEvent); }
///////////////////////////////////////////////////
	
	/* Set up Consumer's pagination and filtering */
	var $consumerListContainer = $("#consumersListContainer")
		.paginatetab({
			"url": "/secure/accounts/consumerAccountsPage/index.html",
			"formSelector": "#consumerFilterForm",
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": unescape($("#consumerTemplate").html()),
			"responseExtractor": extractConsumersList,
			"itemLocatorTemplate": "#consumer_[%= id %]",
			"itemLocatorURL": "/secure/accounts/consumerAccountsPage/locateItemPage.html"
		})
		.paginatetab("addRawData", $("#consumersListData").text())
		.paginatetab("setupSortingMenu", "#consumersListContainerHeader");
	
	scrollListWidth(["consumersListContainer"]); // Sets column widths based on data in list ADDED BY TODD
	
	var autoSelectFn = function(error, rawResponse, translatedResponse) {
		if(translatedResponse && (translatedResponse.length === 1)) {
			$consumerPanel.crudpanel("showViewPanel", null, { "id": translatedResponse[0].randomConsumerId });
		} else {
			$consumerPanel.crudpanel("hideActivePanel", true);
		}
	};
	
	var consumerFilterFn = function(event) {
		event.preventDefault();
		event.stopPropagation();
		if($("#mainContent").hasClass("edit")) {
			inEditAlert("menu");
		}
		else {
			$consumerListContainer
				.paginatetab("option", "triggerSelector", this)
				.paginatetab("reloadData", autoSelectFn);
		}		
		return false;
	};
	
	$("#consumerFilterForm")
		.on("submit", consumerFilterFn)
		.on("click", ".search", consumerFilterFn);
	
	var $consumerSearchButton = $("#consumerFilterForm .search");
	$("#formFilterValue")
		.autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"remoteFn": consumerAutoComplete,
			"selectByTyping": false
		})
		.on("itemSelected", function(ui, selectedItem) {
			if(selectedItem.obj) {
				// Let the filter function handle item selection.
				$("#formFilterValue").val(selectedItem.searchTerm);
				$consumerListContainer.paginatetab("reloadData", function() {
					$consumerPanel.crudpanel("showViewPanel", null, { "id": selectedItem.obj.randomConsumerId });
				});
			}
		})
		.on("keydown", function(event) {
			if(event.which == 13) {
				$consumerSearchButton.trigger("click");
			}
		})
		.on("keyup", function(event) {
				if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
		});
	
	$("#searchClearBtn").on('click', function(event){ event.preventDefault(); $("#formFilterValue").val(""); $("#formFilterValueLabel").fadeIn().removeClass("active"); $consumerSearchButton.trigger("click"); $(this).hide(); });
	
	setupCouponsSelector();
	var $editCouponContainer = $("#consumerCouponsListContainer");
	
	setupCardsSelector();
	var $editCardContainer = $("#consumerCardsSelectorContainer");
	
	/* Bind events to prepare the Consumer's Form */
	$consumerPanel
		.on("afterSave", function(event, context) {
			if(!context.activeId) {
				reloadPage();
			}
			else {
				$consumerListContainer.paginatetab(
						"reloadPageWithItem",
						{ "id": context.activeId },
						function() {
							$consumerPanel.crudpanel("showViewPanel", null, { "id": context.activeId });
						});
			}
		})
		.on("afterExecute", reloadPage)
		.on("detachedPanel", function(event, panel) {
			$editCouponContainer.ajaxtab("clearData");
			$editCardContainer.ajaxtab("clearData");
		});
}

function setupCouponsSelector() {
	/* Set up Consumer's coupons list */
	$("#consumerCouponsListContainer")
		.ajaxtab({
			"rowTemplate": $("#couponListTemplate").html(),
			"idExtractor": extractRandomId
		});
	
	/* Set up Consumer's coupon pagination and filtering */
	var $searchForm = $("#couponSelectedTabForm");
	var $quickAddText = $("#couponQuickAddText");
	
	var $selectedTabData = $("#formCouponRandomIds");
	var $selectedTab = $("#couponSelectedTab");
	$selectedTab
		.ajaxtab({
			"rowTemplate": $("#couponSelectedTabTemplate").html(),
			"idExtractor": extractRandomId
		});
	
	setupCheckBoxes($selectedTab, $selectedTabData);
	
	$quickAddText.on("keydown", function(event) {
		var continuePropagation = true;
		if(event.which == 13) {
			event.preventDefault();
			event.stopPropagation();
			continuePropagation = false;
			
			var selectedObj = $quickAddText.autoselector("getSelectedObject");
			if(selectedObj && selectedObj.obj) {
				$searchForm.find(".add").trigger("click");
			}
		}
		
		return continuePropagation;
	});
	
	var $quickAddButton = $("#couponSelectedTabForm .add");
	$quickAddText
		.autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"remoteFn": consumerCouponAutoComplete,
			"selectByTyping": false,
			"descTemplate": " [%= discount %]"
		})
		.on("keyup", function(event) {
			if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
		})
		.on("itemSelected", function() {
			$quickAddButton.removeClass("inactive");
		})
		.on("reset", function() {
			$quickAddButton.addClass("inactive");
		});
	
	/* Set up Coupon pagination and filtering */
	var newlySelectedIds = {
		"ids": {}
	};
	var $selectableTab = $("#couponListContainer");
	$selectableTab
		.paginatetab({
			"url": "/secure/accounts/coupons/searchUnusedCoupons.html",
			"formSelector": $searchForm,
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": $("#couponTemplate").html(),
			"responseExtractor": extractConsumerCouponsList,
			"idExtractor": function(rowObj) { return rowObj.randomId; },
			"drawFilter": function(rowObj) {
				return !(newlySelectedIds.ids[rowObj.randomId] === true);
			}
		})
		.paginatetab("setupSortingMenu", "#couponListContainerHeader")
		.on("reloaded", function(event) {
			newlySelectedIds.ids = {};
		});
	
	var $selectableDialog = $("#couponSelectorDialog");
	$selectableDialog
		.dialog({
			title: couponAdvancedSearchMsg,
			classes: { "ui-dialog": "consumerAccountDialog" },
			modal: true,
			resizable: false,
			closeOnEscape: true,
			width: 800,
			maxHeight:  $(window).innerHeight()-150,
			position: {my: "center center-100"},
			hide: "fade",
			autoOpen: false,
			stack: true,
			buttons: {
				done: {
					text: "done",
					click: function() {
						$quickAddText.autoselector("reset");
						$selectableDialog.dialog("close");
					}
				}
			},
			dragStop: function(){ checkDialogPlacement($(this)); }
		})
		.on("dialogopen", function(event, ui) {
			var innerMaxHeight = $(window).innerHeight()-330;
			$("#couponSelectorDialog").find("ul#couponListContainer").css("max-height", innerMaxHeight+"px");
			resizeAutoWidthFilters($selectableDialog);
		});
	
	btnStatus("done");
	
	var $searchText = $selectableDialog.find("#formFilterValue");
	var $searchDiscount = $selectableDialog.find("#formFilterDiscount");
	var $consumer = $searchForm.find("#couponSelectedTabConsumerId");
	var $discount = $searchForm.find("#couponSelectedTabDiscount");
	var $searchSelectedData = $searchForm.find("#couponSelectedTabSelectedIds");
	var addFn = function(obj) {
		var $elements = $selectedTab.ajaxtab("getRowElements", obj.randomId);
		if($elements.length > 0) {
			$elements.find(".checkBox:not(.checked)").trigger("click");
		}
		else {
			$elements = $selectedTab.ajaxtab("addRow", obj);
			$elements.find(".checkBox").trigger("click");
		}
		
		newlySelectedIds.ids[obj.randomId] = true;
	};
	var removeFn = function(obj) {
		var $elements = $selectedTab.ajaxtab("getRowElements", obj.randomId);
		if($elements.length > 0) {
			$elements.find(".checkBox").trigger("click");
			$elements.fadeOut(function(){ $selectedTab.ajaxtab("deleteRow", obj.randomId); });
		}		
	};
	
	$searchForm
		.on("click", ".add", function(event) {
			event.stopPropagation();
			event.preventDefault();
			
			var selectedCoupon = $quickAddText.autoselector("getSelectedObject");
			if((!selectedCoupon) || (!selectedCoupon.obj)) {
				// DO NOTHING.
			}
			else {
				addFn(selectedCoupon.obj);
				$quickAddText.autoselector("reset");
			}
		})
		.on("click", ".search", function(event) {
			event.preventDefault();
			$searchText.val($quickAddText.val());
			$searchDiscount.val("");
			$discount.val("");
			
			$consumer.val($("#formRandomId").val());
			$searchSelectedData.val($("#formCouponRandomIds").val());
			
			$selectableDialog.dialog("open");
			$selectableTab.paginatetab("reloadData");
		});
	
	$selectableDialog
		.on("click", ".search", function(event) {
			event.preventDefault();
			$quickAddText.val($searchText.val());
			$quickAddText.siblings("label").hide();
			$discount.val($searchDiscount.val());
			
			$selectableTab.paginatetab("reloadData");
		});
	
	$selectableTab
		.on("click", "li, .checkBox", function(event) {
			event.preventDefault(); event.stopPropagation();
			var coupon = null;
			var pos = null;
			var id = retrieveId(this);
			var that = null;
			if($(this).hasClass("checkBox")){ that = $(this); }
			else { that = $(this).find(".checkBox"); }
			
			if(that.hasClass("checked")){
				that.removeClass("checked");
				if(id) {
					pos = $selectableTab.paginatetab("getPositionById", id);
					if(pos) { coupon = $selectableTab.paginatetab("getObjectAt", pos.page, pos.row); }}
				
				if((!coupon) || (!pos)) { alert("ERROR !"); }
				else {
					removeFn(coupon);
					pos.dom.removeClass("selected");					
					$selectableTab.paginatetab("justify"); }}
			else {
				that.addClass("checked");
				if(id) {
					pos = $selectableTab.paginatetab("getPositionById", id);
					if(pos) {
						coupon = $selectableTab.paginatetab("getObjectAt", pos.page, pos.row);
					}
				}
				
				if((!coupon) || (!pos)) {
					alert("ERROR !");
				}
				else {
					addFn(coupon);
					pos.dom.addClass("selected");
					$selectableTab.paginatetab("justify");
				}}
		});
}

function retrieveId(aChild) {
	var id = null;
	var $aChild = $(aChild);
	if($aChild.is("li")) {
		id = $aChild.attr("id");
	}
	else {
		var $menu = $aChild.parents("section.ddMenu");
		if($menu.length > 0) {
			var $hiddenInput = $menu.find("input[type=hidden][name=id]");
			if($hiddenInput.length > 0) {
				id = $hiddenInput.val();
			}
			
			if(id && (id.length > 0)) {
				id = "id_" + id;
			}
			else {
				id = $menu.attr("id");
				if((id === "menu_couponMenu") || (id === "menu_cardMenu")) {
					id = null;
				}
			}
		}
		else {
			id = $aChild.parents("li:first").attr("id");
//			if(!id) {
//				id = this.previousId;
//			}
		}
	}
	
	if(id) {
		var _idx = id.lastIndexOf("_");
		if(_idx >= 0) {
			id = id.substring(_idx + 1);
		}
	}
	
	return id;
}

function setupCardsSelector() {
	/* Set up Consumer's coupons list */
	$("#consumerCardsListContainer")
		.ajaxtab({
			"rowTemplate": $("#cardListTemplate").html(),
			"idExtractor": extractRandomId
		});
	
	/* Set up Consumer's coupon pagination and filtering */
	var $searchForm = $("#cardSelectedTabForm");
	var $quickAddText = $("#cardQuickAddText");
	
	var $selectedTabData = $("#formCustomerCardRandomIds");
	var $selectedTab = $("#cardSelectedTab");
	$selectedTab
		.ajaxtab({
			"rowTemplate": $("#cardSelectedTabTemplate").html(),
			"idExtractor": extractRandomId
		});
	
	setupCheckBoxes($selectedTab, $selectedTabData);
	
	$quickAddText.on("keydown", function(event) {
		var continuePropagation = true;
		if(event.which == 13) {
			event.preventDefault();
			event.stopPropagation();
			continuePropagation = false;
			
			var selectedObj = $quickAddText.autoselector("getSelectedObject");
			if(selectedObj && selectedObj.obj) {
				$searchForm.find(".add").trigger("click");
			}
		}
		
		return continuePropagation;
	});
	
	var $quickAddButton = $("#cardSelectedTabForm .add");
	$quickAddText
		.autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"remoteFn": consumerCardAutoComplete,
			"selectByTyping": false
		})
		.on("keyup", function(event) {
			if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
		})
		.on("itemSelected", function() {
			$quickAddButton.removeClass("inactive");
		})
		.on("reset", function() {
			$quickAddButton.addClass("inactive");
		});
	
	/* Set up Coupon pagination and filtering */
	var newlySelectedIds = {
		"ids": {}
	};
	var $selectableTab = $("#cardListContainer");
	$selectableTab
		.paginatetab({
			"url": "/secure/accounts/customerCards/searchUnused.html",
			"formSelector": $searchForm,
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": $("#customerCardTemplate").html(),
			"responseExtractor": extractConsumerCardsList,
			"idExtractor": function(rowObj) { return rowObj.randomId; },
			"drawFilter": function(rowObj) {
				return !(newlySelectedIds.ids[rowObj.randomId] === true);
			}
		})
		.paginatetab("setupSortingMenu", "#cardListContainerHeader")
		.on("reloaded", function(event) {
			newlySelectedIds.ids = {};
		});
	
	var $selectableDialog = $("#cardSelectorDialog");
	$selectableDialog
		.dialog({
			"title": cardAdvancedSearchMsg,
			"dialogClass": "consumerAccountDialog",
			"modal": true,
			"resizable": false,
			"closeOnEscape": true,
			"width": 800,
			"maxHeight":  $(window).innerHeight()-150,
			position: {my: "center center-100"},
			"hide": "fade",
			"autoOpen": false,
			"stack": true,
			"buttons": {
				"done": {
					"text": "done",
					"click": function() {
						$quickAddText.autoselector("reset");
						$selectableDialog.dialog("close");
					}
				}
			},
			"dragStop": function(){ checkDialogPlacement($(this)); }
		})
		.on("dialogopen", function(event, ui) {
			var innerMaxHeight = $(window).innerHeight()-330;
			$("#cardSelectorDialog").find("ul#cardListContainer").css("max-height", innerMaxHeight+"px");
			resizeAutoWidthFilters($selectableDialog);
			resizeAutoWidthFilters($selectableDialog);
		});	
	
	btnStatus("done");
	
	var $searchText = $selectableDialog.find("#formFilterValue");
	var $consumer = $searchForm.find("#cardSelectedTabConsumerId");
	var $searchSelectedData = $searchForm.find("#cardSelectedTabSelectedIds");
	var addFn = function(obj) {
		var $elements = $selectedTab.ajaxtab("getRowElements", obj.randomId);
		if($elements.length > 0) {
			$elements.find(".checkBox:not(.checked)").trigger("click");
		}
		else {
			$elements = $selectedTab.ajaxtab("addRow", obj);
			$elements.find(".checkBox").trigger("click");
		}
		
		newlySelectedIds.ids[obj.randomId] = true;
	};
	var removeFn = function(obj) {
		var $elements = $selectedTab.ajaxtab("getRowElements", obj.randomId);
		if($elements.length > 0) {
			$elements.find(".checkBox").trigger("click");
			$elements.fadeOut(function(){ $selectedTab.ajaxtab("deleteRow", obj.randomId); });
		}		
	};
	
	$searchForm
		.on("click", ".add", function(event) {
			event.stopPropagation();
			event.preventDefault();
			
			var selectedCoupon = $quickAddText.autoselector("getSelectedObject");
			if((!selectedCoupon) || (!selectedCoupon.obj)) {
				// DO NOTHING.
			}
			else {
				addFn(selectedCoupon.obj);
				$quickAddText.autoselector("reset");
			}
		})
		.on("click", ".search", function(event) {
			event.preventDefault();
			$searchText.val($quickAddText.val());
			$consumer.val($("#formRandomId").val());
			$searchSelectedData.val($("#formCustomerCardRandomIds").val());
			
			$selectableDialog.dialog("open");
			$selectableTab.paginatetab("reloadData");
		});
	
	$selectableDialog
		.on("click", ".search", function(event) {
			event.preventDefault();
			$quickAddText.val($searchText.val());
			$quickAddText.siblings("label").hide();
			$selectableTab.paginatetab("reloadData");
		});
	
	$selectableTab
		.on("click", "li, .checkBox", function(event) {
			event.preventDefault(); event.stopPropagation();
			var passCard = null;
			var pos = null;
			var id = retrieveId(this);
			var that = null;
			if($(this).hasClass("checkBox")){ that = $(this); }
			else { that = $(this).find(".checkBox"); }
			
			if(that.hasClass("checked")){
				that.removeClass("checked");
				if(id) {
					pos = $selectableTab.paginatetab("getPositionById", id);
					if(pos) { passCard = $selectableTab.paginatetab("getObjectAt", pos.page, pos.row); }}
				
				if((!passCard) || (!pos)) { alert("ERROR !"); }
				else {
					removeFn(passCard);
					pos.dom.removeClass("selected");					
					$selectableTab.paginatetab("justify"); }}
			else {
				that.addClass("checked");
				if(id) {
					pos = $selectableTab.paginatetab("getPositionById", id);
					if(pos) {
						passCard = $selectableTab.paginatetab("getObjectAt", pos.page, pos.row);
					}
				}
				
				if((!passCard) || (!pos)) {
					alert("ERROR !");
				}
				else {
					addFn(passCard);
					pos.dom.addClass("selected");
					$selectableTab.paginatetab("justify");
				} }
		});
}

function extractRandomId(obj) {
	return (!obj) ? null : obj.randomId;
}

function consumerAutoComplete(request, response) {
	var $searchTermInput = $("#formFilterValue");
	if(inlineValidate($searchTermInput)) {
		var $postToken = $("#searchPostToken");
		var $form = $("#consumerFilterForm");
		var searchTerm = $searchTermInput.val();
		var request = GetHttpObject({
			"showLoader": false,
			"postTokenSelector": $postToken
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				var result = [];
				var consumersList = extractConsumersList(request.responseText);
				if(!consumersList) {
					consumersList = [];
				}
				else {
					var consumer, resultElem;
					var idx = -1, len = consumersList.length;
					while(++idx < len) {
						consumer = consumersList[idx];
						resultElem = {
							"label": consumer.firstName + " " + consumer.lastName,
							"value": consumer.randomConsumerId,
							"obj": consumer,
							"searchTerm": searchTerm
						};
						
						result.push(resultElem);
					}
				}
				
				response(result);
			}
		};
		
		request.open("POST", "/secure/accounts/consumerAccountsPage/index.html", true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize() + "&wrappedObject.page=1&wrappedObject.itemsPerPage=10");
	}
}

function consumerCouponAutoComplete(request, response) {
	var $searchTermInput = $("#couponQuickAddText");
	if(inlineValidate($searchTermInput)) {
		var $postToken = $("#searchPostToken");
		var $form = $("#couponSelectedTabForm");
		
		var $selected = $form.find("#couponSelectedTabSelectedIds");
		$selected.val($("#formCouponRandomIds").val());
		
		$("#couponSelectedTabConsumerId").val($("#formRandomId").val());
		
		var searchTerm = $searchTermInput.val();
		var request = GetHttpObject({
			"showLoader": false,
			"postTokenSelector": $postToken
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				var result = [];
				var couponsList = extracCouponsList(request.responseText);
				if(!couponsList) {
					couponsList = [];
				}
				else {
					var coupon, resultElem;
					var idx = -1, len = couponsList.length;
					while(++idx < len) {
						coupon = couponsList[idx];
						resultElem = {
							"label": coupon.coupon,
							"value": coupon.randomId,
							"category": coupon.locationName,
							"obj": coupon,
							"searchTerm": searchTerm
						};
						
						result.push(resultElem);
					}
				}
				
				response(result);
			}
		};
		
		request.open("POST", "/secure/accounts/coupons/searchUnusedCoupons.html", true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize() + "&wrappedObject.quickAdd=true&wrappedObject.page=1&wrappedObject.itemsPerPage=10");
	}
}

function consumerCardAutoComplete(request, response) {
	var $searchTermInput = $("#cardQuickAddText");
	if(inlineValidate($searchTermInput)) {
		var $postToken = $("#searchPostToken");
		var $form = $("#cardSelectedTabForm");
		
		var $selected = $form.find("#cardSelectedTabSelectedIds");
		$selected.val($("#formCustomerCardRandomIds").val());
		
		$("#cardSelectedTabConsumerId").val($("#formRandomId").val());
		
		var searchTerm = $searchTermInput.val();
		var request = GetHttpObject({
			"showLoader": false,
			"postTokenSelector": $postToken
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				var result = [];
				var cardsList = extracCardsList(request.responseText);
				if(!cardsList) {
					cardsList = [];
				}
				else {
					var card, resultElem;
					var idx = -1, len = cardsList.length;
					while(++idx < len) {
						card = cardsList[idx];
						resultElem = {
							"label": card.cardNumber + "",
							"value": card.randomId,
							"category": card.cardType,
							"obj": card,
							"searchTerm": searchTerm
						};
						
						result.push(resultElem);
					}
				}
				
				response(result);
			}
		};
		
		request.open("POST", "/secure/accounts/customerCards/searchUnused.html", true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize() + "&wrappedObject.page=1&wrappedObject.itemsPerPage=10");
	}
}

function prepareConsumerDetails(responseText, context) {
	var consumer = JSON.parse(responseText).consumerAccountDetails;
	var $viewPanel = $("#consumerView");
	$viewPanel.find("input[type=hidden][name=id]").val(consumer.randomId);
	$viewPanel.find("[name=dtlFirstName]").text(consumer.firstName);
	$viewPanel.find("[name=dtlLastName]").text(consumer.lastName);
	$viewPanel.find("[name=dtlEmail]").text(consumer.emailAddress);
	$viewPanel.find("[name=dtlDescription]").text(consumer.description);

	var $consumerCouponsList = $("#consumerCouponsListContainer");
	$consumerCouponsList.ajaxtab("clearData");
	if(consumer.coupons) {
		$consumerCouponsList.ajaxtab("addRow", consumer.coupons);
	}
	
	var $consumerCardsList = $("#consumerCardsListContainer");
	$consumerCardsList.ajaxtab("clearData");
	if(consumer.customCards) {
		$consumerCardsList.ajaxtab("addRow", consumer.customCards);
	}
	
	return $viewPanel;
}

function prepareConsumerForm(responseText, context) {
	var $formPanel = $("#consumerFormPanel");
	
	$formPanel.find("#filterShowOnlySelectedCoupons").val("false");
	$formPanel.find("#formCouponRandomIds,#filterSelectedCouponIds").val("");
	$formPanel.find("#filterShowOnlySelectedCards").val("false");
	$formPanel.find("#formCustomerCardRandomIds,#filterSelectedCardIds").val("");

	var $consumerCouponsList = $("#couponSelectedTab");
	$consumerCouponsList.ajaxtab("clearData");
	
	var $consumerCardsList = $("#cardSelectedTab");
	$consumerCardsList.ajaxtab("clearData");
	
	if(responseText) {
		var consumer = JSON.parse(responseText).consumerAccountDetails;
		$formPanel.find("#formRandomId").val(consumer.randomId);
		$formPanel.find("#formFirstName").val(consumer.firstName);
		$formPanel.find("#formLastName").val(consumer.lastName);
		$formPanel.find("#formEmail").val(consumer.emailAddress);
		$formPanel.find("#formEmailRandomId").val(consumer.emailAddressRandomId);
		$formPanel.find("#formDescription").val(consumer.description);
		
		var selectedCoupons = "";
		if(consumer.couponRandomIds) {
			selectedCoupons = retrieveCommaSeparatedData(consumer.couponRandomIds);
			$formPanel.find("#formCouponRandomIds").val(selectedCoupons);
		}
		
		var selectedCustomCards = "";
		if(consumer.customCardRandomIds) {
			selectedCustomCards = retrieveCommaSeparatedData(consumer.customCardRandomIds);
			$formPanel.find("#formCustomerCardRandomIds").val(selectedCustomCards);
		}
		
		$("#filterSelectedCouponIds").val(selectedCoupons);
		$("#filterSelectedCardIds").val(selectedCustomCards);
		
		if(consumer.coupons) {
			$consumerCouponsList.ajaxtab("addRow", consumer.coupons);
			$consumerCouponsList.find(".checkBox").filter(":not(.checked)").trigger("click");
		}
		
		if(consumer.customCards) {
			$consumerCardsList.ajaxtab("addRow", consumer.customCards);
			$consumerCardsList.find(".checkBox").filter(":not(.checked)").trigger("click");
		}
	}
	
	msgResolver.clearErrors();
	$formPanel.find(":input[class*=hiddenLabel]").trigger('blur');
	
	return $formPanel;
}

function retrieveCommaSeparatedData(data) {
	var result = "";
	if(data) {
		if(Array.isArray(data)) {
			result = data.join(",");
		}
		else if(typeof data === "string") {
			result = data;
		}
	}
	
	return result;
}

function retrieveConsumerCouponId(checkBox) {
	var id = $(checkBox).parents("li:first").attr("id");
	return id.substring(id.indexOf("_") + 1);
}

function extractConsumersList(responseText, context) {
	var result = null;
	
	var consumersList = JSON.parse(responseText);
	if(consumersList) {
		if(consumersList.list) {
			result = consumersList.list[0].CustomerAdminConsumerAccountInfo;
		}
		else {
			consumersList = consumersList.Consumers;
			result = consumersList.elements;
			if(context) {
				context["dataKey"] = consumersList.dataKey;
				context["page"] = consumersList.page;
			}
		}
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

function extractConsumerCouponsList(responseText, context) {
	var result = null;
	
	var couponsList = JSON.parse(responseText).couponsList;
	if(couponsList) {
		$("#couponFilterToken").val(couponsList.token);
		
		result = couponsList.elements;
		if(context) {
			context["dataKey"] = couponsList.dataKey;
		}
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

function extractConsumerCardsList(responseText, context) {
	var result = null;
	
	var cardsList = JSON.parse(responseText).customerCards;
	if(cardsList) {
		$("#customerCardFilterToken").val(cardsList.token);
		
		result = cardsList.elements;
		if(context) {
			context["dataKey"] = cardsList.dataKey;
		}
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

/* Scripts for Coupons Section */
function coupons(accntID, pageEvent) {
	/* Map Messages */
	msgResolver.mapAllAttributes({
		"couponCode": "#formCouponCode",
		"description": "#formDescription",
		"discountValue": "#formDiscountValue",
		"startDate": "#formStartDate",
		"endDate": "#formEndDate",
		"numUsesType": "#formMaxNumOfUsesType",
		"useNumUses": "#formMaxNumOfUses",
		"payOptions": "#payOptions",
		"stallRange": "#formSpaceRange",
		"locationId": "#formLocation",
		"file": "#formCouponFile",
		"importMode": "#formUploadMode",
		"filterValue": "#formFilterValue",
		"filterDiscount": "#formFilterDiscount"
	});
	
	/* Setup Shrinkable */
	$(".layout1")
		.shrinkable();
	
	/* Set up Coupon CRUD Panel */
	var $couponPanel = $("#couponPanel")
		.crudpanel({
			"viewURL": "/secure/accounts/coupons/viewCoupon.html?id=[%= id %]",
			"viewRender": prepareCouponDetails,
			"formRender": prepareCouponForm,
			"saveURL": "/secure/accounts/coupons/updateCoupon.html",
			"deleteVerifyURL": "/secure/accounts/coupons/deleteCouponVerify.html?id=[%= id %]",
			"deleteURL": "/secure/accounts/coupons/deleteCoupon.html?id=[%= id %]",
			"itemName": COUPON_LABEL.coupon,
			"postTokenSelector": "#postToken",
			"itemSelector": "#couponListContainer li *:not(a)",
			"idExtractor": retrieveId,
			"customPanels": 
				[{
					"name": "import",
					"prepareFn": prepareCouponImportForm, 
					"shouldBindActions": true
				}],
			"customActions":
				[{
					"name": "export",
					"actionFn": downloadCoupons
				}]
		})
		.on("beforeShowPanel", function(event, actionName) {
			if(blockedActions[actionName]) {
				testFormEdit($("#couponForm"));
			}
		})
		.on("showPanel", function(event, actionName) {
			if(actionName == "add") {
				$("#formCouponCode").trigger('focus');
			}
			else {
				$("#formDescription").trigger('focus');
			}
		})
		.on("detachedPanel", function(event) {
			$("#mainContent").removeClass("edit");
		});
	
	/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempItemID = '',
			tempEvent = '';
		if(stateObj !== null){
			tempItemID = stateObj.itemObject;
			tempEvent = stateObj.action; }
		popAccountAction($("#couponPanel"), tempItemID, tempEvent, stateObj); }
	 
	if(!popStateEvent && (pageEvent !== undefined && pageEvent !== '')){ 
		popAccountAction($("#couponPanel"), accntID, pageEvent); 
	}
	///////////////////////////////////////////////////

	
	/* Set up Coupon pagination and filtering */
	var $couponListContainer = $("#couponListContainer")
		.paginatetab({
			"url": "/secure/accounts/coupons/searchCoupons.html",
			"formSelector": "#couponFilterForm",
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": $("#couponTemplate").html(),
			"responseExtractor": extracCouponsList,
			"itemLocatorTemplate": "#couponItem_[%= id %]",
			"itemLocatorURL": "/secure/accounts/coupons/locateItemPage.html"
		})
		.paginatetab("addRawData", $("#couponListData").text())
		.paginatetab("setupSortingMenu", "#couponListContainerHeader");
	
	scrollListWidth(["couponListContainer"]);
	
	var autoSelectFn = function(error, rawResponse, translatedResponse) {
		if(translatedResponse && (translatedResponse.length === 1)) {
			$couponPanel.crudpanel("showViewPanel", null, { "id": translatedResponse[0].randomId });
		} else {
			$couponPanel.crudpanel("hideActivePanel", true);
		}
	};
	
	var filterFn = function(event) {
		if($("#mainContent").hasClass("edit")) {
			inEditAlert("menu");
		}
		else {
			$couponListContainer
				.paginatetab("option", "triggerSelector", this)
				.paginatetab("reloadData", autoSelectFn);
		}
		
		event.stopPropagation();		
		return false;
	};
	
	$("#couponFilterForm")
		.on("click", ".search", filterFn);
	
	var $couponSearchButton = $("#couponFilterForm .search");
	$("#formFilterValue")
		.autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"remoteFn": couponAutoComplete,
			"selectByTyping": false
		})
		.on("itemSelected", function(ui, selectedItem) {
			if(selectedItem.obj) {
				// Let the filter function handle the item selection
				$("#formFilterValue").val(selectedItem.searchTerm);
				$couponListContainer.paginatetab("reloadData", function() {
					$couponPanel.crudpanel("showViewPanel", null, { "id": selectedItem.obj.randomId });
				});
			}
		})
		.on("keydown", function(event) {
			if(event.which == 13) {
				$couponSearchButton.trigger("click");
			}
		})
		.on("keyup", function(event) {
			if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
		});
	
	$("#searchClearBtn").on('click', function(event){ event.preventDefault(); $("#formFilterValue").val(""); $("#formFilterValueLabel").fadeIn().removeClass("active"); $couponSearchButton.trigger("click"); $(this).hide(); });
	
	/* Setup inputs */
	coupleDateComponents(createDatePicker("#formStartDate"), null, createDatePicker("#formEndDate"), null);
	
	$("#formDiscountType")
		.autoselector({
			"isComboBox": true,
			"data": JSON.parse($("#formDiscountTypeData").text())
		})
		.on("itemSelected", updateDiscountDisplay);
	
	updateDiscountDisplay();
	
	var statusData = JSON.parse($("#couponStatusFilterData").text());
	statusData = statusData.list;
	$("#formFilterStatus")
		.autoselector({
			"isComboBox": true,
			"data": statusData
		});
	
	$("#formMaxNumOfUsesType")
		.autoselector({
			"isComboBox": true,
			"data": JSON.parse($("#formMaxNumOfUsesTypeData").text())
		})
		.on("itemSelected", updateMaxUsageDisplay);
	
	$("#formLocation").autoselector({
		"isComboBox": true,
		"defaultValue": "0",
		"descTitle": "Parent Location",
		"data": extractLocationsList($("#formLocationData").text(), { "label": labelAll, "value": "0" })
	});
	
	setupCheckBoxes($("#formDailySingleUseOptions"), $("#formDailySingleUse"));
	setupCheckBoxes($("#formPndEnabledOptions"), $("#formPndEnabled"));
	setupCheckBoxes($("#formPbsEnabledOptions"), $("#formPbsEnabled"));
	
	/* Setup Upload Form */
	var $importForm = $("#couponUploadForm")
		.dptupload({
			"postTokenSelector": "#uploadPostToken",
			"url": "/secure/accounts/coupons/importCoupons.html?" + document.location.search.substring(1)
		})
		.on("uploadSuccess", function(event, responseContext) {
			if(!responseContext.displayedMessages) {
				noteDialog(COUPON_LABEL.importSuccess);
				reloadPage();
			}
			else {
				window.location.reload();
			}
		});
	
	$("#couponImportPanel").on("click", ".upload", function() {
		accountsImportActionStatus.status = true;
		
		$importForm
			.dptupload("option", "triggerSelector", this)
			.dptupload("upload");
	});
	
	/* Setup Upload Inputs */
	$("#formUploadMode").autoselector({
		"isComboBox": true,
		"defaultValue": "Merge",
		"data": "#formUploadModeData"
	});
	
	/* Bind Events */
	$("#formPbsEnabledOptions").on("checkBoxClick", updateStallRangesDisplay);
	
	$couponPanel
		.on("afterSave", function(event, context) {
			var reloadId = context.activeId;
			if(reloadId) {
				// Lookup the Id from response
				if(context.responseText) {
					var buffer = context.responseText.split(":");
					if(buffer.length >= 3) {
						reloadId = buffer[2];
					}
				}
			}
			
			if(!reloadId) {
				reloadPage();
			}
			else {
				$couponListContainer.paginatetab(
						"reloadPageWithItem",
						{ "id": reloadId },
						function() {
							$couponPanel.crudpanel("showViewPanel", null, { "id": reloadId });
						});
			}
		})
		.on("afterExecute", reloadPage);
	
	// Display error import messages
	var errorData = $("#_importErrors").text().trim();
	if(errorData.length > 0) {
		errorData = JSON.parse(errorData);
		if(errorData && errorData.errorStatus.errorStatus) {
			$("#couponImportPanel #menu_couponMenu .import").trigger("click");
			msgResolver.displaySerMsg(errorData.errorStatus.errorStatus);
		}
	}
	
	/* Setup char blockers */
	setupCharBlocker(".intNumberWithSeparator", /[^-, 0-9]/, COUPON_LABEL.errorSpaceRange);
}

function couponAutoComplete(request, response) {
	var $searchTermInput = $("#formFilterValue");
	if(inlineValidate($searchTermInput)) {
		var $postToken = $("#searchPostToken");
		var $form = $("#couponFilterForm");
		var searchTerm = $searchTermInput.val();
		var request = GetHttpObject({
			"showLoader": false,
			"postTokenSelector": $postToken
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				var result = [];
				var couponsList = extracCouponsList(request.responseText);
				if(!couponsList) {
					couponsList = [];
				}
				else {
					var coupon, resultElem;
					var idx = -1, len = couponsList.length;
					while(++idx < len) {
						coupon = couponsList[idx];
						resultElem = {
							"label": coupon.coupon,
							"value": coupon.randomId,
							"category": coupon.locationName,
							"obj": coupon,
							"searchTerm": searchTerm
						};
						
						result.push(resultElem);
					}
				}
				
				response(result);
			}
		};
		
		request.open("POST", "/secure/accounts/coupons/searchCoupons.html", true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize() + "&wrappedObject.page=1&wrappedObject.itemsPerPage=10");
	}
}

function prepareCouponImportForm() {
	var $importPanel = $("#couponImportPanel");
	$importPanel.find("#formUploadMode").autoselector("reset");
	$importPanel.find("#formCouponFile").val("");
	testFormEdit($("#couponUploadForm"));
	
	var stateObj = { itemObject: null, action: "import" };
	crntPageAction = stateObj.action;
	history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action);
	
	msgResolver.clearErrors();
	
	return $importPanel;
}


function downloadCoupons() {
	window.location = "/secure/accounts/coupons/exportCoupons.html" + "?" + scrubQueryString(document.location.search.substring(1), "all");
}

function updateDiscountDisplay() {
	var discountType = $("#formDiscountType").autoselector("getSelectedValue");
	var $discountVal = $("#formDiscountValue");
	
	var maxlength = null;
	if(discountType === "P") {
		maxlength = 3;
		$discountVal.removeClass("nmbrChrctr").addClass("intNmbrChrctr");
	}
	else if(discountType === "D") {
		maxlength = 6;
		$discountVal.addClass("nmbrChrctr").removeClass("intNmbrChrctr");
	}
	
	if(maxlength === null) {
		$discountVal.attr("maxlength", "");
	}
	else {
		$discountVal.attr("maxlength", maxlength);
		
		var currVal = $discountVal.val();
		if(currVal.length > maxlength) {
			$discountVal.val(currVal.substring(0, maxlength));
		}
	}
}

function updateStallRangesDisplay() {
	var pbs = $("#formPbsEnabled").val().toLowerCase();
	var $sectionStall = $("#sectionStallRanges");
	if(pbs == "true") {
		$sectionStall.show();
	}
	else {
		$sectionStall.hide();
	}
}

function updateMaxUsageDisplay() {
	var discountType = $("#formMaxNumOfUsesType").autoselector("getSelectedValue");
	var $usage = $("#formMaxNumOfUses");
	if(discountType === "U") {
		$usage.hide().val("");
	}
	else {
		$usage.show();
	}
}

function extracCouponsList(responseText, context) {
	var result = null;
	
	var couponsList = JSON.parse(responseText).couponsList;
	if(couponsList) {
		$("#searchPostToken").val(couponsList.token);
		
		result = couponsList.elements;
		if(context) {
			context["dataKey"] = couponsList.dataKey;
			context["page"] = couponsList.page;
		}
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

function prepareCouponDetails(responseText, context) {
	var $panel = $("#couponView");
	if(responseText) {
		var coupon = JSON.parse(responseText).coupon;
		
		$panel.find("input[type=hidden][name=id]").val(coupon.randomId);
		
		$panel.find("[name=dtlCouponCode]").text(coupon.coupon);
		if(coupon.description) {
			$panel.find("[name=dtlDescription]").text(coupon.description);
		}
		
		var $discount = $panel.find("[name=dtlDiscount]");
		if(coupon.discountType === "P") {
			$discount.text(coupon.discount + " %");
		}
		else {
			$discount.text("$" + coupon.discount);
		}
		
		if(coupon.startDate) {
			$panel.find("[name=dtlStartDate]").text(coupon.startDate);
		}
		if(coupon.endDate) {
			$panel.find("[name=dtlEndDate]").text(coupon.endDate);
		}
		
		if(coupon.locationName) {
			$panel.find("[name=dtlLocation]").text(coupon.locationName);
		}
		else {
			$panel.find("[name=dtlLocation]").text(labelAll);
		}
		
		if(coupon.spaceRange) {
			$panel.find("[name=dtlSpaceRange]").text(coupon.spaceRange);
		}
		
		if(coupon.numberOfUsesRemaining) {
			$panel.find("[name=dtlMaxNumOfUses]").text(coupon.numberOfUsesRemaining);
		}
		
		$panel.find("[name=dtlAssignedTo]").text(coupon.consumerName);
		
		var $restrictions = $panel.find("[name=dtlRestriction]");
		if(coupon.validForNumOfDay <= 0) {
			$restrictions.text(COUPON_LABEL.notDailySingleUse);
		}
		else {
			$restrictions.text(COUPON_LABEL.isDailySingleUse);
		}
		
		var $availability = $panel.find("[name=dtlAvailableFor]");
		var restrictions = [];
		if(coupon.pndEnabled && (coupon.pndEnabled == true)) {
			restrictions.push(COUPON_LABEL.pnd);
		}
		if(coupon.pbsEnabled && (coupon.pbsEnabled == true)) {
			restrictions.push(COUPON_LABEL.pbs);
		}
		
		if(restrictions.length <= 0) {
			$availability.text("-");
		}
		else {
			$availability.html(restrictions.join("<br/>"));
		}
		
		var $viewMenu = $panel.find("#opBtn_couponView");
		if(coupon.offline) {
			$viewMenu.hide();
		}
		else {
			$viewMenu.show();
		}
	}
	
	return $panel;
}

function prepareCouponForm(responseText, context) {
	var $formPanel = $("#couponFormPanel");
	var validForNumOfDay = null, pndEnabled = null, pbsEnabled = null;
	
	if(!responseText) {
		$("#formCouponCode").attr("disabled", null).removeClass("disabledArea");
		
		$("#formDiscountType,#formLocation,#formMaxNumOfUsesType").autoselector("reset");
	}
	else {
		var coupon = JSON.parse(responseText).coupon;
		
		$("#formRandomId").val(coupon.randomId);
		$("#formCustomerRandomId").val(coupon.customerRandomId);
		$("#formCouponCode").val(coupon.coupon).attr("disabled", true).addClass("disabledArea");
		$("#formDescription").val(coupon.description);
		$("#formDiscountValue").val(coupon.discount);
		$("#formDiscountType").autoselector("setSelectedValue", coupon.discountType);
		
		$formPanel.find("#formMaxNumOfUsesType").autoselector("setSelectedValue", coupon.numUsesType);
		if(coupon.numUsesType.toUpperCase() == 'S') {
			$("#formMaxNumOfUses").val(coupon.numberOfUsesRemaining);
		}
		
		$("#formStartDate").val(coupon.startDate);
		$("#formEndDate").val(coupon.endDate);
		if(typeof coupon.locationRandomId !== "undefined") {
			$("#formLocation").autoselector("setSelectedValue", coupon.locationRandomId);
		}
		else {
			$("#formLocation").autoselector("reset");
		}
		
		$("#formSpaceRange").val(coupon.spaceRange);
		
		validForNumOfDay = coupon.validForNumOfDay;
		pndEnabled = coupon.pndEnabled;
		pbsEnabled = coupon.pbsEnabled;
	}
	
	deserializeCheckBoxValues("#formDailySingleUseOptions", ((validForNumOfDay) && (validForNumOfDay > 0)) ? "true" : null, "#formDailySingleUse");
	deserializeCheckBoxValues("#formPndEnabledOptions", pndEnabled, "#formPndEnabled");
	deserializeCheckBoxValues("#formPbsEnabledOptions", pbsEnabled, "#formPbsEnabled");
	
	updateMaxUsageDisplay();
	updateStallRangesDisplay();
	
	msgResolver.clearErrors();
	$formPanel.find(":input[class*=hiddenLabel]").trigger('blur');
	
	return $formPanel;
}

/* Scripts for Customer Cards Section */
function customerCards(accntID, pageEvent) {
	setupCharBlocker(".cardNumberChrctr", /[^0-9=]/i, function() { return nmbbrChrctrsOnlyMsg; });
	
	/* Map Messages */
	msgResolver.mapAllAttributes({
		"cardNumber": "#formCardNumber",
		"cardType": "#formCardType",
		"location": "#formLocation",
		"validFrom": "#formValidFrom",
		"validTo": "#formValidTo",
		"gracePeriod": "#formGracePeriod",
		"maxNumOfUses": "#formMaxNumOfUses",
		"comment": "#formComment",
		"file": "#formFile",
		"importMode": "#formUploadMode",
		"filterValue": "#formFilterValue"
	});
	
	/* Setup Shrinkable */
	$(".layout1")
		.shrinkable();
	
	/* Set up Card CRUD Panel */
	var $cardPanel = $("#customerCardPanel")
		.crudpanel({
			"viewURL": "/secure/accounts/customerCards/viewCard.html?cardId=[%= id %]",
			"viewRender": prepareCardDetails,
			"formRender": prepareCardForm,
			"saveURL": "/secure/accounts/customerCards/saveCard.html",
			"deleteVerifyURL": "/secure/accounts/customerCards/deleteCardVerify.html?cardId=[%= id %]",
			"deleteURL": "/secure/accounts/customerCards/deleteCard.html?cardId=[%= id %]",
			"itemName": CARD_LABEL.customerCard,
			"postTokenSelector": "#postToken",
			"itemSelector": "#cardListContainer li *:not(a)",
			"idExtractor": retrieveId,
			"customPanels":
				[{
					"name": "import",
					"prepareFn": prepareCardImportForm, 
					"shouldBindActions": true
				}],
			"customActions":
				[{
					"name": "deleteAll",
					"url": "/secure/accounts/customerCards/deleteAllCard.html",
					"pollable": true,
					"confirmMessage": CARD_LABEL.deleteAllConfirm,
					"successMessage": CARD_LABEL.deleteAllSuccess,
					"errorMessage": CARD_LABEL.deleteAllError
				},
				{
					"name": "export",
					"actionFn": downloadCards
				}]
		})
		.on("beforeShowPanel", function(event, actionName) {
			if(blockedActions[actionName]) {
				testFormEdit($("#customerCardForm"));
			}
		})
		.on("showPanel", function(event, actionName) {
			if(actionName == "add") {
				$("#formCardNumber").trigger('focus');
			}
			else {
				$("#formComment").trigger('focus');
			}
		})
		.on("detachedPanel", function(event) {
			$("#mainContent").removeClass("edit");
		});

	/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempItemID = '',
			tempEvent = '';
		if(stateObj !== null){
			tempItemID = stateObj.itemObject;
			tempEvent = stateObj.action; }
		popAccountAction($("#customerCardPanel"), tempItemID, tempEvent, stateObj); }
	 
	if(!popStateEvent && (pageEvent !== undefined && pageEvent !== '')){ popAccountAction($("#customerCardPanel"), accntID, pageEvent); }
	///////////////////////////////////////////////////
	
	/* Set up Card pagination and filtering */
	var $cardListContainer = $("#cardListContainer")
		.paginatetab({
			"url": "/secure/accounts/customerCards/search.html",
			"formSelector": "#customerCardFilterForm",
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": $("#customerCardTemplate").html(),
			"responseExtractor": extracCardsList,
			"itemLocatorTemplate": "#cardItem_[%= id %]",
			"itemLocatorURL": "/secure/accounts/customerCards/locateItemPage.html"
		})
		.paginatetab("addRawData", $("#cardListData").text())
		.paginatetab("setupSortingMenu", "#cardListContainerHeader");
	
	var $cardMenu = $("#menu_cardMenu");
	var hideDeleteAll = function() {
		if($cardListContainer.paginatetab("containsData")) {
			$cardMenu.find(".deleteAll").show();
		}
		else {
			$cardMenu.find(".deleteAll").hide();
		}
	};
	
	$cardListContainer.on("reloaded", hideDeleteAll);
	
	hideDeleteAll();
	
	scrollListWidth(["cardListContainer"]);
	
	var autoSelectFn = function(error, rawResponse, translatedResponse) {
		if(translatedResponse && (translatedResponse.length === 1)) {
			$cardPanel.crudpanel("showViewPanel", null, { "id": translatedResponse[0].randomId });
		} else {
			$cardPanel.crudpanel("hideActivePanel", true);
		}
	};
	
	var filterFn = function(event) {
		if($("#mainContent").hasClass("edit")) {
			inEditAlert("menu");
		}
		else {
			$cardListContainer
				.paginatetab("option", "triggerSelector", this)
				.paginatetab("reloadData", autoSelectFn);
		}
		
		event.stopPropagation();
		return false;
	};
	
	$("#customerCardFilterForm")
		.on("click", ".search", filterFn);
	
	var $customerCardSerachButton = $("#customerCardFilterForm .search");
	$("#formFilterValue")
		.autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"remoteFn": cardAutoComplete,
			"selectByTyping": false
		})
		.on("itemSelected", function(ui, selectedItem) {
			if(selectedItem.obj) {
				// Let the filter function handle the item selection
				$("#formFilterValue").val(selectedItem.searchTerm);
				$cardListContainer.paginatetab("reloadData", function() {
					$cardPanel.crudpanel("showViewPanel", null, { "id": selectedItem.obj.randomId });
				});
			}
		})
		.on("keydown", function(event) {
			if(event.which == 13) {
				$customerCardSerachButton.trigger("click");
			}
		})
		.on("keyup", function(event) {
			if($(this).val() !== ""){$("#searchClearBtn").fadeIn(); } else {$("#searchClearBtn").fadeOut();}
		});
	
	$("#searchClearBtn").on('click', function(event){ event.preventDefault(); $("#formFilterValue").val(""); $("#formFilterValueLabel").fadeIn().removeClass("active"); $customerCardSerachButton.trigger("click"); $(this).hide(); });
	
	/* Setup inputs */
	coupleDateComponents(createDatePicker("#formValidFrom"), null, createDatePicker("#formValidTo"), null);
	
	$("#formCardType").autoselector({
		"isComboBox": true,
		"defaultValue": "0",
		"allowNonMatchedValue": "true",
		"data": JSON.parse($("#formCardTypeData").text()).list[0].Filter
	});
	
	$("#formLocation").autoselector({
		"isComboBox": true,
		"defaultValue": "0",
		"shouldCategorize": true,
		"descTitle": "Parent Location",
		"data": extractLocationsList($("#formLocationData").text(), { "label": labelAll, "value": "0" })
	});
	
	setupCheckBoxes("#formIsRestrictedOptions", "#formIsRestricted");
	$("#formIsRestrictedOptions").on("checkBoxClick", updateCardGracePeriodVisibility);
	
	/* Setup Upload Form */
	var $importForm = $("#cardUploadForm")
		.dptupload({
			"postTokenSelector": "#uploadPostToken",
			"url": "/secure/accounts/customerCards/importCards.html?" + document.location.search.substring(1)
		})
		.on("uploadSuccess", function(event, responseContext) {
			if(!responseContext.displayedMessages) {
				noteDialog(CARD_LABEL.importSuccess);
				reloadPage();
			}
			else {
				window.location.reload();
			}
		});
	
	$("#cardImportPanel").on("click", ".upload", function() {
		accountsImportActionStatus.status = true;
		
		$importForm
			.dptupload("option", "triggerSelector", this)
			.dptupload("upload");
	});
	
	/* Setup Upload Inputs */
	$("#formUploadMode").autoselector({
		"isComboBox": true,
		"defaultValue": "Merge",
		"data": "#formUploadModeData"
	});
	
	/* Bind Events */
	$cardPanel
		.on("afterSave", function(event, context) {
			if(!context.activeId) {
				reloadPage();
			}
			else {
				$cardListContainer.paginatetab(
						"reloadPageWithItem",
						{ "id": context.activeId },
						function() {
							$cardPanel.crudpanel("showViewPanel", null, { "id": context.activeId });
						});
			}
		})
		.on("afterExecute", reloadPage);
	
	// Initialize Filter Menu
	setupFilterMenu($(".filterHeader"));
	
	// Display error import messages
	var errorData = $("#_importErrors").text().trim();
	if(errorData.length > 0) {
		errorData = JSON.parse(errorData);
		if(errorData && errorData.errorStatus.errorStatus) {
			$("#customerCardPanel #menu_cardMenu .import").trigger("click");
			msgResolver.displaySerMsg(errorData.errorStatus.errorStatus);
		}
	}
}

function cardAutoComplete(request, response) {
	var $searchTermInput = $("#formFilterValue");
	if(inlineValidate($searchTermInput)) {
		var $postToken = $("#searchPostToken");
		var $form = $("#customerCardFilterForm");
		var searchTerm = $searchTermInput.val();
		var request = GetHttpObject({
			"showLoader": false,
			"postTokenSelector": $postToken
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				var result = [];
				var cardsList = extracCardsList(request.responseText);
				if(!cardsList) {
					cardsList = [];
				}
				else {
					var card, resultElem;
					var idx = -1, len = cardsList.length;
					while(++idx < len) {
						card = cardsList[idx];
						resultElem = {
							"label": card.cardNumber + "",
							"value": card.randomId,
							"category": card.cardType,
							"obj": card,
							"searchTerm": searchTerm
						};
						
						result.push(resultElem);
					}
				}
				
				response(result);
			}
		};
		
		request.open("POST", "/secure/accounts/customerCards/search.html", true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize() + "&wrappedObject.page=1&wrappedObject.itemsPerPage=10");
	}
}

function extracCardsList(responseText, context) {
	var result = null;
	
	var cardsList = JSON.parse(responseText).customerCards;
	if(cardsList) {
		$("#searchPostToken").val(cardsList.token);
		
		result = cardsList.elements;
		if(context) {
			context["dataKey"] = cardsList.dataKey;
			context["page"] = cardsList.page;
		}
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

function prepareCardDetails(responseText, context) {
	var $panel = $("#customerCardView");
	if(responseText) {
		var card = JSON.parse(responseText).card;
		
		$panel.find("input[type=hidden][name=id]").val(card.randomId);
		$panel.find("[name=dtlCardNumber]").text(card.cardNumber);
		$panel.find("[name=dtlCardType]").text(card.cardType);
		if(card.location) {
			$panel.find("[name=dtlLocation]").text(card.location);
		}
		else {
			$panel.find("[name=dtlLocation]").text(labelAll);
		}
		
		$panel.find("[name=dtlComment]").text(card.comment);
		$panel.find("[name=dtlAssignedTo]").text(card.consumerName);
		
		$panel.find("[name=dtlValidFrom]").text(card.startValidDate);
		$panel.find("[name=dtlValidTo]").text(card.expireDate);
		var restricted = (card.restricted && ((card.restricted == "true") || (card.restricted == true)));
		$panel.find("[name=dtRestricted]").text((restricted) ? "Yes" : "No");
		if(restricted !== true) {
			$panel.find("[name=dtGracePeriod]").text("N/A");
		}
		else {
			$panel.find("[name=dtGracePeriod]").text(card.gracePeriodMinutes);
		}
		
		$panel.find("[name=dtlMaxNumOfUses]").text(card.maxNumOfUses);
		$panel.find("[name=dtlRemainingUsage]").text(card.remainingNumOfUses);
	}
	
	return $panel;
}

function prepareCardForm(responseText, context) {
	var $formPanel = $("#customerCardFormPanel");
	
	var $formCardType = $("#formCardType");
	var $formCardTypeEdit = $("#formCardTypeEditSection");
	var $formCardTypeView = $("#formCardTypeViewSection");
	var restricted = null;
	if(!responseText) {
		$("#sectionAssignedTo").hide();
		$("#sectionRemainingUsage").hide();
		$("#formCardNumber").attr("disabled", null).removeClass("disabledArea");
		
		$formCardTypeView.hide();
		$formCardTypeEdit.show();
		
		$("#formCardType,#formLocation").autoselector("reset");
	}
	else {
		var card = JSON.parse(responseText).card;
		
		$("#sectionAssignedTo").show();
		$("#sectionRemainingUsage").show();
		
		$("#formRandomId").val(card.randomId);
		$("#formCardNumber").val(card.cardNumber).attr("disabled", true).addClass("disabledArea");
		
		$formCardType.autoselector("setSelectedValue", card.cardTypeRandomId);
		//var curLabel = $formCardType.autoselector("getSelectedLabel"); // setting variable so if nothing is returned from autoselector the HTML input object isn't written - TODD
		//$formCardTypeView.show().text(curLabel);
		$formCardTypeView.show().text(card.cardType);
		$formCardTypeEdit.hide();
		
		$("#formLocation").autoselector("setSelectedValue", (card.locationRandomId) ? card.locationRandomId : "0");
		$("#formComment").val(card.comment);
		$("#formAssignedTo").text(card.assignedTo);
		
		$("#formValidFrom").val(card.startValidDate);
		$("#formValidTo").val(card.expireDate);
		$("#formGracePeriod").val(card.gracePeriodMinutes);
		$("#formMaxNumOfUses").val(card.maxNumOfUses);
		$("#formRemainingUsage").text(card.remainingNumOfUses);
		
		restricted = card.restricted;
	}
	
	msgResolver.clearErrors();
	
	deserializeCheckBoxValues("#formIsRestrictedOptions", restricted, "#formIsRestricted");
	$formPanel.find(":input[class*=hiddenLabel]").trigger('blur');
	updateCardGracePeriodVisibility();
	
	return $formPanel;
}


function prepareCardImportForm() {
	var $importPanel = $("#cardImportPanel");
	$importPanel.find("#formUploadMode").autoselector("reset");
	$importPanel.find("#formCardFile").val("");
	testFormEdit($("#cardUploadForm"));
	
	var stateObj = { itemObject: null, action: "import" };
	crntPageAction = stateObj.action;
	history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action);
	
	msgResolver.clearErrors();
	
	return $importPanel;
}

function downloadCards() {
	window.location = "/secure/accounts/customerCards/exportCards.html" + "?" + scrubQueryString(document.location.search.substring(1), "all");
}

function updateCardGracePeriodVisibility() {
	var $gracePeriod = $("#sectionGracePeriod");
	if($("#formIsRestricted").val() == "true") {
		$gracePeriod.show();
	}
	else {
		$gracePeriod.hide();
	}
}

function extractLocationsList(jsonStr, defaultObj) {
	var result = [];
	if(defaultObj) {
		result.push(defaultObj);
	}
	
	var raw = JSON.parse(jsonStr);
	if(typeof raw.locationTree !== "undefined") {
		convertLocationTreeToFilter(result, raw.locationTree);
	}
	
	return result;
}

function convertLocationTreeToFilter(result, parent) {
	var childsList = parent.children;
	if(typeof childsList !== "undefined") {
		if(!Array.isArray(childsList)) {
			childsList = [ childsList ];
		}
		
		var parentName = parent.name;
		if(!parentName) {
			parentName = "";
		}
		
		var idx = -1, len = childsList.length, child, convertedChild;
		while(++idx < len) {
			child = childsList[idx];
			convertedChild = {
				"label": child.name + "",
				"value": child.randomId
			};
			
			if(parentName.length > 0) {
				convertedChild["desc"] = parentName;
			}
			
			if(child.isLocation) {
				convertedChild["category"] = labelLocation;
			}
			else if(child.isPayStation) {
				convertedChild["category"] = labelPayStation;
			}
			
			result.push(convertedChild);
			
			convertLocationTreeToFilter(result, child);
		}
	}
}
