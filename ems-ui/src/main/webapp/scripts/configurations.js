// JavaScript Document

var	groupData,
	sysAdminQuerry,
	bossKeyRequired,
	configStatus = null,
	hwConfiguration = {};

tempOverlayPOSstatus = function(){
	$("#psChildList").find("li").each(function(){

		if($(this).hasClass("statIncomplete") || $(this).hasClass("statCompleted")){ 
			$(this).removeClass("statIncomplete statCompleted");
			$(this).addClass(cnfgStats[configStatus].listClass); }
		
	})
}

tempTestShowEditBtn = function(){
	if(configStatus !== "inactive"){ 
		if(configStatus === "scheduled" && (usrManagePermish && !userSchedulePermish)){
			$(".hdrButtonIcn.btnEditGroup").hide(); }
		else if(configStatus === "active" && (!usrManagePermish && userSchedulePermish)){ 
			$(".hdrButtonIcn.btnEditGroup").hide(); }
		else { $(".hdrButtonIcn.btnEditGroup").show(); } } 
	else if(configStatus === "inactive"){ $(".hdrButtonIcn.btnEditGroup").hide(); }
	else { $(".hdrButtonIcn.btnEditGroup").show(); } }


//--------------------------------------------------------------------------------------

hwConfiguration.autoComplete = function(request, response) {
	var responseOptions = [], cnfgGroupResponse = [], data, cnfgGroupOpts = [], obj, i, len,
		searchTerm = request.term.trim().toLowerCase(),
		ajaxCnfgGroupReq = GetHttpObject(),
		allcnfgGroupItem = {label: "All Configuration Groups", value: "all"},
		cnfStatusItems = [{ value: "unscheduled", label: cnfgStats.unscheduled.title, category: configGroupStatusCat },
						  { value: "active", label: cnfgStats.active.title, category: configGroupStatusCat },
						  { value: "incomplete", label: cnfgStats.incomplete.title, category: configGroupStatusCat },
						  { value: "scheduled", label: cnfgStats.scheduled.title, category: configGroupStatusCat },
						  { value: "inactive", label: cnfgStats.inactive.title, category: configGroupStatusCat }];

	if(request.term.length >= 4){
		ajaxCnfgGroupReq.onreadystatechange = function(){//responseFalse, displayedError) {
			if(ajaxCnfgGroupReq.readyState === 4 && ajaxCnfgGroupReq.status === 200) {
				data = JSON.parse(ajaxCnfgGroupReq.responseText);
				
				if(!Array.isArray(data.groupSearchResult)){ data.groupSearchResult = [data.groupSearchResult]; }

				cnfgGroupResponse = $.map(data.groupSearchResult, function(item) { return{ 
					label: item.groupName, 
					value: item.groupId,
					desc: (item.groupStatus === "incomplete")? inprogressLbl : item.groupStatus, 
					category: configGroupCat }; }); }
				
			i = -1; len = cnfStatusItems.length;
			while(++i < len) {
				obj = cnfStatusItems[i];
				if((searchTerm.length <= 0) || (obj.label.toLowerCase().indexOf(searchTerm) >= 0)) {
					cnfgGroupOpts.push(obj);
				}
			}

			responseOptions = responseOptions.concat(allcnfgGroupItem,cnfgGroupResponse,cnfgGroupOpts);
			response(responseOptions);
			
		};
		ajaxCnfgGroupReq.open("GET", LOC.search+"?search="+encodeURIComponent(request.term)+sysAdminQuerry, true);
		ajaxCnfgGroupReq.send(); }
	else{
		
		cnfgGroupResponse =  noConfigGroupMsg;
		
		i = -1; len = cnfStatusItems.length;
		while(++i < len) {
			obj = cnfStatusItems[i];
			if((searchTerm.length <= 0) || (obj.label.toLowerCase().indexOf(searchTerm) >= 0)) {
				cnfgGroupOpts.push(obj);
			}
		}
		
		responseOptions = responseOptions.concat(allcnfgGroupItem,cnfgGroupResponse,cnfgGroupOpts);
		response(responseOptions);
	}	
};

//--------------------------------------------------------------------------------------

hwConfiguration.clearKey = function(){
	$("#downloadKeyStatus").hide();
	$("#dwnldKeyDetails").html("");
};

//--------------------------------------------------------------------------------------

hwConfiguration.testKeyProgress = function(){
	var dateGenObj = "",
		dwnldNowBtn = $("<a>").attr({id: "dwnldNowBtn", href: LOC.downloadKey}).addClass("linkButtonFtr textButton right").html(dwnLoadBtnMsg),
		ajaxDownloadKeyProgrss = GetHttpObject();
	ajaxDownloadKeyProgrss.onreadystatechange = function(){
		if(ajaxDownloadKeyProgrss.readyState === 4 && ajaxDownloadKeyProgrss.status === 200){
			data = JSON.parse(ajaxDownloadKeyProgrss.responseText);
			if(data.pending){ 
				$("#generateKey").addClass("disabled");
				$("#downloadKeyStatus").addClass("inProgress").show(); }
			else if(!data.pending){ 
				$("a#generateKey").removeClass("disabled");
				if(typeof data.dateGenerated === undefined){ hwConfiguration.clearKey(); }
				else{ 
					if(data.dateGenerated && data.dateGenerated !== ''){
						dateGenObj =  moment.parseZone(data.dateGenerated);
						$("#dwnldKeyDetails").html("<h2>"+ dwnLoadKeyTtl +" - " + dateGenObj.format("MMMM D, YYYY h:mm A") + "</h2>").append(dwnldNowBtn);
						$("#downloadKeyStatus").removeClass("inProgress").show(); }
					else{ hwConfiguration.clearKey(); } } } } 
		else if(ajaxDownloadKeyProgrss.readyState === 4){ hwConfiguration.clearKey(); $("a#generateKey").removeClass("disabled"); } }; //Unable to load location details
	ajaxDownloadKeyProgrss.open("GET",LOC.keyProgress,true);
	ajaxDownloadKeyProgrss.send();

	if(!$("#scheduleDownloadKey").is(":visible")){ snglSlideFadeTransition("show", $("#scheduleDownloadKey")); pausePropagation = false; } };

//--------------------------------------------------------------------------------------

hwConfiguration.upgradeGroupList = function(){
	if(configScheduleStatus){
		var	tempLiObject = "",
			tempLinkObject = "",
			tempIDObject = "",
			$tempGroupList = "",
			ajaxLoadUpgradeList = GetHttpObject();
		ajaxLoadUpgradeList.onreadystatechange = function()
		{
			if (ajaxLoadUpgradeList.readyState === 4 && ajaxLoadUpgradeList.status === 200){
				data = JSON.parse(ajaxLoadUpgradeList.responseText);
				if(data.groupsList.length > 0){
					$("#scheduledConfigsList, #unscheduledConfigsList").html("");
					$.each(data.groupsList, function(index, val){ 
						$tempGroupList = (val.groupStatus === "scheduled")? $("#scheduledConfigsList") : $("#unscheduledConfigsList");
						tempLiObject = $("<li>").attr({id: "keyCnfg_" + val.groupId});
						tempCheckBxObject = $("<a>").attr({href: "#", class: "checkBox"}).html("&nbsp;");
						tempLinkObject = $("<a>").attr({href: "#", class: "groupDetailLink"}).html(val.groupName);
						if(!val.enableIris){ 
							if(!$("#offlineConfigGroupWrnng").is(":visible")){ $("#offlineConfigGroupWrnng").show(); }
							tempLiObject.addClass("offline"); tempLinkObject.attr("tooltip", "Offline Setting"); };
						tempLiObject.append(tempCheckBxObject, tempLinkObject);
						$tempGroupList.append(tempLiObject); });
					
					if($("#unscheduledConfigsList").find("li").length > 0){ $("#noUnschdldMsg").addClass("hide").hide(); }
					else { $("#unscheduledConfigsList, #unSchdlSelectAllLbl").addClass("hide").hide(); }
					
					if($("#scheduledConfigsList").find("li").length > 0){ $("#noSchdldMsg").addClass("hide").hide(); } 
					else { $("#scheduledConfigsList, #schdlSelectAllLbl").addClass("hide").hide(); } }
				hwConfiguration.testKeyProgress(); } 
			else if(ajaxLoadUpgradeList.readyState === 4){
				alertDialog(systemErrorMsg); }//Unable to load location details
		};
		ajaxLoadUpgradeList.open("GET",LOC.upgradeList,true);
		ajaxLoadUpgradeList.send();
	}
	else { pausePropagation = false; }
};

//--------------------------------------------------------------------------------------

hwConfiguration.extractList = function(cnfgList, context) {
	var result = null,
		buffer = JSON.parse(cnfgList).groupsList;
	
	result = buffer.elements;
	
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;	
};

//--------------------------------------------------------------------------------------

hwConfiguration.loadList = function(){
	$("#configList").paginatetab({
		"url": LOC.list+"?"+sysAdminQuerry,
		/* --- MICRO SERVICE UPDATE REQUIRED --- "itemLocatorURL": "/secure/locatePageWithPayStations.html", */
		"formSelector": "#cnfgGrpFilterForm",
		"submissionFormat": "POST",
		"emptyMessage": noConfigGroupMsg,
		"rowTemplate": unescape($("#configListTemplate").html()),
		"responseExtractor": hwConfiguration.extractList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData");

	$("#configList").on("pageLoaded", function(){ 
		$("ul.pageItems").find("li").each(function(){			
			if(selectedItemLoad){
				if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
					$(this).trigger('click');
					checkVisibleItem($(this), $("ul.pageItems"));
					selectedItemLoad = false; } }
			else if($("#contentID").val() !== '' && $("#groupDetails").is(":visible")){
				if(($(this).attr("id") && $(this).attr("id").match($("#contentID").val())) && !$(this).hasClass("selected")){
					$(this).addClass("selected");
					if($(this).hasClass("actv-scheduled") || $(this).hasClass("actv-unscheduled")){ 
						configStatus = ($(this).hasClass("actv-scheduled"))? "scheduled" : "unscheduled";
						tempOverlayPOSstatus();	}
					else if($(this).hasClass("actv-inactive")){	configStatus = "inactive"; }
					else { configStatus = "active"; }
					tempTestShowEditBtn();
					checkVisibleItem($(this), $("ul.pageItems")); } }}); }); };

//--------------------------------------------------------------------------------------

hwConfiguration.newFormSetup = function(event){
	$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); });
	hwConfiguration.resetGroupForm(event); };

//--------------------------------------------------------------------------------------

hwConfiguration.resetGroupForm = function(){//event) { 
/*	$("#groupDetails").fadeOut("fast", function(){
		document.getElementById("groupSettings").reset();
		$("#formGroupPowerProfile").selectedIndex = 0;
		$("#formGroupPowerProfile").trigger('change')
		$(".optionCheckList * .checkBox").each(function(){ $(this).removeClass("checked"); });
		$("#groupDispName").hide();
		$("#groupDetails").removeClass("editForm").addClass("form addForm").fadeIn(function(){$("#formGroupName").trigger('focus');}); });*/ };

//--------------------------------------------------------------------------------------

hwConfiguration.showObj = function(configGrpData, action){
	//Process JSON
	groupData =	JSON.parse(configGrpData.responseText);
	var itemID = groupData.groupId,
		ovrdData = groupData.groupConfiguration,
		irisOvrdData = (ovrdData.irisSettings)? ovrdData.irisSettings : null,
		bossData = (ovrdData.bossSettings)? ovrdData.bossSettings : null,
		irisCardPrcssngData = (irisOvrdData)? irisOvrdData.cardProcessing : null,
		bossCardPrcssngData = (bossData)? bossData.cardProcessing : null,
		schdlDate = (groupData.scheduleDate && groupData.scheduleDate !== '')?  moment.parseZone(groupData.scheduleDate) : '',
		bossDate = (groupData.settingsReceivedDate && groupData.settingsReceivedDate !== '')?  moment.parseZone(groupData.settingsReceivedDate) : '',
		displayCallBack = function(){ },
		//childFullList = '',
		//childSelectedList = '',
		showOptions = false,
		selectedCount = 0,
		//allSelected = true,
		colPlacement = 0,
		childType = '',
		childStatus =  '',
		childName =  '',
		childID =  '',
		childSerialNmbr = '',
		childActivated = '',
		childDecommissioned = '',
		childHidden = '',
		allPaystationList = '',
		allMissingPaystationList = '',
		//tempString = '',
		optionSlctdStyle = '',
		scrubbedQuerry = '',
		posItem = '',
		posSerial = '',
		posLI = '',
		blankItem = "<a name='blankSpace'>&nbsp</a><br><span class='note'>&nbsp</span>",
		blankLI2 = $("<li>").addClass("detailValue blank col2").html(blankItem),
		blankLI3 = $("<li>").addClass("detailValue blank col3").html(blankItem),
		noPosLI = $("<li>").addClass("onlyCol detailValue blank").html(noPayStationMsg);
	
	bossKeyRequired = groupData.bossKeyRequired;

	$("#actionFlag").val("1");
	$("#mapHolder").html('');
	$("#contentID, #formGroupID").val(itemID);
	$("#titleGroupName, #groupDispName").html(groupData.groupName);
	$("#formGroupName").val(groupData.groupName);
	
	if(bossKeyRequired){ $("#offlineWarning").show(); } else { $("#offlineWarning").hide(); }
	
	if(bossCardPrcssngData){		
		$("#cardProcessingDisplay, #cardProcessingForm").find(".unavailbleArea").hide();
		$("#irisCardProcessOveride, #cardProcessingFormArea").show();
		
		if(ovrdData.cardProcessingOverride){
			$("#cardProcessHeader").html(cardProcessOverideTtl);
			$("#formCardOvrrdHidden").val(true);
			if($("#formCardOvrrd").hasClass("checkBox")){ $("#formCardOvrrd").addClass("checked"); }
			else{ $("#formCardOvrrd").html(cardProcessOverideTtl); }
			$("#formIrisOverRideObj").show().css('opacity', 1);
			$("#formBossSettingObj").hide(); }
		else{
			$("#cardProcessHeader").html(cardProcessBossTtl);
			$("#formCardOvrrdHidden").val(false);
			if($("#formCardOvrrd").hasClass("checkBox")){ $("#formCardOvrrd").removeClass("checked"); }
			else{ $("#formCardOvrrd").html(cardProcessBossTtl); }
			$("#formBossSettingObj").show().css('opacity', 1);
			$("#formIrisOverRideObj").hide(); }	

		if(ovrdData.cardProcessingOverride && irisCardPrcssngData.enableOnlineComm){
			$("#detailIrisEnabled").addClass("activatedService");
			if(bossCardPrcssngData.enableOnlineComm){ $("#formBossDetailIrisEnabled").addClass("activatedService"); } 
			else { $("#formBossDetailIrisEnabled").removeClass("activatedService"); }
			$("#formIrisEnabledHidden").val(true);
			$("#formIrisEnabled").addClass("checked"); 
			$("#irisEnabledGroup").removeClass("inactive");
			$("#offlineWarning").show();
			if(!bossKeyRequired){ $("#offlineWarning").hide(); }; } 
		else if(!ovrdData.cardProcessingOverride && bossCardPrcssngData.enableOnlineComm){ 
			$("#formBossDetailIrisEnabled, #detailIrisEnabled").addClass("activatedService");
			$("#formIrisEnabledHidden").val(true);
			$("#formIrisEnabled").addClass("checked"); 
			$("#irisEnabledGroup").removeClass("inactive");
			if(!bossKeyRequired){ $("#offlineWarning").hide(); }; }
		else{ 
			$("#formBossDetailIrisEnabled, #detailIrisEnabled").removeClass("activatedService");
			if(bossCardPrcssngData.enableOnlineComm){ $("#formBossDetailIrisEnabled").addClass("activatedService"); } 
			$("#formIrisEnabledHidden").val(false);
			$("#formIrisEnabled").removeClass("checked");
			$("#irisEnabledGroup").addClass("inactive");
			$("#offlineWarning").show(); }

		if(ovrdData.cardProcessingOverride && irisCardPrcssngData.enableOnlineProcess){
			$("#detailOnlineCard").addClass("activatedService");
			if(bossCardPrcssngData.enableOnlineProcess){ $("#formBossDetailOnlineCard").addClass("activatedService"); } 
			else { $("#formBossDetailOnlineCard").removeClass("activatedService"); }
			$("#formOnlineCardHidden").val(true);
			$("#formOnlineCard").addClass("checked");
			$("#formOfflineArea").show(); } 
		else if(!ovrdData.cardProcessingOverride && bossCardPrcssngData.enableOnlineProcess){
			$("#formBossDetailOnlineCard, #detailOnlineCard").addClass("activatedService");
			$("#formOnlineCardHidden").val(true);
			$("#formOnlineCard").addClass("checked");
			$("#formOfflineArea").show(); }
		else{ 
			$("#formBossDetailOnlineCard, #detailOnlineCard").removeClass("activatedService");
			if(bossCardPrcssngData.enableOnlineProcess){ $("#formBossDetailOnlineCard").addClass("activatedService"); } 
			$("#formOnlineCardHidden").val(false);
			$("#formOnlineCard").removeClass("checked");
			$("#formOfflineArea").hide(); }

		if(ovrdData.cardProcessingOverride && irisCardPrcssngData.acceptCardsOffline){ 
			$("#detailOfflineCard").addClass("activatedService");
			if(bossCardPrcssngData.acceptCardsOffline){ $("#formBossDetailOfflineCard").addClass("activatedService"); } 
			else { $("#formBossDetailOfflineCard").removeClass("activatedService"); }
			$("#formOfflineCardHidden").val(true);
			$("#formOfflineCard").addClass("checked"); } 
		else if(!ovrdData.cardProcessingOverride && bossCardPrcssngData.acceptCardsOffline){
			$("#formBossDetailOfflineCard, #detailOfflineCard").addClass("activatedService");
			$("#formOfflineCardHidden").val(true);
			$("#formOfflineCard").addClass("checked"); }
		else{ 
			$("#formBossDetailOfflineCard, #detailOfflineCard").removeClass("activatedService");
			if(bossCardPrcssngData.acceptCardsOffline){ $("#formBossDetailOfflineCard").addClass("activatedService"); } 
			$("#formOfflineCardHidden").val(false);
			$("#formOfflineCard").removeClass("checked"); }

		if(ovrdData.cardProcessingOverride && irisCardPrcssngData.acceptPasscard){
			$("#detailOnlineCustomCard").addClass("activatedService");
			if(bossCardPrcssngData.acceptPasscard){ $("#formBossDetailOnlineCustomCard").addClass("activatedService"); } 
			else { $("#formBossDetailOnlineCustomCard").removeClass("activatedService"); }
			$("#formOnlineCustomCardHidden").val(true);
			$("#formOnlineCustomCard").addClass("checked"); } 
		else if(!ovrdData.cardProcessingOverride && bossCardPrcssngData.acceptPasscard){
			$("#formBossDetailOnlineCustomCard, #detailOnlineCustomCard").addClass("activatedService");
			$("#formOnlineCustomCardHidden").val(true);
			$("#formOnlineCustomCard").addClass("checked"); }
		else{ 
			$("#formBossDetailOnlineCustomCard, #detailOnlineCustomCard").removeClass("activatedService");
			if(bossCardPrcssngData.acceptPasscard){ $("#formBossDetailOnlineCustomCard").addClass("activatedService"); } 
			$("#formOnlineCustomCardHidden").val(false);
			$("#formOnlineCustomCard").removeClass("checked"); } }
	else {
		$("#irisCardProcessOveride, #cardProcessingFormArea").hide();
		$("#cardProcessHeader").html(cardProcessBossTtl);
		$("#cardProcessingDisplay, #cardProcessingForm").find(".unavailbleArea").show();
		$("#cardProcessingForm").find("input").each(function(){ $(this).val("null"); });
	}
	
	
	if(bossDate !== ''){
		$("#detailReceivedDate").html(bossDate.format("MMMM D, YYYY h:mm A")); }
	
	if(schdlDate !== ''){
		$("#detailScheduledDate").html(schdlDate.format("MMMM D, YYYY h:mm A"));
		if(!schdlDate.isBefore()){
			$("#formScheduledDate").val(schdlDate.format("MM/DD/YYYY"));
			$("#formScheduledTime").autominutes("setTime", schdlDate.format("HH:mm")); 
			$("#unscheduleSwitch").show(); }
		else{
			$("#formScheduledDate").val('');
			$("#formScheduledTime").autominutes("setTime", '');
			$("#unscheduleSwitch").hide(); } }
	else{ 
		$("#detailScheduledDate").html(unscheduleTtl);
		$("#formScheduledDate").val('');
		$("#formScheduledTime").autominutes("setTime", '');
		$("#unscheduleSwitch").hide(); }

	//PAYSTATIONS
	$("#psChildList").html('');
	$("#unrecognizedPOS").html('');
	
	if(groupData.unrecognizedDevices){
		allMissingPaystationList = groupData.unrecognizedDevices;
		$("#unrecognizedPOSArea").show();
		if(allMissingPaystationList instanceof Array === false){ allMissingPaystationList = [allMissingPaystationList]; }
		$.each(allMissingPaystationList, function(index, val){
			$("#unrecognizedPOS").append("<li>"+val.deviceId+"</li>"); }); }
	else{ $("#unrecognizedPOSArea").hide(); }
	
	if(groupData.deviceList){
		allPaystationList = groupData.deviceList;
		if(allPaystationList instanceof Array === false){ allPaystationList = [allPaystationList]; }
		
		$.each(allPaystationList, function(index, val){
			childType = val.paystationType;
			childStatus =  (configStatus === "scheduled" || configStatus === "unscheduled" || configStatus === "inactive")? configStatus : val.upgradeStatus;
			childName =  val.payStationName;
			childID =  val.deviceId;
			childSerialNmbr = val.serialNumber;
			childActivated = val.isPosActivated;
			childDecommissioned = val.isPosDecommissioned;
			childHidden = val.isPosHidden;
			colPlacement = selectedCount%3+1;
			optionSlctdStyle = slctdOpacityLevel;
			scrubbedQuerry = scrubQueryString(document.location.search.substring(1), "all");
			childStatus = (!childActivated || childDecommissioned || childHidden)? 'inactive' : childStatus;
			childHidden = (childHidden)? 'hddn-true' : '' ;
			posItem = (posPermission)? $("<a>").attr("href", detailsUrl+'payStationList.html?itemID='+childID+'&pgActn=display&tabID=status&'+scrubbedQuerry).html(childName) : $("<a>").attr("name", childName).html(childName);
			posSerial = $("<span>").addClass("note").html(childSerialNmbr);
			posLI = $("<li>").addClass("col"+colPlacement+" detailValue posT"+childType+"-0 " + cnfgStats[childStatus].listClass + ' ' + childHidden).append(posItem).append("<br>").append(posSerial);
			$("#psChildList").append(posLI);
			selectedCount++; });
		
		if(colPlacement === 1 && selectedCount > 1){ $("#psChildList").append(blankLI2).append(blankLI3); }
		else if(colPlacement === 2){ $("#psChildList").append(blankLI3); }
		
		if(selectedCount === 0){ $("#psChildList").append(noPosLI); }
		if(selectedCount === 1){ $("#psChildList").find("li").addClass("onlyCol").removeClass("col1"); } }
	else { $("#psChildList").append(noPosLI); }
	
	$("#formPayStations").html($("#PaystationChildren").html());
	
	if(action === "display"){
		
		tempTestShowEditBtn();
		
		displayCallBack = function(){ 
			var payStationMarkers = [];
			if(groupData.mapInfo && groupData.mapInfo.length > 0){
				var payStationList = groupData.mapInfo;
				if(payStationList instanceof Array === false){ payStationList = [payStationList]; }
				for(var x = 0; x < payStationList.length; x++){ 
//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
					var tempStatus = (payStationList[x].isUpdateInProgress)? 2 : 0 ; //severity
					if(payStationList[x].latitude && payStationList[x].latitude !== '' && payStationList[x].latitude !== null){
						payStationMarkers.push(new mapMarker(
							payStationList[x].latitude, 
							payStationList[x].longitude, 
							payStationList[x].name,
							payStationList[x].payStationType, 
							payStationList[x].randomId,
							tempStatus,'','','','','','',
							payStationList[x].serialNumber )); } 
					if(payStationMarkers.length === 0){payStationMarkers = "noData";}
					initMapStatic(payStationMarkers); }
				} 
			else { initMapStatic("noData"); }
			
			pausePropagation = false; }; 
			
		$(".mainContent").removeClass("form editForm addForm"); }
	else if(action === "edit") {
		$(".hdrButtonIcn.btnEditGroup").hide();
		testFormEdit($("#groupSettings"));
		$(".mainContent").removeClass("addForm").addClass("form editForm");
		displayCallBack = function(){ $("#formRouteName").trigger('focus'); pausePropagation = false; }; }
	
	$("#mainContent").removeClass("edit"); //Removes any Edit flags thrown by Date and Time changes during setup.
	snglSlideFadeTransition("show", $("#groupDetails"), displayCallBack);
	$(document.body).scrollTop(0);
};

//--------------------------------------------------------------------------------------

hwConfiguration.loadSelected = function(itemID, action){
	var hideCallBack = '';

	if($("#contentID").val() === itemID){
		if(action === "edit" && !$(".mainContent").hasClass("editForm")){
			testFormEdit($("#groupSettings"));
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); $(".hdrButtonIcn.btnEditGroup").hide(); };
			slideFadeTransition($("#groupDetails"), $("#groupDetails"), hideCallBack); } //payStationSelectList(itemID, '', '', false, ''); }
		else if(action === "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); };
			slideFadeTransition($("#groupDetails"), $("#groupDetails"), hideCallBack); }
		pausePropagation = false; }
	else {
		var $this = $("#cnfgGroup_"+itemID);
		if($("#configList").find("li.selected").length){ $("#configList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");
		if($this.hasClass("actv-scheduled") || $this.hasClass("actv-unscheduled")){
			configStatus = ($this.hasClass("actv-scheduled"))? "scheduled" : "unscheduled"; }
		else if($this.hasClass("actv-inactive")){ configStatus = "inactive"; }
		else if($this.length){ configStatus = "active"; }
		else{ configStatus = null; }

		
		hideCallBack = function(){ //hwConfiguration.showObj('', action); };
			var ajaxLoadItem = GetHttpObject();
			ajaxLoadItem.onreadystatechange = function()
			{
				if (ajaxLoadItem.readyState === 4 && ajaxLoadItem.status === 200){
					hwConfiguration.showObj(ajaxLoadItem, action);
				} else if(ajaxLoadItem.readyState === 4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			}; //"&"+document.location.search.substring(1)
			ajaxLoadItem.open("GET",LOC.details+"?"+sysAdminQuerry+"&groupId="+itemID,true);
			ajaxLoadItem.send(); };
		
		if($("#scheduleDownloadKey").is(":visible")){ snglSlideFadeTransition("hide", $("#scheduleDownloadKey"), hideCallBack);  }
		else if($("#groupDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#groupDetails"), hideCallBack); }
		else{ hideCallBack(); } } };

//--------------------------------------------------------------------------------------

hwConfiguration.hideSelected = function($this){
	if($("#formBulkGroupIds").val() === ""){ snglSlideFadeTransition("hide", $("#groupDetails"), hwConfiguration.upgradeGroupList); }
	else{ slideFadeTransition($("#groupDetails"), $("#scheduleDownloadKey")); }
	$("#contentID").val(''); 
	$("#mainContent").removeClass("edit");
	$(".mainContent").removeClass("form editForm");
	configStatus = null;
	if($this){$this.toggleClass("selected"); } 
	else{ if($("#configList").find("li.selected").length){ $("#configList").find("li.selected").toggleClass("selected"); } } };


//--------------------------------------------------------------------------------------

hwConfiguration.verifyDelete = function(){//itemID){
	// verify delete then call hwConfiguration.delete(itemID);
};

//--------------------------------------------------------------------------------------

hwConfiguration.delete = function(){//itemID){
	// delete group;
};

//--------------------------------------------------------------------------------------

 hwConfiguration.schdlSelectGroupItem = function(action, $item){	
	var $inputObj = $("#formBulkGroupIds"),
		itemID = $item.attr("id").split("_")[1];
	if(action === "select"){
		$item.addClass("selected");
		$item.find(".checkBox").addClass("checked");
		if($inputObj.val() === ""){ $inputObj.val(itemID).trigger('change'); }
		else{ if($inputObj.val().search(itemID) < 0){ $inputObj.val($inputObj.val()+","+itemID).trigger('change'); }	} }
	else{
		$item.removeClass("selected");
		$item.find(".checkBox").removeClass("checked");	
		$inputObj.val(stringArrayValCleanUp($inputObj.val(), itemID)); } }

//--------------------------------------------------------------------------------------

hwConfiguration.selectionCheck = function(objID, eventAction){
	var itemID = objID.split("_")[1],
		$this = $("#cnfgGroup_"+itemID),
		cleanQuerryString = '';
	pausePropagation = true;
//	if(eventAction == "delete"){ hwConfiguration.verifyDelete(itemID); }
	if(eventAction === "hide"){ 
		cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
		crntPageAction = "";
		history.pushState(null, null, location.pathname+"?"+cleanQuerryString);
		hwConfiguration.hideSelected($this); }
	else { 
		if($("#contentID").val() !== itemID || (eventAction === "edit" && !$(".mainContent").hasClass("form")) || (!$(".mainContent").hasClass(eventAction) && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: itemID, action: eventAction };
			cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+itemID+"&pgActn="+stateObj.action);
			hwConfiguration.loadSelected(itemID, stateObj.action); }} };

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
hwConfiguration.popAction = function(tempItemID, tempEvent, stateObj){
	if(stateObj === null || (tempEvent === "display" && tempItemID === "New")){ hwConfiguration.hideSelected(); }
//	else if(tempEvent === "Add"){ newRouteForm(); }
	else { hwConfiguration.loadSelected(tempItemID, tempEvent);  } };
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

hwConfiguration.main = function(updateFilterObj, tempID, tempEvent) {
	sysAdminQuerry = (typeof curCustomerID !== 'undefined')? "&customerID="+curCustomerID : "";
	if(!updateFilterObj){ hwConfiguration.loadList(); }

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	$("#contentID").val('');
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempID = '',
			tempEvent = '';
		if(stateObj !== null){
			tempID = stateObj.itemObject;
			tempEvent = stateObj.action; }
		hwConfiguration.popAction(tempID, tempEvent, stateObj); };
	
	if(!popStateEvent && (typeof tempID !== undefined && tempID !== '')){ hwConfiguration.popAction(tempID, tempEvent, ''); }
	else{ hwConfiguration.upgradeGroupList(); }
///////////////////////////////////////////////////
	
	var optsTime = { "militaryMode": false },
		//optsSecTime = { "militaryMode": false, "isSeconds": true },
		hideCallBack = '';

	createDatePicker("#formScheduledDate", {"minDate": 0 });
	$("#formScheduledTime").autominutes(optsTime);
	
	createDatePicker("#formBlkScheduledDate", {"minDate": 0 });
	$("#formBlkScheduledTime").autominutes(optsTime);
	
	$(document.body).on("click", "#configList * li", function(event){ event.preventDefault();
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display",
			itemID = $(this).attr("id");
		if(pausePropagation !== true){ hwConfiguration.selectionCheck(itemID, actionEvent); } });
	
	$(document.body).on("click", ".cnfgKeyList * a", function(event){ event.preventDefault();
		var itemType = ($(this).hasClass("checkBox"))? "select" : "detail",
			$listItem = $(this).parent("li"),
			itemID = $listItem.attr("id"),
			$listSlctAll = ($(this).parents("ul").attr("id") === "scheduledConfigsList")? $("#schdlSelectAllLbl") : $("#unSchdlSelectAllLbl") ;
		if(itemType === "select"){ 
			if(!$(this).hasClass("checked")){ hwConfiguration.schdlSelectGroupItem("select", $listItem); }
			else{ $listSlctAll.find(".checkBox").removeClass("checked"); hwConfiguration.schdlSelectGroupItem("unselect", $listItem); } }
		else if(pausePropagation !== true){ hwConfiguration.selectionCheck(itemID, "display"); } });

	$(document.body).on("click", "#unSchdlSelectAllLbl, #schdlSelectAllLbl", function(event){ event.preventDefault();
		var actionEvent = ($(this).find(".checkBox").hasClass("checked"))? "selectAll" : "unSelectAll",
			itemID = $(this).attr("id"),
			$listObj = (itemID === "unSchdlSelectAllLbl")?  $("#unscheduledConfigsList") : $("#scheduledConfigsList") ;
		$listObj.find("li").each(function(index){ 
			if(actionEvent === "selectAll"){ 
				if(!$(this).find(".checkBox").hasClass("checked")){ hwConfiguration.schdlSelectGroupItem("select", $(this)); } }
			else{ hwConfiguration.schdlSelectGroupItem("unselect", $(this)); } });	});
	
	$(document.body).on("click", "#toggleSelectedView", function(event){ event.preventDefault();
		if($(this).find(".checkBox").hasClass("checked")){
			$("#scheduleConfigList").find(".checkboxLabel").each(function(){ if(!$(this).find(".checkBox").hasClass("checked")){ $(this).hide(); } })
			$(".cnfgKeyList").addClass("selectedOnly"); }
		else{ 
			$("#scheduleConfigList").find(".checkboxLabel").show();
			$(".cnfgKeyList").removeClass("selectedOnly"); } });
	
	$("#groupBulkSchedule").on("click", "#bulkScheduleBtn", function(event){ event.preventDefault();
		var params = '', ajaxBlkScheduleGroup = '';					
		params = $("#groupBulkSchedule").serialize();
		ajaxBlkScheduleGroup = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxBlkScheduleGroup.onreadystatechange = function(){
			if (ajaxBlkScheduleGroup.readyState === 4 && ajaxBlkScheduleGroup.status === 200){
				var response = ajaxBlkScheduleGroup.responseText.split(":");
				if(response[0] === "true"){
					noteDialog(savedMsg);
					setTimeout(function(){ window.location.reload(); }, 500); } }
			else if(ajaxBlkScheduleGroup.readyState === 4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxBlkScheduleGroup.open("POST",LOC.bulkSchedule+"?"+sysAdminQuerry,true);
		ajaxBlkScheduleGroup.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxBlkScheduleGroup.send(params);	});

	
	$(document.body).on("click", "a#generateKey", function(event){
		event.preventDefault(); event.stopImmediatePropagation();
		var tempData = "",
			$that = $(this);
		if(configScheduleStatus && !$(this).hasClass("disabled")){
			
			ajaxGenerateKey = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
			ajaxGenerateKey.onreadystatechange = function(){
				if(ajaxGenerateKey.readyState === 4 && ajaxGenerateKey.status === 200){
					var data = JSON.parse(ajaxGenerateKey.responseText);
					if(data === "Success"){ 
						$that.addClass("disabled");
						if($("#dwnldKeyDetails").is(":visible")){ $("#dwnldKeyDetails").fadeOut(function(){
							$("#dwnldKeyDetails").html(""); $("#downloadKeyStatus").addClass("inProgress"); }); }
						else{ $("#downloadKeyStatus").addClass("inProgress"); snglSlideFadeTransition("show", $("#downloadKeyStatus")); }} }
				else if(ajaxGenerateKey.readyState === 4){ alertDialog(failedKeyGenerateMsg); } };
			ajaxGenerateKey.open("PUT",LOC.generateKey,true);
			ajaxGenerateKey.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			ajaxGenerateKey.send(); } });
	
	$(document.body).on("click", "a.reload", function(event){
		event.preventDefault(); event.stopImmediatePropagation(); hwConfiguration.testKeyProgress(); });
	
	$(document.body).on("click", "a.btnEditGroup", function(event){
		if(pausePropagation !== true){
			if(!$(this).hasClass("formAction")){
				event.preventDefault(); event.stopImmediatePropagation();
				testFormEdit($("#groupSettings"));
				var itemID = ($(this).hasClass("hdrButtonIcn"))? "_"+$("#contentID").val() : $(this).attr("id");
				if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){ hwConfiguration.selectionCheck(itemID, "edit"); }); }
				else{ hwConfiguration.selectionCheck(itemID, "edit"); }}}
	});
		
/*	$("#btnAddGroup").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			var stateObj = { action: "Add" };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID=New&pgActn=Add");
			hwConfiguration.newFormSetup(event); }
	});*/
	
	$(document.body).on("click", "#formCardOvrrdLabel", function(event){ event.preventDefault();
		if($(this).find(".checkBox").hasClass("checked")){
			hideCallBack = function(){ 
				var tempBossData = groupData.groupConfiguration.bossSettings.cardProcessing;
				if(tempBossData.enableOnlineComm){ 
					$("#formIrisEnabledHidden").val("true"); $("#formIrisEnabled").addClass("checked"); $("#irisEnabledGroup").removeClass("inactive"); }
				if(tempBossData.enableOnlineProcess){ 
					$("#formOnlineCardHidden").val("true"); $("#formOnlineCard").addClass("checked"); $("#formOfflineArea").show(); }
				if(tempBossData.acceptCardsOffline){ 
					$("#formOfflineCardHidden").val("true"); $("#formOfflineCard").addClass("checked"); }
				if(tempBossData.enableOnlineProcess){ 
					$("#formOnlineCardHidden").val("true"); $("#formOnlineCard").addClass("checked"); }
				if(tempBossData.acceptPasscard){ 
					$("#formOnlineCustomCardHidden").val("true"); $("#formOnlineCustomCard").addClass("checked"); } };
			slideFadeTransition($("#formBossSettingObj"), $("#formIrisOverRideObj"), hideCallBack); }
		else{ 
			hideCallBack = function(){ 
				$("#irisEnabledGroup").addClass("inactive");
				$("#formIrisEnabledHidden, #formOfflineCardHidden, #formOnlineCardHidden, #formOnlineCustomCardHidden").val("false"); 
				$("#formIrisEnabled, #formOfflineCard, #formOnlineCard, #formOnlineCustomCard").removeClass("checked");
				$("#formOfflineArea").hide(); };
			slideFadeTransition($("#formIrisOverRideObj"), $("#formBossSettingObj"), hideCallBack); } });
	
	$(document.body).on("click", "#formIrisEnabledLabel", function(event){ event.preventDefault();
		if($(this).find(".checkBox").hasClass("checked")){
			$("#irisEnabledGroup").removeClass("inactive");
			if(!bossKeyRequired){ snglSlideFadeTransition("hide", $("#offlineWarning")); } }
		else{ 
			if(!$("#offlineWarning").is(":visible")){ snglSlideFadeTransition("show", $("#offlineWarning")); }
			$("#irisEnabledGroup").addClass("inactive");
			$("#formOfflineCardHidden, #formOnlineCardHidden, #formOnlineCustomCardHidden").val("false"); 
			$("#formOfflineCard, #formOnlineCard, #formOnlineCustomCard").removeClass("checked");
			$("#formOfflineArea").slideUp(); } });

	$(document.body).on("click", "#formOnlineCardLabel", function(event){ event.preventDefault();
		if(!$("#irisEnabledGroup").hasClass("inactive")){
			if($(this).find(".checkBox").hasClass("checked")){
				$("#formOfflineArea").slideDown(); }
			else{ $("#formOfflineArea").slideUp(function(){ $("#formOfflineCardHidden").val("false"); $("#formOfflineCard").removeClass("checked"); }); } } });
	
	$(document.body).on("click", "#formUnscheduleLabel", function(event){ event.preventDefault(); 
		if($(this).find(".checkBox").hasClass("checked")){
			$("#formScheduledDate").val('').prop("disabled", true).addClass("inactive");
			$("#formScheduledTime").autominutes("resetTime").prop("disabled", true).addClass("inactive");}
		else{ 
			$("#formScheduledDate").prop("disabled", false).removeClass("inactive");
			$("#formScheduledTime").prop("disabled", false).removeClass("inactive");} });
	
	$("#groupAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"persistToHidden": false,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"remoteFn": hwConfiguration.autoComplete })
	.on("itemSelected", function(){//event, ui) {
		var slctdCategory = $(this).autoselector("getSelectedObject").category;
		var slctValue = $(this).autoselector("getSelectedValue");
		if(slctValue !== '-1') { 
			if(slctdCategory === configGroupCat){
				hwConfiguration.selectionCheck("_"+slctValue, "display");
				itemSelectedID = slctValue;
				
				if(!$("#cnfgGroup_"+slctValue).is(":visible")){
					/* --- MICRO SERVICE CHANGE IS REQUIRED --- updateFilter('', slctValue, "searchDisplay");*/ }
				else{ 
					$("#cnfgGroup_"+slctValue).addClass("selected");
					checkVisibleItem($("#cnfgGroup_"+slctValue), $("#configList")); }
				$("#groupStatusVal").val('all'); }
			else {
				$("#groupStatusVal").val(slctValue);
				if($(".menuList > li").is(":visible")){ $("#configList").paginatetab("reloadData"); }
				else{ hwConfiguration.loadList(); } }			
			if($(this).parents("ul.filterForm") && slctValue !== ''){ filterQuerry = "&filterValue="+slctValue; }
			$(this).trigger('blur');
		}
		return false; })
	.on("change", function(){
		var slctName = $(this).autoselector("getSelectedObject");
		if($(this).val()==='' && slctName.label !==''){$(this).val(slctName.label);} });
	
	$("#groupSettings").on("click", ".save", function(event){ event.preventDefault();
		var params = '', ajaxSaveGroup = '', scheduleDateTimeVal = '';					
		params = $("#groupSettings").serialize();
		ajaxSaveGroup = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxSaveGroup.onreadystatechange = function(){
			if (ajaxSaveGroup.readyState === 4 && ajaxSaveGroup.status === 200){
				var response = ajaxSaveGroup.responseText.split(":");
				if(response[0] === "true"){
					noteDialog(savedMsg);
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "itemID", "pgActn"]);
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry+"&pgActn="+crntPageAction+"&itemID="+$("#contentID").val();
					setTimeout(function(){window.location = newLocation}, 500); } }
			else if(ajaxSaveGroup.readyState === 4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveGroup.open("POST",LOC.save+"?"+sysAdminQuerry,true);
		ajaxSaveGroup.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveGroup.send(params);	});

	$("#groupSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
	
/*	var $select = $("#formGroupPowerProfile"),
    	$slider = $("#powerSldr").slider({
		  min: 1,
		  max: 3,
		  range: "min",
		  animate: true,
		  value: $select[ 0 ].selectedIndex + 1,
		  slide: function( event, ui ) {
			$select[ 0 ].selectedIndex = ui.value - 1;
			 $select.trigger('change');
		  }
		});
	
    $select.on("change", function() {
		var powerModeClass = ["solar", "powered", "custom"];
    	$slider.slider( "value", this.selectedIndex + 1 );
		var tempSelected = powerModeClass[this.selectedIndex];
		$("#powerProfileArea").fadeOut(function(){ 
			$("#powerProfileArea").removeClass("solar powered custom").addClass(tempSelected).fadeIn(); });
    });
	
	$(".sldrLabel").on('click', function(){
		$select[0].selectedIndex = Number($(this).attr("val")) - 1;
		$select.trigger('change');
	});



		
	var prvwImg = function(input) 
	{
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#wlcmImgPreview').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#formWlcmImg").on('change', function(){ prvwImg(this);	});*/
};

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
/*
var hwConfigAccess = {};

hwConfigAccess.resetAxsForm = function(event) {
	$("#accessDetails").fadeOut("fast", function(){
		document.getElementById("accessSettings").reset();
		$("#accessProfile * .checkBox").each(function(){ $(this).removeClass("checked"); });
		$("#accessDispName").hide();
		$("#accessDetails").removeClass("editForm").addClass("form addForm").fadeIn(function(){$("#formAxsName").trigger('focus');}); }); }

hwConfigAccess.showObj = function(configGrpData, action){
	var itemID = configGrpData.split("_");
	$("#contentID").val(itemID[1]);
	$("#mainContent").removeClass("edit");
	$("#accessDispName").show();
	$("#accessDetails").removeClass("form editForm addForm").fadeIn();
	$(document.body).scrollTop(0);
}

hwConfigAccess.loadObj = function(objId, eventAction){
	$("#accessDetails").fadeOut("fast");
	hwConfigAccess.showObj(objId, eventAction);
}

hwConfigAccess.selectionCheck = function(objID, eventAction){
	var itemID = objID.split("_"),
		$this = $("#axs"+itemID[1]);
	if(eventAction === "hide"){ 
		$this.toggleClass("selected"); $("#accessDetails").fadeOut(); $("#contentID").val(''); $("#mainContent").removeClass("edit"); }
	else { 
		if($("#contentID").val() != itemID[1] || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			if($("#accessList").find("li.selected").length){ $("#accessList").find("li.selected").toggleClass("selected"); }
			$this.toggleClass("selected");
			if(eventAction == "edit" && $("#contentID").val() == itemID[1]){
				$("#accessDetails").fadeOut(function(){$(".mainContent").addClass("form editForm"); testFormEdit($("#accessSettings")); $("#accessDetails").fadeIn();});} 
			else if(eventAction == "display" && $("#contentID").val() == itemID[1]){
				$("#accessDetails").fadeOut(function(){$("#accessDispName").show(); $(".mainContent").removeClass("form editForm"); $("#mainContent").removeClass("edit"); $("#accessDetails").fadeIn();});} 
			else {hwConfigAccess.loadObj(itemID[1], eventAction); }}} }

hwConfigAccess.main = function() {
	
	$(document.body).on("click", "#accessList > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var itemID = $(this).attr("id");	
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ hwConfigAccess.selectionCheck(itemID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ hwConfigAccess.selectionCheck(itemID, actionEvent); } });
	
	$("#btnAddAccess").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }

		else{ 
			$(".pageItems").find("li.selected").each(function(count){ $(this).removeClass("selected"); })
			hwConfigAccess.resetAxsForm(); }
	});
	
	$("#accessForm").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });

}
*/
