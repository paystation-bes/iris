// JavaScript Document
var termsAlertOptions = "";
function logIN()
{
	$("#submit").on('focus', function(){
		$(this).addClass("active");
	}).on('blur', function(){
		$(this).removeClass("active");
	});
	
	$("#login").on('submit', function(){
		var $userName = $('input[name=j_username]');
		var $password = $('input[name=j_password]');
		if($userName.val() == '' || $password.val() == '')
		{
			$userName.trigger("focus").val("").trigger("keyup").trigger("blur");
			$password.trigger("focus").val("").trigger("keyup").trigger("blur");
			
			alertDialog(alertLogin);
			return false;
		}
	});
	
	$('#username, #password').inputfit();
	
	closeAlertMessage();
}

function terms()
{	$("#saDetails").scroll(function(){
		var scrollPos = $(this).scrollTop();
		var scrollTotal = $("#legalDisclaimer").height() - $(this).outerHeight();
		if(scrollPos >= scrollTotal)
		{
			$("#saAccept").attr("disabled", false).removeClass("disabled");
		}
	});

	setupCharBlocker("#name,#title", characterTypes.stndrdChrctr, function() { return stndrdChrctrsOnlyMsg; });
	setupCharBlocker("#organization", characterTypes.prntableChrctr, function() { return prntableChrctrsOnlyMsg; });
	
	$("#saDecline").on('click', function(){
		window.location = "/secure/serviceAgreementDecline.html";
	});
	
	termsAlertOptions = { 
		classes: { "ui-dialog": "popAlert" },
		width: 450,
		maxHeight:  600,
		position: {my: "center center-100"},
		resizable: false,
		closeOnEscape: true,
		hide: "fade" }

	
	closeAlertMessage();
}

function passWord()
{
	var psswrdPass = null;
	var psswrd = '';
	
	$("#c_newPassword").attr("disabled", true);

	$("#newPassword").on('keyup', function(){
		psswrd = $(this).val();

		if(psswrd.length >= 7 && psswrd.length <= 15){ psswrdPass = testPasswrd(psswrd);}
		else if(psswrd.length > 15){psswrdPass = false;}
		else {psswrdPass = null;}
		
		if(psswrdPass == true){$("#passwordIcn").css("background-position", "left -78px"); $('#passwordIcn, #newPassword, #newPasswordLabel').attr("ref", "#passwordNoteValid"); $("#c_newPassword").attr("disabled", false);}
		else if(psswrdPass == false){$("#passwordIcn").css("background-position", "left -38px"); $('#passwordIcn, #newPassword, #newPasswordLabel').attr("ref", "#passwordNote"); $("#c_newPassword").attr("disabled", true);}
		else{$("#passwordIcn").css("background-position", "left 2px"); $('#passwordIcn, #newPassword, #newPasswordLabel').attr("ref", "#passwordNote"); $("#c_newPassword").attr("disabled", true);}
	});
	
	$("#c_newPassword").on('keyup', function(){
		var c_psswrd = $(this).val();

		if(c_psswrd.length == psswrd.length)
		{
			if(c_psswrd == psswrd){
				$("#c_passwordIcn").css("background-position", "left -78px"); 
				$("#changePassword").attr("disabled", false).removeClass("disabled"); 
				$('#c_passwordIcn').attr("ref", "#c_passwordNoteValid");}
			else {
				$("#c_passwordIcn").css("background-position", "left -38px"); 
				$("#changePassword").attr("disabled", true).addClass("disabled"); 
				$('#c_passwordIcn').attr("ref", "#c_passwordNote");}
		} 
		else if(c_psswrd.length > psswrd.length) {
			$("#c_passwordIcn").css("background-position", "left -38px"); 
			$("#changePassword").attr("disabled", true).addClass("disabled"); 
			$('#c_passwordIcn').attr("ref", "#c_passwordNote");}
		else {
			$("#c_passwordIcn").css("background-position", "left 2px"); 
			$("#changePassword").attr("disabled", true).addClass("disabled"); 
			$('#c_passwordIcn').attr("ref", "#c_passwordNote");}
	});
	
	$("#passwordChange").on('submit', function(){
		if($("#c_newPassword").val() != $("#newPassword").val())
		{
			alertDialog(alertPassword);
			return false;
		}
	});
	
	closeAlertMessage();
	
	$("#resetPasswordBtn").on('click', function(event){
		$("#resetPasswordBtnArea").fadeOut(function(){
			if($("#formHidePassword").length){ $("#formHidePassword").val(false); }
			$("#newPassword, #c_newPassword").val("");
			$("#c_newPassword").attr("disabled", true);
			$("#resetPassword").fadeIn("slow");
		});
		
	});
	
	$("#cancelResetBtn").on('click', function(event){
		$("#resetPassword").fadeOut(function(){
			if($("#formHidePassword").length){ $("#formHidePassword").val(true); }
			$("#newPassword, #c_newPassword").val("");
			$("#c_newPassword").attr("disabled", true);
			$("#resetPasswordBtnArea").fadeIn("slow");
		});
	});
}
