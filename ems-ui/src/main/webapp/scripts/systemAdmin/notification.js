var PTTRN_DATE = "mm/dd/yy",
	callbackFN = {display: displayNotificationDetail, edit: displayNotificationEditForm};

//--------------------------------------------------------------------------------------

function hideNotification(){
	snglSlideFadeTransition("hide", $("#notificationDetail, #notificationForm"));
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($("#currentList").find("li.selected").length){$("#currentList").find("li.selected").toggleClass("selected");} }

//--------------------------------------------------------------------------------------

function notificationSelectionCheck(itemID, eventAction, callback)
{
	itemID = itemID.split("_")[1];
	var $this = $("#note_"+itemID);
	if(eventAction == "hide"){ 
		crntPageAction = "";
		history.pushState(null, null, location.pathname);
		hideNotification($this); }
	else { 
		if($("#contentID").val() != itemID || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: itemID, action: eventAction };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			loadNotification(stateObj.itemObject, stateObj.action, callback); } } }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popNotificationAction(tempItemID, tempEvent, stateObj){ 
	if(stateObj === null || (tempEvent === "display" && tempItemID === "New")){ hideNotification(); }
	else if(tempEvent === "add"){ displayNotificationAddForm(); }
	else { loadNotification(tempItemID, tempEvent, callbackFN[tempEvent]);  } }
	
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function notification(tempID, tempEvent)
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popNotificationAction(tempItemID, tempEvent, stateObj); }

if(!popStateEvent && (tempEvent !== undefined && tempEvent !== '')){ popNotificationAction(tempID, tempEvent); }
///////////////////////////////////////////////////	msgResolver.mapAttribute("routeName", "#formRouteName");

	msgResolver.mapAttribute("title", "#formNotificationTitle");
	msgResolver.mapAttribute("message", "#formNotificationMessage");
	msgResolver.mapAttribute("link", "#formNotificationMessageLink");
	msgResolver.mapAttribute("startDate", "#formNotificationStartDate");
	msgResolver.mapAttribute("startTime", "#formNotificationStartTime");
	msgResolver.mapAttribute("endDate", "#formNotificationEndDate");
	msgResolver.mapAttribute("endTime", "#formNotificationEndTime");
	
//	var $notificationsMenu = $("#systemNotification,#menu_pageContent");
	
	coupleDateComponents(
		createDatePicker("#formNotificationStartDate"),
		$("#formNotificationStartTime").autominutes({
			"militaryMode": false
		}),
		createDatePicker("#formNotificationEndDate"),
		$("#formNotificationEndTime").autominutes({
			"militaryMode": false
		})
	);
	
	$("[id^=btnAddNotification]").on("click", function(event) {
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			var stateObj = { itemID: "New", action: "add" };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemID+"&pgActn="+stateObj.action);
			displayNotificationAddForm();
			$("#formNotificationTitle").trigger('focus'); return false; }
	});
	
	$("#notificationManagementForm")
		.on("click", ".save", saveNotificationUpdates)
		.on("click", ".cancel", cancelNotificationUpdates);
	
	var $affectPreviewElem = $("#formNotificationTitle, #formNotificationMessage, #formNotificationMessageLink");
	$affectPreviewElem.on("change", previewNotification);
	$affectPreviewElem.on("keyup", previewNotification);
	
	$(document.body).on("click", "#currentNotifications > li, #pastNotifications > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var itemID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ notificationSelectionCheck(itemID, actionEvent, displayNotificationDetail); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false; }}, 100); }
		else{ notificationSelectionCheck(itemID, actionEvent, displayNotificationDetail); } });
		
	$(document.body).on("click", "a.view", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		if($(this).parent().hasClass("innerBorder")){
			notificationSelectionCheck(itemID, "display", displayNotificationDetail);} });

	$(document.body).on("click", "a.edit", function(event) {
		event.preventDefault(); event.stopImmediatePropagation();
		var itemID = $(this).attr("id");
		if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){notificationSelectionCheck(itemID, "edit", displayNotificationEditForm);}); }
		else{ notificationSelectionCheck(itemID, "edit", displayNotificationEditForm); } });
	
	$(document.body).on("click", "a.delete", function(event) {
		event.preventDefault();
		var itemID = $(this).attr("id").split("_");
		deleteNotification(itemID[1]);
		
		return false;
	});
	
	$("#currentList").on("click", ".sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortRouteList = GetHttpObject();
		ajaxSortRouteList.onreadystatechange = function()
		{
			if (ajaxSortRouteList.readyState==4 && ajaxSortRouteList.status == 200){
				$("#currentList").html(ajaxSortRouteList.responseText);
				balanceNotificationList();
				scrollListWidth(["currentNotifications"]);
			} else if(ajaxSortRouteList.readyState==4){
				alertDialog(sortFail);
			}
		};
		ajaxSortRouteList.open("GET","/systemAdmin/notifications/sortCurrentNotificationList.html?notificationType=current&sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortRouteList.send();
		
	});
	
	$("#pastList").on("click", ".sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortRouteList = GetHttpObject();
		ajaxSortRouteList.onreadystatechange = function()
		{
			if (ajaxSortRouteList.readyState==4 && ajaxSortRouteList.status == 200){
				$("#pastList").html(ajaxSortRouteList.responseText);
				balanceNotificationList();
				scrollListWidth(["pastNotifications"]);
			} else if(ajaxSortRouteList.readyState==4){
				alertDialog(sortFail);
			}
		};
		ajaxSortRouteList.open("GET","/systemAdmin/notifications/sortPastNotificationList.html?notificationType=past&sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortRouteList.send();		
	});

}

function retrieveMenuNotificationId(element) {
	var result = null;
	var $listItem = $(element).parents("li");
	if($listItem.length > 0) {
		result = $listItem.attr("id").substring(5);
	}
	else {
		result = $("#contentID").val();
	}
	
	return result;
}

function loadNotification(randomId, action, callback) {
	var hideCallBack = '';
	if($("#contentID").val() === randomId){
		if(action == "edit"){
			testFormEdit($("#notificationManagementForm"));
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#notificationDetail"), $("#notificationForm"), hideCallBack); }
		else if(action == "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#notificationForm"), $("#notificationDetail"), hideCallBack); } }
	else { 
		var $this = $("#note_"+randomId);
		if($("#currentList").find("li.selected").length){ $("#currentList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");
		hideCallBack = function(){
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if((request.status !== 200) || (request.responseText === "false")) {
						alertDialog(systemErrorMsg); //Unable to load location details
					}
					else {
						var notification = JSON.parse(request.responseText).notificationDetails;
						callback.call(this, randomId, notification);
						$("#formNotificationTitle").trigger('focus');
						$("#formNotificationStartDate").siblings("label").hide();
						$("#formNotificationStartTime").siblings("label").hide();
						$("#formNotificationEndDate").siblings("label").hide();
						$("#formNotificationEndTime").siblings("label").hide();
					}
				}
			};
			request.open("GET","/systemAdmin/notifications/notificationDetails.html?notificationID=" + randomId+"&"+document.location.search.substring(1), true);
			request.send(); }
		if($("#notificationDetail").is(":visible")){ snglSlideFadeTransition("hide", $("#notificationDetail"), hideCallBack); }
		else if($("#notificationForm").is(":visible")){ snglSlideFadeTransition("hide", $("#notificationForm"), hideCallBack); }
		else{ hideCallBack(); } } }

function displayNotificationAddForm() {
	$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); });
	var showCallBack = function(){
		$("#actionFlag").val(0);
		resetNotificationForm();
		testFormEdit($("#notificationManagementForm")); }
	snglSlideFadeTransition("show", $("#notificationForm"), showCallBack);
}

function displayNotificationEditForm(randomId, notification) {
	var showCallBack = function(){
		$("#actionFlag").val(1);
		$(".mainContent").addClass("form");
		$("#contentID").val(randomId);
		fillNotificationForm(randomId, notification);
		testFormEdit($("#notificationManagementForm")); }
	snglSlideFadeTransition("show", $("#notificationForm"), showCallBack);
}

function displayNotificationDetail(randomId, notification) {
	var showCallBack = function(){
		var beginTime = parseJSONDate(notification.beginGmt);
		var endTime = parseJSONDate(notification.endGmt);
		
		$(".mainContent").removeClass("form editForm");
		$("#contentID").val(randomId);
		$("#notificationStartDate").text($.datepicker.formatDate(PTTRN_DATE, beginTime));
		$("#notificationStartTime").text($("#formNotificationStartTime").autominutes("formatTime", beginTime));
		$("#notificationEndDate").text($.datepicker.formatDate(PTTRN_DATE, endTime));
		$("#notificationEndTime").text($("#formNotificationEndTime").autominutes("formatTime", endTime));
		fillNotificationForm(randomId, notification);
		if(syncMenuItems($("#menu_" + randomId + ">.menuOptions"), $("#menu_pageContent"))) {
			$("#opBtn_pageContent").show();
		}
		else {
			$("#opBtn_pageContent").hide();
		}
		
		displayNotificationPreview($("#notificationDetail"), notification); }
	snglSlideFadeTransition("show", $("#notificationDetail"), showCallBack);
}

function resetNotificationForm() {
	fillNotificationForm("", {
		"title": "",
		"message": "",
		"messageUrl": "",
		"beginGmt": "",
		"endGmt": ""
	});
}

function saveNotificationUpdates(event) { event.preventDefault();
	var params = $("#notificationManagementForm").serialize();
	var request = GetHttpObject({
		"triggerSelector": "#notificationManagementForm .save"
	});
	request.onreadystatechange = function() {
		if((request.readyState === 4) && (request.status === 200)) {
			if(request.responseText === "true") {
				noteDialog(noteSaveSuccess);
				var tempSavelUrl = location.pathname+"?"+scrubQueryString(document.location.search.substring(1), "pgActn");
				setTimeout(function(){window.location = tempSavelUrl+"&pgActn=display";}, 500); } }// new location Saved.
		else if(request.readyState === 4) { alertDialog(noteSaveFail); } };
	
	request.open("POST", "/systemAdmin/notifications/saveNotification.html", true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.setRequestHeader("Content-length", params.length);
	request.setRequestHeader("Connection", "close");
	request.send(params);
}

function cancelNotificationUpdates(event) { event.preventDefault();
	confirmDialog({
		title: attentionTitle,
		message: cancelConfirmMsg,
		okLabel: cancelCnfrmBtn,
		okCallback: function() {
			var tempCancelUrl = location.pathname+"?"+scrubQueryString(document.location.search.substring(1), "pgActn");
			window.location = tempCancelUrl+"&pgActn=display"; },
		cancelLabel: cancelGoBackBtn }); }

function deleteNotification(randomId) {
	confirmDialog({
		title: alertTitle,
		message: noteDeleteConfirm,
		okCallback: function() {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if((request.readyState === 4) && (request.status === 200)) {
					if(request.responseText === "true") {
						noteDialog(noteDeleteSuccess);
						var newLocation = location.pathname+"?"+filterQuerry;
						setTimeout(function(){window.location = newLocation}, 800);
					}
					else {
						alertDialog(noteDeleteFail);
					}
				}
				else if(request.readyState === 4) {
					alertDialog(noteDeleteFail);
				}
			};
			
			request.open("GET", "/systemAdmin/notifications/deleteNotification.html?notificationID=" + randomId+"&"+document.location.search.substring(1), true);
			request.send();
		}
	});
}

function fillNotificationForm(randomId, notification) {
	
	var $formBox = $("#notificationForm");
	var beginTime = parseJSONDate(notification.beginGmt);
	var endTime = parseJSONDate(notification.endGmt);
	
	$("#formNotificationID").val((randomId) ? randomId : "");
	$("#formNotificationTitle").val((notification.title) ? notification.title : "");
	$("#formNotificationMessage").val((notification.message) ? notification.message : "");
	$("#formNotificationMessageLink").val((notification.messageUrl) ? notification.messageUrl : "");
	$("#formNotificationStartDate").datepicker("setDate", beginTime);
	$("#formNotificationStartTime").autominutes("setTime", beginTime);
	$("#formNotificationEndDate").datepicker("setDate", endTime);
	$("#formNotificationEndTime").autominutes("setTime", endTime);
	
	displayNotificationPreview($formBox, notification);
}

function previewNotification() {
	displayNotificationPreview($("#notificationForm"), {
		"title": $("#formNotificationTitle").val(),
		"message": $("#formNotificationMessage").val(),
		"messageUrl": $("#formNotificationMessageLink").val()
	});
}

function displayNotificationPreview(container, notification) {
	var $previewBox = $(container).find("#smplNotificationMsg");
	$previewBox.find("h4").html((notification.title) ? escapeHTML(notification.title) : "");
	$previewBox.find(".notificationMsg").html((notification.message) ? escapeHTML(notification.message) : "");
	
	var moreInfoUrl = escapeHTML(notification.messageUrl);
	if(moreInfoUrl) {
		moreInfoUrl = moreInfoUrl.trim();
		if(moreInfoUrl.length <= 0) {
			moreInfoUrl = null;
		}
		else if(moreInfoUrl) {
			var protocol = moreInfoUrl.substring(0, 8);
			if(protocol !== "https://") {
				protocol = protocol.substring(0, 7);
				if(protocol !== "http://") {
					moreInfoUrl = "http://" + moreInfoUrl;
				}
			}
		}
	}
	
	if(moreInfoUrl) {
		$previewBox.find("a").attr("href", moreInfoUrl).show();
	}
	else {
		$previewBox.find("a").hide();
	}
}
