/* Startup scripts for System Monitor Page */
function systemMonitor() {
}

/* Startup scripts for Server Admin Page */
function serverAdmin() {
	$("#btnUploadedFile").on('click', function(event){
		if((!event) || (!$(event.currentTarget).hasClass("disabled"))) {
			var uploadConfirmButtons = {};
				uploadConfirmButtons.btn1 = { text : processNowBtn, click : function() { processUploadedFiles(event); $(this).scrollTop(0); $(this).dialog( "close" ); } };
				uploadConfirmButtons.btn2 = { text : cancelBtn, click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); } };
			if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
			$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmUploadMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: uploadConfirmButtons });
			btnStatus(processNowBtn);
		}
	});
	$("#btnCardRetry").on('click', processCardRetry);
//	$("#btnNextExternalKey").on('click', processNextKey);
//	$("#btnShutDownTomcat").on('click', processShutdown);  REMOVED PER EMS-11379
	$("a[id^=btnPromote]").on('click', promoteMember);
	$("#cardProcessingQueueReload").on('click', refreshCardProcessingQueue);
	$("body").on("click", "#cardProcessingQueue .pause", toggleProcessorMode);
	
	// Ad-hoc move the primary member on top.
	$clusterMembersList = $("#clusterMembersList");
	$clusterMembersList.prepend($clusterMembersList.find("> li:has(div:contains(" + labelPrimary + "))").remove());
}

function renderClusterMembers($container, json) {
	$container.empty();
	
	var membersList = json.list[0].ClusterMemberInfo;
	if(membersList) {
		var membersLen = membersList.length;
		for (var i = -1; ++i < membersLen; ) {
			var memberData = membersList[i];
			var $memberElem = $(
				"<li>" +
					"<div class='col1'>" + memberData.name + "</div>" +
					"<div class='col2'>" + ((memberData.isPrimary) ? labelPrimary : labelSecondary) + "</div>" +
				"</li>"
			);
			
			if (memberData.isPrimary) {
				$memberElem.prependTo($container);
			}
			else {
				$memberElem.append($(
					"<a id='btnPromote" + memberData.name + "' href='javascript:;' class='menuListButton moveUp' title='Promote'>" +
						"<img width='18' height='1' title='Promote' alt='Promote' src='" + imgLocalDir + "spacer.gif'  border='0'/>" +
					"</a>").on('click', promoteMember));
				
				$memberElem.appendTo($container);
			}
		}
	}
}

function renderCardProcessingQueue($container, json) {
	$container.fadeOut({
		"complete": function() {
			$(this).empty();
			
			var queuesList = json.cardProcessingQueueStatus.cardRetryStatusEntries;
			var queueLen = queuesList.length;
			
			for (var i = -1; ++i < queueLen; )
			{
				$container.append(createCardProcessingQueueElement(queuesList[i]));
			}
	
			$container.append("<hr class='listDivider' />");
			$container.append(createSimpleProcessingQueueElement({
				"key": labelSettlementQSize,
				"value": ((json.settlementQueueSize) ? 0 : json.settlementQueueSize)
			}));
			$container.append(createSimpleProcessingQueueElement({
				"key": labelStoreForwardQSize,
				"value": ((json.storeForwardQueueSize) ? 0 : json.storeForwardQueueSize)
			}));
			$container.append(createSimpleProcessingQueueElement({
				"key": labelReversalQSize,
				"value": ((json.reversalQueueSize) ? 0 : json.reversalQueueSize)
			}));
			
			$container.fadeIn();
		}
	});
}

function createSimpleProcessingQueueElement(qData) {
	return $(
			"<li>" +
				"<div class='col1'>" + qData.key + "</div>" +
				"<div class='col2'>" + ((!qData.value) ? 0 : qData.value) + "</div>" +
			"</li>"
		);
}

function createCardProcessingQueueElement(qData) {
	return $(
		"<li id='pq_" + qData.randomProcessorId + "'>" +
			"<div class='col1'>" + qData.name + "</div>" +
			"<div class='col2'>" + ((!qData.cardRetryCount) ? 0 : qData.cardRetryCount) + "</div>" +
			"<a href='#' class='menuListButton pause" +
			((qData.isPaused) ? " restart" : "") +
			"'><img width='19' height='1' border='0' src='"+imgLocalDir+"spacer.gif' alt='' title=''></a>" +
		"</li>"
	);
}

function refreshCardProcessingQueue(event) {
	var request = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	request.onreadystatechange = function(responseFalse, displayedMessages) {
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
					renderCardProcessingQueue($("#cardProcessingQueue"), JSON.parse(request.responseText));
				}
				else if(!displayMessages) {
					alertDialog(failRefreshCardProcessingQueue);
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("GET", "/systemAdmin/systemStatus/refreshQueueStatus.html?"+document.location.search.substring(1), true);
	request.send();
}

function promoteMember(event) {
	var memberId = event.target.id.substring(10);
	var request = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	request.onreadystatechange = function(responseFalse, displayedMessages) {
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
					noteDialog(successPromoteMember.replace("{memberName}", memberId));
					setTimeout("location.reload()", 500);
				}
				else if(!displayedMessages) {
					alertDialog(failPromoteMember);
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("GET", "/systemAdmin/systemStatus/promoteClusterMember.html?ProcessPromoteServer="+memberId+"&"+document.location.search.substring(1), true);
	request.send();
}

function processUploadedFiles(event) {
	if((!event) || (!$(event.currentTarget).hasClass("disabled"))) {
		var request = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		request.onreadystatechange = function(responseFalse, displayedMessages) {
			if(request.readyState === 4) {
				if(request.status === 200) {
					if(!responseFalse) {
						noteDialog(successProcessUploadedFiles, "timeOut", 2500);
					}
					else if(!displayedMessages) {
						alertDialog(failProcessUploadedFiles);
					}
				}
				else {
					alertDialog(systemErrorMsg);
				}
			}
		};
		
		request.open("GET", "/systemAdmin/systemStatus/processUploadedFile.html?"+document.location.search.substring(1), true);
		request.send();
	}
}

function processCardRetry(event) {
	if(!$(event.currentTarget).hasClass("disabled")) {
		var request = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		request.onreadystatechange = function(responseFalse, displayedMessages) {
			if(request.readyState === 4) {
				if(request.status === 200) {
					if(!responseFalse) {
						noteDialog(successProcessCardRetry, "timeOut", 2500);
					}
					else if(!displayedMessages) {
						alertDialog(failProcessCardRetry);
					}
				}
				else{
					alertDialog(systemErrorMsg);
				}
			}
		};
		
		request.open("GET", "/systemAdmin/systemStatus/processCardRetry.html?"+document.location.search.substring(1), true);
		request.send();
	}
}

/*function verifyProcessNextKey(event) {
	var request = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	request.onreadystatechange = function(responseFalse, displayedMessages) {
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
					processNextKey();
				}
				else if(!displayedMessages) {
					alertDialog(failKeyRotation);
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("GET", "/systemAdmin/systemStatus/verifyProcessNextKey.html?"+document.location.search.substring(1), true);
	request.send();
}*/

/* function processNextKey(event) {
	if(!$(event.currentTarget).hasClass("disabled")) {
		confirmDialog({
			title: alertTitle,
			message: confirmKeyRotation,
			okCallback: function() {
				var request = GetHttpObject({
					"triggerSelector": (event) ? event.target : null
				});
				request.onreadystatechange = function(responseFalse, displayedMessages) {
					if(request.readyState === 4) {
						if(request.status === 200) {
							if(!responseFalse) {
								noteDialog(successKeyRotation);
								setTimeout("location.reload()", 500); // new location Saved.
							}
							else if(!displayedMessages) {
								alertDialog(failKeyRotation);
							}
						}
						else {
							alertDialog(systemErrorMsg);
						}
					}
				};
				
				request.open("GET", "/systemAdmin/systemStatus/processNextKey.html?"+document.location.search.substring(1), true);
				request.send();
			}
		});
	}
} */

/*  REMVOED PER EMS-11379
function verifyProcessShutdown(event) {
	var request = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	request.onreadystatechange = function(responseFalse, displayedMessages) {
		if(request.readyState === 4) {
			if(request.status === 200) {
				if(!responseFalse) {
					processShutdown();
				}
				else if(!displayedMessages) {
					alertDialog(failShutdown);
				}
			}
			else {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	request.open("GET", "/systemAdmin/systemStatus/verifyShutdownTomcat.html?"+document.location.search.substring(1), true);
	request.send();
}

function processShutdown(event) {
	if(!$(event.currentTarget).hasClass("disabled")) {
		var blocker = null;
		confirmDialog({
			title: alertTitle,
			message: confirmShutdown,
			okCallback: function() {
				var request = GetHttpObject({
					"triggerSelector": (event) ? event.target : null
				});
				request.onreadystatechange = function(responseFalse, displayedMessages) {
					if(request.readyState === 4) {
						blocker.dialog("close");
						if(request.status === 200) {
							if(request.responseText === "true") {
								noteDialog(successShutdown, "timeOut", 2500);
							}
							else {
								alertDialog(failShutdown);
							}
						}
						else {
							resetSessionActvty();
						}
					}
				};
				
				request.open("GET", "/systemAdmin/systemStatus/shutdownTomcat.html?"+document.location.search.substring(1), true);
				request.send();
				blocker = blockPage();
			}
		});
	}
}
*/
function toggleProcessorMode(event) {
	var $listItem = $(event.target).parent("li");
	var processorId = $listItem.attr("id").split("_")[1];
	var buttonSelector = "li#pq_" + processorId + " .pause";
	
	var $button = $(buttonSelector);
	
	var restart = $button.hasClass("restart");
	
	var request = GetHttpObject({
		"triggerSelector": buttonSelector
	});
	request.onreadystatechange = function(responseFalse, displayedMessages) {
		if(request.readyState === 4) {
			var $button = $(buttonSelector)
			if(responseFalse) {
				if(restart) {
					alertDialog(failRestartProcessor);
				}
				else {
					alertDialog(failPauseProcessor);
				}
			}
			else {
				if(restart) {
					$button
						.removeClass("restart")
						.find("img")
						.attr("alt", labelRestartProcessor)
						.attr("title", labelRestartProcessor);
				}
				else {
					$button
						.addClass("restart")
						.find("img")
						.attr("alt", labelPauseProcessor)
						.attr("title", labelPauseProcessor);
				}
			}
		}
	};
	
	var url = "/systemAdmin/systemStatus/pauseCardProcessing.html";
	if(restart) {
		url = "/systemAdmin/systemStatus/resumeCardProcessing.html";
	}
	
	request.open("GET", url + "?" + document.location.search.substring(1) + "&processorId=" + processorId, true);
	request.send();
}
