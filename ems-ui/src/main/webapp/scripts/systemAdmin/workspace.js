// JavaScript Document

var selectedCustomerPos = "";

function saveCustomer(event)
{
	msgResolver.mapAttribute("legalName", "#formCustomerName");
	msgResolver.mapAttribute("parentCompany", "#prntCustomerAC");
	msgResolver.mapAttribute("userName", "#formUserName");
	msgResolver.mapAttribute("legalName", "#formCustomerName");
	msgResolver.mapAttribute("accountEnabled", "#customerStatus");
	msgResolver.mapAttribute("adminPass", "#newPassword,#c_newPassword");

	for(var subscription in subScriptionObj)
	{
		if($("#subscriptionTypeID").val() === ""){ $("#subscriptionTypeID").val(subScriptionObj[subscription].typeId); }
		else{ $("#subscriptionTypeID").val($("#subscriptionTypeID").val()+"," + subScriptionObj[subscription].typeId); }
		if($("#subscriptionRandomID").val() === ""){ $("#subscriptionRandomID").val(subScriptionObj[subscription].randomId); }
		else{ $("#subscriptionRandomID").val($("#subscriptionRandomID").val()+"," + subScriptionObj[subscription].randomId); }
		if($("#subscriptionStatus").val() === ""){ $("#subscriptionStatus").val(subScriptionObj[subscription].status); }
		else{ $("#subscriptionStatus").val($("#subscriptionStatus").val()+"," + subScriptionObj[subscription].status); }
	}
	//$("#custPostToken").val(globalPostToken);
	var params = $("#newCustomerForm").serialize();
	var ajaxNewCustomer = GetHttpObject({
		"triggerSelector": (event) ? event.target : event
	});
	ajaxNewCustomer.onreadystatechange = function()
	{
		if (ajaxNewCustomer.readyState==4 && ajaxNewCustomer.status == 200)
		{
			var response = ajaxNewCustomer.responseText.split(":");
			if(response[0] == "true")
			{
				noteDialog(customerSavedMsg);
				setTimeout("location.reload()", 800);
			}
		} else if(ajaxNewCustomer.readyState==4){
			alertDialog(failedSaveMsg);// Location changes and/or submission is unable to Save.
		}
	};
	ajaxNewCustomer.open("POST","/systemAdmin/workspace/customers/saveCustomer.html",true);
	ajaxNewCustomer.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxNewCustomer.setRequestHeader("Content-length", params.length);
	ajaxNewCustomer.setRequestHeader("Connection", "close");
	ajaxNewCustomer.send(params);
}

function anotherMrchntReset(formObj){
	var sbmtdProcessor;
	for(x=0; x < formObj.length; x++){
		if(formObj[x].name === "wrappedObject.processorName"){
			sbmtdProcessor = formObj[x].value; 
			break;
		}
	}
	
	switch(sbmtdProcessor){
		case "processor.alliance": 
		case "processor.concord": 
		case "processor.firstHorizon": 
		case "processor.moneris": 
		case "processor.tdmerchant": 
			$("input#field1, input#field2").val("");
			break;
		case "processor.elavon.viaconex": 
			$("input#field1").val("");
			break;
		case "processor.firstDataNashville": 
		case "processor.heartland": 
		case "processor.heartland.spplus": 
		case "processor.paymentech": 
			$("input#field2").val("");
			break;
		case "processor.link2gov": 
			$("input#field1, input#field2, input#field3, input#settleCode1, input.field1Input, input.field2Input").val("");
			break;
		case "processor.authorize.net": 
			$("input#field1, input#field3").val("");
			$("#field2").autoselector("reset");
			break;
		case "processor.elavon": 
			$("input#field1, input#field2, input#field3").val("");
			$("#field4").autoselector("reset");
			break;
		case "processor.paradata": 
			$("input#field1").val("");
			$("#field2").autoselector("reset");
			break;
		case "processor.blackboard": 
			$("input#field1, input#field2, input#field3, input#field4").val("");
			break;
		case "processor.cbord.csgold": 
			$("input#field1, input#field2, input#field3, input#field4, input#field5").val("");
			break;
		case "processor.cbord.odyssey": 
			$("input#field1, input#field2, input#field3, input#field4").val("");
			$("input#field5").val("1");
			break;
		case "processor.nuvision": 
		case "processor.totalcard": 
			$("input#field1, input#field2, input#field3, input#field4").val("");
			break;
		case "processor.creditcall": 
			$("input#field3").val("");
			break;
		default: 
			break;
			
	}
	
	$("input#account").val("").trigger('focus');
}

function saveMerchantAccount(event)
{
	msgResolver.mapAttribute("account", "#account");
	msgResolver.mapAttribute("processorName", "#processor");
	msgResolver.mapAttribute("field1", "#field1,.field1Input");
	msgResolver.mapAttribute("field2", "#field2,.field2Input");
	msgResolver.mapAttribute("field3", "#field3");
	msgResolver.mapAttribute("field4", "#field4");
	msgResolver.mapAttribute("field5", "#field5");
	msgResolver.mapAttribute("field6", "#field6");
	
	var	fieldSplit,
		responseMessage,
		tempSaveMerchantAccountMsg,
		alertMessageTime;
	if($(".field1Input").length){ $("#field1").val(""); $(".field1Input").each(function(index){ 
		fieldSplit = (index > 0)? "-": "";
		$("#field1").val($("#field1").val()+fieldSplit+$(this).val()); }); }
	if($(".field2Input").length){ $("#field2").val(""); $(".field2Input").each(function(index){ 
		fieldSplit = (index > 0)? "-": "";
		$("#field2").val($("#field2").val()+fieldSplit+$(this).val()); }); }
	//$("#merchPostToken").val(globalPostToken);
	var params = $("#merchantAccountEditForm").serialize(),
		formObj = $("#merchantAccountEditForm").serializeArray();
	var ajaxSaveMerchantAccount = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	ajaxSaveMerchantAccount.onreadystatechange = function()
	{
		if (ajaxSaveMerchantAccount.readyState==4 && ajaxSaveMerchantAccount.status == 200)
		{
			responseMessage = ajaxSaveMerchantAccount.responseText.split(":");
			if(responseMessage[0] == "true")
			{
				if(responseMessage[2]){ tempSaveMerchantAccountMsg = saveMerchantAccountMsg +"<br>"+ testFailedLabel + responseMessage[2];
					alertMessageTime = 3500; }
				else{ tempSaveMerchantAccountMsg = saveMerchantAccountMsg;
					alertMessageTime =  1000; }
				
				if($("#createAnother").is(":visible") && $("#createAnotherCheckHidden").val() === "true"){ 
					noteDialog(tempSaveMerchantAccountMsg, "timeOut", alertMessageTime, anotherMrchntReset(formObj)); }
				else{ 
					$("#mrchntAccntFormArea").dialog("close"); 
					noteDialog(tempSaveMerchantAccountMsg);
					setTimeout("location.reload()", alertMessageTime); }
			}		
		} else if(ajaxSaveMerchantAccount.readyState==4){
			alertDialog(failedSaveMsg);
		}
	};
	ajaxSaveMerchantAccount.open("POST","/systemAdmin/workspace/customers/saveMerchantAccount.html",true);
	ajaxSaveMerchantAccount.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxSaveMerchantAccount.setRequestHeader("Content-length", params.length);
	ajaxSaveMerchantAccount.setRequestHeader("Connection", "close");
	ajaxSaveMerchantAccount.send(params);
}

function deleteMerchantAccnt(accountID)
{
	var ajaxDeleteMrchntAccnt = GetHttpObject();
	ajaxDeleteMrchntAccnt.onreadystatechange = function()
	{
		if (ajaxDeleteMrchntAccnt.readyState==4 && ajaxDeleteMrchntAccnt.status == 200){
			if(ajaxDeleteMrchntAccnt.responseText == "true") { 
				noteDialog(deleteMerchantAccountMsg);
				setTimeout("location.reload()", 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete
		} else if(ajaxDeleteMrchntAccnt.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxDeleteMrchntAccnt.open("GET","/systemAdmin/workspace/customers/deleteMerchantAccount.html?&merchantAccountId="+accountID+"&"+document.location.search.substring(1),true);
	ajaxDeleteMrchntAccnt.send();	
}

function verifyDelete(accountID)
{
	var 	deleteButtons = {};
		deleteButtons.btn1 = {
			text : deleteBtn,
			click : 	function() { deleteMerchantAccnt(accountID); $(this).scrollTop(0); $(this).dialog( "close" ); },
		};
		deleteButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmDeleteMerchantMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteButtons });
	btnStatus(deleteBtn);
}

//--------------------------------------------------------------------------------------------

function saveSoapAccount(event)
{
	msgResolver.mapAttribute("endPointType", "#soapType");
	msgResolver.mapAttribute("token", "#soapToken");
	
	//$("#soapPostToken").val(globalPostToken);
	var params = $("#soapAccountEditForm").serialize();
	var ajaxSaveSOAPAccnt = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	ajaxSaveSOAPAccnt.onreadystatechange = function()
	{
		if (ajaxSaveSOAPAccnt.readyState==4 && ajaxSaveSOAPAccnt.status == 200)
		{
			var response =  ajaxSaveSOAPAccnt.responseText.split(":");
			if(response[0] == "true")
			{
				$("#soapAccntFormArea").dialog("close");
				noteDialog(saveSoapAccountMsg);
				setTimeout("location.reload()", 800); // new location Saved.
			}
		} else if(ajaxSaveSOAPAccnt.readyState==4){
			alertDialog(failedSaveMsg);// Location changes and/or submission is unable to Save.
		}
	};
	ajaxSaveSOAPAccnt.open("POST","/systemAdmin/workspace/customers/saveSoapAccount.html",true);
	ajaxSaveSOAPAccnt.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxSaveSOAPAccnt.setRequestHeader("Content-length", params.length);
	ajaxSaveSOAPAccnt.setRequestHeader("Connection", "close");
	ajaxSaveSOAPAccnt.send(params);	
}

function deleteSoapAccnt(accountID)
{
	var ajaxDeleteSoapAccnt = GetHttpObject();
	ajaxDeleteSoapAccnt.onreadystatechange = function()
	{
		if (ajaxDeleteSoapAccnt.readyState==4 && ajaxDeleteSoapAccnt.status == 200){
			if(ajaxDeleteSoapAccnt.responseText == "true") { 
				noteDialog(deleteSoapAccountMsg);
				setTimeout("location.reload()", 800); }
			else { alertDialog(systemErrorMsg); } //Unable to delete
		} else if(ajaxDeleteSoapAccnt.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxDeleteSoapAccnt.open("GET","/systemAdmin/workspace/customers/deleteSoapAccount.html?soapAccountId="+accountID+"&"+document.location.search.substring(1),true);
	ajaxDeleteSoapAccnt.send();	
}

//--------------------------------------------------------------------------------------------

function saveRestAccnt(event)
{	
	msgResolver.mapAttribute("type", "#accountType");
	msgResolver.mapAttribute("accountName", "#customerID");
	msgResolver.mapAttribute("virtualPOSId", "#virtualPaystation");
	
	//$("#restPostToken").val(globalPostToken);
	var params = $("#restAccountAPIForm").serialize();
	var ajaxSaveRestAccnt = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	ajaxSaveRestAccnt.onreadystatechange = function()
	{
		if (ajaxSaveRestAccnt.readyState==4 && ajaxSaveRestAccnt.status == 200)
		{
			var response =  ajaxSaveRestAccnt.responseText.split(":");
			if(response[0] == "true")
			{
				$("#restAccntFormArea").dialog("close");
				noteDialog(saveRestAccountMsg);
				setTimeout("location.reload()", 800); // new location Saved.
			}
		} else if(ajaxSaveRestAccnt.readyState==4){
			alertDialog(failedSaveMsg);// Location changes and/or submission is unable to Save.
		}
	};
	ajaxSaveRestAccnt.open("POST","/systemAdmin/workspace/customers/saveRestAccount.html",true);
	ajaxSaveRestAccnt.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxSaveRestAccnt.setRequestHeader("Content-length", params.length);
	ajaxSaveRestAccnt.setRequestHeader("Connection", "close");
	ajaxSaveRestAccnt.send(params);
}

function deleteRestAccnt(accountID)
{
	var ajaxDeleteRestApiAccnt = GetHttpObject();
	ajaxDeleteRestApiAccnt.onreadystatechange = function()
	{
		if (ajaxDeleteRestApiAccnt.readyState==4 && ajaxDeleteRestApiAccnt.status == 200){
			if(ajaxDeleteRestApiAccnt.responseText == "true") { 
				noteDialog(deleteRestAccountMsg);
				setTimeout("location.reload()", 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete
		} else if(ajaxDeleteRestApiAccnt.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxDeleteRestApiAccnt.open("GET","/systemAdmin/workspace/customers/deleteRestAccount.html?restAccountId="+accountID+"&"+document.location.search.substring(1),true);
	ajaxDeleteRestApiAccnt.send();	
}

//--------------------------------------------------------------------------------------

function showMerchantAccntDetails(accntID)
{
		accntID = accntID.split("_");
		
		var ajaxViewMrchntAccnt = GetHttpObject();
		ajaxViewMrchntAccnt.onreadystatechange = function()
		{
			if (ajaxViewMrchntAccnt.readyState==4 && ajaxViewMrchntAccnt.status == 200){
				if(ajaxViewMrchntAccnt.responseText != "false")
				{
					if($("#mrchntAccntFormArea.ui-dialog-content").length){$("#mrchntAccntFormArea.ui-dialog-content").dialog("destroy");}
					$("#mrchntAccntFormArea").html(ajaxViewMrchntAccnt.responseText);
					var viewAccountBtn = {};
					viewAccountBtn.btn1 = { text: okBtn, click: function(event, ui) { $(this).scrollTop(0); $(this).dialog( "close" ); } };
					$("#mrchntAccntFormArea").dialog({
						title: merchantAccountTitle,
						modal: true,
						resizable: false,
						closeOnEscape: false,
						width: 450,
						height: "auto",
						hide: "fade",
						buttons: viewAccountBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }			
					});
					btnStatus(okBtn);
					
					$("#mrchntAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
				} else {
					alertDialog(systemErrorMsg); //Unable to load details
				}
			} else if(ajaxViewMrchntAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxViewMrchntAccnt.open("GET","/systemAdmin/workspace/customers/viewMerchantAccount.html?merchantAccountId="+accntID[1]+"&"+document.location.search.substring(1),true);
		ajaxViewMrchntAccnt.send();
}

//--------------------------------------------------------------------------------------

function migrateMerchantAccnt(accntID){
   var ajaxMrchntAccntMigration = GetHttpObject();
    ajaxMrchntAccntMigration.onreadystatechange = function()
    {
        if (ajaxMrchntAccntMigration.readyState==4 && ajaxMrchntAccntMigration.status == 200){
            if(ajaxMrchntAccntMigration.responseText != "false")
            {
                setTimeout("location.reload()", 800); // new location Saved.
            } else {
                alertDialog(systemErrorMsg); //Unable to load details
            }
        } else if(ajaxMrchntAccntMigration.readyState==4){
            alertDialog(systemErrorMsg); //Unable to load details
        }
    };
    ajaxMrchntAccntMigration.open("GET","/systemAdmin/workspace/customers/migrateMerchantAccount.html?merchantAccountId="+accntID+"&"+document.location.search.substring(1),true);
    ajaxMrchntAccntMigration.send();
}

//--------------------------------------------------------------------------------------

function customer()
{	
	$("#btnEditCustomer").on('click', function(event){ event.preventDefault();
		var ajaxNewCustomerForm = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxNewCustomerForm.onreadystatechange = function()
		{
			if (ajaxNewCustomerForm.readyState==4 && ajaxNewCustomerForm.status == 200){
				if($("#editCustomerFormArea.ui-dialog-content").length){ $("#editCustomerFormArea.ui-dialog-content").dialog("destroy"); }
				$("#editCustomerFormArea").html(ajaxNewCustomerForm.responseText);

				var addCustomerBtn = {};
				addCustomerBtn.btn1 = { text: editCustomerBtn, click: function(event, ui) { saveCustomer(event); } };
				addCustomerBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$("#editCustomerFormArea").dialog({
					title: editCustomerBtn,
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 600,
					height: "auto",
					hide: "fade",
					buttons: addCustomerBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }			
				});
				btnStatus(editCustomerBtn);
				testPopFormEdit($("#newCustomerForm"), $("#editCustomerFormArea"));
				$("#editCustomerFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
				$("#formCustomerName").trigger('focus');
			} else if(ajaxNewCustomerForm.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxNewCustomerForm.open("GET","/systemAdmin/workspace/customers/editCustomer.html?"+document.location.search.substring(1),true);
		ajaxNewCustomerForm.send();
	});
	
//--------------------------------------------------------------------------------------

	$("#btnAddMrchntAccnt").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		var	tmpDfltTimeZone = $("#cstmrTimeZone").attr("timezone"),
			ajaxAddMrchntAccnt = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxAddMrchntAccnt.onreadystatechange = function()
		{
			if (ajaxAddMrchntAccnt.readyState==4 && ajaxAddMrchntAccnt.status == 200){
				if($("#mrchntAccntFormArea.ui-dialog-content").length){ $("#mrchntAccntFormArea.ui-dialog-content").dialog("destroy"); }
				$("#mrchntAccntFormArea").html(ajaxAddMrchntAccnt.responseText);
				var addAccountBtn = {};
				addAccountBtn.btn1 = { text: addAccountBtnMsg, click: function(event, ui) { saveMerchantAccount(event); } };
				addAccountBtn.btn2 = { text: cancelBtn, click: function(event) { 
					var cancelAction = ($("#createAnother").is(":visible") && $("#createAnotherCheckHidden").val() === "true")? "reload" : $(this) ; event.preventDefault(); cancelConfirm(cancelAction); } };
				
				$("#mrchntAccntFormArea").dialog({
					title: addMerchantAccountMsg,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					open: function(){ showCreateAnother($("#mrchntAccntFormArea")) },
					closeOnEscape: false,
					width: 450,
					height: "auto",
					hide: "fade",
					buttons: addAccountBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }				
				});
				btnStatus(addAccountBtnMsg);
				testPopFormEdit($("#merchantAccountEditForm"), $("#mrchntAccntFormArea"));
				
				$("#mrchntAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
				$("#account").trigger('focus');
			} else if(ajaxAddMrchntAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxAddMrchntAccnt.open("GET","/systemAdmin/workspace/customers/addMerchantAccount.html?notValid=true&tmpTimeZone="+tmpDfltTimeZone+"&"+document.location.search.substring(1),true);
		ajaxAddMrchntAccnt.send();
	});

//--------------------------------------------------------------------------------------

    $(".merchantAccntMenu").find(".migrate").on('click', function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        $(this).parents(".optionMenu").hide();
        var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
        var accountStatusObj = $(this).parents(".optionMenu").attr("id").replace("menu_", "mA_");
        var accountStatus = $("#" + accountStatusObj).hasClass("statusFailed");
        var accountName = $("#" + accountStatusObj).find(".col1")[0].innerText;
        var migrateAcctnFn = function () {
            migrateMerchantAccnt(accountID);
        };

        // if(!accountStatus){
			confirmDialog({
				title: "Merchant Account Migration",
				message: "Are you sure you would like to migrate " + accountName,
				okLabel: "Migrate",
				okCallback: migrateAcctnFn
			});
		// } else {
		// 	noteDialog(accountName + " is not available to Migrate.");
		// }

    });

//--------------------------------------------------------------------------------------

	$(".merchantAccntMenu").find(".edit").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var accountStatusObj = $(this).parents(".optionMenu").attr("id").replace("menu_", "mA_");
		var accountStatus = $("#"+accountStatusObj).hasClass("statusFailed");
		var ajaxEditMrchntAccnt = GetHttpObject();
		ajaxEditMrchntAccnt.onreadystatechange = function()
		{
			if (ajaxEditMrchntAccnt.readyState==4 && ajaxEditMrchntAccnt.status == 200){
				if(ajaxEditMrchntAccnt.responseText != "false")
				{
					if($("#mrchntAccntFormArea.ui-dialog-content").length){ $("#mrchntAccntFormArea.ui-dialog-content").dialog("destroy"); }
					$("#mrchntAccntFormArea").html(ajaxEditMrchntAccnt.responseText);
					var editAccountBtn = {};
					editAccountBtn.btn1 = { text: saveAccountBtnMsg, click: function(event, ui) { saveMerchantAccount(event); } };
					editAccountBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
					
					$("#mrchntAccntFormArea").dialog({
						title: merchantAccountTitle,
						classes: { "ui-dialog": "visibleOverflow" },
						modal: true,
						resizable: false,
						closeOnEscape: false,
						width: 450,
						height: "auto",
						hide: "fade",
						buttons: editAccountBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }				
					});
					btnStatus(saveAccountBtnMsg);
					testPopFormEdit($("#merchantAccountEditForm"), $("#mrchntAccntFormArea"));
					$("#mrchntAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
					$("#account").trigger('focus');
				} else {
					alertDialog(systemErrorMsg); //Unable to load details
				}
			} else if(ajaxEditMrchntAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxEditMrchntAccnt.open("GET","/systemAdmin/workspace/customers/editMerchantAccount.html?merchantAccountId="+accountID+"&notValid="+accountStatus+"&"+document.location.search.substring(1),true);
		ajaxEditMrchntAccnt.send();
	});
		
//--------------------------------------------------------------------------------------

	$(document.body).on("click", "#merchantAccountList > li", function(event){ if(!$(this).hasClass('notclickable')){ showMerchantAccntDetails($(this).attr("id")); } });

	$(".merchantAccntMenu").find(".view").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		showMerchantAccntDetails($(this).parents(".optionMenu").attr("id")); });	

//--------------------------------------------------------------------------------------

	$(".merchantAccntMenu").find(".test").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		var	accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", ""),
			ajaxTestMrchntAccnt = GetHttpObject(),
			responseMessage,
			tempTestMerchantAccountFailMsg,
			alertMessageTime;
		ajaxTestMrchntAccnt.onreadystatechange = function()
		{
			if (ajaxTestMrchntAccnt.readyState==4 && ajaxTestMrchntAccnt.status == 200){
				responseMessage = ajaxTestMrchntAccnt.responseText.split(":")
				if(responseMessage[0] === "true") { 
					noteDialog(testMerchantAccountSuccessMsg, "timeOut", 2500);
					if($("#mA_"+accountID).hasClass("statusFailed")){ 
						$("#mA_"+accountID).removeClass("statusFailed"); 
						$("#mA_"+accountID).find(".col1").removeClass("statusFailed"); 
					}
				}
				else if(responseMessage[0] === "false") { 
					if(responseMessage[1]){ tempTestMerchantAccountFailMsg = testMerchantAccountFailMsg +"<br>"+ responseMessage[1];
						alertMessageTime = 3500; }
					else{ tempTestMerchantAccountFailMsg = testMerchantAccountFailMsg;
						alertMessageTime = 1000; }
					noteDialog(tempTestMerchantAccountFailMsg, "timeOut", alertMessageTime);
					$("#mA_"+accountID).addClass("statusFailed");
					$("#mA_"+accountID).find(".col1").addClass("statusFailed");
				 } //Unable to delete
			} else if(ajaxTestMrchntAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxTestMrchntAccnt.open("GET","/systemAdmin/workspace/customers/testMerchantAccount.html?merchantAccountId="+accountID+"&"+document.location.search.substring(1),true);
		ajaxTestMrchntAccnt.send();
	});	

//--------------------------------------------------------------------------------------

	$(".merchantAccntMenu").find(".delete").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var ajaxVerifyMerchantDelete = GetHttpObject();
		ajaxVerifyMerchantDelete.onreadystatechange = function(responseFalse, displayedError, confirmed) {
			if(ajaxVerifyMerchantDelete.readyState==4 && ajaxVerifyMerchantDelete.status == 200){
				if(responseFalse == false){
					if(confirmed) {
						deleteMerchantAccnt(accountID);
					}
					else {
						verifyDelete(accountID);
					}
				}
			} 
			else if(ajaxVerifyMerchantDelete.readyState==4){
				alertDialog(systemErrorMsg); } };//Unable to load details
		ajaxVerifyMerchantDelete.open("GET","/systemAdmin/workspace/customers/verifyDeleteMerchantAccount.html?merchantAccountId="+accountID+"&"+document.location.search.substring(1),true);
		ajaxVerifyMerchantDelete.send();
	});

//--------------------------------------------------------------------------------------

	var showSoapAddForm = function(category, event) { event.stopImmediatePropagation(); event.preventDefault();
		var dialogTitle;
		if (category === "STANDARD") {
			dialogTitle = addSoapAccountMsg;
		} else if (category === "PRIVATE") {
			dialogTitle = addSoapPrivateAccountMsg;
		}
		var ajaxAddSoapAccnt = GetHttpObject();
		ajaxAddSoapAccnt.onreadystatechange = function()
		{
			if (ajaxAddSoapAccnt.readyState==4 && ajaxAddSoapAccnt.status == 200){
				if($("#soapAccntFormArea.ui-dialog-content").length){ $("#soapAccntFormArea.ui-dialog-content").dialog("destroy"); }
				var $area = $("#soapAccntFormArea");
				$area.html(ajaxAddSoapAccnt.responseText);

				var addAccountBtn = {};
				addAccountBtn.btn1 = { text: addAccountBtnMsg, click: function(event, ui) { saveSoapAccount(event); } };
				addAccountBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$area.dialog({
					title: dialogTitle,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 450,
					height: "auto",
					hide: "fade",
					buttons: addAccountBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }					
				});
				btnStatus(addAccountBtnMsg);
				testPopFormEdit($("#soapAccountEditForm"), $("#soapAccntFormArea"));
				$area.css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
			} else if(ajaxAddSoapAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxAddSoapAccnt.open("GET","/systemAdmin/workspace/customers/addSoapAccount.html?category="+category+"&"+document.location.search.substring(1),true);
		ajaxAddSoapAccnt.send();
	};
	
	$("#btnAddSOAP").on('click', function(event){
		showSoapAddForm("STANDARD", event);
	});
	
	$("#btnAddSOAPXchange").on('click', function(event){
		showSoapAddForm("PRIVATE", event);
	});
	
	$(document.body).on("click", "#generateToken", function(event){ event.preventDefault();
		var ajaxGenerateToken = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxGenerateToken.onreadystatechange = function()
		
		{
			if (ajaxGenerateToken.readyState==4 && ajaxGenerateToken.status == 200){
				$("#soapToken").val(ajaxGenerateToken.responseText);
			} else if(ajaxGenerateToken.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxGenerateToken.open("GET","/systemAdmin/workspace/customers/getNewToken.html?"+document.location.search.substring(1),true);
		ajaxGenerateToken.send();
	});

//--------------------------------------------------------------------------------------

	$(".soapAccntMenu").find(".edit").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		var dialogTitle = ($(this).parents("#soapXchangeAcct").length > 0) ? editSoapPrivateAccountMsg : editSoapAccountMsg;
		
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var ajaxAddSoapAccnt = GetHttpObject();
		ajaxAddSoapAccnt.onreadystatechange = function()
		{
			if (ajaxAddSoapAccnt.readyState==4 && ajaxAddSoapAccnt.status == 200){
				if($("#soapAccntFormArea.ui-dialog-content").length){ $("#soapAccntFormArea.ui-dialog-content").dialog("destroy"); }
				$("#soapAccntFormArea").html(ajaxAddSoapAccnt.responseText);

				var editAccountBtn = {};
				editAccountBtn.btn1 = { text: editAccountBtnMsg, click: function(event, ui) { saveSoapAccount(event); } };
				editAccountBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$("#soapAccntFormArea").dialog({
					title: dialogTitle,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 450,
					height: "auto",
					hide: "fade",
					buttons: editAccountBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }					
				});
				btnStatus(editAccountBtnMsg);
				testPopFormEdit($("#soapAccountEditForm"), $("#soapAccntFormArea"));
				$("#soapAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
			} else if(ajaxAddSoapAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxAddSoapAccnt.open("GET","/systemAdmin/workspace/customers/editSoapAccount.html?soapAccountId="+accountID+"&"+document.location.search.substring(1),true);
		ajaxAddSoapAccnt.send();
	});
	
//--------------------------------------------------------------------------------------

	$(".soapAccntMenu").find(".delete").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		var confirmDeleteMsg = ($(this).parents("#soapXchangeAcct").length > 0) ? confirmDeletePrivateMsg : confirmDeleteSoapMsg;
		$(this).parents(".optionMenu").hide();
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var 	deleteButtons = {};
			deleteButtons.btn1 = {
				text : deleteBtn,
				click : 	function() { deleteSoapAccnt(accountID); $(this).scrollTop(0); $(this).dialog( "close" ); },
			};
			deleteButtons.btn2 = {
				text : cancelBtn,
				click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmDeleteMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteButtons });
		btnStatus(deleteBtn);
	});

//--------------------------------------------------------------------------------------

	$("#btnAddRestApi").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		var ajaxAddRestApiAccnt = GetHttpObject();
		ajaxAddRestApiAccnt.onreadystatechange = function()
		{
			if (ajaxAddRestApiAccnt.readyState===4 && ajaxAddRestApiAccnt.status===200){
				if($("#restAccntFormArea.ui-dialog-content").length){ $("#restAccntFormArea.ui-dialog-content").dialog("destroy"); }
				$("#restAccntFormArea").html(ajaxAddRestApiAccnt.responseText);

				var addAccountBtn = {};
				addAccountBtn.btn1 = { text: addAccountBtnMsg, click: function(event, ui) { saveRestAccnt(event); } };
				addAccountBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$("#restAccntFormArea").dialog({
					title: addRestAccountMsg,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 450,
					height: "auto",
					hide: "fade",
					buttons: addAccountBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }				
				});
				btnStatus(addAccountBtnMsg);
				testPopFormEdit($("#restAccountAPIForm"), $("#restAccntFormArea"));
				$("#restAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
			} else if(ajaxAddRestApiAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxAddRestApiAccnt.open("GET","/systemAdmin/workspace/customers/addRestAccount.html?"+document.location.search.substring(1),true);
		ajaxAddRestApiAccnt.send();
	});

//--------------------------------------------------------------------------------------

	$(".restAccntMenu").find(".edit").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var ajaxEditRestApiAccnt = GetHttpObject();
		ajaxEditRestApiAccnt.onreadystatechange = function()
		{
			if (ajaxEditRestApiAccnt.readyState===4 && ajaxEditRestApiAccnt.status===200){
				if($("#restAccntFormArea.ui-dialog-content").length){ $("#restAccntFormArea.ui-dialog-content").dialog("destroy"); }
				$("#restAccntFormArea").html(ajaxEditRestApiAccnt.responseText);

				var addAccountBtn = {};
				addAccountBtn.btn1 = { text: saveAccountBtnMsg, click: function(event, ui) { saveRestAccnt(event); } };
				addAccountBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$("#restAccntFormArea").dialog({
					title: editRestAccountMsg,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 450,
					height: "auto",
					hide: "fade",
					buttons: addAccountBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }				
				});
				btnStatus(saveAccountBtnMsg);
				testPopFormEdit($("#restAccountAPIForm"), $("#restAccntFormArea"));
				$("#restAccntFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
			} else if(ajaxEditRestApiAccnt.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxEditRestApiAccnt.open("GET","/systemAdmin/workspace/customers/editRestAccount.html?restAccountId="+accountID+"&"+document.location.search.substring(1),true);
		ajaxEditRestApiAccnt.send();
	});
	
//--------------------------------------------------------------------------------------

	$(".restAccntMenu").find(".delete").on('click', function(event){ event.stopImmediatePropagation(); event.preventDefault();
		$(this).parents(".optionMenu").hide();
		var accountID = $(this).parents(".optionMenu").attr("id").replace("menu_", "");
		var 	deleteButtons = {};
			deleteButtons.btn1 = {
				text : deleteBtn,
				click : 	function() { deleteRestAccnt(accountID); $(this).scrollTop(0); $(this).dialog( "close" ); },
			};
			deleteButtons.btn2 = {
				text : cancelBtn,
				click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmDeleteRestMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteButtons });
		btnStatus(deleteBtn);
	});
}

function customerPOSAutoComplete(request, response) {
	var typeTest = characterTypes.stndrdChrctr;	
	if(typeTest.test(request.term.replace(/[\r\n]/g, "")) !== true){
		var ajaxCustomerPOSAC = GetHttpObject();
		ajaxCustomerPOSAC.onreadystatechange = function(responseFalse, displayedError) {
			if(ajaxCustomerPOSAC.readyState === 4 && ajaxCustomerPOSAC.status === 200) {
				var responseOptions = new Array(), customerResponse = new Array(),  posResponse = new Array(), posNameResponse = new Array();
				var data = JSON.parse(ajaxCustomerPOSAC.responseText);
				var itemDesc = "";
				if(data.searchCustomerResult.customerSearchResult instanceof Array)
				{ var customerResponse = $.map(data.searchCustomerResult.customerSearchResult, function(item) {
					if(item.isParent){itemDesc = parentTitle}else if(item.parentName){itemDesc = item.parentName}else{ itemDesc = "" }
					return{ label: item.name, 
						value: item.randomId, 
						desc: itemDesc,
						category: customerMsg 
				}}); } 
				else if(data.searchCustomerResult.customerSearchResult) 
				{ 
					if(data.searchCustomerResult.customerSearchResult.isParent){itemDesc = parentTitle}
					else if(data.searchCustomerResult.customerSearchResult.parentName){itemDesc = data.searchCustomerResult.customerSearchResult.parentName}
					else{ itemDesc = "" }
					var customerResponse = [{
						label: data.searchCustomerResult.customerSearchResult.name, 
						value: data.searchCustomerResult.customerSearchResult.randomId, 
						desc: itemDesc,
						category: customerMsg
				}]; }
				
				if(data.searchCustomerResult.posSearchResult instanceof Array)
				{ var posResponse = $.map(data.searchCustomerResult.posSearchResult, function(item) {return{ 
					label: item.name, 
					value: item.randomId,
					desc: item.label, 
					category: payStationMsg
				}}); } 
				else if(data.searchCustomerResult.posSearchResult)
				{ var posResponse = [{
					label: data.searchCustomerResult.posSearchResult.name, 
					value: data.searchCustomerResult.posSearchResult.randomId, 
					desc: data.searchCustomerResult.posSearchResult.label, 
					category: payStationMsg
				}]; }
				
				if(data.searchCustomerResult.posNameSearchResult instanceof Array)
				{ var posNameResponse = $.map(data.searchCustomerResult.posNameSearchResult, function(item) {return{ 
					label: item.name, 
					value: item.randomId, 
					desc: item.label, 
					category: payStationMsg
				}}); } 
				else if(data.searchCustomerResult.posNameSearchResult)
				{ var posNameResponse = [{
					label: data.searchCustomerResult.posNameSearchResult.name, 
					value: data.searchCustomerResult.posNameSearchResult.randomId, 
					desc: data.searchCustomerResult.posNameSearchResult.label, 
					category: payStationMsg
				}]; }
				
				responseOptions = responseOptions.concat(customerResponse,posResponse,posNameResponse);
				response(responseOptions);
			}
		};
		ajaxCustomerPOSAC.open("GET", "/systemAdmin/searchCustomerAndPos.html?search="+request.term, true);
		ajaxCustomerPOSAC.send();
	}
}

function childCustomerAutoComplete(request, response) {
	var typeTest = characterTypes.stndrdChrctr;	
	if(typeTest.test(request.term.replace(/[\r\n]/g, "")) !== true){
		var ajaxChildCustomerAC = GetHttpObject();
		ajaxChildCustomerAC.onreadystatechange = function(responseFalse, displayedError) {
			if(ajaxChildCustomerAC.readyState === 4) {
				var responseOptions = new Array(), customerResponse = new Array(),  posResponse = new Array(), posNameResponse = new Array();
				var data = JSON.parse(ajaxChildCustomerAC.responseText);
				var itemDesc = "";
				if(data.searchCustomerResult.customerSearchResult instanceof Array)
				{ var customerResponse = $.map(data.searchCustomerResult.customerSearchResult, function(item) {
					if(item.isParent){itemDesc = "Parent"}else if(item.parentName){itemDesc = item.parentName}
					return{ label: item.name, 
						value: item.randomId, 
						desc: itemDesc,
						category: customerMsg 
				}}); } 
				else if(data.searchCustomerResult.customerSearchResult) 
				{ 
					if(data.searchCustomerResult.customerSearchResult.isParent){itemDesc = "Parent"}
					else if(data.searchCustomerResult.customerSearchResult.parentName){itemDesc = data.searchCustomerResult.customerSearchResult.parentName}
					var customerResponse = [{
						label: data.searchCustomerResult.customerSearchResult.name, 
						value: data.searchCustomerResult.customerSearchResult.randomId, 
						desc: itemDesc,
						category: customerMsg
				}]; }
				response(customerResponse);
			}
		};
		ajaxChildCustomerAC.open("GET", "/systemAdmin/searchCustomerAndPos.html?childCompany=true&search="+request.term, true);
		ajaxChildCustomerAC.send(); }
}

function workspace(){
	$("#btnAddNewCustomer").on('click', function(event){ event.preventDefault();
		var ajaxNewCustomerForm = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxNewCustomerForm.onreadystatechange = function()
		{
			if (ajaxNewCustomerForm.readyState==4 && ajaxNewCustomerForm.status == 200){
				if($("#newCustomerFormArea.ui-dialog-content").length){$("#newCustomerFormArea.ui-dialog-content").dialog("destroy");}
				$("#newCustomerFormArea").html(ajaxNewCustomerForm.responseText);
				//document.getElementById("cardTypeForm").reset();
				var addCustomerBtn = {};
				addCustomerBtn.btn1 = { text: addCustomer, click: function(event, ui) { saveCustomer(event); } };
				addCustomerBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
				
				$("#newCustomerFormArea").dialog({
					title: newCustomerTitle,
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 600,
					height: "auto",
					hide: "fade",
					buttons: addCustomerBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }				
				});
				btnStatus(addCustomer);
				testPopFormEdit($("#newCustomerForm"), $("#newCustomerFormArea"));
				$("#newCustomerFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
				$("#formCustomerName").trigger('focus');
			} else if(ajaxNewCustomerForm.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxNewCustomerForm.open("GET","/systemAdmin/workspace/customers/newCustomer.html?"+document.location.search.substring(1),true);
		ajaxNewCustomerForm.send();
	});

//--------------------------------------------------------------------------------------
	
	var itemSelected = false;
	
	$("#search").autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"submitOnEnter": true,
			"remoteFn": customerPOSAutoComplete
		}).on("itemSelected", function(ui, selectedItem) {
			if(selectedItem){
				itemSelected = true;
				selectedCustomerPos = selectedItem;
				$("#randomId").val(selectedItem.value);
				$("#btnGo").trigger('focus'); }
			else{ itemSelected = false; }
			return false;
		}).on("change", function(ui, selectedItem) {
			if(itemSelected === false){ 
				$("#randomId").val(''); }
			else{ itemSelected = false; }
		});

$("#search").inputfit()
		
	$("#CustomerSearch").autoselector({
			"minLength": 3,
			"persistToHidden": false,
			"shouldCategorize": true,
			"submitOnEnter": true,
			"remoteFn": childCustomerAutoComplete
		}).on("itemSelected", function(ui, selectedItem) {
			if(selectedItem){
				itemSelected = true;
				selectedCustomerPos = selectedItem;
				$("#CustomerSearchId").val(selectedItem.value); }
			else{ itemSelected = false; }
			return false;
		}).on("change", function(ui, selectedItem) {
			if(itemSelected === false){ 
				$("#CustomerSearchId").val(''); }
			else{ itemSelected = false; }
		});

	$("#custPosSearch").on('submit', function(event){
		if($("#mainContent").hasClass("edit")){ event.preventDefault(); inEditAlert("menu"); }
		else{if($("#status").val() == "false"){ $("#btnGo").trigger('click'); return false; } else { return true; } }});
	
	$("#btnGo").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{
			if($("#randomId").val() == "" || ($("#randomId").val() == selectedCustomerPos.value && $("#search").val() != selectedCustomerPos.label)) {
				var formStop = false;
				$("#randomId").val("");
				$.ajax({
				url: "/systemAdmin/searchCustomerAndPos.html?"+document.location.search.substring(1),
				dataType: "json",
				data: { search: $("#search").val() },
				success: function(data) {
					if(data.searchCustomerResult.customerSearchResult && (!data.searchCustomerResult.posSearchResult && !data.searchCustomerResult.posNameSearchResult)){
						if(data.searchCustomerResult.customerSearchResult instanceof Array){ formStop = true; } 
						else{ $("#randomId").val(data.searchCustomerResult.customerSearchResult.randomId); } }
					if(data.searchCustomerResult.posSearchResult && (!data.searchCustomerResult.customerSearchResult && !data.searchCustomerResult.posNameSearchResult)) {
						if(data.searchCustomerResult.posSearchResult instanceof Array){ formStop = true; } 
						else { $("#randomId").val(data.searchCustomerResult.posSearchResult.randomId); } }
					if(data.searchCustomerResult.posNameSearchResult && (!data.searchCustomerResult.customerSearchResult && !data.searchCustomerResult.posSearchResult)) {
						if(data.searchCustomerResult.posNameSearchResult instanceof Array){ formStop = true; } 
						else { $("#randomId").val(data.searchCustomerResult.posNameSearchResult.randomId); } }
					  
					if(formStop == true || (formStop == false && $("#randomId").val() == "")){ $("#status").val("false"); alertDialog(selectOptionMsg); }
					else{ $("#status").val("true"); $("#custPosSearch").trigger('submit'); }
				}}); } 
			else { $("#status").val("true"); $("#custPosSearch").trigger('submit'); } }
	});
	
	$(document.body).on("click", "#printCollectionReport", function() {
		submitForm($("#collectionReportSortForm"), LOC.printCollectionReport, "_blank");
	});
	
	$("ul.currentContactList").on("click", "li", function(event){
			event.preventDefault();
			if($(this).attr("id") != "noContact"){
				if($(this).hasClass("selected")){ 
					$(this).removeClass("selected").addClass("unselected").attr("title", clickToAdd); 
					var removeContact = $(this).find(".contactEmail").html().replace(" ","");
					$("#formAlertNotifyList").val($("#formAlertNotifyList").val().replace(removeContact, "").replace(",,", ",").replace(" ",""));
					if($("#formAlertNotifyList").val().charAt(0)==","){ $("#formAlertNotifyList").val($("#formAlertNotifyList").val().substring(1)); }
					if($("#formAlertNotifyList").val().charAt($("#formAlertNotifyList").val().length-1)==","){$("#formAlertNotifyList").val($("#formAlertNotifyList").val().substring(0, $("#formAlertNotifyList").val().length-1));}
				} else{ 
					$(this).removeClass("unselected").addClass("selected").attr("title", clickToRemove);
					if($("#formAlertNotifyList").val() == ""){ $("#formAlertNotifyList").val($(this).find(".contactEmail").html()); } else{$("#formAlertNotifyList").val($("#formAlertNotifyList").val() +","+ $(this).find(".contactEmail").html()); }
				}
				
			}
		});
}

//--------------------------------------------------------------------------------------

function loadLicense(slctValue){
	var params = $.param(serializeForm("#mobileAppSelection"));

	var ajaxLoadLicense = GetHttpObject();
	ajaxLoadLicense.onreadystatechange = function(){
		if (ajaxLoadLicense.readyState==4 && ajaxLoadLicense.status == 200){			
			var data = JSON.parse(ajaxLoadLicense.responseText);
			var licenseCount = data.licenses.licenseCount;
			
			$("#licenseAccnt").html(licenseCount);
			$("#licenseCountForm").val(licenseCount);
			if(slctValue){ $("#appIdForm").val(slctValue); }
			
			if(licenseCount === 0){ $("#licenseListContentArea").hide(); $("#noLicenses").show(); }
			else{
				var licenseList = "";
				var licenseDataList = (data.licenses.licenseInfos[0].mobileLicense)? data.licenses.licenseInfos[0].mobileLicense : "";
				licenseDataList = (licenseDataList instanceof Array)? licenseDataList : [licenseDataList];
				for(x=0; x < licenseCount; x++){
					var licenseNumber = x+1;
					if(x < licenseDataList.length && licenseDataList[x] !== ""){ 
						var deviceName = (licenseDataList[x].deviceName && licenseDataList[x].deviceName !== "")? licenseDataList[x].deviceName : unNamedDeviceTtl;
						licenseList += "<li><div name='"+deviceName+"' class='col1'><p>"+licenseNumber+". <span class='activeDevice'>"+deviceName+"</span></p></div><div name='"+ lastUsedTtl +"' class='col2'><p class='activeDevice'>"+licenseDataList[x].lastUsed+"</p></div></li>"; }
					else {
						licenseList += "<li><div name='"+licenseAvailTtl+"' class='col1'><p>"+licenseNumber+". <span class='available'>"+ licenseAvailTtl +"</span></p></div><div name='"+ lastUsedTtl +"' class='col2'></div></li>"; }} 
				$("#licenseList").html(licenseList); $("#licenseListContentArea").show(); soh(["licenseList"]); scrollListWidth(["licenseList"]); $("#noLicenses").hide(); }
		} else if(ajaxLoadLicense.readyState==4){
			$("#licenseListContentArea").hide(); $("#noLicenses").show();
			alertDialog(systemErrorMsg); //Unable to load details
		} };
	ajaxLoadLicense.open("GET","/systemAdmin/workspace/licenses/mobileLicenseList.html"+document.location.search.substring()+"&"+params,true);
	ajaxLoadLicense.send(); }

//--------------------------------------------------------------------------------------

function mobileLicense(multplLicenses){
	loadLicense();
	
	if(multplLicenses == true){	
		var appData = JSON.parse($("#mobileAppData").text());
		appData = appData.apps;

		$("#mobileAppAC").autoselector({
				"isComboBox": true, 
				"defaultValue": null,
				"selectedCSS": "autoselector-selected",
				"shouldCategorize": false,
				"data": appData
			})
			.on("itemSelected", function(){ 
				var slctValue = $("#mobileAppAC").autoselector("getSelectedValue");
				$("#appTitle").html($("#mobileAppAC").autoselector("getSelectedLabel"));
				loadLicense(slctValue); }); }
	
	$(document.body).on("click", "#licenseCountArea * .edit", function(event){ event.preventDefault(); 
		testFormEdit($("#mobileLicenseEditForm")); $("#licenseDisplay").hide(); $("#licenseEdit").show(); $("#licenseCountForm").trigger('focus'); });
	$(document.body).on("click", "#licenseCountArea * .cancel", function(event){ event.preventDefault(); 
		$("#licenseDisplay").show(); $("#licenseEdit").hide(); 
		$("#licenseCountForm").val($("#licenseAccnt").html())});
	
	$("#mobileLicenseEditForm").on('submit', function(event){ event.preventDefault();
		var params = $.param(serializeForm("#mobileLicenseEditForm"));
		var ajaxSaveLicenses = GetHttpObject();
		ajaxSaveLicenses.onreadystatechange = function(){
			if (ajaxSaveLicenses.readyState==4 && ajaxSaveLicenses.status == 200){
				var response =  ajaxSaveLicenses.responseText.split(":");
				if(response[0] == "true") {
					$("#postToken").val(response[1]);
					loadLicense();
					$("#mainContent").removeClass("edit");
					$("#licenseEdit").hide(); $("#licenseDisplay").show(); }} 
			else if(ajaxSaveLicenses.readyState==4){
				alertDialog(failedSaveMsg); } };
		ajaxSaveLicenses.open("POST","/systemAdmin/workspace/licenses/saveLicenseCount.html",true);
		ajaxSaveLicenses.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveLicenses.setRequestHeader("Content-length", params.length);
		ajaxSaveLicenses.setRequestHeader("Connection", "close");
		ajaxSaveLicenses.send(params); });
	
	$(document.body).on("click", "#licenseCountArea * .save", function(event){ event.preventDefault(); $("#mobileLicenseEditForm").trigger('submit'); });}
