//--------------------------------------------------------------------------------------

var scrollTest = false;
var scrollPage = '';

function getNoteHistory(){
	actvTabSwtch = false;
	$("#notesPosID").val($("#contentID").val());
	$("#noteList")
		.paginatetab({
			"url": "payStationDetails/historyNotes.html",
			"formSelector": "#noteListForm",
			"rowTemplate": unescape($("#noteListTemplate").html()),
			"responseExtractor": extractNotesLog,
			"dataKeyExtractor": extractDataKey,
			"emptyMessage": noNotesMsg,
			"submissionFormat": "GET"
		})
		.paginatetab("setupSortingMenu", "#noteListHeader")
		.paginatetab("reloadData");
}

function extractNotesLog(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).noteHistoryList;
	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;	
	context.page = buffer.page;
	
	return result;
}


//--------------------------------------------------------------------------------------

function merchantAccountWarning(status, merchantType)
{
	var merchantAccntBtn = {},
	merchantAreaObj = {};
	
	if(merchantType === "creditMerchant"){
		merchantAreaObj.expand = $("#formCreditCardAccountExpand");
		merchantAreaObj.field = $("#formCreditCardAccount");
		merchantAreaObj.parent = $("#paystationMerchantAccount");
		merchantAreaObj.btn = $("#merchantAccountSwitchBtn");
	}
	else if(merchantType === "customMerchant"){
		merchantAreaObj.expand = $("#formCustomCardAccountExpand");
		merchantAreaObj.field = $("#formCustomCardAccount");
		merchantAreaObj.parent = $("#customMerchantAccount");
		merchantAreaObj.btn = $("#customMerchantSwitchBtn");
	}
	else if(merchantType === "all"){
		merchantAreaObj.expand = $("#formCreditCardAccountExpand, #formCustomCardAccountExpand");
		merchantAreaObj.field = $("#formCreditCardAccount, #formCustomCardAccount");
		merchantAreaObj.parent = $("#paystationMerchantAccount, #customMerchantAccount");
		merchantAreaObj.btn = $("#merchantAccountSwitchBtn, #customMerchantSwitchBtn");
	}

	if(status == true)
	{		
		merchantAccntBtn.btn1 = { text: changeMrchntBtn, click: function(event, ui) { 
			merchantAreaObj.expand.removeClass("disabled"); 
			merchantAreaObj.field.attr("disabled", false); 
			merchantAreaObj.parent.children("section").removeClass("disabled"); 
			$("#payStationSettings").find(".save").removeClass("disabled"); 
			$(this).scrollTop(0); $(this).dialog( "close" );  } };
		merchantAccntBtn.btn2 = { text: cancelBtn, click: function() { 
			$(this).scrollTop(0); $(this).dialog( "close" ); 
			merchantAreaObj.btn.children(".checkBox").removeClass("checked"); } };
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox").html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article>"+merchantAccountChangeMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: merchantAccntBtn });
		btnStatus(changeMrchntBtn);
	} else {
		merchantAreaObj.parent.children("section").addClass("disabled");
		merchantAreaObj.expand.addClass("disabled");
				
		if($("#merchantAccountSwitchHidden").val() === "false" && $("#customMerchantSwitchHidden").val() === "false" && $("#locationSwitchHidden").val() === "false"){ 
			$("#payStationSettings").find(".save").addClass("disabled"); }
		
		if(merchantType === "creditMerchant" || merchantType === "all"){
			$("#formCreditCardAccount")
				.autoselector("reset")
				.attr("disabled", true).removeClass("active"); }
		if(merchantType === "customMerchant" || merchantType === "all"){
			$("#formCustomCardAccount")
				.autoselector("reset")
				.attr("disabled", true).removeClass("active"); }
	}
}

//--------------------------------------------------------------------------------------

function locationWarning(status)
{
	if(status == true)
	{
		var locationChngBtn = {};
		locationChngBtn.btn1 = { text: chngLocationBtn, click: function(event, ui) { $("#locationSlctExpand").removeClass("disabled"); $("#locationSlct").attr("disabled", false); $("#paystationLocation").children("section").removeClass("disabled"); $("#payStationSettings").find(".save").removeClass("disabled"); $(this).scrollTop(0); $(this).dialog( "close" ); } };
		locationChngBtn.btn2 = { text: cancelBtn, click: function() { $(this).scrollTop(0); $(this).dialog( "close" ); $("#locationBtn").children(".checkBox").removeClass("checked"); } };
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox").html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article>"+chngLocationMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: locationChngBtn });
		btnStatus(chngLocationBtn);
	} else {
		$("#paystationLocation").children("section").addClass("disabled");
		$("#locationSlctExpand").addClass("disabled");
		
		if($("#merchantAccountSwitchHidden").val() === "false" && $("#customMerchantSwitchHidden").val() === "false" && $("#locationSwitchHidden").val() === "false"){ 
			$("#payStationSettings").find(".save").addClass("disabled"); }

		
		$("#locationSlct")
			.autoselector("reset")
			.attr("disabled", true).removeClass("active");
	}
}

//--------------------------------------------------------------------------------------

function linuxWarning(status)
{
	if(status == true)
	{
		var linuxChngBtn = {};
		linuxChngBtn.btn1 = { text: chngLinuxBtn, click: function(event, ui) { $("#linuxMultiEditLabel").removeClass("disabled"); $("#payStationSettings").find(".save").removeClass("disabled"); $(this).scrollTop(0); $(this).dialog( "close" ); } };
		linuxChngBtn.btn2 = { text: cancelBtn, click: function() { $(this).scrollTop(0); $(this).dialog( "close" ); } };
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox").html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article>"+chngLinuxMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: linuxChngBtn });
		btnStatus(chngLinuxBtn);
	} else {
		$("#linuxMultiEditLabel").addClass("disabled");
		if($("#merchantAccountSwitchHidden").val() === "false" && $("#customMerchantSwitchHidden").val() === "false" && $("#locationSwitchHidden").val() === "false"){ 
			$("#payStationSettings").find(".save").addClass("disabled"); }
	}
}

//--------------------------------------------------------------------------------------

function moveCustomerWarning(status)
{
	if(status == true)
	{
		var $moveCustomerBox = $("<section></section>");
		$moveCustomerBox.attr("id", "movePayStationWrning");
		
		var moveCustomerBtn = {};
		moveCustomerBtn.btn1 = { text: moveCustomerMsgBtn, click: function(event, ui) { 
			if($(".multiEditForm").length){
				locationWarning(false);
				merchantAccountWarning(false, "all");
				$("#customerMoveInput").removeClass("disabled").parent("section").removeClass("disabled");
				$("#locationBtn").addClass("disabled").children(".checkBox").removeClass("checked");
				$("#merchantAccountSwitchBtn").addClass("disabled").children(".checkBox").removeClass("checked");
				$("#payStationSettings").find(".save").removeClass("disabled"); 
			} else {
				$("#customerMoveInput").removeClass("disabled").parent("section").removeClass("disabled");
				$("#formPayStationName").addClass("disabled").attr("disabled", true);
				$("#formSerialNumber").addClass("disabled").attr("disabled", true);
				$("#paystationMerchantAccount").children("section").addClass("disabled");
				$("#customMerchantAccount").children("section").addClass("disabled");
				$("#formCreditCardAccountExpand, #formCustomCardAccountExpand").addClass("disabled");
				$("#formCreditCardAccount, #formCustomCardAccount").attr("disabled", true).removeClass("active");
				$("#paystationLocation").children("section").addClass("disabled");
				$("#locationSlctExpand").addClass("disabled");
				$("#locationSlct").attr("disabled", true).removeClass("active");
				$("#paystationBillable").find(".checkboxLabel").addClass("disabled");
				$("#paystationBundledData").find(".checkboxLabel").addClass("disabled");
				$("#btnPayStationActivate").addClass("disabled");
				$("#formHidePayStation").addClass("disabled");
			}
			 
			$("#CustomerSearch").attr("disabled", false); $(this).scrollTop(0); $(this).dialog( "close" ).dialog("destroy"); 
		} };
		moveCustomerBtn.btn2 = { text: cancelBtn, click: function() { $(this).scrollTop(0); $(this).dialog( "close" ).dialog("destroy"); $("#customerMoveBtn").children(".checkBox").removeClass("checked"); $("#movePayStationHidden").val(""); } };
		if($("#movePayStationWrning.ui-dialog-content").length){$("#movePayStationWrning.ui-dialog-content").dialog("destroy");}
		$moveCustomerBox.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article>"+moveCustomerMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: moveCustomerBtn });
		btnStatus(moveCustomerMsgBtn);
	} else {
		
		if($(".multiEditForm").length){
			$("#customerMoveInput").addClass("disabled").parent("section").addClass("disabled");
			$("#locationBtn").removeClass("disabled").children(".checkBox").removeClass("checked");
			$("#merchantAccountSwitchBtn").removeClass("disabled").children(".checkBox").removeClass("checked");
			$("#payStationSettings").find(".save").addClass("disabled");
		} else {
			$("#customerMoveInput").addClass("disabled");
			$("#formPayStationName").removeClass("disabled").attr("disabled", false);
			$("#formSerialNumber").removeClass("disabled").attr("disabled", false);
			$("#paystationMerchantAccount").children("section").removeClass("disabled");
			$("#customMerchantAccount").children("section").removeClass("disabled");
			$("#formCreditCardAccountExpand, #formCustomCardAccountExpand").removeClass("disabled");
			$("#formCreditCardAccount, #formCustomCardAccount").attr("disabled", false);
			$("#paystationLocation").children("section").removeClass("disabled");
			$("#locationSlctExpand").removeClass("disabled");
			$("#locationSlct").attr("disabled", false);
			$("#paystationBillable").find(".checkboxLabel").removeClass("disabled");
			$("#paystationBundledData").find(".checkboxLabel").removeClass("disabled");
			$("#btnPayStationActivate").removeClass("disabled");
			$("#formHidePayStation").removeClass("disabled");
		}
		$("#CustomerSearchId").val("");
		$("#CustomerSearch").val("").attr("disabled", true).removeClass("active");
		$("#customerMoveInput").children("label").removeClass("active").show();
	}
}

//--------------------------------------------------------------------------------------

function showPayStationDetails(payStationDetails, payStationID, action, tabID)
{
	var showCallBack = '',
		psData = JSON.parse(payStationDetails.responseText);
	$("#payStationSettings").find(".checkBox").removeClass("checked");
	$("#movePayStationHidden").val("");
	$("#mapHolder").html("");
	posStatusMsg = "";
	if(action === "multiEdit"){
		$("#posEditToken").val(psData.payStationForm.postToken);
		
		var psRandomIds = "", psSerialNumbers = "", linuxStatusArray = psData.payStationForm.linuxes, linuxStatus = true;
		for(x=0;x<payStationID.length;x++)
		{
			if(x==0){ psRandomIds = payStationID[x].randomId; psSerialNumbers = payStationID[x].label}
			else{ psRandomIds = psRandomIds + "," + payStationID[x].randomId; psSerialNumbers = psSerialNumbers + ", " + payStationID[x].label }
		}
		// globalPostToken = psData.payStationForm.postToken;
		$("#formPayStationName").val("");
		$("#formSerialNumber").val("");
		$("#formPayStationID").val("");
		$("#formPayStationStatus").val("");
		$("#formComment").val("");
		$("#formPayStationList").val(psRandomIds);
		$("#payStationSerialNumbers").html(psSerialNumbers);
		$("#formPOSBillableHidden").val("");
		$("#formPOSBillable").removeClass("checked");
		$("#formBundledDataHidden").val("");
		$("#formBundledData").removeClass("checked");
		$("#paystationMerchantAccount").children("section").addClass("disabled");
		$("#customMerchantAccount").children("section").addClass("disabled");
		$("#formCreditCardAccount, #formCustomCardAccount, #locationSlct").attr("disabled", true).removeClass("hasValue");
		$("#formCreditCardAccountExpand, #formCustomCardAccountExpand, #locationSlctExpand").addClass("disabled");
		$("#customerMoveInput").parent("section").addClass("disabled");
		
		if(!Array.isArray(linuxStatusArray)) { linuxStatusArray = [ linuxStatusArray ]; }
		
		for(var x = 0; x < linuxStatusArray.length; x++){
			if(linuxStatusArray[x] === false){ linuxStatus = false; break; } }
		
		if(linuxStatus){ 
			$("#linuxFlagCheckHidden").val("true"); 
			$("#linuxMultiEditLabel").addClass("active");
			$("#linuxMultiEditLabelMultiBtn").addClass("disabled");
			$("#linuxMultiEditLabelMultiBtn").children(".checkBox").addClass("checked"); }
		else { 
			$("#linuxFlagCheckHidden").val("false"); 
			$("#linuxMultiEditLabel").removeClass("active");
			$("#linuxMultiEditLabelMultiBtn").removeClass("disabled");
			$("#linuxMultiEditLabelMultiBtn").children(".checkBox").removeClass("checked"); }	
		
		if(psData.payStationForm.locations){ 
			var locationIDValue = (psData.payStationForm.locations.randomId)? psData.payStationForm.locations.randomId : null; }
		else{ var locationIDValue = null; }	
		$("#locationSlct")
			.autoselector({"defaultValue": locationIDValue})
			.autoselector("setSelectedValue", locationIDValue, true);
		
		if(psData.payStationForm.creditCardMerchantAccounts){
			var crdtCardIDValue = (psData.payStationForm.creditCardMerchantAccounts.randomId)? psData.payStationForm.creditCardMerchantAccounts.randomId : null; }
		else { var crdtCardIDValue = null; }
		$("#formCreditCardAccount")
			.autoselector({"defaultValue": crdtCardIDValue})
			.autoselector("setSelectedValue", crdtCardIDValue, true);
			
		if(psData.payStationForm.customCardMerchantAccounts){
			var cstmCardIDValue = (psData.payStationForm.customCardMerchantAccounts.randomId)? psData.payStationForm.customCardMerchantAccounts.randomId : null; }
		else{ var cstmCardIDValue = null; }
		$("#formCustomCardAccount")
			.autoselector({"defaultValue": cstmCardIDValue})
			.autoselector("setSelectedValue", cstmCardIDValue, true);
		
		var anyMovedPOS = false;
		if(psData.payStationForm.payStations){
			if(psData.payStationForm.payStations instanceof Array === false){ var tempPosList = [psData.payStationForm.payStations]; }
			else{ var tempPosList = psData.payStationForm.payStations; }
			for(x = 0;x<tempPosList.length;x++)
			{ if(tempPosList[x].isDecommissioned || tempPosList[x].active){ anyMovedPOS = true; } }
		}
		
		$("#paystationHide, #paystationActivation").hide();
		if(anyMovedPOS){ $("#deactiveFeaturesArea").hide(); }
		else{  $("#deactiveFeaturesArea").show(); }
		
		$("#formPOSBillableHidden").val("");
		$("#formPOSBillable").removeClass("checked");
		$("#formBundledDataHidden").val("");
		$("#formBundledData").removeClass("checked");
		$("#paystationLocation").children("section").addClass("disabled");
		$("#payStationSettings").find(".save").addClass("disabled");
		$("#paystationDetails").addClass("form multiEditForm");
		snglSlideFadeTransition("show", $("#paystationDetails"));
		testFormEdit($("#payStationSettings"));
		$(document.body).scrollTop(0); } 
	else {
		$("#posEditToken").val(psData.payStationDetails.postToken);
		
		psType = psData.payStationDetails.mapInfo.payStationType;
		if(psType === 9){ $("#alertsBtn").hide(); }
		else{ $("#alertsBtn").show(); }
		$("#formPayStationList").val("");
		$("#formComment").val("");
		$("#payStationSerialNumbers").html("");
		var parentObj =$("#statusBtn");
		var parentObjStatus = parentObj.hasClass("current");
		if(!parentObjStatus){ 
			parentObj.siblings(".tabSection").each(function(){
				if($(this).hasClass("current")){var curArea = $(this).attr("id").replace("Btn", "Area"); $("#"+curArea).hide(); $(this).removeClass("current");}
			});
			parentObj.addClass("current"); }	
		//Process JSON
		// globalPostToken = psData.payStationDetails.postToken;
		if(psData.payStationDetails.active === false || psData.payStationDetails.isDecommissioned){
			if(psData.payStationDetails.isDecommissioned){ tempStatusMsg = decommissionedMsg; }
			else { tempStatusMsg = deactivatedMsg; }		
			var posActiveState = " <span class='statusNote'>[ "+tempStatusMsg,
				posVisibility = (psData.payStationDetails.hidden === true)? " - <em>"+hiddenMsg+"</em> ]</span>" : " ]</span>";
			posStatusMsg = posActiveState + posVisibility; }
		
		var posLinuxStatus = (psData.payStationDetails.isLinux)? " <strong>"+linuxPOSMsg+"</strong>" : "";
		posStatusMsg = posLinuxStatus + posStatusMsg;

		if(psData.payStationDetails.alertCount > 0){ $("#activeAlerts").html(psData.payStationDetails.alertCount).show(); }
		else{ $("#activeAlerts").html("").hide(); }

		$("#contentID, #formPayStationID").val(payStationID);
		$("#paystationName").html(psData.payStationDetails.name + " <span class='note'>"+psData.payStationDetails.serialNumber + posStatusMsg +"</span>");
		$("#formPayStationName").val(psData.payStationDetails.name);
		$("#formSerialNumber").val(psData.payStationDetails.serialNumber);
		if(psData.payStationDetails.isSerialNumberFixed == true){$("#formSerialNumber").addClass("disabled").attr("disabled", "disabled");} 
		else {$("#formSerialNumber").removeClass("disabled").attr("disabled", false);}
		$("#payStationTypeImg").children("img").attr("src", imgLocalDir+"paystation_"+payStationIcn[psType].img[0]+"Lrg.jpg");

		$("#formPayStationStatus").val(psData.payStationDetails.active);
		$("#formCreditCardAccount, #formCustomCardAccount, #locationSlct").val("").attr("disabled", false).removeClass("hasValue");
		$("#formCreditCardAccountExpand, #formCustomCardAccountExpand, #locationSlctExpand").removeClass("disabled");
		$("#paystationMerchantAccount").children("section").removeClass("disabled");
		$("#customMerchantAccount").children("section").removeClass("disabled");
		$("#paystationLocation").children("section").removeClass("disabled");
		
		if(psData.payStationDetails.isLinux){ 
			$("#linuxFlagCheckHidden").val("true"); 
			$(".payStationLinuxUpgrade-edit").find("label").addClass("disabled active");
			$(".payStationLinuxUpgrade-edit").find(".checkBox").addClass("checked"); }
		else{ 
			$("#linuxFlagCheckHidden").val("false"); 
			$(".payStationLinuxUpgrade-edit").find("label").removeClass("disabled active");
			$(".payStationLinuxUpgrade-edit").find(".checkBox").removeClass("checked"); }	

		
		var locationIDValue = (psData.payStationDetails.locationRandomId)? psData.payStationDetails.locationRandomId : null;
		$("#locationSlct")
			.autoselector({"defaultValue": locationIDValue})
			.autoselector("setSelectedValue", locationIDValue, true);
		
		var crdtCardIDValue = (psData.payStationDetails.creditCardMerchantRandomId)? psData.payStationDetails.creditCardMerchantRandomId : null; 
		$("#formCreditCardAccount")
			.autoselector({"defaultValue": crdtCardIDValue})
			.autoselector("setSelectedValue", crdtCardIDValue, true);
		
		var cstmCardIDValue = (psData.payStationDetails.customCardMerchantRandomId)? psData.payStationDetails.customCardMerchantRandomId : null; 
		$("#formCustomCardAccount")
			.autoselector({"defaultValue": cstmCardIDValue})
			.autoselector("setSelectedValue", cstmCardIDValue, true);
				
		if(psData.payStationDetails.active == true){ 
			$("#btnPayStationDeactivate").show(); $("#btnPayStationActivate").hide(); $("#deactiveFeaturesArea").hide(); } 
		else { $("#btnPayStationDeactivate").hide(); $("#btnPayStationActivate").show(); $("#deactiveFeaturesArea").show();}
		
		$("#formPOSBillableHidden").val(psData.payStationDetails.billable);
		
		if(psData.payStationDetails.billable == true) { $("#formPOSBillable").addClass("checked"); } 
		else { $("#formPOSBillable").removeClass("checked"); }
		
		$("#formBundledDataHidden").val(psData.payStationDetails.bundledData);
		
		if(psData.payStationDetails.bundledData == true) { $("#formBundledData").addClass("checked"); } 
		else { $("#formBundledData").removeClass("checked"); }
		
		$("#paystationHide").show();
		
		$("#hidePayStationCheckHidden").val(psData.payStationDetails.hidden);
		if(psData.payStationDetails.hidden == true){ $("#hidePayStationCheck").addClass("checked"); } 
		else { $("#hidePayStationCheck").removeClass("checked"); }
		
		if(psData.payStationDetails.isDecommissioned){ $("#paystationActivation, #customerMove").hide();}
		else{ $("#paystationActivation, #customerMove").show(); }
		
		$("#payStationSettings").find(".save").removeClass("disabled");
		clearErrors();
		loadPosTab(tabID, payStationID, psData, true);
		
		if(action == "edit") { 
			testFormEdit($("#payStationSettings"));
			$("#customerMoveInput").parent("section").removeClass("disabled");
			$("#paystationDetails").removeClass("multiEditForm").addClass("form editForm");
			snglSlideFadeTransition("show", $("#paystationDetails"), function(){$("#formPayStationName").trigger('focus'); }); }
		else {
			action = "display";
			showCallBack = function(){
				if(psType !== 9){
					var	payStationMarkers = new Array();
					if(psData.payStationDetails.mapInfo.latitude){
						payStationMarkers.push(new mapMarker(
							psData.payStationDetails.mapInfo.latitude, 
							psData.payStationDetails.mapInfo.longitude, 
							psData.payStationDetails.mapInfo.name, 
							psType,'',
							psData.payStationDetails.mapInfo.severity,'','','','','','',''
						));
						initMapStatic(payStationMarkers); } 
					else { initMapStatic("noData"); } } }
			$("#mainContent").removeClass("edit");
			$("#paystationDetails").removeClass("form editForm multiEditForm");
			snglSlideFadeTransition("show", $("#paystationDetails"), showCallBack); }
		$(document.body).scrollTop(0);
	}
}

//--------------------------------------------------------------------------------------

function loadPayStation(itemID, action, tabID, allPayStations){
	var hideCallBack = '';
	if(action == "multiEdit") {
		var payStationGroup = [];
		for(x=0;x<itemID.length;x++){ 
			payStationGroup[x] = itemID[x].randomId;
			$("#ps_"+itemID[x].randomId).find(".checkBox").addClass("checked"); }
		if(allPayStations === "all" || allPayStations === "locationRoute"){ 
			$("#paystationList").find("li").addClass("selected");
			$("#paystationList").find(".checkBox").addClass("checked");
			$("#selectAllBtn").find(".checkBox").addClass("checked"); }
		else { $("#paystationList").find(".checked").each(function(index) { $(this).parents("li").addClass("selected"); }); }

		hideCallBack = function(){
			var ajaxLoadPayStation = GetHttpObject();
			ajaxLoadPayStation.onreadystatechange = function(){
				if (ajaxLoadPayStation.readyState==4 && ajaxLoadPayStation.status == 200){
					showPayStationDetails(ajaxLoadPayStation, itemID, action); } 
				else if(ajaxLoadPayStation.readyState==4){
					alertDialog(systemErrorMsg); }//Unable to load location details
			};
			ajaxLoadPayStation.open("GET",LOC.editMultiPayStations+"?payStationID="+payStationGroup+"&"+document.location.search.substring(1),true);
			ajaxLoadPayStation.send(); }
		snglSlideFadeTransition("hide", $("#paystationDetails"), hideCallBack); } 
	else {
		if($("#contentID").val() === itemID){ 
			if(action === "edit"){
				hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
				slideFadeTransition($("#paystationDetails"), $("#paystationDetails"), hideCallBack); }
			else if(action === "display"){
				if($("#mainContent").hasClass("edit")){
					hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
					slideFadeTransition($("#paystationDetails"), $("#paystationDetails"), hideCallBack); }
				else{ 
					if($("#detailView").find("article:visible").length){ 
						var	$detailObj = $("#detailView").find("article:visible"),
							reportStrng = "report";
						if($detailObj.attr("id").indexOf(reportStrng) > -1 && tabID.indexOf(reportStrng) > -1){
							loadPosTab(tabID, itemID, ""); }
						else{
							snglSlideFadeTransition("hide", $detailObj, function(){ loadPosTab(tabID, itemID, ""); }); } }
					else{ loadPosTab(tabID, itemID, ""); } } } }
		else { 
			var $this = $("#ps_"+itemID);
			if($("#paystationList").find("li.selected").length){ $("#paystationList").find("li.selected").toggleClass("selected"); }
			$this.toggleClass("selected");
	
			hideCallBack = function(){
				var ajaxLoadPayStation = GetHttpObject();
				ajaxLoadPayStation.onreadystatechange = function()
				{
					if (ajaxLoadPayStation.readyState==4 && ajaxLoadPayStation.status == 200){
						showPayStationDetails(ajaxLoadPayStation, itemID, action, tabID);
					} else if(ajaxLoadPayStation.readyState==4){
						alertDialog(systemErrorMsg); //Unable to load location details
					}
				};
				ajaxLoadPayStation.open("GET",LOC.loadPayStations+"?payStationID="+itemID+"&"+document.location.search.substring(1),true);
				ajaxLoadPayStation.send(); }
			
			if($("#paystationDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#paystationDetails"), hideCallBack); }
			else{ hideCallBack(); } } } }

//--------------------------------------------------------------------------------------

function psSystemLoad(psID)
{
	if($("#systemLoadWdgt").length > 0){
		if(psType === 9){ $("#systemLoadWdgt").hide(); }
		else{ 
			$("#systemLoadWdgt").show();
			var ajaxPSSystemLoad = GetHttpObject({"showLoader": false});
			ajaxPSSystemLoad.onreadystatechange = function()
			{
				if (ajaxPSSystemLoad.readyState==4 && ajaxPSSystemLoad.status == 200){
					var systemLoad = JSON.parse(ajaxPSSystemLoad.responseText);
					var posWdgtValue = validatePOSValue(systemLoad.float);
					$("#systemLoadWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Plug.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'mA</h1></section>');
				} else if(ajaxPSSystemLoad.readyState==4){
					$("#systemLoadWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Plug.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A mA</h1></section>');
				}
			};
			ajaxPSSystemLoad.open("GET",LOC.payStationsSystemLoad+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSSystemLoad.send();  
			
			var ajaxPSSystemLoadGraph = GetHttpObject({"showLoader": false});
			ajaxPSSystemLoadGraph.onreadystatechange = function()
			{
				if (ajaxPSSystemLoadGraph.readyState==4 ){
					$("#systemLoadGraphData").html(ajaxPSSystemLoadGraph.responseText);
					displaySensorGraph($("#systemLoadGraph"), $("#systemLoadGraphData"));
				}
			};
			ajaxPSSystemLoadGraph.open("GET",LOC.payStationsSystemLoadTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSSystemLoadGraph.send();} }
}

//--------------------------------------------------------------------------------------

function showPSStatus(psID, psData){
	if(psData){ 
		$("#psLastSeen").find(".value").html(psData.payStationDetails.lastSeen);
		$("#batteryVoltageWdgt, #inputCurrentWdgt, #ambientTemperatureWdgt, #relativeHumidityWdgt, #ControllerTemperatureWdgt, #systemLoadWdgt").children(".groupBoxBody").html(loadObj); 
		psBatteryVoltage(psID);
		psInputCurrent(psID);
		psAmbientTemprature(psID);
		psRelativeHumidity(psID);
		psControllerTemprature(psID);
		psSystemLoad(psID);
		$(document.body).scrollTop(0); $("#statusArea").fadeIn(function(){ actvTabSwtch = false; }); 
	} else{
		var ajaxLoadPayStation = GetHttpObject();
		ajaxLoadPayStation.onreadystatechange = function()
		{
			if (ajaxLoadPayStation.readyState==4 && ajaxLoadPayStation.status == 200){
				var psData = JSON.parse(ajaxLoadPayStation.responseText);
				$("#psLastSeen").children(".value").html(psData.payStationDetails.lastSeen); 
				psBatteryVoltage(psID);
				psInputCurrent(psID);
				psAmbientTemprature(psID);
				psRelativeHumidity(psID);
				psControllerTemprature(psID);
				psSystemLoad(psID);
				$(document.body).scrollTop(0); $("#statusArea").fadeIn(function(){ actvTabSwtch = false; }); 
			} else if(ajaxLoadPayStation.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load location details
			}
		};
		ajaxLoadPayStation.open("GET",LOC.loadPayStations+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
		ajaxLoadPayStation.send();
	}
}

//--------------------------------------------------------------------------------------

function openAddPayStationForm() {
	var $addDialogBox = $("#payStationsAddFormBox");
	var bttns = {
			"btn1": {
				text: addPayStation,
				click: saveNewPayStations
			},
			"btn2": {
				text: cancelBtn,
				click: function() { cancelConfirm($(this)); }
			}
		};
		
		document.getElementById("payStationEditForm").reset();
		$addDialogBox
			.dialog({
				title: addPayStation,
				modal: true,
				resizable: false,
				closeOnEscape: false,
				width: 350,
				height: "auto",
				hide: "fade",
				zIndex: 3000,
				buttons: bttns,
				stack: true,
				dragStop: function(){ checkDialogPlacement($(this)); }
			});
		btnStatus(addPayStation);
		testPopFormEdit($("#payStationEditForm"), $("#payStationsAddFormBox"));
		$("#formSerialNumbers").trigger('focus');
}

function initNewPayStationsForm() {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if((request.readyState === 4) && (request.status === 200)) {
			openAddPayStationForm(); } };
	request.open("GET", LOC.addPayStationsForm+"?"+document.location.search.substring(1), true);
	request.send();
}

function saveNewPayStations(event) {
/*	var	serialError = false,
		serialErrorNumber = [],
		serialNum = $("#formSerialNumbers").val().split("\n");
	for(x=0; x<serialNum.length; x++){
			var serialNumSplit = serialNum[x].split(" "); 
			if(serialNumSplit instanceof Array){
				serialNum.splice(x,1);
				for(var y=serialNumSplit.length-1; y>=0; y--){ 
					serialNum.splice(x,0,serialNumSplit[y]); } } }
	for(x=0; x<serialNum.length; x++){
		if(serialNum[x].length > 12){ serialError = true; serialErrorNumber.push(serialNum[x]); } }
	$("#formSerialNumbers").val(serialNum.join("\n"));
	if(serialError === true){ 
		$("#formSerialNumbers").addClass("error");
		var serialErrorNumberString = "";
		for(var x=0; x<serialErrorNumber.length; x++){ serialErrorNumberString += "<li>"+ serialErrorNumber[x] +"</li>"; }
		alertDialog(serialNmbrLengthExceedMsg+ "<ul>" +serialErrorNumberString+ "<ul>");
		serialErrorNumber = []; }
	else { 
*/
		//	$("#posAddToken").val(globalPostToken);
		var params = $("#payStationEditForm").serialize();
		var request = GetHttpObject({
			"postTokenSelector": "#posAddToken",
			"triggerSelector": (event) ? event.target : null
		});
		request.onreadystatechange = function(responseFalse, displayedMessages) {
			if((request.readyState === 4) && (request.status === 200)) {
				if(!responseFalse) {
					if(!displayedMessages) {
						noteDialog(payStationSavedMsg);
					}
					
					setTimeout("location.reload()", 500);
				}
			}
			else if(request.readyState === 4) {
				alertDialog(payStationSaveFailedMsg);
			}
		};	
		request.open("POST", LOC["addPayStations"], true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.setRequestHeader("Content-length", params.length);
		request.setRequestHeader("Connection", "close");
		request.send(params); //}
}

//--------------------------------------------------------------------------------------

/*function posSelectionCheck(posID, eventAction)
{
	var itemID = posID.split("_");
	var $this = $("#ps_"+itemID[1]);
	if(eventAction == "hide"){ 
		$this.toggleClass("selected"); $("#paystationDetails").fadeOut(); $("#contentID").val(""); }
	else { 
		if($("#contentID").val() != itemID[1] || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			if($("#paystationList").find("li.selected").length){ $("#paystationList").find("li.selected").toggleClass("selected"); }
			$this.toggleClass("selected");
			
			if(eventAction == "edit" && $("#contentID").val() == itemID[1]){
				$("#paystationDetails").fadeOut(function(){$(".mainContent").addClass("form editForm"); $("#paystationDetails").fadeIn();});} 
			else if(eventAction == "display" && $("#contentID").val() == itemID[1]){
				$("#paystationDetails").fadeOut(function(){$(".mainContent").removeClass("form editForm"); $("#paystationDetails").fadeIn();});} 
			else {loadPayStation(itemID[1], eventAction); }}}
}
*/
//--------------------------------------------------------------------------------------

function posSelectionCheck(posID, eventAction, tabID)
{
	var	itemID = posID.split("_"),
		tempID = itemID[1],
		$this = $("#ps_"+tempID),
		allPayStations = '';
	if(eventAction == "hide"){ 
		crntPageAction = "";
		history.pushState(null, null, location.pathname);
		hidePS($this); }
	else { 
		if($("#contentID").val() != itemID[1] || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
///////////////
			if(eventAction === "edit"){ $this.siblings().find(".checkBox").removeClass("checked"); $this.find(".checkBox").addClass("checked"); }
///////////////			
			var stateObj = { itemObject: tempID, action: eventAction, tabObject: tabID };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
			if(eventAction == "multiEdit"){ allPayStations = stateObj.tabObject; }
			loadPayStation(stateObj.itemObject, stateObj.action, stateObj.tabObject, allPayStations); }}}

//--------------------------------------------------------------------------------------

function extractPOSList(POSList, context) {
	var buffer = JSON.parse(POSList).payStationsList;
	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

//--------------------------------------------------------------------------------------

function hidePS($this){
	snglSlideFadeTransition("hide", $("#paystationDetails"), function(){
		$("#contentID").val(""); 
		$("#mainContent").removeClass("edit");
		$("#paystationDetails").removeClass("form editForm multiEditForm"); });
	updateFilter(); 
	$("#selectAllBtn").find(".checkBox").removeClass("checked");
	$("#paystationList").find(".checkBox").removeClass("checked");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#paystationList").find("li.selected").length){ $("#paystationList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function loadPOSList()
{
	$("#paystationList").paginatetab({
		"url": "/systemAdmin/workspace/listPayStations.html",
		"itemLocatorURL": "/systemAdmin/workspace/locatePageWithPayStations.html",
		"formSelector": "#posFilterForm",
		"submissionFormat": "GET",
		"emptyMessage": noPaystationsMsg,
		"rowTemplate": unescape($("#paystationListTemplate").html()),
		"responseExtractor": extractPOSList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData"); 

	$("#paystationList").on("pageLoaded", function(){
	$("ul.pageItems").find("li").each(function(index, element) {
		if(selectedItemLoad){ 
			if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
				$(this).trigger('click');
				checkVisibleItem($(this), $("ul.pageItems"));
				selectedItemLoad = false; } }
		else if($("#contentID").val() !== "" && $("#paystationDetails").is(":visible")){ 
			if(($(this).attr("id") && $(this).attr("id").match($("#contentID").val())) && !$(this).hasClass("selected")){ 
				$(this).addClass("selected");
				checkVisibleItem($(this), $("ul.pageItems")); } } }); });		
}

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popPSAction(tempItemID, tempEvent, tempTab, stateObj){
	var allPayStation = '';
	if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#ps_"+tempItemID) : ""; hidePS($this); }
	else if(tempEvent === "multiEdit" && tempItemID instanceof Array === false){
		var posList = [];
		tempItemID = tempItemID.split(",");
		if(tempItemID.length === 1){ 
			if(tempItemID[0] === "all"){ updateFilter(); }
			else{ updateFilter(tempItemID[0]); } }
			$("#selectAllBtn").find(".checkBox").removeClass("checked");
			alertDialog(formLoadErrorMsg);
		allPayStation = tempTab; }
	else { 
		if(tempEvent === "multiEdit" && tempItemID[0].randomId == 0){ allPayStation = "all" }; 
		loadPayStation(tempItemID, tempEvent, tempTab, allPayStation); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

var scrollTest = false;
var scrollPage = '';
var itemSelectedID = '';
var allPayStations = '';

function paystations(updateFilter, tempPsID, tempEvent, tempTabID){
	reloadTab();
	if(!updateFilter){ loadPOSList(); }

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempTabID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempTabID = stateObj.tabObject;
		tempEvent = stateObj.action; }
	popPSAction(tempItemID, tempEvent, tempTabID, stateObj); }

if(!popStateEvent && (tempPsID !== undefined && tempPsID !== '')){ popPSAction(tempPsID, tempEvent, tempTabID); }
///////////////////////////////////////////////////	
	
	$("#locationPOSAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"persistToHidden": false,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"remoteFn": posLocationRouteAutoComplete })
	.on("itemSelected", function(event, ui) {
		var slctdCategory = $(this).autoselector("getSelectedObject").category;
		var slctValue = $(this).autoselector("getSelectedValue");
		if(slctValue != '-1') {
			if(slctdCategory == payStationMsg){ 
				posSelectionCheck("_"+slctValue, "display");
				itemSelectedID = slctValue;
				if(!$("#ps_"+slctValue).is(":visible")){
					updateFilter("", slctValue, "searchDisplay"); }
				else{ 
					$("#ps_"+slctValue).addClass("selected");
					checkVisibleItem($("#ps_"+slctValue), $("#paystationList")); } }
			else{
				if(slctdCategory === labelLocation){ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(slctValue); $("#routeACVal").val(''); }
				else if(slctdCategory === labelRoute){ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(''); $("#routeACVal").val(slctValue); }
				else{ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(''); $("#routeACVal").val(''); }
				$("#paystationList").paginatetab("reloadData"); }			
			if($(this).parents("ul.filterForm") && slctValue != ""){ filterQuerry = "&filterValue="+slctValue; }
			$(this).trigger('blur'); }
		return false; })
	.on("change", function(){
		var slctName = $(this).autoselector("getSelectedObject");
		if($(this).val()==="" && slctName.label !==""){$(this).val(slctName.label);} });
	
	//MERCHANT ACCOUNT AUTO COMPLETE
	$("#formCreditCardAccount").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": false,
		"data": ccMerchantAccntObj });

	$("#formCustomCardAccount").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": false,
		"data": vcMerchantAccntObj });
	
	$("#multiEditBtn").on('click', function(event){ event.preventDefault();
		var filterValue = $("#locationPOSAC").autoselector("getSelectedValue");
		
		if(!$(this).hasClass("disabled"))
		{
			if((filterValue === "" || filterValue === "all") && $("#selectAllBtn").children(".checkBox").hasClass("checked")){ allPayStations = "all"; }
			else if($("#selectAllBtn").children(".checkBox").hasClass("checked")) { allPayStations = "locationRoute"; }
			else{ allPayStations = ""; }
			
			if($("#paystationList").find(".checked").length === 1){ var itemID = $("#paystationList").find(".checked").parents("li").attr("id"); posSelectionCheck(itemID, "edit"); }
			else{
				$("#paystationList").find(".selected, .highlight").removeClass("selected highlight");
				var	selectedPayStations = [],
					payStationIDLst = '';
				if(allPayStations === "all" || allPayStations === "locationRoute"){ 
					$("#paystationList").find("li").addClass("selected");
					if(allPayStations === "all"){ 
						allSlctdMsg = allPOSMsg;
						payStationIDLst = "all";
						selectedPayStations[0] = {"randomId": "0", "label": allSlctdMsg };}
					else if(allPayStations === "locationRoute"){ 
						allSlctdMsg = $("#locationPOSAC").autoselector("getSelectedObject").label +" : "+ allPOSMsg;
						localRouteID = $("#locationPOSAC").autoselector("getSelectedValue");
						payStationIDLst = localRouteID;
						selectedPayStations[0] = {"randomId": localRouteID, "label": allSlctdMsg }; } }
				else {
					$("#paystationList").find(".checked").each(function(index) {
						$(this).parents("li").addClass("selected");
						var tempID = $(this).parents("li").attr("id").replace("ps_", "")
						payStationIDLst = (index > 0)? tempID+","+payStationIDLst : tempID ;
						selectedPayStations[index] = {"randomId":tempID, "label":$(this).attr("id")}; }); }
				var stateObj = { itemObject: selectedPayStations, action: "multiEdit" };
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
				crntPageAction = stateObj.action;history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+payStationIDLst+"&pgActn="+stateObj.action);
				loadPayStation(selectedPayStations, stateObj.action, "", allPayStations); } }
	});
	
	$(document.body).on("click", "li", function(event){
		if($(this).parents("ul").attr("id") === "paystationList"){
			if (event.ctrlKey) { $(this).find(".checkboxLabel").trigger('click'); } else if(!$(".multiEditForm").length){
				var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
				var locationID = $(this).attr("id");
				if(pausePropagation === true){ 
					pausePropagationLoop = setInterval(function(){ 
						if(pausePropagation === false){ posSelectionCheck(locationID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
						else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
				else{ posSelectionCheck(locationID, actionEvent); } } } });

	$("#paystationList").on("click", "a.edit", function(event){	event.preventDefault(); event.stopImmediatePropagation();
		if($("#mainContent").hasClass("edit")){ 
			event.stopImmediatePropagation(); 
			var itemID = $(this).attr("id");
			inEditAlert("site", function(){ posSelectionCheck(itemID, "edit");}); }
		else{ var itemID = $(this).attr("id"); posSelectionCheck(itemID, "edit"); }
	});

	
	$(document.body).on("click", ".checkboxLabel", function(event) {
		event.stopImmediatePropagation();
		if($(this).attr("id") == "selectAllBtn"){
			var checkBoxStatus = $(this).children(".checkBox").hasClass("checked");
			$("#paystationList").find(".checkBox").each(function(){ 
				if(checkBoxStatus){ $(this).addClass("checked"); $("#multiEditBtn").removeClass("disabled"); } 
				else { $(this).removeClass("checked"); $("#multiEditBtn").addClass("disabled"); } });
			if(checkBoxStatus){ 
				if($(".multiEditForm").length || $(".editForm").length){ $("#multiEditBtn").trigger('click'); }}
			else { 
				if($(".multiEditForm").length || $(".editForm").length){ $(".cancel").trigger('click'); } } }
		else if($(this).attr("id") == "merchantAccountSwitchBtn"){
			if($(this).children(".checkBox").hasClass("checked")){ merchantAccountWarning(true, "creditMerchant"); } else { merchantAccountWarning(false, "creditMerchant"); }} 
		else if($(this).attr("id") == "customMerchantSwitchBtn"){
			if($(this).children(".checkBox").hasClass("checked")){ merchantAccountWarning(true, "customMerchant"); } else { merchantAccountWarning(false, "customMerchant"); }} 
		else if($(this).attr("id") == "locationBtn"){
			if($(this).children(".checkBox").hasClass("checked")){ locationWarning(true); } else { locationWarning(false); }} 
		else if($(this).attr("id") == "linuxMultiEditLabelMultiBtn" && !$(this).hasClass("disabled")){
			if($(this).children(".checkBox").hasClass("checked")){ linuxWarning(true); } else { linuxWarning(false); }} 
		else if($(this).attr("id") == "customerMoveBtn"){
			if($(this).children(".checkBox").hasClass("checked")){ moveCustomerWarning(true); } else { moveCustomerWarning(false); }} 
		else if($(this).parents("#paystationList").length > 0) {
			if($("#paystationList").find(".checked").length > 0){ 
				$("#multiEditBtn").removeClass("disabled"); } 
			else{ $("#multiEditBtn").addClass("disabled"); }
	
			if($(".multiEditForm").length || $(".editForm").length){ 
				$(this).parent("li").toggleClass("selected"); 				
				if($("#paystationList").find(".checked").length == $("#paystationList").find(".checkBox").length){ 
					$("#selectAllBtn").children(".checkBox").addClass("checked"); $("#multiEditBtn").trigger('click');}
				else if($("#selectAllBtn").children(".checkBox").hasClass("checked")){ 
					$("#selectAllBtn").children(".checkBox").removeClass("checked");  $("#multiEditBtn").trigger('click'); }
				else if($("#paystationList").find(".checked").length > 1){ $("#multiEditBtn").trigger('click'); }
				else if($("#paystationList").find(".checked").length === 1){ 
					var itemID = $("#paystationList").find(".checked").parents("li").attr("id"); $("#contentID").val(""); 
					posSelectionCheck(itemID, "edit"); }
				else{ $(".cancel").trigger('click'); }}
			else if(!$(this).parent("li").hasClass("selected") && $(this).children(".checkBox").hasClass("checked")){ $(this).parent("li").addClass("highlight"); }
			else{$(this).parent("li").removeClass("highlight");} }
	});
	
	$(".pin").on('click', function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		window.location = LOC.payStationsPlacement+"?payStationID="+psID+"&"+document.location.search.substring(1);
	});
	
	$("#paystationNavArea").find(".tab").on('click', function(event){
		event.preventDefault();
		if(!actvTabSwtch){
			actvTabSwtch = true;
			var psID = $("#contentID").val(),
				$parentObj = $(this).parent(".tabSection"),
				$parentObjID = $parentObj.attr("id"),
				tabID = $parentObj.attr("id").replace("Btn", "");
			if(!$parentObj.hasClass("current")){ 
				var $that = $("#psNav").find(".tabSection.current")
				$that.removeClass("current"); 
				var curArea = $that.attr("id").replace("Btn", "Area"); 
				var stateObj = { itemObject: psID, action: "display", tabObject: tabID };
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
				snglSlideFadeTransition("hide",$("#"+curArea), function(){ loadPosTab(stateObj.tabObject, stateObj.itemObject, ""); }); }
			else{ actvTabSwtch = false; }} });	
	
	$("#reportsArea").find(".tab").on('click', function(event){ event.preventDefault();
		if(!actvTabSwtch && !$(this).hasClass("current")){
			var psID = $("#contentID").val(),
				tabID = '';
			if($(this).attr("id") === "transactionReportBtn"){ tabID = "reportsTransactions"; }
			else if($(this).attr("id") === "notesHistoryBtn"){ tabID = "reportsNotes"; }
			else{ tabID = "reports"; }
			var	stateObj = { itemObject: psID, action: "display", tabObject: tabID };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
			loadPosTab(stateObj.tabObject, stateObj.itemObject, ""); } });
	
	$(".reload").on('click', function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		var objID = $(this).attr("id");
		if(objID == "batteryVoltageReload"){ psBatteryVoltage(psID); }
		else if(objID == "inputCurrentReload"){ psInputCurrent(psID); }
		else if(objID == "ambientTemperatureReload"){ psAmbientTemprature(psID); }
		else if(objID == "relativeHumidityReload"){ psRelativeHumidity(psID); }
		else if(objID == "ControllerTemperatureReload"){ psControllerTemprature(psID); }
		else if(objID == "systemLoadReload"){ psSystemLoad(psID); }
		else if(objID == "btnReload"){ loadPayStation(psID, 'display');  }
	});

	$(document.body).on("click", ".svCSV", function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		var objID = $(this).attr("id");
		if(objID == "batteryVoltageHistory"){ objID = 0; }
		else if(objID == "inputCurrentHistory"){ objID = 5;}
		else if(objID == "ambientTemperatureHistory"){ objID = 3; }
		else if(objID == "relativeHumidityHistory"){ objID = 4; }
		else if(objID == "ControllerTemperatureHistory"){ objID = 2; }
		else if(objID == "systemLoadHistory"){ objID = 6; }
		$.ajax({
			url: LOC.historyDownload+"?payStationID="+psID+"&sensorTypeID="+objID+"&"+document.location.search.substring(1),
			success: function(data) { window.location = LOC.historyDownload+"?payStationID="+psID+"&sensorTypeID="+objID+"&"+document.location.search.substring(1); },
			error: function(data){ alertDialog(systemErrorMsg); } }); });
			
	$(document.body).on("click", "#posActivityExport", function(event){event.preventDefault();
		var psID = $("#contentID").val();
		$.ajax({
			url: LOC.activityHistoryDownload+"?payStationID="+psID+"&"+document.location.search.substring(1),
			success: function(data) { window.location = LOC.activityHistoryDownload+"?payStationID="+psID+"&"+document.location.search.substring(1); },
			error: function(data){ alertDialog(systemErrorMsg); } }); });
	
	$("#collectionReportArea").on("click", "#collectionReportList > span > li", function(event){
		event.preventDefault();
		if(!$(this).hasClass("notclickable")){
			$("#collectionDetailView").css('max-height', ($(window).innerHeight()-150)+"px"); 
			var	itemID = $(this).attr("id").split("_"),
				itemID = itemID[1];
			var ajaxCollectionReport = GetHttpObject();
			ajaxCollectionReport.onreadystatechange = function()
			{
				if (ajaxCollectionReport.readyState==4 && ajaxCollectionReport.status == 200){
					if($("#collectionDetailView.ui-dialog-content").length){$("#collectionDetailView.ui-dialog-content").dialog("destroy");}
					$("#collectionDetailView").html(ajaxCollectionReport.responseText);
					var collectionViewBtn = {};
					collectionViewBtn.btn1 = { text: exportPDFMsg, click: function(event, ui) {
						window.open(LOC.printCollectionDetailReport + "?collectionId=" + itemID + "&" + document.location.search.substring(1), "reportViewer");
					}};
					collectionViewBtn.btn2 = { text: closeMsg, click: function(event, ui) { $(this).scrollTop(0); $(this).dialog( "close" ); } };
					$("#collectionDetailView").dialog({
						title: collectionDetailsMsg,
						modal: true,
						resizable: false,
						closeOnEscape: true,
						maxHeight: $(window).innerHeight()-150,
						width: 475,
						hide: "fade",
						buttons: collectionViewBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }
					});
					btnStatus(closeMsg);	
				} else if(ajaxCollectionReport.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load details
				}
			};
			ajaxCollectionReport.open("GET",LOC.viewCollectionReportsDetail+"?collectionId="+itemID+"&"+document.location.search.substring(1),true);
			ajaxCollectionReport.send(); }
	});
	
	$("#transactionReportArea").on("click", "#transactionReportList > span > li", function(event){
		event.preventDefault();
		if(!$(this).hasClass("notclickable")){
			$("#transactionDetailView").css('max-height', ($(window).innerHeight()-150)+"px"); 
			var	itemID = $(this).attr("id").split("_"),
				itemID = itemID[1];
			var ajaxTransactionReport = GetHttpObject();
			ajaxTransactionReport.onreadystatechange = function()
			{
				if (ajaxTransactionReport.readyState==4 && ajaxTransactionReport.status == 200){
					if($("#transactionDetailView.ui-dialog-content").length){$("#transactionDetailView.ui-dialog-content").dialog("destroy");}
					$("#transactionDetailView").html(ajaxTransactionReport.responseText);
					var transactionViewBtn = {};
					transactionViewBtn.btn1 = { text: closeMsg, click: function(event, ui) {  $(this).scrollTop(0); $(this).dialog( "close" ); } };
					$("#transactionDetailView").dialog({
						title: transactionDetailsMsg,
						modal: true,
						resizable: false,
						closeOnEscape: true,
						maxHeight: $(window).innerHeight()-150,
						width: 475,
						hide: "fade",
						buttons: transactionViewBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }
					});
					btnStatus(closeMsg);
				} else if(ajaxTransactionReport.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load details
				}
			};
			ajaxTransactionReport.open("GET",LOC.viewTransactionReportsDetail+"?transactionId="+itemID+"&"+document.location.search.substring(1),true);
			ajaxTransactionReport.send(); }
	});
	
	$(document.body).on("click", ".removeFile", function(event){ event.preventDefault(); clearPOSAlerts($(this)); });
	
	$("#paystationForm").on("click", ".save", function(event) { event.preventDefault();
		if(!$(this).hasClass("disabled")) {
			var psID = $("#payStationSettings").find("#formPayStationID").val();
			// $("#posEditToken").val(globalPostToken);
			var params = $("#payStationSettings").serialize();
			var ajaxSavePayStation = GetHttpObject({ "postTokenSelector": "#posEditToken", "triggerSelector": (event) ? event.target : null });
			ajaxSavePayStation.onreadystatechange = function() {
				if (ajaxSavePayStation.readyState==4 && ajaxSavePayStation.status == 200) {
					var response = ajaxSavePayStation.responseText.split(":");
					if(response[0] == "true"){
						noteDialog(payStationSavedMsg);
						var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "psID", "pgActn"]);
						var itemValueString = ($(".multiEditForm").length)? "" : "&psID="+$("#contentID").val();
						crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
						var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry+itemValueString+"&pgActn="+crntPageAction;
						setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
/*					} else {
						var jsonData = JSON.parse(ajaxSavePayStation.responseText); // Evaluate message object returned from backend.
						var errorData = jsonData.errorStatus;
						$("#posEditToken").val(errorData.token);	
					}
*/				else if(ajaxSavePayStation.readyState==4){ alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
			ajaxSavePayStation.open("POST",LOC["savePayStations"],true);
			ajaxSavePayStation.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			ajaxSavePayStation.send(params);

		}
	});
	
	$("#locationSlct").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"selectedCSS": "autoselector-selected",
		"shouldCategorize": false,
		"data": locationsObj });
	
	$("#payStationSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
	
	$("#btnPayStationDeactivate").on('click', function(event){
		event.preventDefault();
		$("#formPOSBillable").removeClass("checked");
		$("#formPOSBillableHidden").val("false").trigger('change');
		$("#formPayStationStatus").val("false").trigger('change');
		$("#btnPayStationDeactivate").hide(); $("#btnPayStationActivate, #deactiveFeaturesArea").show();
	});
	$("#btnPayStationActivate").on('click', function(event){
		event.preventDefault();
		$("#formPOSBillable").addClass("checked");
		$("#formPOSBillableHidden").val("true").trigger('change');
		$("#formPayStationStatus").val("true").trigger('change');
		$("#btnPayStationDeactivate").show(); $("#btnPayStationActivate, #deactiveFeaturesArea").hide();
		$("#hidePayStationCheckHidden").val("false").trigger('change');
		$("#hidePayStationCheck").removeClass("checked");
	});
	
	$("#btnAddPayStation").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ initNewPayStationsForm(); } 
	}); 
}
