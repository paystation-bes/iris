(function($, undefined) {
	$.widget("ui.ajaxtab", {
		"options": {
			"formSelector": "",
			"rowTemplate": " - ",
			"responseExtractor": function(rawResponse, processingCtx) {
				return JSON.parse(rawResponse);
			},
			"rowExtractor": function(listObj, idx, processingCtx) {
				var list = listObj;
				if(!Array.isArray(list)) {
					list = [ listObj ];
				}
				
				return (idx >= list.length) ? null : list[idx];
			},
			"loadingPanelCSS": "loadItem",
			"compileTemplate": compileTemplate,
			"postTokenSelector": null,
			"triggerSelector": null,
			"idExtractor": function(rowObj) {
				return this.lastItemIdx;
			}
		},
		"_create": function() {
			var conf = this.options;
			
			if(typeof conf.emptyMessage === "undefined") {
				conf.emptyMessage = "<li id='noResults' class=\"listMessage notclickable\">" + ((typeof emptyMsg === "undefined") ? "Not Available" : emptyMsg) + "</li>";
			}
			
			this.container = this.element;
			this.loadingObj = this.element.find("." + this.options.loadingPanelCSS).remove().hide().appendTo(this.element);
			this.emptyMessage = $(this.options.emptyMessage).appendTo(this.container);
			this.templateFn = conf.compileTemplate(conf.rowTemplate);
			this.lastItemIdx = 0;
			
			return this;
		},
		"addRawData": function(rawData, clean) {
			if((typeof clean !== "undefined") && (clean === true)) {
				this.clearData();
			}
			
			this._drawPageData(rawData);
		},
		"_createItemIdxElement": function(rowId) {
			return $("<span>").addClass("row_" + rowId);
		},
		"getRowElements": function(rowId) {
			var result = $();
			if(rowId) {
				result = this.container.find("span.row_" + rowId).nextUntil("span[class*=row_]");
			}
			
			return result;
		},
		"addRow": function(rows) {
			var $result = $();
			if(rows) {
				if(!Array.isArray(rows)) {
					rows = [ rows ];
				}
				
				var len = rows.length, idx = -1, rowObj, $rowElem;
				while(++idx < len) {
					rowObj = rows[idx];
					
					++this.lastItemIdx;
					this.container.append(this._createItemIdxElement(this.options.idExtractor.call(this, rowObj)));
					
					$rowElem = $(this.templateFn(rowObj));
					this.container.append($rowElem);
					$result = $result.add($rowElem);
				}
				
				if(len > 0) {
					this.emptyMessage.remove();
				}
			}
			
			return $result;
		},
		"deleteRow": function(rowId) {
			var $result = $();
			if(rowId) {
				var $rowStart = this.container.find("span.row_" + rowId);
				$result = $rowStart.nextUntil("span[class*=row_]");
				
				$rowStart.remove();
				$result.remove();
				if(this.container.find("span[class*=row_]:first").length <= 0) {
					this.emptyMessage.appendTo(this.container).show();
				}
			}
			
			return $result;
		},
		"hasRows": function() {
			return (this.container.find("span[class*=row_]:first").length > 0);
		},
		"loadData": function(extraParams, clean, callback) {
			var conf = this.options;
			var that = this;
			
			var shouldLoad = true;
			if(conf.triggerSelector) {
				this._$triggerObj = $(conf.triggerSelector);
				if(this._$triggerObj.hasClass("inactive")) {
					shouldLoad = false;
				}
				else {
					this._$triggerObj.addClass("inactive");
				}
			}
			
			if(shouldLoad) {
				if((typeof clean !== "undefined") && (clean === true)) {
					this.clearData();
				}
				
				this.emptyMessage.hide();
				
				this.loadingObj.show();
				
				var params = "";
				
				var formSelector = conf.formSelector;
				if((formSelector != null) && (formSelector.length > 0)) {
					params = $(formSelector).serialize();
				}
				else {
					params = document.location.search.substring(1);
				}
				
				if(extraParams) {
					var buffer = [], value;
					for(var key in extraParams) {
						value = extraParams[key];
						if((typeof value === "undefined") || (value === null)) {
							// DO NOTHING !
						}
						else {
							buffer.push(key + "=" + value);
						}
					}
					
					params += "&" + buffer.join("&");
				}
				
				var request = GetHttpObject({
					"postTokenSelector": conf.postTokenSelector
				});
				
				request.onreadystatechange = function(responseFalse, displayedError) {
					if(request.readyState === 4) {
						that.loadingObj.fadeOut("slow", function() {
							var error = true;
							var drawResult = null;
							if((request.status === 200) && (!responseFalse)) {
								drawResult = that._drawPageData(request.responseText);
								that.element.trigger("pageLoaded");
								error = false;
							}
							else {
								that.container.show();
								if((typeof clean !== "undefined") && (clean === true)) {
									that.emptyMessage.show().appendTo(that.container);
								}
								
								if(!displayedError) {
									alertDialog(systemErrorMsg);
								}
							}
							
							that.loadingObj = that.loadingObj.remove().appendTo(that.element);
							
							if(that._$triggerObj) {
								that._$triggerObj.removeClass("inactive");
								delete that._$triggerObj;
							}
							
							if(typeof callback === "function") {
								callback(error, request.responseText, (drawResult) ? drawResult.translatedObj : null);
							}
						});
					}
				};
				
				request.open("POST", this.options.url, true);
				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				request.send(params);
			}
			
			return shouldLoad;
		},
		"clearData": function() {
			var $data = this.container.find(">*");
			
			var loadingPanelCSS = this.options.loadingPanelCSS;
			if(loadingPanelCSS && (loadingPanelCSS.length > 0)) {
				$data = $data.filter(":not(." + loadingPanelCSS + ")");
			}
			
			$data.remove();
			
			this.emptyMessage.appendTo(this.container).show();
		},
		"_drawPageData": function(rawData) {
			var conf = this.options;
			var processingCtx = {};
			var idx = 0;
			
			this.loadingObj.hide();
			
			var translatedObj = conf.responseExtractor(rawData, processingCtx);
			if(translatedObj) {
				var rowExtractor = conf.rowExtractor;
				
				var rowObj = rowExtractor(translatedObj, idx, processingCtx);
				while(rowObj) {
					this.addRow(rowObj);
					
					++idx;
					rowObj = rowExtractor(translatedObj, idx, processingCtx);
				}
			}
			
			if(idx <= 0) {
				this.emptyMessage.appendTo(this.container).show();
			}
			else {
				this.emptyMessage.remove();
			}
			
			return {
				"translatedObj": translatedObj,
				"rows": idx
			};
		}
	});
}(jQuery));
