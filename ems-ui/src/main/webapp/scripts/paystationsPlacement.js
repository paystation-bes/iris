function aniListHeight($listObj, newHeight, listChildCount)
{
	if($("html.ie8").length){ 
		curHeight = $listObj.height();
		if(curHeight > newHeight){$listObj.css("min-height", newHeight); }
		else if(curHeight < newHeight){$listObj.css("max-height", newHeight); }
		$listObj.animate({height:newHeight}, 
				function(){$(this).css("min-height", newHeight).css("max-height", newHeight);});}
	else{ 
		if(listChildCount === 0){ $listObj.css("min-height", newHeight); }
		$listObj.animate({height:newHeight}, 
				function(){ if(listChildCount === 1){ $(this).css("min-height", newHeight); }}); }
}

function balancePayStationLists()
{
var unplacedPayStations = $("#unplacedListItems").children("li.unplaced").length,
	placedPayStations = $("#placedListItems").children("li.placed").length;
	if($("li.listHeader").is(":visible")){ var listHeaderHeight = $("li.listHeader").filter(":visible").outerHeight(); }
	else{ var listHeaderHeight = 34; }
	if($("li.unplaced").is(":visible")){ var listItemHeight = $("li.unplaced").filter(":visible").outerHeight(); }
	else if($("li.placed").is(":visible")){ var listItemHeight = $("li.placed").filter(":visible").outerHeight(); }
	else{ var listItemHeight = 34; }

var	unplacedPayStationHeight = $("#unplacedListArea").height(),
	placedPayStationHeight = $("#placedListArea").height(),
	listMargin = $("#unplacedListArea").outerHeight(true) - unplacedPayStationHeight,
	allListMargin = listMargin*2,
	marginPlusHeader = allListMargin+listHeaderHeight,
	unplacedDif = (unplacedPayStationHeight-(unplacedPayStations*listItemHeight))-listHeaderHeight,
	placedDif = (placedPayStationHeight-(placedPayStations*listItemHeight))-listHeaderHeight,
	parentHeight = $("#unplacedListArea").parent().innerHeight()-$("#unplacedListArea").position().top-8,
	tempUnplacedListHeight = (unplacedPayStations*listItemHeight),
	tempPlacedListHeight = (placedPayStations*listItemHeight),
	ogListHeight = ((parentHeight/2)-listMargin)-listHeaderHeight,
	ogListAreaHeight = ogListHeight+listHeaderHeight+2;
	
	 if(unplacedPayStations == 0){
		 aniListHeight($("#unplacedListArea"), 0, 0);
		 tempPlacedListAreaHeight = tempPlacedListHeight+listHeaderHeight+2;
		 if(parentHeight <= tempPlacedListAreaHeight){
			$("#placedList").height(parentHeight-listHeaderHeight);
			aniListHeight($("#placedListArea"), parentHeight, placedPayStations); }
		 else{
			$("#placedList").height(tempPlacedListHeight);
		 	aniListHeight($("#placedListArea"), tempPlacedListAreaHeight, placedPayStations); }}
	else if(placedPayStations == 0){
		 aniListHeight($("#placedListArea"), 0, 0);
		tempUnplacedListAreaHeight = tempUnplacedListHeight+listHeaderHeight+2;
		if(parentHeight <= tempUnplacedListAreaHeight){ 
			$("#unplacedList").height(parentHeight-listHeaderHeight);
			aniListHeight($("#unplacedListArea"), parentHeight, unplacedPayStations); }
		else{ 
			$("#unplacedList").height(tempUnplacedListHeight);
			aniListHeight($("#unplacedListArea"), tempUnplacedListAreaHeight, unplacedPayStations); }}
	else if(tempPlacedListHeight > ogListHeight && tempUnplacedListHeight <= ogListHeight){ // Placed List is Larger than OGList Height but Unplaced is Not
		tempUnplacedListAreaHeight = tempUnplacedListHeight+listHeaderHeight+2;
		tempPlacedListAreaHeight = parentHeight-tempUnplacedListAreaHeight+2;
		tempPlacedListHeight = tempPlacedListAreaHeight-listHeaderHeight;
		$("#unplacedList").height(tempUnplacedListHeight+1);
		aniListHeight($("#unplacedListArea"), tempUnplacedListAreaHeight, unplacedPayStations);

		$("#placedList").height(tempPlacedListHeight);
		aniListHeight($("#placedListArea"), tempPlacedListAreaHeight, placedPayStations); }
	else if(tempPlacedListHeight <= ogListHeight && tempUnplacedListHeight > ogListHeight){ // Unplaced List is Larger than OGList Height but Placed is Not
		tempPlacedListAreaHeight = tempPlacedListHeight+listHeaderHeight+2;
		tempUnplacedListAreaHeight = parentHeight-tempPlacedListAreaHeight+2;
		tempUnplacedListHeight = tempUnplacedListAreaHeight-listHeaderHeight;
		$("#unplacedList").height(tempUnplacedListHeight);
		aniListHeight($("#unplacedListArea"), tempUnplacedListAreaHeight, unplacedPayStations);

		$("#placedList").height(tempPlacedListHeight+1);
		aniListHeight($("#placedListArea"), tempPlacedListAreaHeight, placedPayStations); }
	else if(tempPlacedListHeight < ogListHeight && tempUnplacedListHeight < ogListHeight){ // Both Lists Are Smaller than OGList Height
		tempPlacedListAreaHeight = tempPlacedListHeight+listHeaderHeight+2;
		tempUnplacedListAreaHeight = tempUnplacedListHeight+listHeaderHeight+2;
		$("#unplacedList").height(tempUnplacedListHeight+1);
		aniListHeight($("#unplacedListArea"), tempUnplacedListAreaHeight, unplacedPayStations);

		$("#placedList").height(tempPlacedListHeight+1);
		aniListHeight($("#placedListArea"), tempPlacedListAreaHeight, placedPayStations); }
	else { // Both Lists Are Larger than OGList Height
		$("#unplacedList").height(ogListHeight);
		aniListHeight($("#unplacedListArea"), ogListAreaHeight, unplacedPayStations);

		$("#placedList").height(ogListHeight);
		aniListHeight($("#placedListArea"), ogListAreaHeight, placedPayStations); }	
		
			
}

var payStation = new Array();

function createPlacementList(payStationList)
{
	var markerList = new Array();
	var payStationData = JSON.parse(payStationList.responseText);
	var psListItems = payStationData.payStationInformation.mapEntries;		
	var itemButtons =	'<a href="#" class="menuListButton edit" title="Edit Placement"><img width="18" height="1" title="Edit Placement" alt="Edit Placement" src="'+imgLocalDir+'spacer.gif" border="0"></a>'+
					'<a href="#" class="menuListButton save Ftr" title="Save"><img width="18" height="1" title="Save" alt="Save" src="'+imgLocalDir+'spacer.gif" border="0"></a>'+
					'<a href="#" class="menuListButton close" title="Remove from Map"><img width="18" height="1" title="Remove from Map" alt="Remove from Map" src="'+imgLocalDir+'spacer.gif" border="0"></a>'+
					'<a href="#" class="menuListButton cancel" title="Cancel"><img width="18" height="1" title="Cancel" alt="Cancel" src="'+imgLocalDir+'spacer.gif" border="0"></a>';
	payStation = new Array();
	if(psListItems){
		$("#map, #unplacedList, #placedList").show(); $("#NoPaystationPlacement").hide();
		if(psListItems instanceof Array === false){ psListItems = [psListItems]; }
		$("#unplacedListItems, #placedListItems").html('');
		for(x=0; x < psListItems.length; x++) {
			payStation["ps_"+psListItems[x].randomId] = new payStationDetails(psListItems[x].randomId, psListItems[x].name, psListItems[x].payStationType, psListItems[x].latitude, psListItems[x].longitude, psListItems[x].serialNumber, psListItems[x].locationName, psListItems[x].severity);
			if(psListItems[x].latitude == 0 && psListItems[x].longitude == 0) {
				$("#unplacedListItems").append("<li id='ups_"+psListItems[x].randomId+"' tooltipHTML='"+unplacedToolTip+"' psType='"+psListItems[x].payStationType+"' class='unplaced'>"+psListItems[x].name+itemButtons+"</li>");
				$("#placedListItems").append("<li id='pps_"+psListItems[x].randomId+"' tooltipHTML='"+placedToolTip+"' psType='"+psListItems[x].payStationType+"' class='unplaced'>"+psListItems[x].name+itemButtons+"</li>"); } 
			else {
				$("#unplacedListItems").append("<li id='ups_"+psListItems[x].randomId+"' tooltipHTML='"+unplacedToolTip+"' psType='"+psListItems[x].payStationType+"' class='placed'>"+psListItems[x].name+itemButtons+"</li>");
				$("#placedListItems").append("<li id='pps_"+psListItems[x].randomId+"' tooltipHTML='"+placedToolTip+"' psType='"+psListItems[x].payStationType+"' class='placed'>"+psListItems[x].name+itemButtons+"</li>");
				
				markerList.push(new mapMarker(
					psListItems[x].latitude, 
					psListItems[x].longitude, 
					psListItems[x].name,
					psListItems[x].payStationType, 
					psListItems[x].randomId,'','','','','','',
					psListItems[x].locationName,
					psListItems[x].serialNumber )); }}

	} else { $("#map, #unplacedList, #placedList").hide(); $("#NoPaystationPlacement").show(); soh('', ['NoPaystationPlacement']); }
	
	$("ul#unplacedList, ul#placedList").find("li").draggable({
		appendTo: "#PaystationPlacement",
		containment: false,
		cursor: "move",
		cursorAt: { left: 7, bottom: 0 },
		helper: function( event ) {
			return $( "<img src='"+imgLocalDir+"mapIcn_"+payStationIcn[payStation[$(this).attr('id').substring(1)].psType].img[0]+".png' border='0'>" ); },
		start: function(){
						if(($(".pin2map").length || $(".pined2map").length) && (!$(this).hasClass("pin2map") && !$(this).hasClass("pined2map"))){ 
							markerListDrag = false; alertDialog(activePlacementMsg); }
						else{ 
							$(this).removeClass("placed unplaced").addClass("pin2map"); markerListDrag = true; 
							curMarker = false; $("#mainContent").addClass("edit"); } },
		stop: function(){
						markerListDrag = false; $(this).removeClass("pin2map"); 
						if(!$(this).hasClass("pined2map")){ 
							if($(this).parent().attr("id") === "unplacedListItems"){ $(this).addClass("unplaced"); } 
							else { $(this).addClass("placed"); } 
						$("#mainContent").removeClass("edit");} },
		zIndex: 1021
	});
	ogListHeight = $("#unplacedListArea").height(); 
	balancePayStationLists();
	initMapPlacement("map", markerList);
}

function payStationPlacementList(locationObj, routeObj, payStationObj)
{
	var ajaxPayStationPlacement = GetHttpObject();
	var querryString = scrubQueryString(document.location.search.substring(1), ["locationID","routeID"]);// need to keep customerID
	
	if(locationObj || routeObj){
		var querryArray = querryString.split("&");
		for(x=0; x < querryArray.length; x++){ var spliceArray = false; var querryObj = querryArray[x].split("="); if(querryObj[0] == "locationID"){ querryArray.splice(x,1); } }
		querryString = querryArray.join("&");
		if(locationObj){querryString = querryString+"&locationID="+locationObj}
		if(routeObj){querryString = querryString+"&routeID="+routeObj}
	}

	ajaxPayStationPlacement.onreadystatechange = function()
	{
		if (ajaxPayStationPlacement.readyState==4 && ajaxPayStationPlacement.status == 200){
			if(ajaxPayStationPlacement.responseText != false){ 
				createPlacementList(ajaxPayStationPlacement); 
				if(payStationObj){ if($("#ups_"+payStationObj).is(":visible")){
					if($(".pin2map").length || $(".pined2map").length){ markerListDrag = false; alertDialog(activePlacementMsg); }
					else{ $("#mainContent").addClass("edit"); $("#ups_"+payStationObj).removeClass("placed unplaced pined2map").addClass("pin2map");}

					$("#unplacedList").scrollTop($("#ups_"+payStationObj).position().top); } 
					else { $("#pps_"+payStationObj).find(".edit").trigger('click'); $("#placedList").scrollTop($("#pps_"+payStationObj).position().top); } }
			}
			else{ alertDialog(systemErrorMsg); } //Unable to load details
		} else if(ajaxPayStationPlacement.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxPayStationPlacement.open("GET",LOC.listPayStation+"?"+querryString,true);
	ajaxPayStationPlacement.send();
}

function updatePayStationPlacement(psID, lat, long, altitude, action)
{
	var ajaxPinPayStation = GetHttpObject();
	ajaxPinPayStation.onreadystatechange = function()
	{
		if (ajaxPinPayStation.readyState==4 && ajaxPinPayStation.status == 200){
			if(ajaxPinPayStation.responseText == 'true'){
				if(action == "save"){
					$("#ups_"+psID+", #pps_"+psID).removeClass("unplaced pin2map pined2map").addClass("placed");
					balancePayStationLists();
				} else if(action == "remove"){
					$("#ups_"+psID+", #pps_"+psID).removeClass("placed pin2map pined2map").addClass("unplaced");
					balancePayStationLists();					
				}
				$("#pinedLat, #pinedLong").val('');
			} else {alertDialog(systemErrorMsg);}
		} else if(ajaxPinPayStation.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxPinPayStation.open("GET",LOC.pinPayStation+"?payStationID="+psID+"&latitude="+lat+"&longitude="+long+"&altitude="+altitude+"&"+document.location.search.substring(1),true);
	ajaxPinPayStation.send();
}

function payStationPlacement(locationObj, routeObj, payStationObj)
{	
	if(locationObj || routeObj) {
		var currentObj = (locationObj) ? locationObj : routeObj;
		for(x=0;x<locationRouteObj.length;x++) {
			if(locationRouteObj[x].value == currentObj){ $("#locationRouteAC").siblings("label").html(locationRouteObj[x].label) } }
		if(locationObj){ payStationPlacementList(locationObj,'',payStationObj); } else { payStationPlacementList('',routeObj,payStationObj); } }
	else{ payStationPlacementList('','',payStationObj); }
	
	$("ul#unplacedList, ul#placedList").on("mousedown", "li", function(event){ event.preventDefault(); })
	
//CANCEL NEW MAP POINT
	$("#unplacedList").on("click", ".cancel", function(event){
		event.preventDefault();
		
		if($(".pined2map").length){
			var 	removePinButtons = {};
			removePinButtons.btn1 = {
				text : removeMsg,
				click : 	function() { 
					$("#mainContent").removeClass("edit");
					var $that = $("ul").find("li.pined2map");
					$that.removeClass("placed pin2map pined2map").addClass("unplaced");
					if(curMarker === false){
					curPayStation = payStation[$that.attr("id").substring(1)];
					markers.eachLayer( function(marker){ if(marker.options.alt == curPayStation.psSN){ markers.removeLayer(marker); }});}
					else {markers.removeLayer(curMarker);}
					curMarker = false;  
					$("#pinedLat, #pinedLong").val(''); $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
			removePinButtons.btn2 = {
				text : cancelBtn,
				click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
			};
			
			if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
			$("#messageResponseAlertBox")
			.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmRemoveFromMapMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: removePinButtons });
			btnStatus("Remove");
		} else {
			$("ul").find("li.pin2map").removeClass("placed pin2map pined2map").addClass("unplaced");
		}
	});
//SAVE NEW MAP POINT
	$("#unplacedList").on("click", ".save", function(event){
		event.preventDefault();
		$("#mainContent").removeClass("edit");
		var psID = $(this).parents("li").attr("id").replace("ups_", "");
		var lat = $("#pinedLat").val();
		var long = $("#pinedLong").val();
		var altitude = $("#pinedAltitude").val();
		
		//Add new Placed PayStation to LayerGroup.
		curMarker.options.alt = curMarker.options.title;
		markers.addLayer(curMarker);

		updatePayStationPlacement(psID, lat, long, altitude, "save");		
		curMarker.dragging.disable();
		curMarker = false;
	});
//EDIT MAP POINT
	$("#placedList").on("click", ".edit", function (event) {
		event.preventDefault();
		if ($(".pin2map").length || $(".pined2map").length) { 
			markerListDrag = false; alertDialog(activePlacementMsg);
		 } else { 
			$("#mainContent").addClass("edit");
			$(this).parents("li.placed").removeClass("placed unplaced pined2map").addClass("pin2map"); 
		}
		curPayStation = payStation[$(".pin2map").attr("id").substring(1)];
		markers.eachLayer(function (marker) { 
			if (marker.options.alt == curPayStation.psSN) {
					curMarker = marker; 
			} 
		});
		curMarker.setZIndexOffset(1200);
		curMarker.openPopup();
		curMarker.on('dragstart', function (event) { this.closePopup(); markerListDrag = true; }).on('dragend', function (event) { markerListDrag = false; });
		curMarker.dragging.enable();
	});
//CANCEL EDIT MAP POINT
	$("#placedList").on("click", ".cancel", function(event){
		event.preventDefault();
		$("#mainContent").removeClass("edit");
		if($(".pined2map").length){
			var $that = $("ul").find("li.pined2map");
			if(curMarker === false){
			curPayStation = payStation[$that.attr("id").substring(1)];
			markers.eachLayer( function(marker){ if(marker.options.alt == curPayStation.psSN){ 
				marker.closePopup();
				marker.dragging.disable();
				marker.setLatLng([payStation[$(".pined2map").attr("id").substring(1)].lat, payStation[$(".pined2map").attr("id").substring(1)].lng]); }});}
			else {
				curMarker.closePopup();
				curMarker.dragging.disable();
				curMarker.setLatLng([payStation[$(".pined2map").attr("id").substring(1)].lat, payStation[$(".pined2map").attr("id").substring(1)].lng]);} 
			$that.removeClass("unplaced pin2map pined2map").addClass("placed");
			$("#pinedLat, #pinedLong").val('');
			curMarker = false;
		} else {
			if(curMarker){ curMarker.closePopup(); curMarker.dragging.disable(); }
			$("ul").find("li.pin2map").removeClass("unplaced pin2map pined2map").addClass("placed");
			curMarker = false;
		}
	});
//UPDATE EDIT MAP POINT
	$("#placedList").on("click", ".save", function(event){
		event.preventDefault();
		$("#mainContent").removeClass("edit");
		var psID = $(this).parents("li.pined2map").attr("id").replace("pps_", "");
		var lat = $("#pinedLat").val();
		var long = $("#pinedLong").val();
		var altitude = $("#pinedAltitude").val();
		updatePayStationPlacement(psID, lat, long, altitude, "save");
		curMarker.dragging.disable();
		curMarker = false;
	});
//DELETE MAP POINT
	$("#placedList").on("click", ".close", function(event){
		event.preventDefault();
		var $that = "";
		if($(this).parents("li").hasClass("pin2map")){ $that = $(this).parents("li.pin2map"); }
		else{ $that = $(this).parents("li.pined2map"); }
		var psID = $that.attr("id").replace("pps_", "");
		var lat = '';
		var long = '';
		var altitude = '';
		var 	deleteMapPinButtons = {};
		deleteMapPinButtons.btn1 = {
			text : removeMsg,
			click : 	function() { 
				$("#mainContent").removeClass("edit"); 
				if(curMarker === false){
				curPayStation = payStation[$that.attr("id").substring(1)];
				markers.eachLayer( function(marker){ if(marker.options.alt == curPayStation.psSN){ markers.removeLayer(marker); }});}
				else {markers.removeLayer(curMarker);}
				curMarker = false; 
				updatePayStationPlacement(psID, lat, long, altitude, "remove"); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deleteMapPinButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+confirmRemoveFromMapMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteMapPinButtons });
		btnStatus(removeMsg);
	});
}
