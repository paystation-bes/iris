(function($, undefined) {
	var rangesHash = {};
	
	$.widget("ui.automonths", $.ui.autoselector, {
		"options": {
			"displayFormat": "MM",
			"minLimit": 0,
			"maxLimit": 11,
			"lowerBoundComponent": null,
			"upperBoundComponent": null,
			"defaultValue": "",
			"isComboBox": true,
			"invalidSelectionMessage": function() {
				return (commonMessages && commonMessages.invalidMonthSelection) ? commonMessages.invalidMonthSelection : "Invalid Month !";
			},
			"data": function() {
				var i = this.options.minLimit;
				
				var boundCmp = this.options.lowerBoundComponent;
				if(boundCmp) {
					if(typeof boundCmp === "string") {
						boundCmp = $("#" + boundCmp);
					}
					
					var lowerBoundVal = boundCmp.automonths("getMonth");
					if(lowerBoundVal) {
						i = Math.max(i, lowerBoundVal);
					}
				}
				
				var len = this.options.maxLimit;
				boundCmp = this.options.upperBoundComponent;
				if(boundCmp) {
					if(typeof boundCmp === "string") {
						boundCmp = $("#" + boundCmp);
					}
					
					var upperBoundVal = boundCmp.automonths("getMonth");
					if(upperBoundVal) {
						len = Math.min(len, upperBoundVal);
					}
				}
				
				var rangeId = this.options.displayFormat + "[" + i + "-" + len + "]";
				var result = rangesHash[rangeId];
				if(!result) {
					result = [];
					while(i <= len) {
						result.push({
							"label": this.formatMonth(i),
							"value": i + ""
						});
						
						++i;
					}
				}
				
				return result;
			}
		},
		"parse": function(label) {
			var result = null;
			if(!isNaN(label)) {
				result = parseInt(label, 10) - 1;
			}
			else {
				result = this.parseMonth(label);
			}
			
			return result;
		},
		"format": function(value) {
			return this.formatMonth(value);
		},
		"getMonth": function() {
			return this.elementValContainer.val();
		},
		"setMonth": function(monthIdx) {
			if(typeof monthIdx === "string") {
				monthIdx = parseInt(monthIdx, 10);
				if(isNaN(monthIdx)) {
					monthIdx = -1;
				}
			}
			
			if((monthIdx > -1) && (monthIdx < 12)) {
				this.element.val(this.formatMonth(monthIdx));
				this.elementValContainer.val(monthIdx);
			}
			else {
				this.element.val("");
				this.elementValContainer.val("");
			}
		},
		"formatMonth": function(monthIdx) {
			var currDate = new Date();
			currDate.setDate(1);
			currDate.setMonth(monthIdx);
			
			return $.datepicker.formatDate(this.options.displayFormat, currDate);
		},
		"parseMonth": function(monthStr) {
			var result = null;
			if(monthStr) {
				var rawDate = null;
				try {
					rawDate = $.datepicker.parseDate(this.options.displayFormat + " yy-d", monthStr + " 2000-1");
					result = rawDate.getMonth();
				}
				catch(error) {
					// DO NOTHING
				}
			}
			
			return result;
		}
	});
}(jQuery));