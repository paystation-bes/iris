var noContactsHTML = '';

function showAlertDetails(alertDetails, action)
{
	//Process JSON
	var alertData =	JSON.parse(alertDetails.responseText);
	//console.log(alertData)
	$("#actionFlag").val("1");
	$("#contentID, #formAlertID").val(alertData.definedAlertDetails.randomAlertId);

	$("#detailAlertName, #alertName").html(alertData.definedAlertDetails.name);
	$("#formAlertName").val(alertData.definedAlertDetails.name);
	if(alertData.definedAlertDetails.status == true){
		$("#detailAlertStatus").html(alertStatusEnabledMsg);
		$("#statusCheck").addClass("checked"); $("#statusCheckHidden").val("true");
	} else{
		$("#detailAlertStatus").html(alertStatusDisabledMsg);
		$("#statusCheck").removeClass("checked"); $("#statusCheckHidden").val("false");
	}
	$("#detailAlertType").html(alertData.definedAlertDetails.alertType);

	var tempAlertTypeId = alertData.definedAlertDetails.alertTypeId;
	var thresoldExceedDisplayId = alertData.definedAlertDetails.alertThresholdExceedDisplay;
	var exceedTitle = (alertData.definedAlertDetails.alertThresholdExceed == 1)?1:0;

	// clear delayed settings by default
    clearDelayedSettings();

	if(tempAlertTypeId == 1){
		$("#formDisplayExceedArea, #exceedsTitle, #detailDisplayExceedArea, #formExceedHour, #detailExceedHour").show();
		$("#formDisplayOccursMsg, #exceedTitle, #detailDisplayOccursMsg, #formExceedDollars, #formExceedDays, #detailExceedDollars, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #formDisplayExceedTypeArea, #detailExceedDay").hide();
		$("#detailExceedHour").html(hour[exceedTitle]);
		$("#detailAlertThresholdExceeds").html(alertData.definedAlertDetails.alertThresholdExceed);
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("reset");
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);}
	else if(tempAlertTypeId == 2){
		$("#formDisplayExceedArea, #exceedsTitle, #detailDisplayExceedArea, #formExceedDollars, #detailExceedDollars").show();
		$("#formDisplayOccursMsg, #exceedTitle, #detailDisplayOccursMsg, #formExceedHour, #detailExceedHour, #formExceedDays, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #formDisplayExceedTypeArea, #detailExceedDay").hide();
		$("#detailAlertThresholdExceeds").html(Highcharts.numberFormat(alertData.definedAlertDetails.alertThresholdExceed,2));
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("reset");
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);}
	else if(tempAlertTypeId == 3){
		$("#formDisplayExceedArea, #exceedsTitle, #detailDisplayExceedArea, #formDisplayExceedTypeArea").show();
		$("#formDisplayOccursMsg, #exceedTitle, #detailDisplayOccursMsg, #formExceedHour, #formExceedDollars, #formExceedDays, #detailExceedHour, #detailExceedDollars, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #detailExceedDay").hide();
		if(thresoldExceedDisplayId == 1){
			$("#detailExceedCoin").html(coin[exceedTitle]).show();
			$("#detailAlertThresholdExceeds").html(alertData.definedAlertDetails.alertThresholdExceed);}
		else if(
			thresoldExceedDisplayId == 2){ $("#detailExceedDollars").show();
			$("#detailAlertThresholdExceeds").html(Highcharts.numberFormat(alertData.definedAlertDetails.alertThresholdExceed,2)); }
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("setSelectedValue", thresoldExceedDisplayId);
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);}
	else if(tempAlertTypeId == 4){
		$("#formDisplayExceedArea, #exceedsTitle, #detailDisplayExceedArea, #formDisplayExceedTypeArea").show();
		$("#formDisplayOccursMsg, #exceedTitle, #detailDisplayOccursMsg, #formExceedHour, #formExceedDollars, #formExceedDays, #detailExceedHour, #detailExceedDollars, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #detailExceedDay").hide();
		if(thresoldExceedDisplayId == 1){
			$("#detailExceedBill").html(bill[exceedTitle]).show();
			$("#detailAlertThresholdExceeds").html(alertData.definedAlertDetails.alertThresholdExceed);}
		else if(thresoldExceedDisplayId == 2){
			$("#detailExceedDollars").show();
			$("#detailAlertThresholdExceeds").html(Highcharts.numberFormat(alertData.definedAlertDetails.alertThresholdExceed, 2));}
		$("#formAlertTypeLabel").hide();
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("setSelectedValue", thresoldExceedDisplayId);
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);}
	else if(tempAlertTypeId == 5){
		$("#formDisplayExceedArea, #exceedTitle, #detailDisplayExceedArea, #formDisplayExceedTypeArea").show();
		$("#formDisplayOccursMsg, #exceedsTitle, #detailDisplayOccursMsg, #formExceedHour, #formExceedDollars, #formExceedDays, #detailExceedHour, #detailExceedDollars, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #detailExceedDay").hide();
		if(thresoldExceedDisplayId == 1){
			$("#detailExceedCard").html(count[exceedTitle]).show();
			$("#detailAlertThresholdExceeds").html(alertData.definedAlertDetails.alertThresholdExceed); }
		else if(thresoldExceedDisplayId == 2){
			$("#detailExceedDollars").show();
			$("#detailAlertThresholdExceeds").html(Highcharts.numberFormat(alertData.definedAlertDetails.alertThresholdExceed, 2));}
		$("#formAlertTypeLabel").hide();
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("setSelectedValue", thresoldExceedDisplayId);
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);}
	else if(tempAlertTypeId == 6){
		$("#formDisplayOccursMsg, #exceedsTitle, #detailDisplayOccursMsg").show();
		$("#formDisplayExceedArea, #exceedTitle, #detailDisplayExceedArea, #formExceedDays, #detailExceedDay").hide();
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("reset");
		$("#formDisplayExceedAmount").val("");
		$("#formAlertDelayed").show();
		if(alertData.definedAlertDetails.isDelayed === true){
			$("#isDelayedCheck").addClass("checked"); $("#isDelayedCheckHidden").val("true");
			$("#delayedByMinutesInput").show();
			$("#delayedByMinutes").val(alertData.definedAlertDetails.delayedByMinutes);
			$("#detailAlertDelayedByLabel").show();
			if(alertData.definedAlertDetails.delayedByMinutes > 0){
				$("#detailAlertDelayedBy").show();$("#detailAlertDelayedBy").html(alertData.definedAlertDetails.delayedByMinutes + " Minutes");
			} else {
				$("#detailAlertDelayedByLabel").hide();
				$("#detailAlertDelayedBy").hide();
			}
		}
		}
	else if(tempAlertTypeId == 7){
		$("#formDisplayExceedArea, #exceedsTitle, #detailDisplayExceedArea, #formExceedDays, #detailExceedDay").show();
		$("#formDisplayOccursMsg, #exceedTitle, #detailDisplayOccursMsg, #formExceedDollars, #detailExceedDollars, #detailExceedCoin, #detailExceedBill, #detailExceedCard, #formDisplayExceedTypeArea, #formExceedHour, #detailExceedHour").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("reset");
		$("#formDisplayExceedAmount").val(alertData.definedAlertDetails.alertThresholdExceed);
		$("#detailAlertThresholdExceeds").html(alertData.definedAlertDetails.alertThresholdExceed);
		$("#detailExceedDay").html(day[exceedTitle]);}
	else{
		$("#formDisplayExceedArea, #detailDisplayExceedArea, #formDisplayOccursMsg, #detailDisplayOccursMsg").hide();
		$("#formAlertTypeLabel").hide();
		$("#formAlertType").autoselector("setSelectedValue", tempAlertTypeId, true);
		$("#formDisplayExceedType").autoselector("reset");
		$("#formDisplayExceedAmount").val("");}

	$("#formAlertTypeInput").hide();
	$("#formAlertTypeDisplay").show().text($("#formAlertType").autoselector("getSelectedLabel"));

	$("#detailAlertRoute").html(alertData.definedAlertDetails.route);

	var routeId = (alertData.definedAlertDetails.randomRouteId)? alertData.definedAlertDetails.randomRouteId : 0;
	$("#formAlertRoute").autoselector("setSelectedValue", routeId, true);

	$("#formDisplayAlertNotifyList").html(noContactsHTML);
	$("#detailNotificationList").html("");
	$("#formAlertNotifyList").val("");

	if(alertData.definedAlertDetails.emailList) {
		$("#noContact").hide();
		var emailList = alertData.definedAlertDetails.emailList;
		if(emailList instanceof Array === false){ emailList = [emailList]; }
		for(var x = 0; x < emailList.length; x++) {
			if($("#formAlertNotifyList").val() == ""){ $("#formAlertNotifyList").val(emailList[x]); }else{ $("#formAlertNotifyList").val($("#formAlertNotifyList").val()+","+emailList[x]); }
			$("#detailNotificationList").append(emailList[x]+"<br>");
			$("#formDisplayAlertNotifyList").append("<li class='selected' title='"+clickToRemove+"'><a href='#' class='checkBox'>&nbsp;</a> <span class='contactEmail'>"+emailList[x]+"</span></li>"); } }
	else { $("#formDisplayAlertNotifyList").html(noContactsHTML); $("#noContact").show(); }
	clearErrors();

	if(action == "edit"){ testFormEdit($("#alertSettings"));
		$("#alertDetails").removeClass("addForm").addClass("form editForm")
		snglSlideFadeTransition("show", $("#alertDetails"), function(){ $("#formAlertName").trigger('focus'); }); }
	else {
		$("#mainContent").removeClass("edit"); $("#alertDetails").removeClass("form editForm addForm");
		snglSlideFadeTransition("show", $("#alertDetails")); }

	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------

function resetAlertForm()
{
	var hideCallBack = function(){
		$("#detailAlertName, #alertName").html("");
		$("#detailAlertStatus").html("");
		$("#statusCheck").addClass("checked");
		$("#detailRouteType").html("");
		$("#formAlertType").autoselector("reset");
		$("#formAlertTypeInput").show();
		$("#formAlertTypeDisplay").hide().text("");
		$("#formDisplayExceedArea, #formDisplayOccursMsg, #formExceedHour, #formExceedDollars, #formDisplayExceedTypeArea").hide();
		$("#formDisplayExceedType").autoselector("reset"); $("#formDisplayExceedAmount").val("");
		$("#detailAlertThreshold").html("");
		$("#detailAlertThresholdExceeds").html("");
		$("#detailAlertRoute").html("");
		$("#formAlertRoute").autoselector("reset");
		$("#detailNotificationList").html("");
		$("#formDisplayAlertNotifyList").html(noContactsHTML);
		$("#noContact").show();
		document.getElementById("alertSettings").reset(); clearErrors();
		$("#actionFlag").val("0");
		$("#contentID, #formAlertID").val("");
		$("#statusCheckHidden").val("true");
		$("#formAlertNotifyList").val("");
		$("#formAlertDelayed").hide();
		clearDelayedSettings();
		$(document.body).scrollTop(0);
		testFormEdit($("#alertSettings"));
		$("#alertDetails").removeClass("editForm").addClass("form addForm");
		snglSlideFadeTransition("show", $("#alertDetails"), function(){ $("#formAlertName").trigger('focus'); });
	}

	if($("#alertDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#alertDetails"), hideCallBack); }
	else{ hideCallBack(); } }

//--------------------------------------------------------------------------------------

function loadAlert(alertID, action)
{
	var hideCallBack = '';
	if($("#contentID").val() === alertID){
		if(action == "edit"){
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#alertDetails"), $("#alertDetails"), hideCallBack); }
		else if(action == "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#alertDetails"), $("#alertDetails"), hideCallBack); } }
	else {
		var $this = $("#alert_"+alertID);
		if($("#alertList").find("li.selected").length){ $("#alertList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");

		hideCallBack = function(){
			var ajaxLoadAlert = GetHttpObject();
			ajaxLoadAlert.onreadystatechange = function()
			{
				if (ajaxLoadAlert.readyState==4 && ajaxLoadAlert.status == 200){
					showAlertDetails(ajaxLoadAlert, action);
				} else if(ajaxLoadAlert.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadAlert.open("GET",LOC.viewAlerts+"?alertID="+alertID+"&"+document.location.search.substring(1),true);
			ajaxLoadAlert.send(); }}
	if($("#alertDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#alertDetails"), hideCallBack); }
	else{ hideCallBack(); } }

function deleteAlert(alertID) {
	var ajaxDeleteAlert = GetHttpObject();
	ajaxDeleteAlert.onreadystatechange = function()
	{
		if (ajaxDeleteAlert.readyState==4 && ajaxDeleteAlert.status == 200){
			if(ajaxDeleteAlert.responseText == "true") {
				noteDialog(alertDeleteMsg);
				var newLocation = location.pathname;
				setTimeout(function(){window.location = newLocation}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteAlert.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteAlert.open("GET",LOC.deleteAlerts+"?alertID="+alertID+"&"+document.location.search.substring(1),true);
	ajaxDeleteAlert.send();
}

function verifyDeleteAlert(alertID)
{
	var 	deleteAlertButtons = {};
		deleteAlertButtons.btn1 = {
			text : deleteBtn,
			click : 	function() { deleteAlert(alertID); $(this).scrollTop(0); $(this).dialog( "close" ); },
		};
		deleteAlertButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteAlertMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteAlertButtons });
	btnStatus(deleteBtn);
}

//--------------------------------------------------------------------------------------

function filterAlertList(filterVal)
{
	for(x = 0; x <= 16; x++){$("#alertList").removeClass("AT"+x);}
	if(filterVal != "AT0"){ $("#alertList").addClass(filterVal); }
//console.log($("#alertList").attr("class"))
}
// Clear and hide everything related to delayed alerts
function clearDelayedSettings() {
    $("#detailAlertDelayedByLabel").hide();
    $("#formAlertDelayed").hide();
    $("#delayedByMinutesInput").hide();
    $("#detailAlertDelayedBy").hide();
    $("#detailAlertDelayedByLabel").hide();
    $("#isDelayedCheck").removeClass("checked");
    $("#isDelayedCheckHidden").val("false");
    $("#delayedByMinutes").val("");
}

//--------------------------------------------------------------------------------------

/*function submitNotificationContact(inputObj)
{
	var newContact = inputObj.val();
	
	if(/,/.test(newContact)) {
		newContact = newContact.split(",");
	}
	
	if(newContact instanceof Array === false) {
		newContact = [newContact];
	}
	
	var $contactsPanel = $("#formDisplayAlertNotifyList");
	for(x = 0; x < newContact.length; x++) {
		if(characterTypes.emailString.test(newContact[x])) {
			var regXContact = new RegExp(newContact[x]);
			var existingContacts = $("#formAlertNotifyList").val();
			if(!regXContact.test(existingContacts)) { 
				if(existingContacts == "") {
					$("#formAlertNotifyList").val(newContact[x]);
				}
				else {
					$("#formAlertNotifyList").val(existingContacts + "," + newContact[x]);
				}
				if($("#formDisplayAlertNotifyList").find("#noContact").is(":visible")) {
					$("#noContact").hide();
				}
				
				var addedContactLIs = $contactsPanel.find("li:visible");
				var idx = addedContactLIs.length, drawn = false;
				
				var $currLI, emailAddr;
				var $newContactLI = $("<li class='selected' title='"+clickToRemove+"'><a href='#' class='checkBox'>&nbsp;</a> <span class='contactEmail'>"+newContact[x]+"</span></li>");
				while((!drawn) && (--idx >= 0)) {
					$currLI = $(addedContactLIs[idx]);
					emailAddr = $currLI.find(".contactEmail").text();
					if(newContact[x] > emailAddr) {
						$newContactLI.insertAfter($currLI);
						drawn = true;
					}
				}
				
				if(!drawn) {
					$contactsPanel.prepend($newContactLI);
				}
			}
		}
	}
	inputObj.siblings("input").val("");
}
*/
//--------------------------------------------------------------------------------------

function hideAlert($this){
	snglSlideFadeTransition("hide", $("#alertDetails"));
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#alertList").find("li.selected").length){ $("#alertList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function alertSelectionCheck(alertID, eventAction)
{
	var itemID = alertID.split("_");
	var tempID = itemID[1],
		$this = $("#alert_"+tempID);
	if(eventAction === "hide"){
		var	cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
		crntPageAction = "";
		history.pushState(null, null, location.pathname+"?"+cleanQuerryString);
		hideAlert($this); }
	else {
		if($("#contentID").val() != itemID[1] || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: tempID, action: eventAction };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+tempID+"&pgActn="+stateObj.action);
			loadAlert(tempID, stateObj.action); }}
}

//--------------------------------------------------------------------------------------

function newAlertForm(){ $(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); }); resetAlertForm(); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popAlertAction(tempItemID, tempEvent, stateObj){
	if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#user_"+tempItemID) : ""; hideAlert($this); }
	else if(tempEvent === "Add"){ newAlertForm(); }
	else { loadAlert(tempItemID, tempEvent); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function alerts(alertID, pageEvent) {
/////////////////////////////////////////////////// AUTOSELECTORY SETUP
	$("#formAlertType").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"data": JSON.parse($("#formAlertTypeData").text()) })

	$("#formDisplayExceedType").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"shouldCategorize": true,
		"data": JSON.parse($("#formDisplayExceedTypeData").text()) })
		
//Route Type AUTO COMPLETE STANDARD 
	$("#formAlertRoute").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"data": routeObj });

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popAlertAction(tempItemID, tempEvent, stateObj); }

if(!popStateEvent && (alertID !== undefined && alertID !== '')){ popAlertAction(alertID, pageEvent); }
///////////////////////////////////////////////////	

	msgResolver.mapAttribute("alertName", "#formAlertName");
	msgResolver.mapAttribute("alertType", "#formAlertType");
	msgResolver.mapAttribute("alertThreshold", "#formAlertType");
	msgResolver.mapAttribute("alertThresholdExceed", "#formDisplayExceedAmount");
	msgResolver.mapAttribute("alertThresholdType", "#formDisplayExceedType");
	msgResolver.mapAttribute("alertRoute", "#formAlertRoute");
	msgResolver.mapAttribute("alertContacts", "#newNotificationEmail");

	noContactsHTML = '<li id="noContact" class="alignCenter">'+ noContactsMsg +'</li>';

	$(document.body).on("click", "#alertList > li", function(){
			var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
			var itemID = $(this).attr("id");
			if(pausePropagation === true){
				pausePropagationLoop = setInterval(function(){
					if(pausePropagation === false){ alertSelectionCheck(itemID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
					else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
			else{ alertSelectionCheck(itemID, actionEvent); } });

	$("#alertListArea").on("click", "#alertList * .view", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		alertSelectionCheck(itemID, "display"); });

	$("#alertListArea").on("click", "#alertList * .edit", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		alertSelectionCheck(itemID, "edit"); });

	$("#alertListArea").on("click", "#alertList * .delete", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id").split("_");
		verifyDeleteAlert(itemID[1]);
	});

	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var alertID = $("#contentID").val();
		verifyDeleteAlert(alertID); });

	$("#btnAddAlert").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{
			var stateObj = { itemObject: "New", action: "Add" };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); })
			resetAlertForm(); } });

	$("#alertListArea").on("click", "#alertListHeader * .sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortAlertList = GetHttpObject();
		ajaxSortAlertList.onreadystatechange = function()
		{
			if (ajaxSortAlertList.readyState==4 && ajaxSortAlertList.status == 200){
				$("#alertListArea").html(ajaxSortAlertList.responseText);
				if($("#aTypeAC").autoselector("getSelectedValue") !== "AT0"){ $("#alertList").addClass($("#aTypeAC").autoselector("getSelectedValue")); }
				scrollListWidth(["alertList"]);
			} else if(ajaxSortAlertList.readyState==4){
				if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
				$('#messageResponseAlertBox').dialog(alertNoticeOptions).html(sortFail);
			}
		};
		ajaxSortAlertList.open("GET",LOC.sortAlerts+"?sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortAlertList.send();
	});

	var $formExceedAmnt = $("#formDisplayExceedAmount");

    $("#formAlertType").on("itemSelected", function(event, ui) {
		var slctValue = $("#formAlertType").autoselector("getSelectedValue")

		// clear delayed settings by default
        clearDelayedSettings();

        if(slctValue == 1){
			$("#formDisplayExceedArea, #exceedsTitle, #formExceedHour").show();
			$("#formDisplayOccursMsg, #exceedTitle, #formExceedDollars, #formDisplayExceedTypeArea, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "3").removeClass("nmbrChrctr").addClass("intNmbrChrctr");}
		else if(slctValue == 2){
			$("#formDisplayExceedArea, #exceedsTitle, #formExceedDollars").show();
			$("#formDisplayOccursMsg, #exceedTitle, #formExceedHour, #formDisplayExceedTypeArea, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr");}
		else if(slctValue == 3){
			$("#formDisplayExceedArea, #exceedsTitle, #formDisplayExceedTypeArea").show();
			$("#formDisplayOccursMsg, #exceedTitle, #formExceedHour, #formExceedDollars, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr");}
		else if(slctValue == 4){
			$("#formDisplayExceedArea, #exceedsTitle, #formDisplayExceedTypeArea").show();
			$("#formDisplayOccursMsg, #exceedTitle, #formExceedHour, #formExceedDollars, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr");}
		else if(slctValue == 5){
			$("#formDisplayExceedArea, #exceedTitle, #formDisplayExceedTypeArea").show();
			$("#formDisplayOccursMsg, #exceedsTitle, #formExceedHour, #formExceedDollars, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr");}
		else if(slctValue == 6){
			$("#formDisplayOccursMsg").show();
			$("#formDisplayExceedArea, #exceedsTitle, #exceedTitle, #formExceedHour, #formExceedDollars, #formDisplayExceedTypeArea, #formExceedDays").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("");
			$("#formAlertDelayed").show();
			$("#isDelayedCheck").removeClass("checked"); $("#isDelayedCheckHidden").val("false");
			$("#delayedByMinutesInput").hide();
			$("#delayedByMinutes").val("");
			}
		else if(slctValue == 7){
			$("#formDisplayExceedArea, #exceedsTitle, #formExceedDays").show();
			$("#formDisplayOccursMsg, #exceedTitle, #formExceedHour, #formExceedDollars, #formDisplayExceedTypeArea").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("").attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr");}
		else{
			$("#formDisplayExceedArea, #formDisplayOccursMsg, #formExceedHour, #formExceedDollars, #formDisplayExceedTypeArea").hide();
			$("#formDisplayExceedType").autoselector("reset");
			$("#formDisplayExceedType").siblings("label.hiddenLabel").removeClass("active").show(); $formExceedAmnt.val("");}});


	$("#formDisplayExceedType").on("itemSelected", function(event, selectedItem) {
		if(selectedItem.value == "1") { $formExceedAmnt.attr("maxlength", "4").removeClass("nmbrChrctr").addClass("intNmbrChrctr"); }
		else { $formExceedAmnt.attr("maxlength", "7").removeClass("intNmbrChrctr").addClass("nmbrChrctr"); }
	});

	$("#newNotificationEmail").on('keypress', function(e) { if(e.keyCode == 13 || e.keyCode == 9){ submitNotificationContact($(this)); $(this).trigger('focus'); } });

	$("#alertEmailAddBtn").on('click', function(event){ event.preventDefault();
		submitNotificationContact($("#newNotificationEmail")); });

	$("#notificationTest").on('click', function(event){
		event.preventDefault();
		var alertContacts = $("#formAlertNotifyList").val();
		var alertName = $("#formAlertName").val();
		var ajaxNotifyTest = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxNotifyTest.onreadystatechange = function()
		{
			if (ajaxNotifyTest.readyState==4 && ajaxNotifyTest.status == 200){
				if(ajaxNotifyTest.responseText == "true"){noteDialog(notificationSuccesful, "timeOut", 2500);}
				else{alertDialog(notificationUnsuccesful);}
			} else if(ajaxNotifyTest.readyState==4){
				alertDialog(notificationUnsuccesful);
			}
		};
		ajaxNotifyTest.open("GET",LOC.testAlertsNotification+"?alertContacts="+alertContacts+"&alertName="+alertName+"&"+document.location.search.substring(1),true);
		ajaxNotifyTest.send();
	});



	$("#isDelayedCheckHidden").on("change", function () {
        var isDelayed = $("#isDelayedCheckHidden").val();
        var delayedByMinutesInput = $("#delayedByMinutesInput");
        if (isDelayed === "true") {
            delayedByMinutesInput.show();
        } else {
            delayedByMinutesInput.hide();
            $("#delayedByMinutes").val(0);
        }
    });

	$("#alertSettings").on("click", ".save", function(event)
	{
		event.preventDefault();
			var alertID = $("#alertSettings").find("#formAlertID").val();
			var params = $("#alertSettings").serialize();
			var ajaxSaveAlert = GetHttpObject({
				"triggerSelector": (event) ? event.target : null });
			ajaxSaveAlert.onreadystatechange = function(){
				if (ajaxSaveAlert.readyState == 4 && ajaxSaveAlert.status == 200){
					var response = ajaxSaveAlert.responseText.split(":");
					if(response[0] == "true"){
						noteDialog(alertSavedMsg);
						var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "itemValue", "pgActn"]);
						filterQuerry = ($("#contentID").val() != "")? filterQuerry+"&itemValue="+$("#contentID").val() : filterQuerry;
						crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
						var newLocation =   location.pathname+"?"+cleanQuerryString+filterQuerry+"&itemValue="+$("#contentID").val()+"&pgActn="+crntPageAction;
						setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
				else if(ajaxSaveAlert.readyState==4){ alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
			ajaxSaveAlert.open("POST",LOC.saveAlerts,true);
			ajaxSaveAlert.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			ajaxSaveAlert.setRequestHeader("Content-length", params.length);
			ajaxSaveAlert.setRequestHeader("Connection", "close");
			ajaxSaveAlert.send(params);
//			console.log(params)

	});

	$("#alertSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });

	//Alert Type AUTO COMPLETE STANDARD
	$("#aTypeAC").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"data": aTypeObj })
	.on("itemSelected", function(event, ui) {
		var slctValue = $("#aTypeAC").autoselector("getSelectedValue");
		if(slctValue == ""){ slctValue = "AT0"; }
		if($(".sectionContent").attr("id") == "DefinedAlerts" && slctValue != ""){ filterAlertList(slctValue); }
		if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue }
		$(this).trigger('blur');
		return false; })
	.on("change", function(){
		if($("#aTypeAC").val() === ""){ filterAlertList("AT0"); }});
}
