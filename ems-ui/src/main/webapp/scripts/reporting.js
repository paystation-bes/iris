if((typeof reportLabel === "undefined") || (reportLabel === null)) {
	reportLabel = [];
}

function reporting() {
	/* Commented to disable js sort
	makeSortableList($reportsList, {
		"col2": { "dataSelector": "span[class=timestamp]", "invertOrderIdenticator": true }
	});
	
	makeSortableList($reportDefinitionsList, {
		"col2": { "dataSelector": "span[class=timestamp]", "invertOrderIdenticator": true }
	});
	*/	
	attachReportsMenuHandler();
	attachReportDefinitionsMenuHandler();
	
	var $reportsPanel = $("#reportsPanel");
	$reportsPanel
		.on("click", "div.sort", function(event) {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if(request.status !== 200) {
						alertDialog(sortFail);
					}
					else {
						renderReportsList(request.responseText);
					}
				}
			};
			var sortOptions = $(this).attr("sort").split(":");
			request.open("GET",LOC.sortReportsList +"?sort=" + sortOptions[0] + "&dir=" + sortOptions[1]+"&"+document.location.search.substring(1), true);
			request.send();
		})
		.on("click", "a.reload", function(event) {
			var request = GetHttpObject();
			request.onreadystatechange = function(responseFalse) {
				if(request.readyState === 4) {
					if(!responseFalse) {
						renderReportsList(request.responseText);
					}
				}
			};
			
			var sortOptions = $reportsPanel.find(".current.sort").attr("sort").split(":");
			var sortDir = (sortOptions[1].toLowerCase() == "desc") ? "asc" : "desc";
			request.open("GET",LOC.sortReportsList +"?sort=" + sortOptions[0] + "&dir=" + sortDir + "&" + document.location.search.substring(1), true);
			request.send();
		});
	
	$("#reportDefinitionsPanel")
		.on("click", "div.sort", function(event) {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if(request.status !== 200) {
						alertDialog(sortFail);
					}
					else {
						renderReportDefinitionsList(request.responseText);
					}
				}
			};
			
			var sortOptions = $(this).attr("sort").split(":");
			request.open("GET",LOC.sortReportDefinitionsList +"?sort=" + sortOptions[0] + "&dir=" + sortOptions[1]+"&"+document.location.search.substring(1), true);
			request.send();
		}); 
	
	scrollListWidth(["queuedReportsList"]);
	scrollListWidth(["reportsList"]);
	scrollListWidth(["reportDefinitionsList"]);
	soh([ "reportDefinitionsList" ], null, null, [ "queuedReportsList", "reportsList" ]);
}

function attachReportsMenuHandler() {
	var $queuedReportsList = $("#queuedReportsList");
	var $reportsList = $("#reportsList");
	$queuedReportsList
		.on("click", "a.rerun", function(event) {
			event.preventDefault();
			rerunReport(retrieveReportId(this), true);
			
			return false;
		})
		.on("click", "a.cancel", function(event) {
			event.preventDefault();
			cancelQueuedReport(retrieveReportId(this));
			
			return false;
		})
		.on("click", "a.view", function(event) {
			event.preventDefault();
			
			var id = retrieveReportId(this);
			var errorDetail = $("#queuedReportsList li#report_" + id + " span").html().trim();
			if(errorDetail.length > 0) {
				formErrorDialog("<li>" + errorDetail + "</li>");
			}
			
			return false;
		})
		.on("click", "a.edit", function(event) {
			event.preventDefault();
			fixReport(retrieveReportId(this));
			
			return false;
		})
		.on("click", "a.delete", function(event) {
			event.preventDefault();
			deleteHistory(retrieveReportId(this));
			
			return false;
		});
	
	$reportsList
		.on("click", "a.view", function(event) {
			event.preventDefault();
			viewReport(retrieveReportId(this), false);
			
			return false;
		})
		.on("click", "a.rerun", function(event) {
			event.preventDefault();
			rerunReport(retrieveReportId(this), false);
			
			return false;
		})
		.on("click", "a.recreate", function(event) {
			event.preventDefault();
			recreateReport(retrieveReportId(this), false);
			
			return false;
		})
		.on("click", "a.delete", function(event) {
			event.preventDefault();
			deleteReport(retrieveReportId(this));
			
			return false;
		});
}

function attachReportDefinitionsMenuHandler() {
	var $reportDefinitionsList = $("#reportDefinitionsList");
	
	$reportDefinitionsList
	.on("click", "a.run", function(event) {
		event.preventDefault();
		runReport(retrieveReportId(this));
		
		return false;
	})
	.on("click", "a.delete", function(event) {
		event.preventDefault();
		deleteReportDefinition(retrieveReportId(this));
		
		return false;
	})
	.on("click", "a.edit", function(event) {
		event.preventDefault();
		loadReportDefinition(retrieveReportId(this));
		
		return false;
	});
	
	$("#btnAddScheduleReport,#btnAddScheduleReportWS").attr("href", LOC.createReportDefinition +"?"+ document.location.search.substring(1));
}

function retrieveReportId(element) {
	var result = null;
	var $containerItem = $(element).parent("section").parent("section");
	if($containerItem.length > 0) {
		result = $containerItem.attr("id");
		result = result.substring(result.indexOf("_") + 1);
	}
	
	return result;
}

function runReport(definitionId) {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status !== 200) {
				alertDialog(systemErrorMsg); //Unable to load location details
			}
			else if(request.responseText === "false") {
				alertDialog(reportRunFail);
			}
			else {
				renderReportsList(request.responseText);
				noteDialog(reportRunSuccess, "timeOut", 2500);
			}
		}
	};
	
	request.open("GET",LOC.runReport +"?definitionID=" + definitionId + formatSortQueryString($("#reportsList"))+"&"+document.location.search.substring(1), true);
	request.send();
}

function deleteReportDefinition(definitionId) {
	confirmDialog({
		title: attentionTitle,
		message: reportDefDeleteConfirm,
		okCallback: function() {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if(request.status !== 200) {
						alertDialog(systemErrorMsg); //Unable to load location details
					}
					else if(request.responseText === "false") {
						alertDialog(reportDefDeleteFail);
					}
					else {
						renderReportDefinitionsList(request.responseText);
						noteDialog(reportDefDeleteSuccess, "timeOut", 800);
					}
				}
			};
			
			request.open("GET",LOC.deleteReportDefinition +"?definitionID=" + definitionId + formatSortQueryString($("#reportDefinitionsList"))+"&"+document.location.search.substring(1), true);
			request.send();
		}
	});
}

function rerunReport(reportId, isFromQueue) {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status !== 200) {
				alertDialog(systemErrorMsg); //Unable to load location details
			}
			else if(request.responseText === "false") {
				alertDialog(reportRerunFail);
			}
			else {
				renderReportsList(request.responseText);
				noteDialog(reportRerunSuccess, "timeOut", 2500);
			}
		}
	};
	
	var paramsStr;
	if(isFromQueue) {
		paramsStr = "queueID=" + reportId;
	}
	else {
		paramsStr = "repositoryID=" + reportId ;
	}
	
	request.open("GET",LOC.rerunReport +"?" + paramsStr + formatSortQueryString($("#reportsList"))+"&"+document.location.search.substring(1), true);
	request.send();
}

function deleteReport(reportId) {
	confirmDialog({
		title: attentionTitle,
		message: reportDeleteConfirm,
		okCallback: function() {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if(request.status !== 200) {
						alertDialog(systemErrorMsg); //Unable to load location details
					}
					else if(request.responseText === "false") {
						alertDialog(reportDeleteFail);
					}
					else {
						renderReportsList(request.responseText);
						noteDialog(reportDeleteSuccess, "timeOut", 800);
					}
				}
			};
			
			request.open("GET",LOC.deleteReport +"?repositoryID=" + reportId + formatSortQueryString($("#reportsList"))+"&"+document.location.search.substring(1), true);
			request.send();
		}
	});
}

function viewReport(reportId) {
	window.open(LOC.viewReport + "?" + scrubQueryString(document.location.search.substring(1), "all") + "&repositoryID=" + reportId, "reportViewer");
}

function cancelQueuedReport(reportId) {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status !== 200) {
				alertDialog(systemErrorMsg); //Unable to load location details
			}
			else if(request.getResponseHeader("CustomStatusCode") == 283) {
				alertDialog(reportCancelRunningFail);
			}
			else if(request.responseText === "false") {
				alertDialog(reportCancelFail);
			}
			else {
				renderReportsList(request.responseText);
				noteDialog(reportCancelSuccess, "timeOut", 2500);
			}
		}
	};
	
	request.open("GET",LOC.cancelQueuedReport +"?queueID=" + reportId + formatSortQueryString($("#reportsList"))+"&"+document.location.search.substring(1), true);
	request.send();
}

function fixReport(reportId) {
	document.location.href = LOC.fixReport +"?"+ scrubQueryString(document.location.search.substring(1), "all") + "&queueID=" + reportId;
}

function deleteHistory(reportId) {
	confirmDialog({
		title: attentionTitle,
		message: reportDeleteHistoryConfirm,
		okCallback: function() {
			var request = GetHttpObject();
			request.onreadystatechange = function() {
				if(request.readyState === 4) {
					if(request.status !== 200) {
						alertDialog(systemErrorMsg); //Unable to load location details
					}
					else if(request.responseText === "false") {
						alertDialog(reportDeleteHistoryFail);
					}
					else {
						renderReportsList(request.responseText);
						noteDialog(reportDeleteHistorySuccess, "timeOut", 800);
					}
				}
			};
			
			request.open("GET",LOC.deleteHistory +"?"+ document.location.search.substring(1) + "&queueID=" + reportId + formatSortQueryString($("#reportsList")), true);
			request.send();
		}
	});
}

function recreateReport(reportId) {
	document.location.href = LOC.recreateReportDefinition + "?" + scrubQueryString(document.location.search.substring(1), "all") + "&repositoryID=" + reportId;
}

function getSToken() {
	var result = "";
	
	var pList = document.location.search.substring(1).split("&");
	var p, idx = -1, len = pList.length;
	while((result.length <= 0) && (++idx < len)) {
		p = pList[idx];
		if(p.indexOf("sessionToken") >= 0) {
			result = p;
		}
	}
	
	return result;
}

function loadReportDefinition(definitionId) {
	document.location.href = LOC.loadReportDefinition + "?" + scrubQueryString(document.location.search.substring(1), "all") + "&definitionID=" + definitionId;
}

/* No need for non JSON mode
function renderReportDefinitionsList(responseText) {
	var definitionsList = JSON.parse(responseText).ReportDefinitionInfo.definitionInfoList;
	
	var menuPrototypeStr = $("#definitionMenuPrototype").html();
	var $container = $("#reportDefinitionsList");
	$container.find("li:not(.listHeader)").remove();
	
	var len = definitionsList.length;
	var i = -1;
	var definition;
	while(++i < len) {
		definition = definitionsList[i];
		var $item = $(applyTemplate(menuPrototypeStr, definition));
		
		$container.append($item);
	}
	
	refreshListSort($container);
}

function renderReportsList(responseText) {
	var reports = JSON.parse(responseText).ReportQueueAndRepositoryInfo;
	renderPendingReports(reports.ReportQueueInfo.queueInfoList);
	renderCompletedReports(reports.ReportRepositoryInfo.repositoryInfoList);
}

function renderPendingReports(reportsList) {
	var menuPrototypeStr = $("#queueMenuPrototype").html();
	var $container = $("#queuedReportsList");
	$container.find("li:not(.listHeader)").remove();
	
	var len = reportsList.length;
	var i = -1;
	var report;
	while(++i < len) {
		report = reportsList[i];
		var $item = $(applyTemplate(menuPrototypeStr, report));
		
		$container.append($item);
	}
}

function renderCompletedReports(reportsList) {
	var menuPrototypeStr = $("#repositoryMenuPrototype").html();
	var $container = $("#reportsList");
	$container.find("li:not(.listHeader)").remove();
	
	var len = reportsList.length;
	var i = -1;
	var report;
	while(++i < len) {
		report = reportsList[i];
		$container.append(applyTemplate(menuPrototypeStr, report));
	}
	
	refreshListSort($container);
}
*/

function renderReportsList(reportsList) {
	$("#reportsPanel").html(reportsList);
	attachReportsMenuHandler();
	soh([ "reportDefinitionsList" ], null, null, [ "queuedReportsList", "reportsList" ]);
}

function renderReportDefinitionsList(definitionsList) {
	$("#reportDefinitionsPanel").html(definitionsList);
	attachReportDefinitionsMenuHandler();
	soh([ "reportDefinitionsList" ], null, null, [ "queuedReportsList", "reportsList" ]);
}

function formatSortQueryString(list) {
	var result = "";
	var sortOptions = $(list).find(".sort").filter(".current");
	if(sortOptions.length <= 0) {
		sortOptions = [];
	}
	else {
		sortOptions = sortOptions.attr("sort").split(":");
	}
	
	if(sortOptions.length > 1) {
		if(sortOptions[1] === "asc") {
			sortOptions[1] = "desc";
		}
		else {
			sortOptions[1] = "asc";
		}
		
		result = "&sort=" + sortOptions[0] + "&dir=" + sortOptions[1];
	}
	
	return result;
}

function _createFormDefinitions() {
	var result = {};
	result[1] = {
			"name": "txAll",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[2] = {
			"name": "txCash",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[3] = {
			"name": "txCC",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.creditCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "4"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber", "filterCardType", "filterCardApprovalStatus", "filterMerchantAccount",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[4] = {
			"name": "txCCRefund",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.cardRefundTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.creditCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "4"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber", "filterCardType", "filterMerchantAccount",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[5] = {
			"name": "txCardProcessing",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.cardProcessingTime,
				"#filterSecondaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.creditCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "4"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime", "filterSecondaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber", "filterCardType", "filterMerchantAccount",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[6] = {
			"name": "txCardRetry",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.cardRetryTime,
				"#filterSecondaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.creditCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.retryType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "4"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime", "filterSecondaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterTicket",
			              "filterCard", "filterCardNumber", "filterCardType",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[7] = {
			"name": "txPatrollerCard",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.creditCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "37"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[8] = {
			"name": "txRate",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "false"
				},
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF", "switchIsNotPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"extraCallback": function() {
//				$("#switchIsSummary").hide();
			},
			"visibles": [ "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[9] = {
			"name": "txRateSummary",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF", "switchIsNotPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"extraCallback": function() {
//				$("#switchIsNotSummary").hide();
			},
			"visibles": [ "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterOther", "filterTransactionType" ]
	};
	result[10] = {
			"name": "txSmartCard",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.smartCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "alphaNumChrctr",
					"maxlength": "20"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[11] = {
			"name": "txValueCard",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterCardLabel": reportLabel.valueCard,
				"#filterTicketLabel": reportLabel.ticketNumber,
				"#filterTransactionTypeLabel": reportLabel.transactionType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#formCardNumberSpecific": {
					"type": "intNmbrChrctr",
					"maxlength": "37"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber",
			              "filterCard", "filterCardNumber", "filterCardType", "filterCardApprovalStatus", "filterMerchantAccount",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[12] = {
			"name": "stallReports",
			"labels": {
				"#filterTransactionTypeLabel": reportLabel.reportType
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF" ]
			},
			"extraCallback": function() {
//				$("#switchIsNotSummary").hide();
				setBooleanRadioValue("isPDF", "true");
				$("#switchIsNotPDF").hide();
			},
			"visibles": [ "filterSpacePayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterSpaceNumber",
			              "filterOther", "filterTransactionType" ]
	};
	result[13] = {
			"name": "couponUsageSummary",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterCoupon", "filterCouponType", "filterAccount" ]
	};
	result[14] = {
			"name": "auditReports",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.auditBeginTime,
				"#filterSecondaryDateTimeLabel": reportLabel.auditEndTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTicketLabel": reportLabel.reportNumber,
				"#filterTransactionTypeLabel": reportLabel.reportType
			},
			"inputs": {
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterTicket",
			              "filterOther", "filterTransactionType", "filterGroupBy" ]
	};
	result[15] = {
			"name": "replenishReports",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.replenishTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected,
				"#filterTicketLabel": reportLabel.reportNumber
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "false"
				},
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterLocation", "filterPayStations",
			              "filterPayStationFlag",
			              "filterCustomer", "filterTicket",
			              "filterOther", "filterGroupBy" ]
	};
	result[16] = {
			"name": "payStationSummary",
			"labels": {
				"#filterPSPickerFullListLabel": reportLabel.locationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationSelected
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF", "switchIsNotPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"extraCallback": function() {
//				$("#switchIsNotSummary").hide();
			},
			"visibles": [ "filterLocation", "filterPayStations",
			              "filterPayStationFlag",
			              "filterOther", "filterSortBy" ]
	};
	result[17] = {
			"name": "taxesReport",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.taxTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "summaryFlag",
			              "filterDateTime", "filterPrimaryDateTime",
			              "filterLocation", "filterPayStations",
			              "filterPayStationFlag",
			              "filterOther", "filterGroupBy" ]
	};
	result[18] = {
			"name": "txDelay",
			"labels": {
				"#filterPrimaryDateTimeLabel": reportLabel.transactionTime,
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "false"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"details": [ "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "filterDateTime", "filterOrganizations",
			              "filterPrimaryDateTime", "filterLocation", "filterPayStations", "filterPayStationFlag",
			              "filterCustomer" ]
	};
	result[19] = {
			"name": "inventoryReports",
			"labels": {
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isPDF": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"summary": [ "switchIsPDF" ],
				"details": [ "switchIsPDF", "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag", "filterOrganizations",
			              "filterLocation", "filterPayStations", "filterPayStationFlag" ]
	};
	result[20] = {
			"name": "paystationInventoryReports",
			"labels": {
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isPDF": {
					"defaultValue": "false"
				},
				"#showHiddenPayStations": {
					"defaultValue": "true"
				}
			},
			"outputs": {
				"details": [ "switchIsNotPDF" ]
			},
			"visibles": [ "generalReportFlag" ]
	};
	result[21] = {
			"name": "maintenanceReqReport",
			"labels": {
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "false"
				},
				"#isPDF": {
					"defaultValue": "true"
				}
			},
			"visibles": [ "generalReportFlag",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterPSAlert", "filterPSAlertSeverity", "filterPSAlertDevice",
			              "filterOther", "filterGroupBy" ]
	};
	result[22] = {
			"name": "collectionReqReport",
			"labels": {
				"#filterPSPickerFullListLabel": reportLabel.locationPayStationFull,
				"#filterPSPickerSelectedListLabel": reportLabel.locationPayStationSelected
			},
			"inputs": {
				"#isSummary": {
					"defaultValue": "true"
				},
				"#showHiddenPayStations": {
					"defaultValue": "false"
				},
				"#isPDF": {
					"defaultValue": "true"
				}
			},
			"visibles": [ "generalReportFlag",
			              "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
			              "filterCollectionAlert", "filterCollectionType",
			              "filterOther", "filterGroupBy" ]
	};
	
	return result;
}

var allElements = [ "generalReportFlag", "summaryFlag",
                    "filterDateTime", "filterPrimaryDateTime", "filterSecondaryDateTime",
                    "filterOrganizations", "filterLocation", "filterPayStationSetting", "filterPayStations",
                    "filterSpacePayStations",
                    "filterPayStationFlag",
                    "filterCustomer", "filterLicensePlate", "filterTicket", "filterCoupon", "filterSpaceNumber", "filterAccount",
                    "filterCard", "filterCardNumber", "filterCardType", "filterCardApprovalStatus", "filterMerchantAccount",
                    "filterPSAlert", "filterPSAlertSeverity", "filterPSAlertDevice",
                    "filterCollectionAlert", "filterCollectionType",
                    "filterOther", "filterTransactionType", "filterGroupBy", "filterSortBy", "filterCouponType" ];

var allContainers = {
	"filterDateTime": 1,
	"filterLocation": 1,
	"filterCustomer": 1,
	"filterCard": 1,
	"filterOther":1
};

var otherParams = {
	"filterTransactionType": 1,
	"filterGroupBy": 1,
	"filterSortBy": 1
};

var allOutputFormats = [ "switchIsPDF", "switchIsNotPDF" ];

var formDefinitions = _createFormDefinitions();

function createIgnoredInputsHash(reportTypeId) {
	var result = {};
	var idx = -1, len = allElements.length;
	var inputId, inputIdx, inputLen, inputList;
	while(++idx < len) {
		inputList = $("#" + allElements[idx]).find(":input");
		inputIdx = -1;
		inputLen = inputList.length;
		while(++inputIdx < inputLen) {
			var $input = $(inputList[inputIdx]);
			inputId = $input.attr("id");
			if(inputId) {
				result[inputId] = true;
			}
		}
	}
	
	var elems = formDefinitions[reportTypeId].visibles;
	idx = -1;
	len = elems.length;
	while(++idx < len) {
		if(!allContainers[elems[idx]]) {
			inputList = $("#" + elems[idx]).find(":input");
			inputIdx = -1;
			inputLen = inputList.length;
			while(++inputIdx < inputLen) {
				inputId = $(inputList[inputIdx]).attr("id");
				if(inputId) {
					delete result[inputId];
				}
			}
		}
	}
	
	return result;
}

var spacerImgStr = "<img src=\"" + imgLocalDir + "spacer.gif\" border=\"0\" style=\"height: 10px; width: 18px;\"/>";

function createSelector(node) {
	return "<a href='#' class='checkBox'>&nbsp;</a>";
}

function createExpander(node) {
	return "<span class='menuExpand treepicker-expander'>" + spacerImgStr + "</span>";
}

function appendMainNode(nodeContainer, node, conf) {
	nodeContainer.push("<strong style='");
	if(!node.isVisible) {
		nodeContainer.push("text-decoration: line-through; ");
	}
	
	nodeContainer.push("'>&nbsp;");
	nodeContainer.push(node.name);
	nodeContainer.push("</strong>");
}

function appendSecondaryNode(nodeContainer, node, padding, conf) {
	nodeContainer.push("<em style='");
	if(!node.isVisible) {
		nodeContainer.push("text-decoration: line-through; ");
	}
	
	if(padding) {
		nodeContainer.push("padding-left: ");
		nodeContainer.push(padding);
		nodeContainer.push("px; ");
	}
	
	nodeContainer.push("'>");
	if(!padding) {
		nodeContainer.push("&nbsp;");
	}
	
	nodeContainer.push(node.name);
	nodeContainer.push("</em>");
}

function drawTreepickerOrgNode(node, nodeContainer, nodeContainerClasses, conf) {
	nodeContainer.push("<span>");
	if(conf.showSelector && (node.isOrganization)) {
		nodeContainerClasses.push("treepicker-selector");
		nodeContainer.push(createSelector(node));
		appendMainNode(nodeContainer, node, conf);
	}
	else {
		appendSecondaryNode(nodeContainer, node, 15, conf);
	}
	
	if(conf.showExpander) {
		nodeContainer.push(createExpander(node, conf));
	}
	nodeContainer.push("</span>");
}

function drawTreepickerLocNode(node, nodeContainer, nodeContainerClasses, conf) {
	nodeContainer.push("<span>");
	if(conf.showSelector && (node.isLocation || node.isChildLocation)) {
		nodeContainerClasses.push("treepicker-selector");
		nodeContainer.push(createSelector(node));
		appendMainNode(nodeContainer, node, conf);
	}
	else if(node.isOrganization) {
		appendSecondaryNode(nodeContainer, node, null, conf);
	}
	else {
		appendSecondaryNode(nodeContainer, node, 15, conf);
	}
	
	if(conf.showExpander) {
		nodeContainer.push(createExpander(node, conf));
	}
	nodeContainer.push("</span>");
}

function drawTreepickerRouteNode(node, nodeContainer, nodeContainerClasses, conf) {
	nodeContainer.push("<span>");
	if(conf.showSelector && (node.isRoute)) {
		nodeContainerClasses.push("treepicker-selector");
		nodeContainer.push(createSelector(node));
		appendMainNode(nodeContainer, node, conf);
	}
	else if(node.isOrganization) {
		appendSecondaryNode(nodeContainer, node, null, conf);
	}
	else {
		appendSecondaryNode(nodeContainer, node, 15, conf);
	}
	
	if(conf.showExpander) {
		nodeContainer.push(createExpander(node, conf));
	}
	nodeContainer.push("</span>");
}

function drawTreepickerPSNode(node, nodeContainer, nodeContainerClasses, conf) {
	nodeContainer.push("<span>");
	if(conf.showSelector) {
		nodeContainerClasses.push("treepicker-selector");
		nodeContainer.push(createSelector(node));
		appendMainNode(nodeContainer, node, conf);
	}
	else if(node.isOrganization) {
		appendMainNode(nodeContainer, node, conf);
	}
	else {
		appendSecondaryNode(nodeContainer, node, 15, conf);
	}
	
	if(conf.showExpander) {
		nodeContainer.push(createExpander(node, conf));
	}
	nodeContainer.push("</span>");
}

function drawTreepickerMultiSelectNode(node, nodeContainer, nodeContainerClasses, conf) {
	nodeContainer.push("<span>");
	if(conf.showSelector) {
		nodeContainerClasses.push("treepicker-selector");
		nodeContainer.push(createSelector(node));
		appendMainNode(nodeContainer, node, conf);
	}
	else {
		appendSecondaryNode(nodeContainer, node, null, conf);
	}
	
	if(conf.showExpander) {
		nodeContainer.push(createExpander(node, conf));
	}
	nodeContainer.push("</span>");
}

function extractAccountList(responseText, context) {
	var result = null;
	
	var accountList = JSON.parse(responseText).searchConsumerResult;
	if(accountList) {
		$("#searchPostToken").val(accountList.token);
		
		result = accountList.searchConsumer;
		
		if(result && (!Array.isArray(result))) {
			result = [ result ];
		}
	}
	
	return result;
}

function reportingForm() {
	setupCalendarAction();
	
	msgResolver.mapAttribute("primaryDateType", "#formPrimaryDateType");
	msgResolver.mapAttribute("primaryDateBegin", "#formPrimaryDateBegin");
	msgResolver.mapAttribute("primaryTimeBegin", "#formPrimaryTimeBegin");
	msgResolver.mapAttribute("primaryDateEnd", "#formPrimaryDateEnd");
	msgResolver.mapAttribute("primaryTimeEnd", "#formPrimaryTimeEnd");
	msgResolver.mapAttribute("primaryMonthBegin", "#formPrimaryMonthBegin");
	msgResolver.mapAttribute("primaryYearBegin", "#formPrimaryYearBegin");
	msgResolver.mapAttribute("primaryMonthEnd", "#formPrimaryMonthEnd");
	msgResolver.mapAttribute("primaryYearEnd", "#formPrimaryYearEnd");
	
	msgResolver.mapAttribute("secondaryDateType", "#formSecondaryDateType");
	msgResolver.mapAttribute("secondaryDateBegin", "#formSecondaryDateBegin");
	msgResolver.mapAttribute("secondaryTimeBegin", "#formSecondaryTimeBegin");
	msgResolver.mapAttribute("secondaryDateEnd", "#formSecondaryDateEnd");
	msgResolver.mapAttribute("secondaryTimeEnd", "#formSecondaryTimeEnd");
	msgResolver.mapAttribute("secondaryMonthBegin", "#formSecondaryMonthBegin");
	msgResolver.mapAttribute("secondaryYearBegin", "#formSecondaryYearBegin");
	msgResolver.mapAttribute("secondaryMonthEnd", "#formSecondaryMonthEnd");
	msgResolver.mapAttribute("secondaryYearEnd", "#formSecondaryYearEnd");
	
	msgResolver.mapAttribute("locationType", "#formLocationType");
	msgResolver.mapAttribute("locationPOSListFilters", "#reportPOSTree .filterableBox,#reportPOSTree .treepicker-list-selected");
	msgResolver.mapAttribute("routeListFilters", "#reportRoute .treepicker-list-full,#reportRoute .treepicker-list-selected");
	msgResolver.mapAttribute("location", "#spaceLocation");
	
	msgResolver.mapAttribute("spaceNumberType", "#formSpaceNumberType");
	msgResolver.mapAttribute("spaceNumberBegin", "#formSpaceNumberBegin");
	msgResolver.mapAttribute("spaceNumberEnd", "#formSpaceNumberEnd");
	msgResolver.mapAttribute("licensePlate", "#formLicensePlate");
	msgResolver.mapAttribute("ticketNumberType", "#formTicketNumberType");
	msgResolver.mapAttribute("ticketNumberBegin", "#formTicketNumberBegin");
	msgResolver.mapAttribute("ticketNumberEnd", "#formTicketNumberEnd");
	msgResolver.mapAttribute("couponNumberType", "#formCouponNumberType");
	msgResolver.mapAttribute("couponNumber", "#formCouponNumber");
	msgResolver.mapAttribute("couponType", "#formCouponType");
	
	msgResolver.mapAttribute("approvalStatus", "#formApprovalStatus");
	msgResolver.mapAttribute("cardType", "#formCardType");
	msgResolver.mapAttribute("cardNumberType", "#formCardNumberType");
	msgResolver.mapAttribute("cardNumberSpecific", "#formCardNumberSpecific");
	msgResolver.mapAttribute("merchantAccount", '#merchantAccount');
	
	msgResolver.mapAttribute("alertSeverity", "#formAlertSeverity");
	msgResolver.mapAttribute("alertDeviceType", "#formAlertDevice");
	msgResolver.mapAttribute("collectionType", "#filterCollectionType");
	
	msgResolver.mapAttribute("otherParameters", "#formOtherParameters");
	msgResolver.mapAttribute("groupBy", "#formGroupBy");
	msgResolver.mapAttribute("sortBy", "#formSortBy");
	
	msgResolver.mapAttribute("outputFormat", "#outputFormat");
	
	msgResolver.mapAttribute("reportTitle", "#reportTitle");
	msgResolver.mapAttribute("startDate", "#formScheduleStartDate");
	msgResolver.mapAttribute("startTime", "#formScheduleStartTime");
	msgResolver.mapAttribute("repeatType", "#formRepeatType");
	msgResolver.mapAttribute("repeateFrequency", "#formRepeatFrequency");
	msgResolver.mapAttribute("repeatOn", "#dayOfWeekSelector, #dayOfMonthSelector");
	msgResolver.mapAttribute("reportEmail", "#formCurrentEmailList");
		
	var	customerTime = -customerOffset,
		userTime = new Date().getTimezoneOffset();
	if(customerTime !== userTime){ $("#timeZoneWrning").css("display", "inline"); }
	else{ $("#timeZoneWrning").css("display", "none"); }

	/* General Options */
	var optsMultiSelect = {
		"fullListCSS": "treepicker-list-full",
		"selectedListCSS": "treepicker-list-selected",
		"rootSelectorCSS": "treepicker-root-selector",
		"propagateSelectionUp": false,
		"childAttribute": "children",
		"selectedAttributeName": "isSelected",
		"allowSelectionToggle": false,
		"rootValue": "0",
		"drawNode": drawTreepickerMultiSelectNode,
		"nodeValue": function(node) {
			return node.randomId;
		},
		"showSelectedHierarchy": false
	};
	
	var optsTime = { "militaryMode": false };
	var optsSecTime = { "militaryMode": false, "isSeconds": true };

	
	/* Report Type */
	var $reportType = $("#reportTypeID").autoselector({
		"data": "#reportTypeIDData",
		"defaultValue": null,
		"isComboBox": true,
		"invalidValue": -1
	});
	
	$("#reportTypeIDData").remove();
	
	/* Report Summary */
	var $isSummary = $("#isSummary");
	setBooleanRadioValue("isSummary", $isSummary.val());
	updateOutputFormatVisibility($isSummary.val().toLowerCase() === "true");
	
	$("#switchIsSummary").on("click", function(event) {
		updateOutputFormatVisibility(true);
	});
	$("#switchIsNotSummary").on("click", function(event) {
		updateOutputFormatVisibility(false);
	});
	
	/* Primary Date */
	var $primaryDateType = $("#formPrimaryDateType")
		.autoselector({
			"isComboBox": true
		})
		.on("itemSelected", function(event, itemSelected) {
			updateDateInputsVisibility("Primary", itemSelected.value);
		}
	);
	
	coupleDateComponents(
			createDatePicker("#formPrimaryDateBegin"),
			$("#formPrimaryTimeBegin").autominutes(optsSecTime),
			createDatePicker("#formPrimaryDateEnd"),
			$("#formPrimaryTimeEnd").autominutes(optsSecTime));
	
	$("#formPrimaryMonthBegin").automonths();
	$("#formPrimaryYearBegin").autoyears();
	$("#formPrimaryMonthEnd").automonths();
	$("#formPrimaryYearEnd").autoyears();
	
	updateDateInputsVisibility("Primary", $primaryDateType.autoselector("getSelectedValue"));
	
	/* Secondary Date */
	var $secondaryDateType = $("#formSecondaryDateType")
		.autoselector({
			"isComboBox": true
		})
		.on("itemSelected", function(event, itemSelected) {
			updateDateInputsVisibility("Secondary", itemSelected.value);
		}
	);
	
	coupleDateComponents(
			createDatePicker("#formSecondaryDateBegin"),
			$("#formSecondaryTimeBegin").autominutes(optsSecTime),
			createDatePicker("#formSecondaryDateEnd"),
			$("#formSecondaryTimeEnd").autominutes(optsSecTime));
	
	$("#formSecondaryMonthBegin").automonths();
	$("#formSecondaryYearBegin").autoyears();
	$("#formSecondaryMonthEnd").automonths();
	$("#formSecondaryYearEnd").autoyears();
	
	updateDateInputsVisibility("Secondary", $secondaryDateType.autoselector("getSelectedValue"));
	
	/* Location */
	var $locationType = $("#formLocationType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			updatePayStationsVisibility(itemSelected.value);
		});
	
	var optsPosTree = $.extend(true, {}, optsMultiSelect);
	optsPosTree["drawNode"] = drawTreepickerOrgNode;
	optsPosTree["showSelectedHierarchy"] = true;
	
	var $orgTree = $("#reportOrgTree");
	$orgTree
			.treepicker(optsPosTree)
			.on("nodeSelected nodeUnselected", updateOrganizationSelection);
	
	optsPosTree = $.extend(true, {}, optsMultiSelect);
	optsPosTree["drawNode"] = drawTreepickerLocNode;
	optsPosTree["showSelectedHierarchy"] = true;
	
	$("#reportPOSTree").treepicker(optsPosTree);
	
	optsPosTree = $.extend(true, {}, optsMultiSelect);
	optsPosTree["drawNode"] = drawTreepickerRouteNode;
	optsPosTree["showSelectedHierarchy"] = true;
	
	$("#reportRoute").treepicker(optsPosTree);
	
	optsPosTree = $.extend(true, {}, optsMultiSelect);
	optsPosTree["drawNode"] = drawTreepickerPSNode;
	
	$("#reportPayStations").treepicker(optsPosTree);
	
	$("#formPaystationsFilter")
		.autoselector({
			"isComboBox": true,
			"shouldCategorize": true
		})
		.on("itemSelected", _updatePayStationPickerData);
	
	var $showHiddenPayStations = $("#showHiddenPayStations");
	setBooleanRadioValue("showHiddenPayStations", $showHiddenPayStations.val());
	
	$("#switchShowHiddenPayStations").on("click", function(event) {
		updateShowHiddenPayStations(true);
	});
	$("#switchNotShowHiddenPayStations").on("click", function(event) {
		updateShowHiddenPayStations(false);
	});
	
	updatePayStationsVisibility($locationType.autoselector("getSelectedValue"));
	
	var $spaceNumberType = $("#formSpaceNumberType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			updateSpaceNumberVisibility(itemSelected.value);
		}
	);
	
	updateSpaceNumberVisibility($spaceNumberType.autoselector("getSelectedValue"));
	
	/* Space's Location */
	$("#spaceLocation").autoselector({
		"isComboBox": true
	});
	
	/* Parker */
	$("#formSpaceNumberType").autoselector({
		"isComboBox": true,
		"invalidValue": -1
	});
	
	var $ticketNumType = $("#formTicketNumberType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			updateTicketNumberVisibility(itemSelected.value);
		}
	);
	
	updateTicketNumberVisibility($ticketNumType.autoselector("getSelectedValue"));
	
	/* Coupon Code */
	var $couponNumType = $("#formCouponNumberType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			updateCouponNumberVisibility(itemSelected.value);
		}
	);
	
	updateCouponNumberVisibility($couponNumType.autoselector("getSelectedValue"));
	
	/* Coupon Type */
	$("#formCouponType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
	});
			
	$("#accountFilterValue")
	.autoselector({
		"minLength": 3,
		"persistToHidden": false,
		"shouldCategorize": false,
		"remoteFn": accountAutoComplete,
		"selectByTyping": false
	})
	.on("itemSelected", function(ui, selectedItem) {
		if(selectedItem.obj) {			
			$("#consumerRandomId").val(selectedItem.obj.consumerRandomId);
			$("#accountFilterValue").val(selectedItem.label);
		}
	})
	.on("keyup", function(event) {
		if($("#accountFilterValue").val() == ""){
			$("#consumerRandomId").val("");
		}
	});


	/* Credit Card */
	var $cardNumType = $("#formCardNumberType")
		.autoselector({
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			updateCardNumberVisibility(itemSelected.value);
		}
	);
	
	updateCardNumberVisibility($cardNumType.autoselector("getSelectedValue"));
	
	$("#formCardType").autoselector({
		"isComboBox": true,
		"itemTemplate": $("#customerCardTypeFilterTemplate").html()
	});
	
	$("#formApprovalStatus").autoselector({
		"isComboBox": true,
		"invalidValue": -1
	});
	
	$("#merchantAccount").autoselector({
		"isComboBox": true,
		"itemTemplate": $("#merchantAccountFilterTemplate").html()
	});
	
	$("#formAlertSeverity").autoselector({
		"isComboBox": true
	});
	
	$("#formAlertDevice").autoselector({
		"isComboBox": true
	});
	
	setupCheckBoxes("#isForCoinCountCheckContainer", "#isForCoinCount", true);
	setupCheckBoxes("#isForCoinAmountCheckContainer", "#isForCoinAmount", true);
	setupCheckBoxes("#isForBillCountCheckContainer", "#isForBillCount", true);
	setupCheckBoxes("#isForBillAmountCheckContainer", "#isForBillAmount", true);
	setupCheckBoxes("#isForCardCountCheckContainer", "#isForCardCount", true);
	setupCheckBoxes("#isForCardAmountCheckContainer", "#isForCardAmount", true);
	setupCheckBoxes("#isForRunningTotalCheckContainer", "#isForRunningTotal", true);
	setupCheckBoxes("#isForOverdueCollectionCheckContainer", "#isForOverdueCollection", true);
	
	/* Other Parameters */
	$("#formOtherParameters").autoselector({
		"isComboBox": true,
		"invalidValue": -1
	});
	$("#formGroupBy").autoselector({
		"isComboBox": true,
		"invalidValue": -1,
		"filterFn": function(elem) {
			var result = true;
			if(elem.value == "1313") {
				result = ($locationType.autoselector("getSelectedValue") == 301);
			}
			else if(elem.value == "1314") {
				var selectedValues = $orgTree.treepicker("getSelectedValues");
				if(selectedValues.length == 0) {
					result = false;
				}
				else if((selectedValues.length > 1) || (selectedValues[0] == "0")) {
					// DO NOTHING
				}
				else {
					result = false;
				}
			}
			
			return result;
		}
	});
	$("#formSortBy").autoselector({
		"isComboBox": true,
		"invalidValue": -1
	});
	
	/* Output */
	$("#switchIsPDF").on('click', function() {
		updateGroupByVisibility(true);
	});
	$("#switchIsNotPDF").on('click', function() {
		updateGroupByVisibility(false);
	});
	
	setBooleanRadioValue("isPDF", $("#isPDF").val());
	
	/* Scheduling */
	var $scheduleForm = $("#reportScheduleDetail");
	
	var scheduleFlag = $("#isScheduled").val();
	setBooleanRadioValue("isScheduled", scheduleFlag);
	
	if(scheduleFlag === "true") {
		$scheduleForm.show();
	}
	else {
		$scheduleForm.hide();
	}
	
	$("#scheduling .radioLabel").on('click', function(event) {
		var $elem = $(event.target);
		if(!$elem.is("a")) {
			$elem = $elem.find("a");
		}
		
		if($elem.attr("id") === "isScheduled:true") {
			$scheduleForm.show();
		}
		else {
			$scheduleForm.hide();
		}
	});
	
	createDatePicker("#formScheduleStartDate", {"minDate": 0 });
	var $scheduleStartDate = $("#formScheduleStartDate");
	
	var $scheduleStartTime = $("#formScheduleStartTime");
	$scheduleStartTime
			.autominutes(optsTime)
			.on("beforeopen", function(itemsList) {
				var selectedDate = $scheduleStartDate.datepicker("getDate");
				var now = new Date();
				if(selectedDate && ((selectedDate.getFullYear() === now.getFullYear()) && (selectedDate.getMonth() === now.getMonth()) && (selectedDate.getDate() === now.getDate()))) {
					$scheduleStartTime.autominutes("option", "minLimit", (now.getHours() * 60) + now.getMinutes());
				}
				else {
					if($scheduleStartTime.autominutes("option", "minLimit") != 0) {
						$scheduleStartTime.autominutes("option", "minLimit", 0);
						//console.log("*");
					}
				}
			});
	
	var $repeatType = $("#formRepeatType")
		.autoselector({
			"data": "#repeatTypeData",
			"isComboBox": true,
			"invalidValue": -1
		})
		.on("itemSelected", function(event, itemSelected) {
			$("#formRepeatOn").val("");
			updateDaySelectorVisibility(itemSelected.value);
		});
	
	$("#repeatTypeData").remove();
	updateDaySelectorVisibility($repeatType.autoselector("getSelectedValue"));
	
	var daySelectorType = $repeatType.autoselector("getSelectedValue");
	var daySelected = $("#formRepeatOn").val();
	if(daySelected.length > 0) {
		if(isDayOfWeekType(daySelectorType)) {
			setArrayCheckboxVals("dow_", daySelected);
		}
		else if(isDayOfMonthType(daySelectorType)) {
			setArrayListVals("dom_", daySelected);
		}
	}
	
	$("#formNewEmail").on('keypress', function(e) {
		if(e.keyCode == 13 || e.keyCode == 9) {
			addEmail($("#formCurrentEmailList"), $("#formEmailList"), $("#formNewEmail"));
			$(this).trigger('focus');
		}
	});
	
	$("#addEmailBtn").on('click', function(event){
		event.preventDefault();
		addEmail($("#formCurrentEmailList"), $("#formEmailList"), $("#formNewEmail"));
		
		return false;
	});
	
	$("#formCurrentEmailList").on("click", "li", function(event) {
		event.preventDefault();
		toggleEmailSelection($("#formCurrentEmailList"), $("#formEmailList"), $(this));
		
		return false;
	});
	
	var emailList = $("#formEmailList").val();
	$("#formEmailList").val("");
	addEmailString($("#formCurrentEmailList"), $("#formEmailList"), emailList);
	
	// Initialize Actions...
	$("#reportForm .save").on("click", saveReportDefinition);
	$("#reportForm .cancel").on("click", cancelReportDefinitionUpdates);
	
	// Pre-initialized data...
	var reportType = $reportType.autoselector("getSelectedValue");
	if(reportType) {
		prepareReportDefinitionForm(reportType);
	}
	else {
		$("#btnStep2Select").on("click", function(event) {
			event.preventDefault();
			prepareReportDefinitionForm($reportType.autoselector("getSelectedValue"));
			
			return false;
		});
		
		$("#btnStep3Select").on("click", function(event) {
			event.preventDefault();
			
			/* Uncomment this to switch to schedule by default.
			$("#isScheduled").val("true");
			setBooleanRadioValue("isScheduled", "true");
			$scheduleForm.show();
			*/
			
			displaySchedulingForm();
			
			return false;
		});
		
		$("#btnCancelReportType").on("click", resetSteps);
	}
	
	// Apply inline validations...
	pairComparableComponents($("#formSpaceNumberBegin"), $("#formSpaceNumberEnd"), function($lesser, $greater) {
		var result = false;
		
		var lesserVal = $lesser.val().trim();
		var greaterVal = $greater.val().trim();
		if((lesserVal.length > 0) && (greaterVal.length > 0) && (parseInt(lesserVal) > parseInt(greaterVal))) {
			result = reportSpaceNumberBeginShouldBeLesser;
		}
		
		return result;
	});
	
	pairComparableComponents($("#formTicketNumberBegin"), $("#formTicketNumberEnd"), function($lesser, $greater) {
		var result = false;
		
		var lesserVal = $lesser.val().trim();
		var greaterVal = $greater.val().trim();
		if((lesserVal.length > 0) && (greaterVal.length > 0) && (parseInt(lesserVal) > parseInt(greaterVal))) {
			result = reportTicketNumberBeginShouldBeLesser;
		}
		
		return result;
	});
	
	testFormEdit($("#reportForm"));
}

function prepareReportDefinitionForm(type) {
	var reportForm;
	
	var $jsonFilter = $("#jsonFilter");
	var jsonFilterText = $jsonFilter.html().trim();
	$jsonFilter.html("");
	if(jsonFilterText.length > 0) {
		reportForm = JSON.parse($("<div>").html(jsonFilterText).text()).ReportFilters;
		if(!reportForm) {
			alertDialog(systemErrorMsg);
		}
		else {
			displayReportDetailsForm(type, reportForm);
			if($("#reportDefinitionId").val().length > 0) {
				displaySchedulingForm();
			}
		}
	}
	else if(!type) {
		alertDialog(reportRequireReportType);
	}
	else {
		var request = GetHttpObject();
		request.onreadystatechange = function() {
			if(request.readyState === 4) {
				if(request.status !== 200) {
					alertDialog(systemErrorMsg); //Unable to load location details
				}
				else if(request.responseText === "false") {
					alertDialog(systemErrorMsg);
				}
				else {
					reportForm = JSON.parse(request.responseText).ReportFilters;
					if(!reportForm) {
						alertDialog(systemErrorMsg);
					}
					else {
						displayReportDetailsForm(type, reportForm);
						if($("#reportDefinitionId").val().length > 0) {
							displaySchedulingForm();
						}
					}
				}
			}
		};
		
		request.open("GET",LOC.prepareReportDefinitionForm +"?reportTypeID=" + type+"&"+document.location.search.substring(1), true);
		request.send();
	}
}

function prepareOrganizationFilter(reportTypeId, organizationId) {
	var request = GetHttpObject();
	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status !== 200) {
				alertDialog(systemErrorMsg); //Unable to load location details
			}
			else if(request.responseText === "false") {
				alertDialog(systemErrorMsg);
			}
			else {
				reportForm = JSON.parse(request.responseText).ReportFilters;
				if(!reportForm) {
					alertDialog(systemErrorMsg);
				}
				else {
					displayCustomerReportDetails(reportTypeId, reportForm);
					updatePayStationsVisibility($("#formLocationType").autoselector("getSelectedValue"));
				}
			}
		}
	};
	
	request.open("GET",LOC.getOrganizationFilters + "?reportTypeID=" + reportTypeId + "&organizationID=" + organizationId + "&" + document.location.search.substring(1), true);
	request.send();
}

function displayReportDetailsForm(type, reportForm) {
	var $step2 = $("#step2");
	var $step2Loader = $step2.find(".dashBoardLoader").fadeIn();
	var $reportDtlForm = $step2.find("#reportDetailsArea");
	
	var def = formDefinitions[type];
	showElements(allElements, def.visibles);
	assignHTMLContents(def.labels);
	applyInputConfigs(def.inputs);
	if((def.extraCallback) && (typeof def.extraCallback === 'function')) {
		def.extraCallback();
	}
	
	$("#btnStep2Select").hide();
	$("#formReportTypeSection").hide();
	$("#formReportTypeTitle").html($("#reportTypeID").autoselector("getSelectedLabel")).show();
	
	$("#formPrimaryDateType")
		.autoselector("option", "data", transformFilterType(reportForm.primaryDateFilters))
		.autoselector("refreshValue");
	$("#formSecondaryDateType")
		.autoselector("option", "data", transformFilterType(reportForm.secondaryDateFilters))
		.autoselector("refreshValue");
	
	displayCustomerReportDetails(type, reportForm);
	
	$("#formLocationType")
		.autoselector("option", "data", transformFilterType(reportForm.locationTypeFilters))
		.autoselector("refreshValue");
	
	if(!reportForm.organizationTree) {
		$("#filterOrganization").hide();
	}
	else {
		$("#filterLocation").hide();
		// Organization Tree
		var $tree = $("#reportOrgTree");
		
		$tree.treepicker("clear");
		$tree.treepicker("add", null, reportForm.organizationTree.children);
		
		var currentSelection = $("#reportOrgTreeVal").val().trim();
		if(currentSelection.length > 0) {
			$tree
				.treepicker("selectByValues", currentSelection.split(","))
				.treepicker("collapse", null, { "selectedList": true });
		}
	}
	
	$("#formSpaceNumberType")
		.autoselector("option", "data", transformFilterType(reportForm.spaceNumberFilters))
		.autoselector("refreshValue");
	$("#formTicketNumberType")
		.autoselector("option", "data", transformFilterType(reportForm.ticketNumberFilters))
		.autoselector("refreshValue");
	$("#formCouponNumberType")
		.autoselector("option", "data", transformFilterType(reportForm.couponNumberFilters))
		.autoselector("refreshValue");
	$("#formCouponType")
		.autoselector("option", "data", transformFilterType(reportForm.couponTypeFilters))
		.autoselector("refreshValue");
	
	$("#formCardNumberType")
		.autoselector("option", "data", transformFilterType(reportForm.cardNumberFilters))
		.autoselector("refreshValue");
	
	$("#formApprovalStatus")
		.autoselector("option", "data", transformFilterType(reportForm.approvalStatusFilters))
		.autoselector("refreshValue");
	
	if(reportForm.severityTypeListFilters) {
		$("#formAlertSeverity")
			.autoselector("option", "data", reportForm.severityTypeListFilters)
			.autoselector("refreshValue");
	}
	
	if(reportForm.deviceTypeListFilters) {
		$("#formAlertDevice")
			.autoselector("option", "data", reportForm.deviceTypeListFilters)
			.autoselector("refreshValue");
	}
	
	$("#formOtherParameters")
		.autoselector("option", "data", transformFilterType(reportForm.otherParameterFilters))
		.autoselector("refreshValue");
	$("#formGroupBy")
		.autoselector("option", "data", transformFilterType(reportForm.groupByFilters))
		.autoselector("refreshValue");
	$("#formSortBy")
		.autoselector("option", "data", transformFilterType(reportForm.sortByFilters))
		.autoselector("refreshValue");
	
	/* Re-Set the radio to make sure the state is sync for defaultValue and other visibilities that update through these radios' events */
	var $isSummary = $("#isSummary");
	setBooleanRadioValue("isSummary", $isSummary.val());
	updateOutputFormatVisibility($isSummary.val().toLowerCase() === "true");
	
	setBooleanRadioValue("isPDF", $("#isPDF").val());
	
	var $showHiddenPayStations = $("#showHiddenPayStations");
	setBooleanRadioValue("showHiddenPayStations", $showHiddenPayStations.val());
	
	$step2Loader.fadeOut(function() {
		if($("#reportDefinitionId").val().length <= 0) {
			$("#btnCancelReportTypeSpan").fadeIn();
		}
		
		$step2.removeClass("disableOverlay");
		$reportDtlForm.slideDown({
			"duration": 1700,
			"complete": function() {
//				displayButtons();
			}
		});
	});
}

function displayCustomerReportDetails(type, reportForm) {
	var payStationInfo = {
		"category": [{
			"label": reportLabel.allPayStations,
			"value": 0
		}],
		"nodes": [],
		"nodesHash": {},
		"orgNamesHash": {}
	};
	
	// Location Tree
	var $tree = $("#reportPOSTree");
	$tree.treepicker("clear");
	
	var $locTree = $tree;
	
	if(reportForm.locationPOSTree) {
		$tree.treepicker("add", null, reportForm.locationPOSTree.children, { "deferRender": true });
		
		$tree.treepicker("travelBFS", null, _childLocationExtractor, payStationInfo);
	}
	else if(reportForm.locationListFilters) {
		var locationTree = transformListToTree(reportForm.locationListFilters);
		if((locationTree) && (locationTree.children) && (locationTree.children.length > 0)) {
			$tree.treepicker("add", null, locationTree.children, { "deferRender": true });
		}
		
		$tree.treepicker("travelBFS", null, _childLocationExtractor, payStationInfo);
	}
	
	var currentSelection = $("#reportLocations").val().trim();
	if(currentSelection.length > 0) {
		currentSelection = currentSelection.split(",");
	}
	
	if(currentSelection.length > 0) {
		$tree
			.treepicker("selectByValues", currentSelection, { "deferRender": true });
	}
	
	// Route Tree
	$tree = $("#reportRoute");
	$tree.treepicker("clear");
	if(reportForm.routePOSTree) {
		$tree.treepicker("add", null, reportForm.routePOSTree.children, { "deferRender": true });
		$tree.treepicker("travelBFS", null, _nonLeafExtractor, payStationInfo);
	}
	
	$("#formPaystationsFilter")
		.autoselector("option", "data", payStationInfo.category)
		.autoselector("refreshValue");
	
	if(currentSelection.length > 0) {
		$tree.treepicker("selectByValues", currentSelection, { "deferRender": true });
	}
	
	// Pay Station Tree
	$tree = $("#reportPayStations");
	$tree.treepicker("clear");
	payStationInfo.nodes.sort(function(ps1, ps2) {
		var result = -1;
		var name1 = ((typeof ps1.name === "undefined") || (ps1.name === null)) ? "" : ps1.name.toLowerCase();
		var name2 = ((typeof ps2.name === "undefined") || (ps2.name === null)) ? "" : ps2.name.toLowerCase();
		if(name1 === name2) {
			result = 0;
		}
		else if(name1 > name2) {
			result = 1;
		}
		
		return result;
	});
	
	$tree.treepicker("add", null, payStationInfo.nodes, { "deferRender": true });
	
	if(currentSelection.length > 0) {
		$tree.treepicker("selectByValues", currentSelection, { "deferRender": true });
	}
	
	$("#spaceLocation")
		.autoselector("option", "data", retrieveLocationFilters($locTree), "text", "randomId")
		.autoselector("refreshValue");
	
	$("#formCardType")
		.autoselector("option", "data", transformFilterType(reportForm.cardTypeListFilters, "label", "value"))
		.autoselector("refreshValue");
	
	$("#merchantAccount")
		.autoselector("option", "data", transformFilterType(reportForm.merchantAccountListFilters, "label", "value"))
		.autoselector("refreshValue");
}

function resetSteps(event) {
	event.preventDefault();
	
	confirmDialog({
		"title": attentionTitle,
		"message": cancelConfirmMsg,
		"okLabel": cancelCnfrmBtn,
		"okCallback": function() {
			document.location.reload();
		},
		"cancelLabel": cancelGoBackBtn
	});
	
	return false;
}

function displaySchedulingForm() {
	$("#btnStep3Select").hide();
	
	var $step3 = $("#step3");
	var $step3Loader = $step3.find(".dashBoardLoader").fadeIn();
	var $scheduleForm = $step3.find("#reportSchedulingArea");
	
	$step3Loader.fadeOut(function() {
		$step3.removeClass("disableOverlay");
		$scheduleForm.slideDown({
			"duration": "slow",
			"complete": function() {
				displayButtons();
			}
		});
	});
}

function displayButtons() {
	$("#reportButtonsSection .save").removeClass("disabled");
}

function assignHTMLContents(fieldsHash) {
	if(fieldsHash) {
		for(var fieldSelector in fieldsHash) {
			$(fieldSelector).html(fieldsHash[fieldSelector]);
		}
	}
}

function applyInputConfigs(configs) {
	if(configs) {
		var conf, $input, oldType;
		for(var inputSelector in configs) {
			conf = configs[inputSelector];
			$input = $(inputSelector);
			if(conf.type) {
				oldType = $input.data("currConfType");
				if(oldType) {
					$input.removeClass(oldType);
				}
				
				$input.data("currConfType", conf.type);
				$input.addClass(conf.type);
			}
			
			if(conf.maxlength) {
				$input.attr("maxlength", conf.maxlength);
				$input.css("width", (conf.maxlength * 8) + "px");
			}
			
			if(conf.defaultValue && ($("#reportDefinitionId").val().length <= 0)) {
				$input.val(conf.defaultValue);
			}
		}
	}
}

function selectFilter($input, fullList, value) {
	if((typeof value !== "undefined") && (value != null)) {
		var label = null;
		var filter, idx = -1, len = fullList.length;
		while((label === null) && (++idx < len)) {
			filter = fullList[idx];
			if(filter.value == value) {
				label = filter.text;
			}
		}
		
		if(label !== null) {
			$input.autoselector("select", label, value);
		}
	}
}

function retrieveLabel(list, value) {
	var result = null;
	var filter, idx = -1, len = list.length;
	while((result === null) && (++idx < len)) {
		filter = list[idx];
		if(filter.value == value) {
			result = filter.text;
		}
	}
	
	return result;
}

function persitReportFormValues() {
	var $reportOrgTree = $("#reportOrgTreeVal");
	$reportOrgTree.val($("#reportOrgTree").treepicker("getSelectedValues").join(","));
	
	var $selectedLocations = $("#reportLocations");
	var $locationType = $("#formLocationType");
	if($locationType.is(":visible")) {
		var allTreeIds = [ "#reportPOSTree", "#reportRoute", "#reportPayStations" ];
		var $tree, idx = allTreeIds.length;
		while(--idx >= 0) {
			$tree = $(allTreeIds[idx]);
			if($tree.is(":visible")) {
				$selectedLocations.val($tree.treepicker("getSelectedValues").join(","));
			}
		}
	}
	else if($reportOrgTree.val().trim().length > 0) {
		$selectedLocations.val("0");
	}
	
	var daySelectorType = $("#formRepeatType").autoselector("getSelectedValue");
	if(isDayOfWeekType(daySelectorType)) {
		$("#formRepeatOn").val(retrieveArrayCheckboxVals("dow_"));
	}
	else if(isDayOfMonthType(daySelectorType)) {
		$("#formRepeatOn").val(retrieveArrayListVals("dom_"));
	}
}

function retrieveParams($form, commaSeparatedFieldNames) {
	var param, idx, len;
	
	var separatedByComma = {};
	if(commaSeparatedFieldNames && (Array.isArray(commaSeparatedFieldNames))) {
		idx = -1;
		len = commaSeparatedFieldNames.length;
		while(++idx < len) {
			separatedByComma[commaSeparatedFieldNames[idx]] = true;
		}
	}
	
	var reportType = parseInt($("#reportTypeID").autoselector("getSelectedValue"));
	var paramsArray = serializeForm($form, createIgnoredInputsHash(reportType), separatedByComma);
	return $.param(paramsArray);
}

function saveReportDefinition(event) {
	event.preventDefault();
	if(!$(this).hasClass("disabled")) {
		persitReportFormValues();
		
		var params = retrieveParams($("#reportForm"));
		var request = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		request.onreadystatechange = function() {
			if((request.readyState === 4) && (request.status === 200)) {
				if(request.responseText.substring(0, 4) === "true") {
					noteDialog(reportSaveSuccess, "timeOut", 2500);
					setTimeout(function() { document.location.href = LOC.viewReportList + "?" + scrubQueryString(document.location.search.substring(1), "all"); }, 500);
				}
			}
			else if(request.readyState === 4) {
				alertDialog(systemErrorMsg);
			}
		};
		
		request.open("POST", LOC.saveReportDefinition, true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.setRequestHeader("Content-length", params.length);
		request.setRequestHeader("Connection", "close");
		request.send(params);
	}
	
	return false;
}

function transformFilterType(filterTypesArr, labelAttrName, valAttrName, extraDataAttrName) {
	var result = [];
	
	if(typeof labelAttrName === "undefined") {
		labelAttrName = "text";
	}
	if(typeof valAttrName === "undefined") {
		valAttrName = "value";
	}
	if(typeof extraDataAttrName === "undefined") {
		extraDataAttrName = "extraData";
	}
	
	if(filterTypesArr) {
		var i = -1;
		var len = filterTypesArr.length;
		var filterObj;
		while(++i < len) {
			filterObj = filterTypesArr[i];
			result[i] = {
				"label": filterObj[labelAttrName],
				"value": filterObj[valAttrName],
				"extraData": (typeof filterObj[extraDataAttrName] === "undefined") ? null : filterObj[extraDataAttrName]
			};
		}
	}
	
	return result;
}

function transformListToTree(filterTypesArr) {
	var result = null;
	if(filterTypesArr && (filterTypesArr.length > 0)) {
		var childs = [];
		
		result = filterTypesArr[0];
		result.children = childs;
		
		var idx = 0, len = filterTypesArr.length;
		while(++idx < len) {
			childs.push(filterTypesArr[idx]);
		}
	}
	
	return result;
}

function retrieveLocationFilters($locationTree) {
	var result = [];
	$locationTree.treepicker("travelDFS", null, function(node, parentNode, breadcrumb, travelData) {
		if(node.isLocation && (!node.isParent)) {
			var filterNode = {
				"label": node.name,
				"value": node.randomId
			};
			
			if((parentNode) && (parentNode.isLocation)) {
				filterNode.desc = parentNode.name;
			}
			
			result.push(filterNode);
		}
	});
	
	return result;
}

function updateOrganizationSelection(event, node) {
	var reportType = $("#reportTypeID").autoselector("getSelectedValue");
	var visibilityHash = createBooleanHash(formDefinitions[reportType].visibles);
	
	var locationSectionId = "filterLocation";
	var parkerSectionId = "filterCustomer";
	var cardSectionId = "filterCard";
	
	var selectedVals = $("#reportOrgTree").treepicker("getSelectedValues");
	var selectedCount = selectedVals.length;
	if((selectedCount > 1) || (selectedCount <= 0) || ((selectedCount == 1) && (selectedVals[0] == "0"))) {
		if(visibilityHash[locationSectionId]) {
			$("#" + locationSectionId).hide().find("input[type=text]").val("");
			$("#formLocationType").autoselector("reset");
			$("#reportPOSTree").treepicker("select", null);
			$("#reportRoute").treepicker("select", null);
			$("#reportPayStations").treepicker("select", null);
		}
		
		if(visibilityHash[parkerSectionId]) {
			$("#" + parkerSectionId).hide().find("input[type=text]").val("");
			$("#formSpaceNumberType").autoselector("reset");
			$("#formTicketNumberType").autoselector("reset");
			$("#formCouponNumberType").autoselector("reset");
			$("#formCouponType").autoselector("reset");
		}
		
		if(visibilityHash[cardSectionId]) {
			$("#" + cardSectionId).hide().find("input[type=text]").val("");
			$("#formCardType").autoselector("reset");
			$("#formApprovalStatus").autoselector("reset");
			$("#merchantAccount").autoselector("reset");
			$("#formCardNumberType").autoselector("reset");
		}
	}
	else {
		prepareOrganizationFilter(reportType, selectedVals[0]);
		
		if(visibilityHash[locationSectionId]) {
			$("#" + locationSectionId).show();
		}
		
		if(visibilityHash[parkerSectionId]) {
			$("#" + parkerSectionId).show();
		}
		
		if(visibilityHash[cardSectionId]) {
			$("#" + cardSectionId).show();
		}
	}
	
	$("#formGroupBy").autoselector("updateData");
}

function updateOutputFormatVisibility(isSummary) {
	var reportTypeId = $("#reportTypeID").autoselector("getSelectedValue");
	if(reportTypeId) {
		var visibilityState = {};
		var formConf = formDefinitions[reportTypeId];
		if(formConf) {
			formConf = formConf.outputs;
		}
		
		if(formConf) {
			var visibleOutputFormats = (isSummary && (isSummary === true)) ? formConf.summary : formConf.details;
			var idx = visibleOutputFormats.length;
			while(--idx >= 0) {
				visibilityState[visibleOutputFormats[idx]] = true;
			}
			
			idx = allOutputFormats.length;
			var $elem, elemId, $lastVisibleElem = null, needSelection = false;
			while(--idx >= 0) {
				elemId = allOutputFormats[idx];
				$elem = $("#" + elemId);
				if(visibilityState[elemId] == true) {
					$elem.show();
					$lastVisibleElem = $elem;
				}
				else {
					$elem.hide();
					needSelection = true;
				}
				
				if(needSelection && ($lastVisibleElem !== null)) {
					setBooleanRadioValueByCheckedId("isPDF", $lastVisibleElem.find(".radio").attr("id"));
					needSelection = false;
				}
			}
		}
	}
}

function filterHiddenPayStation(node, context) {
	return (!context.showAll) && (!node.isVisible);
}

function updateShowHiddenPayStations(showAll) {
	var $reportPOSTree = $("#reportPOSTree");
	var $reportRoute = $("#reportRoute");
	var $reportPayStations = $("#reportPayStations");
	
	$reportPOSTree.treepicker("disableNodes", filterHiddenPayStation, { "showAll": showAll, "deferRender": !$reportPOSTree.is(":visible") });
	$reportRoute.treepicker("disableNodes", filterHiddenPayStation, { "showAll": showAll, "deferRender": !$reportRoute.is(":visible") });
	$reportPayStations.treepicker("disableNodes", filterHiddenPayStation, { "showAll": showAll, "deferRender": !$reportPayStations.is(":visible") });
}

function updateDateInputsVisibility(group, dateType) {
	var $dateRange = $("#form" + group + "DateRange");
	var $monthRange = $("#form" + group + "MonthRange");
	
	var $runNowOption = $("#radioRunNowReport");
	var $scheduleOption = $("#radioScheduleReport");
	if(dateType == "") {
		// DO NOTHING.
	}
	else if(dateType == 100) {
		$dateRange.show();
		if($dateRange.find(":input").val().length > 0) {
			$dateRange.find("label").hide();
		}
//		$dateRange.find(":input").removeAttr("disabled");
		
		$monthRange.hide();
		$monthRange.find(":input").val("");
		$monthRange.find("label").show();
//		$monthRange.find(":input").attr("disabled", "disabled");
		
		$runNowOption.trigger("click");
		$scheduleOption.hide();
	}
	else if(dateType == 101) {
		$dateRange.hide();
		$dateRange.find(":input").val("");
		$dateRange.find("label").show();
//		$dateRange.find(":input").attr("disabled", "disabled");
		
		$monthRange.show();
		if($monthRange.find(":input").val().length > 0) {
			$monthRange.find("label").hide();
		}
//		$monthRange.find(":input").removeAttr("disabled");
		
		$runNowOption.trigger("click");
		$scheduleOption.hide();
	}
	else {
		$dateRange.hide();
		$dateRange.find(":input").val("");
		$dateRange.find("label").show();
//		$dateRange.find(":input").attr("disabled", "disabled");
		
		$monthRange.hide();
		$monthRange.find(":input").val("");
		$monthRange.find("label").show();
//		$monthRange.find(":input").attr("disabled", "disabled");
		
		$scheduleOption.show();
	}
}

function updateSpaceNumberVisibility(spaceNumberType) {
	var $numBegin = $("#formSpaceNumberBegin");
	var $numRange = $("#formSpaceNumberRange");
	if((spaceNumberType >= 403) && (spaceNumberType <= 406)) {
		$numBegin.show();
//		$numBegin.removeAttr("disabled");
		
		if(spaceNumberType == 406) {
			$numRange.show();
//			$numRange.find(":input").removeAttr("disabled");
		}
		else {
			$numRange.hide();
			$numRange.find(":input").val("");
//			$numRange.find(":input").attr("disabled", "disabled");
		}
	}
	else {
		$numBegin.hide();
		$numBegin.val("");
//		$numBegin.attr("disabled", "disabled");
		
		$numRange.hide();
		$numRange.find(":input").val("");
//		$numRange.find(":input").attr("disabled", "disabled");
	}
}

function updatePayStationsVisibility(type) {
	var $locations = $("#formPayStationLocations");
	var $locTree = $("#reportPOSTree");
	
	var $routes = $("#formPayStationRoutes");
	var $routeTree = $("#reportRoute");
	
	var $payStations = $("#formPayStations");
	var $psTree = $("#reportPayStations");
	
	var deferRenderConf = { "deferRender": true };
	if(type == 300) {
		// Location Tree
		$locTree.treepicker("redraw");
		$locations.show();
		
		$routes.hide();
		$routeTree.treepicker("resetSelection", deferRenderConf);
		
		$payStations.hide();
		$psTree.treepicker("resetSelection", deferRenderConf);
	}
	else if(type == 301) {
		// Route Tree
		$locations.hide();
		$locTree.treepicker("resetSelection", deferRenderConf);
		
		$routeTree.treepicker("redraw");
		$routes.show();
		
		$payStations.hide();
		$psTree.treepicker("resetSelection", deferRenderConf);
	}
	else if(type == 302) {
		// PS Tree
		$locations.hide();
		$locTree.treepicker("resetSelection", deferRenderConf);
		
		$routes.hide();
		$routeTree.treepicker("resetSelection", deferRenderConf);
		
		$psTree.treepicker("redraw");
		$payStations.show();
	}
	else {
		// NOTHING
		$locations.hide();
		$locTree.treepicker("resetSelection", deferRenderConf);
		
		$routes.hide();
		$routeTree.treepicker("resetSelection", deferRenderConf);
		
		$payStations.hide();
		$psTree.treepicker("resetSelection", deferRenderConf);
	}
	
	try {
		$("#formGroupBy").autoselector("updateData");
	}
	catch(error) {
		// DO NOTHING.
	}
}

function updateTicketNumberVisibility(type) {
	var $ticketNumberBegin = $("#formTicketNumberBegin");
	var $ticketNumberRange = $("#formTicketNumberRange");
	if((type == 701) || (type == 801) || (type == 802) || (type == 803)) {
		$ticketNumberBegin.show();
//		$ticketNumberBegin.removeAttr("disabled");
		
		$ticketNumberRange.hide();
		$ticketNumberRange.find(":input").val("");
//		$ticketNumberRange.find(":input").attr("disabled", "disabled");
	}
	else if((type == 702) || (type == 804)) {
		$ticketNumberBegin.show();
//		$ticketNumberBegin.removeAttr("disabled");
		
		$ticketNumberRange.show();
//		$ticketNumberRange.find(":input").removeAttr("disabled");
	}
	else {
		$ticketNumberBegin.hide();
		$ticketNumberBegin.val("");
//		$ticketNumberBegin.attr("disabled", "disabled");
		
		$ticketNumberRange.hide();
		$ticketNumberRange.find(":input").val("");
//		$ticketNumberRange.find(":input").attr("disabled", "disabled");
	}
}

function updateCouponNumberVisibility(type) {
	var $couponNumber = $("#formCouponNumber");
	if(type == 903) {
		$couponNumber.show();
//		$couponNumber.removeAttr("disabled");
	}
	else {
		$couponNumber.hide();
		$couponNumber.val("");
//		$couponNumber.attr("disabled", "disabled");
	}
}

function updateCardNumberVisibility(type) {
	var $cardNumber = $("#formCardNumberSpecific");
	if(type == 501) {
		$cardNumber.show();
//		$cardNumber.removeAttr("disabled");
	}
	else {
		$cardNumber.hide();
		$cardNumber.val("");
//		$cardNumber.attr("disabled", "disabled");
	}
}

function isDayOfMonthType(type) {
	return type == 4;
}

function isDayOfWeekType(type) {
	return type == 3;
}

function updateDaySelectorVisibility(type) {
	var $daySelector = $("#daySelector");
	var $dayOfWeekSelector = $("#dayOfWeekSelector");
	var $dayOfMonthSelector = $("#dayOfMonthSelector");
	var $repeatFrequencyUnit = $("#formRepeatFrequencyUnit");
	if(isDayOfWeekType(type)) {
		$dayOfWeekSelector.show();
		$dayOfMonthSelector.hide();
		resetArrayListVals("dom_");
		
		$daySelector.show();
		
		$repeatFrequencyUnit.html(reportLabel.frequencyWeekly);
	}
	else if(isDayOfMonthType(type)) {
		$dayOfWeekSelector.hide();
		resetArrayCheckboxVals("dow_");
		$dayOfMonthSelector.show();
		
		$daySelector.show();
		
		$repeatFrequencyUnit.html(reportLabel.frequencyMonthly);
	}
	else {
		resetArrayListVals("dom_");
		resetArrayCheckboxVals("dow_");
		
		$daySelector.hide();
		
		$repeatFrequencyUnit.html(reportLabel.frequencyDaily);
	}
}

function updateGroupByVisibility(isVisible) {
	var isAllowGroupBy = false;
	var isAlone = true;
	var formConf = formDefinitions[$("#reportTypeID").autoselector("getSelectedValue")];
	if(formConf) {
		var allVisibles = formConf.visibles;
		var i = -1, len = allVisibles.length;
		while(++i < len) {
			if("filterGroupBy" === allVisibles[i]) {
				isAllowGroupBy = true;
			}
			else if(otherParams[allVisibles[i]]) {
				isAlone = false;
			}
		}
	}
	
	if(isAllowGroupBy) {
		var $groupBy = $("#filterGroupBy");
		if(isVisible && ((isVisible === true) || (isVisible.toLowerCase() === "true"))) {
			$groupBy.show();
			$("#filterOther").show();
		}
		else {
			$groupBy.hide();
			if(isAlone) {
				$("#filterOther").hide();
			}
		}
	}
}

function createBooleanHash(elements) {
	var result = {};
	var idx = -1, len = elements.length;
	while(++idx < len) {
		result[elements[idx]] = true;
	}
	
	return result;
}

function showElements(allElementIds, visibleElementIds) {
	var visibleHash = createBooleanHash(visibleElementIds);
	var elemId, idx = -1, len = allElementIds.length;
	var elemSelector;
	while(++idx < len) {
		elemId = allElementIds[idx];
		elemSelector = "#" + elemId;
		if(visibleHash[elemId]) {
			$(elemSelector).show();
//			$(elemSelector + " :input").removeAttr("disabled");
		}
		else {
			$(elemSelector).hide();
//			$(elemSelector + " :input").attr("disabled", "disabled");
		}
	}
}

function retrieveArrayCheckboxVals(idPrefix) {
	var result = [];
	
	var prefixLen = idPrefix.length;
	var $checkboxes = $("a[id^=" + idPrefix + "]");
	var $checkboxObj, idx = -1, len = $checkboxes.length;
	while(++idx < len) {
		$checkboxObj = $($checkboxes[idx]);
		if($checkboxObj.hasClass("checked")) {
			result.push($checkboxObj.attr("id").substring(prefixLen));
		}
	}
	
	return result.join(",");
}

function retrieveArrayListVals(idPrefix) {
	var result = [];
	
	var prefixLen = idPrefix.length;
	var $checkboxes = $("li[id^=" + idPrefix + "]");
	var $checkboxObj, idx = -1, len = $checkboxes.length;
	while(++idx < len) {
		$checkboxObj = $($checkboxes[idx]);
		if($checkboxObj.hasClass("selected")) {
			result.push($checkboxObj.attr("id").substring(prefixLen));
		}
	}
	
	return result.join(",");
}

function resetArrayCheckboxVals(idPrefix) {
	$("a[id^=" + idPrefix + "]").removeClass("checked");
}

function resetArrayListVals(idPrefix) {
	$("li[id^=" + idPrefix + "]").removeClass("selcted");
}

function setArrayCheckboxVals(idPrefix, selectedValues) {
	if(selectedValues) {
		var selectedHash = {};
		if(!Array.isArray(selectedValues)) {
			selectedValues = selectedValues.split(",");
			if((selectedValues.length === 1) && (selectedValues[0].trim().length <= 0)) {
				selectedValues = [];
			}
		}
		
		var idx = -1, len = selectedValues.length;
		while(++idx < len) {
			selectedHash[idPrefix + selectedValues[idx].trim()] = true;
		}
		
		var $checkboxes = $("a[id^=" + idPrefix + "]");
		var $checkboxObj, attrId;
		idx = -1, len = $checkboxes.length;
		while(++idx < len) {
			$checkboxObj = $($checkboxes[idx]);
			attrId = $checkboxObj.attr("id");
			if(selectedHash[attrId] === true) {
				$checkboxObj.addClass("checked");
			}
			else {
				$checkboxObj.removeClass("checked");
			}
		}
	}
}

function setArrayListVals(idPrefix, selectedValues) {
	if(selectedValues) {
		var selectedHash = {};
		if(!Array.isArray(selectedValues)) {
			selectedValues = selectedValues.split(",");
			if((selectedValues.length === 1) && (selectedValues[0].trim().length <= 0)) {
				selectedValues = [];
			}
		}
		
		var idx = -1, len = selectedValues.length;
		while(++idx < len) {
			selectedHash[idPrefix + selectedValues[idx].trim()] = true;
		}
		
		var $checkboxes = $("li[id^=" + idPrefix + "]");
		var $checkboxObj, attrId;
		idx = -1, len = $checkboxes.length;
		while(++idx < len) {
			$checkboxObj = $($checkboxes[idx]);
			attrId = $checkboxObj.attr("id");
			if(selectedHash[attrId] === true) {
				$checkboxObj.addClass("selected");
			}
			else {
				$checkboxObj.removeClass("selected");
			}
		}
	}
}

function addEmail($containerObj, $selectedObj, $newObj) {
	if(addEmailString($containerObj, $selectedObj, $newObj.val())) {
		$newObj.val("");
	}
}

function addEmailString($containerObj, $selectedObj, emailStr) {
	var addr, addrKey, idx, len;
	var newEmailsList = emailStr.split(",");
	if((newEmailsList.length === 1) && (newEmailsList[0].trim().length <= 0)) {
		newEmailsList = [];
	}
	
	var valid = true;
	idx = newEmailsList.length;
	while(valid && (--idx >= 0)) {
		newEmailsList[idx] = newEmailsList[idx].trim();
		valid = characterTypes.emailString.test(newEmailsList[idx]);
	}
	
	if(valid) {
		var currEmailsList = $selectedObj.val().split(",");
		if((currEmailsList.length === 1) && (currEmailsList[0].trim().length <= 0)) {
			currEmailsList = [];
		}
		
		var currEmailsHash = {};
		idx = -1;
		len = currEmailsList.length;
		while(++idx < len) {
			addr = currEmailsList[idx].trim();
			currEmailsHash[addr.toLowerCase()] = addr;
		}
		
		var addedEmailsList = [];
		
		idx = -1;
		len = newEmailsList.length;
		while(++idx < len) {
			addr = newEmailsList[idx];
			if(addr) {
				addrKey = addr.toLowerCase();
				if(typeof currEmailsHash[addrKey] === "undefined") {
					currEmailsHash[addrKey] = addr;
					addedEmailsList.push(addr);
					
					var addedContactLIs = $containerObj.find("li:visible");
					var contactIdx = addedContactLIs.length, drawn = false;
					
					var $currLI, currAddr;
					var $newContactLI = $("<li class='selected' title='Click to remove'><a href='#' class='checkBox'>&nbsp;</a> <span class='contactEmail'>" + addr + "</span></li>");
					while((!drawn) && (--contactIdx >= 0)) {
						$currLI = $(addedContactLIs[contactIdx]);
						currAddr = $currLI.find(".contactEmail").text();
						if(addr > currAddr) {
							$newContactLI.insertAfter($currLI);
							drawn = true;
						}
					}
					
					if(!drawn) {
						$containerObj.prepend($newContactLI);
					}
				}
			}
		}
		
		if(newEmailsList.length > 0) {
			if(currEmailsList.length > 0) {
				$selectedObj.val($selectedObj.val() + "," + addedEmailsList.join(","));
			}
			else {
				$selectedObj.val(addedEmailsList.join(","));
			}
			
			var $noContact = $containerObj.find("#noContact");
			$noContact.hide();
		}
	}
	
	return valid;
}

function toggleEmailSelection($containerObj, $selectedObj, $toggleObj) {
	var targetAddrOri = $toggleObj.find("span.contactEmail").text();
	if(targetAddrOri) {
		var currEmailsList = $selectedObj.val().split(",");
		if((currEmailsList.length === 1) && (currEmailsList[0].trim().length <= 0)) {
			currEmailsList = [];
		}
		
		if(!$toggleObj.hasClass("selected")) {
			$toggleObj.removeClass("unselected").addClass("selected");
			currEmailsList.push(targetAddrOri);
		}
		else {
			$toggleObj.removeClass("selected").addClass("unselected");
			if(currEmailsList.length > 0) {
				var targetAddr = targetAddrOri.toLowerCase();
				var idx = -1, len = currEmailsList.length;
				while(++idx < len) {
					if(currEmailsList[idx].toLowerCase() === targetAddr) {
						currEmailsList.splice(idx, 1);
						break;
					}
				}
			}
		}
		
		$selectedObj.val(currEmailsList.join(","));
	}
}

function setBooleanRadioValueByCheckedId(id, checkedId) {
	setBooleanRadioValue(id, (checkedId.indexOf("true") >= 0) ? "true" : "false");
}

function setBooleanRadioValue(id, value) {
	var selector = "#" + id;
	if((value === true) || (value.toLowerCase() === "true")) {
		$(selector).val("true");
		$(selector + "\\:true").addClass("checked").trigger("click");
		$(selector + "\\:false").removeClass("checked");
	}
	else {
		$(selector).val("false");
		$(selector + "\\:true").removeClass("checked");
		$(selector + "\\:false").addClass("checked").trigger("click");
	}
}

function cancelReportDefinitionUpdates(event) {
	event.preventDefault();
	
	confirmDialog({
		"title": attentionTitle,
		"message": cancelConfirmMsg,
		"okLabel": cancelCnfrmBtn,
		"okCallback": function() {
			document.location.href = LOC.viewReportList + "?" + scrubQueryString(document.location.search.substring(1), "all");
		},
		"cancelLabel": cancelGoBackBtn
	});
	
	return false;
}

function setupCalendarAction() {
	$("body").on("click", "ul.dayCal li:not(.filter)", function(event) {
		event.preventDefault();
		var $elem = $(this);
		if($elem.hasClass("selected")) {
			$elem.removeClass("selected");
		}
		else {
			$elem.addClass("selected");
		}
		
		return false;
	});
}

function _nonLeafExtractor(node, parentNode, breadcrumb, travelData) {
	if(node.isRoute) {
		var parentStr = "";
		if(parentNode && parentNode.isOrganization) {
			parentStr = " (" + parentNode.name + ")";
		}
		
		travelData.category.push({
			"label": node.name,
			"value": node.randomId,
			"category": "Route" + parentStr
		});
	}
	else if(node.isPayStation) {
		var nodesHash = travelData.nodesHash;
		var psNode = nodesHash[node.randomId];
		if(typeof psNode === "undefined") {
			nodesHash[node.randomId] = psNode = $.extend({}, node);
			delete psNode.parent;
			delete psNode._nodeId;
			delete psNode._siblingOrder;
			delete psNode.isSelected;
			delete psNode.isPartialSelected;
			
			travelData.nodes.push(psNode);
		}
		
		if(parentNode) {
			if(!psNode.routeId) {
				psNode.routeId = {};
			}
			
			psNode.routeId[parentNode.randomId] = true;
		}
	}
}

function _childLocationExtractor(node, parentNode, breadcrumb, travelData) {
	if(node.isLocation) {
		var onh = travelData.orgNamesHash;
		var parentStr = "";
		if(parentNode) {
			if(parentNode.isOrganization) {
				parentStr = " (" + parentNode.name + ")";
			}
			else {
				parentStr = onh[parentNode.randomId];
			}
		}
		
		onh[node.randomId] = parentStr;
		
		if(!node.isParent) {
			travelData.category.push({
				"label": node.name,
				"value": node.randomId,
				"desc": (parentNode && parentNode.isLocation) ? parentNode.name : null,
				"category": "Location" + parentStr
			});
		}
	}
	else if(node.isPayStation) {
		var nodesHash = travelData.nodesHash;
		var psNode = nodesHash[node.randomId];
		if(typeof psNode === "undefined") {
			nodesHash[node.randomId] = psNode = $.extend({}, node);
			delete psNode.parent;
			delete psNode._nodeId;
			delete psNode._siblingOrder;
			delete psNode.isSelected;
			delete psNode.isPartialSelected;
			
			travelData.nodes.push(psNode);
		}
		
		psNode.locationId = (parentNode) ? parentNode.randomId : null;
	}
}

function _updatePayStationPickerData(event, itemSelected) {
	var randomId = itemSelected.value;
	
	var $reportPS = $("#reportPayStations");
	var $selAllBtn = $("#reportPayStationsSelAllBtn");
	if(randomId == 0) {
		$selAllBtn.fadeIn();
		$reportPS.treepicker("filter", function() { return true; });
	}
	else {
		$selAllBtn.fadeOut();
		$reportPS.treepicker("filter", function(node) {
			var shouldDisplay = (randomId == node.locationId);
			if(!shouldDisplay) {
				shouldDisplay = (node.routeId) && (node.routeId[randomId] === true);
			}
			return shouldDisplay;
		});
	}
}


/* TRANSACTION LOOKUP */

function showTransaction(transactionData, step)
{
	$(".second, .third").attr("style","");
	if(transactionData === "clear"){
		$(".sectionContent").removeClass("layout3").addClass("layout1")
		$("#transReceiptListContainer > span").html('<li class="listMessage notclickable">'+noPermitsFound+'</li>'); }
	else {		
		var trnData = JSON.parse(transactionData);
		trnData = trnData.transactionReceiptDetails;
		
		var activeNote = ($(".selectedActive").length)? "<span class='active'>("+activeLabel+")</span>": "";
		var amountLabel = '<span class="label">'+amountTitle+'</span>';
		var expireLabel = '<span class="label">'+expiresTitle+'</span>';
		
		$("#receiptID").val(trnData.purchaseRandomId);
		
		$("#transactionDetails").find("span").show();
		$("#receiptHistoryTitle,#receiptHistoryList,#rcpt_details,#yurRcpt,#emailReceiptForm").show();
		
		$("#psName_Value,#rcpt_pos,#psSN_Value,#locName_Title,#locName_Value,#rcpt_location,#purchaseDate_Value,#rcpt_purchaseTime,#rcpt_purchaseDate,#rcpt_expires,#amountDue_Value,#rcpt_due,#adTime_Value,#plate_Value,#rcpt_license,#transaction_Value,#rcpt_transNumber,#payType_Value,#cardNumber_Value,#rcpt_paymentInfo,#rcpt_auth,#authNumber_Value,#rcpt_cardType,#rate_Value,#rcpt_rate,#cvm_Value,#rcpt_cvm,#apl_Value,#rcpt_apl,#aid_Value,#rcpt_aid,#rcpt_parkingRate,#totalPaid_Value,#rcpt_taxDisplay,#amountPaid_Value,#rcpt_paid,#coupon_Value,#rcpt_coupon,#change_Value,#rcpt_changeIssued,#rcpt_refundSlip,#psSN_Value,#rcpt_serialNumber,#receiptHistoryList,#rcpt_facilityName,#permitNumber_Value").html("");
		
		$("#psName_Value").html(trnData.payStationName);
		$("#rcpt_pos").html(trnData.payStationName);
		$("#locName_Title").html("Location Name");
		$("#locName_Value").html(trnData.locationName);
		$("#rcpt_location").html(trnData.locationName);
		$("#purchaseDate_Value").html(trnData.purchasedDate +" "+ trnData.purchasedTime);
		$("#rcpt_purchaseTime").html(trnData.purchasedTime);
		$("#rcpt_purchaseDate").html(trnData.purchasedDate);
		if(trnData.transactionType === "Cancelled" || trnData.transactionType === "Test"){
			if(trnData.transactionType === "Test"){ $("#purchaseDate_Value").append(" <strong class='blueHighlight'>("+ testMsg +")</strong>"); }
			else{ $("#purchaseDate_Value").append(" <strong class='highlight'>("+ cancelledMsg +")</strong>"); }
			$(".column.third .groupBox, #receiptHistoryTitle, #receiptHistoryList, #expiryDate_Value, #expiryDate_Title").hide(); }
		else{
			$("#expiryDate_Value").html(trnData.expiryDate +" "+ trnData.expiryTime +" "+ activeNote);
			var expiryDate = '<span class="dateLine">'+trnData.expiryDate+'</span>';
			$(".column.third .groupBox, #receiptHistoryTitle, #receiptHistoryList, #expiryDate_Value, #expiryDate_Title").show(); }
		$("#rcpt_expires").html(expireLabel+expiryDate+" "+trnData.expiryTime);
		$("#amountDue_Value").html("$"+Highcharts.numberFormat(trnData.chargedAmount, 2));
		$("#rcpt_due").html(totalDueLabel+": $"+Highcharts.numberFormat(trnData.chargedAmount, 2));
//		$("#rcpt_amount").html(amountLabel+"$"+Highcharts.numberFormat(trnData.chargedAmount, 2));
		
		if(typeof trnData.addTimeNumber === "undefined" || trnData.addTimeNumber === 0){ 
			$("#addTimeLine, #adTime_Line").hide(); }
		else{ 
			var addTimeIndctr = (trnData.transactionType === "AddTime")? "+ " : "" ;
			var addTimeSection = $("<section id='addTimeLine'>").html(addTimeIndctr + addTimeNmbrMsg + " : " + trnData.addTimeNumber);
			$("#rcpt_expires").append(addTimeSection);
			$("#adTime_Value").html(trnData.addTimeNumber);
			$("#addTimeLine, #adTime_Line").show(); }
		
		if(typeof trnData.licensePlateNumber === "undefined" || trnData.licensePlateNumber === "N/A"){ 
			$("#plateNumber_Line, #rcpt_license").hide(); }
		else{ 
			$("#plateNumber_Line, #rcpt_license").show(); 
			$("#plate_Value").html(trnData.licensePlateNumber); 
			$("#rcpt_license").html('<span class="label">'+licenseTitle+'</span> ' + trnData.licensePlateNumber); }
		
		if(typeof trnData.stallNumber === "undefined" || trnData.stallNumber === 0){ 
			$("#spaceNumber_Line, #rcpt_space").hide(); }
		else{
			$("#spaceNumber_Line, #rcpt_space").show(); 
			$("#space_Value").html(trnData.stallNumber);
			$("#rcpt_space").html('<span class="label">'+spaceTitle+' #:</span>' + trnData.stallNumber); }
		
		$("#transaction_Value").html(trnData.transactionNumber);
		$("#rcpt_transNumber").html(trnData.transactionNumber);
		
		if(typeof trnData.isCreditCard === "undefined"){ 
			$("#authNumber_Line, #cardNumber_Line, #rcpt_cardType, #rcpt_auth").hide(); 
			$("#payType_Value").html(trnData.paymentType); 
			$("#cardNumber_Value").html("");
			$("#cardNumber_Line").hide();
			$("#rcpt_paymentInfo").html(paymentTypeLabel+": "+trnData.paymentType);
			$("#rcpt_auth").html('');
			 }
		else{
			var numLength = trnData.cardNumber.toString();
			var crdNumber = (numLength.length < 4)? " "+ trnData.cardNumber : " **** "+ trnData.cardNumber ;
			$("#rcpt_paymentInfo").html(paymentTypeLabel+": "+trnData.paymentType);
			$("#payType_Value").html(trnData.paymentType); 
			$("#authNumber_Line, #cardNumber_Line, #rcpt_cardType, #rcpt_auth").show(); 
			$("#cardNumber_Value").html(trnData.cardType+' '+crdNumber);
			if(typeof trnData.authorizationNumber === "undefined" || trnData.authorizationNumber === ""){
				$("#authNumber_Line, #rcpt_auth").hide(); }
			else{
				$("#authNumber_Value").html(trnData.authorizationNumber);
				$("#rcpt_auth").html(authorizationTitle +" #: "+ trnData.authorizationNumber); }
			$("#rcpt_cardType").html(trnData.cardType + crdNumber); }

		if((trnData.smartCardPaid && trnData.smartCardPaid !== 0) || (trnData.valueCardPaid && trnData.valueCardPaid !== 0)){ 
			$("#rcpt_paymentInfo").html(paymentTypeLabel+": "+trnData.paymentType);
			$("#payType_Value").html(trnData.paymentType); 
			$("#cardNumber_Line, #rcpt_cardType").show(); 
			var crdNumber = trnData.cardNumber;
			if(trnData.valueCardPaid){ 
				var numLength = trnData.cardNumber.toString();
				crdNumber = (numLength.length < 4)? " "+ trnData.cardNumber : " **** "+ trnData.cardNumber ;
				$("#rcpt_cardType").html(trnData.paymentType + crdNumber); }
			else{ $("#rcpt_cardType").html(trnData.paymentType +" "+ trnData.cardNumber); }
			
			$("#cardNumber_Value").html(crdNumber);
		}

		if(typeof trnData.rateName === "undefined"){ $("#rate_Line, #rcpt_rate").hide(); }
		else{
			$("#rate_Line, #rcpt_rate").show(); 
			$("#rate_Value").html(trnData.rateName);
			$("#rcpt_rate").html(rateLabel+ ": "+ trnData.rateName); }
		
		if(typeof trnData.cvm === "undefined"){ $("#rcpt_cvm").hide(); }
		else{
			$("#rcpt_cvm").show(); 
			$("#cvm_Value").html(trnData.cvm);
			$("#rcpt_cvm").html(cvmLabel+ ": "+ trnData.cvm); 
		}
		
		if(typeof trnData.apl === "undefined"){ $("#rcpt_apl").hide(); }
		else{
			$("#rcpt_apl").show(); 
			$("#apl_Value").html(trnData.apl);
			$("#rcpt_apl").html(aplLabel+ ": "+ trnData.apl); 
		}
		
		if(typeof trnData.aid === "undefined"){ $("#rcpt_aid").hide(); }
		else{
			$("#rcpt_aid").show(); 
			$("#aid_Value").html(trnData.aid);
			$("#rcpt_aid").html(aidLabel+ ": "+ trnData.aid); 
		}
		
		if(typeof trnData.totalParking !== "undefined" && trnData.totalParking !== ""){
			$("#rcpt_parkingRate").html(totalParkingLabel+": "+"$"+Highcharts.numberFormat(trnData.totalParking, 2));
			$("#totalPaid_Value").html("$"+Highcharts.numberFormat(trnData.totalParking, 2));
			$("#rcpt_parkingRate, #totalParking_Line").show(); }
		else{ $("#rcpt_parkingRate, #totalParking_Line").hide(); }
		
		if(typeof trnData.taxDetailList !== "undefined"){
			var detailList = trnData.taxDetailList;
			if(detailList instanceof Array === false){detailList = [detailList];}
			$("#rcpt_taxDisplay").html("");
			for(x=0; x < detailList.length; x++){
				var taxObj = detailList[x];
				var taxSection = $("<section>");
				taxSection.append(totalLabel+" "+taxObj.name+": $"+Highcharts.numberFormat(taxObj.amount, 2));
				if(taxObj.rate){
					taxSection.append(" ("+taxObj.rate+"%)");
				}
				$("#rcpt_taxDisplay").append(taxSection); }
			$("#rcpt_taxDisplay").show(); 
		}
		else{ $("#rcpt_taxDisplay").hide(); }
		
		$("#amountPaid_Value").html("$"+Highcharts.numberFormat(trnData.totalPaid, 2));
		$("#rcpt_paid").html(totalPaidLabel+": $"+Highcharts.numberFormat(trnData.totalPaid, 2));
		
		if((trnData.transactionType === "Cancelled") && (trnData.amountRefundedCard)) {
			$("#cancelled_Refund_Line").show();
			$("#amountRefund_Value").text("$"+Highcharts.numberFormat(trnData.amountRefundedCard, 2));
		}
		else {
			$("#cancelled_Refund_Line").hide();
		}
		
		if(typeof trnData.couponNumber === "undefined") {
			$("#coupon_Line").hide();
			$("#rcpt_coupon").hide();
		}
		else{
			$("#coupon_Line, #rcpt_coupon").show(); 
			var couponNumber = trnData.couponNumber +  ((!trnData.isCouponOffline) ? "" : " (" + couponOfflineLabel + ")");
			$("#coupon_Value").html(couponNumber);
			$("#rcpt_coupon").html(couponTitle + " #: "+ couponNumber);
		}
		
		if(typeof trnData.changeDispensed === "undefined" || trnData.changeDispensed === 0){ $("#change_Line, #rcpt_changeIssued").hide(); }
		else{
			$("#change_Line, #rcpt_changeIssued").show();
			$("#change_Value").html("$"+Highcharts.numberFormat(trnData.changeDispensed,2));
			$("#rcpt_changeIssued").html(changeIssuedLabel+": "+"$"+Highcharts.numberFormat(trnData.changeDispensed,2)); }

		if(typeof trnData.refundSlip === "undefined" || trnData.refundSlip === false){ $("#refund_Line, #refundSlip_Line, #rcpt_refundSlip").hide(); }
		else{
			$("#refund_Line, #refundSlip_Line, #rcpt_refundSlip").show();
			// For refund receipt, we do not no for sure how much. This is because the amount get setup by BOSS
			// $("#refund_Value").html("$"+Highcharts.numberFormat(trnData.changeDispensed,2));
			$("#rcpt_refundSlip").html(refundSlipMsg); }

		if(trnData.status === "Refund") {
			$("#rfndDate_Value").html(trnData.processingDate);
			var refundAuthNmbr = (typeof trnData.refundAuthorizationNumber === "undefined")? "N/A" : trnData.refundAuthorizationNumber;
			$("#rfndNumber_Value").html(refundAuthNmbr);
			$("#rfndAmount_Value").html("$"+Highcharts.numberFormat(trnData.amountRefundedCard,2));
			$("#refund_Line").show(); }
		else{ $("#refund_Line").hide(); }
		
		$("#psSN_Value").html(trnData.payStationSerial);
		$("#rcpt_serialNumber").html(snLabel+": "+ trnData.payStationSerial);
		
		//$("#rcpt_settings").html(settingLabel+": "+ trnData.paystationSetting);

		(typeof trnData.mobileNumber === "undefined")? $("#smsNumber_Line").hide() : $("#smsNumber_Line").show(); $("#smsNumber_Value").html(trnData.mobileNumber);
		
		var emailCount = 0;
		var emailHistory = trnData.emailAddressHistoryDetailList;
		$("#receiptHistoryList").html("");
		if(typeof emailHistory !== "undefined"){
			if(emailHistory instanceof Array === false){emailHistory = [emailHistory];}
			for(var x = 0; x < emailHistory.length; x++){
				if(emailHistory[x] !== ""){
					emailCount++;
					if(emailHistory[x].isEmailSent){ var emailSentDate = emailHistory[x].sentEmailDate;}else{ var emailSentDate = pendingEmailLbl;}
					$("#receiptHistoryList").append(
						"<li><span>"+emailSentDate+"</span><br><span>"+emailHistory[x].emailAddress+"</span>"+
						"<a href='#' id='"+trnData.randomId+"' email='"+emailHistory[x].emailAddress+"' tooltip='"+resendReceiptTitle+" "+emailHistory[x].emailAddress+"' class='email resend menuListButton'></a></li>"); } } } 
		if(emailCount === 0){ $("#receiptHistoryList").html("<li class='notclickable alignCenter'>"+noEmailSentMsg+"</li>"); }
	
	if(trnData.isRefundable && $("#refundBtnArea").length){ 
		if(trnData.isPANNeeded){ $("#refundBtn").attr("panneeded", true); }
		else{ $("#refundBtn").attr("panneeded", false); }
		$("#refundBtn").attr("ptrID", trnData.processorTransactionRandomId);
		$("#refundBtnArea").show(); }
	else if($("#refundBtnArea").length){ 
		$("#refundBtnArea").hide(); 
		$("#refundBtn").attr("ptrID", "null");}
	
	if(trnData.isFlex){
		$("#psName_Line,#psSN_Line,#amountDue_Line,#transaction_Line,#receiptHistoryTitle,#receiptHistoryList,#receiptDetails,#rcpt_details,#emailReceiptForm,#yurRcpt").hide();
		$("#locName_Title").html("Facility Name");
		$("#locName_Value,#rcpt_facilityName").html(trnData.facilityName);
		$("#prchsrName_Value").html(trnData.accountName);
		$("#amountPaid_Value").html("$"+Highcharts.numberFormat(trnData.originalAmount, 2));
		$("#permitNumber_Value,#rcpt_permitNumber").html(trnData.permitNumber);
		
		$("#rcpt_facility").show();
		$("#permitNumber_Line").show();
		$("#rcpt_flexPermit").show();
	}
	else{ $("#prchsrName_Line,#rcpt_facility,#permitNumber_Line,#rcpt_flexPermit").hide(); }

	if(step === "newDisplay"){
		$(".sectionContent").removeClass("layout1").addClass("layout3");
		snglSlideFadeTransition("show", $(".sectionContent"), soh(["receiptHistoryList"])); }
	else if(step === "changeDisplay" || step === "updateDisplay"){
		snglSlideFadeTransition("show", $(".second, .third"), soh(["receiptHistoryList"])); } } }
		

function loadTransaction(itemObj, step)
{
	if(step !== "updateDisplay"){
		var itemID = itemObj.attr("id").replace("tr_", "");
		itemObj.siblings(".selected").removeClass("selected");
		itemObj.siblings(".selectedActive").removeClass("selectedActive").addClass("active");
		if(itemObj.hasClass("active")){ itemObj.removeClass("active").addClass("selectedActive"); }
		itemObj.addClass("selected"); }
	else { itemID = itemObj; }
	
	var ajaxTranSearch = GetHttpObject();
	ajaxTranSearch.onreadystatechange = function() {
		if (ajaxTranSearch.readyState==4 && ajaxTranSearch.status == 200){
			showTransaction(ajaxTranSearch.responseText, step);
		} else if(ajaxTranSearch.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxTranSearch.open("GET","/secure/reporting/transactionReceipts/receiptDetails.html"+"?transactionId="+itemID+"&"+document.location.search.substring(1),true);
	ajaxTranSearch.send();
}

function clearTransaction(itemObj)
{
	itemObj.removeClass("selected");
	if(itemObj.hasClass("selectedActive")){ itemObj.removeClass("selectedActive").addClass("active"); }
	$(".sectionContent").removeClass("layout3").addClass("layout1");
	$(".second, .third").attr("style","");
}

function extractTransactions(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).transactionList;
	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

//--------------------------------------------------------------------------------------

function resetTXForm(){
	$("#searchID").val(""); 
	$("form#transactionSearchForm").trigger("reset"); 
	$("#searchLocation, #searchTxDateType").autoselector("reset");  }
				
//--------------------------------------------------------------------------------------

function selectedPermitAction($itemObj, thisAction){		
	var hideCallBack = '',
		tempDisplayMode = '';
	if(thisAction === "hide"){ 
		hideCallBack = function(){ clearTransaction($itemObj); }
		slideFadeTransition($(".sectionContent"), $(".sectionContent"), hideCallBack); }
	else{
		tempDisplayMode = ($itemObj.parents(".sectionContent").hasClass("layout1"))? "newDisplay" : "changeDisplay";
		hideCallBack = function(){ loadTransaction($itemObj, tempDisplayMode); };
		if(tempDisplayMode === "newDisplay"){ snglSlideFadeTransition("hide", $(".sectionContent"), hideCallBack); }
		else{ snglSlideFadeTransition("hide", $(".second, .third"), hideCallBack);}} }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popReceiptAction(tempItemID, tempSearchID, tempEvent){
	var tempFailMsg = false,
		hideCallBack = '';
	if(tempEvent === "search"){ 
		if(tempEvent === "search" && (tempSearchID !== null && tempSearchID == $("#searchID").val())){ 
			selectedPermitAction( $("#transReceiptListContainer").find("li.selected"), "hide"); }
		else{ tempFailMsg = true; } }
	else { 
		if(tempEvent === "display" && (tempSearchID !== null && tempSearchID == $("#searchID").val())){ 
			if($("#tr_"+tempItemID).length > 0){ 
				selectedPermitAction( $("#tr_"+tempItemID), "show"); }
			else{ tempFailMsg = true; }}
		else{ tempFailMsg = true; } } 
		
	if(tempFailMsg){ 
		hideCallBack = (tempEvent === "noSearch")? 
			function(){ resetTXForm(); showTransaction("clear"); } : 
			function(){ resetTXForm(); showTransaction("clear"); alertDialog(noPermitSearchData); } ;
		slideFadeTransition($(".sectionContent"), $(".sectionContent"), hideCallBack); }}
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function transactionReceiptSearch(pageAction)
{

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempItemID = '',
			tempSearchID = '',
			tempEvent = 'noSearch';
		if(stateObj !== null){
			tempItemID = stateObj.itemObject;
			tempSearchID = stateObj.timeStmp;
			tempEvent = stateObj.action; }
		popReceiptAction(tempItemID, tempSearchID, tempEvent); }
	
	if(!popStateEvent && (pageAction !== undefined && pageAction !== '')){ popReceiptAction(null, null, pageAction); }
///////////////////////////////////////////////////
	
	$(document.body).on("submit", "#emailReceiptForm", function(event){event.preventDefault(); $("#emailReceiptFormSubmit").trigger('click');  });
	
	$(document.body).on("click", ".email", function(event){
		event.stopImmediatePropagation();
		event.preventDefault();
		var mainList = false;
		if($(this).hasClass("send")){ $("#emailToSend").val($("#emailDisplay").val()); }
		else if($(this).hasClass("resend")){ 
			if($(this).parents("ul").attr("id") === "transReceiptListContainer"){ 
				$("#receiptID").val($(this).parents("li").attr("id").replace("tr_", ""));
				mainList = true; }
			$("#emailToSend").val($(this).attr("email")); }
		var params =  $.param(serializeForm("#emailReceiptForm"));
		var ajaxSendEmail = GetHttpObject();
		ajaxSendEmail.onreadystatechange = function()
		{
			if (ajaxSendEmail.readyState==4 && ajaxSendEmail.status == 200)
			{
				var response =  ajaxSendEmail.responseText.split(":");
				if(response[0] == "true")
				{
					$("#postToken").val(response[1]);
					$("#emailDisplay").val("");
					noteDialog(emailedReceiptMsg +" "+ $("#emailToSend").val()+".", "timeOut", 2500);
					var parentListItem = $("#transReceiptListContainer").find("li#tr_"+$("#receiptID").val());
					if(!parentListItem.find(".email").is(":visible")){ 
						parentListItem.find(".transactionEmail").append($("#emailToSend").val());
						parentListItem.find(".email").attr("email", $("#emailToSend").val()).removeClass("hide"); } 
					if(!mainList){ setTimeout(function(){ $(".second, .third").fadeOut(function(){ loadTransaction($("#receiptID").val(), "updateDisplay"); }); }, 500); }
				}else{
					var failData =	JSON.parse(ajaxSendEmail.responseText);
		//console.log( ajax.responseText);
					$("#postToken").val(failData.errorStatus.token); }} 
			else if(ajaxSendEmail.readyState==4){
				alertDialog(emailedFailMsg);
			}
		};
		ajaxSendEmail.open("POST","/secure/reporting/transactionReceipts/sendReceipt.html",true);
		ajaxSendEmail.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSendEmail.setRequestHeader("Content-length", params.length);
		ajaxSendEmail.setRequestHeader("Connection", "close");
		ajaxSendEmail.send(params); });

	$(document.body).on("click", "#transReceiptListSection > ul * li", function(){
		if(!$(this).hasClass("notclickable")){
			var $itemObj = $(this);
			if($itemObj.hasClass("selected")){
				var	searchStamp = $("#searchID").val(),
					stateObj = { itemObject: null, timeStmp: searchStamp, action: "search" };
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&searchID="+stateObj.timeStmp+"&pgActn="+stateObj.action);
				selectedPermitAction($itemObj, "hide"); }
			else{
				var	searchStamp = $("#searchID").val(),
					itemID = $itemObj.attr("id").replace("tr_", ""),
					stateObj = { itemObject: itemID, timeStmp: searchStamp, action: "display" };
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&searchID="+stateObj.timeStmp+"&pgActn="+stateObj.action);
				selectedPermitAction($itemObj, "show"); } } });
	
	
	$(document.body).on("click", ".print", function(event){
		$("body").addClass("printReceiptOnly");
		window.print();
		$("body").removeClass("printReceiptOnly");
		$(this).trigger('blur');
	});

	$("#searchTxDateType")
		.on("itemSelected", function(event, itemSelected) {
			var $start = $("#searchTxDateTimeStart");
			var $end = $("#searchTxDateTimeEnd");
			var $connector = $("#searchTxDateTimeConnector");
			if(itemSelected.displayStart) {
				$start.show();
			}
			else {
				$start.hide();
			}
			
			if(itemSelected.displayEnd) {
				$end.show();
			}
			else {
				$end.hide();
			}
			
			if(itemSelected.displayStart && itemSelected.displayEnd) {
				$connector.show();
			}
			else {
				$connector.hide();
			}
		})
		.autoselector({
			"isComboBox": true,
			"data": "#searchTxDateTypeData",
			"defaultValue": "TODAY"
		});
		
	$("#searchLocation")
		.autoselector({
			"isComboBox": true,
			"data": "#searchLocationData"
		});


	var today = moment().toDate();
	coupleDateComponents(
		createDatePicker("#searchTxDateStart", {
			"minDate": moment().subtract(2, "years").add(1, "days").startOf("day").toDate(),
			"maxDate": today,
			"onClose": function(selectedDate){ 
							var	selectedMoment = moment(selectedDate, "MM/DD/YYYY"),
								minEndDate = (selectedMoment.isBefore(today, "day"))? selectedMoment.add(1, "days").toDate() : today,
								maxEndDate = (selectedMoment.isBefore(moment(today).subtract(29, "days"), "day"))? selectedMoment.add(28, "days").toDate() : today;
							$("#searchTxDateEnd").datepicker("option", "minDate", minEndDate);
							$("#searchTxDateEnd").datepicker("option", "maxDate", maxEndDate); } }),
		$("#searchTxTimeStart").autominutes(),
		createDatePicker("#searchTxDateEnd", {
			"minDate": moment().subtract(2, "years").startOf("day").toDate(),
			"maxDate": today }),
		$("#searchTxTimeEnd").autominutes());
	
	$("#transactionSearchForm").on("click", ".search", function(event){ event.preventDefault();
		var	searchStamp = new Date().getTime(),
			stateObj = { itemObject: null, timeStmp: searchStamp, action: "search" },
			hideCallBack = function(){ showTransaction("clear"); },
			showCallBack = function(){ $("#transReceiptListContainer").paginatetab("reloadData"); };
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&searchID="+stateObj.timeStmp+"&pgActn="+stateObj.action);
		if($("#searchID").val() === ""){ showCallBack(); }
		else{ slideFadeTransition($(".sectionContent"), $(".sectionContent"), hideCallBack, showCallBack); }
		$("#searchID").val(searchStamp); });
	 
	 $("#transReceiptListContainer")
	 	.paginatetab({
			"url": "/secure/reporting/transactionReceipts/transactionSearch.html",
			"formSelector": "#transactionSearchForm",
			"postTokenSelector": "#searchPostToken",
			"rowTemplate": unescape($("#transReceiptListTemplate").html()),
			"responseExtractor": extractTransactions,
			"emptyMessage": noPermitsFound,
			"triggerSelector": "#transactionSearchForm > .search" });
}

//--------------------------------------------------------------------------------------

function accountAutoComplete(request, response) {
	var $searchTermInput = $("#accountFilterValue");
	
	var $postToken = $("#searchPostToken");
	var searchTerm = $searchTermInput.val();
	var request = GetHttpObject({
		"showLoader": false,
		"postTokenSelector": $postToken
	});
	
	request.onreadystatechange = function(responseFalse, displayedError) {
		if(request.readyState === 4) {
			var result = [];
			var accountList = extractAccountList(request.responseText);
			if(!accountList) {
				accountList = [];
			}
			else {
				var consumer, resultElem;
				var idx = -1, len = accountList.length;
				while(++idx < len) {
					consumer = accountList[idx];
					var fullname = consumer.firstName + " " + consumer.lastName;
					resultElem = {
						"label": fullname,
						"value": consumer.randomId,						
						"obj": consumer,
						"searchTerm": searchTerm
					};
					
					result.push(resultElem);
				}
			}
			
			response(result);
		}
	};
	
	request.open("POST", LOC.searchConsumers+"?"+document.location.search.substring(1), true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send("search=" + searchTerm);
	
}
