(function($, undefined) {
	$.widget("ui.uploadform", {
		"options": {
			"url": "#",
			"complete": function(responseText) { }
		},
		"_create": function() {
			if(!this.element.is("form")) {
				throw "This plugin only support <form> !";
			}
			
			var panelId = "_" + this.element.attr("id") + "_panel";
			this.submitFrame = $("<iframe id='" + panelId + "'>")
				.attr("src", "javascript:false;")
				.appendTo(document);
			
			this.element.attr("target", panelId);
			
			this.current = null;
			this.uploadQ = [];
		},
		"destroy": function() {
			this.submitFrame.remove();
			
			delete this.submitFrame;
			delete this.current;
			delete this.uploadQ;
			
			$.Widget.prototype.destroy.call(this);
		},
		"submit": function() {
			this.element.get(0).trigger('submit');
			
			if(this.current !== null) {
				this.uploadQ.push(this._createContext());
			}
			else {
				this._submit(this._createContext());
			}
		},
		"_submit": function(context) {
			this.current = context;
			this.element.get(0).trigger('submit');
		},
		"response": function(responseText) {
			if(this.current === null) {
				throw "Illegal State: There are nothing in submit queue !";
			}
			else {
				if(typeof this.current.complete === "function") {
					this.current.complete(responseText);
				}
				
				if(this.uploadQ.length > 0) {
					this._submit(this.uploadQ.shift());
				}
			}
		},
		"_createContext": function() {
			return {
				"url": this.options.url,
				"complete": this.options.complete
			};
		}
	});
}(jQuery));
