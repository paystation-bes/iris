(function($, undefined) {
	$.widget("ui.dptupload", {
		"options": {
			"postTokenSelector": "#postToken",
			"triggerSelector": null,
			"url": "",
			"timeout": 60000,
			"systemErrorMsg": (typeof systemErrorMsg !== "undefined") ? systemErrorMsg : "Could not upload data !",
			"messageObjParser": parseMessageInfo,
			"messageObjHandler": handleResponse,
			"successDelayMillisecs": 3500
		},
		"_create": function() {
			var id = "_uploadframe_" + (new Date()).getTime();
			this.uploading = false;
			
			var that = this;
			this.uploadFrame = $("<iframe id='" + id + "'>").attr("name", id)
					.hide()
					.appendTo("body")
					.on('load', function() {
						if(that.uploading) {
							var content = null;
							try {
								content = that.uploadFrame.contents().find("body");
							}
							catch(error) {
								// DO NOTHING.
							}
							
							if(!content) {
								try {
									content = $(that.uploadFrame[0].document.body.innerHTML);
								}
								catch(error) {
									// DO NOTHING.
								}
							}
							
							if(content) {
								var buffer = content.children("pre");
								if(buffer.length > 0) {
									content = buffer;
								}
								
								if(typeof content.innerHTML !== "undefined") {
									content = content.innerHTML;
								}
								else {
									content = content.html();
								}
							}
							
							that._translateResponse(content);
							that._reEnableTriggerObjects();
							that.uploading = false;
						}
					});
			
			this.element.attr("target", id);
			
			this.scramble = this.element.find("input[type=hidden][id=_scramble]");
			if(this.scramble.length <= 0) {
				this.scramble = $("<input id='_scramble' name='_scramble' type='hidden'>").appendTo(this.element);
			}
			
			return this;
		},
		"upload": function() {
			var conf = this.options;
			var that = this;
			var uploading = false;
			if(conf.triggerSelector) {
				this._$triggerObj = $(conf.triggerSelector);
				if(this._$triggerObj.hasClass("inactive")) {
					uploading = true;
				}
				else {
					this._$triggerObj.addClass("inactive");
				}
			}
			
			if(!uploading && !this.uploading) {
				this.uploading = true;
				
				if(RequestMonitor && RequestMonitor.updateState) {
					try {
						this.mntrObj = new RequestMonitor({
							"cancelFn": function() {
								that._reEnableTriggerObjects();
								that.uploading = false;
							},
							"delays": [ 1500, 3500, this.options.timeout ]
						});
					}
					catch(error) {
						this.mntrObj = null;
					}
				}
				
				var url = this.options.url;
				this.element.attr("action", url);
				
				this.scramble.val((new Date()).getTime());
				
				this.element.trigger('submit');
			}
		},
		"_reEnableTriggerObjects": function() {
			if(this._$triggerObj) {
				this._$triggerObj.removeClass("inactive");
				delete this._$triggerObj;
			}
		},
		"_translateResponse": function(responseText) {
			var that = this;
			var responseContext = {};
			var handleRespConf = {
				"messageInfo": this.options.messageObjParser(responseText),
				"postTokenSelector": this.options.postTokenSelector,
				"responseContext": responseContext,
				"requestMonitorObj": this.mntrObj,
				"callbackDelayMillisecs": this.options.successDelayMillisecs,
				"callbackFn": function(messageInfo, xhrObj) {
					if((typeof responseContext.responseFalse === "undefined") || (responseContext.responseFalse == null) || (responseContext.responseFalse === true)) {
						if(!responseContext.displayedMessages) {
							alertDialog(that.options.systemErrorMsg);
						}
						
						that.element.trigger("uploadError", {
							"responseFalse": true,
							"displayedMessages": responseContext.displayedMessages
						});
					}
					else {
						that.element.trigger("uploadSuccess", {
							"responseFalse": false,
							"displayedMessages": responseContext.displayedMessages
						});
					}
				}
			};
			
			if((!responseText) || (responseText.trim().length <= 0)) {
				handleRespConf.messageInfo.error = true;
				handleRespConf.pollingURI = null;
			}
			
			this.options.messageObjHandler(handleRespConf);
		}
	});
}(jQuery));
