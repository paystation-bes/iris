var	locationData, profCount = 0,
	dayTimeID = new Array();
	dayTimeID[0] = ["#SundayTime", "#SundayStart", "#SundayEnd", "#SundayOpen", "#SundayClosed"];
	dayTimeID[1] = ["#MondayTime", "#MondayStart", "#MondayEnd", "#MondayOpen", "#MondayClosed"];
	dayTimeID[2] = ["#TuesdayTime", "#TuesdayStart", "#TuesdayEnd", "#TuesdayOpen", "#TuesdayClosed"];
	dayTimeID[3] = ["#WednesdayTime", "#WednesdayStart", "#WednesdayEnd", "#WednesdayOpen", "#WednesdayClosed"];
	dayTimeID[4] = ["#ThursdayTime", "#ThursdayStart", "#ThursdayEnd", "#ThursdayOpen", "#ThursdayClosed"];
	dayTimeID[5] = ["#FridayTime", "#FridayStart", "#FridayEnd", "#FridayOpen", "#FridayClosed"];
	dayTimeID[6] = ["#SaturdayTime", "#SaturdayStart", "#SaturdayEnd", "#SaturdayOpen", "#SaturdayClosed"];

//--------------------------------------------------------------------------------------

function allDayClosed(parentObj, status)
{
	if($(parentObj).attr("id") == "Day0"){ var rowObj = $("#Day6"); } else { var rowObj = $(parentObj).prev();} 
	var prevCloseMenu = rowObj.children(".closedtime").children("select");
	
	if(status == true){
		if($(prevCloseMenu).val() > 95) { $(prevCloseMenu).val(95).width("120px"); }
		for(x = 97; x <= 191; x++){ $(prevCloseMenu).children("option:eq("+x+")").attr("disabled", true); }} 
	else {
		for(x = 97; x <= 191; x++){ $(prevCloseMenu).children("option:eq("+x+")").attr("disabled", false); }}
}

//--------------------------------------------------------------------------------------

function openTimeChange(formObj)
{
	var parentObj = formObj.parents("tr");
	var selectedValue = parseInt(formObj.val());
	if(parentObj.attr("id") == "Day0"){ var prevRow = $("#Day6"); } else { var prevRow = parentObj.prev();} 
	if(parentObj.attr("id") == "Day6"){ var nextRow = $("#Day0"); } else { var nextRow = parentObj.next();}
	var closedMenu = "#"+formObj.attr("id").replace("Open", "Close");
	var selectedValueIndex = selectedValue + 1;
	var prevSelectedValue = selectedValue + 95;
	var prevCloseMenu = prevRow.children(".closedtime").children("select");
	var prevOpen24 = prevRow.find(".open24").find("input").val();
	var prevClose24 = prevRow.find(".closed24").find("input").val();
	var prevOpenValue = parseInt(prevRow.children(".opentime").children("select").val());
	var nextOpenValue = parseInt(nextRow.children(".opentime").children("select").val());
	var nextClose24 = nextRow.find(".closed24").find("input").val();
	if(parentObj.children(".open24").children("input").val() != true && parentObj.children(".closed24").children("input").val() != true ) {	
		if(nextClose24 === "true"){ var nextOpenValueIndex = 97; }
		else{ var nextOpenValueIndex = (nextOpenValue != -1)? nextOpenValue + 97 : 1000; }
		$(closedMenu).children("option").each(function(index, element) { 
			if(index <= selectedValue || index >= nextOpenValueIndex){$(this).attr("disabled", true);}
			else {$(this).attr("disabled", false);} });
		if($(closedMenu).val() < selectedValue && $(closedMenu).val() != -1){ $(closedMenu).val(selectedValue) }}
	if(!prevOpen24 || !prevClose24){
		$(prevCloseMenu).children("option").each(function(index, element) { 
			if(index >= selectedValue+97){$(this).attr("disabled", true);}
			else{$(this).attr("disabled", false);} });
		if($(prevCloseMenu).val() > selectedValue+97){ $(prevCloseMenu).val(selectedValue+95) } }
}

//--------------------------------------------------------------------------------------

function closedTimeChange(formObj)
{ 
	var parentObj = formObj.parents("tr");
	var selectedValue = parseInt(formObj.val());
	if(parentObj.attr("id") == "Day0"){ var prevRow = $("#Day6"); } else { var prevRow = parentObj.prev();} 
	if(parentObj.attr("id") == "Day6"){ var nextRow = $("#Day0"); } else { var nextRow = parentObj.next();}
	var nextCloseValue = nextRow.children(".closedtime").children("select").val();
	var nextOpenMenu = nextRow.children(".opentime").children("select");
	var nextOpenValue = nextOpenMenu.val();

	if((parentObj.children(".open24").children("input").val() !== true && parentObj.children(".closed24").children("input").val() !== true) && selectedValue >= 95) {	
		formObj.width("120px");	
		var nextSelectedValue = selectedValue - 95;
		$(nextOpenMenu).children("option").each(function(index, element) { 
			if(index <= nextSelectedValue){$(this).attr("disabled", true);}
			else{$(this).attr("disabled", false);} });		
	} else { 
		formObj.width("80px");
		$(nextOpenMenu).children("option").attr("disabled", false);
	}
}

//--------------------------------------------------------------------------------------

function showProfileCalendar(){
	var	profileCalBtn = {};
	profileCalBtn.btn1 = { text: closeMsg, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
	
	$("#rateProfileCalendar").dialog({
		title: profileRateTtl,
		classes: { "ui-dialog": "visibleOverflow" },
		modal: true,
		resizable: false,
		closeOnEscape: false,
		width: 1000,
		height: "auto",
		hide: "fade",
		buttons: profileCalBtn,
		dragStop: function(){ checkDialogPlacement($(this)); },
		open: function(ui, dialog) {
			var $calendar = $("#profileCal");
			
			var	calHeight = $calendar.height() - 15;
			
			$calendar.fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				scrollTime: moment(),
				slotDuration: "00:30:00",
				slotEventOverlap: false,
				defaultDate: moment(),
				defaultView: 'agendaWeek',
				height: calHeight,
				editable: false,
				selectable: false,
				aspectRatio: 2,
				eventSources: [ loadLocationRateRateProfiles ]
			});
		},
		close: function(ui, dialog) {
			$("#profileCal").fullCalendar("destroy");
		}
	}); 
	btnStatus("");}

function loadLocationRateRateProfiles(scheduleStart, scheduleEnd, timeZone, callback) {
	var $calendar = $("#profileCal");
	removeAllEvents($calendar);
	
	var request = GetHttpObject({
		"showLoader": true
	});
	
	request.onreadystatechange = function(responseFalse, displayedError) {
		if(request.readyState === 4) {
			if(responseFalse || (request.status != 200)) {
				if(!displayedError) {
					alertDialog(systemErrorMsg);
				}
			}
			else {
				var jsonData = parseJSON(request.responseText);
				var rateRateProfilesList = jsonData.list[0].RateRateProfileEditForm;
				var colorsHash = createRateProfileColorsHash(rateRateProfilesList);
				
				callback(generateEvents(scheduleStart, scheduleEnd, __FMT_FORM_DATETIME, rateRateProfilesList, function(eventObj) {
					eventObj = decorateRateProfileEvent(eventObj);
					eventObj.backgroundColor = colorsHash[eventObj.rateRateProfileObj.rateProfileRandomId];
					
					return eventObj;
				}));
			}
		}
	};
	
	request.open("GET", "/secure/settings/locations/listRateRateProfiles.html"
				+ "?locationID=" + $("#contentID").val()
				+ "&startDate=" + encodeURIComponent(scheduleStart.format(__FMT_FORM_DATE))
				+ "&endDate=" + encodeURIComponent(scheduleEnd.format(__FMT_FORM_DATE))
			, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	request.send();
}

function createRateProfileColorsHash(rateRateProfilesList) {
	var rrp, idx = -1, len = rateRateProfilesList.length;
	var rateProfilesColorHash = {};
	var rateProfilesList = [];
	while(++idx < len) {
		rrp = rateRateProfilesList[idx];
		if(!rateProfilesColorHash[rrp.rateProfileRandomId]) {
			rateProfilesColorHash[rrp.rateProfileRandomId] = true;
			rateProfilesList.push(rrp.rateProfileRandomId);
		}
	}
	
	var colorOptions, maxColorIdx = colorList.length - 1;
	if(rateProfilesList.length > maxColorIdx) {
		colorOptions = colorList[maxColorIdx];
	}
	else {
		colorOptions = colorList[rateProfilesList.length];
	}
	
	--maxColorIdx;
	
	idx = -1;
	len = rateProfilesList.length;
	var rpId;
	while(++idx < len) {
		rpId = rateProfilesList[idx];
		rateProfilesColorHash[rpId] = colorOptions[(idx % maxColorIdx) + 1];
	}
	
	return rateProfilesColorHash;
}

function decorateRateProfileEvent(event) {
	var rateSchedule = event.rateRateProfileObj;
	if(rateSchedule.useScheduled) {
		event.className = "rate" + rateSchedule.rateTypeName;
	}
	else {
		event.className = "reOccurEvent rate" + rateSchedule.rateTypeName;
	}
	
	return event;
}

//--------------------------------------------------------------------------------------

function showRateProfiles(profileObj){
	var now = moment();
	var endChartDate = moment().add(1,"y");
	var thisMonth = now.month();
	var thisYear = now.year();
	var thisMonthDays = now.daysInMonth();
	var monthCats = new Array();
	var profileSeries = new Array();
	var colorObj = "";
	var startItem = "";
	var endItem = "";
	
	for(x=0; x<12; x++){
		var monthNow = moment().month(thisMonth+x);
		monthCats.push(monthNow.format("MMM YYYY")); }
	
	for(x=0;x < profileObj.length; x++){
		var itemSkip = false;
		var startDate = moment(profileObj[x].startDate, "MM/DD/YYYY");
		if(startDate.isBefore(thisMonth)){ startItem = 0; }
		else if(startDate.isAfter(endChartDate)){ itemSkip = true; }
		else if((startDate.month()-thisMonth) < 0){ startItem = ((startDate.month()-thisMonth)+12)+(startDate.date()/thisMonthDays); }
		else{ startItem = (startDate.month()-thisMonth)+(startDate.date()/thisMonthDays); }
		if(!itemSkip){
			if(profileObj[x].endDate){ 
				var endDate = moment(profileObj[x].endDate, "MM/DD/YYYY");
				var displayEndDate = endDate.format("MMM DD, YYYY");
				if(endDate.isAfter(endChartDate)){ endItem = 11; }
				else if(endDate.isBefore(now)){ itemSkip = true; }
				else if((endDate.month()-thisMonth) < 0){ endItem = ((endDate.month()-thisMonth)+12)+(endDate.date()/thisMonthDays); } 
				else{ endItem = ((endDate.month()-thisMonth)+(endDate.date()/thisMonthDays)); }}
			else { 
				endItem = 11;
				var displayEndDate = noEndDatMsg; }

			if(!itemSkip){ profileSeries.push({id:'profile-'+x,name:profileObj[x].rateProfileName,data:[[startItem,endItem]],startData:startDate.format("MMM DD, YYYY"),endData:displayEndDate}); } }}

		if(profileSeries.length >= 14){ colorObj = colorList[14]; }
		else{ colorObj = colorList[profileSeries.length]; }
		var profileChart = new Highcharts.Chart({
			chart: {
				renderTo: 'rateProfileCal',
				type: 'columnrange',
				inverted: true,
				spacingLeft: 0 },	
			title: { text: null, margin: 0 },
			colors: colorObj,
			yAxis: {
				categories: monthCats,
				tickmarkPlacement: 'on',
				title: null },
			xAxis: {
				labels: { enabled:false } },
			tooltip: {
				borderRadius: 6,
				formatter: function(){
					return	"<strong>"+ this.series.name +"</strong><br/>" + 
							profileSeries[this.series.index].startData + " - " + 
							profileSeries[this.series.index].endData; }},
			plotOptions: { columnrange: {	states: { hover: { color: '#F88B2B' } }, borderColor: '#e8e9ea' }},
			legend: {
				symbolRadius: 2,
				maxHeight: 100,
				layout: 'vertical',
				itemStyle: { "fontWeight": "normal", "color": "#555F66"},
				itemMarginBottom: 4,
				itemHoverStyle: {color: "#DA7923"},
				labelFormatter: function () {
					return	"<strong>"+ this.name + "</strong>: "+ 
							profileSeries[this.index].startData + " - " + 
							profileSeries[this.index].endData; }},
			credits: { enabled: false },
			navigation: { buttonOptions: { enabled: false } },
			series: profileSeries },
			function(profileChart){
				$(profileChart.series).each(function(i, serie){
					serie.legendItem
					.on('mouseover', function(){$(serie.data).each(function(j, point){ point.graphic.attr('fill', '#F88B2B') }) })
					.on('mouseout', function(){ $(serie.data).each(function(j, point){ point.graphic.attr('fill', serie.color) }) }) }); }); }

//--------------------------------------------------------------------------------------

function showLocationDetails(locationDetails, action)
{
	//Process JSON
	locationData =	JSON.parse(locationDetails.responseText);
	setupInEditAlertCancelFn(function(callbackFn) {
		cancelLocationForm(locationData.locationFormDetails.randomId, callbackFn);
	});
	
//$("#locationDetails").append(locationDetails.responseText);
	$("#mapHolder, #mapParkingHolder").html("");
	$("#actionFlag").val("1");
	$("#locSelections").val("");
	$("#psSelections").val("");
	$("#contentID, #formLocationID").val(locationData.locationFormDetails.randomId);
	$("#detailLocationName").html(locationData.locationFormDetails.name);
	$("#formLocationName").val(locationData.locationFormDetails.name);
	$("#detailCapacity").html(Highcharts.numberFormat(locationData.locationFormDetails.capacity,0));
	$("#detailTargetRevenue").html('$'+ Highcharts.numberFormat(locationData.locationFormDetails.targetMonthlyRevenue, 2));
	$("#rateProfileCal").html(loadObj);
	$("#profileAssignmentAreaList").html("");
	
	if(locationData.locationFormDetails.description && locationData.locationFormDetails.description != "")
	{$("#detailDescription, #formDescription").html(locationData.locationFormDetails.description); $("#detailDescriptionTitle, #detailDescription").show();}
	else{$("#detailDescriptionTitle, #detailDescription").hide(); $("#detailDescription, #formDescription").html("");}

	if(locationData.locationFormDetails.isParent == true)
	{
//PARENT LOCATION DETAIL DISPLAY
		$("#locationDetails").addClass("parent");
		$("#menu_pageContent").find(".btnEditParkingLocation").hide();
		$("#parentFlagCheckHidden").val(true);
		$("#parentFlagCheck").addClass("checked");
		$("#locationName, #locationNameSpot").html(locationData.locationFormDetails.name);
		$("#formParentLocationName").autoselector("setSelectedValue", locationData.locationFormDetails.parentLocationId);
		$("#formCapacity").val("");
		$("#formTargetRevenue").val("");
		$("#detailOpMode").html("");
	} else { 
//CHILDREN LOCATION DETAIL DISPLAY
		$("#locationDetails").removeClass("parent");
		$("#menu_pageContent").find(".btnEditParkingLocation").show();
		$("#parentFlagCheckHidden").val(false);
		$("#parentFlagCheck").removeClass("checked");
		$("#formCapacity").val(locationData.locationFormDetails.capacity);
		$("#formTargetRevenue").val(locationData.locationFormDetails.targetMonthlyRevenue);
		$("#formOperatingMode").autoselector("setSelectedValue", locationData.locationFormDetails.permitIssueTypeId);
		$("#detailOpMode").html($("#formOperatingMode").autoselector("getSelectedLabel"));

		if(locationData.locationFormDetails.parentLocation && locationData.locationFormDetails.parentLocation != ""){
			$("#locationName").html(locationData.locationFormDetails.name + '<span title="Parent Location" class="notation">('+ locationData.locationFormDetails.parentLocation +')</span>');
			$("#locationNameSpot").html(locationData.locationFormDetails.name);
			$("#detailParentLocationName").html(locationData.locationFormDetails.parentLocation);
			$("#formParentLocationName").autoselector("setSelectedValue", locationData.locationFormDetails.parentLocationId);
			$("#detailParentLocationTitle, #detailParentLocationName").show();
		}else{
			$("#locationName, #locationNameSpot").html(locationData.locationFormDetails.name);
			$("#detailParentLocationTitle, #detailParentLocationName").hide();
			$("#formParentLocationName").autoselector("reset");
		}
		
				
		//START | END TIME
		for(var x = 0; x < locationData.locationFormDetails.locationOpen.length; x++)
		{
			if(locationData.locationFormDetails.openEntireDay[x] == true)
			{ 
				$(dayTimeID[x][0]+", "+dayTimeID[x][4]).hide(); $(dayTimeID[x][3]).show();
				$("#Day"+x).find(".allDayOpen").show().siblings(".opentime, .closedtime").hide();
				$("#Day"+x).find(".opentime").find("select").val(0);
				$("#Day"+x).find(".closedtime").find("select").val(95);
				$("#Day"+x).find(".allDayClosed").hide();
				$("#Day"+x).find(".open24").find("input").val(true);
				$("#Day"+x).find(".open24").find(".checkBox").addClass("checked");
				$("#Day"+x).find(".closed24").find("input").val(false);
				$("#Day"+x).find(".closed24").find(".checkBox").removeClass("checked");
			}
			else if(locationData.locationFormDetails.closedEntireDay[x] == true)
			{ 
				$(dayTimeID[x][0]+", "+dayTimeID[x][3]).hide(); $(dayTimeID[x][4]).show();
				$("#Day"+x).find(".allDayClosed").show().siblings(".opentime, .closedtime").hide();
				$("#Day"+x).find(".opentime").find("select").val(-1);
				$("#Day"+x).find(".closedtime").find("select").val(-1);
				$("#Day"+x).find(".allDayOpen").hide();
				$("#Day"+x).find(".open24").find("input").val(false);
				$("#Day"+x).find(".open24").find(".checkBox").removeClass("checked");
				$("#Day"+x).find(".closed24").find("input").val(true);
				$("#Day"+x).find(".closed24").find(".checkBox").addClass("checked");
			}
			else
			{
				$(dayTimeID[x][3]+", "+dayTimeID[x][4]).hide();
				var closingTime = closeTime[x];
				$(dayTimeID[x][1]).html(openHrs[locationData.locationFormDetails.locationOpen[x]]);
				$(dayTimeID[x][2]).html(closingTime[locationData.locationFormDetails.locationClose[x]]);
				var closingTime = "";
				$(dayTimeID[x][0]).show();
				
				$("#Day"+x).find(".opentime").find("select").val(locationData.locationFormDetails.locationOpen[x]);
				openTimeChange($("#Day"+x).find(".opentime").find("select"));
				$("#Day"+x).find(".closedtime").find("select").val(locationData.locationFormDetails.locationClose[x]);
				closedTimeChange($("#Day"+x).find(".closedtime").find("select"));
				$("#Day"+x).find(".allDayClosed, .allDayOpen").hide();
				$("#Day"+x).find(".opentime, .closedtime").show();
				$("#Day"+x).find(".open24").find("input").val(false);
				$("#Day"+x).find(".open24").find(".checkboxLabel").removeClass("disabled");
				$("#Day"+x).find(".open24").find(".checkBox").removeClass("checked");
				$("#Day"+x).find(".closed24").find("input").val(false);
				$("#Day"+x).find(".closed24").find(".checkboxLabel").removeClass("disabled");
				$("#Day"+x).find(".closed24").find(".checkBox").removeClass("checked");
			}
		}

		if(locationData.locationFormDetails.mapInfo.missingPayStation == false && $("#PlacePS").length ){ $("#PlacePS").hide(); } else { $("#PlacePS").show(); }		
	}

	//CHILD LOCATIONS
	$("#locChildList").html("");				

	var childFullList = "";
	var childSelectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	var colPlacement = 0;
	var rateProfileObj = false;
	
	if(locationData.locationFormDetails.childLocations){
		var childLocations = locationData.locationFormDetails.childLocations;
		if(childLocations instanceof Array === false){ childLocations = [childLocations]; }
		for(var x = 0; x < childLocations.length; x++)
		{
			var childStatus = childLocations[x].status;
			var childName = childLocations[x].name;
			var childID = childLocations[x].randomId;
			var unassigned = childLocations[x].isUnassigned;
			var childSlctdStyle = "";
			
			if(!unassigned)
			{
				if(childStatus != "selected" && allSelected == true){ allSelected = false; }
				if(childStatus == "selected"){ 
					var colPlacement = selectedCount%3+1;
					childSlctdStyle = slctdOpacityLevel;
					$("#locChildList").append("<li class='col"+colPlacement+" detailValue'>"+childName+"</li>");
					if($("#locSelections").val() == ""){ 
						$("#locSelections").val(childID); 
					}else{ 
						$("#locSelections").val($("#locSelections").val()+","+childID); 
					}
					selectedCount++;
				}
				
				childFullList += '<li id="full-'+ childID +'" class="'+ childStatus + '"'+childSlctdStyle+'><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
				childSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';						
			} }
		if(selectedCount === 0){ $("#locChildList").append("<li class='onlyCol detailValue blank'><a name='noLocations'>"+ noLocationsMsg +"</a></li>"); }
		if(selectedCount === 1){ $("#locChildList").find("li").addClass("onlyCol").removeClass("col1"); } }
	else { childFullList += "<li class='detailValue blank'><a name='noLocations'>"+ noLocationsMsg +"</a></li>"; }
	
	if(colPlacement == 1 && selectedCount > 1){$("#locChildList").append("<li class='col2 detailValue blank'><a name='blankSpace'>&nbsp;</a></li><li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a></li>");}
	if(colPlacement == 2){$("#locChildList").append("<li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a></li>");}

	if(selectedCount == 1){ $("#locChildList").find("li").addClass("onlyCol").removeClass("col1"); }	
			
	$("#formLocationChildren ul.fullList").html(childFullList);
	$("#formLocationChildren ul.selectedList").html(childSelectedList);
	$("#formLocationChildren  #locSelectionsAll").parents("label").show();
	if(allSelected == true){$("#formLocationChildren  #locSelectionsAll").addClass("checked");} else {$("#formLocationChildren  #locSelectionsAll").removeClass("checked");}

	//CHILD PAYSTATIONS
	$("#psChildList").html("");				
	$("#psSelections").val(""); 
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	var colPlacement = 0;
	
	if(locationData.locationFormDetails.payStations) {
		var payStations = locationData.locationFormDetails.payStations;
		if(payStations instanceof Array === false){ payStations = [payStations] } 
		for(var x = 0; x < payStations.length; x++)
		{
			var childType = payStations[x].payStationType;
			var childSeverity = payStations[x].severity;
			var childStatus = payStations[x].status;
			var childName = payStations[x].name;
			var childID = payStations[x].randomId;
			var childSerialNmbr = payStations[x].serialNumber;
			var childSlctdStyle = "";
			
			if(childStatus != "selected" && allSelected == true){ allSelected = false; }
			if(childStatus == "selected"){ 
				var colPlacement = selectedCount%3+1;
				childSlctdStyle = slctdOpacityLevel;
				var scrubbedQuerry = scrubQueryString(document.location.search.substring(1), "all");
				var posItem = (posPermission)? "<a href='"+detailsUrl+"payStationList.html?itemID="+childID+"&pgActn=display&tabID=status&"+scrubbedQuerry+"'>"+childName+"</a>" : childName ;
				$("#psChildList").append("<li class='col"+colPlacement+" detailValue posT"+childType+"-"+childSeverity+"'>"+ posItem +"<br><span class='note'>"+ childSerialNmbr +"</span></li>");
				if($("#psSelections").val() == ""){ 
					$("#psSelections").val(childID); 
				}else{ 
					$("#psSelections").val($("#psSelections").val()+","+childID); 
				}
				selectedCount++;
			}}}

	if(colPlacement == 1 && selectedCount > 1){$("#psChildList").append("<li class='col2 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li><li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li>");}
	if(colPlacement == 2){$("#psChildList").append("<li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li>");}
	
	if(selectedCount === 0){ $("#psChildList").append("<li class='onlyCol detailValue blank'>"+ noPaystationsMsg +"</li>"); }
	if(selectedCount == 1){ $("#psChildList").find("li").addClass("onlyCol").removeClass("col1"); }
	
	if(locationData.locationFormDetails.isUnassigned){ 
		$("#locationDetails").addClass("isUnassigned"); $(".UnassignedName").html(locationData.locationFormDetails.name); $("#unassignedFlag").val("true");
	} else { 
		$("#locationDetails").removeClass("isUnassigned"); $(".UnassignedName").html(""); $("#unassignedFlag").val("false");
	}

	//FACILITIES
	$("#facChildList").html("");
	$("#facSelections").val(""); 
	if(locationData.locationFormDetails.facilities){ 
		var facilityList = locationData.locationFormDetails.facilities;
		if(facilityList instanceof Array === false){facilityList = [facilityList] }
		for(var x = 0; x < facilityList.length; x++){ 
			if(facilityList[x].status === "selected"){
				if($("#facSelections").val() === ""){ 
					$("#facSelections").val(facilityList[x].randomId); 
				}else{ 
					$("#facSelections").val($("#facSelections").val()+","+facilityList[x].randomId); 
				}
				$("#facChildList").append("<li>"+facilityList[x].description+"</li>"); $("#FacilitiesChildren, #flexViewHeading").show(); } } }
	else{ $("#FacilitiesChildren").hide();}
	
	//PROPERTIES
	$("#propChildList").html("");
	$("#propSelections").val("");
	if(locationData.locationFormDetails.properties){
		var propertyList = locationData.locationFormDetails.properties;
		if(propertyList instanceof Array === false){propertyList = [propertyList] }
		for(var x = 0; x < propertyList.length; x++){ 
			if(propertyList[x].status === "selected"){
				if($("#propSelections").val() === ""){ 
					$("#propSelections").val(propertyList[x].randomId); 
				}else{ 
					$("#propSelections").val($("#propSelections").val()+","+propertyList[x].randomId); 
				}
				$("#propChildList").append("<li>"+propertyList[x].name+"</li>"); $("#PropertiesChildren, #flexViewHeading").show();
				if(!locationData.locationFormDetails.facilities){ $("#flexViewArea").width("33%"); $("#PropertiesChildren").width("100%"); }
				else{ $("#flexViewArea").width("67%"); $("#PropertiesChildren, #FacilitiesChildren").width("49%"); } } } }
	else{ 
		$("#PropertiesChildren").hide();
		if(!locationData.locationFormDetails.facilities){ $("#flexViewHeading").hide(); }
		else{ $("#flexViewArea").width("33%");  $("#FacilitiesChildren").width("100%"); $("#flexViewHeading").show(); } }
	
	//LOTS
	$("#lotChildList").html("");
	$("#lotSelections").val("");
	if(locationData.locationFormDetails.lots){
		var lotList = locationData.locationFormDetails.lots;
		if(lotList instanceof Array === false){lotList = [lotList] }
		for(x = 0; x < lotList.length; x++){ 
			if(lotList[x].status === "selected"){
				if($("#lotSelections").val() === ""){ 
					$("#lotSelections").val(lotList[x].randomId); 
				}else{ 
					$("#lotSelections").val($("#lotSelections").val()+","+lotList[x].randomId); 
				}
				$("#lotChildList").append("<li>"+lotList[x].name+"</li>"); $("#LotsChildren, h2#viewAutoCountHeading").show(); } } }
	else{ $("#LotsChildren, h2#viewAutoCountHeading").hide(); }

	if(locationData.locationFormDetails.rateProfilesList){
		rateProfileObj = (!Array.isArray(locationData.locationFormDetails.rateProfilesList))? [locationData.locationFormDetails.rateProfilesList]:locationData.locationFormDetails.rateProfilesList; }
	
	if(rateProfileObj){
		for(x=0;x<rateProfileObj.length;x++){ 
			profCount = x; showProfileAssignment();			
			$("#profileInstanceID"+x).val(rateProfileObj[x].randomId)
			$("#profileAssign"+x).autoselector("setSelectedValue", rateProfileObj[x].rateProfileRandomId)

			var selectedProfileObj = $("#profileAssign"+x).autoselector("getSelectedObject");
			if(selectedProfileObj.extraData === "3"){
				$("#rangeSpec"+x).val(rateProfileObj[x].spaceRange);
				$("#profileAssignmentItem"+x).find(".spaceRange").html(rateProfileObj[x].spaceRange); }

			$("#profileAssignmentItem"+x).find("#pubStartDate"+x).val(rateProfileObj[x].startDate);
			$("#profileAssignmentItem"+x).find(".startDate").html(rateProfileObj[x].startDate);
			
			if(rateProfileObj[x].endDate){ 
				$("#profileAssignmentItem"+x).find("#pubEndDate"+x).val(rateProfileObj[x].endDate);
				$("#profileAssignmentItem"+x).find(".endDate").html(rateProfileObj[x].endDate); }
			else { $("#profileAssignmentItem"+x).find("#pubEndDate"+x).val("");
				$("#profileAssignmentItem"+x).find(".endDate").html(noEndDatMsg); }			
			
			$("#profileAssignmentItem"+x).find(".save").hide();
			$("#profileAssignmentItem"+x).removeClass("form").addClass("display"); }}
	profCount++; showProfileAssignment();
	
	//Display Method
	if(action == "display")
	{
		$("#mainContent").removeClass("edit");
		if(!rateProfileObj){ $("#rateProfileCal").html(noProfileAssignMsg).height("75px"); $("#profileDetailBtn").hide(); }
		//if($(".mainContent").find(".viewBox").is(":hidden")){$(".mainContent").find(".viewBox").show();}
		var displayCallBack = function(){
			if(rateProfileObj && ($("#rateProfileCal").length > 0)) {
				$("#rateProfileCal").height("250px");
				$("#profileDetailBtn").show();
				showRateProfiles(rateProfileObj);
			}
			var	payStationMarkers = [],
				parkingAreaMarkers;
			if(locationData.locationFormDetails.mapInfo.mapEntries || (locationData.locationFormDetails.mapInfo.geoLocationEntries && locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString)){
				if(locationData.locationFormDetails.mapInfo.mapEntries){
					var mapEntries = locationData.locationFormDetails.mapInfo.mapEntries;
					if(mapEntries instanceof Array === false){ mapEntries = [mapEntries]; }
					for(x = 0; x < mapEntries.length; x++) { 
//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
						payStationMarkers.push(new mapMarker(
							mapEntries[x].latitude, 
							mapEntries[x].longitude, 
							mapEntries[x].name, 
							mapEntries[x].payStationType, 
							mapEntries[x].randomId,
							mapEntries[x].severity,'','','','','','',
							mapEntries[x].serialNumber )); }}
				else{ if(!locationData.locationFormDetails.mapInfo.missingPayStation){ payStationMarkers = null; } else{ payStationMarkers = "noData"; } }
				
				if(locationData.locationFormDetails.mapInfo.geoLocationEntries && locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString){
					var parkingAreaEntries = JSON.parse(locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString),
						occupancyLevel,
						occupancyCount;
						
					if(locationData.locationFormDetails.mapInfo.geoLocationEntries.physicalOccupancy){
						occupancyCount = locationData.locationFormDetails.mapInfo.geoLocationEntries.physicalOccupancy; }
					else { 
						occupancyCount = locationData.locationFormDetails.mapInfo.geoLocationEntries.paidOccupancy; }
					if(occupancyCount < 70){ occupancyLevel = 3; } else if(occupancyCount < 90){ occupancyLevel = 2; } else{ occupancyLevel = 1; }
					parkingAreaMarkers = (parkingAreaEntries instanceof Array === false)? [parkingAreaEntries] : parkingAreaEntries ; }
				else{ parkingAreaMarkers = "noData"; occupancyLevel = 0; }
				
				initMapStatic(payStationMarkers, "", parkingAreaMarkers, occupancyLevel); }
			else if(!locationData.locationFormDetails.mapInfo.missingPayStation){ initMapStatic(); }
			else { initMapStatic("noData", "", "noData"); } };
		
		$("#locationDetails").removeClass("form editForm editPA addForm");
		snglSlideFadeTransition("show", $("#locationDetails"), displayCallBack); }
	else if(action === "edit"){
		$("#formPaystationChildren, #formPropertyChildren, #formFacilityChildren, #formLotChildren").find("ul.fullList, ul.selectedList").html("<li style='height: inherit !important;'>"+ loadObj +"</li>");
		payStationSelectList($("#contentID").val(), "", "", false);
		facilitySelectedList($("#contentID").val(), "", false);
		propertySelectedList($("#contentID").val(), "", false);
		lotSelectedList($("#contentID").val(), "", false);
		clearErrors();
		$("#locationDetails").removeClass("addForm editPA").addClass("form editForm")
		snglSlideFadeTransition("show", $("#locationDetails"), function(){ $("#formLocationName").trigger('focus'); }); }
	else if(action === "editPA"){ setParkingAreaMap(); }
	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------

var $profAssignForm = $("#ProfileAssignmentSmpl").html();

function showProfileAssignment(){	
	if(!$("#profileAssignmentArea").is(":visible")){ $("#profileAssignmentArea").show().css("opacity", 1); }
	
	$("#profileAssignmentAreaList").append($profAssignForm);

	var $profAssignContent = $("#formRateProfile").find("#profileAssignmentItem");
	
	$profAssignContent.attr("id", $profAssignContent.attr("id")+profCount);
	$profAssignContent.find("form").attr("id", $profAssignContent.find("form").attr("id")+profCount);
	$profAssignContent.find("input").each(function(){ 
		if($(this).hasClass("profileAssignVal")){ var tempID = $(this).attr("id").replace("Val", ""); $(this).attr("id", tempID+profCount+"Val"); } 
		else{ $(this).attr("id", $(this).attr("id")+profCount); }});
	$profAssignContent.find(".menu").attr("id", $profAssignContent.find(".menu").attr("id")+profCount);
	$profAssignContent.find(".ddMenu").attr("id", $profAssignContent.find(".ddMenu").attr("id")+profCount);
	$profAssignContent.find(".selectMenu").each(function(){ var tempID = $(this).attr("id").replace("Expand", ""); $(this).attr("id", tempID+profCount+"Expand"); });
	
	$("#pubProfileID"+profCount).val($("#profileID").val())
	$profAssignContent.find(".operatingModeDisplay").html($("#formOperatingMode").val())
			
	$profAssignContent.css('opacity', 0)
		.slideDown('slow', function(){ 
			if(profCount >= 4){ var newScroll = (profCount+1)*44-195; $("#profileAssignmentAreaList").animate({scrollTop: newScroll},{ queue: false, duration: 'fast'}); } })
		.animate({ opacity: 1 },{ queue: false, duration: 'slow' });
	
	if(parseInt($("#publishProfileArea").parent(".ui-dialog").css("top")) >= 125 && profCount < 4 ){ $("#publishProfileArea").parent(".ui-dialog").animate({"top": "-=25"},{ queue: false, duration: 'slow' }); }

	
	createDatePicker("#pubStartDate"+profCount, {"minDate": 0,"onClose": function(selectedDate){
		var minDate = moment(selectedDate, "MM/DD/YYYY"); minDate.add(1, 'd');
		$(this).siblings(".endDate").datepicker( "option", "minDate", minDate.format("MM/DD/YYYY"));} });
	createDatePicker("#pubEndDate"+profCount, {"minDate": 1});
	
	coupleDateComponents($("#pubStartDate"+profCount), null, $("#pubEndDate"+profCount), null, true)
	
	var profilesList = $("#profileObjData").text().trim();
	if(profilesList.length <= 0) {
		profilesList = [];
	}
	else {
		profilesList = JSON.parse(profilesList);
	}
	
	var $prevPostToken = $("#pubPostToken" + (profCount-1));
	if($prevPostToken.length > 0) {
		$("#pubPostToken" + profCount).val($prevPostToken.val());
	}
	
	testFormEdit($profAssignContent.find("form"));
	
	$("#profileAssign"+profCount).autoselector({
		"isComboBox": true, 
		"defaultValue": -1,
		"shouldCategorize": false,
		"blockOnEdit": false,
		"data": profilesList})
	.on("itemSelected", function(event, ui) {
		var slctValue = $("#profileAssign"+profCount).autoselector("getSelectedValue");
		var slctdObject = $("#profileAssign"+profCount).autoselector("getSelectedObject");
		
		if(slctValue === "-1"){
			if($("#profileAssignmentItem"+profCount).find(".operatingMode").is(":visible")){
				$("#profileAssignmentItem"+profCount).find(".operatingMode, .dateRange, .confirmBtn.save").fadeOut();
				$("#profileAssignmentItem"+profCount).find(".operatingMode").find("input").val("");
				$("#profileAssignmentItem"+profCount).find(".dateRange").find("input").val(""); }}
		else{
			var opModeObj = $("#formOperatingMode").autoselector("findByValue", slctdObject.extraData);
			$("#profileAssignmentItem"+profCount).find(".operatingModeDisplay").html(opModeObj.label);
			$("#profileAssignmentItem"+profCount).find(".profileNameBoxDsp").prepend(slctdObject.label);
			
			if(opModeObj.value === "3"){ $("#profileAssignmentItem"+profCount).find(".payBySpaceDisplay").show(); }
			else{ $profAssignContent.find(".payBySpaceDisplay").hide(); }
			
			if(!$("#profileAssignmentItem"+profCount).find(".operatingMode").is(":visible")){
				$("#profileAssignmentItem"+profCount).find(".operatingMode, .dateRange, .confirmBtn.save").fadeIn(); }}}); }

//-------------------------------------------------------------------------------------

function facilitySelectedList(curentObjID, locationRandomId, filterStatus)
{
	var faciLists = [];
	var ajaxFlexFacilityList = GetHttpObject();
	var childFullList = "";
	var childSelectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	ajaxFlexFacilityList.onreadystatechange = function()	{
		if (ajaxFlexFacilityList.readyState==4 && ajaxFlexFacilityList.status == 200){
				var faciLists =	JSON.parse(ajaxFlexFacilityList.responseText);
				
				if(faciLists.flexLocationFacilityList.elements){
					faciLists = faciLists.flexLocationFacilityList.elements; 
				
					if(faciLists instanceof Array === false){ faciLists = [faciLists] } 
					
					for(var x = 0; x < faciLists.length; x++){
						var childStatus = faciLists[x].status;
						var childName = faciLists[x].facDescription;
						var childID = faciLists[x].randomId;
						var childSlctdStyle = "";
						
						if(filterStatus === true && $("#formFacilityChildren ul.selectedList").find("li#select-"+childID).hasClass("selected")){ childStatus = "selected" }						
						if(childStatus != "selected" && allSelected == true){ allSelected = false; }
						if(childStatus == "selected"){ 
							var colPlacement = selectedCount%2+1;
							childStatus = childStatus;
							selectedCount++; }
						childFullList += '<li id="full-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
						if(filterStatus !== true){
							childSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>'; } }
							
					$("#formFacilityChildren  #facSelectionsAll").parents("label").show();
					if(allSelected == true){$("#formFacilityChildren  #facSelectionsAll").addClass("checked");} 
					else{$("#formFacilityChildren  #facSelectionsAll").removeClass("checked");} }
				else { childFullList += '<li>'+ noFacilityMsg +'</li>'; }
				$("#formFacilityChildren ul.fullList").html(childFullList);

				if(filterStatus !== true){ $("#formFacilityChildren ul.selectedList").html(childSelectedList); }
		} else if(ajaxFlexFacilityList.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxFlexFacilityList.open("GET",LOC.facilitiesList+"?"+document.location.search.substring(1)+"&wrappedObject.selectedId="+curentObjID,true);
	ajaxFlexFacilityList.send(); }

//-------------------------------------------------------------------------------------

function lotSelectedList(curentObjID, locationRandomId, filterStatus)
{
	var	propLists = [],
		ajaxAutoCountLotList = new GetHttpObject(),
		lotFullList = "",
		lotSelectedList = "",
		showOptions = false,
		selectedCount = 0,
		allSelected = true;
	ajaxAutoCountLotList.onreadystatechange = function()	{
		if (ajaxAutoCountLotList.readyState === 4 && ajaxAutoCountLotList.status === 200){
				var lotLists =	JSON.parse(ajaxAutoCountLotList.responseText);
				
				if(lotLists.caseLocationLotList.elements){
					lotLists = lotLists.caseLocationLotList.elements; 
				
					if(lotLists instanceof Array === false){ lotLists = [lotLists] } 
					
					for(var x = 0; x < lotLists.length; x++){
						var lotStatus = lotLists[x].status;
						var lotName = lotLists[x].lotName;
						var lotID = lotLists[x].randomId;
						var lotSlctdStyle = "";
						
						if(filterStatus === true && $("#formLotChildren ul.selectedList").find("li#select-"+childID).hasClass("selected")){ lotStatus = "selected" }						
						if(lotStatus != "selected" && allSelected == true){ allSelected = false; }
						if(lotStatus == "selected"){ 
							var colPlacement = selectedCount%2+1;
							selectedCount++;
						}
						lotFullList += '<li id="full-'+ lotID +'" class="'+ lotStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ lotName +'</li>';
						if(filterStatus !== true){
							lotSelectedList += '<li id="select-'+ lotID +'" class="'+ lotStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ lotName +'</li>'; } }
							
					$("#formLotChildren #lotSelectionsAll").parents("label").show();
					if(allSelected == true){$("#formLotChildren  #lotSelectionsAll").addClass("checked");} 
					else{$("#formLotChildren  #lotSelectionsAll").removeClass("checked");} }
				else { lotFullList += '<li class="notclickable">'+ noLotMsg +'</li>'; }
				$("#formLotChildren ul.fullList").html(lotFullList);

				if(filterStatus !== true){ $("#formLotChildren ul.selectedList").html(lotSelectedList); }
		} else if(ajaxAutoCountLotList.readyState === 4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxAutoCountLotList.open("GET",LOC.lotList+"?"+document.location.search.substring(1)+"&wrappedObject.selectedId="+curentObjID,true);
	ajaxAutoCountLotList.send(); }

//-------------------------------------------------------------------------------------

function propertySelectedList(curentObjID, locationRandomId, filterStatus)
{
	var propLists = [];
	var ajaxFlexPropertyList = GetHttpObject();
	var childFullList = "";
	var childSelectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	ajaxFlexPropertyList.onreadystatechange = function()	{
		if (ajaxFlexPropertyList.readyState==4 && ajaxFlexPropertyList.status == 200){
				var propLists =	JSON.parse(ajaxFlexPropertyList.responseText);
				
				if(propLists.flexLocationPropertyList.elements){
					propLists = propLists.flexLocationPropertyList.elements; 
				
					if(propLists instanceof Array === false){ propLists = [propLists] } 
					
					for(var x = 0; x < propLists.length; x++){
						var childStatus = propLists[x].status;
						var childName = propLists[x].proName;
						var childID = propLists[x].randomId;
						var childSlctdStyle = "";
						
						if(filterStatus === true && $("#formPropertyChildren ul.selectedList").find("li#select-"+childID).hasClass("selected")){ childStatus = "selected" }						
						if(childStatus != "selected" && allSelected == true){ allSelected = false; }
						if(childStatus == "selected"){ 
							var colPlacement = selectedCount%2+1;
							childStatus = childStatus;
							selectedCount++;
						}
						childFullList += '<li id="full-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
						if(filterStatus !== true){
							childSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>'; } }
							
					$("#formPropertyChildren  #propSelectionsAll").parents("label").show();
					if(allSelected == true){$("#formPropertyChildren  #propSelectionsAll").addClass("checked");} 
					else{$("#formPropertyChildren  #propSelectionsAll").removeClass("checked");} }
				else { childFullList += '<li>'+ noPropertyMsg +'</li>'; }
				$("#formPropertyChildren ul.fullList").html(childFullList);

				if(filterStatus !== true){ $("#formPropertyChildren ul.selectedList").html(childSelectedList); }
		} else if(ajaxFlexPropertyList.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxFlexPropertyList.open("GET",LOC.propertiesList+"?"+document.location.search.substring(1)+"&wrappedObject.selectedId="+curentObjID,true);
	ajaxFlexPropertyList.send(); }

//-------------------------------------------------------------------------------------

function assignProfileNow($saveObj){
	var $formRowObj = $saveObj.parents(".assignmentForm");
	var formID = $formRowObj.find("form").attr("id");
	var $assignRowObj = $saveObj.parents(".profileAssignmentItem");

	$formRowObj.find(".locationID").val($("#contentID").val())
	var params = $.param(serializeForm("#"+formID));
	var ajaxAssignProfile = GetHttpObject({
		"postTokenSelector": "input[id^=pubPostToken]"
	});
	ajaxAssignProfile.onreadystatechange = function(){
		if (ajaxAssignProfile.readyState==4 && ajaxAssignProfile.status == 200){
			var response =  ajaxAssignProfile.responseText.split(":");
			if(response[0] == "true"){
				$formRowObj.find(".profileInstncID").val(response[2]);
				$formRowObj.animate({opacity: "0.1"}, function(){ 
					$assignRowObj.removeClass("form").addClass("display"); 
					$saveObj.hide().siblings(".confirmation_icn, .menu").show(); 
					
					var selectedProfileObj = $(this).find(".profileNameBox").find("input[type=text]").autoselector("getSelectedObject");
					if(selectedProfileObj.extraData === "3"){
						if($(this).find(".payBySpaceDisplay").val() === ""){
							$(this).find(".spaceRange").html(allSpacesMsg); }
						else{
							$(this).find(".spaceRange").html($(this).find(".payBySpaceDisplay").val());}}
							
					$(this).find("span.startDate").html($(this).find("input.startDate").val());
					if($(this).find("input.endDate").val() === ""){
						$(this).find("span.endDate").html(noEndDatMsg); }
					else{
						$(this).find("span.endDate").html($(this).find("input.endDate").val()); 
						$(this).find("span.toMsg, span.endDate").show(); }
					$(this).animate({opacity: "1"},function(){
						if(!$assignRowObj.hasClass("formEdit")){ profCount++; showProfileAssignment(); }}); }); }
		} 
		else if(ajaxAssignProfile.readyState==4){
			alertDialog(failedSaveMsg);
		}
	};
	
	ajaxAssignProfile.open("POST",LOC.assignProfile,true);
	ajaxAssignProfile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxAssignProfile.setRequestHeader("Content-length", params.length);
	ajaxAssignProfile.setRequestHeader("Connection", "close");
	ajaxAssignProfile.send(params);}

//-------------------------------------------------------------------------------------

function removeProfileNow($removeObj){
	$("#mainContent").addClass("edit");
	
	var $formRowObj = $removeObj.parents(".profileAssignmentItem").find(".assignmentForm");
	var $assignRowObj = $removeObj.parents(".profileAssignmentItem");

	var params = "locationRandomId="+$("#contentID").val()+"&randomId="+$formRowObj.find(".profileInstncID").val();	
	var ajaxRemoveLoc = GetHttpObject();
	ajaxRemoveLoc.onreadystatechange = function(){
		if (ajaxRemoveLoc.readyState==4 && ajaxRemoveLoc.status == 200){
			var response =  ajaxRemoveLoc.responseText.split(":");
			if(response[0] == "true"){
				$assignRowObj.slideUp('slow', function(){ 
					$assignRowObj.remove(); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); }
			else{
				var failData =	JSON.parse(ajaxRemoveLoc.responseText);
				alertDialog(systemErrorMsg);
				$formRowObj.find(".postToken").val(failData.errorStatus[0].token);}} 
		else if(ajaxRemoveLoc.readyState==4){
			alertDialog(systemErrorMsg); } };
	ajaxRemoveLoc.open("GET",LOC.deleteProfile+"?"+params,true);
	ajaxRemoveLoc.send();}

//-------------------------------------------------------------------------------------

function resetLocationForm(event)
{
	var hideCallBack = function(){ 
		
		var ajaxNewLocation = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxNewLocation.onreadystatechange = function()
		{
			if (ajaxNewLocation.readyState==4 && ajaxNewLocation.status == 200){
				document.getElementById("locationSettings").reset();
				$("#locationDetails").removeClass("parent");
				$("#parentFlagCheckHidden").val(false);
				$("#unassignedFlag").val(false);
				$("#parentFlagCheck").removeClass("checked");
				$("#contentID, #formLocationID").val("");
				$("#actionFlag").val("0")
				$("#detailDescription, #formDescription").html("");
				$("#formParentLocationName").autoselector("reset");
				$("#profileAssignmentAreaList").html("");
				
				for(var x = 0; x < 7; x++)
				{
					$("#Day"+x).find(".allDayClosed, .allDayOpen").hide();
					$("#Day"+x).find(".opentime, .closedtime").show();
					$("#Day"+x).find(".opentime").find("select").val(-1);
					$("#Day"+x).find(".closedtime").find("select").val(-1);
					$("#Day"+x).find(".open24").find("input").val("false");
					$("#Day"+x).find(".open24").find(".checkboxLabel").removeClass("disabled");
					$("#Day"+x).find(".open24").find(".checkBox").removeClass("checked");
					$("#Day"+x).find(".closed24").find("input").val("false");
					$("#Day"+x).find(".closed24").find(".checkboxLabel").removeClass("disabled");
					$("#Day"+x).find(".closed24").find(".checkBox").removeClass("checked");
				}
				
				showProfileAssignment();
				
				var childFullList = "";
				var childSelectedList = "";
				var showOptions = false;
				var selectedCount = 0;
				var allSelected = true;
				var colPlacement = 0;
				var childData = JSON.parse(ajaxNewLocation.responseText);
	//$("#locationDetails").append(ajaxNewLocation.responseText);

				$("#formPaystationChildren, #formPropertyChildren, #formFacilityChildren, #formLotChildren").find("ul.selectedList").html("");				
				$("#formPaystationChildren, #formPropertyChildren, #formFacilityChildren, #formLotChildren").find("ul.fullList, ul.selectedList").html("<li style='height: inherit !important;'>"+ loadObj +"</li>");
				payStationSelectList("", "", "", false, "");
				facilitySelectedList("", "", false, "");
				propertySelectedList("", "", false, "");
				lotSelectedList("", "", false, "");
				
				var childFullList = "";
				var childSelectedList = "";
				var showOptions = false;
				var selectedCount = 0;
				var allSelected = true;
				var colPlacement = 0;
				if(childData.locationFormDetails.childLocations) {
					var childLocations = childData.locationFormDetails.childLocations;
					if(childLocations instanceof Array === false){ childLocations = [childLocations]; }
					for(var x = 0; x < childLocations.length; x++) {
						var childStatus = childLocations[x].status;
						var childName = childLocations[x].name;
						var childID = childLocations[x].randomId;
				
						childFullList += '<li id="full-'+ childID +'" class="'+ childStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
						childSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus +'"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>'; }} 
				else { childFullList += '<li>'+ noLocationsMsg +'</li>'; }
				$("#locSelections").val("");
				$("#psSelections").val("");
				$("#propSelections").val("");
				$("#facSelections").val("");
				$("#lotSelections").val(""); 
				$("#formLocationChildren ul.fullList").html(childFullList);
				$("#formLocationChildren ul.selectedList").html(childSelectedList);
				$("#formLocationChildren #locSelectionsAll").parents("label").show();
				
				clearErrors();
				$(document.body).scrollTop(0);
				$("#locationDetails").removeClass("editForm isUnassigned").addClass("form addForm");
				snglSlideFadeTransition("show", $("#locationDetails"), function(){ $("#formLocationName").trigger('focus'); }); }
			else if(ajaxNewLocation.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load Company Locations and Paystations
			}
		};
		ajaxNewLocation.open("GET",LOC.formLocations+"?"+document.location.search.substring(1),true);
		ajaxNewLocation.send(); }

	if($("#locationDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#locationDetails"), hideCallBack); }
	else{ hideCallBack(); }
}

//--------------------------------------------------------------------------------------
//AJAX CALLS

function loadLocation(locationID, action)
{
	var hideCallBack = '';
	if($("#contentID").val() === locationID){
		if(action === "edit"){
			hideCallBack = function(){ $(".mainContent").removeClass("editPA").addClass("form editForm"); }
			slideFadeTransition($("#locationDetails"), $("#locationDetails"), hideCallBack);
			payStationSelectList(locationID, "", "", false);
			facilitySelectedList(locationID, "", false);
			propertySelectedList(locationID, "", false);
			lotSelectedList(locationID, "", false); } 
		else if(action === "editPA"){ setParkingAreaMap();  }
		else if(action === "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm editPA addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#locationDetails"), $("#locationDetails"), hideCallBack); } }
	else {
		hideCallBack = function(){ 
			$(".mainContent").removeClass("form editForm editPA addForm");
			var ajaxLoadLocation = GetHttpObject();
			ajaxLoadLocation.onreadystatechange = function()
			{
				if (ajaxLoadLocation.readyState==4 && ajaxLoadLocation.status == 200){
					showLocationDetails(ajaxLoadLocation, action);
				} else if(ajaxLoadLocation.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadLocation.open("GET",LOC.loadLocations+"?locationID="+locationID+"&"+document.location.search.substring(1),true);
			ajaxLoadLocation.send(); }
		if($("#locationDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#locationDetails"), hideCallBack); }
		else{ hideCallBack(); } }
}

//--------------------------------------------------------------------------------------

function deleteLocation(locationID)
{
	var ajaxDeleteLocation = GetHttpObject();
	ajaxDeleteLocation.onreadystatechange = function()
	{
		if (ajaxDeleteLocation.readyState==4 && ajaxDeleteLocation.status == 200){
			if(ajaxDeleteLocation.responseText == "true") { 
				noteDialog(locationDeleteMsg);
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
				var newLocation = location.pathname+"?"+cleanQuerryString;
				setTimeout(function(){window.location = newLocation}, 800); 
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteLocation.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteLocation.open("GET",LOC.deleteLocations+"?locationID="+locationID+"&"+document.location.search.substring(1),true);
	ajaxDeleteLocation.send();
}

//--------------------------------------------------------------------------------------

function verifyDeleteLocation(locationID)
{
	var ajaxVDeleteLocation = GetHttpObject();
	ajaxVDeleteLocation.onreadystatechange = function()
	{
		if(ajaxVDeleteLocation.readyState==4 && ajaxVDeleteLocation.getResponseHeader("CustomStatusCode") == 280){ //Has Locations
			alertDialog(locationDeleteLocations); // Unable to delete until Child Locations are removed
		} else if(ajaxVDeleteLocation.readyState==4 && ajaxVDeleteLocation.getResponseHeader("CustomStatusCode") == 281){ //Has Paystations
			alertDialog(locationDeletePayStations); // Unable to delete until Child Paystations are removed
		} else if(ajaxVDeleteLocation.readyState==4 && ajaxVDeleteLocation.getResponseHeader("CustomStatusCode") == 282){ //Has Permits Attached to it
			alertDialog(locationDeletePermits); // Unable to delete Attached Permits are removed
		} else if (ajaxVDeleteLocation.readyState==4 && ajaxVDeleteLocation.status == 200){
			if(ajaxVDeleteLocation.responseText === "true")
			{
				var deleteLocationButtons = {};
				deleteLocationButtons.btn1 = {
					text : deleteBtn,
					click : 	function() { deleteLocation(locationID); $(this).scrollTop(0); $(this).dialog( "close" ); },
				};
				deleteLocationButtons.btn2 = {
					text : cancelBtn,
					click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
				};
				if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
				$("#messageResponseAlertBox")
				.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteLocationMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteLocationButtons });
				btnStatus(deleteBtn);
			} else {
				alertDialog(systemErrorMsg);  // Unable to delete.
			}
		} else if(ajaxVDeleteLocation.readyState==4){
			alertDialog(systemErrorMsg);  // Unable to delete.
		}
	};
	ajaxVDeleteLocation.open("GET",LOC.verifyDeleteLocations+"?locationID="+locationID+"&"+document.location.search.substring(1),true);
	ajaxVDeleteLocation.send();
}

//--------------------------------------------------------------------------------------

function cancelLocationForm(locationId, callbackFn) {
	var ajaxRequest = GetHttpObject();
	ajaxRequest.onreadystatechange = function(responseFalse, displayedMessage) {
		if(ajaxRequest.readyState == 4) {
			if(!responseFalse) {
				if(typeof callbackFn === "function") {
					callbackFn();
				}
			}
			else if(!displayedMessage) {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	ajaxRequest.open("GET", LOC.cancelLocationForm + "?locationRandomId=" + locationId, true);
	ajaxRequest.send();
}

//--------------------------------------------------------------------------------------

function setParkingAreaMap(){
	var hideCallBack = function(){ $("#locationDetails").removeClass("addForm editForm").addClass("form editPA"); },
		showCallBack = function(){ 
			var	posMarkers = [],
				parkingAreaMarkers = '';
			if(locationData.locationFormDetails.mapInfo.mapEntries || (locationData.locationFormDetails.mapInfo.geoLocationEntries && locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString)){
				if(locationData.locationFormDetails.mapInfo.mapEntries){
					var mapEntries = locationData.locationFormDetails.mapInfo.mapEntries;
					if(mapEntries instanceof Array === false){ mapEntries = [mapEntries]; }
					for(x = 0; x < mapEntries.length; x++) { 
//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
						posMarkers.push(new mapMarker(
							mapEntries[x].latitude, 
							mapEntries[x].longitude, 
							mapEntries[x].name, 
							mapEntries[x].payStationType, 
							mapEntries[x].randomId,
							mapEntries[x].severity,'','','','','','',
							mapEntries[x].serialNumber )); }}
				else{ posMarkers = "noData"; }
				if(locationData.locationFormDetails.mapInfo.geoLocationEntries && locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString){
					var parkingAreaEntries = JSON.parse(locationData.locationFormDetails.mapInfo.geoLocationEntries.jsonString),
					parkingAreaMarkers = (parkingAreaEntries instanceof Array === false)? [parkingAreaEntries] : parkingAreaEntries ; }
				else{ parkingAreaMarkers = "noData"; } }
			else{ posMarkers = "noData"; parkingAreaMarkers = "noData"; }
			initMapParkingAreaEdit("mapParkingArea", posMarkers, parkingAreaMarkers); };
		
		if($("#locationDetails").is(":visible")){ slideFadeTransition($("#locationDetails"), $("#locationDetails"), hideCallBack, showCallBack); }
		else{ 
			if(!$("#locationDetails").hasClass("editPA")){ $("#locationDetails").removeClass("addForm editForm").addClass("form editPA"); }
			snglSlideFadeTransition("show", $("#locationDetails"), showCallBack); } }

//--------------------------------------------------------------------------------------

function postGeoParkingData(tempGeoData){
	$.ajax({
		contentType: 'application/json',
		data: JSON.stringify({ locationGeoData: {randomId: $("#contentID").val(), data: tempGeoData }}),
		processData: false,
		type: 'POST',
		success: function(data){ 
			var response = data.split(":");
			if(response[0] === "true") {
				noteDialog(locationSavedMsg);
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "filterValue");
				var thisParentID = ($("#formParentLocationNameVal").val() !== "")? "_"+$("#formParentLocationNameVal").val() : "";
				var newLocation = (response[1] !== "")? location.pathname+"?"+cleanQuerryString+"&filterValue=loc_"+response[1]+thisParentID : location.pathname+"?"+cleanQuerryString;
				setTimeout(function(){window.location = newLocation}, 500); } },
		error: function(){ alertDialog(failedSaveMsg); },
		url: LOC.saveParkingArea }); }

//--------------------------------------------------------------------------------------

function manageLocList(locID, parentID, $this, eventAction){
	if($("#locationList").find("li.selected").length){ $("#locationList").find("li.selected").each(function(){ 
				var selectedID = $(this).attr("id").split("_");
	// Is clicked Obj is a child of Selected || Is clicked Obj a child of the Same Parent of Selected || Is clicked Obj is Parent of Selected = toggle selection and leave child list visible
				if((parentID && ((parentID == selectedID[1]) || (parentID == selectedID[2]))) || locID == selectedID[2]) {
					$(this).toggleClass("selected"); }
	// toggle selection and if visible child list hide and if clicked Obj is Parent show its child list
				else {
	//Selected is a Parent or has a Parent = hide child List
					if($(this).hasClass("Parent")){
						 $(this).toggleClass("expanded"); 
						 $("#childList_"+selectedID[1]).slideToggle(function(){ if($(this).hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } }); }
					else if(selectedID[2]){
						 $("#loc_"+selectedID[2]).toggleClass("expanded"); 
						 $("#childList_"+selectedID[2]).slideToggle(function(){ if($("#loc_"+selectedID[2]).hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } });  } 
	//clicked Obj is a Parent Show child list
					if($this.hasClass("Parent")) {  
						$this.toggleClass("expanded"); 
						$("#childList_"+locID).slideToggle(function(){ if($this.hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } }); }
	// if child list of clicked Obj is not visisble show it
					else if(parentID && !$("#childList_"+parentID).is(":visible")){ 
						$("#loc_"+parentID).toggleClass("expanded"); 
						$("#childList_"+parentID).slideToggle(function(){ if($("#loc_"+parentID).hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } }); }
					$(this).toggleClass("selected"); }//toggle selected
			}); $this.toggleClass("selected"); }
	// if nothing is currently selected = ad selection and show child list if Parent
			else{ 
				$this.toggleClass("selected");
				if($this.hasClass("Parent")) { 
					$this.toggleClass("expanded"); 
					$("#childList_"+locID).slideToggle(function(){ if($this.hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } }); }
	// if child list of clicked Obj is not visisble show it
				else if(parentID && !$("#childList_"+parentID).is(":visible")){ 
					$("#loc_"+parentID).toggleClass("expanded"); 
					$("#childList_"+parentID).slideToggle(function(){ if($("#loc_"+parentID).hasClass("expanded")){ checkVisibleItem($this, $("#locationList")); } }); } } }

//--------------------------------------------------------------------------------------

function hideSelected($this){
	snglSlideFadeTransition("hide", $("#locationDetails")); 
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){
		$this.toggleClass("selected"); 
		if($this.hasClass("Parent") || $this.parent("ul").hasClass("childList")) { $("ul.childList:visible").slideToggle(); $(".Parent.expanded").toggleClass("expanded"); } } }

//--------------------------------------------------------------------------------------

function newLocForm(event){
	cancelLocationForm("");
	$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); })
	$("ul.childList:visible").slideToggle(); $(".Parent.expanded").toggleClass("expanded");
	setupInEditAlertCancelFn(function() { cancelLocationForm(""); });
	resetLocationForm(event); }

//--------------------------------------------------------------------------------------

function localSelectionCheck(locationID, eventAction){
	var	itemID = locationID.split("_"),
		locID = itemID[1],
		parentID = itemID[2],
		$this = (parentID)? $("#loc_"+locID+"_"+parentID) : $("#loc_"+locID);
		
	if(eventAction == "delete"){ verifyDeleteLocation(locID); }
	else if(eventAction == "hide"){ 
		var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
		crntPageAction = "";
		history.pushState(null, null, location.pathname+"?"+cleanQuerryString);
		hideSelected($this); }
	else {
		if($("#contentID").val() != locID || ((eventAction === "edit" || eventAction === "editPA") && !$(".mainContent").hasClass("form")) || ($(".mainContent").hasClass("form") && !$(".mainContent").hasClass(eventAction))){
			var stateObj = { itemObject: locID, parentObj: parentID, action: eventAction };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&prntID="+stateObj.parentObj+"&pgActn="+stateObj.action);
			manageLocList(locID, parentID, $this);
			loadLocation(locID, eventAction);
			checkVisibleItem($this, $("#locationList")); }}
}

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popAction(tempLocID, tempPrntLocID, tempLocEvent, stateObj){
	if(stateObj === null || (tempLocEvent === "display" && tempLocID === "New")){ 
		if($("li.selected").is(":visible")){ hideSelected($("li.selected")); $("#locationAC").val("").trigger('blur'); }
		else{ hideSelected(); } }
	else if(tempLocEvent === "Add"){
		 newLocForm(); }
	else {
		var tempPopItem = (tempPrntLocID)? $("#loc_"+tempLocID+"_"+tempPrntLocID) : $("#loc_"+tempLocID)
		manageLocList(tempLocID, tempPrntLocID, tempPopItem); 
		loadLocation(tempLocID, tempLocEvent); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function locations(tempLocID, tempPrntLocID, tempLocEvent)
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	$("#contentID").val("");
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempLocID = '',
			tempPrntLocID = '',
			tempLocEvent = '';
		if(stateObj !== null){
			tempLocID = stateObj.itemObject;
			tempPrntLocID = stateObj.parentObj;
			tempLocEvent = stateObj.action; }
		popAction(tempLocID, tempPrntLocID, tempLocEvent, stateObj); }
	if(!popStateEvent && (typeof tempLocID !== undefined && tempLocID !== '')){ popAction(tempLocID, tempPrntLocID, tempLocEvent, ''); }
///////////////////////////////////////////////////
	
	$([imgLocalDir+'icn_LocationChildSelected.png',imgLocalDir+'icn_LocationParentSelected.png']).preload();
	msgResolver.mapAttribute("name", "#formLocationName");
	msgResolver.mapAttribute("locationDescription", "#formDescription");
	msgResolver.mapAttribute("capacity", "#formCapacity");
	msgResolver.mapAttribute("targetMonthlyRevenue", "#formTargetRevenue");
	msgResolver.mapAttribute("locationOpen", "#formOperationHours");
	msgResolver.mapAttribute("locationClose", "#formOperationHours");
	msgResolver.mapAttribute("openEntireDay", "#formOperationHours");
	msgResolver.mapAttribute("closedEntireDay", "#formOperationHours");
	msgResolver.mapAttribute("parentLocationRandomId", "#formParentLocationName");
	msgResolver.mapAttribute("payStations", "#locPSSelectedList");

	testFormEdit($("#locationSettings"))

	$(document.body).on("click", "#locationList > li, .childList > li", function(){ 
		var	actionEvent = ($(this).hasClass("selected"))? "hide" : "display",
			locationID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ localSelectionCheck(locationID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ localSelectionCheck(locationID, actionEvent); }});
	
	$("#locationList").find(".view").on('click', function(event){
		event.preventDefault();
		var locationID = $(this).attr("id");
		localSelectionCheck(locationID, "display");
	});
	
	$(document.body).on("click", "a.btnEditLocation, .menuListButton.edit", function(event) {
		if(!$(this).hasClass("formAction")){
			event.preventDefault(); event.stopImmediatePropagation();
			testFormEdit($("#locationSettings"));
			var locationID = $(this).attr("id");
			if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){localSelectionCheck(locationID, "edit");}); }
			else{ localSelectionCheck(locationID, "edit"); }} });

	$(document.body).on("click", "a.btnEditParkingLocation", function(event) {
		if(!$(this).hasClass("formAction")){
			event.preventDefault(); event.stopImmediatePropagation();
			testFormEdit($("#locationParkingArea"));
			var locationID = $(this).attr("id");
			if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){localSelectionCheck(locationID, "editPA");}); }
			else{ localSelectionCheck(locationID, "editPA"); }} });
	
	$("#locationList").find(".delete").on('click', function(event){
		event.preventDefault();
		var locationID = $(this).attr("id");
		localSelectionCheck(locationID, "delete");
	});
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var locationID = $("#contentID").val();
		verifyDeleteLocation(locationID);
	});
	
	$("#btnAddLocation").on('click', function(event){
		event.preventDefault(); testFormEdit($("#locationSettings"));
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{
			var stateObj = { action: "Add" };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID=New&pgActn="+stateObj.action);
			newLocForm(event); }
	});
	
	$(document.body).on("click", "#parentFlag-edit .checkboxLabel", function(event) // SELECT ALL FEATURE, SELECTS OR DESELECTS ALL IN FULL LIST 
	{
		event.preventDefault();
		var $that = $(this)
		var operatingHoursSet = false;
		
		if($("#psSelections").val() != ""){
			operatingHoursSet = true; }
		if(operatingHoursSet === false && $("#formParentLocationNameVal").val() != ""){
			operatingHoursSet = true; }
		if(operatingHoursSet === false && $("#formCapacity").val() != ""){
			operatingHoursSet = true; }
		if(operatingHoursSet === false && $("#formTargetRevenue").val() != ""){
			operatingHoursSet = true; }
		if(operatingHoursSet === false){
			$("td.opentime, td.closedtime").each(function(index, element){ if($(this).find("select").val() > -1){ operatingHoursSet = true; } }); }
		if(operatingHoursSet === false){
			$("td.open24, td.closed24").each(function(index, element){ if($(this).find("input").val() === true){ operatingHoursSet = true; } }); }
		
		if($(this).find(".checkBox").hasClass("checked")){ 
			if(operatingHoursSet){ 
				confirmDialog({
					title: alertTitle,
					message: convertToParentLocation,
					okLabel: convertToParentLocBtn,
					okCallback: function() { 
						$("#formParentLocationName").autoselector("reset");
						$("#formCapacity").val("");
						$("#formTargetRevenue").val("");
						$("#psSelections").val("");
						$("#psSelections").siblings(".selectedList").find("li").removeClass("selected").addClass("active");
						$("td.opentime").each(function(index, element){ $(this).find("select").val("") });
						$("td.closedtime").each(function(index, element){ $(this).find("select").val("") });
						$("td.open24, td.closed24").each(function(index, element){ 
							$(this).find("input").val("false"); 
							$(this).find(".checkBox").removeClass("checked");
							$(this).find(".checkboxLabel").removeClass("disabled"); });
						$("#locationDetails").addClass("parent"); },
					cancelLabel: "Cancel",
					cancelCallback: function(){$that.find(".checkBox").removeClass("checked"); $that.siblings("input").val("false"); } }); } // GIVE WARNING THAT CHILD LOCATIONS VALUES WILL BE DELETED
			else { $("#locationDetails").addClass("parent"); } }// MAKE PARENT
		else {  
			if($("#locSelections").val() != ""){ 
				confirmDialog({
					title: alertTitle,
					message: convertToChildLocation,
					okLabel: convertToChildLocBtn,
					okCallback: function() { 
						$("#locSelections").val("");
						$("#locSelections").siblings(".selectedList").find("li").removeClass("selected").addClass("active");
						$("#locationDetails").removeClass("parent"); },
					cancelLabel: "Cancel",
					cancelCallback: function(){$that.find(".checkBox").addClass("checked"); $that.siblings("input").val("true"); } }); } // GIVE WARNING PARENT LOCATIONS SETTINGS WILL BE DELETED
			else { $("#locationDetails").removeClass("parent"); } } // MAKE CHILD
	});
	
	$("#formParentLocationName").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"data": JSON.parse($("#formParentLocationNameData").text())
	});
	
	$("#formOperatingMode").autoselector({
		"isComboBox": true,
		"defaultValue": -1,
		"shouldCategorize": false,
		"blockOnEdit": false,
		"data": JSON.parse($("#formOperatingModeData").text())
	});

	$(document.body).on("click", "#locationForm * .textButton.save", function(event){ event.preventDefault(); $("#locationSettings").trigger('submit'); })

	$("#locationSettings").on('submit', function(event){
		event.preventDefault();
		if($(".confirmBtn.save").is(":visible")) {
			alertDialog(alertProfileAssignMsg);
		}
		else {
			var locationID = $("#locationSettings").find("#formLocationID").val();
	//		var params = $("#locationSettings").serialize();
			var params = $.param(serializeForm("#locationSettings"));
			var ajaxSaveLocation = GetHttpObject({
				"triggerSelector": (event) ? event.target : null
			});
			ajaxSaveLocation.onreadystatechange = function(){
				if (ajaxSaveLocation.readyState==4 && ajaxSaveLocation.status == 200) {
					var response = ajaxSaveLocation.responseText.split(":");
					if(response[0] == "true") {
						noteDialog(locationSavedMsg);
						var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "pgActn"]);
						var thisParentID = ($("#formParentLocationNameVal").val() !== "")? "_"+$("#formParentLocationNameVal").val() : "";
						cleanQuerryString = (locationID != "")? cleanQuerryString+"&filterValue=loc_"+locationID+thisParentID : cleanQuerryString;
						crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
						var newLocation = location.pathname+"?"+cleanQuerryString+"&pgActn="+crntPageAction;
						setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
				else if(ajaxSaveLocation.readyState==4){
					alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
			ajaxSaveLocation.open("POST",LOC.saveLocations,true);
			ajaxSaveLocation.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			ajaxSaveLocation.setRequestHeader("Content-length", params.length);
			ajaxSaveLocation.setRequestHeader("Connection", "close");
			ajaxSaveLocation.send(params);
		}
	});
	
	$(document.body).on("click", "#locationForm * .textButton.cancel, #cancelMap", function(event){event.preventDefault(); cancelConfirm("reload"); });
	
	$("#locationView").on("click", "#profileDetailBtn", function(event){event.preventDefault(); showProfileCalendar(); });
	
//--------------------------------
	
	$(document.body).on("click", "#assignNowLabel", function(event){ event.preventDefault();
	if($("#assignNowHidden").val() === "true"){ showLocationAssignment(); }
	else{ 
		if(parseInt($("#publishProfileArea").parent(".ui-dialog").css("top")) >= 75 ){ 
			var tempTopDif = (locCount >= 3)? "+=100": "+="+(locCount+1)*25; 
			$("#publishProfileArea").parent(".ui-dialog").animate({"top": tempTopDif},{ queue: false, duration: 'slow' }); }
		$("#profileAssignmentArea").slideUp('slow', function(){ 
			$("#profileAssignmentAreaList").html(""); locCount = 0; }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });

	$(document.body).on("click", ".confirmBtn.save", function(event){ event.preventDefault(); assignProfileNow($(this)); });
	
	$(document.body).on("click", ".btnEditProfile", function(event){ event.preventDefault();
		$(this).parents(".profileAssignmentItem").find(".assignmentForm").animate({opacity: "0.1"}, function(){ 
			$(this).parents(".profileAssignmentItem").removeClass("display").addClass("formEdit"); 
			$(this).find(".confirmation_icn, .menu").hide();
			$(this).find(".save, span.toMsg").show();
			$(this).animate({opacity: "1"}); }); });
	
	$(document.body).on("click", ".btnDeleteProfile", function(event){ event.preventDefault(); removeProfileNow($(this)); });

//--------------------------------
	
	$("#locationView").on("click", ".pin", function(event){
		event.preventDefault();
		var locationID = $("#contentID").val();
		window.location = LOC.payStationsPlacement+"?locationID="+locationID+"&"+document.location.search.substring(1); });

	$(document.body).on("click", ".open24 > .checkboxLabel", function(event){
		if(!$(this).hasClass("disabled")){ 
			var checkBoxObj = $(this).children(".checkBox");
			var parentObj = $(this).parents("tr"); 
			if(checkBoxObj.hasClass("checked")){ 
				var isClosed = parentObj.children(".closed24").children("input").val();
				if(isClosed === "true"){ parentObj.children(".closed24").children(".checkboxLabel").trigger('click'); }
				parentObj.children(".opentime, .closedtime").hide();
				allDayClosed(parentObj, true);
				parentObj.children(".opentime").children("select").val(0).trigger('change');
				parentObj.children(".closedtime").children("select").val(95).trigger('change');
				parentObj.children(".allDayOpen").show(); } 
			else { 
				parentObj.children(".allDayOpen").hide();
				allDayClosed(parentObj, false);
				parentObj.children(".opentime").children("select").val(-1).trigger('change');
				parentObj.children(".closedtime").children("select").width("80px").val(-1).trigger('change');
				parentObj.children(".opentime, .closedtime").show(); }}});
	
	$(document.body).on("click", ".closed24 > .checkboxLabel", function(event){
		if(!$(this).hasClass("disabled")){
			var checkBoxObj = $(this).children(".checkBox");
			var parentObj = $(this).parents("tr");
			if(checkBoxObj.hasClass("checked"))
			{
				var isOpen = parentObj.children(".open24").children("input").val();
				if(isOpen === "true"){parentObj.children(".open24").children(".checkboxLabel").trigger('click');}
				parentObj.children(".opentime, .closedtime").hide();
				parentObj.children(".opentime").children("select").val(0);
				parentObj.children(".closedtime").children("select").val(0).width("80px");
				allDayClosed(parentObj, true);
				parentObj.children(".allDayClosed").show();
			} else {
				parentObj.children(".allDayClosed").hide();
				parentObj.children(".opentime").children("select").val(-1);
				parentObj.children(".closedtime").children("select").val(-1).width("80px");
				allDayClosed(parentObj, false);
				parentObj.children(".opentime, .closedtime").show(); }}});
	
	$(".opentime > select").on('change', function(){ openTimeChange($(this)); });
	
	$(".closedtime").children("select").on('change', function(){ closedTimeChange($(this)); });
}

//--------------------------------------------------------------------------------------------------------------------------------------

