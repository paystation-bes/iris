(function($, undefined) {
	$.widget("custom.showCat", $.ui.autocomplete, {
        _renderMenu: function(ul, items) {
        	var self = this;
        	var showCat = this.options.shouldCategorize;
        	if((!showCat) && (!this.options.itemTemplate)) {
        		$.ui.autocomplete.prototype._renderMenu.call(this, ul, items);
        	}
        	else {
        		var lastItemInCat = {};
                $.each(items, function(index, item) {
                	if(!showCat) {
                		self._renderItem(ul, item).appendTo(ul);
                	}
                	else {
                		var shouldRemoveCat = false;
                    	if(!item.category) {
                    		item["category"] = " ";
                    		shouldRemoveCat = true;
                    	}
                    	
                    	var $new = self._renderItem(ul, item);
                		var $last = lastItemInCat[item.category];
                		if($last) {
                			lastItemInCat[item.category] = $new.insertAfter($last);
                		}
                		else {
                			if(shouldRemoveCat) {
                				lastItemInCat[item.category] = $new.prependTo(ul);
                			}
                			else {
                				ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    			lastItemInCat[item.category] = $new.appendTo(ul);
                			}
                		}
                		
                		if(shouldRemoveCat) {
                			delete item.category;
                		}
                	}
                });
        	}
        },
        _renderItem: function(ul, item) {
        	var conf = this.options;
        	
        	var $result = $("<li>")
        		.data("ui-autocomplete-item", item);
        	var $itemLink = $("<a>");
        	if(conf.itemTemplate === null) {
        		$itemLink.append(item.label);
        	}
        	else {
        		$itemLink.append(conf.itemTemplate(item));
        	}
        	
        	var description = item.desc;
        	if((!description) || (description.length <= 0)) {
        		if((conf.descTemplate != null) && item.obj) {
        			description = conf.descTemplate(item.obj);
        		}
        	}
        	
        	if(description) {
        		$itemLink.append($("<span>")
    				.addClass("info")
    				.attr("title", conf.descTitle)
    				.text(" (" + description + ")")
    			);
        	}
        	
        	$itemLink.appendTo($result);
        	$result.appendTo(ul);
        	
        	if((conf.selectedCSS) && (item.value == conf.valueContainer.val())) {
        		$result.addClass(conf.selectedCSS);
        	}
        	
        	return $result;
	    }
    });
	
	$.widget("ui.autoselector", {
		"options": {
			"minLength": 0,
			"data": [],
			"isComboBox": false,
			"persistToHidden": true,
			"shouldCategorize": false,
			"itemTemplate": null,
			"descTitle": null,
			"descTemplate": null,
			"selectedCSS": null,
			"remoteFn": null,
			"allowNonMatchedValue": false,
			"nonMatchedLabel": "",
			"remoteCacheSize": 1,
			"filterFn": null,
			"invalidSelectionCSS": "error",
			"blockOnEdit": false,
			"submitOnEnter": false,
			"selectByTyping": true,
			"invalidSelectionMessage": function() {
				return (commonMessages && commonMessages.invalidSelection) ? commonMessages.invalidSelection : "Invalid Option !";
			},
			"invalidValue": "NULL",
			"compileTemplate": compileTemplate
		},
		"_prepareRelatedElements": function() {
			var elemId = this.element.attr("id");
			if(this.options.persistToHidden != true) {
				this.elementValContainer = $("<input type='hidden' id='" + elemId + "Val'>");
			}
			else if(this.options.valueContainerSelector) {
				this.elementValContainer = $(this.options.valueContainerSelector);
				if(this.elementValContainer.length <= 0) {
					throw "Could not locate hidden input to persist value to with selector \"" + this.options.valueContainerSelector + "\"!";
				}
			}
			else {
				this.elementValContainer = this.element.siblings("input[type=hidden][id^=" + elemId + "]");
				if(this.elementValContainer.length > 1) {
					throw "Ambiguous autoselector hidden value, all relatives of the autoselector should be separately group under a parent. "
							+ "This parent shouldn't contain any html elements which id started with '" + elemId + "' !";
				}
				if(this.elementValContainer.length <= 0) {
					this.elementValContainer = $("<input type='hidden' id='" + elemId + "Val'>").insertAfter(this.element);
				}
			}
			
			var $elemSelector = this.element.siblings("a[id^=" + elemId + "]");
			if($elemSelector.length > 1) {
				throw "Multi selector links ('a' element) are not supported by autoselector. "
						+ "To prevent ambiguity, the parent of this autoselector should contains only one a element which id started with '" + elemId + "'";
			}
			else if($elemSelector.length > 0) {
				this.elementSelector = $elemSelector;
			}
		},
		"_bindEvents": function() {
			var that = this;
			var blurTracker = function() {
				var $elem = that.element;
				var label = $elem.val();
				if((!that._typedData) || (that._typedData.length <= 0) || (!that.options.selectByTyping)) {
					if(label.length <= 0) {
						that.reset(true);
						that._hideInvalidNotification();
					}
					else {
						var currentLabel = (!that._currentSelectedItem) ? "" : that._currentSelectedItem.label;
						if(that.options.isComboBox && (label != currentLabel)) {
							that.refreshValue(true);
						}
						
						var hiddenVal = that.elementValContainer.val();
						if((hiddenVal.length > 0) && (hiddenVal != that.options.invalidValue)) {
							that._hideInvalidNotification();
						}
					}
				}
				else {
					label = that._typedData + "";
					var value, selectedItem;
					if(label.length <= 0) {
						that.reset();
						
						selectedItem = {
							"label": that.element.val(),
							"value": that.elementValContainer.val()
						};
					}
					else {
						value = that.parse(label);
						if(value) {
							selectedItem = that.findByValue(value);
						}
						else {
							selectedItem = that.findByLabel(label);
						}
					}
					
					if(!selectedItem) {
						that.element.val(label);
						that._showInvalidNotification();
					}
					else {
						that.setSelectedValue(selectedItem.value);
						that._hideInvalidNotification();
					}
				}
				
				return false;
			};
			
			var selectOnEnter = function() {
				if(that.allData.length == 1) {
					that.setSelectedValue(that.allData[0].value);
					$(".ui-autocomplete").hide();
				}
			};
			
			var itemTemplateFn = null;
			if(this.options.itemTemplate !== null) {
				itemTemplateFn = this.options.compileTemplate(this.options.itemTemplate);
			}
			
			var descTemplateFn = null;
			if(this.options.descTemplate !== null) {
				descTemplateFn = this.options.compileTemplate(this.options.descTemplate);
			}
			
			this.element
				.showCat({
					"minLength": this.options.minLength,
					"source": function(request, response) {
						that._loadAllData(request, response);
					},
					"shouldCategorize": this.options.shouldCategorize,
					"selectedCSS": this.options.selectedCSS,
					"valueContainer": this.elementValContainer,
					"itemTemplate": itemTemplateFn,
					"descTitle": this.options.descTitle,
					"descTemplate": descTemplateFn,
					"search": function(event, ui) {
						if(that._suggestionMode !== true) {
							that.element.trigger("beforeopen", ui);
							if(that.options.selectedCSS) {
								that.element.removeClass(that.options.selectedCSS);
							}
						}
					},
					"focus": function(event, ui) {
						if(that._suggestionMode === true && event.keyCode != null) {
							if(ui.item != null && ui.item.value != -1){ that.element.val(ui.item.label);}
							else{ that.element.val(that._typedData); }}
						return false;
					},
					"select": function(event, ui) {
						event.preventDefault();
						
						delete that._typedData;
						if((that._currentSelectedItem == null) 
								|| ((that._currentSelectedItem.label != ui.item.label) || (that._currentSelectedItem.value != ui.item.value))) {
							that._currentSelectedItem = ui.item;
							that.element.val(ui.item.label);
							that.elementValContainer.val(ui.item.value).trigger('change');
							
							that.element.trigger("itemSelected", that._currentSelectedItem);
							that.element.trigger('blur');
							
							return false;
						}
						
					},
					"open": function(event) {
							that._suggestionMode = true;
					},
					"close": function(event, ui) {
						that._suggestionMode = false;
						if(that.options.selectedCSS) {
							that.element.addClass(that.options.selectedCSS);
						}
						if(that.element.val() == ""){that.element.siblings("label").show();} // Shows label when option menu closes and no value has been given
						$(".ui-autocomplete").scrollTop(0);
						
						return false;
					}
				})
				.on("focus", function(event) {
					if(that.options.blockOnEdit && $("#mainContent").hasClass("edit")) { 
						event.preventDefault();
						$(this).trigger('blur');
						inEditAlert("menu");
					}
					else {
					// Open up the suggestion list when the textbox is focused
						if(that._isBrowsing !== true) {
							if(!that._typedData) {
								if(typeof that.options.remoteFn === "function") {
									that._typedData = that.element.val();
								}
							}
							
							that.element.showCat("search", (that._typedData) ? that._typedData : "");
							that._isBrowsing = true;
						}}
				})
				.on("blur", function(event) {
					blurTracker();
					that._isBrowsing = false;
				})
				.on("keydown", function(event) {
					// When users press a key inside this textbox, it means that they wanna do the search !
					that._isBrowsing = false;
					
					var propagate = true;
					
					// If "enter" key is pressed and the suggestion list has only one item, select it !
					var charCode = event.charCode || event.keyCode || event.which;
					if((charCode == 13) || (charCode == 9)) {
						if(!that._hasNewTerms) {
							selectOnEnter();
						}
						else {
							that._deferedOps.push(selectOnEnter);
						}
					}
					
					if((!that.options.submitOnEnter) && (charCode == 13)){
						event.preventDefault; 
						propagate = false;
					}
					
					return propagate;
				})
				.on("keyup", function(event) {
					var charCode = event.charCode || event.keyCode || event.which;
					if(((charCode < 37) || (charCode > 40)) && (charCode != 13) && (charCode != 9)) {
						// Skip Arrow key
						that._typedData = that.element.val();
						that._hasNewTerms = true;
					}
				});
			
			if(this.elementSelector !== null) {
				this.elementSelector.on('click', function(event) { event.preventDefault();
					if(that.options.blockOnEdit && $("#mainContent").hasClass("edit")){
						$(this).trigger('blur'); inEditAlert("menu"); }
					else if(!$(this).hasClass("disabled")){
						if($(".ui-autocomplete").is(":visible")) {
							$(".ui-autocomplete").hide();
						}
						else {
							that.element.trigger('focus');
						}}
				});
			}
		},
		"_create": function() {
			this._isBrowsing = false;
			this._suggestionMode = false;
			this._currentSelectedItem = null;
			
			this._hasNewTerms = false;
			this._deferedOps = [];
			
			this.elementValContainer = null;
			this.elementSelector = null;
			
			this._invalidLabel = this.element.val();
			
			this.updateData();
			this._prepareRelatedElements();
			this._bindEvents();
			this.refreshValue();
			
			this.clearCache();
			
			return this;
		},
		"refreshValue": function(stopItemSelected) {
			var source = this.allData;
			// This function is gonna work only if there are data in the suggestion box.
			if(source && Array.isArray(source) && (source.length > 0)) {
				var $elemValContainer = this.elementValContainer;
				
				var currVal = $elemValContainer.val().trim();
				
				var defaultVal = this.options.defaultValue;
				var hasDefaultVal = (typeof defaultVal !== "undefined");
				if(currVal.length <= 0) {
					// If currVal is not specified, try the "defaultVal" first. Then try to select first thing on the list.
					if(hasDefaultVal) {
						currVal = defaultVal;
					}
				}
				
				var selectedItem = this.findByValue(currVal);
				if((this.options.isComboBox) && (!selectedItem) && (!hasDefaultVal)) {
					selectedItem = source[0];
				}
				
				if(selectedItem) {
					this.setSelectedValue(selectedItem.value, stopItemSelected);
				}
				else if(this._invalidLabel) {
					this.element.val(this._invalidLabel);
				}
			}
		},
		"reset": function(stopItemSelected) {
			var resetVal = "";
			var defaultVal = this.options.defaultValue;
			if(typeof defaultVal !== "undefined") {
				resetVal = defaultVal;
			}
			else {
				var source = this.allData;
				if(this.options.isComboBox && (typeof defaultVal === "undefined")) {
					if(source && Array.isArray(source) && (source.length > 0)) {
						resetVal = source[0].value;
					}
				}
			}
			this.setSelectedValue(resetVal, stopItemSelected);
			this.element.trigger("reset");
			this.clearCache();
		},
		"setSelectedValue": function(value, stopItemSelected) {
			var $elem = this.element;
			var selected = this.findByValue(value);
			if(selected === null) {
				if(this.options.allowNonMatchedValue) {
					selected = {
						"label": this.options.nonMatchedLabel,
						"value": value
					};
				}
				else {
					selected = {
						"label": "",
						"value": ""
					};
				}
			}
			
			if(this.options.selectedCSS) {
				$elem.addClass(this.options.selectedCSS);
			}
						
			$elem.val(selected.label);
			this.elementValContainer.val(selected.value);

			if((!selected) || (selected.label.length > 0)) {
				$elem.siblings("label").hide();
			}
			else {
				$elem.siblings("label").show();
			}
			
			delete this._typedData;
			
			this._currentSelectedItem = selected;
			if(!stopItemSelected || stopItemSelected !== true) {
				$elem.trigger("itemSelected", this._currentSelectedItem);
			}
		},
		"format": function(value) {
			var result = null;
			
			var resultBuffer = this.findByValue(value);
			if(resultBuffer !== null) {
				result = resultBuffer.value;
			}
			
			return result;
		},
		"parse": function(label) {
			var result = null;
			
			var resultBuffer = this.findByLabel(label);
			if(resultBuffer !== null) {
				result = resultBuffer.value;
			}
			
			return result;
		},
		"findByValue": function(value) {
			var result = null;
			if((typeof value !== "undefined") && (value !== null)) {
				var source = this.allData;
				if(Array.isArray(source)) {
					var obj, idx = -1, len = source.length;
					while((result === null) && (++idx < len)) {
						obj = source[idx];
						if(obj.value == value) {
							result = obj;
						}
					}
				}
			}
			
			return result;
		},
		"findByLabel": function(label) {
			var result = null;
			if((typeof label !== "undefined") && (label !== null)) {
				label = label.toLowerCase();
				var source = this.allData;
				if(Array.isArray(source)) {
					var obj, idx = -1, len = source.length;
					while((result === null) && (++idx < len)) {
						obj = source[idx];
						if(label == (obj.label + "").toLowerCase()) {
							result = obj;
						}
					}
				}
			}
			
			return result;
		},
		"getSelectedObject": function() {
			return this._currentSelectedItem || {};
		},
		"getSelectedValue": function() {
			return (this.elementValContainer) ? this.elementValContainer.val() : null;
		},
		"getSelectedLabel": function() {
			return this.element.val();
		},
		"_filterSuggestions": function(searchTerm) {
			var suggestions = [];
			var data = this.rawData;
			if(typeof this.options.remoteFn === "function") {
				suggestions = data;
			}
			else {
				suggestions = [];
				searchTerm = searchTerm.toLowerCase();
				var item, idx = -1, len = data.length, suggestionsIdx = 0;
				while(++idx < len) {
					item = data[idx];
					if(item.label) {
						if(("" + item.label.toLowerCase()).indexOf(searchTerm) >= 0) {
							suggestions[suggestionsIdx++] = item;
						}
					}
				}
			}
			
			return suggestions;
		},
		"_setOption": function(key, value) {
			$.Widget.prototype._setOption.apply(this, arguments);
			if(key === "data") {
				this.updateData();
			}
		},
		"updateData": function() {
			this.rawData = this._translateData(this.options.data);
			this.allData = this._performFilter(this.rawData);
		},
		"_translateData": function(raw) {
			var result = raw;
			if(typeof result === 'function') {
				result = raw.call(this);
			}
			else if(typeof result === "string") {
				var $dataContainer = $(result);
				if($dataContainer.length > 0) {
					if($dataContainer.is(":input")) {
						result = JSON.parse($dataContainer.val());
					}
					else {
						result = JSON.parse($dataContainer.text());
					}
				}
			}
			
			if(!Array.isArray(result)) {
				result = [ result ];
			}
			
			var elem, i = result.length;
			while(--i >= 0) {
				elem = result[i];
				if(typeof elem === "object") {
					elem.label = elem.label.trim() + "";
					elem.value = elem.value + "";
				}
			}
			
			return result;
		},
		"_performFilter": function(data) {
			var result = data;
			
			var shouldReset = false;
			var selectedValue = this.getSelectedValue();
			var filterFn = this.options.filterFn;
			if(typeof filterFn === "function") {
				result = [];
				
				var idx = -1, len = data.length, elem;
				while(++idx < len) {
					elem = data[idx];
					if(filterFn(elem)) {
						result.push(elem);
					}
					else if((!shouldReset) && (elem.value == selectedValue)) {
						shouldReset = true;
					}
				}
				
				if(shouldReset && (result.length > 0)) {
					this.setSelectedValue(result[0].value);
				}
			}
			
			return result;
		},
		"_loadAllData": function(request, response) {
			var remoteFn = this.options.remoteFn;
			if(typeof remoteFn === "function") {
				var keyword = request.term;
				var cachedData = this._cachedData[keyword];
				
				var that = this;
				var dataFn = function(data) {
					that.rawData = data;	
					that.allData = that._filterSuggestions(request.term);
					if((that._isBrowsing === true) && (that._currentSelectedItem !== null)) {
						if(that.element.val() == that._currentSelectedItem.value) {
							that.allData = that.rawData;
						}
					}
					that.allData = that._performFilter(that.allData);
					response(that.allData);
					that._hasNewTerms = false;
					
					if(that._cachedKey.length > that.options.remoteCacheSize) {
						delete that._cachedData[that._cachedKey.shift()];
					}
					
					that._cachedKey.push(keyword);
					that._cachedData[keyword] = that.allData;
					
					while(that._deferedOps.length > 0) {
						that._deferedOps.shift().call();
					}
				};
				
				if(cachedData) {
					dataFn(cachedData);
				}
				else {
					remoteFn(request, dataFn);
				}
			}
			else {
				this.allData = this._filterSuggestions(request.term);
				if((this._isBrowsing === true) && (this._currentSelectedItem !== null)) {
					if(this.element.val() == this._currentSelectedItem.value) {
						this.allData = this.rawData;
					}
				}
				
				this.allData = this._performFilter(this.allData);
				response(this.allData);
				while(this._deferedOps.length > 0) {
					this._deferedOps.shift().call();
				}
				
				this._hasNewTerms = false;
			}
		},
		"_showInvalidNotification": function() {
			var conf = this.options;
			if(conf.isComboBox) {
				var shown = false;
				if(conf.invalidSelectionCSS) {
					shown = this.element.hasClass(conf.invalidSelectionCSS) || this.element.hasClass("inline-error");
					if(!shown) {
						this.element.addClass(conf.invalidSelectionCSS);
					}
				}
				
				if((typeof this._typedData === "undefined") || (this._typedData == null)) {
					this.refreshValue(true);
				}
				else {
					this._invalidLabel = this._typedData;
					this.reset();
					this.element.addClass(conf.invalidSelectionCSS);
					
					this.element.val(this._invalidLabel);
					this.elementValContainer.val(this.options.invalidValue);
					this._currentSelectedItem = null;
				}
				
				if(this.element.val().length > 0) {
					this.element.siblings("label").hide();
				}
				else {
					this.element.siblings("label").show();
				}
				
				if(!shown) {
					var msg = conf.invalidSelectionMessage;
					if(msg) {
						if(typeof msg === "function") {
							msg = msg();
						}
						
						alertDialog(msg,'','',this.element);
					}
				}
			}
		},
		"_hideInvalidNotification": function() {
			var conf = this.options;
			if(conf.invalidSelectionCSS) {
				this.element.removeClass(conf.invalidSelectionCSS);
			}
		},
		"clearCache": function() {
			this._cachedKey = [];
			this._cachedData = {};
		}
	});
}(jQuery));
