function MessageResolver()
{
	this.fieldsHash = {}; // Hash between Server field name and UI field name.
}

// Teach this object to map between Server Field name and UI field selector (jquery) with the hash between Server Field Name and UI Field ({ <server-fields>, <ui-fields> })
MessageResolver.prototype.mapAllAttributes = function (hash)
{
	for(var serField in hash)
	{
		this.fieldsHash[serField] = hash[serField];
	}
};

// Teach this object to map between Server field name and UI field selector (jquery).
MessageResolver.prototype.mapAttribute = function (serField, uiField)
{
	this.fieldsHash[serField] = uiField;
};

// Returns string represent elements related to the specified "serField". 
MessageResolver.prototype.getField = function (serField)
{
	var fieldId = (serField) ? this.fieldsHash[serField.trim()] : null;
	return (fieldId) ? fieldId : null;
};

// Display server message (simply just create alertCode array and pass it to displayErrorMessage).
MessageResolver.prototype.displaySerMsg = function (serErrObjsArr)
{
	if (serErrObjsArr)
	{
		var errors = serErrObjsArr;
		if (!Array.isArray(errors))
		{
			errors = [ errors ];
		}
		
		var msgObjsArr = [];
		var serFieldNameList, len = errors.length;
		for (var x = 0; x < len; x++)
		{
			serFieldNameList = errors[x].errorFieldName || "";
			serFieldNameList = serFieldNameList.split(",");
			
			var serFieldIdx = 0, serFieldLen = serFieldNameList.length;
			var serFieldName = this.getField(serFieldNameList[serFieldIdx]);
			while(++serFieldIdx < serFieldLen) {
				serFieldName += "," + this.getField(serFieldNameList[serFieldIdx]);
			}
			
			msgObjsArr.push({
				"id": serFieldName,
				"message": errors[x].message,
				"serField": serFieldNameList
			});
		}
		
		this.displayErrorMessage(msgObjsArr);
	}
};

// Display error message in alertCodesArr (Array containing alertCode string).
// If the alertCode string is not defined, that means the string is simply just a message key returned from server.
MessageResolver.prototype.displayErrorMessage = function (msgObjsArr)
{
	// Remove all the error from input fields.
	this.clearErrors();
	
	var message = [], msgObj;
	var displayedMessages = {}, displayedKey;
	var len = msgObjsArr.length;
	for (var x = 0; x < len; x++)
	{
		msgObj = msgObjsArr[x];
		displayedKey = msgObj.id + " : " + msgObj.message;
		if (displayedMessages[displayedKey] !== true)
		{
			if (msgObj.id)
			{
				var testStr = /(Children)/g;
				if (testStr.test(msgObj.id))
				{
					$(msgObj.id).children("ul").addClass("error");
				}
				else
				{
					$(msgObj.id).addClass("error");
				}
			}
			
			if (msgObj.message)
			{
				message.push("<li>" + msgObj.message + "</li>");
			}
			else
			{
				if (msgObj.id) {
					message.push("<li>unmapped error message for " + msgObj.id + "</li>");
				}
				else {
					message.push("<li>unmapped error message for " + msgObj.serField + "</li>");
				}
			}
			
			displayedMessages[displayedKey] = true;
		}
	}
	
	this.displayErrorDialog(message);
};

MessageResolver.prototype.displayErrorDialog = function(message) {
	if (message.length > 0)
	{
		if(Array.isArray(message)) {
			message = message.join("");
		}
		
		formErrorDialog(message);
	}
};

MessageResolver.prototype.clearErrors = function()
{
	$(".error").removeClass("error");
};

var msgResolver = new MessageResolver();