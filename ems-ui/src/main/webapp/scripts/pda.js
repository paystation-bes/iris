function updateOption(type) {
	var span_setting = document.getElementById("selSetting");
	if(span_setting) {
		span_setting.className = type;
	}
	
	var span_location = document.getElementById("selLocation");
	if(span_location) {
		span_location.className = type;
	}
}