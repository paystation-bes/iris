(function($, undefined) {
	// This one is the last resource for unload checking.
	/*
	$(window).bind("beforeunload", function() {
		if(formObject !== null) {
			return commonMessages.cancelConfirm;
		}
	});
	*/
	
	var shrinkableInstance = null;
	var crudInstances = {};
	
	var formatURL = function(urlTemplate, context) {
		var url = compileTemplate(urlTemplate)(context);
		if(url.indexOf("?") >= 0) {
			url = url + "&" + document.location.search.substring(1);
		}
		else {
			url = url +"?"+ document.location.search.substring(1);
		}
		
		return url;
	};
	
	$.widget("ui.shrinkable", {
		"options": {
			"shrinkCSS": "layout4",
			"stretchCSS": "layout1",
			"headerRowSelector": "li.scrollingListHeader",
			"rowSelector": "li",
			"columnSelector": ">div",
			"shrinkColumnCSS": null,
			"hiddenColumnCSS": "shrinked",
			"animationDuration": 1200,
			"menuPercentageWidth": 20
		},
		"_create": function() {
			shrinkableInstance = this;
			
			/* Attributes */
			this.shrinked = false;
			this.shrinkWidths = this._calculateShrinkPercentages();
			
			this._transformDuration = this.options.animationDuration / 2;
			this._isIE = (typeof isIE === "function") && (isIE(8));
		},
		"destroy": function() {
			stretch();
			shrinkableInstance = null;
		},
		"transform": function(transformFn, complete) {
			if(typeof transformFn === "function") {
				var that = this;
				this.element.fadeOut({
					"duration": this._transformDuration,
					"complete": function() {
						transformFn.call(that);
						that.element.fadeIn({
							"duration": this._transformDuration,
							"complete": function() {
								complete.call(that);
								if(this._isIE) {
									$(document.body).trigger("resize");
								}
								else {
									$(window).trigger("resize");
								}
							}
						});
					}
				});
			}
		},
		"shrink": function(callbackFn) {
			var conf = this.options;
			if(this.element.hasClass(conf.shrinkCSS)) {
				callbackFn.call(this, true);
//				this.transform(callbackFn);
			}
			else {
				this.transform(function() {
					if((conf.shrinkColumnCSS == null) || (conf.shrinkColumnCSS.length <= 0)) {
						// DO NOTHING. This means everything is already handled through CSS.
					}
					else {
						var $columns = $(conf.rowSelector + " " + conf.columnSelector);
						
						var shrinkColumnCSS = conf.shrinkColumnCSS;
						var cssClass, idx = conf.shrinkColumnCSS.length;
						while(--idx >= 0) {
							cssClass = shrinkColumnCSS[idx];
							$columns.filter("." + cssClass).css("width", this.shrinkWidths[cssClass] + "%");
						}
						
						var hideSelector = $columns.filter(":not(." + conf.shrinkColumnCSS.join(",.") + ")");
						$(hideSelector).addClass(conf.hiddenColumnCSS);
					}
					
					this.element.removeClass(conf.stretchCSS).addClass(conf.shrinkCSS);
					if(typeof callbackFn === "function") {
						callbackFn(false);
					}
				},
				function() {
					this.element.trigger("shrinked");
				});
			}
		},
		"stretch": function(callbackFn) {
			var conf = this.options;
			if(this.element.hasClass(conf.stretchCSS)) {
				callbackFn.call(this, false);
//				this.transform(callbackFn);
			}
			else {
				this.transform(function() {
					this.element.removeClass(conf.shrinkCSS).addClass(conf.stretchCSS);
					
					if((conf.shrinkColumnCSS == null) || (conf.shrinkColumnCSS.length <= 0)) {
						// DO NOTHING. This means everything is already handled through CSS.
					}
					else {
						var $columns = $(conf.rowSelector + " " + conf.columnSelector);
						
						var hideSelector = $columns.filter(":not(." + conf.shrinkColumnCSS.join(",.") + ")");
						$(hideSelector).removeClass(conf.hiddenColumnCSS);
						
						var shrinkColumnCSS = conf.shrinkColumnCSS;
						var cssClass, idx = conf.shrinkColumnCSS.length;
						while(--idx >= 0) {
							cssClass = shrinkColumnCSS[idx];
							$columns.filter("." + cssClass).css("width", "");
						}
					}
					
					if(typeof callbackFn === "function") {
						callbackFn(true);
					}
				},
				function() {
					this.element.trigger("stretched");
				});
			}
		},
		"_calculateShrinkPercentages": function() {
			var result = {};
			var conf = this.options;
			
			var defsList = conf.shrinkColumnCSS;
			if(defsList !== null) {
				var totalWidth = 0;
				var defIdx = -1, defLen = defsList.length, def;
				
				var $columns = this.element.find(conf.headerRowSelector).find(conf.columnSelector);
				var colIdx = -1, colLen = $columns.length, $col, stop;
				while(++colIdx < colLen) {
					$col = $($columns[colIdx]);
					
					defIdx = -1;
					defLen = defsList.length;
					stop = false;
					while((!stop) && (++defIdx < defLen)) {
						def = defsList[defIdx];
						if((!result[def]) && ($col.is("." + def))) {
							stop = true;
							
							totalWidth += $col.width();
							result[def] = $col.width();
						}
					}
				}
				
				totalWidth += (totalWidth * conf.menuPercentageWidth / 100); 
				for(var key in result) {
					if(result[key]) {
						result[key] = result[key] * 100 / totalWidth;
					}
				}
			}
			
			return result;
		}
	});
	
	$.widget("ui.crudpanel", {
		"options": {
			"viewURL": "",
			"viewRender": function() {},
			"formSelector": "form:first",
			"formURL": "",
			"formRender": function() {},
			"saveURL": "",
			"saveErrorHandler": null,
			"deleteURL": "",
			"deleteVerifyURL": "",
			"deleteErrorHandler": null,
			"itemName": "",
			"autoCloseTimeOut": 1500,
			"fadeDuration": 500,
			"postTokenSelector": null,
			"cleanableInputCSS": "cleanable",
			"customPanels": [],
			"customActions": [],
			"itemSelector": null,
			"itemClickAction": "view",
			"idExtractor": function(elem) { return null; },
			"activeRowCSS": "selected",
			"rowLocator": function($element, id) {
				return $element.find("li[id$=" + id + "]");
			},
			"disabledButtonCSS": "inactive",
			"isActionAllowFn": function(actionName) { return true; }
		},
		"_create": function() {
			var conf = this.options;
			
			/* Attributes */
			this.id = this.options.itemName + "_" + (new Date()).getTime();
			
			this.currentTrigger = null;
			this.activePanel = null;
			this.activeId = null;
			this.previousRawData = null;
			
			this.definedActions = {};
			
			/* Bind Messages */
			var messageCtx = {
				"itemType": conf.itemName
			};
			
			this.messages = {
				"systemError": commonMessages.systemError,
				"saveSuccess": compileTemplate(commonMessages.saveSuccess)(messageCtx),
				"saveError": compileTemplate(commonMessages.saveError)(messageCtx),
				"deleteConfirm": compileTemplate(commonMessages.deleteConfirm)(messageCtx),
				"deleteSuccess": compileTemplate(commonMessages.deleteSuccess)(messageCtx),
				"deleteError": compileTemplate(commonMessages.deleteError)(messageCtx),
				"cancelConfirm": compileTemplate(commonMessages.cancelConfirm)(messageCtx)
			};
			
			/* Bind Events for menu */
			var that = this;
			if(conf.viewURL.length > 0) {
				this.definedActions["view"] = function(event, parameters) {
					var triggerObj = null;
					if (event) {
						event.preventDefault();
					}
					
					that.showViewPanel(this, parameters);
				};
			}
			
			if(conf.saveURL.length > 0) {
				this.definedActions["add"] = function(event, parameters) {
					var triggerObj = null;
					if (event) {
						event.preventDefault();
						var stateObj = { itemObject: "new", action: "add" };
						crntPageAction = stateObj.action;
						history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
					}
					
					that.showAddPanel(this, parameters);
				};
				
				this.definedActions["edit"] = function(event, parameters) {
					var triggerObj = null;
					if (event) {
						event.preventDefault();
						var	tempRandomID = "";
						if($(this).parents(".optionMenu").length){	tempRandomID = $(this).parents(".optionMenu").attr("id").replace("menu_", "") }
						else{ tempRandomID = $(this).parents(".ddMenu").find("input").val(); }
						var stateObj = { itemObject: tempRandomID, action: "edit" };
						crntPageAction = stateObj.action;
						history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
					}
					that.showEditPanel(this, parameters);
				};
				
				this.definedActions["save"] = function(event) {
					that.saveForm(this);
					if (event) {
						event.preventDefault();
					}
				};
				
				this.definedActions["cancel"] = function(event) {
					that.cancelForm();
					
					if(event) {
						event.preventDefault();
					}
				};
			}
			
			if(conf.itemSelector) {
				this.element.on("click", conf.itemSelector, function(event) {
					var newId = that.options.idExtractor(this);
					if(newId === that.activeId) {
						crntPageAction = "";
						history.pushState(null, null, location.pathname);
						that.hideActivePanel(true);
					}
					else {						
						var stateObj = { itemObject: newId, action: "display" };
						crntPageAction = stateObj.action;
						history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
						that.definedActions[conf.itemClickAction].call(this, event);
					}
					
					event.stopPropagation();
				});
			}
			
			for(var actionName in this.definedActions) {
				this.element.on("click", "." + actionName, this.definedActions[actionName]);
			}
			
			if(conf.deleteURL.length > 0) {
				this.definedActions["delete"] = function(event) {
					event.preventDefault();
					that.deleteItem(this);
				};
				
				this.element.on("click", ".delete", this.definedActions["delete"]);
			}
			
			if(crudInstances[this.id]) {
				throw "Duplicated ID: " + this.id;
			}
			
			crudInstances[this.id] = this;
			
			/* Custom Actions */
			var idx = conf.customActions.length;
			var actionConf;
			while(--idx >= 0) {
				actionConf = conf.customActions[idx];
				if(!actionConf.name) {
					throw "Could not register an action with out name !";
				}
				
				this._setupAction(this.executeAction, conf.customActions[idx]);
			}
			
			idx = conf.customPanels.length;
			while(--idx >= 0) {
				actionConf = conf.customPanels[idx];
				if(!actionConf.name) {
					throw "Could not register an panel with out name !";
				}
				
				this._setupAction(this.openPanel, conf.customPanels[idx]);
			}
		},
		"_setupAction": function(actionFn, customConf) {
			var that = this;
			var actionName = customConf.name;
			if(actionName && (!this.definedActions[actionName])) {
				this.definedActions[actionName] = function(event, parameters) {
					if (event) {
						event.preventDefault();
					}
					
					var actionConf = $.extend({}, customConf);
					if (!actionConf.parameters) {
						actionConf.parameters = {};
					}
					
					if (parameters) {
						actionConf.parameters = $.extend(actionConf.parameters, parameters);
					}
					
					actionFn.call(that, this, actionConf);
				};
				
				this.element.on("click", "." + customConf.name, this.definedActions[customConf.name]);
			}
		},
		"destroy": function() {
			delete crudInstances[this.id];
			this.hidePanel(true);
		},
		"showPanel": function(name, parameters) {
			if(name === "display"){ name = "view"; }
			var panel = this.definedActions[name];
			if (panel) {
				panel.call(this, null, parameters);
			}
		},
		"showViewPanel": function(triggerObj, parameters) {
			this._tryPerform(this._showViewPanel, triggerObj, {
				"name": "view",
				"parameters": parameters
			}, true);
		},
		"_showViewPanel": function(triggerObj, actionConf) {
			var parameters = actionConf.parameters;
			if(!parameters) {
				parameters = {};
			}
			if(!parameters.id) {
				parameters.id = this.options.idExtractor(triggerObj);
			}
			
			var context = {
				"triggerObj": triggerObj,
				"parameters": parameters,
				"actionConf": actionConf
			};
			
			var that = this;
			if((this.activeId !== null) && (this.activeId === parameters.id) && (this.previousRawData !== null)) {
				this._showPanel({
					"name": "view",
					"prepareFn": function() {
						return that.options.viewRender.call(context.triggerObj, that.previousRawData, context);
					},
					"shouldBindActions": true,
					"cancelable": true,
					"triggerObj": triggerObj,
					"actionConf": actionConf
				});
			}
			else {
				var req = GetHttpObject();
				req.onreadystatechange = function(responseFalse, displayedError) {
					if(req.readyState == 4) {
						if((!responseFalse) && (req.status == 200)) {
							that._showPanel({
								"name": "view",
								"prepareFn": function() {
									var result = that.options.viewRender.call(context.triggerObj, req.responseText, context);
									that.previousRawData = req.responseText;
									
									return result;
								},
								"shouldBindActions": true,
								"cancelable": true,
								"triggerObj": triggerObj,
								"actionConf": actionConf
							});
						}
						else {
							context["errorStatusCode"] = req.status;
							context["responseFalse"] = responseFalse;
							
							if(!displayedError) {
								alertDialog(that.messages.systemError);
							}
							
							that.element.trigger("errorShowPanel", context);
						}
					}
				};
				
				req.open("GET", formatURL(this.options.viewURL, parameters), true);
				req.send();
			}
		},
		"showAddPanel": function(triggerObj) {
			this._tryPerform(this._showAddPanel, triggerObj, {
				"name": "add"
			});
		},
		"_showAddPanel": function(triggerObj, conf) {
			var context = {
				"triggerObj": triggerObj,
				"mode": "add",
				"actionConf": conf
			};
			
			this._clearViewEditCache();
			
			var that = this;
			var formPrepareFn = function() {
				var formPanel = that.options.formRender.call(triggerObj, null, context);
				if(formPanel) {
					$(formPanel).find("[name=actionFlag]").val("0");
				}
				
				return formPanel;
			};
			
			if(this.options.formURL.length <= 0) {
				this._showPanel({
					"name": "add",
					"prepareFn": formPrepareFn,
					"shouldBindActions": true,
					"triggerObj": triggerObj,
					"actionConf": conf
				});
			}
			else {
				var req = GetHttpObject();
				req.onreadystatechange = function(responseFalse, displayedError) {
					if(req.readyState == 4) {
						if((!responseFalse) && (req.status == 200)) {
							that._showPanel({
								"name": "add",
								"prepareFn": formPrepareFn,
								"shouldBindActions": true,
								"triggerObj": triggerObj,
								"actionConf": conf
							});
						}
						else {
							context["errorStatusCode"] = req.status;
							context["responseFalse"] = responseFalse;
							
							if(!displayedError) {
								alertDialog(that.messages.systemError);
							}
							
							that.element.trigger("errorShowPanel", context);
						}
					}
				};
			}
		},
		"showEditPanel": function(triggerObj, parameters) {
			this._tryPerform(this._showEditPanel, triggerObj, {
				"name": "edit",
				"parameters": parameters
			});
		},
		"_showEditPanel": function(triggerObj, actionConf) {
			var parameters = actionConf.parameters;
			if(!parameters) {
				parameters = {};
			}
			if(!parameters.id) {
				parameters.id = this.options.idExtractor(triggerObj);
			}
			
			var context = {
				"triggerObj": triggerObj,
				"actionConf": actionConf,
				"parameters": parameters,
				"mode": "edit"
			};
			
			var that = this;
			if((this.activeId !== null) && (this.activeId === parameters.id) && (this.previousRawData !== null)) {
				this._showPanel({
					"name": "edit",
					"prepareFn": function() {
						var formPanel = that.options.formRender.call(triggerObj, that.previousRawData, context);
						if(formPanel) {
							$(formPanel).find("[name=actionFlag]").val("1");
						}
						
						return formPanel;
					},
					"shouldBindActions": true,
					"triggerObj": triggerObj,
					"actionConf": actionConf
				});
			}
			else {
				var req = GetHttpObject();
				var formPrepareFn = function() {
					var formPanel = that.options.formRender.call(triggerObj, req.responseText, context);
					if(formPanel) {
						$(formPanel).find("[name=actionFlag]").val("1");
					}
					
					that.previousRawData = req.responseText;
					
					return formPanel;
				};
				
				req.onreadystatechange = function(responseFalse, displayedError) {
					if(req.readyState == 4) {
						if((!responseFalse) && (req.status == 200)) {
							that._showPanel({
								"name": "edit",
								"prepareFn": formPrepareFn,
								"shouldBindActions": true,
								"triggerObj": triggerObj,
								"actionConf": actionConf
							});
						}
						else {
							context["errorStatusCode"] = req.status;
							context["responseFalse"] = responseFalse;
							
							if(!displayedError) {
								alertDialog(that.messages.systemError);
							}
							
							that.element.trigger("errorShowPanel", context);
						}
					}
				};
				
				req.open("GET", formatURL(this.options.viewURL, parameters), true);
				req.send();
			}
		},
		"deleteItem": function(triggerObj, parameters) {
			this._tryPerform(this._deleteItem, triggerObj, {
				"name": "delete",
				"parameters": parameters
			});
		},
		"_deleteItem": function(triggerObj, actionConf) {
			var parameters = actionConf.parameters;
			if(!parameters) {
				parameters = {};
			}
			if(!parameters.id) {
				parameters.id = this.options.idExtractor(triggerObj);
			}
			
			var context = {
				"triggerObj": triggerObj,
				"parameters": parameters,
				"actionConf": actionConf
			};
			
			this._clearViewEditCache();
			this.element.trigger("beforeExecute", context);
			if(this.options.deleteVerifyURL.length <= 0) {
				this._performDeleteItem(context);
			}
			else {
				var that = this;
				var req = GetHttpObject();
				req.onreadystatechange = function(responseFalse, displayedError) {
					if(req.readyState == 4) {
						if((!responseFalse) && (req.status == 200)) {
							var resultObj = JSON.parse(req.responseText);
							var confirmMsg = null;
							if(resultObj.result.message) {
								confirmMsg = resultObj.result.message;
							}
							
							that._performDeleteItem(context, confirmMsg);
						}
						else {
							context["errorStatusCode"] = req.status;
							context["responseFalse"] = responseFalse;
							
							var deleteErrorHandler = that.options.deleteErrorHandler;
							if(deleteErrorHandler !== null) {
								deleteErrorHandler(req.responseText, context);
							}
							else if(!displayedError) {
								alertDialog(that.messages.deleteError);
							}
							
							that.element.trigger("errorExecute", context);
						}
					}
				};
				
				req.open("GET", formatURL(this.options.deleteVerifyURL, parameters), true);
				req.send();
			}
		},
		"_performDeleteItem": function(context, deleteConfirm) {
			var parameters = context.parameters;
			var confirmMessage;
			if(deleteConfirm && (deleteConfirm.length > 0)) {
				confirmMessage = deleteConfirm;
			}
			else {
				confirmMessage = this.messages.deleteConfirm;
			}
			
			var that = this;
			confirmDialog({
				title: attentionTitle,
				message: confirmMessage,
				okCallback: function() {
					var req = GetHttpObject();
					req.onreadystatechange = function(responseFalse, displayedError) {
						if(req.readyState == 4) {
							if((!responseFalse) && (req.status == 200)) {
								noteDialog(that.messages.deleteSuccess, "timeOut", that.options.autoCloseTimeOut);
								that.element.trigger("afterExecute", context);
							}
							else {
								context["errorStatusCode"] = req.status;
								context["responseFalse"] = responseFalse;
								
								var deleteErrorHandler = that.options.deleteErrorHandler;
								if(deleteErrorHandler !== null) {
									deleteErrorHandler(req.responseText, context);
								}
								else if(!displayedError) {
									alertDialog(that.messages.deleteError);
								}
								
								that.element.trigger("errorDelete", context);
							}
						}
					};
					
					req.open("GET", formatURL(that.options.deleteURL, parameters), true);
					req.send();
				}
			});
		},
		"saveForm": function(triggerObj) {
			if(this.options.isActionAllowFn("save")) {
				var $triggerObj = $(triggerObj);
				var conf = this.options;
				if(this.activePanel && (!$triggerObj.hasClass(conf.disabledButtonCSS))) {
					$triggerObj.addClass(conf.disabledButtonCSS);
					
					var $form;
					if(this.activePanel.is("form")) {
						$form = this.activePanel;
					}
					else {
						$form = this.activePanel.find(conf.formSelector);
					}
					
					var formParams = $form.serialize();
					
					var context = {
						"parameters": formParams,
						"triggerObj": triggerObj,
						"activeId": this.activeId
					};
					
					this.element.trigger("beforeSave", context);
					
					var that = this;
					var req = GetHttpObject({
						"postTokenSelector": conf.postTokenSelector
					});
					
					req.onreadystatechange = function(responseFalse, displayedError) {
						if(req.readyState == 4) {
							context.responseText = req.responseText;
							
							if((!responseFalse) && (req.status == 200)) {
								noteDialog(that.messages.saveSuccess, "timeOut", that.options.autoCloseTimeOut);
								
								that._clearViewEditCache(); 
								that.hideActivePanel(true, true, true, function() {
									var	tempObj = getQueryVal(document.location.search.substring(1), "itemID"),
										stateObj = { itemObject: tempObj, action: "display" };	
									crntPageAction = stateObj.action;
									history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
									that.element.trigger("afterSave", context);
								});
							}
							else {
								context["errorStatusCode"] = req.status;
								context["responseFalse"] = responseFalse;
								
								var saveErrorHandler = that.options.saveErrorHandler;
								if(saveErrorHandler !== null) {
									saveErrorHandler(req.responseText, context);
								}
								else if(!displayedError) {
									alertDialog(that.messages.saveError);
								}

								that.element.trigger("errorDelete", context);
							}
							
							$triggerObj.removeClass(conf.disabledButtonCSS);
						}
					};
					
					req.open("POST", conf.saveURL, true);
					req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//				req.setRequestHeader("Content-length", params.length);
	//				req.setRequestHeader("Connection", "close");
					req.send(context.parameters);
				}
			}
		},
		"cancelForm": function(afterCancelFn) { 
			if(this.options.isActionAllowFn("cancel")) {				
				this._clearViewEditCache();
				this.hideActivePanel(true);
			}
		},
		"hideActivePanel": function(stretch, forced, allow, callbackFn) { 
			var that = this;
			if((typeof forced !== "undefined") && (forced === true)) {
				this._detachActivePanel(false, callbackFn);
			}
			else { 
				this._tryPerform(function() {
					if(arguments[2] && arguments[2] !== "new"){ 
						this._detachActivePanel(false, callbackFn);
						this.showPanel("view", { "id":arguments[2] }); }
					else{
						if((!stretch) || (shrinkableInstance === null)) { 
							this._detachActivePanel(false, callbackFn);
						}
						else {					
							shrinkableInstance.stretch(function(executeInBackground) {
								that._detachActivePanel(executeInBackground, callbackFn);
							});
						}}
				}, null, { "name": "hideActivePanel" }, true);
			}
		},

		"hideActivePanelUpload": function(stretch, forced, allow, callbackFn) { 
			var that = this;
			if((typeof forced !== "undefined") && (forced === true)) {
				this._detachActivePanel(false, callbackFn);
			}
			else { 
				this._tryPerform(function() {
					if(arguments[2] && arguments[2] !== "new"){ 
						this._detachActivePanel(false, callbackFn);
						this.showPanel("view", { "id":arguments[2] }); }
					else{
						if((!stretch) || (shrinkableInstance === null)) { 
							this._detachActivePanel(false, callbackFn);
						}
						else {					
							shrinkableInstance.stretch(function(executeInBackground) {
								that._detachActivePanel(executeInBackground, callbackFn);
							});
						}}
				}, null, { "name": "hideActivePanelUpload" }, false);
			}
		},						
		"_tryPerform": function(processFn, triggerObj, conf, allow) {
			if((!conf) || (this.options.isActionAllowFn(conf.name))) {
				if(!this.activePanel || ((typeof this.cancelable !== "undefined") && (this.cancelable === true))) {
					processFn.call(this, triggerObj, conf);
				}
				else {
					if((typeof allow !== "undefined") && (allow === true)) { 
						var that = this;
						confirmDialog({
							title: alertTitle,
							message: cancelConfirmMsg,
							okLabel: cancelCnfrmBtn,
							okCallback: function() {
								var	tempObj = getQueryVal(document.location.search.substring(1), "itemID");
								if(!tempObj){ 
									crntPageAction = "";
									history.pushState(null, null, location.pathname);
									processFn.call(that, triggerObj, conf); }
								else{ 
									var stateObj = { itemObject: tempObj, action: "display" };
									crntPageAction = stateObj.action;
									history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
									processFn.call(that, triggerObj, conf, tempObj); }
							},
							cancelLabel: cancelGoBackBtn,
							selectedText: cancelCnfrmBtn });
						
					} else {
						// Skipping the Cancel Confirmation Alert(which is been tracking by change of state with History API)
						if(conf.name === "hideActivePanelUpload"){
							history.pushState(null, null, location.pathname); 									
						}else{
							inEditAlert("menu");							
						}
					}
					// COMMENTED OUT ORIGINAL TO MATCH REST OF SITE BUT KEEP TO EVENTUALY MIMIC THIS FUNCTIONALITY THROUGH THE SITE.
					/* 	var that = this;
						confirmDialog({
							title: attentionTitle,
							message: this.messages.cancelConfirm,
							okLabel: cancelCnfrmBtn,
							cancelLabel:cancelGoBackBtn,
							okCallback: function() {
								processFn.call(that, triggerObj, conf);
							}
						}); */ 
			}
			}
		},
		"_showPanel": function(options) {
			var conf = this.options;
			
			var that = this;
			var showFn = function(executeInBackground) {
				that._detachActivePanel(executeInBackground, function() {
					var eventParams = [ options.name ];
					that.element.trigger("beforeShowPanel", eventParams);
					
					var parameters = options.actionConf.parameters;
					if(parameters && parameters.id) {
						that.activeId = parameters.id;
					}
					
					if(!that.activeId) {
						that.activeId = that.options.idExtractor(options.triggerObj);
					}
					
					if(that.activeId) {
						that.options.rowLocator(that.element, that.activeId).addClass(conf.activeRowCSS);
					}
					
					if(typeof options.prepareFn === "function") {
						that.activePanel = options.prepareFn();
					}
					else {
						that.activePanel = options.prepareFn;
					}
					
					if(that.activePanel) {
						if((options.shouldBindActions) && (options.shouldBindActions === true)) {
//							that.activePanel.on("submit", "form", that.definedActions["save"]);
							
							for(var action in that.definedActions) {
								that.activePanel.on("click", "." + action, that.definedActions[action]);
							}
						}
						
						that.cancelable = options.cancelable;
						
						if(executeInBackground) {
							that.activePanel.switchClass("invisible", null, {
								complete: function() {
									window.setTimeout(function() {
										that.activePanel.css({'display': 'block'});
										that.element.trigger("showPanel", eventParams);
									}, 0);
								}
							});
						}
						else {
							that.activePanel.switchClass("invisible", null, {
								duration: conf.fadeDuration / 2,
								complete: function() {
									window.setTimeout(function() {
										that.activePanel.css({'display': 'block'});
										that.element.trigger("showPanel", eventParams);
									}, 0);
								}
							});
						}
					}
				});
			};
			
			if(shrinkableInstance === null) {
				showFn(false);
			}
			else {
				shrinkableInstance.shrink(showFn);
			}
		},
		"_detachActivePanel": function(executeInBackground, callbackFn) {
			var conf = this.options;
			
			if(typeof callbackFn !== "function") {
				callbackFn = function() { };
			}
			
			this.previousId = this.activeId;
			if(this.activeId !== null) {
				this.options.rowLocator(this.element, this.activeId).removeClass(conf.activeRowCSS);
				this.activeId = null;
			}
			
			if(!this.activePanel) {
				callbackFn.call(this);
			}
			else {
				this.activePanel
					.off("submit", "form", this.definedActions["save"]);
				
				for(var action in this.definedActions) {
					this.activePanel.off("click", "." + action, this.definedActions[action]);
				}
				
				this.cleanActivePanel();
				
				var previousActivePanel = this.activePanel;
				this.element.trigger("detachedPanel", this.activePanel);
				this.activePanel = null;
				
				if(executeInBackground) {
					previousActivePanel.addClass("invisible").css({'display': 'none'});
					callbackFn.call(this);
				}
				else {
					var that = this;
					previousActivePanel.switchClass(null, "invisible", conf.fadeDuration / 2, "swing"
							, function() {
								previousActivePanel.css({'display': 'none'});
								callbackFn.call(that);
							}
					);
				}
			}
		},
		"cleanActivePanel": function() {
			var $needCleaning = this.activePanel.find("." + this.options.cleanableInputCSS);
			var target = $needCleaning.filter(":input");
			var idx = target.length, $elem;
			while(--idx >= 0) {
				$elem = $(target[idx]);
				if($elem.attr("type") == "file") {
					$elem.replaceWith($elem.clone());
				}
				else {
					$elem.val("");
				}
			}
			
			target = $needCleaning.filter(":not(:input)");
			idx = target.length;
			while(--idx >= 0) {
				$(target[idx]).empty();
			}
		},
		"openPanel": function(triggerObj, panelConf) {
			this._tryPerform(this._openPanel, triggerObj, panelConf);
		},
		"_openPanel": function(triggerObj, panelConf) {
			this._showPanel({
				"name": panelConf.name,
				"prepareFn": panelConf.prepareFn,
				"shouldBindActions": panelConf.shouldBindActions,
				"triggerObj": triggerObj,
				"actionConf": panelConf
			});
		},
		"executeAction": function(triggerObj, actionConf) {
			this._tryPerform(this._executeAction, triggerObj, actionConf);
		},
		"_executeAction": function(triggerObj, actionConf) {
			var parameters = {
				"id": this.options.idExtractor(triggerObj)
			};
			
			var context = {
				"triggerObj": triggerObj,
				"parameters": parameters,
				"actionConf": actionConf
			};
			
			this._clearViewEditCache();
			this.element.trigger("beforeExecute", context);
			if(typeof actionConf.confirmMessage !== "string") {
				this._performAction(context);
			}
			else {
				var that = this;
				confirmDialog({
					title: attentionTitle,
					message: actionConf.confirmMessage,
					okCallback: function() {
						that._performAction(context);
					}
				});
			}
		},
		"_performAction": function(context) {
			var actionConf = context.actionConf;
			var parameters = context.parameters;
			
			if(actionConf.url) {
				var that = this;
				var req = GetHttpObject();
				var callback = function(responseFalse, displayedError) {
					if(req.readyState == 4) {
						if((!responseFalse) && (req.status == 200)) {
							noteDialog(actionConf.successMessage, "timeOut", that.options.autoCloseTimeOut);
							that.element.trigger("afterExecute", context);
						}
						else {
							context["errorStatusCode"] = req.status;
							context["responseFalse"] = responseFalse;
							
							var errorHandler = actionConf.errorHandler;
							if((errorHandler !== null) && (typeof errorHandler === "function")) {
								deleteErrorHandler(req.responseText, context);
							}
							if((!displayedError) && (typeof actionConf.errorMessage === "string")) {
								alertDialog(actionConf.errorMessage);
							}
							
							that.element.trigger("error" + actionConf.name, context);
						}
					}
				};
				
				if(actionConf.pollable && (actionConf.pollable == true)) {
					req.onprocesscompleted = callback;
				}
				else {
					req.onreadystatechange = callback;
				}
				
				req.open("GET", formatURL(actionConf.url, parameters), true);
				req.send();
			}
			else if(typeof actionConf.actionFn === "function") {
				actionConf.actionFn.call(this, actionConf);
			}
		},
		"_clearViewEditCache": function() {
			this.previousRawData = null;
		}
	});
}(jQuery));
