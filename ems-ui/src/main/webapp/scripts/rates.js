// JavaScript Document

var	ogCalHeight = "",
	optsTime = { "militaryMode": false },
	thisSunday =- moment().day(),
	locCount = 0;
//-------------------------------------------------------------------------------------

function extractRateList(rateList, context) {
	var buffer = JSON.parse(rateList).rateListInfo;
	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}	
	context.dataKey = buffer.dataKey;
	
	return result;
}

//-------------------------------------------------------------------------------------

function loadRateList(){
	$("#rateList").paginatetab({
		"url": "/secure/settings/rates/rateList.html",
		"formSelector": "#rateFilterForm",
		"submissionFormat": "GET",
		"emptyMessage": noRatesMsg,
		"rowTemplate": unescape($("#rateListTemplate").html()),
		"responseExtractor": extractRateList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData"); }

//-------------------------------------------------------------------------------------

function showRateDetails(rateDetails, itemID, eventAction){
	$("#contentID, #formRateID").val(itemID);
	$("#actionFlag").val("1");
	
	var rateData =	JSON.parse(rateDetails);
	rateData = rateData.rateInfo;
//// Rate Name
	$("#rateName, #detailRateName").html(rateData.name);
	$("#formRateName").val(rateData.name)

//// Rate Type
	var rateTypeObj = ($("#formRateType").autoselector("findByValue", rateData.rateType));
	$("#detailRateType").html(rateTypeObj.label);
	$("#formRateType").autoselector("setSelectedValue", rateData.rateType);

//// Payment Types
	var paymentDisplayList = "";
	if(rateData.paymentCoin){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentCoin"]+"<br>";
		$("#paymentCoin").trigger('click'); }
	else{ $("#paymentCoin").removeClass("checked"); $("#paymentCoinHidden").val(false); }
	
	if(rateData.paymentBill){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentBill"]+"<br>";
		$("#paymentBill").trigger('click'); }
	else{ $("#paymentBill").removeClass("checked"); $("#paymentBillHidden").val(false); }
	
	if(rateData.paymentCC){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentCC"]+"<br>";
		$("#paymentCC").addClass("checked"); $("#paymentCCHidden").val(true); }
	else{ 
		$("#paymentCC").removeClass("checked"); $("#paymentCCHidden").val(false); }
	
	if(rateData.paymentSC){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentSC"]+"<br>";
		$("#paymentSC").trigger('click'); }
	else{ $("#paymentSC").removeClass("checked"); $("#paymentSCHidden").val(false); }
	
	if(rateData.paymentRF){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentRF"]+"<br>";
		$("#paymentRF").trigger('click'); }
	else{ $("#paymentRF").removeClass("checked"); $("#paymentRFHidden").val(false); }
	
	if(rateData.paymentPC){ 
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentPC"]+"<br>";
		$("#paymentPC").trigger('click'); }
	else{ $("#paymentPC").removeClass("checked"); $("#paymentPCHidden").val(false); }
	
	if(rateData.paymentCoupon){ 
		var prompted = (rateData.promptForCoupon)? " (prompted)" : "" ;
		paymentDisplayList = paymentDisplayList + paymentTypes["paymentCoupon"] + prompted +"<br>";
		$("#paymentCoupon").trigger('click');
		$("#couponPrompt").show();
		$("#promptCoupon").autoselector("setSelectedValue", String(rateData.promptForCoupon)); }
	else{ 
		$("#paymentCoupon").removeClass("checked"); $("#paymentCouponHidden").val(false);
		$("#couponPrompt").hide(); }
	
	$("#detailPaymentTypes").html(paymentDisplayList);
	
//// Enable Add Time
	if(rateData.enableAddTime){ 
		var addtimeOptions = "";
		if($("#addTimeHidden").val() === "false"){ $("#addTime").trigger('click'); }
		
		if(rateData.useAddTimeNumber){ 
			addtimeOptions = addtimeOptions + atNumberMsg +"<br>";
			$("#ADT-useAddNumber").trigger('click'); }
		else{ $("#ADT-useAddNumber").removeClass("checked"); $("#ADT-useAddNumberHidden").val(false); }

		if(rateData.useSpaceOrPlate){ 
			addtimeOptions = addtimeOptions + atSpacePlateMsg +"<br>";
			$("#ADT-useSpaceNumber").trigger('click'); }
		else{ $("#ADT-useSpaceNumber").removeClass("checked"); $("#ADT-useSpaceNumberHidden").val(false); }

		if(rateData.useEbP){ 
			addtimeOptions = addtimeOptions + atXbPMsg +"<br>" + 
				"<span class='detailLabel'>" + atXbPServiceFeeMsg +":</span> "+ rateData.ebPServiceFeeAmount +"<br>" + 
				"<span class='detailLabel'>" + atXbPMinExtensionMsg +":</span> "+ rateData.ebPMiniumExt;
			$("#ADT-xbpServiceFee").val(rateData.ebPServiceFeeAmount);
			$("#ADT-xbpMinExtension").val(rateData.ebPMiniumExt);
			$("#ADT-XBPhone").trigger('click'); }
		else{ 
			$("#ADT-xbpServiceFee").val(""); $("#ADT-xbpMinExtension").val("");
			$("#xbpArea").hide(); $("#ADT-XBPhone").removeClass("checked"); $("#ADT-XBPhoneHidden").val(false); }
				
		$("#detailAddTimeTypes").html(addtimeOptions);
		$("#detailsAdTime, #addTimeDetailArea").show(); }
	else{ 
		$("#addTimeDetailArea").find("input:hidden").val(false);
		$("#addTimeDetailArea").find("input:text").val("");
		$("#detailAddTimeTypes").html("");
		$("#addTime").removeClass("checked"); $("#addTimeHidden").val(false);
		$("#detailsAdTime, #addTimeDetailArea, #xbpArea").hide(); }


//// INCREMENTAL RATE
//// Rate
	$("#detailRateAmount").html("$"+rateData.rateAmount);
	$("#formRateAmount").val(rateData.rateAmount);

//// Minimum Payment
	$("#detailINC-minPayment").html("$"+rateData.minPaymentAmount);
	$("#INC-minPayment").val(rateData.minPaymentAmount);

//// Maximum Payment
	if(rateData.maxPaymentAmount !== "0.00"){
		$("#detailINC-maxPayment").html("$"+rateData.maxPaymentAmount)
		$("#detailINC-maxPaymentLabel, #detailINC-maxPayment").show();
		$("#INC-maxPayment").val(rateData.maxPaymentAmount); }
	else{ $("#INC-maxPayment").val(""); $("#detailINC-maxPaymentLabel, #detailINC-maxPayment").hide(); }

//// Enable Flat Rate
	if(rateData.useFlatRate === true){ 
		$("#flatRateDetails").show();
		if($("#INC-flatRateCheckHidden").val() === "false"){ 
            $("#flatRateDetails, #detailFlatRate").show();
            $("#INC-flatRateCheck").addClass("checked"); $("#INC-flatRateCheckHidden").val(true); } }
	else{ 
		$("#flatRateDetails, #detailFlatRate").hide();
        $("#INC-flatRateCheck").removeClass("checked"); $("#INC-flatRateCheckHidden").val(false);
		$("#INC-flatRateAmount, #INC-startTime, #INC-endTime").val(""); }

//// Flat Rate
	if(rateData.flatRateAmount !== "0.00"){
		$("#detailINC-flatRateAmount").html("$"+rateData.flatRateAmount)
		$("#detailINC-flatRateAmountLabel, #detailINC-flatRateAmount").show();
		$("#INC-flatRateAmount").val(rateData.flatRateAmount); }
	else{ $("#INC-flatRateAmount").val(""); $("#detailINC-flatRateAmountLabel, #detailINC-flatRateAmount").hide(); }
	
//// Start Time
	if(rateData.flatRateStartTime){
		$("#INC-startTime").autominutes("setTime", rateData.flatRateStartTime);
		$("#detailINC-startTime").html($("#INC-startTime").val());
		$("#detailINC-startTimeLabel, #detailINC-startTime").show(); }
	else{ $("#INC-startTime, #INC-startTimeVal").val(""); $("#detailINC-startTimeLabel, #detailINC-startTime").hide(); }

//// End Time
	if(rateData.flatRateEndTime){
		$("#INC-endTime").autominutes("setTime", rateData.flatRateEndTime);
		$("#detailINC-endTime").html($("#INC-endTime").val());
		$("#detailINC-endTimeLabel, #detailINC-endTime").show(); }
	else{ $("#INC-endTime, #INC-endTimeVal").val(""); $("#detailINC-endTimeLabel, #detailINC-endTime").hide(); }

//// Card Increments
	if(rateData.paymentCC){ 
		$("#cardIncrementsArea, #cardIncrementalArea").show();
        $("#cardIncrementsArea").css('opacity', 1);
		$("#inc01CheckHidden").val(true);
		$("#inc01Check").addClass("checked");
		if(rateData.useLastIncrement){ 
			$("#detailuseLastIncremental").show();
			if($("#INC-lastValueCheckHidden").val() === "false"){ $("#INC-lastValueCheck").trigger('click'); }}
		else{ $("#detailuseLastIncremental").hide(); $("#INC-lastValueCheck").removeClass("checked"); $("#INC-lastValueCheckHidden").val(false); }
		
		var	incSteps = [rateData.useInc01, rateData.useInc02, rateData.useInc03, rateData.useInc04, rateData.useInc05, rateData.useInc06, rateData.useInc07, rateData.useInc08, rateData.useInc09, rateData.useInc10 ],
			incMins = [rateData.minsInc01, rateData.minsInc02, rateData.minsInc03, rateData.minsInc04, rateData.minsInc05, rateData.minsInc06, rateData.minsInc07, rateData.minsInc08, rateData.minsInc09, rateData.minsInc10 ],
			incCheck = [$("#INC-checkInc01"), $("#INC-checkInc02"), $("#INC-checkInc03"), $("#INC-checkInc04"), $("#INC-checkInc05"), $("#INC-checkInc06"), $("#INC-checkInc07"), $("#INC-checkInc08"), $("#INC-checkInc09"), $("#INC-checkInc10") ],
			incField = [$("#INC-minsInc01"), $("#INC-minsInc02"), $("#INC-minsInc03"), $("#INC-minsInc04"), $("#INC-minsInc05"), $("#INC-minsInc06"), $("#INC-minsInc07"), $("#INC-minsInc08"), $("#INC-minsInc09"), $("#INC-minsInc10") ],
			incItems = "";
		
		for(x = 0; x < 10; x++){ 
			if(incSteps[x]){ 
				incItems = incItems +"<li>"+ incMins[x]+" min.</li>";
				if(x > 0){ incCheck[x].trigger('click'); if(incCheck[x+1]){incCheck[x+1].removeClass("disabled");} }
				incField[x].attr("disabled", false).parent("span").removeClass("disabled");
				incField[x].val(incMins[x]); }
			else{
				if(incCheck[x].hasClass("checked") && x !== 0){ incCheck[x].trigger('click').addClass("disabled"); }
				if(x > 0){ incField[x].attr("disabled", true).parent("span").addClass("disabled"); }
				incField[x].val("0"); }}
		$("#detailCardIncremental").html(incItems); }
	else{ 
		$("#cardIncrementsArea, #cardIncrementalArea, #detailuseLastIncremental").hide();
		$("#INC-lastValueCheck").removeClass("checked"); $("#INC-lastValueCheckHidden").val(false);
		$("#cardIncrementsArea > .checkBox").removeClass("checked");
		$("#cardIncrementsArea > input:text").val(""); $("#cardIncrementsArea > input:hidden").val("false");
		$("#detailCardIncremental").html(""); }

	$("#mainContent").removeClass("edit");

	if(eventAction === "display"){
		$("#rateDetails").removeClass("editForm form addForm").fadeIn(); }
	else if(eventAction === "edit"){
		testFormEdit($("#rateSettings"));
		$("#rateDetails").removeClass("addForm").addClass("form editForm").fadeIn(function(){ $("#formRateName").trigger('focus'); }); } }

//-------------------------------------------------------------------------------------

// This function should be pull out into a seperate set/reset function for each group of objects on the page.
// The reason because this function depends too much on resetting the values on click actions which cause race condition
// between clearing and setting values on the Card Incremental. This is the cause of issue EMS-6088.
function resetRateForm(itemID, eventAction, rateData){
	$("#rateDetails").fadeOut("fast", function(){
		$(document.body).scrollTop(0);
		$("#rateSettings").trigger("reset");
        $("#rateSettings").find("#formRateID").val("");
		$("#formRateType, #promptCoupon").autoselector("reset");
		var chkBoxObjs = $("#rateSettings").find(".checkboxLabel"), chkBoxCount = chkBoxObjs.length-1;
		chkBoxObjs.each(function(index){ 
			// This is the cause of EMS-6088. The check "#paymentCCLabel" event cause all other objects under "#cardIncrementsArea" to be cleared and displayed using slide-down.
			// However, the values of objects under "#cardIncrementsArea" have already been assigned.
			var tempCheckBox = $(this).find(".checkBox"),
                tempInputID = "#"+$(this).attr("for"),
                tempInput = $(tempInputID);
            if(tempCheckBox.hasClass("checked")){ tempCheckBox.removeClass("checked"); tempInput.val(false); }
			if(index === chkBoxCount){
				if(eventAction === "add"){
					$("#actionFlag").val("0");
					$("#rateDetails").removeClass("editForm").addClass("form addForm").fadeIn(function(){ $("#formRateName").trigger('focus'); });
					$("#mainContent").removeClass("edit");
					testFormEdit($("#rateSettings")); }
				else{ showRateDetails(rateData, itemID, eventAction); } } }); }); }

//-------------------------------------------------------------------------------------

function loadRate(itemID, eventAction){
	$("#rateDetails").fadeOut("fast", function(){
		var ajaxRateDetails = GetHttpObject();
		ajaxRateDetails.onreadystatechange = function()
		{
			if (ajaxRateDetails.readyState==4 && ajaxRateDetails.status == 200){
				resetRateForm(itemID, eventAction, ajaxRateDetails.responseText); } 
			else if(ajaxRateDetails.readyState==4){
				alertDialog(systemErrorMsg); }//Unable to load details
		};
		ajaxRateDetails.open("GET",LOC.loadRates+"?rateID="+itemID,true);
		ajaxRateDetails.send(); }); }

//-------------------------------------------------------------------------------------

function deleteRate(itemID){
	var ajaxDeleteRate = GetHttpObject();
	ajaxDeleteRate.onreadystatechange = function(){
		if (ajaxDeleteRate.readyState==4 && ajaxDeleteRate.status == 200){
			if(ajaxDeleteRate.responseText == "true") { 
				noteDialog(rateDeleteMsg, "", "", "", true);
				var newLocation = location.pathname+"?"+filterQuerry;
				setTimeout(function(){window.location = newLocation}, 800); }
			else { alertDialog(systemErrorMsg); }} 
		else if(ajaxDeleteRate.readyState==4){ alertDialog(systemErrorMsg); } };
	ajaxDeleteRate.open("GET",LOC.deleteRates+"?rateID="+itemID,true);
	ajaxDeleteRate.send(); }

function verifyDeleteRate(itemID){
	var req = GetHttpObject();
	req.onreadystatechange = function(responseFalse, displayedError, displayedMessage) {
		if(req.readyState == 4) {
			if((!responseFalse) && (req.status == 200)) {
				if(displayedMessage) { deleteRate(itemID); }
				else {
					var deleteRateButtons = {
						"btn1": {
							text : deleteBtn,
							click : function() { deleteRate(itemID); $(this).scrollTop(0); $(this).dialog( "close" ); } },
						"btn2": {
							text : cancelBtn,
							click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); } } };
					
					if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
					$("#messageResponseAlertBox")
							.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteRateMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteRateButtons });
					btnStatus(deleteBtn); }}}};	
	req.open("GET", LOC.verifyDeleteRates+"?rateID="+itemID, true);
	req.send();
}

//-------------------------------------------------------------------------------------

function rateSelectionCheck(selectedID, eventAction){
	var itemID = selectedID.split("_");
	if(itemID instanceof Array === true){ itemID = itemID[1]; }
	var $this = $("#rt_"+itemID);
	if(eventAction === "delete"){ verifyDeleteRate(itemID); }
	else if(eventAction === "hide"){ 
		$this.toggleClass("selected"); $("#rateDetails").fadeOut(); $("#contentID").val(""); $("#mainContent").removeClass("edit"); }
	else { if($("#contentID").val() !== itemID || (eventAction === "edit" && !$(".mainContent").hasClass("form")) || (eventAction === "display" && $(".mainContent").hasClass("form"))){
			if($("#rateList").find("li.selected").length){ $("#rateList").find("li.selected").toggleClass("selected"); }
			$this.toggleClass("selected");
			if(eventAction == "edit" && $("#contentID").val() === itemID){
				testFormEdit($("#rateSettings")); 
				$("#rateDetails").fadeOut(function(){
					$(".mainContent").addClass("form editForm"); 
					$("#rateDetails").fadeIn(); }); }
			else if(eventAction == "display" && $("#contentID").val() === itemID){
				$("#rateDetails").fadeOut(function(){$(".mainContent").removeClass("form editForm"); $("#mainContent").removeClass("edit"); $("#rateDetails").fadeIn();}); }
			else {loadRate(itemID, eventAction); }}} }

//-------------------------------------------------------------------------------------

function showRate(itemID){ loadRate(itemID, "display"); }

//-------------------------------------------------------------------------------------

function rates(updateFilter){
	msgResolver.mapAllAttributes({
		"rateType": "#formRateType",
		"rateName": "#formRateName",
		"paymentCoin": "#paymentTypeSection",
		"rateAmount": "#formRateAmount",
		"minPaymentAmount": "#INC-minPayment",
		"maxPaymentAmount": "#INC-maxPayment",
		"flatRateAmount": "#INC-flatRateAmount",
		"flatRateStartTime": "#INC-startTime",
		"flatRateEndTime": "#INC-endTime",
		"ebPServiceFeeAmount": "#ADT-xbpServiceFee",
		"ebPMiniumExt": "#ADT-xbpMinExtension",
		"minsInc01": "#INC-minsInc01",
		"minsInc02": "#INC-minsInc02",
		"minsInc03": "#INC-minsInc03",
		"minsInc04": "#INC-minsInc04",
		"minsInc05": "#INC-minsInc05",
		"minsInc06": "#INC-minsInc06",
		"minsInc07": "#INC-minsInc07",
		"minsInc08": "#INC-minsInc08",
		"minsInc09": "#INC-minsInc09",
		"minsInc10": "#INC-minsInc10"
	});
	
	loadRateList();

	var rateTypes = JSON.parse($("#formRateTypeData").text());
	$("#formRateType").autoselector({
		isComboBox: true,
		defaultValue: 3,
		shouldCategorize: false,
		data: JSON.parse($("#formRateTypeData").text()) })
 //Here for Demo as Incremental is the only available rate type at this time.
	.on("itemSelected", function(event, ui) { 
		if($(this).autoselector("getSelectedValue")!== "3"){ $(this).autoselector("reset"); } });
	
	$("#promptCoupon").autoselector({
		isComboBox: true,
		defaultValue: "false",
		shouldCategorize: false,
		data: JSON.parse($("#promptCouponData").text()) });

/// FILTER RATE
	var rateFilterRemoteFn = function(request, response) {
		var ajaxRequest = GetHttpObject();
		ajaxRequest.onreadystatechange = function(responseFalse, displayedError) {
			if(ajaxRequest.readyState === 4) {
				var searchTerm = (request.term) ? request.term.toLowerCase() : "";
				
				var result = [];
				var rawData = JSON.parse(ajaxRequest.responseText).searchRateResult.posNameSearchResult;
				if(!rawData) {
					rawData = [];
				}
				else if(!Array.isArray(rawData)) {
					rawData = [ rawData ];
				}
				
				var includeRateTypes = false;
				var item, idx = -1, len = rawData.length;
				if(len <= 0) {
					result.push({
						label: "",
						value: "",
						desc: noRateFilterTerm,
						category: rateLbl
					});
					
					includeRateTypes = true;
				}
				else {
					while(++idx < len) {
						item = rawData[idx];
						result.push({
							label: item.name,
							value: item.randomId,
							desc: item.label,
							category: rateLbl,
							isRate: true
						});
					}
				}
				
				if(includeRateTypes) {
					idx = 0, len = rateTypes.length;
					while(++idx < len) {
						item = rateTypes[idx];
						if((searchTerm.length <= 0) || (item.label.toLowerCase().indexOf(searchTerm) >= 0)) {
							result.push($.extend({
								category: typeLbl,
								isRateType: true
							}, item));
						}
					}
				}
				
				response(result);
			}
		};
		ajaxRequest.open("POST", LOC.rateSearch, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxRequest.send("search="+request.term);
	};
	
	$("#rateAC").autoselector({
		"isComboBox": true, 
		"invalidValue": -1,
		"persistToHidden": false,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"remoteFn": rateFilterRemoteFn
	});
	$("#rateAC").on("itemSelected", function(ui, selectedItem) {
		var $rateList = $("#rateList");
		
		if(selectedItem.isRateType) {
			$("#rateACVal").val(selectedItem.value);
			$rateList.paginatetab("reloadData");
		}
		else if(selectedItem.isRate) {
			var $rate = $rateList.find("li#rt_" + selectedItem.value);
			if($rate.length > 0) {
				$rate.trigger("click");
			}
			else {
				$rateList.paginatetab("reloadPageWithItem", { "id": selectedItem.value }, function() {
					$rateList.find("li#rt_" + selectedItem.value).trigger('click');
				});
			}
		}
	});
	
/// VIEW RATE

	$(document.body).on("click", "#rateList > span > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var rateID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ rateSelectionCheck(rateID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ rateSelectionCheck(rateID, actionEvent); }});


/// EDIT RATE

	$("#rateList").on("click", "a.edit", function(event) {
		event.preventDefault();
		testFormEdit($("#rateSettings"));
		var rateID = $(this).attr("id");
		if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){rateSelectionCheck(rateID, "edit");}); }
		else{ rateSelectionCheck(rateID, "edit"); } });
	
	
//// DELETE RATE

	$("#rateList").on("click", "a.delete", function(event){
		event.preventDefault();
		var rateID = $(this).attr("id");
		rateSelectionCheck(rateID, "delete");
	});
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var rateID = "fubar_"+$("#contentID").val();
		rateSelectionCheck(rateID, "delete");
	});


//// ADD RATE
	
	$("#btnAddRates").on('click', function(event){
		event.preventDefault(); //testFormEdit($("#locationSettings"));
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); })
			resetRateForm("", "add"); }
	});
	
	
//// RATE FORM FUNCTIONALITY	
	
	$("#rateSettings").on("click", "#flatRateCheckLabel", function(event){ 
		if($("#INC-flatRateCheckHidden").val() === "false"){
			$("#INC-startTime, #INC-endTime").val("12:00 AM");
			$("#INC-startTimeVal, #INC-endTimeVal").val("00:00");
			$("#flatRateDetails").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else{
			$("#flatRateDetails").slideUp('slow', function(){ 
				$(this).find("input").val(""); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });
	
	$("#rateSettings").on("click", "#paymentCCLabel", function(event){ 
		if($("#paymentCCHidden").val() === "false"){ 
			$("#inc01CheckHidden").val(true);
			$("#inc01Check").addClass("checked");
			$("#INC-minsInc01").val(10);
			$("#cardIncrementsArea").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else{ 
			$("#cardIncrementsArea").slideUp('slow', function(){
				$("#INC-lastValueCheck").removeClass("checked");
				$("#INC-lastValueCheckHidden").val(false);
				$(".cardIncrementObj").each(function(index){
					$(this).find("input:hidden").val(false);
					$(this).find(".checkBox").removeClass("checked");
					$(this).find("input:text").val("0");
					if(index === 1){ $(this).find("span").addClass("disabled"); $(this).find("input:text").attr("disabled", true); }
					else if(index > 1){ $(this).find("span, .checkboxLabel").addClass("disabled"); $(this).find("input:text").attr("disabled", true); } }); })
			.animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });

	$("#rateSettings").on("click", "#paymentCouponLabel", function(event){ 
		if($("#paymentCouponHidden").val() === "false"){ 
			$("#couponPrompt").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else{ 
			$("#couponPrompt").slideUp('slow', function(){ $("#promptCoupon").autoselector("reset"); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });

	$("#rateSettings").on("click", "#cardIncrement * .checkboxLabel", function(){ 
		if(!$(this).hasClass("disabled")){
			var parentObj = $(this).parents(".cardIncrementObj");
			var $checkBox = $(this).find(".checkBox");
			var incPos = parentObj.attr("count");
			var nextInc = parentObj.next(".cardIncrementObj");
			
			if(!$checkBox.hasClass("checked")){
				parentObj.find("span").removeClass("disabled");
				parentObj.find("input:text").attr("disabled", false).val(10).trigger('focus');
				nextInc.find(".checkboxLabel").removeClass("disabled"); }
			else{
				parentObj.find("span").addClass("disabled");
				parentObj.find("input:text").attr("disabled", true).val(0);
				$("#cardIncrement").children(".cardIncrementObj").each(function(){
					if($(this).attr("count") > incPos && !$(this).find(".checkboxLabel").hasClass("disabled")){ 
					 if($(this).find(".checkBox").hasClass("checked")){ $(this).find(".checkboxLabel").trigger('click'); }
					 $(this).find(".checkboxLabel").addClass("disabled");
					 $(this).find("span").addClass("disabled");
					 $(this).find("input:text").attr("disabled", true).val(0); }}) }} });
	
	$("#INC-startTime, #INC-endTime").autominutes(optsTime);

	$("#INC-startTimeVal, #INC-endTimeVal").on('change', function(){timeValidation = true; validateTime($(this));});

	$("#INC-maxPayment, #INC-minPayment").on('change', function(){ if($("#INC-maxPayment").val() > 0 && $("#INC-maxPayment").val() < $("#INC-minPayment").val()){ alertDialog(maxMinPayErrorMsg); } });

	$("#rateSettings").on("click", "#addTimeLabel", function(event){ 
		if($("#addTimeHidden").val() === "false"){ 
			$("#addTimeDetailArea").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else{ 
			$("#addTimeDetailArea").slideUp('slow', function(){ 
				$(this).find("input:hidden").val(false);
				$(this).find(".checkBox").removeClass("checked");
				$("#xbpArea").hide();
				$(this).find("input:text").val(""); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });
	

	$("#rateSettings").on("click", "#ADT-XBPhoneLabel", function(event){ 
		if($("#ADT-XBPhoneHidden").val() === "false"){ 
			$("#xbpArea").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else{ 
			$("#xbpArea").slideUp('slow', function(){ 
				$(this).find("input:text").val(""); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });


//// SAVE RATE

	$("#rateSettings").on("click", ".save", function(event){ event.preventDefault(); $("#rateSettings").trigger('submit'); });

	$("#rateSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });

	$("#rateSettings").on('submit', function(event){
		event.preventDefault();
		var rateID = $("#rateSettings").find("#formRateID").val();
		var params = $.param(serializeForm("#rateSettings"));
		var ajaxSaveRate = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxSaveRate.onreadystatechange = function(){
			if (ajaxSaveRate.readyState==4 && ajaxSaveRate.status == 200) {
				var response = ajaxSaveRate.responseText.split(":");
				if(response[0] == "true") {
					noteDialog(rateSavedMsg, "", "", "", true);
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "itemID");
					var newLocation = (rateID !== "")? location.pathname+"?"+cleanQuerryString+"&itemID="+rateID : location.pathname+"?"+cleanQuerryString;
					setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
			else if(ajaxSaveRate.readyState==4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveRate.open("POST",LOC.saveRates,true);
		ajaxSaveRate.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveRate.send(params);
	}); 	}

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

function getTempProfileID(callback){ $.ajax({ url:LOC.addProfile+"?"+document.location.search.substring(1), dataType: "text", success: function(data) {
	$("#profileID, #contentID").val(data);
	
	setupInEditAlertCancelFn(function(callbackFn) {
		cancelRateProfileForm(data, callbackFn);
	});
	
	callback();
} }); }

//-------------------------------------------------------------------------------------

function profileAutoComplete(request, response) {
	var ajaxProfileLists = GetHttpObject();
//	var querryCustID = (typeof curCustomerID === "undefined")? "" : "&customerID="+curCustomerID ;
	ajaxProfileLists.onreadystatechange = function(responseFalse, displayedError) {
		if(ajaxProfileLists.readyState === 4) {
			var responseOptions = new Array(), profileNameResponse = new Array();
			var data = JSON.parse(ajaxProfileLists.responseText);
			var itemDesc = "";
			
			var allProfileItem = {label: allProfilesMsg, value: "all"}
						
			if(data.searchRateProfileResult.posNameSearchResult instanceof Array)
			{ profileNameResponse = $.map(data.searchRateProfileResult.posNameSearchResult, function(item) {return{ 
				label: item.name, 
				value: item.randomId, 
				desc: item.label, 
				category: profileCatLbl
			}}); } 
			else if(data.searchRateProfileResult.posNameSearchResult)
			{ profileNameResponse = [{
				label: data.searchRateProfileResult.posNameSearchResult.name, 
				value: data.searchRateProfileResult.posNameSearchResult.randomId, 
				desc: data.searchRateProfileResult.posNameSearchResult.label, 
				category: profileCatLbl
			}]; }
			
			if(profileNameResponse.length === 0 && request.term === "")
			{ profileNameResponse = [{
				label: "", 
				value: "-1", 
				desc: noFilterTerm, 
				category: profileCatLbl }]; }
			
			var searchTerm = request.term.trim().toLowerCase();
			var statusModeOpts= JSON.parse($("#statusModeObjData").text());
			
			responseOptions = responseOptions.concat(allProfileItem,profileNameResponse,statusModeOpts);
			response(responseOptions);
			
		}
	};
	ajaxProfileLists.open("POST", LOC.profileSearch, true);
	ajaxProfileLists.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxProfileLists.send("search="+request.term);
}

//-------------------------------------------------------------------------------------

function rateAutoComplete(request, response) {
	var ajaxRateLists = GetHttpObject();
//	var querryCustID = (typeof curCustomerID === "undefined")? "" : "&customerID="+curCustomerID ;
	ajaxRateLists.onreadystatechange = function(responseFalse, displayedError) {
		if(ajaxRateLists.readyState === 4) {
			var rateNameResponse = new Array();
			var data = JSON.parse(ajaxRateLists.responseText);
			var itemDesc = "";
									
			if(data.searchRateResult.posNameSearchResult instanceof Array)
			{ rateNameResponse = $.map(data.searchRateResult.posNameSearchResult, function(item) {return{ 
				label: item.name, 
				value: item.randomId, 
				//desc: , 
				category: item.label
			}}); } 
			else if(data.searchRateResult.posNameSearchResult)
			{ rateNameResponse = [{
				label: data.searchRateResult.posNameSearchResult.name, 
				value: data.searchRateResult.posNameSearchResult.randomId, 
				//desc: , 
				category: data.searchRateResult.posNameSearchResult.label
			}]; }
			
			if(rateNameResponse.length === 0 || request.term === "")
			{ rateNameResponse = [{
				label: "", 
				value: "-1", 
				desc: noRateFilterTerm }]; }
				
			response(rateNameResponse);
		}
	};
	ajaxRateLists.open("POST", LOC.rateSearch, true);
	ajaxRateLists.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxRateLists.send("search="+request.term);	}

//-------------------------------------------------------------------------------------

function addRateRateProfile(event, objWindow, status){
	var originalStartDate = $("#startDate").datepicker("getDate");
	if($("#endTime").autominutes("getTimeInMinutes") === 0) {
		$("#endDate").datepicker("setDate", new Date(originalStartDate.getTime() + 86400000));
	}
	
	var params = $.param(serializeForm("#rateRateProfileForm"));
	var ajaxSaveRateRateProfile = GetHttpObject({ "triggerSelector": (event) ? event.target : null, "postTokenSelector": "#rateRateProfilePostToken" });
	ajaxSaveRateRateProfile.onreadystatechange = function(){
		if (ajaxSaveRateRateProfile.readyState==4 && ajaxSaveRateRateProfile.status == 200) {
			var response = ajaxSaveRateRateProfile.responseText.split(":");
			if(response[0] == "true") {
				$("#mainContent").addClass("edit");
				
				var $calendar = $("#calendarView");
				var rateRateProfile = toRateRateProfile({}, "#rateRateProfileForm", "wrappedObject.");
				var $thisWindow = objWindow;
				replaceEvents($calendar, rateRateProfile, decorateRateProfileEvent);
				if(rateRateProfile.useScheduled) {
					$calendar.fullCalendar("gotoDate", moment(rateRateProfile.availabilityStartDate, __FMT_FORM_DATE));
				}
				
				msgPrint = (status && status == "edit")? rateUpdatedMsg : rateAddedMsg;
				noteDialog(msgPrint, "timeOut", 1500, function(){ $thisWindow.dialog("close"); $thisWindow.removeClass("edit"); }, true);
			}
			else {
				$("#endDate").datepicker("setDate", originalStartDate);
			}
		}// new location Saved.
		else if(ajaxSaveRateRateProfile.readyState==4){
			alertDialog(failedSaveMsg);
			$("#endDate").datepicker("setDate", originalStartDate);
		}
	};// Location changes and/or submission is unable to Save.
	
	ajaxSaveRateRateProfile.open("POST",LOC.saveRate2Profile,true);
	ajaxSaveRateRateProfile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxSaveRateRateProfile.setRequestHeader("Content-length", params.length);
	ajaxSaveRateRateProfile.setRequestHeader("Connection", "close");
	ajaxSaveRateRateProfile.send(params); }

//-------------------------------------------------------------------------------------

function resetRateProfileForm(itemID, eventAction, rateData){
	$("#profileDetails").fadeOut("fast", function(){
		$(document.body).scrollTop(0);
		$("#profileSettings").trigger("reset");
		$("#formOperatingMode").autoselector("reset");
		//$("#calendarTrash").append("body");
		$("#calendarView").fullCalendar('destroy');
		if(eventAction === "add"){
			getTempProfileID(function() {
				$("#actionFlag").val("0");
				$("#saveDraftBtn").show();
				$("#profileDetails").removeClass("editForm").addClass("form addForm").fadeIn(function(){ $("#formRateName").trigger('focus'); showRateCalendar(eventAction); });
				$("#mainContent").removeClass("edit");
				testFormEdit($("#rateSettings"));
			});
		}
		else{ showRateDetails(rateData, itemID, eventAction); } }); }

//-------------------------------------------------------------------------------------

function extractRateProfileList(rateList, context) {
	var buffer = JSON.parse(rateList).rateProfileListInfo;
	var result = buffer.elements;
	if(!Array.isArray(result)) {
		result = [ result ];
	}	
	context.dataKey = buffer.dataKey;
	
	return result;
}

//-------------------------------------------------------------------------------------

function loadRateProfileList(){
	$("#rateProfileList").paginatetab({
		"url": "/secure/settings/rates/rateProfileList.html",
		"itemLocatorURL": "/secure/settings/rates/locatePageWithRateProfile.html",
		"formSelector": "#rateProfileFilterForm",
		"submissionFormat": "POST",
		"emptyMessage": noRateProfileMsg,
		"rowTemplate": unescape($("#rateProfileListTemplate").html()),
		"responseExtractor": extractRateProfileList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData"); }

//-------------------------------------------------------------------------------------

function showRateCalendar(eventAction) {
	ogCalHeight = (ogCalHeight === "")? $("#calendarView").height()-25 : ogCalHeight ;
	var	editableOpt = (eventAction === "display")? false : true,
		selectableOpt = (eventAction === "display")? false : true,
		calHeight = (eventAction === "display")? ogCalHeight : ogCalHeight-50,
		calWidthDif = $("#calendarView").width()/2;
	
	$("#calendarView").height(calHeight);
	
	
//	var $calendarTrash = $('<img id="calendarTrash" width="18" height="18" border="0" src="/images/spacer.gif" alt="' + dragToDeletRateRateProfileeLbl + '" title="' + dragToDeletRateRateProfileeLbl + '" style="position: absolute; left: 0px; top: 0px;">');
//	var $calendarTrash = $('<div id="calendarTrash" title="' + dragToDeletRateRateProfileeLbl + '" alt="' + dragToDeletRateRateProfileeLbl + '" style="position: absolute; left: 0px; top: 0px;"><a class="trashcan" href="#"><img id="calendarTrash" width="18" height="1" border="0" src="/images/spacer.gif"/></a></div>');
	
	var $calendarTrash = $();
	var calendarTrashTimeoutId = null;
	$("#calendarView").fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		scrollTime: moment(),
		slotDuration: "00:30:00",
		slotEventOverlap: false,
		defaultDate: moment(),
		defaultView: 'agendaWeek',
		editable: editableOpt,
		selectable: selectableOpt,
		height: calHeight,
		aspectRatio: 2,
		select: createEvent,
		eventClick: selectEvent,
		eventResize: resizeEvent,
		eventDrop: moveEvent,
		viewRender: function(view, element) {
			var $container;
			if((view.name === "agendaWeek") || (view.name === "agendaDay")) {
				$container = element.find("tr.fc-slot0>td.fc-widget-content:first");
			}
			else if(view.name === "month") {
				$container = element.find("tr.fc-week.fc-first>td.fc-day:first");
			}

			
			if($container && ($container.length > 0)) {
				$calendarTrash = $("#calendarTrash").clone();
				
				$container.append($calendarTrash);
				$calendarTrash
						.hide()
						.droppable({
							tolerance: "pointer",
							greedy: true,
							drop: function(jsEvent, ui) {
								var event = ui.helper.calendarEvent;
								var rateRateProfile = (event) ? event.rateRateProfileObj : null;
								if(rateRateProfile) {
									deleteRateRateProfile(rateRateProfile.rateRateProfileRandomId, jsEvent);
								}
							}
						});
			}
		},
		eventDragStart: function(event, jsEvent, ui, view) {
			if(event.start && event.end) {
				event.duration = event.end.diff(event.start);
			}
			
			if(calendarTrashTimeoutId) {
				window.clearTimeout(calendarTrashTimeoutId);
			}
			else {
				$calendarTrash
						.css("top", ($("#calendarView").offset().top - $(window).scrollTop())+80 + "px")
						.css("left", ($("#calendarView").offset().left - $(window).scrollLeft())+calWidthDif + "px")
						.fadeIn();
			}
			
			ui.helper.calendarEvent = event;
		},
		eventDragStop: function(event, jsEvent, ui, view) {
			calendarTrashTimeoutId = window.setTimeout(function() {
				$calendarTrash.fadeOut();
				calendarTrashTimeoutId = null;
			}, 2000);
		},
		eventSources: [ loadEvents ]
	});
}

function loadRateDialog(formDsply, event, start, end){
	var profileID = $("#profileSettings").find("#profileID").val();
	if(moment().day(0).diff(start, "days") <= 0){
		var reOccurOnly = (moment().diff(start, "days") <= 0)? false : true ;
		if(formDsply){
			var rateAddress = LOC.addRate;
			var requestQuerry = "&rateProfileID="+profileID+"&startDate="+start.format("MM/DD/YYYY")+"&startTime="+start.format("hh:mm A")+"&startTimeVal="+start.format("HH:mm")+"&endDate="+end.format("MM/DD/YYYY")+"&endTime="+end.format("hh:mm A")+"&endTimeVal="+end.format("HH:mm")+"&duration="+end.diff(start, 'days')+"&reoccurance="+reOccurOnly;
			var rateRateProfileHdr = addRateTtlMsg;
			
			var	rateDialogBtn = {};
				rateDialogBtn.btn1 = { text: addRateBtnMsg, click: function(event, ui) { event.preventDefault(); addRateRateProfile(event, $(this)); } };
				rateDialogBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this)); } };
			} //toDo when Add
		else{
			var rateRateProfileHdr = detailRateTtlMsg;
			var rateRateProfile = event.rateRateProfileObj;
			var rateAddress = LOC.editRate;
			var requestQuerry = "&rateProfileID="+profileID+"&rateRateProfileID="+event.rateRateProfileObj.rateRateProfileRandomId+"&rateName="+event.title;
			
			} // to do when View
		
		var ajaxRateDialog = GetHttpObject();
		ajaxRateDialog.onreadystatechange = function(){
			if (ajaxRateDialog.readyState==4 && ajaxRateDialog.status == 200){
				if($("#rateFormArea.ui-dialog-content").length){ $("#rateFormArea.ui-dialog-content").dialog("destroy"); }
				$("#rateFormArea").html(ajaxRateDialog.responseText);
				
				$("#rateFormArea").find(".delete").on('click', function(jsEvent) { jsEvent.preventDefault();
					var rateRateProfileId = $("#rateFormArea").find("#rateRateProfileForm").find("input[name='wrappedObject.rateRateProfileRandomId']").val();
					deleteRateRateProfile(rateRateProfileId, jsEvent);
				});
	
				$("#rateFormArea").dialog({
					title: rateRateProfileHdr,
					classes: { "ui-dialog": "visibleOverflow" },
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 800,
					height: "auto",
					hide: "fade",
					buttons: rateDialogBtn,
					dragStop: function(){ checkDialogPlacement($(this)); }				
				});
				if(formDsply){ btnStatus(addRateBtnMsg); testPopFormEdit($("#rateRateProfileForm"),$("#rateFormArea"));}
				else if($("#profileDetails").hasClass("form")){ $("#rateFormArea").siblings(".ui-dialog-titlebar").append($("#rateViewMenuButton").html()); }
				
				
				$("#rateFormArea").css('max-height', ($(window).innerHeight()-150)+"px").parent("div").css('top', '83px');
				$("#formRateNameName").trigger('focus');
				} 
			else if(ajaxRateDialog.readyState==4){
				alertDialog(systemErrorMsg); } };//Unable to load details
		ajaxRateDialog.open("GET",rateAddress+"?addForm="+formDsply+requestQuerry,true);
		ajaxRateDialog.send();}
	else{ alertDialog(noScheduleRateBeforNow); }}

function createEvent(start, end, jsEvent, view) {
	loadRateDialog(true, "", start, end);
}

function resizeEvent(event, delta, revertFunc, jsEvent, ui, view) {
	updateEvents(event, delta, revertFunc, jsEvent, ui, view)
}

function moveEvent(event, delta, revertFunc, jsEvent, ui, view) {
	updateEvents(event, delta, revertFunc, jsEvent, ui, view)
}

function selectEvent(event, delta, revertFunc, jsEvent, ui, view) {
	loadRateDialog(false, event); 
}

function loadEvents(scheduleStart, scheduleEnd, timeZone, callback) {
	var $calendar = $("#calendarView");
	removeAllEvents($calendar);
	
	var request = GetHttpObject({
		"showLoader": true
	});
	
	var $profileSettings = $("#profileSettings");
	
	request.onreadystatechange = function(responseFalse, displayedError) {
		if(request.readyState === 4) {
			if(responseFalse || (request.status != 200)) {
				if(!displayedError) {
					alertDialog(systemErrorMsg);
				}
			}
			else {
				var jsonData = parseJSON(request.responseText);
				callback(generateEvents(scheduleStart, scheduleEnd, __FMT_FORM_DATETIME, jsonData.list[0].RateRateProfileEditForm, decorateRateProfileEvent));
			}
		}
	};
	
	request.open("GET", "/secure/settings/rates/rateRateProfileList.html"
				+ "?rateProfileID=" + $profileSettings.find("#profileID").val()
				+ "&startDate=" + encodeURIComponent(scheduleStart.format(__FMT_FORM_DATE))
				+ "&endDate=" + encodeURIComponent(scheduleEnd.format(__FMT_FORM_DATE))
			, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	request.send();
}

function decorateRateProfileEvent(event) {
	var rateSchedule = event.rateRateProfileObj;
	if(rateSchedule.useScheduled) {
		event.className = "rate" + rateSchedule.rateTypeName;
	}
	else {
		event.className = "reOccurEvent rate" + rateSchedule.rateTypeName;
	}
	
	return event;
}

function deleteRateRateProfile(id, jsEvent) {
	$("#mainContent").addClass("edit");
	
	var $calendar = $("#calendarView");
	var originalRateRateProfile = getRateRateProfileFromCalendar($calendar, id);
	
	var rateRateProfile = $.extend({}, originalRateRateProfile);
	rateRateProfile.isToBeDeleted = true;
	
	var confirmMsg = deleteRateRateProfileMsg;
	if(!rateRateProfile.useScheduled) {
		confirmMsg = deleteReoccurenceRateRateProfileMsg;
	}
	
	removeEvents($calendar, id);
	try {
		confirmDialog({
			title: alertTitle,
			message: confirmMsg,
			okCallback: function() {
				deleteServerRateRateProfile($calendar, rateRateProfile);
			},
			cancelCallback: function() {
				replaceEvents($calendar, originalRateRateProfile, decorateRateProfileEvent);
			}
		});
	}
	catch(error) {
		replaceEvents($calendar, originalRateRateProfile, decorateRateProfileEvent);
		
		console.log(error);
	}
}

function deleteServerRateRateProfile($calendar, rateRateProfile) {
	var request = GetHttpObject();
	request.onreadystatechange = function(responseFalse, displayedError) {
		if(request.readyState === 4) {
			if(responseFalse || (request.status != 200)) {
				if(!displayedError) {
					alertDialog(systemErrorMsg);
				}
				replaceEvents($calendar, rateRateProfile, decorateRateProfileEvent);
			}
			else {
				if($("#rateFormArea").is(":visible")){ cancelConfirm($("#rateFormArea.ui-dialog-content")); }
			}
		}
	};
	
	request.open("GET", LOC.deleteRateRateProfile + "?rateRateProfileID=" + rateRateProfile.rateRateProfileRandomId + "&rateProfileID=" + rateRateProfile.rateProfileRandomId, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send();
}

// DON'T TRUST delta !
function updateEvents(event, delta, revertFunc, jsEvent, ui, view) {
	$("#mainContent").addClass("edit");
	
	var rateRateProfile = event.rateRateProfileObj;
	
	var $calendar = $("#calendarView");
	var error = false;
	if(rateRateProfile.useScheduled) {
		updateClientCalendar(event, delta, revertFunc, jsEvent, ui, view, $calendar)
	}
	else {
		if(event.id.split("_")[1] != event.start.isoWeekday()) {
			if(rateRateProfile[__FLAG_ISO_WEEK_DAY[event.start.isoWeekday()]]) {
				error = true;
				
				alertDialog(alreadyReoccuredMsg.replace("{0}", event.start.format("dddd")));
				revertFunc();
			}
		}
		
		if(!error) {
			updateClientCalendar(event, delta, revertFunc, jsEvent, ui, view, $calendar)
		}
	}
}

function updateClientCalendar(event, delta, revertFunc, jsEvent, ui, view, $calendar) {
	var originalRateRateProfile = event.rateRateProfileObj;
	var rateRateProfile = $.extend({}, originalRateRateProfile);
	
	var confirmMessage = null;
	var cstmRevertFunc;
	if(rateRateProfile.useScheduled) {
		event.rateRateProfileObj = rateRateProfile;
		
		rateRateProfile.availabilityStartDate = event.start.format(__FMT_FORM_DATE);
		rateRateProfile.availabilityStartTime = event.start.format(__FMT_TIME);
		if(!event.end) {
			event.end = event.start.clone();
			if(event.duration) {
				event.end.add(event.duration, "ms");
			}
			else {
				event.end.add(1, "d");
			}
		}
		
		rateRateProfile.availabilityEndDate = event.end.format(__FMT_FORM_DATE);
		rateRateProfile.availabilityEndTime = event.end.format(__FMT_TIME);
		cstmRevertFunc = function() {
			event.rateRateProfileObj = originalRateRateProfile;
			revertFunc();
		};
	}
	else {
		confirmMessage = modifyReoccurenceRateRateProfileMsg;
		
		rateRateProfile.availabilityStartTime = event.start.format(__FMT_TIME);
		if(!event.end) {
			rateRateProfile.availabilityEndTime = "00:00";
		}
		else if(event.start.date() != event.end.date()) {
			confirmMessage = modifyReoccurenceAndTrimRateRateProfileMsg;
			
			rateRateProfile.availabilityEndTime = "00:00";
		}
		else {
			rateRateProfile.availabilityEndTime = event.end.format(__FMT_TIME);
		}
		
		var oldDow = parseInt(event.id.split("_")[1], 10);
		var newDow = event.start.isoWeekday();
		if(oldDow !== newDow) {
			rateRateProfile[__FLAG_ISO_WEEK_DAY[oldDow]] = false;
			rateRateProfile[__FLAG_ISO_WEEK_DAY[newDow]] = true;
		}
		
		replaceEvents($calendar, rateRateProfile, decorateRateProfileEvent);
		cstmRevertFunc = function() {
			replaceEvents($calendar, originalRateRateProfile, decorateRateProfileEvent);
		};
	}
	
	delete event.duration;
	
	if(!confirmMessage) {
		updateServerCalendar(rateRateProfile, cstmRevertFunc, jsEvent);
	}
	else {
		confirmDialog({
			title: alertTitle,
			message: confirmMessage,
			okCallback: function() {
				updateServerCalendar(rateRateProfile, cstmRevertFunc, jsEvent);
			},
			cancelCallback: cstmRevertFunc
		});
	}
}

function updateServerCalendar(rateRateProfile, revertFunc, jsEvent) {
	try {
		var $form = $("#rescheduleRateForm");
		$form.get(0).reset();
		toScheduleForm($("#rescheduleRateForm"), "wrappedObject.", rateRateProfile);
		
		var request = GetHttpObject({
			"postTokenSelector": "#rateSchedulePostToken",
			"triggerSelector": (jsEvent) ? jsEvent.target : null
		});
		
		request.onreadystatechange = function(responseFalse, displayedError) {
			if(request.readyState === 4) {
				if(responseFalse || (request.status != 200)) {
					if(!displayedError) {
						alertDialog(systemErrorMsg);
					}
					
					revertFunc();
				}
				else {
					// @ TODO Anything ?
				}
			}
		};
		
		request.open("POST", LOC.rescheduleRateRateProfile, true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send($form.serialize());
	}
	catch(error) {
		revertFunc();
		
		console.log(error);
	}
}

//-------------------------------------------------------------------------------------

var $locAssignForm = $("#LocationAssignmentSmpl").html();

function showLocationAssignment(){	
	$("#publishProfileArea").addClass("edit");
	if(!$("#locationAssignmentArea").is(":visible")){ $("#locationAssignmentArea").show().css("opacity", 1); }
	
	$("#locationAssignmentAreaList").append($locAssignForm);
	
	var $locAssignContent = $("#locationAssignmentArea").find("#locationAssignmentItem");
	
	$locAssignContent.attr("id", $locAssignContent.attr("id")+locCount);
	$locAssignContent.find("form").attr("id", $locAssignContent.find("form").attr("id")+locCount);
	$locAssignContent.find("input").each(function(){ 
		if($(this).hasClass("locationAssignVal")){ var tempID = $(this).attr("id").replace("Val", ""); $(this).attr("id", tempID+locCount+"Val"); } 
		else{ $(this).attr("id", $(this).attr("id")+locCount); }});
	$locAssignContent.find(".menu").attr("id", $locAssignContent.find(".menu").attr("id")+locCount);
	$locAssignContent.find(".ddMenu").attr("id", $locAssignContent.find(".ddMenu").attr("id")+locCount);
	$locAssignContent.find(".selectMenu").each(function(){ var tempID = $(this).attr("id").replace("Expand", ""); $(this).attr("id", tempID+locCount+"Expand"); });
	
	$("#pubProfileID"+locCount).val($("#profileID").val())
	$locAssignContent.find(".operatingModeDisplay").html($("#formOperatingMode").val())
	
	if($("#formOperatingModeVal").val() === "3"){ $locAssignContent.find(".payBySpaceDisplay").show(); }
	else{ $locAssignContent.find(".payBySpaceDisplay").hide(); }
	
	$locAssignContent.css('opacity', 0)
		.slideDown('slow', function(){ 
			if(locCount >= 4){ var newScroll = (locCount+1)*44-195; $("#locationAssignmentAreaList").animate({scrollTop: newScroll},{ queue: false, duration: 'fast'}); } })
		.animate({ opacity: 1 },{ queue: false, duration: 'slow' });
	
	if(parseInt($("#publishProfileArea").parent(".ui-dialog").css("top")) >= 125 && locCount < 4 ){ $("#publishProfileArea").parent(".ui-dialog").animate({"top": "-=25"},{ queue: false, duration: 'slow' }); }


	createDatePicker("#pubStartDate"+locCount, {"minDate": 0,"onClose": function(selectedDate){$("#pubEndDate"+locCount).datepicker( "option", "minDate", selectedDate);} });

	createDatePicker("#pubEndDate"+locCount, {"minDate": 0,"onClose": function(selectedDate){$("#pubStartDate"+locCount).datepicker( "option", "maxDate", selectedDate);} });
	
	var $prevPostToken = $("#pubPostToken" + (locCount-1));
	if($prevPostToken.length > 0) {
		$("#pubPostToken" + locCount).val($prevPostToken.val());
	}
		
	$("#locationAssign"+locCount).autoselector({
		"isComboBox": true, 
		"defaultValue": -1,
		"shouldCategorize": false,
		"blockOnEdit": false,
		"data": JSON.parse($("#locationObjData").text())})
	.on("itemSelected", function(event, ui) { 
		var slctValue = $("#locationAssign"+locCount).autoselector("getSelectedValue");
		var slctLbl = $("#locationAssign"+locCount).autoselector("getSelectedLabel");
		if(slctValue === "-1"){
			if($("#locationAssignmentItem"+locCount).find(".operatingMode").is(":visible")){
				$("#locationAssignmentItem"+locCount).find(".operatingMode, .dateRange, .confirmBtn.save").fadeOut();
				$("#locationAssignmentItem"+locCount).find(".operatingMode").find("input").val("");
				$("#locationAssignmentItem"+locCount).find(".dateRange").find("input").val(""); }}
		else{
			$("#locationAssignmentItem"+locCount).find(".locationNameBoxDsp").prepend(slctLbl)
			if(!$("#locationAssignmentItem"+locCount).find(".operatingMode").is(":visible")){
				$("#locationAssignmentItem"+locCount).find(".operatingMode, .dateRange, .confirmBtn.save").fadeIn(); }}}); }

//-------------------------------------------------------------------------------------

function clearLocationAssignment(){ if($("#locationAssignmentArea").is(":visible")){ $("#assignNowLabel").trigger('click'); }}

//-------------------------------------------------------------------------------------

function publishNow(){
	if($(".confirmBtn.save").is(":visible")){ alertDialog(alertLocationAssignMsg); }
	else{
		var params = $.param(serializeForm("#profileSettings"));
		var ajaxPublishNow = GetHttpObject();
		ajaxPublishNow.onreadystatechange = function(){
			if (ajaxPublishNow.readyState==4 && ajaxPublishNow.status == 200) {
				var response = ajaxPublishNow.responseText.split(":");
				if(response[0] == "true") {
					noteDialog(profilePublishedMsg, "", "", "", true);
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "itemID");
					var newLocation = location.pathname+"?"+cleanQuerryString+"&itemID="+response[2];
					setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
			else if(ajaxPublishNow.readyState==4){ 
				alertDialog(failedPublishMsg); } };
		ajaxPublishNow.open("POST",LOC.publishProfile,true);
		ajaxPublishNow.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxPublishNow.setRequestHeader("Content-length", params.length);
		ajaxPublishNow.setRequestHeader("Connection", "close");
		ajaxPublishNow.send(params);}}

//-------------------------------------------------------------------------------------

function assignLocationNow($saveObj){
	var $formRowObj = $saveObj.parents(".assignmentForm");
	var formID = $formRowObj.find("form").attr("id");
	var $assignRowObj = $saveObj.parents(".locationAssignmentItem");
	
	var params = $.param(serializeForm("#"+formID));
	var ajaxAssignLocation = GetHttpObject({"postTokenSelector": "input[id^=pubPostToken]"});
	ajaxAssignLocation.onreadystatechange = function(responseFalse, displayedMessages){
		if (ajaxAssignLocation.readyState==4 && ajaxAssignLocation.status == 200){
			var response =  ajaxAssignLocation.responseText.split(":");
			if(!responseFalse){
				$formRowObj.find(".locationInstncID").val(response[2]);
				$formRowObj.animate({opacity: "0.1"}, function(){ 
					$assignRowObj.removeClass("form").addClass("display"); 
					$saveObj.hide().siblings(".confirmation_icn, .menu").show(); 
					if($("#formOperatingModeVal").val() === "3"){
						if($(this).find(".payBySpaceDisplay").val() === ""){
							$(this).find(".spaceRange").html(allSpacesMsg); }
						else{
							$(this).find(".spaceRange").html($(this).find(".payBySpaceDisplay").val());}}
					$(this).find("span.startDate").html($(this).find("input.startDate").val());
					if($(this).find("input.endDate").val() === ""){
						$(this).find("span.toMsg, span.endDate").hide(); }
					else{
						$(this).find("span.endDate").html($(this).find("input.endDate").val()); 
						$(this).find("span.toMsg, span.endDate").show(); }
					$(this).animate({opacity: "1"},function(){
						if(!$assignRowObj.hasClass("formEdit")){ locCount++; showLocationAssignment(); }}); }); }
			else if(!displayedMessages){
				alertDialog(failedSaveMsg);}} 
		else if(ajaxAssignLocation.readyState==4){
			alertDialog(failedSaveMsg); } };
	ajaxAssignLocation.open("POST",LOC.saveLocation,true);
	ajaxAssignLocation.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxAssignLocation.setRequestHeader("Content-length", params.length);
	ajaxAssignLocation.setRequestHeader("Connection", "close");
	ajaxAssignLocation.send(params);}

//-------------------------------------------------------------------------------------

function removeLocationNow($removeObj){
	var $formRowObj = $removeObj.parents(".locationAssignmentItem").find(".assignmentForm");
	var $assignRowObj = $removeObj.parents(".locationAssignmentItem");

	var params = "rateProfileRandomId="+$formRowObj.find(".profileID").val()+"&randomId="+$formRowObj.find(".locationInstncID").val();
	var ajaxRemoveLoc = GetHttpObject();
	ajaxRemoveLoc.onreadystatechange = function(){
		if (ajaxRemoveLoc.readyState==4 && ajaxRemoveLoc.status == 200){
			var response =  ajaxRemoveLoc.responseText.split(":");
			if(response[0] == "true"){
				$assignRowObj.slideUp('slow', function(){ 
					$assignRowObj.remove(); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); }
			else{
				var failData =	JSON.parse(ajaxRemoveLoc.responseText);
				alertDialog(systemErrorMsg);
				$formRowObj.find(".postToken").val(failData.errorStatus[0].token);}} 
		else if(ajaxRemoveLoc.readyState==4){
			alertDialog(systemErrorMsg); } };
	ajaxRemoveLoc.open("GET",LOC.deletLocation+"?"+params,true);
	ajaxRemoveLoc.send();}


//-------------------------------------------------------------------------------------

function validateProfile(callbackFn) {
	var params = $.param(serializeForm("#profileSettings"));
	var req = GetHttpObject({
		"postTokenSelector": "#postToken"
	});
	req.onreadystatechange = function(responseFalse, displayedMessages) {
		if(req.readyState == 4) {
			if(!responseFalse && (req.status == 200)) {
				callbackFn();
			}
			else if(!displayedMessages) {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	req.open("POST", LOC.validateProfile, true);
	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.setRequestHeader("Content-length", params.length);
	
	req.send(params);
}

//-------------------------------------------------------------------------------------

function publishProfile(){
	locCount = 0;
	var	publishDialogBtn = {};
	publishDialogBtn.btn1 = { text: publishRateProfileBtnMsg, click: function(event, ui) { event.preventDefault(); publishNow(); } };
	publishDialogBtn.btn2 = { text: cancelBtn, click: function(event) { event.preventDefault(); cancelConfirm($(this),"",clearLocationAssignment); } };

	$("#publishProfileArea").dialog({
		title: "Publish Rate Profile",
		classes: { "ui-dialog": "visibleOverflow" },
		modal: true,
		resizable: false,
		closeOnEscape: false,
		width: 800,
		height: "auto",
		hide: "fade",
		buttons: publishDialogBtn,
		dragStop: function(){ checkDialogPlacement($(this)); }				
	});
	btnStatus(publishRateProfileBtnMsg); testPopFormEdit($("#profileAssignmentForm"),$("#publishProfileArea"));  }

//-------------------------------------------------------------------------------------

function deleteProfile(itemID){
	var ajaxDeleteRate = GetHttpObject();
	ajaxDeleteRate.onreadystatechange = function(){
		if (ajaxDeleteRate.readyState==4 && ajaxDeleteRate.status == 200){
			if(ajaxDeleteRate.responseText == "true") { 
				noteDialog(rateDeleteMsg, "", "", "", true);
				var newLocation = location.pathname+"?"+filterQuerry;
				setTimeout(function(){window.location = newLocation}, 800); }
			else { alertDialog(systemErrorMsg); }} 
		else if(ajaxDeleteRate.readyState==4){ alertDialog(systemErrorMsg); } };
	ajaxDeleteRate.open("GET",LOC.deleteProfile+"?rateProfileID="+itemID,true);
	ajaxDeleteRate.send(); }

function verifyDeleteProfile(itemID){
	var req = GetHttpObject();
	req.onreadystatechange = function(responseFalse, displayedError, displayedMessage) {
		if(req.readyState == 4) {
			if((!responseFalse) && (req.status == 200)) {
				if(displayedMessage) { deleteProfile(itemID); }
				else {
					var deleteRateButtons = {
						"btn1": {
							text : deleteBtn,
							click : function() { deleteProfile(itemID); $(this).scrollTop(0); $(this).dialog("close"); } },
						"btn2": {
							text : cancelBtn,
							click :	function() { $(this).scrollTop(0); $(this).dialog("close"); } } };
					
					if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
					$("#messageResponseAlertBox")
							.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteProfileMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteRateButtons });
					btnStatus(deleteBtn); }}}};	
	req.open("GET", LOC.verifyDeleteProfile+"?rateProfileID="+itemID, true);
	req.send();
}

//-------------------------------------------------------------------------------------

function cancelRateProfileForm(rateProfileId, callbackFn) {
	var ajaxRequest = GetHttpObject();
	ajaxRequest.onreadystatechange = function(responseFalse, displayedMessage) {
		if(ajaxRequest.readyState == 4) {
			if(!responseFalse) {
				if(typeof callbackFn === "function") {
					callbackFn();
				}
			}
			else if(!displayedMessage) {
				alertDialog(systemErrorMsg);
			}
		}
	};
	
	ajaxRequest.open("GET", LOC.cancelProfileForm + "?rateProfileID=" + rateProfileId, true);
	ajaxRequest.send();
}

//-------------------------------------------------------------------------------------

function loadProfile(itemID, eventAction){
	$("#profileDetails").fadeOut("fast", function(){
		var $currentObj = $("#rpt_"+itemID);
		$("#actionFlag").val("1");
		
		$("#contentID, #profileID").val(itemID);
		$("#profileName, #detailProfileName").html($currentObj.attr("title"));
		$("#formRateProfileName").val($currentObj.attr("title"));
		
		var operatingMode = ($("#formOperatingMode").autoselector("findByValue", $currentObj.attr("opMode")));
		$("#detailProfileOpMode").html(operatingMode.label);
		$("#formOperatingMode").autoselector("setSelectedValue", $currentObj.attr("opMode"));
		
		if(eventAction === "display"){
			$("#profileDetails").removeClass("editForm form addForm").fadeIn(function(){ showRateCalendar(eventAction); }); }
		else if(eventAction === "edit"){
			if($("#rpt_"+itemID).hasClass("hddn-true")){ $("#saveDraftBtn").show(); }
			else{ $("#saveDraftBtn").hide(); }
			testFormEdit($("#profileSettings"));
			$.ajax({
				url: LOC.editProfile+"?"+"rateProfileID="+itemID+"&"+document.location.search.substring(1),
				success: function(data, status, jqXHR) {
					$("#profileDetails").removeClass("addForm").addClass("form editForm").fadeIn(function(){
						$("#formRateProfileName").trigger('focus');
						setupInEditAlertCancelFn(function(callbackFn) {
							cancelRateProfileForm(itemID, callbackFn);
						});
						
						showRateCalendar(eventAction);
					});
				}
			});
		}
	});
}

//-------------------------------------------------------------------------------------

function profileSelectionCheck(selectedID, eventAction){
	var itemID = selectedID.split("_");
	if(itemID instanceof Array === true){ itemID = itemID[1]; }
	var $this = $("#rpt_"+itemID);
	if(eventAction === "delete"){ verifyDeleteProfile(itemID); }
	else if(eventAction === "hide"){ 
		$this.toggleClass("selected"); $("#profileDetails").fadeOut(); $("#contentID").val(""); $("#mainContent").removeClass("edit"); }
	else { if($("#contentID").val() !== itemID || (eventAction === "edit" && !$(".mainContent").hasClass("form")) || (eventAction === "display" && $(".mainContent").hasClass("form"))){
			if($("#rateProfileList").find("li.selected").length){ $("#rateProfileList").find("li.selected").toggleClass("selected"); }
			$this.toggleClass("selected");
			$("#calendarView").fullCalendar('destroy');					
			if(eventAction == "edit" && $("#contentID").val() === itemID){
				$("#profileDetails").fadeOut(function(){
					$(".mainContent").addClass("form editForm");
					testFormEdit($("#profileSettings"));
					$.ajax({ url:LOC.editProfile+"?"+"rateProfileID="+itemID+"&"+document.location.search.substring(1) });
					if($("#rpt_"+itemID).hasClass("hddn-true")){ $("#saveDraftBtn").show(); }
					else{ $("#saveDraftBtn").hide(); }
					$("#profileDetails").fadeIn(function(){ showRateCalendar(eventAction); }); }); }
			else if(eventAction == "display" && $("#contentID").val() === itemID){
				$("#profileDetails").fadeOut(function(){$(".mainContent").removeClass("form editForm"); $("#mainContent").removeClass("edit"); 
				$("#profileDetails").fadeIn(function(){ showRateCalendar(eventAction); });}); }
			else {loadProfile(itemID, eventAction); }}} }
	
//-------------------------------------------------------------------------------
	
function rateProfile(updateFilter){
	
	if(!updateFilter){ loadRateProfileList(); }

	$("#formOperatingMode").autoselector({
		isComboBox: true,
		defaultValue: -1,
		shouldCategorize: false,
		data: JSON.parse($("#formOperatingModeData").text()) });
		
		$("#rateProfileAC").autoselector({
			"isComboBox": true, 
			"defaultValue": null,
			"persistToHidden": false,
			"shouldCategorize": true,
			"blockOnEdit": true,
			"remoteFn": profileAutoComplete })
		.on("itemSelected", function(event, ui) {
			var slctdCategory = $(this).autoselector("getSelectedObject").category;
			var slctValue = $(this).autoselector("getSelectedValue");
			if(slctValue != '-1') {
				if(slctdCategory === profileCatLbl){ 
					$("#rpt_"+slctValue).addClass("selected");
					profileSelectionCheck("_"+slctValue, "display");
					$("#profileStatusACVal").val(''); $("#profileTypeACVal").val(''); }
				else {
					if(slctdCategory === statusCatLbl){ 
						$("#profileStatusACVal").val(slctValue); $("#profileTypeACVal").val('');}
					else if(slctdCategory === opModeCatLbl){ 
						$("#profileStatusACVal").val(''); $("#profileTypeACVal").val(slctValue); }
					else{ 
						$("#profileStatusACVal").val(''); $("#profileTypeACVal").val(''); }
					if($(".menuList > li").is(":visible")){ $("#rateProfileList").paginatetab("reloadData"); }
					else{ loadRateProfileList(); } }
				if($(this).parents("ul.filterForm") && slctValue != ""){ filterQuerry = "&filterValue="+slctValue; }
				$(this).trigger('blur'); }
			return false; })
		.on("change", function(){
			var slctName = $(this).autoselector("getSelectedObject");
			if($(this).val()==="" && slctName.label !==""){$(this).val(slctName.label);} });

/// VIEW RATE PROFILE

	$(document.body).on("click", "#rateProfileList * li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var profileID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ rateSelectionCheck(profileID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ profileSelectionCheck(profileID, actionEvent); }});


/// EDIT RATE PROFILE

	$("#rateProfileList").on("click", "a.edit", function(event) {
		event.preventDefault();
		var profileID = $(this).attr("id");
		if($("#mainContent").hasClass("edit")){ inEditAlert("site", function(){rateSelectionCheck(profileID, "edit");}); }
		else{ profileSelectionCheck(profileID, "edit"); } });
	
	
//// DELETE RATE PROFILE

	$("#rateProfileList").on("click", "a.delete", function(event){
		event.preventDefault();
		var profileID = $(this).attr("id");
		profileSelectionCheck(profileID, "delete");
	});
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var profileID = "fubar_"+$("#contentID").val();
		profileSelectionCheck(profileID, "delete");
	});

//// ADD RATE PROFILE
	
	$("#btnAddRateProfile").on('click', function(event){
		event.preventDefault(); //testFormEdit($("#locationSettings"));
		testFormEdit($("#profileSettings"));
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); })
			resetRateProfileForm("", "add"); }
	});
	

	
//// RATE FORM FUNCTIONALITY	
	
	$(document.body).on("click", "#useXpryTimeLbl", function(event){ 
		if($("#useXpryTimeHidden").val() === "true"){
			$("#xpryTime").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' });
			$("#formXpryDay").autoselector("reset"); $("#xpryTm").val("12:00 AM"); $("#xpryTmVal").val("00:00"); }
		else{
			$("#xpryTime").slideUp('slow', function(){ 
				$(this).find("input").val(""); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });
	var	startWdth = 0,
		startPddng = new Array(2),
		startMrgn = new Array(2),
		endWdth = 0,
		endPddng = new Array(2),
		endMrgn = new Array(2),
		nxtDyLbl = false;
	$(document.body).on("click", ".scheduledRdio", function(event){ 
		if($("#scheduled").val() === "false" && !$("#reoccuringArea").is(":visible")){
			startWdth = $("#startDate").width();
			startPddng[0] = $("#startDate").css("padding-left");
			startPddng[1] = $("#startDate").css("padding-right");
			startMrgn[0] = $("#startDate").css("margin-left");
			startMrgn[1] = $("#startDate").css("margin-right");
			$("#startDate").animate({width: "0px", paddingLeft: 0, marginLeft: 0, paddingRight: 0, marginRight: 0, opacity: 0}, 500, function(){ $(this).hide(); });
			if($("#nxtDayLabel").is(":visible")){ nxtDyLbl = true; }
			else if($("#endDate").is(":visible")){
				endWdth = $("#startDate").width();
				endPddng[0] = $("#startDate").css("padding-left");
				endPddng[1] = $("#startDate").css("padding-right");
				endMrgn[0] = $("#startDate").css("margin-left");
				endMrgn[1] = $("#startDate").css("margin-right");
				$("#endDate").animate({width: "0px", paddingLeft: 0, marginLeft: 0, paddingRight: 0, marginRight: 0, opacity: 0}, 500, function(){ $(this).hide(); if($("#endTime").val() === "12:00 AM"){ $("#nxtDayLabel").fadeIn(); } }); }
			
			$("#reoccuringArea").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
		else if($("#scheduled").val() === "true"){
			if(startWdth <= 0) {
				$("#startDate").fadeIn();
			}
			else {
				$("#startDate").show().animate({width: startWdth+"px", paddingLeft: startPddng[0], marginLeft: startMrgn[0], paddingRight: startPddng[1], marginRight: startMrgn[1], opacity: 1});
			}
			
			if($("#startDate").val() !== $("#endDate").val()){
				if($("#nxtDayLabel").is(":visible") && !nxtDyLbl){ $("#nxtDayLabel").fadeOut();
				if(endWdth <= 0) {
					$("#endDate").fadeIn();
				}
				else {
					$("#endDate").show().animate({width: endWdth+"px", paddingLeft: endPddng[0], marginLeft: endMrgn[0], paddingRight: endPddng[1], marginRight: endMrgn[1], opacity: 1});
				}
			}}
			$("#reoccuringArea").slideUp('slow', function(){ $(this).find("input").val(""); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });
	
	$(document.body).on("change", "#endTimeVal, #endTime", function(){
		var timeCheck = "00:00";
		if($(this).attr("id") === "endTime"){ timeCheck = "12:00 AM"; }
		if($(this).val() === timeCheck){ 
			if(!$("#endDate").is(":visible")){ $("#nxtDayLabel").fadeIn(); $("#endDate").val(moment($("#startDate").val()).add('d', 1).format("MM/DD/YYYY")); }}
		else if($("#nxtDayLabel").is(":visible")){ $("#nxtDayLabel").fadeOut(); $("#endDate").val($("#startDate").val()); } });
	
	$(document.body).on("click", "#rateViewBtns > .close", function(event){ event.preventDefault(); cancelConfirm($("#rateFormArea.ui-dialog-content")); }); 
	
	$(document.body).on("click", "#menu_rateRateProfile * .edit", function(event){ 
		event.preventDefault(); 
		$("#rateFormArea").siblings(".ui-dialog-titlebar").find(".ui-dialog-title").fadeOut("slow", function(){
			$(this).html(editRateTtlMsg).fadeIn(); });
		$("#rateRateProfileDetails, #opBtn_rateRateProfile").fadeOut('slow', function(){
			$("#rateEditBtns").show();
			$("#rateRateProfileFormArea").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }, function(){ testPopFormEdit($("#rateRateProfileForm"),$("#rateFormArea")); });}); })
			
	$(document.body).on("click", "#rateEditBtns > .save", function(event){event.preventDefault(); addRateRateProfile(event, $("#rateFormArea.ui-dialog-content"), "edit"); });
	$(document.body).on("click", "#rateEditBtns > .cancel", function(event){event.preventDefault(); cancelConfirm($("#rateFormArea.ui-dialog-content")); });
		
	$(document.body).on("click", "#assignNowLabel", function(event){ event.preventDefault();
		if($("#assignNowHidden").val() === "true"){ showLocationAssignment(); }
		else{ 
			if(parseInt($("#publishProfileArea").parent(".ui-dialog").css("top")) >= 75 ){ 
				var tempTopDif = (locCount >= 3)? "+=100": "+="+(locCount+1)*25; 
				$("#publishProfileArea").parent(".ui-dialog").animate({"top": tempTopDif},{ queue: false, duration: 'slow' }); }
			$("#locationAssignmentArea").slideUp('slow', function(){ 
				$("#locationAssignmentAreaList").html(""); locCount = 0; }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } });

	$(document.body).on("click", ".confirmBtn.save", function(event){ event.preventDefault(); assignLocationNow($(this)); });
	
	$(document.body).on("click", ".btnEditLocation", function(event){ event.preventDefault();
		$(this).parents(".locationAssignmentItem").find(".assignmentForm").animate({opacity: "0.1"}, function(){ 
			$(this).parents(".locationAssignmentItem").removeClass("display").addClass("formEdit"); 
			$(this).find(".confirmation_icn, .menu").hide();
			$(this).find(".save, span.toMsg").show();
			$(this).animate({opacity: "1"}); }); });
	
	$(document.body).on("click", ".btnDeleteLocation", function(event){ event.preventDefault(); removeLocationNow($(this)); });

//// SAVE AS DRAFT RATE PROFILE

	$("#profileFormBtns").on("click", ".save", function(event){ event.preventDefault(); $("#profileSettings").trigger('submit'); });

	$("#profileFormBtns").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload", null, null, function() {
		cancelRateProfileForm($("#profileID").val());
	}); });

	$("#profileFormBtns").on("click", ".publish", function(event){ event.preventDefault(); validateProfile(publishProfile); });

	$("#profileSettings").on('submit', function(event){
		event.preventDefault();
		var params = $.param(serializeForm("#profileSettings"));
		var ajaxSaveRateProfile = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxSaveRateProfile.onreadystatechange = function(){
			if (ajaxSaveRateProfile.readyState==4 && ajaxSaveRateProfile.status == 200) {
				var response = ajaxSaveRateProfile.responseText.split(":");
				if(response[0] == "true") {
					noteDialog(profileSavedMsg, "", "", "", true);
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "itemID");
					var newLocation = location.pathname+"?"+cleanQuerryString+"&itemID="+response[2];
					setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
			else if(ajaxSaveRateProfile.readyState==4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveRateProfile.open("POST",LOC.saveDraft,true);
		ajaxSaveRateProfile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveRateProfile.setRequestHeader("Content-length", params.length);
		ajaxSaveRateProfile.setRequestHeader("Connection", "close");
		ajaxSaveRateProfile.send(params);
	}); 	}
