// JavaScript Document
// EMS 7 GLOBAL VARIABLES AND STANDARD JS

//globalPostToken
//var	globalPostToken = "",
var	filterQuerry = "",
	forceClick = false,
	pausePropagation = false,
	pausePropagationLoop = "",
	crntPageAction = "";

//global variables for resize end tracking
var rtime = new Date(1, 1, 2000, 12,0,0);
var timeout = false;
var delta = 200;
var ogWidth = $(window).width();
var ogHeight = $(window).height();

//global for current day in milliseconds
var	todayMillSec = new Date(),
	todayMillSec = todayMillSec.getTime();
var	oneDayMillSec = 86400000;

//globalTimeFormat map for HighCharts
var timeFormat = { second: '%I:%M:%S %P', minute: '%I:%M %P', hour: '%I:%M %P', day: '%a, %b. %d', week: '%a, %b. %d', month: '%B', year: '%Y' };
var listTimeFormat = { second: '%I:%M:%S %P', minute: '%I:%M %P', hour: '%A, %b. %d <strong>%I:%M %P</strong>', day: '%A, %b. %d', week: '%a %b. %d', month: '%B %Y', year: '%Y' };
//global variable for "for" loops and genral interger count tests
var x = 0;

//global variable for regEx to test for types of characters
var 	characterTypes = new Object();
	characterTypes.uprCase = /[A-Z]/;
	characterTypes.lwrCase = /[a-z]/;
	characterTypes.nmbr = /[0-9]/;
	characterTypes.alphaNumeric = /[^a-z0-9]/i;
	characterTypes.alphaNumericSpace = /[^a-z 0-9]/i;
	characterTypes.spclCharacter = /[!"#$%&'()*+,\-.\\\/:;<=>?@\\[\\\]\^_`{|}~]/;
	characterTypes.nonIntNumeric = /[^0-9]/;
	characterTypes.nonDblNumeric = /[^0-9.]/;
	characterTypes.nonDollarPercentage = /[^0-9 .$%]/;
	characterTypes.numericTimeExpression = /[^0-9 :]/;//^[0-9\:]+$/;
	characterTypes.numericMeridianTimeExpression = /[^0-9 :apm]/i;
	characterTypes.emailInvalidChrctr = /[^a-z0-9.~!#@$%&'`"\^*+\-=?_|\/{}()[\]]/i;
	characterTypes.emailString = /^(([a-z0-9.~!#$%&'`\^*+\-=?_|\/{}]+)|(\"([^"\\]|(\\\\)|(\\"))+\"))(\.(([a-z0-9.~!#$%&'`\^*+\-=?_|\/{}]+)|(\"([^"\\]|(\\\\)|(\\"))+\")))*@((\[[0-9]{1,3}(\.[0-9]{1,3}){3}\])|(\[IPv6:[0-9a-f]{1,4}(:[0-9a-f]{1,4}){7}\])|([a-z0-9]+([a-z0-9-]*[a-z0-9])?(\.+[a-z0-9]([a-z0-9-]*[a-z0-9])?)*))$/i;
	characterTypes.serialNumbers = /[^a-z0-9\n]/i;
	
	characterTypes.locationChrctr = 	/[^a-z0-9 .,]/i;
	characterTypes.couponSearchChrctr = /[^a-z0-9 .,?*]/i;
	characterTypes.couponWithDiscountSearchChrctr = /[^a-z0-9 .,$%?*]/i;
	characterTypes.stndrdChrctr =		/[^a-z0-9 .,:;#@'\-|\/\\{}()]/i;
	characterTypes.lglNameChrctr =	/[^a-z0-9 .,:;#@&'\-|\/\\{}()]/i;
	characterTypes.psChrctr =		/[^a-z0-9 .,:;#@'\-_\/\\{}()]/i;
	characterTypes.extStndrdChrctr =	/[^a-z0-9 .,:;#@$'\-=_|\/\\{}()]/i;
	characterTypes.prntableChrctr =	/[^a-z0-9 .,:;~!#@$%&'`"\^*+\-=?_|\/\\{}()[\]]/i;
	characterTypes.link2GovPass = 	/[^a-z0-9 .,:\-_|#@\/\\{}()]/i;

//global test for Valid Password

var 	menuACdiffOG = 24, menuACwdOG = "", menuACdiff = "", menuACwd = "", menuACdiffWs = "", menuACwdWs = "";

/* Create console.log if couldn't locate one to prevent error */
if(typeof console === "undefined") {
	console = {};
}
if(typeof console.log === "undefined") {
	console.log = function() {
		// DO NOTHING.
	};
}

//global variable for active tab switch
var actvTabSwtch = false;
var tabClick = "";

//global variables for Map Markers
var curMarker = false, markerListDrag = false;

//global preload function for images.
var preloadedIMG = new Array();
$.fn.preload = function() { this.each(function(index){ preloadedIMG[index] = new Image(); preloadedIMG[index].src = this; }); };

function isEmpty(value){
	return !value || value === " " || typeof value === 'undefined' || value.length === 0;
}

function testPasswrd(passwordStrng)
{
	var psswrdResult = true;
	var psswrdTest = new Array(4);
	psswrdTest[0] = characterTypes.uprCase.test(passwordStrng);
	psswrdTest[1] =  characterTypes.lwrCase.test(passwordStrng);
	psswrdTest[2] =  characterTypes.nmbr.test(passwordStrng);
	psswrdTest[3] =  characterTypes.spclCharacter.test(passwordStrng);
	
	for(x = 0; x < psswrdTest.length; x++)
	{
		if(psswrdTest[x] == false){psswrdResult = false;}
	}
	return psswrdResult;
}

//global paystation details
function payStationDetails(id, name, type, lat, lng, serialNmbr, location, severity)
{
	this.psID=id;
	this.name=name;
	this.psType=type;
	this.lat=lat;
	this.lng=lng;
	this.psSN=serialNmbr;
	this.location=location;
	this.severity=severity;
}

//global paystation ICN type arrays
function payStationIcnInfo(id, title, imgName, shdwImgName, iconSize, iconAnchor, popupAnchor, shadowSize, shadowAnchor)
{
	this.psID=id;
	this.title=title;
	this.img=[imgName, imgName+"_alrt1", imgName+"_alrt2", imgName+"_alrt3"];
	this.shdwImg=shdwImgName;
	this.icnSize=iconSize;
	this.iconAnchor=iconAnchor;
	this.popupAnchor=popupAnchor;
	this.shdwSize=shadowSize;
	this.shdwAnchor=shadowAnchor;
}

function mapUserIcnInfo(title, imgName, shdwImgName, iconSize, iconAnchor, popupAnchor, shadowSize, shadowAnchor)
{
	this.title=title;
	this.img=imgName;
	this.shdwImg=shdwImgName;
	this.icnSize=iconSize;
	this.iconAnchor=iconAnchor;
	this.popupAnchor=popupAnchor;
	this.shdwSize=shadowSize;
	this.shdwAnchor=shadowAnchor;
}

var payStationIds = [0, 1, 2, 3, 5, 6, 8, 9, 10, 255];

var psType = null;

var	payStationIcn = new Array(10);
	payStationIcn[0] = new payStationIcnInfo(0, "Other", "Unknown", "UnknownShdw", [25,36], [8,36], [0, -30], [49,26], [8,26]);
	payStationIcn[1] = new payStationIcnInfo(1, "Luke", "Luke", "Luke2Shdw", [23, 50], [7,50], [0, -47], [46, 36], [7,36]);
	payStationIcn[2] = new payStationIcnInfo(2, "Shelby", "Shelby", "ShelbyShdw", [24, 39], [10,39], [0, -36], [51, 29], [10,29]);
	payStationIcn[3] = new payStationIcnInfo(3, "Luke Radius", "LukeRadius", "Luke2Shdw", [23, 50], [7,50], [0, -47], [46, 36], [7,36]);
	payStationIcn[5] = new payStationIcnInfo(5, "Luke II", "Luke2", "Luke2Shdw", [23, 50], [7,50], [0, -47], [46, 36], [7,36]);
	payStationIcn[6] = new payStationIcnInfo(6, "Luke Cosmo", "LukeCosmo", "LukeCosmoShdw", [24, 37], [4,37], [0, -30], [38, 31], [4,31]);
	payStationIcn[8] = new payStationIcnInfo(8, "Test\Demo", "Unknown", "UnknownShdw", [25,36], [8,36], [0, -30], [49,26], [8,26]);
	payStationIcn[9] = new payStationIcnInfo(9, "Virtual Pay Station", "Unknown", "UnknownShdw", [25,36], [8,36], [0, -33], [49,26], [12,26]);
	payStationIcn[10] = new payStationIcnInfo(10, "Verrus", "Unknown", "UnknownShdw", [25,36], [8,36], [0, -30], [49,26], [8,26]);
	payStationIcn[255] = new payStationIcnInfo(255, "Intellepay", "Unknown", "UnknownShdw", [25,36], [8,36], [0, -30], [49,26], [8,26]);
	
var	mapUserIcn = new Array();
	mapUserIcn[0] = new mapUserIcnInfo("User", "User", "UserShdw", [19,49], [10,49], [0, -46], [56,44], [10,44]);
	mapUserIcn[1] = new mapUserIcnInfo("Active User", "User_Active", "UserShdw", [19,49], [10,49], [0, -46], [56,44], [10,44]);
	mapUserIcn[2] = new mapUserIcnInfo("User", "User", "UserShdw", [19,49], [10,49], [0, -46], [56,44], [10,44]);
	mapUserIcn[3] = new mapUserIcnInfo("User", "User", "UserShdw", [19,49], [10,49], [0, -46], [56,44], [10,44]);
	mapUserIcn[4] = new mapUserIcnInfo("Citation", "Citation", "CitationShdw", [25,47], [12,47], [0, -41], [60,39], [12,39]);


var	mapLotIcn = new Array();
	mapLotIcn[0] = new mapUserIcnInfo("Parking Lot", "Lot", "LotShdw", [35,44], [18,44], [0, -42], [67,36], [18,36]);
	mapLotIcn[1] = new mapUserIcnInfo("Parking Lot - Low Availability", "Lot_High", "LotShdw", [35,44], [18,44], [0, -42], [67,36], [18,36]);
	mapLotIcn[2] = new mapUserIcnInfo("Parking Lot - Parking Available", "Lot_Medium", "LotShdw", [35,44], [18,44], [0, -42], [67,36], [18,36]);
	mapLotIcn[3] = new mapUserIcnInfo("Parking Lot - High Availability", "Lot_Low", "LotShdw", [35,44], [18,44], [0, -42], [67,36], [18,36]);


function payStationAlert(title, collTitle, paperTitle, listClass)
{
	this.title=title;
	this.collTitle=collTitle;
	this.pprTitle=paperTitle;
	this.listClass=listClass;
}

//global Paystation alert status values
var	psAlerts = new Array();
	psAlerts[0] = new payStationAlert(alertTitleMsgs[0], collectTitleMsgs[0], paperTitleMsgs[0], "severityNull");
	psAlerts[1] = new payStationAlert(alertTitleMsgs[1], collectTitleMsgs[1], paperTitleMsgs[1], "severityLow");
	psAlerts[2] = new payStationAlert(alertTitleMsgs[2], collectTitleMsgs[2], paperTitleMsgs[2], "severityMedium");
	psAlerts[3] = new payStationAlert(alertTitleMsgs[3], collectTitleMsgs[3], paperTitleMsgs[3], "severityHigh");
	psAlerts[4] = new payStationAlert(alertTitleMsgs[4], collectTitleMsgs[4], paperTitleMsgs[4], "severityHigh");

var	deviceInfo = [{"class":"lgdIn"},{"class":"assigned"},{"class":"lgdIn"},{"class":"lgdOut"}];

var	collectionType = new Array();
	collectionType[0] = "collAll";
	collectionType[1] = "collBill";
	collectionType[2] = "collCoin";
	collectionType[3] = "collCard";

function configurationStat(title, listClass)
{
	this.title=title;
	this.listClass=listClass;
}

//global Hardware Configuration status values
var	cnfgStats = new Array();
	cnfgStats['inactive'] = new configurationStat(cnfgGrpStatusMsgs[0], "statInactive");
	cnfgStats['scheduled'] = new configurationStat(cnfgGrpStatusMsgs[1], "statScheduled");
	cnfgStats['incomplete'] = new configurationStat(cnfgGrpStatusMsgs[2], "statIncomplete");
	cnfgStats['completed'] = new configurationStat(cnfgGrpStatusMsgs[3], "statCompleted");
	cnfgStats['active'] = new configurationStat(cnfgGrpStatusMsgs[4], "statCompleted");
	cnfgStats['unscheduled'] = new configurationStat(cnfgGrpStatusMsgs[5], "statUnScheduled");

//global options for the Dialog window when using this function for an alert window
var genralAlertDialogOptions = { 
						classes: { "ui-dialog": "btnAlert" },
						width: 450,
						maxHeight:  $(window).innerHeight()-150,
						modal: true,
						resizable: false,
						closeOnEscape: false,
						dragStop: function(){ checkDialogPlacement($(this)); }/*,
						open: function(){ $("body").css("overflow", "hidden"); },
						close: function(){ $("body").css("overflow", ""); }*/
						
					};


//global options for the Dialog window when using this function for an alert window
var alertDialogOptions = { 
						classes: { "ui-dialog": "btnAlert" },
						width: 450,
						maxHeight:  $(window).innerHeight()-150,
						modal: true,
						resizable: false,
						closeOnEscape: false,
						buttons: {
							Ok: 	function() {
									$(this).scrollTop(0); $(this).dialog( "close" );
								}
						},
						dragStop: function(){ checkDialogPlacement($(this)); }/*,
						open: function(){ $("body").css("overflow", "hidden"); },
						close: function(){ $("body").css("overflow", ""); }*/
					};

//global options for the Dialog window when using this function for an alert window
var alertNoticeOptions = { 
						classes: { "ui-dialog": "popAlert" },
						modal: true,
						width: 450,
						maxHeight:  $(window).innerHeight()-150,
						position: {my: "center center-100"},
						resizable: false,
						closeOnEscape: true,
						hide: "fade",
						dragStop: function(){ checkDialogPlacement($(this)); }/*,
						open: function(){ $("body").css("overflow", "hidden"); },
						close: function(){ $("body").css("overflow", ""); }*/
					};
					
//global options for the Dialog window for Slow responding Ajax Calls
var loadWindowOptions = { 
						classes: { "ui-dialog": "loadAlert" },
						modal: true,
						width: 300,
						maxHeight:  $(window).innerHeight()-150,
						resizable: false,
						closeOnEscape: false,
						hide: "fade",
						dragStop: function(){ checkDialogPlacement($(this)); }/*,
						open: function(){ $("body").css("overflow", "hidden"); },
						close: function(){ $("body").css("overflow", ""); }*/
					};

//--------------------------------------------------------------------------------------

//GlobalSettings for HighCharts
var	minuteMS = 	60000;
var	hourMS = 		3600000;
var	dayMS = 		86400000;
var	weekMS = 		604800000;
var	colorList = 	new Array();
	colorList[1]=	new Array('#005782');
	colorList[2]=	new Array('#005782', '#003E5E');
	colorList[3]=	new Array('#1E76A2', '#005782', '#003E5E');
	colorList[4]=	new Array('#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[5]=	new Array('#3d89af', '#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[6]=	new Array('#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[7]=	new Array('#95BFD3', '#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[8]=	new Array('#AECEDE', '#95BFD3', '#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[9]=	new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00283C');
	colorList[10]=	new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00334D', '#00283C');
	colorList[11]=	new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#79AEC8', '#5C9CBC', '#3d89af', '#1E76A2', '#005782', '#003E5E', '#00334D', '#00283C');
	colorList[12]=	new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#79AEC8', '#5C9CBC', '#3d89af', '#1E76A2', '#046595', '#005782', '#003E5E', '#00334D', '#00283C');
	colorList[13]= new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#79AEC8', '#5C9CBC', '#3d89af', '#1E76A2', '#046595', '#005782', '#004B70', '#003E5E', '#00334D', '#00283C');
	colorList[14]= new Array('#C6DDE8', '#AECEDE', '#95BFD3', '#79AEC8', '#5C9CBC', '#3d89af', '#1E76A2', '#046595', '#005782', '#004B70', '#003E5E', '#00334D', '#00283C', '#00334D', '#003E5E', '#004B70', '#005782', '#046595', '#1E76A2', '#3d89af', '#5C9CBC', '#79AEC8', '#95BFD3', '#AECEDE' );
	
//--------------------------------------------------------------------------------------

// Opacity levels for select menus to fade to when selected or disabled
var	oplSelected = 0.6,
	oplDisabled = 0.25,
	slctdOpacityLevel = ' style="opacity: '+oplSelected+';"',
	dsabldOpacityLevel = ' style="opacity: '+oplDisabled+';"';

//--------------------------------------------------------------------------------------

//Time Arrays

var openHrs = new Array();
var closeTime = new Array();

var hr = 0;
var mn = 0;
var dayPortion = ["am","pm"];
var days = ["Sun","Mon","Tues","Wed","Thur","Fri","Sat"];

for(hr; hr < 24; hr++)
{
	var hour = hr;
	var ampm = '';
	
	if(hr < 12){ampm = 0;}
	else{ampm = 1;}
	
	if(hour == 0){hour = 12;}
	else if(hour < 10){hour = "0"+hour;}
	else if(hour > 12){hour = hour-12; if(hour < 10){hour = "0"+hour;}}
	
	for(mn; mn <= 45; mn= mn+15)
	{
		if(mn==0) { var minute = "0"+mn; } else { var minute = mn; };
		openHrs.push(hour+":"+minute+" "+dayPortion[ampm]);
	}
	mn = 0;
}


var hr = 0;
var mn = 0;
var nextDay = 0;
var crntDay;

for(crntDay = 0; crntDay < days.length; crntDay++)
{
	var closeHrs = new Array();
	
	if(crntDay < 6){nextDay = crntDay+1;}
	else{nextDay = 0;}

	for(hr; hr < 48; hr++)
	{
		var hour = hr;
		var ampm = '';
		
		if(hr < 12 || (hr >= 24 && hr < 36)){ampm = 0;}
		else{ampm = 1;}
		
		if(hour == 0){hour = 12;}
		else if(hour < 10){hour = "0"+hour;}
		else if(hour > 12 && hour <= 24){hour = hour-12; if(hour < 10){hour = "0"+hour;}}
		else if(hour > 24 && hour <= 36){hour = hour-24; if(hour < 10){hour = "0"+hour;}}
		else if(hour > 36){hour = hour-36; if(hour < 10){hour = "0"+hour;}}
		
		for(mn; mn <= 45; mn= mn+15)
		{
			if(hr == 0 && mn == 0){}
			else{
				if(mn==0) { var minute = "0"+mn; } else { var minute = mn; };
				if(hr >= 24){ closeHrs.push(hour+":"+minute+" "+dayPortion[ampm]+" ("+ days[nextDay] +")"); }
				else{ closeHrs.push(hour+":"+minute+" "+dayPortion[ampm]); }
			}
		}
		mn = 0;
		
	}
	hr = 0;
	closeTime.push(closeHrs);
}

// EXTEND BY PHONE 24Hour TIME OBJ
	var timeSuggest = new Array();
	var hr = 0;
	var mn = 0;
	for(hr; hr < 25; hr++)
	{
		for(mn; mn <= 45; mn= mn+15)
		{
			if(hr<10) { var hour = "0"+hr; } else { var hour = hr; };
			if(mn==0) { var minute = "0"+mn; } else { var minute = mn; };
			if(hr==24 && mn >= 15) { } else {timeSuggest.push(hour+":"+minute);};
		}
		mn = 0;
	}


function testSelections(fullListObj)
{
	if(fullListObj.children("li").length > 0 && (fullListObj.children("li").length === fullListObj.children("li.selected").length)){ 
		fullListObj.siblings(".fullListHeader").find(".checkBox").addClass("checked"); }
	else{ 
		fullListObj.siblings(".fullListHeader").find(".checkBox").removeClass("checked"); }
}

function stringArrayValCleanUp(string, removeStrng)
{
	var tempString = string;
	tempString = tempString.replace(removeStrng, "").replace(",,", ",");
	if(tempString.charAt(0)==","){ tempString = tempString.substring(1); }
	if(tempString.charAt(tempString.length-1)==","){tempString = tempString.substring(0, tempString.length-1);}
	return tempString;
}

//--------------------------------------------------------------------------------------

function mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
{
	this.lat=lat;
	this.long=long;
	this.name=name;
	this.type=type;
	this.id=id;
	this.status=severity;
	this.lastSeen=lastSeen;
	this.batteryVoltage=voltage;
	this.paperStatus=paperStatus;
	this.lastCollection=lastCollection;
	this.runningTotal=runningTotal;
	this.location=location;
	this.serialNumber=serialNumber;
}

//--------------------------------------------------------------------------------------

// Allows ability to move items within an Array
Array.prototype.move = function (old_index, index_chng) {
	var new_index = old_index+index_chng;
	if (new_index >= this.length) {
		var k = new_index - this.length;
		while ((k--) + 1) {
		  this.push(undefined);
		}
	}
	this.splice(new_index, 0, this.splice(old_index, 1)[0]);
	return this;
};

//--------------------------------------------------------------------------------------

//sets Dialog Buttons Classes
function btnStatus(){
	var args = btnStatus.arguments;
	$(".ui-dialog-buttonset").children().each(function(){
		$(this).addClass("btnCancel");
		for (x = 0; x < args.length; x++){
			if($(this).html() == args[x]){
				$(this).removeClass("btnCancel");
				$(this).addClass("active").trigger('focus');} }}); }

/* Because the override doesn't work on ActiveXObject, we have to use this to wrap around to support older IE */
function MSXHR()
{
	this.core = new ActiveXObject("Microsoft.XMLHTTP");
}

// Expose the original "open" and attach "_trackState" to update all the required attribute members.
MSXHR.prototype.open = function(bstrMethod, bstrUrl, varAsync, bstrUser, bstrPassword)
{
	var that = this;
	this.core.onreadystatechange = function() { that._trackState(); };
	this.core.open(bstrMethod, bstrUrl, varAsync, bstrUser, bstrPassword);
};

// Expose the original "send".
MSXHR.prototype.send = function(postParams)
{
	this.core.send(postParams);
};

MSXHR.prototype._trackState = function()
{
	this.readyState = this.core.readyState;
	if (this.readyState == 4)
	{
		this.status = this.core.status;
		this.statusText = this.core.statusText;
		
		this.response = this.core.response;
		this.responseText = this.core.responseText;
		this.responseXML = this.core.responseXML;
	}
	
	if (this.onreadystatechange)
	{
		this.onreadystatechange();
	}
};

// Expose the original "setRequestHeader".
MSXHR.prototype.setRequestHeader = function(key, value)
{
	this.core.setRequestHeader(key, value);
};

//--------------------------------------------------------------------------------------

var xhrLoaderObj = "", delayStep = [];

function Wait4HttpObject(conf)
{
	if(xhrLoaderObj.readyState < 4){
		if(!$("#messageResponseAlertBox").is(":visible")){ confirmDialog({
		title: delayedTitle,
		message: delayedDialogMsg,
		okLabel: waitBtn,
		okCallback: function() { resetSessionActvty(); },
		cancelLabel: dontWaitBtn,
		cancelCallback: function() { 
			delayStep[3] = window.clearInterval(delayStep[3]); 
			xhrLoaderObj.abort(); 
			if($("#ajaxLoad").is(":visible")){ $("#ajaxLoad").dialog("close").dialog("destroy"); }
			if(conf.triggerSelector) {
				conf.triggerSelector.removeClass("inactive");
				delete conf.triggerSelector; }} }); }  }	
}

//--------------------------------------------------------------------------------------
httpObjReuestCount = 0;
function GetHttpObject(conf)
{
	var defaultConf = {
		"postTokenSelector": "#postToken,input[type=hidden][name=postToken]",
		"triggerSelector": null,
		"deferCompletion": false,
		"showLoader": true,
		"wait4BoxDelay": 30000,
		"sessionReset": true
	};
	
	if(!conf) {
		conf = defaultConf;
	}
	else {
		conf = $.extend(defaultConf, conf);
	}
	
	var xhrReq = null;
	if(window.XMLHttpRequest) {
		xhrReq = new XMLHttpRequest();
	} // code for IE7+, Firefox, Chrome, Opera, Safari
	else if(window.ActiveXObject) {
		xhrReq = new MSXHR();
	} // code for IE6, IE5
	
	var xhrObj = xhrReq;
	if(conf.showLoader) {
		xhrLoaderObj = xhrReq;
	}
	
	// If MessageResolver "msgResolver" is presence, we will override the XHR object to add the message & token processes.
	if(typeof msgResolver !== "undefined") {
		// Override original "open" method by buffering it for later call.
		var originalOpen = xhrObj.open;
		xhrObj.open = function() {
			var originalOnreadystatechange = xhrObj.onreadystatechange;
			xhrObj.onreadystatechange = function() {
				var messageInfo = null;
				var responseContext = null;
				if(xhrObj.readyState === 4) {
					if(conf.showLoader) {
						delayStep[3] = window.clearInterval(delayStep[3]);
						if($("#ajaxLoad").is(":visible")){
							if($("#messageResponseAlertBox").is(":visible")){ $("#messageResponseAlertBox").dialog("close").dialog("destroy"); }
							if($("#ajaxLoad").is(":visible")){ $("#ajaxLoad").dialog("close").dialog("destroy"); } } }

					var sessInvalidResp = xhrObj.getResponseHeader("session.invalid");
					if(xhrObj.status == 200 && sessInvalidResp == "true") {
						location.reload();
					}
					else if(xhrObj.status === 200) {
						messageInfo = parseMessageInfo(xhrObj.responseText);
						responseContext = xhrObj.responseContext;
						
						handleResponse({
							"messageInfo": messageInfo,
							"postTokenSelector": conf.postTokenSelector,
							"callbackFn": (typeof conf.onprocesscompleted === "function") ? conf.onprocesscompleted : xhrObj.onprocesscompleted,
							"onreadystatechange": originalOnreadystatechange,
							"responseContext": responseContext,
							"callbackDelayMillisecs": conf.callbackDelayMillisecs
						});
					}
					else {
						if(xhrObj.responseContext && xhrObj.responseContext.polling && (xhrObj.responseContext.polling === true)) {
							var mntrObj = xhrObj.responseContext.requestMonitorObj;
							if(mntrObj && (typeof mntrObj.reset === "function")) {
								mntrObj.reset();
							}
						}
						
						if(typeof originalOnreadystatechange === "function") {
							originalOnreadystatechange(null, false);
						}
					}
				}
				
				if(xhrObj.readyState === 4) {
					if(!conf.deferCompletion) {
						xhrObj.processCompleted();
					}
					if(conf.sessionReset){
						resetSessionActvty();
					}
				}
			};
			
			// Call the buffered "open" method.
			originalOpen.apply(this, arguments);
		};
		
		// Override "send" method.
		var originalSend = xhrObj.send;
		xhrObj.send = function() { 
			var send = true;
			if(conf.triggerSelector) {
				xhrObj._$triggerObj = $(conf.triggerSelector);
				if(xhrObj._$triggerObj.hasClass("inactive")) {
					send = false;
				}
				else {
					xhrObj._$triggerObj.addClass("inactive");
				}
			}
			
			if(send) {
				if(typeof xhrObj.onsend === "function") {
					xhrObj.onsend.call(xhrObj);
				}

				if(xhrObj.readyState === 1) {
					if(conf.showLoader) { 
						delayStep[1] = setTimeout(function(){ if(xhrObj.readyState < 4){ loadOverlay(); }}, 1500);
						delayStep[2] = setTimeout(function(){ if(xhrObj.readyState < 4){ $("#ajaxLoad").find("#loadMessage").html(delayedLoadMsg).css("color", "#555F66"); }}, 15000);
						delayStep[3] = setInterval(function(){ if(xhrObj.readyState < 4){ Wait4HttpObject(conf); } }, conf.wait4BoxDelay);
					}
				}
				
				// Call the buffered "send" method.
				return originalSend.apply(this, arguments);
			}
			else {
				return false;
			}
		};
		
		xhrObj.processCompleted = function() {
			if(xhrObj._$triggerObj) {
				xhrObj._$triggerObj.removeClass("inactive");
				delete xhrObj._$triggerObj;
			}
		};
	}
	
	return xhrObj;
}

function parseMessageInfo(rawResponse) {
	var messageInfo = {
		"error": null,
		"rawResponse": rawResponse
	};
	
	var trimmedResponse = rawResponse.trim();
	var matched = trimmedResponse.match(/^(true|false)(:(.*))?/);
	if(matched) {
		messageInfo.error = (matched[1] && matched[1].toLowerCase() === "false");
		if((typeof matched[2] === "string") && (matched[2].charAt(0) === ":")) {
			var tranlatedResponse = trimmedResponse.split(":");
			if(tranlatedResponse[1] && (tranlatedResponse[1].length > 0)) {
				messageInfo.token = tranlatedResponse[1];
			}
		}
	}
	else if(trimmedResponse.indexOf("errorStatus") > 0) {
		var jsonData = parseJSON(trimmedResponse); // Evaluate message object returned from backend.
		var errorData = jsonData.errorStatus;
		if(errorData) {
			messageInfo.error = true;
			messageInfo.token = errorData.token;
			messageInfo.errorStatus = errorData.errorStatus;
			if(messageInfo.errorStatus) {
				if(!Array.isArray(messageInfo.errorStatus)) {
					messageInfo.errorStatus = [ messageInfo.errorStatus ];
				}
				
				var buffer = messageInfo.errorStatus;
				var idx = buffer.length;
				while(--idx >= 0) {
					buffer[idx].message = unescapeJSON(buffer[idx].message);
				}
			}
		}
	}
	else if (trimmedResponse.indexOf("messages") > 0) {
		var jsonData = parseJSON(trimmedResponse);
		var messageData = jsonData.messages;
		if (messageData) {
			messageInfo = messageData;
			if(messageInfo.messages && (messageInfo.messages.length > 0)) {
				var buffer = messageInfo.messages;
				var idx = buffer.length;
				while(--idx >= 0) {
					buffer[idx] = unescapeJSON(buffer[idx]);
				}
			}
		}
	}
	
	return messageInfo;
}

/*
 * conf = {
 * 		messageInfo: MessageInfo Object
 * 		postTokenSelector: jQuery selector for post tokens
 * 		callbackFn: Function that will be called after everything completed,
 * 		responseContext: The context (placeholder) for processed request, responseFalse, displayedMessages
 * 		requestMonitorObj: Timer that display wait message, please see RequestMonitor
 * }
 */
function handleResponse(conf) {
	var messageInfo = conf.messageInfo;
	var callbackContext = conf.responseContext;
	if(!callbackContext) {
		callbackContext = {};
	}
	
	callbackContext.responseFalse = messageInfo.error;
	callbackContext.displayedMessages = false;
	if(conf.requestMonitorObj) {
		callbackContext.requestMonitorObj = conf.requestMonitorObj;
	}
	
	callbackContext.responseText = conf.messageInfo.rawResponse;
	
	// Update post tokens
	if(messageInfo.token) {
//		globalPostToken = messageInfo.token;
		
		if(conf.postTokenSelector) {
			var tokenInputs = $(conf.postTokenSelector);
			var idx = tokenInputs.length, $postToken;
			while(--idx >= 0) {
				$postToken = $(tokenInputs);
				if($postToken.is(":input")) {
					$postToken.val(messageInfo.token);
				}
			}
		}
	}
	
	var messagesStr = null;
	if(messageInfo.messages) {
		if(Array.isArray(messageInfo.messages)) {
			messagesStr = messageInfo.messages.join("<br/>");
		}
		else {
			messagesStr = messageInfo.messages + "";
		}
	}
	
	if(messageInfo.pollingURI) {
		if(typeof conf.callbackFn !== "function") {
			throw "The 'onprocesscompleted' must be attached to GetHttpObject for proper execution in polling mode !";
		}
		
		callbackContext.polling = true;
		
		if(!callbackContext.requestMonitorObj) {
			callbackContext.requestMonitorObj = new RequestMonitor({
				"delays": [ 1500, 3500, 30000 ]
			});
		}
		
		if(typeof callbackContext.requestMonitorObj.triggerFirstState === "function") {
			callbackContext.requestMonitorObj.triggerFirstState();
		}
		
		if(messagesStr) {
//			noteDialog(messagesStr, "", "");
			if(!callbackContext.messages) {
				callbackContext.messages = [];
			}
			
			callbackContext.messages.unshift(messagesStr);
			
			if((typeof messageInfo.requireManualClose !== "undefined") && (messageInfo.requireManualClose == true)) {
				callbackContext.requireManualClose = true;
			}
		}
		
		if(messageInfo.waitMillisecs <= 10) {
			messageInfo.waitMillisecs = 1000;
		}
		
		window.setTimeout(function() {
			delete callbackContext.responseFalse;
			
			var request = GetHttpObject({
				"showLoader": false,
				"onprocesscompleted": conf.callbackFn,
				"callbackDelayMillisecs": conf.callbackDelayMillisecs
			});
			
			request.responseContext = callbackContext;
			
			request.open("GET", messageInfo.pollingURI,true);
			request.send();
		}, messageInfo.waitMillisecs);
	}
	else {
		if(callbackContext.requestMonitorObj && (typeof callbackContext.requestMonitorObj.reset === "function")) {
			callbackContext.requestMonitorObj.reset();
		}
		
		// Display messages and call callback right after that
		if(messageInfo.errorStatus) {
			callbackContext.displayedMessages = true;
			msgResolver.displaySerMsg(messageInfo.errorStatus);
		}
		
		var callback = conf.onreadystatechange;
		if(typeof callback !== "function") {
			callback = conf.callbackFn;
			if(typeof callback !== "function") {
				callback = function() {};
			}
		}
		
		if(callbackContext.messages) {
			callbackContext.messages.unshift(messagesStr);
			messagesStr = callbackContext.messages.join("<br/>");
			
			if((typeof callbackContext.requireManualClose !== "undefined") && (callbackContext.requireManualClose == true)) {
				messageInfo.requireManualClose = true;
			}
		}
		
		if(!messagesStr) {
			callback(callbackContext.responseFalse, callbackContext.displayedMessages);
		}
		else {
			callbackContext.displayedMessages = true;
			
			if(messageInfo.requireConfirmation) {
				confirmDialog({
					title: attentionTitle,
					message: messagesStr,
					okCallback:  function() {
						callback(callbackContext.responseFalse, callbackContext.displayedMessages, true);
					}
				});
			}
			else if(messageInfo.error) {
				alertDialog(messagesStr, null, null, null, function() {
					callback(callbackContext.responseFalse, callbackContext.displayedMessages);
				});
			}
			else if(messageInfo.requireManualClose) {
				noteDialog(messagesStr, "", "",  function() {
					callback(callbackContext.responseFalse, callbackContext.displayedMessages);
				});
			}
			else if(conf.callbackDelayMillisecs) {
				noteDialog(messagesStr, "", "");
				window.setTimeout(function() {
					callback(callbackContext.responseFalse, callbackContext.displayedMessages);
				}, conf.callbackDelayMillisecs);
			}
			else {
				noteDialog(messagesStr, "", "");
				callback(callbackContext.responseFalse, callbackContext.displayedMessages);
			}
		}
	}
}

//--------------------------------------------------------------------------------------

function GetCookie (name) 
{  
    var arg = name + "=";  
    var alen = arg.length;  
    var clen = document.cookie.length;  
    var i = 0;  
    while (i < clen) 
    {
        var j = i + alen;    
        if (document.cookie.substring(i, j) == arg)      
        {
            return getCookieVal (j);    
        }
        i = document.cookie.indexOf(" ", i) + 1;    
        if (i == 0) break;   
    }  
    return null;
}

//--------------------------------------------------------------------------------------

function SetCookie (name, value) 
{  
    var argv = SetCookie.arguments;  
    var argc = SetCookie.arguments.length;  
    var expires = (argc > 2) ? argv[2] : null;  
    var path = (argc > 3) ? argv[3] : null;  
    var domain = (argc > 4) ? argv[4] : null;  
    var secure = (argc > 5) ? argv[5] : false;  
    document.cookie = name + "=" + escape (value) + 
        ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + 
        ((path == null) ? "" : ("; path=" + path)) +  
        ((domain == null) ? "" : ("; domain=" + domain)) + "; secure";
}

//--------------------------------------------------------------------------------------

function DeleteCookie (name) 
{  
    var exp = new Date();  
    exp.setTime (exp.getTime() - 1);  
    var cval = GetCookie (name);  
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}

//--------------------------------------------------------------------------------------

function getCookieVal(offset) 
{
    var endstr = document.cookie.indexOf (";", offset);
    if (endstr == -1)
    {
        endstr = document.cookie.length;
    }
    
    return unescape(document.cookie.substring(offset, endstr));
}

//--------------------------------------------------------------------------------------
					
//Session Variables and Functions for testing Session time out
					
var sssnCnt = "";
var sssnTmr = "";
var sssnTtl = "";
var sssnMsg = "";
var sssnBtnAccpt = "";
var sssnBtnCncl = "";
var logoutTimer = "";

//--------------------------------------------------------------------------------------

function resetSessionActvty()
{
	var ajaxSession = GetHttpObject({"showLoader": false, "sessionReset": false});
	ajaxSession.onreadystatechange = function()
	{	
		if (ajaxSession.readyState==4 && ajaxSession.status==200){
			if(ajaxSession.responseText == "true"){clearTimeout(sssnCnt); clearTimeout(logoutTimer); sessionActvty();} 
			else {window.location = "/secure/j_spring_security_logout";}
		}
	};
	ajaxSession.open("GET","/secure/validateSession.html?"+document.location.search.substring(1),true);
	ajaxSession.send();
}

//--------------------------------------------------------------------------------------

var sessionExpiryButtons = {};

function sessionActvty() {
	sessionExpiryButtons.btn1 = {
		text : sssnBtnAccpt,
		click : function() {
				$(this).scrollTop(0); $(this).dialog( "close" );
				resetSessionActvty(); } };
	sessionExpiryButtons.btn2 = {
		text : sssnBtnCncl,
		click : function() {
				$(this).scrollTop(0); $(this).dialog( "close" );
				window.location = "/secure/j_spring_security_logout"; } };
	sssnCnt = setTimeout(function(){
		if($("#sessionResponseAlertBox.ui-dialog-content").length){$("#sessionResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#sessionResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+sssnTtl+
			"</strong></h4><article>"+sssnMsg+"</article></section>")
		.dialog($.extend(true, {stack: true, zIndex: 6000}, genralAlertDialogOptions), { buttons: sessionExpiryButtons });
		btnStatus(sssnBtnAccpt);
		logoutTimer = setTimeout(function(){window.location = "/secure/j_spring_security_logout?status=inactive";}, 59000); }, sssnTmr); }
//end Session time out

//--------------------------------------------------------------------------------------

//Alert Close Button functionality
function closeAlertMessage()
{
	$("#messageResponseAlertBox").on("click", ".close", function(event){ 
		$(this).parents("#messageResponseAlertBox").find("article.scrollToTop").scrollTop(0);
		$(this).parents("#messageResponseAlertBox").dialog("close");
		
	});
}

//--------------------------------------------------------------------------------------

function submitNotificationContact(inputObj)
{
	var newContact = inputObj.val();
	
	if(/,/.test(newContact)){ var newContact = newContact.split(","); }
	
	if(newContact instanceof Array === false){ newContact = [newContact];}
	for(x = 0; x < newContact.length; x++) {
		if(characterTypes.emailString.test(newContact[x])) {
			var regXContact = new RegExp(newContact[x]);
			var existingContacts = $("#formAlertNotifyList").val();
			if(!regXContact.test(existingContacts)) { 
				if(existingContacts == ""){ $("#formAlertNotifyList").val(newContact[x]);  }
				else{ $("#formAlertNotifyList").val(existingContacts + "," + newContact[x]); }
				if($("#formDisplayAlertNotifyList").find("#noContact").is(":visible")){ $("#noContact").hide(); }
				$("#formDisplayAlertNotifyList").append("<li class='selected' title='"+clickToRemove+"'><a href='#' class='checkBox'>&nbsp;</a> <span class='contactEmail'>"+newContact[x]+"</span></li>"); } 
			inputObj.siblings("input").val(""); }
		else{ alertDialog(resolveMessage(emailChrctrsOnlyMsg),'','',inputObj.siblings("input")); } }
}

//--------------------------------------------------------------------------------------

//User Notifications
function notificationSlider(navTop)
{
	$("nav#main").animate({top: navTop}, 850);
	$("#notification").slideDown(850, function(){$("#notification").css("position", "fixed"); $("#content").css("margin-top",  navTop);});
}

function userNotification()
{
	var showNotification = false,
		noteHeightDif = 80,
		topPos = "74px",
		navTop = $("#notification").height() + noteHeightDif + "px";

	if($("#portal").length) {
        noteHeightDif = 6;
        topPos = "0px";
        navTop = $("#notification").height() + noteHeightDif + "px";

		if($("#parentRow").length){
            noteHeightDif = 46;
            topPos = "40px";
            navTop = $("#notification").height() + noteHeightDif + "px";
		}
	} else if ($("#parentRow").length) {
        noteHeightDif = 120;
        topPos = "114px";
        navTop = $("#notification").height() + noteHeightDif + "px";
	}

	$("#notificationMsg > #articleArea").children("article").each(function(){
		var shownPrior = GetCookie($(this).attr("id"));
		if(shownPrior == null) {
			showNotification = true;
		}
	});
	
	if(showNotification == true)
	{
		window.setTimeout(function(){notificationSlider(navTop)}, 2000);
	} else if($("#notificationAlert").length){
		$("#notificationAlert").delay(2000).fadeIn("slow");
	}
	
	$("#closeNote").on('click', function(event){
		event.preventDefault(); 
		$("#notification").css("position", "relative"); 
		$("#content").css("margin-top",  topPos);
		$("#notification").slideUp(500);
		$("nav#main").animate({top: topPos}, 500);
		$("#notificationAlert").delay(800).fadeIn("slow");
		
		$("#notificationMsg > #articleArea").children("article").each(function(){
			SetCookie($(this).attr("id"), true);	
		});
	});
	
	$("#notificationAlert").on('click', function(event){
		event.preventDefault();
		$("#notificationAlert").fadeOut("fast", function(){ notificationSlider(navTop); });
	});
}

// end User Notifications

//-------------------------------------------------------------------------------------------

var scrollTest = false;
var scrollPage = '';

function recentActivity() {
	 $("#activityLogList")
		.paginatetab({
			"url": "/secure/settings/getActivityLog.html",
			"formSelector": "#recentActivityFilterForm",
			"postTokenSelector": "#postToken",
			"rowTemplate": unescape($("#activityLogTemplate").html()),
			"responseExtractor": extractActivityLog,
			"dataKeyExtractor": extractDataKey
		})
		.paginatetab("setupSortingMenu", "#activityLogListHeader")
		.paginatetab("reloadData");
}

function extractDataKey(activityMsgs, context) {
	return context.dataKey;
}

function extractActivityLog(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).customerActivityLog;
	var result = buffer.data;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

//-------------------------------------------------------------------------------------------

function formErrorClass(formObj)
{
	switch(formObj.attr("type"))
	{
		case "text":	$(formObj).addClass("error"); break;
		case "radio":	$(formObj).parent().addClass("error"); break;
		default:		$(formObj).addClass("error"); break;
	}
}

//-------------------------------------------------------------------------------------------

function formClearErrorClass(formObj)
{
	switch(formObj.attr("type"))
	{
		case "text":	$(formObj).removeClass("error"); break;
		case "radio":	$(formObj).parent().removeClass("error"); break;
		default:		$(formObj).removeClass("error"); break;
	}
}

//-------------------------------------------------------------------------------------------

function testFormEdit($formID){
	$formID.one("change", ":input", function(event) {
		$("#mainContent").addClass("edit"); }); }

function testPopFormEdit($formID, $formParent){
	$formID.one("change", ":input", function() {
		$formParent.addClass("edit"); }); }

//-------------------------------------------------------------------------------------------


function setMinHeight(){
	var	thisHeight = $(window).height(),
		headerHeight = $("#titleBar").outerHeight(true),
		footerHeight = $("footer").outerHeight(true),
		contentDif = $("#mainContent").outerHeight(true) - $("#mainContent").outerHeight(false),
		contentHeight = (thisHeight-headerHeight-footerHeight-contentDif)+"px";
	$("#mainContent").css("min-height", contentHeight);//.css("margin-bottom", 0);
 }

//-------------------------------------------------------------------------------------------

function resizeend() {
	if (new Date() - rtime < delta) {
	   setTimeout(resizeend, delta);
	} else {
		timeout = false;
		if($("#xpndContent").is(":visible")){ 
			var tempWdgt = $("#expandedWdgt").attr("wdgtID"), tempWidth = $(window).innerWidth()-20, tempHeight = $(window).innerHeight()-53;
			$("#xpndContent").parents(".ui-dialog").css("width", tempWidth+"px");
			$("#expandedWdgt").css("height", tempHeight+"px");
			$("#xpndContent").html(""); 
			
			if($("body").attr("id") === "settings"){ displaySensorGraph($("#xpndContent"), $("#"+$("#expandedWdgt").attr("wdgtID")+"GraphData")); }
			else{ displayChart($("#expandedWdgt").attr("wdgtID"), "xpnd"); } }
		if($("#dashboardPage").length){
			dashboardNav(); // Tracks the window Size of the Browser changing and resets the Dashboard Nav function
			$(".chartGraph:visible").each(function(index) { if($(this).attr("id") !== "xpndContent"){ $(this).html(""); displayChart($(this).attr("id").replace("content_", "")); }}); }
		if($(".balanceLists").length){ 
			if(($("#unplacedListArea:visible").length && $("#placedListArea:visible").length) && markerListDrag !== true){ 
				balancePayStationLists(); }
			else if($("#currentNotifications").length){ balanceNotificationList(); } }
	}
}

//-------------------------------------------------------------------------------------------

function resizeEvents(){
	rtime = new Date();
	if (timeout === false) {
	   timeout = true;
	   setTimeout(resizeend, delta);
	}
	
	setMinHeight();
	
	if($("#dashboardPage").length){
		$(".chartGraph:hidden").each(function(index) { if(!$(this).hasClass("resizeChartGraph")){ $(this).addClass("resizeChartGraph")}});
		$(".chartGraph:visible").each(function(index) {$(this).css("height", $(this).outerHeight()).html(loadObj);});
		if($("#xpndContent").is(":visible")){ $("#xpndContent").html(""); } }
//Resizes Location tree Nested list width
	if($("li.Parent").length){$("li.Parent").children("ul").css("width", $("li.Parent").width()+"px");}
//Resizes Auto Complete Field with No Search Button
	if($(".acWidthNoSearch").length){$(".acWidthNoSearch").css("width", $(".acWidthNoSearch").parents(".filterHeader").innerWidth()-menuACdiff+"px");}
//Resizes Location Auto Complete Field with Search Button
	if($(".acWidthSearch").length){$(".acWidthSearch").css("width", $(".acWidthSearch").parents(".filterHeader").width()-menuACdiffWs+"px");}
	if($(".setWidth").length){
		if($(".setWidth").length > 1){ $(".setWidth").each(function(){ sow([$(this).attr("id")]); }); }
		else{ sow([$(".setWidth").attr("id")]); }
	}
// Sets Height on items being controlled by the SOH function
	if($(".setMaxHeight").length){ 
		if($(".setMaxHeight").length > 1){ $(".setMaxHeight").each(function(){ soh([$(this).attr("id")]); }); }
		else{ soh([$(".setMaxHeight").attr("id")]); }
	}
	if($(".setHeight").length){ 
		if($(".setHeight").length > 1){ $(".setHeight").each(function(){ soh('',[$(this).attr("id")]); }); }
		else{ soh('',[$(".setHeight").attr("id")]); } 
	}
	if($(".setSharedMaxHeight").length){ 
		var itemGroup = new Array(); 
		itemGroup.push("max");
		var parentObj = $(".setSharedMaxHeight:first").parents("section");
		while(!parentObj.attr("id") || parentObj.attr("id") == "") { parentObj = parentObj.parents("section"); }
		itemGroup.push(parentObj.attr("id"));
		if($(".setSharedMaxHeight").length > 1){ $(".setSharedMaxHeight").each(function(index){ itemGroup.push($(this).attr("id")); }); }
		else{ itemGroup.push($(".setSharedMaxHeight").attr("id")); }
		soh('','',itemGroup);
	}
	if($(".setSharedHeight").length){ 
		var itemGroup = new Array(); 
		itemGroup.push("height");
		var parentObj = $(".setSharedHeight:first").parents("section");
		while(!parentObj.attr("id") || parentObj.attr("id") == "") { parentObj = parentObj.parents("section"); }
		itemGroup.push(parentObj.attr("id"));
		if($(".setSharedHeight").length > 1){ $(".setSharedHeight").each(function(index){ itemGroup.push($(this).attr("id")); }); }
		else{ itemGroup.push($(".setSharedHeight").attr("id")); }
		soh('','',itemGroup);
	}
	if($(".scrollingList").length) {
		var listObj = new Array($(".scrollingList").length);
		$(".scrollingList").each(function(index, element){ listObj[index] = element.id; });
		scrollListWidth(listObj); }
}
//-------------------------------------------------------------------------------------------

function posLocationRouteAutoComplete(request, response) {
	var responseOptions = new Array(), posResponse = new Array(), posNameResponse = new Array(), itemDesc = "", 
		allPayStationsItem = {label: allPOSMsg, value: "all"},
		noPaystation = [{ label: "", value: "-1", desc: noFilterTerm, category: payStationMsg }];

	if(request.term.length >= 4){
		var	ajaxPosLocationRoute = GetHttpObject(),
			querryCustID = (typeof curCustomerID === "undefined")? "" : "&customerID="+curCustomerID ;
		ajaxPosLocationRoute.onreadystatechange = function(responseFalse, displayedError) {
			if(ajaxPosLocationRoute.readyState === 4) {
				var data = JSON.parse(ajaxPosLocationRoute.responseText);
							
				if(data.searchCustomerResult.posSearchResult instanceof Array)
				{ posResponse = $.map(data.searchCustomerResult.posSearchResult, function(item) {return{ 
					label: item.name, 
					value: item.randomId,
					desc: item.label, 
					category: payStationMsg
				}}); } 
				else if(data.searchCustomerResult.posSearchResult)
				{ posResponse = [{
					label: data.searchCustomerResult.posSearchResult.name, 
					value: data.searchCustomerResult.posSearchResult.randomId, 
					desc: data.searchCustomerResult.posSearchResult.label, 
					category: payStationMsg
				}]; }
				
				if(data.searchCustomerResult.posNameSearchResult instanceof Array)
				{ posNameResponse = $.map(data.searchCustomerResult.posNameSearchResult, function(item) {return{ 
					label: item.name, 
					value: item.randomId, 
					desc: item.label, 
					category: payStationMsg
				}}); } 
				else if(data.searchCustomerResult.posNameSearchResult)
				{ posNameResponse = [{
					label: data.searchCustomerResult.posNameSearchResult.name, 
					value: data.searchCustomerResult.posNameSearchResult.randomId, 
					desc: data.searchCustomerResult.posNameSearchResult.label, 
					category: payStationMsg
				}]; }
				
				if((posResponse.length === 0 && posNameResponse.length === 0) && request.term === "")
				{ posResponse =  noPaystation; }
				
				var searchTerm = request.term.trim().toLowerCase();
				
				var locRouteOpts = [], obj;
				var i = -1, len = locationRouteObj.length;
				while(++i < len) {
					obj = locationRouteObj[i];
					if((searchTerm.length <= 0) || (obj.label.toLowerCase().indexOf(searchTerm) >= 0)) {
						locRouteOpts.push(obj);
					}
				}
				
				responseOptions = responseOptions.concat(allPayStationsItem,posResponse,posNameResponse,locRouteOpts);
				response(responseOptions);
			}
		};
		ajaxPosLocationRoute.open("POST", LOC.posSearch, true);
		ajaxPosLocationRoute.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxPosLocationRoute.send("search="+encodeURIComponent(request.term)+querryCustID); }
	else{
		var searchTerm = request.term.trim().toLowerCase(), locRouteOpts = [], obj, i = -1, len = locationRouteObj.length;
		while(++i < len) {
			obj = locationRouteObj[i];
			if((searchTerm.length <= 0) || (obj.label.toLowerCase().indexOf(searchTerm) >= 0)) {
				locRouteOpts.push(obj);
			}
		}
		
		responseOptions = responseOptions.concat(allPayStationsItem,noPaystation,locRouteOpts);
		response(responseOptions);
	}
	
}

//--------------------------------------------------------------------------------------

function standard()
{	
	$('input, textarea').placeholder();
	
	$(window).on('beforeunload', function() { $(".scrollToTop").scrollTop(0); });
	
	setMinHeight();
	
/*	$(document.body).find("input").tooltip({
		items: "[required]",
		effect: 'slideDown',
		tooltipClass: "reqField",
		delay: 550,  
		position: { my: "left+5 top", at: "right top" },
		content: "<strong>"+requiredFieldMsg+"</strong>" });
*/
	$(document.body).find("label, span").tooltip({
		items: "[required]",
		effect: 'slideDown',
		tooltipClass: "reqField",
		track: true,
		delay: 550,  
		position: { my: "left+10 bottom-5", at: "right top" },
		content: "<strong>"+requiredFieldMsg+"</strong>" });

	$(document.body).tooltip({
		items: "[passwrd], [tooltip], [tooltipHTML], [description], [comment]",
		effect: 'slideDown', 
		track: true,
		delay: 550,  
		position: { my: "left+10 bottom-5", at: "right top" },
		content: function() { 
			var element = $(this);
			var returnContent = "";
			var contentID = "";
			if(element.is("[passwrd]")){ contentID = $(this).attr("ref"); returnContent = $(contentID).html();}
			else if(element.is("[tooltip]")){ returnContent = basicHtmlEncode(element.attr("tooltip")); }
			else if(element.is("[tooltipHTML]")){ returnContent = element.attr("tooltipHTML"); }
			else if(element.is("[description]")){ if(element.attr("description") !== ""){ returnContent = "<strong>"+descriptionLbl+"</strong> : " + element.attr("description")}; }
			else if(element.is("[comment]")){ if(element.attr("comment") !== ""){ returnContent = "<strong>"+commentLbl+"</strong> : " + element.attr("comment")}; }
			return returnContent } });

	var ddMenuHover = false;
	
	if($(".acWidthNoSearch").length)
	{
		menuACwd = menuACwdOG; menuACdiff = menuACdiffOG;
		$(".acWidthNoSearch").each(function(index){
			if(index > 0){menuACwd = menuACwdOG; menuACdiff = menuACdiffOG;}
			menuACwd = $(this).parents(".filterHeader").width();
			menuACdiff = menuACdiff+$(this).parents(".filterHeader").children(".title").outerWidth();
			menuACdiff = menuACdiff+$(this).siblings(".selectMenu").outerWidth();
			menuACwd = menuACwd-menuACdiff+"px";
			if(menuACwd && (menuACwd == "" || menuACwd < 0)){ menuACwd = "120px";}
			$(this).css("width", menuACwd); });	
	}
	
	if($(".acWidthSearch").length)
	{
		menuACwdWs = menuACwdOG; menuACdiffWs = menuACdiffOG;
		$(".acWidthSearch").each(function(index) {
			if($(this).parents(".filterHeader").length){ menuACwdWs = $(this).parents(".filterHeader").width(); }
			else if($(this).parents(".workspaceSwitch").length){ menuACwdWs = $(this).parents(".workspaceSwitch").width(); }
			else if($(this).parents(".landing").length){ menuACwdWs = $(this).parents(".landing").width(); }
			else{ menuACwdWs = $(this).parents("section").width(); }
			menuACdiffWs = menuACdiffWs+$(this).parents(".filterHeader").children(".title").outerWidth();
			menuACdiffWs = menuACdiffWs+$(this).siblings(".selectMenu").outerWidth();
			menuACdiffWs = menuACdiffWs+$(this).siblings(".search").outerWidth();
			menuACwdWs = menuACwdWs-menuACdiffWs+"px";
			if(menuACwdWs && menuACwdWs == ""){menuACwdWs = "120px";}
			$(this).css("width", menuACwdWs); });
	}
	
	resizeAutoWidthFilters();
	
	//Close Button to block un needed # from being added to URL
	$(document.body).on("click", ".close", function(event){ event.preventDefault(); });
	
	//Print Button
	$(document.body).on("click", "#print", function(event){
		event.preventDefault();
		window.print();
	});
	
	//Main Navigation Scroll Feature
	$("nav#main").hoverIntent(function(){
		$(this).css("boxShadow", "3px 0 6px 0 rgba(0, 0, 0, 0.4)")
		$(this).animate({width: "250px"}, 300);
	}, function(){
		$(this).animate({width: "60px"}, 250, function(){ $(this).css("boxShadow", "0 0 0 0") });
	});
			
	// Setup inline-validation...
	setupCharBlocker(".noSpclChrctr", characterTypes.spclCharacter, function() { return alphaNumOnlyMsg; });
	setupCharBlocker(".alphaNumChrctr", characterTypes.alphaNumeric, function() { return alphaNumOnlyMsg; });
	setupCharBlocker(".nmbrChrctr", characterTypes.nonDblNumeric, function() { return dblChrctrsOnlyMsg; });
	setupCharBlocker(".dllrPrcntgChrctr", characterTypes.nonDollarPercentage, function() { return dllrPrcntgChrctrMsg; });
	setupCharBlocker(".timeNmbrChrctr", characterTypes.numericTimeExpression, function() { return timeChrctrsOnlyMsg; });
	setupCharBlocker(".meridianTimeNmbrChrctr", characterTypes.numericMeridianTimeExpression, function() { return timeChrctrsOnlyMsg; });
	setupCharBlocker(".intNmbrChrctr", characterTypes.nonIntNumeric, function() { return nmbbrChrctrsOnlyMsg; });
	setupCharBlocker(".serialNmbr", characterTypes.serialNumbers, function() { return alphaNumOnlyMsg; });
	setupCharBlocker(".locationChrctr", characterTypes.locationChrctr, function() { return locationChrctrsOnlyMsg; });
	setupCharBlocker(".couponSearchChrctr", characterTypes.couponSearchChrctr, function() { return couponSearchChrctrsOnlyMsg; });
	setupCharBlocker(".couponWithDiscountSearchChrctr", characterTypes.couponWithDiscountSearchChrctr, function() { return couponWithDiscountSearchChrctrsOnlyMsg; });
	setupCharBlocker(".stndrdChrctr", characterTypes.stndrdChrctr, function() { return stndrdChrctrsOnlyMsg; });
	setupCharBlocker(".psChrctr", characterTypes.psChrctr, function() { return psChrctrsOnlyMsg; });
	setupCharBlocker(".extStndrdChrctr", characterTypes.extStndrdChrctr, function() { return extStndrdChrctrsOnlyMsg; });
	setupCharBlocker(".prntableChrctr", characterTypes.prntableChrctr, function() { return prntableChrctrsOnlyMsg; });
	setupCharBlocker(".emailChrctr", characterTypes.emailInvalidChrctr, function() { return emailChrctrsOnlyMsg; });
	setupCharBlocker(".link2GovPass", characterTypes.link2GovPass, function() { return link2GovChrctrsOnlyMsg; });


	setupCharBlocker(".couponNumber", /[^a-z0-9*]/i, function() { return couponReportChrctrsOnlyMsg; });
	setupCharBlocker(".licensePlate", /[^a-z0-9 ]/i, function() { return alphaNumWithSpaceOnlyMsg; });

	// Setup textarea charactor counts limit...
	$(document.body).on("keypress", "textarea[class*=chrctrCount]", function(event) {
		var maxLen = getMaxCharsCountByCSS(this);
		if((event.charCode != 0) && ($(this).val().length >= maxLen)) {
			event.preventDefault();
		}
	});
	$(document.body).on("change", "textarea[class*=chrctrCount]", function(event) {
		var maxLen = getMaxCharsCountByCSS(this);
		var $this = $(this);
		var currLen = $this.val().length;
		if(currLen > maxLen) {
			alertDialog(lengthMsg.replace("{0}", maxLen).replace("{1}", currLen));
			$this.addClass("error").addClass("lengthError");
		}
		else {
			if($this.hasClass("lengthError")) {
				$this.removeClass("error").removeClass("lengthError");
			}
		}
	});
	
	//Custom Check Box
	$(document.body).on("click", ".checkboxLabel", function(event){ event.preventDefault();
		var checkBoxObj = $(this).children(".checkBox"),
			checkBoxObjs = [
				$(this).hasClass("disabled"), $(this).hasClass("unavailable"), $(this).hasClass("inactive"), checkBoxObj.hasClass("disabled"), checkBoxObj.hasClass("unavailable"), checkBoxObj.hasClass("inactive")],
			disabledCheckBox = true;
																   
		$.each(checkBoxObjs, function(index, value){ if(value){ disabledCheckBox = false; return false; } });
		if(disabledCheckBox && $(this).parents().hasClass("inactive")){ disabledCheckBox = false; }

		if(disabledCheckBox){
			checkBoxObj.toggleClass("checked");
			var subscriptList = (checkBoxObj.attr("id"))? checkBoxObj.attr("id").match("subscriptionList") : null;
			if(subscriptList === null){
				var checkboxField = $("#"+checkBoxObj.attr("id")+"Hidden");
				if(checkBoxObj.hasClass("checked")){ checkboxField.val("true").trigger('change'); }
				else { checkboxField.val("false").trigger('change'); } } } });
	
	//Custom Radio Button
	$(document.body).on("click", ".radioLabel", function(event)
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){
			var radioObj = $(this).children(".radio");
			var radioData = radioObj.attr("id").split(":");
			var radioField = $("#"+radioData[0]);
			$(".radio").each(function(index, element) {
				var tempRadioData = $(this).attr("id").split(":");
				if(tempRadioData[0] == radioData[0] && tempRadioData[1] != radioData[1]){$(this).removeClass("checked");}
				else{ radioObj.addClass("checked"); radioField.val(radioData[1]).trigger('change');}
			});
		}
	});
	
	$(document.body).on("click", ".checkBox", function(event){ event.preventDefault(); });
	
	$(document.body).on("click", ".radio", function(event){ event.preventDefault(); });
	
	closeAlertMessage();
	
	//Menu Button to show Drop Down Menu
	$(document.body).on("click", ".menu", function(event){ event.preventDefault(); event.stopImmediatePropagation();
//console.log($(this).attr("id"))
		if($(this).hasClass("on")){ ddMenuHover = false; $(".menu.on").trigger('blur'); }
		else {
			if($("body").attr("id") !== "dashboardPage" && ($("#mainContent").hasClass("edit") && (!$(this).hasClass("dialogHdrIcn")&&!$(this).hasClass("confirmBtn")))){ inEditAlert("menu"); }
			else{ 
				if($(".ddMenu").is(":visible")){ddMenuHover = false; $(".ddMenu").hide(); $(".menu.on").trigger('blur');}
				$(this).toggleClass("on");
				var placement = $(this).offset();
				var localPlacement = $(this).position();
				var right = $(window).innerWidth() - (placement.left + $(this).outerWidth() + 8);
				var top = placement.top + $(this).outerHeight() + 13;
				var BtnMenuID = "#"+$(this).attr("id").replace("opBtn_", "menu_");
				var targetPlacement = "bellow";
				$(this).trigger('focus');
				if($(this).parents().hasClass("widget")){
					var wdgtPlacement = $(this).parents(".widget").position();
					$(BtnMenuID).css("right", "-6px");
					$(BtnMenuID).css("top", wdgtPlacement.top+42+"px"); }
				else if($(this).parents("section").hasClass("psWidget")){
					var wdgtPlacement = $(this).parents(".mainContent").position();
					$(BtnMenuID).css("right", "-6px");
					$(BtnMenuID).css("top", wdgtPlacement.top+34+"px"); }
				else if($(this).hasClass("confirmBtn")){
					var dilgPlacement = $(this).position();
					targetPlacement = "rightSide";
					$(BtnMenuID).css("right", "46px");
					$(BtnMenuID).css("top", dilgPlacement.top-2+"px"); }
				else if ($(this).parents("section").hasClass("mainContent")){
					var prntPlacement = $(this).parents(".mainContent").position();
					$(BtnMenuID).css("right", "-6px");
					$(BtnMenuID).css("top", prntPlacement.top+42+"px"); }
				else if ($(this).parents("section").attr("id") === "systemNotification"){
					var parentObj = $(this).parents("li");
					var prntPlacement = parentObj.offset();
					$(BtnMenuID).css("position", "absolute");
					$(BtnMenuID).css("left", prntPlacement.left + parentObj.outerWidth() + 4+"px");
					$(BtnMenuID).css("top", prntPlacement.top - 2+"px");
					var menu = $(BtnMenuID).detach();
					menu.appendTo("body"); } 
				else if ($(this).hasClass("menuHeader")){
					var prntPlacement = $(this).parents(".menuBox, .accountsItemList").position();
					$(BtnMenuID).css("right", "-6px");
					$(BtnMenuID).css("top", prntPlacement.top+42+"px"); }
				else if ($(this).hasClass("tableMenu")){
					targetPlacement = "side";
					var prntPlacement = $(this).parents("td").position();
					$(BtnMenuID).css("left", $(this).parents("td").width()+10+"px");
					$(BtnMenuID).css("top", prntPlacement.top-4+"px"); }
				else if ($(this).hasClass("dialogHdrIcn")){
					var dilgPlacement = $(this).position();
					$(BtnMenuID).css("right", "2px");
					$(BtnMenuID).css("top", dilgPlacement.top+10+"px"); }
				else { 
					targetPlacement = "side";
					var prntPlacement;
					var $parent = $(this).parents("li");
					if($parent.length <= 0) { $parent = $(this).parent("*"); }
					prntPlacement = $parent.position();
					var menuWidth = ($(this).parents("ul.menuButtonList").length)? $(this).parents("ul.menuButtonList").width() : $(this).parents("ul.menuList").width() ;
					$(BtnMenuID).css("left", menuWidth+prntPlacement.left+4+"px");
					$(BtnMenuID).css("top", prntPlacement.top-4+"px"); }
				if(right < 200 && targetPlacement === "side"){ 
					$(BtnMenuID).css("left",localPlacement.left-$(BtnMenuID).outerWidth()+"px");
					$(BtnMenuID).find(".point").attr("class", "rightSidePoint");
					$(BtnMenuID).fadeIn(400); } 
				else if(targetPlacement === "rightSide") {
					$(BtnMenuID).find(".point").attr("class", "rightSidePoint"); 
					$(BtnMenuID).fadeIn(400); }
				else{ $(BtnMenuID).fadeIn(400, function(){ $(BtnMenuID).find(".rightSidePoint").attr("class", "point"); }); }} }
	}).on("mouseenter", ".menu", function(){
		var BtnMenuID = "#"+$(this).attr("id").replace("opBtn_", "menu_");
		if($(BtnMenuID).is(":visible")){
			ddMenuHover = true;
			$(BtnMenuID).stop(true, true).show(); }
	}).on("mouseleave", ".menu", function(){
		var $that = $(this);
		var BtnMenuID = "#"+$(this).attr("id").replace("opBtn_", "menu_");
		if($(BtnMenuID).is(":visible")){
			ddMenuHover = false;
			$(BtnMenuID).delay(1200).fadeOut(400, function(){ $that.trigger('blur'); }); }
	}).on("blur", ".menu", function(){
		if(ddMenuHover == false){
			$(this).removeClass("on");
			var BtnMenuID = "#"+$(this).attr("id").replace("opBtn_", "menu_");
			$(BtnMenuID).fadeOut(400, function(){ $(this).find(".rightSidePoint").attr("class", "point"); }); }
	});
	
	//Hide Menu when an option is selected from the Menu
	$(document.body).on("click", ".ddMenu * a", function(event){ $(".menu.on").removeClass("on"); $(this).parents(".ddMenu").hide(); });
	
	//Drop Down Menu functionality
	$(document.body).on("mouseenter", ".ddMenu", function(event){ event.preventDefault();
		ddMenuHover = true;
		$(this).stop(true, true).show();
	}).on("mouseleave", ".ddMenu", function(){
		ddMenuHover = false;
		$(this).delay(800).fadeOut(400, function(){
			$(".menu.on").trigger('blur');
		});
	});
	
	//Chart Expand functionality
	$(document.body).on("click", "a.xpndWdgt", function(event){ event.preventDefault(); var xpndBtn = ($(this).parents(".psWidget").length)? "psWdgt" : "dbrdWdgt" ; expandChart($(this), xpndBtn); });
	
	$(document.body).on("click", "nav#main a, header#titleBar a, footer a, #secondNav a, #thirdNav a, .pageItems > li > a.menuListButton, .pageItems > li, .pageItems * li, #custSwitchBtn", function(event){
		// tracks if a click is made outside of the dashboard when in edit mode.
		if(($("body").attr("id") !== "dashboardPage" && $("#mainContent").hasClass("edit")) && !$(this).is(".help")){
			if(event.ctrlKey && ($("#DefinedPaystations").length && $(".sysAdminWorkSpace").length)){  }
			else if($("#mainContent").hasClass("edit") && (forceClick === false)) {
					event.preventDefault(); pausePropagation = true;
					inEditAlert("site", $(this));
			}
		}
		forceClick = false;
	});
	
	if($("#childCoSwitchAC").length){
		$("#childCoSwitchAC").autoselector({
			isComboBox: true,
			defaultValue: "",
			shouldCategorize: false,
			data: JSON.parse($("#chilCustomerData").text()) })
		.on("itemSelected", function(){
			var thisLabel = $(this).autoselector("getSelectedLabel"); 
			var thisVal = $(this).autoselector("getSelectedValue");
			if(thisLabel !== $("#parentRow").find("h2").html() && thisVal !== "-1"){
				$("#custSwitchBtn").removeClass("inactive"); }
			else{ $("#custSwitchBtn").addClass("inactive"); }}); }
	
	$(document.body).on("click", "#custSwitchBtn", function(event){ event.preventDefault();
		if(!$(this).hasClass("inactive")){
			if(pausePropagation === true){ 
				pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false; }}, 100); }
			else{ customerSwitch(); }  }});
	
	$("#locationAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"selectedCSS": "autoselector-selected",
		"shouldCategorize": false,
		"blockOnEdit": true,
		"data": locationsObj })
	.on("itemSelected", function(event, ui) {
		var slctdCategory = $("#locationAC").autoselector("getSelectedObject").category;
		var slctValue = $("#locationAC").autoselector("getSelectedValue");
		if($(".sectionContent").attr("id") == "DefinedLocations" && slctValue != ""){
			/*$("#mainContent").addClass("edit");*/ localSelectionCheck(slctValue, "display"); }// Location Detail Page Outcome.
		else if(($(".sectionContent").attr("id") == "XtendByPhone" || $(".sectionContent").attr("id") == "XBP") && slctValue != ""){ // Extend By Phone Pages Outcome.
			if($("#contentID").val() == slctValue){ $("#locationAC").val("");} 
			else {
				var locIdString = /([?|&]locId=)/;
				if(locIdString.test(document.location.search.substring()) == true){
					window.location = "/secure/settings/locations/extendByPhone/overview.html" + document.location.search.substring().replace(/([?|&]locId=)[^\&]+/, '$1' + slctValue); } 
				else {
					window.location = "/secure/settings/locations/extendByPhone/overview.html?locId=" + slctValue+"&"+document.location.search.substring(1); }}}
		if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue }
		$(this).trigger('blur');
		return false;
	});
	
	var lracItemSelected = false;
	$("#locationRouteAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"valueContainerSelector": "#locationRouteACVal",
		"data": locationRouteObj })
	.on("itemSelected", function(event, selectedItem) {
		var slctdCategory = $(this).autoselector("getSelectedObject").category;
		var slctValue = $(this).autoselector("getSelectedValue");
		if($("#PaystationPlacement").length){
			if(slctdCategory == labelLocation){ payStationPlacementList(slctValue); }
			else if(slctdCategory == labelRoute){ payStationPlacementList('', slctValue); }
			else{ payStationPlacementList(); } }
		else if($("#Collections").length || $("#Maintenance").length ) {
			updateList(); } 
		else {
			if(slctdCategory === labelLocation){ $("#locationACVal").val(slctValue); $("#routeACVal").val(''); }
			else if(slctdCategory === labelRoute){ $("#locationACVal").val(''); $("#routeACVal").val(slctValue); }
			else{ $("#locationACVal").val(''); $("#routeACVal").val(''); }
			$("#paystationList").paginatetab("reloadData"); }
		if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue }
		$(this).trigger('blur');
		return false; })
	.on("change", function(){
		var slctName = $(this).autoselector("getSelectedObject");
		if($(this).val()==="" && slctName.label !==""){$(this).val(slctName.label)} });

	$("#locationPOSAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"persistToHidden": false,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"remoteFn": posLocationRouteAutoComplete })
	.on("itemSelected", function(event, ui) {
		var slctdCategory = $(this).autoselector("getSelectedObject").category;
		var slctValue = $(this).autoselector("getSelectedValue");
		if(slctValue != '-1') {
			if(slctdCategory == payStationMsg){ 
				posSelectionCheck("_"+slctValue, "display");
				itemSelectedID = slctValue;
				
				if(!$("#ps_"+slctValue).is(":visible")){
					updateFilter("", slctValue, "searchDisplay"); }
				else{ 
					$("#ps_"+slctValue).addClass("selected");
					checkVisibleItem($("#ps_"+slctValue), $("#paystationList")); }
				
				$("#itemACVal").val(''); $("#locationACVal").val(''); $("#routeACVal").val(''); }
			else {
				if(slctdCategory === labelLocation){ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(slctValue); $("#routeACVal").val(''); }
				else if(slctdCategory === labelRoute){ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(''); $("#routeACVal").val(slctValue); }
				else{ itemSelectedID = ''; $("#itemACVal").val(''); $("#locationACVal").val(''); $("#routeACVal").val(''); }
				if($(".menuList > li").is(":visible")){ $("#paystationList").paginatetab("reloadData"); }
				else{ loadPOSList(); } }
			if($(this).parents("ul.filterForm") && slctValue != ""){ filterQuerry = "&filterValue="+slctValue; }
			$(this).trigger('blur');
		}
		return false; })
	.on("change", function(){
		var slctName = $(this).autoselector("getSelectedObject");
		if($(this).val()==="" && slctName.label !==""){$(this).val(slctName.label);} });

	$("#payStationSlctFltr").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": true,
		"data": locationRouteObj })
	.on("itemSelected", function(event, ui) {
		var slctdCategory = $("#payStationSlctFltr").autoselector("getSelectedObject").category;
		var slctValue = $("#payStationSlctFltr").autoselector("getSelectedValue");
		var showCount = 0;
		indexTest = 0;
		$(".fullList > li").fadeOut("fast");
		locationID = (slctdCategory === labelLocation)? slctValue : '';
		routeID = (slctdCategory === labelRoute)? slctValue: '';
		payStationSelectList($("#contentID").val(), locationID, routeID, true, "");
		testSelections($(".fullList"));
		$(this).trigger('blur');
		return false;
	});	

	if($("html.ie8").length){	
		document.body.onresize = function () { 
			var widthDif = ogWidth - $(window).width();
			var heightDif = ogHeight - $(window).height();
			if((widthDif > 20 || widthDif < -20) || (heightDif > 20 || heightDif < -20)){ resizeEvents(); }
			resizeAutoWidthFilters();

		};
	}
	else{ $(window).on('resize', function() { resizeEvents(); resizeAutoWidthFilters(); }); }
	
	$(document.body).on("click", ".optionListArea .selectedListHeader #clearSelected, .optionListArea .selectedListHeader .clearSelected", function(event) // SELECT ALL FEATURE, SELECTS OR DESELECTS ALL IN FULL LIST 
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){
			var parentTier = $(this).parents(".optionListArea");
			var checkBoxObj = $(this).children(".checkBox");
			var inputObj = parentTier.children("input");
			var inputObj = (parentTier.children("input").length)? parentTier.children("input") : $("input#"+$(this).attr("for"));
			
			parentTier.find("ul.fullList > li").each(function(index, element){ $(this).removeClass("selected"); $(this).fadeIn("fast") });
			parentTier.find("ul.selectedList > li").each(function(index){ $(this).fadeOut("fast").removeClass("selected"); });
			parentTier.find(".fullListHeader").find(".checkBox.checked").trigger('click');
			inputObj.val("");
		}
	});
	
	$(document.body).on("click", ".optionListArea .fullListHeader .checkboxLabel", function(event) // SELECT ALL FEATURE, SELECTS OR DESELECTS ALL IN FULL LIST 
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled")){
			var parentTier = $(this).parents(".optionListArea");
			var checkBoxObj = $(this).children(".checkBox");
			var inputObj = (parentTier.children("input").length)? parentTier.children("input") : $("input#"+$(this).attr("for"));
			var slctAllComplete = false;
			
			if(parentTier.find("ul.fullList > li:visible").length > 500){ 
				loadOverlay();
				
				setTimeout( function(){ 
					slctAllComplete = selectAll(parentTier, checkBoxObj, inputObj);
					var delayStatus = setInterval(function(){ if(slctAllComplete){ $("#ajaxLoad").dialog("close").dialog("destroy"); delayStatus = window.clearInterval(delayStatus); }}, 0); }, 1); }
			else { slctAllComplete = selectAll(parentTier, checkBoxObj, inputObj); } }
	});
	
	$(document.body).on("click", "ul.fullList  li, ul.selectedList li", function(event) // LIST ITEM SELECTION FEATURE
	{
		event.preventDefault();
		if(!$(this).hasClass("disabled") || !$(this).hasClass("notclickable")){
			if(!$("#wdgtSettings").length)
			{
				var fullList = /full/i;
				var selectedList = /select/i;
				var currObjID = $(this).attr("id");
				var currID = currObjID.split("-");
				var parentList = $(this).parent("ul");
				var inputObj = (parentList.hasClass("locationForm"))? $("input."+$(this).parents(".optionListArea").attr("id")) : $(this).parents(".optionListArea").children("input");

				if(parentList.hasClass("fullList") && !$(this).hasClass("selected"))
				{
					if(!$(this).hasClass("selected")){ if(inputObj.val() == ""){ inputObj.val(currID[1]).trigger('change'); }else{ inputObj.val(inputObj.val()+","+currID[1]).trigger('change'); } }
					var newObjID = currObjID.replace(fullList, "#select");
					$("#"+currObjID).addClass("select").fadeOut("fast", function(){ $(this).removeClass("select").addClass("selected"); testSelections(parentList); });
					$(newObjID).addClass("selected").fadeIn("slow");
					$(newObjID).parent("ul").removeClass("error");
					parentList.removeClass("error");
				}
				else if(parentList.hasClass("selectedList"))
				{
					inputObj.val(stringArrayValCleanUp(inputObj.val(), currID[1])).trigger('change');
					
					var newObjID = currObjID.replace(selectedList, "#full");
					var fullListObj = parentList.siblings(".fullList");
					var levelSelected = false;
					$(newObjID).fadeIn("fast").removeClass("selected");
					$("#"+currObjID).slideUp(function(){ $(this).removeClass("selected"); });
					testSelections(fullListObj);
				}
			}
		}
	});
	
	$("input.smplValue").on("focus", this, function(event){$(this).removeClass("smplValue").val("");});
	
	$(document.body).on("click", "#menu_pageContent * .edit", function(event){ event.preventDefault(); event.stopImmediatePropagation();
		var hideCallBack = '',
			showCallBack = '';
		//$("#mainContent").addClass("edit");
		if($("#DefinedRateProfile").length){ profileSelectionCheck("_"+$("#contentID").val(), "edit"); }
		else{ 
			if($("#DefinedRoutes").length){ 
				hideCallBack = function(){ return $(".mainContent").addClass("form editForm"); };
				showCallBack = function(){ return payStationSelectList($("#contentID").val(), "", "", false, ""); }; }
			else if($("#DefinedLocations").length){ 
				if($(this).hasClass("btnEditLocation")){ 
					hideCallBack = function(){ $(".mainContent").addClass("form editForm"); };
					showCallBack = function(){ 
							payStationSelectList($("#contentID").val(), "", "", false, false);
							facilitySelectedList($("#contentID").val(), "", false, false);
							propertySelectedList($("#contentID").val(), "", false, false);
							lotSelectedList($("#contentID").val(), "", false, false); }; }
				else if($(this).hasClass("btnEditParkingLocation")){ setParkingAreaMap(); }}
			else{ 
				hideCallBack = function(){ return $(".mainContent").addClass("form editForm"); };
				showCallBack = ''; }
			if(!$(this).hasClass("btnEditParkingLocation")){ slideFadeTransition($(".mainContent"), $(".mainContent"), hideCallBack, showCallBack); }}
	});

	
	$(document.body).on("keyup", ".autoSearchInput", function(event) { 
		var $thisClearBtn = $("#"+$(this).attr("id")+"ClearBtn");
		if($(this).val() !== ""){$thisClearBtn.fadeIn(); $(this).css("background-image", "none"); } 
		else {$thisClearBtn.hide(); $(this).css("background-image", "url('"+imgLocalDir+"icn_Search.png')"); }
	});
	
	$(document.body).on("focus", ".autoSearchInput", function(event) { 
		var $thisClearBtn = $("#"+$(this).attr("id")+"ClearBtn");
		if($(this).val() !== "" && !$thisClearBtn.is(":visible")){$thisClearBtn.fadeIn(); $(this).css("background-image", "none"); } 
		else if($(this).val() === "" && $thisClearBtn.is(":visible")){$thisClearBtn.hide(); $(this).css("background-image", "url('"+imgLocalDir+"icn_Search.png')"); }
	});
	
	$(document.body).on("click", ".autoSearchClearBtn", function(event){ event.preventDefault();
		var $thisClearInput = $("#"+$(this).attr("id").replace("ClearBtn", "")), thisID = $thisClearInput;
		$(this).hide(); $thisClearInput.autoselector("reset"); $thisClearInput.css("background-image", "url('"+imgLocalDir+"icn_Search.png')"); $thisClearInput.trigger('focus'); });

	$(document.body).on("click", ".cmpntHead", function(event){ event.preventDefault();  toggleComponent($(this)); });
	
}

var toggleComponent = function($cmpntHdObj){
	var listObjID = $cmpntHdObj.attr("id").replace("-Lbl", "-Lst"),
		$listObj = $("#"+listObjID),
		$xpandObj = $cmpntHdObj.find(".menuExpand");
		
	if($cmpntHdObj.hasClass("expanded")){
		$cmpntHdObj.removeClass("expanded");
		$xpandObj.attr("title", clickViewDetails);
		$xpandObj.find("img").attr("title", clickViewDetails).attr("alt", clickViewDetails);
		snglSlideFadeTransition("hide", $listObj); }
	else{
		$cmpntHdObj.addClass("expanded");
		$xpandObj.attr("title", clickHideDetails);
		$xpandObj.find("img").attr("title", clickHideDetails).attr("alt", clickHideDetails);
		snglSlideFadeTransition("show", $listObj); }
}

//-------------------------------------------------------------------------------------------

function showClearBtnTest(objName){
	var	$thisField = $("#"+objName),
		$thisClearBtn = $("#"+objName+"ClearBtn");
	if($thisField.val() !== ""){$thisClearBtn.fadeIn(); $thisField.css("background-image", "none"); } 
	else {$thisClearBtn.hide(); $thisField.css("background-image", "url('"+imgLocalDir+"icn_Search.png')"); } }

//-------------------------------------------------------------------------------------------

function customerSwitch() {
	var ajaxSwitchCus = new GetHttpObject();
	ajaxSwitchCus.onreadystatechange = function(){
		if (ajaxSwitchCus.readyState==4 && ajaxSwitchCus.status == 200){
			if(ajaxSwitchCus.responseText){ location.reload(); }
			else{ alertDialog(unabletoSwitchMsg); }
		} else if(ajaxSwitchCus.readyState==4){
			alertDialog(unabletoSwitchMsg); //Unable to load details
		}
	};
	ajaxSwitchCus.open("GET","/secure/settings/users/changeUser.html?randomId="+$("#childCoSwitchACVal").val(),true);
	ajaxSwitchCus.send(); }

//-------------------------------------------------------------------------------------------

function selectAll(parentTier, checkBoxObj, inputObj)
{
	if(checkBoxObj.hasClass("checked"))
	{
		parentTier.find("ul.fullList > li:visible").each(function(index)
		{
			var currObjID = $(this).attr("id"), currID = currObjID.split("-"), selectedObjID = currObjID.replace("full", "select");
			if(inputObj.val() == ""){ inputObj.val(currID[1]).trigger('change'); }
			else{ if(inputObj.val().search(currID[1]) < 0){ inputObj.val(inputObj.val()+","+currID[1]).trigger('change'); } }
			$(this).fadeOut("fast").addClass("selected"); 
			parentTier.children("input").val(parentTier.children("input").val()+"");
			$("#"+selectedObjID).fadeIn("slow").addClass("selected");
		});
		parentTier.find("ul").removeClass("error");
	} else {
		parentTier.find("ul.fullList > li").each(function(index){ 
			var currObjID = $(this).attr("id"), currID = currObjID.split("-"), selectedObjID = currObjID.replace("full", "select");
			inputObj.val(stringArrayValCleanUp(inputObj.val(), currID[1]));
			$("#"+selectedObjID).fadeOut("fast").removeClass("selected"); 
			$(this).fadeIn("fast").removeClass("selected"); });
	}
	
	return true;
}

//-------------------------------------------------------------------------------------------

function filterAction()
{
	$(".filterHeader").on("mouseenter mouseleave", ".filterForm > li, .filterForm * li", function(event){
		var	$menu = $(this).children(".filterMenu"),
			$title =  $(this).children(".filterTitle");
		if(event.type === "mouseenter"){
			var stringCount = 0;
			var xHeight = parseInt($menu.find("li").css("font-size"));
			var ogWidth = $menu.width();
			$menu.find("li").each(function(){ tempCount = $(this).html().length; if(tempCount > stringCount){ stringCount = tempCount} });
			var listWidth = ((stringCount * (xHeight / 1.125)) > ogWidth)? stringCount * (xHeight/1.125) : ogWidth ; 
			$menu.width(listWidth).fadeIn(1, function(){$title.css("z-index", 61);});  }
		else if(event.type === "mouseleave"){ $title.css("z-index", 40); $menu.width("100%").hide(); }
	});
	
	$(".filterHeader").on("click", ".filterMenu > ul > li", function(event){
		if(!$(this).hasClass("selected") && $(this).html() != "Custom Date Range")
		{
			if($(this).siblings("li").hasClass("selected")){$(this).siblings("li").removeClass("selected");}
			$(this).addClass("selected");
			$(this).parents(".filterMenu").siblings(".filterTitle").children(".logValue").html("<a href='#' title='Undo Filter' class='undo'>"+$(this).html()+"</a>");
			var listItem = $(this).parents("ul").attr("id").replace("List", "");
			$("#"+listItem).val($(this).attr("id"));
			if($("#crntAlertList").length){ $("#crntAlertList").paginatetab("reloadData"); }
			else{ $("#activityLogList").paginatetab("reloadData"); }
		}
	});
	$(".filterHeader").on("click", ".filterTitle > .logValue > .undo", function(event){ 
		event.preventDefault(); 
		var parentSection = $(this).parents(".logValue");
		$(this).parents(".filterTitle").siblings(".filterMenu").children("ul").children("li").removeClass("selected"); 
		$(this).parents(".logValue").html("All");
		var listItem = parentSection.attr("id").replace("CurVal", "");
		$("#"+listItem).val("");
		if($("#crntAlertList").length){ $("#crntAlertList").paginatetab("reloadData"); }
		else{ $("#activityLogList").paginatetab("reloadData"); };
	});
	
	$("#timeRange > .filterMenu > ul > li").on('click', function(event){
		if($(this).html() == "Custom Date Range"){
			var listItem = $(this).parents("ul").attr("id").replace("List", "");
			$("#"+listItem).val("");
			$("#timeRange").fadeOut(250); 
			$("#dateRange").delay(250).fadeIn(250);
		}
	});
	$("#dateRange").on("click", ".undo", function(event){ event.preventDefault(); $("#dateRange").hide().fadeOut(250);  $( "#filterStartDate, #filterEndDate" ).val("").trigger('blur'); $("#timeRange").delay(250).fadeIn(250); $("#activityLogList").paginatetab("reloadData");});
	
	$("#dateRange").find(".search").on('click', function(event){
		event.preventDefault();
		$("#activityLogList").paginatetab("reloadData");
	});
}

//-------------------------------------------------------------------------------------------

function datePicker(minimumDate, maximumDate)
{
	var options = {};
	if(minimumDate && minimumDate != ''){ options.minDate = minimumDate; }
	if(maximumDate && maximumDate != ''){ options.maxDate = maximumDate; }
	
	createDatePicker(".datePicker", options);
}

//-------------------------------------------------------------------------------------------

	//HOLIDAYS FOR 2015 and 2016 Repalce if Migration goes past 2016
	var holidays = [[1, 1], [1, 2, 115], [2, 9, 115], [4, 3, 115], [4, 6, 115], [5, 18, 115], [7, 1], [8, 3, 115], [9, 7, 115], [10, 12, 115], [11, 11], [12, 25], [12, 26], 
				[12, 28, 115], [2, 8, 116], [3, 25, 116], [3, 28, 116], [5, 23, 116], [8, 1, 116], [9, 5, 116], [10, 10, 116], [12, 27, 116]];

	function holidayDays(date) {
	for (i = 0; i < holidays.length; i++) { 
		if (date.getMonth() == holidays[i][0] - 1 && date.getDate() == holidays[i][1] && (date.getYear() == holidays[i][2] || !holidays[i][2])) { return [false, '']; }}
	return [true, '']; }
	
	function noWeekendsOrHolidays(date) {
		var noWeekend = $.datepicker.noWeekends(date);
		if (noWeekend[0]) { return holidayDays(date); } else { return noWeekend; }}

//-------------------------------------------------------------------------------------------

function dateRange(minimumDate, maximumDate)
{
	var options = {};
	if(minimumDate && minimumDate != ''){ options.minDate = minimumDate; }
	if(maximumDate && maximumDate != ''){ options.maxDate = maximumDate; }
	
	coupleDateComponents(createDatePicker("#filterStartDate", options), null, createDatePicker("#filterEndDate", options), null);
}

//-------------------------------------------------------------------------------------------

function createDatePicker(selector, conf) { 
	var opts = {
		"dateFormat": "mm/dd/yy",
		"minDate": "01/01/2003",
		"numberOfMonths": 1,
		"onSelect": function(selectedDate) {
			$(this).siblings("label").hide();
		}
	};
	
	if((typeof conf === "undefined") || (conf === null)) {
		conf = opts;
	}
	else {
		opts = $.extend(opts, conf);
	}

	return $(selector).datepicker(opts).on('focus', function(){ $(this).select().trigger('change'); });
}

//-------------------------------------------------------------------------------------------

function soh(objIDMH, objIDH, objIDSH, rowObjID) //Set Object Height - controls the hieght of objects to take the visible height of the available window for content to scroll within.
//objIDMH - object ID array for page elements to set the Max Height
//objIDH - object ID array for page elements to set the Height
{
	var heightLimit = null;
	var searchHeaderHeight = ($(".searchHeader").length)? $(".searchHeader").outerHeight(true) : 0 ;
	if(objIDMH){for(x=0; x<objIDMH.length; x++){ 
		var objOffset = $("#"+objIDMH[x]).offset().top == 0 ?  200 : $("#"+objIDMH[x]).offset().top;
		heightLimit = $(window).height()-objOffset-20+searchHeaderHeight;
//console.log($("#"+objIDMH[x]).offset().top, objOffset, $(window).height(), heightLimit);
		$("#"+objIDMH[x]).css("max-height", heightLimit+"px"); }}
	if(objIDH){for(x=0; x<objIDH.length; x++){ 
//console.log(objIDH[x] + " | " + $("#"+objIDH[x]).height());
		var objOffset = $("#"+objIDH[x]).offset().top == 0 ?  200 : $("#"+objIDH[x]).offset().top;  
		heightLimit = $(window).height()-objOffset-20+searchHeaderHeight;
		$("#"+objIDH[x]).height(heightLimit); }}
	if(objIDSH){
		var y = objIDSH.length-2, parentHeight = $("#"+objIDSH[2]).parents("#"+objIDSH[1]).height()-$("#"+objIDSH[2]).position().top;
//console.log(objIDSH[1] +" | "+ parentHeight);
		for(x=2; x<objIDSH.length; x++){ 
			var heightDif = $("#"+objIDSH[x]).outerHeight()-$("#"+objIDSH[x]).height(), itemHeight = (parentHeight/y)-heightDif-8; 
			if(objIDSH[0] === "max"){ $("#"+objIDSH[x]).css("max-height", itemHeight+"px"); } else { $("#"+objIDSH[x]).height(itemHeight); } }}
	
	if(rowObjID && rowObjID.length > 0){
		var $firstObj = $("#" + rowObjID[0]);
		
		var totalObj = rowObjID.length;
		var totalSpace = 0;
		
		var $obj, $upperObj;
		for(x = 1; x < totalObj; x++) {
			$obj = $("#" + rowObjID[x]);
			$upperObj = $("#" + rowObjID[x - 1]);
			
			totalSpace += ($obj.offset().top - ($upperObj.offset().top + $upperObj.outerHeight()));
		}
		
		var availableHeight;
		if(heightLimit != null) {
			availableHeight = heightLimit - totalSpace;
		}
		else {
			availableHeight = $(window).height() - $firstObj.offset().top - totalSpace;
		}
		
		for(x=0; x<totalObj; x++) {
			$obj = $("#" + rowObjID[x]);
			var itemHeight = (availableHeight/(totalObj - x)) - ($obj.outerHeight() - $obj.height()) - 8; 
			$obj.css("max-height", itemHeight+"px");
			
			availableHeight -= $obj.height();
		}
	}
}

function sow(objIDW, parentObjW, diff) //Set Object Width - controls the width of objects that are to be dynamic in width based on window size but have fixed width elements they are trying to float with.
{
	if(objIDW instanceof Array === false){objIDW = [objIDW];}
	diff = (!diff || diff === "")? 10 : diff;
	for(x=0; x<objIDW.length; x++){
		var $obj = null;
		var maxWidthLimit = 0;
		if($("#"+objIDW[x]).length){ $obj = $("#"+objIDW[x]); }
		else { $obj = $("#"+parentObjW[x]).find("."+objIDW[x]+":first"); }
		if(parentObjW && parentObjW[x]){ maxWidthLimit = $("#"+parentObjW[x]).innerWidth(); }
		else{ maxWidthLimit = $obj.parent().innerWidth(); }
//console.log(parentObjW[x], maxWidthLimit); 
		var reduceWidthBy = 0;
		$obj.siblings().each(function(index, element) {reduceWidthBy = reduceWidthBy+$(this).outerWidth(true); });
		var objNewWidth = maxWidthLimit - reduceWidthBy - diff;
		$obj.width(objNewWidth);
//console.log(maxWidthLimit, reduceWidthBy, parseInt(diff), objNewWidth, $obj.outerWidth(true))
	}
}

function scrollListWidth(listObj){
	var statusListDif = 0;
	for(y=0; y < listObj.length; y++){
		var $divFinder = "";
		var $divParent = "";
		$.each($("#"+listObj[y]).find("li"), function(){ if($(this).is(":visible") && $divFinder === ""){ $divParent = $(this); $divFinder = $divParent.find("div"); return false; } })
		//if(!$("#"+listObj[y]).find("li:first").children("div:visible").length){ statusListDif = 6;}
//console.log(listObj[y] + " | " + y +" | "+ $("#"+listObj[y]).find("li:first").children("div:visible").length);
//console.log($divFinder);
		if($divFinder.length){
			var lastColIdx = $divFinder.length;
			while ((lastColIdx > 0) && (!$($divFinder[lastColIdx - 1]).is(":visible"))) {
				--lastColIdx;
			}
			
			var runningWidth = 0;
			$divFinder.each(function(index, element){
				if ($(element).is(":visible")) {
					var widthDiff = 0;
					if(index==0){ widthDiff = statusListDif +
						parseInt($divParent.css("border-left-width")) + 
						parseInt($divParent.css("padding-left")) +
						parseInt($divParent.css("margin-left")); }
//console.log(listObj[y] +"-"+ statusListDif +" : "+ parseInt($("#"+listObj[y]).find("li:first").css("border-left-width")) + " : " + parseInt($("#"+listObj[y]).find("li:first").css("padding-left")) + " : " + parseInt($("#"+listObj[y]).find("li:first").css("margin-left")));
					var	colCount =  index+1,
						thisLine = $divParent,
						thisCol = thisLine.find(".col"+colCount),
						headerCol = $("#"+listObj[y]+"Header").find(".col"+colCount);
//console.log(colCount == thisLine.children("div:visible").length);
					
					if(colCount == lastColIdx){ 
						headerCol.width($("#"+listObj[y]+"Header").outerWidth(true)-runningWidth);
						var positionTest = $("#"+listObj[y]+"Header").find(".col"+colCount).position();
						if(positionTest && positionTest.top > 0){ 
							marginRight = -1;
							while(positionTest.top > 0){ 
								$("#"+listObj[y]+"Header").find(".col"+colCount).css("margin-right", marginRight+"px");
								marginRight--;
								positionTest = $("#"+listObj[y]+"Header").find(".col"+colCount).position(); } }}
					else { 
						if(headerCol.attr("spancol") && headerCol.attr("spancol") == colCount){ 
							headerCol.each(function(){ 
								if($(this).hasClass("spanCol")){ $(this).width($(this).width()+thisCol.outerWidth()+widthDiff); }
								else{ $(this).width(thisCol.outerWidth()+widthDiff); }}); }
						else{ headerCol.width(thisCol.outerWidth()+widthDiff); }
						runningWidth += (thisCol.outerWidth()+widthDiff)} 
//console.log("col"+colCount+" : " + thisCol.width()+" : "+ thisCol.outerWidth()+" : "+ thisCol.outerWidth(true)+" : "+ widthDiff); 
			}}); } 
		else { 
			var colCount = $("#"+listObj[y]+"Header").find("li:first").children("div:visible").length;
			var runningWidth = 0;
			$("#"+listObj[y]+"Header").find("li:first").children("div:visible").each(function(index, element) { 
				if(index < (colCount-1)){ runningWidth += $(this).outerWidth(true); } });
			$("#"+listObj[y]+"Header").find(".col"+colCount).width($("#"+listObj[y]+"Header").outerWidth(true)-runningWidth);
			var positionTest = $("#"+listObj[y]+"Header").find(".col"+colCount).position();
			if(positionTest && positionTest.top > 0){ 
				marginRight = -1;
				while(positionTest.top > 0){ 
					$("#"+listObj[y]+"Header").find(".col"+colCount).css("margin-right", marginRight+"px");
					marginRight--;
					positionTest = $("#"+listObj[y]+"Header").find(".col"+colCount).position(); } }
//console.log(colCount);
			}}}

//--------------------------------------------------------------------------------------

function balanceNotificationList()
{
	var	headingHeight = $("#systemNotification").find("h3:first").outerHeight(true),
		parentHeight = $("#currentList").parent().innerHeight()-$("#currentList").position().top-headingHeight-12,
		listHeaderHeight = $("#currentNotificationsHeader").outerHeight(true),
		currentListCount = $("#currentNotifications").find("li").length,
		pastListCount = $("#pastNotifications").find("li").length,
		tempCurrentListHeight = currentListCount * $("#currentNotifications").find("li:first").outerHeight(true),
		tempPastListHeight = pastListCount * $("#pastNotifications").find("li:first").outerHeight(true),
		ogListHeight = (parentHeight/2)-listHeaderHeight,
		ogListAreaHeight = ogListHeight+listHeaderHeight+2;
//	Current == Placed // Past == Unplaced
	if(tempCurrentListHeight > ogListHeight && tempPastListHeight <= ogListHeight){
		tempPastListAreaHeight = tempPastListHeight+listHeaderHeight+2;
		tempCurrentListAreaHeight = parentHeight-tempPastListAreaHeight+2;
		tempCurrentListHeight = (tempCurrentListHeight > (tempCurrentListAreaHeight-listHeaderHeight))? tempCurrentListAreaHeight-listHeaderHeight : tempCurrentListHeight;
		$("#pastNotifications").height(tempPastListHeight);
		$("#pastList").height(tempPastListAreaHeight);
		$("#currentNotifications").height(tempCurrentListHeight);
		$("#currentList").height(tempCurrentListAreaHeight); }
	else if(tempCurrentListHeight <= ogListHeight && tempPastListHeight > ogListHeight){
		tempCurrentListAreaHeight = tempCurrentListHeight+listHeaderHeight+2;
		tempPastListAreaHeight = parentHeight-tempCurrentListAreaHeight+2;
		tempPastListHeight = (tempPastListHeight > (tempPastListAreaHeight-listHeaderHeight))? tempPastListAreaHeight-listHeaderHeight : tempPastListHeight;
		$("#pastNotifications").height(tempPastListHeight);
		$("#pastList").height(tempPastListAreaHeight);
		$("#currentNotifications").height(tempCurrentListHeight);
		$("#currentList").height(tempCurrentListAreaHeight); }
	else if(tempCurrentListHeight < ogListHeight && tempPastListHeight < ogListHeight){
		tempCurrentListAreaHeight = tempCurrentListHeight+listHeaderHeight+2;
		tempPastListAreaHeight = tempPastListHeight+listHeaderHeight+2;
		$("#pastNotifications").height(tempPastListHeight);
		$("#pastList").height(tempPastListAreaHeight);
		$("#currentNotifications").height(tempCurrentListHeight);
		$("#currentList").height(tempCurrentListAreaHeight); }
	else {
		$("#pastNotifications").height(ogListHeight);
		$("#pastList").height(ogListAreaHeight);
		$("#currentNotifications").height(ogListHeight);
		$("#currentList").height(ogListAreaHeight); }
}

//--------------------------------------------------------------------------------------

function checkDialogPlacement($thisDialog)
{
	var	dialogPosition = $thisDialog.parent(".ui-dialog").position(),
		dialogHeight = $thisDialog.parent(".ui-dialog").outerHeight(true),
		windowHieght = $(window).innerHeight();
	if(dialogPosition.top < 10) { $thisDialog.parent(".ui-dialog").css("top", "10px") }
	if((dialogPosition.top+dialogHeight) > windowHieght) { $thisDialog.parent(".ui-dialog").css("top", (windowHieght-dialogHeight-10)+"px") }
}

//--------------------------------------------------------------------------------------

function noteDialog(msgString, furtherAction, timeVal, closeHandlerFn, highlightMsg)
{ //console.log(typeof closeHandlerFn); 
	var $box = $("#messageResponseAlertBox");
	try {
		$box.dialog("close").dialog("destroy");
	}
	catch(error) {
		// DO NOTHING.
	}
	var maxHeight = $(window).innerHeight()-225;
	var boxStyle = (highlightMsg)? "max-height:"+maxHeight+"px; width: 93%; margin: 0; text-align: center; font-size:  1.25em;" : "max-height:"+maxHeight+"px; width: 97%; margin-top: 18px;";
	$box.dialog(alertNoticeOptions).html("<section class='innerBorder'><article class='scrollToTop' style='"+boxStyle+"'><strong>"+msgString+"</strong></article><a href='#' class='close' title='Close'>&nbsp;</a></section>").on("dialogclose", function(){ if(typeof closeHandlerFn === "function") { closeHandlerFn(); closeHandlerFn = false;} });
	if(furtherAction == "timeOut"){ 
		setTimeout(function() { $box.dialog("close").dialog("destroy"); }, timeVal); }
}

//--------------------------------------------------------------------------------------

function alertDialog(msgString, furtherAction, timeVal, trgrObj, closeHandlerFn)
{
	var $box = $("#messageResponseAlertBox");
	try {
		$box.dialog("close").dialog("destroy");
	}
	catch(error) {
		// DO NOTHING.
	}
	var maxHeight = $(window).innerHeight()-225;
	$box.dialog($.extend({}, alertNoticeOptions));
	$box.html("<section class='innerBorder'  class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article class='scrollToTop' style='max-height:"+maxHeight+"px'>"+msgString+"</article><a href='#' class='close' title='Close'>&nbsp;</a></section>");
	$box.on("dialogclose", function(event){
			if(trgrObj && (!$(trgrObj).is(".autoText"))) {
				trgrObj.trigger('focus');
			}
			if(typeof closeHandlerFn === "function") {
				closeHandlerFn(event);
			}
		});
	if(furtherAction == "timeOut"){ setTimeout("$('#messageResponseAlertBox').dialog( 'close' )", timeVal); }
}

//--------------------------------------------------------------------------------------

function formErrorDialog(msgString)
{
	var $box = $("#messageResponseAlertBox");
	try {
		$box.dialog("close").dialog("destroy");
	}
	catch(error) {
		// DO NOTHING
	}
	var maxHeight = $(window).innerHeight()-225;
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$box.dialog(alertNoticeOptions)
		.html(
			"<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+
			"</strong></h4><article class='scrollToTop' style='max-height:"+maxHeight+"px'>"+ locationErrorMsg +" <ul>"+msgString+"</ul></article><a href='#' class='close' title='Close'>&nbsp;</a></section>")
		.on( "dialogclose", function(event){
			$(".error:not(.autoText):first").trigger('focus');
		});
}

//--------------------------------------------------------------------------------------

function cancelConfirm(objWindow, objStatus, furtherAction, cancelAction, cstmConfirmMsg)
{ 
	var actionItem = null;
	var $box = $("#messageResponseAlertBox");
	try {
		$box.dialog("close").dialog("destroy");
	}
	catch(error) {
		// DO NOTHING.
	}

	if(objWindow === "reload"){ actionItem = ($("#mainContent").hasClass("edit"))? "confirmOption" : "reload"; } 
	else if(typeof objWindow === "object" && objWindow.hasClass("cleanReset")){ actionItem = ($("#mainContent").hasClass("edit"))? "confirmOption" : "cleanReset"; } 
	else if(objWindow === "xbpCleanEditReload"){ actionItem = ($("#mainContent").hasClass("edit"))? "confirmOption" : "xbpScrubQuerryReload"; }
	else if(typeof objWindow === "object" && objWindow.hasClass("edit")){ actionItem = "confirmOption"; }
	else{ actionItem = "closeWindow"; }
	
	if(actionItem === "reload"){ 
		var actnArgVal = getQueryVal(document.location.search.substring(1), "pgActn"),
			scrubClear = (actnArgVal === "multiEdit")? "all" : "pgActn",
			loadAction = (actnArgVal === "multiEdit")? "" : "&pgActn=display";
		var tempCancelUrl = location.pathname+"?"+scrubQueryString(document.location.search.substring(1), scrubClear);
		window.location = tempCancelUrl+loadAction; }
	else if(actionItem === "cleanReset"){ 
		objWindow[0].reset(); $("#mainContent").removeClass("edit"); 
		if(typeof furtherAction === 'function'){furtherAction();} }
	else if(actionItem === "xbpScrubQuerryReload"){ 
		var querryString = scrubXbpQuerryString(location.search); 
		window.location = location.pathname+querryString; }
	else if(actionItem === "closeWindow"){ 
		if(typeof furtherAction === 'function'){furtherAction();}
		objWindow.dialog("close"); 
		if(objStatus === "remove"){ $("html").remove(objWindow.attr("id")); }
		else if(objStatus === "reset"){ objWindow.find("form").trigger("reset"); }}
	else if(actionItem === "confirmOption"){
		var cancelButtons = {};
		cancelButtons.btn1 = {
			text : cancelCnfrmBtn,
			click : function() {
				if(typeof cancelAction === "function") { cancelAction(); }
				
				$(this).scrollTop(0); $(this).dialog( "close" ); 
				if(objWindow === "reload"){ 
					var actnArgVal = getQueryVal(document.location.search.substring(1), "pgActn"),
						scrubClear = (actnArgVal === "multiEdit")? "all" : "pgActn",
						loadAction = (actnArgVal === "multiEdit")? "" : "&pgActn=display";
					var tempCancelUrl = location.pathname+"?"+scrubQueryString(document.location.search.substring(1), scrubClear);
					window.location = tempCancelUrl+loadAction; }
				else if(objWindow === "xbpCleanEditReload"){ 
					var querryString = scrubXbpQuerryString(location.search);
					window.location = location.pathname+querryString; } 
				else if(objWindow.hasClass("cleanReset")){
					objWindow[0].reset(); $("#mainContent").removeClass("edit"); 
					if(typeof furtherAction === 'function'){furtherAction();} }
				else { 
					if(furtherAction){ furtherAction(); }
					objWindow.dialog("close"); objWindow.removeClass("edit"); 
					if(objStatus === "remove"){ $("html").remove(objWindow.attr("id")); }
					else if(objStatus === "reset"){ objWindow.find("form").trigger("reset"); } } }
		};
		cancelButtons.btn2 = {
			text : cancelGoBackBtn,
			click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		var maxHeight = $(window).innerHeight()-225;
		tempCancelConfirmMsg = (cstmConfirmMsg && cstmConfirmMsg !== "")? cstmConfirmMsg : cancelConfirmMsg ;
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$box.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+attentionTitle+"</strong></h4><article class='scrollToTop' style='max-height:"+maxHeight+"px'>"+tempCancelConfirmMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: cancelButtons });
		btnStatus(cancelCnfrmBtn); }
}

//--------------------------------------------------------------------------------------

function loadOverlay(){
	if($("#ajaxLoad.ui-dialog-content").length){$("#ajaxLoad.ui-dialog-content").dialog("destroy");}
	$("#ajaxLoad")
		.dialog(loadWindowOptions)
		.html("<section class='innerBorder'>"+ loadObj +"<br /><section id='loadMessage'>"+ loadMsg +"</section></section>"); }

//--------------------------------------------------------------------------------------

function noop() { }
/*
 * options = {
 * 		title: <String>,
 * 		message: <String>,
 * 		okLabel: <String>,
 * 		okCallback: <Function>,
 * 		cancelLabel: <String>,
 * 		cancelCallback: <Function>,
 * 		icon: <String for icon image path>
 * }
 */
function confirmDialog(options)
{
	var $box = $("#messageResponseAlertBox");
	try {
		$box.dialog("destroy");
	}
	catch(error) {
		// DO NOTHING.
	}
	
	var $message = $("<section></section>")
		.addClass("innerBorder")
		.append((options.icon) ? options.icon : "<img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'/>")
		.append($("<h4>")
			.append($("<strong>")
					.append((options.title) ? options.title : "Confirmation"))
		)
		.append($("<article>")
			.append(options.message)
		);
	
	var confirmButtons = {
		"okBtn": {
			text: (options.okLabel) ? options.okLabel : ((typeof okBtn !== "undefined") ? okBtn : "ok"),
			click: function(){
				$(this).scrollTop(0); $(this).dialog( "close" ).dialog("destroy");
				if (typeof options.okCallback === "function"){ options.okCallback.apply(this, arguments); } }},
		"cancelBtn": {
			text: (options.cancelLabel) ? options.cancelLabel : ((typeof cancelBtn !== "undefined") ? cancelBtn : "cancel"),
			click: function(){
				$(this).scrollTop(0); $(this).dialog( "close" ).dialog("destroy");
				if (typeof options.cancelCallback === "function"){ options.cancelCallback.apply(this, arguments); } }}};
	
	$box.empty()
		.append($message)
		.dialog($.extend(true, {}, genralAlertDialogOptions),{buttons: confirmButtons});
	
	var selectedBtn = (options.selectedText)?options.selectedText:confirmButtons.okBtn.text;
	btnStatus(selectedBtn);
}

function setupInEditAlertCancelFn(cancelFn) {
	$("#mainContent").data("cancelFn", cancelFn);
}

function inEditAlert(alertAction, linkObject)
{
	if(alertAction === "menu"){ alertDialog(leaveEditModeMsg); } // alert Message for Menu button or other actions that can't fire post popup.
	else if(alertAction === "site"){
		var callBackFunction = function(){ pausePropagation = false; };
		if(linkObject){
			if(typeof linkObject === "function"){ callBackFunction = linkObject; } 
			else if(linkObject.attr("href") && linkObject.attr("href") !== "#") { callBackFunction = (linkObject.attr("target") && linkObject.attr("target") === "_blank")? function(){ location.reload(); window.open(linkObject.attr("href")) } : function(){ window.location = linkObject.attr("href") }; }		
			else { callBackFunction = function(){
				forceClick = true;
				pausePropagation = false;
				$("#mainContent").removeClass("edit");
				if(!pausePropagationLoop){ linkObject.trigger('click'); }
			} }
		}
		
		var cancelChangesFn = callBackFunction;
		var performCancellation = $("#mainContent").data("cancelFn");
		if(typeof performCancellation === "function") {
			$("#mainContent").data("cancelFn", "");
			cancelChangesFn = function() {
				performCancellation(callBackFunction);
			};
		}
		
		confirmDialog({
			title: alertTitle,
			message: leaveEditModeOptionMsg,
			okLabel: cancelChangesBtn,
			okCallback: cancelChangesFn,
			cancelLabel: return2FormBtn,
			cancelCallback: function() { pausePropagation = "done"; },
			selectedText: return2FormBtn }); }
}


function clearErrors(){ if($('.error').length){ $('.error').removeClass("error"); } }

function getMaxCharsCountByCSS(elem) {
	var $elem = $(elem);
	var maxCharsCount = $elem.data("maxCharsCount");
	if(typeof maxCharsCount === "undefined") {
		maxCharsCount = -1;
		
		var classes = $(elem).attr("class").split(" ");
		var len = classes.length;
		for(i = -1; ++i < len; i) {
			var className = classes[i];
			if((className.length > 11) && (className.substring(0, 11) === "chrctrCount")) {
				var buff = className.substring(11);
				if(!isNaN(buff)) {
					buff = parseInt(buff, 10);
					if((maxCharsCount < 0) || (maxCharsCount < buff)) {
						maxCharsCount = buff;
					}
				}
			}
		}
		
		$elem.data("maxCharsCount", maxCharsCount);
	}
	
	return maxCharsCount;
}

function parseJSONDate(dateAttribute) {
	var result = null;
	var dateStr = null;
	if(dateAttribute) {
		if(typeof dateAttribute === "string") {
			dateStr = dateAttribute;
		}
		else if(typeof dateAttribute["$"] === "string") {
			dateStr = dateAttribute["$"];
		}
		
		if(dateStr) {
			var ctxArr = dateStr.split(" ");
			if(ctxArr.length >= 2) {
				// Handle Date...
				var datePartArr = ctxArr[0].split("-");
				var timePartArr = ctxArr[1].split(":");
				
				var secs = timePartArr[2];
				var millisecs = 0;
				var dotIdx = secs.indexOf(".");
				if(dotIdx > 0) {
					millisecs = secs.substring(dotIdx + 1);
					secs = secs.substring(0, dotIdx);
				}
				
				result = new Date(datePartArr[0], parseInt(datePartArr[1], 10) - 1, datePartArr[2], timePartArr[0], timePartArr[1], secs, millisecs);
			}
		}
	}
	
	return result;
}

function pairComparableComponents(lesserCmps, greaterCmps, comparators) {
	var $lesserCmps = $(lesserCmps);
	var $greaterCmps = $(greaterCmps);
	var cmprtrs = comparators;
	if(!Array.isArray(cmprtrs)) {
		cmprtrs = [ cmprtrs ];
	}
	
	var changeTracker = function() {
		var continueCmprsn = true;
		var $lesser, $greater, comparatorFn, cmprsnRes;
		var idx = -1, len = cmprtrs.length;
		while(continueCmprsn && (++idx < len)) {
			$lesser = $($lesserCmps.get(idx));
			$greater = $($greaterCmps.get(idx));
			comparatorFn = cmprtrs[idx];
			
			cmprsnRes = comparatorFn($lesser, $greater);
			if((typeof cmprsnRes === "undefined") || (cmprsnRes === null)) {
				// Continue to next component pair.
			}
			else if(cmprsnRes == false) {
				$lesserCmps.removeClass("error");
				$greaterCmps.removeClass("error");
				continueCmprsn = false;
			}
			else {
				$lesserCmps.addClass("error");
				$greaterCmps.addClass("error");
				
				formErrorDialog(cmprsnRes);
			}
		}
	};
	
	$lesserCmps.on('change', changeTracker);
	$greaterCmps.on('change', changeTracker);
}

function coupleDateComponents(startDate, startTime, endDate, endTime, mustNotEqual) {
	var hasDate = ($(startDate).length > 0) && ($(endDate).length > 0);
	var hasTime = ($(startTime).length > 0) && ($(endTime).length > 0);
	
	var $startDate = $();
	if(hasDate) {
		$startDate = $(startDate);
		//$startDate.datepicker("option", "originalMaxDate", $startDate.datepicker("option", "maxDate"));
	}
	
	var $startTime = $();
	if(hasTime) {
		$startTime = $(startTime);
	}
	
	var $endDate = $();
	if(hasDate) {
		$endDate = $(endDate);
		$endDate.datepicker("option", "origianlMinDate", $endDate.datepicker("option", "minDate"));
	}
	
	var $endTime = $();
	if(hasTime) {
		$endTime = $(endTime);
	}
	
	var notEqual = (mustNotEqual && ((mustNotEqual === true) || (mustNotEqual === "true")));
	var errorDateMsg = notEqual ? notLesserNorEqualMsg : notLesserDateMsg;
	var errorTimeMsg = errorDateMsg.replace("#lesser", labelStartTime).replace("#greater", labelEndTime);
	errorDateMsg = errorDateMsg.replace("#lesser", labelStartDate).replace("#greater", labelEndDate);
	
	var changeTracker = function() {
		var error = false;
		var startDateTS = (hasDate) ? $startDate.datepicker("getDate") : 0;
		if(startDateTS && (typeof startDateTS.getTime === "function")) {
			startDateTS = startDateTS.getTime();
		}
		
		var endDateTS = (hasDate) ? $endDate.datepicker("getDate") : 0;
		if(endDateTS && (typeof endDateTS.getTime === "function")) {
			endDateTS = endDateTS.getTime();
		}
		
		var displayedMessage = ($startDate.hasClass("error") && $endDate.hasClass("error")) || $startTime.hasClass("error") || $endTime.hasClass("error");
		if((startDateTS !== null) && (endDateTS !== null)) {
			if((startDateTS > endDateTS) || (notEqual && (startDateTS === endDateTS))) {
				error = true;
				$startDate.addClass("error");
				$endDate.addClass("error");
				
				if(!displayedMessage) {
					formErrorDialog(errorDateMsg);
				}
			}
			else if(startDateTS === endDateTS) {
				var startMins = $startTime.autominutes("getTimeInMinutes");
				var endMins = $endTime.autominutes("getTimeInMinutes");
				if((endMins !== 0) || (!$endTime.autominutes("isNextDayMode"))) {
					if((startMins !== null) && (endMins !== null)) {
						if((startMins > endMins) || (notEqual && (startMins === endMins))) {
							error = true;
							$startTime.addClass("error");
							$endTime.addClass("error");
							
							if(!displayedMessage) {
								formErrorDialog(errorTimeMsg);
							}
						}
					}
				}
			}
		}
		
		if(!error) {
			$startDate.removeClass("error");
			$endDate.removeClass("error");
			$startTime.removeClass("error");
			$endTime.removeClass("error");
		}
	};
	
	if(hasDate) {
		var fireChangeOnSelect = function(dateText, datepicker) {
			var $datepicker = $("#" + datepicker.id); //If you don't do this selector, you will have different object.
			$datepicker.trigger('change');
			
			var originalOnSelect = $datepicker.data("originalOnSelect");
			if(typeof originalOnSelect === "function") {
				originalOnSelect.call($datepicker, dateText, datepicker);
			}
		};
		
		/*
		$startDate.data("originalOnSelect", $startDate.datepicker("option", "onSelect"));
		$startDate
			.on('change', changeTracker)
			.datepicker("option", "beforeShow", function(inputElem, dpInstance) {
				var currDate = $startDate.datepicker("getDate");
				var endDate = $endDate.datepicker("getDate");
				if(endDate && currDate && (endDate.getTime() < currDate.getTime())) {
					endDate = currDate;
				}
				
				if(!endDate) {
					endDate = $startDate.datepicker("option", "originalMaxDate");
				}
				
				return {
					"maxDate": endDate
				};
			})
			.datepicker("option", "onSelect", fireChangeOnSelect);
		*/
		
		$endDate.data("originalOnSelect", $endDate.datepicker("option", "onSelect"));
		$endDate
			.on('change', changeTracker)
			.datepicker("option", "beforeShow", function(inputElem, dpInstance) {
				var currDate = $endDate.datepicker("getDate");
				var startDate = $startDate.datepicker("getDate");
				
				if(startDate && currDate && (startDate.getTime() > currDate.getTime())) {
					startDate = currDate;
				}
				
				if(!startDate) {
					startDate = $endDate.datepicker("option", "originalMinDate");
				}
				
				if(notEqual){
					startDate = moment(startDate); startDate.add(1, "d");
					startDate = startDate.format("MM/DD/YYYY");
				}
				
				return {
					"minDate": startDate
				};
			})
			.datepicker("option", "onSelect", fireChangeOnSelect);
	}
	
	if(hasTime) {
		/*
		$startTime
			.on("itemSelected", changeTracker)
			.on("beforeopen", function(itemsList) {
				var endTimeStr = $endTime.autominutes("getTime");
				if(hasDate && endTimeStr) {
					var startDateTS = $startDate.datepicker("getDate");
					if(startDateTS) {
						startDateTS = startDateTS.getTime();
					}
					
					var endDateTS = $endDate.datepicker("getDate");
					if(endDateTS) {
						endDateTS = endDateTS.getTime();
					}
					
					if((startDateTS) && (endDateTS) && (startDateTS !== endDateTS)) {
						endTimeStr = null;
					}
				}
				
				if(endTimeStr) {
					$startTime.autominutes("option", "maxLimit", endTimeStr);
				}
				else {
					$startTime.autominutes("option", "maxLimit", null);
				}
			});
		*/
	
		$endTime
			.on("itemSelected", changeTracker)
			.on("beforeopen", function(itemsList) {
				var startTimeStr = $startTime.autominutes("getTime");
				if(hasDate && startTimeStr) {
					var startDateTS = $startDate.datepicker("getDate");
					if(startDateTS) {
						startDateTS = startDateTS.getTime();
					}
					
					var endDateTS = $endDate.datepicker("getDate");
					if(endDateTS) {
						endDateTS = endDateTS.getTime();
					}
					
					if((startDateTS) && (endDateTS) && (startDateTS !== endDateTS)) {
						startTimeStr = null;
					}
				}
				
				if(startTimeStr) {
					$endTime.autominutes("option", "minLimit", startTimeStr);
				}
				else {
					$endTime.autominutes("option", "minLimit", null);
				}
			});
	}
}

function syncMenuItems(source, target, checkVisibility) {
	var $source = $(source);
	var $target = $(target);
	
	var itemActive, itemMatched, classes, srcItems = {};
	$source.find("a").each(function(index, element) {
		classes = $(element).attr("class").split(" ");
		for(i = -1; ++i < classes.length; i) {
			srcItems[classes[i]] = !checkVisibility || !$(element).is(":hidden");
		}
	});
	
	var hasMenuItems = false;
	$target.find("a").each(function(index, element) {
		itemActive = false;
		itemMatched = false;
		
		classes = $(element).attr("class").split(" ");
		for(i = -1; ++i < classes.length; i) {
			itemActive = srcItems[classes[i]];
			if((typeof itemActive === "undefined") || (itemActive === null)) {
				itemActive = false;
			}
			else {
				itemMatched = true;
				if(itemActive === true) {
					break;
				}
			}
		}
		
		if(checkVisibility && !itemMatched) {
			hasMenuItems = true;
		}
		else {
			if(itemActive === true) {
				hasMenuItems = true;
				$(element).show();
			}
			else {
				$(element).hide();
			}
		}
	});
	
	return hasMenuItems;
}

function blockPage() {
	var blocker = $("<div>").addClass("messageBox").html(pleaseWait).dialog({
		"dialogClass": "btnAlert",
		"modal": true,
		"resizable": false,
		"closeOnEscape": false,
		"dragStop": function(){ checkDialogPlacement($(this)); }
	});
	
	return blocker.dialog("open");
}

function makeSortableList(list, sortOptionsHash) {
	if(!sortOptionsHash) {
		sortOptionsHash = {};
	}

	var $list = $(list);
	var $header = $list.find("li.listHeader");
	if($header.length > 0) {
		$header = $header.find(">.sort");
		for(x = -1; ++x < $header.length; x) {
			var $sorter = $($header.get(x)).on('click', performListSort);
			var cssClasses = $sorter.attr("class").split(" ");
			for(i = -1; ++i < cssClasses.length; i) {
				if(cssClasses[i].match(/col[0-9]+/)) {
					var sortOptions = $.extend({
						"order": "desc",
						"targetCSS": cssClasses[i]
					}, sortOptionsHash[cssClasses[i]]);
					
					if($sorter.filter(".current").length > 0) { 
// print sorter object to see if I can get the ttoltip to switch
//Sort tooltip Messages = sortAscendingMsg | sortDescendingMsg
						var isAsc = true;
						if($sorter.find("img[src='"+imgLocalDir+"icn_ArwUp.png']").length > 0) {
							isAsc = false;
						}
						
						if(sortOptions.invertOrderIdenticator) {
							isAsc = !isAsc;
						}
						
						sortOptions["order"] = (isAsc) ? "asc" : "desc";
					}
					
					$sorter.data("sortOptions", sortOptions);
					
					break;
				}
			}
		}
	}
}

function performListSort(event) {
	var $sorter = $(event.currentTarget);
	var sortOptions = $sorter.data("sortOptions");
	
	if(sortOptions.order === "desc") {
		sortOptions.order = "asc";
	}
	else {
		sortOptions.order = "desc";
	}
	
	_sortList($sorter, sortOptions);
}

function _sortList($sorter, sortOptions) {
	$sorter.addClass("current").siblings().removeClass("current");
	
	var dataSelector = "." + sortOptions.targetCSS;
	if(sortOptions.dataSelector) {
		dataSelector += ">" + sortOptions.dataSelector;
	}
	
	var isAsc = true;
	if(sortOptions.order === "desc") {
		isAsc = false;
	}
	
	if(sortOptions.invertOrderIdenticator) {
		isAsc = !isAsc;
	}
	
	if(isAsc) {
		$sorter.find("img[src='"+imgLocalDir+"icn_ArwUp.png']").attr("src", imgLocalDir+"icn_ArwDown.png");
	}
	else {
		$sorter.find("img[src='"+imgLocalDir+"icn_ArwDown.png']").attr("src", imgLocalDir+"icn_ArwUp.png");
	}
	
	$sorter.parents("ul").find(">li:not(.listHeader)").tsort(dataSelector, sortOptions);
}

function refreshListSort(list) {
	var $activeSorters = $(list).find("li.listHeader").find(".sort").filter(".current");
	if($activeSorters.length > 0) {
		var $sorter = $($activeSorters.get(0));
		_sortList($sorter, $sorter.data("sortOptions"));
	}
}

/* Deprecated 
function applyTemplate(templateStr, dataObj) {
	return templateStr
		.replace(/\#{([^}]+)}/g, function(wholeMatched, firstGroupMatched) {
			var result = dataObj[firstGroupMatched];
			if((typeof result === "undefined") || (result === null) || (result.length <= 0)) {
				result = " ";
			}
			
			return result;
		})
		.replace(/\@{([^}]+)}/g, function(wholeMatched, firstGroupMatched) {
			var result = firstGroupMatched;
			if((typeof result === "undefined") || (result === null) || (result.length <= 0)) {
				result = "";
			}
			else {
				result = (new Function("dataObj", "with(dataObj) {" + result + "}"))(dataObj);
			}
			
			return result;
		});
}
*/

function compileTemplate(templateStr, contextRefName) {
	var result = null;
	var ctxName = contextRefName || '__data';
	
	var templateType = typeof templateStr;
	if(templateType === "function") {
		result = templateStr;
	}
	else if(templateType !== "string") {
		throw new "compileTemplate only support either function or string !";
	}
	else {
		var implStr = [];
		implStr.push("var out = [];");
		
		implStr.push("for(var attr in arguments[0]) {");
		implStr.push("arguments[0][attr] = (typeof arguments[0][attr] !== 'string') ? arguments[0][attr] : escapeHTML(arguments[0][attr]).split('\"').join('&quot;');");
		implStr.push("}");
		
		implStr.push("with(" + ctxName + ") {");
		
		var expression = false, statement = false, skipChar = false;
		var prevIdx = 0, currChar;
		var idx = -1, len = templateStr.length;
		while(++idx < len) {
			currChar = templateStr.charAt(idx);
			if((expression === false) && (statement === false)) {
				switch(currChar) {
					case '\n':
					case '\r':
						if(skipChar === false) {
							if(prevIdx !== idx) {
								implStr.push("\tout.push('" + templateStr.substring(prevIdx, idx).split("'").join("\\'") + "');\n");
							}
							
							skipChar = true;
						}
						
						break;
					case '[':
						skipChar = false;
						if(templateStr.charAt(idx + 1) === '%') {
							if(prevIdx !== idx) {
								implStr.push("\tout.push('" + templateStr.substring(prevIdx, idx) + "');");
							}
							
							++idx;
							if(templateStr.charAt(idx + 1) === '=') {
								++idx;
								expression = true;
							}
							else {
								statement = true;
							}
							
							prevIdx = idx + 1;
						}
						
						break;
					default:
						if(skipChar === true) {
							prevIdx = idx;
							skipChar = false;
						}
						
						break;
				}
			}
			else {
				switch(currChar) {
					case '\n':
					case '\r':
						if(skipChar === false) {
							if(prevIdx === idx) {
								// DO NOTHING.
							}
							else if(statement === true) {
								implStr.push($("<div>").html(templateStr.substring(prevIdx, idx)).text());
							}
							else {
								implStr.push("\tout.push(" + $("<div>").html(templateStr.substring(prevIdx, idx)).text() + ");\n");
							}
							skipChar = true;
						}
						
						break;
					case '%':
						if(templateStr.charAt(idx + 1) === ']') {
							if(statement === true) {
								implStr.push($("<div>").html(templateStr.substring(prevIdx, idx)).text());
							}
							else {
								implStr.push("\tout.push(" + $("<div>").html(templateStr.substring(prevIdx, idx)).text() + ");");
							}
							++idx;
							prevIdx = idx + 1;
							
							statement = false;
							expression = false;
						}
					default:
						if(skipChar === true) {
							prevIdx = idx;
							skipChar = false;
						}
						
						break;
				}
			}
		}
		
		if((prevIdx < len) && (!skipChar)) {
			if(statement === true) {
				implStr.push($("<div>").html(templateStr.substring(prevIdx, len)).text());
			}
			else if(expression === true) {
				implStr.push("\tout.push(" + $("<div>").html(templateStr.substring(prevIdx, len)).text() + ");");
			}
			else {
				implStr.push("\tout.push('" + templateStr.substring(prevIdx, len) + "');");
			}
		}
		
		implStr.push("}");
		implStr.push("return out.join('')");

		result = new Function(ctxName, implStr.join('\n'));
	}
	
	return result;
}

function scrubQueryString(curQuerryString, argumentObj)
{
	var newQuerry = [];
	
	argumentObj = (Array.isArray(argumentObj)) ? argumentObj : [ argumentObj ];
	
	var argName, removeAll = false, removeParamNames = {};
	var idx = -1, len = argumentObj.length;
	while((!removeAll) && (++idx < len)) {
		argName = argumentObj[idx].trim();
		if(argName === "all") {
			removeAll = true;
		}
		else {
			removeParamNames[argName] = true;
		}
	}
	
	var querryArray = curQuerryString.split("&");

	var queryObj;
	idx = -1; len = querryArray.length;
	while(++idx < len) {
		queryObj = querryArray[idx].split("=");
		argName = queryObj[0];
		if(((removeAll) && ((argName === "sessionToken") || (argName === "customerID"))) || ((!removeAll) && (removeParamNames[argName] !== true))) {
			newQuerry.push(querryArray[idx]);
		}
	}
	
	return newQuerry.join("&");
}

function getQueryVal(curQuerryString, argumentObj){
	var querryArray = curQuerryString.split("&"),
		queryObj = [],
		argumentVal = false;
	idx = -1; len = querryArray.length;
	while(++idx < len) {
		queryObj = querryArray[idx].split("=");
		if(argumentObj === queryObj[0]){ argumentVal = queryObj[1]; break; } }
	return argumentVal; }

var inlineValidatorsHash = {};

function setupCharBlocker(selector, regularExpr, errorMessage) {
	var regex = regularExpr;
	if(typeof regex === "string") {
		regex = new RegExp(regex);
	}
	
	inlineValidatorsHash[selector] = function(e) {
		var $this = $(this);
		var pass = (typeof $this.attr("readonly") !== "undefined") && ($this.attr("readonly") != null);
		
		if(!pass) {
			if(regex.test($this.val().replace(/[\r\n]/g, "")) === true) {
				if(e && (typeof e.preventDefault === "function")) {
					e.preventDefault();
				}
				
				if(!$this.hasClass("error")) {
					$this.addClass("error");
					alertDialog(resolveMessage(errorMessage),'','',$(this));
				}
			}
			else {
				pass = true;
				$this.removeClass("error");
			}
		}
		
		return pass;
	};
	
	$(document.body)
		.on("keypress", selector, function(e) {
			if((!e.ctrlKey) && (!$(this).attr("readonly"))) {
				var val = null;
				if(e.charCode) {
					// If the key that user press is not a visible character, modern browser will not fire keypress event.
					val = ((e.charCode === 13) || (e.charCode === 27)) ? null : String.fromCharCode(e.charCode);
				}
				else if(e.keyCode && isIE()) {
					val = ((e.keyCode <= 13) || (e.keyCode === 27)) ? null : String.fromCharCode(e.keyCode);
				}
				
				if((val !== null) && (regex.test(val))) {
					var $this = $(this);
					
					e.preventDefault();
					$this.addClass("inline-error");
					alertDialog(resolveMessage(errorMessage), '', '', $this, function() { $this.removeClass("inline-error"); });
				}
			}
		})
		.on("change", selector, inlineValidatorsHash[selector]);
}

function inlineValidate(input) {
	var validator = null;
	var $input = $(input);
	var selector;
	for(selector in inlineValidatorsHash) {
		if((typeof selector === "string") && ($input.is(selector))) {
			validator = inlineValidatorsHash[selector];
			break;
		}
	}
	
	return (!validator) ? true : validator.call($input);
}

function resolveMessage(message) {
	var result = message;
	if(typeof result === "function") { result = result(); }
	return result; }

function deserializeCheckBoxValues(checkBoxesContainer, values, hiddenInputs) {
	var valsHash = {};
	var $hiddenInputs = $(hiddenInputs);
	var allItems;
	if(!values) {
		values = $hiddenInputs.val();
	}
	if(typeof values === "string") {
		allItems = values.split(",");
	}
	else {
		allItems = [ values + "" ];
	}
	
	var idx = -1, len = allItems.length;
	while(++idx < len) {
		valsHash[allItems[idx]] = true;
	}
	
	allItems = $(checkBoxesContainer).find("label:has(>.checkBox)");
	idx = -1, len = allItems.length;
	var id, $checkBoxLabel, $checkBox, _idx;
	while(++idx < len) {
		$checkBoxLabel = $(allItems[idx]);
		$checkBox = $checkBoxLabel.find(">.checkBox");
		
		id = $checkBoxLabel.attr("id");
		_idx = id.indexOf("_");
		if(_idx >= 0) {
			id = id.substring(_idx + 1);
		}
		
		if(valsHash[id]) {
			$checkBox.addClass("checked");
		}
		else {
			$checkBox.removeClass("checked");
		}
	}
	if(values && $hiddenInputs.val() !== values){ $hiddenInputs.val(values); } else { $hiddenInputs.val(values); }
}

function deserializeBooleanCheckBox(checkBoxesContainer, values, hiddenInputs) {
	var $hiddenInputs = $(hiddenInputs);
	if(!values) {
		values = $hiddenInput.val();
	}
	
	var checked = (values && ((values.trim() == "true") || (values == 1)));
	var allItems = $(checkBoxesContainer).find("label:has(>.checkBox)");
	var idx = -1, len = allItems.length;
	var id, $checkBoxLabel, $checkBox;
	while(++idx < len) {
		$checkBoxLabel = $(allItems[idx]);
		$checkBox = $checkBoxLabel.find(">.checkBox");
		
		if(checked) {
			$checkBox.addClass("checked");
		}
		else {
			$checkBox.removeClass("checked");
		}
	}
	
	$hiddenInputs.val((checked) ? "true" : "false").trigger('change');
}

function setupCheckBoxes(checkBoxesContainer, targetObj, isBooleanCheckBox) {
	var $target = $(targetObj);
	var isBoolean = (isBooleanCheckBox && (isBooleanCheckBox === true)) ? true : false;
	$(checkBoxesContainer).on("click", "label:has(>.checkBox)", function(event) {
		var $this = $(this);
		var $checkBox = $this.find(">.checkBox");
		event.preventDefault();
		
		var currVal = $(targetObj).val().trim();
		var val = $this.attr("id");
		var _idx = val.indexOf("_");
		if(_idx >= 0) {
			val = val.substring(_idx + 1);
		}
		
		if(!$checkBox.hasClass("checked")) {
			if(isBoolean) {
				currVal = "true";
				$(targetObj).trigger('change');
			}
			else if(currVal.length <= 0) {
				currVal += val;
				$(targetObj).trigger('change');
			}
			else {
				currVal += "," + val;
				$(targetObj).trigger('change');
			}
			
			$checkBox.addClass("checked");
		}
		else {
			if(isBoolean) {
				currVal = "false";
				$(targetObj).trigger('change');
			}
			else {
				var valsArr = currVal.split(",");
				currVal = [];
				
				var idx = -1, len = valsArr.length;
				while(++idx < len) {
					if(val !== valsArr[idx]) {
						currVal.push(valsArr[idx]);
					}
				}
				
				currVal = currVal.join(",");
				$(targetObj).trigger('change');
			}
			
			$checkBox.removeClass("checked");
		}
		
		$target.val(currVal);
		$(checkBoxesContainer).trigger("checkBoxClick", this);
		
		return false;
	});
	
	if(isBoolean) {
		deserializeBooleanCheckBox(checkBoxesContainer, $target.val());
	}
	else {
		deserializeCheckBoxValues(checkBoxesContainer, $target.val());
	}
}

function setupFilterMenu($container, menuWidth) {
	menuWidth = menuWidth || "180px";
	var hoverHandler = function(event) {
		var	$this = $(this),
			$menu = $this.children(".filterMenu"),
			$title = $this.children(".filterTitle");
		
		if(event.type === "mouseenter") {
			$menu.width(menuWidth).fadeIn(1, function(){$title.css("z-index", 51);});
		}
		else if(event.type === "mouseleave") {
			$title.css("z-index", 40); $menu.width("100%").hide();
		}
	};

	var menuClickHandler = function() {
		var $this = $(this);
		if(!$this.hasClass("selected")) {
			var $input, $undo;
			
			var $menu = $this.parents("li:has(.filterMenu):first");
			var menuContext = $menu.data("filterMenu.context");
			if(menuContext) {
				$input = menuContext.input;
				$undo = menuContext.undo;
			}
			else {
				$input = $menu.find("input[type=hidden]:first");
				$undo = $("<a href='#' title='Undo Filter' class='undo'/></a>").appendTo($menu.find(".logValue:first"));
				
				menuContext = {
					"input": $input,
					"undo": $undo,
					"defaultVal": $input.val()
				};
				
				$menu.data("filterMenu.context", menuContext);
			}
			
			$this.siblings("li").removeClass("selected");
			
			var val = $this.attr("name");
			$this.addClass("selected");
			$input.val(val);
			$undo.html($this.html());
			$undo.attr("name", val);
		}
	};
	
	var undoHandler = function() {
		var $this = $(this);
		var $menu = $this.parents("li:has(.filterMenu):first");
		var menuContext = $filterMenu.data("filterMenu.context");
		if(menuContext) {
			var $input = menuContext.input;
			$menu.find("li[name='" + $input.val() + "']").removeClass("selected");
			
			$input.val(menuContext.defaultVal);
			var $defaultSel = $menu.find("li[name='" + menuContext.defaultVal + "']").addClass("selected");
			if($defaultSel.length > 0) {
				menuContext.undo.html($defaultSel.html());
			}
		}
	};
	
	var filterMenusList = $container.find("li:has(.filterMenu)");
	var $filterMenu, $hiddenInput, idx = -1, len = filterMenusList.length;
	while(++idx < len) {
		$filterMenu = $(filterMenusList[idx]);
		$hiddenInput = $filterMenu.find("input[type=hidden]");
		menuClickHandler.call($filterMenu.find("li[name='" + $hiddenInput.val() + "']"));
		$filterMenu
			.on("mouseenter mouseleave", hoverHandler)
			.on("click", ".filterMenu > ul > li", menuClickHandler)
			.on("click", ".undo", undoHandler);
	}
}

function getAutoWidthFilterWidthFromCSS(elem) {
	var result = null;
	
	var $autoWidthBox = $(elem);
	
	result = $autoWidthBox.data("autoWidthFilter");
	if(typeof result === "undefined") {
		var classes = $autoWidthBox.attr("class").split(" ");
		var idx = classes.length, className;
		while((result == null) && (--idx >= 0)) {
			className = classes[idx];
			if((className.length > 15) && (className.substring(0, 15) === "autoWidthFilter")) {
				result = className.substring(15);
				if(isNaN(result)) {
					result = null;
				}
				else {
					result = parseInt(result, 10);
					if(result <= 0) {
						result = null;
					}
				}
			}
		}
		
		if(result == null) {
			throw new "Invalid autoWidthFilter element: " + elem.attr("id");
		}
		else {
			$autoWidthBox.data("autoWidthFilter", result);
		}
	}
	
	return result;
}

function resizeAutoWidthFilters() {
	var filterBoxes = $(".filterHeader");
	var filterBoxIdx = filterBoxes.length;
	var $filterBox, space, $filterForm;
	var autoWidthElems, autoWidthElemIdx, $autoWidthBox, shrinkRatio;
	var totalElemsWidth, newTotalElemsWidth, totalAssetsWidth, widthBuffer;
	var widthPercentage;
	while(--filterBoxIdx >= 0) {
		$filterBox = $(filterBoxes[filterBoxIdx]);
		if($filterBox.is(":visible")){
			totalElemsWidth = 0;
			totalAssetsWidth = 0;
			
			autoWidthElems = $filterBox.find("[class*=autoWidthFilter]");
			autoWidthElemIdx = autoWidthElems.length;
			if(autoWidthElemIdx > 0) {
				space = $filterBox.innerWidth() - $filterBox.children(".title").width();
				space -= 20; // More or Less.
				if(!space) {
					// Magic Number
					space = 120;
				}
				else {
					// Reduce space with "fixed" width assets.
					var awAssets = $filterBox.find(".autoWidthAsset");
					var assetIdx = awAssets.length, $asset;
					while(--assetIdx >= 0) {
						$asset = $(awAssets[assetIdx]);
						widthBuffer = $asset.outerWidth();
						
						space -= widthBuffer;
						totalAssetsWidth += widthBuffer;
					}
				}

				
				newTotalElemsWidth = totalAssetsWidth;
				
				while(--autoWidthElemIdx >= 0) {
					totalElemsWidth += $(autoWidthElems[autoWidthElemIdx]).outerWidth();
				}
				
				$filterForm = $filterBox.find(".filterForm");
				shrinkRatio = space / $filterForm.width();
				
				autoWidthElemIdx = autoWidthElems.length;
				while(--autoWidthElemIdx >= 0) {
					$autoWidthBox = $(autoWidthElems[autoWidthElemIdx]);
					widthPercentage = getAutoWidthFilterWidthFromCSS($autoWidthBox);
					$autoWidthBox.outerWidth((totalElemsWidth * widthPercentage * shrinkRatio) / 100);
					newTotalElemsWidth += $autoWidthBox.outerWidth();
				}
				
				
				if(newTotalElemsWidth > space) {
					shrinkRatio = space / (newTotalElemsWidth + 50);
					autoWidthElemIdx = autoWidthElems.length;
					while(--autoWidthElemIdx >= 0) {
						$autoWidthBox = $(autoWidthElems[autoWidthElemIdx]);
						$autoWidthBox.outerWidth($autoWidthBox.outerWidth() * shrinkRatio);
					}
				}
			}
			//$filterForm.width(space);
		}
	}
}

//--------------------------------------------------------------------------------------

function checkVisibleItem($item, $list)
{
	var	$thisPosition = $item.position().top,
		$thisListHieght = $list.innerHeight(),
		$thisListVisibleStart = $list.scrollTop(),
		$thisListVisibleEnd = $thisListHieght + $thisListVisibleStart;
	if($thisPosition < $thisListVisibleStart || $thisPosition > $thisListVisibleEnd){ $list.scrollTop($thisPosition) }
}

//--------------------------------------------------------------------------------------

var selectedItemLoad = false, selectedItemID = "";

function updateFilter(fltrValue, itmValue, eventType){ 
	if($(".filterForm").length && fltrValue !== ""){
		var autoTextID = "#"+$(".filterForm").find("input.autoText").attr("id");
		if($("#DefinedLocations").length){
			if(!$(fltrValue).length){ $(".menuList").find("li").each(function(index, element){ if($(this).attr("id").indexOf(fltrValue) != -1){ fltrValue = $(this).attr("id"); } }); }	
			$(autoTextID).autoselector("setSelectedValue", fltrValue, false); }
		else if($("#DefinedPaystations").length && $(".sysAdminWorkSpace").length){ 
			$(autoTextID).autoselector({"data":locationRouteObj}).autoselector("setSelectedValue", fltrValue, false); }
		else{ 
			$(autoTextID).autoselector("setSelectedValue", fltrValue, false); }}
	
	if(itmValue && itmValue !== ""){
		selectedItemLoad = true, selectedItemID = itmValue;
		if($("#DefinedRoutes").length){
			$("#routeList").find("li#route_"+selectedItemID).trigger('click'); checkVisibleItem($("#routeList").find("li#route_"+selectedItemID), $("#routeList")); }
		else if($("#DefinedAlerts").length){
			$("#alertList").find("li#alert_"+selectedItemID).trigger('click'); checkVisibleItem($("#alertList").find("li#alert_"+selectedItemID), $("#alertList")); }
		else if($("#DefinedRoles").length){
			$("#roleList").find("li#role_"+selectedItemID).trigger('click'); checkVisibleItem($("#roleList").find("li#role_"+selectedItemID), $("#roleList")); }
		else if($("#DefinedRateProfile").length){
			$("#rateProfileList").paginatetab("reloadPageWithItem", { "id": selectedItemID }, function(){
				$("ul.pageItems").find("li").each(function(index, element) {
				if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
					$(this).trigger('click'); checkVisibleItem($(this), $("ul.pageItems")); } }); }); }
		else if($("#Configurations").length){
			/* --- MICRO SERVICE CHANGE IS REQUIRED --- $("#configList").paginatetab("reloadPageWithItem", { "id": selectedItemID }, function(){
				$("ul.pageItems").find("li").each(function(index, element) {
				if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
					if(eventType && eventType === "searchDisplay"){
						$(this).addClass("selected"); checkVisibleItem($(this), $("ul.pageItems"));}
					else{
						$(this).trigger('click'); checkVisibleItem($(this), $("ul.pageItems")); } } }); });*/ }
		else{
			$("#paystationList").paginatetab("reloadPageWithItem", { "id": selectedItemID }, function(){ 
				$("ul.pageItems").find("li").each(function(index, element) {
				if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){
					if(eventType && eventType === "searchDisplay"){
						$(this).addClass("selected"); checkVisibleItem($(this), $("ul.pageItems"));}
					else{
						$(this).trigger('click'); checkVisibleItem($(this), $("ul.pageItems")); } } }); }); } } }

//--------------------------------------------------------------------------------------

function payStationSelectList(curentObjID, locationRandomId, routeRandomId, filterStatus)
{
	var posLists = [];
	var ajaxPOSSelectList = GetHttpObject();
	var childFullList = "";
	var childSelectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	ajaxPOSSelectList.onreadystatechange = function()	{
		if (ajaxPOSSelectList.readyState==4 && ajaxPOSSelectList.status == 200){
				var posData =	JSON.parse(ajaxPOSSelectList.responseText);
				
				if(posData.payStationsList.elements){
					posData = posData.payStationsList.elements; 
				
					if(posData instanceof Array === false){ posData = [posData] } 
					
					for(var x = 0; x < posData.length; x++){
						var childStatus = posData[x].status;
						var childName = posData[x].name;
						var childID = posData[x].randomId;
						var childSlctdStyle = "";
						
						if(filterStatus === true && $("#formPaystationChildren ul.selectedList").find("li#select-"+childID).hasClass("selected")){ childStatus = "selected" }						
						if(childStatus != "selected" && allSelected == true){ allSelected = false; }
						if(childStatus == "selected"){ 
							var colPlacement = selectedCount%2+1;
							childStatus = childStatus;
							selectedCount++;
						}
						childFullList += '<li id="full-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
						if(filterStatus !== true){
							childSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus + '"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>'; } }
							
					$("#formPaystationChildren  #psSelectionsAll").parents("label").show();
					if(allSelected == true){$("#formPaystationChildren  #psSelectionsAll").addClass("checked");} 
					else{$("#formPaystationChildren  #psSelectionsAll").removeClass("checked");} }
				else { childFullList += '<li>'+ noPaystationsMsg +'</li>'; }
				$("#formPaystationChildren ul.fullList").html(childFullList);

				if(filterStatus !== true){ $("#formPaystationChildren ul.selectedList").html(childSelectedList); }
		} else if(ajaxPOSSelectList.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxPOSSelectList.open("GET",LOC.payStationList+"&wrappedObject.selectedId="+curentObjID+"&wrappedObject.locationRandomId="+locationRandomId+"&wrappedObject.routeRandomId="+routeRandomId+"&"+document.location.search.substring(),true);
	ajaxPOSSelectList.send(); }

//--------------------------------------------------------------------------------------

function slideFadeTransition($hideObj, $showObj, callbackOnHide, callbackOnShow, hideDuration, showDuration){
	if(typeof hideDuration === undefined || hideDuration === ''){hideDuration = "slow"}
	if(typeof showDuration === undefined || showDuration === ''){showDuration = "slow"}
	if($hideObj.is(":animated")){ $hideObj.stop(true); } 
	if($showObj.is(":animated")){ $showObj.stop(true); } 
	
	$hideObj.slideUp(hideDuration, function(){ 
			$showObj
				.css('opacity', 0).slideDown(showDuration, function(){ 
					$(this).removeClass("hide"); if(typeof callbackOnShow === 'function'){ callbackOnShow(); }; })
				.animate({ opacity: 1 },{ queue: false, duration: showDuration });
			if(typeof callbackOnHide === 'function'){ callbackOnHide() }; })
		.animate({ opacity: 0 },{ queue: false, duration: hideDuration, complete: function(){ $(this).addClass("hide"); } }); }

//--------------------------------------------------------------------------------------

function snglSlideFadeTransition(action, $transitionObj, callBack, duration){
	if(typeof duration === undefined || duration === ''){duration = "slow"}
	if(action === "show"){
		 $transitionObj
		 	.css('opacity', 0).slideDown(duration, function(){ 
				$(this).removeClass("hide"); if(typeof callBack === 'function'){ callBack(); }; })
			.animate({ opacity: 1 },{ queue: false, duration: duration }); }
	else if(action === "hide"){
		$transitionObj
			.slideUp(duration)
			.animate({ opacity: 0 },{ queue: false, duration: duration, complete: function(){ 
				$(this).addClass("hide");  if(typeof callBack === 'function'){ callBack(); } } }); } }

//--------------------------------------------------------------------------------------

// PAYSTATION DETAILS GLOBALSCRIPTS //

//--------------------------------------------------------------------------------------

function validatePOSValue(posValue)
{
	var returnVal;
	var posValueStrng = posValue.toString();
	if((posValue === 32767 || posValueStrng.match(/^32767/)) || (posValue === 59012.6 || posValueStrng.match(/^59012.6/))){ returnVal = "N/A"; } 
	else { returnVal =  posValue}
	return returnVal;
}

//--------------------------------------------------------------------------------------

function loadPosTab(tabID, psID, psData, staticLoad){
	if(staticLoad){
		$("#detailView").find("article").hide(); 
		$("#psNav").find(".tabSection").removeClass("current"); }
	else{
		$("#detailView").find("article:visible").each(function(index, element) {
			if(tabID.indexOf("Transactions") > -1){ var parentTabArea = tabID.replace("Transactions", "")+"Area"; }
			else if(tabID.indexOf("Notes") > -1){ var parentTabArea = tabID.replace("Notes", "")+"Area"; } 
			else{ var parentTabArea = tabID+"Area"; }
			if($(this).attr("id") !== parentTabArea){ snglSlideFadeTransition("hide",$(this)); } }); 
		$("#psNav").find(".tabSection.current").toggleClass("current"); }

	switch(tabID){
			case 'alerts':
				$("#alertsBtn").addClass("current");
				snglSlideFadeTransition("show",$("#alertsArea"));
				showPSAlerts(psID, psData);
				break;
			case 'info':
				$("#infoBtn").addClass("current");
				showPSInfo(psID, psData);
				break;
			case 'reports':
			case 'reportsTransactions':
			case 'reportsNotes':
				$("#reportsBtn").addClass("current");
				var	$curListArea = true;
					showCallBack = function(){ 
					var subTab = '';
					if(tabID === 'reportsTransactions'){ subTab =  'transaction'} else if(tabID === 'reportsNotes'){ subTab = 'notes'}
					if(!$("#reportsArea").is(":visible")){ snglSlideFadeTransition("show",$("#reportsArea"), function(){ showPSCollections(psID, psData, subTab); }); }
					else{ showPSCollections(psID, psData, subTab); } };
				$(".reportListAreas").each(function(){ if(!$(this).hasClass("hide")){ $curListArea = $(this); return false; }});
				if($curListArea){ snglSlideFadeTransition("hide", $curListArea, showCallBack); }
				else{ showCallBack(); }
				break;
			case 'status':
			default:
				$("#statusBtn").addClass("current");
				snglSlideFadeTransition("show",$("#statusArea"));
				showPSStatus(psID, psData);
				break; } }	


//--------------------------------------------------------------------------------------

function reloadTab()
{
	$(document.body).on("click", ".refreshPOSCurrentTab", function(){ 
		var $currentTab = $("#paystationNavArea").find(".current"); 
		if($currentTab.attr("id") === "reportsBtn"){ 
			var section = $("#reportsArea").find(".tab.current").attr("id").replace("Btn", "");
			if(section === "collectionReport"){ getCollReports(); } else if(section === "transactionReport"){ getTransReports(); } else { getNoteHistory(); }; }
		else if($currentTab.attr("id") === "alertsBtn"){ posRecentActivity(); }
		else{ loadPosTab($currentTab.attr("id"), $("#contentID").val()); } });
}

//--------------------------------------------------------------------------------------

function expandChart(xpndObj, wdgtArea)
{
	var	parentObj = (wdgtArea === "psWdgt")? xpndObj.parents(".psWidget") : xpndObj.parents(".widgetObject"),
		wdgtID = (wdgtArea === "psWdgt")? parentObj.attr("id").replace("Wdgt","") : parentObj.attr("id").replace("widgetID_",""),
		wdgtTitle = parentObj.find(".wdgtName");

	var xpndChrtSettings = 	{ 
							title: wdgtTitle.html(),
							modal: true,
							classes: { "ui-dialog": "closeDialog" },
							draggable: false,
							resizable: false,
							closeOnEscape: true,
							position: {my: "center center"},
							width: $(window).innerWidth()-20,
							height: $(window).innerHeight()-20,
							close: function(){ 
								$(this).attr("wdgtID", "").attr("style", ""); 
								$("#xpndContent").html("").attr("style", "");
								$("#expandedWdgt").dialog("destroy"); }
						};	
	
	$("#expandedWdgt").attr("wdgtID", wdgtID).dialog(xpndChrtSettings);
	
	if(wdgtArea === "psWdgt"){ displaySensorGraph($("#xpndContent"), $("#"+wdgtID+"GraphData")); }
	else{ displayChart(wdgtID, "xpnd"); }
}

//--------------------------------------------------------------------------------------

function displaySensorGraph($graphObj, $graphDataObj)
{
	Highcharts.setOptions({
		global: {
			useUTC: false
		}
	});
	
	var dataHtml =	$graphDataObj.html();
	var tempChart = null;
	var data = (dataHtml != "")? JSON.parse($graphDataObj.html()) : "";
	var seriesList = [,,,,,,];
	var timeList = [(todayMillSec-(oneDayMillSec*6)),(todayMillSec-(oneDayMillSec*5)),(todayMillSec-(oneDayMillSec*4)),(todayMillSec-(oneDayMillSec*3)),(todayMillSec-(oneDayMillSec*2)),(todayMillSec-oneDayMillSec),todayMillSec];
	var targetList = null;
	var plotLines = null;
	var noData = false;
	
	var	valueFormat = new Array();
		valueFormat["V"] = "V";
		valueFormat["C"] = "\u00B0 C";
		valueFormat["F"] = "\u00B0 F";
		valueFormat["Milliamps"] = "mA";
		valueFormat["%"] = "%";

	var	customerTime = -customerOffset,
		userTime = new Date().getTimezoneOffset();
	var timeMod = (customerTime !== userTime)?(userTime - customerTime)*60000 : 0;

	if(typeof data.trendData != "undefined"){
		if(typeof data.trendData.dataPointList != "undefined"){
			if(data.trendData.dataPointList instanceof Array === false){
				seriesList = [data.trendData.dataPointList];
				timeList = [data.trendData.dateTimeStampList]; } 
			else { seriesList = data.trendData.dataPointList; timeList = data.trendData.dateTimeStampList;} }
		else{ noData = true; }
		
		if(typeof data.trendData.targetValueList != "undefined"){
			if(data.trendData.targetValueList instanceof Array === false){
				targetList = [data.trendData.targetValueList]; } 
			else { targetList = data.trendData.targetValueList; } }
	
		if(targetList && targetList.length){
			plotLines = new Array();
			for(y = 0; y < targetList.length; y++){
				if(typeof targetList[y] != "undefined"){
					var tempval = targetList[y];
					plotLines.push({color: '#F88B2B', width: 1, value: tempval, zIndex: 5}); } } } }
	else{ noData = true;}

	var highChartData = new Array();
	for(x = 0; x < seriesList.length; x++){ var timeStamp = Date.parse(new Date(timeList[x])); highChartData.push([timeMod+timeStamp, seriesList[x]]); }

	$graphObj.highcharts({
		chart: { zoomType: 'x', resetZoomButton: { theme: { fill: '#F4F5F6', stroke: '#CFD2D4', style:{ color: '#1E76A2' }, r: 4, states: { hover: { fill: '#AECEDE', stroke: '#3d89af', style: { color: '#003E5E' } } } } }, type: "line" },
		title: { text: null, margin: 0 },
		xAxis: { type: 'datetime', dateTimeLabelFormats: timeFormat, labels: { overflow: 'justify' }, tickmarkPlacement: 'on' },
		yAxis: { startOnTick: false, endOnTick: false, plotBands: plotLines, title:{ text: null } },
		colors: ["#003E5E"],
		tooltip: { 
			formatter : function() { return '<strong>'+ Highcharts.dateFormat('%a, %b %d %I:%M %P', this.x) +'</strong><br/>' + this.y+valueFormat[data.trendData.unitType]; },
			crosshairs : [{ width: 1, color: "#CFD2D4" }, { width: 1, color: "#CFD2D4" }] },
		legend: { enabled: false },
		plotOptions: {
			line: { 	lineWidth: 2, states: { hover: { lineWidth: 2 } },
					marker: { enabled: false, symbol: 'circle', states: { hover: { enabled: true, fillColor: '#F88B2B'} } } } },
		credits: { enabled: false },	
		navigation: { buttonOptions: { enabled: false } },
		series:[{data: highChartData}]
	});
	
	if(noData){ $graphObj.append("<section class='noWdgtData'><span>"+noDataHistoryMsg+"</span></section>"); }
}

//--------------------------------------------------------------------------------------

function psBatteryVoltage(psID)
{
	if($("#batteryVoltageWdgt").length > 0){
		if(psType === 9){ $("#batteryVoltageWdgt").hide(); }
		else{ 
			$("#batteryVoltageWdgt").show();
			var ajaxPSBatteryVolt = GetHttpObject({"showLoader": false});
			ajaxPSBatteryVolt.onreadystatechange = function()
			{
				if (ajaxPSBatteryVolt.readyState==4 && ajaxPSBatteryVolt.status == 200){
					var batteryVoltage = JSON.parse(ajaxPSBatteryVolt.responseText);
					var value = batteryVoltage.float.toFixed(1);
					var posWdgtValue = validatePOSValue(value);
					$("#batteryVoltageWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Battery.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'V</h1></section>');
				} else if(ajaxPSBatteryVolt.readyState==4){
					$("#batteryVoltageWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Battery.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A V</h1></section>');
				}
			};
			ajaxPSBatteryVolt.open("GET",LOC.payStationsBattVolt+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSBatteryVolt.send(); 
			
			var ajaxPSBatteryVoltGraph = GetHttpObject({"showLoader": false});
			ajaxPSBatteryVoltGraph.onreadystatechange = function()
			{
				if (ajaxPSBatteryVoltGraph.readyState==4 ){
					$("#batteryVoltageGraphData").html(ajaxPSBatteryVoltGraph.responseText);
					displaySensorGraph($("#batteryVoltageGraph"), $("#batteryVoltageGraphData"));
				}
			};
			ajaxPSBatteryVoltGraph.open("GET",LOC.payStationsBattVoltTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSBatteryVoltGraph.send(); } }
}

function psInputCurrent(psID)
{
	if($("#inputCurrentWdgt").length > 0){
		if(psType === 9){ $("#inputCurrentWdgt").hide(); }
		else{ 
			$("#inputCurrentWdgt").show();
			var ajaxPSInputCurrent = GetHttpObject({"showLoader": false});
			ajaxPSInputCurrent.onreadystatechange = function()
			{
				if (ajaxPSInputCurrent.readyState==4 && ajaxPSInputCurrent.status == 200){
					var inputCurrent = JSON.parse(ajaxPSInputCurrent.responseText);
					var value = inputCurrent.float.toFixed(1);
					var posWdgtValue = validatePOSValue(value);
					$("#inputCurrentWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Plug.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'mA</h1></section>');
				} else if(ajaxPSInputCurrent.readyState==4){
					$("#inputCurrentWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Plug.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A mA</h1></section>');
				}
			};
			ajaxPSInputCurrent.open("GET",LOC.payStationsInputCurr+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSInputCurrent.send(); 
			
			var ajaxPSInputCurrentGraph = GetHttpObject({"showLoader": false});
			ajaxPSInputCurrentGraph.onreadystatechange = function()
			{
				if (ajaxPSInputCurrentGraph.readyState==4){
					$("#inputCurrentGraphData").html(ajaxPSInputCurrentGraph.responseText);
					displaySensorGraph($("#inputCurrentGraph"), $("#inputCurrentGraphData"));
				}
			};
			ajaxPSInputCurrentGraph.open("GET",LOC.payStationsInputCurrTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSInputCurrentGraph.send(); } }
}

function psAmbientTemprature(psID)
{
	if($("#ambientTemperatureWdgt").length > 0){
		if(psType === 9){ $("#ambientTemperatureWdgt").hide(); }
		else{ 
			$("#ambientTemperatureWdgt").show();
			var ajaxPSambientTemp = GetHttpObject({"showLoader": false});
			ajaxPSambientTemp.onreadystatechange = function()
			{
				if (ajaxPSambientTemp.readyState==4 && ajaxPSambientTemp.status == 200){
					var ambientTemp = JSON.parse(ajaxPSambientTemp.responseText);
					var value = ambientTemp.float.toFixed(1);
					var posWdgtValue = validatePOSValue(value);
					$("#ambientTemperatureWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Temp.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'&deg; '+tempLabel+'</h1></section>');
				} else if(ajaxPSambientTemp.readyState==4){
					$("#ambientTemperatureWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Temp.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A&deg; '+tempLabel+'</h1></section>');
				}
			};
			ajaxPSambientTemp.open("GET",LOC.payStationsAmbientTemp+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSambientTemp.send();
			
			var ajaxPSambientTempGraph = GetHttpObject({"showLoader": false});
			ajaxPSambientTempGraph.onreadystatechange = function()
			{
				if (ajaxPSambientTempGraph.readyState==4){
					$("#ambientTemperatureGraphData").html(ajaxPSambientTempGraph.responseText);
					displaySensorGraph($("#ambientTemperatureGraph"), $("#ambientTemperatureGraphData"));
				}
			};
			ajaxPSambientTempGraph.open("GET",LOC.payStationsAmbientTempTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSambientTempGraph.send(); } }
}

function psRelativeHumidity(psID)
{
	if($("#relativeHumidityWdgt").length > 0){
		if(psType === 9){ $("#relativeHumidityWdgt").hide(); }
		else{ 
			$("#relativeHumidityWdgt").show();
			var ajaxPSRelativeHumidity = GetHttpObject({"showLoader": false});
			ajaxPSRelativeHumidity.onreadystatechange = function()
			{
				if (ajaxPSRelativeHumidity.readyState==4 && ajaxPSRelativeHumidity.status == 200){
					var humidity = JSON.parse(ajaxPSRelativeHumidity.responseText);
					var value = humidity.float.toFixed(1);
					var posWdgtValue = validatePOSValue(value);
					$("#relativeHumidityWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Humid.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'%</h1></section>');
				} else if(ajaxPSRelativeHumidity.readyState==4){
					$("#relativeHumidityWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Humid.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A%</h1></section>');
				}
			};
			ajaxPSRelativeHumidity.open("GET",LOC.payStationsRelHumidity+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSRelativeHumidity.send();
			
			var ajaxPSRelativeHumidityGraph = GetHttpObject({"showLoader": false});
			ajaxPSRelativeHumidityGraph.onreadystatechange = function()
			{
				if (ajaxPSRelativeHumidityGraph.readyState==4){
					$("#relativeHumidityGraphData").html(ajaxPSRelativeHumidityGraph.responseText);
					displaySensorGraph($("#relativeHumidityGraph"), $("#relativeHumidityGraphData"));
				}
			};
			ajaxPSRelativeHumidityGraph.open("GET",LOC.payStationsRelHumidityTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSRelativeHumidityGraph.send(); } }
}

function psControllerTemprature(psID)
{
	if($("#ControllerTemperatureWdgt").length > 0){
		if(psType === 9){ $("#ControllerTemperatureWdgt").hide(); }
		else{ 
			$("#ControllerTemperatureWdgt").show();
			var ajaxPSControllerTemp = GetHttpObject({"showLoader": false});
			ajaxPSControllerTemp.onreadystatechange = function()
			{
				if (ajaxPSControllerTemp.readyState==4 && ajaxPSControllerTemp.status == 200){
					var controlerTemp = JSON.parse(ajaxPSControllerTemp.responseText);
					var value = controlerTemp.float.toFixed(1);
					var posWdgtValue = validatePOSValue(value);
					$("#ControllerTemperatureWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Temp.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">'+ posWdgtValue +'&deg; '+tempLabel+'</h1></section>');
				} else if(ajaxPSControllerTemp.readyState==4){
					$("#ControllerTemperatureWdgt").children(".groupBoxBody").html(
						'<section class="snglValueIcon"><img src="'+imgLocalDir+'icnLrg-Temp.jpg" width="125" height="125" border="0" /></section>' +
						'<section class="snglValue"><h1 class="valueDisplay">N/A&deg; '+tempLabel+'</h1></section>');
				}
			};
			ajaxPSControllerTemp.open("GET",LOC.payStationsCntrlrTemp+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSControllerTemp.send();
			
			var ajaxPSControllerTempGraph = GetHttpObject({"showLoader": false});
			ajaxPSControllerTempGraph.onreadystatechange = function()
			{
				if (ajaxPSControllerTempGraph.readyState==4){
					$("#ControllerTemperatureGraphData").html(ajaxPSControllerTempGraph.responseText);
					displaySensorGraph($("#ControllerTemperatureGraph"), $("#ControllerTemperatureGraphData"));
				}
			};
			ajaxPSControllerTempGraph.open("GET",LOC.payStationsCntrlrTempTrend+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
			ajaxPSControllerTempGraph.send(); } }
}

function posRecentActivity() {
	$("#crntAlertList")
		.paginatetab({
			"url": LOC.payStationsAlertDetails,
			"formSelector": "#currentAlertFilterForm",
			"rowTemplate": unescape($("#activityLogTemplate").html()),
			"responseExtractor": extractPOSActivityLog,
			"dataKeyExtractor": extractPOSDataKey,
			"emptyMessage": noActivityMsg,
			"submissionFormat": "GET",
			"emptyMsgTemplate": unescape($("#activityEmptyLogTemplate").html()) })
		.paginatetab("setupSortingMenu", "#crntAlertListHeader")
		.paginatetab("reloadData");
}

function extractPOSDataKey(activityMsgs, context) {
	return context.dataKey;
}

function extractPOSActivityLog(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).alertInfo;
	var result = buffer.alerts;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

function extractPOSCollList(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).payStationListCollectionReportList;
	var result = buffer.payStationListCollectionReport;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

function extractPOSTransList(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).payStationListTransactionReportList;
	var result = buffer.payStationListTransactionReportList;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;

	return result;
}


function collectionReportList(psID, sortOrder, sortItem)
{
	$("#collectionReportList")
	.paginatetab({
		"url": LOC.viewCollectionReports,
		"formSelector": "#collectionReportSortForm",
		"rowTemplate": unescape($("#collectionItemTemplate").html()),
		"responseExtractor": extractPOSCollList,
		"dataKeyExtractor": extractPOSDataKey,
		"emptyMessage": noReportsMsg,
		"submissionFormat": "GET"
	})
	.paginatetab("setupSortingMenu", "#collectionReportListHeader")
	.paginatetab("reloadData");
}

function transactionReportList(psID, sortOrder, sortItem)
{
	$("#transactionReportList")
	.paginatetab({
		"url": LOC.viewTransactionReports,
		"formSelector": "#transactionReportSortForm",
		"rowTemplate": unescape($("#transactionItemTemplate").html()),
		"responseExtractor": extractPOSTransList,
		"dataKeyExtractor": extractPOSDataKey,
		"emptyMessage": noReportsMsg,
		"submissionFormat": "GET"
	})
	.paginatetab("setupSortingMenu", "#transactionReportListHeader")
	.paginatetab("reloadData");
}

function getCollReports()
{
	var ajaxCollectionForm = GetHttpObject();
	var psID = $("#contentID").val();
	ajaxCollectionForm.onreadystatechange = function()
	{
		if (ajaxCollectionForm.readyState==4 && ajaxCollectionForm.status == 200){
			$("#collectionReportArea").scrollTop(0).html(ajaxCollectionForm.responseText);
			collectionReportList(); } 
		else if(ajaxCollectionForm.readyState==4){
			alertDialog(systemErrorMsg); }//Unable to load details
	};
	ajaxCollectionForm.open("GET",LOC.viewCollectionForm+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
	ajaxCollectionForm.send();
	$(document.body).scrollTop(0);
}

function getTransReports()
{
	var psID = $("#contentID").val();
	var ajaxTransactionsForm = GetHttpObject();	
	ajaxTransactionsForm.onreadystatechange = function()
	{
		if (ajaxTransactionsForm.readyState==4 && ajaxTransactionsForm.status == 200){
			$("#transactionReportArea").scrollTop(0).html(ajaxTransactionsForm.responseText);
			transactionReportList(); } 
		else if(ajaxTransactionsForm.readyState==4){
			alertDialog(systemErrorMsg); }//Unable to load details
	};
	ajaxTransactionsForm.open("GET",LOC.viewTransactionForm+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
	ajaxTransactionsForm.send();
	$(document.body).scrollTop(0);
}

function showPSCollections(psID, psData, subTab){
	if(psType === 9){ 
		$("#collectionReportBtn, #collectionReportArea").hide();
		$("#reportsArea").find(".tab.current").removeClass("current");
		$("#transactionReportBtn").addClass("current");
		snglSlideFadeTransition("show",$("#transactionReportArea"), function(){ getTransReports(); actvTabSwtch = false; } ); }
	else{
		if(!$("#collectionReportBtn").is(":visible")){ $("#collectionReportBtn, #collectionReportArea").show(); }
		if(subTab && subTab !== ''){
			$("#reportsArea").find(".tab.current").removeClass("current");
			if(subTab === "transaction"){
				$("#transactionReportBtn").addClass("current");
				snglSlideFadeTransition("show",$("#transactionReportArea"), function(){ getTransReports(); actvTabSwtch = false; }); }
			else if(subTab === "notes"){
				$("#notesHistoryBtn").addClass("current");
				snglSlideFadeTransition("show",$("#notesHistoryArea"), function(){ getNoteHistory(); actvTabSwtch = false; } ); } }
		else {
			$("#reportsArea").find(".tab.current").removeClass("current");
			$("#collectionReportBtn").addClass("current");
			snglSlideFadeTransition("show",$("#collectionReportArea"), function(){ getCollReports(); actvTabSwtch = false; } ); } } }
			
var R = 0;
function reportTabClick($this){
	actvTabSwtch = true;
	var section = $this.attr("id").replace("Btn", "");
	var thisStatus = $this.hasClass("current");
	if(!thisStatus){ 
		$this.siblings(".tab").each(function(){ 
			var $that = $(this);
			if($that.hasClass("current")){
				$that.removeClass("current"); 
				$this.addClass("current");
				var curArea = $that.attr("id").replace("Btn", "Area"); 					
				$("#"+curArea).fadeOut("fast", function(){
					if(section === "collectionReport"){ getCollReports(); } 
					else if(section === "transactionReport"){ getTransReports(); } 
					else { getNoteHistory(); } }); } }); }
	else{ actvTabSwtch = false; }}

function showPSAlerts(psID){ 
	var ajaxAlertFilters = GetHttpObject();
	ajaxAlertFilters.onreadystatechange = function()
	{
		if (ajaxAlertFilters.readyState==4 && ajaxAlertFilters.status == 200){
			$("#alertFilterMenu").html(ajaxAlertFilters.responseText);
			posRecentActivity();
			$("#alertsArea").fadeIn(function(){ actvTabSwtch = false; }); } 
		else if(ajaxAlertFilters.readyState==4){
			alertDialog(systemErrorMsg); }//Unable to load details
	};
	ajaxAlertFilters.open("GET",LOC.payStationsAlertFilters+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
	ajaxAlertFilters.send();
	$(document.body).scrollTop(0);
}

function clearPOSAlerts($that){
	var	alertID = $that.attr("id").split("_")[1],
		alertRow = $("#alert_"+alertID),
		activeAlertCount = $("#activeAlerts").html(),
		alertTypeVar = ($that.hasClass("posActiveAlert"))? "activePosAlertRandomId" : "posAlertRandomId";
	
	$.ajax({
		url: LOC.clearAlert+"?"+alertTypeVar+"="+alertID,
		success: function(data) { if(data){
			$that.fadeOut("slow");
			alertRow.removeClass("activeAlert");
			if(activeAlertCount === "1"){
				$("#activeAlerts").fadeOut("slow", function(){$(this).html(0);});
				$("#paystationList").find(".selected").find("section").attr("class", "severityNull"); }
			else{ 
				var ogCount = parseInt(activeAlertCount); 
				$("#activeAlerts").html(ogCount-1);
				var highestAlrt = $("#crntAlertList").find(".activeAlert:first").attr("class");
				$("#paystationList").find(".selected").find("section").attr("class", highestAlrt); }
			alertRow.children(".col4").html("<p>"+resolvedNow+"</p>");
			alertRow.children(".col3").html("<p>"+justNowMsg+"</p>"); } },
		error: function(data){ alertDialog(systemErrorMsg); } }); }

function showPSInfo(psID){ 
	var ajaxPSInfo = GetHttpObject(),
		showCallBack = function(){ actvTabSwtch = false; };
	ajaxPSInfo.onreadystatechange = function()
	{
		if (ajaxPSInfo.readyState==4 && ajaxPSInfo.status == 200){
			//Process JSON
			var psInfoData = JSON.parse(ajaxPSInfo.responseText);
			if(!psInfoData.payStationDetails.name || psInfoData.payStationDetails.name == ""){ $("#psNameLine").hide(); }
			else{ $("#psNameLine").show(); $("#psName").html(psInfoData.payStationDetails.name); }
			
			if(!psInfoData.payStationDetails.serialNumber || psInfoData.payStationDetails.serialNumber == ""){ $("#psSerialNumberLine").hide(); }
			else{ $("#psSerialNumberLine").show(); $("#psSerialNumber").html(psInfoData.payStationDetails.serialNumber); }
			
			if(!psInfoData.payStationDetails.controllerSerialNumber || psInfoData.payStationDetails.controllerSerialNumber == ""){ $("#psControllerNumberLine").hide(); }
			else{ $("#psControllerNumberLine").show(); $("#psControllerNumber").html(psInfoData.payStationDetails.controllerSerialNumber); }
			
			if(!psInfoData.payStationDetails.payStationType || psInfoData.payStationDetails.payStationType == ""){ $("#psTypeLine").hide(); }
			else{ $("#psTypeLine").show(); $("#psType").html(psInfoData.payStationDetails.payStationType); }
			
			if(!psInfoData.payStationDetails.upgradeStatus || psInfoData.payStationDetails.upgradeStatus == ""){ $("#psLinuxStatusLine").hide(); }
			else{ $("#psLinuxStatusLine").show(); $("#psLinuxStatus").html(psInfoData.payStationDetails.upgradeStatus); }
			
			if(!psInfoData.payStationDetails.softwareVersion || psInfoData.payStationDetails.softwareVersion == ""){ $("#psSoftwareVersionLine").hide(); }
			else{ $("#psSoftwareVersionLine").show(); $("#psSoftwareVersion").html(psInfoData.payStationDetails.softwareVersion); }
			
			if(!psInfoData.payStationDetails.firmwareVersion || psInfoData.payStationDetails.firmwareVersion == ""){ $("#psFirmwareVersionLine").hide(); }
			else{ $("#psFirmwareVersionLine").show(); $("#psFirmwareVersion").html(psInfoData.payStationDetails.firmwareVersion); }

			if(!psInfoData.payStationDetails.groupName || psInfoData.payStationDetails.groupName == ""){ $("#psConfigGroupLine").hide(); $("#psConfigGroupVersion").html(""); }
			else{ 
				var tempQuerry = (typeof curCustomerID !== 'undefined')? "&customerID="+curCustomerID : "";
				$("#psConfigGroupLine").show();
				var tempConfigObj = (posConfigPermission)? $("<a>").html(psInfoData.payStationDetails.groupName).attr("href", LOC.configGroupDetails+"?pgActn=display&itemID="+psInfoData.payStationDetails.groupId+tempQuerry) : $("<span>").html(psInfoData.payStationDetails.groupName);
				$("#psConfigGroupVersion").html(tempConfigObj); }

			if(!psInfoData.payStationDetails.creditCardMerchantAccount || psInfoData.payStationDetails.creditCardMerchantAccount == ""){ $("#psCCMerchantAccountLine").hide(); }
			else{ $("#psCCMerchantAccountLine").show(); $("#psCCMerchantAccount").html(psInfoData.payStationDetails.creditCardMerchantAccount); }
			
			if(!psInfoData.payStationDetails.valueCardMerchantAccount || psInfoData.payStationDetails.valueCardMerchantAccount == ""){ $("#psVCMerchantAccountLine").hide(); }
			else{ $("#psVCMerchantAccountLine").show(); $("#psVCMerchantAccount").html(psInfoData.payStationDetails.valueCardMerchantAccount); }
			
			if(psInfoData.payStationDetails.location == ""){ $("#psLocationLine").hide(); }
			else{ $("#psLocationLine").show(); 
				if(locationPerm){ 
					var linkStart = "<a href='"+LOC.locationLink+"&filterValue=loc_"+psInfoData.payStationDetails.locationRandomId+"'>"; 
					var linkEnd = "</a>"; }
				else { var linkStart = ""; var linkEnd = ""; }
				$("#psLocation").html(linkStart+psInfoData.payStationDetails.location+linkEnd); }
			
			if(!psInfoData.payStationDetails.setting || psInfoData.payStationDetails.setting == ""){ $("#psSettingLine").hide(); }
			else{ $("#psSettingLine").show(); $("#psSetting").html(psInfoData.payStationDetails.setting); }
			
			if(psInfoData.payStationDetails.groupAssignment){
				var groupAssignment = psInfoData.payStationDetails.groupAssignment;
				var groupIDs = psInfoData.payStationDetails.groupAssignmentRandomIds;
				if(groupAssignment instanceof Array === false){ groupAssignment = [groupAssignment]; }
				if(groupIDs instanceof Array === false){ groupIDs = [groupIDs]; }
				var groupList = "";
			
				for(var x=0; x < groupAssignment.length; x++){ 
					if(routePerm){ 
						var linkStart = "<a href='"+LOC.routeLink+"&itemValue="+groupIDs[x]+"'>"; 
						var linkEnd = "</a>"; }
					else { var linkStart = ""; var linkEnd = ""; }
					if(groupList == ""){ groupList = linkStart + groupAssignment[x]+ linkEnd; }
					else{ groupList =  groupList+"<br>"+ linkStart + groupAssignment[x]+ linkEnd; } }
				$("#psGroupAssignmentLine").show(); $("#psGroupAssignment").html(groupList); }
			else { $("#psGroupAssignmentLine").hide(); }

			if(psInfoData.payStationDetails.installedModules || psInfoData.payStationDetails.telemetryData){ 
				var	telemetryData = "", telemetryList = "", telemetryLblObj = "", telemetryLstObj = "", installedModules = "", moduleList = "";
				
				if(psInfoData.payStationDetails.telemetryData){
					telemetryData = psInfoData.payStationDetails.telemetryData;
					if(telemetryData instanceof Array === false){ telemetryData = [telemetryData]; }
					telemetryList = $("<section>");
					telemetryLblObj = $("<section>");
					telemetryLblObj.attr("id", "emvTelemetry-Lbl");
					telemetryLblObj.addClass("cmpntHead");
					telemetryLblObj.text(emvTelemetryLbl);
					telemetryLblObj.append('<span class="menuExpand" title="'+ clickViewDetails +'"><img width="18" border="0" height="1" src="'+imgLocalDir+'spacer.gif" alt="'+ clickViewDetails +'" title="'+ clickViewDetails +'"></span>');

					telemetryLstObj = $("<dl>");
					telemetryLstObj.attr("id", "emvTelemetry-Lst");
					telemetryLstObj.addClass("cmpntList");
					
					for(var x=0; x < telemetryData.length; x++){ 
						telemetryLstObj.append("<dt class='detailLabel'>"+ telemetryData[x].key+":</dt><dd class='detailValue'>"+ telemetryData[x].value+"</dd>"); }
						telemetryList.append(telemetryLblObj);
						telemetryList.append(telemetryLstObj);
					 }	

				if(psInfoData.payStationDetails.installedModules){
					installedModules = psInfoData.payStationDetails.installedModules 
					if(installedModules instanceof Array === false){ installedModules = [installedModules]; }
					moduleList = "";
					for(var x=0; x < installedModules.length; x++){ 
						if(moduleList == ""){ moduleList = installedModules[x]; }
						else{ moduleList =  moduleList+"<br>"+ installedModules[x]; }}}
									
				$("#psInstalledModulesLine").show(); 
				$("#psInstalledModules").empty();
				$("#psInstalledModules").append(telemetryList);
				$("#psInstalledModules").append(moduleList); } 
			else { $("#psInstalledModulesLine").hide(); }
			
			if(!psInfoData.payStationDetails.modemType || psInfoData.payStationDetails.modemType == ""){ $("#psModemSettingLine").hide(); }
			else{ 
				$("#psModemSettingLine").show(); $("#psModemSetting").html(psInfoData.payStationDetails.modemType); 
				
				if(psInfoData.payStationDetails.modemSettingsAPN == ""){ $("#psModemAPNLine").hide(); }
				else{ $("#psModemAPNLine").show(); $("#psModemAPN").html(psInfoData.payStationDetails.modemSettingsAPN); }
				
				if((!psInfoData.payStationDetails.modemSettingsCCID || psInfoData.payStationDetails.modemSettingsCCID === "") && (!psInfoData.payStationDetails.modemSettingsMEID || psInfoData.payStationDetails.modemSettingsMEID === "")){ 
					$("#psModemCCIDLine").hide(); $("#psModemCCID").html(""); }
				else if(psInfoData.payStationDetails.modemSettingsMEID && psInfoData.payStationDetails.modemSettingsMEID !== ""){ 
					$("#psModemCCIDLine").show(); 
					$("#psModemIDLabel").html(meidLabel);
					$("#psModemCCID").html(psInfoData.payStationDetails.modemSettingsMEID); }
				else{ 
					$("#psModemCCIDLine").show(); 
					$("#psModemIDLabel").html(ccidLabel);
					$("#psModemCCID").html(psInfoData.payStationDetails.modemSettingsCCID); }
				
				if(psInfoData.payStationDetails.modemSettingsCarrier == ""){ $("#psModemCarrierLine").hide(); }
				else{ $("#psModemCarrierLine").show(); $("#psModemCarrier").html(psInfoData.payStationDetails.modemSettingsCarrier); }
				
				if( isEmpty(psInfoData.payStationDetails.modemSettingsHardwareManufacturer) ){ $("#psModemHardwareManufacturerLine").hide(); }
				else{ $("#psModemHardwareManufacturerLine").show(); $("#psModemHardwareManufacturer").html(psInfoData.payStationDetails.modemSettingsHardwareManufacturer); } 
				
				if( isEmpty(psInfoData.payStationDetails.modemSettingsHardwareModel) ){ $("#psModemHardwareModelLine").hide(); }
				else{ $("#psModemHardwareModelLine").show(); $("#psModemHardwareModel").html(psInfoData.payStationDetails.modemSettingsHardwareModel); } 				

				if( isEmpty(psInfoData.payStationDetails.modemSettingsHardwareFirmware) ){ $("#psModemHardwareFirmwareLine").hide(); }
				else{ $("#psModemHardwareFirmwareLine").show(); $("#psModemHardwareFirmware").html(psInfoData.payStationDetails.modemSettingsHardwareFirmware); } 
				
			}
			/*//if( == ""){*/ $("#psVerrusEnabledLine").hide(); /*}else{ $("#psVerrusEnabledLine").show(); $("#psVerrusEnabled").html( ); }*/			
			
			if(!psInfoData.payStationDetails.status || psInfoData.payStationDetails.status == ""){ $("#psStatusLine").hide(); }
			else{ $("#psStatusLine").show(); $("#psStatus").html(psInfoData.payStationDetails.status+" ("+psInfoData.payStationDetails.activationDate+")"); }
			
			snglSlideFadeTransition("show",$("#infoArea"), showCallBack);
			
		} else if(ajaxPSInfo.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
			snglSlideFadeTransition("show",$("#infoArea"));
		}
	};
	ajaxPSInfo.open("GET",LOC.payStationsInfo+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
	ajaxPSInfo.send();
	$(document.body).scrollTop(0);
}

function showPSReports(psID){
	collectionReportList(psID);
	transactionReportList(psID);
	$("#reportsArea").fadeIn(function(){ $("#reportsArea").find(".tab:first").trigger('click'); });
	$(document.body).scrollTop(0);
}

function serializeForm(formElem, ignoredHash, commaSeparatedHash) {
	var result = [];
	
	if(!ignoredHash) {
		ignoredHash = {};
	}
	if(!commaSeparatedHash) {
		commaSeparatedHash = {};
	}
	
	var $form = $(formElem);
	var inputsList = $form.find(":input");
	
	var $input, inputId, inputIdx = -1, inputLen = inputsList.length;
	var paramName, paramValues;
	while(++inputIdx < inputLen) {
		$input = $(inputsList[inputIdx]);
		inputId = $input.attr("id");
		paramName = $input.attr("name");
		paramValues = $input.val();
		if(paramName && (!$input.is(":disabled"))) {
			if(!inputId) {
				result.push({
					"name": paramName,
					"value": paramValues
				});
			}
			else if(!ignoredHash[inputId]) {
				if(!commaSeparatedHash[inputId]) {
					result.push({
						"name": paramName,
						"value": paramValues
					});
				}
				else {
					paramValues = $input.val();
					if(paramValues.length > 0) {
						var buffer = paramValues.split(",");
						
						var bufferIdx = -1;
						var bufferLen = buffer.length;
						while(++bufferIdx < bufferLen) {
							result.push({
								"name": paramName,
								"value": buffer[bufferIdx]
							});
						}
					}
				}
			}
		}
	}
	
	return result;
}

function mergeSerializedForms() {
	var result = "";
	var forms = arguments;
	if(forms.length > 0) {
		if((forms.length == 1) && (Array.isArray(forms[0]))) {
			forms = forms[0];
		}
		
		var i = 0, len = forms.length, currForm = createFormDataHash(forms[i]), nextForm;
		while(++i < len) {
			nextForm = createFormDataHash(forms[i]);
			for(var key in nextForm) {
				currForm[key] = nextForm[key];
			}
		}
		
		var resultBuffer = [];
		for(var key in currForm) {
			resultBuffer.push(key + "=" + currForm[key]);
		}
		
		result = resultBuffer.join("&");
	}
	
	return result;
}

function createFormDataHash(serializedForm) {
	var result = {}, buffer = serializedForm.split("&");
	var i = buffer.length, wholeStr, sepIdx;
	while(--i >= 0) {
		wholeStr = buffer[i];
		sepIdx = wholeStr.indexOf("=");
		
		result[wholeStr.substring(0, sepIdx)] = wholeStr.substring(sepIdx + 1);
	}
	
	return result;
}

function escapeFileName(input) {
	var result = [];
	var idx = input.length, curr, charCode;
	while(--idx >= 0) {
		curr = input.charAt(idx);
		charCode = input.charCodeAt(idx);
		if(charCode == 92) {
			result[idx] = "%5C";
		}
		else if(((charCode >= 65) && (charCode <= 122)) || ((charCode >= 48) && (charCode <= 57))) {
			result[idx] = curr;
		}
		else {
			switch(curr) {
				case '.': result[idx] = ""; break;
				case '%': result[idx] = ""; break;
				case '/': result[idx] = ""; break;
				case ':': result[idx] = ""; break;
				case '*': result[idx] = ""; break;
				case '?': result[idx] = ""; break;
				case '"': result[idx] = ""; break;
				case '|': result[idx] = ""; break;
				case '<': result[idx] = ""; break;
				case '>': result[idx] = ""; break;
				default: result[idx] = curr; break;
			}
		}
	}
	return result.join('');
}

function escapeHTML(input) {
    return input.split("<").join("&#60;").split(">").join("&#62;").split(/&(?!#[0-9]+;)/).join("&#38;");
}

function parseJSON(input) {
	return JSON.parse(escapeHTML(input));
}

function unescapeJSON(input) {
    return input.split("&lt;").join("<").split("&#60;").join("<").split("&gt;").join(">").split("&#62;").join(">").split("&amp;").join("&").split("&#38;").join("&");
}

function downloadFile(url) {
	var $dlFrame = $("<iframe>")
			.hide()
			.appendTo("body")
			.attr("src", url);
	
	$dlFrame.on('load', function() {
		$dlFrame.remove();
	});
}

function calculateScrollAdjustmentPerPixel($listBox, $selectedItem, $selectedItemSibling, targetHeight) {
	var result = 0;
	
	var totalChange = $listBox.height() - targetHeight;
	var currentScroll = $listBox.scrollTop();
	var itemTop = ($selectedItemSibling.length > 0) ? $selectedItemSibling.offset().top : ($selectedItem.offset().top + $selectedItem.height());
	
	var distance = itemTop - ($listBox.offset().top + totalChange);
	if((distance < 0) || (distance > targetHeight)) {
		result = currentScroll + (distance - targetHeight) / totalChange;
	}
	
	return result;
}

function calculateNewScrollTop($listBox, $selectedItem, newListBoxTop, newListBoxHeight) {
	var curScrollTop = $listBox.scrollTop();
	
	var curListBoxTop = $listBox.offset().top;
	var curListBoxHeight = $listBox.height();
	var curListBoxBottom = curListBoxTop + curListBoxHeight;
	
	var selectedTop = $selectedItem.offset().top;
	var selectedHeight = $selectedItem.height();
	var selectedBottom = selectedTop + selectedHeight;
	
	var curDistance = selectedTop - curListBoxTop;
	var ratio = 0.5;
	if(selectedTop < curListBoxTop) {
		if(selectedBottom > curListBoxTop) {
			ratio = 0;
		}
	}
	else if(selectedBottom > curListBoxBottom) {
		if(selectedTop < curListBoxBottom) {
			ratio = 1;
		}
	}
	else {
		ratio = curDistance / curListBoxHeight;
	}
	
	var newDistance = newListBoxHeight * ratio;
	if((selectedHeight < newListBoxHeight) && (newDistance > (newListBoxHeight - selectedHeight))) {
		newDistance = newListBoxHeight - selectedHeight;
	}
	
	return curScrollTop + (curDistance - newDistance);
}

function calculateNewScrollTopLockAtBottom($listBox, $selectedItem, targetListBoxHeight) {
	var result = $listBox.scrollTop();
	
	var $selectedItemSibling = $selectedItem.next($selectedItem.attr("tagName"));
	var itemTop = ($selectedItemSibling.length > 0) ? $selectedItemSibling.offset().top : ($selectedItem.offset().top + $selectedItem.height());
	
	var distance = itemTop - $listBox.offset().top;
	if((distance < 0) || (distance > targetListBoxHeight)) {
		result += (distance - targetListBoxHeight);
	}
	
	return result;
}
/**
 * 
 * @param options a configuration object that allow the following attributes
 * {
 * 		cancelFn: Function that perform cleanup for the request.
 * 		delays: Array of delay for 1st, 2nd, 3rd monitoring action in milliseconds
 * }
 * @returns
 */
function RequestMonitor(options) {
	this.options = options;
	this.state = 0;
	
	var that = this;
	this.thirdAlert = function() {
		if(that.isActive()) {
			RequestMonitor.updateState(that, (that.state < 3) ? 3:4);
			delete that.timeoutId;
		}
	};
	
	this.secondAlert = function() {
		if(that.isActive()) {
			RequestMonitor.updateState(that, 2);
			that.timeoutId = window.setTimeout(that.thirdAlert, that.options.delays[2]);
		}
	};
	
	this.firstAlert = function() {
		if(that.isActive()) {
			RequestMonitor.updateState(that, 1);
			that.timeoutId = window.setTimeout(that.secondAlert, that.options.delays[1]);
		}
	};
	
	this.timeoutId = window.setTimeout(this.firstAlert, this.options.delays[0]);
}

RequestMonitor.requestCounts = [ 0, 0 ];
RequestMonitor.lastQueue = [];
RequestMonitor.removedFromLastQueue = 0;

RequestMonitor.updateState = function(monitorObj, newState) {
	var currState = monitorObj.state;
	if(typeof currState === "undefined") {
		currState = 0;
	}
	
	if(currState === newState) {
		// DO NOTHIN!
	}
	else {
		if(newState < 0) {
			if(currState === 1) {
				--RequestMonitor.requestCounts[0];
			}
			else if(currState === 2) {
				--RequestMonitor.requestCounts[1];
			}
			else if(currState >= 3) {
				++RequestMonitor.removedFromLastQueue;
				if((RequestMonitor.lastQueue.length - RequestMonitor.removedFromLastQueue) <= 0) {
					RequestMonitor.lastQueue = [];
				}
			}
		}
		else {
			if(currState === 1) {
				--RequestMonitor.requestCounts[0];
			}
			else if(currState === 2) {
				--RequestMonitor.requestCounts[1];
			}
			
			if(newState === 1) {
				++RequestMonitor.requestCounts[0];
			}
			else if(newState === 2) {
				++RequestMonitor.requestCounts[1];
			}
			else {
				RequestMonitor.lastQueue.push(monitorObj);
			}
		}
		
		monitorObj.state = newState;
	}
	
	if(RequestMonitor.lastQueue.length > 0) {
		if(!$("#messageResponseAlertBox").is(":visible")) {
			confirmDialog({
				title: delayedTitle,
				message: delayedDialogMsg,
				okLabel: waitBtn,
				okCallback: function() { resetSessionActvty(); monitorObj.timeoutId = window.setTimeout(monitorObj.thirdAlert, monitorObj.options.delays[2]); },
				cancelLabel: dontWaitBtn,
				cancelCallback: function() { RequestMonitor.resetLastQueue(); }
			});
		}
	}
	else {
		if(RequestMonitor.requestCounts[1] > 0) {
			$("#ajaxLoad").find("#loadMessage").html(delayedLoadMsg).css("color", "#555F66");
		}
		else if(RequestMonitor.requestCounts[0] > 0) {
			loadOverlay();
		}
		else {
			if($("#ajaxLoad").is(":visible")) {
				$("#ajaxLoad").dialog("close").dialog("destroy");
			}
		}
	}
};

RequestMonitor.resetLastQueue = function() {
	var idx = RequestMonitor.lastQueue.length;
	while(--idx >= 0) {
		RequestMonitor.lastQueue[idx].reset();
	}
};

RequestMonitor.prototype.isActive = function() {
	return this.state >= 0;
};

RequestMonitor.prototype.triggerFirstState = function() {
	if(this.state < 1) {
		if(this.timeoutId) {
			window.clearTimeout(this.timeoutId);
			delete this.timeoutId;
		}
		
		this.firstAlert();
	}
};

RequestMonitor.prototype.reset = function() {
	if(this.timeoutId) {
		window.clearTimeout(this.timeoutId);
	}
	else if(this.intervalId) {
		window.clearInterval(this.intervalId);
	}
	
	if(this.isActive()) {
		RequestMonitor.updateState(this, -1);
		if(typeof this.options.cancelFn === "function") {
			this.options.cancelFn();
		}
	}
};

//------------------------------------------------------------------------------------

function indexOfObject(theArray, prprty, prprtyVal){
	for(var x = 0; x < theArray.length; x++) {
		if (theArray[x][prprty] === prprtyVal) return x; }
	return -1; }

//------------------------------------------------------------------------------------

function submitForm($form, newAction, newTarget) {
	$form = $($form);
	
	var originalTarget = $form.attr("target");
	$form.attr("target", newTarget);
	
	var originalAction = $form.attr("action");
	$form.attr("action", newAction);
	
	try {
		$form.get(0).trigger('submit')
	} finally {
		$form.attr("target", originalTarget);
		$form.attr("action", originalAction);
	}
}

function basicHtmlEncode(string){ 
    return String(string).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;'); }

//------------------------------------------------------------------------------------

// Shows a Create Another option check box by dialog button to allow users to continue to to use dialog again instead of starting over/
function showCreateAnother($dialogBox){	$dialogBox.next(".ui-dialog-buttonpane").append(createAnthrChkBx); }

//------------------------------------------------------------------------------------
// JavaScript Document
