var __FMT_TIME = "HH:mm";
var __FMT_FORM_DATE = "MM/DD/YYYY";
var __FMT_FORM_DATETIME = __FMT_FORM_DATE + " " + __FMT_TIME;
var __FLAG_ISO_WEEK_DAY = [ "", "useMonday", "useTuesday", "useWednesday", "useThursday", "useFriday", "useSaturday", "useSunday" ];
var __HASH_WEEK_DAY_ATTR = {"useMonday": true, "useTuesday": true, "useWednesday": true, "useThursday": true, "useFriday": true, "useSaturday": true, "useSunday": true};

function getRateRateProfileFromCalendar($calendar, rateRateProfileId) {
	var events = $calendar.fullCalendar("clientEvents", function(affectedEvent) {
		return rateRateProfileId && affectedEvent.rateRateProfileObj && (affectedEvent.rateRateProfileObj.rateRateProfileRandomId === rateRateProfileId);
	});
	
	return (events && (events.length > 0)) ? events[0].rateRateProfileObj : null;
}

function removeAllEvents($calendar) {
	var localEventSources = $calendar.data("localEventSources");
	if(localEventSources) {
		for(var randomId in localEventSources) {
			removeEvents($calendar, randomId);
			delete localEventSources[randomId];
		}
	}
	
	$calendar.data("localEventSources", localEventSources);
}

function removeEvents($calendar, rateRateProfileId) {
	$calendar.fullCalendar("removeEvents", function(affectedEvent) {
		return rateRateProfileId && affectedEvent.rateRateProfileObj && (affectedEvent.rateRateProfileObj.rateRateProfileRandomId === rateRateProfileId);
	});
	
	var localEventSources = $calendar.data("localEventSources");
	if(localEventSources && localEventSources[rateRateProfileId]) {
		$calendar.fullCalendar("removeEventSource", localEventSources[rateRateProfileId]);
	}
}

function addEvents($calendar, eventsArray) {
	if(eventsArray) {
		if(!Array.isArray(eventsArray)) {
			eventsArray = [ eventsArray ];
		}
		
		if(eventsArray.length > 0) {
			var rateRateProfile = eventsArray[0].rateRateProfileObj;
			
			var localEventSources = $calendar.data("localEventSources");
			if(!localEventSources) {
				localEventSources = {};
			}
			
			localEventSources[rateRateProfile.rateRateProfileRandomId] = {
				"events": eventsArray
			};
			
			$calendar.fullCalendar("addEventSource", localEventSources[rateRateProfile.rateRateProfileRandomId]);
			$calendar.data("localEventSources", localEventSources)
		}
	}
}

function replaceEvents($calendar, rateRateProfile, decorateFn) {
	removeEvents($calendar, rateRateProfile.rateRateProfileRandomId);
	
	var currentView = $calendar.fullCalendar("getView");
	addEvents($calendar, generateEvents(currentView.start, currentView.end, __FMT_FORM_DATETIME, rateRateProfile, decorateFn));
}

function toRateRateProfile(rateRateProfileObj, form, namePrefix) {
	var namePrefixLen = namePrefix.length;
	
	var elems = $(form).find("[name^='" + namePrefix + "']");
	var idx = elems.length;
	
	var $elem, elemName, elemVal;
	while(--idx >= 0) {
		$elem = $(elems[idx]);
		elemName = $elem.attr("name").substring(namePrefixLen);
		elemName = elemName.charAt(0).toLowerCase() + elemName.substring(1);
		
		elemVal = $elem.val();
		if(elemVal.toLowerCase() === "true") {
			elemVal = true;
		}
		else if(elemVal.toLowerCase() === "false") {
			elemVal = false;
		}
		
		rateRateProfileObj[elemName] = elemVal;
	}
	
	rateRateProfileObj.rateName = $("#formRateName").autoselector("getSelectedLabel");
	
//	convertFormFmtToListFmt(rateRateProfileObj, "availabilityStartDate");
//	convertFormFmtToListFmt(rateRateProfileObj, "availabilityEndDate");
	
	return rateRateProfileObj;
}

function toScheduleForm(form, namePrefix, rateRateProfileObj) {
	var namePrefixLen = namePrefix.length;
	
	var elems = $(form).find("[name^='" + namePrefix + "']");
	var idx = elems.length;
	
	var $elem, elemName, elemVal;
	while(--idx >= 0) {
		$elem = $(elems[idx]);
		elemName = $elem.attr("name").substring(namePrefixLen);
		elemName = elemName.charAt(0).toLowerCase() + elemName.substring(1);
		
		elemVal = rateRateProfileObj[elemName];
		if(elemVal) {
			$elem.val(elemVal);
		}
		else if(typeof elemVal === "boolean") {
			$elem.val(false);
		}
		else {
			$elem.val("");
		}
	}
	
	return rateRateProfileObj;
}

function generateEvents(scheduleStart, scheduleEnd, dateTimeFormat, schedules, decorateFn) {
	var result = [];
	
	var eventStart, eventEnd, isAllDay;
	var rateSchedule, startTimeMins, endTimeMins;
	var dateInc, dateIncIdx, dateIncLen;
	
	var maxMoment = scheduleEnd.clone();
	
	var schedulesArr = (!schedules) ? [] : ((Array.isArray(schedules)) ? schedules : [ schedules ]);
	var idx = -1, len = schedulesArr.length;
	
	var eventObj, shouldDecorate = (typeof decorateFn === "function");
	while(++idx < len) {
		rateSchedule = schedulesArr[idx];
		if(!rateSchedule.availabilityStartTime) {
			rateSchedule.availabilityStartTime = "00:00";
		}
		if(!rateSchedule.availabilityEndTime) {
			rateSchedule.availabilityEndTime = "00:00";
		}
		
		dateInc = createDateIncremental(rateSchedule, scheduleStart.isoWeekday());
		dateIncIdx = 0, dateIncLen = dateInc.length;
		
		startTimeMins = inMinutes(rateSchedule.availabilityStartTime);
		endTimeMins = inMinutes(rateSchedule.availabilityEndTime);
		
		isAllDay = false;
		if(endTimeMins === 0) {
			isAllDay = (startTimeMins == 0);
			endTimeMins = 1440;
		}
		
		if(rateSchedule.useScheduled) {
			eventStart = moment(rateSchedule.availabilityStartDate + " " + rateSchedule.availabilityStartTime, dateTimeFormat);
			eventEnd = moment(rateSchedule.availabilityEndDate + " " + rateSchedule.availabilityEndTime, dateTimeFormat);
			if(!eventStart.isValid()) {
				console.log("RateRateProfile '" + rateSchedule.rateRateProfileRandomId + "' has invalid start datetime: " + rateSchedule.availabilityStartDate + " " + rateSchedule.availabilityStartTime);
			}
			else if(!eventEnd.isValid()) {
				console.log("RateRateProfile '" + rateSchedule.rateRateProfileRandomId + "' has invalid end datetime: " + rateSchedule.availabilityEndDate + " " + rateSchedule.availabilityEndTime);
			}
			else {
				eventObj = {
					id: rateSchedule.rateRateProfileRandomId,
					title: rateSchedule.rateName,
					start: eventStart.clone(),
					end: eventEnd.clone(),
					allDay: isAllDay,
					rateRateProfileObj: rateSchedule
				};
				
				if(shouldDecorate) {
					eventObj = decorateFn(eventObj);
				}
				
				result.push(eventObj);
			}
		}
		else if(dateInc.length > 0) {
			eventStart = scheduleStart.clone();
			eventStart.millisecond(0);
			eventStart.second(0);
			eventStart.minute(startTimeMins);
			eventStart.add(dateInc[0], "d");
			
			eventEnd = eventStart.clone();
			eventEnd.add((endTimeMins - startTimeMins), "m");
			dateIncIdx = 1;
			while(eventStart.isBefore(maxMoment)) {
				eventObj = {
					id: rateSchedule.rateRateProfileRandomId + "_" + eventStart.isoWeekday(),
					title: rateSchedule.rateName,
					start: eventStart.clone(),
					end: eventEnd.clone(),
					allDay: isAllDay,
					rateRateProfileObj: rateSchedule
				};
				
				if(shouldDecorate) {
					eventObj = decorateFn(eventObj);
				}
				
				result.push(eventObj);
				
				eventStart.add(dateInc[dateIncIdx], "d");
				eventEnd.add(dateInc[dateIncIdx], "d");
				++dateIncIdx;
				if(dateIncIdx >= dateIncLen) {
					dateIncIdx = 1;
				}
			}
		}
	}
	
	return result;
}

function inMinutes(timeStr) {
	var timeArr = timeStr.split(":");
	return parseInt(timeArr[1], 10) + (parseInt(timeArr[0], 10) * 60);
}

function createDateIncremental(rateSchedule, initialIsoWeekday) {
	var dow = [];
	var dowIdx = 0, dowLen = __FLAG_ISO_WEEK_DAY.length;
	var markedIdx = -1;
	while(++dowIdx < dowLen) {
		if(rateSchedule[__FLAG_ISO_WEEK_DAY[dowIdx]]) {
			if((markedIdx == -1) && (initialIsoWeekday <= dowIdx)) {
				markedIdx = dow.length;
			}
			
			dow.push(dowIdx);
		}
	}
	
	dowIdx = markedIdx, dowLen = dow.length;
	
	var result = [];
	if(dowLen > 0) {
		if(markedIdx >= 0) {
			result.push(dow[dowIdx] - initialIsoWeekday); // First day of calendar until first day of schedule.
		}
		else {
			dowIdx = 0;
			result.push(7 + dow[dowIdx] - initialIsoWeekday); // First day of calendar until first day of schedule.
		}
		
		if(dowLen == 1) {
			result.push(7);
		}
		else {
			--dowLen;
			while(dowIdx < dowLen) {
				result.push(dow[dowIdx + 1] - dow[dowIdx]);
				++dowIdx;
			}
			
			result.push(7 + dow[0] - dow[dowIdx]);
			
			if(markedIdx > 0) {
				dowIdx = 0;
				while(dowIdx < markedIdx) {
					result.push(dow[dowIdx + 1] - dow[dowIdx]);
					++dowIdx;
				}
			}
		}
	}
	
	return result;
}