/*global $, GetHttpObject, alertDialog, systemErrorMsg, noteDialog, LABEL, LOC, testingMsg, successMsg, failMsg, characterTypes, testFormEdit, slideFadeTransition, cancelConfirm, msgResolver, savedCaseCustomerMsg */ 

var animateDuration = 800;

function loadFlexCredentials(){
	var ajaxloadFlex = new GetHttpObject();
	ajaxloadFlex.onreadystatechange = function()
	
	{
		if (ajaxloadFlex.readyState === 4 && ajaxloadFlex.status === 200){
			$("#flexIntegration").html(ajaxloadFlex.responseText);
		} else if(ajaxloadFlex.readyState === 4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxloadFlex.open("GET","/systemAdmin/workspace/licenses/flexCredentials.html"+document.location.search.substring(),true);
	ajaxloadFlex.send(); }


//--------------------------------------------------------------------------------------

function saveFlexCredentials(event) {
	event.preventDefault();
	
	var	$form = $("#flexCredentialsForm"),
		req = new GetHttpObject({
		"postTokenSelector": "#flexCredentialPostToken",
		"triggerSelector": (event && event !== null) ? event.target : null }),
		callBackAction = function(){ return location.reload(); };
	
	req.onreadystatechange = function(responseFalse, displayedError) {
		if(req.readyState === 4) {
			if(responseFalse === false) {
				noteDialog(LABEL.saveSuccess);
				setTimeout(callBackAction, 1500);
			} else if(!displayedError) {
				alertDialog(systemErrorMsg); 
			}
		}
	};
	
	req.open("POST", LOC.saveFlexCredentials, true);
	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.send($form.serialize() + "&" + document.location.search.substring(1));
}

//--------------------------------------------------------------------------------------

function updateFlexCredentialsFlag(success, testType) {
	if(success === true) {
		if(testType === "testResult"){ $("#flexTestButtonResult").html(successMsg); } else { $("#testNewFlexCredentialsResult").html(successMsg); }
		$("#flexTestButtons").removeClass("disabled"); }
	else { 
		if(testType === "testResult"){ $("#flexTestButtonResult").html(failMsg); } else { $("#testNewFlexCredentialsResult").html(failMsg); }
		$("#flexTestButtons").removeClass("disabled"); }
}

//--------------------------------------------------------------------------------------

function testNewFlexCredentials(event) {
	event.preventDefault();
	$("#testNewFlexCredentialsResult").html(testingMsg);
	var	$form = $("#flexCredentialsForm"),
		req = new GetHttpObject({
		"postTokenSelector": "#flexCredentialPostToken",
		"triggerSelector": (event && event !== null) ? event.target : null });
	
	req.onreadystatechange = function(responseFalse, displayedError) {
		if(req.readyState === 4) {
			if(responseFalse === false) {
				updateFlexCredentialsFlag(true, "newTestResult");
			} else {
				updateFlexCredentialsFlag(false, "newTestResult");
				if(!displayedError) { alertDialog(systemErrorMsg); }
			}
		}
	};
	
	req.open("POST", LOC.testNewFlexCredentials, true);
	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.send($form.serialize() + "&" + document.location.search.substring(1));
}

//--------------------------------------------------------------------------------------

function testFlexCredentials(event, booleanMode) {
	if(event){ event.preventDefault(); }
	var	proceed = (event && $(event.target).hasClass("disabled"))? false : true,
		req;
	if (proceed) {
		$("#flexTestButtonResult").html(testingMsg);
		req = new GetHttpObject({ "triggerSelector": (event && event !== null)? event.target : null });
		req.onreadystatechange = function(responseFalse, displayedError) {
			if(req.readyState === 4) {
				if(responseFalse === false) {
					updateFlexCredentialsFlag(true, "testResult");
				} else {
					updateFlexCredentialsFlag(false, "testResult");
					if((!displayedError) && (!booleanMode)) { alertDialog(systemErrorMsg); }
				}
			}
		};
		req.open("GET", LOC.testFlexCredentials + "?" + document.location.search.substring(1) + (booleanMode ? "&booleanMode=1" : ""), true);
		req.send();
	}
}

//--------------------------------------------------------------------------------------

function loadAutoCountCredentials(){
	var ajaxLoadCase = new GetHttpObject();
	ajaxLoadCase.onreadystatechange = function()
	
	{
		if (ajaxLoadCase.readyState === 4 && ajaxLoadCase.status === 200){
			$("#autoCountIntegration").html(ajaxLoadCase.responseText);
		} else if(ajaxLoadCase.readyState === 4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxLoadCase.open("GET","/systemAdmin/workspace/licenses/viewCaseAccount.html"+document.location.search.substring(),true);
	ajaxLoadCase.send(); }

//--------------------------------------------------------------------------------------

function autoCountCustAutoComplete(request, response) {
	var	typeTest = characterTypes.stndrdChrctr,
		ajaxCaseCustomer;	
	if(typeTest.test(request.term.replace(/[\r\n]/g, "")) !== true){
		ajaxCaseCustomer = new GetHttpObject();
		ajaxCaseCustomer.onreadystatechange = function() {
			if(ajaxCaseCustomer.readyState === 4 && ajaxCaseCustomer.status === 200) {
				var responseOptions = [], data = JSON.parse(ajaxCaseCustomer.responseText);
				data = data.searchCustomerResult.customerSearchResult;
				if(data !== ""){
					if(data instanceof Array){ 
						responseOptions = $.map(data, function(item) { return{ label: item.name, value: item.randomId }; }); } 
					else if(data){
						responseOptions = [{ label: data.name, value: data.randomId }]; }
					response(responseOptions); }
			}
		};
		ajaxCaseCustomer.open("GET", "/systemAdmin/workspace/licenses/searchCaseAccounts.html?search="+request.term+"&"+document.location.search.substring(1), true);
		ajaxCaseCustomer.send();
	}
}

//--------------------------------------------------------------------------------------

function saveAutoCountCustomer(event) {
	var	ajaxSaveCaseCustomer = new GetHttpObject({ "triggerSelector": (event && event !== null) ? event.target : null }),
		selectedID = $("#autoCountRandomId").val();

	if($("#autoCountRandomId").val() !== ""){			
		ajaxSaveCaseCustomer.onreadystatechange = function()
		{
			if (ajaxSaveCaseCustomer.readyState === 4 && ajaxSaveCaseCustomer.status === 200){
				if(ajaxSaveCaseCustomer.responseText){  
					noteDialog(savedCaseCustomerMsg, "timeOut", 1000);
					$("#mainContent").removeClass("edit");
					$("#autoCountDetails").find("#autoCountCustName").html($("#autoCountSearch").val());
					setTimeout(function(){ slideFadeTransition($("#autoCountFormArea"), $("#autoCountDetails")); $("#btnEditAutoCountCredentials").fadeIn("fast"); }, 800); }
			} else if(ajaxSaveCaseCustomer.readyState === 4){
				alertDialog(systemErrorMsg); //Unable to load details
			}
		};
		ajaxSaveCaseCustomer.open("GET","/systemAdmin/workspace/licenses/saveCaseAccounts.html?randomId="+ selectedID +"&"+document.location.search.substring(1),true);
		ajaxSaveCaseCustomer.send(); }
	else{ alertDialog("Please Select an Auto<strong>Count</strong> Customer."); }}

//--------------------------------------------------------------------------------------

function autoCountCredentials(){
	loadAutoCountCredentials();
	var hideCallBack = '',
		showCallBack = '',
		furtherAction = '';
	$(document.body)
		.on("click", "#btnEditAutoCountCredentials", function(event){ event.preventDefault();
			hideCallBack = function(){ return testFormEdit($("#autoCountForm")); };
			showCallBack = function(){ return showClearBtnTest("autoCountSearch"); }
			slideFadeTransition($("#autoCountDetails"), $("#autoCountFormArea"), hideCallBack, showCallBack, animateDuration, animateDuration);
			$(this).fadeOut("fast"); })
		.on("click", "#autoCountForm .cancel", function(event){ event.preventDefault();
			furtherAction = function(){ return slideFadeTransition($("#autoCountFormArea"), $("#autoCountDetails"), '', '',animateDuration,animateDuration); };
			cancelConfirm($("#autoCountForm"), '', furtherAction);
			$("#btnEditAutoCountCredentials").fadeIn("fast"); })
		.on("click", "#autoCountForm .save", function(event){ $("#autoCountForm").trigger('submit'); })
		.on("submit", "#autoCountForm", function(event){ event.preventDefault(); saveAutoCountCustomer(event); }); }

//--------------------------------------------------------------------------------------

function flexCredentials() {	
	if($(".sysAdminWorkSpace").length){ loadFlexCredentials(); }
	/* Map Messages */
	msgResolver.mapAllAttributes({
		"url": "#formURL",
		"username": "#formUsername",
		"password": "#formPassword"
	});
	var hideCallBack = '',
		furtherAction = '';
	$(document.body)
		.on("click", "#btnEditFlexCredentialsSysAdmin", function(event){ event.preventDefault();
			hideCallBack = function(){ return testFormEdit($("#flexCredentialsForm")); };
			slideFadeTransition($("#flexCredentialsViewPanel"), $("#flexCredentialsFormPanel"), hideCallBack, '',animateDuration,animateDuration);
			$(this).fadeOut("fast");
		})
		.on("click", "#flexCredentialsFormPanel .cancel", function(event){ event.preventDefault();
			furtherAction = function(){ return slideFadeTransition($("#flexCredentialsFormPanel"), $("#flexCredentialsViewPanel"), '', '',animateDuration,animateDuration); };
			cancelConfirm($("#flexCredentialsForm"), '', furtherAction);
			$("#btnEditFlexCredentialsSysAdmin").fadeIn("fast"); })
		.on("click", "#flexCredentialsFormPanel .save", function(event){ saveFlexCredentials(event, "sysAdminForm"); })
		.on("click", "#testNewFlexCredentials", testNewFlexCredentials)
		.on("click", "#flexTestButton", testFlexCredentials);
	
	if (!$("#flexTestButton").hasClass("disabled")) {
		testFlexCredentials(null, true);
	}
}

//--------------------------------------------------------------------------------------

function integrations(){
	flexCredentials();
	autoCountCredentials(); }

//--------------------------------------------------------------------------------------
