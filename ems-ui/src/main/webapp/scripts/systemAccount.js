/* Commons */

var	formErrors = [], $, testFormEdit, loadUser, msgResolver, pausePropagation, pausePropagationLoop, verifyDeleteUser, inEditAlert, resetUserForm, GetHttpObject, noteDialog, userSavedMsg, scrubQueryString, alertDialog, failedSaveMsg, LOC, cancelConfirm, userObj, filterQuerry, clearChildCompanies, characterTypes, testPasswrd, x, y, formErrorDialog, userStatusEnabledMsg, userStatusDisabledMsg, slctdOpacityLevel, childCompanies, systemErrorMsg, syncMenuItems, systemErrorMsg, userDeleteMsg, deleteBtn, cancelBtn, imgLocalDir, alertTitle, deleteUserMsg, genralAlertDialogOptions, btnStatus, loadRole, verifyDeleteRole, resetRoleForm, roleSavedMsg, roleStatusEnabledMsg, roleStatusDisabledMsg, noPermissions, noUsersAsigned, noUsersAsignedMsg, childPermissions, childFormItem, alertNoticeOptions, editFail, roleDeleteMsg, deleteRoleMsg, defaultCustomers = [];

defaultCustomers.push({ "label": "No Default", "value": -1 });


function formErrorObjects(id, message) {
	this.id = id;
	this.message = message;
}

/* Account Settings */

//--------------------------------------------------------------------------------------

function hideUser($this){
	snglSlideFadeTransition("hide", $("#userDetails")); 
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#userList").find("li.selected").length){ $("#userList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function usersSelectionCheck(itemID, eventAction)
{
	itemID = itemID.split("_");
	var tempID = itemID[1],
		$this = $("#user_"+tempID);
	if(eventAction == "hide"){ 
		crntPageAction = "";
		history.pushState(null, null, location.pathname);
		hideUser($this); }
	else { 
		if($("#contentID").val() != tempID || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: tempID, action: eventAction };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			loadUser(stateObj.itemObject, stateObj.action); }}
}

//--------------------------------------------------------------------------------------

function newUserForm(){ $(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); }); resetUserForm(); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popUserAction(tempItemID, tempEvent, stateObj){ 
	if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#user_"+tempItemID) : ""; hideUser($this); }
	else if(tempEvent === "Add"){ newUserForm(); }
	else { loadUser(tempItemID, tempEvent); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function users(userID, pageEvent)
{
	$("#defaultCust").autoselector({
		isComboBox: true, // To make it force selection to an item in the list
		defaultValue: -1,
		shouldCategorize: true, // Categorize the list using the “category” attribute in your data
		data: defaultCustomers // List items
	});

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popUserAction(tempItemID, tempEvent, stateObj); }

if(!popStateEvent && (userID !== undefined && userID !== '')){ popUserAction(userID, pageEvent); }
///////////////////////////////////////////////////	

//USERS FORM
	msgResolver.mapAttribute("firstName", "#formUserFirstName");
	msgResolver.mapAttribute("lastName", "#formUserLastName");
	msgResolver.mapAttribute("userName", "#formUserName");
	msgResolver.mapAttribute("userPassword", "#newPassword");
	msgResolver.mapAttribute("passwordConfirm", "#c_newPassword");
	msgResolver.mapAttribute("RoleList", "#userSelectedRoles");

	$(document.body).on("click", "#userList > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display",
			itemID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ usersSelectionCheck(itemID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false; }}, 100); }
		else{ usersSelectionCheck(itemID, actionEvent); } });

	$("#userList").on("click", ".view", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		if($(this).parent().hasClass("innerBorder")){
			usersSelectionCheck(itemID, "display");} });

	$("#userList").on("click", ".edit", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		usersSelectionCheck(itemID, "edit"); });
	
	$("#userList").on("click", ".delete", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id").split("_");
		verifyDeleteUser(itemID[1]); });
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var userID = $("#contentID").val();
		verifyDeleteUser(userID);
	});
	
	$("#btnAddUser").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			var stateObj = { itemObject: "New", action: "Add" };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); });
			resetUserForm(); }
	});
	
	$("#resetPasswordBtn").on('click', function(event){ event.preventDefault();
		$("#resetPasswordBtnArea").fadeOut(function(){
			$("#newPassword, #c_newPassword").val("");
			$("#c_newPassword").attr("disabled", true);
			$("#resetPassword").fadeIn("slow");
		});
		
	});
	
	$("#cancelResetBtn").on('click', function(event){ event.preventDefault();
		$("#resetPassword").fadeOut(function(){
			$("#newPassword, #c_newPassword").val("");
			$("#c_newPassword").attr("disabled", true);
			$("#resetPasswordBtnArea").fadeIn("slow");
		});
	});
	
	$("#userSettings").on('submit', function(event){ event.preventDefault(); });
	
	$("#userSettings").on("click", ".save", function(event){ event.preventDefault();
		var userID = $("#formUserID").val(),
			params = $("#userSettings").serialize(),
			ajaxSaveUser = new GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxSaveUser.onreadystatechange = function() {
			if (ajaxSaveUser.readyState==4 && ajaxSaveUser.status == 200) {
				var response = ajaxSaveUser.responseText.split(":"), cleanQuerryString, newLocation;
				if(response[0] == "true") {
					noteDialog(userSavedMsg);
					cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "pgActn"]);
					cleanQuerryString  = (userID != "")? cleanQuerryString+"&filterValue="+userID : cleanQuerryString;
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					newLocation = location.pathname+"?"+cleanQuerryString+"&pgActn="+crntPageAction;
					setTimeout(function(){ window.location = newLocation; }, 500); } }// new location Saved.
			else if(ajaxSaveUser.readyState==4){ alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveUser.open("POST",LOC.saveUser,true);
		ajaxSaveUser.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveUser.setRequestHeader("Content-length", params.length);
		ajaxSaveUser.setRequestHeader("Connection", "close");
		ajaxSaveUser.send(params); });
	
	$("#userSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
	
	//Alert Type AUTO COMPLETE STANDARD
	$("#userAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"blockOnEdit": true,
		"data": userObj })
	.on("itemSelected", function() {
		var slctValue = $("#userAC").autoselector("getSelectedValue");
		if(slctValue != ""){  usersSelectionCheck("_"+slctValue, "display"); }// Location Detail Page Outcome.
		if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue; }
		$(this).trigger('blur');
		return false; });
		
	$("#userRoles").on("click", "li", function(){
		if($(this).hasClass("childRole")){
			var	selectedList = ($(this).parent("ul").hasClass("selectedList"))? true : false,
				selectedChildRole = $("#userSelectedRoles").find("li.childRole.selected").length;
			
			
			if(!$("#childCompanies").is(":visible")){ $("#childCompanies").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); }
			else if($("#childCompanies").is(":visible") && (selectedList && selectedChildRole === 1)){ clearChildCompanies("hide"); }} });
	
	$("#selectAllRoles").on('click', function(){
		if(!$(this).children("a").hasClass("checked") && $("#userRoles > ul.fullList > li.childRole").length){  if(!$("#childCompanies").is(":visible")){ 
			$("#childCompanies").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); } }
		else{ clearChildCompanies("hide"); } });
		
	$("#userRoles").on("click", ".clearSelected", function(){ clearChildCompanies("hide"); });
	
	$(document.body).on("click", "#companySelectionLabel", function(){
		var $that = $(this).children("#companySelectionsAll");
		$("ul#companyAllRoles > li").each(function() {
			var	custName = $(this).attr("custName"),
			custID = $(this).attr("id").split("-")[1],
			indexPoint = indexOfObject(defaultCustomers, "value", custID);
			if($that.hasClass("checked")){
				if(indexPoint < 0){ defaultCustomers.push({ "label": custName, "value": custID }); }}
			else{ 
				var custListLength = defaultCustomers.length;
				for(x=0; x < custListLength; x++){ if(x > 0){ defaultCustomers.splice(1, 1); } }}
		$("#defaultCust").autoselector("updateData"); }); });
	$(document.body).on("click", "ul#companyAllRoles li", function(){
		var	custName = $(this).attr("custName"),
			custID = $(this).attr("id").split("-")[1],
			indexPoint = indexOfObject(defaultCustomers, "value", custID);
		if(indexPoint < 0){ defaultCustomers.push({ "label": custName, "value": custID }); }
		$("#defaultCust").autoselector("updateData"); });
	$(document.body).on("click", "ul#companySelectedRoles li", function(){
		var custName = $(this).attr("custName"),
			custID = $(this).attr("id").split("-")[1],
			indexPoint = indexOfObject(defaultCustomers, "value", custID);
		if(indexPoint > 0){ defaultCustomers.splice(indexPoint, 1); }
		$("#defaultCust").autoselector("updateData");
		if($("#defaultCust").autoselector("getSelectedValue") === custID){ $("#defaultCust").autoselector("reset"); } });
}

//--------------------------------------------------------------------------------------

function userSettingsValidate()
{
	var returnStatus = true,
		alertMsg = '',
		alertCode = [],
		testStr;
//User First Name
	if($(formErrors.user101.id).val() == ''){returnStatus = false; alertCode.push("user101");} 
	else if(characterTypes.spclCharacter.test($(formErrors.user102.id).val()) == true){returnStatus = false; alertCode.push("user102");}
	else if($(formErrors.user103.id).val().length > 25){returnStatus = false; alertCode.push("user103");}
//User Last Name
	if($(formErrors.user201.id).val() == ''){returnStatus = false; alertCode.push("user201");} 
	else if(characterTypes.spclCharacter.test($(formErrors.user202.id).val()) == true){returnStatus = false; alertCode.push("user202");}
	else if($(formErrors.user203.id).val().length > 25){returnStatus = false; alertCode.push("user203");}
//Username
	if($(formErrors.user301.id).val() == ''){returnStatus = false; alertCode.push("user301");} 
	else if($(formErrors.user302.id).val().length > 255 || $(formErrors.user302.id).val().length < 8){returnStatus = false; alertCode.push("user302");}
//User Password
	if($(formErrors.user401.id).val() != ''){
		if(!testPasswrd($(formErrors.user401.id).val())){returnStatus = false; alertCode.push("user401");}
		else if($(formErrors.user402.id).val().length > 15 || $(formErrors.user402.id).val().length < 7){returnStatus = false; alertCode.push("user402");}
//ConfirmPassword
		else if($(formErrors.user401.id).val() != $(formErrors.user501.id).val()){returnStatus = false; alertCode.push("user501");}
	}
	
	for(x = 0; x < alertCode.length; x++)
	{	testStr = /(Children)/g;
		if(testStr.test(formErrors[alertCode[x]].id)){ $(formErrors[alertCode[x]].id).children("ul").addClass("error"); }
		else { $(formErrors[alertCode[x]].id).addClass("error"); }
		alertMsg += "<li>" + formErrors[alertCode[x]].message + "</li>";
	}
	
	if(alertCode.length > 0){ formErrorDialog(alertMsg); }
	
	return returnStatus;
}

//--------------------------------------------------------------------------------------

function showUserDetails(userDetails, action)
{	
	//Process JSON
	var userData =	JSON.parse(userDetails.responseText),
		roleFullList = "",
		roleSelectedList = "",
		selectedCount = 0,
		allSelected = true,
		colPlacement = 0,
		roleItems = userData.userSettingDetails.roleList,
		roleStatus, roleName, roleID, optionSlctdStyle, roleType, childClass, childCompFullList, childCompSelectedList, childSelectedCount, childAllSelected, childColPlacement, childCompItems;
//console.log(alertDetails.responseText);
	$("#actionFlag").val("1");
	$("#contentID, #formUserID").val(userData.userSettingDetails.randomId);
	$("#detailUserDisplayName, #userDispName").html(userData.userSettingDetails.firstName +" "+ userData.userSettingDetails.lastName).attr("title", userData.userSettingDetails.firstName +" "+ userData.userSettingDetails.lastName);

	$("#formUserFirstName").val(userData.userSettingDetails.firstName);
	$("#userFirstNameLabel").hide();

	$("#formUserLastName").val(userData.userSettingDetails.lastName);
	$("#userLastNameLabel").hide();

	if(userData.userSettingDetails.status == true){
		$("#detailUserStatus").html(userStatusEnabledMsg);
		$("#statusCheck").addClass("checked"); $("#statusCheckHidden").val("true");
	} else{ 
		$("#detailUserStatus").html(userStatusDisabledMsg);
		$("#statusCheck").removeClass("checked"); $("#statusCheckHidden").val("false");
	}	
	$("#detailUserName").html(userData.userSettingDetails.userName).attr("title", userData.userSettingDetails.userName);
	$("#formUserName").val(userData.userSettingDetails.userName);
	
	$("#resetPasswordBtnArea").show();
	$("#newPassword, #c_newPassword").val("");
	$("#c_newPassword").attr("disabled", true);
	$("#resetPassword").hide();
	
	//ROLES
	$("#roleChildList").html("");				
	$("#roleSelections").val("");
		
	if(!Array.isArray(roleItems)){ roleItems = [ roleItems ]; }
	
	var showChildComp = false;
	for(x = 0; x < roleItems.length; x++)
	{	
		roleStatus = roleItems[x].status;
		roleName = roleItems[x].roleName;
		roleID = roleItems[x].randomizedRoleId;
		optionSlctdStyle = "";
		roleType = "";
		childClass = "";
			
		if( $("#childCompanies").length ){
			roleType = (roleItems[x].customerTypeId === 3)? " (Child)" : " (Parent)";
			childClass = (roleItems[x].customerTypeId === 3)? "childRole" : ""; }
		else if(roleItems[x].customerName && roleItems[x].customerName != ""){
			roleType = " ("+roleItems[x].customerName+")";}

		if(roleStatus != "selected" && allSelected == true){ allSelected = false; }
		if(roleStatus == "selected"){ 
			colPlacement = selectedCount%4+1;
			optionSlctdStyle = slctdOpacityLevel;
			$("#roleChildList").append("<li class='col"+colPlacement+" detailValue'>"+roleName +' '+ roleType +"</li>");
			if($("#roleSelections").val() == ""){ 
				$("#roleSelections").val(roleID); }
			else{ 
				$("#roleSelections").val($("#roleSelections").val()+","+roleID); }
			selectedCount++;
		}
		
		roleFullList += '<li id="full-'+ roleID +'" class="'+ roleStatus +' '+childClass+'"><a href="#" class="checkBox">&nbsp;</a> '+ roleName  +' '+ roleType +'</li>';
		roleSelectedList += '<li id="select-'+ roleID +'" class="'+ roleStatus +' '+childClass+'"><a href="#" class="checkBox">&nbsp;</a> '+ roleName  +' '+ roleType +'</li>';
		if(roleStatus === "selected" && roleItems[x].customerTypeId === 3){ showChildComp = true; } }

	if(showChildComp === true){ $("#childCompanies").show(); }else{ $("#childCompanies").hide(); }
	
	if(selectedCount == 1){ $("#roleChildList").find("li").addClass("onlyCol").removeClass("col1"); }
	else if(colPlacement == 1){$("#roleChildList").append("<li class='col2 detailValue blank'>&nbsp;</li><li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
	else if(colPlacement == 2){$("#roleChildList").append("<li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
	else if(colPlacement == 3){$("#roleChildList").append("<li class='col4 detailValue blank'>&nbsp;</li>");}
	
	$("#userRoles ul.fullList").html(roleFullList);
	$("#userRoles ul.selectedList").html(roleSelectedList);
	$("#userRoles #roleSelectionsAll").parents("label").show();
	if(allSelected == true){$("#userRoles  #roleSelectionsAll").addClass("checked");} else {$("#userForm  #roleSelectionsAll").removeClass("checked");}

	//CHILD COMPANIES
	var custListLength = defaultCustomers.length;
	for(x=0; x < custListLength; x++){ if(x > 0){  defaultCustomers.splice(1, 1); } }
	$("#defaultCust").autoselector("updateData");

	
	if(userData.userSettingDetails.childCustomerInfos !== undefined){
		$("#compChildList").html("");				
		$("#companySelections, #companySelectionsAllHidden").val("");
		$("#companySelectionsAll").removeClass("checked");
		
		childCompFullList = "";
		childCompSelectedList = "";
		childSelectedCount = 0;
		childAllSelected = userData.userSettingDetails.isAllChilds;
		childColPlacement = 0;
		childCompItems = userData.userSettingDetails.childCustomerInfos;
			
		if(!Array.isArray(childCompItems)){ childCompItems = [ childCompItems ]; }
		
		if(childAllSelected){ $("#companySelectionsAllHidden").val(childAllSelected); $("#companySelectionsAll").addClass("checked"); }
		
		for(x = 0; x < childCompItems.length; x++)
		{
			var	childStatus = childCompItems[x].status,
				childName = childCompItems[x].name,
				childID = childCompItems[x].randomizedCustomerId,
				isChildDefault = childCompItems[x].isDefaultAlias,
				childOptionSlctdStyle = "";
	
			if(childStatus == "selected"){ 
				childColPlacement = childSelectedCount%4+1;
				childOptionSlctdStyle = slctdOpacityLevel;
				$("#compChildList").append("<li class='col"+childColPlacement+" detailValue'>"+childName+"</li>");
				if($("#companySelections").val() == ""){ 
					$("#companySelections").val(childID); 
				}else{ 
					$("#companySelections").val($("#companySelections").val()+","+childID); 
				}
				defaultCustomers.push({ "label": childName, "value": childID });
				if(isChildDefault){ $("#defaultCust").autoselector("setSelectedValue", childID); }
				childSelectedCount++;
			}
			
			childCompFullList += '<li id="full-'+ childID +'" class="'+ childStatus +'" custName="'+ childName +'"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
			childCompSelectedList += '<li id="select-'+ childID +'" class="'+ childStatus +'" custName="'+ childName +'"><a href="#" class="checkBox">&nbsp;</a> '+ childName +'</li>';
		}
	
		if(childSelectedCount == 1){ $("#compChildList").find("li").addClass("onlyCol").removeClass("col1"); }
		else if(childColPlacement == 1){$("#compChildList").append("<li class='col2 detailValue blank'>&nbsp;</li><li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
		else if(childColPlacement == 2){$("#compChildList").append("<li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
		else if(childColPlacement == 3){$("#compChildList").append("<li class='col4 detailValue blank'>&nbsp;</li>");}
		
		$("#childCompanies ul.fullList").html(childCompFullList);
		$("#childCompanies ul.selectedList").html(childCompSelectedList);
		
		if(childSelectedCount >= 1){ $("#compChildListHdr, #compChildList").show(); }
		else{ 
			$("#compChildListHdr, #compChildList").hide(); } }
	else{ $("#compChildListHdr, #compChildList").hide(); }

	if(action == "edit"){ testFormEdit($("#userSettings")); 
		$("#userDetails").removeClass("addForm").addClass("form editForm");
		snglSlideFadeTransition("show", $("#userDetails"), function(){ $("#formRouteName").trigger('focus'); }); }
	else { 
		$("#mainContent").removeClass("edit"); $("#userDetails").removeClass("form editForm addForm");
		snglSlideFadeTransition("show", $("#userDetails")); }	
	$(document.body).scrollTop(0);	
}

//--------------------------------------------------------------------------------------

function resetUserForm(){
	var hideCallBack = function(){
		document.getElementById("userSettings").reset();
		$("#userDetails").hide();
		$("#actionFlag").val("0");
		$("#contentID, #formUserID").val("");
		$("#formUserFirstName").val("");
		$("#userFirstNameLabel").show();
		$("#formUserLastName").val("");
		$("#userLastNameLabel").show();
		$("#statusCheck").removeClass("checked"); $("#statusCheckHidden").val("false");
		$("#formUserName").val("");
		$("#defaultCust").autoselector("reset");
		
		$("#resetPasswordBtnArea").hide();
		$("#newPassword, #c_newPassword").val("");
		$("#c_newPassword").attr("disabled", true);
		$("#cancelResetBtn").hide();
		$("#resetPassword").show();
		$("#userRoles ul.fullList").html("");
		$("#userRoles ul.selectedList").html("");
		$("#roleSelections").val("");
		$("#companySelections").val("");
		$("#userForm  #roleSelectionsAll").removeClass("checked");
		testFormEdit($("#userSettings"));
		
		var custListLength = defaultCustomers.length;
		for(x=0; x < custListLength; x++){ if(x > 0){  defaultCustomers.splice(1, 1); } }
		$("#defaultCust").autoselector("updateData");
		
		//ROLES
		$("#roleChildList").html("");				
		//$("#roleSelections").val("");
		
		var	roleFullList = "",
			roleSelectedList = "",
			compFullList = "",
			compSelectedList = "",
			roleType = "",
			childClass = "",
			ajaxRoleList = new GetHttpObject();
			
		ajaxRoleList.onreadystatechange = function()
		{
			if (ajaxRoleList.readyState==4 && ajaxRoleList.status == 200){
				var	roleList =	JSON.parse(ajaxRoleList.responseText),
					roleItems = roleList.roleSettingInfoWrapper.roleSettingInfos[0].roleSettingInfo;
					childCompanies = roleList.roleSettingInfoWrapper.childCustomerInfos[0].childCustomerInfo;
				if(!Array.isArray(roleItems)){ roleItems = [ roleItems ]; }
				if(childCompanies){ if(!Array.isArray(childCompanies)){ childCompanies = [ childCompanies ]; }}
				
				for(x = 0; x < roleItems.length; x++){	
					var	roleStatus =  roleItems[x].status,
						roleName =  roleItems[x].roleName,
						roleID =  roleItems[x].randomizedRoleId;
					
					roleType = (roleItems[x].customerName && roleItems[x].customerName != "")? " (<em>"+ roleItems[x].customerName +"</em>)": "";
					
					if(childCompanies){
						roleType = (roleItems[x].customerTypeId === 3)? " (<em>Child</em>)" : " (<em>Parent</em>)";
						childClass = (roleItems[x].customerTypeId === 3)? "childRole" : ""; }
					
					roleFullList += '<li id="full-'+ roleID +'" class="'+ roleStatus +' '+ childClass +'"><a href="#" class="checkBox">&nbsp;</a> '+ roleName +' '+ roleType +'</li>';
					roleSelectedList += '<li id="select-'+ roleID +'" class="'+ roleStatus +' '+ childClass +'"><a href="#" class="checkBox">&nbsp;</a> '+ roleName +' '+ roleType +'</li>'; }
				$("#userRoles ul.fullList").html(roleFullList);
				$("#userRoles ul.selectedList").html(roleSelectedList);
				$("#userRoles #roleSelectionsAll").parents("label").show();
	
				if(childCompanies){
					for(x = 0; x < childCompanies.length; x++){	
						var compStatus =  childCompanies[x].status,
							compName =  childCompanies[x].name,
							compID =  childCompanies[x].randomizedCustomerId;
						
						compFullList += '<li id="full-'+ compID +'" class="'+ compStatus +'" custName="'+ compName +'"><a href="#" class="checkBox">&nbsp;</a> '+ compName +'</li>';
						compSelectedList += '<li id="select-'+ compID +'" class="'+ compStatus +'" custName="'+ compName +'"><a href="#" class="checkBox">&nbsp;</a> '+ compName +'</li>'; }
					$("#childCompanies ul.fullList").html(compFullList);
					$("#childCompanies ul.selectedList").html(compSelectedList);
					$("#childCompanies").hide(); }
					
				$(document.body).scrollTop(0); 
				$("#userDetails").removeClass("editForm").addClass("form addForm");
				testFormEdit($("#userSettings")); 
				snglSlideFadeTransition("show", $("#userDetails"), function(){ $("#formUserFirstName").trigger('focus'); });
			} else if(ajaxRoleList.readyState==4){
				alertDialog(systemErrorMsg);  // Unable to delete.
			}
			
		};
		ajaxRoleList.open("GET",LOC.roleList+"?"+document.location.search.substring(1),true);
		ajaxRoleList.send(); }
	
	if($("#userDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#userDetails"), hideCallBack); }
	else{ hideCallBack(); } }

//--------------------------------------------------------------------------------------

function clearChildCompanies(action){
	if(action === "hide"){ $("#childCompanies").slideUp('slow', function(){ 
		$("#companySelectionsAll").removeClass("checked");
		$("#companySelectionsAllHidden").val(false);
		$("#companySelections").val("");
		$("#childCompanies").find("ul.selectedList").find("li.selected").trigger('click'); }).animate({ opacity: 0 },{ queue: false, duration: 'slow' }); }}


/* Account Settings: Ajax Calls */
//--------------------------------------------------------------------------------------

function loadUser(userID, action)
{
	var hideCallBack = '';
	if($("#contentID").val() === userID){
		if(action === "edit"){
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#userDetails"), $("#userDetails"), hideCallBack); }
		else if(action === "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#userDetails"), $("#userDetails"), hideCallBack); } }
	else {
		var $this = $("#user_"+userID);
		if($("#userList").find("li.selected").length){ $("#userList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");

		hideCallBack = function(){
			if(syncMenuItems($("#menu_" + userID + " > .menuOptions"), $("#menu_pageContent"))) {
				$("#opBtn_pageContent").show();
			}
			else {
				$("#opBtn_pageContent").hide();
			}
			
			var ajaxLoadUser = new GetHttpObject();
			ajaxLoadUser.onreadystatechange = function()
			{
				if (ajaxLoadUser.readyState==4 && ajaxLoadUser.status == 200){
					showUserDetails(ajaxLoadUser, action);
				} else if(ajaxLoadUser.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadUser.open("GET",LOC.loadUser+"?userAccountID="+userID+"&"+document.location.search.substring(1),true);
			ajaxLoadUser.send(); }}
		
		if($("#userDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#userDetails"), hideCallBack); }
		else{ hideCallBack(); } }

//--------------------------------------------------------------------------------------

function deleteUser(userID)
{
	var ajaxDeleteUser = new GetHttpObject();
	ajaxDeleteUser.onreadystatechange = function()
	{
		if (ajaxDeleteUser.readyState==4 && ajaxDeleteUser.status == 200){
			if(ajaxDeleteUser.responseText == "true") { 
				noteDialog(userDeleteMsg);
				var newLocation = location.pathname;
				setTimeout(function(){window.location = newLocation;}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteUser.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteUser.open("GET",LOC.deleteUser+"?userAccountID="+userID+"&"+document.location.search.substring(1),true);
	ajaxDeleteUser.send();
}

function verifyDeleteUser(userID)
{
	var	deleteUserButtons = {};
		deleteUserButtons.btn1 = {
			text : deleteBtn,
			click : function() { deleteUser(userID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deleteUserButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteUserMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteUserButtons });
	btnStatus(deleteBtn);
}

//--------------------------------------------------------------------------------------

/* ROLE SETTINGS */

//--------------------------------------------------------------------------------------

function hideRole($this){
	snglSlideFadeTransition("hide", $("#roleDetails")); 
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#roleList").find("li.selected").length){ $("#roleList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function roleSelectionCheck(itemID, eventAction, lockedRole)
{
	itemID = itemID.split("_");
	var tempID = itemID[1],
		$this = $("#role_"+tempID);
	if(eventAction == "hide"){ 
		crntPageAction = "";
		history.pushState(null, null, location.pathname);
		hideUser($this); }
	else { 
		if($("#contentID").val() != tempID || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: tempID, action: eventAction };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			loadRole(stateObj.itemObject, stateObj.action, lockedRole); }}
}

//--------------------------------------------------------------------------------------

function newRoleForm(){ $(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); }); resetRoleForm(); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popRoleAction(tempItemID, tempEvent, stateObj){
	if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#user_"+tempItemID) : ""; hideRole($this); }
	else if(tempEvent === "Add"){ newRoleForm(); }
	else { loadRole(tempItemID, tempEvent); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function roles(roleID, pageEvent)
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popRoleAction(tempItemID, tempEvent, stateObj); }

if(!popStateEvent && (roleID !== undefined && roleID !== '')){ popRoleAction(roleID, pageEvent); }
///////////////////////////////////////////////////	

	//USERS FORM
	msgResolver.mapAttribute("roleName", "#formRoleName");

	$(document.body).on("click", "#roleList > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display",
			itemID = $(this).attr("id"),
			lockedRole = $(this).hasClass("lockedRole");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ roleSelectionCheck(itemID, actionEvent, lockedRole); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false; }}, 100); }
		else{ roleSelectionCheck(itemID, actionEvent, lockedRole); } });

	$("#roleList").on("click", ".view", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id"),
			lockedRole = $(this).hasClass("lockedRole");
		if($(this).parent().hasClass("innerBorder")){
			roleSelectionCheck(itemID, "display", lockedRole);} });

	$("#roleList").on("click", ".edit", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		roleSelectionCheck(itemID, "edit"); });
	
	$("#roleList").on("click", ".delete", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id").split("_");
		verifyDeleteRole(itemID[1]); });
		
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var roleID = $("#contentID").val();
		verifyDeleteRole(roleID);
	});
	
	$("#btnAddRole").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			var stateObj = { itemObject: "New", action: "Add" };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); });
			resetRoleForm(); }
	});
	
	$("#roleSwitch").on("click", ".radioLabel", function(){
		if(!$(this).find("a").hasClass("checked")){
			if($(this).find("a").attr("id").split(":")[1] == 2){ 
				$("#childPermissionHeader").fadeOut("slow", function(){ $("#parentPermissionHeader").fadeIn("slow"); });
				$("#childPermissionList")
					.slideUp('slow', function(){ 
						$("#formPermissionIds").val("");
						$("#childPermissionList").find("a.checked").each(function(){ $(this).removeClass("checked"); });
						$("#parentPermissionList").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); })
					.animate({ opacity: 0 },{ queue: false, duration: 'slow' }); }
			else{ 
				$("#parentPermissionHeader").fadeOut("slow", function(){ $("#childPermissionHeader").fadeIn("slow"); });
				$("#parentPermissionList")
					.slideUp('slow', function(){ 
						$("#formPermissionIds").val("");
						$("#parentPermissionList").find("a.checked").each(function(){ $(this).removeClass("checked"); });
						$("#childPermissionList").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' }); })
					.animate({ opacity: 0 },{ queue: false, duration: 'slow' }); } }});
	
	//Custom Check Box
	$("#roleForm").on("click", ".permissionCheckboxLabel", function(event){
		event.preventDefault();
		if(!$(this).hasClass("disabled")){
			var checkBoxObj = $(this).children(".checkBox"),
				checkboxField = $("#formPermissionIds"),
				permissionID = $(this).attr("id").replace("perm_", "");
			checkBoxObj.toggleClass("checked");
			if(checkBoxObj.hasClass("checked"))
			{
				if(checkboxField.val() == ""){ checkboxField.val(permissionID).trigger('change'); }
				else{ checkboxField.val(checkboxField.val()+","+permissionID).trigger('change'); }
			} else {
				checkboxField.val(checkboxField.val().replace(permissionID, "").replace(",,", ",").replace(" ","")).trigger('change');
				if(checkboxField.val().charAt(0)==","){ checkboxField.val(checkboxField.val().substring(1)); }
				if(checkboxField.val().charAt(checkboxField.val().length-1)==","){checkboxField.val(checkboxField.val().substring(0, checkboxField.val().length-1));}
			}
		}
	});
	
	$("#roleSettings").on('submit', function(event){ event.preventDefault(); });
	
	$("#roleSettings").on("click", ".save", function(event)
	{
		event.preventDefault();
		var roleID = $("#formRoleID").val(),
			params = $("#roleSettings").serialize(),
			ajaxSaveRole = new GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxSaveRole.onreadystatechange = function(){
			if (ajaxSaveRole.readyState==4 && ajaxSaveRole.status == 200) {
				var response = ajaxSaveRole.responseText.split(":");
				if(response[0] == "true") {
					noteDialog(roleSavedMsg);
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["itemValue", "pgActn"]),
						newLocation = location.pathname+"?"+cleanQuerryString+"&pgActn="+crntPageAction;
					setTimeout(function(){window.location = newLocation;}, 500); } }// new location Saved.
			else if(ajaxSaveRole.readyState==4){ alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveRole.open("POST",LOC.saveRole,true);
		ajaxSaveRole.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveRole.setRequestHeader("Content-length", params.length);
		ajaxSaveRole.setRequestHeader("Connection", "close");
		ajaxSaveRole.send(params);

//		}
	});
	
	$("#roleSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
}

function roleSettingsValidate()
{
	var returnStatus = true,
		alertMsg = '',
		alertCode = [];
//Role Name
	if($(formErrors.role101.id).val() == ''){returnStatus = false; alertCode.push("role101");} 
	else if(characterTypes.spclCharacter.test($(formErrors.role102.id).val()) == true){returnStatus = false; alertCode.push("role102");}
	else if($(formErrors.role103.id).val().length > 25){returnStatus = false; alertCode.push("role103");}

	for(x = 0; x < alertCode.length; x++)
	{	var testStr = /(Children)/g;
		if(testStr.test(formErrors[alertCode[x]].id)){ $(formErrors[alertCode[x]].id).children("ul").addClass("error"); }
		else { $(formErrors[alertCode[x]].id).addClass("error"); }
		alertMsg += "<li>" + formErrors[alertCode[x]].message + "</li>";
	}
	
	if(alertCode.length > 0){ formErrorDialog(alertMsg); }
	
	return returnStatus;
}

function showRoleDetails(roleDetails, action, locked)
{	
	//Process JSON
	var roleData =	JSON.parse(roleDetails.responseText);
//console.log(alertDetails.responseText);
	$("#actionFlag").val("1");
	$("#contentID, #formRoleID").val(roleData.roleSettingDetails.randomId);
	
	$("#detailRoleName, #roleDispName").html(roleData.roleSettingDetails.roleName);
	$("#formRoleName").val(roleData.roleSettingDetails.roleName);
	
	if(roleData.roleSettingDetails.isEnabled == true){
		$("#detailRoleStatus").html(roleStatusEnabledMsg);
		$("#statusCheck").addClass("checked"); $("#statusCheckHidden").val("true");
	} else{ 
		$("#detailRoleStatus").html(roleStatusDisabledMsg);
		$("#statusCheck").removeClass("checked"); $("#statusCheckHidden").val("false");
	}
	
	var	roleType = [];
		roleType[0] = "";
		roleType[1] = "System Admin";
		roleType[2] = "Parent";
		roleType[3] = "Child";
		
	if($("#roleSwitch").length){ 
		$("#detailRoleType").html(roleType[roleData.roleSettingDetails.typeId]);
		$("#roleType").val(roleData.roleSettingDetails.typeId);
		$("#roleSwitchForm").hide();
		$("#roleSwitchDsp").html(roleType[roleData.roleSettingDetails.typeId]).show(); }
	
	//PERMISSIONS
	$("#detailPermissionList, .permissionList").html("");				
	$("#formPermissionIds").val("");
	var permissionFullList = "",
		permissionSelectedList = "",
		permissionSelectedIds = "";

	for(x=0; x < roleData.roleSettingDetails.permissionList.length; x++)
	{
		var parentItem = "<li class='Parent'>"+roleData.roleSettingDetails.permissionList[x].permissionName+"</li>",
			childDisplayItem = "",
			childFormItem = "",
			childPermissions = roleData.roleSettingDetails.permissionList[x].childPermissions;
		if(childPermissions instanceof Array === false){ childPermissions = [childPermissions]; }
		for(y=0; y < childPermissions.length; y++){
			var selectedClass = "";
			if(childPermissions[y].status == "selected"){ 
				childDisplayItem = childDisplayItem + "<li class='Child'>"+childPermissions[y].permissionName+"</li>"; 
				if(permissionSelectedIds == ""){ 
					permissionSelectedIds = childPermissions[y].randomPermissionId; }
				else{ permissionSelectedIds = permissionSelectedIds + "," + childPermissions[y].randomPermissionId; }
				selectedClass = "checked"; }
			childFormItem = childFormItem + "<li class='Child'><label class='permissionCheckboxLabel switch' id='perm_"+ childPermissions[y].randomPermissionId +"'><a href='#' class='checkBox "+selectedClass+"'>&nbsp;</a> "+childPermissions[y].permissionName+"</label></li>"; }
		
		if(childDisplayItem != ""){ permissionSelectedList = permissionSelectedList + parentItem + childDisplayItem; }
		permissionFullList = permissionFullList + parentItem + childFormItem;
	}
	
	if(permissionSelectedList == ""){ permissionSelectedList = "<li class='alignCenter notclickable'><span class='textLine'>"+noPermissions+"</span></li>"; }
	$("#detailPermissionList").html(permissionSelectedList);
	$("#parentPermissionList").html(permissionFullList);
	$("#childPermissionList").html(permissionFullList);
	$("#formPermissionIds").val(permissionSelectedIds);
	
	if(roleData.roleSettingDetails.typeId === 2){
		$("#childPermissionHeader, #childPermissionList").hide();
		$("#parentPermissionHeader, #parentPermissionList").css("opacity", 1).show(); }
	else{
		$("#parentPermissionHeader, #parentPermissionList").hide();
		$("#childPermissionHeader, #childPermissionList").css("opacity", 1).show(); }
	
	

	//USERS
	$("#userChildList").html("");
	var selectedCount = 0,
		colPlacement = 0;
	if(roleData.roleSettingDetails.assignedUsers){ 
		var assignedUsers = roleData.roleSettingDetails.assignedUsers;
		if(assignedUsers instanceof Array === false){ assignedUsers = [assignedUsers]; }
		for(x=0; x < assignedUsers.length; x++) {
			var userName = assignedUsers[x].firstName +" "+ assignedUsers[x].lastName;
			var custName = (assignedUsers[x].customerName && assignedUsers[x].customerName != "")? " (<em>"+ assignedUsers[x].customerName +"</em>)": "";
			colPlacement = selectedCount%4+1;
			$("#userChildList").append("<li class='col"+colPlacement+" detailValue'>"+userName+" "+custName+"</li>");
			selectedCount++; }
		if(selectedCount == 0){ $("#userChildList").append("<li class='onlyCol detailValue centerAlign'>"+noUsersAsigned+"</li>"); }
		else if(selectedCount == 1){ $("#userChildList").find("li").addClass("onlyCol").removeClass("col1"); }
		else if(colPlacement == 1){$("#userChildList").append("<li class='col2 detailValue blank'>&nbsp;</li><li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
		else if(colPlacement == 2){$("#userChildList").append("<li class='col3 detailValue blank'>&nbsp;</li><li class='col4 detailValue blank'>&nbsp;</li>");}
		else if(colPlacement == 3){$("#userChildList").append("<li class='col4 detailValue blank'>&nbsp;</li>");} } 
	else {
		$("#userChildList").append("<li class='onlyCol detailValue'>"+noUsersAsignedMsg+"</li>"); }

	if(action == "edit" && !locked){ testFormEdit($("#roleSettings")); 
		$("#roleDetails").removeClass("addForm").addClass("form editForm");
		snglSlideFadeTransition("show", $("#roleDetails"), function(){$("#formRoleName").trigger('focus'); }); }
	else { 
		$("#mainContent").removeClass("edit"); $("#roleDetails").removeClass("form editForm addForm");
		snglSlideFadeTransition("show", $("#roleDetails")); }
	
	if(locked && $("#opBtn_pageContent").length){ $("#opBtn_pageContent").hide(); } else { $("#opBtn_pageContent").show(); }
	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------

function resetRoleForm(){	
	var hideCallBack = function(){
		$("#roleDetails").hide();
		$("#actionFlag").val("0");
		$("#contentID, #formRoleID").val("");
		$("#formRoleName").val("");
		$("#statusCheck").removeClass("checked"); 
		$("#statusCheckHidden").val("false");
		$(".permissionList").html("");
		$("#formPermissionIds").val("");	
		$("#roleType").val(3);
		
		//PERMISSIONS
		var permissionFullList = "", childPermissionFullList = "",
			ajaxPermissionList = new GetHttpObject();
		ajaxPermissionList.onreadystatechange = function()
		{
			if (ajaxPermissionList.readyState==4 && ajaxPermissionList.status == 200){
				var	permissionList = JSON.parse(ajaxPermissionList.responseText),
					parentChildPermission = false,
					permissionTreeObj = permissionList.permissionTreeWrapper.permissionTrees[0].PermissionTree;
					
				if(permissionList.permissionTreeWrapper.secondaryPermissionTrees){ parentChildPermission = true; }
				
				for(x=0; x < permissionTreeObj.length; x++)
				{
					var parentItem = "<li class='Parent'>"+permissionTreeObj[x].permissionName+"</li>",
						childFormItem = "",
						childPermissions = permissionTreeObj[x].childPermissions;
					if(childPermissions instanceof Array === false){ childPermissions = [childPermissions]; }
					for(y=0; y < childPermissions.length; y++) {
						childFormItem = childFormItem + "<li class='Child'><label class='permissionCheckboxLabel switch' id='perm_"+ childPermissions[y].randomPermissionId +"'><a href='#' class='checkBox'>&nbsp;</a> "+childPermissions[y].permissionName+"</label></li>"; }
					permissionFullList = permissionFullList + parentItem + childFormItem;
				}
				if(parentChildPermission){ $("#parentPermissionList").html(permissionFullList); }
				else{ $("#childPermissionList").html(permissionFullList); }
				
				if(parentChildPermission){
					$("#roleType").val(2);
					$("#parentSwitch").find("a.radio").addClass("checked");
					$("#childSwitch").find("a.radio").removeClass("checked");
					$("#roleSwitchForm").show();
					$("#roleSwitchDsp").hide();
	
					var childPermissionTreeObj = permissionList.permissionTreeWrapper.secondaryPermissionTrees[0].PermissionTree;
					for(x=0; x < childPermissionTreeObj.length; x++){
						var childParentItem = "<li class='Parent'>"+childPermissionTreeObj[x].permissionName+"</li>",
							thisChildFormItem = "",
							thisChildPermissions = childPermissionTreeObj[x].childPermissions;
						if(thisChildPermissions instanceof Array === false){ thisChildPermissions = [thisChildPermissions]; }
						for(y=0; y < thisChildPermissions.length; y++) {
							thisChildFormItem = thisChildFormItem + "<li class='Child'><label class='permissionCheckboxLabel switch' id='perm_"+ thisChildPermissions[y].randomPermissionId +"'><a href='#' class='checkBox'>&nbsp;</a> "+thisChildPermissions[y].permissionName+"</label></li>"; }
						childPermissionFullList = childPermissionFullList + childParentItem + thisChildFormItem;
					}
					$("#childPermissionList").html(childPermissionFullList);
					
					$("#childPermissionHeader, #childPermissionList").hide();
					$("#parentPermissionHeader, #parentPermissionList").css("opacity", 1).show(); }

				$(document.body).scrollTop(0);
				testFormEdit($("#roleSettings"));
				$("#roleDetails").removeClass("editForm").addClass("form addForm");
				snglSlideFadeTransition("show", $("#roleDetails"), function(){ $("#formRoleName").trigger('focus'); });
			} else if(ajaxPermissionList.readyState==4){
				$('#messageResponseAlertBox').dialog(alertNoticeOptions).html(editFail);
			}
		};
		ajaxPermissionList.open("GET",LOC.permissionList+"?"+document.location.search.substring(1),true);
		ajaxPermissionList.send(); }
		
	if($("#roleDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#roleDetails"), hideCallBack); }
	else{ hideCallBack(); } }

//--------------------------------------------------------------------------------------

/* Role Settings Ajax Calls */

function loadRole(roleID, action, locked)
{
	var hideCallBack = '';
	if($("#contentID").val() === roleID){
		if(action == "edit"){
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#roleDetails"), $("#roleDetails"), hideCallBack); }
		else if(action == "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#roleDetails"), $("#roleDetails"), hideCallBack); } }
	else {
		var $this = $("#role_"+roleID);
		if($("#roleList").find("li.selected").length){ $("#roleList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");

		hideCallBack = function(){
			var ajaxLoadRole = new GetHttpObject();
			ajaxLoadRole.onreadystatechange = function()
			{
				if (ajaxLoadRole.readyState==4 && ajaxLoadRole.status == 200){
					showRoleDetails(ajaxLoadRole, action, locked);
				} else if(ajaxLoadRole.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadRole.open("GET",LOC.loadRole+"?roleID="+roleID+"&"+document.location.search.substring(1),true);
			ajaxLoadRole.send(); }}
		
	if($("#roleDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#roleDetails"), hideCallBack); }
	else{ hideCallBack(); } }

//--------------------------------------------------------------------------------------

function deleteRole(roleID)
{
	var ajaxDeleteRole = new GetHttpObject();
	ajaxDeleteRole.onreadystatechange = function()
	{
		if (ajaxDeleteRole.readyState==4 && ajaxDeleteRole.status == 200){
			if(ajaxDeleteRole.responseText == "true") { 
				noteDialog(roleDeleteMsg);
				var newLocation = location.pathname+"?"+filterQuerry;
				setTimeout(function(){window.location = newLocation;}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteRole.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteRole.open("GET",LOC.deleteRole+"?roleID="+roleID+"&"+document.location.search.substring(1),true);
	ajaxDeleteRole.send();
}

function verifyDeleteRoleMsg(roleID)
{
	var deleteRoleButtons = {};
	deleteRoleButtons.btn1 = {
		text : deleteBtn,
		click : function() { deleteRole(roleID); $(this).scrollTop(0); $(this).dialog( "close" ); }
	};
	deleteRoleButtons.btn2 = {
		text : cancelBtn,
		click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
	};
	
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteRoleMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteRoleButtons });
	btnStatus(deleteBtn);	
}

function verifyDeleteRole(roleID)
{
	var ajaxVerfiyDeleteRole = new GetHttpObject();
	ajaxVerfiyDeleteRole.onreadystatechange = function() {
		if(ajaxVerfiyDeleteRole.readyState==4 && ajaxVerfiyDeleteRole.status == 200){
			var response = ajaxVerfiyDeleteRole.responseText.split(":");
			if(response[0] === "false"){
				formErrorDialog(response[1]);
			}
			else {
				verifyDeleteRoleMsg(roleID);
			}
		} else if(ajaxVerfiyDeleteRole.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxVerfiyDeleteRole.open("GET",LOC.verifyRole+"?roleID="+roleID+"&"+document.location.search.substring(1),true);
	ajaxVerfiyDeleteRole.send();
}
