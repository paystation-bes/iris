(function($, undefined) {
	$.widget("ui.paginatetab", {
		"options": {
			"formSelector": "",
			"rowTemplate": " - ",
			"responseExtractor": function(rawResponse, processingCtx) {
				return JSON.parse(rawResponse);
			},
			"dataKeyExtractor": function(rawResponse, processingCtx) {
				return (processingCtx.dataKey) ? processingCtx.dataKey : "";
			},
			"pageExtractor": function(rawResponse, processingCtx) {
				return (processingCtx.page) ? processingCtx.page : "";
			},
			"rowExtractor": function(listObj, idx, processingCtx) {
				var list = listObj;
				if(!Array.isArray(list)) {
					list = [ listObj ];
				}
				
				return (idx >= list.length) ? null : list[idx];
			},
			"headerCSS": "scrollingListHeader",
			"loadingPanelCSS": "loadItem",
			"pagingAndOrdering": function(page, sortParams, dataKey) {
				/* This is for multi-sort option
				var result = [ "page=" + page ];
				var sortObj, idx = -1, len = sortParams.length;
				while(++idx < len) {
					sortObj = sortParams[idx];
					result.push("order=" + sortObj.name + ":" + ((sortObj.desc) ? "desc" : "asc"));
				}
				*/
				var result = [];
				if(page) {
					result.push("wrappedObject.page=" + page);
				}
				
				if(dataKey) {
					result.push("wrappedObject.dataKey=" + dataKey);
				}
				
				if(sortParams && (sortParams.length > 0)) {
					var sortObj = sortParams[0];
					
					result.push("wrappedObject.column=" + sortObj.name);
					result.push("wrappedObject.order=" + ((sortObj.desc) ? "DESC" : "ASC"));
				}
				
				return result.join("&");
			},
			"compileTemplate": compileTemplate,
			"submissionFormat": "POST",
			"postTokenSelector": null,
			"triggerSelector": null,
			"emptyMsgTemplate": null,
			"maxPagesLimit": 40,
			"dialogFn": alertDialog,
			"itemLocatorURL": null,
			"itemLocatorTemplate": "li#[%= id %]",
			"itemLocatorParamTemplate": "wrappedObject.targetRandomId=[%= id %]",
			"idExtractor": null,
			"justifier": function() {
				var id = this.element.attr("id");
				scrollListWidth([id]);
			},
			"drawFilter": function(rowObj) {
				return true;
			}
		},
		"_create": function() {
			this.requestQueue = [];
			
			this.container = $("<span>").css("display", "block").appendTo(this.element);
			
			this.loadingObj = this.element.find("." + this.options.loadingPanelCSS).hide().remove();
			
			this._reconfigure(null);
			
			this.scrollLoading = false;
			this.firstPageLoaded = false;
			this.lastPageLoaded = false;
			
			this.scopeStart = 0;
			this.scopeEnd = 0;
			
			this.dataKey = "";
			
			this.__currentSortEventHandler = null;
			this.sortParams = [];
			
			var that = this;
			this.element.on("scroll", function() {
				that._scrollLoading();
			});
			
			this.clearData();
			
			return this;
		},
		"_scrollLoading": function() {
			var scrollPos = this.element.scrollTop();
			var scrollTotal = this.container.height() - 5 - this.element.height();
			if(scrollPos >= scrollTotal) {
				this._loadNextPage();
			}
			else if(scrollPos <= 5) {
				this._loadPrevPage();
			}
		},
		"_reconfigure": function(key) {
			var conf = this.options;
			
			if((key === null) || (key === "emptyMessage") || (key === "emptyMsgTemplate")) {
				if(typeof conf.emptyMessage === "undefined") {
					// Getting "emptyMsg" from standard.js
					if(typeof emptyMsg !== "undefined") messageBuffer = emptyMsg;
				}
				else {
					messageBuffer = conf.emptyMessage;
				}
				
				if(conf.emptyMsgTemplate !== null) {
					conf.emptyMessage = conf.emptyMsgTemplate.replace("***", messageBuffer);
				}
				else {
					conf.emptyMessage = "<li class=\"listMessage notclickable\">" + messageBuffer + "</li>";
				}
				
				if(this.emptyMessage) {
					this.emptyMessage.remove();
				}
				
				this.emptyMessage = $(conf.emptyMessage);
			}
			
			if((key === null) || (key === "compileTemplate") || (key === "rowTemplate")) {
				this.templateFn = conf.compileTemplate(conf.rowTemplate);
			}
			
			if((key === null) || (key === "compileTemplate") || (key === "itemLocatorParamTemplate")) {
				if(conf.itemLocatorParamTemplate && (conf.itemLocatorParamTemplate.length > 0)) {
					this.itemLocatorParamTemplateFn = conf.compileTemplate(conf.itemLocatorParamTemplate);
				}
				else {
					this.itemLocatorParamTemplateFn = null;
				}
			}
			
			if((key === null) || (key === "itemLocatorTemplate")) {
				if(conf.itemLocatorTemplate && (conf.itemLocatorTemplate.length > 0)) {
					this.itemLocatorTemplateFn = conf.compileTemplate(conf.itemLocatorTemplate);
				}
				else {
					this.itemLocatorTemplateFn = null;
				}
			}
		},
		"addSorting": function(attr, isDescending, reloadData) {
			if(attr) {
				this.removeSorting(attr, false);
				
				this.sortParams.push({ "name": attr, "desc": (typeof isDescending !== "undefined") && (isDescending === true) });
				if((typeof reloadData === "undefined") || (reloadData === true)) {
					this.reloadData();
				}
			}
		},
		"getCurrentPage": function() {
			var result;
			
			var viewportTop = this.element.scrollTop();
			var currHeight = this.container.height();
			
			var allPage = this.container.find("span[class*=page]");
			var len = allPage.length;
			if((viewportTop === 0) || (len <= 1)) {
				result = 1;
			}
			else {
				var avgHeight = $(allPage[1]).offset().top - $(allPage[0]).offset().top;
				var estPage = Math.ceil(currHeight / avgHeight);
				if(estPage > len) {
					estPage = len;
				}
				
				--estPage;
				
				var pageTop = $(allPage[estPage]).offset().top;
				if(pageTop == 0) {
					// Bingo !
					result = estPage;
				}
				else if(pageTop > 0) {
					// Search Up
					while((estPage > 1) && (pageTop > 0)) {
						--estPage;
						pageTop = $(allPage[estPage - 1]).offset().top;
					}
					
					result = estPage;
				}
				else {
					// Search Down
					while((estPage < len) && (pageTop < 0)) {
						++estPage;
						pageTop = $(allPage[estPage - 1]).offset().top;
					}
					
					result = estPage - 1;
				}
				
				result += this.scopeStart;
			}
			
			return result;
		},
		"removeSorting": function(attr, reloadData) {
			if(attr) {
				var removed = false;
				var sortObj;
				var idx = -1, len = this.sortParams.length;
				while(++idx < len) {
					sortObj = this.sortParams[idx];
					if(sortObj.name == attr) {
						this.sortParams.splice(idx, 1);
						removed = true;
					}
				}
				
				if(removed && ((typeof reloadData === "undefined") || (reloadData === true))) {
					this.reloadData();
				}
			}
		},
		"clearCurrentSortings": function(reloadData) {
			var oldSortParams = this.sortParams;
			this.sortParams = [];
			if(oldSortParams && (oldSortParams.length <= 0) && ((typeof reloadData === "undefined") || (reloadData === true))) {
				this.reloadData();
			}
		},
		"replaceSorting": function(attr, isDescending) {
			this.clearCurrentSortings(false);
			this.addSorting(attr, isDescending, true);
		},
		"addRawData": function(rawData, preventNextPage) {
			var page = this.getCurrentPage();
			this._drawPageData(page, rawData, false);
			this._updatePageInfo(page);
			
			this.options.justifier.call(this);
			
			if(!preventNextPage) {
				this._ensureScroll();
			}
		},
		"forcePageData": function(page, rawData, preventNextPage) {
			this.clearData(true);
			
			this._drawPageData(page, rawData, false);
			this._updatePageInfo(page);
			if(!preventNextPage) {
				this._ensureScroll();
			}
		},
		"completeData": function(){
			var that = this;
			if(!this.lastPageLoaded){
			this._loadPage({
				"page": this.scopeEnd + 1,
				"reloading": false,
				"callback": function() {
					if(!that.lastPageLoaded){ that.completeData(); } } }); }
		},
		"_dumpState": function() {
			return {
				"firstPageLoaded": this.firstPageLoaded,
				"lastPageLoaded": this.lastPageLoaded,
				"scopeStart": this.scopeStart,
				"scopeEnd": this.scopeEnd,
				"dataKey": this.dataKey
			};
		},
		"reloadData": function(callback) {
			return this.reloadPage(1, callback);
		},
		"reloadPage": function(page, callback) {
			var oldState = this._dumpState();
			var reloaded = this._loadPage({
				"page": page,
				"reloading": true,
				"callback": callback
			});
			if(!reloaded) {
				$.extend(this, oldState);
			}
			this.element.find("li").hide();
			this.loadingObj.appendTo(this.element).show();
			return reloaded;
		},
		"reloadPageWithItem": function(itemParams, callback) {
			var reloaded = false;
			if(this.options.itemLocatorURL && (this.options.itemLocatorURL.length > 0)
					&& this.itemLocatorParamTemplateFn) {
				var oldState = this._dumpState();
				
				var originalCallback = callback;
				if(this.itemLocatorTemplateFn) {
					var that = this;
					callback = function() {
						var $item = $(that.itemLocatorTemplateFn(itemParams));
						if($item.length > 0) {
							that.element.scrollTop($item.offset().top);
						}
						
						originalCallback();
					};
				}

				reloaded = this._loadPage({
					"page": 1,
					"reloading": true,
					"callback": callback,
					"url": this.options.itemLocatorURL,
					"params": this.itemLocatorParamTemplateFn(itemParams)
				});
				
				if(!reloaded) {
					$.extend(this, oldState);
				}
			}
			
			return reloaded;
		},
		"reloadCurrentPage": function(callback) {
			this.reloadPage(this.getCurrentPage(), callback);
		},
		"_getPageIndexElement": function(page) {
			var $pageIdxElem = this.container.find(".page" + page);
			if($pageIdxElem.length <= 0) {
				$pageIdxElem = this._createPageIndexElement(page);
				if((this.scopeStart <= 0) || (this.scopeEnd < page)) {
					$pageIdxElem.appendTo(this.container);
				}
				else {
					$pageIdxElem.insertBefore(this._getPageIndexElement(page + 1));
				}
			}
			
			return $pageIdxElem;
		},
		"_createPageIndexElement": function(page) {
			return $("<span class='page" + page + " empty'></span>");
		},
		"createParameterString": function(page, specialParams) {
			var params = "";
			
			var formSelector = this.options.formSelector;
			if((formSelector != null) && (formSelector.length > 0)) {
				params = $(formSelector).serialize();
			}
			else {
				params = document.location.search.substring(1);
			}
			
			var sortParams = this.sortParams;
			if(sortParams && (sortParams.length <= 0)) {
				sortParams = this.defaultSortParams;
			}
			
			var extraParams = this.options.pagingAndOrdering(page, sortParams, this.dataKey);
			if(params.length > 0) {
				params = params + "&" + extraParams;
			}
			else {
				params = extraParams;
			}
			
			if(specialParams && (specialParams.length > 0)) {
				if(params.length > 0) {
					params = params + "&" + specialParams;
				}
				else {
					params = specialParams;
				}
			}
			
			return params;
		},
		"_updatePageInfo": function(page) {
			if((this.scopeEnd <= 0) || (this.scopeEnd < page)) {
				this.scopeEnd = page;
			}
			
			if((this.scopeStart <= 0) || (page === this.scopeStart)) {
				this.scopeStart = page;
			}
			else if(page > this.scopeStart) {
				var offset = page - this.scopeStart;
				if(offset >= this.options.maxPagesLimit) {
					var newScrollTop = this.element.scrollTop() - this.calculatePageHeight(this.scopeStart);
					
					var $scopeStartElem = this._getPageIndexElement(this.scopeStart);
					$scopeStartElem.nextUntil(this._getPageIndexElement(this.scopeStart + 1)).remove();
					$scopeStartElem.remove();
					
					++this.scopeStart;
					this.element.scrollTop(newScrollTop);
				}
			}
			else {
				this.scopeStart = page;
				var newScrollTop = this.calculatePageHeight(this.scopeStart);
				if(this._currTopPos) {
					newScrollTop -= this._currTopPos;
					this._currTopPos = null;
				}
				
				this.element.scrollTop(newScrollTop);
				
				if((this.scopeEnd - this.scopeStart) >= this.options.maxPagesLimit) {
					var $lastScopeStartElem = this._getPageIndexElement(this.scopeEnd);
					var $lastScopeEndElem = this._getPageIndexElement(this.scopeEnd + 1);
					
					$lastScopeStartElem.nextUntil($lastScopeEndElem).remove();
					$lastScopeEndElem.remove();
					$lastScopeStartElem.addClass("empty");
					
					--this.scopeEnd;
				}
			}
			
			this.firstPageLoaded = (this.scopeStart <= 1);
		},
		"calculatePageHeight": function(page) {
			return (this._getPageIndexElement(page + 1).offset().top - this._getPageIndexElement(page).offset().top);
		},
		"calculatePageTop": function(page) {
			return (this._getPageIndexElement(page).offset().top - this.element.offset().top);
		},
		/*
		 * loadOptions = {
		 * 		page: Page to be loaded.
		 * 		reloading: If 'true', paginatetab will clear everything and load the specified 'page'.
		 * 		callback: function that will get executed when the page is already drawn or when an error occurred.
		 * 		url: Special url for this load.
		 * 		params: Extra parameter string to be merge with the request.
		 * }
		 */
		"_loadPage": function(loadOptions) {
			var conf = this.options;
			
			var $pageIdxElem = this._getPageIndexElement(loadOptions.page);
			var shouldLoad = true;
			if(loadOptions.reloading) {
				this.dataKey = "";
			}
			else {
				shouldLoad = $pageIdxElem.hasClass("empty");
			}
			
			if(shouldLoad) {
				var params = this.createParameterString(loadOptions.page, loadOptions.params);
				var isGET = (conf.submissionFormat === "GET");
				
				var that = this;
				var loadFn = function(responseFalse, displayedError) {
					var error = true;
					var drawResult = null;
					if((request.status === 200) && (!responseFalse)) {
						drawResult = that._drawPageData(loadOptions.page, request.responseText, loadOptions.reloading);
						if(!loadOptions.reloading) {
							that.element.trigger("pageLoaded");
						}
						else {
							that.element.trigger("reloaded");
							that.element.trigger("pageLoaded");
						}
						
						error = false;
					}
					else {
						if((!displayedError) && (typeof that.options.dialogFn === "function")) {
							that.options.dialogFn(systemErrorMsg);
						}
					}
					
					if(that._$triggerObj) {
						that._$triggerObj.removeClass("inactive");
						delete that._$triggerObj;
					}
					
					if(!error) {
						that._updatePageInfo(drawResult.actualPage);
					}
					
					if(typeof loadOptions.callback === "function") {
						loadOptions.callback(error, request.responseText, (drawResult) ? drawResult.translatedObj : null);
					}
					
					if(loadOptions.reloading) {
						that.options.justifier.call(that);
					}
					
					// Process next request.
					if(that.requestQueue.length <= 0) {
						that.loading = false;
						if(!error) {
							that._ensureScroll(); 
						}
					}
					else {
						window.setTimeout(function() {
							var reqCtx = that.requestQueue.pop();
							reqCtx.request.send(reqCtx.params);
						}, 100);
					}
				};
				
				var request = GetHttpObject({
					"postTokenSelector": this.options.postTokenSelector
				});
				
				request.onreadystatechange = function(responseFalse, displayedError) {
					if(request.readyState === 4) {
						if((!that.loadingObj) || (!that.loadingObj.is(":visible"))) {
							loadFn(responseFalse, displayedError);
						}
						else {
							that.loadingObj.fadeOut("slow", function() {
								that.loadingObj.remove();
								loadFn(responseFalse, displayedError);
							});
						}
					}
				};
				
				var dataURL = loadOptions.url;
				if((!dataURL) || (dataURL.length <= 0)) {
					dataURL = this.options.url;
				}
				
				if(isGET) {
					request.open("GET", dataURL + "?" + params, true);
				}
				else {
					request.open("POST", dataURL, true);
				}
				
				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
				if(!this.loading) {
					this.loading = true;
					if(isGET) {
						request.send();
					}
					else {
						request.send(params);
					}
				}
				else {
					if(loadOptions.reloading) {
						this.requestQueue = [];
					}
					
					this.requestQueue.push({
						"request": request,
						"params": params
					});
				}
			}
			
			return shouldLoad;
		},
		"_loadPrevPage": function(callbackFn) {
			var conf = this.options;
			var shouldLoad = (!this.scrollLoading) && (!this.firstPageLoaded);
			if(shouldLoad) {
				if(conf.triggerSelector) {
					this._$triggerObj = $(conf.triggerSelector);
					if(this._$triggerObj.hasClass("inactive")) {
						shouldLoad = false;
					}
					else {
						this._$triggerObj.addClass("inactive");
					}
				}
			}
			
			if(!shouldLoad) {
				if(typeof callbackFn === "function") {
					callbackFn();
				}
			}
			else {
				this.scrollLoading = true;
				
				var that = this;
				this.loadingObj.prependTo(this.element).show({
					"complete": function() {
						that._currTopPos = that.calculatePageTop(that.scopeStart);
					}
				});
				
				this._loadPage({
					"page": this.scopeStart - 1,
					"reloading": false,
					"callback": function() {
						that.scrollLoading = false;
						if(typeof callbackFn === "function") {
							callbackFn();
						}
					}
				});
			}
			
			return shouldLoad;
		},
		"_loadNextPage": function(callbackFn) {
			var conf = this.options;
			var shouldLoad = (!this.scrollLoading) && (!this.lastPageLoaded);
			if(shouldLoad) {
				if(conf.triggerSelector) {
					this._$triggerObj = $(conf.triggerSelector);
					if(this._$triggerObj.hasClass("inactive")) {
						shouldLoad = false;
					}
					else {
						this._$triggerObj.addClass("inactive");
					}
				}
			}
			
			if(!shouldLoad) {
				if(typeof callbackFn === "function") {
					callbackFn();
				}
			}
			else {
				this.scrollLoading = true;
				this.loadingObj.appendTo(this.element).show();
				
				var that = this;
				this._loadPage({
					"page": this.scopeEnd + 1,
					"reloading": false,
					"callback": function() {
						that.scrollLoading = false;
						if(typeof callbackFn === "function") {
							callbackFn();
						}
					}
				});
			}
			
			return shouldLoad;
		},
		"containsData": function() {
			return this.hasData;
		},
		"justify": function() {
			if(!this.hasData) {
				this.emptyMessage.appendTo(this.container).show();
			}
			else {
				this.emptyMessage.hide().remove();
			}
			
			this._ensureScroll();
		},
		"_ensureScroll": function() {
			var maxHeight = this.element.css("max-height");
			if(maxHeight === "none") {
				maxHeight = -1;
			}
			else {
				maxHeight = maxHeight.toLowerCase().replace("px", "");
				maxHeight = parseInt(maxHeight, 10);
				if(isNaN(maxHeight)) {
					maxHeight = -1;
				}
			}
						
			if(maxHeight <= 0) {
				maxHeight = this.element.height();
			}
			
			var loadPrevPage = (this.element.scrollTop() <= 0);
			var loadNextPage = (maxHeight >= this.container.height());
			
			var that = this;
			if(!loadPrevPage) {
				if(loadNextPage) {
					this._loadNextPage(function() {
						that.options.justifier.call(that);
					}); 
				}
			}
			else {
				this._loadPrevPage(function() {
					if(loadNextPage) {
						that._loadNextPage(function() {
							that.options.justifier.call(that);
						});
					}
				});
			}
		},
		"clearData": function(options) {
			var defaultOptions = {
				"hideEmptyMessage": false,
				"showLoadingPanel": false,
				"callback": null
			};
			
			var conf;
			if(!options) {
				conf = defaultOptions;
			}
			else {
				conf = $.extend(defaultOptions, options);
			}
			
			var $data = this.container.find(">*");
			
			var headerCSS = this.options.headerCSS;
			if(headerCSS && (headerCSS.length > 0)) {
				$data = $data.filter(":not(." + headerCSS + ")");
			}
			
			var loadingPanelCSS = this.options.loadingPanelCSS;
			if(loadingPanelCSS && (loadingPanelCSS.length > 0)) {
				$data = $data.filter(":not(." + loadingPanelCSS + ")");
			}
			
			$data.remove();
			
			if(!conf.hideEmptyMessage) {
				this.emptyMessage.appendTo(this.container);
			}
			
			if(conf.showLoadingPanel) {
				this.loadingObj.appendTo(this.element).show();
			}
			
			this.pageData = [];
			this.idHash = {};
			
			this.scopeStart = 0;
			this.scopeEnd = 0;
			
			this.hasData = false;
			this.firstPageLoaded = false;
			this.lastPageLoaded = false;
			
			this._currTopPos = null;
			
			if(typeof conf.callback === "function") {
				conf.callback();
			}
			
			this.element.trigger("clear");
		},
		"setupSortingMenu": function(selector) { 
			var paginatetab = this;
			var $target = $(selector);
			
			this.defaultSortParams = [];
			var $currSortElem = $target.find(".sort.current");
			var sortName;
			if($currSortElem.length > 0) {
				$currSortElem = $($currSortElem[0]);
				sortName = $currSortElem.attr("name");
				this.defaultSortParams.push({
					"name": sortName.substring(sortName.indexOf("_") + 1),
					"desc": ($currSortElem.find("img.sortIcn").attr("src").indexOf("ArwUp") > 0) ? true : false
				});
			}
			
			if(typeof this.__currentSortEventHandler === "function") {
				$target.off("click", ".sort", this.__currentSortEventHandler);
			}
			
			this.__currentSortEventHandler = function() {
				var $this = $(this);
				var enableTooltip = false;
				var attrNames = $this.attr("sort");
				if(attrNames) {
					attrNames = attrNames.split(":");
					if(attrNames.length >= 2) {
						enableTooltip = true;
					}
				}
				
				var sortIcon = $this.find("img.sortIcn");
				if(sortIcon.length > 0) {
					var iconSrc = sortIcon.attr("src");
					var sortDesc = (iconSrc.indexOf("ArwUp") > 0);
					if($this.hasClass("current")) {
						sortDesc = !sortDesc;
					}
					
					var sortCol = $this.attr("name");
					sortCol = sortCol.substring(sortCol.indexOf("_") + 1);
					
					$target.find(".sort.current").each(function(index, sortElem) {
						var $sortElem = $(sortElem);
						$sortElem.removeClass("current");
						$sortElem.find("img.sortIcn").attr("src", $sortElem.find("img.sortIcn").attr("src").replace("ArwUp", "ArwDown"));
						$sortElem.attr("sort", $sortElem.attr("sort").replace("DESC", "ASC"));
						$sortElem.attr("tooltip", $this.attr("colTitle")+ " : "+sortAscendingMsg);
					});
					
					if(sortDesc) {
						sortIcon.attr("src", iconSrc.replace("ArwDown", "ArwUp"));
						$this.attr("tooltip", $this.attr("colTitle")+ " : "+sortAscendingMsg);
						if($(".ui-tooltip:visible").length > 0){$(".ui-tooltip:visible").html($this.attr("colTitle")+ " : "+sortAscendingMsg);}
					}
					else {
						sortIcon.attr("src", iconSrc.replace("ArwUp", "ArwDown"));
						$this.attr("tooltip", $this.attr("colTitle")+ " : "+sortDescendingMsg);
						if($(".ui-tooltip:visible").length > 0){$(".ui-tooltip:visible").html($this.attr("colTitle")+ " : "+sortDescendingMsg);}
					}
					
					$this.addClass("current");
					if(enableTooltip) {
						if(sortDesc) {
							attrNames[1] = "DESC";
						}
						else {
							attrNames[1] = "ASC";
						}
						
						$this.attr("sort", attrNames.join(":"));
					}
					
					paginatetab.replaceSorting(sortCol, sortDesc);
				}
			}
			
			$target.on("click", ".sort", this.__currentSortEventHandler);
		},
		"_drawPageData": function(page, rawData, reloading) {
			if(reloading) {
				this.clearData({ hideEmptyMessage: true });
			}
			
			var processingCtx = {};
			var idx = 0;
			
			var rows = [];
			var translatedObj = this.options.responseExtractor(rawData, processingCtx);
			var actualPage = this.options.pageExtractor(rawData, processingCtx);
			if((!reloading) || (!actualPage) || (actualPage <= 0)) {
				actualPage = page;
			}
			
			var $pageIdxElem = this._getPageIndexElement(actualPage);
			var $nextPageIdxElem = this._getPageIndexElement(actualPage + 1);
			if(translatedObj) {
				var rowExtractor = this.options.rowExtractor;
				var rowObj = rowExtractor(translatedObj, idx, processingCtx);
				var id, domElem, extractId = (typeof this.options.idExtractor === "function");
				while(rowObj) {
					if(this.options.drawFilter(rowObj)) {
						rows.push(rowObj);
						domElem = $(this.templateFn(rowObj)).insertBefore($nextPageIdxElem);
						if(extractId) {
							id = this.options.idExtractor(rowObj);
							if(id) {
								this.idHash[id] = {
									"page": page,
									"row": idx,
									"dom": domElem
								};
							}
						}
						
						this.hasData = true;
					}
					
					++idx;
					rowObj = rowExtractor(translatedObj, idx, processingCtx);
				}
				
				if(rows.length > 0) {
					$pageIdxElem.removeClass("empty");
				}
			}
			
			var dataKeyBuffer = this.options.dataKeyExtractor(rawData, processingCtx);
			if(dataKeyBuffer) {
				this.dataKey = dataKeyBuffer;
			}
			
			if(idx <= 0) {
				// End of pages or no data.
				this.lastPageLoaded = true;
			}
			
			this.pageData[actualPage] = rows;
			
			var result = {
				"translatedObj": translatedObj,
				"actualPage": actualPage,
				"rows": rows
			};
			
			this.element.trigger("pageDrawn", result);
			this._afterDrawn();
			
			return result;
		},
		"redrawPage": function(page, preventNextPage) {
			var rows = this.pageData[page];
			if((!rows) || (rows.length <= 0)) {
				throw new "Could not locate local data for page: " + page;
			}
			
			var $pageIdxElem = this._getPageIndexElement(page);
			var $nextPageIdxElem = this._getPageIndexElement(page + 1);
			
			$pageIdxElem.nextUntil($nextPageIdxElem).remove();
			
			var idx = -1, len = rows.length, rowObj;
			while(++idx < len) {
				rowObj = rows[idx];
				if(this.options.drawFilter(rowObj)) {
					$(this.templateFn(rowObj)).insertBefore($nextPageIdxElem);
					this.hasData = true;
				}
			}
			
			var result = {
				"rows": rows
			};
			
			this.element.trigger("pageRedrawn", result);
			this._afterDrawn();
			
			if((!preventNextPage) || (preventNextPage !== true)) {
				this._ensureScroll();
			}
			else {
				this.justifier.call(this);
			}
			
			return result;
		},
		"getPositionById": function(id) {
			var result = null;
			if(id) {
				result = this.idHash[id];
			}
			
			return result;
		},
		"getObjectAt": function(page, row) {
			var result = null;
			if((typeof page === "number") && (typeof row === "number")) {
				result = this.pageData[page][row];
			}
			
			return result;
		},
		"getObjceById": function(id) {
			var result = null;
			var pos = this.getPositionById(id);
			if(pos) {
				result = getObjectAt(pos.page, pos.row);
			}
			
			return result;
		},
		"getDomElementsById": function(id) {
			var result = $();
			var pos = this.getPositionById(id);
			if(pos) {
				result.add(pos.dom);
			}
			
			return result;
		},
		"_afterDrawn": function() {
			if(!this.hasData) {
				this.emptyMessage.appendTo(this.container).show();
			}
			else {
				this.emptyMessage.hide().remove();
			}
		},
		"_setOption": function(key, value) {
			$.Widget.prototype._setOption.apply(this, arguments);
			this._reconfigure(key);
		},
		"_getCssHeight": function(element) {
			var result = element.css("height");
			if(result === "none") {
				result = 0;
			}
			else {
				result = parseInt(result.toLowerCase().replace("px", ""));
				if(isNaN(result)) {
					result = 0;
				}
			}
			
			return result;
		}
	});
}(jQuery));
