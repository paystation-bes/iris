// JavaScript Document

function showRouteDetails(routeDetails, action)
{
	//Process JSON
	var routeData =	JSON.parse(routeDetails.responseText);
//$("#routeDetails").append(routeDetails.responseText);
	$("#actionFlag").val("1");
	$("#mapHolder").html("");
	$("#contentID, #formRouteID").val(routeData.routeDetails.randomRouteId);
	$("#detailRouteName, #routeName").html(routeData.routeDetails.name);
	$("#formRouteName").val(routeData.routeDetails.name);
	$("#detailRouteType").html(routeData.routeDetails.typeName);
	$("#formRouteType").autoselector("setSelectedValue", routeData.routeDetails.randomTypeId);

	//PAYSTATIONS
	$("#psChildList").html("");				
	$("#psSelections").val("");
	$("#locationRouteAC").val("");
	$(".routePaystationLabel").removeClass("active");
	$(".routePaystationLabel, .routePaystationSelectAllBtn").show();
	
	var childFullList = "";
	var childSelectedList = "";
	var showOptions = false;
	var selectedCount = 0;
	var allSelected = true;
	var colPlacement = 0;
	if(routeData.routeDetails.allPaystationList){
		var allPaystationList = routeData.routeDetails.allPaystationList;
		if(allPaystationList instanceof Array === false){ allPaystationList = [allPaystationList]; }
		
		for(var x = 0; x < allPaystationList.length; x++)
		{
			var childType = allPaystationList[x].payStationType;
			var childSeverity = allPaystationList[x].alertSeverity;
			var childStatus =  allPaystationList[x].status;
			var childName =  allPaystationList[x].name;
			var childID =  allPaystationList[x].randomPosId;
			var childLocations =  allPaystationList[x].randomLocationId;
			var childRoutes =  allPaystationList[x].randomRouteId;
			var childSerialNmbr = allPaystationList[x].serialNumber;
			var childRoutesStr = "";
			var optionSlctdStyle = "";
			
			if(childRoutes instanceof Array === false){ childRoutes = [childRoutes]; }
			for(var n = 0; n < childRoutes.length; n++){ childRoutesStr = childRoutesStr + " " + childRoutes[n]; }
	
			if(childStatus != "selected" && allSelected == true){ allSelected = false; }
			if(childStatus == "selected"){ 
				var colPlacement = selectedCount%3+1;
				optionSlctdStyle = slctdOpacityLevel;
				var scrubbedQuerry = scrubQueryString(document.location.search.substring(1), "all");
								
				var posItem = (posPermission)? "<a href='"+detailsUrl+"payStationList.html?itemID="+childID+"&pgActn=display&tabID=status&"+scrubbedQuerry+"'>"+childName+"</a>" : childName ;
				$("#psChildList").append("<li class='col"+colPlacement+" detailValue posT"+childType+"-"+childSeverity+"'>"+posItem+"<br><span class='note'>"+ childSerialNmbr +"</span></li>");
				if($("#psSelections").val() == ""){ 
					$("#psSelections").val(childID); 
				}else{ 
					$("#psSelections").val($("#psSelections").val()+","+childID); 
				}
				selectedCount++;
			} }
		
		if(colPlacement == 1 && selectedCount > 1){$("#psChildList").append("<li class='col2 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li><li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li>");}
		if(colPlacement == 2){$("#psChildList").append("<li class='col3 detailValue blank'><a name='blankSpace'>&nbsp;</a><br><span class='note'>&nbsp;</span></li>");}
		
		if(selectedCount === 0){ $("#psChildList").append("<li class='onlyCol detailValue blank'>"+ noPaystationsMsg +"</li>"); }
		if(selectedCount == 1){ $("#psChildList").find("li").addClass("onlyCol").removeClass("col1"); } }
	else { $("#psChildList").append("<li class='onlyCol detailValue blank'>"+ noPaystationsMsg +"</li>"); }
	
	clearErrors();
	//Display Method
	if(action == "display")
	{
		$("#mainContent").removeClass("edit"); 
		if(routeData.routeDetails.widgetMapInfo.missingPayStation === false && $("#PlacePS").length ){ $("#PlacePS").hide(); } else { $("#PlacePS").show(); }
		var displayCallBack = function(){
			var payStationMarkers = new Array();
			if(routeData.routeDetails.widgetMapInfo.mapEntries){
				var payStationList = routeData.routeDetails.widgetMapInfo.mapEntries;
				if(payStationList instanceof Array === false){ payStationList = [payStationList]; }
				for(x = 0; x <  payStationList.length; x++){ 
//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
					if(payStationList[x].latitude && payStationList[x].latitude != ''){
						payStationMarkers.push(new mapMarker(
							payStationList[x].latitude, 
							payStationList[x].longitude, 
							payStationList[x].name, 
							payStationList[x].payStationType, 
							payStationList[x].randomId,
							payStationList[x].severity,'','','','','','',
							payStationList[x].serialNumber )); } 
							initMapStatic(payStationMarkers);	}} 
			else if(!routeData.routeDetails.widgetMapInfo.missingPayStation){ initMapStatic(); }
			else { initMapStatic("noData"); } }; 
			
		$("#routeDetails").removeClass("form editForm addForm"); }
	else if(action == "edit") {
		testFormEdit($("#routeSettings"));
		$("#formPaystationChildren ul.fullList").html("<li>"+ loadObj +"</li>");
		$("#formPaystationChildren ul.selectedList").html("<li>"+ loadObj +"</li>");
		payStationSelectList($("#contentID").val(), "", "", false, "");
		$("#routeDetails").removeClass("addForm").addClass("form editForm");
		displayCallBack = function(){ $("#formRouteName").trigger('focus'); } }
	snglSlideFadeTransition("show", $("#routeDetails"), displayCallBack);
	$(document.body).scrollTop(0); }

//--------------------------------------------------------------------------------------

function resetRouteForm(event)
{
	var hideCallBack = function(){ 
		var ajaxNewRoute = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxNewRoute.onreadystatechange = function()
		{
			if (ajaxNewRoute.readyState==4 && ajaxNewRoute.status == 200){
				//$("#routeView").hide();
				$("#formRouteType").autoselector("reset");
				$(".routePaystationLabel").removeClass("active");
				$(".routePaystationLabel, .routePaystationSelectAllBtn").show();
	
				var childFullList = "";
				var childSelectedList = "";
				var showOptions = false;
				var selectedCount = 0;
				var allSelected = true;
				var colPlacement = 0;
				var childData = JSON.parse(ajaxNewRoute.responseText);
	//console.log(ajaxNewRoute.responseText);
				$("#formPaystationChildren ul.fullList").html("<li>"+ loadObj +"</li>");
				$("#formPaystationChildren ul.selectedList").html("");
				payStationSelectList("","","",false,"");

				$("#formPaystationChildren #psSelectionsAll").parents("label").show();
				clearErrors();
				document.getElementById("routeSettings").reset();
				$("#psSelections").val("")
				$("#contentID, #formRouteID").val("");
				$("#actionFlag").val("0")
				$("#locationRouteAC").val("");
				$(document.body).scrollTop(0); 
				$("#routeDetails").removeClass("editForm").addClass("form addForm");
				testFormEdit($("#routeSettings")); 
				snglSlideFadeTransition("show", $("#routeDetails"), function(){ $("#formRouteName").trigger('focus'); }); }
			else if(ajaxNewRoute.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load Company Locations and Paystations
			}
		};
		ajaxNewRoute.open("GET",LOC.formRoutes+"?"+document.location.search.substring(1),true);
		ajaxNewRoute.send(); }

	if($("#routeDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#routeDetails"), hideCallBack); }
	else{ hideCallBack(); }
}

//--------------------------------------------------------------------------------------

function loadRoute(routeID, action)
{
	var hideCallBack = '';
	if($("#contentID").val() === routeID){
		if(action == "edit"){
			testFormEdit($("#routeSettings"));
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#routeDetails"), $("#routeDetails"), hideCallBack);
			payStationSelectList(routeID, "", "", false, ""); }
		else if(action == "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#routeDetails"), $("#routeDetails"), hideCallBack); } }
	else { 
		var $this = $("#route_"+routeID);
		if($("#routeList").find("li.selected").length){ $("#routeList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");
		hideCallBack = function(){ 
			var ajaxLoadRoute = GetHttpObject();
			ajaxLoadRoute.onreadystatechange = function()
			{
				if (ajaxLoadRoute.readyState==4 && ajaxLoadRoute.status == 200){
					showRouteDetails(ajaxLoadRoute, action);
				} else if(ajaxLoadRoute.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadRoute.open("GET",LOC.viewRoute+"?routeID="+routeID+"&"+document.location.search.substring(1),true);
			ajaxLoadRoute.send(); } 
			
		if($("#routeDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#routeDetails"), hideCallBack); }
		else{ hideCallBack(); } } }

//--------------------------------------------------------------------------------------

function deleteRoute(routeID)
{
	var ajaxDeleteRoute = GetHttpObject();
	ajaxDeleteRoute.onreadystatechange = function()
	{
		if (ajaxDeleteRoute.readyState==4 && ajaxDeleteRoute.status == 200){
			if(ajaxDeleteRoute.responseText == "true") { 
				noteDialog(routeDeleteMsg);
				var newLocation = location.pathname+"?"+filterQuerry;
				setTimeout(function(){window.location = newLocation}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteRoute.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteRoute.open("GET",LOC.deleteRoute+"?routeID="+routeID+"&"+document.location.search.substring(1),true);
	ajaxDeleteRoute.send();
}

function verifyDeleteRoute(routeID)
{
	var req = GetHttpObject();
	req.onreadystatechange = function(responseFalse, displayedError, displayedMessage) {
		if(req.readyState == 4) {
			if((!responseFalse) && (req.status == 200)) {
				if(displayedMessage) {
					deleteRoute(routeID);
				}
				else {
					var deleteRouteButtons = {
						"btn1": {
							text : deleteBtn,
							click : function() { deleteRoute(routeID); $(this).scrollTop(0); $(this).dialog( "close" ); }
						},
						"btn2": {
							text : cancelBtn,
							click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
						}
					};
					
					if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
					$("#messageResponseAlertBox")
							.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteRouteMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteRouteButtons });
					
					btnStatus(deleteBtn);
				}
			}
		}
	};
	
	req.open("GET", LOC.verifyDeleteRoute+"?routeID="+routeID+"&"+document.location.search.substring(1), true);
	req.send();
}

//--------------------------------------------------------------------------------------

function hideSelected($this){
	snglSlideFadeTransition("hide", $("#routeDetails")); 
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); } 
	else{ if($("#routeList").find("li.selected").length){ $("#routeList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function newRouteForm(event){
	$(".pageItems").find("li.selected").each(function(){ $(this).removeClass("selected"); });
	resetRouteForm(event); }

//--------------------------------------------------------------------------------------

function routeSelectionCheck(routeID, eventAction){
	var itemID = routeID.split("_"),
		tempID = itemID[1],
		$this = $("#route_"+tempID);
	if(eventAction == "hide"){ 
		var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
		crntPageAction = "";
		history.pushState(null, null, location.pathname+"?"+cleanQuerryString);
		hideSelected($this); }
	else { 
		if($("#contentID").val() != tempID || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: tempID, action: eventAction };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action);
			loadRoute(tempID, eventAction); } } }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popAction(tempItemID, tempEvent, stateObj){
	if(stateObj === null || (tempEvent === "display" && tempItemID === "New")){ hideSelected(); }
	else if(tempEvent === "Add"){ newRouteForm(); }
	else { loadRoute(tempItemID, tempEvent);  } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

function routes(tempID, tempEvent)
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	popAction(tempItemID, tempEvent, stateObj); }

if(!popStateEvent && (tempEvent !== undefined && tempEvent !== '')){ popAction(tempID, tempEvent); }
///////////////////////////////////////////////////	msgResolver.mapAttribute("routeName", "#formRouteName");

	msgResolver.mapAttribute("routeType", "#formRouteType");
	msgResolver.mapAttribute("PSList", "#routePSSelectedList");

	$(document.body).on("click", "#routeList > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var itemID = $(this).attr("id");
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ routeSelectionCheck(itemID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ routeSelectionCheck(itemID, actionEvent); } });

	$("#routeListArea").on("click", "#routeList * .view", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		routeSelectionCheck(itemID, "display"); });
	
	$("#routeListArea").on("click", "#routeList * .edit", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id");
		routeSelectionCheck(itemID, "edit");
	});
	
	$("#routeListArea").on("click", "#routeList * .delete", function(event){
		event.preventDefault();
		var itemID = $(this).attr("id").split("_");
		verifyDeleteRoute(itemID[1]);
	});
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var routeID = $("#contentID").val();
		verifyDeleteRoute(routeID);
	});
	
	$("#btnAddRoute").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{ 
			var stateObj = { action: "Add" };
			var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "all");
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?"+cleanQuerryString+"&itemID=New&pgActn="+stateObj.action);
			newRouteForm(event); }
	});

	$("#routeListArea").on("click", "#routeListHeader * .sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortRouteList = GetHttpObject();
		ajaxSortRouteList.onreadystatechange = function()
		{
			if (ajaxSortRouteList.readyState==4 && ajaxSortRouteList.status == 200){
				$("#routeListArea").html(ajaxSortRouteList.responseText);
				if($("#rTypeAC").autoselector("getSelectedValue") !== "showAll"){ $("#routeList").addClass($("#rTypeAC").autoselector("getSelectedValue")); }
				scrollListWidth(["routeList"]);
			} else if(ajaxSortRouteList.readyState==4){
				if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
				$('#messageResponseAlertBox').dialog(alertNoticeOptions).html(sortFail);
			}
		};
		ajaxSortRouteList.open("GET",LOC.sortRoutes+"?sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortRouteList.send();
	});
	
	$("#formRouteType").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"data": JSON.parse($("#formRouteTypeData").text()) });

	$("#routeSettings").on("click", ".save", function(event)
	{
		event.preventDefault();
		var routeID = $("#routeSettings").find("#formRouteID").val();
		var params = $("#routeSettings").serialize();
		var ajaxSaveRoute = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxSaveRoute.onreadystatechange = function(){
			if (ajaxSaveRoute.readyState==4 && ajaxSaveRoute.status == 200){
				var response = ajaxSaveRoute.responseText.split(":");
				if(response[0] == "true"){
					noteDialog(routeSavedMsg);
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "pgActn"]),
						newLocation = location.pathname+"?"+cleanQuerryString+"&pgActn="+crntPageAction;
					setTimeout(function(){window.location = newLocation}, 500); } }
			else if(ajaxSaveRoute.readyState==4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveRoute.open("POST",LOC.saveRoute,true);
		ajaxSaveRoute.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveRoute.setRequestHeader("Content-length", params.length);
		ajaxSaveRoute.setRequestHeader("Connection", "close");
		ajaxSaveRoute.send(params);

	});
	
	$("#routeSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
	
	//Route Type AUTO COMPLETE STANDARD
	$("#rTypeAC").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": true,
		"blockOnEdit": true,
		"data": rTypeObj })
	.on("itemSelected", function(event, ui) {
		var slctValue = $("#rTypeAC").autoselector("getSelectedValue");
		if(slctValue == ""){ slctValue = "showAll"; }
		if($(".sectionContent").attr("id") == "DefinedRoutes" && slctValue != ""){  filterRouteList(slctValue) }// Location Detail Page Outcome.
		if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue }
		$(this).trigger('blur');
		return false; })
	.on("change", function(){
		if($("#rTypeAC").val() === ""){ filterRouteList("showAll"); }});
		
	$("#routeView").on("click", ".pin", function(event){
		event.preventDefault();
		var routeID = $("#contentID").val();
		window.location = "/secure/settings/locations/payStationPlacement.html?routeID="+routeID+"&"+document.location.search.substring(1);
	});
}
