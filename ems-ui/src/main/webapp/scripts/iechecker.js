//--------------------------------------------------------------------------------------

var isIE = (function(undefined) {
	var cache = {}, $testSpace;
	
	return function(version) {
		var result;
		if(/*@cc_on!@*/true) {
			result = false;
		}
		else {
			if(!$testSpace) {
				$testSpace = $("<b>").appendTo("body");
			}
			
			var key = "IE " + (version || '');
			if(typeof cache[key] === "undefined") {
				cache[key] = ($testSpace.html("<!--[if " + key + "]><b></b><![endif]-->").find("b").length > 0);
			}
			
			result = cache[key];
		}
		
		return result;
	};
})();

//--------------------------------------------------------------------------------------

var isIE10 = (function(undefined) {
	var result = false;
	/*@cc_on
	   if (/^10/.test(@_jscript_version)) {
		  result = true;
	   }
	@*/
	return result;
});

//--------------------------------------------------------------------------------------