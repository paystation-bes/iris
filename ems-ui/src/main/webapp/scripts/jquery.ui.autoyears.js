(function($, undefined) {
	var rangesHash = {};
	
	$.widget("ui.autoyears", $.ui.autoselector, {
		options: {
			"minLimit": -1,
			"maxLimit": -1,
			"distanceFromCurrentYear": 50,
			"defaultValue": "",
			"isComboBox": true,
			"data": [],
			"persistToHidden": false,
			"invalidSelectionMessage": function() {
				return (commonMessages && commonMessages.invalidYearSelection) ? commonMessages.invalidYearSelection : "Invalid Year !";
			}
		},
		"_filterSuggestions": function(termStr) {
			var result = null;
			
			var options = this.options;
			var currYear = (new Date()).getFullYear();
			var termLen = termStr.length;
			if(termLen > 3) {
				result = [];
				if(termLen === 4) {
					var year = parseInt(termStr, 10);
					if(((options.minLimit <= 0) || (year >= options.minLimit)) && ((options.maxLimit <= 0) || (year <= options.maxLimit)) && (Math.abs(year - currYear) <= options.distanceFromCurrentYear)) {
						result.push({
							"label": termStr,
							"value": year
						});
					}
				}
			}
			else {
				var rangeId = termStr + "[" + options.minLimit + ", " + options.maxLimit + ", " + options.distanceFromCurrentYear + "]";
				result = rangesHash[rangeId];
				if(!result) {
					var currYearStr = currYear + "";
					if(termLen <= 0) {
						result = this._generateYears([], currYear, -9, 10, options);
					}
					else if(termLen === 3) {
						result = this._generateThreeDigitSuggestion(termStr, currYear, options);
					}
					else if(termStr === currYearStr.substring(0, termLen)) {
						result = this._generateYears([], currYear, -9, 10, options);
					}
					else if(termLen === 1) {
						result = this._generateSingleDigitSuggestion(termStr, currYear, options);
					}
					else {
						result = this._generateTwoDigitSuggestion(termStr, currYear, options);
					}
					
					rangesHash[rangeId] = result;
				}
			}
			
			return result;
		},
		"_generateSingleDigitSuggestion": function(digitStr, refYear, options) {
			var result = [];
			
			var currCentury = Math.floor(refYear / 100);
			var decade = parseInt(digitStr, 10) * 10;
			
			var year = ((currCentury - 1) * 100) + decade;
			if(Math.abs(year - refYear) <= options.distanceFromCurrentYear) {
				this._generateYears(result, year, 0, 10, options);
			}
			
			year = (currCentury * 100) + decade;
			if(Math.abs(year - refYear) <= options.distanceFromCurrentYear) {
				this._generateYears(result, year, 0, 10, options);
			}
			
			return result;
		},
		"_generateTwoDigitSuggestion": function(digitStr, refYear, options) {
			var result = [];
			
			var twoDigitYear = parseInt(digitStr, 10);
			var currCentury = Math.floor(refYear / 100);
			
			var anotherYear = (twoDigitYear * 100) + (refYear % 100);
			if(Math.abs(refYear - anotherYear) <= options.distanceFromCurrentYear) {
				this._generateYears(result, anotherYear, 0, 20, options);
			}
			else {
				var year = ((currCentury - 1) * 100) + twoDigitYear;
				if(((options.minLimit <= 0) || (year >= options.minLimit)) && ((options.maxLimit <= 0) || (year <= options.maxLimit)) && (Math.abs(year - refYear) <= options.distanceFromCurrentYear)) {
					result.push({
						"label": year,
						"value": year
					});
				}
				
				year = (currCentury * 100) + twoDigitYear;
				if(((options.minLimit <= 0) || (year >= options.minLimit)) && ((options.maxLimit <= 0) || (year <= options.maxLimit)) && (Math.abs(year - refYear) <= options.distanceFromCurrentYear)) {
					result.push({
						"label": year,
						"value": year
					});
				}
			}
			
			return result;
		},
		"_generateThreeDigitSuggestion": function(digitStr, refYear, options) {
			var result = [];
			
			var threeDigitYear = parseInt(digitStr, 10);
			var year = threeDigitYear * 10;
			if(Math.abs(year - refYear) <= options.distanceFromCurrentYear) {
				this._generateYears(result, year, 0, 20, options);
			}
			else {
				var firstDigit = Math.floor(refYear / 1000);
				year = ((firstDigit - 1) * 1000) + threeDigitYear;
				if(Math.abs(year - refYear) <= options.distanceFromCurrentYear) {
					this._generateYears(result, year, 0, 20, options);
				}
				else {
					year = (firstDigit * 1000) + threeDigitYear;
					if(Math.abs(year - refYear) <= options.distanceFromCurrentYear) {
						this._generateYears(result, year, 0, 20, options);
					}
				}
			}
			
			return result;
		},
		"_generateYears": function(result, refYear, incBegin, incEnd, options) {
			var year;
			var inc = incBegin;
			while(inc < incEnd) {
				year = (refYear + inc) + "";
				if(((options.minLimit <= 0) || (year >= options.minLimit)) && ((options.maxLimit <= 0) || (year <= options.maxLimit))) {
					result.push({
						"label": year,
						"value": year
					});
				}
				
				++inc;
			}
			
			return result;
		},
		"resetYear": function() {
			this.setYear(this.options.defaultValue);
		},
		"getYear": function() {
			return this.element.val();
		},
		"setYear": function(yearIdx) {
			this.element.val(yearIdx);
		}
	});
}(jQuery));