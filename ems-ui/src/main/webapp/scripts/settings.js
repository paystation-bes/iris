// JavaScript Document

var unifiedRateNames = new Array(), optsTime = { "militaryMode": false }, noContactsHTML = "", ogListHeight = "", formErrors = new Array(), $uploadForm = "";

//--------------------------------------------------------------------------------------

function formErrorObjects(id, message)
{
	this.id=id;
	this.message=message;
}

//--------------------------------------------------------------------------------------

function showXbpRateDetails(xbpRateDetails, action)
{
	//Process JSON
	var xbpRateData =	JSON.parse(xbpRateDetails.responseText);
	$("#actionFlag").val("1")
	$("#contentID, #formRateID").val(xbpRateData.ebpRatePermissionEditForm.extensibleRateId);
	$("#xbpItemName, #xbpRateNameDisplay").html(xbpRateData.ebpRatePermissionEditForm.name);
	$("#formRateName").val(xbpRateData.ebpRatePermissionEditForm.name);

	var startTime = xbpRateData.ebpRatePermissionEditForm.startTime.split(":");
	var endTime = xbpRateData.ebpRatePermissionEditForm.endTime.split(":");

	if((endTime[0] == "00" || endTime[0] == 0) && (endTime[1] == "00" || endTime[1] == 0)){ endTime[0] = 24; }
	if(startTime[0] < 10){startTime[0] = "0"+startTime[0]}
	if(startTime[1] < 10){startTime[1] = "0"+startTime[1]}
	if(endTime[0] < 10){endTime[0] = "0"+endTime[0]}
	if(endTime[1] < 10){endTime[1] = "0"+endTime[1]}

	var formatedStart = startTime[0]+":"+startTime[1];
	var formatedEnd = endTime[0]+":"+endTime[1];

	$("#formItemStartTime").autominutes("setTime", formatedStart);
	$("#xbpStartTime").html($("#formItemStartTime").val());
	$("#formItemEndTime").autominutes("setTime", formatedEnd);
	$("#xbpEndTime").html($("#formItemEndTime").val());
		
	if(xbpRateData.ebpRatePermissionEditForm.sunday == true){ 
		$("#sundayCheckDisplay").addClass("activatedService"); $("#sundayCheck").addClass("checked"); $("#sundayCheckHidden").val("true"); } 
	else{ 
		$("#sundayCheckDisplay").removeClass("activatedService"); $("#sundayCheck").removeClass("checked"); $("#sundayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.monday == true){ 
		$("#mondayCheckDisplay").addClass("activatedService"); $("#mondayCheck").addClass("checked"); $("#mondayCheckHidden").val("true"); } 
	else{ 
		$("#mondayCheckDisplay").removeClass("activatedService"); $("#mondayCheck").removeClass("checked"); $("#mondayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.tuesday == true){ 
		$("#tuesdayCheckDisplay").addClass("activatedService"); $("#tuesdayCheck").addClass("checked"); $("#tuesdayCheckHidden").val("true");} 
	else{ 
		$("#tuesdayCheckDisplay").removeClass("activatedService"); $("#tuesdayCheck").removeClass("checked"); $("#tuesdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.wednesday == true){ 
		$("#wednesdayCheckDisplay").addClass("activatedService"); $("#wednesdayCheck").addClass("checked"); $("#wednesdayCheckHidden").val("true"); } 
	else{ 
		$("#wednesdayCheckDisplay").removeClass("activatedService"); $("#wednesdayCheck").removeClass("checked"); $("#wednesdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.thursday == true){ 
		$("#thursdayCheckDisplay").addClass("activatedService"); $("#thursdayCheck").addClass("checked"); $("#thursdayCheckHidden").val("true"); } 
	else{ 
		$("#thursdayCheckDisplay").removeClass("activatedService"); $("#thursdayCheck").removeClass("checked"); $("#thursdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.friday == true){ 
		$("#fridayCheckDisplay").addClass("activatedService"); $("#fridayCheck").addClass("checked"); $("#fridayCheckHidden").val("true"); } 
	else{ 
		$("#fridayCheckDisplay").removeClass("activatedService"); $("#fridayCheck").removeClass("checked"); $("#fridayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.saturday == true){ $("#saturdayCheckDisplay").addClass("activatedService"); $("#saturdayCheck").addClass("checked"); $("#saturdayCheckHidden").val("true"); } 
	else{ $("#saturdayCheckDisplay, #saturdayCheck").removeClass("checked"); $("#saturdayCheckHidden").val("false"); }
	
	var rateValue = xbpRateData.ebpRatePermissionEditForm.rateValue/100;
	var serviceFee = xbpRateData.ebpRatePermissionEditForm.rateServiceFee/100;
	$("#xbpSiPrice").html(Highcharts.numberFormat(rateValue, 2));
	$("#formRateValue").val(Highcharts.numberFormat(rateValue, 2)).removeClass("smplValue");
	$("#xbpSiFee").html(Highcharts.numberFormat(serviceFee,2));
	$("#formRateFee").val(Highcharts.numberFormat(serviceFee,2)).removeClass("smplValue");
	$("#xbpSiExtension").html(xbpRateData.ebpRatePermissionEditForm.minExtensionMinutes);
	$("#formRateMinExt").val(xbpRateData.ebpRatePermissionEditForm.minExtensionMinutes).removeClass("smplValue")
	
	//Display Method
	clearErrors();
	if(action == "display"){ $("#mainContent").removeClass("edit"); $("#xbpDetails").removeClass("form editForm addForm"); snglSlideFadeTransition("show", $("#xbpDetails")); }
	else if(action == "edit"){ $("#xbpDetails").removeClass("addForm").addClass("form editForm"); snglSlideFadeTransition("show", $("#xbpDetails"), function(){ $("#formRateName").trigger('focus'); }); }
	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------

function fadeOutTaskList(){ $("#migrationTaskListArea").slideUp('slow').animate({ opacity: 0 },{ queue: false, duration: 'slow' }); }

//--------------------------------------------------------------------------------------

function showXbpPolicyDetails(xbpPolicyDetails, action)
{
	//Process JSON
	var xbpRateData =	JSON.parse(xbpPolicyDetails.responseText);
//console.log(xbpPolicyDetails.responseText);
	$("#actionFlag").val("1")
	$("#contentID, #formPolicyID").val(xbpRateData.ebpRatePermissionEditForm.parkingPermissionId);
	$("#xbpItemName, #xbpPolicyNameDisplay").html(xbpRateData.ebpRatePermissionEditForm.name);
	$("#formPolicyName").val(xbpRateData.ebpRatePermissionEditForm.name);
	
	var startTime = xbpRateData.ebpRatePermissionEditForm.startTime.split(":");
	var endTime = xbpRateData.ebpRatePermissionEditForm.endTime.split(":");
	
	
	if((endTime[0] == "00" || endTime[0] == 0) && (endTime[1] == "00" || endTime[1] == 0)){ endTime[0] = 24; }
	if(startTime[0] < 10){startTime[0] = "0"+startTime[0]}
	if(startTime[1] < 10){startTime[1] = "0"+startTime[1]}
	if(endTime[0] < 10){endTime[0] = "0"+endTime[0]}
	if(endTime[1] < 10){endTime[1] = "0"+endTime[1]}

	var formatedStart = startTime[0]+":"+startTime[1];
	var formatedEnd = endTime[0]+":"+endTime[1];

	$("#formItemStartTime").autominutes("setTime", formatedStart);
	$("#xbpStartTime").html($("#formItemStartTime").val());
	$("#formItemEndTime").autominutes("setTime", formatedEnd);
	$("#xbpEndTime").html($("#formItemEndTime").val());

	if(xbpRateData.ebpRatePermissionEditForm.sunday == true){ 
		$("#sundayCheckDisplay").addClass("activatedService"); $("#sundayCheck").addClass("checked"); $("#sundayCheckHidden").val("true"); } 
	else{ 
		$("#sundayCheckDisplay").removeClass("activatedService"); $("#sundayCheck").removeClass("checked"); $("#sundayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.monday == true){ 
		$("#mondayCheckDisplay").addClass("activatedService"); $("#mondayCheck").addClass("checked"); $("#mondayCheckHidden").val("true"); } 
	else{ 
		$("#mondayCheckDisplay").removeClass("activatedService"); $("#mondayCheck").removeClass("checked"); $("#mondayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.tuesday == true){ 
		$("#tuesdayCheckDisplay").addClass("activatedService"); $("#tuesdayCheck").addClass("checked"); $("#tuesdayCheckHidden").val("true");} 
	else{ 
		$("#tuesdayCheckDisplay").removeClass("activatedService"); $("#tuesdayCheck").removeClass("checked"); $("#tuesdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.wednesday == true){ 
		$("#wednesdayCheckDisplay").addClass("activatedService"); $("#wednesdayCheck").addClass("checked"); $("#wednesdayCheckHidden").val("true"); } 
	else{ 
		$("#wednesdayCheckDisplay").removeClass("activatedService"); $("#wednesdayCheck").removeClass("checked"); $("#wednesdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.thursday == true){ 
		$("#thursdayCheckDisplay").addClass("activatedService"); $("#thursdayCheck").addClass("checked"); $("#thursdayCheckHidden").val("true"); } 
	else{ 
		$("#thursdayCheckDisplay").removeClass("activatedService"); $("#thursdayCheck").removeClass("checked"); $("#thursdayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.friday == true){ 
		$("#fridayCheckDisplay").addClass("activatedService"); $("#fridayCheck").addClass("checked"); $("#fridayCheckHidden").val("true"); } 
	else{ 
		$("#fridayCheckDisplay").removeClass("activatedService"); $("#fridayCheck").removeClass("checked"); $("#fridayCheckHidden").val("false"); }

	if(xbpRateData.ebpRatePermissionEditForm.saturday == true){ 
		$("#saturdayCheckDisplay").addClass("activatedService"); $("#saturdayCheck").addClass("checked"); $("#saturdayCheckHidden").val("true"); } 
	else{ $("#saturdayCheckDisplay, #saturdayCheck").removeClass("checked"); $("#saturdayCheckHidden").val("false"); }
	
	var policyType = xbpRateData.ebpRatePermissionEditForm.parkingPermissionTypeId;
	$("#xbpPolicy").html(permissionType[policyType]);
	if(policyType == 2){ 
		$("#formPermissionStatus").val("unlimited"); 
		$(".radio").each(function(index, element){ 
			var radioData = $(this).attr("id").split(":"); if(radioData[1] == "unlimited"){$(this).addClass("checked")}else{$(this).removeClass("checked");} 
		}); 
	} else if(policyType == 1) { 
		$("#formPermissionStatus").val("timelimited"); 
		$(".radio").each(function(index, element){ 
			var radioData = $(this).attr("id").split(":"); if(radioData[1] == "timelimited"){$(this).addClass("checked")}else{$(this).removeClass("checked");} 
		});
	}
	
	if(policyType == 2){ $(".maxDurDisplay").hide(); $("#formMaxDur").val("120").addClass("smplValue"); }
	else if(policyType == 1){ $(".maxDurDisplay").show(); $("#xbpDuration").html(xbpRateData.ebpRatePermissionEditForm.maxDurationMinutes).removeClass("smplValue"); $("#formMaxDur").val(xbpRateData.ebpRatePermissionEditForm.maxDurationMinutes).attr("disabled", false)}
	
	clearErrors();
	//Display Method
	if(action == "display"){ $("#mainContent").removeClass("edit"); $("#xbpDetails").removeClass("form editForm addForm"); snglSlideFadeTransition("show", $("#xbpDetails")); }
	else if(action == "edit"){ $("#xbpDetails").removeClass("addForm").addClass("form editForm"); snglSlideFadeTransition("show", $("#xbpDetails"), function(){ $("#formPolicyName").trigger('focus'); if(policyType == 2){$("#xbpMaxDurationArea").addClass("disabledArea");} }); }
	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------

function resetXbpForm(itemType, day, startTime, endTime)
{ 
	var hideCallBack = '',
		showCallBack = '';
	if(itemType == "rate"){ 
		hideCallBack = function(){
			$("#xbpDetails").removeClass("editForm").addClass("form addForm");
			$("#actionFlag").val("0")
			$("#contentID, #formRateID").val("");
			$("#formRateName").val("");
			$("#sundayCheckDisplay").addClass("activatedService");
			$("#sundayCheck").addClass("checked");
			$("#sundayCheckHidden").val("true")
			$("#mondayCheckDisplay").addClass("activatedService");
			$("#mondayCheck").addClass("checked");
			$("#mondayCheckHidden").val("true");
			$("#tuesdayCheckDisplay").addClass("activatedService"); 
			$("#tuesdayCheck").addClass("checked"); 
			$("#tuesdayCheckHidden").val("true");
			$("#wednesdayCheckDisplay").addClass("activatedService"); 
			$("#wednesdayCheck").addClass("checked"); 
			$("#wednesdayCheckHidden").val("true");
			$("#thursdayCheckDisplay").addClass("activatedService"); 
			$("#thursdayCheck").addClass("checked"); 
			$("#thursdayCheckHidden").val("true");
			$("#fridayCheckDisplay").addClass("activatedService"); 
			$("#fridayCheck").addClass("checked"); 
			$("#fridayCheckHidden").val("true");
			$("#saturdayCheckDisplay").addClass("activatedService"); 
			$("#saturdayCheck").addClass("checked"); 
			$("#saturdayCheckHidden").val("true");
			$("#formRateValue").val("1.00").addClass("smplValue");
			$("#formRateFee").val("0.25").addClass("smplValue");
			$("#formRateMinExt").val("15").addClass("smplValue")
			clearErrors(); } }
	else {
		hideCallBack = function(){
			$("#xbpDetails").removeClass("editForm").addClass("form addForm");
			$("#actionFlag").val("0")
			$("#contentID, #formPolicyID").val("");
			$("#formPolicyName").val("");
			$("#sundayCheckDisplay, #sundayCheck").addClass("checked"); 
			$("#sundayCheckHidden").val("true")
			$("#mondayCheckDisplay, #mondayCheck").addClass("checked"); 
			$("#mondayCheckHidden").val("true");
			$("#tuesdayCheckDisplay, #tuesdayCheck").addClass("checked"); 
			$("#tuesdayCheckHidden").val("true");
			$("#wednesdayCheckDisplay, #wednesdayCheck").addClass("checked"); 
			$("#wednesdayCheckHidden").val("true");
			$("#thursdayCheckDisplay, #thursdayCheck").addClass("checked"); 
			$("#thursdayCheckHidden").val("true");
			$("#fridayCheckDisplay, #fridayCheck").addClass("checked"); 
			$("#fridayCheckHidden").val("true");
			$("#saturdayCheckDisplay, #saturdayCheck").addClass("checked"); 
			$("#saturdayCheckHidden").val("true");
			$("#formPermissionStatus").val("unlimited"); 
			$("#xbpParkPolicy").find(".radio").each(function(index, element){ 
				var radioData = $(this).attr("id").split(":"); if(radioData[1] == "unlimited"){$(this).addClass("checked")}else{$(this).removeClass("checked");} 
			}); 
			$("#formMaxDur").val("120").addClass("smplValue").attr("disabled", true);
			clearErrors(); } }

	if($("#xbpDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#xbpDetails"), hideCallBack); }
	else{ hideCallBack(); } 

	// day comes as string were index is an integer so !== will not work.
	if(day && day != ""){ $(".daySelection").find("label.checkboxLabel").each(function(index, element) { if(index != day){ $(this).trigger('click'); } }); }

	showCallBack = function(){
		$("#formRateName").trigger('focus');
		if(startTime && startTime !== ""){ $("#formItemStartTime").autominutes("setTime", startTime.replace(" hr", "")); }
		else{ $("#formItemStartTime").autominutes("setTime", "00:00"); }
		if(endTime && endTime !== ""){ $("#formItemEndTime").autominutes("setTime", endTime.replace(" hr", "")); }
		else{ $("#formItemEndTime").autominutes("setTime", "24:00"); } }

	snglSlideFadeTransition("show", $("#xbpDetails"), showCallBack );
	
	$(document.body).scrollTop(0); }

//--------------------------------------------------------------------------------------

function showPayStationDetails(payStationDetails, payStationID, action, tabID)
{
	//Process JSON
	var posStatusMsg = "",
		showCallBack = "",
		psData = JSON.parse(payStationDetails.responseText);
	if(psData.payStationDetails.active === false || psData.payStationDetails.isDecommissioned){
		if(psData.payStationDetails.isDecommissioned){ tempStatusMsg = decommissionedMsg; }
		else { tempStatusMsg = deactivatedMsg; }
		var posActiveState = " <span class='statusNote'>[ "+tempStatusMsg;
		var posVisibility = (psData.payStationDetails.hidden === true)? " - <em>"+hiddenMsg+"</em> ]</span>" : " ]</span>";
		posStatusMsg = posActiveState +  posVisibility; }
	
	var posLinuxStatus = (psData.payStationDetails.isLinux)? " <strong>"+linuxPOSMsg+"</strong>" : "";
		posStatusMsg = posLinuxStatus + posStatusMsg;
	
	psType = psData.payStationDetails.mapInfo.payStationType;
	if(psType === 9){ $("#alertsBtn").hide(); }
	else{ $("#alertsBtn").show(); }
	$("#mapHolder").html("");
	$("#contentID, #formPayStationID").val(payStationID);
	$("#paystationName").html(psData.payStationDetails.name + " <span class='note'>"+psData.payStationDetails.serialNumber + posStatusMsg +"</span>");
	$("#formPayStationName").val(psData.payStationDetails.name);
	$("#payStationTypeImg").children("img").attr("src", imgLocalDir+"paystation_"+payStationIcn[psType].img[0]+"Lrg.jpg");
		
	if(psData.payStationDetails.alertCount > 0){ $("#activeAlerts").html(psData.payStationDetails.alertCount).show(); }
	else{ $("#activeAlerts").html("").hide(); }
	
	var locationIDValue = (psData.payStationDetails.locationRandomId)? psData.payStationDetails.locationRandomId : null;
	$("#formPayStationLocation")
		.autoselector({"defaultValue": locationIDValue})
		.autoselector("setSelectedValue", locationIDValue, true);
	
//	$("#formPayStationLocationLabel").html(psData.payStationDetails.locationName);
	$("#formPayStationStatus").val(psData.payStationDetails.active);
	if(psData.payStationDetails.active == true){ $("#btnPayStationDeactivate").show(); $("#btnPayStationActivate").hide(); $("#formHidePayStation").hide(); } 
	else { $("#btnPayStationDeactivate").hide(); $("#btnPayStationActivate").show(); $("#formHidePayStation").show();}

	if(psData.payStationDetails.isDecommissioned){ $("#paystationActivation").hide(); $("#formHidePayStation").show();}
	else{ $("#paystationActivation").show(); }

	
	$("#hidePayStationCheckHidden").val(psData.payStationDetails.hidden);
	if(psData.payStationDetails.hidden == true){ $("#hidePayStationCheck").addClass("checked"); } 
	else { $("#hidePayStationCheck").removeClass("checked"); }
	clearErrors();
	loadPosTab(tabID, payStationID, psData, true);

	if(action == "display"){
		showCallBack = function(){
			if(psType === 9){ initMapStatic("emptyMap"); }
			else {
				var	payStationMarkers = new Array();
				if(psData.payStationDetails.mapInfo.latitude){
					payStationMarkers.push(new mapMarker(
						psData.payStationDetails.mapInfo.latitude, 
						psData.payStationDetails.mapInfo.longitude, 
						psData.payStationDetails.mapInfo.name, 
						psType,'',
						psData.payStationDetails.mapInfo.severity,'','','','','','','' ));
					initMapStatic(payStationMarkers); } 
				else { initMapStatic("noData"); } } }
		$("#mainContent").removeClass("edit");
		$("#paystationDetails").removeClass("form editForm")
		snglSlideFadeTransition("show", $("#paystationDetails"), showCallBack);	}
	else if(action == "edit") { 
		testFormEdit($("#payStationSettings")); 
		$("#paystationDetails").addClass("form editForm");
		snglSlideFadeTransition("show", $("#paystationDetails"), function(){ $("#formPayStationName").trigger('focus'); }); }
	$(document.body).scrollTop(0);
}

//--------------------------------------------------------------------------------------
//AJAX CALLS

function loadRate(rateID, action)
{
	var callBackFctn = function(){ 
		var ajaxLoadXbpRate = GetHttpObject();
		ajaxLoadXbpRate.onreadystatechange = function()
		{
			if (ajaxLoadXbpRate.readyState==4 && ajaxLoadXbpRate.status == 200){
				showXbpRateDetails(ajaxLoadXbpRate, action);
			} else if(ajaxLoadXbpRate.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load location details
			}
		};
		ajaxLoadXbpRate.open("GET","/secure/settings/locations/extendByPhone/viewRate.html?rateId="+rateID+"&"+document.location.search.substring(1),true);
		ajaxLoadXbpRate.send();	}
	snglSlideFadeTransition("hide", $("#xbpDetails"), callBackFctn);
}

function deleteRate(rateID)
{
	var ajaxDeleteRate = GetHttpObject();
	ajaxDeleteRate.onreadystatechange = function()
	{
		if (ajaxDeleteRate.readyState==4 && ajaxDeleteRate.status == 200){
			if(ajaxDeleteRate.responseText == "true") { 
				noteDialog(rateDeleteMsg);
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "itemID");
				var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry;
				setTimeout(function(){window.location = newLocation}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteRate.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteRate.open("GET","/secure/settings/locations/extendByPhone/deleteRate.html?id="+rateID+"&"+document.location.search.substring(1),true);
	ajaxDeleteRate.send();
}

function verifyDeleteRate(rateID)
{
	var 	deleteRateButtons = {};
		deleteRateButtons.btn1 = {
			text : deleteBtn,
			click : 	function() { deleteRate(rateID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deleteRateButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteRateMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteRateButtons });
	btnStatus(deleteBtn);
}


//--------------------------------------------------------------------------------------

function loadPolicy(policyID, action)
{
	var callBackFctn = function(){ 
		var ajaxLoadXbpPolicy = GetHttpObject();
		ajaxLoadXbpPolicy.onreadystatechange = function()
		{
			if (ajaxLoadXbpPolicy.readyState==4 && ajaxLoadXbpPolicy.status == 200){
				showXbpPolicyDetails(ajaxLoadXbpPolicy, action);
			} else if(ajaxLoadXbpPolicy.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load location details
			}
		};
		ajaxLoadXbpPolicy.open("GET","/secure/settings/locations/extendByPhone/viewPolicy.html?policyId="+policyID+"&"+document.location.search.substring(1),true);
		ajaxLoadXbpPolicy.send();	}
	snglSlideFadeTransition("hide", $("#xbpDetails"), callBackFctn);
}

function deletePolicy(policyID)
{
	var ajaxDeletePolicy = GetHttpObject();
	ajaxDeletePolicy.onreadystatechange = function()
	{
		if (ajaxDeletePolicy.readyState==4 && ajaxDeletePolicy.status == 200){
			if(ajaxDeletePolicy.responseText == "true") { 
				noteDialog(policyDeleteMsg);
				var cleanQuerryString = scrubQueryString(document.location.search.substring(1), "itemID");
				var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry;
				setTimeout(function(){window.location = newLocation}, 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeletePolicy.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeletePolicy.open("GET","/secure/settings/locations/extendByPhone/deletePolicy.html?id="+policyID+"&"+document.location.search.substring(1),true);
	ajaxDeletePolicy.send();
}


function verifyDeletePolicy(policyID)
{
	var 	deletePolicyButtons = {};
		deletePolicyButtons.btn1 = {
			text : deleteBtn,
			click : 	function() { deletePolicy(policyID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deletePolicyButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deletePolicyMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deletePolicyButtons });
	btnStatus(deleteBtn);
}

//--------------------------------------------------------------------------------------

function loadPayStation(itemID, action, tabID){ 
	var hideCallBack = '';
	if($("#contentID").val() === itemID){ 
		if(action === "edit"){
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#paystationDetails"), $("#paystationDetails"), hideCallBack); }
		else if(action === "display"){
			if($("#mainContent").hasClass("edit")){
				hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); };
				slideFadeTransition($("#paystationDetails"), $("#paystationDetails"), hideCallBack); }
			else{ 
				if($("#detailView").find("article:visible").length){ 
					var	$detailObj = $("#detailView").find("article:visible"),
						reportStrng = "report";
					if($detailObj.attr("id").indexOf(reportStrng) > -1 && tabID.indexOf(reportStrng) > -1){
						loadPosTab(tabID, itemID, ""); }
					else{
						snglSlideFadeTransition("hide", $detailObj, function(){ loadPosTab(tabID, itemID, ""); }); } }
				else{ 
					loadPosTab(tabID, itemID, ""); } } } }
	else {
		var $this = $("#ps_"+itemID);
		if($("#paystationList").find("li.selected").length){ $("#paystationList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");

		hideCallBack = function(){
			var ajaxLoadPayStation = GetHttpObject();
			ajaxLoadPayStation.onreadystatechange = function()
			{
				if (ajaxLoadPayStation.readyState==4 && ajaxLoadPayStation.status == 200){
					showPayStationDetails(ajaxLoadPayStation, itemID, action, tabID);
				} else if(ajaxLoadPayStation.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load location details
				}
			};
			ajaxLoadPayStation.open("GET",LOC.loadPayStations+"?payStationID="+itemID+"&"+document.location.search.substring(1),true);
			ajaxLoadPayStation.send(); }

		if($("#paystationDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#paystationDetails"), hideCallBack); }
		else{ hideCallBack(); } } }

//--------------------------------------------------------------------------------------

function filterRouteList(filterVal)
{
	var rTypeClass = ["RT23", "RT975", "RT2494"]	
	for(x = 0; x < rTypeClass.length; x++){$("#routeList").removeClass(rTypeClass[x]);}
	if(filterVal !== "showAll"){ $("#routeList").addClass(filterVal); }
//console.log($("#routeList").attr("class"))
}

//--------------------------------------------------------------------------------------

function viewNotification(itemID, actionEvent) {
	var	itemID = itemID.split("_"),
		$this = $("#note_"+itemID[1]);
	
	if($("#systemNotification * li.selected").length){ $("#systemNotification * li.selected").toggleClass("selected"); }
	
	if(actionEvent === "hide"){ 
		snglSlideFadeTransition("hide", $("#seletectedNotification")); }
	else {
		$this.toggleClass("selected");
		showNotification = function(){ 
			$("#contentID").val(itemID[1]);
			var ajaxShowNote = GetHttpObject();
			ajaxShowNote.onreadystatechange = function(){
				if (ajaxShowNote.readyState==4 && ajaxShowNote.status == 200){
				var notificationData =	JSON.parse(ajaxShowNote.responseText);
					$("#articleArea").find("article").attr("id", "noteDisplay_"+itemID[1]);
					$("#articleArea").find("h4").html(notificationData.notificationDetails.title);
					$("#articleArea").find(".notificationMsg").html(notificationData.notificationDetails.message);
					if(notificationData.notificationDetails.messageUrl && notificationData.notificationDetails.messageUrl != ""){
						$("#articleArea").find("article > a").attr("href", notificationData.notificationDetails.messageUrl).show(); }
					else {$("#articleArea").find("article > a").hide(); }
					$("#notificationDate").html("&nbsp; " + notificationData.notificationDetails.beginTime);
					snglSlideFadeTransition("show", $("#seletectedNotification")); } 
				else if(ajaxShowNote.readyState==4){ alertDialog(editFail); } };
			ajaxShowNote.open("GET","/secure/settings/global/sysNotificationDetails.html?notificationID="+itemID[1]+"&"+document.location.search.substring(1),true);
			ajaxShowNote.send(); } 
			
		if($("#seletectedNotification").is(":visible")){
			snglSlideFadeTransition("hide", $("#seletectedNotification"), showNotification); }
		else{ showNotification(); } } }

//--------------------------------------------------------------------------------------

function settings()
{
	$("ul.currentContactList").on("click", "li", function(event){
		event.preventDefault();
		if($(this).attr("id") !== "noContact"){
			if($(this).hasClass("selected")){ 
				$(this).removeClass("selected").addClass("unselected").attr("title", clickToAdd); 
				var removeContact = $(this).find(".contactEmail").html().replace(" ","");
				$("#formAlertNotifyList").val($("#formAlertNotifyList").val().replace(removeContact, "").replace(",,", ",").replace(" ",""));
				if($("#formAlertNotifyList").val().charAt(0)==","){ $("#formAlertNotifyList").val($("#formAlertNotifyList").val().substring(1)); }
				if($("#formAlertNotifyList").val().charAt($("#formAlertNotifyList").val().length-1)==","){$("#formAlertNotifyList").val($("#formAlertNotifyList").val().substring(0, $("#formAlertNotifyList").val().length-1));}
			} else{ 
				$(this).removeClass("unselected").addClass("selected").attr("title", clickToRemove);
				if($("#formAlertNotifyList").val() == ""){ $("#formAlertNotifyList").val($(this).find(".contactEmail").html()); } else{$("#formAlertNotifyList").val($("#formAlertNotifyList").val() +","+ $(this).find(".contactEmail").html()); }
			}

		}
	});

	$("#selectPreferredJurisdictionByVal").on("change", function (event) {
        if($("#selectPreferredJurisdictionByVal").val() !== $("#ogPreferredJurisdictionVal").val() && ($("#selectPreferredJurisdictionByVal").val() !== '0' && $("#selectPreferredJurisdictionByVal").val() !== '-1')){
			$("#preferredParkingFile").addClass("reqField").attr("required", "required");
        } else {
            $("#preferredParkingFile").removeClass("reqField").attr("required", "false");
		}

        if($("#selectPreferredJurisdictionByVal").val() === '0'){
            $("#preferredParkingFile").addClass("disabled").val('').prop("disabled", true);
        } else {
            $("#preferredParkingFile").removeClass("disabled").prop("disabled", false);
        }

    });

	msgResolver.mapAttribute("selectTimeZone", "#selectTimeZone");
	msgResolver.mapAttribute("selectQuerySpacesBy", "#selectQuerySpacesBy");

	var $timeZoneSelector = $("#selectTimeZone");
	if($timeZoneSelector.length > 0) {
		$timeZoneSelector.autoselector({ "isComboBox": true, "defaultValue": null, "data": JSON.parse($("#selectTimeZoneData").text()) });
		setupGlobalInlineEdit("timeZone", "/secure/settings/global/saveTimeZone.html")
			.on("enterEditMode", function(event) {
				$timeZoneSelector.autoselector("option", "defaultValue", $timeZoneSelector.autoselector("getSelectedValue"));
			})
			.on("leaveEditMode", function(event) {
				$timeZoneSelector.autoselector("reset");
			})
			.on("saveSuccess", function(event) {
				$("#curTimeZone").html($timeZoneSelector.autoselector("getSelectedLabel"));
				noteDialog(timezoneSavedMsg, "timeOut", 2500);// Timezone changes Saved.
			});
	}
	
	var $querySpacesBySelector = $("#selectQuerySpacesBy");
	if($querySpacesBySelector.length > 0) {
		$querySpacesBySelector.autoselector({ "isComboBox": true, "defaultValue": null, "data": JSON.parse($("#selectQuerySpacesByData").text()), "invalidValue": -1 });
		setupGlobalInlineEdit("querySpacesBy", "/secure/settings/global/saveQuerySpacesBy.html")
			.on("enterEditMode", function(event) {
				$querySpacesBySelector.autoselector("option", "defaultValue", $querySpacesBySelector.autoselector("getSelectedValue"));
			})
			.on("leaveEditMode", function(event) {
				$querySpacesBySelector.autoselector("reset");
			})
			.on("saveSuccess", function(event) {
				$("#curQuerySpacesBy").html($querySpacesBySelector.autoselector("getSelectedLabel"));
				noteDialog(querySpacesBySavedMsg, "timeOut", 2500);// Timezone changes Saved.
			});
	}

	var $preferredJurisdictionBySelector = $("#selectPreferredJurisdictionBy"),
        $limitedJurisdictionBySelector = $("#selectLimitedJurisdictionBy");
	if($preferredJurisdictionBySelector.length > 0) {
        $preferredJurisdictionBySelector.autoselector({ "isComboBox": true, "defaultValue": null, "data": JSON.parse($("#selectPreferredJurisdictionByData").text()), "invalidValue": -1 });
        $limitedJurisdictionBySelector.autoselector({ "isComboBox": true, "defaultValue": null, "data": JSON.parse($("#selectLimitedJurisdictionByData").text()), "invalidValue": -1 });

        var $jurisdictionSection = $("#jurisdictionBy"),
			$jurisdictionView = $("#jurisdictionByView"),
			$jurisdictionForm = $("#jurisdictionByEdit"),
            $jurisdictionActionBtn = '';

        $jurisdictionSection.find("#btnEditJurisdictionBy").on('click', function(event){
            event.preventDefault();
            $preferredJurisdictionBySelector.autoselector("option", "defaultValue", $preferredJurisdictionBySelector.autoselector("getSelectedValue"));
            $limitedJurisdictionBySelector.autoselector("option", "defaultValue", $limitedJurisdictionBySelector.autoselector("getSelectedValue"));
            $jurisdictionActionBtn = $(this);
            $jurisdictionActionBtn.fadeOut();


            $jurisdictionView.fadeOut(200, function(){ $jurisdictionForm.fadeIn(400); });
        });

        $jurisdictionForm.find("#btnCancelJurisdictionBy").on('click', function(event){
            event.preventDefault();

            $jurisdictionForm.fadeOut(200, function(){
                $jurisdictionActionBtn.show();
                $preferredJurisdictionBySelector.autoselector("reset");
                $limitedJurisdictionBySelector.autoselector("reset");
                $("#preferredParkingFile").val('').removeClass("disabled reqField").attr("required", "false").prop("disabled", false);
                $jurisdictionSection.trigger("leaveEditMode");
                $jurisdictionSection.trigger("endEditMode");
                $jurisdictionView.fadeIn(400); });
        });
	}

	$(document.body).on("click", "#btnEditFlexCredentials", function(event){ event.preventDefault();
		var felxEditBtn = {};
		felxEditBtn.btn1 = { text: saveBtn, click: function(){ saveFlexCredentials(event, "custAdmin", $(this));  } };
		felxEditBtn.btn2 = { text: cancelBtn, click: function(event){ 
			event.preventDefault(); 
			cancelConfirm($(this), "reset"); } };
		
		$("#flexCredentialsFormArea").dialog({
			title: flexCredentialFormTitle,
			modal: true,
			resizable: false,
			closeOnEscape: false,
			width: 560,
			hide: "fade",
			buttons: felxEditBtn,
			dragStop: function(){ checkDialogPlacement($(this)); }
		});
		btnStatus(saveBtn);
		testPopFormEdit($("#flexCredentialsForm"), $("#flexCredentialsFormArea")); });
		
	$(document.body).on("click", "#currentNotifications > li, #pastNotifications > li", function(){
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display",
			itemID = $(this).attr("id"),
			stateObj = { itemObject: itemID, action: actionEvent };
		if(actionEvent === "hide"){ crntPageAction = ""; history.pushState(null, null, location.pathname); }
		else{ 
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action); }
		viewNotification(itemID, stateObj.action); });
		
	$(document.body).on("click", "#systemNotification > .view", function(){
		var actionEvent = ($(this).parent("li").hasClass("selected"))? "hide" : "display";
		var itemID = $(this).parent("li").attr("id");
		viewNotification(itemID, actionEvent); });
	
		
	$("#currentList").on("click", ".sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortRouteList = GetHttpObject();
		ajaxSortRouteList.onreadystatechange = function()
		{
			if (ajaxSortRouteList.readyState==4 && ajaxSortRouteList.status == 200){
				$("#currentList").html(ajaxSortRouteList.responseText);
				balanceNotificationList();
				scrollListWidth(["currentNotifications"]);
			} else if(ajaxSortRouteList.readyState==4){
				alertDialog(sortFail);
			}
		};
		ajaxSortRouteList.open("GET","/secure/settings/global/sortCurrentNotificationList.html?notificationType=current&sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortRouteList.send();
		
	});
	
	$("#pastList").on("click", ".sort", function(event){
		event.preventDefault();
		var sortSettings = $(this).attr("sort").split(":");
		var ajaxSortRouteList = GetHttpObject();
		ajaxSortRouteList.onreadystatechange = function()
		{
			if (ajaxSortRouteList.readyState==4 && ajaxSortRouteList.status == 200){
				$("#pastList").html(ajaxSortRouteList.responseText);
				balanceNotificationList();
				scrollListWidth(["pastNotifications"]);
			} else if(ajaxSortRouteList.readyState==4){
				alertDialog(sortFail);
			}
		};
		ajaxSortRouteList.open("GET","/secure/settings/global/sortPastNotificationList.html?notificationType=past&sort="+sortSettings[0]+"&dir="+sortSettings[1]+"&curItem="+$("#contentID").val()+"&"+document.location.search.substring(1),true);
		ajaxSortRouteList.send();
		
	});
}

function preferredParkingUploadSetup(){
    $uploadForm = $("#preferredParkingUploadForm")
        .dptupload({
            "postTokenSelector": "#preferredParkingPostToken",
            "url": "/secure/settings/global/saveJurisdictionType.html"
        })
        .on("uploadSuccess", function(event, responseContext) {
            if(!responseContext.displayedMessages) {
                noteDialog(jurisdictionBySavedMsg);
                setTimeout("location.reload()", 1200);
            }
            else {
                window.location.reload();
            }
        });

    $(document.body).on("click", "#btnSaveJurisdictionBy", function(event){
    	event.preventDefault();
        savePreferredParkers($(this));  });
}

function savePreferredParkers(btnObjct){
	$uploadForm
        .dptupload("option", "triggerSelector", btnObjct)
        .dptupload("upload");
}

function setupGlobalInlineEdit(sectionId, saveURL) {
	var $section = $("#" + sectionId);
	var $sectionView = $("#" + sectionId + "View");
	var $sectionForm = $("#" + sectionId + "Edit");
	var $actionBtn = '';
	
	$section.find(".edit").on('click', function(event){
		event.preventDefault();
        $actionBtn = $(this);
		$actionBtn.fadeOut();

		$section.trigger("enterEditMode");
		
		$("#mainContent").addClass("edit_" + sectionId);
		testFormEdit($("#timeZoneEditForm"));
		$sectionView.fadeOut(200, function(){ $sectionForm.fadeIn(400); });
	});
	
	var exitEditMode = function() {
		var $mainContent = $("#mainContent").removeClass("edit_" + sectionId);
		if(!$mainContent.is("[class*=edit_]")) {
			$mainContent.removeClass("edit");
            $actionBtn.show();
		}
		
		$sectionForm.fadeOut(200, function(){ $sectionView.fadeIn(400); $section.trigger("endEditMode"); });
	};
	
	$section.find(".cancel").on('click', function(event){
		event.preventDefault();
		exitEditMode();
		$section.trigger("leaveEditMode");
	});
	
	$section.find(".save").on('click', function(event){
		var params = $("#timeZoneEditForm").serialize();
		var ajaxTimeZoneSet = GetHttpObject({
			"postTokenSelector": "#postToken"
		});
		ajaxTimeZoneSet.onreadystatechange = function()
		{
			if (ajaxTimeZoneSet.readyState==4 && ajaxTimeZoneSet.status == 200)
			{
				var response =  ajaxTimeZoneSet.responseText.split(":");
				if(response[0] == "true")
				{
					exitEditMode();
					$section.trigger("saveSuccess");
				}
			} else if(ajaxTimeZoneSet.readyState==4){
				alertDialog(failedSaveMsg);  // Timezone change Save Failed
			}
		};
		ajaxTimeZoneSet.open("POST", saveURL,true);
		ajaxTimeZoneSet.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxTimeZoneSet.send(params);
	});
	
	return $section;
}

//--------------------------------------------------------------------------------------

function showXBPItem(area, task, itemID, day, startTime, endTime)
{
	var $this = $("#xbp_"+itemID+"_"+day),
		$that = $("#xbpItemList * li.selected");
	if($("#contentID").val() == itemID && $this === $that){
		if(task === "edit"){ 
			slideFadeTransition($("#xbpDetails"), $("#xbpDetails"), function(){$(".mainContent").addClass("form editForm")}); } 
		else if(task === "display"){ 
			slideFadeTransition($("#xbpDetails"), $("#xbpDetails"), function(){$(".mainContent").removeClass("form editForm"); $("#mainContent").removeClass("edit"); }); } }
	else {
		if($("#xbpItemList * li.selected").length){ $("#xbpItemList * li.selected").toggleClass("selected"); }
		if(task === "add"){
			$("ul.childList").each(function() { $(this).find("li.selected").each(function(){ $(this).removeClass("selected"); }) });
			resetXbpForm(area, day, startTime, endTime); } 
		else if(task === "hide"){
			$(".childList * li.selected").toggleClass("selected"); $("#contentID").val(""); snglSlideFadeTransition("hide", $("#xbpDetails")); }
		else{
			$this.addClass("selected");
			if(!$this.parent("ul.childList").is(":visible")){ $this.parent("ul.childList").slideToggle(); }
			if(area === "rate"){ loadRate(itemID, task); } else { loadPolicy(itemID, task); } } } }

//--------------------------------------------------------------------------------------

function xbpDetailSelect(itemID, itemType, eventAction)
{
	var tempItemID = itemID.split("_"),
		tempID = tempItemID[1],
		tempDayID = tempItemID[2],
		$this = $("#xbp_"+tempID+"_"+tempDayID),
		tempLocationID = $('#formLocationID').val();
	if(eventAction === "delete"){ if(itemType === "rate"){ verifyDeleteRate(tempID); } else { verifyDeletePolicy(tempID); } }
	else if(eventAction === "hide"){ 
		var tempQuerryString = scrubXbpQuerryString(location.search.substring(1), tempLocationID);
		crntPageAction = "";
		history.pushState(null, null, location.pathname+"?"+tempQuerryString);
		$this.toggleClass("selected"); $("#contentID").val(""); snglSlideFadeTransition("hide", $("#xbpDetails")); }
	else { 
		var stateObj = { area : itemType, action : eventAction, itemObject : tempID, itemDay: tempDayID };
		crntPageAction = stateObj.action;
		history.pushState(stateObj, null, location.pathname+"?area="+stateObj.area+"&locId="+tempLocationID+"&itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&day="+stateObj.itemDay);
		showXBPItem(stateObj.area, stateObj.action, stateObj.itemObject, stateObj.itemDay); }
}

//--------------------------------------------------------------------------------------

function xtendByPhone(tempPG, tempEvent, tempItemID, tempDay, tempStart, tempEnd)
{
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
	$("#contentID").val("");
	var popStateEvent = false;
	window.onpopstate = function(event) {
		popStateEvent = true;
		var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
			tempPG = ($("#xbpItemList").hasClass("xbpRates"))? 'rate' : 'policy', 
			tempEvent = 'hide', 
			tempItemID = '',
			tempDay = '';
		if(stateObj !== null){
			tempPG = stateObj.area, 
			tempEvent = stateObj.action, 
			tempItemID = stateObj.itemObject,
			tempDay = stateObj.itemDay; }
		showXBPItem(tempPG, tempEvent, tempItemID, tempDay, tempStart, tempEnd); }

	if(!popStateEvent && (tempEvent !== undefined && tempEvent !== '')){ showXBPItem(tempPG, tempEvent, tempItemID, tempDay, tempStart, tempEnd); }
///////////////////////////////////////////////////

	$([imgLocalDir+'icn_LocationChildSelected.png',imgLocalDir+'icn_LocationParentSelected.png']).preload();
	
	msgResolver.mapAttribute("warningPeriod", "#wrnngPeriodFormVal");
	
	msgResolver.mapAttribute("startTime", "#formItemStartTime");
	msgResolver.mapAttribute("endTime", "#formItemEndTime");
	
	msgResolver.mapAttribute("sunday", ".daySelection");
	msgResolver.mapAttribute("monday", ".daySelection");
	msgResolver.mapAttribute("tuesday", ".daySelection");
	msgResolver.mapAttribute("wednesday", ".daySelection");
	msgResolver.mapAttribute("thursday", ".daySelection");
	msgResolver.mapAttribute("friday", ".daySelection");
	msgResolver.mapAttribute("saturday", ".daySelection");
	
	msgResolver.mapAttribute("name", "#formRateName,#formPolicyName"); // Has to be like this because I don't want to track which page I am atm.
	
	// Rates
	msgResolver.mapAttribute("minExtensionMinutes", "#formRateMinExt")
	msgResolver.mapAttribute("rateValue", "#formRateValue")
	msgResolver.mapAttribute("rateServiceFee", "#formRateFee")
	
	// Policies
	msgResolver.mapAttribute("permissionStatus", "#xbpParkPolicy");
	msgResolver.mapAttribute("maxDurationMinutes", "#formMaxDur");
	
	$("#formItemStartTime").autominutes(optsTime);
	$("#formItemEndTime").autominutes($.extend({}, optsTime, { "allows2400": true }));
	coupleDateComponents(null, $("#formItemStartTime"), null, $("#formItemEndTime"));

	
	$(document.body).on("click", "#XtendByPhone * #locationList > li, #XtendByPhone * .childList > li", function(){
		var currentLocationID = $(this).attr("id").split("_");
		if($(this).hasClass("Parent")){ 
			if($("#mainContent").hasClass("edit")) { pausePropagation = true; inEditAlert("menu"); }
			else{ $("#childList_"+currentLocationID[1]).slideToggle(); $(this).toggleClass("expanded"); } }
		else{
			if($("#mainContent").hasClass("edit")) { pausePropagation = true; inEditAlert("site"); };
						
			if(pausePropagation === true){ 
				pausePropagationLoop = setInterval(function(){ 
					if(pausePropagation === false){ 
						window.location = "/secure/settings/locations/extendByPhone/overview.html?locId=" + currentLocationID[1]+"&"+document.location.search.substring(1);
						pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
					else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
			else{ 
				window.location = "/secure/settings/locations/extendByPhone/overview.html?locId=" + currentLocationID[1]+"&"+document.location.search.substring(1); } } });
				
	$(document.body).on("click", "#xbpShowAll", function(event){ event.preventDefault();
		var tempStatus = $(this).attr("status");
		if(tempStatus === "show"){ 
			$(this).html(hideAllBtn).attr("status", "hide");
			$(document.body).find("#XtendByPhoneDetails * li.Parent").each(function(){
				var dayID = $(this).attr("id").split("_");
				if(!$("#childList_"+dayID[1]).is(":visible")){ $("#childList_"+dayID[1]).slideDown(); } }); }
		else{ 
			$(this).html(showAllBtn).attr("status", "show"); 
			$(document.body).find("#XtendByPhoneDetails * li.Parent").each(function(){
				var dayID = $(this).attr("id").split("_");
				if($("#childList_"+dayID[1]).is(":visible")){ $("#childList_"+dayID[1]).slideUp(); } }); }  });

	$(document.body).on("click", "#XtendByPhoneDetails * li.Parent", function(event){
		var dayID = $(this).attr("id").split("_");
		if($("#childList_"+dayID[1]).find("li.selected").length){ 
			var objectID = $("#childList_"+dayID[1]).find("li.selected").attr("id"); 
			var itemType = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy";
			xbpDetailSelect(objectID, itemType, "hide");
			$("#childList_"+dayID[1]).slideToggle(); }
		else if($("#childList_"+dayID[1]).find("li").length){
			$("#childList_"+dayID[1]).slideToggle(); } });

	$(document.body).on("click", "#xbpItemList * li", function(event){
		var	objectID = $(this).attr("id"),
			itemType = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy",
			actionEvent = ($(this).hasClass("selected"))? "hide" : "display";	
		if(pausePropagation === true){ 
			pausePropagationLoop = setInterval(function(){ 
			if(pausePropagation === false){ xbpDetailSelect(objectID, itemType, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
			else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ xbpDetailSelect(objectID, itemType, actionEvent); } });
		
	$("#XtendByPhone").find(".overview").on('click', function(event){
		event.preventDefault();
		var currentLocationID = $(this).parent(".innerBorder").attr("id").split("_");
		window.location = "/secure/settings/locations/extendByPhone/overview.html?locId=" + currentLocationID[1]+"&"+document.location.search.substring(1);
	});
	
	$("#XtendByPhoneItems").find(".overview").on('click', function(event){
		event.preventDefault();
		var currentLocationID = $("#xbpCurLocal").val();
		var querryString = scrubXbpQuerryString(document.location.search, currentLocationID);
		if(querryString.indexOf("?") != 0){ querryString = "?"+querryString; }
		window.location = "/secure/settings/locations/extendByPhone/overview.html" + querryString;
	});
	
	$("#XtendByPhone").find(".rate").on('click', function(event){ //Link from Overview
		event.preventDefault();
		if($(this).parents("#menu_pageContent").hasClass("ddMenu")){ var currentLocationID = $("#contentID").val(); 
			var querryString = scrubXbpQuerryString(document.location.search, currentLocationID);}
		else{ var querryString = "?locId=" + $(this).parent(".innerBorder").attr("id").split("_")[1]; }
		if(querryString.indexOf("?") != 0){ querryString = "?"+querryString; }
		window.location = "/secure/settings/locations/extendByPhone/rate.html" + querryString + "&area=rate";
	});
	
	$("#XtendByPhoneItems").find(".rate").on('click', function(event){ //Link from Policy
		event.preventDefault();
		var currentLocationID = $("#xbpCurLocal").val();
		var querryString = scrubXbpQuerryString(document.location.search, currentLocationID);
		if(querryString.indexOf("?") != 0){ querryString = "?"+querryString; }
		window.location = "/secure/settings/locations/extendByPhone/rate.html" + querryString + "&area=rate";
	});
	
	$("#XtendByPhone").find(".policy").on('click', function(event){ //Link from Overview
		event.preventDefault();
		if($(this).parents("#menu_pageContent").hasClass("ddMenu")){ var currentLocationID = $("#contentID").val(); 
			var querryString = scrubXbpQuerryString(document.location.search, currentLocationID);}
		else{ var querryString = "?locId=" + $(this).parent(".innerBorder").attr("id").split("_")[1]; }
		if(querryString.indexOf("?") != 0){ querryString = "?"+querryString; }
		window.location = "/secure/settings/locations/extendByPhone/policy.html" + querryString + "&area=policy";
	});
	
	$("#XtendByPhoneItems").find(".policy").on('click', function(event){ //Link from Rates
		event.preventDefault();
		var currentLocationID = $("#xbpCurLocal").val();
		var querryString = scrubXbpQuerryString(document.location.search, currentLocationID);
		if(querryString.indexOf("?") != 0){ querryString = "?"+querryString; }
		window.location = "/secure/settings/locations/extendByPhone/policy.html" + querryString + "&area=policy";
	});
	
	$("#warningPeriod").find(".edit").on('click', function(event){
		event.preventDefault();
		testFormEdit($("#xbpWarningPeriod"));
		$("#wrnngPeriodValMsg").fadeOut(200, function(){ $("#wrnngPeriodFormArea").fadeIn(400); });
	});
	
	$("#warningPeriod").find(".cancel").on('click', function(event){
		event.preventDefault();
		$("#mainContent").removeClass("edit");
		$("#wrnngPeriodFormArea").fadeOut(200, function(){ $("#wrnngPeriodValMsg").fadeIn(400); });
		$("#wrnngPeriodFormVal").val($("#wrnngPeriodVal").html());
	});
	
	$(document.body).on("submit", "#xbpWarningPeriod", function(event){ event.preventDefault(); });
	
	$("#xbpWarningPeriod").find(".save").on('click', function(event){
		event.preventDefault();
		var params = $.param(serializeForm("#xbpWarningPeriod"));
		var ajaxWrnngPeriod = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxWrnngPeriod.onreadystatechange = function()
		{
			if (ajaxWrnngPeriod.readyState==4 && ajaxWrnngPeriod.status == 200)
			{
				var response =  ajaxWrnngPeriod.responseText.split(":");
				if(response[0] == "true")
				{
					$("#wrnngPeriodVal").html($("#wrnngPeriodFormVal").val());
					$("#mainContent").removeClass("edit");
					$("#wrnngPeriodFormArea").fadeOut(200, function(){ $("#wrnngPeriodValMsg").fadeIn(400); });
					noteDialog(warningPeriodSavedMsg, "timeOut", 2500);// Warning Period changes Saved.
				}
			} else if(ajaxWrnngPeriod.readyState==4){
				alertDialog(failedSaveMsg);// Warning Period Save Failed
			}
		};
		ajaxWrnngPeriod.open("POST","/secure/settings/locations/extendByPhoneEditWarningPeriod.html",true);
		ajaxWrnngPeriod.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxWrnngPeriod.setRequestHeader("Content-length", params.length);
		ajaxWrnngPeriod.setRequestHeader("Connection", "close");
		ajaxWrnngPeriod.send(params);
	});
	
	var aniStart = false;
	$(document.body).on("click", "section[rate-item], section[policy-item]", function(event){
		var $that = $(this);
		var $this = $that.next(".xbpItemInfo");
		
		if($this.is(":visible")){ $this.fadeOut(200); }
		else{
			if($(".xbpItemInfo").is(":visible")){ $(".xbpItemInfo").hide(); }
			var horzPos = $that.position();
			var positioning = "left";
			if((horzPos.left+250) > $("#AuraArea").innerWidth()){ horzPos = $("#AuraArea").innerWidth() - (horzPos.left+$that.outerWidth(true)); positioning = "right"}
			else{ horzPos = horzPos.left; }
			if($that.is("[rate-item]")){ $this.css("top", "-56px").css(positioning, horzPos); }
			else{ $this.css("bottom", "-56px").css(positioning, horzPos); }
			$this.fadeIn(400); }
	}).on("mouseenter", "section[rate-item], section[policy-item]", function(){
		var $this = $(this).next(".xbpItemInfo");
		if($this.is(":visible")){ $this.stop(true, true).show(); }
	}).on("mouseleave", "section[rate-item], section[policy-item]", function(){
		var $this = $(this).next(".xbpItemInfo");
		if($this.is(":visible")){ $this.delay(1200).fadeOut(400); } });
	
	$(document.body).on("mouseenter", ".xbpItemInfo", function(event){ event.preventDefault();
		$(this).stop(true, true).show();
	}).on("mouseleave", ".xbpItemInfo", function(){
		$(this).delay(800).fadeOut(400); });
	
	$("#xbpItemList").find(".view").on('click', function(event){
		event.preventDefault();
		var	itemID = $(this).parents(".innerBorder").attr("id"),
			itemType = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy" ;
		xbpDetailSelect(itemID, itemType, "display");
	});
	
	$("#xbpItemList").find(".edit").on('click', function(event){
		event.preventDefault();
		testFormEdit($("#xbpItemSettings"));
		var	itemID = $(this).parents(".innerBorder").attr("id"),
			itemType = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy" ;
		xbpDetailSelect(itemID, itemType, "edit");
	});

	$(".xbpItemInfo").find(".edit").on('click', function(event){
		event.preventDefault();
		testFormEdit($("#xbpItemSettings"));
			window.location = $(this).attr("href");
	});

	$(".xbpItemInfo").find(".add").on('click', function(event){
		event.preventDefault();
		testFormEdit($("#xbpItemSettings"));
			window.location = $(this).attr("href");
	});
	
	$(".xbpItemInfo").find(".delete").on('click', function(event){
		event.preventDefault();
		var $containerBox = $(this).parents(".xbpItemInfo");
		var itemID = $containerBox.attr("id").replace("xbpItmID_", "");
		if($containerBox.hasClass("type_rate")){ verifyDeleteRate(itemID); }
		else{ verifyDeletePolicy(itemID); }
	});

	$("#xbpItemList").find(".delete").on('click', function(event){
		event.preventDefault();
		var	itemID = $(this).parents(".innerBorder").attr("id"),
			itemType = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy" ;
		xbpDetailSelect(itemID, itemType, "delete");
	});
	
	$("#menu_pageContent").find(".delete").on('click', function(event){
		event.preventDefault();
		var itemID = $("#contentID").val();
		if($("#xbpItemList").hasClass("xbpRates")){ verifyDeleteRate(itemID); }
		else{ verifyDeletePolicy(itemID); }
	});
	
	$("#XtendByPhoneItems").find(".add").on('click', function(event){
		event.preventDefault(); testFormEdit($("#xbpItemSettings"));
		var	tempArea = ($("#xbpItemList").hasClass("xbpRates"))? "rate" : "policy",
			stateObj = { area : tempArea, action : "add", itemObject : "New", itemDay: "" },
			tempLocationID = $('#formLocationID').val(),
			tempQuerryString = scrubXbpQuerryString(location.search.substring(1), tempLocationID);;
		$("#xbpItemList").find("li.selected").each(function(){ $(this).removeClass("selected"); })
		crntPageAction = stateObj.action;
		history.pushState(stateObj, null, location.pathname+"?"+tempQuerryString+"itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&area="+stateObj.area);		
		resetXbpForm(stateObj.area);
	});
	
	$("#xbpParkPolicy").find(".radioLabel").on('click', function(event){
		event.preventDefault();
		var radioData = $(this).children(".radio").attr("id").split(":");
		if(radioData[1] == "timelimited"){ $("#xbpMaxDurationArea").removeClass("disabledArea"); $("#formMaxDur").attr("disabled", false); }
		else{ $("#xbpMaxDurationArea").addClass("disabledArea"); $("#formMaxDur").attr("disabled", true); }
	});
		
	$("#formRateName").autocomplete({source: unifiedRateNames});
	
	$("#xbpItemSettings").on("click", ".save", function(event)
	{
		event.preventDefault();
		
		var params = $("#xbpItemSettings").serialize();
		var ajaxSaveXbpItem = GetHttpObject({
			"triggerSelector": (event) ? event.target : null
		});
		ajaxSaveXbpItem.onreadystatechange = function()
		{
			if (ajaxSaveXbpItem.readyState==4 && ajaxSaveXbpItem.status == 200)
			{
				var response = ajaxSaveXbpItem.responseText.split(":");
				if(response[0] == "true")
				{
					noteDialog(xbpItemSavedMsg);
					var querryString = scrubXbpQuerryString(location.search);
					var itemID = ($("#formRateID").length)? $("#formRateID").val() : $("#formPolicyID").val();
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					if(itemID){ var newLocation = location.pathname+querryString+"&itemID="+itemID+"&pgActn="+crntPageAction; }
					else{ var newLocation = location.pathname+querryString+"&pgActn="+crntPageAction; }
					setTimeout(function(){window.location = newLocation}, 500);
				}
			} else if(ajaxSaveXbpItem.readyState==4){
				alertDialog(failedSaveMsg);// Rate/Policy changes and/or submission is unable to Save.
			}
		};
		ajaxSaveXbpItem.open("POST","/secure/settings/locations/extendByPhone/saveExtendByPhone.html",true);
		ajaxSaveXbpItem.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveXbpItem.setRequestHeader("Content-length", params.length);
		ajaxSaveXbpItem.setRequestHeader("Connection", "close");
		ajaxSaveXbpItem.send(params);
//		}
	});
	
	$("#xbpItemSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("xbpCleanEditReload"); });
}

//--------------------------------------------------------------------------------------

function showPSStatus(psID, psData){
	if(psData){ 
		$("#psLastSeen").find(".value").html(psData.payStationDetails.lastSeen);
		$("#batteryVoltageWdgt, #inputCurrentWdgt, #ambientTemperatureWdgt, #relativeHumidityWdgt, #ControllerTemperatureWdgt").children(".groupBoxBody").html(loadObj); 
		psBatteryVoltage(psID);
		psInputCurrent(psID);
		psAmbientTemprature(psID);
		psRelativeHumidity(psID);
		psControllerTemprature(psID);

		$(document.body).scrollTop(0); $("#statusArea").fadeIn(function(){ actvTabSwtch = false; }); 
	} else{
		var ajaxLoadPayStation = GetHttpObject();
		ajaxLoadPayStation.onreadystatechange = function()
		{
			if (ajaxLoadPayStation.readyState==4 && ajaxLoadPayStation.status == 200){
				var psData =	JSON.parse(ajaxLoadPayStation.responseText);
				$("#psLastSeen").find(".value").html(psData.payStationDetails.lastSeen); 
				psBatteryVoltage(psID);
				psInputCurrent(psID);
				psAmbientTemprature(psID);
				psRelativeHumidity(psID);
				psControllerTemprature(psID);

				$(document.body).scrollTop(0); $("#statusArea").fadeIn(function(){ actvTabSwtch = false; }); 
			} else if(ajaxLoadPayStation.readyState==4){
				alertDialog(systemErrorMsg); //Unable to load location details
			}
		};
		ajaxLoadPayStation.open("GET",LOC.loadPayStations+"?payStationID="+psID+"&"+document.location.search.substring(1),true);
		ajaxLoadPayStation.send();
	}
}

//--------------------------------------------------------------------------------------

function hidePS($this){
	snglSlideFadeTransition("hide", $("#paystationDetails"));
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#paystationList").find("li.selected").length){ $("#paystationList").find("li.selected").toggleClass("selected"); } } }

//--------------------------------------------------------------------------------------

function posSelectionCheck(posID, eventAction, tabID)
{
	var	itemID = posID.split("_"),
		tempID = itemID[1],
		$this = $("#ps_"+tempID);
	if(eventAction == "hide"){ 
		crntPageAction = "";
		history.pushState(null, null, location.pathname);
		hidePS($this); }
	else { 
		if($("#contentID").val() != itemID[1] || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: tempID, action: eventAction, tabObject: tabID };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
			loadPayStation(stateObj.itemObject, stateObj.action, stateObj.tabObject); }}}

//--------------------------------------------------------------------------------------

function extractPOSList(POSList, context) {
	var result = null,
		buffer = JSON.parse(POSList).payStationsList;
	
	result = buffer.elements;
	
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

function loadPOSList()
{
	$("#paystationList").paginatetab({
		"url": "/secure/listPayStations.html",
		"itemLocatorURL": "/secure/locatePageWithPayStations.html",
		"formSelector": "#posFilterForm",
		"submissionFormat": "GET",
		"emptyMessage": noPaystationsMsg,
		"rowTemplate": unescape($("#paystationListTemplate").html()),
		"responseExtractor": extractPOSList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData"); 
	
	$("#paystationList").on("pageLoaded", function(){
	$("ul.pageItems").find("li").each(function(index, element) {
		if(selectedItemLoad){ 
			if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
				$(this).trigger('click');
				checkVisibleItem($(this), $("ul.pageItems")); 
				selectedItemLoad = false; } }
		else if($("#contentID").val() !== "" && $("#paystationDetails").is(":visible")){  
			if(($(this).attr("id") && $(this).attr("id").match($("#contentID").val())) && !$(this).hasClass("selected")){ 
				$(this).addClass("selected");
				checkVisibleItem($(this), $("ul.pageItems")); }  }}); }); }

//--------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popPSAction(tempItemID, tempEvent, tempTab, stateObj){ 
	if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#ps_"+tempItemID) : ""; hidePS($this); }
	else { loadPayStation(tempItemID, tempEvent, tempTab); } }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

var scrollTest = false;
var scrollPage = '';

function paystations(updateFilter, tempPsID, tempEvent, tempTabID){
	reloadTab();	
	if(!updateFilter){ loadPOSList(); }

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempTabID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempTabID = stateObj.tabObject;
		tempEvent = stateObj.action; }
	popPSAction(tempItemID, tempEvent, tempTabID, stateObj); }

if(!popStateEvent && (tempPsID !== undefined && tempPsID !== '')){ popPSAction(tempPsID, tempEvent, tempTabID); }
///////////////////////////////////////////////////	


	$("#paystationList").on("click", "li", function(event){
		event.preventDefault(); event.stopImmediatePropagation();
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var locationID = $(this).attr("id");
		if($("#mainContent").hasClass("edit")){ 
			event.stopImmediatePropagation(); 
			inEditAlert("site", function(){ posSelectionCheck(locationID, actionEvent);}); }
		else{ posSelectionCheck(locationID, actionEvent, "status"); } });
	
	$("#paystationList").on("click", "a.edit", function(event){
		event.preventDefault(); event.stopImmediatePropagation();
		if($("#mainContent").hasClass("edit")){ 
			event.stopImmediatePropagation(); 
			var itemID = $(this).attr("id");
			inEditAlert("site", function(){ posSelectionCheck(itemID, "edit", "");}); }
		else{ var itemID = $(this).attr("id"); posSelectionCheck(itemID, "edit", ""); }
	});
	
	$(".pin").on('click', function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		window.location = LOC.payStationsPlacement+"?payStationID="+psID+"&"+document.location.search.substring(1);
	});
	
	$("#paystationNavArea").find(".tab").on('click', function(event){
		event.preventDefault();
		if(!actvTabSwtch){
			actvTabSwtch = true;
			var psID = $("#contentID").val(),
				$parentObj = $(this).parent(".tabSection"),
				$parentObjID = $parentObj.attr("id"),
				tabID = $parentObj.attr("id").replace("Btn", "");
			if(!$parentObj.hasClass("current")){ 
				var $that = $("#psNav").find(".tabSection.current")
				$that.removeClass("current"); 
				var curArea = $that.attr("id").replace("Btn", "Area"); 
				var stateObj = { itemObject: psID, action: "display", tabObject: tabID };
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
				snglSlideFadeTransition("hide",$("#"+curArea), function(){ loadPosTab(stateObj.tabObject, stateObj.itemObject, ""); }); }
			else{ actvTabSwtch = false; }} });	
	
	$("#reportsArea").find(".tab").on('click', function(event){ event.preventDefault();
		if(!actvTabSwtch && !$(this).hasClass("current")){
			var psID = $("#contentID").val(),
				tabID =($(this).attr("id") === "transactionReportBtn")? "reportsTransactions" : "reports" ;
				stateObj = { itemObject: psID, action: "display", tabObject: tabID };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&tabID="+stateObj.tabObject);
			loadPosTab(stateObj.tabObject, stateObj.itemObject, ""); } });
	
	$(".reload").on('click', function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		var objID = $(this).attr("id");
		if(objID == "batteryVoltageReload"){ psBatteryVoltage(psID); }
		else if(objID == "inputCurrentReload"){ psInputCurrent(psID); }
		else if(objID == "ambientTemperatureReload"){ psAmbientTemprature(psID); }
		else if(objID == "relativeHumidityReload"){ psRelativeHumidity(psID); }
		else if(objID == "ControllerTemperatureReload"){ psControllerTemprature(psID); }
		else if(objID == "btnReload"){ loadPayStation(psID, 'display'); }
	});

	$(document.body).on("click", ".svCSV", function(event){
		event.preventDefault();
		var psID = $("#contentID").val();
		var objID = $(this).attr("id");
		if(objID == "batteryVoltageHistory"){ objID = 0; }
		else if(objID == "inputCurrentHistory"){ objID = 5;}
		else if(objID == "ambientTemperatureHistory"){ objID = 3; }
		else if(objID == "relativeHumidityHistory"){ objID = 4; }
		else if(objID == "ControllerTemperatureHistory"){ objID = 2; }
		$.ajax({
			url: LOC.historyDownload+"?payStationID="+psID+"&sensorTypeID="+objID+"&"+document.location.search.substring(1),
			success: function(data) { window.location = LOC.historyDownload+"?payStationID="+psID+"&sensorTypeID="+objID+"&"+document.location.search.substring(1); },
			error: function(data){ alertDialog(systemErrorMsg); } }); });
			
	$(document.body).on("click", "#posActivityExport", function(event){event.preventDefault();
		var psID = $("#contentID").val();
		$.ajax({
			url: LOC.activityHistoryDownload+"?payStationID="+psID+"&"+document.location.search.substring(1),
			success: function(data) { window.location = LOC.activityHistoryDownload+"?payStationID="+psID+"&"+document.location.search.substring(1); },
			error: function(data){ alertDialog(systemErrorMsg); } }); });
	
	$("#collectionReportArea").on("click", "#collectionReportList > span > li", function(event){
		event.preventDefault();
		if(!$(this).hasClass("notclickable")){
			$("#collectionDetailView").css('max-height', ($(window).innerHeight()-150)+"px"); 
			var	itemID = $(this).attr("id").split("_"),
				itemID = itemID[1];
			var ajaxCollectionReport = GetHttpObject();
			ajaxCollectionReport.onreadystatechange = function()
			{
				if (ajaxCollectionReport.readyState==4 && ajaxCollectionReport.status == 200){
					if($("#collectionDetailView.ui-dialog-content").length){$("#collectionDetailView.ui-dialog-content").dialog("destroy");}
					$("#collectionDetailView").html(ajaxCollectionReport.responseText);
					var collectionViewBtn = {};
					collectionViewBtn.btn1 = { text: exportPDFMsg, click: function(event, ui) {
						window.open(LOC.printCollectionDetailReport + "?collectionId=" + itemID + "&" + document.location.search.substring(1), "reportViewer");
					}};
					collectionViewBtn.btn2 = { text: closeMsg, click: function(event, ui) { $(this).scrollTop(0); $(this).dialog( "close" ); } };
					$("#collectionDetailView").dialog({
						title: collectionDetailsMsg,
						modal: true,
						resizable: false,
						closeOnEscape: true,
						maxHeight: $(window).innerHeight()-150,
						width: 475,
						hide: "fade",
						buttons: collectionViewBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }
					});
					btnStatus(closeMsg);	
				} else if(ajaxCollectionReport.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load details
				}
			};
			ajaxCollectionReport.open("GET",LOC.viewCollectionReportsDetail+"?collectionId="+itemID+"&"+document.location.search.substring(1),true);
			ajaxCollectionReport.send(); }
	});
	
	$("#transactionReportArea").on("click", "#transactionReportList > span > li", function(event){
		event.preventDefault();
		if(!$(this).hasClass("notclickable")){
			$("#transactionDetailView").css('max-height', ($(window).innerHeight()-150)+"px"); 
			var	itemID = $(this).attr("id").split("_"),
				itemID = itemID[1];
			var ajaxTransactionReport = GetHttpObject();
			ajaxTransactionReport.onreadystatechange = function()
			{
				if (ajaxTransactionReport.readyState==4 && ajaxTransactionReport.status == 200){
					if($("#transactionDetailView.ui-dialog-content").length){$("#transactionDetailView.ui-dialog-content").dialog("destroy");}
					$("#transactionDetailView").html(ajaxTransactionReport.responseText);
					var transactionViewBtn = {};
					transactionViewBtn.btn1 = { text: closeMsg, click: function(event, ui) { $(this).scrollTop(0); $(this).dialog( "close" ); } };
					$("#transactionDetailView").dialog({
						title: transactionDetailsMsg,
						modal: true,
						resizable: false,
						closeOnEscape: true,
						maxHeight: $(window).innerHeight()-150,
						width: 475,
						hide: "fade",
						buttons: transactionViewBtn,
						dragStop: function(){ checkDialogPlacement($(this)); }
					});
					btnStatus(closeMsg);
				} else if(ajaxTransactionReport.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load details
				}
			};
			ajaxTransactionReport.open("GET",LOC.viewTransactionReportsDetail+"?transactionId="+itemID+"&"+document.location.search.substring(1),true);
			ajaxTransactionReport.send(); }
	});
	
	$("#formPayStationLocation").autoselector({
		"isComboBox": true, 
		"defaultValue": null,
		"shouldCategorize": false,
		"data": childLocationObj });
	
	$("#btnPayStationDeactivate").on('click', function(event){
		event.preventDefault();
		$("#formPayStationStatus").val("false").trigger('change');
		$("#btnPayStationDeactivate").hide(); $("#btnPayStationActivate, #formHidePayStation").show();
	});
	$("#btnPayStationActivate").on('click', function(event){
		event.preventDefault();
		$("#formPayStationStatus").val("true");
		$("#btnPayStationDeactivate").show(); $("#btnPayStationActivate, #formHidePayStation").hide();
		$("#hidePayStationCheckHidden").val("false");
		$("#hidePayStationCheck").removeClass("checked");
	});
	
	$(document.body).on("click", ".removeFile", function(event){ event.preventDefault(); clearPOSAlerts($(this)); });
	
	$("#paystationForm").on("click", ".save", function(event)
	{
		event.preventDefault();
			var psID = $("#payStationSettings").find("#formPayStationID").val();
			var params = $("#payStationSettings").serialize();
			var ajaxSavePayStation = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
			ajaxSavePayStation.onreadystatechange = function() {
				if (ajaxSavePayStation.readyState==4 && ajaxSavePayStation.status == 200) {
					var response = ajaxSavePayStation.responseText.split(":");
					if(response[0] == "true") {
						noteDialog(payStationSavedMsg);
						var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "itemValue", "pgActn"]);
						crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
						var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry+"&itemValue="+$("#contentID").val()+"&pgActn="+crntPageAction;
						setTimeout(function(){window.location = newLocation}, 500); } }// new location Saved.
				else if(ajaxSavePayStation.readyState==4){ alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
			ajaxSavePayStation.open("POST","/secure/settings/locations/payStationList/savePayStation.html",true);
			ajaxSavePayStation.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			ajaxSavePayStation.setRequestHeader("Content-length", params.length);
			ajaxSavePayStation.setRequestHeader("Connection", "close");
			ajaxSavePayStation.send(params);
//		}
	});
	
	$("#payStationSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
}

//--------------------------------------------------------------------------------------

function exportCollectionReport() {
	window.open(LOC.printCollectionDetailReport + "?" + scrubQueryString(document.location.search.substring(1), "all") + "&repositoryID=" + reportId, "reportViewer");
}

//--------------------------------------------------------------------------------------

function saveCardType(event)
{
	var params = $("#cardTypeForm").serialize();
	var ajaxCardTypeForm = GetHttpObject({
		"triggerSelector": (event) ? event.target : null
	});
	ajaxCardTypeForm.onreadystatechange = function(responseFalse, displayedMessages)
	{
		if (ajaxCardTypeForm.readyState==4 && ajaxCardTypeForm.status == 200)
		{
			if(!responseFalse)
			{
				$("#cardTypeSettingsForm").dialog("close");
				if(!displayedMessages) {
					noteDialog(cardTypeSaveMsg);
				}
				
				setTimeout("location.reload()", 1500); // Route Deleted.
			}
			else{
				var failData =	JSON.parse(ajaxCardTypeForm.responseText);
		//console.log( ajax.responseText);
				$("#CSpostToken, #CTpostToken").val(failData.errorStatus.token);
			}
		} else if(ajaxCardTypeForm.readyState==4){
			alertDialog(failedSaveMsg);
		}
	};
	ajaxCardTypeForm.open("POST","/secure/settings/cardSettings/saveCardType.html",true);
	ajaxCardTypeForm.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	ajaxCardTypeForm.setRequestHeader("Content-length", params.length);
	ajaxCardTypeForm.setRequestHeader("Connection", "close");
	ajaxCardTypeForm.send(params);
}

//--------------------------------------------------------------------------------------

function deleteCardType(cardTypeID)
{
	var ajaxDeleteCardType = GetHttpObject();
	ajaxDeleteCardType.onreadystatechange = function(responseFalse, displayedMessages)
	{
		if (ajaxDeleteCardType.readyState==4 && ajaxDeleteCardType.status == 200){
			if(!responseFalse) { 
				noteDialog(itemDeleteMsg);
				setTimeout("location.reload()", 800);
			}
			else if(!displayedMessages) { alertDialog(systemErrorMsg); } //Unable to process Request
		} else if(ajaxDeleteCardType.readyState==4){
			alertDialog(systemErrorMsg); //Unable to process Request
		}
	};
	ajaxDeleteCardType.open("GET","/secure/settings/cardSettings/deleteCardType.html?id="+cardTypeID+"&"+document.location.search.substring(1),true);
	ajaxDeleteCardType.send();
}

function verifyCardTypeDelete(cardTypeID)
{
	var 	deleteCTButtons = {};
		deleteCTButtons.btn1 = {
			text : deleteBtn,
			click : 	function() { deleteCardType(cardTypeID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deleteCTButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	
//	$("#messageResponseAlertBox").dialog("destroy");
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteCardTypeMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteCTButtons });
	btnStatus(deleteBtn);
}

//--------------------------------------------------------------------------------------

function cardSettings()
{
	msgResolver.mapAttribute("creditCardOfflineRetry", "#formCCRetry");
	msgResolver.mapAttribute("maxOfflineRetry", "#formMaxRetry");
	msgResolver.mapAttribute("cardTypeName", "#formCardTypeName");
	msgResolver.mapAttribute("track2Pattern", "#formCardTypeTrack2");
	msgResolver.mapAttribute("description", "#formCardTypeDescription");
	msgResolver.mapAttribute("authorizationMethod", "#formCardTypeAuthMethod");
	msgResolver.mapAttribute("creditCardOfflineRetry", "#formCCRetry");
	msgResolver.mapAttribute("maxOfflineRetry", "#formMaxRetry");
	
	
	$("#btnEditCardSettings").on('click', function(event){
		event.preventDefault();
		$(this).fadeOut();
		testFormEdit($("#cardSettingsForm"));
		$("#cardSettingDisplay").fadeOut(function(){$("#cardSettingForm").fadeIn(function(){$("#formCCRetry").trigger('focus');});});
	});
	
	$("#cardSettingsCncl").on('click', function(event){
		event.stopImmediatePropagation();
		event.preventDefault();
		$("#mainContent").removeClass("edit");
		$("#formCCRetry").val($("#detailCCRetry > p").html());
		$("#formMaxRetry").val($("#detailMaxRetry > p").html());
		$("#cardSettingForm").fadeOut(function(){ $("#cardSettingDisplay, #btnEditCardSettings").fadeIn(); });
	});
	
	$("#cardSettingsSv").on('click', function(event){
		event.preventDefault();
		var params = $("#cardSettingsForm").serialize();
		var ajaxCardSttngsForm = GetHttpObject();
		ajaxCardSttngsForm.onreadystatechange = function(responseFalse, displayedMessages)
		{
			if (ajaxCardSttngsForm.readyState==4 && ajaxCardSttngsForm.status == 200)
			{
				var response =  ajaxCardSttngsForm.responseText.split(":");
				if(!responseFalse)
				{
					$("#mainContent").removeClass("edit");
					$("#CSpostToken, #CTpostToken").val(response[1]);
					$("#detailCCRetry").html("<p>"+$("#formCCRetry").val()+"</p>");
					$("#detailMaxRetry").html("<p>"+$("#formMaxRetry").val()+"</p>");
					if(!displayedMessages) {
						noteDialog("Card Settings Saved.", "timeOut", 2500);
					}
					$("#cardSettingForm").fadeOut(function(){ $("#cardSettingDisplay, #btnEditCardSettings").fadeIn(); });
				}else{
					var failData =	JSON.parse(ajaxCardSttngsForm.responseText);
					$("#CSpostToken, #CTpostToken").val(failData.errorStatus.token);
				}
			} else if(ajaxCardSttngsForm.readyState==4){
				alertDialog(failedSaveMsg);
			}
		};
		ajaxCardSttngsForm.open("POST","/secure/settings/cardSettings/saveCardSettings.html",true);
		ajaxCardSttngsForm.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxCardSttngsForm.setRequestHeader("Content-length", params.length);
		ajaxCardSttngsForm.setRequestHeader("Connection", "close");
		ajaxCardSttngsForm.send(params);
	});
	
	$("#btnAddCardType").on('click', function(event){
		event.preventDefault();
		if($("#mainContent").hasClass("edit")){ inEditAlert("menu"); }
		else{
			if($("#cardTypeSettingsForm.ui-dialog-content").length){$("#cardTypeSettingsForm.ui-dialog-content").dialog("destroy");}
			document.getElementById("cardTypeForm").reset();
			$("#formCardTypeAuthMethod").autoselector("reset");
			$("formCardTypeID").val("");
			var editCardTypeBtn = {};
			editCardTypeBtn.btn1 = { text: addCardTypeMsg, click: function(event, ui) { saveCardType(event); } };
			editCardTypeBtn.btn2 = { text: cancelBtn, click: function(event){ event.preventDefault(); cancelConfirm($(this)); } };
			
			$("#cardTypeSettingsForm").dialog({
				title: addCardTypeMsg,
				modal: true,
				resizable: false,
				closeOnEscape: false,
				width: 350,
				height: 350,
				hide: "fade",
				zIndex: 3000,
				buttons: editCardTypeBtn,
				stack: true,
				dragStop: function(){ checkDialogPlacement($(this)); }
			});
			btnStatus(addCardTypeMsg);
			testPopFormEdit($("#cardTypeForm"), $("#cardTypeSettingsForm"));
			$("#formCardTypeName").trigger('focus'); }
	});
	
	$(".btnEditCardType").on('click', function(event){
		event.preventDefault();
		var cardTypeID = $(this).parents(".ddMenu").attr("id").replace("menu_", "")
		var editCardTypeBtn = {};
		editCardTypeBtn.btn1 = { text: editCardTypeMsg, click: function(event, ui) { saveCardType(event); } };
		editCardTypeBtn.btn2 = { text: cancelBtn, click: function(event){ event.preventDefault(); cancelConfirm($(this)); } };
		
		var ajaxCardType = GetHttpObject();
		ajaxCardType.onreadystatechange = function()
		{
			if (ajaxCardType.readyState==4 && ajaxCardType.status == 200){
				if($("#cardTypeSettingsForm.ui-dialog-content").length){$("#cardTypeSettingsForm.ui-dialog-content").dialog("destroy");}
				var cardTypeData =	JSON.parse(ajaxCardType.responseText);
				$("#formCardTypeID").val(cardTypeData.cardSettingsEditForm.customerCardTypeRandomId);
				$("#formCardTypeName").val(cardTypeData.cardSettingsEditForm.cardTypeName);
				$("#formCardTypeTrack2").val(cardTypeData.cardSettingsEditForm.track2Pattern);
				$("#formCardTypeDescription").val(cardTypeData.cardSettingsEditForm.description);
				$("#formCardTypeAuthMethod").autoselector("setSelectedValue", cardTypeData.cardSettingsEditForm.authorizationMethodRandomId);								
				
				$("#cardTypeSettingsForm").dialog({
					title: editCardTypeMsg,
					modal: true,
					resizable: false,
					closeOnEscape: false,
					width: 350,
					height: 350,
					hide: "fade",
					zIndex: 3000,
					buttons: editCardTypeBtn,
					stack: true,
					dragStop: function(){ checkDialogPlacement($(this)); }
				});
				btnStatus(editCardTypeMsg);
				$("#formCardTypeName").trigger('focus');
			} else if(ajaxCardType.readyState==4){
				alertDialog(systemErrorMsg);
			}
		};
		ajaxCardType.open("GET","/secure/settings/cardSettings/editCardType.html?id="+cardTypeID+"&"+document.location.search.substring(1),true);
		ajaxCardType.send();
	});
	
	$("#formCardTypeAuthMethod").autoselector({
		"isComboBox": true,
		"defaultValue": null,
		"data": JSON.parse($("#formCardTypeAuthMethodData").text()) });

	
	$(".btnDeleteCardType").on('click', function(event){
		event.preventDefault();
		var cardTypeID = $(this).parents(".ddMenu").attr("id").replace("menu_", "")
		verifyCardTypeDelete(cardTypeID);
	});
}

//--------------------------------------------------------------------------------------

function scrubXbpQuerryString(curQuerryString, currentLocationID)
{
	var querryArray = curQuerryString.split("&");
	var locId = false;
	var newQuerry = ""; 
	for(x=0; x < querryArray.length; x++)
	{
		var spliceArray = false;
		var querryObj = querryArray[x].split("="); 
		if(querryObj[0] == "locId"){ if(currentLocationID && currentLocationID != ""){ querryObj[1] = currentLocationID; } locId = true;}
		if(querryObj[0] == "area"){ spliceArray = true; }
		if(querryObj[0] == "pgActn"){ spliceArray = true; }
		if(querryObj[0] == "day"){ spliceArray = true; }
		if(querryObj[0] == "itemID"){ spliceArray = true; }
		if(querryObj[0] == "startTime"){ spliceArray = true; }
		if(querryObj[0] == "endTime"){ spliceArray = true; }
		if(spliceArray !== true) { newQuerry = newQuerry + querryObj.join("=") + "&"; }
	}
	return(newQuerry);
}

//GLOBAL VARIABLES	FOR XBP START END TIME VALIDATION
	var timeValidation = true;
	var alertMsg = false;
	var showAllMsg = false;
	var testVar = false;
	var singleMin = false;
	var timeInMin = 0;
	var tmpStartTime = 0;
	var tmpEndTime = 0;
// /GLOBAL VARIABLES


function minToTime(theMinTime)
{
	var theMinTime = new Number(theMinTime);
	var Hours = Math.floor(theMinTime/60);
	var Minutes = theMinTime%60;
	var Hours = (Hours < 10) ?  "0"+Hours : Hours;
	var Minutes = (Minutes < 10) ?  "0"+Minutes : Minutes;
	var Time = Hours + ":" + Minutes;
	return Time;
}

function time2Min(theTimeObj)
{
	var theTime = theTimeObj.val(), theMin = 0;
	if(!characterTypes.numericTimeExpression.test(theTime)){
		var theTimeArray = theTime.split(":");
		if(theTimeArray[1] && theTimeArray[1].length < 2){ theMin = new Number(theTimeArray[1]*10); singleMin = true; } 
		else if(theTimeArray[1]){ theMin = new Number(theTimeArray[1]); singleMin = false; }
		if(theMin > 59){ xtrHour = Math.floor(theMin/60); theHour = (theTimeArray[0]*1 + xtrHour)*60; theMin = theMin%60; var timeMin = theHour + theMin; theTimeObj.val(minToTime(timeMin)); } 
		else { var timeMin = theTimeArray[0]*60 + theMin; }
		if(timeMin > 1440) { timeMin = 1440; theTimeObj.val("24:00"); } 
		else { return timeMin;	 }
	}
	else { return false; }
}

function validateTime(timeObj, alertStatus)
{
	if(timeValidation == true)
	{
		timeInMin = time2Min(timeObj);
	
		if(timeInMin == false && timeObj.val() != "00:00") { return false; } 
		else if(timeObj.attr("id") == "formItemStartTimeVal") { // Start Time Test
			if(timeInMin == 1440){ if(alertMsg != "before24" && (!alertStatus || alertStatus != false)){ alertDialog(xbpEarlierTimeMsg); alertMsg = "before24"; } formErrorClass(timeObj); $(timeObj).trigger('focus'); return false; }
			else { alertMsg = false; formClearErrorClass(timeObj); return true; }
		} else if(timeObj.attr("id") == "formItemEndTimeVal") { // End Time Test
			formClearErrorClass(timeObj);
			var tempSingleMin = singleMin, formItemStartTimeInMin = time2Min($("#formItemStartTimeVal"));
			// Test if end time is before or at the same time as Start Time
			if(formItemStartTimeInMin > timeInMin || (formItemStartTimeInMin == timeInMin && tempSingleMin == false))
			{
				if(alertMsg != "endBeforeStart" && (!alertStatus || alertStatus != false)){ alertDialog(xbpEndAfterStartMsg); timeValidation = false; alertMsg = "endBeforeStart"; formErrorClass(timeObj); $(timeObj).trigger('focus'); singleMin = tempSingleMin; return false; }
				if(timeInMin == 1440){ formErrorClass($('#formItemStartTime')); $('#formItemStartTime').trigger('focus'); singleMin = tempSingleMin; return false; }
				else { formErrorClass(timeObj); $(timeObj).trigger('focus'); singleMin = tempSingleMin; return false; }
			} else { alertMsg = false; formClearErrorClass(timeObj); singleMin = tempSingleMin; return true; }
		} else if(timeObj.attr("id") == "formMaxDur") {
			$(timeObj).removeClass("textError");
			var tempSingleMin = singleMin, tempStartTime = time2Min($("#formItemStartTime")), tempEndTime = time2Min($("#formItemEndTime")), tempDuration = tempEndTime - tempStartTime, tempDurationTime = time2Min(timeObj);
			if(tempDuration < tempDurationTime) { if(alertMsg != "formMaxDur" && (!alertStatus || alertStatus != false)){ alertDialog(xbpMaxDurationLength); alertMsg = "formMaxDur"; } formErrorClass(timeObj); $(timeObj).trigger('focus'); return false; } }
		else if(timeObj.attr("id") == "INC-startTimeVal") { // Start Time Test
			if(timeInMin == 1440){ alertDialog(startB4MidnightMsg,"","",timeObj); return false; }
			else { alertMsg = false; formClearErrorClass(timeObj); return true; }}
		else if(timeObj.attr("id") == "INC-endTimeVal") { // Start Time Test
			var tempSingleMin = singleMin, formItemStartTimeInMin = time2Min($("#INC-startTimeVal"));
			// Test if end time is before or at the same time as Start Time
			if(formItemStartTimeInMin > timeInMin || (formItemStartTimeInMin == timeInMin && tempSingleMin == false))
			{
				alertDialog(endB4StartMsg,"","",timeObj); $timeObj.trigger('focus'); singleMin = tempSingleMin; return false;
			} else { alertMsg = false; formClearErrorClass(timeObj); singleMin = tempSingleMin; return true; }}
		else { alertMsg = false; formClearErrorClass(timeObj); singleMin = tempSingleMin; return true; }
	}
}

// ----------------------------------------------------------------------------------------------------

// PAY STATION SETTINGS PAGE FUNCTIONS //

// ----------------------------------------------------------------------------------------------------

var	posMarkers = new Array(),
	settingDisplayList = "",
	scrollTest = false,
	scrollPage = "",
	listType = "";

// ----------------------------------------------------------------------------------------------------

function noSettingResultList()
{
	scrollTest = true;
	if(scrollPage == 1){ $("#activeList > .listItems").fadeOut("fast", function(){ 
		$("#activeList > .listItems")
			.html("<li class='alignCenter notclickable'><span class='textLine'>"+noSettingsMsg+"</span></li>")
			.fadeIn("slow"); }); }
	else if($("#activeList > .listItems > .loadItem").length){$("#activeList > .listItems > .loadItem").remove();}
}

// ----------------------------------------------------------------------------------------------------

function settingDetail(settingObj, settingName)
{	
	var 	posClass = "severityActive",
		latlongPOS = "lat='' long='' ",
		dateString = "N/A",
		severityLevel = "0",
		clickClass = "notclickable";
	if(settingObj.downloadDate){ dateString = settingObj.downloadDate; } 
	
	if(settingObj.isNotExist == true){ posClass = "severityHigh", severityLevel = 3; }
	else if(settingObj.isUpdateInProgress == true){ posClass = "severityLow", severityLevel = 1, dateString = pendingMsg; }

	if(settingObj.latitude)
	{
		posMarkers.push(new mapMarker(
			settingObj.latitude, 
			settingObj.longitude, 
			settingObj.name,
			settingObj.payStationType, 
			settingObj.randomId,
			severityLevel,
			settingName,
			posClass,
			dateString,'','',
			settingObj.locationName,
			settingObj.serialNumber
		));
		latlongPOS = "lat='"+ settingObj.latitude +"' long='"+ settingObj.longitude +"' ",
		clickClass = "clickable";
	}

	$("#posStatusList").append("<li class='"+clickClass+"' "+
		"serial='"+ settingObj.serialNumber +"' "+
		latlongPOS +"><section class='"+posClass+"'>"+
		"<h3>"+settingObj.name+"</h3>"+
		"<span class='location' title="+locationLbl+">"+settingObj.locationName+"</span>"+
		"<span class='downloadDate' title="+dwnDateLbl+">"+dateString+"</span></section></li>");
}

// ----------------------------------------------------------------------------------------------------

function insertPayStationDetails(settingDetails) //Inserts Details into Detail Box and Map Marker InfoBox
{
	if(settingDetails && settingDetails != "") {
		$("#selectedSettingDetails").find("h2").html(settingDetails.name);
		 $("#posStatusList").html("");
		if(settingDetails.uploadDate){ $("#dateCreated").html(settingDetails.uploadDate) } 
		else { $("#dateCreated").html("N/A"); }
		
		if(settingDetails.payStationList){
			posMarkers = new Array();
			var payStations = settingDetails.payStationList;
			if(payStations && payStations instanceof Array === false){ payStations = [payStations] }
			for(x=0; x<payStations.length; x++){ settingDetail(payStations[x], settingDetails.name); }
			initMapSettings("map", posMarkers); }
		else { $("#posStatusList").html("<li>"+noPaystationsMsg+"</li>"); initMapSettings("map"); }} 
	else {
		$("#selectedPOSdetails").find("h2").html("");
		$("#dateCreated").html("");
		$("#posStatusList").html(""); }
}


// ----------------------------------------------------------------------------------------------------

function getSettingInfo(settingID, eventType) //Collects Details on Point of Sale Alert Info and hide/displays detail Box and Marker on Map
{
	var heightDiff = $("#selectedSetting").outerHeight(true) - $("#selectedSetting").height(),
	objBaseHeight = 78,
	objHeight = 0,
	listHeight = $("#activeList").height()-heightDiff,
	contentHeightDiff = 0;

//	console.log(listHeight, $("#activeList").height(), heightDiff);
//	console.log(eventType);
	if(eventType == "clear")
	{
		var	ogSelectedDetailOuterHeight = $("#selectedSetting").outerHeight(true),
			ogSelectedDetailHeight = $("#selectedSetting").height(),
			currentListHeight = $("#activeList").height(); // USE mapItemList class instead of currentListObj for tab Switch.
		$("#selectedSetting").animate({height: "0px", marginTop: -(ogSelectedDetailOuterHeight-ogSelectedDetailHeight)}, { duration: 800, 
			step: function(now, fx){ if(fx.prop === "height"){$("#activeList").height(currentListHeight+(ogSelectedDetailOuterHeight-now))}; },
			complete: function(){
				$("#selectedSetting").hide().css("margin-top", "0");
				insertPayStationDetails(); 
				$("#selectedSettingDetails").width(100);
				toShowInfo = false; }});
	} else {
		$.ajax({
			url: "/secure/settings/locations/payStationSettingsDetail.html?"+document.location.search.substring(1),
			dataType: "json",
			data: {settingsFileId: settingID},
			success: function(data) {				
				var 	settingDetails = data.paystationSetting,
					listScrollLevel = $("#activeList").scrollTop(),
					itemTopPosition = $("li.selected").position().top+listScrollLevel;
				if(eventType == "new")
				{
					insertPayStationDetails(settingDetails);
					
					$("#selectedSetting").height(0).show();
					$("#selectedSettingDetails").width("100%");
					var objContentHeight = $("#selectedSettingDetails").height();
					if(objContentHeight > objBaseHeight){ objHeight = objContentHeight+8; }
					else{ objHeight = objBaseHeight; }
					$("#selectedSetting").animate({height: objHeight+"px"},{
						duration: 800, 
						step: function(now, fx){ $("#activeList").height(listHeight-now);
							var	itemTopMaxPosition =  itemTopPosition-15,
								scrollPosition = listScrollLevel+now+15;
							if(itemTopMaxPosition >= scrollPosition){$("#activeList").scrollTop(scrollPosition);}},
						complete: function(){ toShowInfo = false; }
					});
				}
				else if(eventType == "update")
				{
					var objHeight = $("#selectedSetting").height(),
					ogObjHeight = objHeight,
					ogCntntHeight = $("#selectedSettingDetails").height()>objBaseHeight? $("#selectedSettingDetails").height() : objBaseHeight,
					ogListHeight = $("#activeList").height(),
					cntntHeightDiff = 0;
					$("#selectedSettingDetails").fadeOut(400,function(){
						var heightStep1 = $("#selectedSettingDetails").innerHeight();
						insertPayStationDetails(settingDetails);
						var heightStep2 = $("#selectedSettingDetails").innerHeight();
						var newCntntHeight = (heightStep1 === heightStep2)? ogCntntHeight : heightStep2-9;
						
						if(ogCntntHeight != newCntntHeight && newCntntHeight > objBaseHeight){
							if(ogCntntHeight > newCntntHeight && newCntntHeight > objBaseHeight){
								cntntHeightDiff = ogCntntHeight - newCntntHeight;
								objHeight = objHeight - cntntHeightDiff;
							} else if(ogCntntHeight < newCntntHeight){
								cntntHeightDiff = newCntntHeight - ogCntntHeight;
								objHeight = objHeight + cntntHeightDiff;
							} else {
								cntntHeightDiff = ogCntntHeight - objBaseHeight;
								objHeight = objBaseHeight;
							}
							$("#selectedSetting").animate({height: objHeight+"px"},{ 
								duration: 800, 
								step: function(now, fx){
									var diff = ogObjHeight - now;
									$("#activeList").height(ogListHeight+diff);
									var	itemTopMaxPosition =  itemTopPosition-15,
										scrollPosition = listScrollLevel+now+45;
									if(itemTopMaxPosition >= scrollPosition){$("#activeList").scrollTop(scrollPosition);}},
								complete: function(){ toShowInfo = false; }
							}); }
						$("#selectedSettingDetails").fadeIn(400, function(){ toShowInfo = false; });
					});
					var listHeightDV =  $("#activeList").height()+listScrollLevel-45;
					if(itemTopPosition > listHeightDV){ 
						while(itemTopPosition > listHeightDV){ 
							$("#activeList").scrollTop(listScrollLevel + 1); 
							listScrollLevel = $("#activeList").scrollTop();
							listHeightDV++; } }
					else if(itemTopPosition < listScrollLevel){
						while((listScrollLevel > 0) && ((itemTopPosition - 15) < listScrollLevel)){ 
							$("#activeList").scrollTop(listScrollLevel - 1);
							listScrollLevel =  $("#activeList").scrollTop(); }}
				}
			},
			error: function(){ alertDialog(systemErrorMsg); }
		});
	}
}

// ----------------------------------------------------------------------------------------------------

function updateList() //Determins Current list and sets "isFiltered" style and calls filterPOSList()
{
	if($(".listItems > li.selected").length){$("li.selected").trigger('click');}
	var	statusValues = new Array(),
		statusFilterClasses =  [".severityActive", ".severityLow", ".severityHigh"],
		showAll = true,
		statusFormObj = $("#settingFilter").serialize().split("&");
	for(x=0; x<statusFormObj.length; x++){
		statusValues.push(statusFormObj[x].split("="));
		if(statusValues[x][1] == "true"){ showAll = false;} }
	if(showAll == true){ $(".listItems > li").each(function(index, element) { if(!$(this).is(":visible")){ $(this).fadeIn(); } }); } 
	else {
		for(x=0; x<statusValues.length; x++){
			if(statusValues[x][1] == "true"){
				$(".listItems > li > section"+statusFilterClasses[x]).each(function(index, element) { 
					if(!$(this).parent("li").is(":visible")){ $(this).parent("li").fadeIn(); } }); }
			else { 
				$(".listItems > li > section"+statusFilterClasses[x]).each(function(index, element) { 
					if($(this).parent("li").is(":visible")){ $(this).parent("li").fadeOut(); } }); } } }
}

// ----------------------------------------------------------------------------------------------------

function deleteSettingFile(fileID)
{
	var ajaxDeleteSettingFile = GetHttpObject();
	ajaxDeleteSettingFile.onreadystatechange = function()
	
	{
		if (ajaxDeleteSettingFile.readyState==4 && ajaxDeleteSettingFile.status == 200){
			if(ajaxDeleteSettingFile.responseText == "true") { 
				noteDialog(settingFileDeleteMessage);
				setTimeout("location.reload()", 800);
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxDeleteSettingFile.readyState==4){
			alertDialog(systemErrorMsg); //Unable to delete Location
		}
	};
	ajaxDeleteSettingFile.open("GET","/secure/settings/locations/deletePayStationSettings.html?settingsFileId="+fileID+"&"+document.location.search.substring(1),true);
	ajaxDeleteSettingFile.send();
}

// ----------------------------------------------------------------------------------------------------

function posSettings(settingList) //Main Function for Pay Station Settings Page
{
	if(settingList && settingList != ""){
		var	posSettingData = JSON.parse(settingList),
			posSettingList = posSettingData.list[0];//.AlertCentreAlertInfo;
		if(posSettingList instanceof Array === false){ posSettingList = [posSettingList]; }
		for(x = 0; x < posSettingList.length; x++){ listContent(posSettingList[x]); }
		$("#activeList > .listItems").html(posDisplayList);
	}
	initMapSettings("map");
	
	$(document.body).on("click", ".settingList > .listItems > li", function(event){
		if(!$(this).hasClass("notclickable")){
			var	markerMatch = false,
				settingID = $(this).attr("id").split("_");
	
			if($(this).hasClass("selected")){ 
				$(this).removeClass("selected");
				getSettingInfo("", "clear"); }
			else {
				var status = "";
				if($(".settingList > .listItems > li.selected").length > 0){
					status = "update";
					$(".settingList > .listItems > li.selected").removeClass("selected"); $(this).addClass("selected"); } 
				else { 
					status = "new";
					$(this).addClass("selected"); }
				getSettingInfo(settingID[1], status); }} });
	
	$(document.body).on("click", "#posStatusList > li", function(event){
		if(!$(this).hasClass("notclickable")){
			var	markerMatch = false,
				posStngSN = $(this).attr("serial"),
				posStngLat = $(this).attr("lat"),
				posStngLong = $(this).attr("long");
	
			if(posStngLat != "" && posStngLong != ""){ mapInPage.panTo(new L.LatLng(posStngLat, posStngLong)); } 
			
			markers.eachLayer( function(marker){ 
				if(marker.options.alt == posStngSN){ marker.openPopup().setZIndexOffset(1400); } 
				else { marker.setZIndexOffset(0); marker.closePopup(); }} ); } });
	
	$(document.body).on("click", ".checkboxLabel", function(event){ if($(this).parents("ul#settingFilterForm")){ updateList(); } });
	
	$("#activeList").on("click", ".settingDelete", function(event){ event.preventDefault(); event.stopImmediatePropagation();  
		var settingFileID = $(this).parents("li").attr("id").split("_");
		
		var 	deleteSettingButtons = {};
		deleteSettingButtons.btn1 = {
			text : deleteBtn,
			click : function() { deleteSettingFile(settingFileID[1]); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		deleteSettingButtons.btn2 = {
			text : cancelBtn,
			click : function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	
		if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
		$("#messageResponseAlertBox")
		.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+deleteSettingFileMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: deleteSettingButtons });
		btnStatus(deleteBtn);
 });
}

// ----------------------------------------------------------------------------------------------------

function extractDeviceActivityLog(activityMsgs, context) {
	var buffer = JSON.parse(activityMsgs).list[0];
	var result = buffer.deviceHistory;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

function extractDeviceList(mDeviceList, context) {
	var buffer = JSON.parse(mDeviceList).deviceList;
	var result = buffer[0].device;
	if(!Array.isArray(result)) {
		result = [ result ];
	}
	
	context.dataKey = buffer.dataKey;
	context.page = buffer.page;
	
	return result;
}

function recentDeviceActivity() {
	 $("#deviceActivityLogList")
		.paginatetab({
			"url": "/secure/settings/devices/deviceHistory.html",
			"formSelector": "#deviceActivityLogFilterForm",
			"postTokenSelector": "#postTokenActivity",
			"rowTemplate": unescape($("#deviceActivityLogTemplate").html()),
			"responseExtractor": extractDeviceActivityLog,
			"dataKeyExtractor": extractDataKey
		})
		.paginatetab("setupSortingMenu", "#deviceActivityLogListHeader")
		.paginatetab("reloadData");
}

function loadDeviceList()
{
	$("#deviceList").paginatetab({
		"url": "/secure/settings/devices/deviceList.html",
		"formSelector": "#deviceFilterForm",
		"submissionFormat": "GET",
		"emptyMessage": noDeviceFoundMsg,
		"rowTemplate": unescape($("#deviceListTemplate").html()),
		"responseExtractor": extractDeviceList,
		"dataKeyExtractor": extractDataKey
	})
	.paginatetab("reloadData"); 

	$("#deviceList").on("pageLoaded", function(){
	$("ul.pageItems").find("li").each(function(index, element) {
		if(selectedItemLoad){ 
			if(($(this).attr("id") && $(this).attr("id").match(selectedItemID)) && !$(this).hasClass("selected")){ 
				$(this).trigger('click');
				checkVisibleItem($(this), $("ul.pageItems")); 
				selectedItemLoad = false; } }
		else if($("#contentID").val() !== "" && $("#deviceDetails").is(":visible")){  
			if(($(this).attr("id") && $(this).attr("id").match($("#contentID").val())) && !$(this).hasClass("selected")){ 
				$(this).addClass("selected");
				checkVisibleItem($(this), $("ul.pageItems")); }  }}); });
}

function displayDevice(data, action)
{
	var deviceData = data.device,
		deviceName = (deviceData.name)? deviceData.name : unNamedDeviceTtl,
		formDeviceName = (deviceData.name)? deviceData.name : "",
		usersName = "",
		showCallBack = '';
	if(deviceData.firstName){ usersName = deviceData.firstName; }
	if(deviceData.lastName){ usersName += " "+deviceData.lastName; }
	
	$("#actionFlag").val("1")
	$("#mapHolder").html("");
	$("#contentID, #formDeviceID, #randomIdActivity").val(deviceData.randomId);
	$("#detailDeviceName, #deviceName").html(deviceName);
	$("#formDeviceName").val(formDeviceName);
	$("#detailDeviceId").html(deviceData.uid);
	if(usersName !== ""){
		if(deviceData.loggedIn){ 
			$(".lastUserLine").hide();
			$("#detailDeviceCurUser").html(usersName);
			$(".curUserLine").show(); }
		else {
			$(".curUserLine").hide();
			$("#detailDeviceLastUser").html(usersName);
			$(".lastUserLine").show(); } } 
	else { $(".curUserLine").hide(); $(".lastUserLine").hide(); }
	
	if(deviceData.description && deviceData.description !== ""){ 
		$("#detailDeviceDescription").html(deviceData.description); 
		$(".deviceDescription").show();
		$("#formDescription").val(deviceData.description); }
	else{ $(".deviceDescription").hide(); $("#detailDeviceDescription").html(""); $("#formDescription").val(""); }
	
	if(deviceData.appInfos instanceof Array === false){ var appList = (deviceData.appInfos); } else { var appList = deviceData.appInfos; }
	$("#formReleasedApps").val("");
	
	if(deviceData.statusId === 3){ 
		$("#menu_pageContent * .btnUnBlkDevice").show(); $("#menu_pageContent * .btnBlkDevice").hide(); 
		$("#detailDeviceName, #deviceName").append(' <span class="note">[Blocked]</span>');
		$("#statusCheck").addClass("checked"); $("#statusCheckHidden").val("true"); }
	else { $("#menu_pageContent * .btnUnBlkDevice").hide(); $("#menu_pageContent * .btnBlkDevice").show(); $("#statusCheck").removeClass("checked"); $("#statusCheckHidden").val("false"); }
	
	x = 0;
	$("#assignedAppList, #formAssignedAppList").html("");
	if(deviceData.statusId === 3 || deviceData.statusId === 2){ $("#assignedAppList, #formAssignedAppList").hide(); $("#assignedAppList, #formAssignedAppList").siblings("h3").hide(); }
	else{ $("#assignedAppList, #formAssignedAppList").show(); $("#assignedAppList, #formAssignedAppList").siblings("h3").show();
		for(x = 0; x < appList.length; x++){
			$("#assignedAppList").append("<li id='clearApp_"+appList[x].app.randomId+"'>"+appList[x].app.appName+"</li>");
			$("#formAssignedAppList").append("<li id='clearApp_"+appList[x].app.randomId+"'>"+appList[x].app.appName+"<a id='clear_"+appList[x].app.randomId+"' class='removeFile settingDelete menuListButton' title='"+ releaseLicenseTtl +"' href='#'>&nbsp;</a><a id='undo_"+appList[x].app.randomId+"' class='undo' title='"+ undoReleaseTtl +"' href='#'>"+ undoTtl +"</a></li>"); } }
	
	recentDeviceActivity();
	
	clearErrors();

	$(document.body).scrollTop(0);
	
	if(action == "display")
	{
		showCallBack = function(){
			soh(["deviceActivityLogList"]); scrollListWidth(["deviceActivityLogList"]);
			if(deviceData.latitude){
//mapMarker(lat, long, name, type, id, severity, lastSeen, voltage, paperStatus, lastCollection, runningTotal, location, serialNumber)
				var deviceMarker = (new mapMarker(
					deviceData.latitude, 
					deviceData.longitude, 
					deviceName, 
					deviceData.statusId, 
					deviceData.randomId,
					'','','','','','','',
					deviceName ));
					initMapStatic(deviceMarker, "user");	}
			else { initMapStatic(); } }
			
		$("#mainContent").removeClass("edit"); 
		$("#deviceDetails").removeClass("form editForm addForm")
		snglSlideFadeTransition("show", $("#deviceDetails"), showCallBack); }
	else if(action == "edit") {
		$("#deviceDetails").removeClass("addForm").addClass("form editForm")
		snglSlideFadeTransition("show", $("#deviceDetails"), function(){$("#formDeviceName").trigger('focus');});} }

function loadDeviceDetails(deviceID, action)
{
	var hideCallBack = '';
	if($("#contentID").val() === deviceID){
		if(action === "edit"){
			hideCallBack = function(){ $(".mainContent").addClass("form editForm"); }
			slideFadeTransition($("#deviceDetails"), $("#deviceDetails"), hideCallBack); }
		else if(action === "display"){
			hideCallBack = function(){ $(".mainContent").removeClass("form editForm addForm"); $("#mainContent").removeClass("edit"); }
			slideFadeTransition($("#deviceDetails"), $("#deviceDetails"), hideCallBack); } }
	else { 
		var $this = $("#md_"+deviceID);
		if($("#deviceList").find("li.selected").length){ $("#deviceList").find("li.selected").toggleClass("selected"); }
		$this.toggleClass("selected");

		hideCallBack = function(){
			var ajaxDeviceDetails = GetHttpObject();
			ajaxDeviceDetails.onreadystatechange = function()
			{
				if (ajaxDeviceDetails.readyState==4 && ajaxDeviceDetails.status == 200){
			displayDevice(JSON.parse(ajaxDeviceDetails.responseText), action);
				} else if(ajaxDeviceDetails.readyState==4){
					alertDialog(systemErrorMsg); //Unable to load details
				}
			};
			ajaxDeviceDetails.open("GET","/secure/settings/devices/deviceDetail.html"+"?randomId="+deviceID,true);
			ajaxDeviceDetails.send(); } }

		if($("#deviceDetails").is(":visible")){ snglSlideFadeTransition("hide", $("#deviceDetails"), hideCallBack); }
		else{ hideCallBack(); } }

function blockDevice(deviceID)
{
	var ajaxBlkDevice = GetHttpObject();
	ajaxBlkDevice.onreadystatechange = function()
	{
		if (ajaxBlkDevice.readyState==4 && ajaxBlkDevice.status == 200){
			if(ajaxBlkDevice.responseText == "true") { 
				noteDialog(deviceBlockedMsg);
				setTimeout("location.reload()", 800); // Rate Deleted.
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxBlkDevice.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxBlkDevice.open("GET","/secure/settings/devices/lockDevice.html?deviceID="+deviceID+"&"+document.location.search.substring(),true);
	ajaxBlkDevice.send();

}

function verifyBlockDevice(deviceID)
{
	var 	blockDeviceButtons = {};
		blockDeviceButtons.btn1 = {
			text : blockBtn,
			click : 	function() { blockDevice(deviceID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		blockDeviceButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+blockDeviceMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: blockDeviceButtons });
	btnStatus(blockBtn);
}

function unblockDevice(deviceID)
{
	var ajaxUnBlkDevice = GetHttpObject();
	ajaxUnBlkDevice.onreadystatechange = function()
	{
		if (ajaxUnBlkDevice.readyState==4 && ajaxUnBlkDevice.status == 200){
			if(ajaxUnBlkDevice.responseText == "true") { 
				noteDialog(deviceUnblockedMsg);
				setTimeout("location.reload()", 800); // Rate Deleted.
			}
			else { alertDialog(systemErrorMsg); } //Unable to delete Location
		} else if(ajaxUnBlkDevice.readyState==4){
			alertDialog(systemErrorMsg); //Unable to load details
		}
	};
	ajaxUnBlkDevice.open("GET","/secure/settings/devices/unLockDevice.html?deviceID="+deviceID+"&"+document.location.search.substring(),true);
	ajaxUnBlkDevice.send();


}

function verifyUnblockDevice(deviceID)
{
	var 	unblockDeviceButtons = {};
		unblockDeviceButtons.btn1 = {
			text : unblockBtn,
			click : 	function() { unblockDevice(deviceID); $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
		unblockDeviceButtons.btn2 = {
			text : cancelBtn,
			click :	function() { $(this).scrollTop(0); $(this).dialog( "close" ); }
		};
	if($("#messageResponseAlertBox.ui-dialog-content").length){$("#messageResponseAlertBox.ui-dialog-content").dialog("destroy");}
	$("#messageResponseAlertBox")
	.html("<section class='innerBorder'><img src='"+imgLocalDir+"icn_AlertLrg.png' height='51' width='58' border='0'><h4><strong>"+alertTitle+"</strong></h4><article>"+unblockDeviceMsg+"</article></section>").dialog($.extend(true, {}, genralAlertDialogOptions),{ buttons: unblockDeviceButtons });
	btnStatus(unblockBtn);
}

// ----------------------------------------------------------------------------------------------------

function hideDevice($this){
	snglSlideFadeTransition("hide", $("#deviceDetails")); 
	$("#contentID").val(""); $("#mainContent").removeClass("edit");
	if($this){$this.toggleClass("selected"); }
	else{ if($("#deviceList").find("li.selected").length){ $("#deviceList").find("li.selected").toggleClass("selected"); } } }


// ----------------------------------------------------------------------------------------------------

function deviceSelectionCheck(deviceID, eventAction)
{	
	var itemID = deviceID.split("_")[1],
		$this = $("#md_"+itemID),
		appID = $("#deviceACVal").val();
	if(eventAction === "hide"){ 
		var stateObj = { itemObject: itemID, action: eventAction, appType: appID };
		crntPageAction = stateObj.action;
		history.pushState(stateObj, null, location.pathname+"?pgActn="+stateObj.action+"&appID="+stateObj.appType);
		hideDevice($this); }
	else { 
		if($("#contentID").val() != itemID || (eventAction == "edit" && !$(".mainContent").hasClass("form")) || (eventAction == "display" && $(".mainContent").hasClass("form"))){
			var stateObj = { itemObject: itemID, action: eventAction, appType: appID };
			crntPageAction = stateObj.action;
			history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&appID="+stateObj.appType);
			loadDeviceDetails(stateObj.itemObject, stateObj.action); } } }

// ----------------------------------------------------------------------------------------------------

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
function popDeviceAction(tempItemID, tempEvent, appID, stateObj){ 
	if(appID != $("#deviceACVal").val()){ 
		autoAppSelection = true;
		$("#deviceAC").autoselector("setSelectedValue", appID, tempItemID);
		if(tempItemID !== ''){ loadDeviceDetails(tempItemID, tempEvent); }
		else{ hideDevice(''); }}
	else if(stateObj === null || (tempItemID === "New" && tempEvent === "display")){ var $this = (tempItemID)? $("#user_"+tempItemID) : ""; hideDevice($this); }
	else { if(tempItemID && tempItemID !== ''){ loadDeviceDetails(tempItemID, tempEvent); } else { hideDevice(''); }} }
///////////////////////////////////////////////////

//--------------------------------------------------------------------------------------

var autoAppSelection = false; //set based on how the app is being selected

function devices(deviceID, pageEvent, appType)
{
	loadDeviceList();

	var appData = JSON.parse($("#appListData").text());
	appData = appData.apps;
	var $appField = $("#formReleasedApps");

	$("#deviceAC").autoselector({
			"isComboBox": true, 
			"defaultValue": null,
			"shouldCategorize": false,
			"data": appData
		})
		.on("itemSelected", function(event, ui){
			var	slctValue = $("#deviceAC").autoselector("getSelectedValue"),
				stateObj = { itemObject: '', action: '', appType: slctValue };
			if(!autoAppSelection){ 
				snglSlideFadeTransition("hide", $("#deviceDetails")); $("#contentID").val("");
				crntPageAction = stateObj.action;
				history.pushState(stateObj, null, location.pathname+"?itemID="+stateObj.itemObject+"&pgActn="+stateObj.action+"&appID="+stateObj.appType); }
			else{ autoAppSelection = false; }
			loadDeviceList();
			if($(this).parents("ul.filterForm")){ filterQuerry = "&filterValue="+slctValue }
			$(this).trigger('blur');
			return false; })
		.on("change", function(){
			if($("#deviceAC").val() === ""){ loadDeviceList(); }});

/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '',
		tempApp = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action;
		tempApp = stateObj.appType }
	popDeviceAction(tempItemID, tempEvent, tempApp, stateObj); }

if(!popStateEvent){
	if(deviceID !== undefined && deviceID!== ''){ popDeviceAction(deviceID, pageEvent, appType); }
	else if(appType !== undefined && appType !== ''){ popDeviceAction(deviceID, 'hide', appType); } }
///////////////////////////////////////////////////	

	$(document.body).on("click", "#deviceList * li", function(event){ event.preventDefault();
		var actionEvent = ($(this).hasClass("selected"))? "hide" : "display";
		var itemID = $(this).attr("id");
		if(pausePropagation === true){
			pausePropagationLoop = setInterval(function(){ 
				if(pausePropagation === false){ deviceSelectionCheck(itemID, actionEvent); pausePropagationLoop = window.clearInterval(pausePropagationLoop); }
				else if(pausePropagation === "done"){ pausePropagationLoop = window.clearInterval(pausePropagationLoop); pausePropagation = false }}, 100); }
		else{ deviceSelectionCheck(itemID, actionEvent); }} );
		
	$(document.body).on("click", "#deviceList * .edit", function(event){ event.preventDefault();
		deviceSelectionCheck($(this).attr("id"), "edit");
	});
	
	$(document.body).on("click", ".removeFile", function(event){ event.preventDefault();
		if($(this).parents("#formAssignedAppList") && $(".mainContent").hasClass("editForm")){ 
			if($appField.val() === ""){ $(this).parent("li").addClass("released"); $appField.val($(this).attr("id").split("_")[1]); } else { $appField.val($appField.val() + "," + $(this).attr("id").split("_")[1]); } }
	});
	
	$(document.body).on("click", ".undo", function(event){ event.preventDefault();
		if($(this).parents("#formAssignedAppList") && $(".mainContent").hasClass("editForm")){ $(this).parent("li").removeClass("released"); $appField.val(stringArrayValCleanUp($appField.val(), $(this).attr("id").split("_")[1])); } 
	});
	
	$(document.body).on("click", ".btnBlkDevice", function(event){ event.preventDefault();
		if($(this).parents("form#deviceSettings").length){
			var deviceID = $("#contentID").val(); }
		else{ var deviceID = $(this).attr("id").split("_")[1]; } 
		verifyBlockDevice(deviceID); });
	
	$(document.body).on("click", ".btnUnBlkDevice", function(event){ event.preventDefault();
		if($(this).parents("form#deviceSettings").length){
			var deviceID = $("#contentID").val(); }
		else{ var deviceID = $(this).attr("id").split("_")[1]; }
		verifyUnblockDevice(deviceID); });
	
	$("#deviceSettings").on('submit', function(event){ event.preventDefault();
		event.preventDefault();
		var params = $(this).serialize();
		var ajaxSaveDevice = GetHttpObject({ "triggerSelector": (event) ? event.target : null });
		ajaxSaveDevice.onreadystatechange = function(){
			if (ajaxSaveDevice.readyState==4 && ajaxSaveDevice.status == 200){
				var response = ajaxSaveDevice.responseText.split(":");
				if(response[0] == "true"){
					noteDialog(deviceSavedMsg);
					var cleanQuerryString = scrubQueryString(document.location.search.substring(1), ["filterValue", "itemValue"]);
					crntPageAction = (crntPageAction === "edit")? "display" : crntPageAction;
					var newLocation = location.pathname+"?"+cleanQuerryString+filterQuerry+"&itemValue="+$("#contentID").val()+"&pgActn="+crntPageAction;
					setTimeout(function(){window.location = newLocation}, 500); } }
			else if(ajaxSaveDevice.readyState==4){
				alertDialog(failedSaveMsg); } };// Location changes and/or submission is unable to Save.
		ajaxSaveDevice.open("POST","/secure/settings/devices/saveDevice.html",true);
		ajaxSaveDevice.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajaxSaveDevice.setRequestHeader("Content-length", params.length);
		ajaxSaveDevice.setRequestHeader("Connection", "close");
		ajaxSaveDevice.send(params);
	});
	
	$("#deviceSettings").on("click", ".save", function(event){ event.preventDefault(); $("#deviceSettings").trigger('submit'); });
	
	$("#deviceSettings").on("click", ".cancel", function(event){ event.preventDefault(); cancelConfirm("reload"); });
}

//-------------------------------------------------------------------------

function systemNotes(noteID, pageEvent){
/////////////////////////////////////////////////// USED FOR AJAX HISTORY PAGE LOAD
$("#contentID").val("");
var popStateEvent = false;
window.onpopstate = function(event) {
	popStateEvent = true;
	var	stateObj = (event.state)? parseJSON(JSON.stringify(event.state)) : null,
		tempItemID = '',
		tempEvent = '';
	if(stateObj !== null){
		tempItemID = stateObj.itemObject;
		tempEvent = stateObj.action; }
	viewNotification(tempItemID, tempEvent); }

if(!popStateEvent && (noteID !== undefined && noteID !== '')){ viewNotification(noteID, pageEvent); }
///////////////////////////////////////////////////	
}
