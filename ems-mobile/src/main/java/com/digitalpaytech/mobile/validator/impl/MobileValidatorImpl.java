package com.digitalpaytech.mobile.validator.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.mobile.validator.MobileValidator;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("mobileValidator")
public class MobileValidatorImpl implements MobileValidator {
    
    @Autowired
    private MobileSessionTokenService mobileSessionTokenService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    /**
     * Verifies that the time stamp is not blank, and is a properly formatted timestamp (matches yyyy-MM-dd'T'HH:mm:ss).
     * Does not verify that the time stamp is the correct time, nor does it compare with other timestamps to catch duplicate API calls.
     */
    @Override
    public final Date validateTimestamp(final String timestamp, final String methodName) throws ApplicationException {
        Date timestampDate = null;
        if (timestamp == null) {
            // throw new
            // IncorrectCredentialsException("13 Mobile validation error in " +
            // methodName + ": no timestamp provided");
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TIMESTAMP);
        }
        if (timestamp.trim().isEmpty()) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TIMESTAMP);
        }
        try {
            timestampDate = DateUtil.convertFromRESTDateString(timestamp);
        } catch (ApplicationException e) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TIMESTAMP);
        }
        return timestampDate;
    }
    
    @Override
    public final MobileSessionToken validateSessionToken(final String sessionToken, final String methodName) throws ApplicationException {
        // closely modeled after RestValidator.validateSessionToken()
        if (sessionToken == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TOKEN);
        }
        final MobileSessionToken token = this.mobileSessionTokenService.findMobileSessionTokenBySessionToken(sessionToken);
        if (token == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TOKEN);
        }
        final Date currDate = new Date();
        if (token.getExpiryDate().before(currDate)) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_TOKEN);
        }
        return token;
    }
    
    @Override
    public final void validateSignatureVersion(final String signatureVersion) throws ApplicationException {
        final String versionStr = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MOBILE_SIGNATURE_VERSION,
                                                                             EmsPropertiesService.DEFAULT_MOBILE_SIGNATURE_VERSION);
        final String[] versions = versionStr.trim().split(RestCoreConstants.MULTI_VALUE_SEPERATOR);
        for (String version : versions) {
            if (signatureVersion.trim().equals(version.trim())) {
                return;
            }
        }
        throw new IncorrectCredentialsException(MobileConstants.ERROR_INVALID_SIGNATURE_VERSION);
    }
    
    @Override
    public final void validateActiveIndicator(final String active) throws ApplicationException {
        if ("1".equals(active)) {
            return;
        }
        if ("0".equals(active)) {
            return;
        }
        throw new IncorrectCredentialsException(MobileConstants.ERROR_ACTIVE_PARAMETER_INCORRECT);
    }
    
    /**
     * Verifies that the route supplied as a parameter by the mobile device is
     * assigned to the same customer as the user account associated with the
     * session token. Throws: IncorrectCredentialsException if the customer
     * object reference does not match the expected value.
     */
    @Override
    public final void validateRouteForUserAccount(final int userAccountId, final int routeId) throws ApplicationException {
        final UserAccount userAccount = this.userAccountService.findUserAccount(userAccountId);
        final Customer userCustomer = userAccount.getCustomer();
        
        final Route route = this.routeService.findRouteById(routeId);
        if (route == null) {
            throw new ApplicationException(MobileConstants.ERROR_ROUTE_DOES_NOT_EXIST);
        }
        final Customer routeCustomer = route.getCustomer();
        
        if (userCustomer != routeCustomer) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_ROUTE_NOT_FOR_CUSTOMER);
        }
    }
    
    @Override
    public PointOfSale validateSerialNumber(String serialNumber, String methodName) throws ApplicationException {
        PointOfSale pointOfSale = null;
        if (serialNumber == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_SERIALNUMBER);
        }
        if (serialNumber.length() < WebSecurityConstants.MIN_SERIALNUMBER_LENGTH || serialNumber.length() > WebSecurityConstants.MAX_SERIALNUMBER_LENGTH) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_SERIALNUMBER);
        }
        if (!serialNumber.matches(WebCoreConstants.REGEX_ALPHANUMERIC)) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_SERIALNUMBER);
        }
        pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
        if (pointOfSale == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_SERIALNUMBER);
        }
        return pointOfSale;
    }
    
    @Override
    public void validateCollectionType(Integer collectionTypeId) throws ApplicationException {
        if (collectionTypeId == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_COLLECTION_TYPE);
        }
        if (collectionTypeId != WebCoreConstants.COLLECTION_TYPE_BILL && collectionTypeId != WebCoreConstants.COLLECTION_TYPE_COIN
            && collectionTypeId != WebCoreConstants.COLLECTION_TYPE_CARD) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_UNABLE_TO_VALIDATE_COLLECTION_TYPE);
        }
        
    }
    
}
