package com.digitalpaytech.mobile.validator;

import java.util.Date;

import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.exception.ApplicationException;

public interface MobileValidator {
    Date validateTimestamp(String timestamp, String methodName) throws ApplicationException;
    
    MobileSessionToken validateSessionToken(String sessionToken, String methodName) throws ApplicationException;

    PointOfSale validateSerialNumber(String serialNumber, String methodName) throws ApplicationException;

    void validateSignatureVersion(String signatureVersion) throws ApplicationException;
    
    void validateRouteForUserAccount(int userAccountId, int routeId) throws ApplicationException;
    
    void validateActiveIndicator(String active) throws ApplicationException;

    void validateCollectionType(Integer collectionTypeId) throws ApplicationException;

}
