package com.digitalpaytech.mobile.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileAppActivityType;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileDevice;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.mobile.rest.MobileDTO;
import com.digitalpaytech.dto.mobile.rest.MobileExceptionDTO;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
@RequestMapping(value = "*")
public class MobileAccessController extends MobileBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(MobileAccessController.class);
    
    /**
     * 
     * @param request
     * @param response
     * @param deviceUid
     * @param applicationId
     * @param friendlyName
     * @param userName
     * @param password
     * @param timestamp
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/provision", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileDTO getSecret(final HttpServletRequest request, final HttpServletResponse response, @RequestParam("device") final String deviceUid,
                                     @RequestParam("application") final String applicationId, @RequestParam("friendlyname") final String friendlyName,
                                     @RequestParam("j_username") final String userName, @RequestParam("j_password") final String password,
                                     @RequestParam("timestamp") final String timestamp) {
        if (!isValidRequest(request, MobileConstants.NO_OF_PARAMS_IN_PROVISION_REQUEST)) {
            response.setStatus(STATUS_CODE_404);
            return null;
        }
        try {
            return super.getSecret(deviceUid, applicationId, friendlyName, userName, password, timestamp);
        } catch (ApplicationException e) {
            if (e instanceof IncorrectCredentialsException) {
                LOGGER.info(e.getMessage());
            } else {
                LOGGER.error(e.getMessage());
            }
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_PROVISION + e.getMessage());
        }
    }
    
    /**
     * 
     * @param request
     * @param response
     * @param deviceId
     * @param applicationId
     * @param userName
     * @param password
     * @param latitude
     * @param longitude
     * @param timestamp
     * @param signature
     * @param signatureVersion
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/login", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileDTO getToken(final HttpServletRequest request, final HttpServletResponse response, 
                                    @RequestParam(MobileConstants.PARAM_NAME_DEVICE_ID) final String deviceId,
                                    @RequestParam(MobileConstants.PARAM_NAME_APPLICATION_ID) final String applicationId, 
                                    @RequestParam(MobileConstants.PARAM_NAME_USER_NAME) final String userName,
                                    @RequestParam(MobileConstants.PARAM_NAME_USER_PASSWORD) final String password, 
                                    @RequestParam(value = MobileConstants.PARAM_NAME_LAT, defaultValue = "") final String latitude,
                                    @RequestParam(value = MobileConstants.PARAM_NAME_LONG, defaultValue = "") final String longitude, 
                                    @RequestParam(MobileConstants.PARAM_NAME_TIMESTAMP) final String timestamp,
                                    @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE) final String signature, 
                                    @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE_VERSION) final String signatureVersion) {
        
        if (!isValidRequest(request, MobileConstants.NO_OF_PARAMS_IN_LOGIN_REQUEST, MobileConstants.PARAM_NAME_LAT, MobileConstants.PARAM_NAME_LONG)) {
            response.setStatus(STATUS_CODE_404);
            return null;
        }
        try {
            return super.getToken(request.getServerName(), deviceId, applicationId, userName, password, latitude, longitude, timestamp, signature,
                                  signatureVersion);
        } catch (ApplicationException e) {
            if (e instanceof IncorrectCredentialsException) {
                // TODO change to info or debug
                LOGGER.error(e.getMessage());
            } else {
                LOGGER.error(e.getMessage());
            }
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_LOGIN + e.getMessage());
        }
    }
   
    /**
     * 
     * @param request
     * @param response
     * @param token
     * @param latitude
     * @param longitude
     * @param timestamp
     * @param signature
     * @param signatureVersion
     */
    @RequestMapping(method = RequestMethod.GET, value = "/logout", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final void releaseToken(final HttpServletRequest request, final HttpServletResponse response,
                                   @RequestParam(MobileConstants.PARAM_NAME_SESSION_TOKEN) final String token,
                                   @RequestParam(value = MobileConstants.PARAM_NAME_LAT, defaultValue = "") final String latitude,
                                   @RequestParam(value = MobileConstants.PARAM_NAME_LONG, defaultValue = "") final String longitude,
                                   @RequestParam(MobileConstants.PARAM_NAME_TIMESTAMP) final String timestamp,
                                   @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE) final String signature,
                                   @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE_VERSION) final String signatureVersion) {
        
        if (!isValidRequest(request, MobileConstants.NO_OF_PARAMS_IN_LOGOUT_REQUEST, MobileConstants.PARAM_NAME_LAT, MobileConstants.PARAM_NAME_LONG)) {
            response.setStatus(STATUS_CODE_404);
            return;
        }
        try {
            super.releaseToken(request.getServerName(), token, latitude, longitude, timestamp, signature, signatureVersion);
        } catch (ApplicationException e) {
            if (e instanceof IncorrectCredentialsException) {
                LOGGER.info(e.getMessage());
            } else {
                LOGGER.error(e.getMessage());
            }
        }
    }
 
    /**
     * The mobileAuthenticationProcessingFilter will redirect to this path if the username + password authentication fails.
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/fail", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileExceptionDTO sendError(final HttpServletRequest request, final HttpServletResponse response) {
        // This method will ALWAYS return an invalid credential error to the user, but first it needs to 
        // save the failed request into the mobileAppActivity table - if possible
        final UserAccount user = userAccountService.findUndeletedUserAccount(request.getParameter(MobileConstants.PARAM_NAME_USER_NAME));
        if (user == null) {
            return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
        }
        final Customer customer = user.getCustomer();
        if (customer == null) {
            return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
        }
        final String uid = request.getParameter(MobileConstants.PARAM_NAME_DEVICE_ID);
        if (uid == null) {
            return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
        }
        
        MobileAppActivityType activityType = null;
        MobileApplicationType appType = null;
        final String appTypeString = request.getParameter(MobileConstants.PARAM_NAME_APPLICATION_ID);
        if (request.getParameter(MobileConstants.PARAM_NAME_SIGNATURE) == null) {
            activityType = mobileAppActivityTypeService.findById(WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION);
            appType = getAppType(appTypeString);
            if (appType == null) {
                return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
            }
        } else if (request.getParameter(MobileConstants.PARAM_NAME_SESSION_TOKEN) == null) {
            activityType = mobileAppActivityTypeService.findById(WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN);
            appType = getAppType(appTypeString);
            if (appType == null) {
                return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
            }
        } else {
            return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
        }
        CustomerMobileDevice customerMobileDevice = customerMobileDeviceService.findCustomerMobileDeviceByCustomerAndDeviceUid(customer.getId(), uid);
        MobileDevice mobileDevice = null;
        if (customerMobileDevice != null) {
            mobileDevice = customerMobileDevice.getMobileDevice();
        } else {
            mobileDevice = new MobileDevice();
            mobileDevice.setUid(uid);
            mobileDeviceService.saveMobileDevice(mobileDevice);
            customerMobileDevice = new CustomerMobileDevice();
            customerMobileDevice.setCustomer(customer);
            customerMobileDevice.setIsBlocked((byte) 0);
            customerMobileDevice.setMobileDevice(mobileDevice);
            customerMobileDeviceService.save(customerMobileDevice);
        }
        //TODO determine whether to log only registration attempts
        if (activityType.getId() == WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION) {
            mobileAppHistoryService.logActivity(customer, user, appType, customerMobileDevice, activityType.getId(), false, "Authentication failure");
        }
        
        return new MobileExceptionDTO(MobileConstants.ERROR_INVALID_USERNAME_PASSWORD);
    }
    
    /**
     * 
     * @param appTypeString
     * @return
     */
    private MobileApplicationType getAppType(final String appTypeString) {
        if (appTypeString == null) {
            return null;
        }
        Integer appTypeInt = null;
        try {
            appTypeInt = Integer.parseInt(appTypeString);
        } catch (NumberFormatException e) {
            return null;
        }
        return mobileApplicationTypeService.getMobileApplicationTypeById(appTypeInt);
    }
}
