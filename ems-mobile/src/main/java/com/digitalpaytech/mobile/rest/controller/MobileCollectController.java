package com.digitalpaytech.mobile.rest.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosCollectionUser;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserAccountRoute;
import com.digitalpaytech.dto.PointOfSaleAlertInfo;
import com.digitalpaytech.dto.mobile.rest.CollectCollectedDTO;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationStatusDTO;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryDTO;
import com.digitalpaytech.dto.mobile.rest.CollectRouteStatusDTO;
import com.digitalpaytech.dto.mobile.rest.CollectRouteSummaryDTO;
import com.digitalpaytech.dto.mobile.rest.CollectStatusDTO;
import com.digitalpaytech.dto.mobile.rest.CollectSummaryDTO;
import com.digitalpaytech.dto.mobile.rest.MobileCollectCollectedQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileCollectStatusQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileCollectSummaryQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileDTO;
import com.digitalpaytech.dto.mobile.rest.MobileExceptionDTO;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.mobile.validator.MobileValidator;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.MobileApplicationTypeService;
import com.digitalpaytech.service.MobileDeviceService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountRouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.paystation.PosCollectionUserService;
import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
@RequestMapping(value = "/collect")
public class MobileCollectController extends MobileBaseController {
    // time to subtract from the timestamp on status updates 
    //- larger time is more likely to duplicate results and less likely to miss alert status changes
    // that occurred during the execution of the getStatus() method
    private static final int STATUS_SYNC_DELAY_MS = 1000;
    private static final Logger LOGGER = Logger.getLogger(MobileBaseController.class);
    private static final String LOGGER_MSG_FAILED_SIGNATURE = "Failed signature verification in ";
    private static final String LOGGER_MSG_EXTEND_SESSION_TOKEN_ERROR = "Unexpected error trying to extend the session token ";
    
    @Autowired
    CustomerAdminService customerAdminService;
    
    @Autowired
    MobileDeviceService mobileDeviceService;
    
    @Autowired
    MobileLicenseService mobileAppService;
    
    @Autowired
    EmsPropertiesService emsPropertiesService;
    
    @Autowired
    MobileSessionTokenService mobileSessionTokenService;
    
    @Autowired
    PointOfSaleService pointOfSaleService;
    
    @Autowired
    RouteService routeService;
    
    @Autowired
    MobileValidator mobileValidator;
    
    @Autowired
    EncryptionService encryptionService;
    
    @Autowired
    UserAccountRouteService userAccountRouteService;
    
    @Autowired
    UserAccountService userAccountService;
    
    @Autowired
    PosCollectionUserService posCollectionUserService;
    
    @Autowired
    CollectionTypeService collectionTypeService;
    
    @Autowired
    MobileApplicationTypeService mobileApplicationTypeService;
    
    /**
     * 
     * @param request
     * @param response
     * @param sessionToken
     * @param latitude
     * @param longitude
     * @param timestamp
     * @param signature
     * @param signatureVersion
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/summary", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileDTO getSummary(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam(MobileConstants.PARAM_NAME_SESSION_TOKEN) final String sessionToken,
        @RequestParam(value = MobileConstants.PARAM_NAME_LAT, defaultValue = "") final String latitude,
        @RequestParam(value = MobileConstants.PARAM_NAME_LONG, defaultValue = "") final String longitude,
        @RequestParam(MobileConstants.PARAM_NAME_TIMESTAMP) final String timestamp,
        @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE) final String signature,
        @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE_VERSION) final String signatureVersion) {
        
        if (!isValidRequest(request, MobileConstants.NO_OF_PARAMS_IN_SUMMARY_REQUEST, MobileConstants.PARAM_NAME_LAT, MobileConstants.PARAM_NAME_LONG)) {
            response.setStatus(STATUS_CODE_404);
            return null;
        }
        MobileSessionToken token = null;
        final String methodName = this.getClass().getName() + ".getSummary";
        try {
            token = this.mobileValidator.validateSessionToken(sessionToken, methodName);
            this.mobileValidator.validateSignatureVersion(signatureVersion);
            this.mobileValidator.validateTimestamp(timestamp, methodName);
        } catch (ApplicationException e) {
            LOGGER.info("Validation failed in " + methodName);
            LOGGER.info(e.getMessage());
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + e.getMessage());
        }
        final int customerId = token.getUserAccount().getCustomer().getId();
        final MobileLicense mobileKey = token.getMobileLicense();
        final String secretKey = mobileKey.getSecretKey();
        final MobileCollectSummaryQueryParamList param = new MobileCollectSummaryQueryParamList();
        param.setSessionToken(sessionToken);
        param.setLatitude(latitude);
        param.setLongitude(longitude);
        param.setTimestamp(timestamp);
        param.setSignatureVersion(signatureVersion);
        if (!this.encryptionService
                .verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_GET, signature, param, null, secretKey)) {
            LOGGER.info(LOGGER_MSG_FAILED_SIGNATURE + methodName);
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + MobileConstants.ERROR_SIGNATURE_DOES_NOT_MATCH);
        }
        
        final CollectSummaryDTO info = new CollectSummaryDTO();
        info.setPaystations(getPaystationInfos(customerId));
        info.setRoutes(getRouteInfos(customerId));
        try {
            updateCoordinates(latitude, longitude, token);
        } catch (ApplicationException e1) {
            LOGGER.info("Encountered application error in " + methodName);
            LOGGER.info(e1.getMessage());
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + e1.getMessage());
        }
        try {
            token.setExpiryDate(createExpiryDate(new Date()));
        } catch (Exception e) {
            LOGGER.error(LOGGER_MSG_EXTEND_SESSION_TOKEN_ERROR + sessionToken + " => " + methodName);
        }
        token.setLastModifiedGmt(new Date());
        this.mobileSessionTokenService.saveMobileSessionToken(token);
        return info;
    }
    
    /**
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/status", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileDTO getStatus(final HttpServletRequest request, final HttpServletResponse response) {
        
        final String sessionToken = request.getParameter("sessiontoken");
        final String latitude = request.getParameter(MobileConstants.PARAM_NAME_LAT) == null ? "" : request
                .getParameter(MobileConstants.PARAM_NAME_LAT);
        final String longitude = request.getParameter(MobileConstants.PARAM_NAME_LONG) == null ? "" : request
                .getParameter(MobileConstants.PARAM_NAME_LONG);
        final String route = request.getParameter(MobileConstants.PARAM_NAME_ROUTE) == null ? "" : request
                .getParameter(MobileConstants.PARAM_NAME_ROUTE);
        final String lastUpdateString = request.getParameter(MobileConstants.PARAM_NAME_LAST_UPDATE) == null ? "" : request
                .getParameter(MobileConstants.PARAM_NAME_LAST_UPDATE);
        final String activeString = request.getParameter(MobileConstants.PARAM_NAME_ACTIVE) == null ? "0" : request
                .getParameter(MobileConstants.PARAM_NAME_ACTIVE);
        final String timestamp = request.getParameter(MobileConstants.PARAM_NAME_TIMESTAMP);
        final String signature = request.getParameter(MobileConstants.PARAM_NAME_SIGNATURE);
        final String signatureVersion = request.getParameter(MobileConstants.PARAM_NAME_SIGNATURE_VERSION);
        
        //	    if(!isValidRequest(request, 9, "lat", "lon", "route", "lastupdate")){
        //	        response.setStatus(STATUS_CODE_404);
        //	        return null;
        //	    }
        if (StringUtils.isEmpty(sessionToken) || StringUtils.isEmpty(activeString) || StringUtils.isEmpty(timestamp)
            || StringUtils.isEmpty(signature) || StringUtils.isEmpty(signatureVersion)) {
            response.setStatus(STATUS_CODE_404);
            final String logString = "Missing parameter, one of: sessionToken=" + sessionToken + ", activeString=" + activeString + ", timestamp="
                                     + timestamp + ", signature=" + signature + ", signatureVersion=" + signatureVersion;
            LOGGER.error(logString);
            return null;
        }
        
        MobileSessionToken token = null;
        final CollectStatusDTO updates = new CollectStatusDTO();
        final Date processingDate = new Date();
        processingDate.setTime(processingDate.getTime() - STATUS_SYNC_DELAY_MS);
        updates.setTimeStamp(processingDate);
        final String methodName = this.getClass().getName() + ".getStatus()";
        try {
            
            token = this.mobileValidator.validateSessionToken(sessionToken, methodName);
            this.mobileValidator.validateTimestamp(timestamp, methodName);
            this.mobileValidator.validateSignatureVersion(signatureVersion);
            this.mobileValidator.validateActiveIndicator(activeString);
            
            final MobileLicense mobileKey = token.getMobileLicense();
            final String secretKey = mobileKey.getSecretKey();
            final int userAccountId = token.getUserAccount().getId();
            
            int routeId = 0;
            if (route != null && !"".equals(route)) {
                try {
                    routeId = Integer.parseInt(route);
                } catch (NumberFormatException nfe) {
                    response.setStatus(STATUS_CODE_404);
                    return null;
                }
            }
            int userCount = 0;
            if (routeId != 0) {
                try {
                    this.mobileValidator.validateRouteForUserAccount(userAccountId, routeId);
                } catch (ApplicationException e) {
                    response.setStatus(STATUS_CODE_404);
                    return null;
                }
                changeRouteForUser(userAccountId, routeId, mobileKey.getMobileApplicationType().getId());
                userCount = (int) this.userAccountRouteService.getUserCountForRouteAppIdAndCurrentSession(routeId, mobileKey
                        .getMobileApplicationType().getId(), new Date());
            } else {
                clearRouteForUser(userAccountId, mobileKey.getMobileApplicationType().getId());
            }
            
            final MobileCollectStatusQueryParamList param = new MobileCollectStatusQueryParamList();
            param.setSessionToken(sessionToken);
            param.setLatitude(latitude);
            param.setLongitude(longitude);
            param.setRoute(route);
            param.setLastUpdate(lastUpdateString);
            param.setActive(activeString);
            param.setTimestamp(timestamp);
            param.setSignatureVersion(signatureVersion);
            
            if (!this.encryptionService.verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_GET, signature, param, null,
                                                            secretKey)) {
                LOGGER.info(LOGGER_MSG_FAILED_SIGNATURE + methodName);
                return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_STATUS + MobileConstants.ERROR_SIGNATURE_DOES_NOT_MATCH);
            }
            
            final Customer customer = mobileKey.getCustomer();
            final int customerId = customer.getId();
            
            Date lastUpdate;
            if (WebCoreConstants.EMPTY_STRING.equals(lastUpdateString)) {
                lastUpdate = null;
            } else {
                try {
                    final Long ms = Long.parseLong(lastUpdateString);
                    lastUpdate = new Date(ms);
                } catch (NumberFormatException nfe) {
                    lastUpdate = null;
                    return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_STATUS + MobileConstants.ERROR_UNABLE_TO_PARSE_LASTUPDATED);
                }
            }
            
            HashMap<Integer, CollectPaystationStatusDTO> posById = new HashMap<Integer, CollectPaystationStatusDTO>();
            HashMap<Integer, HashMap<Short, Date>> mostRecentCleared = new HashMap<Integer, HashMap<Short, Date>>();
            HashMap<Integer, HashMap<Short, Date>> earliestCreated = new HashMap<Integer, HashMap<Short, Date>>();
            HashMap<Integer, HashMap<Short, Integer>> mostSevere = new HashMap<Integer, HashMap<Short, Integer>>();
            for (PointOfSaleAlertInfo info : this.pointOfSaleService.findActivePointOfSalesWithRecentHeartbeatOrCollection(customerId, routeId,
                                                                                                                           lastUpdate)) {
                // add pay stations that have had heart beats or collections
                addPaystationStatus(posById, mostRecentCleared, earliestCreated, mostSevere, info);
            }
            if (lastUpdate == null) {
                for (PointOfSaleAlertInfo info : this.pointOfSaleService.findActivePointOfSalesWithCollectionAlertsInfo(customerId, routeId == 0
                        ? null : routeId, null, 0, true, false)) {
                    addPaystationStatus(posById, mostRecentCleared, earliestCreated, mostSevere, info);
                }
                // calculate beginning of customer's day in UTC
                Date midnight = null;
                final CustomerProperty timezoneProperty = this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                String timezoneString;
                if (timezoneProperty == null) {
                    timezoneString = WebCoreConstants.SERVER_TIMEZONE_GMT;
                } else {
                    timezoneString = timezoneProperty.getPropertyValue();
                }
                TimeZone timezone;
                timezone = TimeZone.getTimeZone(timezoneString);
                final Calendar calendar = Calendar.getInstance(timezone);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                midnight = calendar.getTime();
                for (PointOfSaleAlertInfo info : this.pointOfSaleService.findActivePointOfSalesWithCollectionAlertsInfo(customerId, routeId == 0
                        ? null : routeId, midnight, 0, false, true)) {
                    addPaystationStatus(posById, mostRecentCleared, earliestCreated, mostSevere, info);
                }
            } else {
                for (PointOfSaleAlertInfo info : this.pointOfSaleService.findActivePointOfSalesWithCollectionAlertsInfo(customerId, routeId == 0
                        ? null : routeId, lastUpdate, 0, false, false)) {
                    addPaystationStatus(posById, mostRecentCleared, earliestCreated, mostSevere, info);
                }
            }
            this.applyAlertStatusbyPOS(posById, mostRecentCleared, earliestCreated, mostSevere);
            final List<CollectRouteStatusDTO> routes = new LinkedList<CollectRouteStatusDTO>();
            if (routeId == 0) {
                for (Route r : this.routeService.findRoutesByCustomerIdAndRouteTypeId(customerId, WebCoreConstants.ROUTE_TYPE_COLLECTIONS)) {
                    final CollectRouteStatusDTO routeStatus = new CollectRouteStatusDTO();
                    final Set<RoutePOS> routePoses = r.getRoutePOSes();
                    for (RoutePOS routePOS : routePoses) {
                        final Integer id = routePOS.getPointOfSale().getId();
                        final CollectPaystationStatusDTO paystation = posById.get(id);
                        // searches for each pay station belonging to a route and adds it if it is mapped by posById
                        // i.e. if it has recent changes to its alert status, collection times, or heart beat
                        if (paystation != null) {
                            
                            routeStatus.addPaystation(paystation);
                        }
                    }
                    routeStatus.setId(r.getId());
                    routeStatus.setName(r.getName());
                    routeStatus.setUserCount((int) this.userAccountRouteService.getUserCountForRouteAppIdAndCurrentSession(r.getId(), mobileKey
                            .getMobileApplicationType().getId(), new Date()));
                    routes.add(routeStatus);
                }
            } else {
                final Route r = this.routeService.findRouteById(routeId);
                final CollectRouteStatusDTO routeStatus = new CollectRouteStatusDTO();
                final Set<RoutePOS> routePoses = r.getRoutePOSes();
                for (RoutePOS routePOS : routePoses) {
                    final Integer id = routePOS.getPointOfSale().getId();
                    final CollectPaystationStatusDTO paystation = posById.get(id);
                    // searches for each pay station belonging to a route and adds it if it is mapped by posById
                    // i.e. if it has recent changes to its alert status
                    if (paystation != null) {
                        routeStatus.addPaystation(paystation);
                    }
                }
                routeStatus.setId(r.getId());
                routeStatus.setName(r.getName());
                routeStatus.setUserCount(userCount);
                routes.add(routeStatus);
            }
            updates.setRoutes(routes);
            updateCoordinates(latitude, longitude, token);
            if ("1".equals(activeString)) {
                try {
                    token.setExpiryDate(createExpiryDate(new Date()));
                } catch (Exception e) {
                    LOGGER.error(LOGGER_MSG_EXTEND_SESSION_TOKEN_ERROR + sessionToken + " in " + methodName);
                }
                token.setLastModifiedGmt(new Date());
                this.mobileSessionTokenService.saveMobileSessionToken(token);
            }
            
        } catch (ApplicationException e) {
            if (e instanceof IncorrectCredentialsException) {
                LOGGER.info("Encountered credentials exception in " + methodName);
                LOGGER.info(e.getMessage());
            } else {
                LOGGER.error("Encountered application exception in " + methodName);
                LOGGER.error(e.getMessage());
            }
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_STATUS + e.getMessage());
        }
        return updates;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/collected", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_JSON)
    @ResponseBody
    public final MobileDTO getCollected(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam(MobileConstants.PARAM_NAME_SESSION_TOKEN) final String sessionToken,
        @RequestParam(value = MobileConstants.PARAM_NAME_LAT, defaultValue = "") final String latitude,
        @RequestParam(value = MobileConstants.PARAM_NAME_LONG, defaultValue = "") final String longitude,
        @RequestParam(MobileConstants.PARAM_NAME_TIMESTAMP) final String timestamp,
        @RequestParam(MobileConstants.PARAM_NAME_COLLECTION_TYPE) final Integer collectionTypeId,
        @RequestParam(MobileConstants.PARAM_NAME_SERIAL_NUMBER) final String serialNumber,
        @RequestParam(MobileConstants.PARAM_NAME_MOBILE_START_GMT) final String mobileStartGmt,
        @RequestParam(MobileConstants.PARAM_NAME_MOBILE_END_GMT) final String mobileEndGmt,
        @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE) final String signature,
        @RequestParam(MobileConstants.PARAM_NAME_SIGNATURE_VERSION) final String signatureVersion) {
        
        if (!isValidRequest(request, MobileConstants.NO_OF_PARAMS_IN_COLLECTED_REQUEST, MobileConstants.PARAM_NAME_LAT,
                            MobileConstants.PARAM_NAME_LONG)) {
            response.setStatus(STATUS_CODE_404);
            return null;
        }
        MobileSessionToken token = null;
        Date timestampDate = null;
        Date mobileStartDate = null;
        Date mobileEndDate = null;
        PointOfSale pointOfSale = null;
        final String methodName = this.getClass().getName() + ".getSummary";
        try {
            token = this.mobileValidator.validateSessionToken(sessionToken, methodName);
            this.mobileValidator.validateSignatureVersion(signatureVersion);
            timestampDate = this.mobileValidator.validateTimestamp(timestamp, methodName);
            mobileStartDate = this.mobileValidator.validateTimestamp(mobileStartGmt, methodName);
            mobileEndDate = this.mobileValidator.validateTimestamp(mobileEndGmt, methodName);
            pointOfSale = this.mobileValidator.validateSerialNumber(serialNumber, methodName);
            this.mobileValidator.validateCollectionType(collectionTypeId);
        } catch (ApplicationException e) {
            LOGGER.info("Validation failed in " + methodName);
            LOGGER.info(e.getMessage());
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + e.getMessage());
        }
        final int customerId = token.getUserAccount().getCustomer().getId();
        final MobileLicense mobileKey = token.getMobileLicense();
        final String secretKey = mobileKey.getSecretKey();
        final MobileCollectCollectedQueryParamList param = new MobileCollectCollectedQueryParamList();
        param.setSessionToken(sessionToken);
        param.setLatitude(latitude);
        param.setLongitude(longitude);
        param.setCollectionTypeId(collectionTypeId);
        param.setTimestamp(timestamp);
        param.setSerialNumber(serialNumber);
        param.setMobileStartGmt(mobileStartGmt);
        param.setMobileEndGmt(mobileEndGmt);
        param.setSignatureVersion(signatureVersion);
        if (!this.encryptionService.verifyHMACSignature(request.getServerName(), RestCoreConstants.HTTP_METHOD_POST, signature, param, null,
                                                        secretKey)) {
            LOGGER.info(LOGGER_MSG_FAILED_SIGNATURE + methodName);
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + MobileConstants.ERROR_SIGNATURE_DOES_NOT_MATCH);
        }
        
        try {
            updateCoordinates(latitude, longitude, token);
        } catch (ApplicationException e1) {
            LOGGER.info("Encountered application error in " + methodName);
            LOGGER.info(e1.getMessage());
            return new MobileExceptionDTO(MobileConstants.ERROR_PREFIX_SUMMARY + e1.getMessage());
        }
        
        int interval = EmsPropertiesService.DEFAULT_COLLECTION_USER_INTERVAL;
        try {
            interval = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.COLLECTION_USER_INTERVAL,
                                                                       EmsPropertiesService.DEFAULT_COLLECTION_USER_INTERVAL);
        } catch (Exception e) {
            // Don't need to do anything use default value
        }
        
        final Calendar startCal = Calendar.getInstance();
        startCal.setTime(mobileEndDate);
        startCal.add(Calendar.MINUTE, -interval);
        final Date startTime = startCal.getTime();
        final Calendar endCal = (Calendar) startCal.clone();
        endCal.add(Calendar.MINUTE, 2 * interval);
        final Date endTime = endCal.getTime();
        
        List<PosCollectionUser> posCollectionUserList = this.posCollectionUserService
                .findPosCollectionUserExistingPosCollectionListByCollectionType(pointOfSale.getId(), collectionTypeId, startTime, endTime);
        
        if (posCollectionUserList != null && !posCollectionUserList.isEmpty()) {
            for (PosCollectionUser collectionUser : posCollectionUserList) {
                collectionUser.setMobileStartGmt(mobileStartDate);
                collectionUser.setMobileEndGmt(mobileEndDate);
                collectionUser.setMobileReceivedGmt(timestampDate);
                collectionUser.setUserAccount(token.getUserAccount());
            }
        } else {
            posCollectionUserList = new ArrayList<PosCollectionUser>();
            final PosCollectionUser collectionUser = new PosCollectionUser();
            collectionUser.setMobileStartGmt(mobileStartDate);
            collectionUser.setMobileEndGmt(mobileEndDate);
            collectionUser.setMobileReceivedGmt(timestampDate);
            collectionUser.setUserAccount(token.getUserAccount());
            collectionUser.setCollectionType(this.collectionTypeService.findCollectionType(collectionTypeId));
            collectionUser.setPointOfSale(pointOfSale);
            posCollectionUserList.add(collectionUser);
        }
        
        this.posCollectionUserService.savePosCollectionUserList(posCollectionUserList);
        
        final CollectCollectedDTO info = new CollectCollectedDTO();
        info.setStatus("OK");
        try {
            token.setExpiryDate(createExpiryDate(new Date()));
        } catch (Exception e) {
            LOGGER.error(LOGGER_MSG_EXTEND_SESSION_TOKEN_ERROR + sessionToken + " => " + methodName);
        }
        token.setLastModifiedGmt(new Date());
        this.mobileSessionTokenService.saveMobileSessionToken(token);
        return info;
    }
    
    /**
     * Given a PointOfSaleAlertInfo object, adds the POS related data to the map if it doesn't already exist there.
     * Also adds any QUALIFYING alert-related data to the alert maps - mostRecentCleared, earliestCreated, mostSevere.
     * 
     * After all of the pos objects have been added this way, the alert maps will store at most one alert
     * timestamp or severity in each map per POS per alert type. (Bill, Coin, Card, Running Total)
     * 
     * @param posById
     * @param mostRecentCleared
     * @param earliestCreated
     * @param mostSevere
     * @param info
     */
    private void addPaystationStatus(final Map<Integer, CollectPaystationStatusDTO> posById,
        final HashMap<Integer, HashMap<Short, Date>> mostRecentCleared, final HashMap<Integer, HashMap<Short, Date>> earliestCreated,
        final HashMap<Integer, HashMap<Short, Integer>> mostSevere, final PointOfSaleAlertInfo info) {
        final Integer psId = info.getPosId();
        CollectPaystationStatusDTO paystation = posById.get(psId);
        if (paystation == null) {
            paystation = new CollectPaystationStatusDTO();
            paystation.setId(info.getPosId());
            paystation.setName(info.getPosName());
            paystation.setSerialNumber(info.getSerialNumber());
            paystation.setHeartBeat(info.getHeartBeat());
            paystation.setLastBillCollection(info.getLastBillCollection());
            paystation.setLastCardCollection(info.getLastCardCollection());
            paystation.setLastCoinCollection(info.getLastCoinCollection());
            paystation.setLastCollection(info.getLastCollection());
            posById.put(psId, paystation);
        }
        HashMap<Short, Date> clearedMap = mostRecentCleared.get(psId);
        if (clearedMap == null) {
            clearedMap = new HashMap<Short, Date>();
            mostRecentCleared.put(psId, clearedMap);
        }
        HashMap<Short, Date> createdMap = earliestCreated.get(psId);
        if (createdMap == null) {
            createdMap = new HashMap<Short, Date>();
            earliestCreated.put(psId, createdMap);
        }
        HashMap<Short, Integer> severityMap = mostSevere.get(psId);
        if (severityMap == null) {
            severityMap = new HashMap<Short, Integer>();
            mostSevere.put(psId, severityMap);
        }
        
        // the pos info object might not have any alert data.
        // enter this block only if there is alert data.
        // (can assume if info.getIsActive() !=null, all alert fields are !=null)
        if (info.getIsActive() != null) {
            final int severity = info.getEventSeverity();
            final short type = info.getAlertTypeId();
            if (info.getIsActive()) {
                if (severityMap.get(type) == null || severityMap.get(type) < severity) {
                    severityMap.put(type, severity);
                }
                final Date oldest = createdMap.get(type);
                if (oldest == null || (info.getCreated() != null && oldest.getTime() > info.getCreated().getTime())) {
                    createdMap.put(type, info.getCreated());
                }
            } else {
                final Date mostRecent = clearedMap.get(type);
                if (mostRecent == null || (info.getCleared() != null && mostRecent.getTime() < info.getCleared().getTime())) {
                    clearedMap.put(type, info.getCleared());
                }
            }
        }
    }
    
    /**
     * Requires that all of the POS info objects have been stored in posById by way of addPaystationStatus(~~).
     * This method will then finish populating the POS dto objects with the alert information from the alert maps
     * (mostRecentCleared, earliestCreated, mostSevere).
     * 
     * @param posById
     * @param mostRecentCleared
     * @param earliestCreated
     * @param mostSevere
     */
    private void applyAlertStatusbyPOS(final Map<Integer, CollectPaystationStatusDTO> posById,
        final HashMap<Integer, HashMap<Short, Date>> mostRecentCleared, final HashMap<Integer, HashMap<Short, Date>> earliestCreated,
        final HashMap<Integer, HashMap<Short, Integer>> mostSevere) {
        for (CollectPaystationStatusDTO paystation : posById.values()) {
            HashMap<Short, Date> clearedMap = mostRecentCleared.get(paystation.getId());
            if (clearedMap == null) {
                clearedMap = new HashMap<Short, Date>();
            }
            HashMap<Short, Date> createdMap = earliestCreated.get(paystation.getId());
            if (createdMap == null) {
                createdMap = new HashMap<Short, Date>();
            }
            HashMap<Short, Integer> severityMap = mostSevere.get(paystation.getId());
            if (severityMap == null) {
                severityMap = new HashMap<Short, Integer>();
            }
            final Date billCreated = createdMap.get(WebCoreConstants.ALERTTYPE_ID_BILLSTACKER);
            final Date coinCreated = createdMap.get(WebCoreConstants.ALERTTYPE_ID_COINCANNISTER);
            final Date creditCreated = createdMap.get(WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD);
            final Date runningCreated = createdMap.get(WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL);
            final Date overdueCreated = createdMap.get(WebCoreConstants.ALERTTYPE_ID_LASTSEENCOLLECTION);
            
            final Integer billSeverity = severityMap.get(WebCoreConstants.ALERTTYPE_ID_BILLSTACKER);
            final Integer coinSeverity = severityMap.get(WebCoreConstants.ALERTTYPE_ID_COINCANNISTER);
            final Integer creditSeverity = severityMap.get(WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD);
            final Integer runningSeverity = severityMap.get(WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL);
            final Integer overdueSeverity = severityMap.get(WebCoreConstants.ALERTTYPE_ID_LASTSEENCOLLECTION);
            
            if (billCreated != null && billSeverity != null) {
                paystation.addCreatedAlertType(MobileConstants.JSON_NAME_FIELD_BILL, billCreated, billSeverity);
            }
            if (coinCreated != null && coinSeverity != null) {
                paystation.addCreatedAlertType(MobileConstants.JSON_NAME_FIELD_COIN, coinCreated, coinSeverity);
            }
            if (creditCreated != null && creditSeverity != null) {
                paystation.addCreatedAlertType(MobileConstants.JSON_NAME_FIELD_CREDIT, creditCreated, creditSeverity);
            }
            if (runningCreated != null && runningSeverity != null) {
                paystation.addCreatedAlertType(MobileConstants.JSON_NAME_FIELD_RUNNING_TOTAL, runningCreated, runningSeverity);
            }
            if (overdueCreated != null && overdueSeverity != null) {
                paystation.addCreatedAlertType(MobileConstants.JSON_NAME_FIELD_OVERDUE_COLLECTION, overdueCreated, overdueSeverity);
            }
            final Date billCleared = clearedMap.get(WebCoreConstants.ALERTTYPE_ID_BILLSTACKER);
            final Date coinCleared = clearedMap.get(WebCoreConstants.ALERTTYPE_ID_COINCANNISTER);
            final Date creditCleared = clearedMap.get(WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD);
            final Date runningCleared = clearedMap.get(WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL);
            final Date overdueCleared = clearedMap.get(WebCoreConstants.ALERTTYPE_ID_LASTSEENCOLLECTION);
            if (billCleared != null) {
                paystation.addClearedAlertType(MobileConstants.JSON_NAME_FIELD_BILL, billCleared);
            }
            if (coinCleared != null) {
                paystation.addClearedAlertType(MobileConstants.JSON_NAME_FIELD_COIN, coinCleared);
            }
            if (creditCleared != null) {
                paystation.addClearedAlertType(MobileConstants.JSON_NAME_FIELD_CREDIT, creditCleared);
            }
            if (runningCleared != null) {
                paystation.addClearedAlertType(MobileConstants.JSON_NAME_FIELD_RUNNING_TOTAL, runningCleared);
            }
            if (overdueCleared != null) {
                paystation.addClearedAlertType(MobileConstants.JSON_NAME_FIELD_OVERDUE_COLLECTION, overdueCleared);
            }
        }
    }
    
    /**
     * Returns a list of meta information for each valid (is provisioned, is visible, is activated,
     * not decommissioned, not deleted) pay station belonging to the customer, as well as a list of
     * collection route ids to which each pay station is assigned.
     * 
     * @param customerId
     * @return List<name, id, location, List<routes>>
     */
    private List<CollectPaystationSummaryDTO> getPaystationInfos(final int customerId) {
        final List<CollectPaystationSummaryDTO> paystationInfos = this.pointOfSaleService
                .findValidAndActivatedPointOfSalesByCustomerIdAndRouteType(customerId, WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        
        return paystationInfos;
    }
    
    /**
     * Returns a list of meta information for each collection route
     * 
     * @param customerId
     * @return List<routeName, routeId>
     */
    private List<CollectRouteSummaryDTO> getRouteInfos(final int customerId) {
        final List<CollectRouteSummaryDTO> routeInfos = new LinkedList<CollectRouteSummaryDTO>();
        final Collection<Route> routes = this.routeService.findRoutesByCustomerIdAndRouteTypeId(customerId, WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        CollectRouteSummaryDTO routeInfo;
        for (Route route : routes) {
            routeInfo = new CollectRouteSummaryDTO();
            routeInfo
                    .setPaystationsWithCollectionAlerts((int) this.pointOfSaleService.countPointOfSalesRequiringCollections(route.getId(),
                                                                                                                            WebCoreConstants.SEVERITY_MINOR));
            routeInfo.setId(route.getId());
            routeInfo.setName(route.getName());
            routeInfo.setUserCount((int) this.userAccountRouteService.getUserCountForRoute(route.getId()));
            routeInfos.add(routeInfo);
        }
        return routeInfos;
    }
    
    /**
     * 
     * @param userAccountId
     * @param routeId
     * @param mobileApplicationTypeId
     */
    private void changeRouteForUser(final int userAccountId, final int routeId, final int mobileApplicationTypeId) {
        final List<UserAccountRoute> userAccountRoutes = this.userAccountRouteService.findUserAccountRouteByUserAccountId(userAccountId);
        final Route route = this.routeService.findRouteById(routeId);
        UserAccountRoute userAccountRoute;
        if (userAccountRoutes.size() == 0) {
            final UserAccount userAccount = this.userAccountService.findUserAccount(userAccountId);
            userAccountRoute = new UserAccountRoute(userAccount, route);
        } else {
            userAccountRoute = userAccountRoutes.get(0);
        }
        userAccountRoute.setMobileApplicationType(this.mobileApplicationTypeService.getMobileApplicationTypeById(mobileApplicationTypeId));
        userAccountRoute.setRoute(route);
        this.userAccountRouteService.saveOrUpdate(userAccountRoute);
    }
}
