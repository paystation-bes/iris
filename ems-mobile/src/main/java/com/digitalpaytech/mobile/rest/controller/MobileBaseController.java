package com.digitalpaytech.mobile.rest.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileAppActivityType;
import com.digitalpaytech.domain.MobileAppHistory;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileDevice;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserAccountRoute;
import com.digitalpaytech.dto.mobile.rest.MobileDTO;
import com.digitalpaytech.dto.mobile.rest.MobileLoginParamList;
import com.digitalpaytech.dto.mobile.rest.MobileLogoutParamList;
import com.digitalpaytech.dto.mobile.rest.MobileSecretDTO;
import com.digitalpaytech.dto.mobile.rest.MobileTokenDTO;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.mobile.validator.MobileValidator;
import com.digitalpaytech.service.CustomerMobileDeviceService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.MobileAppActivityTypeService;
import com.digitalpaytech.service.MobileAppHistoryService;
import com.digitalpaytech.service.MobileApplicationTypeService;
import com.digitalpaytech.service.MobileDeviceService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.service.UserAccountRouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class MobileBaseController {
    
    protected static final int STATUS_CODE_404 = 404;
    private static final String LOGGER_MSG_NO_KEY_FOR_TOKEN = "No key for token ";
    private static final String LOGGER_MSG_NO_TOKEN_FOUND = "No token found for ";
    private static final Logger LOGGER = Logger.getLogger(MobileBaseController.class);
    
    @Autowired
    protected MobileAppActivityTypeService mobileAppActivityTypeService;
    
    @Autowired
    protected MobileAppHistoryService mobileAppHistoryService;
    
    @Autowired
    protected MobileApplicationTypeService mobileApplicationTypeService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    protected UserAccountService userAccountService;
    
    @Autowired
    protected MobileDeviceService mobileDeviceService;
    
    @Autowired
    protected CustomerMobileDeviceService customerMobileDeviceService;
    
    @Autowired
    private MobileLicenseService mobileLicenseService;
    
    @Autowired
    private MobileValidator mobileValidator;
    
    @Autowired
    private MobileSessionTokenService mobileSessionTokenService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    protected EncryptionService encryptionService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private UserAccountRouteService userAccountRouteService;
    
    public final MobileDTO getSecret(final String deviceUid, final String applicationId, final String friendlyName, final String userName,
                                     final String password, final String timestamp) throws ApplicationException {
        this.mobileValidator.validateTimestamp(timestamp, this.getClass().getName() + "getSecret()");
        String encodedUserName = null;
        try {
            encodedUserName = URLEncoder.encode(userName, WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            //LOGGER.error("AuthenticationServiceException", e);
            throw new ApplicationException(MobileConstants.ERROR_USER_NOT_FOUND);
        }
        final UserAccount user = this.userAccountService.findUndeletedUserAccount(encodedUserName);
        if (user == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_USER_NOT_FOUND);
        }
        final Customer customer = user.getCustomer();
        final int customerId = customer.getId();
        
        MobileDevice mobileDevice = this.mobileDeviceService.findMobileDeviceByUid(deviceUid);
        CustomerMobileDevice customerMobileDevice = null;
        if (mobileDevice != null) {
            final int mobileDeviceId = mobileDevice.getId();
            if (this.mobileDeviceService.getIsMobileDeviceBlocked(customerId, mobileDeviceId)) {
                this.logActivity(user, applicationId, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION, false,
                                 "Cannot provision device: device is blocked");
                throw new IncorrectCredentialsException(MobileConstants.ERROR_DEVICE_BLOCKED);
            }
            customerMobileDevice = this.customerMobileDeviceService.findCustomerMobileDeviceByCustomerAndDeviceUid(customerId, deviceUid);
            if (customerMobileDevice == null) {
                customerMobileDevice = new CustomerMobileDevice();
                customerMobileDevice.setCustomer(customer);
                customerMobileDevice.setMobileDevice(mobileDevice);
                customerMobileDevice.setIsBlocked((byte) 0);
                this.customerMobileDeviceService.save(customerMobileDevice);
            }
        }
        
        if (mobileDevice == null) {
            mobileDevice = new MobileDevice();
            mobileDevice.setUid(deviceUid);
            this.mobileDeviceService.saveMobileDevice(mobileDevice);
            customerMobileDevice = new CustomerMobileDevice();
            customerMobileDevice.setCustomer(customer);
            customerMobileDevice.setMobileDevice(mobileDevice);
            customerMobileDevice.setIsBlocked((byte) 0);
            this.customerMobileDeviceService.save(customerMobileDevice);
        }
        
        int appId;
        try {
            appId = Integer.parseInt(applicationId);
        } catch (NumberFormatException nfe) {
            throw new ApplicationException(MobileConstants.ERROR_APPLICATION_ID_NOT_INTEGER);
        }
        final int deviceId = customerMobileDevice.getId();
        final MobileLicense provisionedLicenceKey = this.mobileLicenseService.findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(customerId, appId,
                                                                                                                                      deviceId);
        if (provisionedLicenceKey != null) {
            // in case mobile device is already provisioned simply return existing key
            // this.logActivity(user, applicationId, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION, true, "Device was already provisioned");
            final String newKey = this.encryptionService.createHMACSecretKey();
            provisionedLicenceKey.setSecretKey(newKey);
            this.mobileLicenseService.saveOrUpdateMobileLicense(provisionedLicenceKey);
            return new MobileSecretDTO(provisionedLicenceKey.getSecretKey());
        }
        final MobileLicense mlk = this.mobileLicenseService.findAvailableMobileLicenseByCustomerAndApplication(customerId, appId);
        if (mlk == null) {
            this.logActivity(user, applicationId, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION, false, "No available license to provision");
            throw new IncorrectCredentialsException(MobileConstants.ERROR_NO_AVAILABLE_KEY);
        }
        final String secretKeyString = this.encryptionService.createHMACSecretKey();
        mlk.setSecretKey(secretKeyString);
        final MobileSecretDTO mobileSecret = new MobileSecretDTO(secretKeyString);
        mlk.setMobileLicenseStatusType(this.mobileLicenseService.findMobileLicenseStatusTypeById(WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED));
        mlk.setCustomerMobileDevice(customerMobileDevice);
        this.mobileLicenseService.saveOrUpdateMobileLicense(mlk);
        this.logActivity(user, applicationId, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION, true, "Successfully provisioned device");
        final int licenseUsed = (int) this.mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appId);
        final int licenseCount = (int) this.mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appId);
        final MobileApplicationType mobileApplicationType = this.mobileApplicationTypeService.getMobileApplicationTypeById(appId);
        final int subscriptionTypeId = mobileApplicationType.getSubscriptionType().getId();
        this.customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customerId, subscriptionTypeId, licenseCount, licenseUsed, user.getId());
        return mobileSecret;
    }
    
    /**
     * 
     * @param hostName
     * @param deviceUid
     * @param applicationIdString
     * @param userName
     * @param password
     * @param latitude
     * @param longitude
     * @param timestamp
     * @param signature
     * @param signatureVersion
     * @return
     * @throws ApplicationException
     */
    public final MobileDTO getToken(final String hostName, final String deviceUid, final String applicationIdString, final String userName, final String password,
                              final String latitude, final String longitude, final String timestamp, final String signature, final String signatureVersion)
            throws ApplicationException {
        this.mobileValidator.validateTimestamp(timestamp, this.getClass().getName() + "getToken()");
        this.mobileValidator.validateSignatureVersion(signatureVersion);
        String encodedUserName = null;
        try {
            encodedUserName = URLEncoder.encode(userName, WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("AuthenticationServiceException", e);
        }
        final UserAccount user = this.userAccountService.findUndeletedUserAccount(encodedUserName);
        if (user == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_USER_NOT_FOUND);
        }
        final Customer customer = user.getCustomer();
        final int customerId = customer.getId();
        final int applicationId = Integer.parseInt(applicationIdString);
        clearRouteForUser(user.getId(), applicationId);
        final CustomerMobileDevice customerDevice = this.customerMobileDeviceService.findCustomerMobileDeviceByCustomerAndDeviceUid(customerId, deviceUid);
        if (customerDevice == null) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_DEVICE_NOT_FOUND);
        }
        final int deviceId = customerDevice.getId();
        if (customerDevice.getIsBlocked() == 1) {
            //this.logActivity(user, applicationIdString, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN, false, "Device is blocked");
            throw new IncorrectCredentialsException(MobileConstants.ERROR_DEVICE_BLOCKED);
        }
        final MobileLicense key = this.mobileLicenseService.findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(customerId, applicationId, deviceId);
        if (key == null) {
            //this.logActivity(user, applicationIdString, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN, false, "No provisioned license");
            throw new IncorrectCredentialsException(MobileConstants.ERROR_KEY_NOT_FOUND);
        }
        final String secretKey = key.getSecretKey();
        
        final MobileLoginParamList param = new MobileLoginParamList();
        param.setDeviceUid(deviceUid);
        param.setApplicationId(applicationIdString);
        param.setUserName(userName);
        param.setPassword(password);
        param.setLatitude(latitude);
        param.setLongitude(longitude);
        param.setTimestamp(timestamp);
        param.setSignature(signature);
        param.setSignatureVersion(signatureVersion);
        
        if (!this.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_GET, signature, param, null, secretKey)) {
            //this.logActivity(user, applicationIdString, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN, false, "Signature does not match");
            throw new IncorrectCredentialsException(MobileConstants.ERROR_SIGNATURE_DOES_NOT_MATCH);
        }
        
        final int userAccountId = user.getId();
        MobileSessionToken token = this.mobileSessionTokenService.findMobileSessionTokenByUserAccountAndMobileLicense(userAccountId, key.getId());
        if (token == null) {
            token = new MobileSessionToken();
        }
        if (StringUtils.isBlank(latitude)) {
            token.setLatitude(null);
            token.setLongitude(null);
        } else {
            try {
                token.setLatitude(new BigDecimal(latitude));
                token.setLongitude(new BigDecimal(longitude));
            } catch (NumberFormatException e) {
                throw new InvalidDataException(MobileConstants.ERROR_LATITUDE_LONGITUDE_CANNOT_PARSE);
            }
        }
        
        String sessionToken = null;
        final Date currentTime = new Date();
        Date expiryDate = null;
        try {
            expiryDate = createExpiryDate(currentTime);
        } catch (Exception e) {
            throw new InvalidDataException(MobileConstants.ERROR_SYSTEM_ERROR + ": Failed to update expiry time for mobile session token");
        }
        // invalidate other tokens:
        // all other tokens for the same user and app (e.g. same user on different device)
        // all other tokens for the same license (e.g. different user on same device)
        // tokens for other apps are still valid - user can be on multiple devices if using different apps
        final List<MobileSessionToken> tokensToInvalidate = this.mobileSessionTokenService.findMobileSessionTokenByUserAccountIdAndApplicationId(userAccountId,
                                                                                                                                                 applicationId);
        tokensToInvalidate.addAll(this.mobileSessionTokenService.findMobileSessionTokensByMobileLicense(key.getId()));
        for (MobileSessionToken mst : tokensToInvalidate) {
            if (mst != token) {
                mst.setExpiryDate(currentTime);
            }
        }
        
        if (token.getExpiryDate() == null || token.getExpiryDate().before(currentTime)) {
            while (true) {
                sessionToken = RandomStringUtils.randomAlphanumeric(RestCoreConstants.SESSION_TOKEN_LENGTH);
                final long resultCount = this.mobileSessionTokenService.getTokenCount(sessionToken);
                if (resultCount == 0) {
                    break;
                }
            }
            token.setMobileLicense(key);
            token.setCreationDate(currentTime);
            token.setExpiryDate(expiryDate);
            token.setLastModifiedByUserId(user.getId());
            token.setLastModifiedGmt(currentTime);
            token.setSessionToken(sessionToken);
            token.setUserAccount(user);
            this.mobileSessionTokenService.saveMobileSessionToken(token);
            //this.logActivity(user, applicationIdString, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN, true, "Successfully logged in");
            return new MobileTokenDTO(token.getSessionToken());
        } else {
            sessionToken = token.getSessionToken();
            token.setExpiryDate(expiryDate);
            token.setLastModifiedByUserId(user.getId());
            token.setLastModifiedGmt(currentTime);
            this.mobileSessionTokenService.updateMobileSessionToken(token);
            //this.logActivity(user, applicationIdString, deviceUid, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_LOGIN, true, "Successfully logged in");
            return new MobileTokenDTO(token.getSessionToken());
        }
    }
    
    /**
     * 
     * @param hostName
     * @param token
     * @param latitude
     * @param longitude
     * @param timestamp
     * @param signature
     * @param signatureVersion
     * @throws ApplicationException
     */
    public final void releaseToken(final String hostName, final String token, final String latitude, final String longitude, final String timestamp,
                                   final String signature, final String signatureVersion) throws ApplicationException {
        try {
            this.mobileValidator.validateTimestamp(timestamp, this.getClass().getName() + ".releaseToken()");
        } catch (ApplicationException e) {
            throw new IncorrectCredentialsException();
        }
        
        final MobileSessionToken mobileToken = this.mobileSessionTokenService.findMobileSessionTokenBySessionToken(token);
        if (mobileToken == null) {
            throw new IncorrectCredentialsException(LOGGER_MSG_NO_TOKEN_FOUND + token);
        }
        final MobileLicense mlk = mobileToken.getMobileLicense();
        if (mlk == null) {
            throw new IncorrectCredentialsException(LOGGER_MSG_NO_KEY_FOR_TOKEN + token);
        } else {
            this.clearRouteForUser(mobileToken.getUserAccount().getId(), mlk.getMobileApplicationType().getId());
        }
        final String secretKey = mlk.getSecretKey();
        if (secretKey == null) {
            throw new IncorrectCredentialsException(LOGGER_MSG_NO_KEY_FOR_TOKEN + token);
        }
        final MobileLogoutParamList param = new MobileLogoutParamList();
        param.setSessionToken(token);
        param.setLatitude(latitude);
        param.setLongitude(longitude);
        param.setTimestamp(timestamp);
        param.setSignature(signature);
        param.setSignatureVersion(signatureVersion);
        if (!this.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_GET, signature, param, null, secretKey)) {
            throw new IncorrectCredentialsException(MobileConstants.ERROR_SIGNATURE_DOES_NOT_MATCH);
        }
        if (mobileToken.getExpiryDate().after(new Date())) {
            mobileToken.setExpiryDate(new Date());
        }
        updateCoordinates(latitude, longitude, mobileToken);
        this.mobileSessionTokenService.saveMobileSessionToken(mobileToken);
        
    }
    
    /**
     * 
     * @param currentTime
     * @return
     * @throws Exception
     */
    protected final Date createExpiryDate(final Date currentTime) throws Exception {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(currentTime);
        final int interval = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.MOBILE_SESSION_TOKEN_EXPIRY_INTERVAL,
                                                                             EmsPropertiesService.DEFAULT_MOBILE_SESSION_TOKEN_EXPIRY_INTERVAL);
        cal.add(Calendar.MINUTE, interval);
        return cal.getTime();
    }
    
    /**
     * 
     * @param latitude
     * @param longitude
     * @param token
     * @throws ApplicationException
     */
    protected final void updateCoordinates(final String latitude, final String longitude, final MobileSessionToken token) throws ApplicationException {
        if (StringUtils.isBlank(latitude)) {
            return;
        } else {
            try {
                token.setLatitude(new BigDecimal(latitude));
                token.setLongitude(new BigDecimal(longitude));
            } catch (NumberFormatException e) {
                throw new ApplicationException(MobileConstants.ERROR_LATITUDE_LONGITUDE_CANNOT_PARSE);
            }
        }
    }
    
    /**
     * 
     * @param userAccount
     * @param appTypeString
     * @param uid
     * @param activityTypeInt
     * @param isSuccessful
     * @param result
     */
    protected final void logActivity(final UserAccount userAccount, final String appTypeString, final String uid, final Integer activityTypeInt,
                                     final boolean isSuccessful, final String result) {
        int appTypeInt;
        final MobileAppActivityType activityType = this.mobileAppActivityTypeService.findById(activityTypeInt);
        if (activityType == null) {
            return;
        }
        try {
            appTypeInt = Integer.parseInt(appTypeString);
        } catch (NumberFormatException e) {
            return;
        }
        final MobileApplicationType appType = this.mobileApplicationTypeService.getMobileApplicationTypeById(appTypeInt);
        final Customer customer = userAccount.getCustomer();
        CustomerMobileDevice customerMobileDevice = this.customerMobileDeviceService.findCustomerMobileDeviceByCustomerAndDeviceUid(customer.getId(), uid);
        MobileDevice mobileDevice = null;
        if (customerMobileDevice != null) {
            mobileDevice = customerMobileDevice.getMobileDevice();
        } else {
            mobileDevice = new MobileDevice();
            mobileDevice.setUid(uid);
            this.mobileDeviceService.saveMobileDevice(mobileDevice);
            customerMobileDevice = new CustomerMobileDevice();
            customerMobileDevice.setCustomer(customer);
            customerMobileDevice.setIsBlocked((byte) 0);
            customerMobileDevice.setMobileDevice(mobileDevice);
            this.customerMobileDeviceService.save(customerMobileDevice);
        }
        final MobileAppHistory history = new MobileAppHistory();
        history.setCustomerMobileDevice(customerMobileDevice);
        history.setUserAccount(userAccount);
        history.setTimestamp(new Date());
        history.setIsSuccessful(isSuccessful ? (byte) 1 : 0);
        history.setResult(result);
        history.setMobileAppActivityType(activityType);
        history.setMobileApplicationType(appType);
        this.mobileAppHistoryService.logActivity(customer, userAccount, appType, customerMobileDevice, activityTypeInt, isSuccessful, result);
    }
    
    /**
     * Determines whether the original request had the correct number of parameters. Will return false
     * if the request has too many parameters, or if only "lat" or "lon" appear in the request alone.
     * 
     * @param request
     * @param expectedParamCount
     * @param optionalParams
     * @return
     */
    protected final boolean isValidRequest(final HttpServletRequest request, final int expectedParamCount, final String... optionalParams) {
        int missingOptParams = 0;
        for (String optionalParam : optionalParams) {
            if (!request.getParameterMap().containsKey(optionalParam)) {
                missingOptParams++;
            }
        }
        if (request.getParameterMap().size() + missingOptParams > expectedParamCount) {
            return false;
        }
        if (!isLatLonTogether(request)) {
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param request
     * @return
     */
    protected final boolean isLatLonTogether(final HttpServletRequest request) {
        return (request.getParameter(MobileConstants.PARAM_NAME_LAT) == null) == (request.getParameter(MobileConstants.PARAM_NAME_LONG) == null);
    }
    
    /**
     * 
     * @param userAccountId
     * @param mobileApplicationTypeId
     */
    protected final void clearRouteForUser(final int userAccountId, final int mobileApplicationTypeId) {
        final UserAccountRoute userAccountRoute = this.userAccountRouteService.findUserAccountRouteByUserAccountIdAndApplicationId(userAccountId,
                                                                                                                                   mobileApplicationTypeId);
        if (userAccountRoute == null) {
            return;
        }
        this.userAccountRouteService.delete(userAccountRoute);
    }
}