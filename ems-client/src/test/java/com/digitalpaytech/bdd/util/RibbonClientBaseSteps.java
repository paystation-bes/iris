package com.digitalpaytech.bdd.util;

import java.nio.charset.Charset;
import java.text.MessageFormat;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.digitalpaytech.ribbon.mock.ExecutionInfo;
import com.digitalpaytech.ribbon.mock.MockClientFactory;

import junit.framework.Assert;

public class RibbonClientBaseSteps extends AbstractSteps {
    private static final String MSG_FMT_EXECUTED = "The {1} request has been sent to {0}";
    
    public RibbonClientBaseSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    /**
     * Assert that request is sent to the microservice.
     * 
     * @.example Then a groupInfoByDevice request is sent to facility managment service
     * @param serviceName
     *            name of the target microservice
     */
    @Then("a $requestType request is sent to $serviceName")
    public final void requestSent(final String requestType, final String serviceName) {
        final TestContext ctx = TestContext.getInstance();
        final MockClientFactory clientFactory = MockClientFactory.getInstance();
        
        final Class<?> clientClass = ctx.getObjectBuilder().resolveType(serviceName);
        
        ExecutionInfo exec = clientFactory.executionHistory(clientClass, requestType).poll();
        
        Assert.assertTrue(MessageFormat.format(MSG_FMT_EXECUTED, clientClass.getName(), requestType), exec != null);
    }
    
    /**
     * Assert that request is not sent to the microservice.
     * 
     * @.example Then a groupInfoByDevice request is not sent to facility managment service
     * @param serviceName
     *            name of the target microservice
     */
    @Then("a $requestType request is not sent to $serviceName")
    public final void requestNotSent(final String requestType, final String serviceName) {
        final TestContext ctx = TestContext.getInstance();
        final MockClientFactory clientFactory = MockClientFactory.getInstance();
        
        final Class<?> clientClass = ctx.getObjectBuilder().resolveType(serviceName);
        
        ExecutionInfo exec = clientFactory.executionHistory(clientClass, requestType).poll();
        
        Assert.assertTrue(MessageFormat.format(MSG_FMT_EXECUTED, clientClass.getName(), requestType), exec == null);
    }
    
    /**
     * Prepare to respond the message from micro-service definded by service name back to Iris.
     * 
     * @.example When the facility management service response with message something
     * @param serviceName the micro-service that will respond the message
     * @param message response message
     */
    @Given("the $serviceName response with message $message")
    @When("the $serviceName response with message $message")
    public final void responseWithMessage(final String serviceName, final String message) {
        final TestContext ctx = TestContext.getInstance();
        final MockClientFactory clientFactory = MockClientFactory.getInstance();
        
        final Class<?> clientClass = ctx.getObjectBuilder().resolveType(serviceName);
        
        clientFactory.prepareForSuccessRequest(clientClass, message.getBytes(Charset.defaultCharset()));
    }
}
