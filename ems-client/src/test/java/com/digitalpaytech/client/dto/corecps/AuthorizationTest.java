package com.digitalpaytech.client.dto.corecps;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import com.digitalpaytech.client.dto.Error;
import com.digitalpaytech.client.dto.Status;
import com.digitalpaytech.util.CardProcessingConstants;

public class AuthorizationTest {
    @Test
    public void isApproved() {
        final Authorization authorization = new Authorization();
        final Status status = new Status();
        authorization.setStatus(status);
        assertFalse(authorization.isApproved());
        
        final List<Error<String>> list = new ArrayList<>();
        list.add(new Error<>());
        list.add(new Error<>());
        list.add(new Error<>());
        authorization.getStatus().setErrors(list);
        assertFalse(authorization.isApproved());
        
        authorization.getStatus().getErrors().get(2).setIdentifier(CardProcessingConstants.APPROVED);
        assertTrue(authorization.isApproved());
    }
}
