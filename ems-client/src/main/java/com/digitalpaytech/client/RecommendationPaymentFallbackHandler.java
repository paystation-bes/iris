package com.digitalpaytech.client;

import com.netflix.hystrix.HystrixInvokableInfo;
import com.netflix.ribbon.hystrix.FallbackHandler;

import rx.Observable;

import java.util.Map;

public class RecommendationPaymentFallbackHandler implements FallbackHandler<String> {

    @Override
    public Observable<String> getFallback(HystrixInvokableInfo<?> hystrixInfo, Map<String, Object> requestProperties) {
        return Observable.just("Unable to make the request");
    }
}