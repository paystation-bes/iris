package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@StoryAlias(TelemetryClient.ALIAS)
@ResourceGroup(name = "telemetryserverlink")
public interface TelemetryClient {
    
    String ALIAS = "Telemetry Service";
    String FAILED_SEND_REQUEST_TELEMETRY = "Failed to send request to Telemetry Service";
    
    @Secure(true)
    @TemplateName("firmwareConfig")
    @Http(method = HttpMethod.GET, uri = "/upgrade/details/{deviceSerialNumber}", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getFirmwareConfig(@Var("deviceSerialNumber") String deviceSerialNumber);
    
    @Secure(true)
    @TemplateName("createDevice")
    @Http(method = HttpMethod.POST, uri = "/devices?deviceSerialNumber={deviceSerialNumber}&customerId={customerId}")
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> createDevice(@Var("deviceSerialNumber") String deviceSerialNumber, @Var("customerId") String customerId);
    
    @Secure(true)
    @TemplateName("deleteDevice")
    @Http(method = HttpMethod.DELETE, uri = "/devices?deviceSerialNumber={deviceSerialNumber}")
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> delete(@Var("deviceSerialNumber") String deviceSerialNumber);
}
