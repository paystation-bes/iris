package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.dto.fms.DeviceUser;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;

import io.netty.buffer.ByteBuf;

@StoryAlias(AuthClient.ALIAS)
@ResourceGroup(name = "authserver")
public interface AuthClient {
    String ALIAS = "Auth Service";
    
    @Secure(true)
    @TemplateName("createDeviceUser")
    @Http(method = HttpMethod.POST, uri = "/deviceusers", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> createDeviceUser(@Content DeviceUser deviceUser);
    
    @Secure(true)
    @TemplateName("updateDeviceUser")
    @Http(method = HttpMethod.PUT, uri = "/deviceusers/{userName}", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> updateDeviceUser(@Var("userName") String userName, @Content DeviceUser deviceUser);
    
    @Secure(true)
    @TemplateName("deleteDeviceUser")
    @Http(method = HttpMethod.DELETE, uri = "/deviceusers/{userName}")
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> deleteDeviceUser(@Var("userName") String userName);
    
}
