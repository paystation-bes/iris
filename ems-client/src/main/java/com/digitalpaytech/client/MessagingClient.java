package com.digitalpaytech.client;

import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;
import io.reactivex.netty.channel.StringTransformer;

@ResourceGroup(name = "messagingserver")
public interface MessagingClient {
    @Secure(true)
    @TemplateName("sendMessageRequest")
    @Http(method = HttpMethod.POST, uri = "/sms", headers = { @Header(name = "Content-Type", value = "application/json") })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = RecommendationPaymentResponseValidator.class)
    RibbonRequest<ByteBuf> sendMessageRequest(@Content String str);
    
    @Secure(true)
    @TemplateName("ReplyRequest")
    @Http(method = HttpMethod.PUT, uri = "/conversation/{callbackId}/reply", headers = { @Header(name = "Content-Type", value = "application/json") })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = RecommendationPaymentResponseValidator.class)
    RibbonRequest<ByteBuf> sendReplyRequest(@Var("callbackId") String callbackId, @Content String str);
    
    @Secure(true)
    @TemplateName("endConversationRequest")
    @Http(method = HttpMethod.PUT, uri = "/conversation/{callbackId}/end")
    @Hystrix(validator = RecommendationPaymentResponseValidator.class)
    RibbonRequest<ByteBuf> endConversationRequest(@Var("callbackId") String callbackId);
}
