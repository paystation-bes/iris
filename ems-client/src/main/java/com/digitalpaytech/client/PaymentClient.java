package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;
import io.reactivex.netty.channel.StringTransformer;

@ResourceGroup(name = "paymentserver")
@StoryAlias(PaymentClient.ALIAS)
public interface PaymentClient {
    
    String ALIAS = "payment server";

    @TemplateName("pushEMVData")
    @Http(method = HttpMethod.POST, uri = "/creditcall/cardtransaction", headers={
            @Header(name = "Content-Type", value = "application/json")
    })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> pushEMVData(@Content String str);

    @TemplateName("refundsRealTime")
    @Http(method = HttpMethod.POST, uri = "/refunds/realTime", headers={
            @Header(name = "Content-Type", value = "application/json")
    })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> refundsRealTime(@Content String str);

    @TemplateName("createTerminalRequest")
    @Http(method = HttpMethod.POST, uri = "/terminals", headers={
            @Header(name = "Content-Type", value = "application/json")
    })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> createTerminalRequest(@Content String str);

    @TemplateName("updateTerminalRequest")
    @Http(method = HttpMethod.PUT, uri = "/terminals/{terminalToken}", headers={
            @Header(name = "Content-Type", value = "application/json")
    })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> updateTerminalRequest(@Var("terminalToken") String terminalToken, @Content String str);
        
    @TemplateName("testTerminalRequest")
    @Http(method = HttpMethod.GET, uri = "/terminals/{terminalToken}/test", headers={
            @Header(name = "Content-Type", value = "text/plain")
    })  
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> testTerminalRequest(@Var("terminalToken") String terminalToken);
    
    @TemplateName("grabTerminalInfo")
    @Http(method = HttpMethod.GET, uri = "/terminals/{terminalToken}", headers={
            @Header(name = "Content-Type", value = "text/plain")
    })  
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> grabTerminalInfo(@Var("terminalToken") String terminalToken);
    
    @TemplateName("deleteTerminalRequest")
    @Http(method = HttpMethod.DELETE, uri = "/terminals/{terminalToken}", headers={
            @Header(name = "Content-Type", value = "text/plain")
    })  
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> deleteTerminalRequest(@Var("terminalToken") String terminalToken);

    @TemplateName("addOn")
    @Http(method = HttpMethod.POST, uri = "/addOn", headers={
            @Header(name = "Content-Type", value = "application/json")
    })
    @ContentTransformerClass(StringTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> addOn(@Content String str);    
}
