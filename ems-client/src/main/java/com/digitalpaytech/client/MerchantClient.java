package com.digitalpaytech.client;

import com.digitalpaytech.client.dto.merchant.Merchant;
import com.digitalpaytech.client.dto.merchant.Terminal;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;

import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;
import io.netty.buffer.ByteBuf;
import java.util.SortedMap;

@ResourceGroup(name = "mss")
public interface MerchantClient {
    String MERCHANT_END_POINT_PREFIX = "/api/v1/merchants";
    String TERMINALS_END_POINT_PREFIX = "/api/v1/terminals";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("lookupMerchantMigration")
    @Http(method = HttpMethod.POST, uri = "/api/v1/merchantMigration/lookup?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> lookupMerchantMigration(@Content SortedMap<String, String> merchantDetails, @Var("customerId") int customerId);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("addMerchant")
    @Http(method = HttpMethod.POST, uri = MERCHANT_END_POINT_PREFIX, headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> addMerchant(@Content Merchant merchant);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("updateMerchant")
    @Http(method = HttpMethod.PUT, uri = MERCHANT_END_POINT_PREFIX + "/{merchantToken}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> updateMerchant(@Content Merchant merchant, @Var("merchantToken") String merchantToken);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("addTerminal")
    @Http(method = HttpMethod.POST, uri = MERCHANT_END_POINT_PREFIX + "/{merchantToken}/terminals", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> addTerminal(@Var("merchantToken") String merchantToken, @Content Terminal terminal);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("updateTerminal")
    @Http(method = HttpMethod.PUT, uri = MERCHANT_END_POINT_PREFIX + "/{merchantToken}/terminals/{terminalToken}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> updateTerminal(@Var("merchantToken") String merchantToken, @Var("terminalToken") String terminalToken,
        @Content Terminal terminal);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getTerminals")
    @Http(method = HttpMethod.GET, uri = MERCHANT_END_POINT_PREFIX + "/{merchantToken}/terminals", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getTerminals(@Var("merchantToken") String merchantToken);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getCardReaderConfig")
    @Http(method = HttpMethod.GET, uri = TERMINALS_END_POINT_PREFIX + "/{terminalToken}/cardReaderConfig", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getCardReaderConfig(@Var("terminalToken") String terminalToken);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("forTransaction")
    @Http(method = HttpMethod.POST, uri = TERMINALS_END_POINT_PREFIX + "/{terminalToken}/forTransaction", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> forTransaction(@Var("terminalToken") String terminalToken);    
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("forTransactionForTest")
    @Http(method = HttpMethod.POST, uri = TERMINALS_END_POINT_PREFIX + "/{terminalToken}/forTransaction?forTest={forTest}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> forTransaction(@Var("terminalToken") String terminalToken, @Var("forTest") String forTest);    
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("testTransaction")
    @Http(method = HttpMethod.POST, uri = MERCHANT_END_POINT_PREFIX + "/{merchantToken}/terminals/{terminalToken}/test", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> testTransaction(@Var("merchantToken") String merchantToken, @Var("terminalToken") String terminalToken);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getMerchantsByProcessorType")
    @Http(method = HttpMethod.GET, uri = MERCHANT_END_POINT_PREFIX + "?customerId={customerId}&processorType={processorType}", headers = { 
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getMerchantsByProcessorType(@Var("customerId") Integer customerId, @Var("processorType") String processorType);
}
