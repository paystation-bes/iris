package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "bcs")
@StoryAlias(BadCardClient.ALIAS)
public interface BadCardClient {
    String ALIAS = "Bad Card Service";
    
    String BAD_CARD_RESOLVER_END_POINT = "/api/v1/badcards";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getBadCard")
    @Http(method = HttpMethod.GET, uri = BAD_CARD_RESOLVER_END_POINT + "/{customerId}?cardHash={cardHash}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getBadCard(@Var("customerId") int customerId, @Var("cardHash") String cardHash);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getBadCards")
    @Http(method = HttpMethod.GET, uri = BAD_CARD_RESOLVER_END_POINT + "?customerId={customerId}&page={page}&size={size}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getBadCards(@Var("customerId") int customerId, @Var("page") int page, @Var("size") int size);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getFilteredBadCards")
    @Http(method = HttpMethod.GET, uri = BAD_CARD_RESOLVER_END_POINT + "/filtered?customerId={customerId}&page={page}&size={size}"
                                         + "&last4Digits={last4Digits}", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getFilteredBadCards(@Var("customerId") int customerId, @Var("last4Digits") String last4Digits, @Var("page") int page,
        @Var("size") int size);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getBadCardsCompact")
    @Http(method = HttpMethod.GET, uri = BAD_CARD_RESOLVER_END_POINT + "/compact?customerId={customerId}&type={type}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getBadCardsCompact(@Var("customerId") int customerId, @Var("type") String type);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("deleteBadCards")
    @Http(method = HttpMethod.DELETE, uri = BAD_CARD_RESOLVER_END_POINT + "?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> deleteBadCards(@Var("customerId") int customerId);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getBadCardFileInfo")
    @Http(method = HttpMethod.GET, uri = BAD_CARD_RESOLVER_END_POINT + "/fileInfo?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getBadCardFileInfo(@Var("customerId") int customerId);
}
