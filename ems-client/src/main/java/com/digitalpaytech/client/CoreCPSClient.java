package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.dto.corecps.Payment;
import com.digitalpaytech.client.dto.corecps.PaymentsList;
import com.digitalpaytech.client.dto.corecps.TransactionRequest;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.QueryParamOptional;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "ccps")
@StoryAlias(CoreCPSClient.ALIAS)
public interface CoreCPSClient {
    String ALIAS = "Core CPS";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("preAuth")
    @Http(method = HttpMethod.POST, uri = "/api/v1/authorizations", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> preAuth(@Content Payment payment);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("onlineRefund")
    @Http(method = HttpMethod.POST, uri = "/api/v1/refunds/online", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> onlineRefund(@Content Payment payment);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("capture")
    @Http(method = HttpMethod.PUT, uri = "/api/v1/authorizations/{chargeToken}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> capture(@Var("chargeToken") String chargeToken);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("push")
    @Http(method = HttpMethod.POST, uri = "/api/v1/transactions", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> push(@Content TransactionRequest transactionRequest);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("charge")
    @Http(method = HttpMethod.POST, uri = "/api/v1/payments/online", headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> charge(@Content Payment payment);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("storeAndForward")
    @Http(method = HttpMethod.POST, uri = "/api/v1/payments/storeAndForward", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> storeAndForward(@Content PaymentsList paymentsList);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("reversals")
    @Http(method = HttpMethod.POST, headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @QueryParamOptional(endPoint = "/api/v1/payments/{chargeToken}/reversals")
    RibbonRequest<ByteBuf> reversals(@Var("chargeToken") String chargeToken, @Var("reversalType") String reversalType,
        @Var("refundToken") String refundToken);
}