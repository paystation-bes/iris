package com.digitalpaytech.client;

import com.digitalpaytech.client.transformer.MultipartFileWrapperTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.clientcommunication.dto.MultipartFileWrapper;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;
import io.reactivex.netty.channel.StringTransformer;

@ResourceGroup(name = "lcs")
public interface LinkCryptoClient {
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("decrypt")
    @Http(method = HttpMethod.POST, uri = "/api/v1/decrypt", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(StringTransformer.class)
    RibbonRequest<ByteBuf> decrypt(@Content String encryptedText);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("encryptInternal")
    @Http(method = HttpMethod.POST, uri = "/api/v1/encrypt/internal", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(StringTransformer.class)
    RibbonRequest<ByteBuf> encryptInternal(@Content String plainText);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("currentExternalKey")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> currentExternalKey();

    @Secure(value = true, oauth2 = true)
    @TemplateName("currentInternalKey")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key/internal", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> currentInternalKey();
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("export")
    @Http(method = HttpMethod.POST, uri = "/api/v1/key/{keyAlias}", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(StringTransformer.class)
    RibbonRequest<ByteBuf> export(@Var("keyAlias") String keyAlias, @Content String hashAlgorithm);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("keyInfo")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key/{keyAlias}/info", headers = { @Header(name = "Content-Type", value = "text/plain") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> keyInfo(@Var("keyAlias") String keyAlias);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("signFile")
    @Http(method = HttpMethod.POST, uri = "/api/v1/filesignature/{algorithm}")
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(MultipartFileWrapperTransformer.class)
    RibbonRequest<ByteBuf> signFile(@Var("algorithm") String algorithm, @Content MultipartFileWrapper multipartFileWrapper);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("listExternalKeys")
    @Http(method = HttpMethod.GET, uri = "/api/v1/keyList")
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> listExternalKeys();
    
}
