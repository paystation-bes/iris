package com.digitalpaytech.client.util;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.client.dto.Error;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.Status;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.json.JSON;
import com.netflix.hystrix.exception.HystrixRuntimeException;

public final class HystrixExceptionUtil {
    private static final String UNDECRYPTABLE_CAUSE = "undecryptable";
    
    private static final String CRYPTO_IDENTIFIER = "crypto";
    
    private static final String CONNECTION_CLOSED_FORCIBLY_MESSAGE = "connection was forcibly closed by the remote host";
    
    private static final Logger LOG = Logger.getLogger(HystrixExceptionUtil.class);
    
    private static final JSON JSON = new JSON();
    
    private HystrixExceptionUtil() {
    }
    
    /**
     * Retrieve com.digitalpaytech.client.util.CommunicationException ResponseMessage from Future.
     * 
     * @param exp
     *            Exception. Only HystrixRuntimeException will be processed, for others an empty String will be returned.
     * @return String ResponseMessage from CommunicationException or an empty String for other exception.
     */
    public static String getRootCause(final HystrixRuntimeException hre) {
        try {
            if (isCommunicationException(hre)) {
                return "HystrixRuntimeException cause: " + getCommunicationException(hre).getResponseMessage().toCompletableFuture().get();
            }
            final String failCause = ExceptionUtils.getRootCauseMessage(hre);
            return failCause;
            
        } catch (InterruptedException | ExecutionException e) {
            LOG.error(e);
            return StandardConstants.STRING_EMPTY_STRING;
        }
    }
    
    /**
     * @return The nested stack trace, with the root cause first.
     */
    public static String getFullStackTrace(final HystrixRuntimeException hre) {
        return ExceptionUtils.getFullStackTrace(hre);
    }
    
    public static boolean isCommunicationException(final HystrixRuntimeException hre) {
        final Throwable rootCause = ExceptionUtils.getRootCause(hre);
        if (rootCause != null && rootCause instanceof CommunicationException) {
            return true;
        }
        return false;
    }
    
    public static CommunicationException getCommunicationException(final HystrixRuntimeException hre) {
        if (isCommunicationException(hre)) {
            return (CommunicationException) ExceptionUtils.getRootCause(hre);
        }
        return null;
    }
    
    public static boolean isCryptoCause(final HystrixRuntimeException hre) {
        final CommunicationException communicationException = getCommunicationException(hre);
        if (communicationException == null) {
            return false;
        }
        
        try {
            final String causeJson = communicationException.getResponseMessage().toCompletableFuture().get();
            
            if (StringUtils.isBlank(causeJson)) {
                return false;
            }
            
            final ObjectResponse<?> objRes = JSON.deserialize(causeJson, ObjectResponse.class);
            final Status status = objRes.getStatus();
            
            if (status == null) {
                return false;
            }
            
            final List<Error<String>> errors = status.getErrors();
            
            if (errors == null) {
                return false;
            }
            
            return errors.stream().anyMatch(err -> CRYPTO_IDENTIFIER.equals(StringUtils.lowerCase(err.getIdentifier()))
                                                   && UNDECRYPTABLE_CAUSE.equals(StringUtils.lowerCase(err.getMessage())));
            
        } catch (InterruptedException | ExecutionException | JsonException e) {
            LOG.error("Unable to determine exception cause for original exception: ", hre);
            LOG.error("Determine HRE exception failed due to : ", e);
            return false;
        }
        
    }
    
    public static boolean isRequestWithClientError(final HystrixRuntimeException hre) {
        final CommunicationException communicationException = getCommunicationException(hre);
        
        if (communicationException == null) {
            return false;
        }
        
        if ((communicationException.getResponseStatus() / 100) != 4) {
            return false;
        }
        return true;
    }
    
    public static boolean isConnectionClosedForcibly(final Exception exception) {
        if (isHystrixRuntimeException(exception)) {
            final boolean isRootCauseIOException = ExceptionUtils.getRootCause(exception) instanceof IOException;
            final String rootCauseMessage = ExceptionUtils.getRootCauseMessage(exception);
            final boolean containClosedForciblyMessage =
                    rootCauseMessage != StandardConstants.STRING_EMPTY_STRING
                                                         && StringUtils.contains(rootCauseMessage, CONNECTION_CLOSED_FORCIBLY_MESSAGE);
            
            if (isRootCauseIOException && containClosedForciblyMessage) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isHystrixRuntimeException(final Exception exception) {
        return exception instanceof HystrixRuntimeException;
    }
}
