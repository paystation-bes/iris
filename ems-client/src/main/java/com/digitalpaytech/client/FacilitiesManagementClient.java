package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.dto.fms.IrisOverrideSettings;
import com.digitalpaytech.client.dto.fms.ScheduleConfiguration;
import com.digitalpaytech.client.transformer.ByteArrayTransformer;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;
import rx.Observable;

@StoryAlias(FacilitiesManagementClient.ALIAS)
@ResourceGroup(name = "fmserver")
public interface FacilitiesManagementClient {
    String ALIAS = "Facility Management Service";
    
    @TemplateName("groupList")
    @Http(method = HttpMethod.GET,
          uri = "/devicegroups?customerId={customerId}&filter={filter}&currentPage={currentPage}&lastAccessed={lastAccessed}",
          headers = {
              @Header(name = "Content-Type", value = "application/json")
          })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> groupList(@Var("customerId") String customerId,
                                     @Var("filter") String filter,
                                     @Var("currentPage") Integer currentPage,
                                     @Var("lastAccessed") Long lastAccessed);

    @Secure(true)
    @TemplateName("groupDetails")
    @Http(method = HttpMethod.GET, uri = "/devicegroups/{groupId}", headers = {
        @Header(name = "Content-Type", value = "text/plain")
        })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> groupDetails(@Var("groupId") String groupId);    

    @Secure(true)
    @TemplateName("groupInfoByDevice")
    @Http(method = HttpMethod.GET, uri = "/devicegroups/devices/{deviceId}?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "text/plain")
        })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> groupInfoFromDevice(@Var("customerId") String customerId, @Var("deviceId") String deviceId);    

    @Secure(true)
    @TemplateName("listUpgradeGroups")
    @Http(method = HttpMethod.GET, 
          uri = "/upgradegroups?customerId={customerId}",
          headers = {
              @Header(name = "Content-Type", value = "text/plain")
              })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> listUpgradeGroups(@Var("customerId") String customerId);    
        
    @Secure(true)
    @TemplateName("groupNameAutoComplete")
    @Http(method = HttpMethod.GET, 
            uri = "/devicegroups?customerId={customerId}&search={search}",
            headers = {
                @Header(name = "Content-Type", value = "text/plain")
            })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> groupNameAutoComplete(@Var("customerId") String customerId,
                                                 @Var("search") String search);    
    
    @Secure(true)
    @TemplateName("uploadPaystationSettings")
    @Http(method = HttpMethod.POST, uri = "/devicegroups?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/octet-stream")
        })
    @ContentTransformerClass(ByteArrayTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> uploadPaystationSettings(@Var("customerId") String customerId, @Content Observable<byte []> file);    
    
    
    @Secure(true)
    @TemplateName("sendSchedule")
    @Http(method = HttpMethod.POST, uri = "/devicegroups/{groupId}/schedule", headers = {
        @Header(name = "Content-Type", value = "application/json")
        })
    @ContentTransformerClass(JSONContentTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> sendSchedule(@Var("groupId") String groupId, @Content ScheduleConfiguration scheduleConfiguration)
            throws CommunicationException;

    @Secure(true)
    @TemplateName("updateDeviceGroup")
    @Http(method = HttpMethod.POST, uri = "/devicegroups/{groupId}/updateDeviceGroup", headers = {
        @Header(name = "Content-Type", value = "application/json")
        })
    @ContentTransformerClass(JSONContentTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> updateDeviceGroup(@Var("groupId") String groupId, @Content IrisOverrideSettings irisOverrideSettings) 
            throws CommunicationException;
    
    @Secure(true)
    @TemplateName("bossKeyGenerate")
    @Http(method = HttpMethod.PUT, uri = "/bosskeys?customerId={customerId}")
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> bossKeyGenerate(@Var("customerId") String customerId);
    
    @Secure(true)
    @TemplateName("bossKeyDownload")
    @Http(method = HttpMethod.GET, uri = "/bosskeys?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json")
        })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> bossKeyDownload(@Var("customerId") String customerId);
    
    @Secure(true)
    @TemplateName("bossKeyProgress")
    @Http(method = HttpMethod.GET, uri = "/bosskeys/progress?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json")
        })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> bossKeyProgress(@Var("customerId") String customerId);
    
    @TemplateName("downloadPaystationSettings")
    @Http(method = HttpMethod.GET, uri = "/settingsconfiguration?customerId={customerId}&deviceId={deviceId}", headers={
            @Header(name = "Content-Type", value = "text/plain")
    })  
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> downloadPaystationSettings(@Var("customerId") String customerId, @Var("deviceId") String deviceId);    

}
