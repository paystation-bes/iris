package com.digitalpaytech.client.dto.merchant;

import java.io.Serializable;
import java.util.SortedMap;
import java.util.UUID;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Terminal implements Serializable {
    private static final long serialVersionUID = -4454475756121034064L;
    private UUID id;
    private String name;
    private Boolean isValidated;
    private Boolean isLocked;
    private Boolean isEnabled;
    private SortedMap<String, String> terminalDetails;
    private Long referenceCounter;
    private String createdUTC;
    private String updatedUTC;

    public Terminal() {
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("isValidated", isValidated).append("isLocked", isLocked)
                .append("isEnabled", isEnabled).append("terminalDetails", terminalDetails).append("referenceCounter", referenceCounter)
                .append("createdUTC", createdUTC).append("updatedUTC", updatedUTC).toString();
    }

    public final UUID getId() {
        return this.id;
    }
    
    public final void setId(final UUID id) {
        this.id = id;
    }
    public final String getName() {
        return this.name;
    }
    public final void setName(final String name) {
        this.name = name;
    }
    public final Boolean getIsValidated() {
        return this.isValidated;
    }
    public final void setIsValidated(final Boolean isValidated) {
        this.isValidated = isValidated;
    }
    public final Boolean getIsLocked() {
        return this.isLocked;
    }
    public final void setIsLocked(final Boolean isLocked) {
        this.isLocked = isLocked;
    }
    public final Boolean getIsEnabled() {
        return this.isEnabled;
    }
    public final void setIsEnabled(final Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    public final SortedMap<String, String> getTerminalDetails() {
        return this.terminalDetails;
    }
    public final void setTerminalDetails(final SortedMap<String, String> terminalDetails) {
        this.terminalDetails = terminalDetails;
    }
    public final Long getReferenceCounter() {
        return this.referenceCounter;
    }
    public final void setReferenceCounter(final Long referenceCounter) {
        this.referenceCounter = referenceCounter;
    }
    public final String getCreatedUTC() {
        return this.createdUTC;
    }
    public final void setCreatedUTC(final String createdUTC) {
        this.createdUTC = createdUTC;
    }
    public final String getUpdatedUTC() {
        return this.updatedUTC;
    }
    public final void setUpdatedUTC(final String updatedUTC) {
        this.updatedUTC = updatedUTC;
    }
}
