package com.digitalpaytech.client.dto.corecps;

import java.time.Instant;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.digitalpaytech.client.dto.Status;
import com.digitalpaytech.util.CardProcessingConstants;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

public class Authorization {
    
    private Status status;
    private UUID chargeToken;
    private UUID cardToken;
    private UUID externalToken;
    private String authorizationNumber;
    private String cardType;
    private String last4Digits;
    private String referenceNumber;
    private String processorTransactionId;
    private Instant processedDate;
    private UUID terminalToken;
    private JsonNode optionalData;
    private UUID refundToken;
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("chargeToken", chargeToken).append("cardToken", cardToken)
                .append("externalToken", externalToken).append("authorizationNumber", authorizationNumber).append("cardType", cardType)
                .append("last4Digits", last4Digits).append("referenceNumber", referenceNumber)
                .append("processorTransactionId", processorTransactionId).append("processedDate", processedDate)
                .append("terminalToken", terminalToken).append("optionalData", optionalData).append("refundToken", refundToken).toString();
    }

    
    /**
     * e.g. {"status":{"responseStatus":"Accepted","errors":[{"identifier":"Approved","message":"Request is approved"}]},
     *      {"status":{"responseStatus":"Declined","errors":[{"identifier":"Declined","message":"Request is declined"}]},
     */
    public final boolean isApproved() {
        if (getStatus() != null && getStatus().getErrors() != null && !getStatus().getErrors().isEmpty()) {
            return getStatus().getErrors().stream().anyMatch(e -> StringUtils.isNotBlank(e.getIdentifier()) 
                                                            && e.getIdentifier().equalsIgnoreCase(CardProcessingConstants.APPROVED));
        }
        return false;
    }    
    
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(final Status status) {
        this.status = status;
    }
    
    public UUID getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final UUID chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    public UUID getCardToken() {
        return this.cardToken;
    }
    
    public void setCardToken(final UUID cardToken) {
        this.cardToken = cardToken;
    }

    public UUID getExternalToken() {
        return externalToken;
    }

    public void setExternalToken(final UUID externalToken) {
        this.externalToken = externalToken;
    }
    
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public String getLast4Digits() {
        return this.last4Digits;
    }
    
    public void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    public String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public Instant getProcessedDate() {
        return this.processedDate;
    }
    
    @JsonProperty("processedDate")
    public String getProcessedDateString() {
        return this.processedDate == null ? null : this.processedDate.toString();
    }
    
    @JsonProperty("processedDate")
    public void setProcessedDate(final String processedDate) {
        this.processedDate = processedDate == null ? null : Instant.parse(processedDate);
    }
    
    public void setProcessedDate(final Instant processedDate) {
        this.processedDate = processedDate;
    }
    
    public UUID getTerminalToken() {
        return this.terminalToken;
    }
    
    public void setTerminalToken(final UUID terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public JsonNode getOptionalData() {
        return this.optionalData;
    }
    
    public void setOptionalData(final JsonNode optionalData) {
        this.optionalData = optionalData;
    }
    
    public UUID getRefundToken() {
        return this.refundToken;
    }

    public void setRefundToken(final UUID refundToken) {
        this.refundToken = refundToken;
    }

    public enum AuthotizationStatus {
        ACCEPTED, DECLINED, FAILED;
    }
    
}
