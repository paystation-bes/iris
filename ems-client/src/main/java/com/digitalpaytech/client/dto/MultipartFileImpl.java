package com.digitalpaytech.client.dto;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

/*
* Copyright 2017 the original author or authors.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* http://www.apache.org/licenses/LICENSE-2.0
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* This file is a modified version of:
* https://github.com/spring-projects/spring-framework/blob/master/spring-test/src/main/java/org/springframework/mock/web/MockMultipartFile.java
*/

public class MultipartFileImpl implements MultipartFile {
    
    private final String name;
    
    private String originalFilename;
    
    private String contentType;
    
    private final byte[] content;
    
    public MultipartFileImpl(final String name, final byte[] content) {
        this.name = name;
        this.originalFilename = "";
        this.contentType = null;
        this.content = content;
    }
    
    @Override
    public final String getName() {
        return this.name;
    }
    
    @Override
    public final String getOriginalFilename() {
        return this.originalFilename;
    }
    
    @Override
    public final String getContentType() {
        return this.contentType;
    }
    
    @Override
    public final boolean isEmpty() {
        return this.content.length < 1;
    }
    
    @Override
    public final long getSize() {
        return this.content.length;
    }
    
    @Override
    public final byte[] getBytes() throws IOException {
        return this.content;
    }
    
    @Override
    public final InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(this.content);
    }
    
    @Override
    public final void transferTo(final File dest) throws IOException, IllegalStateException {
        FileCopyUtils.copy(this.content, dest);
    }
    
}