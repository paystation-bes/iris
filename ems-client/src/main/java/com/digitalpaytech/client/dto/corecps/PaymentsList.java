package com.digitalpaytech.client.dto.corecps;

import java.util.List;

public class PaymentsList {
    private List<Payment> payments;

    public PaymentsList() {
    }

    public PaymentsList(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(final List<Payment> payments) {
        this.payments = payments;
    }
    
    @Override
    public final String toString() {
        if (this.payments == null || this.payments.isEmpty()) {
            return "It's null or empty";
        }
        final StringBuilder bdr = new StringBuilder();
        for (Payment p : payments) {
            bdr.append(p).append("\r\n");
        }
        return bdr.toString();
    }
}
