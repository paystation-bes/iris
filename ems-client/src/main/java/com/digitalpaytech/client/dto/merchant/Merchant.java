package com.digitalpaytech.client.dto.merchant;

import java.io.Serializable;
import java.util.SortedMap;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Merchant implements Serializable {
    private static final long serialVersionUID = 6845711375449623539L;
    private UUID id;
    private String name;
    private Integer customerId;
    private String processorType;
    private SortedMap<String, String> merchantDetails;
    private List<Terminal> terminals;
    private List<Terminal> deletedTerminals;
    private Boolean isEnabled;
    private Boolean isLocked;
    private String timeZone;
    private Integer numberOfRetries;
    private Integer badCardThreshold;
    private String createdUTC;
    private String updatedUTC;
    private Integer closeQuarterOfDay;


    public Merchant() {
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("customerId", customerId).append("processorType", processorType)
                .append("merchantDetails", merchantDetails).append("terminals", terminals).append("deletedTerminals", deletedTerminals)
                .append("isEnabled", isEnabled).append("isLocked", isLocked).append("timeZone", timeZone).append("numberOfRetries", numberOfRetries)
                .append("badCardThreshold", badCardThreshold).append("createdUTC", createdUTC).append("updatedUTC", updatedUTC)
                .append("closeQuarterOfDay", closeQuarterOfDay).toString();
    }
    
    public final UUID getId() {
        return this.id;
    }

    public final void setId(final UUID id) {
        this.id = id;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final Integer getCustomerId() {
        return this.customerId;
    }

    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }

    public final String getProcessorType() {
        return this.processorType;
    }

    public final void setProcessorType(final String processorType) {
        this.processorType = processorType;
    }

    public final SortedMap<String, String> getMerchantDetails() {
        return this.merchantDetails;
    }

    public final void setMerchantDetails(final SortedMap<String, String> merchantDetails) {
        this.merchantDetails = merchantDetails;
    }

    public final List<Terminal> getTerminals() {
        return this.terminals;
    }

    public final void setTerminals(final List<Terminal> terminals) {
        this.terminals = terminals;
    }

    public final List<Terminal> getDeletedTerminals() {
        return this.deletedTerminals;
    }

    public final void setDeletedTerminals(final List<Terminal> deletedTerminals) {
        this.deletedTerminals = deletedTerminals;
    }

    public final Boolean getIsEnabled() {
        return this.isEnabled;
    }

    public final void setIsEnabled(final Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public final Boolean getIsLocked() {
        return this.isLocked;
    }

    public final void setIsLocked(final Boolean isLocked) {
        this.isLocked = isLocked;
    }

    public final String getTimeZone() {
        return this.timeZone;
    }

    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }

    public final Integer getNumberOfRetries() {
        return this.numberOfRetries;
    }

    public final void setNumberOfRetries(final Integer numberOfRetries) {
        this.numberOfRetries = numberOfRetries;
    }

    public final Integer getBadCardThreshold() {
        return this.badCardThreshold;
    }

    public final void setBadCardThreshold(final Integer badCardThreshold) {
        this.badCardThreshold = badCardThreshold;
    }

    public final String getCreatedUTC() {
        return this.createdUTC;
    }

    public final void setCreatedUTC(final String createdUTC) {
        this.createdUTC = createdUTC;
    }

    public final String getUpdatedUTC() {
        return this.updatedUTC;
    }

    public final void setUpdatedUTC(final String updatedUTC) {
        this.updatedUTC = updatedUTC;
    }

    public final Integer getCloseQuarterOfDay() {
        return this.closeQuarterOfDay;
    }

    public final void setCloseQuarterOfDay(final Integer closeQuarterOfDay) {
        this.closeQuarterOfDay = closeQuarterOfDay;
    }
}
