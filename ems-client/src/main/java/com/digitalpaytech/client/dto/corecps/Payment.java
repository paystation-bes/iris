package com.digitalpaytech.client.dto.corecps;

import java.time.Instant;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.databind.JsonNode;

public class Payment {
    private String deviceSerialNumber;
    private Integer customerId;
    private UUID terminalToken;
    private String paymentType;
    private UUID chargeToken;
    private String cardData;
    private UUID cardToken;
    private String cardType;
    private String purchaseUTC;
    private Integer amount;
    private JsonNode optionalData;
    private UUID refundToken;
    
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public UUID getTerminalToken() {
        return this.terminalToken;
    }
    
    public void setTerminalToken(final UUID terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public String getPaymentType() {
        return this.paymentType;
    }
    
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }
    
    public UUID getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final UUID uuid) {
        this.chargeToken = uuid;
    }
    
    public String getCardData() {
        return this.cardData;
    }
    
    public void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    
    public UUID getCardToken() {
        return this.cardToken;
    }
    
    public void setCardToken(final UUID cardToken) {
        this.cardToken = cardToken;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public String getPurchaseUTC() {
        return this.purchaseUTC;
    }
    
    public void setPurchaseUTC(final String purchaseUTC) {
        this.purchaseUTC = purchaseUTC;
    }
    
    public void setPurchaseUTC(final Instant purchaseUTC) {
        this.purchaseUTC = purchaseUTC.toString();
    }
    
    public Integer getAmount() {
        return this.amount;
    }
    
    public void setAmount(final Integer amount) {
        this.amount = amount;
    }
    
    public JsonNode getOptionalData() {
        return this.optionalData;
    }
    
    public void setOptionalData(final JsonNode optionalData) {
        this.optionalData = optionalData;
    }
    
    public UUID getRefundToken() {
        return this.refundToken;
    }
    
    public void setRefundToken(final UUID refundToken) {
        this.refundToken = refundToken;
    }
    
    @Override
    public String toString() {
        final boolean cardDataFlag = StringUtils.isBlank(this.cardData);
        return new ToStringBuilder(this).append("deviceSerialNumber", deviceSerialNumber).append("customerId", customerId)
                .append("terminalToken", terminalToken).append("paymentType", paymentType).append("chargeToken", chargeToken)
                .append("cardToken", cardToken).append("cardType", cardType).append("purchaseUTC", purchaseUTC).append("amount", amount)
                .append("optionalData", optionalData).append("refundToken", refundToken).append(". Is cardData null? ").append(cardDataFlag)
                .toString();
    }
}
