package com.digitalpaytech.client.dto.merchant;

import java.util.List;

public class TerminalPage extends AbstractPage<Terminal> {
    public TerminalPage() {
    }

    public TerminalPage(final List<Terminal> content, 
                        final boolean last, 
                        final int totalElements, 
                        final int totalPages, 
                        final int size, 
                        final int number, 
                        final boolean first,
                        final int numberOfElements) {
        super(content, last, totalElements, totalPages, size, number, first, numberOfElements);
    }
}
