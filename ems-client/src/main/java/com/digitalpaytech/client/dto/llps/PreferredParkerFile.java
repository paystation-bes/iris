package com.digitalpaytech.client.dto.llps;

import java.util.List;

public class PreferredParkerFile {
    
    public enum ProcessStatus {
        NOT_STARTED, IN_PROCESS, SUCCESS, ERROR_OUT
    }
    
    private String id;
    private int customerId;
    private ProcessStatus processStatus;
    private List<String> errorRows;
    
    public PreferredParkerFile() {
        super();
    }
    
    public PreferredParkerFile(final String id, final int customerId, final ProcessStatus processStatus, final List<String> errorRows) {
        super();
        this.id = id;
        this.customerId = customerId;
        this.processStatus = processStatus;
        this.errorRows = errorRows;
    }
    
    public final String getId() {
        return id;
    }
    
    public final void setId(final String id) {
        this.id = id;
    }
    
    public final int getCustomerId() {
        return customerId;
    }
    
    public final void setCustomerId(final int customerId) {
        this.customerId = customerId;
    }
    
    public final ProcessStatus getProcessStatus() {
        return processStatus;
    }
    
    public final void setProcessStatus(final ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }
    
    public final List<String> getErrorRows() {
        return errorRows;
    }
    
    public final void setErrorRows(final List<String> errorRows) {
        this.errorRows = errorRows;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("id: ").append(this.id).append(", customerId: ").append(this.customerId).append(", processStatus: ").append(this.processStatus)
        .append(", errorRows: ").append(this.errorRows);
        return bdr.toString();
    }
}
