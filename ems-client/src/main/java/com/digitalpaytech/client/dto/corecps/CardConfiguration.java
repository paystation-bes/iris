package com.digitalpaytech.client.dto.corecps;

public class CardConfiguration {
    
    private String id;
    private String cardType;
    private String shortName;
    private String cardCategory;
    private String[] patterns;
    private boolean enableLuhnCheck;
    private Integer customerId;
    
    public String getCardCategory() {
        return this.cardCategory;
    }
    
    public void setCardCategory(final String cardCategory) {
        this.cardCategory = cardCategory;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }
    
    public String[] getPatterns() {
        return this.patterns;
    }
    
    public void setPatterns(final String[] patterns) {
        this.patterns = patterns;
    }
    
    public boolean isEnableLuhnCheck() {
        return this.enableLuhnCheck;
    }
    
    public void setEnableLuhnCheck(final boolean applyAlgorithm) {
        this.enableLuhnCheck = applyAlgorithm;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getId() {
        return this.id;
    }
}
