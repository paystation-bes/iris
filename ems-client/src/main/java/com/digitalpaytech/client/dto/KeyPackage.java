package com.digitalpaytech.client.dto;

import java.io.Serializable;

public final class KeyPackage implements Serializable {
    private static final long serialVersionUID = -7406124296201043197L;
    
    private RSAKeyInfo key;
    private String signatureSource;
    private String signature;
    private String hash;
    
    private String nextHash;
    
    public KeyPackage() {
        
    }

    public RSAKeyInfo getKey() {
        return this.key;
    }

    public void setKey(final RSAKeyInfo key) {
        this.key = key;
    }

    public String getSignatureSource() {
        return this.signatureSource;
    }

    public void setSignatureSource(final String signatureSource) {
        this.signatureSource = signatureSource;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(final String signature) {
        this.signature = signature;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public String getNextHash() {
        return this.nextHash;
    }

    public void setNextHash(final String nextHash) {
        this.nextHash = nextHash;
    }
}
