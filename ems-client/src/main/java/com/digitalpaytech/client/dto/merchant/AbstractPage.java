package com.digitalpaytech.client.dto.merchant;

import java.util.List;

public class AbstractPage<T> {
    private List<T> content;
    private boolean last;
    private int totalElements;
    private int totalPages;
    private int size;
    private int number;
    private boolean first;
    private int numberOfElements;

    public AbstractPage() {
    }
    
    public AbstractPage(final List<T> content, 
                        final boolean last, 
                        final int totalElements, 
                        final int totalPages, 
                        final int size, 
                        final int number, 
                        final boolean first, 
                        final int numberOfElements) {
        this.content = content;
        this.last = last;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.size = size;
        this.number = number;
        this.first = first;
        this.numberOfElements = numberOfElements;
    }

    public final List<T> getContent() {
        return this.content;
    }
    public final void setContent(final List<T> content) {
        this.content = content;
    }    
    public final boolean isLast() {
        return this.last;
    }
    public final void setLast(final boolean last) {
        this.last = last;
    }
    public final int getTotalElements() {
        return this.totalElements;
    }
    public final void setTotalElements(final int totalElements) {
        this.totalElements = totalElements;
    }
    public final int getTotalPages() {
        return this.totalPages;
    }
    public final void setTotalPages(final int totalPages) {
        this.totalPages = totalPages;
    }
    public final int getSize() {
        return this.size;
    }
    public final void setSize(final int size) {
        this.size = size;
    }
    public final int getNumber() {
        return this.number;
    }
    public final void setNumber(final int number) {
        this.number = number;
    }
    public final boolean isFirst() {
        return this.first;
    }
    public final void setFirst(final boolean first) {
        this.first = first;
    }
    public final int getNumberOfElements() {
        return this.numberOfElements;
    }
    public final void setNumberOfElements(final int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }
    
}
