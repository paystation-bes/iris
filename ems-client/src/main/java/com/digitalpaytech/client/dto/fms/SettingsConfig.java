package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SettingsConfig implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2127694410874341186L;
    @JsonProperty("cardProcessing")
    private CardProcessingConfig cardProcessing;

    public SettingsConfig() {
    }
    
    public SettingsConfig(final CardProcessingConfig cardProcessing) {
        this.cardProcessing = cardProcessing;
    }
    
    public final CardProcessingConfig getCardProcessing() {
        return this.cardProcessing;
    }
    
    public final void setCardProcessing(final CardProcessingConfig cardProcessing) {
        this.cardProcessing = cardProcessing;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("\r\nSettingsConfig.cardProcessing: ").append(this.cardProcessing);
        return bdr.toString();
    }
}
