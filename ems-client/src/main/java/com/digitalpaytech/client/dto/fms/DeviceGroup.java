package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceGroup implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4998639939805621123L;
    @JsonProperty("groupId")
    private String groupId;
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("groupStatus")
    private String groupStatus;
    @JsonProperty("lastModified")
    private Date lastModified;
    @JsonProperty("scheduleDate")
    private String scheduleDate;
    @JsonProperty("scheduleTime")
    private String scheduleTime;
    @JsonProperty("groupConfiguration")
    private GroupConfiguration groupConfiguration;
    
    public DeviceGroup() {
    }

    public DeviceGroup(final String groupId, 
                       final String groupName, 
                       final String groupStatus, 
                       final Date lastModified, 
                       final String scheduleDate,
                       final String scheduleTime,
                       final GroupConfiguration groupConfiguration) {
        
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupStatus = groupStatus;
        this.lastModified = lastModified;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
        this.groupConfiguration = groupConfiguration;
    }
  
    
    public final String getGroupId() {
        return this.groupId;
    }

    public final void setGroupId(final String groupId) {
        this.groupId = groupId;
    }

    public final String getGroupName() {
        return this.groupName;
    }

    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    public final Date getLastModified() {
        return this.lastModified;
    }

    public final void setLastModified(final Date lastModified) {
        this.lastModified = lastModified;
    }

    public final String getScheduleDate() {
        return this.scheduleDate;
    }

    public final void setScheduleDate(final String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public final String getScheduleTime() {
        return this.scheduleTime;
    }

    public final void setScheduleTime(final String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public final String getGroupStatus() {
        return this.groupStatus;
    }

    public final void setGroupStatus(final String groupStatus) {
        this.groupStatus = groupStatus;
    }

    public final GroupConfiguration getGroupConfiguration() {
        return this.groupConfiguration;
    }

    public final void setGroupConfiguration(final GroupConfiguration groupConfiguration) {
        this.groupConfiguration = groupConfiguration;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("groupId: ").append(this.groupId).append(", groupName: ").append(this.groupName);
        bdr.append(", groupStatus: ").append(this.groupStatus).append(", lastModified: ").append(this.lastModified);
        bdr.append(", scheduleDate: ").append(this.scheduleDate).append(", scheduleTime: ").append(this.scheduleTime);
        bdr.append(", groupConfiguration: ").append(this.groupConfiguration);
        return bdr.toString();
    }
}
