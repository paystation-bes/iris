package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BulkSchedule implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5769664229226342257L;
    @JsonProperty("scheduleDate")
    private String scheduleDate;
    @JsonProperty("scheduleTime")
    private String scheduleTime;
    @JsonProperty("groupIds")
    private String groupIds;
    
    public BulkSchedule() {
        
    }
    
    public BulkSchedule(final String scheduleDate, final String scheduleTime, final String groupIds) {
        super();
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
        this.groupIds = groupIds;
    }
    
    public final String getScheduleDate() {
        return scheduleDate;
    }
    
    public final void setScheduleDate(final String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
    
    public final String getScheduleTime() {
        return scheduleTime;
    }
    
    public final void setScheduleTime(final String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }
    
    public String getGroupIds() {
        return groupIds;
    }
    
    public void setGroupIds(final String groupIds) {
        this.groupIds = groupIds;
    }
    
}
