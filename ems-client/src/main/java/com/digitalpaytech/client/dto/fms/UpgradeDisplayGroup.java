package com.digitalpaytech.client.dto.fms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpgradeDisplayGroup {
    @JsonProperty("groupName")
    private String groupName;
    @JsonProperty("groupId")
    private String groupId;
    @JsonProperty("groupStatus")
    private String groupStatus;
    @JsonProperty("enableIris")
    private Boolean enableIris;
    
    public final String getGroupName() {
        return groupName;
    }
    
    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }
    
    public final String getGroupId() {
        return groupId;
    }
    
    public final void setGroupId(final String groupId) {
        this.groupId = groupId;
    }
    
    public final String getGroupStatus() {
        return groupStatus;
    }
    
    public final void setGroupStatus(final String groupStatus) {
        this.groupStatus = groupStatus;
    }
    
    public final Boolean getEnableIris() {
        return enableIris;
    }
    
    public final void setEnableIris(final Boolean enableIris) {
        this.enableIris = enableIris;
    }
    
}
