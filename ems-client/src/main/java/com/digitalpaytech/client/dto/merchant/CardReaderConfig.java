package com.digitalpaytech.client.dto.merchant;

public class CardReaderConfig {
    
    private String terminalId;
    private String merchantId;
    private String processorUrl;
    private String transactionKey;
    private String tmsUrl;
    private String merchantProcessor;
    private String currencyCode;
    private String currency;
    private String merchantStatus;
    
    public CardReaderConfig() {
        super();
    }
    
    public final String getTerminalId() {
        return this.terminalId;
    }
    
    public final void setTerminalId(final String terminalId) {
        this.terminalId = terminalId;
    }
    
    public final String getMerchantId() {
        return this.merchantId;
    }
    
    public final void setMerchantId(final String merchantId) {
        this.merchantId = merchantId;
    }
    
    public final String getProcessorUrl() {
        return this.processorUrl;
    }
    
    public final void setProcessorUrl(final String processorUrl) {
        this.processorUrl = processorUrl;
    }
    
    public final String getTransactionKey() {
        return this.transactionKey;
    }
    
    public final void setTransactionKey(final String transactionKey) {
        this.transactionKey = transactionKey;
    }
    
    public final String getTmsUrl() {
        return this.tmsUrl;
    }
    
    public final void setTmsUrl(final String tmsUrl) {
        this.tmsUrl = tmsUrl;
    }
    
    public final String getMerchantProcessor() {
        return this.merchantProcessor;
    }
    
    public final void setMerchantProcessor(final String merchantProcessor) {
        this.merchantProcessor = merchantProcessor;
    }
    
    public final String getCurrencyCode() {
        return this.currencyCode;
    }
    
    public final void setCurrencyCode(final String currencyCode) {
        this.currencyCode = currencyCode;
    }
    
    public final String getCurrency() {
        return this.currency;
    }
    
    public final void setCurrency(final String currency) {
        this.currency = currency;
    }
    
    public final String getMerchantStatus() {
        return this.merchantStatus;
    }
    
    public final void setMerchantStatus(final String merchantStatus) {
        this.merchantStatus = merchantStatus;
    }
    
}
