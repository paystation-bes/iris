package com.digitalpaytech.client.dto;

import java.io.Serializable;

public class LinuxConfigurationAck implements Serializable {
    private static final long serialVersionUID = 8748755672690860168L;
    private Integer customerId;
    private String deviceSerialNumber;
    private String configSnapshotId;
    private String activeDateTime;
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public final void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public final String getConfigSnapshotId() {
        return this.configSnapshotId;
    }
    
    public final void setConfigSnapshotId(final String configSnapshotId) {
        this.configSnapshotId = configSnapshotId;
    }
    
    public final String getActiveDateTime() {
        return this.activeDateTime;
    }
    
    public final void setActiveDateTime(final String activeDateTime) {
        this.activeDateTime = activeDateTime;
    }
    
}
