package com.digitalpaytech.client.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public final class KeyIdentity implements Serializable {
    private static final long serialVersionUID = 4862321163870346881L;
    
    private String keyAlias;
    private Map<String, String> hashes;
    
    private int keyType;
    private int keyIndex;
    
    public KeyIdentity() {
        this.hashes = new HashMap<String, String>();
    }
    
    public String getKeyAlias() {
        return this.keyAlias;
    }
    
    public void setKeyAlias(final String keyAlias) {
        this.keyAlias = keyAlias;
    }
    
    public Map<String, String> getHashes() {
        return this.hashes;
    }
    
    public int getKeyType() {
        return this.keyType;
    }
    
    public void setKeyType(final int keyType) {
        this.keyType = keyType;
    }
    
    public int getKeyIndex() {
        return this.keyIndex;
    }
    
    public void setKeyIndex(final int keyIndex) {
        this.keyIndex = keyIndex;
    }
}
