package com.digitalpaytech.client.dto.merchant;

import java.util.List;

public class MerchantPage extends AbstractPage<Merchant> {
    public MerchantPage() {
    }

    public MerchantPage(final List<Merchant> content, 
                        final boolean last, 
                        final int totalElements, 
                        final int totalPages, 
                        final int size, 
                        final int number, 
                        final boolean first,
                        final int numberOfElements) {
        super(content, last, totalElements, totalPages, size, number, first, numberOfElements);
    }
}
