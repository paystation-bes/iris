package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IrisOverrideSettings implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5728820727388172156L;
    @JsonProperty("cardProcessingOverride")
    private boolean cardProcessingOverride;
    @JsonProperty("canSchedule")
    private boolean canSchedule;
    @JsonProperty("cardProcessingConfig")
    private CardProcessingConfig cardProcessingConfig;
    
    public IrisOverrideSettings() {
    }
    
    public IrisOverrideSettings(final boolean cardProcessingOverride, final boolean canSchedule, final CardProcessingConfig cardProcessingConfig) {
        this.cardProcessingOverride = cardProcessingOverride;
        this.canSchedule = canSchedule;
        this.cardProcessingConfig = cardProcessingConfig;
    }
    
    public final boolean isCardProcessingOverride() {
        return this.cardProcessingOverride;
    }
    
    public final void setCardProcessingOverride(final boolean cardProcessingOverride) {
        this.cardProcessingOverride = cardProcessingOverride;
    }
    
    public final CardProcessingConfig getCardProcessingConfig() {
        return this.cardProcessingConfig;
    }
    
    public final void setCardProcessingConfig(final CardProcessingConfig cardProcessingConfig) {
        this.cardProcessingConfig = cardProcessingConfig;
    }
    
    public final boolean isCanSchedule() {
        return canSchedule;
    }
    
    public final void setCanSchedule(final boolean canSchedule) {
        this.canSchedule = canSchedule;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("cardProcessingOverride: ").append(this.cardProcessingOverride).append(", canSchedule: ").append(this.canSchedule);
        bdr.append(", cardProcessingConfig: ").append(this.cardProcessingConfig);
        return bdr.toString();
    }
}
