package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceUser implements Serializable {
    private static final long serialVersionUID = 2001697562865493072L;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("customerId")
    private Integer unifiId;
    @JsonProperty("userStatusType")
    private String userStatusType;
    @JsonProperty("userType")
    private String userType;
    
    public DeviceUser(final String userName, final Integer unifiId, final String userStatusType, final String userType) {
        this.userName = userName;
        this.unifiId = unifiId;
        this.userStatusType = userStatusType;
        this.userType = userType;
    }
    
    public DeviceUser() {
    }

    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("DeviceUser - userName: ").append(this.userName).append(", customerId: ").append(this.unifiId);
        bdr.append(", userStatusType: ").append(this.userStatusType).append(", userType: ").append(this.userType);
        return bdr.toString();
    }
    
    public final String getUserName() {
        return this.userName;
    }
    
    public final void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public final Integer getUnifiId() {
        return this.unifiId;
    }
    
    public final void setUnifiId(final Integer unifiId) {
        this.unifiId = unifiId;
    }
    
    public final String getUserStatusType() {
        return this.userStatusType;
    }
    
    public final void setUserStatusType(final String userStatusType) {
        this.userStatusType = userStatusType;
    }
    
    public final String getUserType() {
        return this.userType;
    }
    
    public final void setUserType(final String userType) {
        this.userType = userType;
    }
    
}
