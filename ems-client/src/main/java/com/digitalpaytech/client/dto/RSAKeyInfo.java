package com.digitalpaytech.client.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public final class RSAKeyInfo implements Serializable {
    private static final long serialVersionUID = 7041142595325155345L;

    private String name;
    
    private BigInteger modulus;
    private BigInteger exponent;
    
    private Date createTime;
    private Date expiryTime;
    
    public RSAKeyInfo() {
        
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public BigInteger getModulus() {
        return this.modulus;
    }

    public void setModulus(final BigInteger modulus) {
        this.modulus = modulus;
    }

    public BigInteger getExponent() {
        return this.exponent;
    }

    public void setExponent(final BigInteger exponent) {
        this.exponent = exponent;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(final Date createTime) {
        this.createTime = createTime;
    }

    public Date getExpiryTime() {
        return this.expiryTime;
    }

    public void setExpiryTime(final Date expiryTime) {
        this.expiryTime = expiryTime;
    }
}
