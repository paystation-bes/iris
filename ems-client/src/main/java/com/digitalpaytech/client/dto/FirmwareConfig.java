package com.digitalpaytech.client.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/*
 * case class FirmwareConfig(requiresNotification: Boolean = false,
 *                           upgradeProgress: Int = UPGRADE_PROGRESS_INIT,
 *                           upgradeState: Option[String] = None,
 *                           upgradeStateTimeStamp: Option[DateTime] = None,
 *                           upgradeErrorCode: Option[String] = None,
 *                           upgradeMode: Option[String] = None,
 *                           downloadWindow: Option[String] = None,
 *                           upgradeWindow: Option[String] = None,
 *                           app: Option[FirmwareDetails] = None,
 *                           platform: Option[FirmwareDetails] = None)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class FirmwareConfig implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -256772932383570815L;
    private boolean requiresNotification;
    private Integer upgradeProgress;
    private String upgradeState;
    private Date upgradeStateTimeStamp;
    private String upgradeErrorCode;
    private String upgradeMode;
    private String downloadWindow;
    private String upgradeWindow;
    
    
    public FirmwareConfig() {
    }

    public Integer getUpgradeProgress() {
        return this.upgradeProgress;
    }
    public void setUpgradeProgress(final Integer upgradeProgress) {
        this.upgradeProgress = upgradeProgress;
    }
    public boolean isRequiresNotification() {
        return this.requiresNotification;
    }
    public void setRequiresNotification(final boolean requiresNotification) {
        this.requiresNotification = requiresNotification;
    }
    public String getUpgradeState() {
        return this.upgradeState;
    }
    public void setUpgradeState(final String upgradeState) {
        this.upgradeState = upgradeState;
    }
    public Date getUpgradeStateTimeStamp() {
        return this.upgradeStateTimeStamp;
    }
    public void setUpgradeStateTimeStamp(final Date upgradeStateTimeStamp) {
        this.upgradeStateTimeStamp = upgradeStateTimeStamp;
    }
    public String getUpgradeErrorCode() {
        return this.upgradeErrorCode;
    }
    public void setUpgradeErrorCode(final String upgradeErrorCode) {
        this.upgradeErrorCode = upgradeErrorCode;
    }
    public String getUpgradeMode() {
        return this.upgradeMode;
    }
    public void setUpgradeMode(final String upgradeMode) {
        this.upgradeMode = upgradeMode;
    }
    public String getDownloadWindow() {
        return this.downloadWindow;
    }
    public void setDownloadWindow(final String downloadWindow) {
        this.downloadWindow = downloadWindow;
    }
    public String getUpgradeWindow() {
        return this.upgradeWindow;
    }
    public void setUpgradeWindow(final String upgradeWindow) {
        this.upgradeWindow = upgradeWindow;
    }
}
