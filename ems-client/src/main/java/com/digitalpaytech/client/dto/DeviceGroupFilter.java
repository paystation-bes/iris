package com.digitalpaytech.client.dto;

import java.io.Serializable;

import com.digitalpaytech.annotation.StoryAlias;

@StoryAlias(DeviceGroupFilter.ALIAS)
public class DeviceGroupFilter implements Serializable {
    public static final String ALIAS = "Device Group Filter";
    
    public static final String ALIAS_STATUS = "status";
    public static final String ALIAS_PAGE = "page";
    public static final String ALIAS_ITEMS_PER_PAGE = "items per page";
    
    private static final long serialVersionUID = 6843671018714729515L;
    
    private String status;
    
    private Integer currentPage;
    private Long lastAccessed;
    
    public DeviceGroupFilter() {
        
    }

    @StoryAlias(ALIAS_STATUS)
    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(final String status) {
        this.status = status;
    }

    @StoryAlias(ALIAS_PAGE)
    public final Integer getPage() {
        return this.currentPage;
    }

    public final void setPage(final Integer page) {
        this.currentPage = page;
    }

    public final Long getLastAccessed() {
        return this.lastAccessed;
    }

    public final void setLastAccessed(final Long lastAccessed) {
        this.lastAccessed = lastAccessed;
    }
    
}
