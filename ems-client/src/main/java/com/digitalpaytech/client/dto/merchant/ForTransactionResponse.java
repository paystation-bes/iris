package com.digitalpaytech.client.dto.merchant;

import java.util.SortedMap;
import java.util.UUID;

public class ForTransactionResponse {
    private boolean isCaptureReversalSupported;
    private boolean isChargeReversalSupported;
    private boolean isPreAuthReversalSupported;
    private boolean isRefundReversalSupported;
    private boolean isPreAuthCaptureSupported;
    private boolean isPaymentInquirySupported;
    private boolean isProfilesSupported;
    private boolean isRefundSupported;

    private long customerId;
    private long referenceCounter;
    
    private Integer closeQuarterOfDay;
    private Integer badCardThreshold;
    private Integer numberOfRetries;
    
    private String microServiceName;
    private String processorType;
    private String processorUrl;
    private String timeZone;
    
    private UUID terminalToken;
    
    private SortedMap<String, String> merchantConfiguration;
    private SortedMap<String, String> processorConfiguration;
    private SortedMap<String, String> terminalConfiguration;

    public ForTransactionResponse() {
    }

    public boolean isCaptureReversalSupported() {
        return this.isCaptureReversalSupported;
    }

    public void setCaptureReversalSupported(final boolean isCaptureReversalSupported) {
        this.isCaptureReversalSupported = isCaptureReversalSupported;
    }

    public boolean isChargeReversalSupported() {
        return this.isChargeReversalSupported;
    }

    public void setChargeReversalSupported(final boolean isChargeReversalSupported) {
        this.isChargeReversalSupported = isChargeReversalSupported;
    }

    public boolean isPreAuthReversalSupported() {
        return this.isPreAuthReversalSupported;
    }

    public void setPreAuthReversalSupported(final boolean isPreAuthReversalSupported) {
        this.isPreAuthReversalSupported = isPreAuthReversalSupported;
    }

    public boolean isRefundReversalSupported() {
        return this.isRefundReversalSupported;
    }

    public void setRefundReversalSupported(final boolean isRefundReversalSupported) {
        this.isRefundReversalSupported = isRefundReversalSupported;
    }

    public boolean isPreAuthCaptureSupported() {
        return this.isPreAuthCaptureSupported;
    }

    public void setPreAuthCaptureSupported(final boolean isPreAuthCaptureSupported) {
        this.isPreAuthCaptureSupported = isPreAuthCaptureSupported;
    }

    public boolean isPaymentInquirySupported() {
        return this.isPaymentInquirySupported;
    }

    public void setPaymentInquirySupported(final boolean isPaymentInquirySupported) {
        this.isPaymentInquirySupported = isPaymentInquirySupported;
    }

    public boolean isProfilesSupported() {
        return this.isProfilesSupported;
    }

    public void setProfilesSupported(final boolean isProfilesSupported) {
        this.isProfilesSupported = isProfilesSupported;
    }

    public boolean isRefundSupported() {
        return this.isRefundSupported;
    }

    public void setRefundSupported(final boolean isRefundSupported) {
        this.isRefundSupported = isRefundSupported;
    }

    public long getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(final long customerId) {
        this.customerId = customerId;
    }

    public long getReferenceCounter() {
        return this.referenceCounter;
    }

    public void setReferenceCounter(final long referenceCounter) {
        this.referenceCounter = referenceCounter;
    }

    public Integer getBadCardThreshold() {
        return this.badCardThreshold;
    }

    public void setBadCardThreshold(final Integer badCardThreshold) {
        this.badCardThreshold = badCardThreshold;
    }

    public Integer getNumberOfRetries() {
        return this.numberOfRetries;
    }

    public void setNumberOfRetries(final Integer numberOfRetries) {
        this.numberOfRetries = numberOfRetries;
    }

    public String getMicroServiceName() {
        return this.microServiceName;
    }

    public void setMicroServiceName(final String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public String getProcessorType() {
        return this.processorType;
    }

    public void setProcessorType(final String processorType) {
        this.processorType = processorType;
    }

    public String getProcessorUrl() {
        return this.processorUrl;
    }

    public void setProcessorUrl(final String processorUrl) {
        this.processorUrl = processorUrl;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }

    public UUID getTerminalToken() {
        return this.terminalToken;
    }

    public void setTerminalToken(final UUID terminalToken) {
        this.terminalToken = terminalToken;
    }

    public SortedMap<String, String> getMerchantConfiguration() {
        return this.merchantConfiguration;
    }

    public void setMerchantConfiguration(final SortedMap<String, String> merchantConfiguration) {
        this.merchantConfiguration = merchantConfiguration;
    }

    public SortedMap<String, String> getProcessorConfiguration() {
        return this.processorConfiguration;
    }

    public void setProcessorConfiguration(final SortedMap<String, String> processorConfiguration) {
        this.processorConfiguration = processorConfiguration;
    }

    public SortedMap<String, String> getTerminalConfiguration() {
        return this.terminalConfiguration;
    }

    public void setTerminalConfiguration(final SortedMap<String, String> terminalConfiguration) {
        this.terminalConfiguration = terminalConfiguration;
    }

    public Integer getCloseQuarterOfDay() {
        return this.closeQuarterOfDay;
    }

    public void setCloseQuarterOfDay(final Integer closeQuarterOfDay) {
        this.closeQuarterOfDay = closeQuarterOfDay;
    }
}
