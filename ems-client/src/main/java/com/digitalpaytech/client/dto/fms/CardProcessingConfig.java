package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardProcessingConfig implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6801303662945274261L;
    @JsonProperty("enableOnlineComm")
    private boolean enableOnlineComm;
    @JsonProperty("enableOnlineProcess")
    private boolean enableOnlineProcess;
    @JsonProperty("acceptCardsOffline")
    private boolean acceptCardsOffline;
    @JsonProperty("acceptPasscard")
    private boolean acceptPasscard;
    
    public CardProcessingConfig() {
    }

    public CardProcessingConfig(final boolean enableOnlineComm, 
                                final boolean enableOnlineProcess, 
                                final boolean acceptCardsOffline, 
                                final boolean acceptPasscard) {
        this.enableOnlineComm = enableOnlineComm;
        this.enableOnlineProcess = enableOnlineProcess;
        this.acceptCardsOffline = acceptCardsOffline;
        this.acceptPasscard = acceptPasscard;
    }

    public final boolean isEnableOnlineComm() {
        return this.enableOnlineComm;
    }

    public final void setEnableOnlineComm(final boolean enableOnlineComm) {
        this.enableOnlineComm = enableOnlineComm;
    }

    public final boolean isEnableOnlineProcess() {
        return this.enableOnlineProcess;
    }

    public final void setEnableOnlineProcess(final boolean enableOnlineProcess) {
        this.enableOnlineProcess = enableOnlineProcess;
    }

    public final boolean isAcceptCardsOffline() {
        return this.acceptCardsOffline;
    }

    public final void setAcceptCardsOffline(final boolean acceptCardsOffline) {
        this.acceptCardsOffline = acceptCardsOffline;
    }

    public final boolean isAcceptPasscard() {
        return this.acceptPasscard;
    }

    public final void setAcceptPasscard(final boolean acceptPasscard) {
        this.acceptPasscard = acceptPasscard;
    }
    
    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("\r\nenableOnlineComm: ").append(this.enableOnlineComm).append(", enableOnlineProcess: ").append(this.enableOnlineProcess);
        bdr.append(", acceptCardsOffline: ").append(this.acceptCardsOffline).append(", acceptPasscard: ").append(this.acceptPasscard);
        return bdr.toString();
    }
}
