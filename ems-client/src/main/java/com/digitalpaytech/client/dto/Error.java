package com.digitalpaytech.client.dto;

import java.io.Serializable;

public class Error<T> implements Serializable {
    private static final long serialVersionUID = -5471964218333787338L;
    private T identifier;
    private String message;
    
    public Error() {
        super();
    }
    
    public Error(final String message) {
        this.message = message;
    }
    
    public Error(final T identifier, final String message) {
        this.identifier = identifier;
        this.message = message;
    }
    
    public final T getIdentifier() {
        return this.identifier;
    }
    
    public final void setIdentifier(final T identifier) {
        this.identifier = identifier;
    }
    
    public final String getMessage() {
        return this.message;
    }
    
    public final void setMessage(final String message) {
        this.message = message;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(35).append("Error{identifier='").append(this.identifier).append('\'').append(", message='")
                .append(this.message).append('\'').append('}');
        return sb.toString();
    }
}
