package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleConfiguration implements Serializable {
    
  
    /**
     * 
     */
    private static final long serialVersionUID = 428550245681570671L;
    @JsonProperty("schedule")
    private boolean schedule;
    @JsonProperty("date")
    private String date;
    @JsonProperty("canManage")
    private boolean canManage;
    
    public ScheduleConfiguration(final boolean schedule, final boolean canManage) {
        this.schedule = schedule;
    }
    
    public ScheduleConfiguration(final boolean schedule, final boolean canManage, final String date) {
        this.schedule = schedule;
        this.date = date;
    }
    
    public final boolean getSchedule() {
        return this.schedule;
    }
    
    public final void setSchedule(final boolean schedule) {
        this.schedule = schedule;
    }
    
    public final String getDate() {
        return this.date;
    }
    
    public final void setDate(final String date) {
        this.date = date;
    }
    
    public final boolean isCanManage() {
        return canManage;
    }
    
    public final void setCanManage(final boolean canManage) {
        this.canManage = canManage;
    }
    
}
