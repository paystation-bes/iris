package com.digitalpaytech.client.dto.corecps;

import java.util.Map;
import java.util.SortedMap;
import java.util.UUID;

import org.apache.commons.lang.builder.ToStringBuilder;

public class TransactionRequest {
    private Integer amount;
    private String authorizationNumber;
    private String cardExpiry;
    private String cardType;
    private SortedMap<String, String> externalCardToken;
    private SortedMap<String, String> cardData;
    private UUID chargeToken;
    private UUID terminalToken;
    private Integer customerId;
    private String deviceSerialNumber;
    private String last4Digits;
    private Map<String, Object> paymentDetails;
    private String paymentType;
    private String processorTransactionId;
    private String processorType;
    private String purchaseUTC;
    private String referenceNumber;
    
    public TransactionRequest() {
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", this.amount).append("authorizationNumber", this.authorizationNumber)
                .append("cardExpiry", this.cardExpiry).append("cardType", this.cardType).append("externalCardToken", this.externalCardToken)
                .append("chargeToken", this.chargeToken).append("terminalToken", this.terminalToken).append("customerId", this.customerId)
                .append("deviceSerialNumber", this.deviceSerialNumber).append("last4Digits", this.last4Digits)
                .append("paymentDetails", this.paymentDetails).append("cardData", this.cardData)
                .append("paymentType", this.paymentType).append("processorTransactionId", this.processorTransactionId)
                .append("processorType", this.processorType).append("purchaseUTC", this.purchaseUTC).append("referenceNumber", this.referenceNumber)
                .toString();
    }
    
    public enum ResponseStatus {
        SUCCESS, FAILURE, DUPLICATED
    }
    
    public Integer getAmount() {
        return this.amount;
    }
    
    public void setAmount(final Integer amount) {
        this.amount = amount;
    }
    
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public String getCardExpiry() {
        return this.cardExpiry;
    }
    
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }
    
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public SortedMap<String, String> getExternalCardToken() {
        return this.externalCardToken;
    }
    
    public void setExternalCardToken(final SortedMap<String, String> externalCardToken) {
        this.externalCardToken = externalCardToken;
    }
    
    public SortedMap<String, String> getCardData() {
        return this.cardData;
    }
    
    public void setCardData(final SortedMap<String, String> cardData) {
        this.cardData = cardData;
    }
    
    public UUID getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final UUID chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    public UUID getTerminalToken() {
        return this.terminalToken;
    }
    
    public void setTerminalToken(final UUID terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public String getLast4Digits() {
        return this.last4Digits;
    }
    
    public void setLast4Digits(final String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public Map<String, Object> getPaymentDetails() {
        return this.paymentDetails;
    }
    
    public void setPaymentDetails(final Map<String, Object> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
    
    public String getPaymentType() {
        return this.paymentType;
    }
    
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }
    
    public String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    public String getProcessorType() {
        return this.processorType;
    }
    
    public void setProcessorType(final String processorType) {
        this.processorType = processorType;
    }
    
    public String getPurchaseUTC() {
        return this.purchaseUTC;
    }
    
    public void setPurchaseUTC(final String purchaseUTC) {
        this.purchaseUTC = purchaseUTC;
    }
    
    public String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
