package com.digitalpaytech.client.dto.corecps;

import java.util.UUID;

public class TransactionResponse {
    private UUID cardToken;

    public TransactionResponse() {
    }
    
    public TransactionResponse(final UUID cardToken) {
        this.cardToken = cardToken;
    }

    public UUID getCardToken() {
        return this.cardToken;
    }

    public void setCardToken(final UUID cardToken) {
        this.cardToken = cardToken;
    }
    
}
