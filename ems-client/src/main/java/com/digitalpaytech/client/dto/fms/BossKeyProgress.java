package com.digitalpaytech.client.dto.fms;

public class BossKeyProgress {
    private boolean pending;
    private String dateGenerated;
    
    public BossKeyProgress() {
        super();
    }
    
    public BossKeyProgress(final boolean pending, final String dateGenerated) {
        super();
        this.pending = pending;
        this.dateGenerated = dateGenerated;
    }
    
    public final boolean isPending() {
        return pending;
    }
    
    public final void setPending(final boolean pending) {
        this.pending = pending;
    }
    
    public final String getDateGenerated() {
        return dateGenerated;
    }
    
    public final void setDateGenerated(final String dateGenerated) {
        this.dateGenerated = dateGenerated;
    }
    
}
