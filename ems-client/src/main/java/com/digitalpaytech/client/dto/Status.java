package com.digitalpaytech.client.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Status implements Serializable {
    
    private static final long serialVersionUID = 3328283499172752511L;
    private String responseStatus;
    private List<Error<String>> errors;
    
    public Status() {
        super();
    }
    
    public Status(final String message) {
        this.errors = new ArrayList<>();
        final Error<String> error = new Error<>(message);
        this.errors.add(error);
    }
    
    public Status(final String responseStatus, final String message) {
        this.responseStatus = responseStatus;
        this.errors = new ArrayList<>();
        final Error<String> error = new Error<>(message);
        this.errors.add(error);
        
    }
    
    public Status(final String responseStatus, final List<Error<String>> errors) {
        this.responseStatus = responseStatus;
        this.errors = errors;
    }
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    
    public final String getResponseStatus() {
        return this.responseStatus;
    }
    
    public final void setResponseStatus(final String responseStatus) {
        this.responseStatus = responseStatus;
    }
    
    public final List<Error<String>> getErrors() {
        return this.errors;
    }
    
    public final void setErrors(final List<Error<String>> errors) {
        this.errors = errors;
    }
    
    public final void addError(final Error<String> error) {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        this.errors.add(error);
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(35).append("Status{responseStatus='").append(this.responseStatus).append('\'').append(", errors=")
                .append(this.errors).append('}');
        return sb.toString();
    }
}
