package com.digitalpaytech.client.dto.fms;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupConfiguration implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8756575584038400878L;
    @JsonProperty("cardProcessingOverride")
    private boolean cardProcessingOverride;
    @JsonProperty("irisSettings")
    private SettingsConfig irisSettings;
    @JsonProperty("bossSettings")
    private SettingsConfig bossSettings;
    
    public GroupConfiguration() {
    }
    
    public GroupConfiguration(final boolean cardProcessingOverride, final SettingsConfig irisSettings, final SettingsConfig bossSettings) {
        this.cardProcessingOverride = cardProcessingOverride;
        this.irisSettings = irisSettings;
        this.bossSettings = bossSettings;
    }

    public final boolean isCardProcessingOverride() {
        return this.cardProcessingOverride;
    }
    
    public final void setCardProcessingOverride(final boolean cardProcessingOverride) {
        this.cardProcessingOverride = cardProcessingOverride;
    }
    
    public final SettingsConfig getIrisSettings() {
        return this.irisSettings;
    }
    
    public final void setIrisSettings(final SettingsConfig irisSettings) {
        this.irisSettings = irisSettings;
    }
    
    public final SettingsConfig getBossSettings() {
        return this.bossSettings;
    }
    
    public final void setBossSettings(final SettingsConfig bossSettings) {
        this.bossSettings = bossSettings;
    }

    @Override
    public final String toString() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("\r\ncardProcessingOverride: ").append(this.cardProcessingOverride).append(", irisSettings: ").append(this.irisSettings);
        bdr.append(", bossSettings: ").append(this.bossSettings);
        return bdr.toString();
    }
}
