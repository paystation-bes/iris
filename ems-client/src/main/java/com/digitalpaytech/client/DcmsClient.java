package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.dto.LinuxConfigurationAck;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@StoryAlias(DcmsClient.ALIAS)
@ResourceGroup(name = "dcms")
public interface DcmsClient {
    String ALIAS = "dcms";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("downloadConfiguration")
    @Http(method = HttpMethod.GET, 
          uri = "/api/v1/devices/downloadFile?deviceSerialNumber={deviceSerialNumber}&customerId={customerId}"
                + "&configSnapshotId={configSnapshotId}&fileId={fileId}", 
          headers = { @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> downloadConfiguration(@Var("deviceSerialNumber") String deviceSerialNumber, @Var("customerId") Integer unifiId, 
        @Var("configSnapshotId") String configSnapshotId, @Var("fileId") String fileId);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("configurationAck")
    @Http(method = HttpMethod.POST, uri = "/api/v1/devices/{deviceSerialNumber}?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @ContentTransformerClass(JSONContentTransformer.class)
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> configurationAck(@Var("customerId") Integer unifiId, 
                                            @Var("deviceSerialNumber") String deviceSerialNumber, 
                                            @Content LinuxConfigurationAck ack);
}
