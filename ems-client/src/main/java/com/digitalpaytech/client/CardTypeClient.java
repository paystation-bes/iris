package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.transformer.JSONContentTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "cts")
@StoryAlias(CardTypeClient.ALIAS)
public interface CardTypeClient {
    String ALIAS = "Card Type Service";
    
    String CARD_TYPE_RESOLVER_END_POINT = "/api/v1/cardresolver";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getCardType")
    @Http(method = HttpMethod.POST, uri = CARD_TYPE_RESOLVER_END_POINT + "/type?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(JSONContentTransformer.class)
    RibbonRequest<ByteBuf> getCardType(@Var("customerId") int customerId, @Content String encryptedCardData);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getBinRanges")
    @Http(method = HttpMethod.GET, uri = CARD_TYPE_RESOLVER_END_POINT + "/binRanges", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getBinRanges();
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("getAllCardNames")
    @Http(method = HttpMethod.GET, uri = CARD_TYPE_RESOLVER_END_POINT + "/cardNames", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> getAllCardNames();
    
}
