package com.digitalpaytech.client;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.client.transformer.MultipartFileWrapperTransformer;
import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.clientcommunication.dto.MultipartFileWrapper;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.ContentTransformerClass;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "llps")
@StoryAlias(LicensePlateClient.ALIAS)
public interface LicensePlateClient {
    String ALIAS = "License Plate Service";
    
    String LICENSE_PLATE_END_POINT = "/api/v1/licenseplates";
    
    String PLATE_CHECK_QUERY =
            "?licensePlate={licensePlate}" + "&customerId={customerId}&locationName={locationName}" + "&isPreferredByLocation={isPreferredByLocation}"
                               + "&isLimitedByLocation={isLimitedByLocation}" + "&localDate={localDate}";
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("plateCheck")
    @Http(method = HttpMethod.GET, uri = LICENSE_PLATE_END_POINT + PLATE_CHECK_QUERY, headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> plateCheck(@Var("licensePlate") String licensePlate, @Var("customerId") int customerId,
        @Var("locationName") String locationName, @Var("isPreferredByLocation") boolean isPreferredByLocation,
        @Var("isLimitedByLocation") boolean isLimitedByLocation, @Var("localDate") String localDate);
    
    @Secure(value = true, oauth2 = true)
    @TemplateName("uploadLicensePlates")
    @Http(method = HttpMethod.POST, uri = LICENSE_PLATE_END_POINT)
    @Hystrix(validator = HttpStatusValidator.class)
    @ContentTransformerClass(MultipartFileWrapperTransformer.class)
    RibbonRequest<ByteBuf> uploadLicensePlates(@Content MultipartFileWrapper multipartFileWrapper);

    
    @Secure(value = true, oauth2 = true)
    @TemplateName("currentStatus")
    @Http(method = HttpMethod.GET, uri = "/api/v1/preferredparkerfiles/{id}?customerId={customerId}", headers = {
        @Header(name = "Content-Type", value = "application/json") })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> currentStatus(@Var("id") String fileId, @Var("customerId") int customerId);           
}
