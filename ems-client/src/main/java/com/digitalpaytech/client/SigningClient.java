package com.digitalpaytech.client;

import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "cryptoserver")
public interface SigningClient {
    @Secure(true)
    @TemplateName("currentInternalKey")
    @Http(method = HttpMethod.POST, uri = "/api/v1/signature/{algorithm}", headers = {
        @Header(name = "Content-Type", value = "application/octet-stream")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> sign(@Var("algorithm") String algorithm, @Content byte[] binary);
}
