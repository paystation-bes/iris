package com.digitalpaytech.client;

import com.digitalpaytech.client.util.HttpStatusValidator;
import com.digitalpaytech.ribbon.annotation.Secure;
import com.netflix.ribbon.RibbonRequest;
import com.netflix.ribbon.proxy.annotation.Content;
import com.netflix.ribbon.proxy.annotation.Http;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.netflix.ribbon.proxy.annotation.ResourceGroup;
import com.netflix.ribbon.proxy.annotation.TemplateName;
import com.netflix.ribbon.proxy.annotation.Var;
import com.netflix.ribbon.proxy.annotation.Http.Header;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;

import io.netty.buffer.ByteBuf;

@ResourceGroup(name = "cryptoserver")
public interface CryptoClient {
    @Secure(true)
    @TemplateName("currentInternalKey")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key/internal", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> currentInternalKey();
    
    @Secure(true)
    @TemplateName("currentExternalKey")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> currentExternalKey();
    
    @Secure(true)
    @TemplateName("listExternalKey")
    @Http(method = HttpMethod.GET, uri = "/api/v1/keyList", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> listExternalKey();
    
    @Secure(true)
    @TemplateName("rotate")
    @Http(method = HttpMethod.PUT, uri = "/api/v1/key", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> rotate();
    
    @Secure(true)
    @TemplateName("keyInfo")
    @Http(method = HttpMethod.GET, uri = "/api/v1/key/{keyAlias}/info", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> keyInfo(@Var("keyAlias") String keyAlias);
    
    @Secure(true)
    @TemplateName("export")
    @Http(method = HttpMethod.POST, uri = "/api/v1/key/{keyAlias}", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> export(@Var("keyAlias") String keyAlias, @Content String hashAlgorithm);
    
    @Secure(true)
    @TemplateName("internalEncrypt")
    @Http(method = HttpMethod.POST, uri = "/api/v1/encrypt/internal", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> encrypt(@Content String decrypted);
    
    @Secure(true)
    @TemplateName("decrypt")
    @Http(method = HttpMethod.POST, uri = "/api/v1/decrypt", headers = {
        @Header(name = "Content-Type", value = "text/plain")
    })
    @Hystrix(validator = HttpStatusValidator.class)
    RibbonRequest<ByteBuf> decrypt(@Content String encrypted);
}
