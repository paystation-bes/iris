package com.digitalpaytech.security;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.digitalpaytech.auth.WebLogoutSuccessHandler;
import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.impl.ActivityLoginServiceImpl;

public class WebLogoutSuccessHandlerTest {
    
    @Test
    public final void testOnLogoutSuccess() throws IOException, ServletException {
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("GET", "");
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        final Authentication authentication = createAuthentication(userAccount, true);
        
        final WebLogoutSuccessHandler handler = new WebLogoutSuccessHandler();
        
        final ActivityLoginService activityLoginService = new ActivityLoginServiceImpl() {
            
            @Override
            public Serializable saveActivityLogin(final ActivityLogin activityLogin) {
                return new ActivityLogin();
            }
        };
        
        handler.setActivityLoginService(activityLoginService);
        handler.onLogoutSuccess(request, response, authentication);
        
        assertEquals(response.getRedirectedUrl(), "/");
    }
    
    private Authentication createAuthentication(final UserAccount userAccount, final boolean isComplexPassword) {
        
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(1, 1, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(isComplexPassword);
        return new UsernamePasswordAuthenticationToken(user, "password", authorities);
    }
}
