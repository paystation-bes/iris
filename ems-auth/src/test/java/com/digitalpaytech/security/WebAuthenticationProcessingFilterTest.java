package com.digitalpaytech.security;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.digitalpaytech.auth.WebAuthenticationProcessingFilter;
import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.ActivityLoginServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * WebAuthenticationProcessingFilterTest
 * 
 * @author Brian Kim
 */
public class WebAuthenticationProcessingFilterTest {
    
    @Autowired
    private WebAuthenticationProcessingFilter filter;
    
    @Autowired
    private AuthenticationManager manager;
    
    @Before
    public final void setUp() throws NoSuchMethodException {
        this.filter = new WebAuthenticationProcessingFilter();
    }
    
    @After
    public final void cleanUp() {
        this.filter = null;
    }
    
    @Test
    public final void testUserLogin() throws Exception {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                final WebUser user = new WebUser(1, 1, "testWebUser", "password", "salt", true, authorities);
                return new UsernamePasswordAuthenticationToken(user, "password", authorities);
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setCustomerType(new CustomerType(1, "", new Date(), 1));
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                account.setFirstName("TestFirstName");
                account.setLastName("TestLastName");
                return account;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                return account;
            }
            
            @Override
            public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
                return new ArrayList<RolePermission>();
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE, "20")).andReturn("5");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE,
                                                              EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE)).andReturn("US/Pacific");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = new ArrayList<ActivityLogin>();
        final LoginResultType loginResultType = new LoginResultType();
        loginResultType.setId(1);
        final ActivityLogin activityLogin = new ActivityLogin();
        activityLogin.setLoginResultType(loginResultType);
        lists.add(activityLogin);
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        final Authentication authentication = this.filter.attemptAuthentication(request, response);
        assertNotNull(authentication);
    }
    
    @Test
    public final void testSessionInvalidate() throws Exception {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        request.getSession();
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                final WebUser user = new WebUser(1, 1, "testWebUser", "password", "salt", true, authorities);
                return new UsernamePasswordAuthenticationToken(user, "password", authorities);
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setCustomerType(new CustomerType(1, "", new Date(), 1));
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                account.setFirstName("TestFirstName");
                account.setLastName("TestLastName");
                return account;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                return account;
            }
            
            @Override
            public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
                return new ArrayList<RolePermission>();
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE, "20")).andReturn("5");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE,
                                                              EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE)).andReturn("US/Pacific");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        final Authentication authentication = this.filter.attemptAuthentication(request, response);
        assertNotNull(authentication);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testInvalidUsername() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "!@#$%^©");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                return null;
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("IUAjJCVe", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        this.filter.setUserAccountService(userAccountService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testEmptyUsername() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testUsernameTooLong() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY,
                             "sdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsd111111");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService
                                .findInvalidActivityLoginByLastModifiedGMT("sdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsdsdfsdfdsfdsfsdfdsfsdfsdfsdfdsfsdfdsfsdfsdfsdfsdfsd111111",
                                                                           StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    public final void testUsernameTooShort() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "sdfsdfd");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("sdfsdfd", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testEmptyPassword() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testTooShortPassword() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "123");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        this.filter.setUserAccountService(userAccountService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testTooLongPassword() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "testtesttesttest");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        this.filter.setUserAccountService(userAccountService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = AuthenticationServiceException.class)
    public final void testAuthenticationServiceException() throws Exception {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        request.getSession();
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final UserAccount ua = EasyMock.createMock(UserAccount.class);
        EasyMock.expect(ua.getId()).andReturn(new Integer(1));
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.saveActivityLogin(lists.get(0))).andReturn(new ActivityLogin());
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                throw new AuthenticationServiceException("AuthenticationServiceException");
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
        };
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test(expected = AuthenticationException.class)
    public final void testAuthenticationException() throws Exception {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        request.getSession();
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                throw new ProviderNotFoundException("");
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount ua = new UserAccount();
                ua.setId(1);
                return ua;
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final ActivityLoginService activityLoginService = new ActivityLoginServiceImpl() {
            @Override
            public Serializable saveActivityLogin(final ActivityLogin activityLogin) {
                return activityLogin;
            }
            
            @Override
            public List<ActivityLogin> findInvalidActivityLoginByLastModifiedGMT(final String username, final int maxLockTimeMinutes) {
                final List<ActivityLogin> lists = createActivityLoginList();
                return lists;
            }
            
            @Override
            public int findCountInvalidActivityLoginByActivityGMT(final Integer userAccountId, final int loginResultTypeId,
                final int maxLockTimeMinutes) {
                return 0;
            }
            
            @Override
            public Date findLastSuccessByUserAccountId(final Integer userAccountId) {
                return new Date();
            }
            
        };
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        this.filter.attemptAuthentication(request, response);
    }
    
    @Test
    public final void testComplexPassword() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "Password$1");
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                final WebUser user = new WebUser(1, 1, "testWebUser", "Password$1", "salt", true, authorities);
                return new UsernamePasswordAuthenticationToken(user, "Password$1", authorities);
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setCustomerType(new CustomerType(1, "", new Date(), 1));
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                account.setFirstName("TestFirstName");
                account.setLastName("TestLastName");
                return account;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                return account;
            }
            
            @Override
            public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
                return new ArrayList<RolePermission>();
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE, "20")).andReturn("5");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE,
                                                              EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE)).andReturn("US/Pacific");
        
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        final Authentication authentication = this.filter.attemptAuthentication(request, response);
        
        assertNotNull(authentication);
        assertTrue(((WebUser) authentication.getPrincipal()).isComplexPassword());
    }
    
    @Test
    public final void testNotComplexPassword() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "password");
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        // mock the AuthenticationManager.
        final List<AuthenticationProvider> providers = new ArrayList<AuthenticationProvider>();
        providers.add(new DaoAuthenticationProvider());
        this.manager = new ProviderManager(providers) {
            @Override
            public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
                final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                final WebUser user = new WebUser(1, 1, "testWebUser", "password", "salt", true, authorities);
                return new UsernamePasswordAuthenticationToken(user, "password", authorities);
            }
        };
        
        final UserAccountService userAccountService = new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerType(new CustomerType(1, "", new Date(), 1));
                customer.setCustomerProperties(properties);
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                account.setFirstName("TestFirstName");
                account.setLastName("TestLastName");
                return account;
            }
            
            @Override
            public UserAccount findUndeletedUserAccount(final String userName) {
                final UserAccount account = new UserAccount();
                account.setId(1);
                final Customer customer = new Customer();
                final Set<CustomerProperty> properties = new HashSet<CustomerProperty>();
                customer.setCustomerProperties(properties);
                customer.setIsMigrated(true);
                account.setCustomer(customer);
                return account;
            }
            
            @Override
            public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
                return new ArrayList<RolePermission>();
            }
            
            @Override
            public UserAccount findUserAccountForLogin(final int userAccountId) {
                return findUserAccount(userAccountId);
            }
        };
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("10");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE, "20")).andReturn("5");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE,
                                                              EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE)).andReturn("US/Pacific");
        
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = createActivityLoginList();
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.expect(activityLoginService.findLastSuccessByUserAccountId(new Integer(1))).andReturn(new Date());
        EasyMock.expect(activityLoginService.findCountInvalidActivityLoginByActivityGMT(new Integer(1),
                                                                                        WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                                        StandardConstants.LIMIT_15)).andReturn(1);
        EasyMock.replay(activityLoginService);
        
        this.filter.setAuthenticationManager(this.manager);
        this.filter.setUserAccountService(userAccountService);
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        final Authentication authentication = this.filter.attemptAuthentication(request, response);
        
        assertNotNull(authentication);
        assertFalse(((WebUser) authentication.getPrincipal()).isComplexPassword());
    }
    
    @Test(expected = BadCredentialsException.class)
    public final void testUserLock() {
        
        // Build mock request
        final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/secure/j_spring_security_check");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, "testWebUser");
        request.setParameter(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, "");
        
        final EmsPropertiesService emsPropertiesService = EasyMock.createMock(EmsPropertiesService.class);
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT, EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT,
                                                              false)).andReturn("1");
        EasyMock.expect(emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                              EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false)).andReturn("15");
        EasyMock.replay(emsPropertiesService);
        
        final List<ActivityLogin> lists = new ArrayList<ActivityLogin>();
        lists.add(new ActivityLogin());
        lists.add(new ActivityLogin());
        
        final ActivityLoginService activityLoginService = EasyMock.createMock(ActivityLoginService.class);
        EasyMock.expect(activityLoginService.findInvalidActivityLoginByLastModifiedGMT("testWebUser", StandardConstants.LIMIT_15)).andReturn(lists);
        EasyMock.replay(activityLoginService);
        
        // Build mock response
        final MockHttpServletResponse response = new MockHttpServletResponse();
        
        this.filter.setEmsPropertiesService(emsPropertiesService);
        this.filter.setActivityLoginService(activityLoginService);
        
        this.filter.attemptAuthentication(request, response);
    }
    
    private List<ActivityLogin> createActivityLoginList() {
        
        final List<ActivityLogin> lists = new ArrayList<ActivityLogin>();
        final LoginResultType loginResultType = new LoginResultType();
        final ActivityLogin activityLogin = new ActivityLogin();
        
        loginResultType.setId(1);
        activityLogin.setLoginResultType(loginResultType);
        lists.add(activityLogin);
        
        return lists;
    }
}
