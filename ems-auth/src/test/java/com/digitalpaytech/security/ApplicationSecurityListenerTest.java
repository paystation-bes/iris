package com.digitalpaytech.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.digitalpaytech.auth.ApplicationSecurityListener;
import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.impl.ActivityLoginServiceImpl;

public class ApplicationSecurityListenerTest {
    
    @Test
    public final void testOnApplicationEvent() {
        
        final AuthenticationSuccessEvent event = new AuthenticationSuccessEvent(createAuthentication(new UserAccount(), true));
        
        final ActivityLoginService activityLoginService = new ActivityLoginServiceImpl() {
            public Serializable saveActivityLogin(final ActivityLogin activityLogin) {
                return new ActivityLogin();
            }
        };
        
        final ApplicationSecurityListener listener = new ApplicationSecurityListener();
        listener.setActivityLoginService(activityLoginService);
        
        listener.onApplicationEvent(event);
    }
    
    private Authentication createAuthentication(final UserAccount userAccount, final boolean isComplexPassword) {
        
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(1, 1, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(isComplexPassword);
        return new UsernamePasswordAuthenticationToken(user, "password", authorities);
    }
}
