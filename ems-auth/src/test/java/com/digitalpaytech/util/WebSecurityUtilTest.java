package com.digitalpaytech.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

public class WebSecurityUtilTest {
    private static final String DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss";
    private static final String TIME_ZONE_GMT = "GMT";
    
    @Test
    public final void appendSessionTokenString() {
        final MockHttpSession sess = new MockHttpSession();
        //TODO Remove SessionToken
        //     sess.setAttribute(WebSecurityConstants.SESSION_TOKEN, "12345");
        final MockHttpServletRequest req = new MockHttpServletRequest();
        req.setSession(sess);
        
        //        TODO Remove SessionToken
        //        		String path = WebSecurityUtil.appendSessionTokenString(req, "redirect:/password?");
        //        		assertEquals("redirect:/password?sessionToken=12345", path);
        //        		
        //        		path = WebSecurityUtil.appendSessionTokenString(req, "redirect:/password");
        //        		assertEquals("redirect:/password?sessionToken=12345", path);
    }
    
    @Test
    public final void getCurrentDate() {
        
        try {
            final Date date = WebSecurityUtil.getCurrentDate(DATE_FORMAT, TIME_ZONE_GMT);
            assertNotNull(date);
            
        } catch (ParseException e) {
            fail();
        }
    }
    
    @Test
    public final void getCurrentDateInGmt() {
        try {
            final Date date = WebSecurityUtil.getCurrentDateInGmt();
            assertNotNull(date);
        } catch (ParseException e) {
            fail();
        }
    }
}
