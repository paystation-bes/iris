package com.digitalpaytech.auth;

import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

/**
 * WebLogoutFilter
 * 
 * @author Brian Kim
 */
public class WebLogoutFilter extends LogoutFilter {

	public WebLogoutFilter(final String logoutSuccessUrl, final LogoutHandler[] handlers) {
		super(logoutSuccessUrl, handlers);
	}
}
