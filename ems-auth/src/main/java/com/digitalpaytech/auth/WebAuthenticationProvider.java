package com.digitalpaytech.auth;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class WebAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    
    private static final String CLASS_BAD_CREDENTIALS = "AbstractUserDetailsAuthenticationProvider.badCredentials";
    private static final String BAD_CREDENTIALS = "Bad credentials";
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SaltSource saltSource;
    @Autowired
    private SaltSource saltSourceEms6;
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Override
    protected final void additionalAuthenticationChecks(final UserDetails userDetails, final UsernamePasswordAuthenticationToken authentication)
        throws AuthenticationException {
        Object salt = null;
        
        final Object tokenBuffer = authentication.getCredentials();
        if (tokenBuffer != null && tokenBuffer instanceof OAuth2AccessToken) {
            final OAuth2AccessToken accessToken = (OAuth2AccessToken) tokenBuffer;
            if (accessToken.getExpiration().before(new Date())) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_TOKEN_EXPIRED);
            }
        } else {
            if (this.saltSource != null) {
                salt = this.saltSource.getSalt(userDetails);
            }
            
            if (authentication.getCredentials() == null) {
                //            logger.debug("Authentication failed: no credentials provided");
                
                throw new BadCredentialsException(this.messages.getMessage(CLASS_BAD_CREDENTIALS, BAD_CREDENTIALS));
            }
            
            final String presentedPassword = authentication.getCredentials().toString();
            
            if (!this.passwordEncoder.isPasswordValid(userDetails.getPassword(), presentedPassword, salt)) {
                //            logger.debug("Authentication failed: password does not match stored value");
                
                // EMS 6 encoded password is 40 characters.
                if (userDetails.getPassword().length() == StandardConstants.CONSTANT_40) {
                    final PasswordEncoder legacyPasswordEncoder = new ShaPasswordEncoder();
                    
                    // Use SystemWideSaltSource implementation to authenticate EMS 6 password. 
                    if (!legacyPasswordEncoder.isPasswordValid(userDetails.getPassword(), presentedPassword, this.saltSourceEms6.getSalt(userDetails))) {
                        throw new BadCredentialsException(this.messages.getMessage(CLASS_BAD_CREDENTIALS, BAD_CREDENTIALS));
                    }
                } else {
                    throw new BadCredentialsException(this.messages.getMessage(CLASS_BAD_CREDENTIALS, BAD_CREDENTIALS));
                }
            }
        }
    }
    
    @Override
    @SuppressWarnings({"checkstyle:illegalcatch", "PMD.AvoidRethrowingException"})
    protected final UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken authentication)
        throws AuthenticationException {
        UserDetails loadedUser;
        
        try {
            loadedUser = getUserDetailsService().loadUserByUsername(username);
        } catch (UsernameNotFoundException notFound) {
            throw notFound;
        } catch (Exception repositoryProblem) {
            throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }
        
        if (loadedUser == null) {
            throw new AuthenticationServiceException("UserDetailsService returned null, which is an interface contract violation");
        }
        return loadedUser;
    }
    
    public final PasswordEncoder getPasswordEncoder() {
        return this.passwordEncoder;
    }
    
    public final void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    public final SaltSource getSaltSource() {
        return this.saltSource;
    }
    
    public final void setSaltSource(final SaltSource saltSource) {
        this.saltSource = saltSource;
    }
    
    public final UserDetailsService getUserDetailsService() {
        return this.userDetailsService;
    }
    
    public final void setUserDetailsService(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
    
    public final SaltSource getSaltSourceEms6() {
        return this.saltSourceEms6;
    }

    public final void setSaltSourceEms6(final SaltSource saltSourceEms6) {
        this.saltSourceEms6 = saltSourceEms6;
    }
    
}
