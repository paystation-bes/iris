package com.digitalpaytech.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.digitalpaytech.auth.dto.TokenUser;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.TLSUtils;

@Controller
public class OAuthController {
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private WebJdbcDaoImpl userDetailsDao;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    @RequestMapping(value = "/oauth_migration/authorize.html", method = RequestMethod.GET)
    public String authorize(final HttpServletRequest request, final Authentication authentication) {
        final String response;
        
        if (this.tlsUtils.shouldRedirect(request)) {
            this.tlsUtils.redirect("/oauth_migration/authorize.html");
        }
        
        final Object principal = authentication.getPrincipal();
        if (principal instanceof WebUser) {
            response = "redirect:/secure/terms.html";
        } else if (principal instanceof TokenUser) {
            // Log them out with new session
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            
            session = request.getSession(true);
            
            session.setAttribute(TokenUser.SESSION_KEY, principal);
            response = "migrate";
        } else {
            throw new IllegalStateException("Unrecognized user type: " + principal.getClass());
        }
        
        return response;
    }
}
