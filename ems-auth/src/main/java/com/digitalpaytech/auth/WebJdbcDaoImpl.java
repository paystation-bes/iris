package com.digitalpaytech.auth;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContextException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

/**
 * WebJdbcDaoImpl
 * 
 * @author Brian Kim
 */
public class WebJdbcDaoImpl extends JdbcDaoImpl {
    
    private MappingSqlQuery authoritiesByUsernameMapping;
    private MappingSqlQuery usersByUsernameMapping;
    private String authoritiesByUsernameQuery;
    private String rolePrefix;
    private String usersByUsernameQuery;
    
    public WebJdbcDaoImpl() {
        this.rolePrefix = "";
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT u.Id, u.CustomerId, u.UserName, u.Password, u.PasswordSalt, ut.Name, ct.Name, c.TrialExpiryGMT, parentCt.Name, parentC.TrialExpiryGMT ");
        sb.append("FROM UserAccount u ");
        sb.append("INNER JOIN UserStatusType ut ON u.UserStatusTypeId = ut.Id INNER JOIN Customer c ON u.CustomerId = c.Id ");
        sb.append("INNER JOIN CustomerStatusType ct ON c.CustomerStatusTypeId = ct.Id LEFT OUTER JOIN Customer parentC ON c.parentCustomerId = parentC.Id ");
        sb.append("LEFT OUTER JOIN CustomerStatusType parentCt ON parentC.CustomerStatusTypeId = parentCt.Id ");
        sb.append("WHERE LOWER(u.UserName) = ? AND u.UserStatusTypeId <> 2");
        this.usersByUsernameQuery = sb.toString();
        this.authoritiesByUsernameQuery = "SELECT u.UserName, r.Name AS Role FROM UserAccount u LEFT OUTER JOIN UserRole ur ON u.Id = ur.UserAccountId LEFT OUTER JOIN Role r ON ur.RoleId = r.Id WHERE u.UserName = ?";
    }
    
    public final void setAuthoritiesByUsernameMapping(final MappingSqlQuery authoritiesByUsernameQuery) {
        this.authoritiesByUsernameMapping = authoritiesByUsernameQuery;
    }
    
    public final MappingSqlQuery getAuthoritiesByUsernameMapping() {
        return this.authoritiesByUsernameMapping;
    }
    
    public final void setAuthoritiesByUsernameQuery(final String queryString) {
        this.authoritiesByUsernameQuery = queryString;
    }
    
    public final String getAuthoritiesByUsernameQuery() {
        return this.authoritiesByUsernameQuery;
    }
    
    public final void setRolePrefix(final String rolePrefix) {
        this.rolePrefix = rolePrefix;
    }
    
    public final String getRolePrefix() {
        return this.rolePrefix;
    }
    
    public final void setUsersByUsernameMapping(final MappingSqlQuery usersByUsernameQuery) {
        this.usersByUsernameMapping = usersByUsernameQuery;
    }
    
    public final MappingSqlQuery getUsersByUsernameMapping() {
        return this.usersByUsernameMapping;
    }
    
    public final void setUsersByUsernameQuery(final String usersByUsernameQueryString) {
        this.usersByUsernameQuery = usersByUsernameQueryString;
    }
    
    public final String getUsersByUsernameQuery() {
        return this.usersByUsernameQuery;
    }
    
    public final UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {
        
        final Object[] param = new Object[1];
        param[0] = (username == null) ? null : username.toLowerCase();
        final List users = this.usersByUsernameMapping.execute(param);
        
        if (users.size() == 0) {
            throw new UsernameNotFoundException("User not found");
        }
        
        final WebUser user = (WebUser) users.get(0);
        if (user != null) {
            param[0] = user.getUsername();
        }
        
        final List<GrantedAuthority> dbAuths = this.authoritiesByUsernameMapping.execute(param);
        if (dbAuths.size() == 0) {
            throw new UsernameNotFoundException("User has no GrantedAuthority");
        } else {
            return new WebUser(user.getCustomerId(), user.getUserAccountId(), user.getUsername(), user.getPassword(), user.getSaltSource(),
                    user.isEnabled(), dbAuths);
        }
    }
    
    protected final void initDao() throws ApplicationContextException {
        initMappingSqlQueries();
    }
    
    protected final void initMappingSqlQueries() {
        setUsersByUsernameMapping(new UsersByUsernameMapping(getDataSource()));
        setAuthoritiesByUsernameMapping(new AuthoritiesByUsernameMapping(getDataSource()));
    }
    
    protected class AuthoritiesByUsernameMapping extends MappingSqlQuery {
        
        protected AuthoritiesByUsernameMapping(final DataSource ds) {
            super(ds, WebJdbcDaoImpl.this.authoritiesByUsernameQuery);
            declareParameter(new SqlParameter("userAccountName", (Types.VARCHAR)));
            compile();
        }
        
        protected final Object mapRow(final ResultSet rs, final int rownum) throws SQLException {
            final String roleName = WebJdbcDaoImpl.this.rolePrefix + rs.getString(2);
            final GrantedAuthority authority = new SimpleGrantedAuthority(roleName);
            return authority;
        }
        
    }
    
    protected class UsersByUsernameMapping extends MappingSqlQuery {
        
        protected UsersByUsernameMapping(final DataSource ds) {
            super(ds, WebJdbcDaoImpl.this.usersByUsernameQuery);
            declareParameter(new SqlParameter("userAccountName", (Types.VARCHAR)));
            compile();
        }
        
        protected final Object mapRow(final ResultSet rs, final int rownum) throws SQLException {
            final int userAccountId = rs.getInt(1);
            final int customerId = rs.getInt(2);
            final String username = rs.getString(3);
            final String password = rs.getString(4);
            final String passwordSalt = rs.getString(5);
            final String accountStatus = rs.getString(6);
            final String customerStatus = rs.getString(7);
            final Date trialExpiry = rs.getTimestamp(8);
            final String parentCustomerStatus = rs.getString(9);
            final Date parentCustomerTrialExpiry = rs.getTimestamp(10);
            
            boolean isEnabled = false;
            
            // check Parent customer status. (if parentCustomerStatus is null, then the user is a parent customer.)
            if (parentCustomerStatus != null) {
                if (parentCustomerStatus.equals("Enabled")) {
                    isEnabled = true;
                } else if (parentCustomerStatus.equals("Disabled")) {
                    isEnabled = false;
                } else if (parentCustomerStatus.equals("Trial")) {
                    if (parentCustomerTrialExpiry != null && parentCustomerTrialExpiry.after(new Date())) {
                        isEnabled = true;
                    } else {
                        isEnabled = false;
                    }
                } else {
                    isEnabled = false;
                }
            } else {
                isEnabled = true;
            }
            
            // check the Customer first
            if (isEnabled) {
                if (customerStatus.equals("Enabled")) {
                    isEnabled = true;
                } else if (customerStatus.equals("Disabled")) {
                    isEnabled = false;
                } else if (customerStatus.equals("Trial")) {
                    if (trialExpiry != null && trialExpiry.after(new Date())) {
                        isEnabled = true;
                    } else {
                        isEnabled = false;
                    }
                } else {
                    isEnabled = false;
                }
            }
            
            if (isEnabled) {
                if (accountStatus.equals("Enabled")) {
                    isEnabled = true;
                } else if (accountStatus.equals("Disabled")) {
                    isEnabled = false;
                } else {
                    isEnabled = false;
                }
            }
            
            final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("HOLDER"));
            final WebUser user = new WebUser(customerId, userAccountId, username, password, passwordSalt, isEnabled, authorities);
            
            return user;
        }
        
    }
    
}
