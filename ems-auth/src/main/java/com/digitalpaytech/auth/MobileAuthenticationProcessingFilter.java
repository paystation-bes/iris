package com.digitalpaytech.auth;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * A subclass of Spring's UsernamePasswordAuthenticationFilter. While it is possible to set
 * success handler, they are not used by this bean. Success behaviour is to call chain.doFilter
 * and allow the AJAX call to propagate to the end resource. This bean will use the failureHandler
 * assigned to it to determine the authentication failure behaviour.
 * 
 * @author danielm
 * 
 */
public class MobileAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {
    private static final String ERROR_MESSAGE = "{\"error\": \"\"}";
    private static final String SPRING_SECURITY_FORM_USERNAME_KEY = "j_username";
    private static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "j_password";
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        this.activityLoginService = activityLoginService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    @Override
    public final Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        String username = request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
        final String password = request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);
        
        if (StringUtils.isNotBlank(username)) {
            username = username.trim();
        }
        
        if (StringUtils.isBlank(username)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_REQUIRED);
        }
        
        if (!username.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID_CHARACTER);
        }
        
        if (username.length() > WebSecurityConstants.USERNAME_MAX_LENGTH || username.length() < WebSecurityConstants.USERNAME_MIN_LENGTH) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID);
        }
        
        if (StringUtils.isBlank(password)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_PASSWORD_REQUIRED);
        }
        String encodedUsername = null;
        try {
            encodedUsername = URLEncoder.encode(username, WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            this.logger.error("AuthenticationServiceException", e);
        }
        final UserAccount account = this.userAccountService.findUndeletedUserAccount(encodedUsername);
        if (account == null) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_BAD_CREDENTIALS);
        }
        if (account.isIsPasswordTemporary()) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_BAD_CREDENTIALS);
        }
        
        validateUserLoginLock(encodedUsername);
        
        final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(encodedUsername, password);
        setDetails(request, authRequest);
        
        Authentication authentication = null;
        try {
            authentication = getAuthenticationManager().authenticate(authRequest);
            
        } catch (AuthenticationServiceException e) {
            this.logger.info("AuthenticationServiceException", e);
            throw e;
        } catch (AuthenticationException e) {
            throw e;
        }
        
        return authentication;
    }
    
    @Override
    public final void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {
        //TODO improve and test this code - currently focused only on the redirect - should test credential lock etc
        final HttpServletRequest request = new DPTHttpServletRequestWrapper((HttpServletRequest) req);
        final HttpServletResponse response = (HttpServletResponse) res;
        if (req.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY) == null || req.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY) == null) {
            chain.doFilter(request, response);
            return;
        }
        Authentication authentication = null;
        try {
            authentication = attemptAuthentication(request, response);
        } catch (AuthenticationException e) {
            unsuccessfulAuthentication(request, response, e);
            this.logger.error("Unsuccessful authentication with reason: " + e.getMessage());
            return;
        }
        
        if (authentication == null) {
            // indicates that the authentication is still in process
            // TODO determine best practice - should this be logged?
            response.getWriter().write(ERROR_MESSAGE);
        } else {
            chain.doFilter(request, response);
        }
    }
    
    @Override
    protected final void unsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
        final AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }
    
    private void validateUserLoginLock(final String username) {
        // retrieve properties file.
        final String maxLoginAttempt = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT,
            EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT, false);
        
        final int maxLoginAttemptInt = Integer.parseInt(maxLoginAttempt);
        
        final String maxLoginLockMinutes = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
            EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false);
        
        final int maxLoginLockMinutesInt = Integer.parseInt(maxLoginLockMinutes);
        
        final UserAccount userAccount = this.userAccountService.findUndeletedUserAccount(username);
        
        /*
         * Temporarily locking a user's account. This is when user has never successfully logged-in in the last "maxLoginLockMinutes"
         * and attempted to login more then the "maxLoginAttemptInt".
         */
        final int count = this.activityLoginService.findCountInvalidActivityLoginByActivityGMT(userAccount.getId(),
            WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL, maxLoginLockMinutesInt);
        
        if (count > maxLoginAttemptInt) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
        }
        
        // retrieve activitylogin data.
        final List<ActivityLogin> lists = this.activityLoginService.findInvalidActivityLoginByLastModifiedGMT(username, maxLoginLockMinutesInt);
        
        if (userAccount != null) {
            for (ActivityLogin activityLogin : lists) {
                if (activityLogin.getLoginResultType().getId() == WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL_LOCKOUT) {
                    throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
                }
            }
            
            if (lists.size() == Integer.parseInt(maxLoginAttempt)) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_CREDENTIAL_LOCKOUT);
            }
            
            if (lists.size() > Integer.parseInt(maxLoginAttempt)) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
            }
        }
    }
}
