package com.digitalpaytech.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.digitalpaytech.auth.dto.TokenUser;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LinkUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {
    @Autowired
    private ObjectMapper objectMapper;
    
    @Override
    public UserDetails loadUserDetails(final PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        final OAuth2AccessToken accessToken = (OAuth2AccessToken) token.getCredentials();
        
        final TokenUser tokenUser = new TokenUser(accessToken, this.objectMapper);
        return tokenUser;
    }
}
