package com.digitalpaytech.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

public class WebAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private RequestCache requestCache = new HttpSessionRequestCache();
    
    @Override
    public final void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
        final Authentication authentication) throws ServletException, IOException {
        final SavedRequest savedRequest = this.requestCache.getRequest(request, response);
        
        if (savedRequest == null) {
            final String targetUrl = getTargetUrl(request, determineTargetUrl(request, response));
            clearAuthenticationAttributes(request);
            getRedirectStrategy().sendRedirect(request, response, targetUrl);
            return;
        }
        
        final String targetUrlParameter = getTargetUrlParameter();
        if (isAlwaysUseDefaultTargetUrl() || (targetUrlParameter != null && StringUtils.hasText(request.getParameter(targetUrlParameter)))) {
            super.onAuthenticationSuccess(request, response, authentication);
            
            return;
        }
        
        clearAuthenticationAttributes(request);
        
        // Use the DefaultSavedRequest URL
        String targetUrl = savedRequest.getRedirectUrl();
        targetUrl = getTargetUrl(request, targetUrl);
        
        logger.debug("Redirecting to DefaultSavedRequest Url: " + targetUrl);
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
    }
    
    public final void setRequestCache(final RequestCache requestCache) {
        this.requestCache = requestCache;
    }
    
    private String getTargetUrl(final HttpServletRequest request, final String targetUrl) {
        //		StringBuffer buffer = new StringBuffer(targetUrl);
        //TODO Remove SessionToken
        //		if (targetUrl != null) {
        //			if (targetUrl.indexOf('?') == -1) {
        //				buffer.append("?");
        //				targetUrl = targetUrl + "?";
        //			} else {
        //				buffer.append("&");
        //				targetUrl = targetUrl + "&";
        //			}
        //			targetUrl = buffer.append(WebSecurityConstants.SESSION_TOKEN + "=")
        //					.append(request.getSession().getAttribute(WebSecurityConstants.SESSION_TOKEN))
        //					.toString();
        //		}
        
        return targetUrl;
    }
}
