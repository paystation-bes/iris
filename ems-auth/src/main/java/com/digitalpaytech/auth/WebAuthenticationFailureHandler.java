package com.digitalpaytech.auth;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.util.StringUtils;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

public class WebAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {
    
    protected final Log logger = LogFactory.getLog(getClass());
    
    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "j_username";
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "j_password";
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        this.activityLoginService = activityLoginService;
    }
    
    @Override
    public final void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
        final AuthenticationException exception) throws IOException, ServletException {
        
        final String username = request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
        
        if (StringUtils.hasText(username)) {
            String encodedUsername = null;
            try {
                encodedUsername = URLEncoder.encode(username, WebSecurityConstants.URL_ENCODING_LATIN1);
            } catch (UnsupportedEncodingException e) {
                this.logger.error("AuthenticationServiceException", e);
            }
            final UserAccount userAccount = this.userAccountService.findUndeletedUserAccount(encodedUsername);
            if (userAccount != null) {
                
                if (exception.getMessage().equals(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_PASSWORD_INVALID)) {
                    saveActivityLog(userAccount, WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL);
                } else if (exception.getMessage().equals(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_BAD_CREDENTIALS)) {
                    saveActivityLog(userAccount, WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL);
                } else if (exception.getMessage().equals(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_CREDENTIAL_LOCKOUT)) {
                    saveActivityLog(userAccount, WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL_LOCKOUT);
                } else if (exception.getMessage().equals(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED)) {
                    saveActivityLog(userAccount, WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL_LOCKOUT);
                }
            }
        }
        
        super.onAuthenticationFailure(request, response, exception);
    }
    
    private void saveActivityLog(final UserAccount userAccount, final int status) {
        
        final ActivityLogin activityLogin = new ActivityLogin();
        final LoginResultType loginResultType = new LoginResultType();
        
        loginResultType.setId(status);
        userAccount.setId(userAccount.getId());
        activityLogin.setLoginResultType(loginResultType);
        activityLogin.setUserAccount(userAccount);
        activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());
        
        this.activityLoginService.saveActivityLogin(activityLogin);
    }
    
    public final void setFailureUrlMap(final Map<?, ?> failureUrlMap) {
        super.setExceptionMappings(failureUrlMap);
    }
    
}
