package com.digitalpaytech.auth;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.CustomerUserInfo;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * WebAuthenticationProcessingFilter
 * 
 * @author Brian Kim
 */
public class WebAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private CustomerMigrationStatusTypeService customerMigrationStatusTypeService;
    
    private String loginPage;
    
    private boolean pda;
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        this.activityLoginService = activityLoginService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setCustomerMigrationStatusTypeService(final CustomerMigrationStatusTypeService customerMigrationStatusTypeService) {
        this.customerMigrationStatusTypeService = customerMigrationStatusTypeService;
    }
    
    @SuppressWarnings("checkstyle:designForExtension")
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        
        HttpSession session = request.getSession(false);
        
        if (session != null) {
            session.invalidate();
        }
        session = request.getSession();
        if (session != null && this.loginPage != null) {
            session.setAttribute(WebCoreConstants.SESSIONKEY_LOGIN_PAGE, this.loginPage);
        }
        
        /* get the user name and password */
        final String username = obtainUsername(request);
        final String password = obtainPassword(request);
        
        validatePassword(password);
        final String encodedUsername = validateUsername(username);
        
        final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(encodedUsername, password);
        setDetails(request, authRequest);
        
        Authentication authentication = null;
        
        try {
            authentication = getAuthenticationManager().authenticate(authRequest);
            
        } catch (AuthenticationServiceException e) {
            logger.info("AuthenticationServiceException", e);
            throw e;
        } catch (AuthenticationException e) {
            throw e;
        }
        
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        if (password.matches(WebSecurityConstants.COMPLEX_PASSWORD_EXP)) {
            webUser.setIsComplexPassword(true);
        }
        
        populateWebUser(webUser, request);
        
        if (webUser.getSwitchToAliasUserAccount() != null) {
            WebSecurityUtil.changeUserOnLogin(webUser);
        }
        
        return authentication;
    }
    
    protected String validateUsername(final String username) {
        
        String encodedUsername = null;
        if (StringUtils.isNotBlank(username)) {
            encodedUsername = username.trim();
        }
        
        if (StringUtils.isBlank(encodedUsername)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_REQUIRED);
        }
        
        if (!encodedUsername.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID_CHARACTER);
        }
        
        if (encodedUsername.length() > WebSecurityConstants.USERNAME_MAX_LENGTH
            || encodedUsername.length() < WebSecurityConstants.USERNAME_MIN_LENGTH) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_INVALID);
        }
        
        /* check out the user lock. */
        
        try {
            encodedUsername = URLEncoder.encode(encodedUsername, WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            logger.error(AuthenticationServiceException.class.getName(), e);
        }
        
        validateUserLoginLock(encodedUsername);
        return encodedUsername;
    }
    
    protected void validatePassword(final String password) {
        if (StringUtils.isBlank(password)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_PASSWORD_REQUIRED);
        }
        
        if (password.length() < WebSecurityConstants.USER_PASSWORD_MIN_LENGTH || password.length() > WebSecurityConstants.USER_PASSWORD_MAX_LENGTH) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_PASSWORD_INVALID);
        }
        
    }
    
    protected void populateWebUser(final WebUser webUser, final HttpServletRequest request) {
        final UserAccount account = this.userAccountService.findUserAccountForLogin(webUser.getUserAccountId());
        
        populateUserAccountValues(webUser, account, request);
        populatePermissionsSubscriptions(webUser, account);
        
        /*
         * set customer time zone. (default time zone: PST if customer doean't have one.)
         */
        String customerTimeZone = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE,
                                                                             EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE);
        final Set<CustomerProperty> customerProperties = account.getCustomer().getCustomerProperties();
        for (CustomerProperty property : customerProperties) {
            if (property.getCustomerPropertyType().getId() == 1) {
                customerTimeZone = property.getPropertyValue();
                break;
            }
        }
        webUser.setCustomerTimeZone(customerTimeZone);
        webUser.setIsMetric(!(customerTimeZone != null && customerTimeZone.startsWith("US/")));
        
        final String sessionTimeout = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE,
                                                                                 Integer.toString(EmsPropertiesService.MAX_DEFAULT_SESSION_TIMEOUT));
        
        if (sessionTimeout == null) {
            webUser.setSessionTimeout(EmsPropertiesService.MAX_DEFAULT_SESSION_TIMEOUT);
        } else {
            webUser.setSessionTimeout(Integer.parseInt(sessionTimeout));
        }
        
        webUser.setLoginViaPDA(this.pda);
        
    }
    
    private void populatePermissionsSubscriptions(final WebUser webUser, final UserAccount userAccount) {
        /* set user permission. */
        final Set<Integer> permissionIds = new HashSet<Integer>();
        final List<RolePermission> rolePermissions = this.userAccountService.findUserRoleAndPermissionForCustomerType(userAccount.getId(),
                                                                                                                      userAccount.getCustomer()
                                                                                                                              .getCustomerType()
                                                                                                                              .getId());
        for (RolePermission rp : rolePermissions) {
            if (!webUser.isSystemAdmin() && rp.getRole().getCustomerType().getId() == WebSecurityConstants.CUSTOMER_TYPE_ID_SYSTEM_ADMIN) {
                webUser.setIsSystemAdmin(true);
            }
            
            if (rp.getPermission().getPermission() != null) {
                permissionIds.add(rp.getPermission().getPermission().getId());
            }
            if (rp.getRole().getCustomerType().getId() == userAccount.getCustomer().getCustomerType().getId()) {
                permissionIds.add(rp.getPermission().getId());
            }
        }
        
        webUser.setUserPermissions(permissionIds);
        
        /* set customer subscription. */
        final Set<CustomerSubscription> subscriptions = userAccount.getCustomer().getCustomerSubscriptions();
        final Collection<Integer> customerSubscriptions = new ArrayList<Integer>();
        for (CustomerSubscription subscription : subscriptions) {
            if (subscription.isIsEnabled()) {
                customerSubscriptions.add(subscription.getSubscriptionType().getId());
            }
        }
        webUser.setCustomerSubscriptions(customerSubscriptions);
        
    }
    
    private void populateUserAccountValues(final WebUser webUser, final UserAccount userAccount, final HttpServletRequest request) {
        // Is Customer Migrated?
        webUser.setIsMigrated(userAccount.getCustomer().isIsMigrated());
        if (!webUser.getIsMigrated()) {
            final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(userAccount.getCustomer()
                    .getId());
            //First time the customer logs in, change the status to Boarded
            if (customerMigration.getCustomerMigrationStatusType().getId() == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED) {
                customerMigration.setCustomerMigrationStatusType(this.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED));
                customerMigration.setCustomerBoardedGmt(DateUtil.getCurrentGmtDate());
                customerMigration.setBackGroundJobNextTryGmt(DateUtil.getCurrentGmtDate());
                customerMigration.setLastModifiedByUserId(userAccount.getId());
                customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                this.customerMigrationService.updateCustomerMigration(customerMigration);
            }
        }
        
        webUser.setIsParent(userAccount.getCustomer().isIsParent());
        // Added for temporary password check filter
        webUser.setIsTemporaryPassword(userAccount.isIsPasswordTemporary());
        webUser.setCustomerName(userAccount.getCustomer().getName());
        
        try {
            webUser.setDecodedUserFirstName(URLDecoder.decode(userAccount.getFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            webUser.setDecodedUserLastName(URLDecoder.decode(userAccount.getLastName(), WebSecurityConstants.URL_ENCODING_LATIN1));
        } catch (UnsupportedEncodingException e) {
            logger.error("+++ Exception on first name, last name decoding. +++");
        }
        
        if (webUser.getIsParent()) {
            final List<UserAccount> aliasAccounts = this.userAccountService.findChildUserAccountsByParentUserId(userAccount.getId());
            if (aliasAccounts != null && !aliasAccounts.isEmpty()) {
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final List<CustomerUserInfo> aliasUserList = new ArrayList<CustomerUserInfo>();
                final List<UserRole> userRoleList = this.userAccountService.findActiveUserRoleByUserAccountId(userAccount.getId());
                CustomerUserInfo parentUser = null;
                if (hasParentRole(userRoleList)) {
                    parentUser = new CustomerUserInfo(userAccount, keyMapping);
                    aliasUserList.add(parentUser);
                }
                
                for (UserAccount childAccount : aliasAccounts) {
                    final CustomerUserInfo childUser = new CustomerUserInfo(childAccount, keyMapping);
                    aliasUserList.add(childUser);
                    if (childAccount.getIsDefaultAlias()) {
                        webUser.setSwitchToAliasUserAccount(childAccount.getId());
                    }
                }                
                
                Collections.sort(aliasUserList, new CustomerUserInfo.CustomerNameComparator());                
                
                if (webUser.getSwitchToAliasUserAccount() == null) {
                    if (parentUser == null) {
                        webUser.setSwitchToAliasUserAccount(aliasAccounts.get(0).getId());
                        
                    } else {
                        webUser.setSwitchToAliasUserAccount(parentUser.getUserAccountId());
                    }
                }
                
                webUser.setAliasUserList(aliasUserList);
                webUser.setIsAliasUser(true);
            }
        }
    }
    
    private void validateUserLoginLock(final String username) {
        // retrieve properties file.
        final String maxLoginAttempt = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_ATTEMPT,
                                                                                  EmsPropertiesService.DEFAULT_MAX_LOGIN_ATTEMPT, false);
        
        final int maxLoginAttemptInt = Integer.parseInt(maxLoginAttempt);
        
        final String maxLoginLockMinutes = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_LOGIN_LOCKUP_MINUTES,
                                                                                      EmsPropertiesService.DEFAULT_MAX_LOGIN_LOCKUP_MINUTES, false);
        
        final int maxLoginLockMinutesInt = Integer.parseInt(maxLoginLockMinutes);
        
        final UserAccount userAccount = this.userAccountService.findUndeletedUserAccount(username);
        
        if (userAccount != null) {
            /*
             * Temporarily locking a user's account. This is when user has never successfully logged-in in the last "maxLoginLockMinutes"
             * and attempted to login more then the "maxLoginAttemptInt".
             */
            final int count = this.activityLoginService
                    .findCountInvalidActivityLoginByActivityGMT(userAccount.getId(), WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL,
                                                                maxLoginLockMinutesInt);
            if (count >= maxLoginAttemptInt) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
            }
        }
        
        // retrieve activitylogin data.
        final List<ActivityLogin> lists = this.activityLoginService.findInvalidActivityLoginByLastModifiedGMT(username, maxLoginLockMinutesInt);
        
        if (userAccount != null) {
            for (ActivityLogin activityLogin : lists) {
                if (activityLogin.getLoginResultType().getId() == WebSecurityConstants.LOGIN_RESULT_TYPE_INVALID_CREDENTIAL_LOCKOUT) {
                    throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
                }
            }
            
            if (lists.size() == maxLoginAttemptInt) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_CREDENTIAL_LOCKOUT);
            }
            
            if (lists.size() > maxLoginAttemptInt) {
                throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_LOCKED);
            }
        }
    }
    
    public final String getLoginPage() {
        return this.loginPage;
    }
    
    public final void setLoginPage(final String loginPage) {
        this.loginPage = loginPage;
    }
    
    public final void setPda(final boolean pda) {
        this.pda = pda;
    }
    
    private boolean hasParentRole(final List<UserRole> userRoles) {
        boolean hasParentRole = false;
        if (userRoles != null) {
            for (UserRole ur : userRoles) {
                if (ur.getRole().getCustomerType() != null && ur.getRole().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT) {
                    hasParentRole = true;
                }
            }
        }
        return hasParentRole;
    }
}
