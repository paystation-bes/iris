package com.digitalpaytech.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.TLSUtils;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@SuppressWarnings("checkstyle:designForExtension")
public class OAuthProcessingFilter extends WebAuthenticationProcessingFilter {
    @Autowired
    private ResourceServerTokenServices tokenService;
    
    @Autowired
    private UserAccountService accountService;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        final String token = request.getParameter("token");
        if (StringUtils.isBlank(token)) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_REQUIRED);
        }
        
        try {
            if (this.tlsUtils.shouldRedirect(request)) {
                this.tlsUtils.redirect(response, request.getServletPath() + "?token=" + token);
                return null;
            }
            
        } catch (IOException e) {
            throw new BadCredentialsException(WebSecurityConstants.LOGIN_FAILURE_MESSAGE_USER_NAME_REQUIRED);
        }
        
        final OAuth2AccessToken accessToken = this.tokenService.readAccessToken(token);
        final String linkUserName = (String) accessToken.getAdditionalInformation().get("sub");
        
        final SecurityContext context = SecurityContextHolder.getContext();
        
        // If authentication is not null, it means that user still have active session with Iris.
        Authentication authentication = (context == null) ? null : context.getAuthentication();
        
        final Object principal = (authentication == null) ? null : authentication.getPrincipal();
        
        if (principal == null) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            session = request.getSession();
            
            final UserAccount account = this.accountService.findUserAccountByLinkUserName(linkUserName);
            if (account == null) {
                final PreAuthenticatedAuthenticationToken authToken = new PreAuthenticatedAuthenticationToken(linkUserName, accessToken,
                        AuthorityUtils.createAuthorityList("ROLE_PRE_AUTHENTICATED"));
                authentication = getAuthenticationManager().authenticate(authToken);
            } else {
                final UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(account.getUserName(), accessToken);
                setDetails(request, authToken);
                
                authentication = getAuthenticationManager().authenticate(authToken);
                
                final WebUser webUser = (WebUser) authentication.getPrincipal();
                webUser.setIsComplexPassword(true);
                
                populateWebUser(webUser, request);
                
                webUser.setLinkUserName(linkUserName);
                webUser.setLoginViaPortal(true);
                
                if (webUser.getSwitchToAliasUserAccount() != null) {
                    WebSecurityUtil.changeUserOnLogin(webUser);
                }
            }
        } else if (principal instanceof WebUser) {
            final WebUser webUser = (WebUser) principal;
            if (webUser.getSwitchToAliasUserAccount() != null) {
                WebSecurityUtil.changeUserOnLogin(webUser);
            }
        }
        
        return authentication;
        
    }
    
    public void setTokenService(final ResourceServerTokenServices tokenService) {
        this.tokenService = tokenService;
    }
    
    public ResourceServerTokenServices getTokenService() {
        return this.tokenService;
    }
}
