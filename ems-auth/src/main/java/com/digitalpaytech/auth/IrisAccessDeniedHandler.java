package com.digitalpaytech.auth;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.access.AccessDeniedHandler;

public class IrisAccessDeniedHandler implements AccessDeniedHandler {
    private String errorPage;
    
    @Override
    public final void handle(final HttpServletRequest request, final HttpServletResponse response, final AccessDeniedException accessDeniedException)
        throws IOException, ServletException {
        if (!response.isCommitted()) {
            if (this.errorPage != null) {
                // Put exception into request scope (perhaps of use to a view)
                request.setAttribute(WebAttributes.ACCESS_DENIED_403, accessDeniedException);
                
                // Set the 403 status code.
                response.setStatus(HttpServletResponse.SC_OK);
                
                // forward to error page.
                final RequestDispatcher dispatcher = request.getRequestDispatcher(this.errorPage);
                dispatcher.forward(request, response);
            } else {
                response.sendError(HttpServletResponse.SC_OK, accessDeniedException.getMessage());
            }
        }
    }
    
    public final void setErrorPage(final String errorPage) {
        if ((errorPage != null) && !errorPage.startsWith("/")) {
            throw new IllegalArgumentException("errorPage must begin with '/'");
        }
        
        this.errorPage = errorPage;
    }
}
