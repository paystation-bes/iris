package com.digitalpaytech.auth;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;

import com.digitalpaytech.auth.dto.TokenUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * ApplicationSecurityListener
 * 
 * @author Brian Kim
 */
public class ApplicationSecurityListener implements ApplicationListener {
    
    private static final Logger LOGGER = Logger.getLogger(ApplicationSecurityListener.class);
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        this.activityLoginService = activityLoginService;
    }
    
    @Override
    public final void onApplicationEvent(final ApplicationEvent event) {
        
        if (event instanceof AuthenticationSuccessEvent) {
            final AuthenticationSuccessEvent authenticationSuccessEvent = (AuthenticationSuccessEvent) event;
            saveLogginAuthenticationSuccessEvent(authenticationSuccessEvent);
        }
    }
    
    public final void saveLogginAuthenticationSuccessEvent(final AuthenticationSuccessEvent authenticationSuccessEvent) {
        
        final Object o = authenticationSuccessEvent.getAuthentication().getPrincipal();
        if (o instanceof WebUser) {
            final WebUser webUser = (WebUser) o;
            
            // save successful login.
            saveActivityLog(webUser);
            
            LOGGER.info("Successful Login for CustomerId: " + webUser.getCustomerId() + ", User: " + webUser.getUsername() + ", role: "
                        + webUser.getAuthorities().toString());
            
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Event: " + authenticationSuccessEvent.toString());
            }
        } else if (o instanceof TokenUser) {
            LOGGER.info("Successful PreAuthenticated for " + o);
        } else {
            throw new IllegalStateException("Unsupported Principal Type: " + o.getClass());
        }
    }
    
    private void saveActivityLog(final WebUser webUser) {
        
        final ActivityLogin activityLogin = new ActivityLogin();
        final UserAccount userAccount = new UserAccount();
        final LoginResultType loginResultType = new LoginResultType();
        
        loginResultType.setId(WebSecurityConstants.LOGIN_RESULT_TYPE_LOGIN_SUCCESS);
        userAccount.setId(webUser.getUserAccountId());
        activityLogin.setLoginResultType(loginResultType);
        activityLogin.setUserAccount(userAccount);
        activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());
        
        this.activityLoginService.saveActivityLogin(activityLogin);
    }
}
