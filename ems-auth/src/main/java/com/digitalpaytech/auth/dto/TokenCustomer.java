package com.digitalpaytech.auth.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenCustomer implements Serializable {
    private static final long serialVersionUID = -5497869541536490452L;
    
    private Integer customerId;
    private boolean isDefault;

    @JsonCreator
    public TokenCustomer(
            @JsonProperty("customer_Id") final Integer customerId,
            @JsonProperty("isDefault") final boolean isDefault) {
        if (customerId == null) {
            throw new IllegalArgumentException("CustomerId must be specified");
        }

        this.customerId = customerId;
        this.isDefault = isDefault;
    }

    @JsonProperty("customer_Id")
    public Integer getCustomerId() {
        return this.customerId;
    }

    @JsonProperty("isDefault")
    public boolean isDefault() {
        return this.isDefault;
    }
}
