package com.digitalpaytech.auth.dto;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toSet;

import java.io.IOException;

public class TokenUser implements UserDetails {
    public enum AuthType {
        MICROSERVICE("microservice"),
        USER("user"),
        DEVICE("device"),
        SCHEDULER("scheduler");

        private final String scope;

        AuthType(final String scope) {
            assert StringUtils.hasText(scope);

            this.scope = scope;
        }

        public static Stream<AuthType> stream(final String name) {
            AuthType t = null;
            if (name != null) {
                try {
                    t = valueOf(AuthType.class, name.toUpperCase());
                } catch (NullPointerException | IllegalArgumentException e) {
                    // DO NOTHING.
                }
            }

            return Optional.ofNullable(t).map(Stream::of).orElse(Stream.empty());
        }

        @Override
        public String toString() {
            return this.scope;
        }
    }
    
    public static final String SESSION_KEY = "tokenUser";
    public static final String ROLE_PRE_LOGIN = "ROLE_PRE_LOGIN";
    
    public static final String ATTR_USER_NAME = "sub";
    public static final String ATTR_USER_ID = "user_id";
    public static final String ATTR_SCOPE = "scope";
    public static final String ATTR_CUSTOMERS = "customers";
    
    private static final TypeReference<HashSet<TokenCustomer>> TYPEREF_CUSTOMERS = new TypeReference<HashSet<TokenCustomer>>() { };
    private static final List<? extends GrantedAuthority> DEFAULT_ROLE;
    static {
        DEFAULT_ROLE = Collections.unmodifiableList(AuthorityUtils.createAuthorityList(ROLE_PRE_LOGIN));
    }
    
    private Integer userId;
    private String userName;
    
    private boolean isMicroService;
    private boolean isUser;
    
    private Set<AuthType> authTypes;

    private TokenCustomer customer;
    
    private String accessToken;
    private String signature;
    
    public TokenUser(final OAuth2AccessToken token, final ObjectMapper objectMapper) {
        this.authTypes = token.getScope().stream().flatMap(AuthType::stream).collect(toSet());
        
        resolveAuthTypes(token.getAdditionalInformation());
        resolveUser(token.getAdditionalInformation());
        resolveCustomers(token.getAdditionalInformation(), objectMapper);
    }
    
    private void resolveAuthTypes(final Map<String, ?> details) {
        this.isMicroService = this.authTypes.contains(AuthType.MICROSERVICE);
        this.isUser = this.authTypes.contains(AuthType.USER);
    }
    
    private void resolveUser(final Map<String, ?> details) {
        this.userName = (String) details.get(ATTR_USER_NAME);
        if (this.userName == null) {
            throw new BadCredentialsException("Token doesn't contains userName");
        }

        final Number userIdBuffer = (Number) details.get(ATTR_USER_ID);
        if (userIdBuffer == null) {
            throw new BadCredentialsException("Token doesn't contains userId");
        } else if (userIdBuffer instanceof Integer) {
            this.userId = (Integer) userIdBuffer;
        } else {
            this.userId = userIdBuffer.intValue();
        }
    }
    
    private void resolveCustomers(final Map<String, ?> details, final ObjectMapper objectMapper) {
        final String customersListStr = (String) details.get(ATTR_CUSTOMERS);
        if (StringUtils.hasText(customersListStr)) {
            try {
                final Set<TokenCustomer> tokenCustomersSet = objectMapper.readValue(customersListStr, TYPEREF_CUSTOMERS);
                final Map<Boolean, Set<TokenCustomer>> allCustomers = tokenCustomersSet.stream()
                        .collect(partitioningBy(TokenCustomer::isDefault, toSet()));

                final Set<TokenCustomer> defaultCustomers = allCustomers.get(true);
                if (defaultCustomers.size() == 1) {
                    this.customer = defaultCustomers.iterator().next();
                } else if (defaultCustomers.size() > 1) {
                    throw new BadCredentialsException("Expecting single default customer but found: " + defaultCustomers.size());
                }
            } catch (IOException ioe) {
                throw new BadCredentialsException("Failed to parse Customers' JSON", ioe);
            }
        }

        if (this.customer == null) {
            throw new BadCredentialsException("Token doesn't contains default customer");
        }
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return DEFAULT_ROLE;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "TokenUser{" + "userName='" + this.userName + '\''
               + ", userId=" + this.userId
               + ", authTypes=" + this.authTypes
               + ", signature='" + this.signature + '\'' + '}';
    }

    public Integer getUserId() {
        return this.userId;
    }
    
    public Set<AuthType> getAuthTypes() {
        return this.authTypes;
    }

    public TokenCustomer getCustomer() {
        return this.customer;
    }

    public boolean isMicroService() {
        return this.isMicroService;
    }

    public boolean isUser() {
        return this.isUser;
    }

    public String getRawToken() {
        return this.accessToken;
    }

    public String getSignature() {
        return this.signature;
    }
}
