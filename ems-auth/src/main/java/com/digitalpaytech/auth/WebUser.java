package com.digitalpaytech.auth;

import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.digitalpaytech.dto.CustomerUserInfo;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * WebUser
 * 
 * @author Brian Kim
 */
public class WebUser extends User {
    
    private static final long serialVersionUID = 8808684475700514306L;
    private Integer customerId;
    private Integer userAccountId;
    private boolean isComplexPassword;
    private String saltSource;
    private String decodedUserFirstName;
    private String decodedUserLastName;
    private int sessionTimeout;
    private String customerTimeZone;
    private int customerTimeZoneOffset;
    private Collection<Integer> userPermissions;
    private Collection<Integer> customerSubscriptions;
    private Integer numberOfUnreadReports;
    private Integer mobileAppCount;
    private boolean isSystemAdmin;
    private boolean loginViaPDA;
    private boolean isMigrated;
    private boolean isMetric = true;
    private boolean isParent;
    private boolean isAliasUser;
    private String customerName;
    private Integer switchToAliasUserAccount;
    
    private boolean isAgreementSigned;
    private boolean isTemporaryPassword = true;
    
    private List<CustomerUserInfo> aliasUserList;
    
    private String linkUserName;
    private boolean loginViaPortal;
    
    public WebUser(final int customerid, final int userAccountid, final String userName, final String password, final String saltSource,
            final boolean isEnabled, final Collection<GrantedAuthority> authorities) {
        super(userName, password, isEnabled, true, true, true, authorities);
        this.customerId = customerid;
        this.userAccountId = userAccountid;
        this.saltSource = saltSource;
        this.isSystemAdmin = false;
        this.isMetric = true;
    }
    
    public void load(final WebUser other) {
        if (this.customerId != other.customerId) {
            throw new BadCredentialsException("Missmatched Customer: " + this.customerId);
        }
        
        this.userAccountId = other.userAccountId;
        this.isComplexPassword = other.isComplexPassword;
        this.saltSource = other.saltSource;
        this.decodedUserFirstName = other.decodedUserFirstName;
        this.decodedUserLastName = other.decodedUserLastName;
        this.sessionTimeout = other.sessionTimeout;
        this.customerTimeZone = other.customerTimeZone;
        this.customerTimeZoneOffset = other.customerTimeZoneOffset;
        this.userPermissions = other.userPermissions;
        this.customerSubscriptions = other.customerSubscriptions;
        this.numberOfUnreadReports = other.numberOfUnreadReports;
        this.mobileAppCount = other.mobileAppCount;
        this.isSystemAdmin = other.isSystemAdmin;
        this.loginViaPDA = other.loginViaPDA;
        this.isMigrated = other.isMigrated;
        this.isMetric = other.isMetric;
        this.isParent = other.isParent;
        this.isAliasUser = other.isAliasUser;
        this.customerName = other.customerName;
        this.switchToAliasUserAccount = other.switchToAliasUserAccount;
        
        this.isAgreementSigned = other.isAgreementSigned;
        this.isTemporaryPassword = other.isTemporaryPassword;
        
        this.aliasUserList = other.aliasUserList;
        
        this.linkUserName = other.linkUserName;
    }
    
    public final Integer getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final Integer customerid) {
        this.customerId = customerid;
    }
    
    public final Integer getUserAccountId() {
        return this.userAccountId;
    }
    
    public final void setUserAccountId(final Integer userAccountid) {
        this.userAccountId = userAccountid;
    }
    
    public final boolean isComplexPassword() {
        return this.isComplexPassword;
    }
    
    public final void setIsComplexPassword(final boolean isComplexPassword) {
        this.isComplexPassword = isComplexPassword;
    }
    
    public final String getSaltSource() {
        return this.saltSource;
    }
    
    public final void setSaltSource(final String saltSource) {
        this.saltSource = saltSource;
    }
    
    public final String getDecodedUserFirstName() {
        return this.decodedUserFirstName;
    }
    
    public final void setDecodedUserFirstName(final String decodedUserFirstName) {
        this.decodedUserFirstName = decodedUserFirstName;
    }
    
    public final String getDecodedUserLastName() {
        return this.decodedUserLastName;
    }
    
    public final void setDecodedUserLastName(final String decodedUserLastName) {
        this.decodedUserLastName = decodedUserLastName;
    }
    
    public final int getSessionTimeout() {
        return this.sessionTimeout;
    }
    
    public final void setSessionTimeout(final int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }
    
    public final String getCustomerTimeZone() {
        return this.customerTimeZone;
    }
    
    public final void setCustomerTimeZone(final String customerTimeZone) {
        this.customerTimeZone = customerTimeZone;
        
        TimeZone timeZone = null;
        if (this.customerTimeZone != null) {
            timeZone = TimeZone.getTimeZone(this.customerTimeZone);
        }
        
        this.customerTimeZoneOffset =
                (timeZone == null) ? 0 : (int) (timeZone.getOffset(System.currentTimeMillis()) / StandardConstants.MINUTES_IN_MILLIS_1);
    }
    
    public final Collection<Integer> getUserPermissions() {
        return this.userPermissions;
    }
    
    public final void setUserPermissions(final Collection<Integer> userPermissions) {
        this.userPermissions = userPermissions;
    }
    
    public final Collection<Integer> getCustomerSubscriptions() {
        return this.customerSubscriptions;
    }
    
    public final void setCustomerSubscriptions(final Collection<Integer> customerSubscriptions) {
        this.customerSubscriptions = customerSubscriptions;
    }
    
    public final Integer getNumberOfUnreadReports() {
        return this.numberOfUnreadReports;
    }
    
    public final void setNumberOfUnreadReports(final Integer numberOfUnreadReports) {
        this.numberOfUnreadReports = numberOfUnreadReports;
    }
    
    public final boolean isSystemAdmin() {
        return this.isSystemAdmin;
    }
    
    public final void setIsSystemAdmin(final boolean isSystemAdmin) {
        this.isSystemAdmin = isSystemAdmin;
    }
    
    public final boolean isLoginViaPDA() {
        return this.loginViaPDA;
    }
    
    public final void setLoginViaPDA(final boolean loginViaPDA) {
        this.loginViaPDA = loginViaPDA;
    }
    
    public final boolean getIsMigrated() {
        return this.isMigrated;
    }
    
    public final void setIsMigrated(final boolean isMigrated) {
        this.isMigrated = isMigrated;
    }
    
    public final boolean getIsMetric() {
        return this.isMetric;
    }
    
    public final void setIsMetric(final boolean isMetric) {
        this.isMetric = isMetric;
    }
    
    public final boolean getIsParent() {
        return this.isParent;
    }
    
    public final void setIsParent(final boolean isParent) {
        this.isParent = isParent;
    }
    
    public final boolean getIsAliasUser() {
        return this.isAliasUser;
    }
    
    public final void setIsAliasUser(final boolean isAliasUser) {
        this.isAliasUser = isAliasUser;
    }
    
    public final boolean getIsAgreementSigned() {
        return this.isAgreementSigned;
    }
    
    public final void setIsAgreementSigned(final boolean isAgreementSigned) {
        this.isAgreementSigned = isAgreementSigned;
    }
    
    public final boolean getIsTemporaryPassword() {
        return this.isTemporaryPassword;
    }
    
    public final void setIsTemporaryPassword(final boolean isTemporaryPassword) {
        this.isTemporaryPassword = isTemporaryPassword;
    }
    
    public final int getCustomerTimeZoneOffset() {
        return this.customerTimeZoneOffset;
    }
    
    public final void setCustomerTimeZoneOffset(final int customerTimeZoneOffset) {
        this.customerTimeZoneOffset = customerTimeZoneOffset;
    }
    
    public final int getMobileAppCount() {
        return this.mobileAppCount == null ? 0 : this.mobileAppCount;
    }
    
    public final void setMobileAppCount(final Integer mobileAppCount) {
        this.mobileAppCount = mobileAppCount;
    }
    
    public final boolean hasAccess(final Integer permission) {
        return this.userPermissions.contains(permission);
    }
    
    public final boolean hasSubscription(final Integer subscriptionType) {
        return this.customerSubscriptions.contains(subscriptionType);
    }
    
    public final boolean hasPdaAccess() {
        return hasAccess(WebSecurityConstants.PERMISSION_PDA_ACCESS);
    }
    
    public final List<CustomerUserInfo> getAliasUserList() {
        return this.aliasUserList;
    }
    
    public final void setAliasUserList(final List<CustomerUserInfo> aliasUserList) {
        this.aliasUserList = aliasUserList;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final Integer getSwitchToAliasUserAccount() {
        return this.switchToAliasUserAccount;
    }
    
    public final void setSwitchToAliasUserAccount(final Integer switchToAliasUserAccount) {
        this.switchToAliasUserAccount = switchToAliasUserAccount;
    }
    
    public final String getLinkUserName() {
        return this.linkUserName;
    }
    
    public final void setLinkUserName(final String linkUserName) {
        this.linkUserName = linkUserName;
    }
    
    public final boolean getLoginViaPortal() {
        return this.loginViaPortal;
    }
    
    public final void setLoginViaPortal(final boolean loginViaPortal) {
        this.loginViaPortal = loginViaPortal;
    }
}
