package com.digitalpaytech.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

public class WebLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
	
	@Autowired
	private ActivityLoginService activityLoginService;
	
	private String successURL = "/";
	
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
		this.activityLoginService = activityLoginService;
	}

	@Override
    public final void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication)
            throws IOException, ServletException {
		final String status = request.getParameter("status");
		
		if (authentication != null) {
			final WebUser user = (WebUser) authentication.getPrincipal();
			if (user != null) {
				if (!StringUtils.isBlank(status) && status.equals("inactive")) {
					saveActivityLog(user.getUserAccountId(), true);
				} else {
					saveActivityLog(user.getUserAccountId(), false);
				}
			}
		}
		
		if (!StringUtils.isBlank(status) && status.equals("inactive")) {
			response.sendRedirect(this.successURL + "?error=-2");
		} else {
			if (request.getParameter("error") == null) {
				response.sendRedirect(this.successURL);
			} else {
				response.sendRedirect(this.successURL + "?error=" + request.getParameter("error"));
			}
			
		}
	}
	
	private void saveActivityLog(final Integer userAccountId, final boolean isSessionTimeout) {

		final ActivityLogin activityLogin = new ActivityLogin();
		final LoginResultType loginResultType = new LoginResultType();
		final UserAccount userAccount = new UserAccount();
		userAccount.setId(userAccountId);
		loginResultType.setId(isSessionTimeout? WebSecurityConstants.LOGIN_RESULT_TYPE_TIMEOUT_LOGOUT : WebSecurityConstants.LOGIN_RESULT_TYPE_USER_LOGOUT);
		activityLogin.setLoginResultType(loginResultType);
		activityLogin.setUserAccount(userAccount);
		activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());

		this.activityLoginService.saveActivityLogin(activityLogin);
	}

	public final String getSuccessURL() {
		return this.successURL;
	}

	public final void setSuccessURL(final String successURL) {
		this.successURL = successURL;
	}
}
