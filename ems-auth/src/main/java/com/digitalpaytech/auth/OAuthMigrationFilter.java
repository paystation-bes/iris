package com.digitalpaytech.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;

import com.digitalpaytech.auth.dto.TokenUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.esotericsoftware.minlog.Log;

@SuppressWarnings("checkstyle:designForExtension")
public class OAuthMigrationFilter extends WebAuthenticationProcessingFilter {
    @Autowired
    private UserAccountService accountService;
    
    @Autowired
    private CustomerService customerService;
    
    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        final HttpSession session = request.getSession(false);
        final TokenUser tokenUser;
        if (session != null) {
            tokenUser = (TokenUser) session.getAttribute(TokenUser.SESSION_KEY);
            request.getSession().invalidate();
            request.getSession(true);
        } else {
            throw new BadCredentialsException("User need to be logged in via OAuth!");
        }
        
        /* get the user name and password */
        final String username = obtainUsername(request);
        final String password = obtainPassword(request);
        
        validatePassword(password);
        final String encodedUsername = validateUsername(username);
        
        final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(encodedUsername, password);
        setDetails(request, authRequest);
        
        final Authentication authentication = getAuthenticationManager().authenticate(authRequest);
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        if (password.matches(WebSecurityConstants.COMPLEX_PASSWORD_EXP)) {
            webUser.setIsComplexPassword(true);
        }
        
        populateWebUser(webUser, request);
        
        final UserAccount account = this.accountService.findUserAccount(webUser.getUserAccountId());
        if (StringUtils.hasText(account.getLinkUserName())) {
            Log.error("Received migration request for user that has already been migrated to SSO: " + webUser.getUserAccountId());
            throw new BadCredentialsException("User has already migrated!");
        }
        
        final Customer customer = this.customerService.findCustomer(account.getCustomer().getId());
        
        final Integer unifiId = customer.getUnifiId();
        if (unifiId == null) {
            throw new BadCredentialsException("Customer doesn't have a valid Unifi Id!");
        } else if (unifiId != tokenUser.getCustomer().getCustomerId()) {
            Log.error("Received missmatched customerId for SSO Migration: expecting " + account.getCustomer().getId() + " but received "
                      + tokenUser.getCustomer().getCustomerId());
            throw new BadCredentialsException("Missmatched CustomerId!");
        }
        
        account.setLinkUserName(tokenUser.getUsername());
        this.accountService.updateUserAccount(account);
        Log.info("Migrated User '" + username + "' to SSO User '" + tokenUser.getUsername() + "'");
        
        webUser.setLinkUserName(tokenUser.getUsername());
        webUser.setLoginViaPortal(true);
        
        if (webUser.getSwitchToAliasUserAccount() != null) {
            WebSecurityUtil.changeUserOnLogin(webUser);
        }
        
        return authentication;
    }
}
