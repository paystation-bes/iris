package com.digitalpaytech.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.service.UserAccountService;

@Component("webSecurityUtil")
public class WebSecurityUtil {
    public static final String QUESTION_MARK = "?";
    public static final String DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss";
    public static final String TIME_ZONE_GMT = "GMT";
    
    private static EntityService entityService;
    private static UserAccountService userAccountService;
    private static ReportRepositoryService reportRepositoryService;
    private static CustomerSubscriptionService customerSubscriptionService;
    private static ActivityLoginService activityLoginService;
    
    @Autowired(required = true)
    public final void setEntityService(final EntityService entityService) {
        WebSecurityUtil.entityService = entityService;
    }
    
    @Autowired(required = true)
    public final void setUserAccountService(final UserAccountService userAccountService) {
        WebSecurityUtil.userAccountService = userAccountService;
    }
    
    @Autowired(required = true)
    public final void setReportRepositoryService(final ReportRepositoryService reportRepositoryService) {
        WebSecurityUtil.reportRepositoryService = reportRepositoryService;
    }
    
    @Autowired(required = true)
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        WebSecurityUtil.customerSubscriptionService = customerSubscriptionService;
    }
    
    @Autowired(required = true)
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        WebSecurityUtil.activityLoginService = activityLoginService;
    }
    
    public static String getRandomLongAsHexString(final int minLength, final int maxLength) {
        long number;
        String hex;
        final Random random = new Random(Calendar.getInstance().getTimeInMillis());
        do {
            number = random.nextLong();
            hex = Long.toHexString(number);
        } while (!GenericValidator.isInRange(hex.length(), minLength, maxLength));
        
        return hex;
    }
    
    public static String appendSessionTokenString(final HttpServletRequest request, final String path) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(path);
        //TODO Remove SessionToken
        //        if (!path.endsWith(QUESTION_MARK)) {
        //            bdr.append(QUESTION_MARK);
        //        }
        //        bdr.append(WebSecurityConstants.SESSION_TOKEN).append("=");
        //        bdr.append(request.getSession().getAttribute(WebSecurityConstants.SESSION_TOKEN));
        return bdr.toString();
    }
    
    public static Date getCurrentDateInGmt() throws ParseException {
        return getCurrentDate(DATE_FORMAT, TIME_ZONE_GMT);
    }
    
    public static Date getCurrentDate(final String format, final String timeZone) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        //Local time zone   
        final SimpleDateFormat dateFormatLocal = new SimpleDateFormat(format);
        final Date date = dateFormatLocal.parse(dateFormat.format(new Date()));
        return date;
    }
    
    public static boolean isCustomerMigrated() {
        final Authentication authentication = getAuthentication();
        return ((WebUser) authentication.getPrincipal()).getIsMigrated();
    }
    
    public static boolean hasUserValidPermission(final int permissionId) {
        
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        
        if (webUser.isSystemAdmin()) {
            return false;
        }
        
        final Collection<Integer> permissions = webUser.getUserPermissions();
        if (permissions == null || permissions.isEmpty()) {
            return false;
        }
        return permissions.contains(new Integer(permissionId));
    }
    
    public static boolean permissionAudit(final HttpServletResponse response, final Integer... permissionId) {
        for (Integer permission : permissionId) {
            if (hasUserValidPermission(permission)) {
                return true;
            }
        }
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        return false;
    }
    
    public static boolean hasSystemAdminUserValidPermission(final int permissionId) {
        
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        
        if (!webUser.isSystemAdmin()) {
            return false;
        }
        
        final Collection<Integer> permissions = webUser.getUserPermissions();
        if (permissions == null || permissions.isEmpty()) {
            return false;
        }
        return permissions.contains(new Integer(permissionId));
    }
    
    public static boolean hasValidCustomerSubscription(final int subscriptionTypeId) {
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final Collection<Integer> subscriptions = webUser.getCustomerSubscriptions();
        if (subscriptions == null || subscriptions.isEmpty()) {
            return false;
        }
        return subscriptions.contains(new Integer(subscriptionTypeId));
    }
    
    /**
     * This method will return Authentication object from security context.
     * 
     * @return Authentication object
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
    
    /**
     * This method will retrieve UserAccount information from security authentication.
     * 
     * @return UserAccount object
     */
    public static UserAccount getUserAccount() {
        /* get customer information from security framework session. */
        final Authentication authentication = getAuthentication();
        UserAccount ua = userAccountService.findUserAccount(((WebUser) authentication.getPrincipal()).getUserAccountId());
        ua = (UserAccount) entityService.merge(ua);
        return ua;
    }
    
    /**
     * This method will retrieve the current customers TimeZone information from security authentication.
     * 
     * @return String timeZone
     */
    public static String getCustomerTimeZone() {
        final Authentication authentication = getAuthentication();
        return ((WebUser) authentication.getPrincipal()).getCustomerTimeZone();
    }
    
    public static boolean checkForPosSubscriptions(final PointOfSale pointOfSale, final Integer[] subscriptionCodes) {
        final List<Integer> subscriptionCodesList = Arrays.asList(subscriptionCodes);
        final Collection<CustomerSubscription> customerSubscriptions =
                WebSecurityUtil.customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(pointOfSale.getCustomer().getId(), true);
        for (Integer subscriptionCode : subscriptionCodesList) {
            for (CustomerSubscription customerSubscription : customerSubscriptions) {
                if (customerSubscription.getIsEnabled()) {
                    if (customerSubscription.getSubscriptionType().getId() == subscriptionCode.intValue()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private static boolean hasPermission(final CustomerRole customerRole, final List<Integer> permissionCodes) {
        RolePermission rolePermission;
        Iterator<RolePermission> rolePerIter;
        rolePerIter = customerRole.getRole().getRolePermissions().iterator();
        while (rolePerIter.hasNext()) {
            rolePermission = rolePerIter.next();
            if (permissionCodes.contains(rolePermission.getPermission().getId())) {
                return true;
            }
        }
        return false;
    }
    
    public static void updateUnreadReportStatus() {
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final Collection<Integer> permissions = webUser.getUserPermissions();
        // If user has permission for reports update numberOfUnreadRepots
        if (permissions.contains(new Integer(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT))) {
            webUser.setNumberOfUnreadReports(reportRepositoryService.countUnreadReportsByUserAccountId(webUser.getUserAccountId()));
        }
    }
    
    public static boolean isCustomerAdmin() {
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        UserAccount userAccount = null;
        if (webUser.getIsParent()) {
            userAccount = userAccountService.findAdminUserAccountByParentCustomerId(webUser.getCustomerId());
        } else {
            userAccount = userAccountService.findAdminUserAccountByChildCustomerId(webUser.getCustomerId());
        }
        if (userAccount.getId().intValue() == webUser.getUserAccountId().intValue()) {
            return true;
        }
        return false;
    }
    
    public static void updateMobileAppCount() {
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final int appCount = (int) customerSubscriptionService.countMobileLicensesForCustomer(webUser.getCustomerId());
        webUser.setMobileAppCount(appCount);
    }
    
    public static void changeUser(final int userAccountId) {
        final Authentication authentication = getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final int originalUserAccountId = webUser.getUserAccountId();
        final UserAccount userAccount = userAccountService.findUserAccount(userAccountId);
        if (userAccount == null) {
            return;
        }
        setupWebUser(webUser, userAccount);
        saveActivityLogSwitchOut(originalUserAccountId);
        saveActivityLogSwitchIn(userAccountId);
    }
    
    public static void changeUserOnLogin(final WebUser webUser) {
        final int userAccountId = webUser.getSwitchToAliasUserAccount();
        final int originalUserAccountId = webUser.getUserAccountId();
        final UserAccount userAccount = userAccountService.findUserAccount(userAccountId);
        if (userAccount == null) {
            return;
        }
        setupWebUser(webUser, userAccount);
        saveActivityLogSwitchOut(originalUserAccountId);
        saveActivityLogSwitchIn(userAccountId);
    }
    
    private static void saveActivityLogSwitchIn(final Integer userAccountId) {
        
        final ActivityLogin activityLogin = new ActivityLogin();
        final LoginResultType loginResultType = new LoginResultType();
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(userAccountId);
        loginResultType.setId(WebSecurityConstants.LOGIN_RESULT_TYPE_USER_SWITCHED_IN);
        activityLogin.setLoginResultType(loginResultType);
        activityLogin.setUserAccount(userAccount);
        activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());
        
        activityLoginService.saveActivityLogin(activityLogin);
    }
    
    private static void saveActivityLogSwitchOut(final Integer userAccountId) {
        
        final ActivityLogin activityLogin = new ActivityLogin();
        final LoginResultType loginResultType = new LoginResultType();
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(userAccountId);
        loginResultType.setId(WebSecurityConstants.LOGIN_RESULT_TYPE_USER_SWITCHED_OUT);
        activityLogin.setLoginResultType(loginResultType);
        activityLogin.setUserAccount(userAccount);
        activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());
        
        activityLoginService.saveActivityLogin(activityLogin);
    }
    
    private static void setupWebUser(final WebUser webUser, final UserAccount userAccount) {
        if (webUser == null || userAccount == null) {
            return;
        }
        webUser.setUserAccountId(userAccount.getId());
        webUser.setCustomerId(userAccount.getCustomer().getId());
        webUser.setCustomerName(userAccount.getCustomer().getName());
        
        String customerTimeZone = webUser.getCustomerTimeZone();
        
        final Set<CustomerProperty> customerProperties = userAccount.getCustomer().getCustomerProperties();
        for (CustomerProperty property : customerProperties) {
            if (property.getCustomerPropertyType().getId() == 1) {
                customerTimeZone = property.getPropertyValue();
                break;
            }
        }
        webUser.setCustomerTimeZone(customerTimeZone);
        
        final List<CustomerSubscription> subscriptionList =
                customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(userAccount.getCustomer().getId(), true);
        final Collection<Integer> ids = new ArrayList<Integer>();
        for (CustomerSubscription customerSubscription : subscriptionList) {
            ids.add(customerSubscription.getSubscriptionType().getId());
        }
        webUser.setCustomerSubscriptions(ids);
        webUser.setIsSystemAdmin(false);
        webUser.setIsParent(userAccount.getCustomer().isIsParent());
        
        final Set<Integer> permissionIds = new HashSet<Integer>();
        final List<RolePermission> rolePermissions =
                userAccountService.findUserRoleAndPermissionForCustomerType(userAccount.getId(), userAccount.getCustomer().getCustomerType().getId());
        for (RolePermission rp : rolePermissions) {
            if ((!webUser.isSystemAdmin()) && (rp.getRole().getCustomerType().getId() == WebSecurityConstants.CUSTOMER_TYPE_ID_SYSTEM_ADMIN)) {
                webUser.setIsSystemAdmin(true);
            }
            
            if (rp.getPermission().getPermission() != null) {
                permissionIds.add(rp.getPermission().getPermission().getId());
            }
            
            if (rp.getRole().getCustomerType().getId() == userAccount.getCustomer().getCustomerType().getId()) {
                permissionIds.add(rp.getPermission().getId());
            }
        }
        
        webUser.setUserPermissions(permissionIds);
    }
    
}
