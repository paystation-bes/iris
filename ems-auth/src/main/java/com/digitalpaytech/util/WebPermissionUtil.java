package com.digitalpaytech.util;

public final class WebPermissionUtil {
    
    private WebPermissionUtil() {
        
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings global tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsGlobalTabPermissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings locations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsLocationsTabPermissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)
            
            || ((WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)
                 || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS))
                && (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
                    || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES))
                && (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                    || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)))) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings routes tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsRoutesTabPersmissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings alerts tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsAlertsTabPersmissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings users tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsUsersTabPersmissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing any sub
     * tab within the customer settings card settings tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsCardSettingsPersmissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab location details within the customer settings locations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsLocationsDetailsTabPermissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab extend by phone within the customer settings locations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsExtendByPhoneTabPermissions() {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab pay station placement within the customer settings locations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsPayStationPlacementPermissions() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_PLACEMENT);
    }
    
    public static boolean hasCustomerSettingsPayStationSettings() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS)
               || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS);
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab pay station list within the customer settings locations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasCustomerSettingsPayStationListPermissions() {
        if ((WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)
             || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS))
            && (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
                || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES))
            && (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS))) {
            return true;
        }
        return false;
    }
    
    public static boolean hasCustomerSettingsConfigGroupView() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS)
               || hasCustomerSettingsConfigGroupManage() || hasCustomerSettingsConfigGroupSchedule();
    }
    
    public static boolean hasCustomerSettingsConfigGroupManage() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS);
    }
    
    public static boolean hasCustomerSettingsConfigGroupSchedule() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SCHEDULE_PAYSTATION_SETTINGS_UPDATE);
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab pay station list within the system admin pay stations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasSystemAdminPayStationPlacementPermissions() {
        if ((WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)
             || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS))
            && (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES))
            && (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS))) {
            return true;
        }
        return false;
    }
    
    /**
     * This method uses the WebSecurityUtil.hasUserValidPermission() method to
     * check if the current user has the correct permissions for viewing the sub
     * tab pay station list within the system admin pay stations tab.
     * 
     * @return true if user has permissions, false if not
     */
    public static boolean hasSystemAdminPayStationListPermissions() {
        if ((WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION)
             || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION))
            && (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES))
            && (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS))) {
            return true;
        }
        return false;
    }
    
    public static boolean hasSystemAdminWorkspacePermissions() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_PAYSTATION_MANAGEMENT)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_REPORTS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MOBILEAPP)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DIGITAL_API_ADMINISTRATION);
    }
}
