package com.digitalpaytech.util;

import java.io.File;
import java.text.MessageFormat;
import java.util.HashSet;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.service.EmsPropertiesService;

/**
 * SessionManager
 * 
 * @author Brian Kim
 * 
 */
public class SessionManager extends HttpSessionEventPublisher {
    private static final Logger LOGGER = Logger.getLogger(SessionManager.class);
    
    private static final String FMT_STACK_TRACE = "{3}@{2}@{1}";
    private static final HashSet<String> IGNORED_LOGOUT_STACK_TRACE = new HashSet<String>();
    static {
        IGNORED_LOGOUT_STACK_TRACE.add(MessageFormat.format(FMT_STACK_TRACE, "com.digitalpaytech.mvc.HomeController", "showHome", 34));
        IGNORED_LOGOUT_STACK_TRACE.add(MessageFormat.format(FMT_STACK_TRACE, "com.digitalpaytech.rpc.RPCConnectorServlet", "doPost", 153));
    }
    
    @Override
    public final void sessionCreated(final HttpSessionEvent arg0) {
        LOGGER.debug("Start session");
        final HttpSession session = arg0.getSession();
        
        /* retrieve defined session timeout */
        final ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
        final EmsPropertiesService emsPropertiesService = (EmsPropertiesService) ctx.getBean("emsPropertiesService");
        
        final String sessionTimeout = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_SESSION_TIMEOUT_MINUTE);
        if (sessionTimeout != null) {
            session.setMaxInactiveInterval(Integer.parseInt(sessionTimeout) * 60);
        }
        
        //TODO Remove SessionToken
        //		String token = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.SESSION_TOKEN_LENGTH);
        //		session.setAttribute(WebSecurityConstants.SESSION_TOKEN, token);
        
        session.setAttribute(WebSecurityConstants.SESSION_TOKEN, session.getId());
        
        super.sessionCreated(arg0);
    }
    
    @Override
    public final void sessionDestroyed(final HttpSessionEvent arg0) {
        String tempReportsPath;
        LOGGER.debug("End session");
        
        if (LOGGER.isDebugEnabled()) { // This is debug on purpose !
            boolean abnormal = true;
            final StackTraceElement[] callStacks = Thread.currentThread().getStackTrace();
            for (StackTraceElement ste : callStacks) {
                if (IGNORED_LOGOUT_STACK_TRACE.contains(MessageFormat.format(FMT_STACK_TRACE, ste.getClassName(), ste.getMethodName(),
                                                                             ste.getLineNumber()))) {
                    abnormal = false;
                    break;
                }
            }
            
            if (abnormal) {
                LOGGER.error("Unexpected logout, call stacks...");
                for (StackTraceElement ste : callStacks) {
                    LOGGER.error(ste);
                }
            }
        }
        
        try {
            final HttpSession session = arg0.getSession();
            //TODO Remove SessionToken
            session.removeAttribute(WebSecurityConstants.SESSION_TOKEN);
            session.removeAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
            session.removeAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
            session.removeAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED);
            session.removeAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED);
            session.removeAttribute(WebCoreConstants.RANDOM_KEY_MAPPING);
            tempReportsPath = session.getServletContext().getRealPath(WebCoreConstants.REPORTS_TEMP_PATH);
            final String fullPathName = tempReportsPath + File.separator + session.getId();
            
            final File fullPathFile = new File(fullPathName);
            if (fullPathFile.exists()) {
                if (WebCoreUtil.deleteDir(fullPathFile)) {
                    LOGGER.info("Delete temp report directory : " + fullPathName);
                } else {
                    LOGGER.error("Can't delete temp report directory");
                }
            }
        } catch (Exception e) {
            LOGGER.error("Can't delete temp report directory, Exception : " + e.toString());
        } finally {
            super.sessionDestroyed(arg0);
        }
    }
}
