package com.digitalpaytech.util;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.digitalpaytech.auth.WebUser;

public class AuthenticatedUserDetails {
    public static WebUser getPrincipal() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        
        final Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (obj instanceof WebUser) {
            return (WebUser) obj;
        } else {
            return null;
        }
    }
    
    public static String getUsername() {
        return getPrincipal().getUsername();
    }
    
    public static String getCustomerTimeZone() {
        return getPrincipal().getCustomerTimeZone();
    }
    
    public static boolean hasUserValidPermission(final int permissionId) {
        
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final Collection<Integer> permissions = webUser.getUserPermissions();
        if (permissions == null || permissions.isEmpty()) {
            return false;
        }
        return permissions.contains(new Integer(permissionId));
    }
    
    public static boolean hasValidCustomerSubscription(final int subscriptionTypeId) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        final Collection<Integer> subscriptions = webUser.getCustomerSubscriptions();
        if (subscriptions == null || subscriptions.isEmpty()) {
            return false;
        }
        return subscriptions.contains(new Integer(subscriptionTypeId));
    }
    
    public static void updateUnreadReportStatus() {
        WebSecurityUtil.updateUnreadReportStatus();
    }
    
    public static void updateMobileAppCount() {
        WebSecurityUtil.updateMobileAppCount();
    }
}
