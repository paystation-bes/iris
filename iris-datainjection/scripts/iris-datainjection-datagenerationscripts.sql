-- ********************************************************************************
-- TO BE RUN ON EMS6 DATABASE
-- ********************************************************************************


Select 
    T.PurchasedDate as PurchasedDate,
    T.ExpiryDate as ExpiryDate,
    T.PaystationId as PaystationId,
    T.TicketNumber as TicketNumber,
    T.LotNumber as LotNumber,
    T.PlateNumber as LicensePlateNo,
    T.AddTimeNum as AddTimeNum,
    T.StallNumber as StallNumber,
    TT.Name as Type,
    TIMESTAMPDIFF(MINUTE,
        T.PurchasedDate,
        T.ExpiryDate) as ParkingTimePurchased,
    T.OriginalAmount as OriginalAmount,
    T.OriginalAmount as CashPaid,
    0 as CardPaid,
    '' as CardData,
    T.IsRefundSlip as IsRefundSlip,
    R.RateName as RateName,
    T.RateId as RateId,
    R.RateValue as RateValue
from
    Transaction T
        LEFT JOIN
    TransactionType TT ON T.TypeId = TT.Id
        LEFT JOIN
    Rates R ON T.PaystationId = R.PaystationId
        and T.TicketNumber = R.TicketNumber
        and T.PurchasedDate = R.PurchasedDate
where
    T.PaystationId in (2101 , 2102, 2873, 2874, 2875)
        and T.PurchasedDate between '2013-10-01 07:00:00' and '2013-10-08 06:59:59'
limit 1;
-- *****
Select 
    count(*)
from
    Transaction T
        LEFT JOIN
    TransactionType TT ON T.TypeId = TT.Id
        LEFT JOIN
    Rates R ON T.PaystationId = R.PaystationId
        and T.TicketNumber = R.TicketNumber
        and T.PurchasedDate = R.PurchasedDate
where
    T.PaystationId in (2101 , 2102, 2873, 2874, 2875)
        and T.PurchasedDate between '2013-10-01 07:00:00' and '2013-10-08 06:59:59'	;
-- *****


Select 
    count(*), SL.PaystationId
from
    SensorLog SL,
    SensorType ST
Where
    SL.TypeId = ST.Id
        and SL.PaystationId in (48901 , 48911, 48922, 48941, 48951)
        and SL.DateField between '2013-10-14 07:00:00' and '2013-10-21 07:00:00'
Group by SL.PaystationId;

Select 
    SL.DateField as Date,
    SL.PaystationId as PaystationId,
    'Sensor' as Type,
    REPLACE(ST.Name, ' ', '') as Value,
    SL.Value as Information
from
    SensorLog SL,
    SensorType ST
Where
    SL.TypeId = ST.Id
        and SL.PaystationId in (48901 , 48911, 48922, 48941, 48951)
        and SL.DateField between '2013-10-14 07:00:00' and '2013-10-21 07:00:00';
