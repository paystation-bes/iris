package com.digitalpaytech.scheduling.datainjection;

/**
 * @author Amanda Chong
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewCollection;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewDataInjection;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewEvent;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewPaystation;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewRequest;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewTransaction;
import com.digitalpaytech.scheduling.datainjection.service.PreviewCollectionService;
import com.digitalpaytech.scheduling.datainjection.service.PreviewDataInjectionService;
import com.digitalpaytech.scheduling.datainjection.service.PreviewEventService;
import com.digitalpaytech.scheduling.datainjection.service.PreviewPaystationService;
import com.digitalpaytech.scheduling.datainjection.service.PreviewRequestService;
import com.digitalpaytech.scheduling.datainjection.service.PreviewTransactionService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

@Component("dataInjectionProcessingTask")
public class DataInjectionProcessingTask {
    private static final int ONE_MINUTE_IN_SECS = 60;
    private static final long ONE_MINUTE_IN_MILLIS = 60000L;
    
    private static Logger log = Logger.getLogger(DataInjectionProcessingTask.class);
    
    private static boolean isLocked = false;
    private static Random secondGenerator = new Random();
    
    @Autowired
    private PreviewRequestService previewRequestService;
    @Autowired
    private PreviewPaystationService previewPaystationService;
    @Autowired
    private PreviewEventService previewEventService;
    @Autowired
    private PreviewTransactionService previewTransactionService;
    @Autowired
    private PreviewCollectionService previewCollectionService;
    @Autowired
    private PreviewDataInjectionService previewDataInjectionService;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public void sendRequests() {
        
        if (DataInjectionProcessingTask.isLocked) {
            return;
        }
        try {
            DataInjectionProcessingTask.isLocked = true;
            
            // Get current time for comparisons with lastRequestSentTime
            final Calendar currentCalendar = Calendar.getInstance();
            final long currentTotalMinute = currentCalendar.getTimeInMillis() / ONE_MINUTE_IN_MILLIS;
            
            // Get last request sent time stamp from database
            final PreviewDataInjection previewDataInjection = this.previewDataInjectionService
                    .findPreviewDataInjectionById(DataInjectionConstants.LASTREQUESTSENT);
            final Date date = previewDataInjection.getLastModifiedGmt();
            final Calendar lastRequestSentcalendar = Calendar.getInstance();
            lastRequestSentcalendar.setTime(date);
            
            long runningTotalMinute = lastRequestSentcalendar.getTimeInMillis() / ONE_MINUTE_IN_MILLIS;
            
            final Map<String, List<PreviewPaystation>> paystationMap = new HashMap<String, List<PreviewPaystation>>();
            while (currentTotalMinute != runningTotalMinute) {
                runningTotalMinute++;
                lastRequestSentcalendar.add(Calendar.MINUTE, 1);
                final int runningDayOfWeek = lastRequestSentcalendar.get(Calendar.DAY_OF_WEEK);
                final int runningHour = lastRequestSentcalendar.get(Calendar.HOUR_OF_DAY);
                final int runningMinute = lastRequestSentcalendar.get(Calendar.MINUTE);
                final int runningMinuteOfDay = (runningHour * ONE_MINUTE_IN_SECS) + runningMinute;
                
                final Iterator<PreviewRequest> previewRequestItr = this.previewRequestService.findPreviewRequestByDayAndMinute(runningDayOfWeek,
                    runningMinuteOfDay).iterator();
                while (previewRequestItr.hasNext()) {
                    final PreviewRequest previewRequest = previewRequestItr.next();
                    previewRequestItr.remove();
                    
                    final List<PreviewPaystation> previewPaystationList = getPaystationList(previewRequest.getPaystationId(), paystationMap);
                    for (final PreviewPaystation previewPaystation : previewPaystationList) {
                        sendPost(previewPaystation, previewRequest, lastRequestSentcalendar);
                    }
                }
            }
            
            // Update preview_datainjection lastRequestSent
            final String updatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastRequestSentcalendar.getTime());
            previewDataInjection.setValue(updatedDate);
            previewDataInjection.setLastModifiedGmt(lastRequestSentcalendar.getTime());
            this.previewDataInjectionService.savePreviewDataInjection(previewDataInjection);
            
        } finally {
            DataInjectionProcessingTask.isLocked = false;
        }
    }
    
    private List<PreviewPaystation> getPaystationList(final String paystationId, final Map<String, List<PreviewPaystation>> paystationMap) {
        List<PreviewPaystation> paystationList = null;
        if (paystationMap.containsKey(paystationId)) {
            paystationList = paystationMap.get(paystationId);
        } else {
            paystationList = this.previewPaystationService.findPreviewPaystationByPaystationId(paystationId);
            paystationMap.put(paystationId, paystationList);
        }
        return paystationList;
    }
    
    private void sendPost(final PreviewPaystation previewPaystation, final PreviewRequest previewRequest, final Calendar runningTime) {
        try {
            String urlString = this.emsPropertiesService.getPropertyValue("ServerUrl");
            if (!urlString.startsWith("https")) {
                urlString = urlString.substring(4);
                urlString = "http" + urlString;
            }
            urlString += DataInjectionConstants.XMLRPC_PS2;
            
            log.info("Injecting data to: " + urlString);
            
            final URL url = new URL(urlString);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod("POST");
            final String request = getXml(previewPaystation, previewRequest, runningTime);
            
            if (request != null) {
                connection.setDoOutput(true);
                final DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(request);
                wr.flush();
                wr.close();
                
                final int responseCode = connection.getResponseCode();
                log.info("Request sent: " + request);
                log.info("Response Code: " + responseCode);
                
                final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();
                
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                
                String responseMessage = response.toString();
                if (response.indexOf("<![CDATA[") > -1) {
                    responseMessage = responseMessage.substring(responseMessage.indexOf("<![CDATA[") + 9, responseMessage.indexOf("]]>"));
                } else {
                    if (responseMessage.indexOf("<string>") > -1) {
                        responseMessage = responseMessage.substring(responseMessage.indexOf("<string>") + 8, responseMessage.indexOf("</string>"));
                    } else {
                        responseMessage = responseMessage.substring(responseMessage.indexOf("<value>") + 7, responseMessage.indexOf("</value>"));
                    }
                    responseMessage = responseMessage.substring(responseMessage.indexOf("<base64>") + 8, responseMessage.indexOf("</base64>"));
                }
                responseMessage = new String(Base64.decode(responseMessage));
                responseMessage = StringEscapeUtils.unescapeXml(responseMessage);
                log.debug("Response Message: " + responseMessage);
            } else {
                log.debug("No request sent for Preview_Request.RequestType = " + previewRequest.getPreviewRequestType().getId()
                          + " and Preview_Request.RequestId = " + previewRequest.getRequestId());
            }
            
        } catch (Base64DecodingException e) {
            log.error("Failed to decode responseMessage", e);
            e.printStackTrace();
        } catch (MalformedURLException e) {
            log.error("Failed to create URL: " + this.emsPropertiesService.getPropertyValue("ServerUrl") + DataInjectionConstants.XMLRPC_PS2, e);
            e.printStackTrace();
        } catch (ProtocolException e) {
            log.error("Failed to setRequestMethod(\"POST\")", e);
            e.printStackTrace();
        } catch (IOException e) {
            log.error("IOException in sendPost method", e);
            e.printStackTrace();
        }
        
    }
    
    private String getXml(final PreviewPaystation previewPaystation, final PreviewRequest previewRequest, final Calendar runningTime) {
        switch (previewRequest.getPreviewRequestType().getId()) {
            case DataInjectionConstants.TRANSACTION:
                final PreviewTransaction previewTransaction = this.previewTransactionService
                        .findPreviewTransactionById(previewRequest.getRequestId());
                if (previewTransaction != null) {
                    return getTransactionXml(previewPaystation, previewRequest, previewTransaction, runningTime);
                } else {
                    log.debug("No Preview_Transaction found for Preview_Request.RequestType = " + previewRequest.getPreviewRequestType().getId()
                              + " and Preview_Request.RequestId = " + previewRequest.getRequestId());
                }
                break;
            case DataInjectionConstants.EVENT:
                final PreviewEvent previewEvent = this.previewEventService.findPreviewEventById(previewRequest.getRequestId());
                if (previewEvent != null) {
                    return getEventXml(previewPaystation, previewRequest, previewEvent, runningTime);
                } else {
                    log.debug("No Preview_Event found for Preview_Request.RequestType = " + previewRequest.getPreviewRequestType().getId()
                              + " and Preview_Request.RequestId = " + previewRequest.getRequestId());
                }
                break;
            case DataInjectionConstants.COLLECTION:
                final PreviewCollection previewCollection = this.previewCollectionService.findPreviewCollectionById(previewRequest.getRequestId());
                if (previewCollection != null) {
                    return getCollectionXml(previewPaystation, previewRequest, previewCollection, runningTime);
                } else {
                    log.debug("No Preview_Collection found for Preview_Request.RequestType = " + previewRequest.getPreviewRequestType().getId()
                              + " and Preview_Request.RequestId = " + previewRequest.getRequestId());
                }
                break;
            default:
                break;
        }
        return null;
    }
    
    private String getTransactionXml(final PreviewPaystation previewPaystation, final PreviewRequest previewRequest,
        final PreviewTransaction previewTransaction, final Calendar runningTime) {
        
        String xml = null;
        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            final Document doc = docBuilder.newDocument();
            
            final Element paystation_2 = doc.createElement("Paystation_2");
            final Attr password = doc.createAttribute("Password");
            password.setValue("zaq123$ESZxsw234%RDX");
            final Attr userId = doc.createAttribute("UserId");
            userId.setValue("emsadmin");
            final Attr version = doc.createAttribute("Version");
            version.setValue("0.0.1");
            paystation_2.setAttributeNode(password);
            paystation_2.setAttributeNode(userId);
            paystation_2.setAttributeNode(version);
            doc.appendChild(paystation_2);
            
            final Element message4EMS = doc.createElement("Message4EMS");
            paystation_2.appendChild(message4EMS);
            
            final Element transaction = doc.createElement("Transaction");
            final Attr messageNumberAtr = doc.createAttribute("MessageNumber");
            final Integer paystationMessageNumber = previewPaystation.getMessageNumber();
            messageNumberAtr.setValue(Integer.toString(paystationMessageNumber));
            previewPaystation.setMessageNumber(paystationMessageNumber + 1);
            this.previewPaystationService.savePreviewPaystation(previewPaystation);
            transaction.setAttributeNode(messageNumberAtr);
            message4EMS.appendChild(transaction);
            
            createElement("PaystationCommAddress", previewPaystation.getSerialNumber(), transaction, doc);
            createElement("Number", Integer.toString(previewTransaction.getNumber()), transaction, doc);
            createElement("LotNumber", previewTransaction.getLotNumber(), transaction, doc);
            
            final String licensePlateNoString = previewTransaction.getLicensePlateNo();
            if (licensePlateNoString != null && !licensePlateNoString.equals("")) {
                createElement("LicensePlateNo", licensePlateNoString, transaction, doc);
            }
            
            createElement("AddTimeNum", Integer.toString(previewTransaction.getAddTimeNum()), transaction, doc);
            createElement("StallNumber", Integer.toString(previewTransaction.getStallNumber()), transaction, doc);
            createElement("Type", previewTransaction.getType(), transaction, doc);
            
            final Calendar purchaseDate = (Calendar) runningTime.clone();
            purchaseDate.set(Calendar.SECOND, getSeconds());
            createElement("PurchasedDate", DateUtil.createColonDelimitedDateString(purchaseDate.getTime()), transaction, doc);
            
            final Calendar transDateTime = (Calendar) purchaseDate.clone();
            transDateTime.add(Calendar.SECOND, getSeconds());
            createElement("TransDateTime", DateUtil.createColonDelimitedDateString(transDateTime.getTime()), transaction, doc);
            
            createElement("ParkingTimePurchased", Integer.toString(previewTransaction.getParkingTimePurchased()), transaction, doc);
            createElement("OriginalAmount", Integer.toString(previewTransaction.getOriginalAmount()), transaction, doc);
            createElement("ChargedAmount", Integer.toString(previewTransaction.getCashPaid()), transaction, doc);
            createElement("CashPaid", Integer.toString(previewTransaction.getCashPaid()), transaction, doc);
            createElement("NumberBillsAccepted", Integer.toString(previewTransaction.getNumberBillsAccepted()), transaction, doc);
            createElement("NumberCoinsAccepted", Integer.toString(previewTransaction.getNumberCoinsAccepted()), transaction, doc);
            
            if (previewTransaction.getNumberBillsAccepted() > 0) {
                
                Integer billColInteger = previewTransaction.getBillCol();
                String result = "";
                boolean changed = false;
                billColInteger /= 100;
                if (billColInteger % 20 == 0) {
                    result += "20:";
                    int twenties = 0;
                    twenties = (int) Math.floor(billColInteger / 20);
                    result += twenties;
                    billColInteger -= twenties * 20;
                    changed = true;
                }
                if (billColInteger % 10 == 0) {
                    if (changed) {
                        result += ",";
                    }
                    result += "10:";
                    int tens = 0;
                    tens = (int) Math.floor(billColInteger / 10);
                    result += tens;
                    billColInteger -= tens * 10;
                    changed = true;
                }
                if (billColInteger % 5 == 0) {
                    if (changed) {
                        result += ",";
                    }
                    result += "5:";
                    int fives = 0;
                    fives = (int) Math.floor(billColInteger / 5);
                    result += fives;
                    billColInteger -= fives * 5;
                }
                
                if (result.equals("")) {
                    createElement("BillCol", " ", transaction, doc);
                } else {
                    createElement("BillCol", result, transaction, doc);
                }
            }
            
            if (previewTransaction.getNumberCoinsAccepted() > 0) {
                
                Integer coinColInteger = previewTransaction.getCoinCol();
                String result = "";
                boolean changed = false;
                coinColInteger /= 100;
                if (coinColInteger % 2 == 0) {
                    result += "200:";
                    int toonies = 0;
                    toonies = (int) Math.floor(coinColInteger / 2);
                    result += toonies;
                    coinColInteger -= toonies * 2;
                    changed = true;
                }
                if (coinColInteger >= 0) {
                    if (changed) {
                        result += ",";
                    }
                    result += "100:";
                    int tens = 0;
                    tens = (int) coinColInteger;
                    result += tens;
                    coinColInteger -= tens * 1;
                    changed = true;
                }
                
                if (result.equals("")) {
                    createElement("CoinCol", " ", transaction, doc);
                } else {
                    createElement("CoinCol", result, transaction, doc);
                }
            }
            
            createElement("CardPaid", Integer.toString(previewTransaction.getCardPaid()), transaction, doc);
            
            final Element cardData = doc.createElement("CardData");
            if (!"".equals(previewTransaction.getCardData()) && previewTransaction.getCardData() != null) {
                cardData.appendChild(doc.createTextNode(previewTransaction.getCardData()));
            } else {
                cardData.appendChild(doc.createTextNode(" "));
            }
            transaction.appendChild(cardData);
            
            createElement("IsRefundSlip", previewTransaction.getIsRefundSlip() ? "true" : "false", transaction, doc);
            
            final Calendar expirationDate = (Calendar) purchaseDate.clone();
            expirationDate.add(Calendar.MINUTE, previewTransaction.getParkingTimePurchased());
            createElement("ExpiryDate", DateUtil.createColonDelimitedDateString(expirationDate.getTime()), transaction, doc);
            
            createElement("Tax1Rate", "7.24", transaction, doc);
            createElement("Tax1Value", "1", transaction, doc);
            createElement("Tax1Name", "GST", transaction, doc);
            createElement("Tax2Rate", "5.00", transaction, doc);
            createElement("Tax2Value", "1.00", transaction, doc);
            createElement("Tax2Name", "PST", transaction, doc);
            createElement("RateName", previewTransaction.getRateName(), transaction, doc);
            createElement("RateID", Integer.toString(previewTransaction.getRateId()), transaction, doc);
            createElement("RateValue", Integer.toString(previewTransaction.getRateValue()), transaction, doc);
            createElement("TaxType", "N/A", transaction, doc);
            
            xml = documentToString(doc);
            if (xml.contains("<CardData> </CardData")) {
                final int index = xml.indexOf("<CardData>");
                final int length = xml.length();
                xml = xml.substring(0, index + 10) + xml.substring(index + 11, length - 1);
            }
            
            if (xml.contains("<BillCol> </BillCol")) {
                final int index = xml.indexOf("<BillCol>");
                final int length = xml.length();
                xml = xml.substring(0, index + 9) + xml.substring(index + 10, length - 1);
            }
            
            if (xml.contains("<CoinCol> </CoinCol")) {
                final int index = xml.indexOf("<CoinCol>");
                final int length = xml.length();
                xml = xml.substring(0, index + 9) + xml.substring(index + 10, length - 1);
            }
            
            return DataInjectionConstants.XML_OPENING_ELEMENTS + xml + DataInjectionConstants.XML_CLOSING_ELEMENTS;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        
        return "";
    }
    
    private String getEventXml(final PreviewPaystation previewPaystation, final PreviewRequest previewRequest, final PreviewEvent previewEvent,
        final Calendar runningTime) {
        
        String xml = null;
        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            final Document doc = docBuilder.newDocument();
            
            final Element paystation_2 = doc.createElement("Paystation_2");
            final Attr password = doc.createAttribute("Password");
            password.setValue("zaq123$ESZxsw234%RDX");
            final Attr userId = doc.createAttribute("UserId");
            userId.setValue("emsadmin");
            final Attr version = doc.createAttribute("Version");
            version.setValue("6.3 build 0013");
            paystation_2.setAttributeNode(password);
            paystation_2.setAttributeNode(userId);
            paystation_2.setAttributeNode(version);
            doc.appendChild(paystation_2);
            
            final Element message4EMS = doc.createElement("Message4EMS");
            paystation_2.appendChild(message4EMS);
            
            final Element event = doc.createElement("Event");
            final Attr messageNumberAtr = doc.createAttribute("MessageNumber");
            final Integer paystationMessageNumber = previewPaystation.getMessageNumber();
            messageNumberAtr.setValue(Integer.toString(paystationMessageNumber));
            previewPaystation.setMessageNumber(paystationMessageNumber + 1);
            this.previewPaystationService.savePreviewPaystation(previewPaystation);
            event.setAttributeNode(messageNumberAtr);
            message4EMS.appendChild(event);
            
            createElement("PaystationCommAddress", previewPaystation.getSerialNumber(), event, doc);
            
            final Calendar requestDate = (Calendar) runningTime.clone();
            requestDate.set(Calendar.SECOND, 0);
            createElement("TimeStamp", DateUtil.createColonDelimitedDateString(requestDate.getTime()), event, doc);
            
            try {
                final String typeString = getCorrectedType(previewEvent.getInformation(), previewEvent.getAction(), previewEvent.getType());
                createElement("Type", "ShockOn".equals(typeString) || "ShockOff".equals(typeString) ? "ShockAlarm" : typeString, event, doc);
            } catch (DOMException e) {
                e.printStackTrace();
            } catch (InvalidDataException e) {
                e.printStackTrace();
            }
            
            createElement("Action", previewEvent.getAction(), event, doc);
            
            if (!"".equals(previewEvent.getInformation()) && previewEvent.getInformation() != null) {
                createElement("Information", previewEvent.getInformation(), event, doc);
            } else {
                createElement("Information", " ", event, doc);
            }
            
            xml = documentToString(doc);
            if (xml.contains("<Information> </Information")) {
                final int index = xml.indexOf("<Information>");
                final int length = xml.length();
                xml = xml.substring(0, index + 13) + xml.substring(index + 14, length - 1);
            }
            
            return DataInjectionConstants.XML_OPENING_ELEMENTS + xml + DataInjectionConstants.XML_CLOSING_ELEMENTS;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        
        return "";
    }
    
    private String getCollectionXml(final PreviewPaystation previewPaystation, final PreviewRequest previewRequest,
        final PreviewCollection previewCollection, final Calendar runningTime) {
        String xml = null;
        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            final Document doc = docBuilder.newDocument();
            
            final Element paystation_2 = doc.createElement("Paystation_2");
            final Attr password = doc.createAttribute("Password");
            password.setValue("zaq123$ESZxsw234%RDX");
            final Attr userId = doc.createAttribute("UserId");
            userId.setValue("emsadmin");
            final Attr version = doc.createAttribute("Version");
            version.setValue("0.0.1");
            paystation_2.setAttributeNode(password);
            paystation_2.setAttributeNode(userId);
            paystation_2.setAttributeNode(version);
            doc.appendChild(paystation_2);
            
            final Element message4EMS = doc.createElement("Message4EMS");
            paystation_2.appendChild(message4EMS);
            
            final Element collection = doc.createElement("Collection");
            final Attr messageNumberAtr = doc.createAttribute("MessageNumber");
            final Integer paystationMessageNumber = previewPaystation.getMessageNumber();
            messageNumberAtr.setValue(Integer.toString(paystationMessageNumber));
            previewPaystation.setMessageNumber(paystationMessageNumber + 1);
            this.previewPaystationService.savePreviewPaystation(previewPaystation);
            collection.setAttributeNode(messageNumberAtr);
            message4EMS.appendChild(collection);
            
            createElement("Type", previewCollection.getType(), collection, doc);
            createElement("PaystationCommAddress", previewPaystation.getSerialNumber(), collection, doc);
            final int weekOfYear = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
            createElement("ReportNumber", Integer.toString(weekOfYear), collection, doc);
            createElement("LotNumber", previewCollection.getLotNumber(), collection, doc);
            createElement("MachineNumber", previewCollection.getMachineNumber(), collection, doc);
            
            Integer startMinutes = previewCollection.getStartDate();
            final int startHours = startMinutes / 60;
            startMinutes -= startHours * 60;
            final Calendar startDateCal = (Calendar) runningTime.clone();
            startDateCal.set(Calendar.SECOND, getSeconds());
            startDateCal.set(Calendar.HOUR_OF_DAY, startHours);
            startDateCal.set(Calendar.MINUTE, startMinutes);
            createElement("StartDate", DateUtil.createColonDelimitedDateString(startDateCal.getTime()), collection, doc);
            
            final Calendar endDateCal = (Calendar) runningTime.clone();
            Integer endMinutes = previewCollection.getEndDate();
            final int endHours = endMinutes / 60;
            endMinutes -= endHours * 60;
            endDateCal.set(Calendar.SECOND, getSeconds());
            endDateCal.set(Calendar.HOUR_OF_DAY, endHours);
            endDateCal.set(Calendar.MINUTE, endMinutes);
            createElement("EndDate", DateUtil.createColonDelimitedDateString(endDateCal.getTime()), collection, doc);
            
            createElement("StartTransNumber", Integer.toString(previewCollection.getStartTransNumber()), collection, doc);
            createElement("EndTransNumber", Integer.toString(previewCollection.getEndTransNumber()), collection, doc);
            createElement("TicketCount", Integer.toString(previewCollection.getTicketCount()), collection, doc);
            
            if (previewCollection.getType().equals(DataInjectionConstants.TYPE_COIN)) {
                
                createElement("CoinCount005", Integer.toString(previewCollection.getCoinCount005()), collection, doc);
                createElement("CoinCount010", Integer.toString(previewCollection.getCoinCount010()), collection, doc);
                createElement("CoinCount025", Integer.toString(previewCollection.getCoinCount025()), collection, doc);
                createElement("CoinCount100", Integer.toString(previewCollection.getCoinCount100()), collection, doc);
                createElement("CoinCount200", Integer.toString(previewCollection.getCoinCount200()), collection, doc);
                
            } else if (previewCollection.getType().equals(DataInjectionConstants.TYPE_BILL)) {
                
                createElement("BillCount01", Integer.toString(previewCollection.getBillCount01()), collection, doc);
                createElement("BillCount02", Integer.toString(previewCollection.getBillCount02()), collection, doc);
                createElement("BillCount05", Integer.toString(previewCollection.getBillCount05()), collection, doc);
                createElement("BillCount10", Integer.toString(previewCollection.getBillCount10()), collection, doc);
                createElement("BillCount20", Integer.toString(previewCollection.getBillCount20()), collection, doc);
                createElement("BillCount50", Integer.toString(previewCollection.getBillCount50()), collection, doc);
                
            }
            
            xml = documentToString(doc);
            
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return DataInjectionConstants.XML_OPENING_ELEMENTS + xml + DataInjectionConstants.XML_CLOSING_ELEMENTS;
    }
    
    private String getCorrectedType(final String information, final String action, final String type) throws InvalidDataException {
        if (WebCoreConstants.EVENT_TYPE_PAYSTATION.equals(type)) {
            if (WebCoreConstants.EVENT_ACTION_DOOR_CLOSED_STRING.equals(action) || WebCoreConstants.EVENT_ACTION_DOOR_OPENED_STRING.equals(action)) {
                if (information == null || WebCoreConstants.EMPTY_STRING.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_STRING;
                } else if (WebCoreConstants.INFO_DOOR_LOWER.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER_STRING;
                } else if (WebCoreConstants.INFO_DOOR_UPPER.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER_STRING;
                } else if (WebCoreConstants.INFO_MAINTENANCE.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_MAINTENANCE_STRING;
                } else if (WebCoreConstants.INFO_CASH_VAULT.equals(information)) {
                    return WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_CASH_VAULT_STRING;
                } else {
                    throw new InvalidDataException();
                }
            } else if (WebCoreConstants.EVENT_ACTION_SHOCK_ON_STRING.equals(action) || WebCoreConstants.EVENT_ACTION_SHOCK_OFF_STRING.equals(action)
                       || WebCoreConstants.EVENT_ACTION_ALARM_ON_STRING.equals(action)
                       || WebCoreConstants.EVENT_ACTION_ALARM_OFF_STRING.equals(action)) {
                return WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM_STRING;
            } else {
                return type;
            }
        } else if (WebCoreConstants.EVENT_DEVICE_TYPE_BILL_CANISTER_STRING.equals(type)) {
            return WebCoreConstants.EVENT_TYPE_BILL_STACKER;
        } else if (WebCoreConstants.EVENT_TYPE_COIN_HOPPER.equals(type)) {
            if (WebCoreConstants.EVENT_ACTION_COINS_DISPENSED_STRING.equals(action)) {
                final StringTokenizer infoTokens = new StringTokenizer(information, WebCoreConstants.COLON);
                final int hopperNumber = Integer.parseInt(infoTokens.nextToken());
                if ((hopperNumber < 1) || (hopperNumber > 2)) {
                    throw new InvalidDataException("Unknown HopperNumber");
                }
                //                information = infoTokens.nextToken();
                return type + hopperNumber;
            } else {
                return type + information;
            }
        } else {
            return type;
        }
    }
    
    private int getSeconds() {
        return secondGenerator.nextInt(Integer.MAX_VALUE) % ONE_MINUTE_IN_SECS;
    }
    
    private void createElement(final String elementString, final String textNode, final Element parent, final Document document) {
        final Element element = document.createElement(elementString);
        element.appendChild(document.createTextNode(textNode));
        parent.appendChild(element);
    }
    
    private String documentToString(final Document document) throws TransformerException {
        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        final Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        
        final StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        
        return writer.getBuffer().toString();
    }
}