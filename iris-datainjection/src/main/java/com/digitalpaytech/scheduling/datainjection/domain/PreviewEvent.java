package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewEvent")
@NamedQueries({ @NamedQuery(name = "PreviewEvent.findPreviewEventById", query = "SELECT pe FROM PreviewEvent pe where pe.id = :requestId") })
public class PreviewEvent implements java.io.Serializable {
    
    private static final long serialVersionUID = -2211593140752728872L;
    private Integer id;
    private String type;
    private String action;
    private String information;
    
    public PreviewEvent() {
    }
    
    public PreviewEvent(String type, String action, String information) {
        this.type = type;
        this.action = action;
        this.information = information;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "Type", nullable = false)
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name = "Action", nullable = false)
    public String getAction() {
        return this.action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    @Column(name = "Information", nullable = false)
    public String getInformation() {
        return this.information;
    }
    
    public void setInformation(String information) {
        this.information = information;
    }
}
