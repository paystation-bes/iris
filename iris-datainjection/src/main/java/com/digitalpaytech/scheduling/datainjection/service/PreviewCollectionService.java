package com.digitalpaytech.scheduling.datainjection.service;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewCollection;

public interface PreviewCollectionService {
    
    public PreviewCollection findPreviewCollectionById(Integer requestId);
}
