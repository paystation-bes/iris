package com.digitalpaytech.scheduling.datainjection.service;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewTransaction;

public interface PreviewTransactionService {
    
    public PreviewTransaction findPreviewTransactionById(Integer requestId);
}
