package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewDataInjection;
import com.digitalpaytech.scheduling.datainjection.service.PreviewDataInjectionService;

@Service("previewDataInjectionService")
public class PreviewDataInjectionServiceImpl implements PreviewDataInjectionService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    @Override
    public PreviewDataInjection findPreviewDataInjectionById(final Integer id) {
        final List<PreviewDataInjection> dataInjectionList = this.entityDao.findByNamedQueryAndNamedParam("PreviewDataInjection.findPreviewDataInjectionById",
                                                                                                          "id", id);
        if (dataInjectionList == null || dataInjectionList.isEmpty()) {
            return null;
        }
        return dataInjectionList.get(0);
    }
    
    @Override
    public void savePreviewDataInjection(final PreviewDataInjection previewDataInjection) {
        this.entityDao.save(previewDataInjection);
    }
}
