package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewRequest")
@NamedQueries({
        @NamedQuery(name = "PreviewRequest.findPreviewRequestByDayAndMinute", query = "SELECT pr FROM PreviewRequest pr where pr.dayOfWeek = :dayOfWeek and pr.minuteOfDay = :minuteOfDay"),
        @NamedQuery(name = "PreviewRequest.findUnsentPreviewRequestForSameDay", query = "SELECT pr FROM PreviewRequest pr where pr.dayOfWeek = :dayOfWeek and pr.minuteOfDay BETWEEN :lastRunMinuteOfDay and :minuteOfDay"),
        @NamedQuery(name = "PreviewRequest.findUnsentPreviewRequestForDifferentDay", query = "SELECT pr FROM PreviewRequest pr where (pr.dayOfWeek = :lastRunDayOfWeek and pr.minuteOfDay BETWEEN :lastRunMinuteOfDay and 1440) or (pr.dayOfWeek = :dayOfWeek and pr.minuteOfDay BETWEEN 0 and :minuteOfDay)"),
        @NamedQuery(name = "PreviewRequest.findUnsentPreviewRequestByDayAndMinuteRange", query = "SELECT pr FROM PreviewRequest pr where pr.dayOfWeek = :dayOfWeek and pr.minuteOfDay BETWEEN :minuteOfDayFirst and :minuteOfDayLast"),
        @NamedQuery(name = "PreviewRequest.findPreviewRequestByDay", query = "SELECT pr FROM PreviewRequest pr where pr.dayOfWeek = :dayOfWeek") })
public class PreviewRequest implements java.io.Serializable {
    
    private static final long serialVersionUID = 2396389858261029302L;
    private Integer id;
    private Integer dayOfWeek;
    private Integer minuteOfDay;
    private String paystationId;
    private PreviewRequestType previewRequestType;
    private Integer requestId;
    
    public PreviewRequest() {
    }
    
    public PreviewRequest(Integer dayOfWeek, Integer minuteOfDay, String paystationId, PreviewRequestType previewRequestType, Integer requestId) {
        this.dayOfWeek = dayOfWeek;
        this.minuteOfDay = minuteOfDay;
        this.paystationId = paystationId;
        this.previewRequestType = previewRequestType;
        this.requestId = requestId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "DayOfWeek", nullable = false)
    public Integer getDayOfWeek() {
        return this.dayOfWeek;
    }
    
    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
    
    @Column(name = "MinuteOfDay", nullable = false)
    public Integer getMinuteOfDay() {
        return this.minuteOfDay;
    }
    
    public void setMinuteOfDay(Integer minuteOfDay) {
        this.minuteOfDay = minuteOfDay;
    }
    
    @Column(name = "PaystationId", nullable = false)
    public String getPaystationId() {
        return this.paystationId;
    }
    
    public void setPaystationId(String paystationId) {
        this.paystationId = paystationId;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RequestTypeId", nullable = false)
    public PreviewRequestType getPreviewRequestType() {
        return this.previewRequestType;
    }
    
    public void setPreviewRequestType(PreviewRequestType previewRequestType) {
        this.previewRequestType = previewRequestType;
    }
    
    @Column(name = "RequestId", nullable = false)
    public Integer getRequestId() {
        return this.requestId;
    }
    
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
