package com.digitalpaytech.scheduling.datainjection.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewDataInjection")
@NamedQueries({ @NamedQuery(name = "PreviewDataInjection.findPreviewDataInjectionById", query = "SELECT pdi FROM PreviewDataInjection pdi where pdi.id = :id") })
public class PreviewDataInjection implements java.io.Serializable {
    
    private static final long serialVersionUID = -2848738354901474205L;
    private Integer id;
    private String name;
    private String value;
    private Date lastModifiedGmt;
    
    public PreviewDataInjection() {
    }
    
    public PreviewDataInjection(String name, String value, Date lastModifiedGmt) {
        this.name = name;
        this.value = value;
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "Name", nullable = false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name = "Value", nullable = false)
    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    @Column(name = "LastModifiedGmt", nullable = false)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
}
