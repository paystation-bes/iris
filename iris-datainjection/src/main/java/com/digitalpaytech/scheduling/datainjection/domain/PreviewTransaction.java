package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewTransaction")
@NamedQueries({ @NamedQuery(name = "PreviewTransaction.findPreviewTransactionById", query = "SELECT pt FROM PreviewTransaction pt where pt.id = :id") })
public class PreviewTransaction implements java.io.Serializable {
    
    private static final long serialVersionUID = -3688690274085012832L;
    private Integer id;
    private Integer number;
    private String lotNumber;
    private String licensePlateNo;
    private Integer addTimeNum;
    private Integer stallNumber;
    private String type;
    private Integer parkingTimePurchased;
    private Integer originalAmount;
    private Integer cashPaid;
    private Integer cardPaid;
    private Integer numberBillsAccepted;
    private Integer numberCoinsAccepted;
    private Integer billCol;
    private Integer coinCol;
    private String cardData;
    private boolean isRefundSlip;
    private String rateName;
    private Integer rateId;
    private Integer rateValue;
    
    public PreviewTransaction() {
    }
    
    public PreviewTransaction(Integer number, String lotNumber, String licensePlateNo, Integer addTimeNum, Integer stallNumber, String type,
            Integer parkingTimePurchased, Integer originalAmount, Integer cashPaid, Integer cardPaid, Integer numberBillsAccepted, Integer numberCoinsAccepted,
            Integer billCol, Integer coinCol, String cardData, boolean isRefundSlip, String rateName, Integer rateId, Integer rateValue) {
        this.number = number;
        this.lotNumber = lotNumber;
        this.licensePlateNo = licensePlateNo;
        this.addTimeNum = addTimeNum;
        this.stallNumber = stallNumber;
        this.type = type;
        this.parkingTimePurchased = parkingTimePurchased;
        this.originalAmount = originalAmount;
        this.cashPaid = cashPaid;
        this.cardPaid = cardPaid;
        this.numberBillsAccepted = numberBillsAccepted;
        this.numberCoinsAccepted = numberCoinsAccepted;
        this.billCol = billCol;
        this.coinCol = coinCol;
        this.cardData = cardData;
        this.isRefundSlip = isRefundSlip;
        this.rateName = rateName;
        this.rateId = number;
        this.rateValue = rateValue;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "AddTimeNum", nullable = false)
    public Integer getAddTimeNum() {
        return this.addTimeNum;
    }
    
    public void setAddTimeNum(Integer addTimeNum) {
        this.addTimeNum = addTimeNum;
    }
    
    @Column(name = "StallNumber", nullable = false)
    public Integer getStallNumber() {
        return this.stallNumber;
    }
    
    public void setStallNumber(Integer stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    @Column(name = "Type", nullable = false)
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name = "ParkingTimePurchased", nullable = false)
    public Integer getParkingTimePurchased() {
        return this.parkingTimePurchased;
    }
    
    public void setParkingTimePurchased(Integer parkingTimePurchased) {
        this.parkingTimePurchased = parkingTimePurchased;
    }
    
    @Column(name = "OriginalAmount", nullable = false)
    public Integer getOriginalAmount() {
        return this.originalAmount;
    }
    
    public void setOriginalAmount(Integer originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    @Column(name = "CashPaid", nullable = false)
    public Integer getCashPaid() {
        return this.cashPaid;
    }
    
    public void setCashPaid(Integer cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    @Column(name = "NumberBillsAccepted", nullable = false)
    public Integer getNumberBillsAccepted() {
        return this.numberBillsAccepted;
    }
    
    public void setNumberBillsAccepted(Integer numberBillsAccepted) {
        this.numberBillsAccepted = numberBillsAccepted;
    }
    
    @Column(name = "CardPaid", nullable = false)
    public Integer getCardPaid() {
        return this.cardPaid;
    }
    
    public void setCardPaid(Integer cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    @Column(name = "NumberCoinsAccepted", nullable = false)
    public Integer getNumberCoinsAccepted() {
        return this.numberCoinsAccepted;
    }
    
    public void setNumberCoinsAccepted(Integer numberCoinsAccepted) {
        this.numberCoinsAccepted = numberCoinsAccepted;
    }
    
    @Column(name = "BillCol", nullable = false)
    public Integer getBillCol() {
        return this.billCol;
    }
    
    public void setBillCol(Integer billCol) {
        this.billCol = billCol;
    }
    
    @Column(name = "CoinCol", nullable = false)
    public Integer getCoinCol() {
        return this.coinCol;
    }
    
    public void setCoinCol(Integer coinCol) {
        this.coinCol = coinCol;
    }
    
    @Column(name = "CardData")
    public String getCardData() {
        return this.cardData;
    }
    
    public void setCardData(String cardData) {
        this.cardData = cardData;
    }
    
    @Column(name = "IsRefundSlip", nullable = false)
    public boolean getIsRefundSlip() {
        return this.isRefundSlip;
    }
    
    public void setIsRefundSlip(boolean isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    @Column(name = "RateName", nullable = false)
    public String getRateName() {
        return this.rateName;
    }
    
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }
    
    @Column(name = "RateId", nullable = false)
    public Integer getRateId() {
        return this.rateId;
    }
    
    public void setRateId(Integer rateId) {
        this.rateId = rateId;
    }
    
    @Column(name = "Number", nullable = false)
    public Integer getNumber() {
        return this.number;
    }
    
    public void setNumber(Integer number) {
        this.number = number;
    }
    
    @Column(name = "LotNumber", nullable = false)
    public String getLotNumber() {
        return this.lotNumber;
    }
    
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    @Column(name = "LicensePlateNo", nullable = true, length = 15)
    public String getLicensePlateNo() {
        return this.licensePlateNo;
    }
    
    public void setLicensePlateNo(String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    @Column(name = "rateValue", nullable = false)
    public Integer getRateValue() {
        return this.rateValue;
    }
    
    public void setRateValue(Integer rateValue) {
        this.rateValue = rateValue;
    }
}
