package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewPaystation;
import com.digitalpaytech.scheduling.datainjection.service.PreviewPaystationService;

@Service("previewPaystationService")
public class PreviewPaystationServiceImpl implements PreviewPaystationService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    public List<PreviewPaystation> findPreviewPaystationByPaystationId(final String paystationId) {
        final List<PreviewPaystation> paystationList = this.entityDao.findByNamedQueryAndNamedParam("PreviewPaystation.findPreviewPaystationByPaystationId",
                                                                                                    new String[] { "paystationId" },
                                                                                                    new Object[] { paystationId });
        return paystationList;
    }
    
    @Override
    public void savePreviewPaystation(final PreviewPaystation previewPaystation) {
        this.entityDao.save(previewPaystation);
    }
}
