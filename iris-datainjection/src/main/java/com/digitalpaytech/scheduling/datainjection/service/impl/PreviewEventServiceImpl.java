package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewEvent;
import com.digitalpaytech.scheduling.datainjection.service.PreviewEventService;

@Service("previewEventService")
public class PreviewEventServiceImpl implements PreviewEventService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    public PreviewEvent findPreviewEventById(final Integer requestId) {
        final List<PreviewEvent> eventList = this.entityDao.findByNamedQueryAndNamedParam("PreviewEvent.findPreviewEventById", "requestId", requestId);
        if (eventList == null || eventList.isEmpty()) {
            return null;
        }
        return eventList.get(0);
    }
}
