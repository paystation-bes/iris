package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PreviewRequestType")
public class PreviewRequestType implements java.io.Serializable {
    
    private static final long serialVersionUID = 9075982716598423700L;
    private Integer id;
    private String name;
    
    public PreviewRequestType() {
    }
    
    public PreviewRequestType(String name) {
        this.name = name;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "Name", nullable = false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
