package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewRequest;
import com.digitalpaytech.scheduling.datainjection.service.PreviewRequestService;

@Service("previewRequestService")
public class PreviewRequestServiceImpl implements PreviewRequestService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    public List<PreviewRequest> findPreviewRequestByDayAndMinute(final Integer dayOfWeek, final Integer minuteOfDay) {
        final List<PreviewRequest> requestList = this.entityDao.findByNamedQueryAndNamedParam("PreviewRequest.findPreviewRequestByDayAndMinute", new String[] {
                "dayOfWeek", "minuteOfDay" }, new Object[] { dayOfWeek, minuteOfDay });
        return requestList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PreviewRequest> findUnsentPreviewRequestForDifferentDay(final Integer lastRunDayOfWeek, final Integer lastRunMinuteOfDay,
                                                                        final Integer dayOfWeek, final Integer minuteOfDay) {
        final List<PreviewRequest> requestList = this.entityDao.findByNamedQueryAndNamedParam("PreviewRequest.findUnsentPreviewRequestForDifferentDay",
                                                                                              new String[] { "lastRunDayOfWeek", "lastRunMinuteOfDay",
                                                                                                      "dayOfWeek", "minuteOfDay" }, new Object[] {
                                                                                                      lastRunDayOfWeek, lastRunMinuteOfDay, dayOfWeek,
                                                                                                      minuteOfDay });
        return requestList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PreviewRequest> findUnsentPreviewRequestByDayAndMinuteRange(final Integer dayOfWeek, final Integer minuteOfDayFirst,
                                                                            final Integer minuteOfDayLast) {
        final List<PreviewRequest> requestList = this.entityDao
                .findByNamedQueryAndNamedParam("PreviewRequest.findUnsentPreviewRequestByDayAndMinuteRange", new String[] { "dayOfWeek", "minuteOfDayFirst",
                        "minuteOfDayLast" }, new Object[] { dayOfWeek, minuteOfDayFirst, minuteOfDayLast });
        return requestList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PreviewRequest> findUnsentPreviewRequestForSameDay(final Integer dayOfWeek, final Integer lastRunMinuteOfDay, final Integer minuteOfDay) {
        final List<PreviewRequest> requestList = this.entityDao
                .findByNamedQueryAndNamedParam("PreviewRequest.findUnsentPreviewRequestForSameDay", new String[] { "dayOfWeek", "lastRunMinuteOfDay",
                        "minuteOfDay" }, new Object[] { dayOfWeek, lastRunMinuteOfDay, minuteOfDay });
        return requestList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PreviewRequest> findPreviewRequestByDayAndMinute(final Integer dayOfWeek) {
        final List<PreviewRequest> requestList = this.entityDao.findByNamedQueryAndNamedParam("PreviewRequest.findPreviewRequestByDay",
                                                                                              new String[] { "dayOfWeek" }, new Object[] { dayOfWeek });
        return requestList;
    }
}
