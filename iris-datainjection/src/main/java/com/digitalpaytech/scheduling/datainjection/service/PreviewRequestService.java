package com.digitalpaytech.scheduling.datainjection.service;

import java.util.List;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewRequest;

public interface PreviewRequestService {
    
    public List<PreviewRequest> findPreviewRequestByDayAndMinute(Integer dayOfWeek, Integer minuteOfDay);
    
    public List<PreviewRequest> findUnsentPreviewRequestForSameDay(Integer dayOfWeek, Integer lastRunMinuteOfDay, Integer minuteOfDay);
    
    public List<PreviewRequest> findUnsentPreviewRequestForDifferentDay(Integer lastRunDayOfWeek, Integer lastRunMinuteOfDay, Integer dayOfWeek,
                                                                         Integer minuteOfDay);
    
    public List<PreviewRequest> findPreviewRequestByDayAndMinute(Integer dayOfWeek);
    
    public List<PreviewRequest> findUnsentPreviewRequestByDayAndMinuteRange(Integer dayOfWeek, Integer minuteOfDayFirst, Integer minuteOfDayLast);
}
