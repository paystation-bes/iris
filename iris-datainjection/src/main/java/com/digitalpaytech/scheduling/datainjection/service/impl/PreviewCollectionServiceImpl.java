package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewCollection;
import com.digitalpaytech.scheduling.datainjection.service.PreviewCollectionService;

@Service("previewCollectionService")
public class PreviewCollectionServiceImpl implements PreviewCollectionService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    public PreviewCollection findPreviewCollectionById(final Integer id) {
        final List<PreviewCollection> collectionList = this.entityDao.findByNamedQueryAndNamedParam("PreviewCollection.findPreviewCollectionById", "id", id);
        if (collectionList == null || collectionList.isEmpty()) {
            return null;
        }
        return collectionList.get(0);
    }
}
