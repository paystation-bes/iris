package com.digitalpaytech.scheduling.datainjection.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.scheduling.datainjection.domain.PreviewTransaction;
import com.digitalpaytech.scheduling.datainjection.service.PreviewTransactionService;

@Service("previewTransactionService")
public class PreviewTransactionServiceImpl implements PreviewTransactionService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    public PreviewTransaction findPreviewTransactionById(final Integer id) {
        final List<PreviewTransaction> transactionList = this.entityDao
                .findByNamedQueryAndNamedParam("PreviewTransaction.findPreviewTransactionById", "id", id);
        if (transactionList == null || transactionList.isEmpty()) {
            return null;
        }
        return transactionList.get(0);
    }
}
