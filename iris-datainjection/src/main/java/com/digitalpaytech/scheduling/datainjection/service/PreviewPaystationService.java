package com.digitalpaytech.scheduling.datainjection.service;

import java.util.List;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewPaystation;

public interface PreviewPaystationService {
    
    public List<PreviewPaystation> findPreviewPaystationByPaystationId(String paystationId);
    
    public void savePreviewPaystation(PreviewPaystation previewPaystation);
}
