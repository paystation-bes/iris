package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewCollection")
@NamedQueries({ @NamedQuery(name = "PreviewCollection.findPreviewCollectionById", query = "SELECT pc FROM PreviewCollection pc where pc.id = :id") })
public class PreviewCollection implements java.io.Serializable {
    
    private static final long serialVersionUID = -6750080649707239741L;
    private Integer id;
    private String type;
    private String lotNumber;
    private String machineNumber;
    private Integer startDate;
    private Integer endDate;
    private Integer startTransNumber;
    private Integer endTransNumber;
    private Integer ticketCount;
    private Integer coinCount005;
    private Integer coinCount010;
    private Integer coinCount025;
    private Integer coinCount100;
    private Integer coinCount200;
    private Integer billCount01;
    private Integer billCount02;
    private Integer billCount05;
    private Integer billCount10;
    private Integer billCount20;
    private Integer billCount50;
    
    public PreviewCollection() {
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "Type")
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name = "LotNumber")
    public String getLotNumber() {
        return this.lotNumber;
    }
    
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    @Column(name = "MachineNumber")
    public String getMachineNumber() {
        return this.machineNumber;
    }
    
    public void setMachineNumber(String machineNumber) {
        this.machineNumber = machineNumber;
    }
    
    @Column(name = "StartDate", nullable = false)
    public Integer getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }
    
    @Column(name = "EndDate", nullable = false)
    public Integer getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }
    
    @Column(name = "StartTransNumber", nullable = false)
    public Integer getStartTransNumber() {
        return this.startTransNumber;
    }
    
    public void setStartTransNumber(Integer startTransNumber) {
        this.startTransNumber = startTransNumber;
    }
    
    @Column(name = "EndTransNumber", nullable = false)
    public Integer getEndTransNumber() {
        return this.endTransNumber;
    }
    
    public void setEndTransNumber(Integer endTransNumber) {
        this.endTransNumber = endTransNumber;
    }
    
    @Column(name = "TicketCount", nullable = false)
    public Integer getTicketCount() {
        return this.ticketCount;
    }
    
    public void setTicketCount(Integer ticketCount) {
        this.ticketCount = ticketCount;
    }
    
    @Column(name = "CoinCount005", nullable = false)
    public Integer getCoinCount005() {
        return this.coinCount005;
    }
    
    public void setCoinCount005(Integer coinCount005) {
        this.coinCount005 = coinCount005;
    }
    
    @Column(name = "CoinCount010", nullable = false)
    public Integer getCoinCount010() {
        return this.coinCount010;
    }
    
    public void setCoinCount010(Integer coinCount010) {
        this.coinCount010 = coinCount010;
    }
    
    @Column(name = "CoinCount025", nullable = false)
    public Integer getCoinCount025() {
        return this.coinCount025;
    }
    
    public void setCoinCount025(Integer coinCount025) {
        this.coinCount025 = coinCount025;
    }
    
    @Column(name = "CoinCount100", nullable = false)
    public Integer getCoinCount100() {
        return this.coinCount100;
    }
    
    public void setCoinCount100(Integer coinCount100) {
        this.coinCount100 = coinCount100;
    }
    
    @Column(name = "CoinCount200", nullable = false)
    public Integer getCoinCount200() {
        return this.coinCount200;
    }
    
    public void setCoinCount200(Integer coinCount200) {
        this.coinCount200 = coinCount200;
    }
    
    @Column(name = "BillCount01", nullable = false)
    public Integer getBillCount01() {
        return this.billCount01;
    }
    
    public void setBillCount01(Integer billCount01) {
        this.billCount01 = billCount01;
    }
    
    @Column(name = "BillCount02", nullable = false)
    public Integer getBillCount02() {
        return this.billCount02;
    }
    
    public void setBillCount02(Integer billCount02) {
        this.billCount02 = billCount02;
    }
    
    @Column(name = "BillCount05", nullable = false)
    public Integer getBillCount05() {
        return this.billCount05;
    }
    
    public void setBillCount05(Integer billCount05) {
        this.billCount05 = billCount05;
    }
    
    @Column(name = "BillCount10", nullable = false)
    public Integer getBillCount10() {
        return this.billCount10;
    }
    
    public void setBillCount10(Integer billCount10) {
        this.billCount10 = billCount10;
    }
    
    @Column(name = "BillCount20", nullable = false)
    public Integer getBillCount20() {
        return this.billCount20;
    }
    
    public void setBillCount20(Integer billCount20) {
        this.billCount20 = billCount20;
    }
    
    @Column(name = "BillCount50", nullable = false)
    public Integer getBillCount50() {
        return this.billCount50;
    }
    
    public void setBillCount50(Integer billCount50) {
        this.billCount50 = billCount50;
    }
}
