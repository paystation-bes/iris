package com.digitalpaytech.scheduling.datainjection;

public final class DataInjectionConstants {
    
    public static final String XMLRPC_PS2 = "/XMLRPC_PS2";
    
    // Preview_RequestType
    public static final int TRANSACTION = 1;
    public static final int EVENT = 2;
    public static final int COLLECTION = 3;
    
    // Preview_DataInjection
    public static final int LASTREQUESTSENT = 1;
    
    // Preview_Collection
    public static final String TYPE_COIN = "Coin";
    public static final String TYPE_BILL = "Bill";
    
    public static final String XML_OPENING_ELEMENTS = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
                                                      + "<methodCall><methodName>XMLRPCRequestHandler.processXML</methodName>"
                                                      + "<params><param><value><?xml version=\"1.0\"?>\n";
    public static final String XML_CLOSING_ELEMENTS = "</value></param></params></methodCall>";
    
    private DataInjectionConstants() {
        
    }
}
