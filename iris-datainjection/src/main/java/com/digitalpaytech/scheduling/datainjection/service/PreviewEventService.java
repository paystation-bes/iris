package com.digitalpaytech.scheduling.datainjection.service;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewEvent;

public interface PreviewEventService {
    
    public PreviewEvent findPreviewEventById(Integer requestId);
}
