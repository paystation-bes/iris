package com.digitalpaytech.scheduling.datainjection.service;

import com.digitalpaytech.scheduling.datainjection.domain.PreviewDataInjection;

public interface PreviewDataInjectionService {
    
    public PreviewDataInjection findPreviewDataInjectionById(Integer id);
    
    public void savePreviewDataInjection(PreviewDataInjection previewDataInjection);
}
