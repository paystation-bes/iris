package com.digitalpaytech.scheduling.datainjection.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PreviewPaystation")
@NamedQueries({ @NamedQuery(name = "PreviewPaystation.findPreviewPaystationByPaystationId", query = "FROM PreviewPaystation pp WHERE pp.paystationId = :paystationId") })
public class PreviewPaystation implements java.io.Serializable {
    
    private static final long serialVersionUID = 8750585473188992411L;
    private Integer id;
    private String serialNumber;
    private Integer customerId;
    private String paystationId;
    private String timeZone;
    private Integer messageNumber;
    
    public PreviewPaystation() {
    }
    
    public PreviewPaystation(String serialNumber, Integer customerId, String paystationId, String timeZone) {
        this.serialNumber = serialNumber;
        this.customerId = customerId;
        this.paystationId = paystationId;
        this.timeZone = timeZone;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "PaystationId", nullable = false)
    public String getPaystationId() {
        return this.paystationId;
    }
    
    public void setPaystationId(String paystationId) {
        this.paystationId = paystationId;
    }
    
    @Column(name = "CustomerId", nullable = false)
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    
    @Column(name = "SerialNumber", unique = true, nullable = false, length = 20)
    public String getSerialNumber() {
        return this.serialNumber;
    }
    
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    @Column(name = "TimeZone", nullable = false)
    public String getTimeZone() {
        return this.timeZone;
    }
    
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
    
    @Column(name = "MessageNumber")
    public Integer getMessageNumber() {
        return this.messageNumber;
    }
    
    public void setMessageNumber(Integer messageNumber) {
        this.messageNumber = messageNumber;
    }
    
}
