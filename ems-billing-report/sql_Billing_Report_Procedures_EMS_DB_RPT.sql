/*  --------------------------------------------------------------------------------------
    This code base reflects recent changes to the Billing Report as of July/August (but not actually implmented until October 19) 2012. 
    These changes include:
    o   trial customers with trial expiry date
        o  trial customers are not billable for monthly EMS services but are billable for Extend-by-Phone
    o   an additional sub-report that extracts data from EMS for consumption by the Warranty Database
        o  when paystations are activated a warranty (that may have been agreed to between DPT and the Customer) can come into effect
    -------------------------------------------------------------------------------------- */  

/*  --------------------------------------------------------------------------------------
    IMPORTANT:  This version prefixes all tables with ems_db.[tablename].
    
                This naming convention supports placing all Billing Report objects
                (tables and procedures) in a separate database (ems_db_rpt).
    -------------------------------------------------------------------------------------- */  
    
/*  --------------------------------------------------------------------------------------
    Script:     Billing_Report_Procedures
    
    Purpose:    Creates all stored procedures required by the Billing Report.
    
                Procedure:                              Purpose:
                ------------------------------------    ---------------------------------------------------------------------------------------------------------------------
                1.  sp_RunBillingReport                 Runs the Billing Report. Prepares all report data. Does not generate report output.
                2.  sp_BillingReportGetSub              The Billing Report is a group of sub-reports. This procedure outputs a specified sub-report.             
                3.  sp_BillingReportLoadLookupTables    Loads Billing Report lookup tables. Rows in these tables influence Billing Report logic. 
                4.  sp_StartBillingReport               Starts a new Billing Report (if previous report was Posted (finalized) or updates an existing non-Posted report.      
                5.  sp_BillingReportBillable            Determines which Paystations have had a 'billable status' change during the Billing Report begin/end date range.
                6.  sp_BillingReportActivation          Determines which Paystations have had a 'activation status' change during the Billing Report begin/end date range.
                7.  sp_BillingReportServices            Determines the list of billable services for each Customer on the Billing Report.
                8.  sp_BillingReportAPN                 Determines additional billing charges (if any) for Customer Paystations with a specific paystation modem APN setting.
                9.  sp_BillingReportConnection          For pre-EMS v6.3.8 only. Attempts to determine Paystation activation status by looking at Events and Transactions.
                10. sp_BillingReportAllPaystations      Prepares detailed information on one calendar years worth of Transactions for all Paystations. Used for verification.
                
    Note:       This script needs to be run only once. 
    
                Once this script has been run and the procedures supporting the Billing Report 
                have been created this script does not need to be run again.  
                
    Author:     Paul Worbey
    Date:       July 13 2011
    -------------------------------------------------------------------------------------- */

/* database for script */     
use ems_db_rpt;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportLoadLookupTables

    Call:       CALL sp_BillingReportLoadLookupTables;
    
                Parameters:
                This procedure takes no parameters;

    Tasks:      Populates (or re-populates) all lookup tables used by the Billing Report.
   
                These lookup tables are:        Number of rows:
                ----------------------------    ---------------
                1.  BillingReportListType       has  6 rows
                2.  BillingReportLogicType      has 20 rows
                3.  BillingReportListLogic      has 31 rows
                
                Rows in these tables are important for Billing Report processing and logic.
                A missing or inadvertently modified row will negatively impact how
                the Billing Report operates.
            
    Note:       This procedure must be called when the Billing Report is installed. 
    
                This procedure can be called any number of times. It simply refreshes
                all Billing Report lookup tables with their required rows. 
                
    Important:  Do NOT call this procedure while the Billing Report is running.
                
    --------------------------------------------------------------------------------------
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.
                
                SELECT * FROM BillingReportListType ORDER BY Id;
                
                SELECT * FROM BillingReportLogicType ORDER BY Id;
                
                SELECT * FROM BillingReportListLogic ORDER BY idBillingReportListType;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportLoadLookupTables;

delimiter //
CREATE PROCEDURE sp_BillingReportLoadLookupTables() 
BEGIN

    /* remove all rows from Billing Report lookup tables */
    DELETE FROM BillingReportListType;
    DELETE FROM BillingReportLogicType;
    DELETE FROM BillingReportListLogic;
    
    /* -------------------------------------------------------------------------------- */
    /* lookup table: BillingReportListType                                              */
    /* -------------------------------------------------------------------------------- */

    /* The first 5 are Billing Report sub-reports */
    INSERT BillingReportListType (Id,Name) VALUES (1,'All Billable Paystations');           
    INSERT BillingReportListType (Id,Name) VALUES (2,'All Non-Billable Paystations');
    INSERT BillingReportListType (Id,Name) VALUES (3,'New Billable Paystations');
    INSERT BillingReportListType (Id,Name) VALUES (4,'New Non-Billable Paystations');
    INSERT BillingReportListType (Id,Name) VALUES (5,'Paystation Changes');
    
    /* The error report is useful for debugging */
    INSERT BillingReportListType (Id,Name) VALUES (6,'Paystations Error');
    
    /* -------------------------------------------------------------------------------- */
    /* lookup table: BillingReportLogicType                                             */
    /* -------------------------------------------------------------------------------- */    

    /* these rows are for Paystations that exist in both the previous Billing Report and the current Billing Report */ 
    /* values for WasActivated, WasBillable, NowActivated, NowBillable: 1=no, 2=yes */
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (1 ,1,1,1,1,'Not Billable. Not Activated. No change.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (2 ,1,1,1,2,'Not Billable. Not Activated. BillingStatusType changed to Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (3 ,1,1,2,1,'Not Billable. Is now Activated. BillingStatusType remains Not Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (4 ,1,1,2,2,'Is now Billable. Is now Activated. Both Activated and Billable changed.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (5 ,1,2,1,1,'Not Billable. Not Activated. BillingStatusType changed to Not Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (6 ,1,2,1,2,'Not Billable. Not Activated. No change. BillingStatusType remains Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (7 ,1,2,2,1,'Not Billable. Is now Activated. BillingStatusType changed to Non-Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (8 ,1,2,2,2,'Is now Billable. Is now Activated. BillingStatusType remains Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (9 ,2,1,1,1,'Not Billable. Is now Not Activated. BillingStatusType remains Not Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (10,2,1,1,2,'Not Billable. Is now Not Activated. BillingStatusType changed to Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (11,2,1,2,1,'Not Billable. Activated. No change. Most likely billed quarterly, annually, or other arrangement.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (12,2,1,2,2,'Is now Billable. Activated. BillingStatusType changed to Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (13,2,2,1,1,'Is now Not Billable. Is now Not Activated.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (14,2,2,1,2,'Is now Not Billable. Is now Not Activated. BillingStatusType remains Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (15,2,2,2,1,'Is now Not Billable. Activated. BillingStatusType changed to Not Billable.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (16,2,2,2,2,'Billable. Activated. No change.');

    /* these rows are for Paystations in the current Billing Report but not in the Previous Billing report (new Paystations) */
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (17,0,0,1,1,'Not Billable. Not Activated. New Paystation.');   
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (18,0,0,1,2,'Not Billable. Not Activated. New Paystation.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (19,0,0,2,1,'Not Billable. Is now Activated. New Paystation.');
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (20,0,0,2,2,'Is now Billable. Is now Activated. New Paystation.');
    
    /* July 20 2012: this row is for inactive Customers. Inactive Customers do not appear on the Billing Report. */
    INSERT BillingReportLogicType (Id,WasActivated,WasBillable,NowActivated,NowBillable,Comment) VALUES (21,0,0,0,0,'Is not Billable. Is not Activated. Inactive Customer.');
       
    /* -------------------------------------------------------------------------------- */
    /* lookup table: BillingReportListLogic                                             */
    /* -------------------------------------------------------------------------------- */

    /* list of All Billable Paystations */
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 1, 4);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 1, 8);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 1,12);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 1,16);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 1,20);

    /* list of All Not Billable Paystations */
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 1);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 2);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 3);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 5);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 6);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 7);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2, 9);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,10);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,11);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,13);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,14);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,15);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,17);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,18);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 2,19);

    /* list of New Billable Paystations */
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 3, 4);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 3, 8);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 3,12);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 3,20);

    /* list of New Not Billable Paystations */
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,13);     
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,14);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,15);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,17);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,18);
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 4,19);

    /* error list of Paystations that could not be assigned to one of the Billing Report sub-reports */
    INSERT BillingReportListLogic (idBillingReportListType,idBillingReportLogicType) VALUES ( 6, 0);  

END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportGetSub
    
    Purpose:    The Billing Report is actually a group of sub-reports.
                This procedure retreives one specific sub-report from a Billing Report.
                Sub-reports 1 through 5 taken together comprise the full Billing Report.
                
                To retrieve a sub-report the Billing Report must already exist.
                This procedure does not create a Billing Report.
                Numerous other sub-reports are provided for testing and debugging.
                
    IMPORTANT:  In July 2012 the business / billing rules changed. 
                These changes impacted the Billing Report code base.
                
                When first designed the Billing Report simply reported on changes to 'activation'
                and 'billable' flags for a paystation. 
                
                Next, the Billing Report began including information on Bundled Data and Extend-by-Phone usage.
                
                To this point only 'active' Customers where included in the Billing Report.
                
                As of July 2012 The Billing Report was to include 'active' and 'trial' Customers.
                A 'trial' Customer does not appear in the 'All Billable' or 'New Billable' list of paystations.
                The introduction of 'trial' Customers breaks the original concept of 'lists'.
                
                Since the Billing Report runs against a legacy database rather than change the concept of 'lists'
                within the Billing Report additional logic has been added to procedure sp_BillingReportGetSub.
                Procedure sp_BillingReportGetSub generates the actual reports. 

    Call:       CALL sp_BillingReportGetSub (bdt,edt,list);
    
                Example call: 
                CALL sp_BillingReportGetSub ('2011-08-31','2011-09-14',1);
                CALL sp_BillingReportGetSub ('2011-06-28','2011-07-14',1);
                
                CALL sp_BillingReportGetSub ('2011-01-01','2011-09-14',1);
    
                Parameters:
                1.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                2.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                3.  list    Specifies a specific Billing Report sub-report

                The Billing Report is actually a series of Sub-Reports. These 5 sub-reports comprise
                the full Billing Report:
                1.  list=1  All Billable Paystations
                2.  list=2  All Non-Billable Paystations
                3.  list=3  New Billable Paystations
                4.  list=4  New Non-Billable Paystations
                5.  list=5  Billable Paystations whose data has changed since the last posted (finalized) Billing Report
                
                Output for each of these 5 sub-reports is written to individual output files.
                These output files are '/tmp/Billing_Report_1.csv' through '/tmp/Billing_Report_5.csv'.
                Ensure these output files do not exist before running these sub-reports.
                
                Other Sub-Reports support testing and debugging:
                1.  list=0  A summary of Paystation counts by BillingReportListType
                2.  list=6  An errors report showing Paystations that could not be assigned a BillingReportLogicType 
                            (meaning the billable status of these Paystations could not be determined)
                3.  list=7  Paystations whose APN = 'digitalpaytech.com' and number of  transactions in the 
                            month preceeding the Billing Report begin date exceed a threshold of 2000,
                            each transaction over 2000 is billed at $0.02 per transaction and/or
                            Extend-by-Phone transactions
                4.  list=8  A report of all Paystations that have changed since the last posted (finalized) Billing Report
                            Applies only to pre-EMS v6.3.8
                5.  list=11 A variant of All Billable Paystations: has information useful for testing and debugging
                6.  list=12 A variant of All Non-Billable Paystations: has information useful for testing and debugging
                7.  list=13 A variant of New Billable Paystations: has information useful for testing and debugging
                8.  list=14 A variant of New Non-Billable Paystations: has information useful for testing and debugging
                9.  list=15 A summary of Paystation counts by BillingReportLogicType
                
                Output for these other sub-reports is directed to standard out (i.e., computer monitor).
                
                --------------------------------------------------------------------------
      

   -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportGetSub;

delimiter //
CREATE PROCEDURE sp_BillingReportGetSub (IN bdt DATE, IN edt DATE, IN list TINYINT UNSIGNED)
BEGIN

    DECLARE idBR            INT UNSIGNED;
    DECLARE prvBR           INT UNSIGNED;
    DECLARE apnThreshold    INT UNSIGNED;
    DECLARE apnFee          INT UNSIGNED;
    DECLARE extendFee       INT UNSIGNED;
   
    /* apply a fee of $0.02 for every APN-'digitalpaytech.com'-Paystation transaction above a thresold of 2000 transactions */
    SET apnThreshold = 2000;    /* for testing: SET apnThreshold = 0; (to catch paystations tripping the threshold */
    SET apnFee = 2;
    SET extendFee = 25;
   
    /* Sub-Report 0: Summary by ListType, for testing and debugging */
    IF list=0 THEN
   
        SELECT L.Id AS 'List', L.Name AS 'List Name', COUNT(*) AS 'Paystations'
        FROM   BillingReportListType L, BillingReportListLogic G, BillingReportPaystation P
        WHERE  L.Id IN (1,2,3,4)
        AND    L.Id = G.idBillingReportListType
        AND    G.idBillingReportLogicType = P.idBillingReportLogicType
        AND    P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        GROUP BY L.Id ORDER BY L.Id;
        
    /* Sub-Report 1: All Billable Paystations */ 
    ELSEIF list = 1 THEN 
   
        SELECT  /*
                IFNULL(P.NavisionId,'')         AS 'Navision Id',
                IFNULL(P.BlanketOrder,'')       AS 'Blanket Order',
                IFNULL(P.BillCycle,'')          AS 'Bill Cycle',
                */
                P.Paystation                    AS 'Paystation', 
                IFNULL(P.ConnectionDate,'')     AS 'Connection Date',
                P.CustomerName                  AS 'Customer',
                P.ServicesInUse                 AS 'Services',
                P.ServiceRealTimeCC             AS 'Real Time CC',
                P.ServiceReports                AS 'Reports',
                P.ServiceTextAlerts             AS 'Text Alerts',
                P.ServiceCoupon                 AS 'Coupons',
                P.ServiceValueCard              AS 'Value Card',
                P.ServiceWebServices            AS 'Web Services',
                P.ServicePayByCell              AS 'Pay by Phone',
                P.ServiceExtendByCell           AS 'Extend by Phone'
                /*
                IFNULL(P.Note,'')               AS 'Note',       
                IF(P.APNTransactions>apnThreshold,P.APNTransactions-apnThreshold,'') AS 'APN Over',
                IF(P.APNTransactions>apnThreshold,FORMAT(((P.APNTransactions-apnThreshold)*apnFee)/100,2),'') AS 'APN Charge',
                IF(P.ExtendTransactions>0,P.ExtendTransactions,'') AS 'Extend Count',
                IF(P.ExtendTransactions>0,FORMAT((P.ExtendTransactions*extendFee)/100,2),'') AS 'Extend Charge'  
                */
        /*
        INTO OUTFILE 'S:\PaulW\Billing_Report_1.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        */
        INTO OUTFILE 'C:\\Billing_Report_1.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        FROM    BillingReportPaystation P, BillingReportListLogic L
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     P.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType = list
        ORDER BY P.CustomerName, P.Paystation;
        
    /* Sub-Report 2: All Non-Billable Paystations */ 
    ELSEIF list = 2 THEN 
   
        SELECT  P.Paystation                    AS 'Paystation', 
                IFNULL(P.ConnectionDate,'')     AS 'Connection Date',
                P.CustomerName                  AS 'Customer',
                P.ServicesInUse                 AS 'Services',
                P.ServiceRealTimeCC             AS 'Real Time CC',
                P.ServiceReports                AS 'Reports',
                P.ServiceTextAlerts             AS 'Text Alerts',
                P.ServiceCoupon                 AS 'Coupons',
                P.ServiceValueCard              AS 'Value Card',
                P.ServiceWebServices            AS 'Web Services',
                P.ServicePayByCell              AS 'Pay by Phone',
                P.ServiceExtendByCell           AS 'Extend by Phone'    
        /*
        INTO OUTFILE 'S:\PaulW\Billing_Report_2.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        */
        INTO OUTFILE 'C:\\Billing_Report_2.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        FROM    BillingReportPaystation P, BillingReportListLogic L
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     P.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType = list
        ORDER BY P.CustomerName, P.Paystation;
        
    /* Sub-Report 3: New Billable Paystations */ 
    ELSEIF list = 3 THEN 
   
        SELECT  P.Paystation                    AS 'Paystation', 
                IFNULL(P.ConnectionDate,'')     AS 'Connection Date',
                P.CustomerName                  AS 'Customer',
                P.ServicesInUse                 AS 'Services',
                P.ServiceRealTimeCC             AS 'Real Time CC',
                P.ServiceReports                AS 'Reports',
                P.ServiceTextAlerts             AS 'Text Alerts',
                P.ServiceCoupon                 AS 'Coupons',
                P.ServiceValueCard              AS 'Value Card',
                P.ServiceWebServices            AS 'Web Services',
                P.ServicePayByCell              AS 'Pay by Phone',
                P.ServiceExtendByCell           AS 'Extend by Phone'  
        /*
        INTO OUTFILE 'S:\PaulW\Billing_Report_3.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        */
        INTO OUTFILE 'C:\\Billing_Report_3.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        FROM    BillingReportPaystation P, BillingReportListLogic L
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     P.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType = list
        ORDER BY P.CustomerName, P.Paystation;
        
    /* Sub-Report 4: New Non-Billable Paystations */ 
    ELSEIF list = 4 THEN 
   
        SELECT  P.Paystation                    AS 'Paystation', 
                IFNULL(P.ConnectionDate,'')     AS 'Connection Date',
                P.CustomerName                  AS 'Customer',
                P.ServicesInUse                 AS 'Services',
                P.ServiceRealTimeCC             AS 'Real Time CC',
                P.ServiceReports                AS 'Reports',
                P.ServiceTextAlerts             AS 'Text Alerts',
                P.ServiceCoupon                 AS 'Coupons',
                P.ServiceValueCard              AS 'Value Card',
                P.ServiceWebServices            AS 'Web Services',
                P.ServicePayByCell              AS 'Pay by Phone',
                P.ServiceExtendByCell           AS 'Extend by Phone'    
        /*
        INTO OUTFILE 'S:\PaulW\Billing_Report_4.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        */
        INTO OUTFILE 'C:\\Billing_Report_4.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        FROM    BillingReportPaystation P, BillingReportListLogic L
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     P.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType = list
        ORDER BY P.CustomerName, P.Paystation;
          
    /* Sub-Report 5: Paystation Changes */
    /* this 'changes' sub-report looks for any changes to a Paystation between the current Billing Report and the previous Billing Report */
    ELSEIF list=5 THEN
   
        /* find requested Billing Report and the Billing Report previous to the requested Billing Report */
        SELECT  Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt INTO idBR;
        SELECT  Id FROM BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
      
        SELECT  IF(STRCMP(C.Paystation,P.Paystation)=0,C.Paystation,CONCAT('* ',C.Paystation,' *'))             AS 'Paystation',  
                IFNULL(P.ConnectionDate,'')                                                                     AS 'Connection Date',
                IF(STRCMP(C.CustomerName,P.CustomerName)=0,C.CustomerName,CONCAT('* ',C.CustomerName,' *'))     AS 'Customer',
                IF(C.ServicesInUse=P.ServicesInUse,C.ServicesInUse,CONCAT('* ',C.ServicesInUse,' *'))           AS 'Services',
                /* --------------------------------------------------------------------------------------------- */
                /* What does this messy code do?                                                                 */
                /* Pseudo code: IF(A=B,THEN 'A',ELSEIF(Length(A)>0 AND Length(B)=0,THEN '* A *',ELSEIF(Length(B)>0 AND Length(A)=0,THEN '* Off *',ELSE '?' */
                /* Meaning:                 no change                                    A is new (put it in asteriks)                   A is turned off (denote as 'off'), otherwise denote error using a question mark */
                /* --------------------------------------------------------------------------------------------- */
                IF(STRCMP(C.ServiceRealTimeCC,P.ServiceRealTimeCC)=0,C.ServiceRealTimeCC,IF(LENGTH(C.ServiceRealTimeCC)>0 AND LENGTH(P.ServiceRealTimeCC)=0,CONCAT('* ',C.ServiceRealTimeCC,' *'),IF(LENGTH(P.ServiceRealTimeCC)>0 AND LENGTH(C.ServiceRealTimeCC)=0,'* Off *','?')))                     AS 'Real Time CC',             
                IF(STRCMP(C.ServiceReports,P.ServiceReports)=0,C.ServiceReports,IF(LENGTH(C.ServiceReports)>0 AND LENGTH(P.ServiceReports)=0,CONCAT('* ',C.ServiceReports,' *'),IF(LENGTH(P.ServiceReports)>0 AND LENGTH(C.ServiceReports)=0,'* Off *','?')))                                             AS 'Reports', 
                IF(STRCMP(C.ServiceTextAlerts,P.ServiceTextAlerts)=0,C.ServiceTextAlerts,IF(LENGTH(C.ServiceTextAlerts)>0 AND LENGTH(P.ServiceTextAlerts)=0,CONCAT('* ',C.ServiceTextAlerts,' *'),IF(LENGTH(P.ServiceTextAlerts)>0 AND LENGTH(C.ServiceTextAlerts)=0,'* Off *','?')))                     AS 'Text Alerts',             
                IF(STRCMP(C.ServiceCoupon,P.ServiceCoupon)=0,C.ServiceCoupon,IF(LENGTH(C.ServiceCoupon)>0 AND LENGTH(P.ServiceCoupon)=0,CONCAT('* ',C.ServiceCoupon,' *'),IF(LENGTH(P.ServiceCoupon)>0 AND LENGTH(C.ServiceCoupon)=0,'* Off *','?')))                                                     AS 'Coupon',
                IF(STRCMP(C.ServiceValueCard,P.ServiceValueCard)=0,C.ServiceValueCard,IF(LENGTH(C.ServiceValueCard)>0 AND LENGTH(P.ServiceValueCard)=0,CONCAT('* ',C.ServiceValueCard,' *'),IF(LENGTH(P.ServiceValueCard)>0 AND LENGTH(C.ServiceValueCard)=0,'* Off *','?')))                             AS 'Value Card',
                IF(STRCMP(C.ServiceWebServices,P.ServiceWebServices)=0,C.ServiceWebServices,IF(LENGTH(C.ServiceWebServices)>0 AND LENGTH(P.ServiceWebServices)=0,CONCAT('* ',C.ServiceWebServices,' *'),IF(LENGTH(P.ServiceWebServices)>0 AND LENGTH(C.ServiceWebServices)=0,'* Off *','?')))             AS 'Web Services',             
                IF(STRCMP(C.ServicePayByCell,P.ServicePayByCell)=0,C.ServicePayByCell,IF(LENGTH(C.ServicePayByCell)>0 AND LENGTH(P.ServicePayByCell)=0,CONCAT('* ',C.ServicePayByCell,' *'),IF(LENGTH(P.ServicePayByCell)>0 AND LENGTH(C.ServicePayByCell)=0,'* Off *','?')))                             AS 'Pay by Phone',             
                IF(STRCMP(C.ServiceExtendByCell,P.ServiceExtendByCell)=0,C.ServiceExtendByCell,IF(LENGTH(C.ServiceExtendByCell)>0 AND LENGTH(P.ServiceExtendByCell)=0,CONCAT('* ',C.ServiceExtendByCell,' *'),IF(LENGTH(P.ServiceExtendByCell)>0 AND LENGTH(C.ServiceExtendByCell)=0,'* Off *','?')))     AS 'Extend by Phone'
        /*
        INTO OUTFILE 'S:\PaulW\Billing_Report_5.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'  
        */
        INTO OUTFILE 'C:\\Billing_Report_5.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'        
        FROM    BillingReportPaystation C, BillingReportPaystation P, BillingReportListLogic L
        WHERE   C.idBillingReport = idBR
        AND     P.idBillingReport = prvBR
        AND     C.idPaystation = P.idPaystation
        AND     C.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType = 1   /* restrict to All Billable Paystations as per Billing Report requirements */
        AND     (
                    STRCMP(C.CustomerName,P.CustomerName)!=0 OR 
                    STRCMP(C.Paystation,P.Paystation)!=0 OR
                    C.ServicesInUse!=P.ServicesInUse OR
                    STRCMP(C.ServiceRealTimeCC,P.ServiceRealTimeCC)!=0 OR
                    STRCMP(C.ServiceReports,P.ServiceReports)!=0 OR 
                    STRCMP(C.ServiceTextAlerts,P.ServiceTextAlerts)!=0 OR
                    STRCMP(C.ServiceCoupon,P.ServiceCoupon)!=0 OR 
                    STRCMP(C.ServiceValueCard,P.ServiceValueCard)!=0 OR 
                    STRCMP(C.ServiceWebServices,P.ServiceWebServices)!=0 OR 
                    STRCMP(C.ServicePayByCell,P.ServicePayByCell)!=0 OR 
                    STRCMP(C.ServiceExtendByCell,P.ServiceExtendByCell)!=0
                )
        ORDER BY C.CustomerName, C.Paystation;
        
    /* Sub-Report 6: Paystation Errors, the billable status of these Paystations could not be determined */
    ELSEIF list=6 THEN
   
        SELECT  CustomerName                    AS Customer, 
                Paystation                      AS Paystation,  /* use the following abbreviations to economize on output width */
                IFNULL(ConnectionDate,'')       AS 'C Dt',      /* Connection Date */ 
                WasActivated                    AS 'pA',        /* Previously Activated */
                WasBillable                     AS 'pB',        /* Previously Billable */
                NowActivated                    AS 'cA',        /* Currently Activated */
                NowBillable                     AS 'cB',        /* Currently Billable */
                IFNULL(ActivationChangeGMT,'')  AS 'A Dt',      /* Activation Change Date */
                IFNULL(BillableChangeGMT,'')    AS 'B Dt',      /* Billable Status Change Date */
                idCustomer                      AS 'eC',        /* ems_db CustomerId */
                idPaystation                    AS 'ePS',       /* ems_db PaystationId */
                idCustomer2                     AS 'cC',        /* cerberus_db CustomerId */
                idPaystation2                   AS 'cPS'        /* cerberus_db PaystationId */
                                                                /* include or exclude this section as needed, it directs output to a file */
                /*
                INTO OUTFILE '/tmp/Billing_Report_6.csv' 
                FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
                LINES TERMINATED BY '\n'   
                */
        FROM    BillingReportPaystation
        WHERE   idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     idBillingReportLogicType = 0    /* billable status of Paystation could not be determined */
        ORDER BY CustomerName, Paystation;
      
    /* Sub-Report 7: APN Transactions Above Threshold and/or Extend Transactions */
    ELSEIF list=7 THEN   
   
        SELECT  P.Paystation                                                                                    AS 'Paystation',
                P.CustomerName                                                                                  AS 'Customer',
                IF(P.APNTransactions>0,P.APNTransactions,'')                                                    AS 'APN Count',                
                IF(P.APNTransactions>apnThreshold,P.APNTransactions-apnThreshold,'')                            AS 'APN Over',
                IF(P.APNTransactions>apnThreshold,FORMAT(((P.APNTransactions-apnThreshold)*apnFee)/100,2),'')   AS 'APN Charge',
                IF(P.ExtendTransactions>0,P.ExtendTransactions,'')                                              AS 'Extend Count',
                IF(P.ExtendTransactions>0,FORMAT((P.ExtendTransactions*extendFee)/100,2),'')                    AS 'Extend Charge'
        INTO OUTFILE 'C:\\Billing_Report_7.csv' 
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
        FROM    BillingReportPaystation P
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND    (P.APNTransactions > 0 OR P.ExtendTransactions > 0 OR P.APN IS NOT NULL)
        ORDER BY P.CustomerName, P.Paystation;
      
    /* Sub-Report 8: Paystation changes for the pre-EMS v6.3.8 version of the Billing Report */
    ELSEIF list=8 THEN
   
        SELECT  CustomerName                    AS Customer, 
                Paystation                      AS Paystation, 
                IFNULL(DATE(ConnectionDate),'') AS 'C Dt', 
                WasActivated                    AS 'pA', 
                WasBillable                     AS 'pB', 
                NowActivated                    AS 'cA', 
                NowBillable                     AS 'cB', 
                IFNULL(Transactions1to5,'')     AS 't5', 
                IFNULL(Transactions6to10,'')    AS 't10', 
                IFNULL(Transactions11to15,'')   AS 't15',
                idCustomer                      AS 'eC', 
                idPaystation                    AS 'ePS', 
                idCustomer2                     AS 'cC', 
                idPaystation2                   AS 'cPS',  
                IFNULL(ActivityType,'')         AS 'Evn'
                                                                /* include or exclude this section as needed, it directs output to a file */
                /*
                INTO OUTFILE '/tmp/Billing_Report_8.csv' 
                FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
                LINES TERMINATED BY '\n'   
                */
        FROM    BillingReportPaystation
        WHERE   idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     HasChanged = 1
        ORDER BY CustomerName, Paystation;
     
   /* Sub-Reports 11 through 14: these are developer/debugging versions of Sub-Reports 1 through 4. 11 = All Billable, 12 = All Non-Billable, 13 = New Billable, 14 = New Non-Billable */
   ELSEIF list>= 11 AND list<= 14 THEN
   
        SELECT  P.CustomerName                  AS 'Customer',
                P.Paystation                    AS 'Paystation', 
                IFNULL(P.ConnectionDate,'')     AS 'C Dt',
                P.WasActivated                  AS 'pA',
                P.WasBillable                   AS 'pB',
                P.NowActivated                  AS 'cA',
                P.NowBillable                   AS 'cB',
                IFNULL(P.ActivationChangeGMT,'')AS 'A Dt',
                IFNULL(P.BillableChangeGMT,'')  AS 'B Dt',
                P.idBillingReportLogicType      AS 'Logic',
                T.Comment                       AS 'Billable Status',
                P.idCustomer                    AS 'eC',
                P.idPaystation                  AS 'ePS',
                P.idCustomer2                   AS 'cC',
                P.idPaystation2                 AS 'cPS'
        FROM    BillingReportPaystation P, BillingReportListLogic L, BillingReportLogicType T
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt)
        AND     P.idBillingReportLogicType = L.idBillingReportLogicType
        AND     L.idBillingReportListType  = list-10    /* list-10: this code dependent on the 'ELSEIF list>= 11 AND list<= 14 THEN' statement just above */
        AND     L.idBillingReportLogicType = T.Id
        ORDER BY P.CustomerName, P.Paystation;
        
    /* Sub-Report 15: Summary by LogicType, for testing and debugging */
    ELSEIF list=15 THEN
 
        SELECT  P.idBillingReportLogicType AS idLogic, COUNT(*) AS Paystations, L.Comment AS 'Current Billable Status', 
                IF(L.WasActivated=2,'Yes','No') AS 'Was Activated', IF(L.WasBillable=2,'Yes','No') AS 'Was Billable', IF(L.NowActivated=2,'Yes','No') AS 'Now Activated', If(L.NowBillable=2,'Yes','No') AS 'Now Billable'
        FROM    BillingReportPaystation P, BillingReportLogicType L
        WHERE   P.idBillingReport = (SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt) 
        AND     P.idBillingReportLogicType = L.Id
        GROUP BY P.idBillingReportLogicType 
        ORDER BY P.idBillingReportLogicType;
            
    END IF;
END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_StartBillingReport 
    
    Purpose:    Starts a new run of the Billing Report.
                This could create new Billing Report or re-run an existing interim Billing Report
                (a non-posted or non-finalized report) to pickup any changes (Billable, Activation or other) 
                that have occurred since the last iteration of the interim report.

    Call:       CALL sp_StartBillingReport (task, idBR, bdt, edt, trace);
   
                Example call:
                CALL sp_StartBillingReport (0, 0, '2011-06-28','2011-07-14');
                
                Parameters:
                1.  task    INOUT parameter, assigned by this procedure,
                            where task = 1 = new-billing-report, and task = 2 = re-run-existing-billing-report
                            any other value for task is an error (most likely related to an incorrect begin or end date)
                2.  idBR    INOUT parameter, assigned by this procedure,
                            the BillingReport.Id of the Billing Report that was newly created or is being re-run
                3.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                4.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                5.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.

                SELECT * FROM BillingReport;
                
                SELECT COUNT(*) FROM BillingReportPaystation;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_StartBillingReport;

delimiter //
CREATE PROCEDURE sp_StartBillingReport (INOUT task TINYINT UNSIGNED, INOUT idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    /* prvbdt & prvedt: the begin and end dates of the most recently posted (i.e., previous) Billing Report */
    DECLARE prvbdt DATE;
    DECLARE prvedt DATE;
    DECLARE prvBR  INT UNSIGNED;
    
    /* get info from the most recently posted Billing Report */
    SELECT Id, DateRangeBegin, DateRangeEnd INTO prvBR, prvbdt, prvedt
    FROM   BillingReport 
    WHERE  Id = (SELECT MAX(Id) FROM BillingReport WHERE IsPosted=1);
   
    /* error check: report end date must be greater than begin date */
    IF bdt > edt THEN SET task = 4; END IF;
                      
    /* check for very first Billing Report (no other Billing Report exists) */
    SELECT IF((SELECT COUNT(*) FROM BillingReport)=0 AND task=0,1,task) INTO task;

    /* determine if a new Billing Report is to be created (the previous Billing Report was posted (closed) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=1 AND task=0,1,task) INTO task;
     
    /* if starting a new Billing Report validate the report's begin and end dates */
    IF task=1 THEN      
        /* the begin date for the next report MUST be the day after the end date from the previous report */
        SELECT IF(ADDDATE(prvedt,INTERVAL 1 DAY)!=bdt,5,task) INTO task;
    END IF;
   
    /* existing Billing Report (existing Billing Report has yet to be posted) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport))=0 AND task=0,3,task) INTO task;

    /* validate dates for existing report (a continuation of the line directly above) */
    SELECT IF((SELECT IsPosted FROM BillingReport WHERE Id=(SELECT MAX(Id) FROM BillingReport) AND DateRangeBegin=bdt AND DateRangeEnd=edt)=0 AND task=3,2,task) INTO task;

    /* task has been assigned a value, examine this value */
    CASE task
        /* task=0 error, task not determined */
        WHEN 0 THEN
        BEGIN
            SELECT 'ERROR. Could not determine whether to create a new Billing Report or update an existing Billing Report.' AS ErrorMessage;
        END;

        /* task=1: create new Billing Report */
        WHEN 1 THEN
        BEGIN
            INSERT BillingReport (DateRangeBegin,DateRangeEnd,RunBegin,RunEnd,IsPosted,IsError) VALUES (bdt,edt,UTC_TIMESTAMP(),UTC_TIMESTAMP(),0,0);
            SET idBR = last_insert_id();
        END;

        /* task=2: update existing Billing Report */
        WHEN 2 THEN
        BEGIN
            SELECT Id FROM BillingReport WHERE DateRangeBegin=bdt AND DateRangeEnd=edt AND IsPosted=0 AND Id=(SELECT MAX(Id) FROM BillingReport) INTO idBR;
            UPDATE BillingReport SET RunBegin=UTC_TIMESTAMP(), RunEnd=UTC_TIMESTAMP(), IsError=0 WHERE Id=idBR;
            DELETE FROM BillingReportPaystation WHERE idBillingReport=idBR;
        END;

        /* task=3: error, invalid dates for existing report: the begin and end dates specified must match the begin and end dates for the current non-posted Billing Report */
        WHEN 3 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. Parameter dates do not match current Billing Report dates: ',bdt,' and ',edt,' do not match ',DateRangeBegin,' and ',DateRangeEnd) AS ErrorMessage
            FROM   BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport);
        END;
         
        /* task=4: error, invalid dates, end date must be greater than begin date */
        WHEN 4 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' cannot be greater than the report end date ',edt) AS ErrorMessage;
        END;
         
         /* task=5: error, the begin date of a new report must be the day after the nend date of the old report */
        WHEN 5 THEN
        BEGIN
            SELECT concat('ERROR. Invalid Billing Report dates. The report begin date ',bdt,' must be the next day following the previous reports end date ',prvedt) AS ErrorMessage;
        END;

        /* task>5: error */
        ELSE
        BEGIN
            SELECT 'ERROR. Could not resolve Billing Report parameters.' AS ErrorMessage;
        END;
    END CASE;
    
    /* add Paystations to Billing Report */
    IF task=1 OR task=2 THEN    
        
        /* create master list of Paystations for Billing Report  */
        INSERT  BillingReportPaystation (idBillingReport,idCustomer,idPaystation,idCustomer2,idPaystation2,CustomerName,Paystation,CerberusCustomerStatus,CerberusCustomerTrialExpiry,CerberusCustomerEnabled,CerberusPsCreationDate,CerberusPsIsDeleted,EmsCustomerName,EmsCustomerAccountStatus,EmsPsIsActive,EmsPsLockState,EmsPsDeleteFlag,EmsPsName,EmsPsLastHeartBeat,EmsPsLocationName,ProvisionDate)
        SELECT  B.Id, C.Id, P.Id, C2.Id, P2.Id, C2.CompanyName, P.CommAddress, C2.Status, C2.Expiry, C2.Enabled, P2.CreationDate, P2.IsDeleted, C.Name, C.AccountStatus, P.IsActive, P.LockState, P.DeleteFlag, P.Name, H.LastHeartBeatGMT, R.Name, P2.CreationDate
        FROM    BillingReport B, cerberus_db.Customer C2, cerberus_db.Paystation P2, ems_db.Customer C, ems_db.Paystation P, ems_db.Region R, ems_db.PaystationHeartBeat H
        WHERE   B.Id = idBR
        AND     C2.ServerId = 1                 
     /* AND     C2.Status = 0                              REMOVED TUESDAY, JULY 24, 2012 */
        AND     P2.IsDeleted = 0
        AND     C2.EmsCustomerId = C.Id  
        AND     UPPER(P.CommAddress) NOT LIKE '%X%'     /* no no-longer-in-service paystations */
        AND     P.CommAddress NOT LIKE '9%'             /* no 3rd party paystations */
        AND     P.CommAddress NOT LIKE '8%'             /* no DPT paystations */ 
     /* AND     P.CommAddress NOT LIKE '5%'                no test paystations: REMOVED THURSDAY, NOVEMBER 24, 2011 */
        AND     UPPER(CommAddress) NOT LIKE '%VER%'     /* no Verrus paystations */
        AND     C2.Id = P2.CustomerId
        AND     C.Id = P.CustomerId
        AND     P2.SerialNumber = P.CommAddress 
        AND     C.Id = R.CustomerId
        AND     R.Id = P.RegionId
        AND     P.Id = H.PaystationId
     /* AND     C2.Id IN (144071,145634,138241,137851,143301)   limited set of Customers for a test run */
        ORDER BY C2.CompanyName, P.CommAddress;
        
        /* Get the Lot Setting for each Paystation (not every paystation has a lot setting) */
        UPDATE  BillingReportPaystation R
                INNER JOIN ems_db.Paystation P ON P.Id = R.idPaystation
                LEFT JOIN ems_db.LotSetting L ON L.Id = P.LotSettingId
        SET     R.EmsPsLotSettingName = IFNULL(L.Name,'n/a')
        WHERE   R.idBillingReport = idBR;
        
        /* set WasActivated & WasBillable based on NowActivated & NowBillable from the previous Billing Report (carry forward Activation and Billable status from the previous Billing Report) */
        /* note: this join will fail for new Paystations that are in the new Billing Report but not in the previous Billing Report, their WasActivated and WasBillable will remain as zeros */
        /* also carry forward ConnectionDate */
        UPDATE  BillingReportPaystation P, BillingReportPaystation P2
        SET     P.WasActivated = P2.NowActivated,
                P.WasBillable  = P2.NowBillable,
                P.ConnectionDate = P2.ConnectionDate,
                P.ActivationChangeGMT = P2.ActivationChangeGMT,
                P.BillableChangeGMT = P2.BillableChangeGMT
        WHERE   P.idBillingReport  = idBR
        AND     P2.idBillingReport = prvBR
        AND     P.idPaystation = P2.idPaystation;  
    END IF;
 
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_StartBillingReport' AS Trace;
    END IF;
    
END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportBillable 
    
    Important:  This procedure is for EMS v6.3.8 and later database schema.
    
    Purpose:    Determines whether or not the Billable status of any Paystation on a Billing Report 
                has changed within the Billing Report run dates. 

    Call:       CALL sp_BillingReportBillable (idBR, bdt, edt, trace);
   
                Example call:
                CALL sp_BillingReportBillable (12,'2011-06-28','2011-07-14');
                
                Parameters:
                1.  idBR    BillingReport.Id
                2.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                3.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                4.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.

                SELECT  idPaystation2 FROM BillingReportPaystation WHERE idBillingReport = 12 AND WasBillable = 1 LIMIT 5;
                SELECT  idPaystation2 FROM BillingReportPaystation WHERE idBillingReport = 12 AND WasBillable = 2 LIMIT 5;
      
                INSERT  cerberus_db.BillingStatusHistory (idPaystation,ChangeGMT,idBillingStatusType,idAccount,Comment)
                VALUES  (1481,UTC_TIMESTAMP(),2,121,'Update paystation');
      
                INSERT  cerberus_db.BillingStatusHistory (idPaystation,ChangeGMT,idBillingStatusType,idAccount,Comment)
                VALUES  (251,UTC_TIMESTAMP(),1,121,'Update paystation');
              
                SELECT  idBillingReport AS idBR, WasActivated, WasBillable, NowActivated, NowBillable, COUNT(*)
                FROM    BillingReportPaystation
                WHERE   idBillingReport IN (12)
                GROUP BY idBillingReport, WasActivated, WasBillable, NowActivated, NowBillable
                ORDER BY idBillingReport, WasActivated, WasBillable, NowActivated, NowBillable;  
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportBillable;

delimiter //
CREATE PROCEDURE sp_BillingReportBillable (IN idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    /* table for recent changes in Paystation billable status (if any) */
    DROP TEMPORARY TABLE IF EXISTS `brBillable`;
    CREATE TEMPORARY TABLE `brBillable` (
        idPaystation2               INT UNSIGNED        NOT NULL,
        ChangeGMT                   DATETIME            NOT NULL,
        NowBillable                 TINYINT UNSIGNED    NOT NULL,
        /* indexes */
        PRIMARY KEY (`idPaystation2`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;  
     
    /* get the most recent billable status change that occurred within the Billing Report run dates for every Paystation on the Billing Report */
    /* there will be relatively few changes; note: this is a correlated subquery */
    /* note: BillingStatusHistory.idBillingStatusType: 1=non-billable, 2=billable */ 
    INSERT  brBillable (idPaystation2,ChangeGMT,NowBillable)
    SELECT  H.idPaystation, H.ChangeGMT, H.idBillingStatusType
    FROM    cerberus_db.BillingStatusHistory H, BillingReportPaystation P
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation2 = H.idPaystation
    AND     H.ChangeGMT >= bdt AND H.ChangeGMT < ADDDATE(edt,INTERVAL 1 DAY)
    AND     H.ChangeGMT = (
            SELECT  MAX(H2.ChangeGMT) 
            FROM    cerberus_db.BillingStatusHistory H2
            WHERE   H2.idPaystation = H.idPaystation
            AND     H2.ChangeGMT >= bdt AND H2.ChangeGMT < ADDDATE(edt,INTERVAL 1 DAY))
    GROUP BY P.idPaystation2;
       
    /* apply any changes in Paystation billable status to the Billing Report */
    UPDATE  BillingReportPaystation P, brBillable B
    SET     P.NowBillable = B.NowBillable,
            P.BillableChangeGMT = B.ChangeGMT
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation2 = B.idPaystation2;    
            
    /* carry forward the billable status for those Paystations whose billable status has not changed */
    UPDATE  BillingReportPaystation P
    SET     P.NowBillable = P.WasBillable
    WHERE   P.idBillingReport = idBR
    AND     P.NowBillable = 0;
    
    /* Edge case discovered through testing: assign default value to the billable flag */
    UPDATE  BillingReportPaystation P
    SET     P.NowBillable = 2
    WHERE   P.idBillingReport = idBR
    AND     P.NowBillable = 0;
      
    /* debug trace */
    IF trace = 1 THEN
        /* view list of billable changes */
        SELECT * from brBillable;
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_BillingReportBillable' AS Trace;
    END IF;

END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportActivation 
    
    Important:  This procedure is for EMS v6.3.8 and later database schema.
    
    Purpose:    Determines whether or not the Activation status of any Paystation on a Billing Report 
                has changed within the Billing Report run dates. 

    Call:       CALL sp_BillingReportActivation (idBR, bdt, edt, trace);
   
                Example call:
                CALL sp_BillingReportActivation (12,'2011-06-28','2011-07-14');
                
                Parameters:
                1.  idBR    BillingReport.Id
                2.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                3.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                4.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.
   
                SELECT  idPaystation, Paystation, WasActivated, WasBillable, NowActivated, NowBillable
                FROM    BillingReportPaystation P 
                WHERE   idBillingReport = 12
                AND     WasActivated = 0 OR NowActivated = 0 OR WasBillable = 0 OR NowBillable = 0;
   -------------------------------------------------------------------------------------- */
   
DROP PROCEDURE IF EXISTS sp_BillingReportActivation;

delimiter //
CREATE PROCEDURE sp_BillingReportActivation (IN idBR INT UNSIGNED, IN bdt DATE, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN

    /* table for recent changes in Paystation billable status (if any) */
    DROP TABLE IF EXISTS `brActivated`;
    CREATE TABLE `brActivated` (
        idPaystation                  INT UNSIGNED         NOT NULL,
        ChangeGMT                     DATETIME             NOT NULL,
        NowActivated                  TINYINT UNSIGNED     NOT NULL,
        /* indexes */
        PRIMARY KEY (`idPaystation`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;  
    
    /* get the most recent activation status change for every Paystation on the Billing Report (there will be relatively few changes), note: correlated subquery */
    /* note: the CAST statement at the end of this query is used to convert character data into integer data: the foreign key to join on is embedded in AuditAccess.Description */
    /* note: AuditAccess.EventTypeId: 35=activate, 36=de-activate */
    INSERT  brActivated (idPaystation,ChangeGMT,NowActivated)  
    SELECT  P.idPaystation AS idPaystation, A.ActivityDate AS ChangeGMT, 37-A.EventTypeId AS NowActivated   /* note: 37-A.EventTypeId results in 1=de-activate, and 2=activate */
    FROM    ems_db.AuditAccess A, BillingReportPaystation P
    WHERE   A.Id > 510067292
    AND     A.EventTypeId IN (35,36) 
    AND     A.ActivityDate >= bdt AND A.ActivityDate < ADDDATE(edt,INTERVAL 1 DAY)
    AND     A.Description LIKE '%PaystationId:%'
    AND     P.idBillingReport = idBR 
    AND     P.idPaystation = CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED)
    AND     A.ActivityDate = (
            SELECT  MAX(A2.ActivityDate) 
            FROM    ems_db.AuditAccess A2 
            WHERE   A2.Id > 510067292
            AND     A2.EventTypeId IN (35,36) 
            AND     A2.ActivityDate >= bdt AND A2.ActivityDate < ADDDATE(edt,INTERVAL 1 DAY)
            AND     CAST(SUBSTRING(A2.Description,Locate('PaystationId: ',A2.Description)+14) AS UNSIGNED) = CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED))
    GROUP BY P.idPaystation;
  
    /* apply any changes in Paystation activation status (Activate or De-Activate) to the Billing Report */
    UPDATE  BillingReportPaystation P, brActivated A
    SET     P.NowActivated = A.NowActivated,
            P.ActivationChangeGMT = A.ChangeGMT
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = A.idPaystation;
   
    /* if the Paystation was Activated set the ConnectionDate based on the Activation date, this applies only to newly Activated Paystations and does not apply to newly De-Activated Paystations */  
    UPDATE  BillingReportPaystation P, brActivated A
    SET     P.ConnectionDate = A.ChangeGMT
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = A.idPaystation
    AND     P.NowActivated = 2                  /* note: 2=activated */
    AND     P.NowBillable = 2;                  /* note: 2=billable (meaning: poised to be Billable once the Paystation is Activated). This is a DPT Business Rule */
  
    /* carry forward the activation status for those Paystations whose activation status has not changed, note that table BillingReportPaystation was populated attribute NowActivated was set to zero by default */
    UPDATE BillingReportPaystation P
    SET    P.NowActivated = P.WasActivated
    WHERE  P.idBillingReport = idBR
    AND    P.NowActivated = 0;
   
    /* a new Paystation (would not have been on the previous Billing Report but is on the current Billing Report) will have WasActivated=0 and NowActivated=0, set NowActivated=1 which means Paystation is not activated */
    /* Also, set NowBillable=2 which means the Paystation will become billable for monthly EMS services upon activation */
    /* note: these are new Paystations added to Cerberus and EMS during the Billing Report begin date / end date window that have not been activated */
    UPDATE  BillingReportPaystation P
    SET     P.NowActivated = 1
    WHERE   P.idBillingReport = idBR
    AND     P.WasActivated = 0
    AND     P.NowActivated = 0;
    
    /* by default a new Paystation is poised to be Billable once it is Activated */
    UPDATE  BillingReportPaystation P
    SET     P.NowBillable = 2,
            P.idBillingReportLogicType = 18
    WHERE   P.idBillingReport = idBR
    AND     P.WasActivated = 0
    AND     P.WasBillable = 0    
    AND     P.NowActivated = 1
    AND     P.NowBillable = 0;
    
    /* a new Paystation that is Activated is Billable */
    UPDATE  BillingReportPaystation P
    SET     P.NowBillable = 2,
            P.idBillingReportLogicType = 20
    WHERE   P.idBillingReport = idBR
    AND     P.WasActivated = 0
    AND     P.WasBillable = 0    
    AND     P.NowActivated = 2
    AND     P.NowBillable = 0;
   
    /* debug trace */
    IF trace = 1 THEN
        /* view list of activation changes */
        SELECT  * from brActivated;
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_BillingReportActivation' AS Trace;
    END IF;

END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportServices 
    
    Purpose:    For each Customer on a Billing Report sets their Services information. 
                Also sets (Billing) Note and Provision Date.
                Services, Note and Provision Date all come from the cerberus_db.

    Call:       CALL sp_BillingReportServices (idBR);
   
                Example call:
                CALL sp_BillingReportServices (12);
                
                Parameters:
                1.  idBR    BillingReport.Id
                2.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.

                SELECT  CustomerName, ServicesInUse, ServiceRealTimeCC, ServiceReports, ServiceTextAlerts, ServiceCoupon,
                        ServiceValueCard, ServiceWebServices, ServicePayByCell, ServiceExtendByCell, ProvisionDate, Note
                FROM    BillingReportPaystation 
                WHERE   idBillingReport = 22;

                SELECT  CustomerName, ServicesInUse, Paystation, ProvisionDate, Note
                FROM    BillingReportPaystation 
                WHERE   idBillingReport = 22;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportServices;

delimiter //
CREATE PROCEDURE sp_BillingReportServices (IN idBR INT UNSIGNED, IN trace TINYINT UNSIGNED)
BEGIN

    DROP TEMPORARY TABLE IF EXISTS `brServices`;

    CREATE TEMPORARY TABLE `brServices` (
        idCustomer2                  INT UNSIGNED               NOT NULL,
        CustomerName                 VARCHAR(40)                NOT NULL,
        ServicesInUse                TINYINT UNSIGNED           NOT NULL,
        ServiceRealTimeCC            VARCHAR(5) DEFAULT '',
        ServiceReports               VARCHAR(5) DEFAULT '',
        ServiceTextAlerts            VARCHAR(5) DEFAULT '',
        ServiceCoupon                VARCHAR(5) DEFAULT '',
        ServiceValueCard             VARCHAR(5) DEFAULT '',
        ServiceWebServices           VARCHAR(5) DEFAULT '',
        ServicePayByCell             VARCHAR(5) DEFAULT '',
        ServiceExtendByCell          VARCHAR(5) DEFAULT '',
        /* indexes */
        PRIMARY KEY (`idCustomer2`),
        KEY Name(`CustomerName`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
 
    /* get count of Services for each Customer */
    INSERT  brServices (idCustomer2, CustomerName, ServicesInUse)
    SELECT  C.Id, C.CompanyName, COUNT(*)
    FROM    cerberus_db.Customer C,
            cerberus_db.Customer_Service_Relation CS
    WHERE   C.Id = CS.CustomerId
    GROUP BY C.Id
    ORDER BY C.Id;
  
    /* Service: Real-Time Credit Cards */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceRealTimeCC = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 5;

    /* Service: Reports */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceReports = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 1;

    /* Service: Text Alerts */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceTextAlerts = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 3;

    /* Service: Coupons */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceCoupon = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 2;

    /* Service: Value Card */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceValueCard = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 4;

    /* Service: Web Services */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceWebServices = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 6;

    /* Service: Pay by Phone */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServicePayByCell = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 7;

    /* Service: Extend by Phone */
    UPDATE  brServices b3, 
            cerberus_db.Customer c,
            cerberus_db.Customer_Service_Relation cs
    SET     b3.ServiceExtendByCell = 'On'
    WHERE   b3.CustomerName = c.CompanyName 
    AND     c.Id = cs.CustomerId
    AND     cs.ServiceId = 8;

    /* reset any pre-existing Service information */
    UPDATE  BillingReportPaystation B, brServices S
    SET     B.ServicesInUse       = 0,
            B.ServiceRealTimeCC   = '',
            B.ServiceReports      = '',
            B.ServiceTextAlerts   = '',
            B.ServiceCoupon       = '',
            B.ServiceValueCard    = '',
            B.ServiceWebServices  = '',
            B.ServicePayByCell    = '',
            B.ServiceExtendByCell = '',
            B.Note                = ''
    WHERE   B.idBillingReport     = idBR;

    /* save Service info in Billing Report */
    UPDATE  BillingReportPaystation B, brServices S
    SET     B.ServicesInUse       = S.ServicesInUse,
            B.ServiceRealTimeCC   = S.ServiceRealTimeCC,
            B.ServiceReports      = S.ServiceReports,
            B.ServiceTextAlerts   = S.ServiceTextAlerts,
            B.ServiceCoupon       = S.ServiceCoupon,
            B.ServiceValueCard    = S.ServiceValueCard,
            B.ServiceWebServices  = S.ServiceWebServices,
            B.ServicePayByCell    = S.ServicePayByCell,
            B.ServiceExtendByCell = S.ServiceExtendByCell
    WHERE   B.idBillingReport     = idBR
    AND     B.idCustomer2         = S.idCustomer2;
   
    /* get latest Billing Notes; note: uses correlated subquery */
    UPDATE  BillingReportPaystation R, cerberus_db.Billing B
    SET     R.Note                = B.Note
    WHERE   R.idBillingReport     = idBR
    AND     R.idCustomer2         = B.CustomerId
    AND     B.Id = (SELECT MAX(BB.Id) FROM cerberus_db.Billing BB WHERE BB.CustomerId = B.CustomerId)
    AND     B.Note IS NOT NULL;

    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_BillingReportServices' AS Trace;
    END IF;
    
END//
delimiter ;


/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportAPN

    Purpose:    Counts the number of APN Transaction records for each Paystation from the 
                calendar month PRIOR to the Billing Report begin date for Paystations with 
                ModemSetting.APN = 'digitalpaytech.com'.
                
                Counts the number of Extend-by-Phone Transaction records for each Paystation 
                from the calendar month PRIOR to the Billing Report begin date.
                
    Note:       DPT generates additional revenue when the monthly APN Transaction count 
                for a Paystation exceeds a threshold value. This threshold value is HARD CODED 
                into procedure sp_BillingReportGetSub. 
                
                Also, DPT generates additional revenue for Extend-by-Phone Transactions.
                Each Extend-by-Phone Transaction is billed at an amount HARD CODED into 
                procedure sp_BillingReportGetSub.

    Important:  The APN search string can change over time. That search string is 
                HARD CODED in this procedure.

    Call:       CALL sp_BillingReportAPN (idBR, edt, trace);
   
                Example call:
                CALL sp_BillingReportAPN (12,'2011-06-28',0);
                
                Parameters:
                1.  idBR    BillingReport.Id
                2.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                3.  trace   enable a debug trace: 1=trace

    --------------------------------------------------------------------------------------   
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging
                
                SELECT  C.Name, P.CommAddress, P.Id    
                FROM    Paystation P, ModemSetting M, Customer C
                WHERE   P.Id = M.PaystationId
                AND     M.APN = 'digitalpaytech.com'
                AND     P.CustomerId = C.Id 
                ORDER BY C.Name, P.CommAddress;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportAPN;

delimiter //
CREATE PROCEDURE sp_BillingReportAPN (IN idBR INT UNSIGNED, IN edt DATE, IN trace TINYINT UNSIGNED)
BEGIN
   
    DECLARE prvBR INT UNSIGNED;
    DECLARE apn VARCHAR(40);
    SET apn = 'digitalpaytech.com';     /* note: this search string could change over time */
    
    /* Get Id of previous Billing Report */
    SELECT Id FROM BillingReport WHERE Id = (SELECT MAX(Id) FROM BillingReport WHERE Id < idBR AND IsPosted = 1) INTO prvBR;
    
    /* debug trace */
    IF trace = 1 THEN
        /* date range for APN transaction count */
        SELECT  date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day) AS 'Additional Billing Begin Date',
                date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month) AS 'Additional Billing End Date';
                
        SELECT UTC_TIMESTAMP() AS Time, 'Enter sp_BillingReportAPN' AS Trace;
    END IF;
   
    DROP TABLE IF EXISTS `Paystation_Transactions`;
    DROP TABLE IF EXISTS `Paystation_Extend_Transactions`;
    DROP TABLE IF EXISTS `Paystation_Regular_Transactions`;

    CREATE TABLE `Paystation_Transactions` (
        PaystationId                 INT UNSIGNED         NOT NULL,
        Transactions                 INT UNSIGNED         NOT NULL,
        PRIMARY KEY (`PaystationId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    CREATE TABLE `Paystation_Extend_Transactions` (
        PaystationId                 INT UNSIGNED         NOT NULL,
        Transactions                 INT UNSIGNED         NOT NULL,
        PRIMARY KEY (`PaystationId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    CREATE TABLE `Paystation_Regular_Transactions` (
        PaystationId                 INT UNSIGNED         NOT NULL,
        Transactions                 INT UNSIGNED         NOT NULL,
        PRIMARY KEY (`PaystationId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
  
    /* initialize APNTransactions and ExtendTransactions: set to zero */
    UPDATE     BillingReportPaystation P
    SET        P.APN = NULL,
               P.APNTransactions = 0,
               P.ExtendTransactions = 0
    WHERE      P.idBillingReport = idBR;   

    /* NOTE: This query temporarily commented out and replaced by the query below. This is a short term condition */
    /* Find APN Transactions (Bundled Data) for each Paystation */
    /* note: match on search string in variable 'apn', this search string could change over time */
    /* 
    ---------- COMMENTED OUT ----------
    INSERT  Paystation_Transactions (PaystationId,Transactions)
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.ModemSetting M, ems_db.Transaction T, BillingReportPaystation B
    WHERE   M.APN = apn                 
    AND     M.PaystationId = T.PaystationId
    AND     B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId 
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
    ---------- COMMENTED OUT ----------
    */
    
    /* NOTE: Temporary query replacing the query above. Rob C. provides a list of APN (bundled data) pay stations */    
    INSERT  Paystation_Transactions (PaystationId,Transactions)
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.Paystation IN
(
'100005051012',
'100005051013',
'100005051154',
'100005051155',
'100005051156',
'100005051157',
'100005051182',
'100005051190',
'100005051191',
'100005051293',
'100006021301',
'100006101323',
'100006241384',
'100006241385',
'100006291437',
'100006291438',
'100006291439',
'100006291441',
'100006301450',
'100006301452',
'100006301453',
'100007031587',
'100007111618',
'100007111620',
'100007111621',
'100007111623',
'100007111624',
'100007111626',
'100007111628',
'100007151651',
'100007151657',
'100007301896',
'100007432092',
'100007472143',
'100007472198',
'100007502239',
'100007502240',
'100007502241',
'100007502248',
'100008100019',
'100008100020',
'100008100021',
'100008100022',
'100008100023',
'100008100024',
'100008100025',
'100008100026',
'100008100027',
'100008100028',
'100008100029',
'100008100030',
'100008100031',
'100008100032',
'100008100033',
'100008100034',
'100008100035',
'100008100036',
'100008100037',
'100008100038',
'100008100039',
'100008100040',
'100008100041',
'100008100042',
'100008100043',
'100008100044',
'100008100045',
'100008100046',
'100008100047',
'100008100048',
'100008100049',
'100008100050',
'100008180012',
'100008210034',
'100008210035',
'100008210036',
'100008220001',
'100008280003',
'100008280005',
'100008280006',
'100008280007',
'100008280009',
'100008280010',
'100008280011',
'100008280012',
'100008280013',
'100008280014',
'100008280015',
'100008280017',
'100008280019',
'100008370023',
'100008380001',
'100008380002',
'100008380003',
'100008380004',
'100008380005',
'100008380006',
'100008380007',
'100008380008',
'100008380009',
'100008380010',
'100008380011',
'100008380012',
'100008380013',
'100008380014',
'100008380015',
'100008380016',
'100008380018',
'100008380019',
'100008380020',
'100008380021',
'100008380022',
'100008380023',
'100008380024',
'100008380025',
'100008380026',
'100008380027',
'100008380028',
'100008380029',
'100008380030',
'100008380031',
'100008380032',
'100008380033',
'100008380034',
'100008380035',
'100008380036',
'100008380037',
'100008380038',
'100008380039',
'100008380040',
'100008380041',
'100008380042',
'100008380043',
'100008380044',
'100008380045',
'100008380046',
'100008380047',
'100008380048',
'100008380049',
'100008380050',
'100008440014',
'100008460041',
'100008460042',
'100009090049',
'100009090050',
'100009090051',
'100009090052',
'100009090053',
'100009110050',
'100011440001',
'100011440002',
'200006130105',
'200006350260',
'200006350261',
'200007510555',
'200007510556',
'200009130002',
'200009420001',
'200010030003',
'200010030004',
'200010190002',
'200010190003',
'200010210006',
'200010210007',
'200010210008',
'200010210009',
'200010210010',
'200010210011',
'200010370018',
'200010370019',
'200010370020',
'200010370039',
'200010370105',
'200010370106',
'200010370107',
'200011030011',
'200011030012',
'200011030013',
'200011090012',
'200011190001',
'200011190002',
'200011190003',
'200011190004',
'200011190005',
'200011190006',
'200011190007',
'200011190008',
'200011190009',
'200011190010',
'200011190011',
'200011190012',
'200011310024',
'200011310025',
'200011310026',
'200011310027',
'200012030001',
'200012030002',
'200012090033',
'200012180036',
'200012180040',
'200012180044',
'200012260053',
'200012260054',
'200012290056',
'200012290057',
'200012290058',
'200012290059',
'200012290060',
'200012290061',
'200012290062',
'200012290063',
'200012350068',
'200012350069',
'200012350071',
'200012470078',
'300009160005',
'300009210003',
'300009230027',
'300009230028',
'300009230031',
'300009230032',
'300009230033',
'300009230034',
'300009230035',
'300009230036',
'300009230037',
'300009230038',
'300009230039',
'300009230040',
'300009230041',
'300009230042',
'300009230043',
'300009230044',
'300009230045',
'300009230046',
'300009230047',
'300009230048',
'300009230049',
'300009230050',
'300009230051',
'300009230052',
'300009230053',
'300009230054',
'300009230055',
'300009230056',
'300009230057',
'300009230058',
'300009230059',
'300009230060',
'300009230061',
'300009230062',
'300009230063',
'300009230064',
'300009230065',
'300009230066',
'300009270080',
'300009270081',
'300009270082',
'300009270083',
'300009270084',
'300009270085',
'300009270086',
'300009270087',
'300009270088',
'300009270089',
'300009270090',
'300009480064',
'300009480065',
'300010050022',
'300010050023',
'300010060002',
'300010080009',
'300010120001',
'300010120002',
'300010120013',
'300010130006',
'300010130007',
'300010130008',
'300010130009',
'300010130010',
'300010140040',
'300010140041',
'300010140042',
'300010140043',
'300010170005',
'300010250106',
'300010250107',
'300010320197',
'300010320198',
'300010360193',
'300010360194',
'300010360195',
'300010390733',
'300010390734',
'300010390735',
'300010390736',
'300010390737',
'300010390738',
'300010390739',
'300010390740',
'300010390741',
'300010390742',
'300010390743',
'300010390744',
'300010390745',
'300010390746',
'300010390747',
'300010390748',
'300010390749',
'300010390750',
'300010390751',
'300010390752',
'300010390753',
'300010390754',
'300010390755',
'300010390756',
'300010390757',
'300010390758',
'300010390759',
'300010390760',
'300010390761',
'300010390762',
'300010390763',
'300010390764',
'300010390765',
'300010390766',
'300010390767',
'300010390768',
'300010390769',
'300010390770',
'300010390810',
'300010390811',
'300010390812',
'300010390814',
'300011010012',
'300011010026',
'300011010027',
'300011010028',
'300011010029',
'300011010030',
'300011010031',
'300011010032',
'300011010039',
'300011010041',
'300011090004',
'300011090005',
'300011090006',
'300011090007',
'300011090008',
'300011090032',
'300011090033',
'300011090034',
'300011090035',
'300011090050',
'300011090051',
'300011090052',
'300011090053',
'300011090054',
'300011090055',
'300011090056',
'300011090057',
'300011090058',
'300011090059',
'300011090060',
'300011090061',
'300011090062',
'300011090063',
'300011090064',
'300011090065',
'300011090066',
'300011090067',
'300011090068',
'300011090069',
'300011090070',
'300011090071',
'300011090072',
'300011090073',
'300011090074',
'300011090075',
'300011090076',
'300011090077',
'300011090078',
'300011090079',
'300011090080',
'300011090081',
'300011090082',
'300011090083',
'300011090084',
'300011090109',
'300011090112',
'300011091009',
'300011200017',
'300011200018',
'300011200019',
'300011200020',
'300011200021',
'300011200022',
'300011200023',
'300011200024',
'300011200025',
'300011200026',
'300011280185',
'300011280186',
'300011280187',
'300011330130',
'300011330143',
'300011330172',
'300011330177',
'300011330178',
'300011330185',
'300011330186',
'300011330187',
'300011330188',
'300011330189',
'300011430190',
'300011480246',
'300011480247',
'300011480248',
'300011480263',
'300011520268',
'300011520269',
'300011520270',
'300011520271',
'300011520272',
'300011520273',
'300011520274',
'300011520275',
'300011520276',
'300011520277',
'300011520278',
'300011520279',
'300011520280',
'300011520281',
'300011520282',
'300011520283',
'500011350156',
'500011350158',
'500011350159',
'500011430164',
'500011430165',
'500011430166',
'500011430167',
'500011430168',
'500011430219',
'500011430220',
'500011430221',
'500011430222',
'500011430225',
'500011480232',
'500011480233',
'500011480234',
'500011490251',
'500011490252',
'500011490253',
'500011490254',
'500011490255',
'500011490256',
'500011490257',
'500011490258',
'500011490259',
'500011490260',
'500011490261',
'500011490262',
'500011490292',
'500011490306',
'500011490307',
'500011490308',
'500012020075',
'500012020076',
'500012020077',
'500012020078',
'500012020092',
'500012020093',
'500012040096',
'500012040097',
'500012040115',
'500012050119',
'500012050120',
'500012050121',
'500012050122',
'500012050123',
'500012050128',
'500012050129',
'500012080171',
'500012080172',
'500012080178',
'500012080179',
'500012080180',
'500012080181',
'500012080182',
'500012080183',
'500012080184',
'500012100200',
'500012100201',
'500012100202',
'500012130221',
'500012130223',
'500012130230',
'500012130231',
'500012180242',
'500012180243',
'500012180248',
'500012180249',
'500012180250',
'500012180251',
'500012180299',
'500012180310',
'500012210366',
'500012210373',
'500012210381',
'500012210382',
'500012210383',
'500012210384',
'500012210402',
'500012210403',
'500012260450',
'500012260453',
'500012260454',
'500012260455',
'500012260458',
'500012260492',
'500012260494',
'500012260497',
'500012260498',
'500012260500',
'500012260501',
'500012260506',
'500012260507',
'500012260508',
'500012260509',
'500012260606',
'500012260607',
'500012300001',
'500012300002',
'500012300003',
'500012300004',
'500012300005',
'500012300006',
'500012300007',
'500012300008',
'500012300009',
'500012300010',
'500012300011',
'500012300012',
'500012300013',
'500012300014',
'500012300015',
'500012300016',
'500012300017',
'500012300018',
'500012300019',
'500012300020',
'500012300021',
'500012300022',
'500012300023',
'500012300024',
'500012300025',
'500012300026',
'500012300027',
'500012300028',
'500012300029',
'500012300030',
'500012300031',
'500012300032',
'500012300033',
'500012300034',
'500012300035',
'500012300036',
'500012300037',
'500012300038',
'500012300039',
'500012300040',
'500012300041',
'500012300042',
'500012300043',
'500012300044',
'500012300045',
'500012300046',
'500012300047',
'500012300048',
'500012300049',
'500012300050',
'500012300051',
'500012300052',
'500012300053',
'500012300054',
'500012300055',
'500012300056',
'500012300057',
'500012300058',
'500012300059',
'500012300060',
'500012300061',
'500012300062',
'500012300063',
'500012300064',
'500012300065',
'500012300066',
'500012300067',
'500012300068',
'500012300069',
'500012300070',
'500012300071',
'500012300072',
'500012300073',
'500012300074',
'500012300075',
'500012300076',
'500012300077',
'500012300078',
'500012300079',
'500012300080',
'500012300081',
'500012300082',
'500012300083',
'500012300084',
'500012300085',
'500012300086',
'500012300087',
'500012300088',
'500012300089',
'500012300090',
'500012300091',
'500012300092',
'500012300093',
'500012300094',
'500012300095',
'500012300096',
'500012300097',
'500012300098',
'500012300099',
'500012300100',
'500012300101',
'500012300102',
'500012300103',
'500012300104',
'500012300105',
'500012300106',
'500012300107',
'500012300108',
'500012300109',
'500012300110',
'500012300111',
'500012300112',
'500012300113',
'500012300114',
'500012300115',
'500012300116',
'500012300117',
'500012300118',
'500012300119',
'500012300120',
'500012300121',
'500012300122',
'500012300123',
'500012300124',
'500012300125',
'500012300126',
'500012300127',
'500012300128',
'500012300129',
'500012300130',
'500012300131',
'500012300132',
'500012300133',
'500012300134',
'500012300135',
'500012300136',
'500012300137',
'500012300138',
'500012300139',
'500012300140',
'500012300141',
'500012300142',
'500012300143',
'500012300144',
'500012300145',
'500012300146',
'500012300147',
'500012300148',
'500012300149',
'500012300150',
'500012300151',
'500012300152',
'500012300153',
'500012300154',
'500012300155',
'500012300156',
'500012300157',
'500012300158',
'500012300159',
'500012300160',
'500012300161',
'500012300162',
'500012300163',
'500012300164',
'500012300165',
'500012300166',
'500012300167',
'500012300168',
'500012300169',
'500012300170',
'500012300171',
'500012300172',
'500012300173',
'500012300174',
'500012300175',
'500012300176',
'500012300177',
'500012300178',
'500012300179',
'500012300180',
'500012300181',
'500012300182',
'500012300183',
'500012300184',
'500012300185',
'500012300186',
'500012300187',
'500012300188',
'500012300189',
'500012300190',
'500012300191',
'500012300192',
'500012300193',
'500012300194',
'500012300195',
'500012300196',
'500012300197',
'500012300198',
'500012300199',
'500012300200',
'500012300201',
'500012300202',
'500012300203',
'500012300204',
'500012300205',
'500012300206',
'500012300207',
'500012300208',
'500012300209',
'500012300210',
'500012300211',
'500012300212',
'500012300213',
'500012300214',
'500012300215',
'500012300216',
'500012300217',
'500012300218',
'500012300219',
'500012300220',
'500012300221',
'500012300222',
'500012300223',
'500012300224',
'500012300225',
'500012300226',
'500012300227',
'500012300228',
'500012300229',
'500012300230',
'500012300231',
'500012300232',
'500012300233',
'500012300234',
'500012300235',
'500012300236',
'500012300237',
'500012300238',
'500012300239',
'500012300240',
'500012300241',
'500012300242',
'500012300243',
'500012300244',
'500012300245',
'500012300246',
'500012300247',
'500012300248',
'500012300249',
'500012300250',
'500012300251',
'500012300252',
'500012300253',
'500012300254',
'500012300255',
'500012300256',
'500012300257',
'500012300258',
'500012300259',
'500012300260',
'500012300261',
'500012300262',
'500012300263',
'500012300264',
'500012300265',
'500012300266',
'500012300267',
'500012300268',
'500012300269',
'500012300270',
'500012300271',
'500012300272',
'500012300273',
'500012300274',
'500012300275',
'500012300276',
'500012300277',
'500012300278',
'500012300279',
'500012300280',
'500012300281',
'500012300282',
'500012300283',
'500012300284',
'500012300285',
'500012300286',
'500012300287',
'500012300288',
'500012300289',
'500012300290',
'500012300291',
'500012300292',
'500012300293',
'500012300294',
'500012300295',
'500012300296',
'500012300297',
'500012300298',
'500012300299',
'500012300300',
'500012300301',
'500012300302',
'500012300303',
'500012300304',
'500012300305',
'500012300306',
'500012300307',
'500012300308',
'500012300309',
'500012300310',
'500012300311',
'500012300312',
'500012300313',
'500012300314',
'500012300315',
'500012310703',
'500012310704',
'500012310705',
'500012310718',
'500012310719',
'500012310720',
'500012310721',
'500012330749',
'500012330765',
'500012330766',
'500012330768',
'500012330779',
'500012330780',
'500012340781',
'500012340782',
'500012340783',
'500012340784',
'500012340785',
'500012340786',
'500012340787',
'500012340788',
'500012340789',
'500012340790',
'500012340791',
'500012340792',
'500012350892',
'500012360894',
'500012360895',
'500012360910',
'500012360911',
'500012360912',
'500012360913',
'500012360915',
'500012360935',
'500012360939',
'500012360940',
'500012360941',
'500012360942',
'500012360943',
'500012360944',
'500012360945',
'500012360946',
'500012360947',
'500012360950',
'500012360951',
'500012360952',
'500012360953',
'500012360954',
'500012360955',
'500012360956',
'500012360958',
'500012360959',
'500012360960',
'500012360961',
'500012360962',
'500012360963',
'500012360964',
'500012360965',
'500012360966',
'500012360967',
'500012360988',
'500012410989',
'500012410992',
'500012411001',
'500012411002',
'500012411003',
'500012411004',
'500012441059',
'500012441060',
'500012441061',
'500012441062',
'500012441072',
'500012451074',
'500012451080',
'500012451083',
'500012451084',
'500012451085',
'500012451086',
'500012451087',
'500012451088',
'500012451089',
'500012451090',
'500012451091',
'500012451092',
'500012451093',
'500012451094',
'500012451107',
'500012451108',
'500012451109',
'500012481123',
'500012481124',
'500012481125',
'500012481131',
'500012481132',
'500012491155',
'500012491156',
'500012491157',
'555511130119',
'555511130120',
'555512441223',
'555512441224'
)
    AND     B.idPaystation = T.PaystationId 
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
    
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Bundled Data query done' AS Trace;
    END IF;
    
    /* Find Extend Transactions (Aura) for each Paystation */
    INSERT  Paystation_Extend_Transactions (PaystationId,Transactions)
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 16   /* 16 is for 'AddTime: Extend By Phone AddTime Transaction' */
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
    
    /* Find Regular Transactions (Aura) for each Paystation */
    INSERT  Paystation_Regular_Transactions (PaystationId,Transactions)
    SELECT  T.PaystationId, COUNT(*)
    FROM    ems_db.Transaction T, BillingReportPaystation B
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = T.PaystationId
    AND     T.PurchasedDate >= date(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day))           
    AND     T.PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     T.TypeId = 17   /* 17 is for 'Regular: Extend By Phone Regular Transaction' */
    AND    (B.ServiceExtendByCell = 'On' OR B.idPaystation IN (SELECT B2.idPaystation FROM BillingReportPaystation B2 WHERE B2.idBillingReport = prvBR AND B2.ServiceExtendByCell = 'On'))   
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
    
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Extend by Phone query done' AS Trace;
    END IF;
    
    /* save APN Transactions in Billing Report */
    UPDATE  BillingReportPaystation B, Paystation_Transactions P
    SET     B.APN = apn,
            B.APNTransactions = P.Transactions
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = P.PaystationId;
    
    /* TEMPORARY FIX for APN Transactions: keep track of all APN paystations, an APN paystation may not have APN transactions */
    UPDATE  BillingReportPaystation B
    SET     B.APN = apn
    WHERE   B.idBillingReport = idBR
    AND     B.Paystation IN   
(
'100005051012',
'100005051013',
'100005051154',
'100005051155',
'100005051156',
'100005051157',
'100005051182',
'100005051190',
'100005051191',
'100005051293',
'100006021301',
'100006101323',
'100006241384',
'100006241385',
'100006291437',
'100006291438',
'100006291439',
'100006291441',
'100006301450',
'100006301452',
'100006301453',
'100007031587',
'100007111618',
'100007111620',
'100007111621',
'100007111623',
'100007111624',
'100007111626',
'100007111628',
'100007151651',
'100007151657',
'100007301896',
'100007432092',
'100007472143',
'100007472198',
'100007502239',
'100007502240',
'100007502241',
'100007502248',
'100008100019',
'100008100020',
'100008100021',
'100008100022',
'100008100023',
'100008100024',
'100008100025',
'100008100026',
'100008100027',
'100008100028',
'100008100029',
'100008100030',
'100008100031',
'100008100032',
'100008100033',
'100008100034',
'100008100035',
'100008100036',
'100008100037',
'100008100038',
'100008100039',
'100008100040',
'100008100041',
'100008100042',
'100008100043',
'100008100044',
'100008100045',
'100008100046',
'100008100047',
'100008100048',
'100008100049',
'100008100050',
'100008180012',
'100008210034',
'100008210035',
'100008210036',
'100008220001',
'100008280003',
'100008280005',
'100008280006',
'100008280007',
'100008280009',
'100008280010',
'100008280011',
'100008280012',
'100008280013',
'100008280014',
'100008280015',
'100008280017',
'100008280019',
'100008370023',
'100008380001',
'100008380002',
'100008380003',
'100008380004',
'100008380005',
'100008380006',
'100008380007',
'100008380008',
'100008380009',
'100008380010',
'100008380011',
'100008380012',
'100008380013',
'100008380014',
'100008380015',
'100008380016',
'100008380018',
'100008380019',
'100008380020',
'100008380021',
'100008380022',
'100008380023',
'100008380024',
'100008380025',
'100008380026',
'100008380027',
'100008380028',
'100008380029',
'100008380030',
'100008380031',
'100008380032',
'100008380033',
'100008380034',
'100008380035',
'100008380036',
'100008380037',
'100008380038',
'100008380039',
'100008380040',
'100008380041',
'100008380042',
'100008380043',
'100008380044',
'100008380045',
'100008380046',
'100008380047',
'100008380048',
'100008380049',
'100008380050',
'100008440014',
'100008460041',
'100008460042',
'100009090049',
'100009090050',
'100009090051',
'100009090052',
'100009090053',
'100009110050',
'100011440001',
'100011440002',
'200006130105',
'200006350260',
'200006350261',
'200007510555',
'200007510556',
'200009130002',
'200009420001',
'200010030003',
'200010030004',
'200010190002',
'200010190003',
'200010210006',
'200010210007',
'200010210008',
'200010210009',
'200010210010',
'200010210011',
'200010370018',
'200010370019',
'200010370020',
'200010370039',
'200010370105',
'200010370106',
'200010370107',
'200011030011',
'200011030012',
'200011030013',
'200011090012',
'200011190001',
'200011190002',
'200011190003',
'200011190004',
'200011190005',
'200011190006',
'200011190007',
'200011190008',
'200011190009',
'200011190010',
'200011190011',
'200011190012',
'200011310024',
'200011310025',
'200011310026',
'200011310027',
'200012030001',
'200012030002',
'200012090033',
'200012180036',
'200012180040',
'200012180044',
'200012260053',
'200012260054',
'200012290056',
'200012290057',
'200012290058',
'200012290059',
'200012290060',
'200012290061',
'200012290062',
'200012290063',
'200012350068',
'200012350069',
'200012350071',
'200012470078',
'300009160005',
'300009210003',
'300009230027',
'300009230028',
'300009230031',
'300009230032',
'300009230033',
'300009230034',
'300009230035',
'300009230036',
'300009230037',
'300009230038',
'300009230039',
'300009230040',
'300009230041',
'300009230042',
'300009230043',
'300009230044',
'300009230045',
'300009230046',
'300009230047',
'300009230048',
'300009230049',
'300009230050',
'300009230051',
'300009230052',
'300009230053',
'300009230054',
'300009230055',
'300009230056',
'300009230057',
'300009230058',
'300009230059',
'300009230060',
'300009230061',
'300009230062',
'300009230063',
'300009230064',
'300009230065',
'300009230066',
'300009270080',
'300009270081',
'300009270082',
'300009270083',
'300009270084',
'300009270085',
'300009270086',
'300009270087',
'300009270088',
'300009270089',
'300009270090',
'300009480064',
'300009480065',
'300010050022',
'300010050023',
'300010060002',
'300010080009',
'300010120001',
'300010120002',
'300010120013',
'300010130006',
'300010130007',
'300010130008',
'300010130009',
'300010130010',
'300010140040',
'300010140041',
'300010140042',
'300010140043',
'300010170005',
'300010250106',
'300010250107',
'300010320197',
'300010320198',
'300010360193',
'300010360194',
'300010360195',
'300010390733',
'300010390734',
'300010390735',
'300010390736',
'300010390737',
'300010390738',
'300010390739',
'300010390740',
'300010390741',
'300010390742',
'300010390743',
'300010390744',
'300010390745',
'300010390746',
'300010390747',
'300010390748',
'300010390749',
'300010390750',
'300010390751',
'300010390752',
'300010390753',
'300010390754',
'300010390755',
'300010390756',
'300010390757',
'300010390758',
'300010390759',
'300010390760',
'300010390761',
'300010390762',
'300010390763',
'300010390764',
'300010390765',
'300010390766',
'300010390767',
'300010390768',
'300010390769',
'300010390770',
'300010390810',
'300010390811',
'300010390812',
'300010390814',
'300011010012',
'300011010026',
'300011010027',
'300011010028',
'300011010029',
'300011010030',
'300011010031',
'300011010032',
'300011010039',
'300011010041',
'300011090004',
'300011090005',
'300011090006',
'300011090007',
'300011090008',
'300011090032',
'300011090033',
'300011090034',
'300011090035',
'300011090050',
'300011090051',
'300011090052',
'300011090053',
'300011090054',
'300011090055',
'300011090056',
'300011090057',
'300011090058',
'300011090059',
'300011090060',
'300011090061',
'300011090062',
'300011090063',
'300011090064',
'300011090065',
'300011090066',
'300011090067',
'300011090068',
'300011090069',
'300011090070',
'300011090071',
'300011090072',
'300011090073',
'300011090074',
'300011090075',
'300011090076',
'300011090077',
'300011090078',
'300011090079',
'300011090080',
'300011090081',
'300011090082',
'300011090083',
'300011090084',
'300011090109',
'300011090112',
'300011091009',
'300011200017',
'300011200018',
'300011200019',
'300011200020',
'300011200021',
'300011200022',
'300011200023',
'300011200024',
'300011200025',
'300011200026',
'300011280185',
'300011280186',
'300011280187',
'300011330130',
'300011330143',
'300011330172',
'300011330177',
'300011330178',
'300011330185',
'300011330186',
'300011330187',
'300011330188',
'300011330189',
'300011430190',
'300011480246',
'300011480247',
'300011480248',
'300011480263',
'300011520268',
'300011520269',
'300011520270',
'300011520271',
'300011520272',
'300011520273',
'300011520274',
'300011520275',
'300011520276',
'300011520277',
'300011520278',
'300011520279',
'300011520280',
'300011520281',
'300011520282',
'300011520283',
'500011350156',
'500011350158',
'500011350159',
'500011430164',
'500011430165',
'500011430166',
'500011430167',
'500011430168',
'500011430219',
'500011430220',
'500011430221',
'500011430222',
'500011430225',
'500011480232',
'500011480233',
'500011480234',
'500011490251',
'500011490252',
'500011490253',
'500011490254',
'500011490255',
'500011490256',
'500011490257',
'500011490258',
'500011490259',
'500011490260',
'500011490261',
'500011490262',
'500011490292',
'500011490306',
'500011490307',
'500011490308',
'500012020075',
'500012020076',
'500012020077',
'500012020078',
'500012020092',
'500012020093',
'500012040096',
'500012040097',
'500012040115',
'500012050119',
'500012050120',
'500012050121',
'500012050122',
'500012050123',
'500012050128',
'500012050129',
'500012080171',
'500012080172',
'500012080178',
'500012080179',
'500012080180',
'500012080181',
'500012080182',
'500012080183',
'500012080184',
'500012100200',
'500012100201',
'500012100202',
'500012130221',
'500012130223',
'500012130230',
'500012130231',
'500012180242',
'500012180243',
'500012180248',
'500012180249',
'500012180250',
'500012180251',
'500012180299',
'500012180310',
'500012210366',
'500012210373',
'500012210381',
'500012210382',
'500012210383',
'500012210384',
'500012210402',
'500012210403',
'500012260450',
'500012260453',
'500012260454',
'500012260455',
'500012260458',
'500012260492',
'500012260494',
'500012260497',
'500012260498',
'500012260500',
'500012260501',
'500012260506',
'500012260507',
'500012260508',
'500012260509',
'500012260606',
'500012260607',
'500012300001',
'500012300002',
'500012300003',
'500012300004',
'500012300005',
'500012300006',
'500012300007',
'500012300008',
'500012300009',
'500012300010',
'500012300011',
'500012300012',
'500012300013',
'500012300014',
'500012300015',
'500012300016',
'500012300017',
'500012300018',
'500012300019',
'500012300020',
'500012300021',
'500012300022',
'500012300023',
'500012300024',
'500012300025',
'500012300026',
'500012300027',
'500012300028',
'500012300029',
'500012300030',
'500012300031',
'500012300032',
'500012300033',
'500012300034',
'500012300035',
'500012300036',
'500012300037',
'500012300038',
'500012300039',
'500012300040',
'500012300041',
'500012300042',
'500012300043',
'500012300044',
'500012300045',
'500012300046',
'500012300047',
'500012300048',
'500012300049',
'500012300050',
'500012300051',
'500012300052',
'500012300053',
'500012300054',
'500012300055',
'500012300056',
'500012300057',
'500012300058',
'500012300059',
'500012300060',
'500012300061',
'500012300062',
'500012300063',
'500012300064',
'500012300065',
'500012300066',
'500012300067',
'500012300068',
'500012300069',
'500012300070',
'500012300071',
'500012300072',
'500012300073',
'500012300074',
'500012300075',
'500012300076',
'500012300077',
'500012300078',
'500012300079',
'500012300080',
'500012300081',
'500012300082',
'500012300083',
'500012300084',
'500012300085',
'500012300086',
'500012300087',
'500012300088',
'500012300089',
'500012300090',
'500012300091',
'500012300092',
'500012300093',
'500012300094',
'500012300095',
'500012300096',
'500012300097',
'500012300098',
'500012300099',
'500012300100',
'500012300101',
'500012300102',
'500012300103',
'500012300104',
'500012300105',
'500012300106',
'500012300107',
'500012300108',
'500012300109',
'500012300110',
'500012300111',
'500012300112',
'500012300113',
'500012300114',
'500012300115',
'500012300116',
'500012300117',
'500012300118',
'500012300119',
'500012300120',
'500012300121',
'500012300122',
'500012300123',
'500012300124',
'500012300125',
'500012300126',
'500012300127',
'500012300128',
'500012300129',
'500012300130',
'500012300131',
'500012300132',
'500012300133',
'500012300134',
'500012300135',
'500012300136',
'500012300137',
'500012300138',
'500012300139',
'500012300140',
'500012300141',
'500012300142',
'500012300143',
'500012300144',
'500012300145',
'500012300146',
'500012300147',
'500012300148',
'500012300149',
'500012300150',
'500012300151',
'500012300152',
'500012300153',
'500012300154',
'500012300155',
'500012300156',
'500012300157',
'500012300158',
'500012300159',
'500012300160',
'500012300161',
'500012300162',
'500012300163',
'500012300164',
'500012300165',
'500012300166',
'500012300167',
'500012300168',
'500012300169',
'500012300170',
'500012300171',
'500012300172',
'500012300173',
'500012300174',
'500012300175',
'500012300176',
'500012300177',
'500012300178',
'500012300179',
'500012300180',
'500012300181',
'500012300182',
'500012300183',
'500012300184',
'500012300185',
'500012300186',
'500012300187',
'500012300188',
'500012300189',
'500012300190',
'500012300191',
'500012300192',
'500012300193',
'500012300194',
'500012300195',
'500012300196',
'500012300197',
'500012300198',
'500012300199',
'500012300200',
'500012300201',
'500012300202',
'500012300203',
'500012300204',
'500012300205',
'500012300206',
'500012300207',
'500012300208',
'500012300209',
'500012300210',
'500012300211',
'500012300212',
'500012300213',
'500012300214',
'500012300215',
'500012300216',
'500012300217',
'500012300218',
'500012300219',
'500012300220',
'500012300221',
'500012300222',
'500012300223',
'500012300224',
'500012300225',
'500012300226',
'500012300227',
'500012300228',
'500012300229',
'500012300230',
'500012300231',
'500012300232',
'500012300233',
'500012300234',
'500012300235',
'500012300236',
'500012300237',
'500012300238',
'500012300239',
'500012300240',
'500012300241',
'500012300242',
'500012300243',
'500012300244',
'500012300245',
'500012300246',
'500012300247',
'500012300248',
'500012300249',
'500012300250',
'500012300251',
'500012300252',
'500012300253',
'500012300254',
'500012300255',
'500012300256',
'500012300257',
'500012300258',
'500012300259',
'500012300260',
'500012300261',
'500012300262',
'500012300263',
'500012300264',
'500012300265',
'500012300266',
'500012300267',
'500012300268',
'500012300269',
'500012300270',
'500012300271',
'500012300272',
'500012300273',
'500012300274',
'500012300275',
'500012300276',
'500012300277',
'500012300278',
'500012300279',
'500012300280',
'500012300281',
'500012300282',
'500012300283',
'500012300284',
'500012300285',
'500012300286',
'500012300287',
'500012300288',
'500012300289',
'500012300290',
'500012300291',
'500012300292',
'500012300293',
'500012300294',
'500012300295',
'500012300296',
'500012300297',
'500012300298',
'500012300299',
'500012300300',
'500012300301',
'500012300302',
'500012300303',
'500012300304',
'500012300305',
'500012300306',
'500012300307',
'500012300308',
'500012300309',
'500012300310',
'500012300311',
'500012300312',
'500012300313',
'500012300314',
'500012300315',
'500012310703',
'500012310704',
'500012310705',
'500012310718',
'500012310719',
'500012310720',
'500012310721',
'500012330749',
'500012330765',
'500012330766',
'500012330768',
'500012330779',
'500012330780',
'500012340781',
'500012340782',
'500012340783',
'500012340784',
'500012340785',
'500012340786',
'500012340787',
'500012340788',
'500012340789',
'500012340790',
'500012340791',
'500012340792',
'500012350892',
'500012360894',
'500012360895',
'500012360910',
'500012360911',
'500012360912',
'500012360913',
'500012360915',
'500012360935',
'500012360939',
'500012360940',
'500012360941',
'500012360942',
'500012360943',
'500012360944',
'500012360945',
'500012360946',
'500012360947',
'500012360950',
'500012360951',
'500012360952',
'500012360953',
'500012360954',
'500012360955',
'500012360956',
'500012360958',
'500012360959',
'500012360960',
'500012360961',
'500012360962',
'500012360963',
'500012360964',
'500012360965',
'500012360966',
'500012360967',
'500012360988',
'500012410989',
'500012410992',
'500012411001',
'500012411002',
'500012411003',
'500012411004',
'500012441059',
'500012441060',
'500012441061',
'500012441062',
'500012441072',
'500012451074',
'500012451080',
'500012451083',
'500012451084',
'500012451085',
'500012451086',
'500012451087',
'500012451088',
'500012451089',
'500012451090',
'500012451091',
'500012451092',
'500012451093',
'500012451094',
'500012451107',
'500012451108',
'500012451109',
'500012481123',
'500012481124',
'500012481125',
'500012481131',
'500012481132',
'500012491155',
'500012491156',
'500012491157',
'555511130119',
'555511130120',
'555512441223',
'555512441224'
);
    
    /* save Aura Extend Transactions in Billing Report */
    UPDATE  BillingReportPaystation B, Paystation_Extend_Transactions P
    SET     B.ExtendTransactions = P.Transactions,
            B.EmsPsType16TxnCount = P.Transactions
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = P.PaystationId;
    
    /* save Aura Regular Transactions in Billing Report */
    UPDATE  BillingReportPaystation B, Paystation_Regular_Transactions P
    SET     B.EmsPsType17TxnCount = P.Transactions
    WHERE   B.idBillingReport = idBR
    AND     B.idPaystation = P.PaystationId;
   
    /* debug trace */
    IF trace = 1 THEN
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_BillingReportAPN' AS Trace;
    END IF;

END//
delimiter ;

/*  --------------------------------------------------------------------------------------
    Procedure:  sp_RunBillingReport
    
    Important:  This procedure can run against pre-EMS v6.3.8 as well as EMS v6.3.8 and later 
                database schema. Which database version the procedure runs against is hard 
                coded into procedure variable DBversion. Review the procedure code below.
    
    Purpose:    Creates a new Billing Report or updates an existing Billing Report.
                Calls other procedures that perform Billing Report business logic.
    
                If the previous (last) Billing Report was posted (finalized) a new 
                Billing Report will be created.
                
                It is possible to run an interim Billing Report earlier than the 
                standardized Billing Report run. An interim report is not posted
                (finalized) and can be re-run numerous times.

                This procedure does not generate report outputL it prepares data for 
                output. For report output use procedure sp_BillingReportGetSub.
                
    Call:       CALL sp_RunBillingReport (bdt,edt,post,trace);
    
                Example call: 
                CALL sp_RunBillingReport ('2011-01-01','2011-09-14',0,1);
                
                Interpretation of example call:
                CALL sp_RunBillingReport ('2011-06-28','2011-07-14',1,0);
                Run the Billing Report with report begin and end dates of 2011-06-28
                and 2011-07-14, post (finalize) the report when the report has completed
                running, and do not run a debug trace while the report runs.
    
                Parameters:
                1.  bdt     Billing Report begin date (BillingReport.DateRangeBegin)
                2.  edt     Billing Report end date (BillingReport.DateRangeEnd)
                3.  post    Post (finalize) this Billing Report at the end of the report run
                            values: 1=post, otherwise do not post
                4.  trace   enable a debug trace as the report runs
                            values: 1=trace, otherwise do not trace
                
    Note:       The begin and end dates for the Billing Report are inclusive.
                Using the example call above the Billing Report window is from the
                beginning of 2011-06-28 until the end of 2011-07-14.
                The next Billing Report would be run from 2011-07-15 to 2011-07-27.
                
    --------------------------------------------------------------------------------------
    SQL:        The SQL below was useful for testing and debugging. 
                Also, use procedure sp_BillingReportGetSub to generate output for testing and debugging.
    
                SELECT  * FROM BillingReport WHERE Id = 22;
                
                SELECT  COUNT(*) FROM BillingReportPaystation WHERE idBillingReport = 2;
              
                SELECT  P.idBillingReportLogicType          AS idLogic, 
                        COUNT(*)                            AS Paystations, 
                        L.Comment                           AS 'Current Billable Status', 
                        IF(L.WasActivated=2,'Yes','No')     AS 'Was Activated', 
                        IF(L.WasBillable=2,'Yes','No')      AS 'Was Billable', 
                        IF(L.NowActivated=2,'Yes','No')     AS 'Now Activated', 
                        If(L.NowBillable=2,'Yes','No')      AS 'Now Billable'
                FROM    BillingReportPaystation P, BillingReportLogicType L
                WHERE   P.idBillingReport = 12 
                AND     P.idBillingReportLogicType = L.Id
                GROUP BY P.idBillingReportLogicType ORDER BY idBillingReportLogicType;
                            
                SELECT  COUNT(*) FROM BillingReportPaystation WHERE idBillingReport = 12 and idBillingReportLogicType = 0;
              
                SELECT  idPaystation, Paystation, WasActivated, NowActivated, WasBillable, NowBillable, ActivationChangeGMT, BillableChangeGMT 
                FROM    BillingReportPaystation 
                WHERE   idBillingReport = 22 
                AND     (ActivationChangeGMT IS NOT NULL OR BillableChangeGMT IS NOT NULL)
                LIMIT   10;
    -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_RunBillingReport;

delimiter //
CREATE PROCEDURE sp_RunBillingReport (IN bdt DATE, IN edt DATE, IN post TINYINT UNSIGNED, IN trace TINYINT UNSIGNED)
BEGIN

    /* task: 1=create-new-billing-report; 2=update-existing-billing-report; 0,3,4,5 (any other value): error; 'task' is set by a call to procedure sp_StartBillingReport() below */
    DECLARE task TINYINT UNSIGNED;    

    /* idBR: stores BillingReport.Id */ 
    DECLARE idBR INT UNSIGNED;
   
    /* DBversion (database version): 1=pre-EMS v6.3.8, 2=EMS v6.3.8 and later */
    DECLARE DBversion TINYINT UNSIGNED;
    SET DBversion = 2;

    /* initialize variables, these variables will be set by the call to procedure sp_StartBillingReport just below */
    SET task = 0, idBR = 0;

    /* start a new or update an existing Billing Report: assigns values to parameters task and idBR */
    CALL sp_StartBillingReport (task, idBR, bdt, edt, trace);

    /* proceed with starting a new Billing Report or re-running an existing non-posted (non-finalized) Billing Report  */
    IF task=1 OR task=2 THEN

        /* set Services for each Customer on the Billing Report */
        CALL sp_BillingReportServices (idBR, trace);  

        /* pre-EMS v6.3.8: cannot use changes in Activation and Billable status, must look at Events and Transactions */
        IF DBversion = 1 THEN
            /* get Connection Date for each Paystation (FOR PRE-EMS v6.3.8 VERSION OF BILLING REPORT) */
            CALL sp_BillingReportConnection (idBR, bdt, edt, trace);
        END IF;
      
        /* for EMS v6.3.8 or later: use changes in Activation status and Billable status */
        IF DBversion = 2 THEN
            /* determine Paystation Billable status */
            CALL sp_BillingReportBillable (idBR, bdt, edt, trace);

            /* determine paystation Activation status */
            CALL sp_BillingReportActivation (idBR, bdt, edt, trace);
            
            /* get APN Transaction counts for each Paystation  */
            CALL sp_BillingReportAPN (idBR, edt, trace);
        END IF;
        
        /* July 30 2012: make changes to the billable status of paystations based on new billing rules */
        /* Trial Customers are NOT billable */
        UPDATE  BillingReportPaystation P
        SET     P.NowBillable = 1
        WHERE   P.idBillingReport = idBR
        AND     P.CerberusCustomerStatus = 1;
        
        /* Inactive Customers have all their status values for billable and activated cleared  */
        UPDATE  BillingReportPaystation P
        SET     P.NowBillable = 0,
                P.NowActivated = 0,
                P.WasBillable = 0,
                P.WasActivated = 0
        WHERE   P.idBillingReport = idBR
        AND     P.CerberusCustomerStatus = -1;
           
        /* determine idBillingReportLogicType for each Paystation on the Billing Report, after this UPDATE statement any BillingReportPaystation with idBillingReportLogicType=0 is an error  */
        /* table BillingReportLogicType contains the complete set of logical possibilities in changes to Activation and Billable status from one Billing Report to the next Billing Report */
        /* if idBillingReportLogicType cannot be set (remains zero) the billable status of a Paystation cannot be determined, and a logic error somewhere in this report has been exposed */
        UPDATE  BillingReportPaystation P, BillingReportLogicType L
        SET     P.idBillingReportLogicType = L.Id
        WHERE   P.idBillingReport = idBR
        AND     P.WasActivated = L.WasActivated 
        AND     P.NowActivated = L.NowActivated
        AND     P.WasBillable  = L.WasBillable  
        AND     P.NowBillable  = L.NowBillable;  
              
        /* post (finalize) the Report if requested */
        IF post = 1 THEN
            UPDATE BillingReport SET IsPosted = 1 WHERE Id = idBR;
        END IF;

        /* report run has completed */
        UPDATE BillingReport SET RunEnd = UTC_TIMESTAMP() WHERE Id = idBR;   
    END IF;
    
    /* debugging trace */
    IF trace = 1 THEN
        SELECT task AS 'Task: 1=new, 2=update', idBR;
        SELECT UTC_TIMESTAMP() AS Time, 'Exit sp_RunBillingReport' AS Trace;
    END IF;
    
    /* perform various transaction counts, these are not part of the Billing Report but the numbers are useful for report verification, and following the adoption of Extend-by-Phone */
    CALL sp_BillingReportTxnCount (idBR, bdt, edt);
  
END//
delimiter ;





/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportTxnCount
    
    Note:       This prodecure is NOT part of the Billing Report.
    
                It is a utility procedure that queries the Transaction table looking
                for Transactions for each Paystation.
    
                The transaction counts it provides are useful for: 
                o  validating the Billing Report
                o  finding inconsistencies in paystation provisioning (setting of flags between Cerberus and EMS for Customers and Paystations) 
                o  determining the utilization rate of Extend-by-Phone (number of actual extensions vs number of extend-by-phone permits)
                
                Set values for the following attributes in the BillingReportPaystation table (for the specified BillingReport.Id):
                o   EmsPsMonth1TxnCount
                o   EmsPsDay5TxnCount
                
    Note:       This procedure can take a while to execute but is now performing considerably less work than earlier versions.
   -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportTxnCount;

delimiter //
CREATE PROCEDURE sp_BillingReportTxnCount (IN idBR INT UNSIGNED, IN bdt DATE, IN edt DATE) 
BEGIN

    DROP TEMPORARY TABLE IF EXISTS `PS_txn`;
    
    CREATE TEMPORARY TABLE `PS_txn` (
        PaystationId                 INT UNSIGNED NOT NULL,
        Transactions                 INT UNSIGNED NOT NULL,
        PRIMARY KEY (`PaystationId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    -- -------------------------------------------------------------------
    -- Current month: EmsPsMonth1TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsMonth1TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
     
    -- -------------------------------------------------------------------            
    -- Last 5 Days: EmsPsDay5TxnCount
   
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date_sub(edt,INTERVAL 4 DAY)
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay5TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
            
END//
delimiter ;





DROP PROCEDURE IF EXISTS sp_BillingReportTxnCount_OLD;

delimiter //
CREATE PROCEDURE sp_BillingReportTxnCount_OLD (IN idBR INT UNSIGNED, IN bdt DATE, IN edt DATE) 
BEGIN

    DROP TEMPORARY TABLE IF EXISTS `PS_txn`;
    
    CREATE TEMPORARY TABLE `PS_txn` (
        PaystationId                 INT UNSIGNED NOT NULL,
        Transactions                 INT UNSIGNED NOT NULL,
        PRIMARY KEY (`PaystationId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    -- -------------------------------------------------------------------
    -- Current month: EmsPsMonth1TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date(date_add(date_sub(date_sub(etd,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsMonth1TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
     
    -- -------------------------------------------------------------------
    -- Previous month: EmsPsMonth2TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date(date_add(date_sub(date_sub(edt,interval 2 month),interval dayofmonth(edt)-1 day),interval 1 month))
    AND     PurchasedDate <  date(date_add(date_sub(date_sub(edt,interval 1 month),interval dayofmonth(edt)-1 day),interval 1 month))
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsMonth2TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
    
    -- -------------------------------------------------------------------
    -- Day 1: EmsPsDay1TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= edt
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay1TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;

    -- -------------------------------------------------------------------   
    -- Day 2: EmsPsDay2TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date_sub(edt,INTERVAL 1 DAY)
    AND     PurchasedDate <  edt
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay2TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
    
    -- -------------------------------------------------------------------      
    -- Day 3: EmsPsDay3TxnCount
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date_sub(edt,INTERVAL 2 DAY)
    AND     PurchasedDate <  date_sub(edt,INTERVAL 1 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay3TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
        
    -- -------------------------------------------------------------------
    -- Day 4: EmsPsDay4TxnCount
/*    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date_sub(edt,INTERVAL 3 DAY)
    AND     PurchasedDate <  date_sub(edt,INTERVAL 2 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay4TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
*/
    -- -------------------------------------------------------------------            
    -- Day 5: EmsPsDay5TxnCount
/*    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.TransactionCollection
    WHERE   PurchasedDate >= date_sub(edt,INTERVAL 4 DAY)
    AND     PurchasedDate <  date_sub(edt,INTERVAL 3 DAY)
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsDay5TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
*/            
    -- -------------------------------------------------------------------
    -- Type 16: EmsPsType16TxnCount (AddTime: Extend by Phone Permit Extension)
    
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.Transaction
    WHERE   PurchasedDate >= bdt
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    AND     TypeId = 16
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsType16TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
            
    -- -------------------------------------------------------------------
    -- Type 17: EmsPsType17TxnCount (Regular: Extend by Phone Permit)
   
    TRUNCATE TABLE PS_txn;
    
    INSERT  PS_txn (PaystationId, Transactions)
    SELECT  PaystationId, COUNT(*)
    FROM    ems_db.Transaction
    WHERE   PurchasedDate >= bdt
    AND     PurchasedDate <  date_add(edt,INTERVAL 1 DAY)
    AND     TypeId = 17
    GROUP BY PaystationId
    ORDER BY PaystationId;
    
    UPDATE  BillingReportPaystation P, PS_txn T
    SET     P.EmsPsType17TxnCount = T.Transactions
    WHERE   P.idBillingReport = idBR
    AND     P.idPaystation = T.PaystationId
    AND     T.Transactions > 0;
  
END//
delimiter ;








/*  --------------------------------------------------------------------------------------
    Procedure:  sp_BillingReportAllPaystations
    
    Note:       This prodecure is NOT part of the Billing Report.
    
                It is a utility procedure that queries the Transaction table looking
                for Transactions for each Paystation.
    
    Purpose:    Gets counts from the Transaction table for every Paystation over the last 
                5 years.
                
    Note:       This procedure can take a while to execute. 

    Call:       CALL sp_BillingReportAllPaystations;
                
                This procedure takes no parameters.    
   -------------------------------------------------------------------------------------- */

DROP PROCEDURE IF EXISTS sp_BillingReportAllPaystations;

delimiter //
CREATE PROCEDURE sp_BillingReportAllPaystations()
BEGIN

    /* variables used for loops */
    DECLARE i, j, k INT UNSIGNED;
    
    /* set when processing is initiated */
    DECLARE RunTime DATETIME;
    SELECT UTC_TIMESTAMP() INTO RunTime;
   
    /* tables for Paystation Transaction data */
    DROP TABLE IF EXISTS `BillingReportCerberusPaystation`;
    DROP TABLE IF EXISTS `BillingReportEmsPaystation`;
    DROP TABLE IF EXISTS `BillingReportPaystationTransaction`;
    DROP TABLE IF EXISTS `BillingReportPaystationTransactionInfo`;   
    DROP TABLE IF EXISTS `BillingReportPaystationFirstTransaction`;    
   
    CREATE TABLE `BillingReportCerberusPaystation` (
        Id                          INT UNSIGNED            NOT NULL,
        SerialNumber                VARCHAR(20)             NOT NULL,
        CustomerId                  INT UNSIGNED            NOT NULL,  
        EmsCustomerId               INT UNSIGNED            DEFAULT 0,        
        CompanyName                 VARCHAR(100)            DEFAULT '',
        CreationDate                DATETIME,
        IsDeleted                   TINYINT,     
        CustomerStatus              TINYINT,
        CustomerEnabled             TINYINT,
        PRIMARY KEY (`Id`),
        KEY Customer (`CustomerId`,`SerialNumber`),
        KEY SerialNumber (`SerialNumber`),
        KEY EmsCustomer (`EmsCustomerId`),
        KEY CompanyName (`CompanyName`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

    CREATE TABLE `BillingReportEmsPaystation` (
        Id                          INT UNSIGNED            NOT NULL,
        CommAddress                 VARCHAR(20)             NOT NULL,
        CustomerId                  INT UNSIGNED            NOT NULL,          
        CustomerName                VARCHAR(40)             DEFAULT '',
        IsActive                    TINYINT,
        LockState                   VARCHAR(20),
        DeleteFlag                  TINYINT,
        CustomerAccountStatus       VARCHAR(10),
        PaystationName              VARCHAR(40),
        PaystationLastHeartBeat     DATETIME,
        LocationName                VARCHAR(255),
        PRIMARY KEY (`Id`),
        UNIQUE KEY CommAddress (`CommAddress`),
        UNIQUE KEY Customer (`CustomerId`,`CommAddress`),
        KEY Name (`CustomerName`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

    CREATE TABLE `BillingReportPaystationTransaction` (
        Id                          INT UNSIGNED            NOT NULL,
        CommAddress                 VARCHAR(20)             NOT NULL,
        Year0                       INT UNSIGNED,
        Year1                       INT UNSIGNED,
        Year2                       INT UNSIGNED,
        Year3                       INT UNSIGNED,
        Year4                       INT UNSIGNED,
        Year5                       INT UNSIGNED,
        Month0                      INT UNSIGNED,
        Month1                      INT UNSIGNED,
        Month2                      INT UNSIGNED,
        Month3                      INT UNSIGNED,
        Month4                      INT UNSIGNED,
        Month5                      INT UNSIGNED,
        Month6                      INT UNSIGNED,
        Month7                      INT UNSIGNED,
        Month8                      INT UNSIGNED,
        Month9                      INT UNSIGNED,
        Month10                     INT UNSIGNED,
        Month11                     INT UNSIGNED,
        Month12                     INT UNSIGNED,
        Week1                       INT UNSIGNED,
        Week2                       INT UNSIGNED,
        Week3                       INT UNSIGNED,  
        Total                       INT UNSIGNED,     
        FirstTransaction            DATETIME,
        PRIMARY KEY (`Id`),
        UNIQUE KEY CommAddress (`CommAddress`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    CREATE TABLE `BillingReportPaystationTransactionInfo` (
        Id                          INT UNSIGNED            NOT NULL,
        Type                        VARCHAR(10)             NOT NULL,
        Period                      INT UNSIGNED            NOT NULL,
        Year                        INT UNSIGNED            NOT NULL    DEFAULT 0,
        Month                       INT UNSIGNED            NOT NULL    DEFAULT 0,
        Transactions                INT UNSIGNED            NOT NULL,
        PRIMARY KEY (`Id`,`Type`,`Period`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    CREATE TABLE `BillingReportPaystationFirstTransaction` (
        Id                          INT UNSIGNED            NOT NULL,
        CommAddress                 VARCHAR(20)             NOT NULL,
        FirstTransaction            DATETIME                NOT NULL,          
        PRIMARY KEY (`Id`),
        UNIQUE KEY CommAddress (`CommAddress`)
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
    
    /* populate table BillingReportCerberusPaystation */
    INSERT  BillingReportCerberusPaystation (Id, SerialNumber, CustomerId, CreationDate, IsDeleted)
    SELECT  Id, SerialNumber, CustomerId, CreationDate, IsDeleted
    FROM    cerberus_db.Paystation
    WHERE   CustomerId IN (SELECT Id FROM cerberus_db.Customer WHERE ServerId = 1);
    
    /* get Customer info for BillingReportCerberusPaystation */
    UPDATE  BillingReportCerberusPaystation P,
            cerberus_db.Customer C
    SET     P.EmsCustomerId   = C.EmsCustomerId,
            P.CompanyName     = C.CompanyName,
            P.CustomerStatus  = C.Status,
            P.CustomerEnabled = C.Enabled
    WHERE   P.CustomerId      = C.Id;
    
    /* populate table BillingReportEmsPaystation */
    INSERT  BillingReportEmsPaystation (Id, CommAddress, CustomerId, IsActive, LockState, DeleteFlag, PaystationName)
    SELECT  Id, CommAddress, CustomerId, IsActive, LockState, DeleteFlag, Name
    FROM    ems_db.Paystation;
    
    /* get Customer info for BillingReportEmsPaystation */
    UPDATE  BillingReportEmsPaystation P,
            ems_db.Customer C
    SET     P.CustomerName = C.Name,
            P.CustomerAccountStatus = C.AccountStatus
    WHERE   P.CustomerId = C.Id;
    
    /* get Paystation info for BillingReportEmsPaystation */
    /* new code as of June 27 2012 */
    UPDATE  BillingReportEmsPaystation P,
            ems_db.Paystation PS,
            ems_db.Region R,
            ems_db.PaystationHeartBeat H
    SET     P.PaystationLastHeartBeat = H.LastHeartBeatGMT,
            P.LocationName = R.Name
    WHERE   P.Id = PS.Id
    AND     PS.RegionId = R.Id
    AND     PS.Id = H.PaystationId;
        
    /* populate table BillingReportPaystationTransaction */
    INSERT  BillingReportPaystationTransaction (Id, CommAddress)
    SELECT  Id, CommAddress
    FROM    BillingReportEmsPaystation;
    
    /* populate table BillingReportPaystationFirstTransaction */ 
    INSERT  BillingReportPaystationFirstTransaction (Id, CommAddress, FirstTransaction)
    SELECT  T.PaystationId, P.CommAddress, MIN(T.PurchasedDate) 
    FROM    ems_db.Transaction T, ems_db.Paystation P
    WHERE   T.PaystationId = P.Id
    AND     T.PaystationId IN (SELECT Id FROM BillingReportEmsPaystation)
    AND     T.TypeId = 2        /* regular permit */
    AND     T.IsOffline = 0     /* no offline transactions */
    GROUP BY T.PaystationId
    ORDER BY T.PaystationId;
    
    /* Transactions from previous 1 - 7 days */ 
    SELECT  UTC_TIMESTAMP() AS 'Time', 'Day' AS Task;
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Transactions)
    SELECT  P.Id, 'Day' AS Type, 251 AS Period, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId 
    AND     T.PurchasedDate >= SUBDATE(DATE(RunTime),INTERVAL 7 DAY) AND T.PurchasedDate < DATE(RunTime)
    AND     T.TypeId = 2        /* regular permit */
    AND     T.IsOffline = 0     /* no offline transactions */
    GROUP BY P.Id
    ORDER BY P.Id;
  
    /* Transactions from previous 8 - 14 days */
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Transactions)
    SELECT  P.Id, 'Day' AS Type, 252 AS Period, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId 
    AND     T.PurchasedDate >= SUBDATE(DATE(RunTime),INTERVAL 14+1 DAY) AND T.PurchasedDate < SUBDATE(DATE(RunTime),INTERVAL 7+1 DAY)
    AND     T.TypeId = 2        /* regular permit */
    AND     T.IsOffline = 0     /* no offline transactions */
    GROUP BY P.Id
    ORDER BY P.Id;
   
    /* Transactions from previous 15 - 21 days */
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Transactions)
    SELECT  P.Id, 'Day' AS Type, 253 AS Period, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId 
    AND     T.PurchasedDate >= SUBDATE(DATE(RunTime),INTERVAL 21+1 DAY) AND T.PurchasedDate < SUBDATE(DATE(RunTime),INTERVAL 14+1 DAY)
    AND     T.TypeId = 2        /* regular permit */
    AND     T.IsOffline = 0     /* no offline transactions */
    GROUP BY P.Id
    ORDER BY P.Id;
    
    /* Transactions from previous 13 calendar months */
    /* CHANGED 2012_01_31 TO REDUCE EXECUTION TIME: reduce to 7 calendar months */
    SELECT  UTC_TIMESTAMP() AS 'Time', 'Month' AS Task;
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Year, Month, Transactions)    
    SELECT  P.Id, 'Month' AS Type, (YEAR(T.PurchasedDate)*1000 + MONTH(T.PurchasedDate)*10)/10 AS Period, YEAR(T.PurchasedDate) AS Year, MONTH(T.PurchasedDate) AS Month, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId
    AND     T.PurchasedDate >= SUBDATE(SUBDATE(DATE(RunTime),INTERVAL 7 MONTH),INTERVAL DAY(DATE(RunTime))-1 DAY)
    AND     T.PurchasedDate <= RunTime
    AND     T.TypeId = 2        /* regular permit */
    AND     T.IsOffline = 0     /* no offline transactions */
    GROUP BY P.Id, YEAR(T.PurchasedDate), MONTH(T.PurchasedDate)
    ORDER BY P.Id, YEAR(T.PurchasedDate), MONTH(T.PurchasedDate);  

    /* Transactions from previous 5 calendar years */
    /* CHANGED 2012_01_31 TO REDUCE EXECUTION TIME: no longer used */
    /*
    SELECT  UTC_TIMESTAMP() AS 'Time', 'Year' AS Task;
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Year, Month, Transactions)    
    SELECT  P.Id, 'Year' AS Type, (YEAR(T.PurchasedDate)*1000 + 10)/10 AS Period, YEAR(T.PurchasedDate) AS Year, 1 AS Month, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId
    AND     T.PurchasedDate >= SUBDATE(SUBDATE(SUBDATE(DATE(RunTime),INTERVAL 48 MONTH),INTERVAL MONTH(DATE(RunTime))-1 MONTH), INTERVAL DAY(DATE(RunTime))-1 DAY)
    AND     T.PurchasedDate <= RunTime
    AND     T.TypeId = 2        
    AND     T.IsOffline = 0     
    GROUP BY P.Id, YEAR(T.PurchasedDate)
    ORDER BY P.Id, YEAR(T.PurchasedDate);   
    */

    /* Total transactions */
    /* CHANGED 2012_01_31 TO REDUCE EXECUTION TIME: no longer used */
    /*
    SELECT  UTC_TIMESTAMP() AS 'Time', 'Total' AS Task;
    INSERT  BillingReportPaystationTransactionInfo (Id, Type, Period, Transactions)    
    SELECT  P.Id, 'Total' AS Type, 0 AS Period, COUNT(*) AS Transactions
    FROM    BillingReportEmsPaystation P, ems_db.Transaction T
    WHERE   P.Id = T.PaystationId
    AND     T.PurchasedDate <= RunTime
    AND     T.TypeId = 2        
    AND     T.IsOffline = 0    
    GROUP BY P.Id
    ORDER BY P.Id;  
    SELECT  UTC_TIMESTAMP() AS 'Time';   
    */
    
    /* debug SELECT */
    /*
    SELECT * FROM BillingReportPaystationTransactionInfo;
    */
    
    /* transfer First Transaction */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationFirstTransaction P
    SET     B.FirstTransaction = P.FirstTransaction
    WHERE   B.Id = P.Id;
    
    /* transfer Total transactions */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Total = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Total';
    
    /* transfer Week transactions: Week1 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Week1 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Day'
    AND     P.Period = 251;
   
    /* transfer Week transactions: Week2 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Week2 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Day'
    AND     P.Period = 252;
    
    /* transfer Week transactions: Week3 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Week3 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Day'
    AND     P.Period = 253;
    
    /*  ---------------------------------------------------------------------------------
        Warning:    This stored procedure was a quick hack. 
                    With each new calendar year bits of code below will have to change.
        ---------------------------------------------------------------------------------   */
    
    /* transfer Year transactions: Current Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year0 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 201201;  /* Change YYYY of YYYYMM */
    
    /* transfer Year transactions: Previous Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year1 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 201101;  /* Change YYYY of YYYYMM */
    
    /* transfer Year transactions: Previous Previous Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year2 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 201001;  /* Change YYYY of YYYYMM */
    
    /* transfer Year transactions: Previous Previous Previous Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year3 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 200901;  /* Change YYYY of YYYYMM */
    
    /* transfer Year transactions: Previous Previous Previous Previous Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year4 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 200801;  /* Change YYYY of YYYYMM */
    
    /* transfer Year transactions: Previous Previous Previous Previous Previous Year */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Year5 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Year'
    AND     P.Period = 200701;  /* Change YYYY of YYYYMM */
    
    /*  ---------------------------------------------------------------------------------
        Warning:    This stored procedure was a quick hack. 
                    With each new calendar month bits of code below will have to change.
        ---------------------------------------------------------------------------------   */
    
    /* transfer Month transactions: Current Month */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month0 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201205;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 1 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month1 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201204;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 2 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month2 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201203;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 3 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month3 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201202;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 4 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month4 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201201;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 5 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month5 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201112;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 6 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month6 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201111;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 7 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month7 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201110;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 8 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month8 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201109;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 9 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month9 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201108;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 10 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month10 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201107;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 11 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month11 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201106;  /* change YYYYMM when needed */
    
    /* transfer Month transactions: Current Month - 12 */
    UPDATE  BillingReportPaystationTransaction B,
            BillingReportPaystationTransactionInfo P
    SET     B.Month12 = P.Transactions
    WHERE   B.Id = P.Id
    AND     P.Type = 'Month'
    AND     P.Period = 201105;  /* change YYYYMM when needed */
      
END//
delimiter ;
