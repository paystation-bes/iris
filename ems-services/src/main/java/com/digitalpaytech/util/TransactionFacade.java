package com.digitalpaytech.util;

import java.util.Date;

import com.digitalpaytech.domain.CardRetryTransactionType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.DenominationType;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.Tax;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.exception.CryptoException;

public interface TransactionFacade {
    PaymentType findPaymentType(Integer id);
    
    TransactionType findTransactionType(String transactionTypeName);
    
    UnifiedRate findUnifiedRate(Integer customerId, String rateName);
    
    PermitType findPermitType(String transactionTypeName);
    
    MobileNumber findMobileNumber(String number);
    
    PermitIssueType findPermitIssueType(String licensePlate, String stallNumber);
    
    PreAuth findApprovedPreAuth(Long preAuthId);
    
    TransactionType findTransactionTypeById(Integer id);
    
    PermitType findPermitTypeById(Integer id);
    
    PaystationSetting findOrCreatePaystationSetting(Customer customer, String name);
    
    Tax findTax(Integer customerId, String name);
    
    CustomerCardType findCustomerCardType(Integer customerId, Integer cardTypeId, String cardNumber);
    
    CustomerCard findCustomerCard(Integer customerCardTypeId, String cardNumber);
    
    DenominationType findDenominationType(boolean isCoin, Integer denominationAmount);
    
    Permit findLatestOriginalPermit(int locationId, int spaceNumber, int addTimeNumber, String licencePlateNumber);
    
    String decryptCardData(String cardData) throws CryptoException;
    
    String encryptCardData(int purpose, String cardData) throws CryptoException;
    
    String getSha1Hash(String creditCardPanAndExpiry, int hashType) throws CryptoException;
    
    CreditCardType findCreditCardType(String decryptedCardNumber);
    
    CreditCardType findCreditCardTypeByName(String name);
    
    CreditCardType findNonCreditCardType();
    
    Coupon findCoupon(Integer customerId, String couponCode, boolean pre641);
    
    Coupon findRecentlyDeletedCoupon(Integer customerId, String couponCode, boolean pre641, Date minArchiveDate);
    
    void updateCoupon(Coupon coupon);
    
    LicencePlate findLicencePlate(String number);
    
    String getClusterName();
    
    CardType findCardType(Integer cardTypeId);
    
    CardRetryTransactionType findCardRetryTransactionType(Integer id);
    
    void verifyIfDuplicateTransaction(PointOfSale pointOfSale, Date purchasedDate, Integer ticketNumber, boolean checkProcessorTransactionAndPurchase);
    
    boolean isCreditCard(int customerId, String track2);
    
    boolean isCreditCard(PreAuth preAuth);
    
    PreAuthHolding findApprovedPreAuthHolding(Long preAuthId, String authorizationNumber);
    
    boolean isArchivedTransaction(final Date purchasedDate);
}
