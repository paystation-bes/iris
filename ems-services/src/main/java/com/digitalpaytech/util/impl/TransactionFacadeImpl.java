package com.digitalpaytech.util.impl;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.CardRetryTransactionType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.DenominationType;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.Tax;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.DenominationTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.PaymentTypeService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PermitTypeService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.TaxService;
import com.digitalpaytech.service.TransactionTypeService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.report.ReportDataService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.CouponUtil;
import com.digitalpaytech.util.TransactionFacade;
import com.digitalpaytech.util.WebCoreConstants;
import com.esotericsoftware.minlog.Log;

@Component("transactionFacade")
@Transactional(propagation = Propagation.SUPPORTS)
public class TransactionFacadeImpl implements TransactionFacade {
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CustomerCardService customerCardService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private PaymentTypeService paymentTypeService;
    
    @Autowired
    private TransactionTypeService transactionTypeService;
    
    @Autowired
    private UnifiedRateService unifiedRateService;
    
    @Autowired
    private PermitTypeService permitTypeService;
    
    @Autowired
    private MobileNumberService mobileNumberService;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private PermitIssueTypeService permitIssueTypeService;
    
    @Autowired
    private PaystationSettingService paystationSettingService;
    
    @Autowired
    private TaxService taxService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private DenominationTypeService denominationTypeService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    @Autowired
    private CouponService couponService;
    
    @Autowired
    private LicensePlateService licensePlateService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ReportDataService reportDataService;
    
    private int archiveYears;
    private int archiveMonths;
    private int archiveDays;
    private int archiveHours;
    
    @PostConstruct
    public void init() {
        final String[] intervals = this.emsPropertiesService
                .getPropertyValue(EmsPropertiesService.TRANSACTION_ARCHIVE_LIMIT, EmsPropertiesService.DEFAULT_TRANSACTION_ARCHIVE_LIMIT)
                .split("\\.");
        this.archiveYears = parseArchiveLimit(intervals, 0, "Years");
        this.archiveMonths = parseArchiveLimit(intervals, 1, "Months");
        this.archiveDays = parseArchiveLimit(intervals, 2, "Dayrs");
        this.archiveHours = parseArchiveLimit(intervals, 3, "Hours");
    }
    
    private int parseArchiveLimit(final String[] limitExpression, final int index, final String name) {
        int result = 0;
        try {
            result = Integer.parseInt(limitExpression[index]);
        } catch (NumberFormatException nfe) {
            Log.error("Failed to parse the " + name + " part of " + EmsPropertiesService.TRANSACTION_ARCHIVE_LIMIT);
        }
        
        return result;
    }
    
    @Override
    public final PaymentType findPaymentType(final Integer id) {
        return this.paymentTypeService.getPaymentTypesMap().get((byte) id.intValue());
    }
    
    @Override
    public final PreAuth findApprovedPreAuth(final Long preAuthId) {
        return this.preAuthService.getApprovedPreAuthById(preAuthId);
    }
    
    @Override
    public final PreAuthHolding findApprovedPreAuthHolding(final Long preAuthId, final String authorizationNumber) {
        return this.preAuthService.findByApprovedPreAuthIdAuthorizationNumber(preAuthId, authorizationNumber);
    }
    
    @Override
    public final TransactionType findTransactionType(final String transactionTypeName) {
        return this.transactionTypeService.findTransactionType(transactionTypeName);
    }
    
    @Override
    public final UnifiedRate findUnifiedRate(final Integer customerId, final String rateName) {
        return this.unifiedRateService.findRateByNameAndCustomerId(rateName, customerId);
    }
    
    @Override
    public final PermitType findPermitType(final String transactionTypeName) {
        return this.permitTypeService.findPermitType(transactionTypeName);
    }
    
    @Override
    public final MobileNumber findMobileNumber(final String number) {
        return this.mobileNumberService.findMobileNumber(number);
    }
    
    @Override
    public final PermitIssueType findPermitIssueType(final String licensePlate, final String stallNumber) {
        return this.permitIssueTypeService.findPermitIssueType(licensePlate, stallNumber);
    }
    
    @Override
    public final TransactionType findTransactionTypeById(final Integer id) {
        return this.transactionTypeService.getTransactionTypesMap().get(id);
    }
    
    @Override
    public final PermitType findPermitTypeById(final Integer id) {
        return this.permitTypeService.findPermitTypeById(id);
    }
    
    @Override
    public final PaystationSetting findOrCreatePaystationSetting(final Customer customer, final String name) {
        // Paystation is created if not found in findByCustomerIdAndLotName
        final PaystationSetting setting = this.paystationSettingService.findByCustomerIdAndLotName(customer, name);
        return setting;
    }
    
    @Override
    public final Tax findTax(final Integer customerId, final String name) {
        return this.taxService.findByCustomerIdAndName(customerId, name);
    }
    
    @Override
    public final CardType findCardType(final Integer cardTypeId) {
        return this.cardTypeService.getCardTypesMap().get(cardTypeId);
    }
    
    @Override
    public final CustomerCardType findCustomerCardType(final Integer customerId, final Integer cardTypeId, final String cardNumber) {
        return this.customerCardTypeService.findCustomerCardTypeForTransaction(customerId, cardTypeId, cardNumber);
    }
    
    @Override
    public final CustomerCard findCustomerCard(final Integer customerId, final String cardNumber) {
        return this.customerCardService.findCustomerCardByCustomerIdAndCardNumber(customerId, cardNumber);
    }
    
    @Override
    public final DenominationType findDenominationType(final boolean isCoin, final Integer denominationAmount) {
        return this.denominationTypeService.findDenominationType(isCoin, denominationAmount);
    }
    
    @Override
    public final Permit findLatestOriginalPermit(final int locationId, final int spaceNumber, final int addTimeNumber,
        final String licencePlateNumber) {
        return this.permitService.findLatestOriginalPermit(locationId, spaceNumber, addTimeNumber, licencePlateNumber);
    }
    
    @Override
    public final String decryptCardData(final String cardData) throws CryptoException {
        return this.cryptoService.decryptData(cardData);
    }
    
    @Override
    public final String encryptCardData(final int purpose, final String cardData) throws CryptoException {
        return this.cryptoService.encryptData(purpose, cardData);
    }
    
    @Override
    public final String getSha1Hash(final String creditCardPanAndExpiry, final int hashType) throws CryptoException {
        return this.cryptoAlgorithmFactory.getSha1Hash(creditCardPanAndExpiry, hashType);
    }
    
    @Override
    public final CreditCardType findCreditCardType(final String decryptedCardNumber) {
        if (StringUtils.isBlank(decryptedCardNumber)) {
            return this.creditCardTypeService.getCreditCardTypesMap().get(new Integer(CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD));
        }
        
        final int idx = decryptedCardNumber.indexOf(WebCoreConstants.EQUAL_SIGN);
        // Not able to find specific credit card, use name 'CreditCard' (id 7).
        if (idx == -1) {
            return this.creditCardTypeService.getCreditCardTypesMap().get(new Integer(CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD));
        }
        
        CustomerCardType ccCardType;
        final int yyMMLength = 4;
        final int lengAfterEqual = decryptedCardNumber.substring(idx + 1, decryptedCardNumber.length()).length();
        if (lengAfterEqual > yyMMLength) {
            ccCardType = this.customerCardTypeService.getCreditCardTypeByTrack2Data(decryptedCardNumber);
        } else {
            final String acctNum = decryptedCardNumber.substring(0, idx);
            ccCardType = this.customerCardTypeService.getCreditCardTypeByAccountNumber(acctNum);
        }
        
        Integer ccId = null;
        if (ccCardType == null || StringUtils.isBlank(ccCardType.getName())) {
            // Not able to find specific credit card, use name 'CreditCard' (id 7).
            ccId = CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD;
        } else {
            ccId = CardProcessingUtil.getCreditCardTypeId(ccCardType.getName());
            if (ccId == null) {
                ccId = CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD;
            }
        }
        return this.creditCardTypeService.getCreditCardTypesMap().get(ccId);
    }
    
    @Override
    public final CreditCardType findCreditCardTypeByName(final String name) {
        Integer ccId = CardProcessingUtil.getCreditCardTypeId(name);
        if (ccId == null) {
            ccId = CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD;
        }
        return this.creditCardTypeService.getCreditCardTypesMap().get(ccId);
    }
    
    @Override
    public final CreditCardType findNonCreditCardType() {
        // It's not credit card, return 'N/A' (id 0)
        return this.creditCardTypeService.getCreditCardTypesMap().get(0);
    }
    
    @Override
    public final Coupon findCoupon(final Integer customerId, final String couponCode, final boolean pre641) {
        if (!pre641 || CouponUtil.isOfflineCoupon(couponCode)) {
            return this.couponService.findByCustomerIdCouponCodeNoRestriction(customerId, couponCode);
        }
        return this.couponService.findCouponWithShortestCode(customerId, couponCode);
    }
    
    @Override
    public final Coupon findRecentlyDeletedCoupon(final Integer customerId, final String couponCode, final boolean pre641,
        final Date minArchiveDate) {
        if (!pre641 || CouponUtil.isOfflineCoupon(couponCode)) {
            return this.couponService.findDeletedByCustomerIdCouponCodeNoRestriction(customerId, couponCode, minArchiveDate);
        }
        
        return this.couponService.findDeletedCouponWithShortestCode(customerId, couponCode, minArchiveDate);
    }
    
    @Override
    public final void updateCoupon(final Coupon coupon) {
        this.couponService.saveOrUpdateCoupon(coupon);
    }
    
    @Override
    public final LicencePlate findLicencePlate(final String number) {
        return this.licensePlateService.findByNumber(number);
    }
    
    @Override
    public final String getClusterName() {
        return this.clusterMemberService.getClusterName();
    }
    
    @Override
    public final CardRetryTransactionType findCardRetryTransactionType(final Integer id) {
        return this.cardRetryTransactionTypeService.findCardRetryTransactionType(id);
    }
    
    @Override
    public final void verifyIfDuplicateTransaction(final PointOfSale pointOfSale, final Date purchasedDate, final Integer ticketNumber,
        final boolean checkProcessorTransactionAndPurchase) throws DuplicateTransactionRequestException {
        ProcessorTransaction tx = this.processorTransactionService.getApprovedTransaction(pointOfSale.getId(), purchasedDate, ticketNumber);
        if (tx != null) {
            throw new DuplicateTransactionRequestException(getErrorMessage(pointOfSale.getId(), purchasedDate, ticketNumber));
        }
        
        if (checkProcessorTransactionAndPurchase) {
            final Purchase purchase = this.purchaseService.findUniquePurchaseForSms(pointOfSale.getCustomer().getId(), pointOfSale.getId(),
                                                                                    purchasedDate, ticketNumber);
            if (purchase != null) {
                throw new DuplicateTransactionRequestException(getErrorMessage(pointOfSale.getId(), purchasedDate, ticketNumber));
            }
        }
        
        if (isArchivedTransaction(purchasedDate)) {
            // Check if ProcessorTransaction exists.
            tx = this.reportDataService.getApprovedTransaction(pointOfSale.getId(), purchasedDate, ticketNumber);
            if (tx != null) {
                throw new DuplicateTransactionRequestException(getErrorMessage(pointOfSale.getId(), purchasedDate, ticketNumber));
            }
            
            // In the reporting database needs to check both CASH and CC transactions for duplications.
            final Purchase purchase = this.reportDataService.findUniquePurchaseForSms(pointOfSale.getCustomer().getId(), pointOfSale.getId(),
                                                                                      purchasedDate, ticketNumber);
            if (purchase != null) {
                throw new DuplicateTransactionRequestException(getErrorMessage(pointOfSale.getId(), purchasedDate, ticketNumber));
            }
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public boolean isCreditCard(final int customerId, final String track2Data) {
        final CustomerCardType cct = this.customerCardTypeService.getCardTypeByTrack2DataOrAccountNumber(customerId, track2Data);
        return (cct != null) && (cct.getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
    }
    
    @Override
    public final boolean isCreditCard(final PreAuth preAuth) {
        return this.creditCardTypeService.getCreditCardTypeByName(preAuth.getCardType()) != null;
    }
    
    private String getErrorMessage(final Integer pointOfSaleId, final Date purchasedDate, final Integer ticketNumber) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Transaction with pointOfSaleId: ").append(pointOfSaleId).append(", purchasedDate: ").append(purchasedDate)
                .append(", ticketNumber: ");
        bdr.append(ticketNumber).append(" already exists in the database.");
        return bdr.toString();
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final void setPaymentTypeService(final PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }
    
    public final void setTransactionTypeService(final TransactionTypeService transactionTypeService) {
        this.transactionTypeService = transactionTypeService;
    }
    
    public final void setUnifiedRateService(final UnifiedRateService unifiedRateService) {
        this.unifiedRateService = unifiedRateService;
    }
    
    public final void setPermitTypeService(final PermitTypeService permitTypeService) {
        this.permitTypeService = permitTypeService;
    }
    
    public final void setMobileNumberService(final MobileNumberService mobileNumberService) {
        this.mobileNumberService = mobileNumberService;
    }
    
    public final void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public final void setPermitIssueTypeService(final PermitIssueTypeService permitIssueTypeService) {
        this.permitIssueTypeService = permitIssueTypeService;
    }
    
    public final void setPreAuthService(final PreAuthService preAuthService) {
        this.preAuthService = preAuthService;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final void setTaxService(final TaxService taxService) {
        this.taxService = taxService;
    }
    
    public final void setCustomerCardService(final CustomerCardService customerCardService) {
        this.customerCardService = customerCardService;
    }
    
    public final void setDenominationTypeService(final DenominationTypeService denominationTypeService) {
        this.denominationTypeService = denominationTypeService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setCreditCardTypeService(final CreditCardTypeService creditCardTypeService) {
        this.creditCardTypeService = creditCardTypeService;
    }
    
    public final void setCouponService(final CouponService couponService) {
        this.couponService = couponService;
    }
    
    public final void setLicensePlateService(final LicensePlateService licensePlateService) {
        this.licensePlateService = licensePlateService;
        
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    public final void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public final void setCardRetryTransactionTypeService(final CardRetryTransactionTypeService cardRetryTransactionTypeService) {
        this.cardRetryTransactionTypeService = cardRetryTransactionTypeService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    @Override
    public boolean isArchivedTransaction(final Date purchasedDate) {
        Calendar archiveDate = Calendar.getInstance();
        archiveDate.add(Calendar.YEAR, -this.archiveYears);
        archiveDate.add(Calendar.MONTH, -this.archiveMonths);
        archiveDate.add(Calendar.DAY_OF_MONTH, -this.archiveDays);
        archiveDate.add(Calendar.HOUR_OF_DAY, -this.archiveHours);
        
        return (purchasedDate != null) && purchasedDate.before(archiveDate.getTime());
    }
    
}
