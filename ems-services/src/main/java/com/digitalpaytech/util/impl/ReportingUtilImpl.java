package com.digitalpaytech.util.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportFilterValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ReportUIFilterType;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.ReportFilterValueService;
import com.digitalpaytech.service.ReportRepeatTypeService;
import com.digitalpaytech.service.ReportStatusTypeService;
import com.digitalpaytech.service.ReportTypeService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.StandardConstants;

@Component("reportingUtil")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReportingUtilImpl implements ReportingUtil, ReportingConstants {
    
    private static Map<Integer, String> reportFilters;
    private static Map<String, String> processorMap;
    private Map<Byte, Map<Integer, List<ReportUIFilterType>>> uiReportFilters;
    
    @Autowired
    private ReportFilterValueService reportFilterValueService;
    @Autowired
    private ReportTypeService reportTypeService;
    @Autowired
    private ReportStatusTypeService reportStatusTypeService;
    @Autowired
    private ReportRepeatTypeService reportRepeatTypeService;
    @Autowired
    private ProcessorService processorService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private EntityDao entityDao;
    
    public final void setReportFilterValueService(final ReportFilterValueService reportFilterValueService) {
        this.reportFilterValueService = reportFilterValueService;
    }
    
    public final void setReportTypeService(final ReportTypeService reportTypeService) {
        this.reportTypeService = reportTypeService;
    }
    
    public final void setReportStatusTypeService(final ReportStatusTypeService reportStatusTypeService) {
        this.reportStatusTypeService = reportStatusTypeService;
    }
    
    public final void setReportRepeatTypeService(final ReportRepeatTypeService reportRepeatTypeService) {
        this.reportRepeatTypeService = reportRepeatTypeService;
    }
    
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    public final void setProcessorService(final ProcessorService processorService) {
        this.processorService = processorService;
    }
    
    @PostConstruct
    public final void fillReportFilterMap() {
        if (reportFilters == null) {
            final List<ReportFilterValue> filterValueList = this.reportFilterValueService.findAll();
            reportFilters = new HashMap<Integer, String>();
            if (filterValueList.size() != 0) {
                for (ReportFilterValue filterValue : filterValueList) {
                    reportFilters.put(filterValue.getReportFilterValue(), filterValue.getName());
                }
            }
        }
        fillUIReportFilters();
    }
    
    @PostConstruct
    public final void fillProcessorMap() {
        if (this.processorService == null) {
            return;
        }
        final List<Processor> processorList = this.processorService.findAll();
        processorMap = new HashMap<String, String>();
        for (Processor processor : processorList) {
            processorMap.put(processor.getName(), this.messageSource.getMessage(processor.getName(), null, null));
        }
        
    }
    
    @Override
    public List<ReportUIFilterType> getFilterList(final int reportType, final int filterType) {
        final Map<Integer, List<ReportUIFilterType>> allFilters = this.uiReportFilters.get((byte) reportType);
        return allFilters.get(filterType);
    }
    
    @Override
    public final Map<Integer, List<ReportUIFilterType>> getReportAllFilters(final int reportType) {
        return this.uiReportFilters.get((byte) reportType);
    }
    
    @Override
    public String findFilterString(final Integer filterValueTypeId) {
        if (filterValueTypeId == null) {
            return null;
        }
        if (!reportFilters.containsKey(filterValueTypeId.intValue())) {
            return null;
        }
        return reportFilters.get(filterValueTypeId);
    }
    
    @Override
    public String getPropertyEN(final String messageKey) {
        if (WebCoreConstants.EMPTY_STRING.equals(messageKey)) {
            return WebCoreConstants.EMPTY_STRING;
        }
        return this.messageSource.getMessage(messageKey, null, null);
    }
    
    @Override
    public String getProperty(final String messageKey, final Locale locale) {
        if (WebCoreConstants.EMPTY_STRING.equals(messageKey)) {
            return WebCoreConstants.EMPTY_STRING;
        }
        return this.messageSource.getMessage(messageKey, null, locale);
    }
    
    @Override
    public final String getPropertyEN(final String messageKey, final String[] args) {
        if (WebCoreConstants.EMPTY_STRING.equals(messageKey)) {
            return WebCoreConstants.EMPTY_STRING;
        }
        return this.messageSource.getMessage(messageKey, args, null);
    }
    
    @Override
    public final String getProperty(final String messageKey, final String[] args, final Locale locale) {
        if (WebCoreConstants.EMPTY_STRING.equals(messageKey)) {
            return WebCoreConstants.EMPTY_STRING;
        }
        return this.messageSource.getMessage(messageKey, args, locale);
    }
    
    @Override
    public final void calculateNextReportDate(final ReportDefinition reportDefinition, final Date initialDateGmt, final String timeZone,
        final boolean isInitial) {
        
        final Calendar nextReportDate = Calendar.getInstance();
        nextReportDate.setTime(initialDateGmt);
        // add timeZoneOffset to calculate correct day
        nextReportDate.setTimeZone(TimeZone.getTimeZone(timeZone));
        
        switch (reportDefinition.getReportRepeatType().getId()) {
            case ReportingConstants.REPORT_REPEAT_TYPE_DAILY:
                calculateDaily(reportDefinition, nextReportDate, isInitial);
                break;
            case ReportingConstants.REPORT_REPEAT_TYPE_WEEKLY:
                calculateWeekly(reportDefinition, nextReportDate, isInitial);
                break;
            case ReportingConstants.REPORT_REPEAT_TYPE_MONTHLY:
                calculateMonthly(reportDefinition, nextReportDate, isInitial);
                break;
            default:
                break;
        }
        
        final String[] reportRepeatTime = reportDefinition.getReportRepeatTimeOfDay().split(":");
        nextReportDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(reportRepeatTime[0]));
        nextReportDate.set(Calendar.MINUTE, Integer.parseInt(reportRepeatTime[1]));
        
        nextReportDate.set(Calendar.SECOND, 0);
        
        reportDefinition.setScheduledToRunGmt(nextReportDate.getTime());
        
    }
    
    private void calculateDaily(final ReportDefinition reportDefinition, final Calendar nextReportDate, final boolean isInitial) {
        if (!isInitial) {
            final Byte reportRepeatFrequencyD = reportDefinition.getReportRepeatFrequency();
            
            nextReportDate.add(Calendar.DAY_OF_YEAR, reportRepeatFrequencyD == null ? 1 : reportRepeatFrequencyD);
        }
    }
    
    private void calculateWeekly(final ReportDefinition reportDefinition, final Calendar nextReportDate, final boolean isInitial) {
        if (isInitial) {
            final int reportRepeatFrequencyW = reportDefinition.getReportRepeatFrequency();
            final String[] reportRepeatDaysW = reportDefinition.getReportRepeatDays().split(StandardConstants.STRING_COMMA);
            final int selectedDay = nextReportDate.get(Calendar.DAY_OF_WEEK);
            final int firstDay = Integer.parseInt(reportRepeatDaysW[0]);
            boolean startsSelectedDay = false;
            int followingDay = firstDay;
            for (String day : reportRepeatDaysW) {
                final int currentDay = Integer.parseInt(day);
                if (selectedDay == currentDay) {
                    startsSelectedDay = true;
                } else if (followingDay == firstDay && selectedDay < currentDay) {
                    followingDay = currentDay;
                }
            }
            if (!startsSelectedDay) {
                if (followingDay == firstDay && firstDay < selectedDay) {
                    nextReportDate.add(Calendar.WEEK_OF_YEAR, reportRepeatFrequencyW);
                }
                nextReportDate.set(Calendar.DAY_OF_WEEK, followingDay);
            }
        } else {
            final int reportRepeatFrequencyW = reportDefinition.getReportRepeatFrequency();
            final String[] reportRepeatDaysW = reportDefinition.getReportRepeatDays().split(StandardConstants.STRING_COMMA);
            if (reportRepeatDaysW.length == 1) {
                nextReportDate.add(Calendar.WEEK_OF_YEAR, reportRepeatFrequencyW);
                nextReportDate.set(Calendar.DAY_OF_WEEK, Integer.parseInt(reportRepeatDaysW[0]));
            } else {
                final int lastReportDay = nextReportDate.get(Calendar.DAY_OF_WEEK);
                int i = 0;
                while (i < reportRepeatDaysW.length && lastReportDay >= Integer.parseInt(reportRepeatDaysW[i])) {
                    i++;
                }
                if (i == reportRepeatDaysW.length) {
                    nextReportDate.add(Calendar.WEEK_OF_YEAR, reportRepeatFrequencyW);
                    //System.out.println(nextReportDate.getTime());
                    nextReportDate.set(Calendar.DAY_OF_WEEK, Integer.parseInt(reportRepeatDaysW[0]));
                    //System.out.println(nextReportDate.getTime());
                } else {
                    nextReportDate.set(Calendar.DAY_OF_WEEK, Integer.parseInt(reportRepeatDaysW[i]));
                }
            }
        }
    }
    
    private void calculateMonthly(final ReportDefinition reportDefinition, final Calendar nextReportDate, final boolean isInitial) {
        if (isInitial) {
            final int reportRepeatFrequencyM = reportDefinition.getReportRepeatFrequency();
            final String[] reportRepeatDaysM = reportDefinition.getReportRepeatDays().split(StandardConstants.STRING_COMMA);
            final int selectedDayMonth = nextReportDate.get(Calendar.DAY_OF_MONTH);
            final int firstDayMonth = Integer.parseInt(reportRepeatDaysM[0]);
            boolean startsSelectedDayMonth = false;
            int followingDayMonth = firstDayMonth;
            for (String day : reportRepeatDaysM) {
                final int currentDayMonth = Integer.parseInt(day);
                if (selectedDayMonth == currentDayMonth) {
                    startsSelectedDayMonth = true;
                } else if (followingDayMonth == firstDayMonth && selectedDayMonth < currentDayMonth) {
                    followingDayMonth = currentDayMonth;
                }
            }
            if (!startsSelectedDayMonth) {
                if (followingDayMonth == firstDayMonth && firstDayMonth < selectedDayMonth) {
                    nextReportDate.add(Calendar.MONTH, reportRepeatFrequencyM);
                }
                
                final int currMonth = nextReportDate.get(Calendar.MONTH);
                nextReportDate.set(Calendar.DAY_OF_MONTH, followingDayMonth);
                // Needed for February. Setting Day to 31 of February will
                // make the Date 3rd of March.
                if (currMonth != nextReportDate.get(Calendar.MONTH)) {
                    nextReportDate.set(Calendar.DAY_OF_MONTH, 1);
                }
            }
        } else {
            final int reportRepeatFrequencyM = reportDefinition.getReportRepeatFrequency();
            final String[] reportRepeatDaysM = reportDefinition.getReportRepeatDays().split(StandardConstants.STRING_COMMA);
            if (reportRepeatDaysM.length == 1) {
                final int lastReportDay = nextReportDate.get(Calendar.DAY_OF_MONTH);
                final int newReportDay = Integer.parseInt(reportRepeatDaysM[0]);
                if (newReportDay > StandardConstants.CONSTANT_28 && lastReportDay == 1) {
                    nextReportDate.set(Calendar.DAY_OF_MONTH, newReportDay);
                } else {
                    nextReportDate.add(Calendar.MONTH, reportRepeatFrequencyM);
                    final int currMonth = nextReportDate.get(Calendar.MONTH);
                    nextReportDate.set(Calendar.DAY_OF_MONTH, newReportDay);
                    // Needed for February. Setting Day to 31 of February will
                    // make the Date 3rd of March.
                    if (currMonth != nextReportDate.get(Calendar.MONTH)) {
                        nextReportDate.set(Calendar.DAY_OF_MONTH, 1);
                    }
                }
            } else {
                final int lastReportDay = nextReportDate.get(Calendar.DAY_OF_MONTH);
                int i = 0;
                while (i < reportRepeatDaysM.length && lastReportDay >= Integer.parseInt(reportRepeatDaysM[i])) {
                    i++;
                }
                final int currMonth = nextReportDate.get(Calendar.MONTH);
                if (i == reportRepeatDaysM.length) {
                    nextReportDate.add(Calendar.MONTH, reportRepeatFrequencyM);
                    nextReportDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(reportRepeatDaysM[0]));
                } else {
                    nextReportDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(reportRepeatDaysM[i]));
                    // Needed for February. Setting Day to 31 of February will
                    // make the Date 3rd of March.
                    if (currMonth != nextReportDate.get(Calendar.MONTH)) {
                        nextReportDate.set(Calendar.DAY_OF_MONTH, 1);
                    }
                }
            }
        }
    }
    
    public void fillUIReportFilters() {
        if (this.uiReportFilters == null) {
            this.uiReportFilters = new HashMap<Byte, Map<Integer, List<ReportUIFilterType>>>();
            
            this.uiReportFilters.put(REPORT_TYPE_COMMON, fillCommonFilters());
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_ALL, fillTransactionFilters(REPORT_TYPE_TRANSACTION_ALL));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_CASH, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CASH));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_CREDIT_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CREDIT_CARD));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_CC_REFUND, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_REFUND));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_CC_PROCESSING, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_PROCESSING));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_CC_RETRY, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_RETRY));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_PATROLLER_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_PATROLLER_CARD));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_SMART_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_SMART_CARD));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_VALUE_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_VALUE_CARD));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_RATE, fillRateFilters(false));
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_RATE_SUMMARY, fillRateFilters(true));
            this.uiReportFilters.put(REPORT_TYPE_STALL_REPORTS, fillStallFilters());
            this.uiReportFilters.put(REPORT_TYPE_COUPON_USAGE_SUMMARY, fillCouponUsageFilters());
            this.uiReportFilters.put(REPORT_TYPE_AUDIT_REPORTS, fillAuditFilters());
            this.uiReportFilters.put(REPORT_TYPE_REPLENISH_REPORTS, fillReplenishFilters());
            this.uiReportFilters.put(REPORT_TYPE_PAY_STATION_SUMMARY, fillPayStationSummaryFilters());
            this.uiReportFilters.put(REPORT_TYPE_TAX_REPORT, fillTaxFilters());
            this.uiReportFilters.put(REPORT_TYPE_TRANSACTION_DELAY, fillTransactionFilters(REPORT_TYPE_TRANSACTION_DELAY));
            this.uiReportFilters.put(REPORT_TYPE_INVENTORY, fillInventoryFilters());
            this.uiReportFilters.put(REPORT_TYPE_PAYSTATION_INVENTORY, fillInventoryFilters());
            this.uiReportFilters.put(REPORT_TYPE_MAINTENANCE_SUMMARY, fillAlertFilters());
            this.uiReportFilters.put(REPORT_TYPE_COLLECTION_SUMMARY, fillAlertFilters());
            
        }
    }
    
    public Map<Integer, List<ReportUIFilterType>> fillCommonFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        
        final List<ReportType> reportTypeList = this.reportTypeService.loadAll();
        final List<ReportUIFilterType> reportTypeFilters = new ArrayList<ReportUIFilterType>(reportTypeList.size());
        // Added control over report type for each customer admin. Note that this is for the minimal & fastest code changes just to get 7.0 out the door.
        // We should consider discussing what report will look like in the future or design it to be super-flexible.
        final List<ReportUIFilterType> saReportTypeFilters = new ArrayList<ReportUIFilterType>(reportTypeList.size());
        final List<ReportUIFilterType> pcaReportTypeFilters = new ArrayList<ReportUIFilterType>(reportTypeList.size());
        final List<ReportUIFilterType> ccaReportTypeFilters = new ArrayList<ReportUIFilterType>(reportTypeList.size());
        for (ReportType reportType : reportTypeList) {
            final ReportUIFilterType filter = new ReportUIFilterType(reportType.getId(), reportType.getName());
            reportTypeFilters.add(filter);
            if (reportType.isAllowSystemAdmin()) {
                saReportTypeFilters.add(filter);
            }
            if (reportType.isAllowParentCustomerAdmin()) {
                pcaReportTypeFilters.add(filter);
            }
            if (reportType.isAllowChildCustomerAdmin()) {
                ccaReportTypeFilters.add(filter);
            }
        }
        inputMap.put(REPORT_FILTER_TYPE_REPORT_TYPE, reportTypeFilters);
        inputMap.put(REPORT_FILTER_TYPE_REPORT_TYPE_SYSADMIN, saReportTypeFilters);
        inputMap.put(REPORT_FILTER_TYPE_REPORT_TYPE_PARENT_CUSTADMIN, pcaReportTypeFilters);
        inputMap.put(REPORT_FILTER_TYPE_REPORT_TYPE_CHILD_CUSTADMIN, ccaReportTypeFilters);
        
        final List<ReportStatusType> reportStatusTypeList = this.reportStatusTypeService.loadAll();
        final List<ReportUIFilterType> reportStatusTypeFilters = new ArrayList<ReportUIFilterType>();
        for (ReportStatusType reportStatusType : reportStatusTypeList) {
            reportStatusTypeFilters.add(new ReportUIFilterType(reportStatusType.getId(), reportStatusType.getName()));
        }
        inputMap.put(REPORT_FILTER_TYPE_REPORT_STATUS_TYPE, reportStatusTypeFilters);
        
        final List<ReportRepeatType> reportRepeatTypeList = this.reportRepeatTypeService.loadAll();
        final List<ReportUIFilterType> reportRepeatTypeFilters = new ArrayList<ReportUIFilterType>();
        for (ReportRepeatType reportRepeatType : reportRepeatTypeList) {
            if (reportRepeatType.getId() > 1) {
                reportRepeatTypeFilters.add(new ReportUIFilterType(reportRepeatType.getId(), reportRepeatType.getName()));
            }
        }
        inputMap.put(REPORT_FILTER_TYPE_REPORT_REPEAT_TYPE, reportRepeatTypeFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillTransactionFilters(final byte reportType) {
        
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        List<ReportUIFilterType> primaryDateFilters;
        if (reportType == REPORT_TYPE_TRANSACTION_DELAY) {
            primaryDateFilters = getTransactionDelayDateOptions();
        } else {
            primaryDateFilters = getDateOptions();
        }
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        if (REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType || REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
            // Secondary Date/Time
            final List<ReportUIFilterType> secondaryDateFilters = getDateOptions();
            inputMap.put(REPORT_FILTER_TYPE_SECONDARY_DATE, secondaryDateFilters);
        }
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        // TODO exclude from Retry if not completely deleted
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Space Number
        final List<ReportUIFilterType> spaceNumberFilters = new ArrayList<ReportUIFilterType>();
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_NA, findFilterString(REPORT_FILTER_SPACE_NUMBER_NA)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_NO_STALL, findFilterString(REPORT_FILTER_SPACE_NUMBER_NO_STALL)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_ALL, findFilterString(REPORT_FILTER_SPACE_NUMBER_ALL)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_EQUAL, findFilterString(REPORT_FILTER_SPACE_NUMBER_EQUAL)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_GREATER, findFilterString(REPORT_FILTER_SPACE_NUMBER_GREATER)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_LESS, findFilterString(REPORT_FILTER_SPACE_NUMBER_LESS)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_BETWEEN, findFilterString(REPORT_FILTER_SPACE_NUMBER_BETWEEN)));
        inputMap.put(REPORT_FILTER_TYPE_SPACE_NUMBER, spaceNumberFilters);
        
        if (REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType || REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
            || REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType || REPORT_TYPE_TRANSACTION_CC_RETRY == reportType
            || REPORT_TYPE_TRANSACTION_SMART_CARD == reportType || REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType
            || REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
            // Card Number
            final List<ReportUIFilterType> cardNumberFilters = new ArrayList<ReportUIFilterType>();
            cardNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_CARD_NUMBER_ALL, findFilterString(REPORT_FILTER_CARD_NUMBER_ALL)));
            cardNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_CARD_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_CARD_NUMBER_SPECIFIC)));
            inputMap.put(REPORT_FILTER_TYPE_CARD_NUMBER, cardNumberFilters);
        }
        if (REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType || REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
            // Approval Status
            final List<ReportUIFilterType> approvalStatusFilters = new ArrayList<ReportUIFilterType>();
            approvalStatusFilters.add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_ALL, findFilterString(REPORT_FILTER_APPROVAL_STATUS_ALL)));
            approvalStatusFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_PRE_AUTH, findFilterString(REPORT_FILTER_APPROVAL_STATUS_PRE_AUTH)));
            approvalStatusFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_REAL_TIME, findFilterString(REPORT_FILTER_APPROVAL_STATUS_REAL_TIME)));
            approvalStatusFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_BATCHED, findFilterString(REPORT_FILTER_APPROVAL_STATUS_BATCHED)));
            approvalStatusFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_OFFLINE, findFilterString(REPORT_FILTER_APPROVAL_STATUS_OFFLINE)));
            approvalStatusFilters.add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_RECOVERABLE,
                    findFilterString(REPORT_FILTER_APPROVAL_STATUS_RECOVERABLE)));
            approvalStatusFilters.add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_UNCLOSEABLE,
                    findFilterString(REPORT_FILTER_APPROVAL_STATUS_UNCLOSEABLE)));
            approvalStatusFilters.add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_ENCRYPTION_ERROR,
                    findFilterString(REPORT_FILTER_APPROVAL_STATUS_ENCRYPTION_ERROR)));
            approvalStatusFilters.add(new ReportUIFilterType(REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED,
                    findFilterString(REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED)));
            inputMap.put(REPORT_FILTER_TYPE_APPROVAL_STATUS, approvalStatusFilters);
        }
        // Ticket Number
        final List<ReportUIFilterType> ticketNumberFilters = new ArrayList<ReportUIFilterType>();
        ticketNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_TICKET_NUMBER_ALL, findFilterString(REPORT_FILTER_TICKET_NUMBER_ALL)));
        ticketNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_TICKET_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_TICKET_NUMBER_SPECIFIC)));
        ticketNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_TICKET_NUMBER_BETWEEN, findFilterString(REPORT_FILTER_TICKET_NUMBER_BETWEEN)));
        inputMap.put(REPORT_FILTER_TYPE_TICKET_NUMBER, ticketNumberFilters);
        if (REPORT_TYPE_TRANSACTION_CC_RETRY != reportType) {
            // Coupon Number
            final List<ReportUIFilterType> couponNumberFilters = new ArrayList<ReportUIFilterType>();
            couponNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_NA, findFilterString(REPORT_FILTER_COUPON_NUMBER_NA)));
            couponNumberFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_ALL_COUPONS, findFilterString(REPORT_FILTER_COUPON_NUMBER_ALL_COUPONS)));
            couponNumberFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_NO_COUPONS, findFilterString(REPORT_FILTER_COUPON_NUMBER_NO_COUPONS)));
            couponNumberFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_COUPON_NUMBER_SPECIFIC)));
            inputMap.put(REPORT_FILTER_TYPE_COUPON_NUMBER, couponNumberFilters);
        }
        // Transaction Type
        final List<ReportUIFilterType> transactionTypeFilters = new ArrayList<ReportUIFilterType>();
        if (REPORT_TYPE_TRANSACTION_CC_REFUND != reportType && REPORT_TYPE_TRANSACTION_CC_PROCESSING != reportType
            && REPORT_TYPE_TRANSACTION_CC_RETRY != reportType) {
            transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_ALL, findFilterString(REPORT_FILTER_TRANSACTION_ALL)));
            transactionTypeFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_REGULAR, findFilterString(REPORT_FILTER_TRANSACTION_REGULAR)));
            transactionTypeFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_ADD_TIME, findFilterString(REPORT_FILTER_TRANSACTION_ADD_TIME)));
            transactionTypeFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_MONTHLY, findFilterString(REPORT_FILTER_TRANSACTION_MONTHLY)));
            if (REPORT_TYPE_TRANSACTION_VALUE_CARD != reportType) {
                if (REPORT_TYPE_TRANSACTION_PATROLLER_CARD != reportType) {
                    //TODO To be added when Citations are implemented
                    //                    if (REPORT_TYPE_TRANSACTION_SMART_CARD != reportType) {
                    //                        transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_CITATION,
                    //                                findFilterString(REPORT_FILTER_TRANSACTION_CITATION)));
                    //                    }
                    transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_SC_RECHARGE,
                            findFilterString(REPORT_FILTER_TRANSACTION_SC_RECHARGE)));
                } else {
                    transactionTypeFilters
                            .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_DEPOSIT, findFilterString(REPORT_FILTER_TRANSACTION_DEPOSIT)));
                }
                if (REPORT_TYPE_TRANSACTION_ALL == reportType || REPORT_TYPE_TRANSACTION_CASH == reportType) {
                    transactionTypeFilters
                            .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_TEST, findFilterString(REPORT_FILTER_TRANSACTION_TEST)));
                }
                if (REPORT_TYPE_TRANSACTION_ALL == reportType) {
                    transactionTypeFilters
                            .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_REPLENISH, findFilterString(REPORT_FILTER_TRANSACTION_REPLENISH)));
                    transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE,
                            findFilterString(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE)));
                } else if (REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType) {
                    transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE,
                            findFilterString(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE)));
                }
            }
        } else if (REPORT_TYPE_TRANSACTION_CC_REFUND == reportType || REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
            if (REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
                transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_ALL, findFilterString(REPORT_FILTER_TRANSACTION_ALL)));
                transactionTypeFilters
                        .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_NON_REFUND, findFilterString(REPORT_FILTER_TRANSACTION_NON_REFUND)));
            }
            transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_REFUND, findFilterString(REPORT_FILTER_TRANSACTION_REFUND)));
            transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE,
                    findFilterString(REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE)));
        }
        inputMap.put(REPORT_FILTER_TYPE_OTHER_PARAMETERS, transactionTypeFilters);
        if (REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
            // Retry Type
            final List<ReportUIFilterType> retryTypeFilters = new ArrayList<ReportUIFilterType>();
            retryTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_RETRY_ALL, findFilterString(REPORT_FILTER_RETRY_ALL)));
            retryTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_RETRY_BATCHED, findFilterString(REPORT_FILTER_RETRY_BATCHED)));
            retryTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_RETRY_STORE_FORWARD, findFilterString(REPORT_FILTER_RETRY_STORE_FORWARD)));
            inputMap.put(REPORT_FILTER_TYPE_OTHER_PARAMETERS, retryTypeFilters);
        }
        
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_DAY, findFilterString(REPORT_FILTER_GROUP_BY_DAY)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MONTH, findFilterString(REPORT_FILTER_GROUP_BY_MONTH)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LOCATION, findFilterString(REPORT_FILTER_GROUP_BY_LOCATION)));
        groupByFilters
                .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION)));
        if (REPORT_TYPE_TRANSACTION_CC_PROCESSING != reportType && REPORT_TYPE_TRANSACTION_CC_RETRY != reportType) {
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_TRANS_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_TRANS_TYPE)));
        }
        if (REPORT_TYPE_TRANSACTION_ALL == reportType) {
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAYMENT_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_PAYMENT_TYPE)));
        } else if (REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType || REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
                   || REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_CARD_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_CARD_TYPE)));
            groupByFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT, findFilterString(REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAYMENT_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_PAYMENT_TYPE)));
        } else if (REPORT_TYPE_TRANSACTION_CC_RETRY == reportType || REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_CARD_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_CARD_TYPE)));
            groupByFilters
                    .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT, findFilterString(REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT)));
        } else if (REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType || REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_CARD_NUMBER, findFilterString(REPORT_FILTER_GROUP_BY_CARD_NUMBER)));
        }
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public Map<Integer, List<ReportUIFilterType>> fillRateFilters(final boolean isSummary) {
        
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        final List<ReportUIFilterType> primaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        // TODO exclude from Retry if not completely deleted
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Transaction Type
        final List<ReportUIFilterType> transactionTypeFilters = new ArrayList<ReportUIFilterType>();
        transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_ALL, findFilterString(REPORT_FILTER_TRANSACTION_ALL)));
        transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_REGULAR, findFilterString(REPORT_FILTER_TRANSACTION_REGULAR)));
        transactionTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_ADD_TIME, findFilterString(REPORT_FILTER_TRANSACTION_ADD_TIME)));
        transactionTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_TRANSACTION_CANCELLED, findFilterString(REPORT_FILTER_TRANSACTION_CANCELLED)));
        inputMap.put(REPORT_FILTER_TYPE_OTHER_PARAMETERS, transactionTypeFilters);
        
        if (!isSummary) {
            // Group By
            final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_DAY, findFilterString(REPORT_FILTER_GROUP_BY_DAY)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_DAY_OF_WEEK, findFilterString(REPORT_FILTER_GROUP_BY_DAY_OF_WEEK)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MONTH, findFilterString(REPORT_FILTER_GROUP_BY_MONTH)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LOCATION, findFilterString(REPORT_FILTER_GROUP_BY_LOCATION)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE,
                    findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION)));
            groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_TRANS_TYPE, findFilterString(REPORT_FILTER_GROUP_BY_TRANS_TYPE)));
            inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        }
        return inputMap;
    }
    
    public Map<Integer, List<ReportUIFilterType>> fillCouponUsageFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        final List<ReportUIFilterType> primaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        // TODO exclude from Retry if not completely deleted
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Coupon Number
        final List<ReportUIFilterType> couponNumberFilters = new ArrayList<ReportUIFilterType>();
        couponNumberFilters
                .add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_ALL_COUPONS, findFilterString(REPORT_FILTER_COUPON_NUMBER_ALL_COUPONS)));
        couponNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_COUPON_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_COUPON_NUMBER_SPECIFIC)));
        inputMap.put(REPORT_FILTER_TYPE_COUPON_NUMBER, couponNumberFilters);
        // Coupon Type
        final List<ReportUIFilterType> couponTypeFilters = new ArrayList<ReportUIFilterType>();
        couponTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_COUPON_TYPE_ALL, findFilterString(REPORT_FILTER_COUPON_TYPE_ALL)));
        couponTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_COUPON_TYPE_PERCENTAGE, findFilterString(REPORT_FILTER_COUPON_TYPE_PERCENTAGE)));
        couponTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_COUPON_TYPE_DOLLAR_BASED, findFilterString(REPORT_FILTER_COUPON_TYPE_DOLLAR_BASED)));
        inputMap.put(REPORT_FILTER_TYPE_COUPON_TYPE, couponTypeFilters);
        return inputMap;
    }
    
    public Map<Integer, List<ReportUIFilterType>> fillStallFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Space Number
        final List<ReportUIFilterType> spaceNumberFilters = new ArrayList<ReportUIFilterType>();
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_ALL, findFilterString(REPORT_FILTER_SPACE_NUMBER_ALL)));
        spaceNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_SPACE_NUMBER_BETWEEN, findFilterString(REPORT_FILTER_SPACE_NUMBER_BETWEEN)));
        inputMap.put(REPORT_FILTER_TYPE_SPACE_NUMBER, spaceNumberFilters);
        // Report Type
        final List<ReportUIFilterType> reportTypeFilters = new ArrayList<ReportUIFilterType>();
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_ALL, findFilterString(REPORT_FILTER_REPORT_ALL)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_VALID, findFilterString(REPORT_FILTER_REPORT_VALID)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_EXPIRED, findFilterString(REPORT_FILTER_REPORT_EXPIRED)));
        inputMap.put(REPORT_FILTER_TYPE_OTHER_PARAMETERS, reportTypeFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillInventoryFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_ORGANIZATION, findFilterString(REPORT_FILTER_GROUP_BY_ORGANIZATION)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LOCATION, findFilterString(REPORT_FILTER_GROUP_BY_LOCATION)));
        groupByFilters
                .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME, findFilterString(REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME)));
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillTaxFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        final List<ReportUIFilterType> primaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_DAY, findFilterString(REPORT_FILTER_GROUP_BY_DAY)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MONTH, findFilterString(REPORT_FILTER_GROUP_BY_MONTH)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LOCATION, findFilterString(REPORT_FILTER_GROUP_BY_LOCATION)));
        groupByFilters
                .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_TAX_NAME, findFilterString(REPORT_FILTER_GROUP_BY_TAX_NAME)));
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillPayStationSummaryFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Sort By
        final List<ReportUIFilterType> sortByFilters = new ArrayList<ReportUIFilterType>();
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_PAY_STATION_NAME, findFilterString(REPORT_FILTER_SORT_BY_PAY_STATION_NAME)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_VOLTAGE, findFilterString(REPORT_FILTER_SORT_BY_VOLTAGE)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_COIN_COUNT, findFilterString(REPORT_FILTER_SORT_BY_COIN_COUNT)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_BILL_COUNT, findFilterString(REPORT_FILTER_SORT_BY_BILL_COUNT)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_PAPER_STATUS, findFilterString(REPORT_FILTER_SORT_BY_PAPER_STATUS)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_RUNNING_TOTAL, findFilterString(REPORT_FILTER_SORT_BY_RUNNING_TOTAL)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_COUNT,
                findFilterString(REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_COUNT)));
        sortByFilters.add(new ReportUIFilterType(REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_AMOUNT,
                findFilterString(REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_AMOUNT)));
        inputMap.put(REPORT_FILTER_TYPE_SORT_BY, sortByFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillReplenishFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        final List<ReportUIFilterType> primaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Report Number
        final List<ReportUIFilterType> reportNumberFilters = new ArrayList<ReportUIFilterType>();
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_ALL, findFilterString(REPORT_FILTER_REPORT_NUMBER_ALL)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_GREATER, findFilterString(REPORT_FILTER_REPORT_NUMBER_GREATER)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_LESS, findFilterString(REPORT_FILTER_REPORT_NUMBER_LESS)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_REPORT_NUMBER_SPECIFIC)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_BETWEEN, findFilterString(REPORT_FILTER_REPORT_NUMBER_BETWEEN)));
        inputMap.put(REPORT_FILTER_TYPE_TICKET_NUMBER, reportNumberFilters);
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MACHINE, findFilterString(REPORT_FILTER_GROUP_BY_MACHINE)));
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillAlertFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        // TODO exclude from Retry if not completely deleted
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_LOCATION, findFilterString(REPORT_FILTER_GROUP_BY_LOCATION)));
        groupByFilters
                .add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_PAY_STATION, findFilterString(REPORT_FILTER_GROUP_BY_PAY_STATION)));
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public final Map<Integer, List<ReportUIFilterType>> fillAuditFilters() {
        final Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
        // Primary Date/Time
        final List<ReportUIFilterType> primaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateFilters);
        // Secondary Date/Time
        final List<ReportUIFilterType> secondaryDateFilters = getDateOptions();
        inputMap.put(REPORT_FILTER_TYPE_SECONDARY_DATE, secondaryDateFilters);
        // Location Type
        final List<ReportUIFilterType> locationTypeFilters = new ArrayList<ReportUIFilterType>();
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_LOCATION_TREE, findFilterString(REPORT_FILTER_LOCATION_LOCATION_TREE)));
        locationTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_LOCATION_ROUTE_TREE, findFilterString(REPORT_FILTER_LOCATION_ROUTE_TREE)));
        // TODO exclude from Retry if not completely deleted
        locationTypeFilters
                .add(new ReportUIFilterType(REPORT_FILTER_LOCATION_PAY_STATION_TREE, findFilterString(REPORT_FILTER_LOCATION_PAY_STATION_TREE)));
        inputMap.put(REPORT_FILTER_TYPE_LOCATION, locationTypeFilters);
        // Report Number
        final List<ReportUIFilterType> reportNumberFilters = new ArrayList<ReportUIFilterType>();
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_ALL, findFilterString(REPORT_FILTER_REPORT_NUMBER_ALL)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_GREATER, findFilterString(REPORT_FILTER_REPORT_NUMBER_GREATER)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_LESS, findFilterString(REPORT_FILTER_REPORT_NUMBER_LESS)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_SPECIFIC, findFilterString(REPORT_FILTER_REPORT_NUMBER_SPECIFIC)));
        reportNumberFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_NUMBER_BETWEEN, findFilterString(REPORT_FILTER_REPORT_NUMBER_BETWEEN)));
        inputMap.put(REPORT_FILTER_TYPE_TICKET_NUMBER, reportNumberFilters);
        // Report Type
        final List<ReportUIFilterType> reportTypeFilters = new ArrayList<ReportUIFilterType>();
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_ALL, findFilterString(REPORT_FILTER_REPORT_ALL)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_COLLECTION_REPORT_ALL,
                findFilterString(REPORT_FILTER_REPORT_COLLECTION_REPORT_ALL)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_COLLECTION_REPORT_COIN,
                findFilterString(REPORT_FILTER_REPORT_COLLECTION_REPORT_COIN)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_COLLECTION_REPORT_BILL,
                findFilterString(REPORT_FILTER_REPORT_COLLECTION_REPORT_BILL)));
        reportTypeFilters.add(new ReportUIFilterType(REPORT_FILTER_REPORT_COLLECTION_REPORT_CREDIT_CARD,
                findFilterString(REPORT_FILTER_REPORT_COLLECTION_REPORT_CREDIT_CARD)));
        inputMap.put(REPORT_FILTER_TYPE_OTHER_PARAMETERS, reportTypeFilters);
        // Group By
        final List<ReportUIFilterType> groupByFilters = new ArrayList<ReportUIFilterType>();
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_NONE, findFilterString(REPORT_FILTER_GROUP_BY_NONE)));
        groupByFilters.add(new ReportUIFilterType(REPORT_FILTER_GROUP_BY_MACHINE, findFilterString(REPORT_FILTER_GROUP_BY_MACHINE)));
        inputMap.put(REPORT_FILTER_TYPE_GROUP_BY, groupByFilters);
        
        return inputMap;
    }
    
    public final List<ReportUIFilterType> getDateOptions() {
        final List<ReportUIFilterType> dateFilters = new ArrayList<ReportUIFilterType>();
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_DATE_TIME, findFilterString(REPORT_FILTER_DATE_DATE_TIME)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_MONTH, findFilterString(REPORT_FILTER_DATE_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_TODAY, findFilterString(REPORT_FILTER_DATE_TODAY)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_YESTERDAY, findFilterString(REPORT_FILTER_DATE_YESTERDAY)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_24_HOURS, findFilterString(REPORT_FILTER_DATE_LAST_24_HOURS)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_THIS_WEEK, findFilterString(REPORT_FILTER_DATE_THIS_WEEK)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_WEEK, findFilterString(REPORT_FILTER_DATE_LAST_WEEK)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_THIS_MONTH, findFilterString(REPORT_FILTER_DATE_THIS_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_MONTH, findFilterString(REPORT_FILTER_DATE_LAST_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_12_MONTHS, findFilterString(REPORT_FILTER_DATE_LAST_12_MONTHS)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_30_DAYS, findFilterString(REPORT_FILTER_DATE_LAST_30_DAYS)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_7_DAYS, findFilterString(REPORT_FILTER_DATE_LAST_7_DAYS)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_THIS_YEAR, findFilterString(REPORT_FILTER_DATE_THIS_YEAR)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_YEAR, findFilterString(REPORT_FILTER_DATE_LAST_YEAR)));
        
        return dateFilters;
    }
    
    public final ReportQueue createReportQueueEntry(final ReportDefinition reportDefinition, final String timeZone, final boolean isRerun) {
        final ReportQueue reportQueue = new ReportQueue();
        reportQueue.setReportDefinition(reportDefinition);
        reportQueue.setCustomer(reportDefinition.getCustomer());
        reportQueue.setUserAccount(reportDefinition.getUserAccount());
        
        final ReportStatusType reportStatusType = new ReportStatusType();
        reportStatusType.setId(ReportingConstants.REPORT_STATUS_SCHEDULED);
        reportQueue.setReportStatusType(reportStatusType);
        
        Date scheduledToRunGmt;
        if (isRerun) {
            final ReportRepeatType repeatType = new ReportRepeatType();
            repeatType.setId(ReportingConstants.REPORT_REPEAT_TYPE_ONLINE);
            reportQueue.setReportRepeatType(repeatType);
            scheduledToRunGmt = new Date();
        } else {
            reportQueue.setReportRepeatType(reportDefinition.getReportRepeatType());
            scheduledToRunGmt = reportDefinition.getScheduledToRunGmt();
        }
        
        reportQueue.setScheduledBeginGmt(scheduledToRunGmt);
        reportQueue.setQueuedGmt(new Date());
        createReportRunDatesStartDate(reportQueue, reportDefinition, timeZone, scheduledToRunGmt);
        return reportQueue;
    }
    
    private void createReportRunDatesStartDate(final ReportQueue reportQueue, final ReportDefinition reportDefinition, final String timeZone,
        final Date scheduledToRunGmt) {
        final Calendar scheduledTime = Calendar.getInstance();
        scheduledTime.setTime(scheduledToRunGmt);
        scheduledTime.setTimeZone(TimeZone.getTimeZone(timeZone));
        Calendar beginTime = (Calendar) scheduledTime.clone();
        //Needed to find the correct day after GMT midnight. a get call recalculates time after setting timezone.
        beginTime.get(Calendar.HOUR_OF_DAY);
        
        Calendar endTime = null;
        
        final int primaryDateFilterType = reportDefinition.getPrimaryDateFilterTypeId() == null ? ReportingConstants.REPORT_SELECTION_ALL
                : reportDefinition.getPrimaryDateFilterTypeId();
        switch (primaryDateFilterType) {
            case ReportingConstants.REPORT_SELECTION_ALL:
                break;
            case ReportingConstants.REPORT_FILTER_DATE_DATE_TIME:
            case ReportingConstants.REPORT_FILTER_DATE_MONTH:
                reportQueue.setPrimaryDateRangeBeginGmt(reportDefinition.getPrimaryDateRangeBeginGmt());
                reportQueue.setPrimaryDateRangeEndGmt(DateUtil.subSecondsToDate(reportDefinition.getPrimaryDateRangeEndGmt(), 1));
                break;
            case ReportingConstants.REPORT_FILTER_DATE_TODAY:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_YESTERDAY:
                beginTime.add(Calendar.DAY_OF_YEAR, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_24_HOURS:
                beginTime.add(Calendar.DAY_OF_YEAR, -1);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_WEEK:
                beginTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_WEEK:
                beginTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_MONTH:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_MONTH:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.add(Calendar.MONTH, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.MONTH, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_12_MONTHS:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.MONTH, -StandardConstants.MONTHS_IN_A_YEAR);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.MONTH, StandardConstants.MONTHS_IN_A_YEAR);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_30_DAYS:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.CONSTANT_30);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.CONSTANT_30);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_7_DAYS:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_YEAR:
                beginTime.set(Calendar.DAY_OF_YEAR, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_YEAR:
                beginTime.set(Calendar.DAY_OF_YEAR, 1);
                beginTime.add(Calendar.YEAR, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            default:
                break;
        }
        
        if (ReportingConstants.REPORT_SELECTION_ALL != primaryDateFilterType
            && ReportingConstants.REPORT_FILTER_DATE_DATE_TIME != primaryDateFilterType
            && ReportingConstants.REPORT_FILTER_DATE_MONTH != primaryDateFilterType) {
            reportQueue.setPrimaryDateRangeBeginGmt(beginTime.getTime());
            reportQueue.setPrimaryDateRangeEndGmt(endTime.getTime());
        }
        
        beginTime = (Calendar) scheduledTime.clone();
        //Needed to find the correct day after GMT midnight. a get call recalculates time after setting timezone.
        beginTime.get(Calendar.HOUR_OF_DAY);
        endTime = null;
        
        final int secondaryDateFilterType = reportDefinition.getSecondaryDateFilterTypeId() == null ? ReportingConstants.REPORT_SELECTION_ALL
                : reportDefinition.getSecondaryDateFilterTypeId();
        switch (secondaryDateFilterType) {
            case ReportingConstants.REPORT_SELECTION_ALL:
                break;
            case ReportingConstants.REPORT_FILTER_DATE_DATE_TIME:
            case ReportingConstants.REPORT_FILTER_DATE_MONTH:
                reportQueue.setSecondaryDateRangeBeginGmt(reportDefinition.getSecondaryDateRangeBeginGmt());
                reportQueue.setSecondaryDateRangeEndGmt(DateUtil.subSecondsToDate(reportDefinition.getSecondaryDateRangeEndGmt(), 1));
                break;
            case ReportingConstants.REPORT_FILTER_DATE_TODAY:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_YESTERDAY:
                beginTime.add(Calendar.DAY_OF_YEAR, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_24_HOURS:
                beginTime.add(Calendar.DAY_OF_YEAR, -1);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_WEEK:
                beginTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_WEEK:
                beginTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_MONTH:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_MONTH:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.add(Calendar.MONTH, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.MONTH, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_12_MONTHS:
                beginTime.set(Calendar.DAY_OF_MONTH, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.MONTH, -StandardConstants.MONTHS_IN_A_YEAR);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.MONTH, StandardConstants.MONTHS_IN_A_YEAR);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_30_DAYS:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.CONSTANT_30);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.CONSTANT_30);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_7_DAYS:
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                beginTime.add(Calendar.DAY_OF_YEAR, -StandardConstants.DAYS_IN_A_WEEK);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
                endTime.add(Calendar.SECOND, -1);
                break;
            case ReportingConstants.REPORT_FILTER_DATE_THIS_YEAR:
                beginTime.set(Calendar.DAY_OF_YEAR, 1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) scheduledTime.clone();
                break;
            case ReportingConstants.REPORT_FILTER_DATE_LAST_YEAR:
                beginTime.set(Calendar.DAY_OF_YEAR, 1);
                beginTime.add(Calendar.YEAR, -1);
                beginTime.set(Calendar.HOUR_OF_DAY, 0);
                beginTime.set(Calendar.MINUTE, 0);
                beginTime.set(Calendar.SECOND, 0);
                beginTime.set(Calendar.MILLISECOND, 0);
                endTime = (Calendar) beginTime.clone();
                endTime.add(Calendar.YEAR, 1);
                endTime.add(Calendar.SECOND, -1);
                break;
            default:
                break;
        }
        
        if (ReportingConstants.REPORT_SELECTION_ALL != secondaryDateFilterType
            && ReportingConstants.REPORT_FILTER_DATE_DATE_TIME != secondaryDateFilterType
            && ReportingConstants.REPORT_FILTER_DATE_MONTH != secondaryDateFilterType) {
            reportQueue.setSecondaryDateRangeBeginGmt(beginTime.getTime());
            reportQueue.setSecondaryDateRangeEndGmt(endTime.getTime());
        }
        
    }
    
    public final int getTimeZoneOffset(final String timeZone) {
        TimeZone timezone = TimeZone.getTimeZone(timeZone);
        if (timezone == null) {
            timezone = TimeZone.getDefault();
        }
        
        final Date datetime = new Date();
        
        final int offsetHour = timezone.getOffset(datetime.getTime()) / 1000 / 60 / 60;
        return offsetHour;
    }
    
    @Override
    public boolean filterValueValid(final int reportType, final int filterType, final int value) {
        boolean result = false;
        final List<ReportUIFilterType> filterList = getFilterList(reportType, filterType);
        if (filterList != null) {
            for (ReportUIFilterType filterTypeObj : filterList) {
                if (filterTypeObj.getValue() == value) {
                    result = true;
                    break;
                }
            }
        }
        
        return result;
    }
    
    public static String findFilterStringStatic(final Integer filterValueTypeId) {
        if (filterValueTypeId == null) {
            return null;
        }
        if (!reportFilters.containsKey(filterValueTypeId.intValue())) {
            return null;
        }
        return reportFilters.get(filterValueTypeId);
    }
    
    public final List<ReportUIFilterType> getTransactionDelayDateOptions() {
        final List<ReportUIFilterType> dateFilters = new ArrayList<ReportUIFilterType>();
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_DATE_TIME, findFilterString(REPORT_FILTER_DATE_DATE_TIME)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_MONTH, findFilterString(REPORT_FILTER_DATE_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_TODAY, findFilterString(REPORT_FILTER_DATE_TODAY)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_YESTERDAY, findFilterString(REPORT_FILTER_DATE_YESTERDAY)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_THIS_WEEK, findFilterString(REPORT_FILTER_DATE_THIS_WEEK)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_WEEK, findFilterString(REPORT_FILTER_DATE_LAST_WEEK)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_THIS_MONTH, findFilterString(REPORT_FILTER_DATE_THIS_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_MONTH, findFilterString(REPORT_FILTER_DATE_LAST_MONTH)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_30_DAYS, findFilterString(REPORT_FILTER_DATE_LAST_30_DAYS)));
        dateFilters.add(new ReportUIFilterType(REPORT_FILTER_DATE_LAST_7_DAYS, findFilterString(REPORT_FILTER_DATE_LAST_7_DAYS)));
        
        return dateFilters;
    }
    
    @Override
    public final List<FilterDTO> getSeverityFilters(List<FilterDTO> result, final RandomKeyMapping keyMapping) {
        if (result == null) {
            result = new ArrayList<FilterDTO>();
        }
        
        @SuppressWarnings("unchecked")
        final List<EventSeverityType> estiesList = this.entityDao.getNamedQuery("EventSeverityType.fildAllActiveTypes").list();
        for (EventSeverityType est : estiesList) {
            result.add(new FilterDTO(keyMapping.getRandomString(EventSeverityType.class, est.getId()), est.getName()));
        }
        
        return result;
    }
    
    @Override
    public final List<FilterDTO> getModuleFilters(List<FilterDTO> result, final RandomKeyMapping keyMapping) {
        if (result == null) {
            result = new ArrayList<FilterDTO>();
        }
        
        @SuppressWarnings("unchecked")
        final List<EventDeviceType> edtsList = this.entityDao.getNamedQuery("EventDeviceType.fildAllTypesExceptPCM").list();
        for (EventDeviceType edt : edtsList) {
            result.add(new FilterDTO(keyMapping.getRandomString(EventDeviceType.class, edt.getId()), this.getPropertyEN(edt.getName())));
        }
        
        return result;
    }
    
    public static String getProcessorPropertyName(final String processorName) {
        if (processorMap == null) {
            return "";
        }
        final String procName = processorMap.get(processorName);
        if (procName == null) {
            return "";
        } else {
            return procName;
        }
    }
    
    public static final Map<Integer, String> getReportFilters() {
        return reportFilters;
    }
    
    public static final void setReportFilters(final Map<Integer, String> reportFilters) {
        ReportingUtilImpl.reportFilters = reportFilters;
    }
    
    public static final Map<String, String> getProcessorMap() {
        return processorMap;
    }
    
    public static void setProcessorMap(final Map<String, String> processorMap) {
        ReportingUtilImpl.processorMap = processorMap;
    }
    
    public final Map<Byte, Map<Integer, List<ReportUIFilterType>>> getUiReportFilters() {
        return this.uiReportFilters;
    }
    
    public final void setUiReportFilters(final Map<Byte, Map<Integer, List<ReportUIFilterType>>> uiReportFilters) {
        this.uiReportFilters = uiReportFilters;
    }
}
