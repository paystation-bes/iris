package com.digitalpaytech.util.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.exception.HystrixRuntimeException;

@Component
public class ManualRetry {
    private RetryTemplate template;
    
    @Value("${reply.request.backoff}")
    private long backoff;
    
    @Value("${reply.request.retries}")
    private int retries;
    
    @PostConstruct
    public final void init() {
        this.template = new RetryTemplate();
        final Map<Class<? extends Throwable>, Boolean> retryableExceptions = new ConcurrentHashMap<>();
        
        retryableExceptions.put(HystrixRuntimeException.class, true);
        final RetryPolicy retryPolicy = new SimpleRetryPolicy(this.retries, retryableExceptions, true);
        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(this.backoff);
        this.template.setRetryPolicy(retryPolicy);
        this.template.setBackOffPolicy(backOffPolicy);
    }
    
    public final RetryTemplate getTemplate() {
        return template;
    }
    
}
