package com.digitalpaytech.util.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.TLSUtils;

@Service("tlsUtils")
public class TLSUtilsImpl implements TLSUtils {
    
    private static final Logger LOG = Logger.getLogger(TLSUtils.class);
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    private List<String> redirectFromHosts;
    
    private String newHost;
    
    private String redirect;
    
    @PostConstruct
    public void init() {
        this.redirectFromHosts = new ArrayList<>();
        
        final String badHostsString = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REDIRECTABLE_SOURCE_HOSTS,
                                                                                 EmsPropertiesService.DEFAULT_REDIRECTABLE_SOURCE_HOSTS);
        if (StringUtils.isNotBlank(badHostsString)) {
            final String[] sp = badHostsString.split(",");
            for (String item : sp) {
                final String tmp = item.trim();
                if (StringUtils.isNotBlank(tmp)) {
                    this.redirectFromHosts.add(tmp);
                }
            }
        }
        
        this.newHost = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REDIRECTABLE_DEST_HOSTS,
                                                                  EmsPropertiesService.DEFAULT_REDIRECTABLE_DEST_HOSTS);
        this.redirect = "redirect:" + newHost;
    }
    
    @Override
    public Object redirectIfBadHost(final HttpServletRequest request, final Object defaultValue) {
        
        if (shouldRedirect(request)) {
            LOG.debug("Request to old host %s being redirected to %s");
            return this.redirect;
        } else {
            return defaultValue;
        }
    }
    
    @Override
    public boolean shouldRedirect(final HttpServletRequest request) {
        return StringUtils.isNotBlank(this.redirect) && this.redirectFromHosts != null && this.redirectFromHosts.contains(request.getHeader("host"));
    }
    
    @Override
    public String redirect(final String path) {
        return this.redirect + path;
    }
    
    @Override
    public void redirect(final HttpServletResponse response, final String path) throws IOException {
        response.sendRedirect(this.newHost + path);
    }
    
}
