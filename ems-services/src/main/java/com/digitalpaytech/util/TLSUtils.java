package com.digitalpaytech.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface TLSUtils {
    
    Object redirectIfBadHost(HttpServletRequest request, Object defaultValue);
    
    boolean shouldRedirect(HttpServletRequest request);
    
    String redirect(String path);
    
    void redirect(HttpServletResponse response, String path) throws IOException;
    
}
