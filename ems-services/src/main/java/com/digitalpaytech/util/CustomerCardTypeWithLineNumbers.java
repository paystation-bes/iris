package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.util.support.ObjectHelper;

public class CustomerCardTypeWithLineNumbers extends CustomerCardType {
	private static final long serialVersionUID = -1423124301899455084L;
	
	private ArrayList<Integer> lineNumbers = new ArrayList<Integer>();
	
	public CustomerCardTypeWithLineNumbers() {
		
	}
	
	public List<Integer> getLineNumbers() {
		return lineNumbers;
	}
	
	public static class Helper implements ObjectHelper<CustomerCardType, String> {
		public Helper() {
			
		}

		@Override
		public CustomerCardType createObject(String initVal) {
			CustomerCardTypeWithLineNumbers result = new CustomerCardTypeWithLineNumbers();
			result.setName(initVal);
			
			return result;
		}

		@Override
		public <ST extends CustomerCardType> void copyProperties(ST source, ST destination) {
			destination.setId(source.getId());
			destination.setName(source.getName());
			destination.setCardType(source.getCardType());
		}
	}
}
