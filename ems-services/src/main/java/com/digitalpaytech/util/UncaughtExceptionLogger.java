package com.digitalpaytech.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.servlet.DPTHttpServletResponseWrapper;

/**
 * 
 * @author Amanda Chong
 * 
 */

public class UncaughtExceptionLogger implements HandlerExceptionResolver, Ordered {
    
    @Autowired
    private KPIService kpiService;
    
    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }
    
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if(!(response instanceof DPTHttpServletResponseWrapper)){
        	kpiService.incrementUncaughtExceptionServerError();
        	return null;
        }
    	int status = ((DPTHttpServletResponseWrapper) response).getStatus();
        
        if (status < 200) {
            kpiService.incrementUncaughtExceptionInformation();
        } else if (status < 300) {
            kpiService.incrementUncaughtExceptionSuccess();
        } else if (status < 400) {
            kpiService.incrementUncaughtExceptionRedirect();
        } else if (status < 500) {
            kpiService.incrementUncaughtExceptionClientError();
        } else if (status < 600) {
            kpiService.incrementUncaughtExceptionServerError();
        }
        
        return null;
    }
}
