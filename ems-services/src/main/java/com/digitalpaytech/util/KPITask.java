package com.digitalpaytech.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.digitalpaytech.service.kpi.KPIService;

/**
 * This is a scheduled cron job that updates the entries for kpiInfo in {@link} KPIServiceImpl
 * 
 * @author Amanda Chong
 */

public class KPITask {
    
    @Autowired
    private KPIService kpiService;
    
    @Scheduled(cron = "0 0/1 * * * *")
    public void update() {
        kpiService.update();
    }
    
    // 3 minutes after rotation background process
    @Scheduled(cron = "0 3 8 * * *")
    public void updateDaily() {
    	kpiService.updateDaily();
    }
}