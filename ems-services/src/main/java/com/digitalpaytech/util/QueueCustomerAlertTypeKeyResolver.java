package com.digitalpaytech.util;

import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.util.queue.RoutingKeyResolver;

public class QueueCustomerAlertTypeKeyResolver extends RandomizedKeyResolver<QueueCustomerAlertType> implements RoutingKeyResolver<QueueCustomerAlertType> {
    private static final String PREFIX_CAT = "cat-";
    
    private static final String SEPARATOR = ",";
    
    public QueueCustomerAlertTypeKeyResolver() {
        super();
    }
    
    @Override
    public final String resolve(final QueueCustomerAlertType message) {
        final StringBuilder resultBuffer = new StringBuilder();
        if ((message.getCustomerAlertTypeIds() != null) && (message.getCustomerAlertTypeIds().size() > 0)) {
            resultBuffer.append(PREFIX_CAT);
            for (Integer catId : message.getCustomerAlertTypeIds()) {
                resultBuffer.append(SEPARATOR);
                resultBuffer.append(catId);
            }
        }
        
        return (resultBuffer.length() <= 0) ? super.resolve(message) : Integer.toString(resultBuffer.toString().hashCode());
    }
}
