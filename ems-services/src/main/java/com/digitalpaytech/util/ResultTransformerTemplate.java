package com.digitalpaytech.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.hibernate.transform.AliasToBeanResultTransformer;

import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;

public abstract class ResultTransformerTemplate<T> extends AliasToBeanResultTransformer {
    private static final long serialVersionUID = -2681205704372480222L;
    
    private MessageHelper messageHelper;
    private TimeZone timeZone;
    
    private Map<String, Integer> aliasIdxMap;
    
    protected ResultTransformerTemplate(final Class<T> type, final MessageHelper messageHelper, final TimeZone timeZone) {
        super(type);
        
        this.messageHelper = messageHelper;
        this.timeZone = timeZone;
    }
    
    @Override
    public final Object transformTuple(final Object[] tuple, final String[] aliases) {
        resolveIdx(tuple, aliases);
        return createInstance(tuple);
    }
    
    protected abstract T createInstance(final Object[] tuple);
    
    protected final Object getAttr(final Object[] tuple, final String name) {
        Object result = null;
        
        final Integer idx = this.aliasIdxMap.get(name);
        if (idx != null) {
            result = tuple[idx];
        }
        
        return result;
    }
    
    protected final Integer getAttrInt(final Object[] tuple, final String name) {
        Integer result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            if (buff instanceof Integer) {
                result = (Integer) buff;
            } else if (buff instanceof Number) {
                result = new Integer(((Number) buff).intValue());
            } else if (buff instanceof String) {
                result = new Integer((String) buff);
            } else {
                throw new RuntimeException("Cannot convert " + buff.getClass() + " to Integer!");
            }
        }
        
        return result;
    }
    
    protected final BigDecimal getAttrBigDecimal(final Object[] tuple, final String name) {
        BigDecimal result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            if (buff instanceof BigDecimal) {
                result = (BigDecimal) buff;
            } else if (buff instanceof Number) {
                result = new BigDecimal(buff.toString());
            } else if (buff instanceof String) {
                result = new BigDecimal((String) buff);
            } else {
                throw new RuntimeException("Cannot convert " + buff.getClass() + " to BigDecimal!");
            }
        }
        
        return result;
    }
    
    protected final Boolean getAttrBoolean(final Object[] tuple, final String name, final boolean defaultVal) {
        Boolean result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff == null) {
            result = defaultVal;
        } else {
            if (buff instanceof Boolean) {
                result = (Boolean) buff;
            } else if (buff instanceof Number) {
                result = ((Number) buff).intValue() > 0;
            }
        }
        
        return result;
    }
    
    protected final String getAttrMessage(final Object[] tuple, final String... name) {
        if (this.messageHelper == null) {
            throw new IllegalStateException("MessageHelper was not provided when this transformer got instantiated!");
        }
        
        String result = null;
        if ((name != null) && (name.length > 0)) {
            Object partialKey;
            final StringBuilder msgKey = new StringBuilder();
            for (String n : name) {
                partialKey = getAttr(tuple, n);
                if (partialKey != null) {
                    msgKey.append(".");
                    msgKey.append(partialKey);
                }
            }
            
            if (msgKey.length() > 0) {
                result = this.messageHelper.getMessage(msgKey.substring(1));
            }
        }
        
        return result;
    }
    
    protected final WebObjectId getAttrWebObjectId(final Object[] tuple, final String name, final Class<?> objectClass) {
        WebObjectId result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            result = new WebObjectId(objectClass, buff);
        }
        
        return result;
    }
    
    protected final RelativeDateTime getAttrRelativeTime(final Object[] tuple, final String name) {
        if (this.timeZone == null) {
            throw new IllegalStateException("TimeZone was not provided when this transformer got instantiated!");
        }
        
        RelativeDateTime result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            result = new RelativeDateTime((Date) buff, this.timeZone);
        }
        
        return result;
    }
    
    protected final Date getAttrDate(final Object[] tuple, final String name) {
        Date result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            if (buff instanceof Date) {
                result = (Date) buff;
            } else if (buff instanceof Number) {
                result = new Date(((Number) buff).longValue());
            } else {
                throw new RuntimeException("UnSupported Date type: " + buff.getClass());
            }
        }
        
        return result;
    }
    
    protected final java.sql.Date getAttrSQLDate(final Object[] tuple, final String name) {
        java.sql.Date result = null;
        
        final Object buff = getAttr(tuple, name);
        if (buff != null) {
            if (buff instanceof java.sql.Date) {
                result = (java.sql.Date) buff;
            } else if (buff instanceof Date) {
                result = new java.sql.Date(((Date) buff).getTime());
            } else if (buff instanceof Number) {
                result = new java.sql.Date(((Number) buff).longValue());
            } else {
                throw new RuntimeException("UnSupported SQL Date type: " + buff.getClass());
            }
        }
        
        return result;
    }
    
    protected final void resolveIdx(final Object[] tuple, final String[] aliases) {
        if (this.aliasIdxMap == null) {
            int idx = aliases.length;
            this.aliasIdxMap = new HashMap<String, Integer>(idx * 2);
            while (--idx >= 0) {
                this.aliasIdxMap.put(aliases[idx], idx);
            }
        }
    }
}
