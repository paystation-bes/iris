package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.util.queue.MessageTransformer;

public class QueueCustomerAlertTypeTransformer implements MessageTransformer<QueueCustomerAlertType> {
    public QueueCustomerAlertTypeTransformer() {
        
    }
    
    @Override
    public final List<QueueCustomerAlertType> transform(final QueueCustomerAlertType message) {
        List<QueueCustomerAlertType> result;
        if ((message.getCustomerAlertTypeIds() == null) || (message.getCustomerAlertTypeIds().size() <= 1)) {
            result = new ArrayList<QueueCustomerAlertType>(1);
            result.add(message);
        } else {
            result = new ArrayList<QueueCustomerAlertType>(message.getCustomerAlertTypeIds().size());
            for (Integer catId : message.getCustomerAlertTypeIds()) {
                final QueueCustomerAlertType transformed = new QueueCustomerAlertType(message);
                transformed.setCustomerAlertTypeIds(new ArrayList<Integer>(1));
                transformed.getCustomerAlertTypeIds().add(catId);
                
                result.add(transformed);
            }
        }
        
        return result;
    }
}
