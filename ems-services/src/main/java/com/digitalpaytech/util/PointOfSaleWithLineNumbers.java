package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.support.ObjectHelper;

public class PointOfSaleWithLineNumbers extends PointOfSale {
	private static final long serialVersionUID = 4312509082760494129L;
	
	private ArrayList<Integer> lineNumbers = new ArrayList<Integer>();
	
	public PointOfSaleWithLineNumbers() {
		
	}
	
	public List<Integer> getLineNumbers() {
		return lineNumbers;
	}
	
	public static class Helper implements ObjectHelper<PointOfSale, String> {
		public Helper() {
			
		}
		
		@Override
		public PointOfSale createObject(String initVal) {
			PointOfSaleWithLineNumbers result = new PointOfSaleWithLineNumbers();
			result.setName(initVal);
			
			return result;
		}

		@Override
		public <ST extends PointOfSale> void copyProperties(ST source, ST destination) {
			destination.setId(source.getId());
			destination.setName(source.getName());
		}
		
	}
}
