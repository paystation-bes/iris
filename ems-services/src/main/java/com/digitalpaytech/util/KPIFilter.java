package com.digitalpaytech.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.servlet.DPTHttpServletResponseWrapper;

/**
 * This class monitors XMLRPC, BOSS Uploads/Downloads, Coupon Uploads/Downloads
 * Filters defined in web.xml
 * 
 * @author Amanda Chong
 * 
 */

public class KPIFilter implements Filter {
    
    private static final Logger logger = Logger.getLogger(KPIFilter.class);
    
    private KPIService kpiService;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
        try {
            kpiService = context.getBean(KPIService.class);
        } catch (NoSuchBeanDefinitionException e) {
            kpiService = null;
        }
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (kpiService == null) {
            chain.doFilter(request, response);
        } else {
            DPTHttpServletResponseWrapper responseWrapper = new DPTHttpServletResponseWrapper((HttpServletResponse) response);
            long startTime = System.currentTimeMillis();
            chain.doFilter(request, responseWrapper);
            long responseTime = System.currentTimeMillis() - startTime;
            
            String requestURI = ((HttpServletRequest) request).getRequestURI();
            
            try {
                if (isSuccessful(responseWrapper.getStatus())) {
                    doSuccessCalculations(request, requestURI, responseTime);
                } else {
                    logger.warn("Failed request for request: " + requestURI + " with status code: " + responseWrapper.getStatus() + "\n");
                    doFailCalculations(request, requestURI, responseTime);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public void destroy() {
        
    }
    
    private boolean isSuccessful(int status) {
        return status <= 200;
    }
    
    /**
     * 1. Checks URI path to assign corresponding # of requests and response times for that request
     * 2. Additional checks beyond URI path required for XMLRPC and BOSS uploads and downloads through attributes set in the request for corresponding classes
     * 3. Additional checks for XMLRPC Transactions for different types of transactions by payment type, permit type, and whether it is store and forward
     * 
     * @param request
     * @param requestURI
     * @param responseTime
     */
    private void doSuccessCalculations(ServletRequest request, String requestURI, long responseTime) {
        if (requestURI.equals(KPIConstants.COUPON_UPLOAD)) {
            kpiService.incrementUIFileUploadSuccessCoupon();
        } else if (requestURI.equals(KPIConstants.PASSCARD_UPLOAD)) {
            kpiService.incrementUIFileUploadSuccessPasscard();
        } else if (requestURI.equals(KPIConstants.BANNEDCARD_UPLOAD)) {
            kpiService.incrementUIFileUploadSuccessBannedCard();
        } else if (requestURI.equals(KPIConstants.COUPON_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadSuccessCoupon();
        } else if (requestURI.equals(KPIConstants.PASSCAR_DDOWNLOAD)) {
            kpiService.incrementUIFileDownloadSuccessPasscard();
        } else if (requestURI.equals(KPIConstants.REPORT_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadSuccessReport();
        } else if (requestURI.equals(KPIConstants.WIDGET_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadSuccessWidget();
        } else if (requestURI.equals(KPIConstants.XMLRPC_PS2)) {
            if (request.getAttribute(KPIConstants.XMLRPC_STATUS_CODE) == null) {
                doXMLRPCSuccessCalculations(request);
            } else {
                doXMLRPCFailCalculations(request);
            }
        } else if (requestURI.contains(KPIConstants.PAYSTATION_REST)) {
            if (request.getAttribute(KPIConstants.XMLRPC_STATUS_CODE) == null) {
                doPaystationRestSuccessCalculations(request, requestURI);
            } else {
                doPaystationRestFailCalculations(request, requestURI);
            }
        } else if (requestURI.equals(KPIConstants.WIDGET)) {
            kpiService.incrementUIDashboardQuerySuccess();
        } else if (requestURI.contains(KPIConstants.REST_COUPONS)) {
            int xmlSize = ((DPTHttpServletRequestWrapper) request).getBody().length();
            kpiService.incrementDigitalAPIRequestCouponSuccess(responseTime, xmlSize);
        } else if (requestURI.equals(KPIConstants.UPLOAD)) {
            kpiService.incrementBOSSUploadSuccessTotal();
            String type = request.getParameter(KPIConstants.TYPE_PARAMETER);
            if (type.equals(KPIConstants.LOT_SETTING)) {
                kpiService.incrementBOSSUploadSuccessLotSetting();
            } else if (type.equals(KPIConstants.FILE_UPLOAD)) {
                kpiService.incrementBOSSUploadSuccessFileUpload();
            } else if (type.equals(KPIConstants.PAYMENTSTATION_QUERY)) {
                kpiService.incrementBOSSUploadSuccessPaystationQuery();
            } else if (type.equals(KPIConstants.OFFLINE_TRANSACTION)) {
                kpiService.incrementBOSSUploadSuccessOfflineTransaction();
            }
        } else if (requestURI.equals(KPIConstants.DOWNLOAD)) {
            kpiService.incrementBOSSDownloadSuccessTotal();
            String type = request.getParameter(KPIConstants.TYPE_PARAMETER);
            if (type.equals(KPIConstants.TYPE_LOT_SETTING)) {
                kpiService.incrementBOSSDownloadSuccessLotSetting();
            } else if (type.equals(KPIConstants.TYPE_PUBLIC_KEY_REQUEST)) {
                kpiService.incrementBOSSDownloadSuccessPublicKeyRequest();
            } else if (type.equals(KPIConstants.TYPE_BAD_CREDITCARD)) {
                kpiService.incrementBOSSDownloadSuccessBadCreditCard();
            } else if (type.equals(KPIConstants.TYPE_BAD_SMARTCARD)) {
                kpiService.incrementBOSSDownloadSuccessBadSmartCard();
            }
        }
    }
    
    private void doFailCalculations(ServletRequest request, String requestURI, long responseTime) {
        if (requestURI.equals(KPIConstants.COUPON_UPLOAD)) {
            kpiService.incrementUIFileUploadFailCoupon();
        } else if (requestURI.equals(KPIConstants.PASSCARD_UPLOAD)) {
            kpiService.incrementUIFileUploadFailPasscard();
        } else if (requestURI.equals(KPIConstants.BANNEDCARD_UPLOAD)) {
            kpiService.incrementUIFileUploadFailBannedCard();
        } else if (requestURI.equals(KPIConstants.COUPON_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadFailCoupon();
        } else if (requestURI.equals(KPIConstants.PASSCAR_DDOWNLOAD)) {
            kpiService.incrementUIFileDownloadFailPasscard();
        } else if (requestURI.equals(KPIConstants.REPORT_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadFailReport();
        } else if (requestURI.equals(KPIConstants.WIDGET_DOWNLOAD)) {
            kpiService.incrementUIFileDownloadFailWidget();
        } else if (requestURI.equals(KPIConstants.XMLRPC_PS2)) {
            doXMLRPCFailCalculations(request);
        } else if (requestURI.contains(KPIConstants.PAYSTATION_REST)) {
            doPaystationRestFailCalculations(request, requestURI);
        } else if (requestURI.equals(KPIConstants.WIDGET)) {
            kpiService.incrementUIDashboardQueryFail();
        } else if (requestURI.contains(KPIConstants.REST_COUPONS)) {
            int xmlSize = ((DPTHttpServletRequestWrapper) request).getBody().length();
            kpiService.incrementDigitalAPIRequestCouponFail(responseTime, xmlSize);
        } else if (requestURI.equals(KPIConstants.UPLOAD)) {
            kpiService.incrementBOSSUploadFailTotal();
            String type = request.getParameter(KPIConstants.TYPE_PARAMETER);
            if (type.equals(KPIConstants.LOT_SETTING)) {
                kpiService.incrementBOSSUploadFailLotSetting();
            } else if (type.equals(KPIConstants.FILE_UPLOAD)) {
                kpiService.incrementBOSSUploadFailFileUpload();
            } else if (type.equals(KPIConstants.PAYMENTSTATION_QUERY)) {
                kpiService.incrementBOSSUploadFailPaystationQuery();
            } else if (type.equals(KPIConstants.OFFLINE_TRANSACTION)) {
                kpiService.incrementBOSSUploadFailOfflineTransaction();
            }
        } else if (requestURI.equals(KPIConstants.DOWNLOAD)) {
            kpiService.incrementBOSSDownloadFailTotal();
            String type = request.getParameter(KPIConstants.TYPE_PARAMETER);
            if (type.equals(KPIConstants.TYPE_LOT_SETTING)) {
                kpiService.incrementBOSSDownloadFailLotSetting();
            } else if (type.equals(KPIConstants.TYPE_PUBLIC_KEY_REQUEST)) {
                kpiService.incrementBOSSDownloadFailPublicKeyRequest();
            } else if (type.equals(KPIConstants.TYPE_BAD_CREDITCARD)) {
                kpiService.incrementBOSSDownloadFailBadCreditCard();
            } else if (type.equals(KPIConstants.TYPE_BAD_SMARTCARD)) {
                kpiService.incrementBOSSDownloadFailBadSmartCard();
            }
        }
    }
    
    private void doXMLRPCSuccessCalculations(ServletRequest request) {
        String requestType = (String) request.getAttribute(KPIConstants.XMLRPC_REQUEST);
        if (requestType != null) {
            if (requestType.equals(KPIConstants.AUDIT)) {
                kpiService.incrementXMLRPCRequestSuccessAudit();
            } else if (requestType.equals(KPIConstants.TRANSACTION)) {
                kpiService.incrementXMLRPCRequestSuccessTransaction();
                doTransactionSuccessCalculations(request, false);
            } else if (requestType.equals(KPIConstants.AUTHORIZE_REQUEST)) {
                kpiService.incrementXMLRPCRequestSuccessAuthorizeRequest();
            } else if (requestType.equals(KPIConstants.EVENT)) {
                kpiService.incrementXMLRPCRequestSuccessEvent();
            } else if (requestType.equals(KPIConstants.HEARTBEAT)) {
                kpiService.incrementXMLRPCRequestSuccessHeartBeat();
            } else if (requestType.equals(KPIConstants.LOTSETTING_SUCCESS)) {
                kpiService.incrementXMLRPCRequestSuccessLotSettingSuccess();
            } else if (requestType.equals(KPIConstants.PLATE_RPT_REQ)) {
                kpiService.incrementXMLRPCRequestSuccessPlateRptReq();
            } else if (requestType.equals(KPIConstants.AUTHORIZE_CARD_REQUEST)) {
                kpiService.incrementXMLRPCRequestSuccessAuthorizeCardRequest();
            } else if (requestType.equals(KPIConstants.STALLINFO_REQUEST)) {
                kpiService.incrementXMLRPCRequestSuccessStallInfoRequest();
            } else if (requestType.equals(KPIConstants.STALLREPORT_REQUEST)) {
                kpiService.incrementXMLRPCRequestSuccessStallReportRequest();
            }
        }
    }
    
    private void doXMLRPCFailCalculations(ServletRequest request) {
        String requestType = (String) request.getAttribute(KPIConstants.XMLRPC_REQUEST);
        if (requestType != null) {
            if (requestType.equals(KPIConstants.AUDIT)) {
                kpiService.incrementXMLRPCRequestFailAudit();
            } else if (requestType.equals(KPIConstants.TRANSACTION)) {
                kpiService.incrementXMLRPCRequestFailTransaction();
                doTransactionFailCalculations(request, false);
            } else if (requestType.equals(KPIConstants.AUTHORIZE_REQUEST)) {
                kpiService.incrementXMLRPCRequestFailAuthorizeRequest();
            } else if (requestType.equals(KPIConstants.EVENT)) {
                kpiService.incrementXMLRPCRequestFailEvent();
            } else if (requestType.equals(KPIConstants.HEARTBEAT)) {
                kpiService.incrementXMLRPCRequestFailHeartBeat();
            } else if (requestType.equals(KPIConstants.LOTSETTING_SUCCESS)) {
                kpiService.incrementXMLRPCRequestFailLotSettingSuccess();
            } else if (requestType.equals(KPIConstants.PLATE_RPT_REQ)) {
                kpiService.incrementXMLRPCRequestFailPlateRptReq();
            } else if (requestType.equals(KPIConstants.AUTHORIZE_CARD_REQUEST)) {
                kpiService.incrementXMLRPCRequestFailAuthorizeCardRequest();
            } else if (requestType.equals(KPIConstants.STALLINFO_REQUEST)) {
                kpiService.incrementXMLRPCRequestFailStallInfoRequest();
            } else if (requestType.equals(KPIConstants.STALLREPORT_REQUEST)) {
                kpiService.incrementXMLRPCRequestFailStallReportRequest();
            }
        }
    }
    
    private void doPaystationRestSuccessCalculations(ServletRequest request, String requestURI) {
        if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_CANCELTRANSACTION_URI)) {
            kpiService.incrementPaystationRestSuccessCancelTransaction();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_HEARTBEAT_URI)) {
            kpiService.incrementPaystationRestSuccessHeartBeat();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_TOKEN_URI)) {
            kpiService.incrementPaystationRestSuccessToken();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_TRANSACTION_URI)) {
            kpiService.incrementPaystationRestSuccessTransaction();
            doTransactionSuccessCalculations(request, true);
        }
    }
    
    private void doPaystationRestFailCalculations(ServletRequest request, String requestURI) {
        if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_CANCELTRANSACTION_URI)) {
            kpiService.incrementPaystationRestFailCancelTransaction();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_HEARTBEAT_URI)) {
            kpiService.incrementPaystationRestFailHeartBeat();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_TOKEN_URI)) {
            kpiService.incrementPaystationRestFailToken();
        } else if (requestURI.contains(PaystationConstants.PAYSTATION_PATH_TRANSACTION_URI)) {
            kpiService.incrementPaystationRestFailTransaction();
            doTransactionFailCalculations(request, true);
        }
    }
    
    private void doTransactionSuccessCalculations(ServletRequest request, boolean isPaystationRest) {
        if (isPaystationRest) {
            if ((Boolean) request.getAttribute(KPIConstants.IS_STOREFORWARD)) {
                kpiService.incrementPaystationRestTransactionSuccessStoreAndForward();
            }
            int paymentType = (Integer) request.getAttribute(KPIConstants.PAYMENT_TYPE);
            switch (paymentType) {
                case WebCoreConstants.PAYMENT_TYPE_CASH:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentCash();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentCredit();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentCashCredit();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentCashSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentCashValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_UNKNOWN:
                    kpiService.incrementPaystationRestTransactionSuccessPaymentUnknown();
                    break;
            }
            int permissionIssueType = (Integer) request.getAttribute(KPIConstants.PERMIT_ISSUE_TYPE);
            switch (permissionIssueType) {
                case WebCoreConstants.PERMIT_ISSUE_TYPE_NA:
                    kpiService.incrementPaystationRestTransactionSuccessPermitNA();
                    break;
                case WebCoreConstants.PERMIT_ISSUE_TYPE_PND:
                    kpiService.incrementPaystationRestTransactionSuccessPermitPayAndDisplay();
                    break;
                case WebCoreConstants.PERMIT_ISSUE_TYPE_PBL:
                    kpiService.incrementPaystationRestTransactionSuccessPermitPayByPlate();
                    break;
                case WebCoreConstants.PERMIT_ISSUE_TYPE_PBS:
                    kpiService.incrementPaystationRestTransactionSuccessPermitPayBySpace();
                    break;
            }
            
        } else {
            if ((Boolean) request.getAttribute(KPIConstants.IS_STOREFORWARD)) {
                kpiService.incrementXMLRPCRequestTransactionSuccessStoreAndForward();
            }
            int paymentType = (Integer) request.getAttribute(KPIConstants.PAYMENT_TYPE);
            switch (paymentType) {
                case WebCoreConstants.PAYMENT_TYPE_CASH:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCash();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCreditSwipe();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCreditCL();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCreditCH();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
                    kpiService.incrementXMLRPCRequestTransactionSuccessSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCashCreditSwipe();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCashCreditCL();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCashCreditCH();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCashSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
                    kpiService.incrementXMLRPCRequestTransactionSuccessValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                    kpiService.incrementXMLRPCRequestTransactionSuccessCashValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_UNKNOWN:
                    kpiService.incrementXMLRPCRequestTransactionSuccessUnknown();
                    break;
            }
            int permissionIssueType = (Integer) request.getAttribute(KPIConstants.PERMIT_ISSUE_TYPE);
            switch (permissionIssueType) {
                case 0:
                    kpiService.incrementXMLRPCRequestTransactionSuccessNA();
                    break;
                case 1:
                    kpiService.incrementXMLRPCRequestTransactionSuccessPayAndDisplay();
                    break;
                case 2:
                    kpiService.incrementXMLRPCRequestTransactionSuccessPayByPlate();
                    break;
                case 3:
                    kpiService.incrementXMLRPCRequestTransactionSuccessPayBySpace();
                    break;
            }
        }
    }
    
    private void doTransactionFailCalculations(ServletRequest request, boolean isPaystationRest) {
        if (isPaystationRest) {
            Boolean isStoreForward = (Boolean) request.getAttribute(KPIConstants.IS_STOREFORWARD);
            if (isStoreForward != null && isStoreForward) {
                kpiService.incrementPaystationRestTransactionFailStoreAndForward();
            }
            Integer paymentType = (Integer) request.getAttribute(KPIConstants.PAYMENT_TYPE);
            if (paymentType != null) {
                switch (paymentType) {
                    case WebCoreConstants.PAYMENT_TYPE_CASH:
                        kpiService.incrementPaystationRestTransactionFailPaymentCash();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                        kpiService.incrementPaystationRestTransactionFailPaymentCredit();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
                        kpiService.incrementPaystationRestTransactionFailPaymentSmart();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
                        kpiService.incrementPaystationRestTransactionFailPaymentCashCredit();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                        kpiService.incrementPaystationRestTransactionFailPaymentCashSmart();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
                        kpiService.incrementPaystationRestTransactionFailPaymentValue();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                        kpiService.incrementPaystationRestTransactionFailPaymentCashValue();
                        break;
                    case WebCoreConstants.PAYMENT_TYPE_UNKNOWN:
                        kpiService.incrementPaystationRestTransactionFailPaymentUnknown();
                        break;
                }
            }
            Integer permissionIssueType = (Integer) request.getAttribute(KPIConstants.PERMIT_ISSUE_TYPE);
            if (permissionIssueType != null) {
                switch (permissionIssueType) {
                    case 0:
                        kpiService.incrementPaystationRestTransactionFailPermitNA();
                        break;
                    case 1:
                        kpiService.incrementPaystationRestTransactionFailPermitPayAndDisplay();
                        break;
                    case 2:
                        kpiService.incrementPaystationRestTransactionFailPermitPayByPlate();
                        break;
                    case 3:
                        kpiService.incrementPaystationRestTransactionFailPermitPayBySpace();
                        break;
                }
            }
            
        } else {
            //        if ((Boolean) request.getAttribute(KPIConstants.ISSTOREFORWARD)) {
            //            kpiService.incrementXMLRPCRequestTransactionFailStoreAndForward();
            //        }
            int paymentType = (Integer) request.getAttribute(KPIConstants.PAYMENT_TYPE);
            switch (paymentType) {
                case WebCoreConstants.PAYMENT_TYPE_CASH:
                    kpiService.incrementXMLRPCRequestTransactionFailCash();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE:
                    kpiService.incrementXMLRPCRequestTransactionFailCreditSwipe();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
                    kpiService.incrementXMLRPCRequestTransactionFailCreditCL();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP:
                    kpiService.incrementXMLRPCRequestTransactionFailCreditCH();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
                    kpiService.incrementXMLRPCRequestTransactionFailSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
                    kpiService.incrementXMLRPCRequestTransactionFailCashCreditSwipe();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL:
                    kpiService.incrementXMLRPCRequestTransactionFailCashCreditCL();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH:
                    kpiService.incrementXMLRPCRequestTransactionFailCashCreditCH();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                    kpiService.incrementXMLRPCRequestTransactionFailCashSmart();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
                    kpiService.incrementXMLRPCRequestTransactionFailValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                    kpiService.incrementXMLRPCRequestTransactionFailCashValue();
                    break;
                case WebCoreConstants.PAYMENT_TYPE_UNKNOWN:
                    kpiService.incrementXMLRPCRequestTransactionFailUnknown();
                    break;
            }
            int permissionIssueType = (Integer) request.getAttribute(KPIConstants.PERMIT_ISSUE_TYPE);
            switch (permissionIssueType) {
                case 0:
                    kpiService.incrementXMLRPCRequestTransactionFailNA();
                    break;
                case 1:
                    kpiService.incrementXMLRPCRequestTransactionFailPayAndDisplay();
                    break;
                case 2:
                    kpiService.incrementXMLRPCRequestTransactionFailPayByPlate();
                    break;
                case 3:
                    kpiService.incrementXMLRPCRequestTransactionFailPayBySpace();
                    break;
            }
        }
    }
}
