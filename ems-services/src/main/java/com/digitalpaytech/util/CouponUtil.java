package com.digitalpaytech.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.InvalidDataException;

public class CouponUtil {
    
    private static Logger log = Logger.getLogger(CouponUtil.class);
    
    public static Coupon createCoupon(PointOfSale pointOfSale, Purchase purchase, TransactionDto td, boolean offlineCouponFlag, String description) {
        Coupon coupon = new Coupon();
        coupon.setCoupon(td.getCouponNumber().toUpperCase());
        coupon.setDescription(description);
        coupon.setNumberOfUsesRemaining(null);
        coupon.setMaxNumberOfUses(null);
        
        if (StringUtils.isNotBlank(td.getCouponAmount())) {
            coupon.setDollarDiscountAmount(new BigDecimal(td.getCouponAmount()).multiply(new BigDecimal(100)).intValue());
            coupon.setPercentDiscount((short) 0);
        } else if (StringUtils.isNotBlank(td.getCouponPercent())) {
            coupon.setDollarDiscountAmount(0);
            coupon.setPercentDiscount(CouponUtil.parseAndDivideIfNecessary(td.getCouponPercent()));
        }
        
        coupon.setIsOffline(offlineCouponFlag);
        coupon.setStartDateLocal(DateUtil.createStartOfTodayDate());
        try {
            coupon.setEndDateLocal(DateUtil.convertFromNewPs2DateString(WebCoreConstants.COUPON_N_A_END));
        } catch (InvalidDataException ide) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("Cannot convert to Date object from: ").append(WebCoreConstants.COUPON_N_A_END);
            bdr.append(", use default end date: ").append(DateUtil.createEndOfTodayDate());
            log.error(bdr.toString(), ide);
            
            coupon.setEndDateLocal(DateUtil.createEndOfTodayDate());
        }
        if (StringUtils.isNotBlank(td.getStallNumber()) && Integer.parseInt(td.getStallNumber()) > 0) {
            coupon.setIsPbsEnabled(true);
        }
        coupon.setIsPndEnabled(true);
        coupon.setSpaceRange(null);
        coupon.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        coupon.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        
        coupon.setCustomer(pointOfSale.getCustomer());
        if (offlineCouponFlag) {
            coupon.setLocation(null);
        } else {
            coupon.setLocation(pointOfSale.getLocation());
        }
        
        purchase.setCoupon(coupon);
        Set<Purchase> set = new HashSet<Purchase>(1);
        set.add(purchase);
        coupon.setPurchases(set);
        
        return coupon;
    }
    
    /**
     * Check if input date is from previous day than today.
     * e.g. date = 23:44 25 March 2013, today = 00:10 26 March 2013, return true.
     * date = 00:10 26 March 2013, today = 23:55 26 March 2013, return false.
     * 
     * @param date
     *            Date for comparison.
     * @return boolean return true if current date is at least a day after input date.
     */
    public static boolean isFromDayBefore(Date date, String customerTimeZone) {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(WebCoreConstants.FORMAT_YYYYMMDD);
        sdf.setTimeZone(TimeZone.getTimeZone(customerTimeZone));
        // Check if today's date is not the same as the input and today is after.
        if (!sdf.format(today).equals(sdf.format(date)) && today.after(date)) {
            return true;
        }
        return false;
    }

    
    /**
     * @param c
     *            First character in the coupon number that's uploaded from pay station.
     * @return Return true if input char is a 'B'.
     */
    public static boolean isOfflineCouponFromPS(String couponCode) {
        if (couponCode.charAt(0) != WebCoreConstants.OFFLINE_COUPON_IDENTIFIER) {
            return false;
        }
        
        String numericPart = couponCode.substring(1);
        try {
            Integer.parseInt(numericPart);
        } catch (NumberFormatException ex) {
            return false;
        }
        
        return true;
    }
    
    /**
     * @param c
     *            First character in the coupon number that's uploaded from pay station.
     * @return Return true if input char is a 'B'.
     */
    public static boolean isOfflineCoupon(char c) {
        if (c == WebCoreConstants.OFFLINE_COUPON_IDENTIFIER) {
            return true;
        }
        return false;
    }
    
    /**
     * @param couponCode
     *            e.g. B12345678+ for offline coupon, BTW224, 12345678 for online.
     * @return Return true if coupon code starts with 'B' and ends with '+'
     */
    public static boolean isOfflineCoupon(String couponCode) {
        if (isOfflineCoupon(couponCode.charAt(0)) && couponCode.endsWith(WebCoreConstants.OFFLINE_COUPON_PLUS)) {
            return true;
        }
        return false;
    }
    
    public static boolean isDiscountValueChanged(Coupon coupon, TransactionDto td) {
        return isDiscountValueChanged(coupon, td.getCouponPercent(), td.getCouponAmount());
    }
    
    public static boolean isDiscountValueChanged(Coupon coupon, String percentDiscount, String dollarDiscount) {
        short percents = (StringUtils.isBlank(percentDiscount)) ? 0 : Short.parseShort(percentDiscount);
        int dollars = (StringUtils.isBlank(dollarDiscount)) ? 0 : WebCoreUtil.convertToBase100IntValue(dollarDiscount);
        
        return isDiscountValueChanged(coupon, percents, dollars);
    }
    
    public static boolean isDiscountValueChanged(Coupon coupon, short percentDiscount, int centsDiscount) {
        return !(((coupon.getPercentDiscount() > 0) && (percentDiscount > 0) && (coupon.getPercentDiscount() == percentDiscount))
                 || ((coupon.getDollarDiscountAmount() > 0) && (centsDiscount > 0) && (coupon.getDollarDiscountAmount() == centsDiscount)) || ((coupon
                .getPercentDiscount() == percentDiscount) && (coupon.getDollarDiscountAmount() == centsDiscount)));
    }
    
    public static short parseAndDivideIfNecessary(final String percent) {
        if (Integer.parseInt(percent) > StandardConstants.CONSTANT_100) {
            return (short) WebCoreUtil.divideBase100ValueBy100(percent);
        } else {
            return Short.parseShort(percent);
        }
    }    
    
    private CouponUtil() {
    }
}
