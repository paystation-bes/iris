package com.digitalpaytech.util;

import java.security.Security;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;

public class CryptoUtil {
	public static final long RELOAD_INTERVAL = 60000L;
	
	private final static Logger logger = Logger.getLogger(CryptoUtil.class);

	private static Crypto hsmCreditCardCrypto = null;
	private static Crypto bcCreditCardCrypto = null;
	
	// It is not a good code to have this here but it is necessary to make things work correctly.
	// We will remove this when we fully moved to CryptoServer.
	private static EmsPropertiesService conf = null;

	private static boolean securityProvidersInitialized_ = false;
	private static boolean isNcipher = false;
	private static long lastReloadTimeStamp = 0;

	private CryptoUtil() {
		// empty constructor
	}

	private static synchronized void addSecurityProviders() {
		if (securityProvidersInitialized_) {
			return;
		}

		// Add security providers
		int provider_list_length = Security.getProviders().length;

		if (StringUtils.isNotBlank(getOperationMode()) && getOperationMode().trim().equals(EncryptionService.PRODUCTION)) {		
		
    		/* UPDATE !!! adding to java.security is NOT enough, need to write code to insert provider.
    		 * 
    		 * For nCipherKM, it should be configured in {JAVA_HOME}/jre/lib/security/java.security file
    		 * based on the NetHSM user guide. 
    		 * Therefore, omit adding nCipher provider here and please refer to the documentation. */
    
            logger.info("nCipherKM provider: "
                    + Security.insertProviderAt(new com.ncipher.provider.km.nCipherKM(), provider_list_length++));
        } else {	
    		logger.info("BC provider: "
    				+ Security
    						.insertProviderAt(
    								new org.bouncycastle.jce.provider.BouncyCastleProvider(),
    								provider_list_length++));
        }
		securityProvidersInitialized_ = true;
	}

	// synchronized so that while the crypto is initializing, another thread
	// doesn't call into this method.
	/*
	 * public static synchronized Crypto
	 * getCreditCardCrypto(EmsPropertiesAppService emsPropertiesAppService)
	 * throws CryptoException { addSecurityProviders();
	 * 
	 * if (creditCardCrypto == null) { String keystore_path =
	 * emsPropertiesAppService.getCryptoDirectory();
	 * 
	 * keystore_path += "creditCardCrypto.keystore";
	 * 
	 * // attempt to use nCipher keystore first try { creditCardCrypto = new
	 * NcipherCrypto(keystore_path); return (creditCardCrypto); } catch
	 * (Exception e) {
	 * logger.info("nCipher Crypto Card not installed, trying Bouncy Castle.",
	 * e); }
	 * 
	 * try { creditCardCrypto = new BouncyCastleCrypto(keystore_path); } catch
	 * (Exception e) { throw new CryptoException("Cannot create Crypto", e); } }
	 * 
	 * return creditCardCrypto; }
	 */

	public static synchronized Crypto getCreditCardCrypto()
            throws CryptoException {
	    final String operationMode = getOperationMode();
        if (operationMode != null
                && operationMode.trim().equals(EncryptionService.PRODUCTION)) {
            logger.debug("!! return hsmCreditCardCrypto !!");
            return hsmCreditCardCrypto;
        } else {
            logger.debug("!! return bcCreditCardCrypto !!");
            return bcCreditCardCrypto;
        }
    }

	public static void initEncryptionMode(
			EmsPropertiesService emsPropertiesService) throws CryptoException {
	    conf = emsPropertiesService;
	    
		getHSMCrypto(emsPropertiesService);
		getBCCrypto(emsPropertiesService);
	}

	public static boolean isHSMMode() {
		return isNcipher;
	}

	private static Crypto getBCCrypto(EmsPropertiesService emsPropertiesService) {
	    try {
    	    addSecurityProviders();
    		if (bcCreditCardCrypto == null) {
    			StringBuilder keystore_path = new StringBuilder(
    					emsPropertiesService.getPropertyValue(
    							EmsPropertiesService.CRYPTO_DIRECTORY,
    							EmsPropertiesService.DEFAULT_CRYPTO_DIRECTORY));
    			if (!keystore_path.toString().endsWith("/")) {
    				keystore_path.append("/");
    			}
    			keystore_path.append("creditCardCrypto.keystore");
				bcCreditCardCrypto = new BouncyCastleCrypto(keystore_path.toString());
				logger.info("!!! Bouncy Castle installed. !!!");
				
				lastReloadTimeStamp = System.currentTimeMillis();
    		}
        } catch (Exception e) {
            bcCreditCardCrypto = null;
            
    		boolean useBouncyCastle = false;
    		
    		final String encryptionMode = getOperationMode();
    		if(encryptionMode != null) {
    			useBouncyCastle = EncryptionService.DEVELOPMENT.equals(encryptionMode);
    		}
    		
    		if(useBouncyCastle) {
    			logger.error("!!! Bouncy Castle **NOT** correctly installed. !!!", e);
    		}
    		else {
    			logger.info("Bouncy Castle is not available for this instance !");
    		}
        }    		
		return bcCreditCardCrypto;
	}

	private static Crypto getHSMCrypto(EmsPropertiesService emsPropertiesService) {
	    try {
    	    addSecurityProviders();
    		if (hsmCreditCardCrypto == null) {
    			StringBuilder keystore_path = new StringBuilder(
    					emsPropertiesService.getPropertyValue(
    							EmsPropertiesService.CRYPTO_DIRECTORY,
    							EmsPropertiesService.DEFAULT_CRYPTO_DIRECTORY));
    			if (!keystore_path.toString().endsWith("/")) {
    				keystore_path.append("/");
    			}
    			keystore_path.append("creditCardCrypto.keystore");
				hsmCreditCardCrypto = new NcipherCrypto(keystore_path.toString());
				isNcipher = true;
				logger.info("!!! nCipher Crypto Card installed. !!!");
				
				lastReloadTimeStamp = System.currentTimeMillis();
    		}
        } catch (Exception e) {
            isNcipher = false;
            hsmCreditCardCrypto = null;
            
            boolean useNCipher = false;
    		
    		final String encryptionMode = getOperationMode();
    		if(encryptionMode != null) {
    			useNCipher = EncryptionService.PRODUCTION.equals(encryptionMode);
    		}
    		
    		if(useNCipher) {
    			logger.error("!!! nCipher Crypto Card **NOT** installed. !!!", e);
    		}
    		else {
    			logger.info("nCipher is not available for this instance !");
    		}
        }
		return hsmCreditCardCrypto;
	}
	
	public static synchronized void reloadCryptoDirectory(EmsPropertiesService emsPropertiesService) {
		StringBuilder keystore_path = new StringBuilder(
				emsPropertiesService.getPropertyValue(
						EmsPropertiesService.CRYPTO_DIRECTORY,
						EmsPropertiesService.DEFAULT_CRYPTO_DIRECTORY));
		if (!keystore_path.toString().endsWith("/")) {
			keystore_path.append("/");
		}
		
		boolean useNCipher = false;
		boolean useBouncyCastle = false;
		
		final String encryptionMode = getOperationMode();
		if(encryptionMode != null) {
			useNCipher = EncryptionService.PRODUCTION.equals(encryptionMode);
			useBouncyCastle = EncryptionService.DEVELOPMENT.equals(encryptionMode);
		}
		
		keystore_path.append("creditCardCrypto.keystore");
		
		long now = System.currentTimeMillis();
		if((now - lastReloadTimeStamp) > RELOAD_INTERVAL) {
			String keystorePath = keystore_path.toString();
			try {
				Crypto hsmBuffer = new NcipherCrypto(keystorePath);
				hsmCreditCardCrypto = hsmBuffer;
			}
			catch(Exception e) {
				if(useNCipher) {
					logger.error("Could not reload nCipher Crypto Path !!!", e);
				}
				else {
					logger.info("nCipher is not available for this instance !");
				}
			}
			
			try {
				Crypto bcBuffer = new BouncyCastleCrypto(keystorePath);
				bcCreditCardCrypto = bcBuffer;
			}
			catch(Exception e) {
				if(useBouncyCastle) {
					logger.error("Could not reload Bouncy Castle Crypto Path !!!", e);
				}
				else {
					logger.info("Bouncy Castle is not available for this instance !");
				}
			}
		}
	}

	public static String getOperationMode() {
		return conf.getPropertyValue(EmsPropertiesService.ENCRYPTION_MODE, null, false);
	}
	
	/**
	 * Escapes '=' sign and not all digits, returns false. Otherwise returns true.
	 * @param cardData
	 * @return boolean true if all digits no matter if '=' exists or not.
	 */
	public static boolean isDecryptedCardData(final String cardData) {
	    /*
	     * Returns 'false' if:
	     *     - cardData is '' or null
	     *     - cardData is not all digits (ignore '=' sign)
	     */
	    if (StringUtils.isBlank(cardData) || !(WebCoreUtil.checkIfAllDigits(cardData) 
                || WebCoreUtil.escapeCharsAndCheckIfAllDigits(new char[] { StandardConstants.CHAR_EQUALS }, cardData))) {
	        return false;
	    }
	    return true;
	}
}
