package com.digitalpaytech.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.AlertWidgetType;
import com.digitalpaytech.dto.CardProcMethodType;
import com.digitalpaytech.dto.CitatType;
import com.digitalpaytech.dto.CollType;
import com.digitalpaytech.dto.DisplayType;
import com.digitalpaytech.dto.FilterType;
import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.MerchType;
import com.digitalpaytech.dto.MetricType;
import com.digitalpaytech.dto.OrganizationType;
import com.digitalpaytech.dto.RangeType;
import com.digitalpaytech.dto.RateType;
import com.digitalpaytech.dto.RevType;
import com.digitalpaytech.dto.RouteType;
import com.digitalpaytech.dto.TierType;
import com.digitalpaytech.dto.TxnType;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.WebWidgetHelperService;

public final class WebWidgetUtil {
    
    private static Logger log = Logger.getLogger(WebWidgetUtil.class);
    
    private WebWidgetUtil() {
    }
    
    public static WidgetData getStartTimeAndInterval(final Widget widget, final WidgetData widgetData, final Date startTime) {
        
        final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        // 1 Hour in milliseconds
        final String hour = "3600000";
        // 1 Day in milliseconds
        final String day = "86400000";
        
        switch (tier1Id) {
            case WidgetConstants.TIER_TYPE_HOUR:
                /* Hour */
                widgetData.setStartTime(startTime.getTime());
                widgetData.setTimeInterval(hour);
                break;
            case WidgetConstants.TIER_TYPE_DAY:
                /* Day */
                widgetData.setStartTime(startTime.getTime());
                widgetData.setTimeInterval(day);
                break;
            case WidgetConstants.TIER_TYPE_MONTH:
                widgetData.setTimeInterval(WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING);
                break;
            default:
                break;
        }
        return widgetData;
    }
    
    public static SQLQuery querySetParameter(final WebWidgetHelperService webWidgetHelperService, final Widget widget, final SQLQuery query,
        final UserAccount userAccount, final boolean isGMT) {
        
        //Deprecated; subsets will be taken from widget object due to possible temporary changes made in
        //edit mode which will not yet have been propagated to the database
        //      used for subsets, so the dynamic query knows what to compare to in the sub query
        /*
         * if(widget.getWidgetFilterType().getId() == WebCoreConstants.WIDGET_FILTER_TYPE_SUBSET){
         * query.setParameter("widgetId", widget.getId());
         * if(widget.isIsSubsetTier1()){
         * //query.setParameter("widgetTier1TypeId", widget.getWidgetTierTypeByWidgetTier1Type());
         * }
         * if(widget.isIsSubsetTier2()){
         * query.setParameter("widgetTier2TypeId", widget.getWidgetTierTypeByWidgetTier2Type());
         * }
         * if(widget.isIsSubsetTier3()){
         * query.setParameter("widgetTier3TypeId", widget.getWidgetTierTypeByWidgetTier3Type());
         * }
         * }
         * else
         */
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP
            || widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
            query.setParameter("N", widget.getWidgetLimitType().getRowLimit());
        }
        
        if (userAccount.getCustomer().isIsParent()) {
            final List<Integer> childCustomerIdList = webWidgetHelperService.getAllChildCustomerId(userAccount.getCustomer().getId());
            if (childCustomerIdList != null && !childCustomerIdList.isEmpty()) {
                query.setParameterList("customerId", webWidgetHelperService.getAllChildCustomerId(userAccount.getCustomer().getId()));
            } else {
                query.setParameter("customerId", userAccount.getCustomer().getId());
            }
        } else {
            query.setParameter("customerId", userAccount.getCustomer().getId());
        }
        
        if (widget.getWidgetRangeType().getId() > 0 && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS) {
            int customerId = widget.getCustomerId();
            
            if (customerId == 0) {
                customerId = userAccount.getCustomer().getId();
            }
            
            final String timezone = webWidgetHelperService.getCustomerTimeZoneByCustomerId(customerId);
            if (isGMT) {
                query.setParameter("currentLocalTime", widget.getWidgetRunDate());
            } else {
                query.setParameter("currentLocalTime", DateUtil.changeTimeZone(widget.getWidgetRunDate(), timezone));
            }
        }
        return query;
    }
    
    public static SQLQuery queryAddScalar(final Widget widget, final SQLQuery query) {
        
        final BigDecimalType bigDecimalType = new BigDecimalType();
        final IntegerType integerType = new IntegerType();
        final StringType stringType = new StringType();
        final BooleanType booleanType = new BooleanType();
        final TimestampType timestampType = new TimestampType();
        
        query.addScalar("CustomerId", integerType);
        
        if (widget.getWidgetTierTypeByWidgetTier1Type().getId() > 0) {
            query.addScalar("Tier1TypeId", integerType);
            query.addScalar("Tier1TypeName", stringType);
            query.addScalar("Tier1TypeHidden", booleanType);
        }
        
        if (widget.getWidgetTierTypeByWidgetTier2Type().getId() > 0) {
            query.addScalar("Tier2TypeId", integerType);
            query.addScalar("Tier2TypeName", stringType);
            query.addScalar("Tier2TypeHidden", booleanType);
        }
        
        if (widget.getWidgetTierTypeByWidgetTier3Type().getId() > 0) {
            query.addScalar("Tier3TypeId", integerType);
            query.addScalar("Tier3TypeName", stringType);
            query.addScalar("Tier3TypeHidden", booleanType);
        }
        
        if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP) {
            query.addScalar("JsonString", stringType);
        }
        
        query.addScalar("WidgetMetricValue", bigDecimalType);
        
        if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_DAY
            || widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_HOUR) {
            query.addScalar("StartTime", timestampType);
        }
        
        return query;
    }
    
    public static List<WidgetMetricInfo> getTopBottom(final List<WidgetMetricInfo> rawWidgetData, final Integer count) {
        
        if (rawWidgetData.size() <= (2 * count)) {
            return rawWidgetData;
        } else {
            final List<WidgetMetricInfo> modifiedWidgetData = rawWidgetData.subList(0, count);
            modifiedWidgetData.addAll(rawWidgetData.subList(rawWidgetData.size() - count, rawWidgetData.size()));
            return modifiedWidgetData;
        }
    }
    
    /**
     * Uses org.apache.commons.beanutils.BeanUtils.copyProperties to copy property values from the origin bean to
     * the destination bean for all cases where the property names are the same.
     * public static void copyProperties(Object dest,
     * Object orig)
     * throws IllegalAccessException, InvocationTargetException
     * 
     * @return Object the copied object.
     */
    public static Object copyObject(final Object dest, final Object orig) {
        try {
            BeanUtils.copyProperties(dest, orig);
        } catch (IllegalAccessException iae) {
            log.error("copyObject error, original object is: " + orig, iae);
        } catch (InvocationTargetException ite) {
            log.error("copyObject reflection error, original object is: " + orig, ite);
        }
        return dest;
    }
    
    public static List<LocationType> copyToLocationTypes(final List<Location> locations) {
        final List<LocationType> locTypes = new ArrayList<LocationType>(locations.size());
        for (Location loc : locations) {
            final LocationType newLocType = new LocationType();
            copyObject(newLocType, loc);
            // orgId will be changed to randomId in WidgetSettingsController, convertIdToRandomId method.           
            newLocType.setOrgId(String.valueOf(loc.getCustomer().getId()));
            locTypes.add(newLocType);
        }
        return locTypes;
    }
    
    public static List<MetricType> copyToMetricTypes(final List<WidgetMetricType> widgetMetricTypes) {
        final List<MetricType> metricTypes = new ArrayList<MetricType>(widgetMetricTypes.size());
        for (WidgetMetricType widgetMetricType : widgetMetricTypes) {
            final MetricType newType = new MetricType();
            copyObject(newType, widgetMetricType);
            metricTypes.add(newType);
        }
        return metricTypes;
    }
    
    public static List<TierType> copyToTierTypes(final List<WidgetTierType> widgetTierTypes, final boolean parentOrganizationFlag) {
        final List<TierType> tierTypes = new ArrayList<TierType>(widgetTierTypes.size());
        for (WidgetTierType widgetTierType : widgetTierTypes) {
            final TierType newType = new TierType();
            copyObject(newType, widgetTierType);
            // If name is 'Parent Organization' and customer DOESN'T have parentCustomer, set status to NOT_AVAILABLE.
            //TODO removed
            //          if (parentOrganizationFlag && widgetTierType.getId() == WidgetConstants.TIER_TYPE_PARENT_ORG) {
            //              newType.setStatus(WebCoreConstants.IN_ACTIVE);
            //          }           
            tierTypes.add(newType);
        }
        return tierTypes;
    }
    
    public static List<RevType> copyToRevTypes(final List<RevenueType> revenueTypes) {
        final List<RevType> revTypes = new ArrayList<RevType>(revenueTypes.size());
        for (RevenueType revenueType : revenueTypes) {
            final RevType newType = new RevType();
            copyObject(newType, revenueType);
            revTypes.add(newType);
        }
        return revTypes;
    }
    
    public static List<RouteType> copyToRouteTypes(final List<Route> routes) {
        final List<RouteType> rouTypes = new ArrayList<RouteType>(routes.size());
        for (Route route : routes) {
            final RouteType newType = new RouteType();
            copyObject(newType, route);
            // orgId will be changed to randomId in WidgetSettingsController, convertIdToRandomId method.           
            newType.setOrgId(String.valueOf(route.getCustomer().getId()));
            rouTypes.add(newType);
        }
        return rouTypes;
    }
    
    public static List<TxnType> copyToTxnTypes(final List<TransactionType> transactionTypes) {
        final List<TxnType> txnTypes = new ArrayList<TxnType>(transactionTypes.size());
        for (TransactionType transactionType : transactionTypes) {
            final TxnType newType = new TxnType();
            copyObject(newType, transactionType);
            txnTypes.add(newType);
        }
        return txnTypes;
    }
    
    public static List<CardProcMethodType> copyCardProcMethodTypes(final List<CardProcessMethodType> cardProcessMethodTypes) {
        final List<CardProcMethodType> cardProcMethodTypes = new ArrayList<CardProcMethodType>(cardProcessMethodTypes.size());
        for (CardProcessMethodType cardType : cardProcessMethodTypes) {
            final CardProcMethodType newType = new CardProcMethodType();
            copyObject(newType, cardType);
            cardProcMethodTypes.add(newType);
        }
        return cardProcMethodTypes;
    }
    
    public static List<FilterType> copyToFilterTypes(final List<WidgetFilterType> widgetFilterTypes) {
        final List<FilterType> fTypes = new ArrayList<FilterType>(widgetFilterTypes.size());
        for (WidgetFilterType widgetFilterType : widgetFilterTypes) {
            final FilterType newType = new FilterType();
            copyObject(newType, widgetFilterType);
            fTypes.add(newType);
        }
        return fTypes;
    }
    
    public static List<DisplayType> copyToDisplayTypes(final List<WidgetChartType> widgetChartTypes) {
        final List<DisplayType> dTypes = new ArrayList<DisplayType>(widgetChartTypes.size());
        for (WidgetChartType widgetChartType : widgetChartTypes) {
            final DisplayType newType = new DisplayType();
            copyObject(newType, widgetChartType);
            newType.setActuaId(widgetChartType.getId());
            
            dTypes.add(newType);
        }
        return dTypes;
    }
    
    public static List<RateType> copyToRateTypes(final List<UnifiedRate> rates) {
        final List<RateType> rTypes = new ArrayList<RateType>(rates.size());
        for (UnifiedRate rate : rates) {
            final RateType newType = new RateType();
            copyObject(newType, rate);
            // orgId will be changed to randomId in WidgetSettingsController, convertIdToRandomId method.
            newType.setOrgId(String.valueOf(rate.getCustomer().getId()));
            rTypes.add(newType);
        }
        return rTypes;
    }
    
    public static List<RangeType> copyToRangeTypes(final List<WidgetRangeType> widgetRangeTypes) {
        final List<RangeType> raTypes = new ArrayList<RangeType>(widgetRangeTypes.size());
        for (WidgetRangeType wrt : widgetRangeTypes) {
            final RangeType newType = new RangeType();
            copyObject(newType, wrt);
            raTypes.add(newType);
        }
        return raTypes;
    }
    
    public static List<OrganizationType> copyToOrganizationTypes(final List<Customer> customers) {
        final List<OrganizationType> orgTypes = new ArrayList<OrganizationType>(customers.size());
        for (Customer customer : customers) {
            final OrganizationType orgType = new OrganizationType();
            copyObject(orgType, customer);
            orgTypes.add(orgType);
        }
        return orgTypes;
    }
    
    public static List<CollType> copyToCollTypes(final List<CollectionType> collections) {
        final List<CollType> collectionTypes = new ArrayList<CollType>(collections.size());
        for (CollectionType collectionType : collections) {
            final CollType collType = new CollType();
            copyObject(collType, collectionType);
            collectionTypes.add(collType);
        }
        return collectionTypes;
    }
    
    public static List<MerchType> copyToMerchTypes(final List<MerchantAccount> merchantAccounts) {
        final List<MerchType> merchTypes = new ArrayList<MerchType>(merchantAccounts.size());
        for (MerchantAccount merchantAccount : merchantAccounts) {
            final MerchType merchType = new MerchType();
            copyObject(merchType, merchantAccount);
            merchTypes.add(merchType);
        }
        return merchTypes;
    }
    
    public static List<AlertWidgetType> copyToAlertTypes(final List<AlertClassType> alertClassTypes) {
        final List<AlertWidgetType> alertTypes = new ArrayList<AlertWidgetType>(alertClassTypes.size());
        for (AlertClassType alertClassType : alertClassTypes) {
            final AlertWidgetType newType = new AlertWidgetType();
            copyObject(newType, alertClassType);
            alertTypes.add(newType);
        }
        return alertTypes;
    }
    
    public static List<CitatType> copyToCitatTypes(final List<CitationType> citationTypes) {
        final List<CitatType> citatTypes = new ArrayList<CitatType>(citationTypes.size());
        for (CitationType citationType : citationTypes) {
            final CitatType citatType = new CitatType();
            copyObject(citatType, citationType);
            citatTypes.add(citatType);
        }
        return citatTypes;
    }
}
