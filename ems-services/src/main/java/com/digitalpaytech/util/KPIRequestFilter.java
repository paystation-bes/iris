package com.digitalpaytech.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.servlet.DPTHttpServletResponseWrapper;

/**
 * This class is for monitoring different status codes
 * Filters defined in web.xml
 * 
 * @author Amanda Chong
 */

public class KPIRequestFilter implements Filter {
    
    private KPIService kpiService;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
        try {
            kpiService = (KPIService) context.getBean(KPIService.class);
        } catch (NoSuchBeanDefinitionException e) {
            kpiService = null;
        }
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (kpiService == null) {
            chain.doFilter(request, response);
        } else {
            DPTHttpServletResponseWrapper responseWrapper = new DPTHttpServletResponseWrapper((HttpServletResponse) response);
            chain.doFilter(request, responseWrapper);
            
            int status = responseWrapper.getStatus();
            if (status < 300) {
                String customStatus = responseWrapper.getHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE);
                if (customStatus != null) {
                    try {
                        status = Integer.parseInt(customStatus);
                        switch (status) {
                            case WebCoreConstants.HTTP_STATUS_CODE_449: {
                                kpiService.incrementUIRequest449();
                                break;
                            }
                            case WebCoreConstants.HTTP_STATUS_CODE_480: {
                                kpiService.incrementUIRequest480();
                                break;
                            }
                            case WebCoreConstants.HTTP_STATUS_CODE_481: {
                                kpiService.incrementUIRequest481();
                                break;
                            }
                            case WebCoreConstants.HTTP_STATUS_CODE_482: {
                                kpiService.incrementUIRequest482();
                                break;
                            }
                            case WebCoreConstants.HTTP_STATUS_CODE_483: {
                                kpiService.incrementUIRequest483();
                                break;
                            }
                            case WebCoreConstants.HTTP_STATUS_CODE_509: {
                                kpiService.incrementUIRequest509();
                                break;
                            }
                        }
                    } catch (NumberFormatException e) {
                        // DO NOTHING.
                    }
                }
                
            } else {
                String requestURI = ((HttpServletRequest) request).getRequestURI();
                if (requestURI.contains(KPIConstants.PAYSTATION_REST)) {
                    switch (status) {
                        case PaystationConstants.FAILURE_INVALID_PARAMETER:
                            kpiService.incrementPaystationRestFailParameterError();
                            break;
                        
                        case PaystationConstants.FAILURE_SIGNATURE_ERROR:
                            kpiService.incrementPaystationRestFailSignatureError();
                            break;
                        
                        case PaystationConstants.FAILURE_INTERNAL_ERROR:
                            kpiService.incrementPaystationRestFailSystemError();
                            break;
                    }
                    
                }
            }
            
            if (status < HttpServletResponse.SC_OK) {
                kpiService.incrementUIRequestInformation();
            } else if (status < 300) {
                kpiService.incrementUIRequestSuccess();
            } else if (status < 400) {
                kpiService.incrementUIRequestRedirect();
            } else if (status < 500) {
                kpiService.incrementUIRequestClientError();
            } else if (status < 600) {
                kpiService.incrementUIRequestServerError();
            }
        }
    }
    
    @Override
    public void destroy() {
        
    }
}
