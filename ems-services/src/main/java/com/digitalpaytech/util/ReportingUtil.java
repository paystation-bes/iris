package com.digitalpaytech.util;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ReportUIFilterType;

public interface ReportingUtil {
    
    List<ReportUIFilterType> getFilterList(int reportType, int filterType);
    
    Map<Integer, List<ReportUIFilterType>> getReportAllFilters(int reportType);
    
    String findFilterString(Integer filterValueTypeId);
    
    String getPropertyEN(String messageKey);
    
    String getProperty(String messageKey, Locale locale);
    
    String getPropertyEN(String messageKey, String[] args);
    
    String getProperty(String messageKey, String[] args, Locale locale);
    
    void calculateNextReportDate(ReportDefinition reportDefinition, Date initialDateGmt, String timeZone, boolean isInitial);
    
    ReportQueue createReportQueueEntry(ReportDefinition reportDefinition, String timeZone, boolean isRerun);
    
    int getTimeZoneOffset(String timeZone);
    
    boolean filterValueValid(int reportType, int filterType, int value);
    
    List<FilterDTO> getSeverityFilters(List<FilterDTO> result, RandomKeyMapping keyMapping);
    
    List<FilterDTO> getModuleFilters(List<FilterDTO> result, RandomKeyMapping keyMapping);
}
