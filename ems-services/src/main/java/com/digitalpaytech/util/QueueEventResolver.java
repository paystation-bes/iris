package com.digitalpaytech.util;

import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.util.queue.RoutingKeyResolver;

public class QueueEventResolver implements RoutingKeyResolver<QueueEvent> {
    public QueueEventResolver() {
        
    }
    
    @Override
    public final String resolve(final QueueEvent message) {
        return Integer.toString(message.getPointOfSaleId());
    }
}
