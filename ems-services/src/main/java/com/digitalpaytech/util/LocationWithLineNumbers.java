package com.digitalpaytech.util;

import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.util.support.ObjectHelper;

public class LocationWithLineNumbers extends Location {
	private static final long serialVersionUID = -7477284013887587561L;
	
	private ArrayList<Integer> lineNumbers = new ArrayList<Integer>();
	
	public LocationWithLineNumbers() {
		
	}
	
	public List<Integer> getLineNumbers() {
		return lineNumbers;
	}
	
	public static class Helper implements ObjectHelper<Location, String> {
		public Helper() {
			
		}
		
		@Override
		public Location createObject(String initVal) {
			LocationWithLineNumbers result = new LocationWithLineNumbers();
			result.setName(initVal);
			
			return result;
		}

		@Override
		public void copyProperties(Location source, Location destination) {
			destination.setId(source.getId());
			destination.setName(source.getName());
		}
		
	}
}
