package com.digitalpaytech.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.service.kpi.KPIService;

/**
 * This class is used to monitor Widgets and DigitalAPI calls
 * 
 * @author Amanda Chong
 */

@Aspect
public final class KPIInterceptor {
    
    @Autowired
    private KPIService kpiService;
    
    @Pointcut("execution(public * *(..))")
    private void anyPublicMethod() {
    }
    
    @Pointcut("execution(public * *(..)) && execution(* get*(..))")
    private void anyPublicGetMethod() {
    }
    
    // Dashboard Widgets
    
    @Pointcut("execution(* getWidgetData(..))")
    private void anyGetWidgetDataMethod() {
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetActiveAlertsService) && anyGetWidgetDataMethod()")
    private void anyWidgetAlertCall() {
    }
    
    @Around("anyWidgetAlertCall()")
    public WidgetData aroundWidgetAlert(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeAlerts(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetRevenueService) && anyGetWidgetDataMethod()")
    private void anyWidgetRevenueCall() {
    }
    
    @Around("anyWidgetRevenueCall()")
    public WidgetData aroundWidgetRevenue(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeRevenue(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetCollectionsService) && anyGetWidgetDataMethod()")
    private void anyWidgetCollectionsCall() {
    }
    
    @Around("anyWidgetCollectionsCall()")
    public WidgetData aroundWidgetCollections(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeCollections(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetSettledCardService) && anyGetWidgetDataMethod()")
    private void anyWidgetSettledCardCall() {
    }
    
    @Around("anyWidgetSettledCardCall()")
    public WidgetData aroundWidgetSettledCard(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeSettledCard(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetPurchasesService) && anyGetWidgetDataMethod()")
    private void anyWidgetPurchasesCall() {
    }
    
    @Around("anyWidgetPurchasesCall()")
    public WidgetData aroundWidgetPurchases(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimePurchases(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetPaidOccupancyService) && anyGetWidgetDataMethod()")
    private void anyWidgetPaidOccupancyCall() {
    }
    
    @Around("anyWidgetPaidOccupancyCall()")
    public WidgetData aroundWidgetPaidOccupancy(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimePaidOccupancy(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetTurnoverService) && anyGetWidgetDataMethod()")
    private void anyWidgetTurnoverCall() {
    }
    
    @Around("anyWidgetTurnoverCall()")
    public WidgetData aroundWidgetTurnover(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeTurnover(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetUtilizationService) && anyGetWidgetDataMethod()")
    private void anyWidgetUtilizationtCall() {
    }
    
    @Around("anyWidgetUtilizationtCall()")
    public WidgetData aroundWidgetUtilization(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeUtilization(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    @Pointcut("anyPublicMethod() && bean(webWidgetMapService) && anyGetWidgetDataMethod()")
    private void anyWidgetMapCall() {
    }
    
    @Around("anyWidgetMapCall()")
    public WidgetData aroundWidgetMap(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final WidgetData widgetData = (WidgetData) pjp.proceed();
        this.kpiService.addUIDashboardResponseTimeMap(System.currentTimeMillis() - startTime);
        return widgetData;
    }
    
    // AuditInfoService
    
    @Pointcut("anyPublicGetMethod() && bean(auditInfoService)")
    private void anyAuditInfoServiceCall() {
    }
    
    @Around("anyAuditInfoServiceCall()")
    public Object aroundAudit(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object object = pjp.proceed();
        this.kpiService.addDigitalAPIResponseTimeAuditInfo(System.currentTimeMillis() - startTime);
        return object;
    }
    
    @AfterReturning("anyAuditInfoServiceCall()")
    public void afterReturningAudit(final JoinPoint joinPoint) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestSuccessAuditInfo();
    }
    
    @AfterThrowing(pointcut = "anyAuditInfoServiceCall()", throwing = "ex")
    public void afterThrowingAudit(final JoinPoint joinPoint, final Throwable ex) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestFailAuditInfo();
    }
    
    // PaystationInfoService
    
    @Pointcut("anyPublicGetMethod() && bean(paystationInfoService)")
    private void anyPaystationInfoServiceCall() {
    }
    
    @Around("anyPaystationInfoServiceCall()")
    public Object aroundPaystation(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object object = pjp.proceed();
        this.kpiService.addDigitalAPIResponseTimePaystationInfo(System.currentTimeMillis() - startTime);
        return object;
    }
    
    @AfterReturning("anyPaystationInfoServiceCall()")
    public void afterReturningPaystation(final JoinPoint joinPoint) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestSuccessPaystationInfo();
    }
    
    @AfterThrowing(pointcut = "anyPaystationInfoServiceCall()", throwing = "ex")
    public void afterThrowingPaystation(final JoinPoint joinPoint, final Throwable ex) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestFailPaystationInfo();
    }
    
    // TransactionInfoService
    
    @Pointcut("anyPublicGetMethod() && bean(transactionInfoService)")
    private void anyTransactionInfoServiceCall() {
    }
    
    @Around("anyTransactionInfoServiceCall()")
    public Object aroundTransaction(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object object = pjp.proceed();
        this.kpiService.addDigitalAPIResponseTimeTransactionInfo(System.currentTimeMillis() - startTime);
        return object;
    }
    
    @AfterReturning("anyTransactionInfoServiceCall()")
    public void afterReturningTransaction(final JoinPoint joinPoint) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestSuccessTransactionInfo();
    }
    
    @AfterThrowing(pointcut = "anyTransactionInfoServiceCall()", throwing = "ex")
    public void afterThrowingTransaction(final JoinPoint joinPoint, final Throwable ex) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestFailTransactionInfo();
    }
    
    // PlateInfoService
    
    @Pointcut("anyPublicGetMethod() && bean(plateInfoService)")
    private void anyPlateInfoServiceCall() {
    }
    
    @Around("anyPlateInfoServiceCall()")
    public Object aroundPlate(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object object = pjp.proceed();
        this.kpiService.addDigitalAPIResponseTimePlateInfo(System.currentTimeMillis() - startTime);
        return object;
    }
    
    @AfterReturning("anyPlateInfoServiceCall()")
    public void afterReturningPlate(final JoinPoint joinPoint) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestSuccessPlateInfo();
    }
    
    @AfterThrowing(pointcut = "anyPlateInfoServiceCall()", throwing = "ex")
    public void afterThrowingPlate(final JoinPoint joinPoint, final Throwable ex) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestFailPlateInfo();
    }
    
    // StallInfoService
    
    @Pointcut("anyPublicGetMethod() && bean(stallInfoService)")
    private void anyStallInfoServiceCall() {
    }
    
    @Around("anyStallInfoServiceCall()")
    public Object aroundStall(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object object = pjp.proceed();
        this.kpiService.addDigitalAPIResponseTimeStallInfo(System.currentTimeMillis() - startTime);
        return object;
    }
    
    @AfterReturning("anyStallInfoServiceCall()")
    public void afterReturningStall(final JoinPoint joinPoint) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestSuccessStallInfo();
    }
    
    @AfterThrowing(pointcut = "anyStallInfoServiceCall()", throwing = "ex")
    public void afterThrowingStall(final JoinPoint joinPoint, final Throwable ex) throws Throwable {
        this.kpiService.incrementDigitalAPIRequestFailStallInfo();
    }
    
    // EmailService
    
    @Pointcut("bean(emailService) && execution(public * sendNow(..))")
    public void anySendEmail() {
    }
    
    @Around("anySendEmail()")
    public Object aroundSendEmail(final ProceedingJoinPoint pjp) throws Throwable {
        final long startTime = System.currentTimeMillis();
        final Object result = pjp.proceed();
        this.kpiService.addBackgroundProcessesEmailAlertResponseTime(System.currentTimeMillis() - startTime);
        return result;
    }
}
