package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
import java.util.zip.ZipOutputStream;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.util.RandomKeyMapping;

public interface CustomerCardTypeService {
    CustomerCardType getUnknownCustomerCardType();
    
    // Credit Card Customer Card
    CustomerCardType getCreditCardTypeByTrack2Data(String track2Data);
    
    CustomerCardType getCreditCardTypeByAccountNumber(String accountNumber);
    
    // Customer Card Type
    CustomerCardType getCardTypeByAccountNumber(String accountNumber, List<CustomerCardType> customerCardTypes);
    
    CustomerCardType getCardTypeByTrack2Data(String track2Data, List<CustomerCardType> customerCardTypes);
    
    CustomerCardType getCardTypeByAccountNumber(int customerId, String accountNumber);
    
    CustomerCardType getCardTypeByTrack2Data(int customerId, String track2Data);
    
    CustomerCardType getCardTypeByTrack2DataOrAccountNumber(int customerId, String track2Data);
    
    List<CustomerCardType> getAllCustomerCardType(int customerId);
    
    CustomerCardType getCardType(int id);
    
    void createCustomerCardType(CustomerCardType cardType);
    
    void updateCardType(CustomerCardType cardType);
    
    void deleteCardType(CustomerCardType cardType);
    
    boolean isExistCardType(CustomerCardType cardType);
    
    int getTotalCustomerCardType(CustomerCardType cardType);
    
    List<CustomerCardType> getCustomerCardTypesByCustomerAndName(int customerId, String name);
    
    boolean isCreditCardData(String track2OrAcctNum);
    
    boolean isCreditCardAccountNumber(String accountNumber);
    
    boolean isCreditCardTrack2(String track2Data);
    
    CustomerCardType getCreditCardTypeByCardNumber(int customerId, String cardNumber);
    
    boolean isExistTrack2Pattern(Integer customerId, String track2pattern);
    
    // It's looking into CustomerBadCard table.
    CustomerCardType findCustomerCardTypeForBadCard(Integer customerId, int cardTypeId);
    
    boolean generateBadCardList(int customerId, int cardTypeId, ZipOutputStream zipfos);
    
    CustomerCardType findCustomerCardTypeForTransaction(Integer customerId, Integer cardTypeId, String cardNumber);
    
    List<CustomerCardType> findByCustomerIdCardTypeIdIsLocked(Integer customerId, Integer cardTypeId, boolean isLocked);
    
    List<CustomerCardType> findByCustomerIdIsLocked(Integer customerId, boolean isLocked);
    
    CustomerCardType getCustomerCardTypeById(int id);
    
    CustomerCardType getPrimaryType(Integer customerId, String name);
    
    void ensurePrimaryReference(Integer... customerCardTypeIds);
    
    Future<Boolean> ensurePrimaryReferenceAsync(Integer... customerCardTypeIds);
    
    CustomerCardType createPrimaryTypeIfAbsent(Integer customerId, String name) throws Exception;
    
    CustomerCardType createPrimaryTypeIfAbsentFor(Integer customerCardTypeId) throws Exception;
    
    int deletePrimaryIfNoChild(CustomerCardType primary);
    
    int countChildType(Integer customerCardTypeId);
    
    List<FilterDTO> getBannableCardTypeFilters(List<FilterDTO> result, Integer customerId, RandomKeyMapping keyMapping);
    
    List<CustomerCardType> getDetachedBannableCardTypes(Integer customerId, Collection<String> names);
    
    List<FilterDTO> getValueCardTypeFilters(List<FilterDTO> result, Integer customerId, boolean showDeleted, RandomKeyMapping keyMapping);
    
    List<FilterDTO> getValueCardTypeFiltersWithDeletedFlag(List<FilterDTO> result, Integer customerId, boolean showDeleted,
        RandomKeyMapping keyMapping);
    
    List<CustomerCardType> getDetachedValueCardTypes(Integer customerId, Collection<String> names);
    
    List<CustomerCardType> getDetachedBannableCardTypes(int customerId);
}
