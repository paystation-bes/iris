package com.digitalpaytech.service;

import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;

import java.util.Set;

public interface ExtensibleRateDayOfWeekService {
    public Set<ExtensibleRateDayOfWeek> findByExtensibleRateId(Integer extensibleRateId);
    public void deleteByExtensibleRateId(Integer extensibleRateId); 
}
