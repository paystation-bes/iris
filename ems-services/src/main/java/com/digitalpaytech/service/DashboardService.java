package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.TabsGroup;

public interface DashboardService {
    
    Collection<Tab> findTabsByUserAccountId(int userAccountId);
    
    Tab findTabByIdEager(int id);
    
    Collection<Tab> findDefaultTabs();
    
    Collection<Tab> findDefaultTabs(boolean parentFlag);
    
    Widget findWidgetById(int widgetId);
    
    Widget findDefaultWidgetByIdAndEvict(int widgetId);
    
    void deleteTabsByUserId(int userId);
    
    void delete(Object object);
    
    void saveOrUpdateTab(Tab id, UserAccount userAccount);
    
    void saveTabs(TabsGroup tabsGroup, UserAccount userAccount);
    
    void updateUserAccount(UserAccount userAccount, UserAccount mergedUserAccount);
    
    List<Location> findLocationsByCustomerId(int customerId);
    
    List<Customer> findAllChildCustomers(int parentCustomerId);
    
    List<Route> findRoutesByCustomerId(int customerId);
    
    WidgetListType getWidgetListTypeById(int widgetListTypeId);
    
    List<Widget> findWidgetMasterList(UserAccount userAccount);
    
    List<UnifiedRate> findUnifiedRatesByCustomerId(int customerId, int pastMonths);
    
    List<WidgetSubsetMember> findSubsetMemberByWidgetIdTierTypeId(Integer id, int widgetTierType);
    
    List<Widget> findMapWidgetsByUserAccountId(int userAccountId);
    
    WidgetTierType findWidgetTierTypeByTierTypeId(int tierTypeId);
    
    UnifiedRate findUnifiedRateById(int rateId);
    
    RevenueType findRevenueTypeById(byte revenueTypeId);
    
    Route findRouteById(int routeId);
    
    TransactionType findTransactionTypeById(int transactionTypeId);
    
    Location findLocationById(int locationId);
    
    Customer findCustomerById(int customerId);
    
    CollectionType findCollectionTypeById(int collectionTypeId);
    
    MerchantAccount findMerchantAccountById(int merchantAccountId);
    
    AlertClassType findAlertClassTypeById(byte alertClassTypeId);
    
    CardProcessMethodType findCardProcessMethodTypeById(int cardProcessMethodTypeId);
    
    CitationType findCitationTypeById(int citationTypeId);
    
    List<CardProcessMethodType> findSettledCardProcessMethodTypes();
    
    <T> T findObjectByClassTypeAndId(Class<T> clazz, int id);
}
