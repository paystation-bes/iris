package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.ReplenishType;

public interface ReplenishTypeService {
    public List<ReplenishType> loadAll();
    public Map<Integer, ReplenishType> getReplenishTypesMap();
    public String getText(int replenishId);
    public ReplenishType getReplenishType(String name);
}
