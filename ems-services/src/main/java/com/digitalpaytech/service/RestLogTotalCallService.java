package com.digitalpaytech.service;

import com.digitalpaytech.domain.RestAccount;

public interface RestLogTotalCallService {
    
    public int saveRestLogTotalCall(RestAccount restAccount);
}
