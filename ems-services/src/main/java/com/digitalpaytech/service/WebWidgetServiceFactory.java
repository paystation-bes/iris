package com.digitalpaytech.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.digitalpaytech.exception.WebWidgetServiceException;
import com.digitalpaytech.util.WidgetConstants;

/**
 * WebWidgetServiceFactory checks if requested WebWidgetService exists in the map, then it would create an instance of WebWidgetService 
 * and places into the map and returns the interface to caller.
 * It uses singleton (Initialization-on-demand holder) pattern to ensure there's only one instance of WebWidgetServiceFactory.
 * 
 * @author Allen Liang
 */

public final class WebWidgetServiceFactory implements ApplicationContextAware {
    
    private static final Logger LOG = Logger.getLogger(WebWidgetServiceFactory.class);
    
    private static final String BEAN_WIDGET_CARD_PROC_SERVICE = "webWidgetCardProcessingService";
    
    private Map<Integer, String> serviceBeanMap;
    private ApplicationContext applicationContext;
    
    /**
     * Private constructor prevents instantiation from other classes.
     */
    private WebWidgetServiceFactory() {
        this.serviceBeanMap = new HashMap<Integer, String>();
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_REVENUE, "webWidgetRevenueService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_COLLECTIONS, "webWidgetCollectionsService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_SETTLED_CARD, "webWidgetSettledCardService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_PURCHASES, "webWidgetPurchasesService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY, "webWidgetPaidOccupancyService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_TURNOVER, "webWidgetTurnoverService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_UTILIZATION, "webWidgetUtilizationService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_DURATION, "webWidgetDurationService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS, "webWidgetActiveAlertsService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_ALERT_STATUS, "webWidgetAlertStatusService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_MAP, "webWidgetMapService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_PRICE, "webWidgetAveragePriceService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE, "webWidgetAverageRevenueService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_DURATION, "webWidgetAverageDurationService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV, BEAN_WIDGET_CARD_PROC_SERVICE);
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX, BEAN_WIDGET_CARD_PROC_SERVICE);
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_CITATIONS, "webWidgetCitationService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_CITATION_MAP, "webWidgetCitationMapService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY, "webWidgetPhysicalOccupancyService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY, "webWidgetCompareOccupancyService");
        this.serviceBeanMap.put(WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP, "webWidgetOccupancyMapService");
    }
    
    public static WebWidgetServiceFactory getInstance() {
        return WebWidgetServiceFactoryHolder.INSTANCE;
    }
    
    /**
     * @param serviceType
     *            service type name for a specific WebWidgetService, e.g. "1 -> WebWidgetRevenueService".
     * @return WebWidgetService interface of a specific implementation.
     * @throws WebWidgetServiceException
     *             It would be thrown if:
     *             - input serviceType is null or empty
     *             - incorrect serviceType that corresponding WebWidgetServce implementation cannot be found or created.
     */
    public WebWidgetService getWebWidgetService(final int serviceType) throws WebWidgetServiceException {
        if (serviceType > this.serviceBeanMap.size() || serviceType < 1) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("serviceType does not exist: ").append(serviceType);
            throw new WebWidgetServiceException(bdr.toString());
        }
        
        // Creates a new WidgetService
        WebWidgetService widgetService = null;
        
        try {
            final String beanName = getBeanName(serviceType);
            widgetService = (WebWidgetService) this.applicationContext.getBean(beanName);
        } catch (NoSuchBeanDefinitionException nsb) {
            LOG.error(nsb);
            throw new WebWidgetServiceException(nsb);
        } catch (BeansException be) {
            LOG.error(be);
            throw new WebWidgetServiceException(be);
        }
        
        return widgetService;
    }
    
    /**
     * Makes input serviceType to lower case and uses it to get proper name from the map.
     * e.g. "revenue" -> "Revenue"
     * 
     * @param serviceType
     *            e.g. "Revenue"
     * @return String proper service type name.
     */
    private String getBeanName(final int serviceType) {
        return this.serviceBeanMap.get(serviceType);
    }
    
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        
    }
    
    /**
     * Initialization-on-demand holder pattern, http://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
     * WebWidgetServiceFactoryHolder is loaded on the first execution of WebWidgetServiceFactory.getInstance()
     * or the first access to WebWidgetServiceFactoryHolder.INSTANCE, not before.
     */
    private static class WebWidgetServiceFactoryHolder {
        public static final WebWidgetServiceFactory INSTANCE = new WebWidgetServiceFactory();
    }
}
