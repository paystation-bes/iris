package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.WebServiceEndPoint;

public interface WebServiceEndPointService {
    List<WebServiceEndPoint> findWebServiceEndPointsByCustomer(int customerId);
    
    List<WebServiceEndPoint> findPrivateWebServiceEndPointsByCustomer(int customerId);
    
    List<WebServiceEndPoint> findWebServiceEndPointByCustomerAndType(int customerId, byte typeId);
    
    WebServiceEndPoint findWebServiceEndPointByToken(String token);
    
    WebServiceEndPoint findWebServiceEndById(int webServiceEndPointId);
    
    void saveOrUpdate(WebServiceEndPoint webServiceEndPoint);
    
    void delete(WebServiceEndPoint webServiceEndPoint);
}
