package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.AccessPoint;

public interface AccessPointService {
    public List<AccessPoint> loadAll();
    public AccessPoint findAccessPoint(String name);
    public void save(AccessPoint accessPoint);
}
