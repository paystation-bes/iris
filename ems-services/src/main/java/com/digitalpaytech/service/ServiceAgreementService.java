package com.digitalpaytech.service;

import java.io.Serializable;

import com.digitalpaytech.domain.ServiceAgreement;

public interface ServiceAgreementService {

    ServiceAgreement findLatestServiceAgreement();

    ServiceAgreement findLatestServiceAgreementByCustomerId(int customerId);

    Serializable saveServiceAgreement(ServiceAgreement serviceAgreement);
}
