package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SettingsFileContent;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.util.RandomKeyMapping;

public interface PaystationSettingService {
    PaystationSetting findPaystationSetting(int id);
    
    List<PaystationSetting> findPaystationSettingsByCustomerId(int customerId);
    
    PaystationSetting findByCustomerIdAndLotName(Customer customer, String name);
    
    // For UploadServlet & DownloadServlet.
    SettingsFile findSettingsFile(Integer id);
    
    SettingsFileContent findSettingsFileContentBySettingsFileId(Integer settingsFile);
    
    void processSettingsFileUpload(Customer customer, String fileFullPath, SettingsFile newSetting, List<String> serialNumbers)
        throws InvalidDataException;
    
    List<SettingsFile> findSettingsFileByCustomerId(Integer customerId);

    List<SettingsFile> findSettingsFileByCustomerIdOrderByUploadGmt(Integer customerId);

    SettingsFile findSettingsFileByCustomerIdAndName(Integer customerId, String name);
    
    SettingsFile findSettingsFileByCustomerIdAndUniqueIdentifier(Integer customerId, Integer uniqueIdentifier);
    
    boolean isPaystationSettingWaitingForUpdate(Integer paystationSettingId);
    
    void deleteSettingsFileContent(SettingsFile settingsFile);
    
    List<CustomerAdminPayStationSettingInfo> findCustomerAdminPayStationSettingInfoListByCustomerId(Integer customerId, 
                                                                                                    String timezone, 
                                                                                                    RandomKeyMapping keyMapping);
    CustomerAdminPayStationSettingInfo findCustomerAdminPayStationSettingInfoBySettingFileId(Integer settingFileId, 
                                                                                             String timezone, 
                                                                                             RandomKeyMapping keyMapping);
    CustomerAdminPayStationSettingInfo findNoSettingCustomerAdminPayStationSettingInfoByCustomerId(Integer customerId, 
                                                                                                   String timezone, 
                                                                                                   RandomKeyMapping keyMapping);
}
