package com.digitalpaytech.service;

import java.util.Collection;

import com.digitalpaytech.domain.RouteType;

public interface RouteTypeService {

    public Collection<RouteType> findAllRouteType();
}
