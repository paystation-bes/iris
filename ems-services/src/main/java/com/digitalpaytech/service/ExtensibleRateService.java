package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.ExtensibleRate;

public interface ExtensibleRateService {
    public ExtensibleRate findByNameLocationIdAndUnifiedRateId(String name, Integer locationId, Integer unifiedRateId);
    
    public Collection<ExtensibleRate> getCurrentExtensibleRate(Integer locationId, Date targetDateLocal);
    
    @SuppressWarnings("rawtypes")
    public List getEmsRatesForSMSMessage(Date localExpiryDate, Date localMaxExtendedDate, Integer locationId);
    
    @SuppressWarnings("rawtypes")
    public List getWeekendEmsRatesForSMSMessage(Date localExpiryDate, Date localMaxExtendedDate, Integer locationId);
}
