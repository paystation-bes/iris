package com.digitalpaytech.service.report.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoReport;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.report.ReportDataService;

@Service("reportDataService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReportDataServiceImpl implements ReportDataService {
    @Autowired
    private EntityDaoReport entityDao;
    
    @Override
    public ProcessorTransaction getApprovedTransaction(final int pointOfSaleId, final Date purchasedDate, final int ticketNumber) {
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> pts =
                this.entityDao.getCurrentSession().getNamedQuery("ProcessorTransaction.getApprovedTransactionByPSDateAndTicketNum")
                        .setParameter(HibernateConstants.POINTOFSALE_ID, pointOfSaleId).setParameter(HibernateConstants.PURCHASED_DATE, purchasedDate)
                        .setParameter(HibernateConstants.TICKET_NUMBER, ticketNumber).setMaxResults(1).setCacheable(false).list();
        
        return (pts.size() <= 0) ? null : pts.get(0);
    }
    
    @Override
    public Purchase findUniquePurchaseForSms(final Integer customerId, final Integer pointOfSaleId, final Date purchaseGmt,
        final int purchaseNumber) {
        return (Purchase) this.entityDao.getCurrentSession().getNamedQuery("Purchase.findUniquePurchaseForSms")
                .setParameter(HibernateConstants.POINTOFSALE_ID, pointOfSaleId).setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .setParameter(HibernateConstants.PURCHASE_GMT, purchaseGmt).setParameter(HibernateConstants.PURCHASE_NUMBER, purchaseNumber)
                .setCacheable(false).uniqueResult();
    }
}
