package com.digitalpaytech.service.report;

import java.util.Date;

import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;

public interface ReportDataService {
    ProcessorTransaction getApprovedTransaction(int pointOfSaleId, Date purchasedDate, int ticketNumber);
    
    Purchase findUniquePurchaseForSms(Integer customerId, Integer pointOfSaleId, Date purchaseGmt, int purchaseNumber);
}
