package com.digitalpaytech.service;

public interface ETLNotProcessedService {
    
    public int getPurchase();
    
    public int getPermit();
}
