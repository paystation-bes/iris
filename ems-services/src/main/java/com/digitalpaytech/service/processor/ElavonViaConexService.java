package com.digitalpaytech.service.processor;

import java.util.List;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.BatchSettlement;
import com.digitalpaytech.domain.BatchSettlementDetail;
import com.digitalpaytech.domain.BatchSettlementType;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.TransactionSettlementStatusType;
import com.digitalpaytech.domain.BatchStatusType;
import com.digitalpaytech.exception.BatchCloseException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.ActiveBatchSummaryService;
import com.digitalpaytech.service.ElavonSemaphoreService;
import com.digitalpaytech.service.ElavonTransactionDetailService;

public interface ElavonViaConexService {
    ActiveBatchSummaryService getActiveBatchSummaryService();
    ElavonSemaphoreService getSemaphoreService();
    ElavonTransactionDetailService getElavonTransactionDetailService();
    
    ActiveBatchSummary findActiveBatchSummaryForCloseBatch(MerchantAccount merchantAccount, short batchNumber) throws BatchCloseException;
    BatchSettlement saveBatchSettlement(BatchSettlement batchSettlement);
    BatchSettlement saveBatchSettlement(ActiveBatchSummary activeBatchSummary, Cluster cluster, short batchSize, 
            BatchSettlementType batchSettlementType);
    void updateBatchSettlement(BatchSettlement batchSettlement);
    void updateActiveBatchSummary(ActiveBatchSummary activeBatchSummary);
    List<ElavonTransactionDetail> findElavonTransactionDetails(MerchantAccount merchantAccount, 
                                                               short batchNumber, 
                                                               TransactionSettlementStatusType transactionSettlementStatusType);
    ElavonTransactionDetail findSaleDetail(Long processorTransactionId, Integer pointOfSaleId, Integer merchantAccountId);
    ElavonTransactionDetail findSaleDetailWithPreAuth(Long preAuthId, Integer pointOfSaleId, Integer merchantAccountId);
    ElavonTransactionDetail findReversalDetail(Long saleDetailId);
    List<BatchSettlementDetail> saveBatchSettlementDetails(BatchSettlement batchSettlement, List<ElavonTransactionDetail> elavonTransactionDetails);
    List<BatchSettlementDetail> saveBatchSettlementDetails(List<BatchSettlementDetail> batchSettlementDetails);
    ClusterMemberService getClusterMemberService();
    CryptoService getCryptoService();
    TransactionSettlementStatusType findTransactionSettlementStatusType(int transactionSettlementStatusTypeId);
    BatchStatusType findBatchStatusType(int batchStatusTypeId);
    void updateForSuccessfulSettlement(List<BatchSettlementDetail> batchSettlementDetails);
    void encryptCardData(List<BatchSettlementDetail> batchSettlementDetails);
    EntityDao getEntityDao();
    BatchSettlementType getBatchSettlementType(byte batchCloseType);
    boolean hasBatchClosed(final Integer activeBatchSummaryId);
}
