package com.digitalpaytech.service.processor;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.beanstream.responses.PaymentResponse;
import com.beanstream.domain.TransactionRecord;

import java.util.List;
import java.util.Date;

public interface TDMerchantCommunicationService {
    
    PaymentResponse makePayment(Gateway gateway, CardPaymentRequest req) throws BeanstreamApiException;
    
    PaymentResponse sendPreAuthRequest(Gateway gateway, CardPaymentRequest req) throws BeanstreamApiException;
    
    PaymentResponse sendPostAuthRequest(Gateway gateway, String authorizationNumber, double amountToCharge) throws BeanstreamApiException;
    
    PaymentResponse sendRefundRequest(Gateway gateway, String authorizationNumber, double amount) throws BeanstreamApiException;
    
    List<TransactionRecord> searchTransaction(Gateway gateway, 
                                              Integer pointOfSaleId, 
                                              Date purchasedDate, 
                                              int ticketNumber) throws BeanstreamApiException;
}
