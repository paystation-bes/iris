package com.digitalpaytech.service.processor.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.BatchSettlement;
import com.digitalpaytech.domain.BatchSettlementDetail;
import com.digitalpaytech.domain.BatchSettlementType;
import com.digitalpaytech.domain.BatchStatusType;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.TransactionSettlementStatusType;
import com.digitalpaytech.exception.BatchCloseException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.ActiveBatchSummaryService;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.ElavonSemaphoreService;
import com.digitalpaytech.service.ElavonTransactionDetailService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.DateUtil;

@Service("elavonViaConexService")
@Transactional(propagation = Propagation.REQUIRED)
public class ElavonViaConexServiceImpl<ElavonViaConexProcessor> implements ElavonViaConexService {
    private static final Logger LOG = Logger.getLogger(ElavonViaConexServiceImpl.class);
    
    private static final String MERCH_ACCT_ID = "merchantAccountId";
    private static final String TRANS_SETTLE_STATUS_TYPE_ID = "transactionSettlementStatusTypeId";        
    
    @Autowired
    private EntityDao entityDao;

    @Autowired
    private ClusterMemberService clusterMemberService;

    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private ActiveBatchSummaryService activeBatchSummaryService;
    
    @Autowired
    private ElavonSemaphoreService semaphoreService;
    
    @Autowired
    private ElavonTransactionDetailService elavonTransactionDetailService;

    @Autowired
    private MailerService mailerService;
    
    /**
     * Find existing ActiveBatchSummary disregarding the status (BatchStatusType).
     * @throws BatchCloseException The exception is thrown if:
     *              - couldn't find ActiveBatchSummary record from the database
     *              - return more than one record. 
     */
    @Override
    public final ActiveBatchSummary findActiveBatchSummaryForCloseBatch(final MerchantAccount merchantAccount, 
                                                                        final short batchNumber) throws BatchCloseException {
        
        final List<ActiveBatchSummary> summaryList = 
                this.entityDao.findByNamedQueryAndNamedParam("ActiveBatchSummary.findActiveBatchSummaryForCloseBatch", 
                                                             new String[] { "merchantAccountId", "batchNumber" }, 
                                                             new Object[] { merchantAccount.getId(), batchNumber },
                                                             false);

        if (summaryList == null || summaryList.isEmpty() || summaryList.size() > 1) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("findActiveBatchSummaryForCloseBatch should only return ONE ActiveBatchSummary record. Instead the query returned: ");
            if (summaryList == null) {
                bdr.append("null");
            } else if (summaryList.isEmpty()) {
                bdr.append("0");
            } else {
                bdr.append(summaryList.size());
            }
            bdr.append(". BatchNumber: ").append(batchNumber).append(", MerchantAccountId: ").append(merchantAccount.getId());
            throw new BatchCloseException(bdr.toString());
        }
        
        return summaryList.get(0);
    }
    
    
    @Override
    public final BatchSettlement saveBatchSettlement(final ActiveBatchSummary activeBatchSummary, final Cluster cluster, final short batchSize,
            final BatchSettlementType batchSettlementType) {
        final BatchSettlement batchSettlement = new BatchSettlement();
        batchSettlement.setActiveBatchSummary(activeBatchSummary);
        batchSettlement.setCluster(cluster);
        batchSettlement.setBatchSize(batchSize);
        batchSettlement.setBatchSettlementStartGmt(DateUtil.getCurrentGmtDate());
        batchSettlement.setBatchSettlementType(batchSettlementType);
        final BatchSettlement savedBatchSettlement = saveBatchSettlement(batchSettlement);
        return savedBatchSettlement;
    }
    
    @Override
    public final BatchSettlement saveBatchSettlement(final BatchSettlement batchSettlement) {
        this.entityDao.save(batchSettlement);
        return batchSettlement;
    }
            
    @SuppressWarnings("unchecked")
    @Override
    public final List<ElavonTransactionDetail> findElavonTransactionDetails(final MerchantAccount merchantAccount, 
                                                                            final short batchNumber, 
                                                                            final TransactionSettlementStatusType transactionSettlementStatusType) {
        return this.entityDao.findByNamedQueryAndNamedParam(
                "ElavonTransactionDetail.findByMerchantAccountIdBatchNumberAndTransactionSettlementStatusTypeId",
                new String[] { MERCH_ACCT_ID, "batchNumber", TRANS_SETTLE_STATUS_TYPE_ID },
                new Object[] { merchantAccount.getId(), batchNumber, transactionSettlementStatusType.getId() },
                false);
    }
    
    
    @Override
    public final List<BatchSettlementDetail> saveBatchSettlementDetails(final BatchSettlement batchSettlement, 
                                                                        final List<ElavonTransactionDetail> elavonTransactionDetails) {
        final List<BatchSettlementDetail> list = new ArrayList<BatchSettlementDetail>(elavonTransactionDetails.size());
        for (ElavonTransactionDetail etd : elavonTransactionDetails) {
            final BatchSettlementDetail detailObj = new BatchSettlementDetail();
            detailObj.setBatchSettlement(batchSettlement);
            detailObj.setElavonTransactionDetail(etd);
            list.add(detailObj);
        }
        return saveBatchSettlementDetails(list);
    }
    
    @Override
    public final ElavonTransactionDetail findSaleDetail(final Long processorTransactionId, 
                                                        final Integer pointOfSaleId, 
                                                        final Integer merchantAccountId) {
        
        return (ElavonTransactionDetail) this.entityDao.findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findSaleDetail",
                                                                 new String[] { "pointOfSaleId", MERCH_ACCT_ID, "processorTransactionId" },
                                                                 new Object[] { pointOfSaleId, merchantAccountId, processorTransactionId },
                                                                 false);
    }
    
    
    @Override
    public final ElavonTransactionDetail findSaleDetailWithPreAuth(final Long preAuthId,
                                                                   final Integer pointOfSaleId, 
                                                                   final Integer merchantAccountId) {
        
        return (ElavonTransactionDetail) this.entityDao.findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findSaleDetailWithPreAuth",
                                                                                            new String[] { "pointOfSaleId", MERCH_ACCT_ID, "preAuthId" },
                                                                                            new Object[] { pointOfSaleId, merchantAccountId, preAuthId },
                                                                                            false);        
    }
    
    /**
     * Find ElavonTransactionDetail record using the id in a Credit Card.Sale ElavonTransactionDetail record. The id should be placed
     * in ElavonTransactionDetailId column of a reversal record. 
     * @return returns null if no reversal record exist. If more than one record are returned, the method will output an error message
     * and send error email saying there should be only ONE reversal record. Then returns the first record in the result list.
     */
    @Override
    public final ElavonTransactionDetail findReversalDetail(final Long saleDetailId) {
        final List<ElavonTransactionDetail> details = this.entityDao.findByNamedQueryAndNamedParam("ElavonTransactionDetail.findReversalDetailById",
                                                                                                   new String[] { "saleId" },
                                                                                                   new Object[] { saleDetailId },
                                                                                                   false);
        if (details == null || details.isEmpty()) {
            return null;
        }
        
        if (details != null && details.size() > 1) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("ElavonTransactionDetail id: ").append(saleDetailId).append(" is in multiple REVERSAL ElavonTransactionDetail records.");
            bdr.append("There should be only ONE reversal record !");
            final String msg = bdr.toString();
            LOG.error(msg);
            this.mailerService.sendAdminErrorAlert("Multiple ElavonTransactionDetail records with same SaleId", msg, null);
        }
        return details.get(0);
    }
    
    
    @Override
    public final List<BatchSettlementDetail> saveBatchSettlementDetails(final List<BatchSettlementDetail> batchSettlementDetails) {
        this.entityDao.saveOrUpdateAll(batchSettlementDetails);
        return batchSettlementDetails;
    }

    @Override
    public final TransactionSettlementStatusType findTransactionSettlementStatusType(final int transactionSettlementStatusTypeId) {
        return this.entityDao.get(TransactionSettlementStatusType.class, transactionSettlementStatusTypeId);
    }

    @Override
    public final BatchStatusType findBatchStatusType(final int batchStatusTypeId) {
        return this.entityDao.get(BatchStatusType.class, batchStatusTypeId);
    }
    
    @Override
    public final void updateBatchSettlement(final BatchSettlement batchSettlement) {
        this.entityDao.update(batchSettlement);
    }
    
    @Override
    public final void updateActiveBatchSummary(final ActiveBatchSummary activeBatchSummary) {
        this.entityDao.update(activeBatchSummary);
    }
    
    @Override
    public final void updateForSuccessfulSettlement(final List<BatchSettlementDetail> batchSettlementDetails) {
                
        final List<Long> ids = new ArrayList<Long>(batchSettlementDetails.size());
        for (BatchSettlementDetail detail : batchSettlementDetails) {
            ids.add(detail.getElavonTransactionDetail().getId());   
        }
        
        @SuppressWarnings("unchecked")
        final List<ElavonTransactionDetail> etds = this.entityDao.findByNamedQueryAndNamedParam("ElavonTransactionDetail.findByIds", 
                                                     new String[] { "ids" }, 
                                                     new Object[] { ids },
                                                     false);
                
        final Date settlementGmt = batchSettlementDetails.get(0).getElavonTransactionDetail().getSettlementGmt();
        
        final TransactionSettlementStatusType tsst = this.entityDao.get(TransactionSettlementStatusType.class, 
                                                                  batchSettlementDetails.get(0)
                                                                  .getElavonTransactionDetail()
                                                                  .getTransactionSettlementStatusType().getId());
        
        for (ElavonTransactionDetail etd : etds) {
            etd.setSettlementGmt(settlementGmt);
            etd.setTransactionSettlementStatusType(tsst);
            this.entityDao.update(etd);
        }        
    }
    
    @Override
    public final void encryptCardData(final List<BatchSettlementDetail> batchSettlementDetails) {
        for (BatchSettlementDetail detail : batchSettlementDetails) {
            String cardData = null;
            if (CryptoUtil.isDecryptedCardData(detail.getElavonTransactionDetail().getCardData())) {
                try {
                    cardData = this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, detail.getElavonTransactionDetail().getCardData());
                    detail.getElavonTransactionDetail().setCardData(cardData);
                } catch (CryptoException ce) {
                    LOG.error("Unable to perform encryption, remove decrypted data to be save, ", ce);
                    detail.getElavonTransactionDetail().setCardData(null);
                }
            }
        }
    }

    public final ActiveBatchSummaryService getActiveBatchSummaryService() {
        return this.activeBatchSummaryService;
    }
    public final ElavonSemaphoreService getSemaphoreService() {
        return this.semaphoreService;
    }
    public final ElavonTransactionDetailService getElavonTransactionDetailService() {
        return this.elavonTransactionDetailService;
    }
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
        
    @Override
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }    

    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    public final ClusterMemberService getClusterMemberService() {
        return this.clusterMemberService;
    }
    public final CryptoService getCryptoService() {
        return this.cryptoService;
    }
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    public final void setActiveBatchSummaryService(final ActiveBatchSummaryService activeBatchSummaryService) {
        this.activeBatchSummaryService = activeBatchSummaryService;
    }
    public final void setSemaphoreService(final ElavonSemaphoreService semaphoreService) {
        this.semaphoreService = semaphoreService;
    }
    public final void setElavonTransactionDetailService(final ElavonTransactionDetailService elavonTransactionDetailService) {
        this.elavonTransactionDetailService = elavonTransactionDetailService;
    }
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    @Override
    public final BatchSettlementType getBatchSettlementType(final byte batchCloseType) {
        final BatchSettlementType batchSettlementType = new BatchSettlementType();
        batchSettlementType.setId(batchCloseType);
        return batchSettlementType;
    }
    
    /**
     * Search an ActiveBatchSummary record with BatchStatusTypeId = 2 (Close), and ActiveBatchSummary Id. If record
     * exists that it's been closed and return true.
     */
    @Override
    public final boolean hasBatchClosed(final Integer activeBatchSummaryId) {
        
        final List<Object> abs = this.entityDao.findByNamedQueryAndNamedParam("ActiveBatchSummary.hasActiveBatchSummaryClosed", 
                                                     new String[] { "id" }, 
                                                     new Object[] { activeBatchSummaryId },
                                                     false);
        if (abs != null && !abs.isEmpty()) {
            return true;
        }
        return false;
    }
}
