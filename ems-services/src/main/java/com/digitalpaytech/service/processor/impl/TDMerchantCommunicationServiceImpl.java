package com.digitalpaytech.service.processor.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.beanstream.Gateway;
import com.beanstream.domain.TransactionRecord;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.beanstream.requests.Criteria;
import com.beanstream.requests.Operators;
import com.beanstream.requests.QueryFields;
import com.beanstream.responses.PaymentResponse;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Service("tdMerchantCommunicationService")
public class TDMerchantCommunicationServiceImpl implements TDMerchantCommunicationService {
    
    private static final Logger LOG = Logger.getLogger(TDMerchantCommunicationServiceImpl.class);
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    
    /**
     * Make a credit card payment. This payment must include credit card data. An Approved request will return 
     * a PaymentResponse. If the request fails in any way, even a card Decline, then an exception will be thrown.
     */
    @Override
    public final PaymentResponse makePayment(final Gateway gateway, final CardPaymentRequest req) throws BeanstreamApiException {
        final PaymentResponse response = gateway.payments().makePayment(req);
        return response;
    }
    
    /**
     * Pre-authorize a payment. Use this if you want to know if a customer has sufficient funds before processing a 
     * payment. A real-world example of this is pre-authorizing at the gas pump for $100 before you fill up, then end 
     * up only using $60 of gas; the customer is only charged $60. 
     * The final payment is used with preAuthCompletion() method.
     * @return PaymentResponse a PaymentResponse pre-approved containing the paymentId you will need to complete the 
     * transaction.
     * @throws BeanstreamApiException - if any validation fail or error occur 
     */
    @Override
    public final PaymentResponse sendPreAuthRequest(final Gateway gateway, final CardPaymentRequest req) throws BeanstreamApiException {
        final PaymentResponse response = gateway.payments().preAuth(req);
        return response;
    }
    
    /**
     * Push the actual payment through after a pre-authorization. Convenience method if you don't want to supply the whole PaymentRequest.
     * @param authorizationNumber Of the pre-authorized transaction
     */
    @Override
    public final PaymentResponse sendPostAuthRequest(final Gateway gateway, final String authorizationNumber, final double amountToCharge) 
            throws BeanstreamApiException {
        final PaymentResponse response = gateway.payments().preAuthCompletion(authorizationNumber, amountToCharge);
        return response;
    }

    /**
     * Return a previous payment made through Beanstream.
     * @param authorizationNumber payment transaction id to return
     */
    @Override
    public final PaymentResponse sendRefundRequest(final Gateway gateway, final String authorizationNumber, final double amount) 
            throws BeanstreamApiException {
        final PaymentResponse response = gateway.payments().returnPayment(authorizationNumber, amount);
        return response;
    }
    
    /**
     * Issue a query to search the last 30 days (in EmsProperties table) for an order number and transaction amount.
     * @param gateway
     * @param pointOfSaleId PointOfSale ID in CardRetryTransaction record.
     * @param purchasedDate Purchased Date in CardRetryTransaction record.
     * @param ticketNumber Ticket number in CardRetryTransaction record.
     * @return List<TransactionRecord> If record exists, Beanstream returns one com.beanstream.domain.TransactionRecord.
     * @throws BeanstreamApiException If it's "Record Not Found", it will return an empty List, otherwise throws the exception. 
     */
    @Override
    public final List<TransactionRecord> searchTransaction(final Gateway gateway, 
                                                           final Integer pointOfSaleId, 
                                                           final Date purchasedDate, 
                                                           final int ticketNumber) throws BeanstreamApiException {

        final ProcessorTransaction pt = 
                this.processorTransactionService.findByPointOfSaleIdPurchasedDateTicketNumber(pointOfSaleId, purchasedDate, ticketNumber);
        if (pt != null && !pt.isIsApproved() && StringUtils.isNotBlank(pt.getReferenceNumber())) {
            final String orderNumber = pt.getReferenceNumber();
            final String amt = WebCoreUtil.convertBase100ValueToFloatString(pt.getAmount());
            
            final int numDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.TD_MERCHANT_MAX_TRANSACTION_SEARCH_DAYS, 
                                                                                EmsPropertiesService.DEFAULT_TD_MERCHANT_MAX_TRANSACTION_SEARCH_DAYS);
            
            final Date endDate = DateUtil.getCurrentGmtDate();
            final Calendar startCal = new GregorianCalendar();
            startCal.setTime(endDate);
            startCal.add(Calendar.DATE, StandardConstants.INT_MINUS_ONE * numDays);
            final Date startDate = startCal.getTime();            
            
            try {
                // query(final Date startDate, final Date endDate, final int startRow, final int endRow, Criteria[] searchCriteria)
                return gateway.reports().query(startDate, endDate, StandardConstants.CONSTANT_1, StandardConstants.CONSTANT_2, buildCriteria(orderNumber, amt));
            } catch (BeanstreamApiException bae) {
                LOG.error(buildErrorMessage(orderNumber, amt, startDate, endDate, bae), bae);
                if (!recordNotExist(bae)) {
                    throw bae;
                }
            }
        }
        return new ArrayList<TransactionRecord>();
    }
    
    /**
     * @param bae
     * @return boolean Return 'true' if BeanstreamApiException is:
     *  com.beanstream.exceptions.InternalServerException code: 9 category: 3 message: No records found
     */
    private boolean recordNotExist(final BeanstreamApiException bae) {
        if (bae != null && bae.getCategory() == CardProcessingConstants.BEANSTREAM_TRANSACTION_SEARCH_RECORD_NOT_FOUND_CATEGORY 
                && bae.getCode() == CardProcessingConstants.BEANSTREAM_TRANSACTION_SEARCH_RECORD_NOT_FOUND_CODE) {
            return true;
        }
        return false;
    }
    
    private Criteria[] buildCriteria(final String orderNumber, final String amount) {
        final Criteria[] arr = new Criteria[] {
            // Criteria(QueryFields field, Operators operator, String value) 
            new Criteria(QueryFields.OrderNumber, Operators.Equals, orderNumber),
            new Criteria(QueryFields.Amount, Operators.Equals, amount),
        };
        return arr;
    }
    
    private String buildErrorMessage(final String orderNumber, final String amount, final Date startDate, final Date endDate, final Exception e) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("BeanstreamApiException for 'query transaction', orderNumber/ReferenceNumber: ").append(orderNumber);
        bdr.append(", amount: ").append(amount).append(", startDate: ").append(startDate).append(", endDate: ");
        bdr.append(endDate).append(", caused by: ").append(e.getCause());
        return bdr.toString();
    }

    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }

    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
}
