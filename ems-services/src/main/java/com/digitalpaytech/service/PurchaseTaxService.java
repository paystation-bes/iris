package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.PurchaseTax;

public interface PurchaseTaxService {
    public List<PurchaseTax> findPurchaseTaxByPurchaseId(Long purchaseId);
}
