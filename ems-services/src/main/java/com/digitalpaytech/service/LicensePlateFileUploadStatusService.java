package com.digitalpaytech.service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.LicensePlateFileUploadStatus;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;

public interface LicensePlateFileUploadStatusService {
    
    void save(LicensePlateFileUploadStatus licensePlateFileUploadStatus);
    
    void update(LicensePlateFileUploadStatus licensePlateFileUploadStatus);
    
    void saveOrUpdate(LicensePlateFileUploadStatus licensePlateFileUploadStatus);
    
    LicensePlateFileUploadStatus findByCustomerIdAndStatus(Integer customerId, String status);
    
    LicensePlateFileUploadStatus findLatestByCustomerId(Integer customerId);

    void setPreferredParkingsStatus(GlobalConfiguration globalConfiguration, Customer customer);
}
