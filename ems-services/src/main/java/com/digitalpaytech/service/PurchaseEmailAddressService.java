package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.dto.customeradmin.EmailAddressHistoryDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;

public interface PurchaseEmailAddressService {
    
    public Collection<PurchaseEmailAddress> findPurchaseEmailAddress(int dataLoadingRows, boolean b);
    
    public void sendReceipt(PurchaseEmailAddress purchaseEmailAddress, boolean isMigrated);
    
    public void savePurchaseEmail(PurchaseEmailAddress purchaseEmailAddress);
    
    public void saveEmailAddress(EmailAddress emailAddress);
    
    public List<PurchaseEmailAddress> findPurchaseEmailAddressByPurchaseId(Long purchaseId);
    
    public EmailAddress findEmailById(Integer emailAddressId);
    
    public TransactionReceiptDetails getTransactionReceiptDetails(Purchase purchase);
    
    public List<EmailAddressHistoryDetail> findEmailAddressHistoryDetail(Long purchaseId);
}
