package com.digitalpaytech.service;

import javax.mail.Address;

import com.digitalpaytech.domain.CustomerAgreement;

public interface MailerService
{
    public final static String PROCESSOR_OFFLINE = "Processor Offline";
    public final static String EXCEPTION = "Exception";
    
//  public void sendMessage(Paystation paystationData, EventData eventData);

    /**
     * Plain Text message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param content
     *            the MIME message CONTENT
     */
    public void sendMessage(String toAddresses, String content);

    /**
     * Generic message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, Object content, String type);

    /**
     * Generic message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param ccAddresses
     *            comma separated RFC822 InternetAddresses for CC
     * @param bccAddresses
     *            comma separated RFC822 InternetAddresses for BCC
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, String ccAddresses, String bccAddresses,
            Object content, String type);

    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(Address fromAddress, String toAddresses, String subject, Object content,
            String type);

    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, String subject, Object content, String type);


    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param ccAddresses
     *            comma separated RFC822 InternetAddresses for CC
     * @param bccAddresses
     *            comma separated RFC822 InternetAddresses for BCC
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses,
            String bccAddresses, String subject, Object content, String type);

    
    /**
   * Generic message sender
   * 
   * @param fromAddress
   *            the MIME message FROM Address
   * @param toAddresses
   *            comma separated RFC822 InternetAddresses for TO
   * @param ccAddresses
   *            comma separated RFC822 InternetAddresses for CC
   * @param bccAddresses
   *            comma separated RFC822 InternetAddresses for BCC
   * @param subject
   *            the MIME message SUBJECT
   * @param content
   *            the MIME message CONTENT
   * @param type
   *            the MIME message TYPE
   * @param attachment
   *            attachment content
   * @param filename
   *            attachment filename
   */
  public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses,
                          String bccAddresses, String subject, Object content, String type, byte[] attachment, String filename);

  
  /**
  * Generic message sender
  * 
  * @param toAddresses
  *            comma separated RFC822 InternetAddresses for TO
  * @param subject
  *            the MIME message SUBJECT
  * @param content
  *            the MIME message CONTENT
  * @param type
  *            the MIME message TYPE
  * @param attachment
  *            attachment content
  * @param filename
  *            attachment filename
  */
 public void sendMessage(String toAddresses, String subject, Object content, String type, byte[] attachment, String filename);

    
    public void sendAdminErrorAlert(String subject, String content, Exception exception);

    public void sendAdminWarnAlert(String subject, String content, Exception exception);

    public void sendAdminAlert(String subject, String content, Exception exception);
    
    public void sendServiceAgreementAccepted(CustomerAgreement customerAgreement, String customerName);
}