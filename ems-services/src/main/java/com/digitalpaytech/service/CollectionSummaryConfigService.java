package com.digitalpaytech.service;

import com.digitalpaytech.domain.CollectionSummaryConfig;

public interface CollectionSummaryConfigService {
    
    CollectionSummaryConfig getCollectionSummaryConfigByUserId(int userAccountId);
}
