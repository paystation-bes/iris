package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Notification;

public interface NotificationService {

    public List<Notification> findNotificationByEffectiveDate();

       public List<Notification> findNotificationAllCurrentAndFuture();

    public List<Notification> findNotificationInPast();

    public Notification findNotificationById(Integer id, boolean doEvict);
    
    public void saveNotification(Notification notification);
    
    public void updateNotification(Notification notification);
}
