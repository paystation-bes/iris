package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;
import java.util.Date;

import org.hibernate.ScrollableResults;

import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;

public interface PreAuthService {
    
    void savePreauth(PreAuth preauth);
    
    PreAuth getApprovedPreAuthByPsRefId(int pointOfSaleId, String psRefId, String cardHash);
    
    PreAuth getApprovedPreAuthByPsRefIdAndPSAmount(int pointOfSaleId, String psRefId, String cardHash, int amount);
    
    PreAuth getApprovedPreAuthById(Long id);
    
    PreAuth getApprovedPreAuthByPSAmountAndAuthNumAndDate(int pointOfSaleId, int amount, String authNumber, long startDate, long endDate);
    
    PreAuth getApprovedPreAuthByPSAmountAndAuthNum(int pointOfSaleId, int amount, String authNumber);
    
    void cleanupExpiredPreauths(Date expiredDate);
    
    void deleteExpiredPreauths(Date expiredDate);
    
    List<PreAuth> findExpiredPreAuths(Date expiredDate);
    
    Collection<PreAuth> findPreAuthsByCardData(String cardData, boolean isEvict);
    
    Collection<PreAuth> findByMerchantAccountId(int merchantAccountId);
    
    void updatePreAuth(PreAuth preAuth);
    
    int findTotalPreAuthsByCardData(String cardData);
    
    void cleanupExpiredPreauthsForMigratedCustomers(Date expiredDate);
    
    void deleteExpiredPreauthsForMigratedCustomers(Date expiredDate);
    
    List<PreAuth> findExpiredPreAuthsForMigratedCustomers(Date expiredDate);
    
    PreAuthHolding findByApprovedPreAuthIdAuthorizationNumber(Long preAuthId, String authorizationNumber);
    
    List<Long> findExpiredElavonPreAuths(Date expiryDate, int maxPreAuthLimit);
    
    void deleteExpiredPreauthForMigratedCustomer(PreAuth preAuth);
    
    void cleanupExpiredPreauthForMigratedCustomer(PreAuth preAuth);
    
    ScrollableResults getExpiredCardAuthorizationBackUp(Date expiryDate);
    
    void deletePreauthsByIds(Collection<Long> preAuthIds);
    
    List<PreAuth> findByIds(Long... ids);
    
    PreAuth findById(Long id);
    
    PreAuth getApprovedByCardRetryData(Integer pointOfSaleId, String cardHash, int amount, int ticketNumber);
    
    Collection<PreAuth> findApprovedByMerchantAcccountId(Integer merchantAccountId);
}
