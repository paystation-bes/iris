package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportEmail;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.dto.AdhocReportInfo;

public interface ReportDefinitionService {
    
    public boolean findReadyReportsWithLock();
    
    public void updateReportDefinition(ReportDefinition reportDefinition);
    
    public ReportDefinition getReportDefinition(int id);
    
    public List<ReportDefinition> findScheduledReportsByUserAccountId(int userAccountId);

    public List<ReportDefinition> findScheduledReportsByCustomerId(int customerId);

    public void saveOrUpdateReportDefinition(ReportDefinition reportDefinition, ReportDefinition originalReportDefinition,
            Collection<ReportLocationValue> reportLocationValueList, Set<ReportEmail> emailList);
    
    public void saveOrUpdateReportDefinitionForOnline(ReportDefinition reportDefinition, ReportDefinition originalReportDefinition,
                                                      Collection<ReportLocationValue> reportLocationValueList, Set<ReportEmail> emailList, String timeZone);
    
    public List<String> findEmailsByReportDefinitionId(int reportDefinitionId);
    
    public int getReadyReportCount();
    
    public AdhocReportInfo runAdhocReport(ReportDefinition report, String timeZone);
    
    public ReportDefinition getAdhocReportDefinition(ReportDefinition reportDefinition);
    
    public ReportDefinition getReportDefinitionFromHistory(int reportHistoryId);

    public List<ReportDefinition> findScheduledReportsByCustomerIdAndUserAccountId(int userAccountId, int customerId);
    
}
