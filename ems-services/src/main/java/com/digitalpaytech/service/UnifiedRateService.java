package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;

public interface UnifiedRateService {
    public List<UnifiedRate> findRatesByCustomerId(Integer customerId);
    public void insertWithNewExtensibleRate(UserAccount userAccount, ExtensibleRate extensibleRate);
    public UnifiedRate findRateByNameAndCustomerId(String unifiedRateName, Integer customerId);
    public UnifiedRate insert(UserAccount userAccount, String name);
    public UnifiedRate findUnifiedRateById(Integer unifiedRateId);
}
