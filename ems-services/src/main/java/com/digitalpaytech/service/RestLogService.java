package com.digitalpaytech.service;

import java.util.Date;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestLog;

public interface RestLogService {

    public void saveRestLog(RestAccount restAccount, String method, boolean isError);

}
