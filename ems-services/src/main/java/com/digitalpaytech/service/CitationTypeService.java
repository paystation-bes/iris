package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.CitationType;

public interface CitationTypeService {
    
    List<CitationType> findCitationTypeByCustomerId(Integer customerId);
    
    List<CitationType> findActiveCitationTypeByCustomerId(Integer customerId);
    
    CitationType findCitationTypeByCustomerIdAndFlexCitationTypeId(Integer customerId, Integer flexCitationTypeId);
    
}
