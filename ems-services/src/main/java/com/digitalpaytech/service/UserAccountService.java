package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;

public interface UserAccountService {
    
    UserAccount findUserAccount(int userAccountId);
    
    UserAccount findUserAccountAndEvict(int userAccountId);
    
    List<Permission> findPermissions(int userAccountId);
    
    List<Role> findRoles(int userAccountId);
    
    UserAccount findUserAccount(String userName);
    
    void updatePassword(UserAccount userAccount);
    
    Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(Integer customerId);
    
    Collection<UserAccount> findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountType(final Integer customerId,
        final Integer controllerCustomerId);
    
    void updateUserAccount(UserAccount userAccount);
    
    void updateUserAccountAndUserRoles(UserAccount userAccount);
    
    void saveUserAccountAndUserRole(UserAccount userAccount);
    
    Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(Integer customerId, Integer roleId);
    
    void saveOrUpdateUserAccount(UserAccount userAccount);
    
    UserAccount findAdminUserAccountByChildCustomerId(int customerId);
    
    UserAccount findAdminUserAccountByParentCustomerId(int customerId);
    
    Collection<UserAccount> findUserAccountsByCustomerId(int customerId);
    
    List<RolePermission> findUserRoleAndPermission(Integer userAccountId);
    
    List<RolePermission> findUserRoleAndPermissionForCustomerType(Integer userAccountId, Integer customerTypeId);
    
    UserAccount findUndeletedUserAccount(String userName);
    
    UserAccount findUserAccountForLogin(int userAccountId);
    
    void deleteUserRoles(UserAccount userAccount);
    
    List<UserAccount> findUserAccountsByCustomerIdAndMobileApplicationTypeId(int customerId, int mobileApplicationTypeId);
    
    List<UserAccount> findChildUserAccountsByParentUserId(Integer parentUserAccountId);
    
    List<UserAccount> findAdminUserAccountsForParent(Integer customerId, Integer parentCustomerId);
    
    List<UserAccount> findAdminUserAccountWithIsAllChilds(Integer customerId);
    
    Collection<UserAccount> findAliasUserAccountsByUserAccountId(Integer userAccountId);
    
    UserAccount findAliasUserAccountByUserAccountIdAndCustomerId(Integer userAccountId, Integer customerId);
    
    UserAccount findUserRoleByUserAccountIdAndRoleId(Integer roleId, Integer userAccountId);
    
    List<UserRole> findActiveUserRoleByUserAccountId(Integer userAccountId);
    
    List<UserAccount> findUserAccountByRoleIdAndCustomerIdList(Integer roleId, List<Integer> customerIdList);

    List<UserAccount> findUserAccountByRoleIdAndCustomerId(Integer roleId, Integer customerId);
    
    Customer findChildCustomer(Integer parentCustomerId, String childCustomerName);
    
    UserAccount findUserAccountByLinkUserName(String linkUserName);

}
