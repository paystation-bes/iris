package com.digitalpaytech.service.kpi.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.CollectionLockService;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.ETLNotProcessedService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.service.SmsAlertService;
import com.digitalpaytech.service.StagingPurchaseService;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.util.KPIConstants;
import com.digitalpaytech.util.StandardConstants;

/**
 * This class is used to increment all values in the application that are being monitored
 * 
 * @author Amanda Chong
 */

public class KPIServiceImpl implements KPIService {
    private static Logger log = Logger.getLogger(KPIServiceImpl.class);
    
    private static final String[] PREFIX_DAILY = { "key." };
    private static final int MONTHS_6 = 6;
    private static final long DAY_MILLISECONDS = 86400000L;
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    
    @Autowired
    private CollectionTypeService collectionTypeService;
    
    @Autowired
    private ReportQueueService reportQueueService;
    
    @Autowired
    private ReportHistoryService reportHistoryService;
    
    @Autowired
    private ReportDefinitionService reportDefinitionService;
    
    @Autowired
    private RawSensorDataService rawSensorDataService;
    
    @Autowired
    private ExtensiblePermitService extensiblePermitService;
    
    @Autowired
    private SmsAlertService smsAlertService;
    
    @Autowired
    private ETLNotProcessedService etlNotProcessedService;
    
    @Autowired
    private StagingPurchaseService stagingPurchaseService;
    
    @Autowired
    private CollectionLockService collectionLockService;
    
    @Autowired
    private EmsPropertiesService emsProperties;
    
    @Autowired
    private CryptoService cryptoService;
    
    private Date lastMinute;
    private Date minute = getCurrentMinute();
    
    // Digital API
    private long digitalAPIResponseTimeAuditInfo;
    private long digitalAPIResponseTimePaystationInfo;
    private long digitalAPIResponseTimePlateInfo;
    private long digitalAPIResponseTimeStallInfo;
    private long digitalAPIResponseTimeTransactionInfo;
    private long digitalAPIResponseTimeCoupon;
    // Digital API Top 5 Tokens
    private int digitalAPIRequestAuditInfo;
    private int digitalAPIRequestPaystationInfo;
    private int digitalAPIRequestPlateInfo;
    private int digitalAPIRequestStallInfo;
    private int digitalAPIRequestTransactionInfo;
    // Digital API Requests
    private int digitalAPIRequestSuccessAuditInfo;
    private int digitalAPIRequestSuccessPaystationInfo;
    private int digitalAPIRequestSuccessPlateInfo;
    private int digitalAPIRequestSuccessStallInfo;
    private int digitalAPIRequestSuccessTransactionInfo;
    private int digitalAPIRequestSuccessCoupon;
    private int digitalAPIRequestFailAuditInfo;
    private int digitalAPIRequestFailPaystationInfo;
    private int digitalAPIRequestFailPlateInfo;
    private int digitalAPIRequestFailStallInfo;
    private int digitalAPIRequestFailTransactionInfo;
    private int digitalAPIRequestFailCoupon;
    // Coupon records per file
    private int digitalAPICouponRecord;
    private int digitalAPICouponFile;
    private int digitalAPICouponFileSize;
    
    // UI File Uploads
    private int uiFileUploadSuccessCoupon;
    private int uiFileUploadSuccessPasscard;
    private int uiFileUploadSuccessBannedCard;
    private int uiFileUploadFailCoupon;
    private int uiFileUploadFailPasscard;
    private int uiFileUploadFailBannedCard;
    // UI File Downloads
    private int uiFileDownloadSuccessCoupon;
    private int uiFileDownloadSuccessPasscard;
    private int uiFileDownloadSuccessReport;
    private int uiFileDownloadSuccessWidget;
    private int uiFileDownloadFailCoupon;
    private int uiFileDownloadFailPasscard;
    private int uiFileDownloadFailReport;
    private int uiFileDownloadFailWidget;
    private int uiFileUploaded;
    private long uiFileSize;
    
    // XMLRPC Requests
    private int xmlrpcRequestSuccessAudit;
    private int xmlrpcRequestSuccessTransaction;
    private int xmlrpcRequestSuccessAuthorizeRequest;
    private int xmlrpcRequestSuccessEvent;
    private int xmlrpcRequestSuccessHeartBeat;
    private int xmlrpcRequestSuccessLotSettingSuccess;
    private int xmlrpcRequestSuccessPlateRptReq;
    private int xmlrpcRequestSuccessAuthorizeCardRequest;
    private int xmlrpcRequestSuccessStallInfoRequest;
    private int xmlrpcRequestSuccessStallReportRequest;
    private int xmlrpcRequestFailAudit;
    private int xmlrpcRequestFailTransaction;
    private int xmlrpcRequestFailAuthorizeRequest;
    private int xmlrpcRequestFailEvent;
    private int xmlrpcRequestFailHeartBeat;
    private int xmlrpcRequestFailLotSettingSuccess;
    private int xmlrpcRequestFailPlateRptReq;
    private int xmlrpcRequestFailAuthorizeCardRequest;
    private int xmlrpcRequestFailStallInfoRequest;
    private int xmlrpcRequestFailStallReportRequest;
    // XMLRPC Transactions
    private int xmlrpcRequestTransactionSuccessStoreAndForward;
    private int xmlrpcRequestTransactionSuccessCashCreditSwipe;
    private int xmlrpcRequestTransactionSuccessCashCreditCL;
    private int xmlrpcRequestTransactionSuccessCashCreditCH;
    private int xmlrpcRequestTransactionSuccessCashValue;
    private int xmlrpcRequestTransactionSuccessValue;
    private int xmlrpcRequestTransactionSuccessCreditSwipe;
    private int xmlrpcRequestTransactionSuccessCreditCL;
    private int xmlrpcRequestTransactionSuccessCreditCH;
    private int xmlrpcRequestTransactionSuccessCashSmart;
    private int xmlrpcRequestTransactionSuccessSmart;
    private int xmlrpcRequestTransactionSuccessUnknown;
    private int xmlrpcRequestTransactionSuccessCash;
    private int xmlrpcRequestTransactionSuccessNA;
    private int xmlrpcRequestTransactionSuccessPayAndDisplay;
    private int xmlrpcRequestTransactionSuccessPayByPlate;
    private int xmlrpcRequestTransactionSuccessPayBySpace;
    //    private int xmlrpcRequestTransactionFailStoreAndForward = 0;
    private int xmlrpcRequestTransactionFailCashCreditSwipe;
    private int xmlrpcRequestTransactionFailCashCreditCL;
    private int xmlrpcRequestTransactionFailCashCreditCH;
    private int xmlrpcRequestTransactionFailCashValue;
    private int xmlrpcRequestTransactionFailValue;
    private int xmlrpcRequestTransactionFailCreditSwipe;
    private int xmlrpcRequestTransactionFailCreditCL;
    private int xmlrpcRequestTransactionFailCreditCH;
    private int xmlrpcRequestTransactionFailCashSmart;
    private int xmlrpcRequestTransactionFailSmart;
    private int xmlrpcRequestTransactionFailUnknown;
    private int xmlrpcRequestTransactionFailCash;
    private int xmlrpcRequestTransactionFailNA;
    private int xmlrpcRequestTransactionFailPayAndDisplay;
    private int xmlrpcRequestTransactionFailPayByPlate;
    private int xmlrpcRequestTransactionFailPayBySpace;
    
    // Pay Station REST
    private int paystationRestFailCancelTransaction;
    private int paystationRestFailHeartBeat;
    private int paystationRestFailToken;
    private int paystationRestFailTransaction;
    
    private int paystationRestSuccessCancelTransaction;
    private int paystationRestSuccessHeartBeat;
    private int paystationRestSuccessToken;
    private int paystationRestSuccessTransaction;
    
    private int paystationRestTransactionFailPaymentCash;
    private int paystationRestTransactionFailPaymentCashSmart;
    private int paystationRestTransactionFailPaymentCashValue;
    private int paystationRestTransactionFailPaymentCashCredit;
    private int paystationRestTransactionFailPaymentCredit;
    private int paystationRestTransactionFailPaymentSmart;
    private int paystationRestTransactionFailPaymentUnknown;
    private int paystationRestTransactionFailPaymentValue;
    private int paystationRestTransactionFailPermitNA;
    private int paystationRestTransactionFailPermitPayAndDisplay;
    private int paystationRestTransactionFailPermitPayByPlate;
    private int paystationRestTransactionFailPermitPayBySpace;
    private int paystationRestTransactionFailStoreAndForward;
    
    private int paystationRestTransactionSuccessPaymentCash;
    private int paystationRestTransactionSuccessPaymentCashSmart;
    private int paystationRestTransactionSuccessPaymentCashValue;
    private int paystationRestTransactionSuccessPaymentCashCredit;
    private int paystationRestTransactionSuccessPaymentCredit;
    private int paystationRestTransactionSuccessPaymentSmart;
    private int paystationRestTransactionSuccessPaymentUnknown;
    private int paystationRestTransactionSuccessPaymentValue;
    private int paystationRestTransactionSuccessPermitNA;
    private int paystationRestTransactionSuccessPermitPayAndDisplay;
    private int paystationRestTransactionSuccessPermitPayByPlate;
    private int paystationRestTransactionSuccessPermitPayBySpace;
    private int paystationRestTransactionSuccessStoreAndForward;
    
    private int paystationRestFailParameterError;
    private int paystationRestFailSignatureError;
    private int paystationRestFailSystemError;
    
    // UI Dashboard
    private long uiDashboardResponseTimeAlerts;
    private long uiDashboardResponseTimeRevenue;
    private long uiDashboardResponseTimeCollections;
    private long uiDashboardResponseTimeSettledCard;
    private long uiDashboardResponseTimePurchases;
    private long uiDashboardResponseTimePaidOccupancy;
    private long uiDashboardResponseTimeTurnover;
    private long uiDashboardResponseTimeUtilization;
    private long uiDashboardResponseTimeMap;
    // Used for response time calculation
    private int uiDashboardQuerySuccess;
    private int uiDashboardQueryFail;
    private long uiDashboardAlerts;
    private long uiDashboardRevenue;
    private long uiDashboardCollections;
    private long uiDashboardSettledCard;
    private long uiDashboardPurchases;
    private long uiDashboardPaidOccupancy;
    private long uiDashboardTurnover;
    private long uiDashboardUtilization;
    private long uiDashboardMap;
    // Response status codes
    private int uiRequestInformation;
    private int uiRequestSuccess;
    private int uiRequestRedirect;
    private int uiRequestClientError;
    private int uiRequestServerError;
    private int uiRequest449;
    private int uiRequest480;
    private int uiRequest481;
    private int uiRequest482;
    private int uiRequest483;
    private int uiRequest509;
    // Uncaught exception types
    private int uncaughtExceptionInformation;
    private int uncaughtExceptionSuccess;
    private int uncaughtExceptionRedirect;
    private int uncaughtExceptionClientError;
    private int uncaughtExceptionServerError;
    
    // Card Processing Request
    private int cardProcessingRequestConcord;
    private int cardProcessingRequestAlliance;
    private int cardProcessingRequestAuthorizeNet;
    private int cardProcessingRequestBlackBoard;
    private int cardProcessingRequestDatawire;
    private int cardProcessingRequestFirstDataNashville;
    private int cardProcessingRequestFirstHorizon;
    private int cardProcessingRequestHeartland;
    private int cardProcessingRequestHeartlandSPPlus;
    private int cardProcessingRequestLink2Gov;
    private int cardProcessingRequestMoneris;
    private int cardProcessingRequestNuVision;
    private int cardProcessingRequestParadata;
    private int cardProcessingRequestPaymentech;
    private int cardProcessingRequestTotalCard;
    private int cardProcessingRequestCBordGold;
    private int cardProcessingRequestCBordOdyssey;
    private int cardProcessingRequestElavonConverge;
    private int cardProcessingRequestElavonViaConex;
    private int cardProcessingRequestTDMerchantServices;
    private int cardProcessingRequestCreditcall;
    
    // Card Processing Response Times
    private long cardProcessingResponseTimeConcord;
    private long cardProcessingResponseTimeAlliance;
    private long cardProcessingResponseTimeAuthorizeNet;
    private long cardProcessingResponseTimeBlackBoard;
    private long cardProcessingResponseTimeDatawire;
    private long cardProcessingResponseTimeFirstDataNashville;
    private long cardProcessingResponseTimeFirstHorizon;
    private long cardProcessingResponseTimeHeartland;
    private long cardProcessingResponseTimeHeartlandSPPlus;
    private long cardProcessingResponseTimeLink2Gov;
    private long cardProcessingResponseTimeMoneris;
    private long cardProcessingResponseTimeNuVision;
    private long cardProcessingResponseTimeParadata;
    private long cardProcessingResponseTimePaymentech;
    private long cardProcessingResponseTimeTotalCard;
    private long cardProcessingResponseTimeCBordGold;
    private long cardProcessingResponseTimeCBordOdyssey;
    private long cardProcessingResponseTimeElavonConverge;
    private long cardProcessingResponseTimeElavonViaConex;
    private long cardProcessingResponseTimeTDMerchantServices;
    private long cardProcessingResponseTimeCreditcall;
    
    // S&F ExhaustedQyeyes
    private AtomicInteger cardProcessingQueueExhaustingStoreAndForward = new AtomicInteger(0);
    
    // Background Processes
    private int backgroundProcessesSMSAlertSent;
    private int backgroundProcessesSMSAlertReceive;
    private int backgroundProcessesAlertEmail;
    private long backgroundProcessesAlertEmailResponseTime;
    
    // BOSS Upload
    private int bossUploadSuccessLotSetting;
    private int bossUploadSuccessFileUpload;
    private int bossUploadSuccessPaystationQuery;
    private int bossUploadSuccessOfflineTransaction;
    private int bossUploadFailLotSetting;
    private int bossUploadFailFileUpload;
    private int bossUploadFailPaystationQuery;
    private int bossUploadFailOfflineTransaction;
    // BOSS Download
    private int bossDownloadSuccessLotSetting;
    private int bossDownloadSuccessPublicKeyRequest;
    private int bossDownloadSuccessCreditCard;
    private int bossDownloadSuccessBadSmartCard;
    private int bossDownloadFailLotSetting;
    private int bossDownloadFailPublicKeyRequest;
    private int bossDownloadFailCreditCard;
    private int bossDownloadFailBadSmartCard;
    private int bossUploadSuccessTotal;
    private int bossDownloadSuccessTotal;
    private int bossUploadFailTotal;
    private int bossDownloadFailTotal;
    
    // Daily KPI values...
    private boolean executedOnce;
    private boolean executedInLastMinute;
    
    private Map<String, Object> kpiInfo = new TreeMap<String, Object>();
    
    @Override
    public final String getValue(final String key) {
        boolean shouldRefreshDaily = (key != null) && (!this.executedInLastMinute);
        if (shouldRefreshDaily) {
            int i = 0;
            while ((i < PREFIX_DAILY.length) && (!key.startsWith(PREFIX_DAILY[i]))) {
                ++i;
            }
            
            shouldRefreshDaily = (i == 0) || (i < PREFIX_DAILY.length);
        }
        
        if (shouldRefreshDaily) {
            updateDaily();
        }
        
        return this.kpiInfo.get(key).toString();
    }
    
    private Date getCurrentMinute() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }
    
    @Override
    public final void updateDaily() {
        // final Date now = new Date();
        
        // Setup Flags...
        this.executedOnce = true;
        this.executedInLastMinute = true;
    }
    
    /**
     * Called by a cron job every minute to put all data into a map and reset all values to 0.
     */
    @SuppressWarnings({ "checkstyle:npathcomplexity", "checkstyle:methodlength", "checkstyle:cyclomaticcomplexity", "checkstyle:illegalcatch" })
    @Override
    public final void update() {
        final long startTime = System.currentTimeMillis();
        this.lastMinute = this.minute;
        this.minute = getCurrentMinute();
        
        final Map<String, Object> map = new TreeMap<String, Object>();
        map.put("!timeInterval", this.lastMinute + "-" + this.minute);
        
        // Response Time
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_AUDITINFO, this.digitalAPIRequestAuditInfo == 0 ? 0 : this.digitalAPIResponseTimeAuditInfo
                                                                                                            / this.digitalAPIRequestAuditInfo);
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_PAYSTATIONINFO, this.digitalAPIRequestPaystationInfo == 0 ? 0
                : this.digitalAPIResponseTimePaystationInfo / this.digitalAPIRequestPaystationInfo);
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_PLATEINFO, this.digitalAPIRequestPlateInfo == 0 ? 0 : this.digitalAPIResponseTimePlateInfo
                                                                                                            / this.digitalAPIRequestPlateInfo);
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_STALLINFO, this.digitalAPIRequestStallInfo == 0 ? 0 : this.digitalAPIResponseTimeStallInfo
                                                                                                            / this.digitalAPIRequestStallInfo);
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_TRANSACTIONINFO, this.digitalAPIRequestTransactionInfo == 0 ? 0
                : this.digitalAPIResponseTimeTransactionInfo / this.digitalAPIRequestTransactionInfo);
        final int digitalAPIRequestCoupon = this.digitalAPIRequestSuccessCoupon + this.digitalAPIRequestFailCoupon;
        map.put(KPIConstants.DIGITALAPI_RESPONSE_TIME_COUPON, digitalAPIRequestCoupon == 0 ? 0 : this.digitalAPIResponseTimeCoupon
                                                                                                 / digitalAPIRequestCoupon);
        map.put(KPIConstants.DIGITALAPI_REQUEST_COUPON_FILESIZE, digitalAPIRequestCoupon == 0 ? 0 : this.digitalAPICouponFileSize
                                                                                                    / digitalAPIRequestCoupon);
        
        // Top 5 Tokens            
        map.put(KPIConstants.DIGITALAPI_REQUEST_AUDITINFO, this.digitalAPIRequestAuditInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_PAYSTATIONINFO, this.digitalAPIRequestPaystationInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_PLATEINFO, this.digitalAPIRequestPlateInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_STALLINFO, this.digitalAPIRequestStallInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_TRANSACTIONINFO, this.digitalAPIRequestTransactionInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_COUPON, this.digitalAPIRequestSuccessCoupon + this.digitalAPIRequestFailCoupon);
        
        // Requests
        map.put(KPIConstants.DIGITALAPI_REQUEST_TOTAL, this.digitalAPIRequestAuditInfo + this.digitalAPIRequestPaystationInfo
                                                       + this.digitalAPIRequestPlateInfo + this.digitalAPIRequestStallInfo
                                                       + this.digitalAPIRequestTransactionInfo + this.digitalAPIRequestSuccessCoupon
                                                       + this.digitalAPIRequestFailCoupon);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_TOTAL, this.digitalAPIRequestSuccessAuditInfo + this.digitalAPIRequestSuccessPaystationInfo
                                                               + this.digitalAPIRequestSuccessPlateInfo + this.digitalAPIRequestSuccessStallInfo
                                                               + this.digitalAPIRequestSuccessTransactionInfo + this.digitalAPIRequestSuccessCoupon);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_AUDITINFO, this.digitalAPIRequestSuccessAuditInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_PAYSTATIONINFO, this.digitalAPIRequestSuccessPaystationInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_PLATEINFO, this.digitalAPIRequestSuccessPlateInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_STALLINFO, this.digitalAPIRequestSuccessStallInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_SUCCESS_TRANSACTIONINFO, this.digitalAPIRequestSuccessTransactionInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_COUPON, this.digitalAPIRequestSuccessCoupon);
        
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_TOTAL, this.digitalAPIRequestFailAuditInfo + this.digitalAPIRequestFailPaystationInfo
                                                            + this.digitalAPIRequestFailPlateInfo + this.digitalAPIRequestFailStallInfo
                                                            + this.digitalAPIRequestFailTransactionInfo + this.digitalAPIRequestFailCoupon);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_AUDITINFO, this.digitalAPIRequestFailAuditInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_PAYSTATIONINFO, this.digitalAPIRequestFailPaystationInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_PLATEINFO, this.digitalAPIRequestFailPlateInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_STALLINFO, this.digitalAPIRequestFailStallInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_TRANSACTIONINFO, this.digitalAPIRequestFailTransactionInfo);
        map.put(KPIConstants.DIGITALAPI_REQUEST_FAIL_COUPON, this.digitalAPIRequestFailCoupon);
        
        map.put(KPIConstants.DIGITALAPI_COUPON_RECORD, this.digitalAPICouponFile == 0 ? 0 : this.digitalAPICouponRecord / this.digitalAPICouponFile);
        
        // UI            
        map.put(KPIConstants.UI_FILE_UPLOAD_TOTAL, this.uiFileUploadSuccessCoupon + this.uiFileUploadSuccessPasscard
                                                   + this.uiFileUploadSuccessBannedCard + this.uiFileUploadFailCoupon + this.uiFileUploadFailPasscard
                                                   + this.uiFileUploadFailBannedCard);
        map.put(KPIConstants.UI_FILE_UPLOAD_SUCCESS_TOTAL, this.uiFileUploadSuccessCoupon + this.uiFileUploadSuccessPasscard
                                                           + this.uiFileUploadSuccessBannedCard);
        map.put(KPIConstants.UI_FILE_UPLOAD_SUCCESS_COUPON, this.uiFileUploadSuccessCoupon);
        map.put(KPIConstants.UI_FILE_UPLOAD_SUCCESS_PASSCARD, this.uiFileUploadSuccessPasscard);
        map.put(KPIConstants.UI_FILE_UPLOAD_SUCCESS_BANNEDCARD, this.uiFileUploadSuccessBannedCard);
        map.put(KPIConstants.UI_FILE_UPLOAD_FAIL_TOTAL, this.uiFileUploadFailCoupon + this.uiFileUploadFailPasscard + this.uiFileUploadFailBannedCard);
        map.put(KPIConstants.UI_FILE_UPLOAD_FAIL_COUPON, this.uiFileUploadFailCoupon);
        map.put(KPIConstants.UI_FILE_UPLOAD_FAIL_PASSCARD, this.uiFileUploadFailPasscard);
        map.put(KPIConstants.UI_FILE_UPLOAD_FAIL_BANNEDCARD, this.uiFileUploadFailBannedCard);
        
        map.put(KPIConstants.UI_FILE_DOWNLOAD_TOTAL, this.uiFileDownloadSuccessCoupon + this.uiFileDownloadSuccessPasscard
                                                     + this.uiFileDownloadSuccessReport + this.uiFileDownloadSuccessWidget
                                                     + this.uiFileDownloadFailCoupon + this.uiFileDownloadFailPasscard
                                                     + this.uiFileDownloadFailReport + this.uiFileDownloadFailWidget);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_SUCCESS_TOTAL, this.uiFileDownloadSuccessCoupon + this.uiFileDownloadSuccessPasscard
                                                             + this.uiFileDownloadSuccessReport + this.uiFileDownloadSuccessWidget);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_SUCCESS_COUPON, this.uiFileDownloadSuccessCoupon);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_SUCCESS_PASSCARD, this.uiFileDownloadSuccessPasscard);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_SUCCESS_REPORT, this.uiFileDownloadSuccessReport);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_SUCCESS_WIDGET, this.uiFileDownloadSuccessWidget);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_FAIL_TOTAL, this.uiFileDownloadFailCoupon + this.uiFileDownloadFailPasscard
                                                          + this.uiFileDownloadFailReport + this.uiFileDownloadFailWidget);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_FAIL_COUPON, this.uiFileDownloadFailCoupon);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_FAIL_PASSCARD, this.uiFileDownloadFailPasscard);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_FAIL_REPORT, this.uiFileDownloadFailReport);
        map.put(KPIConstants.UI_FILE_DOWNLOAD_FAIL_WIDGET, this.uiFileDownloadFailWidget);
        
        map.put(KPIConstants.UI_FILE_UPLOAD_SIZE, this.uiFileUploaded == 0 ? 0 : this.uiFileSize / this.uiFileUploaded);
        
        // XMLRPC
        map.put(KPIConstants.XMLRPC_REQUEST_THROUGHPUT, (this.xmlrpcRequestSuccessAudit + this.xmlrpcRequestSuccessTransaction
                                                         + this.xmlrpcRequestSuccessAuthorizeRequest + this.xmlrpcRequestSuccessEvent
                                                         + this.xmlrpcRequestSuccessHeartBeat + this.xmlrpcRequestSuccessLotSettingSuccess
                                                         + this.xmlrpcRequestSuccessPlateRptReq + this.xmlrpcRequestSuccessAuthorizeCardRequest
                                                         + this.xmlrpcRequestSuccessStallInfoRequest + this.xmlrpcRequestSuccessStallReportRequest
                                                         + this.xmlrpcRequestFailAudit + this.xmlrpcRequestFailTransaction
                                                         + this.xmlrpcRequestFailAuthorizeRequest + this.xmlrpcRequestFailEvent
                                                         + this.xmlrpcRequestFailHeartBeat + this.xmlrpcRequestFailLotSettingSuccess
                                                         + this.xmlrpcRequestFailPlateRptReq + this.xmlrpcRequestFailAuthorizeCardRequest
                                                         + this.xmlrpcRequestFailStallInfoRequest + this.xmlrpcRequestFailStallReportRequest)
                                                        / StandardConstants.MINUTES_IN_SECS_1_DBL);
        
        map.put(KPIConstants.XMLRPC_REQUEST_TOTAL, this.xmlrpcRequestSuccessAudit + this.xmlrpcRequestSuccessTransaction
                                                   + this.xmlrpcRequestSuccessAuthorizeRequest + this.xmlrpcRequestSuccessEvent
                                                   + this.xmlrpcRequestSuccessHeartBeat + this.xmlrpcRequestSuccessLotSettingSuccess
                                                   + this.xmlrpcRequestSuccessPlateRptReq + this.xmlrpcRequestSuccessAuthorizeCardRequest
                                                   + this.xmlrpcRequestSuccessStallInfoRequest + this.xmlrpcRequestSuccessStallReportRequest
                                                   + this.xmlrpcRequestFailAudit + this.xmlrpcRequestFailTransaction
                                                   + this.xmlrpcRequestFailAuthorizeRequest + this.xmlrpcRequestFailEvent
                                                   + this.xmlrpcRequestFailHeartBeat + this.xmlrpcRequestFailLotSettingSuccess
                                                   + this.xmlrpcRequestFailPlateRptReq + this.xmlrpcRequestFailAuthorizeCardRequest
                                                   + this.xmlrpcRequestFailStallInfoRequest + this.xmlrpcRequestFailStallReportRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_TOTAL, this.xmlrpcRequestSuccessAudit + this.xmlrpcRequestSuccessTransaction
                                                           + this.xmlrpcRequestSuccessAuthorizeRequest + this.xmlrpcRequestSuccessEvent
                                                           + this.xmlrpcRequestSuccessHeartBeat + this.xmlrpcRequestSuccessLotSettingSuccess
                                                           + this.xmlrpcRequestSuccessPlateRptReq + this.xmlrpcRequestSuccessAuthorizeCardRequest
                                                           + this.xmlrpcRequestSuccessStallInfoRequest + this.xmlrpcRequestSuccessStallReportRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_AUDIT, this.xmlrpcRequestSuccessAudit);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_TRANSACTION, this.xmlrpcRequestSuccessTransaction);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_AUTHORIZEREQUEST, this.xmlrpcRequestSuccessAuthorizeRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_EVENT, this.xmlrpcRequestSuccessEvent);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_HEARTBEAT, this.xmlrpcRequestSuccessHeartBeat);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_LOTSETTING_SUCCESS, this.xmlrpcRequestSuccessLotSettingSuccess);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_PLATE_RPT_REQ, this.xmlrpcRequestSuccessPlateRptReq);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_AUTHORIZE_CARD_REQUEST, this.xmlrpcRequestSuccessAuthorizeCardRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_STALLINFO_REQUEST, this.xmlrpcRequestSuccessStallInfoRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_SUCCESS_STALLREPORT_REQUEST, this.xmlrpcRequestSuccessStallReportRequest);
        
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_TOTAL, this.xmlrpcRequestFailAudit + this.xmlrpcRequestFailTransaction
                                                        + this.xmlrpcRequestFailAuthorizeRequest + this.xmlrpcRequestFailEvent
                                                        + this.xmlrpcRequestFailHeartBeat + this.xmlrpcRequestFailLotSettingSuccess
                                                        + this.xmlrpcRequestFailPlateRptReq + this.xmlrpcRequestFailAuthorizeCardRequest
                                                        + this.xmlrpcRequestFailStallInfoRequest + this.xmlrpcRequestFailStallReportRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_AUDIT, this.xmlrpcRequestFailAudit);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_TRANSACTION, this.xmlrpcRequestFailTransaction);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_AUTHORIZE_REQUEST, this.xmlrpcRequestFailAuthorizeRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_EVENT, this.xmlrpcRequestFailEvent);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_HEARTBEAT, this.xmlrpcRequestFailHeartBeat);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_LOTSETTING_SUCCESS, this.xmlrpcRequestFailLotSettingSuccess);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_PLATE_RPT_REQ, this.xmlrpcRequestFailPlateRptReq);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_AUTHORIZE_CARD_REQUEST, this.xmlrpcRequestFailAuthorizeCardRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_STALLINFO_REQUEST, this.xmlrpcRequestFailStallInfoRequest);
        map.put(KPIConstants.XMLRPC_REQUEST_FAIL_STALLREPORT_REQUEST, this.xmlrpcRequestFailStallReportRequest);
        
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_STOREANDFORWARD, this.xmlrpcRequestTransactionSuccessStoreAndForward);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_SWIPE, this.xmlrpcRequestTransactionSuccessCashCreditSwipe);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_CL, this.xmlrpcRequestTransactionSuccessCashCreditCL);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_CH, this.xmlrpcRequestTransactionSuccessCashCreditCH);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHVALUE, this.xmlrpcRequestTransactionSuccessCashValue);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_VALUE, this.xmlrpcRequestTransactionSuccessValue);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_SWIPE, this.xmlrpcRequestTransactionSuccessCreditSwipe);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_CL, this.xmlrpcRequestTransactionSuccessCreditCL);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_CH, this.xmlrpcRequestTransactionSuccessCreditCH);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHSMART, this.xmlrpcRequestTransactionSuccessCashSmart);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_SMART, this.xmlrpcRequestTransactionSuccessSmart);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_UNKNOWN, this.xmlrpcRequestTransactionSuccessUnknown);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASH, this.xmlrpcRequestTransactionSuccessCash);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_NA, this.xmlrpcRequestTransactionSuccessNA);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYANDDISPLAY, this.xmlrpcRequestTransactionSuccessPayAndDisplay);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYBYPLATE, this.xmlrpcRequestTransactionSuccessPayByPlate);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYBYSPACE, this.xmlrpcRequestTransactionSuccessPayBySpace);
        
        //        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTIONFAILSTOREANDFORWARD, this.xmlrpcRequestTransactionFailStoreAndForward);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_SWIPE, this.xmlrpcRequestTransactionFailCashCreditSwipe);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_CL, this.xmlrpcRequestTransactionFailCashCreditCL);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_CH, this.xmlrpcRequestTransactionFailCashCreditCH);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASHVALUE, this.xmlrpcRequestTransactionFailCashValue);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_VALUE, this.xmlrpcRequestTransactionFailValue);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_SWIPE, this.xmlrpcRequestTransactionFailCreditSwipe);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_CL, this.xmlrpcRequestTransactionFailCreditCL);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_CH, this.xmlrpcRequestTransactionFailCreditCH);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASHSMART, this.xmlrpcRequestTransactionFailCashSmart);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_SMART, this.xmlrpcRequestTransactionFailSmart);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_UNKNOWN, this.xmlrpcRequestTransactionFailUnknown);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_CASH, this.xmlrpcRequestTransactionFailCash);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_NA, this.xmlrpcRequestTransactionFailNA);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_PAYANDDISPLAY, this.xmlrpcRequestTransactionFailPayAndDisplay);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_PAYBYPLATE, this.xmlrpcRequestTransactionFailPayByPlate);
        map.put(KPIConstants.XMLRPC_REQUEST_TRANSACTION_FAIL_PAYBYSPACE, this.xmlrpcRequestTransactionFailPayBySpace);
        
        // Pay Station REST
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_THROUGHPUT, (this.paystationRestSuccessCancelTransaction + this.paystationRestSuccessHeartBeat
                                                                  + this.paystationRestSuccessToken + this.paystationRestSuccessTransaction
                                                                  + this.paystationRestFailCancelTransaction + this.paystationRestFailHeartBeat
                                                                  + this.paystationRestFailToken + this.paystationRestFailTransaction)
                                                                  / StandardConstants.MINUTES_IN_SECS_1_DBL);
        
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TOTAL, this.paystationRestSuccessCancelTransaction + this.paystationRestSuccessHeartBeat
                                                            + this.paystationRestSuccessToken + this.paystationRestSuccessTransaction
                                                            + this.paystationRestFailCancelTransaction + this.paystationRestFailHeartBeat
                                                            + this.paystationRestFailToken + this.paystationRestFailTransaction);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_SUCCESS_TOTAL, this.paystationRestSuccessCancelTransaction + this.paystationRestSuccessHeartBeat
                                                                    + this.paystationRestSuccessToken + this.paystationRestSuccessTransaction);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_SUCCESS_CANCEL_TRANSACTION, this.paystationRestSuccessCancelTransaction);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_SUCCESS_HEARTBEAT, this.paystationRestSuccessHeartBeat);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_SUCCESS_TOKEN, this.paystationRestSuccessToken);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_SUCCESS_TRANSACTION, this.paystationRestSuccessTransaction);
        
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_TOTAL, this.paystationRestFailCancelTransaction + this.paystationRestFailHeartBeat
                                                                 + this.paystationRestFailToken + this.paystationRestFailTransaction);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_CANCEL_TRANSACTION, this.paystationRestFailCancelTransaction);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_HEARTBEAT, this.paystationRestFailHeartBeat);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_TOKEN, this.paystationRestFailToken);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_TRANSACTION, this.paystationRestFailTransaction);
        
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_STOREANDFORWARD, this.paystationRestTransactionSuccessStoreAndForward);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHCREDIT, this.paystationRestTransactionSuccessPaymentCashCredit);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHVALUE, this.paystationRestTransactionSuccessPaymentCashValue);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_VALUE, this.paystationRestTransactionSuccessPaymentValue);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CREDIT, this.paystationRestTransactionSuccessPaymentCredit);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHSMART, this.paystationRestTransactionSuccessPaymentCashSmart);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_SMART, this.paystationRestTransactionSuccessPaymentSmart);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_UNKNOWN, this.paystationRestTransactionSuccessPaymentUnknown);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASH, this.paystationRestTransactionSuccessPaymentCash);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_NA, this.paystationRestTransactionSuccessPermitNA);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYANDDISPLAY, this.paystationRestTransactionSuccessPermitPayAndDisplay);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYBYPLATE, this.paystationRestTransactionSuccessPermitPayByPlate);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYBYSPACE, this.paystationRestTransactionSuccessPermitPayBySpace);
        
        //        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANSACTIONFAILSTOREANDFORWARD, this.paystationRestTransactionFailStoreAndForward);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHCREDIT, this.paystationRestTransactionFailPaymentCashCredit);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHVALUE, this.paystationRestTransactionFailPaymentCashValue);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_VALUE, this.paystationRestTransactionFailPaymentValue);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CREDIT, this.paystationRestTransactionFailPaymentCredit);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHSMART, this.paystationRestTransactionFailPaymentCashSmart);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_SMART, this.paystationRestTransactionFailPaymentSmart);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_UNKNOWN, this.paystationRestTransactionFailPaymentUnknown);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASH, this.paystationRestTransactionFailPaymentCash);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_NA, this.paystationRestTransactionFailPermitNA);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYANDDISPLAY, this.paystationRestTransactionFailPermitPayAndDisplay);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYBYPLATE, this.paystationRestTransactionFailPermitPayByPlate);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYBYSPACE, this.paystationRestTransactionFailPermitPayBySpace);
        
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_PARAMETER_ERROR, this.paystationRestFailParameterError);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_SIGNATURE_ERROR, this.paystationRestFailSignatureError);
        map.put(KPIConstants.PAYSTATION_REST_REQUEST_FAIL_SYSTEM_ERROR, this.paystationRestFailSystemError);
        
        // UI Dashboard
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_ALERTS, this.uiDashboardAlerts == 0 ? 0 : this.uiDashboardResponseTimeAlerts
                                                                                                  / this.uiDashboardAlerts);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_REVENUE, this.uiDashboardRevenue == 0 ? 0 : this.uiDashboardResponseTimeRevenue
                                                                                                    / this.uiDashboardRevenue);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_COLLECTIONS, this.uiDashboardCollections == 0 ? 0 : this.uiDashboardResponseTimeCollections
                                                                                                            / this.uiDashboardCollections);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_SETTLEDCARD, this.uiDashboardSettledCard == 0 ? 0 : this.uiDashboardResponseTimeSettledCard
                                                                                                            / this.uiDashboardSettledCard);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_PURCHASES, this.uiDashboardPurchases == 0 ? 0 : this.uiDashboardResponseTimePurchases
                                                                                                        / this.uiDashboardPurchases);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_PAIDOCCUPANCY, this.uiDashboardPaidOccupancy == 0 ? 0
                : this.uiDashboardResponseTimePaidOccupancy / this.uiDashboardPaidOccupancy);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_TURNOVER, this.uiDashboardTurnover == 0 ? 0 : this.uiDashboardResponseTimeTurnover
                                                                                                      / this.uiDashboardTurnover);
        
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_UTILIZATION, this.uiDashboardUtilization == 0 ? 0 : this.uiDashboardResponseTimeUtilization
                                                                                                            / this.uiDashboardUtilization);
        map.put(KPIConstants.UI_DASHBOARD_RESPONSE_TIME_MAP, this.uiDashboardMap == 0 ? 0 : this.uiDashboardResponseTimeMap / this.uiDashboardMap);
        
        map.put(KPIConstants.UI_DASHBOARD_QUERY_TOTAL, this.uiDashboardQuerySuccess + this.uiDashboardQueryFail);
        map.put(KPIConstants.UI_DASHBOARD_QUERY_SUCCESS, this.uiDashboardQuerySuccess);
        map.put(KPIConstants.UI_DASHBOARD_QUERY_FAIL, this.uiDashboardQueryFail);
        map.put(KPIConstants.UI_DASHBOARD_ALERTS, this.uiDashboardAlerts);
        map.put(KPIConstants.UI_DASHBOARD_REVENUE, this.uiDashboardRevenue);
        map.put(KPIConstants.UI_DASHBOARD_COLLECTIONS, this.uiDashboardCollections);
        map.put(KPIConstants.UI_DASHBOARD_SETTLEDCARD, this.uiDashboardSettledCard);
        map.put(KPIConstants.UI_DASHBOARD_PURCHASES, this.uiDashboardPurchases);
        map.put(KPIConstants.UI_DASHBOARD_PAIDOCCUPANCY, this.uiDashboardPaidOccupancy);
        map.put(KPIConstants.UI_DASHBOARD_TURNOVER, this.uiDashboardTurnover);
        map.put(KPIConstants.UI_DASHBOARD_UTILIZATION, this.uiDashboardUtilization);
        map.put(KPIConstants.UI_DASHBOARD_MAP, this.uiDashboardMap);
        
        // UI Response Status Codes
        map.put(KPIConstants.UI_REQUEST_INFORMATION, this.uiRequestInformation);
        map.put(KPIConstants.UI_REQUEST_SUCCESS, this.uiRequestSuccess);
        map.put(KPIConstants.UI_REQUEST_REDIRECT, this.uiRequestRedirect);
        map.put(KPIConstants.UI_REQUEST_CLIENT_ERROR, this.uiRequestClientError);
        map.put(KPIConstants.UI_REQUEST_SERVER_ERROR, this.uiRequestServerError);
        
        map.put(KPIConstants.UI_REQUEST_449, this.uiRequest449);
        map.put(KPIConstants.UI_REQUEST_480, this.uiRequest480);
        map.put(KPIConstants.UI_REQUEST_481, this.uiRequest481);
        map.put(KPIConstants.UI_REQUEST_482, this.uiRequest482);
        map.put(KPIConstants.UI_REQUEST_483, this.uiRequest483);
        map.put(KPIConstants.UI_REQUEST_509, this.uiRequest509);
        
        // Global uncaught exceptions
        map.put(KPIConstants.UNCAUGHT_EXCEPTION_INFORMATION, this.uncaughtExceptionInformation);
        map.put(KPIConstants.UNCAUGHT_EXCEPTION_SUCCESS, this.uncaughtExceptionSuccess);
        map.put(KPIConstants.UNCAUGHT_EXCEPTION_REDIRECT, this.uncaughtExceptionRedirect);
        map.put(KPIConstants.UNCAUGHT_EXCEPTION_CLIENT_ERROR, this.uncaughtExceptionClientError);
        map.put(KPIConstants.UNCAUGHT_EXCEPTION_SERVER_ERROR, this.uncaughtExceptionServerError);
        
        // Card Processing
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_CONCORD, this.cardProcessingRequestConcord);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_ALLIANCE, this.cardProcessingRequestAlliance);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_AUTHORIZENET, this.cardProcessingRequestAuthorizeNet);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_BLACKBOARD, this.cardProcessingRequestBlackBoard);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_DATAWIRE, this.cardProcessingRequestDatawire);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_FIRSTDATANASHVILLE, this.cardProcessingRequestFirstDataNashville);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_FIRSTHORIZON, this.cardProcessingRequestFirstHorizon);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_HEARTLAND, this.cardProcessingRequestHeartland);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_HEARTLAND_SPPLUS, this.cardProcessingRequestHeartlandSPPlus);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_LINK2GOV, this.cardProcessingRequestLink2Gov);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_MONERIS, this.cardProcessingRequestMoneris);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_NUVISION, this.cardProcessingRequestNuVision);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_PARADATA, this.cardProcessingRequestParadata);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_PAYMENTECH, this.cardProcessingRequestPaymentech);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_TOTALCARD, this.cardProcessingRequestTotalCard);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_CBORDGOLD, this.cardProcessingRequestCBordGold);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_CBORDODYSSEY, this.cardProcessingRequestCBordOdyssey);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_ELAVON_CONVERGE, this.cardProcessingRequestElavonConverge);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_ELAVON_VIACONEX, this.cardProcessingRequestElavonViaConex);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_TD_MERCHANT_SERVICES, this.cardProcessingRequestTDMerchantServices);
        map.put(KPIConstants.CARD_PROCESSING_REQUEST_CREDITCALL, this.cardProcessingRequestCreditcall);
        
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_CONCORD, this.cardProcessingRequestConcord == 0 ? 0
                : this.cardProcessingResponseTimeConcord / this.cardProcessingRequestConcord);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_ALLIANCE, this.cardProcessingRequestAlliance == 0 ? 0
                : this.cardProcessingResponseTimeAlliance / this.cardProcessingRequestAlliance);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_AUTHORIZENET, this.cardProcessingRequestAuthorizeNet == 0 ? 0
                : this.cardProcessingResponseTimeAuthorizeNet / this.cardProcessingRequestAuthorizeNet);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_BLACKBOARD, this.cardProcessingRequestBlackBoard == 0 ? 0
                : this.cardProcessingResponseTimeBlackBoard / this.cardProcessingRequestBlackBoard);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_DATAWIRE, this.cardProcessingRequestDatawire == 0 ? 0
                : this.cardProcessingResponseTimeDatawire / this.cardProcessingRequestDatawire);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_FIRSTDATANASHVILLE, this.cardProcessingRequestFirstDataNashville == 0 ? 0
                : this.cardProcessingResponseTimeFirstDataNashville / this.cardProcessingRequestFirstDataNashville);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_FIRSTHORIZON, this.cardProcessingRequestFirstHorizon == 0 ? 0
                : this.cardProcessingResponseTimeFirstHorizon / this.cardProcessingRequestFirstHorizon);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_HEARTLAND, this.cardProcessingRequestHeartland == 0 ? 0
                : this.cardProcessingResponseTimeHeartland / this.cardProcessingRequestHeartland);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_HEARTLAND_SPPLUS, this.cardProcessingRequestHeartlandSPPlus == 0 ? 0
                : this.cardProcessingResponseTimeHeartlandSPPlus / this.cardProcessingRequestHeartlandSPPlus);        
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_LINK2GOV, this.cardProcessingRequestLink2Gov == 0 ? 0
                : this.cardProcessingResponseTimeLink2Gov / this.cardProcessingRequestLink2Gov);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_MONERIS, this.cardProcessingRequestMoneris == 0 ? 0
                : this.cardProcessingResponseTimeMoneris / this.cardProcessingRequestMoneris);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_NUVISION, this.cardProcessingRequestNuVision == 0 ? 0
                : this.cardProcessingResponseTimeNuVision / this.cardProcessingRequestNuVision);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_PARADATA, this.cardProcessingRequestParadata == 0 ? 0
                : this.cardProcessingResponseTimeParadata / this.cardProcessingRequestParadata);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_PAYMENTECH, this.cardProcessingRequestPaymentech == 0 ? 0
                : this.cardProcessingResponseTimePaymentech / this.cardProcessingRequestPaymentech);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_TOTALCARD, this.cardProcessingRequestTotalCard == 0 ? 0
                : this.cardProcessingResponseTimeTotalCard / this.cardProcessingRequestTotalCard);
        
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_CBORDGOLD, this.cardProcessingRequestCBordGold == 0 ? 0
                : this.cardProcessingResponseTimeCBordGold / this.cardProcessingRequestCBordGold);
        
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_CBORDODYSSEY, this.cardProcessingRequestCBordOdyssey == 0 ? 0
                : this.cardProcessingResponseTimeCBordOdyssey / this.cardProcessingRequestCBordOdyssey);
        
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_ELAVON_CONVERGE, this.cardProcessingRequestElavonConverge == 0 ? 0
                : this.cardProcessingResponseTimeElavonConverge / this.cardProcessingRequestElavonConverge);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_ELAVON_VIACONEX, this.cardProcessingRequestElavonViaConex == 0 ? 0
                : this.cardProcessingResponseTimeElavonViaConex / this.cardProcessingRequestElavonViaConex);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_TD_MERCHANT_SERVICES, this.cardProcessingRequestTDMerchantServices == 0 ? 0
                : this.cardProcessingResponseTimeTDMerchantServices / this.cardProcessingRequestTDMerchantServices);
        map.put(KPIConstants.CARD_PROCESSING_RESPONSE_TIME_CREDITCALL, this.cardProcessingRequestCreditcall == 0 ? 0
                : this.cardProcessingResponseTimeCreditcall / this.cardProcessingRequestCreditcall);        
        
        map.put(KPIConstants.CARD_PROCESSING_QUEUE_EXHAUSTING_STOREANDFORWARD, this.cardProcessingQueueExhaustingStoreAndForward.getAndSet(0));
        
        // Background Processes
        map.put(KPIConstants.BACKGROUND_PROCESSES_SMSALERT_SENT, this.backgroundProcessesSMSAlertSent);
        map.put(KPIConstants.BACKGROUND_PROCESSES_SMSALERT_RECEIVE, this.backgroundProcessesSMSAlertReceive);
        
        map.put(KPIConstants.BACKGROUND_PROCESSES_ALERT_EMAIL_RESPONSE_TIME, this.backgroundProcessesAlertEmail == 0 ? 0
                : this.backgroundProcessesAlertEmailResponseTime / this.backgroundProcessesAlertEmail);
        
        // BOSS
        map.put(KPIConstants.BOSS_UPLOAD_SUCCESS_LOTSETTING, this.bossUploadSuccessLotSetting);
        map.put(KPIConstants.BOSS_UPLOAD_SUCCESS_FILE_UPLOAD, this.bossUploadSuccessFileUpload);
        map.put(KPIConstants.BOSS_UPLOAD_SUCCESS_PAYSTATION_QUERY, this.bossUploadSuccessPaystationQuery);
        map.put(KPIConstants.BOSS_UPLOAD_SUCCESS_OFFLINE_TRANSACTION, this.bossUploadSuccessOfflineTransaction);
        map.put(KPIConstants.BOSS_UPLOAD_FAIL_LOTSETTING, this.bossUploadFailLotSetting);
        map.put(KPIConstants.BOSS_UPLOAD_FAIL_FILE_UPLOAD, this.bossUploadFailFileUpload);
        map.put(KPIConstants.BOSS_UPLOAD_FAIL_PAYSTATION_QUERY, this.bossUploadFailPaystationQuery);
        map.put(KPIConstants.BOSS_UPLOAD_FAIL_OFFLINE_TRANSACTION, this.bossUploadFailOfflineTransaction);
        
        map.put(KPIConstants.BOSS_DOWNLOAD_SUCCESS_LOTSETTING, this.bossDownloadSuccessLotSetting);
        map.put(KPIConstants.BOSS_DOWNLOAD_SUCCESS_PUBLICKEY_REQUEST, this.bossDownloadSuccessPublicKeyRequest);
        map.put(KPIConstants.BOSS_DOWNLOAD_SUCCESS_CREDITCARD, this.bossDownloadSuccessCreditCard);
        map.put(KPIConstants.BOSS_DOWNLOAD_SUCCESS_BADSMARTCARD, this.bossDownloadSuccessBadSmartCard);
        map.put(KPIConstants.BOSS_DOWNLOAD_FAIL_LOTSETTING, this.bossDownloadFailLotSetting);
        map.put(KPIConstants.BOSS_DOWNLOAD_FAIL_PUBLICKEY_REQUEST, this.bossDownloadFailPublicKeyRequest);
        map.put(KPIConstants.BOSS_DOWNLOAD_FAIL_CREDITCARD, this.bossDownloadFailCreditCard);
        map.put(KPIConstants.BOSS_DOWNLOAD_FAIL_BADSMARTCARD, this.bossDownloadFailBadSmartCard);
        
        map.put(KPIConstants.BOSS_UPLOAD_SUCCESS_TOTAL, this.bossUploadSuccessTotal);
        map.put(KPIConstants.BOSS_DOWNLOAD_SUCCESS_TOTAL, this.bossDownloadSuccessTotal);
        map.put(KPIConstants.BOSS_UPLOAD_FAIL_TOTAL, this.bossUploadFailTotal);
        map.put(KPIConstants.BOSS_DOWNLOAD_FAIL_TOTAL, this.bossDownloadFailTotal);
        
        map.put(KPIConstants.BOSS_UPLOAD_TOTAL, this.bossUploadSuccessTotal + this.bossUploadFailTotal);
        map.put(KPIConstants.BOSS_DOWNLOAD_TOTAL, this.bossDownloadSuccessTotal + this.bossDownloadFailTotal);
        
        map.put(KPIConstants.BUILD_VERSION, this.emsProperties.getIrisVersion());
        map.put(KPIConstants.BUILD_NUMBER, this.emsProperties.getIrisBuildNumber());
        map.put(KPIConstants.BUILD_DATE, this.emsProperties.getIrisBuildDate());
        map.put(KPIConstants.BUILD_DEPLOYMENT_MODE, this.emsProperties.getIrisDeploymentMode());
        
        /* For Daily KPI Start */
        // These are for running daily update once when server startup
        if (!this.executedOnce) {
            try {
                updateDaily();
            } catch (Exception e) {
                log.error("Error while executing first KPI Daily update !", e);
            }
        }
        
        // This is to reset last minute flag. So that query by key "name" will force the daily values to get updated.
        this.executedInLastMinute = false;
        
        /* For Daily KPI End */
        
        reset();
        
        map.put("!execution.time", System.currentTimeMillis() - startTime);
        
        this.kpiInfo = map;
    }
    
    @Async
    @Override
    public final Future<Void> runSQL() {
        final long startTime = System.currentTimeMillis();
        
        this.kpiInfo.put(KPIConstants.UI_LOGGED_IN, this.activityLoginService.countSuccessfulLoginWithinLastMinute(new Date(startTime)));
        
        // Background Processes
        final int backgroundProcessesAlertCommunication = this.posAlertStatusService.getCommunicationAlertsSum();
        final int backgroundProcessesAlertCollection = this.posAlertStatusService.getCollectionAlertsSum();
        final int backgroundProcessesAlertPaystation = this.posAlertStatusService.getPaystationAlertsSum();
        
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_ALERT_TOTAL, backgroundProcessesAlertCommunication + backgroundProcessesAlertCollection
                                                                        + backgroundProcessesAlertPaystation);
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_ALERT_COMMUNICATION, backgroundProcessesAlertCommunication);
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_ALERT_COLLECTION, backgroundProcessesAlertCollection);
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_ALERT_PAYSTATION, backgroundProcessesAlertPaystation);
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_RECALC, this.collectionTypeService.getPOSBalanceRecalcableCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_RECALC_FAIL, this.collectionLockService.getFailedRecalcs());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_READY, this.reportDefinitionService.getReadyReportCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_SCHEDULED, this.reportQueueService.getScheduledReportCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_LAUNCHED, this.reportQueueService.getLaunchedReportCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_COMPLETED, this.reportHistoryService.getCompletedReports());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_CANCELLED, this.reportHistoryService.getCancelledReports());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_FAILED, this.reportHistoryService.getFailedReports());
        
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_REPORT_RESPONSE_TIME, this.reportHistoryService.getReportResponseTime());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_SENSOR_DATA_TO_PROCESS, this.rawSensorDataService.getRawSensorDataCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_EXTENSIBLE_PERMIT, this.extensiblePermitService.getExtensiblePermitByCount());
        this.kpiInfo.put(KPIConstants.BACKGROUND_PROCESSES_SMSALERT_TO_PROCESS, this.smsAlertService.getSmsAlertNotAlerted());
        
        // ETL
        this.kpiInfo.put(KPIConstants.INCREMENTAL_MIGRATION_TRANSACTION_SIZE, this.stagingPurchaseService.getRecordsToProcessByCount());
        this.kpiInfo.put(KPIConstants.INCREMENTAL_MIGRATION_CUSTOMER_TO_PROCESS, this.stagingPurchaseService.getCustomersToProcessByCount());
        this.kpiInfo.put(KPIConstants.INCREMENTAL_MIGRATION_RECORD_ERROR,
                         this.etlNotProcessedService.getPermit() + this.etlNotProcessedService.getPurchase());
        
        this.kpiInfo.put("!execution.time.sql", System.currentTimeMillis() - startTime);
        return new AsyncResult<Void>(null);
    }
    
    private void reset() {
        
        // Digital API
        this.digitalAPIResponseTimeAuditInfo = 0;
        this.digitalAPIResponseTimePaystationInfo = 0;
        this.digitalAPIResponseTimePlateInfo = 0;
        this.digitalAPIResponseTimeStallInfo = 0;
        this.digitalAPIResponseTimeTransactionInfo = 0;
        
        this.digitalAPIRequestAuditInfo = 0;
        this.digitalAPIRequestPaystationInfo = 0;
        this.digitalAPIRequestPlateInfo = 0;
        this.digitalAPIRequestStallInfo = 0;
        this.digitalAPIRequestTransactionInfo = 0;
        
        this.digitalAPIRequestSuccessAuditInfo = 0;
        this.digitalAPIRequestSuccessPaystationInfo = 0;
        this.digitalAPIRequestSuccessPlateInfo = 0;
        this.digitalAPIRequestSuccessStallInfo = 0;
        this.digitalAPIRequestSuccessTransactionInfo = 0;
        this.digitalAPIRequestSuccessCoupon = 0;
        this.digitalAPIRequestFailAuditInfo = 0;
        this.digitalAPIRequestFailPaystationInfo = 0;
        this.digitalAPIRequestFailPlateInfo = 0;
        this.digitalAPIRequestFailStallInfo = 0;
        this.digitalAPIRequestFailTransactionInfo = 0;
        this.digitalAPIRequestFailCoupon = 0;
        
        this.digitalAPICouponRecord = 0;
        this.digitalAPICouponFile = 0;
        this.digitalAPICouponFileSize = 0;
        
        // UI
        this.uiFileUploadSuccessCoupon = 0;
        this.uiFileUploadSuccessPasscard = 0;
        this.uiFileUploadSuccessBannedCard = 0;
        this.uiFileUploadFailCoupon = 0;
        this.uiFileUploadFailPasscard = 0;
        this.uiFileUploadFailBannedCard = 0;
        
        this.uiFileDownloadSuccessCoupon = 0;
        this.uiFileDownloadSuccessPasscard = 0;
        this.uiFileDownloadSuccessReport = 0;
        this.uiFileDownloadSuccessWidget = 0;
        this.uiFileDownloadFailCoupon = 0;
        this.uiFileDownloadFailPasscard = 0;
        this.uiFileDownloadFailReport = 0;
        this.uiFileDownloadFailWidget = 0;
        
        this.uiFileUploaded = 0;
        this.uiFileSize = 0;
        
        // UI Dashboard
        this.uiDashboardResponseTimeAlerts = 0;
        this.uiDashboardResponseTimeRevenue = 0;
        this.uiDashboardResponseTimeCollections = 0;
        this.uiDashboardResponseTimeSettledCard = 0;
        this.uiDashboardResponseTimePurchases = 0;
        this.uiDashboardResponseTimePaidOccupancy = 0;
        this.uiDashboardResponseTimeTurnover = 0;
        this.uiDashboardResponseTimeUtilization = 0;
        this.uiDashboardResponseTimeMap = 0;
        
        this.uiDashboardAlerts = 0;
        this.uiDashboardRevenue = 0;
        this.uiDashboardCollections = 0;
        this.uiDashboardSettledCard = 0;
        this.uiDashboardPurchases = 0;
        this.uiDashboardPaidOccupancy = 0;
        this.uiDashboardTurnover = 0;
        this.uiDashboardUtilization = 0;
        this.uiDashboardMap = 0;
        
        // UI Request Status Codes
        this.uiRequestInformation = 0;
        this.uiRequestSuccess = 0;
        this.uiRequestRedirect = 0;
        this.uiRequestClientError = 0;
        this.uiRequestServerError = 0;
        this.uiRequest449 = 0;
        this.uiRequest480 = 0;
        this.uiRequest481 = 0;
        this.uiRequest482 = 0;
        this.uiRequest483 = 0;
        this.uiRequest509 = 0;
        
        this.uncaughtExceptionInformation = 0;
        this.uncaughtExceptionSuccess = 0;
        this.uncaughtExceptionRedirect = 0;
        this.uncaughtExceptionClientError = 0;
        this.uncaughtExceptionServerError = 0;
        
        // XMLRPC
        this.xmlrpcRequestSuccessAudit = 0;
        this.xmlrpcRequestSuccessTransaction = 0;
        this.xmlrpcRequestSuccessAuthorizeRequest = 0;
        this.xmlrpcRequestSuccessEvent = 0;
        this.xmlrpcRequestSuccessHeartBeat = 0;
        this.xmlrpcRequestSuccessLotSettingSuccess = 0;
        this.xmlrpcRequestSuccessPlateRptReq = 0;
        this.xmlrpcRequestSuccessAuthorizeCardRequest = 0;
        this.xmlrpcRequestSuccessStallInfoRequest = 0;
        this.xmlrpcRequestSuccessStallReportRequest = 0;
        this.xmlrpcRequestFailAudit = 0;
        this.xmlrpcRequestFailTransaction = 0;
        this.xmlrpcRequestFailAuthorizeRequest = 0;
        this.xmlrpcRequestFailEvent = 0;
        this.xmlrpcRequestFailHeartBeat = 0;
        this.xmlrpcRequestFailLotSettingSuccess = 0;
        this.xmlrpcRequestFailPlateRptReq = 0;
        this.xmlrpcRequestFailAuthorizeCardRequest = 0;
        this.xmlrpcRequestFailStallInfoRequest = 0;
        this.xmlrpcRequestFailStallReportRequest = 0;
        
        this.xmlrpcRequestTransactionSuccessStoreAndForward = 0;
        this.xmlrpcRequestTransactionSuccessCashCreditSwipe = 0;
        this.xmlrpcRequestTransactionSuccessCashCreditCL = 0;
        this.xmlrpcRequestTransactionSuccessCashCreditCH = 0;
        this.xmlrpcRequestTransactionSuccessCashValue = 0;
        this.xmlrpcRequestTransactionSuccessValue = 0;
        this.xmlrpcRequestTransactionSuccessCreditSwipe = 0;
        this.xmlrpcRequestTransactionSuccessCreditCL = 0;
        this.xmlrpcRequestTransactionSuccessCreditCH = 0;
        this.xmlrpcRequestTransactionSuccessCashSmart = 0;
        this.xmlrpcRequestTransactionSuccessSmart = 0;
        this.xmlrpcRequestTransactionSuccessUnknown = 0;
        this.xmlrpcRequestTransactionSuccessCash = 0;
        this.xmlrpcRequestTransactionSuccessNA = 0;
        this.xmlrpcRequestTransactionSuccessPayAndDisplay = 0;
        this.xmlrpcRequestTransactionSuccessPayByPlate = 0;
        this.xmlrpcRequestTransactionSuccessPayBySpace = 0;
        
        //        this.xmlrpcRequestTransactionFailStoreAndForward = 0;
        this.xmlrpcRequestTransactionFailCashCreditSwipe = 0;
        this.xmlrpcRequestTransactionFailCashCreditCL = 0;
        this.xmlrpcRequestTransactionFailCashCreditCH = 0;
        this.xmlrpcRequestTransactionFailCashValue = 0;
        this.xmlrpcRequestTransactionFailValue = 0;
        this.xmlrpcRequestTransactionFailCreditSwipe = 0;
        this.xmlrpcRequestTransactionFailCreditCL = 0;
        this.xmlrpcRequestTransactionFailCreditCH = 0;
        this.xmlrpcRequestTransactionFailCashSmart = 0;
        this.xmlrpcRequestTransactionFailSmart = 0;
        this.xmlrpcRequestTransactionFailUnknown = 0;
        this.xmlrpcRequestTransactionFailCash = 0;
        this.xmlrpcRequestTransactionFailNA = 0;
        this.xmlrpcRequestTransactionFailPayAndDisplay = 0;
        this.xmlrpcRequestTransactionFailPayByPlate = 0;
        this.xmlrpcRequestTransactionFailPayBySpace = 0;
        
        // Pay Station REST
        this.paystationRestFailCancelTransaction = 0;
        this.paystationRestFailHeartBeat = 0;
        this.paystationRestFailToken = 0;
        this.paystationRestFailTransaction = 0;
        
        this.paystationRestSuccessCancelTransaction = 0;
        this.paystationRestSuccessHeartBeat = 0;
        this.paystationRestSuccessToken = 0;
        this.paystationRestSuccessTransaction = 0;
        
        this.paystationRestTransactionFailPaymentCash = 0;
        this.paystationRestTransactionFailPaymentCashSmart = 0;
        this.paystationRestTransactionFailPaymentCashValue = 0;
        this.paystationRestTransactionFailPaymentCashCredit = 0;
        this.paystationRestTransactionFailPaymentCredit = 0;
        this.paystationRestTransactionFailPaymentSmart = 0;
        this.paystationRestTransactionFailPaymentUnknown = 0;
        this.paystationRestTransactionFailPaymentValue = 0;
        this.paystationRestTransactionFailPermitNA = 0;
        this.paystationRestTransactionFailPermitPayAndDisplay = 0;
        this.paystationRestTransactionFailPermitPayByPlate = 0;
        this.paystationRestTransactionFailPermitPayBySpace = 0;
        this.paystationRestTransactionFailStoreAndForward = 0;
        
        this.paystationRestTransactionSuccessPaymentCash = 0;
        this.paystationRestTransactionSuccessPaymentCashSmart = 0;
        this.paystationRestTransactionSuccessPaymentCashValue = 0;
        this.paystationRestTransactionSuccessPaymentCashCredit = 0;
        this.paystationRestTransactionSuccessPaymentCredit = 0;
        this.paystationRestTransactionSuccessPaymentSmart = 0;
        this.paystationRestTransactionSuccessPaymentUnknown = 0;
        this.paystationRestTransactionSuccessPaymentValue = 0;
        this.paystationRestTransactionSuccessPermitNA = 0;
        this.paystationRestTransactionSuccessPermitPayAndDisplay = 0;
        this.paystationRestTransactionSuccessPermitPayByPlate = 0;
        this.paystationRestTransactionSuccessPermitPayBySpace = 0;
        this.paystationRestTransactionSuccessStoreAndForward = 0;
        
        this.paystationRestFailParameterError = 0;
        this.paystationRestFailSignatureError = 0;
        this.paystationRestFailSystemError = 0;
        
        // Card Processing
        this.cardProcessingRequestConcord = 0;
        this.cardProcessingRequestAlliance = 0;
        this.cardProcessingRequestAuthorizeNet = 0;
        this.cardProcessingRequestBlackBoard = 0;
        this.cardProcessingRequestDatawire = 0;
        this.cardProcessingRequestFirstDataNashville = 0;
        this.cardProcessingRequestFirstHorizon = 0;
        this.cardProcessingRequestHeartland = 0;
        this.cardProcessingRequestHeartlandSPPlus = 0;
        this.cardProcessingRequestLink2Gov = 0;
        this.cardProcessingRequestMoneris = 0;
        this.cardProcessingRequestNuVision = 0;
        this.cardProcessingRequestParadata = 0;
        this.cardProcessingRequestPaymentech = 0;
        this.cardProcessingRequestTotalCard = 0;
        this.cardProcessingRequestCBordGold = 0;
        this.cardProcessingRequestCBordOdyssey = 0;
        this.cardProcessingRequestElavonConverge = 0;
        this.cardProcessingRequestElavonViaConex = 0;
        this.cardProcessingRequestTDMerchantServices = 0;
        this.cardProcessingRequestCreditcall = 0;
        
        this.cardProcessingResponseTimeConcord = 0;
        this.cardProcessingResponseTimeAlliance = 0;
        this.cardProcessingResponseTimeAuthorizeNet = 0;
        this.cardProcessingResponseTimeBlackBoard = 0;
        this.cardProcessingResponseTimeDatawire = 0;
        this.cardProcessingResponseTimeFirstDataNashville = 0;
        this.cardProcessingResponseTimeFirstHorizon = 0;
        this.cardProcessingResponseTimeHeartland = 0;
        this.cardProcessingResponseTimeHeartlandSPPlus = 0;
        this.cardProcessingResponseTimeLink2Gov = 0;
        this.cardProcessingResponseTimeMoneris = 0;
        this.cardProcessingResponseTimeNuVision = 0;
        this.cardProcessingResponseTimeParadata = 0;
        this.cardProcessingResponseTimePaymentech = 0;
        this.cardProcessingResponseTimeTotalCard = 0;
        this.cardProcessingResponseTimeCBordGold = 0;
        this.cardProcessingResponseTimeCBordOdyssey = 0;
        this.cardProcessingResponseTimeElavonConverge = 0;
        this.cardProcessingResponseTimeElavonViaConex = 0;
        this.cardProcessingResponseTimeTDMerchantServices = 0;
        this.cardProcessingResponseTimeCreditcall = 0;
        
        // Background Processes
        this.backgroundProcessesSMSAlertSent = 0;
        this.backgroundProcessesAlertEmail = 0;
        this.backgroundProcessesAlertEmailResponseTime = 0;
        
        // BOSS
        this.bossUploadSuccessLotSetting = 0;
        this.bossUploadSuccessFileUpload = 0;
        this.bossUploadSuccessPaystationQuery = 0;
        this.bossUploadSuccessOfflineTransaction = 0;
        this.bossDownloadSuccessLotSetting = 0;
        this.bossDownloadSuccessPublicKeyRequest = 0;
        this.bossDownloadSuccessCreditCard = 0;
        this.bossDownloadSuccessBadSmartCard = 0;
        
        this.bossUploadFailLotSetting = 0;
        this.bossUploadFailFileUpload = 0;
        this.bossUploadFailPaystationQuery = 0;
        this.bossUploadFailOfflineTransaction = 0;
        this.bossDownloadFailLotSetting = 0;
        this.bossDownloadFailPublicKeyRequest = 0;
        this.bossDownloadFailCreditCard = 0;
        this.bossDownloadFailBadSmartCard = 0;
        
        this.bossUploadSuccessTotal = 0;
        this.bossDownloadSuccessTotal = 0;
        this.bossUploadFailTotal = 0;
        this.bossDownloadFailTotal = 0;
    }
    
    @Override
    public final Map<String, Object> getKPIInfo() {
        return this.kpiInfo;
    }
    
    // UI Upload
    
    @Override
    public final void incrementUIFileUploadSuccessCoupon() {
        this.uiFileUploadSuccessCoupon++;
    }
    
    @Override
    public final void incrementUIFileUploadSuccessPasscard() {
        this.uiFileUploadSuccessPasscard++;
    }
    
    @Override
    public final void incrementUIFileUploadSuccessBannedCard() {
        this.uiFileUploadSuccessBannedCard++;
    }
    
    @Override
    public final void incrementUIFileUploadFailCoupon() {
        this.uiFileUploadFailCoupon++;
    }
    
    @Override
    public final void incrementUIFileUploadFailPasscard() {
        this.uiFileUploadFailPasscard++;
    }
    
    @Override
    public final void incrementUIFileUploadFailBannedCard() {
        this.uiFileUploadFailBannedCard++;
    }
    
    // UI Download
    
    @Override
    public final void incrementUIFileDownloadSuccessCoupon() {
        this.uiFileDownloadSuccessCoupon++;
    }
    
    @Override
    public final void incrementUIFileDownloadSuccessPasscard() {
        this.uiFileDownloadSuccessPasscard++;
    }
    
    @Override
    public final void incrementUIFileDownloadSuccessReport() {
        this.uiFileDownloadSuccessReport++;
    }
    
    @Override
    public final void incrementUIFileDownloadSuccessWidget() {
        this.uiFileDownloadSuccessWidget++;
    }
    
    @Override
    public final void incrementUIFileDownloadFailCoupon() {
        this.uiFileDownloadFailCoupon++;
    }
    
    @Override
    public final void incrementUIFileDownloadFailPasscard() {
        this.uiFileDownloadFailPasscard++;
    }
    
    @Override
    public final void incrementUIFileDownloadFailReport() {
        this.uiFileDownloadFailReport++;
    }
    
    @Override
    public final void incrementUIFileDownloadFailWidget() {
        this.uiFileDownloadFailWidget++;
    }
    
    // Digital API
    
    @Override
    public final void addDigitalAPIResponseTimeAuditInfo(final long responseTime) {
        this.digitalAPIRequestAuditInfo++;
        this.digitalAPIResponseTimeAuditInfo += responseTime;
    }
    
    @Override
    public final void addDigitalAPIResponseTimePaystationInfo(final long responseTime) {
        this.digitalAPIRequestPaystationInfo++;
        this.digitalAPIResponseTimePaystationInfo += responseTime;
    }
    
    @Override
    public final void addDigitalAPIResponseTimePlateInfo(final long responseTime) {
        this.digitalAPIRequestPlateInfo++;
        this.digitalAPIResponseTimePlateInfo += responseTime;
    }
    
    @Override
    public final void addDigitalAPIResponseTimeStallInfo(final long responseTime) {
        this.digitalAPIRequestStallInfo++;
        this.digitalAPIResponseTimeStallInfo += responseTime;
    }
    
    @Override
    public final void addDigitalAPIResponseTimeTransactionInfo(final long responseTime) {
        this.digitalAPIRequestTransactionInfo++;
        this.digitalAPIResponseTimeTransactionInfo += responseTime;
    }
    
    @Override
    public final void incrementDigitalAPIRequestSuccessAuditInfo() {
        this.digitalAPIRequestSuccessAuditInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestSuccessPaystationInfo() {
        this.digitalAPIRequestSuccessPaystationInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestSuccessPlateInfo() {
        this.digitalAPIRequestSuccessPlateInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestSuccessStallInfo() {
        this.digitalAPIRequestSuccessStallInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestSuccessTransactionInfo() {
        this.digitalAPIRequestSuccessTransactionInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestCouponSuccess(final long responseTime, final int fileSize) {
        this.digitalAPIRequestSuccessCoupon++;
        this.digitalAPIResponseTimeCoupon += responseTime;
        this.digitalAPICouponFileSize += fileSize;
    }
    
    @Override
    public final void addDigitalAPICouponRecord(final int records) {
        this.digitalAPICouponRecord += records;
        this.digitalAPICouponFile++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestFailAuditInfo() {
        this.digitalAPIRequestFailAuditInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestFailPaystationInfo() {
        this.digitalAPIRequestFailPaystationInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestFailPlateInfo() {
        this.digitalAPIRequestFailPlateInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestFailStallInfo() {
        this.digitalAPIRequestFailStallInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestFailTransactionInfo() {
        this.digitalAPIRequestFailTransactionInfo++;
    }
    
    @Override
    public final void incrementDigitalAPIRequestCouponFail(final long responseTime, final int fileSize) {
        this.digitalAPIRequestFailCoupon++;
        this.digitalAPIResponseTimeCoupon += responseTime;
        this.digitalAPICouponFileSize += fileSize;
    }
    
    // XMLRPC Requests
    
    @Override
    public final void incrementXMLRPCRequestSuccessAudit() {
        this.xmlrpcRequestSuccessAudit++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessTransaction() {
        this.xmlrpcRequestSuccessTransaction++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessAuthorizeRequest() {
        this.xmlrpcRequestSuccessAuthorizeRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessEvent() {
        this.xmlrpcRequestSuccessEvent++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessHeartBeat() {
        this.xmlrpcRequestSuccessHeartBeat++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessLotSettingSuccess() {
        this.xmlrpcRequestSuccessLotSettingSuccess++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessPlateRptReq() {
        this.xmlrpcRequestSuccessPlateRptReq++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessAuthorizeCardRequest() {
        this.xmlrpcRequestSuccessAuthorizeCardRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessStallInfoRequest() {
        this.xmlrpcRequestSuccessStallInfoRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestSuccessStallReportRequest() {
        this.xmlrpcRequestSuccessStallReportRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailAudit() {
        this.xmlrpcRequestFailAudit++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailTransaction() {
        this.xmlrpcRequestFailTransaction++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailAuthorizeRequest() {
        this.xmlrpcRequestFailAuthorizeRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailEvent() {
        this.xmlrpcRequestFailEvent++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailHeartBeat() {
        this.xmlrpcRequestFailHeartBeat++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailLotSettingSuccess() {
        this.xmlrpcRequestFailLotSettingSuccess++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailPlateRptReq() {
        this.xmlrpcRequestFailPlateRptReq++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailAuthorizeCardRequest() {
        this.xmlrpcRequestFailAuthorizeCardRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailStallInfoRequest() {
        this.xmlrpcRequestFailStallInfoRequest++;
    }
    
    @Override
    public final void incrementXMLRPCRequestFailStallReportRequest() {
        this.xmlrpcRequestSuccessStallReportRequest++;
    }
    
    // Pay Station REST
    @Override
    public final void incrementPaystationRestFailCancelTransaction() {
        this.paystationRestFailCancelTransaction++;
    }
    
    @Override
    public final void incrementPaystationRestFailHeartBeat() {
        this.paystationRestFailHeartBeat++;
    }
    
    @Override
    public final void incrementPaystationRestFailToken() {
        this.paystationRestFailToken++;
    }
    
    @Override
    public final void incrementPaystationRestFailTransaction() {
        this.paystationRestFailTransaction++;
    }
    
    @Override
    public final void incrementPaystationRestSuccessCancelTransaction() {
        this.paystationRestSuccessCancelTransaction++;
    }
    
    @Override
    public final void incrementPaystationRestSuccessHeartBeat() {
        this.paystationRestSuccessHeartBeat++;
    }
    
    @Override
    public final void incrementPaystationRestSuccessToken() {
        this.paystationRestSuccessToken++;
    }
    
    @Override
    public final void incrementPaystationRestSuccessTransaction() {
        this.paystationRestSuccessTransaction++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentCash() {
        this.paystationRestTransactionFailPaymentCash++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentCashSmart() {
        this.paystationRestTransactionFailPaymentCashSmart++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentCashValue() {
        this.paystationRestTransactionFailPaymentCashValue++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentCashCredit() {
        this.paystationRestTransactionFailPaymentCashCredit++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentCredit() {
        this.paystationRestTransactionFailPaymentCredit++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentSmart() {
        this.paystationRestTransactionFailPaymentSmart++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentUnknown() {
        this.paystationRestTransactionFailPaymentUnknown++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPaymentValue() {
        this.paystationRestTransactionFailPaymentValue++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPermitNA() {
        this.paystationRestTransactionFailPermitNA++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPermitPayAndDisplay() {
        this.paystationRestTransactionFailPermitPayAndDisplay++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPermitPayByPlate() {
        this.paystationRestTransactionFailPermitPayByPlate++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailPermitPayBySpace() {
        this.paystationRestTransactionFailPermitPayBySpace++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionFailStoreAndForward() {
        this.paystationRestTransactionFailStoreAndForward++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentCash() {
        this.paystationRestTransactionSuccessPaymentCash++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentCashSmart() {
        this.paystationRestTransactionSuccessPaymentCashSmart++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentCashValue() {
        this.paystationRestTransactionSuccessPaymentCashValue++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentCashCredit() {
        this.paystationRestTransactionSuccessPaymentCashCredit++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentCredit() {
        this.paystationRestTransactionSuccessPaymentCredit++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentSmart() {
        this.paystationRestTransactionSuccessPaymentSmart++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentUnknown() {
        this.paystationRestTransactionSuccessPaymentUnknown++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPaymentValue() {
        this.paystationRestTransactionSuccessPaymentValue++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPermitNA() {
        this.paystationRestTransactionSuccessPermitNA++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPermitPayAndDisplay() {
        this.paystationRestTransactionSuccessPermitPayAndDisplay++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPermitPayByPlate() {
        this.paystationRestTransactionSuccessPermitPayByPlate++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessPermitPayBySpace() {
        this.paystationRestTransactionSuccessPermitPayBySpace++;
    }
    
    @Override
    public final void incrementPaystationRestTransactionSuccessStoreAndForward() {
        this.paystationRestTransactionSuccessStoreAndForward++;
    }
    
    @Override
    public final void incrementPaystationRestFailParameterError() {
        this.paystationRestFailParameterError++;
    }
    
    @Override
    public final void incrementPaystationRestFailSignatureError() {
        this.paystationRestFailSignatureError++;
    }
    
    @Override
    public final void incrementPaystationRestFailSystemError() {
        this.paystationRestFailSystemError++;
    }
    
    // XMLRPC Transactions
    
    // Dashboard
    
    @Override
    public final void addUIDashboardResponseTimeAlerts(final long responseTime) {
        this.uiDashboardAlerts++;
        this.uiDashboardResponseTimeAlerts += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeRevenue(final long responseTime) {
        this.uiDashboardRevenue++;
        this.uiDashboardResponseTimeRevenue += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeCollections(final long responseTime) {
        this.uiDashboardCollections++;
        this.uiDashboardResponseTimeCollections += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeSettledCard(final long responseTime) {
        this.uiDashboardSettledCard++;
        this.uiDashboardResponseTimeSettledCard += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimePurchases(final long responseTime) {
        this.uiDashboardPurchases++;
        this.uiDashboardResponseTimePurchases += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimePaidOccupancy(final long responseTime) {
        this.uiDashboardPaidOccupancy++;
        this.uiDashboardResponseTimePaidOccupancy += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeTurnover(final long responseTime) {
        this.uiDashboardTurnover++;
        this.uiDashboardResponseTimeTurnover += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeUtilization(final long responseTime) {
        this.uiDashboardUtilization++;
        this.uiDashboardResponseTimeUtilization += responseTime;
    }
    
    @Override
    public final void addUIDashboardResponseTimeMap(final long responseTime) {
        this.uiDashboardMap++;
        this.uiDashboardResponseTimeMap += responseTime;
    }
    
    @Override
    public final void incrementUIDashboardQuerySuccess() {
        this.uiDashboardQuerySuccess++;
    }
    
    @Override
    public final void incrementUIDashboardQueryFail() {
        this.uiDashboardQueryFail++;
    }
    
    @Override
    public final void incrementCardProcessingRequestConcord(final long responseTime) {
        this.cardProcessingRequestConcord++;
        this.cardProcessingResponseTimeConcord += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestAlliance(final long responseTime) {
        this.cardProcessingRequestAlliance++;
        this.cardProcessingResponseTimeAlliance += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestAuthorizeNet(final long responseTime) {
        this.cardProcessingRequestAuthorizeNet++;
        this.cardProcessingResponseTimeAuthorizeNet += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestBlackBoard(final long responseTime) {
        this.cardProcessingRequestBlackBoard++;
        this.cardProcessingResponseTimeBlackBoard += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestDatawire(final long responseTime) {
        this.cardProcessingRequestDatawire++;
        this.cardProcessingResponseTimeDatawire += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestFirstDataNashville(final long responseTime) {
        this.cardProcessingRequestFirstDataNashville++;
        this.cardProcessingResponseTimeFirstDataNashville += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestFirstHorizon(final long responseTime) {
        this.cardProcessingRequestFirstHorizon++;
        this.cardProcessingResponseTimeFirstHorizon += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestHeartland(final long responseTime) {
        this.cardProcessingRequestHeartland++;
        this.cardProcessingResponseTimeHeartland += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestHeartlandSPPlus(final long responseTime) {
        this.cardProcessingRequestHeartlandSPPlus++;
        this.cardProcessingResponseTimeHeartlandSPPlus += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestLink2Gov(final long responseTime) {
        this.cardProcessingRequestLink2Gov++;
        this.cardProcessingResponseTimeLink2Gov += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestMoneris(final long responseTime) {
        this.cardProcessingRequestMoneris++;
        this.cardProcessingResponseTimeMoneris += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestNuVision(final long responseTime) {
        this.cardProcessingRequestNuVision++;
        this.cardProcessingResponseTimeNuVision += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestParadata(final long responseTime) {
        this.cardProcessingRequestParadata++;
        this.cardProcessingResponseTimeParadata += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestPaymentech(final long responseTime) {
        this.cardProcessingRequestPaymentech++;
        this.cardProcessingResponseTimePaymentech += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestTotalCard(final long responseTime) {
        this.cardProcessingRequestTotalCard++;
        this.cardProcessingResponseTimeTotalCard += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestCBordGold(final long responseTime) {
        this.cardProcessingRequestCBordGold++;
        this.cardProcessingResponseTimeCBordGold += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestCBordOdyssey(final long responseTime) {
        this.cardProcessingRequestCBordOdyssey++;
        this.cardProcessingResponseTimeCBordOdyssey += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestElavonConverge(final long responseTime) {
        this.cardProcessingRequestElavonConverge++;
        this.cardProcessingResponseTimeElavonConverge += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestElavonViaConex(final long responseTime) {
        this.cardProcessingRequestElavonViaConex++;
        this.cardProcessingResponseTimeElavonViaConex += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestTDMerchantServices(final long responseTime) {
        this.cardProcessingRequestTDMerchantServices++;
        this.cardProcessingResponseTimeTDMerchantServices += responseTime;
    }
    
    @Override
    public final void incrementCardProcessingRequestCreditcall(final long responseTime) {
        this.cardProcessingRequestCreditcall++;
        this.cardProcessingResponseTimeCreditcall += responseTime;
    }    
    
    @Override
    public final void incrementUIRequestInformation() {
        this.uiRequestInformation++;
    }
    
    @Override
    public final void incrementUIRequestSuccess() {
        this.uiRequestSuccess++;
    }
    
    @Override
    public final void incrementUIRequestRedirect() {
        this.uiRequestRedirect++;
    }
    
    @Override
    public final void incrementUIRequestClientError() {
        this.uiRequestClientError++;
    }
    
    @Override
    public final void incrementUIRequestServerError() {
        this.uiRequestServerError++;
    }
    
    @Override
    public final void incrementUIRequest449() {
        this.uiRequest449++;
    }
    
    @Override
    public final void incrementUIRequest480() {
        this.uiRequest480++;
    }
    
    @Override
    public final void incrementUIRequest481() {
        this.uiRequest481++;
    }
    
    @Override
    public final void incrementUIRequest482() {
        this.uiRequest482++;
    }
    
    @Override
    public final void incrementUIRequest483() {
        this.uiRequest483++;
    }
    
    @Override
    public final void incrementUIRequest509() {
        this.uiRequest509++;
    }
    
    public final void addUIFileSize(final long size) {
        this.uiFileUploaded++;
        this.uiFileSize += size;
    }
    
    // Background Processes SMS Alert
    
    @Override
    public final void incrementBackgroundProcessesSMSAlertSent() {
        this.backgroundProcessesSMSAlertSent++;
    }
    
    @Override
    public final void incrementBackgroundProcessesSMSAlertReceive() {
        this.backgroundProcessesSMSAlertReceive++;
    }
    
    @Override
    public final void addBackgroundProcessesEmailAlertResponseTime(final long responseTime) {
        this.backgroundProcessesAlertEmail++;
        this.backgroundProcessesAlertEmailResponseTime += responseTime;
    }
    
    @Override
    public final void incrementUncaughtExceptionInformation() {
        this.uncaughtExceptionInformation++;
    }
    
    @Override
    public final void incrementUncaughtExceptionSuccess() {
        this.uncaughtExceptionSuccess++;
    }
    
    @Override
    public final void incrementUncaughtExceptionRedirect() {
        this.uncaughtExceptionRedirect++;
    }
    
    @Override
    public final void incrementUncaughtExceptionClientError() {
        this.uncaughtExceptionClientError++;
    }
    
    @Override
    public final void incrementUncaughtExceptionServerError() {
        this.uncaughtExceptionServerError++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessStoreAndForward() {
        this.xmlrpcRequestTransactionSuccessStoreAndForward++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCashCreditSwipe() {
        this.xmlrpcRequestTransactionSuccessCashCreditSwipe++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCashCreditCL() {
        this.xmlrpcRequestTransactionSuccessCashCreditCL++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCashCreditCH() {
        this.xmlrpcRequestTransactionSuccessCashCreditCH++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCashValue() {
        this.xmlrpcRequestTransactionSuccessCashValue++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessValue() {
        this.xmlrpcRequestTransactionSuccessValue++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCreditSwipe() {
        this.xmlrpcRequestTransactionSuccessCreditSwipe++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCreditCL() {
        this.xmlrpcRequestTransactionSuccessCreditCL++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCreditCH() {
        this.xmlrpcRequestTransactionSuccessCreditCH++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCashSmart() {
        this.xmlrpcRequestTransactionSuccessCashSmart++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessSmart() {
        this.xmlrpcRequestTransactionSuccessSmart++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessUnknown() {
        this.xmlrpcRequestTransactionSuccessUnknown++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessCash() {
        this.xmlrpcRequestTransactionSuccessCash++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessNA() {
        this.xmlrpcRequestTransactionSuccessNA++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessPayAndDisplay() {
        this.xmlrpcRequestTransactionSuccessPayAndDisplay++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessPayByPlate() {
        this.xmlrpcRequestTransactionSuccessPayByPlate++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionSuccessPayBySpace() {
        this.xmlrpcRequestTransactionSuccessPayBySpace++;
    }
    
    //    @Override
    //    public void incrementXMLRPCRequestTransactionFailStoreAndForward() {
    //        xmlrpcRequestTransactionFailStoreAndForward++;
    //    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCashCreditSwipe() {
        this.xmlrpcRequestTransactionFailCashCreditSwipe++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionFailCashCreditCL() {
        this.xmlrpcRequestTransactionFailCashCreditCL++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionFailCashCreditCH() {
        this.xmlrpcRequestTransactionFailCashCreditCH++;
    }

    @Override
    public final void incrementXMLRPCRequestTransactionFailCashValue() {
        this.xmlrpcRequestTransactionFailCashValue++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailValue() {
        this.xmlrpcRequestTransactionFailValue++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCreditSwipe() {
        this.xmlrpcRequestTransactionFailCreditSwipe++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCreditCL() {
        this.xmlrpcRequestTransactionFailCreditCL++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCreditCH() {
        this.xmlrpcRequestTransactionFailCreditCH++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCashSmart() {
        this.xmlrpcRequestTransactionFailCashSmart++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailSmart() {
        this.xmlrpcRequestTransactionFailSmart++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailUnknown() {
        this.xmlrpcRequestTransactionFailUnknown++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailCash() {
        this.xmlrpcRequestTransactionFailCash++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailNA() {
        this.xmlrpcRequestTransactionFailNA++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailPayAndDisplay() {
        this.xmlrpcRequestTransactionFailPayAndDisplay++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailPayByPlate() {
        this.xmlrpcRequestTransactionFailPayByPlate++;
    }
    
    @Override
    public final void incrementXMLRPCRequestTransactionFailPayBySpace() {
        this.xmlrpcRequestTransactionFailPayBySpace++;
    }
    
    // BOSS Success
    
    @Override
    public final void incrementBOSSUploadSuccessLotSetting() {
        this.bossUploadSuccessLotSetting++;
    }
    
    @Override
    public final void incrementBOSSUploadSuccessFileUpload() {
        this.bossUploadSuccessFileUpload++;
    }
    
    @Override
    public final void incrementBOSSUploadSuccessPaystationQuery() {
        this.bossUploadSuccessPaystationQuery++;
    }
    
    @Override
    public final void incrementBOSSUploadSuccessOfflineTransaction() {
        this.bossUploadSuccessOfflineTransaction++;
    }
    
    @Override
    public final void incrementBOSSDownloadSuccessLotSetting() {
        this.bossDownloadSuccessLotSetting++;
    }
    
    @Override
    public final void incrementBOSSDownloadSuccessPublicKeyRequest() {
        this.bossDownloadSuccessPublicKeyRequest++;
    }
    
    @Override
    public final void incrementBOSSDownloadSuccessBadCreditCard() {
        this.bossDownloadSuccessCreditCard++;
    }
    
    @Override
    public final void incrementBOSSDownloadSuccessBadSmartCard() {
        this.bossDownloadSuccessBadSmartCard++;
    }
    
    // BOSS Fail
    
    @Override
    public final void incrementBOSSUploadFailLotSetting() {
        this.bossUploadFailLotSetting++;
    }
    
    @Override
    public final void incrementBOSSUploadFailFileUpload() {
        this.bossUploadFailFileUpload++;
    }
    
    @Override
    public final void incrementBOSSUploadFailPaystationQuery() {
        this.bossUploadFailPaystationQuery++;
    }
    
    @Override
    public final void incrementBOSSUploadFailOfflineTransaction() {
        this.bossUploadFailOfflineTransaction++;
    }
    
    @Override
    public final void incrementBOSSDownloadFailLotSetting() {
        this.bossDownloadFailLotSetting++;
    }
    
    @Override
    public final void incrementBOSSDownloadFailPublicKeyRequest() {
        this.bossDownloadFailPublicKeyRequest++;
    }
    
    @Override
    public final void incrementBOSSDownloadFailBadCreditCard() {
        this.bossDownloadFailCreditCard++;
    }
    
    @Override
    public final void incrementBOSSDownloadFailBadSmartCard() {
        this.bossDownloadFailBadSmartCard++;
    }
    
    @Override
    public final void incrementBOSSUploadSuccessTotal() {
        this.bossUploadSuccessTotal++;
    }
    
    @Override
    public final void incrementBOSSDownloadSuccessTotal() {
        this.bossDownloadSuccessTotal++;
    }
    
    @Override
    public final void incrementBOSSUploadFailTotal() {
        this.bossUploadFailTotal++;
    }
    
    @Override
    public final void incrementBOSSDownloadFailTotal() {
        this.bossDownloadFailTotal++;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void incrementQueueExhaustingStoreAndForward() {
        this.cardProcessingQueueExhaustingStoreAndForward.incrementAndGet();
    }
}
