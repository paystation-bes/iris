package com.digitalpaytech.service.kpi.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.XMLErrorException;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.util.KPIConstants;

/**
 * 
 * @author Amanda Chong
 *
 */

@Component("kpiListenerService")
public class KPIListenerServiceImpl implements KPIListenerService, InvocationHandler, ApplicationContextAware {
    
    private KPIService kpiService;
    
    public KPIService getKPIService() {
        return kpiService;
    }
    
    private Object kpiDisabled(Object proxy, Method method, Object[] args) {
        Object result = null;
        try {
            result = method.invoke(proxy, args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Exception {
        Object result = null;
        if (kpiService == null) {
            kpiDisabled(proxy, method, args);
        }
        try {
            long startTime = System.currentTimeMillis();
            result = method.invoke(proxy, args);
            long responseTime = System.currentTimeMillis() - startTime;
            
            gatherData(proxy.getClass().getSimpleName(), responseTime, method.getName());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.getCause().printStackTrace();
            e.printStackTrace();
            throw (Exception) e.getCause();
        }
        return result;
    }
    
    @Override
    public Object invokeProcessPreAuth(Object proxy, Method method, Object[] args) throws CryptoException, CardTransactionException {
        Object result = null;
        if (kpiService == null) {
            kpiDisabled(proxy, method, args);
        }
        try {
            long startTime = System.currentTimeMillis();
            result = method.invoke(proxy, args);
            long responseTime = System.currentTimeMillis() - startTime;
            
            gatherData(proxy.getClass().getSimpleName(), responseTime, method.getName());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            if (e.getCause().getClass().equals(CryptoException.class)) {
                throw (CryptoException) e.getCause();
            } else if (e.getCause().getClass().equals(CardTransactionException.class)) {
                throw (CardTransactionException) e.getCause();
            } else if (e.getCause().getClass().equals(XMLErrorException.class)) {
                throw (XMLErrorException) e.getCause();
            } else if (e.getCause() instanceof RuntimeException 
                    && e.getCause().getCause() != null 
                    && e.getCause().getCause() instanceof SocketTimeoutException) {
                throw new XMLErrorException(e.getCause().getCause());
            } else {
                e.getCause().printStackTrace();
                e.printStackTrace();
            }
        }
        return result;
    }
    
    private void gatherData(String proxyString, long responseTime, String methodName) {
        if (proxyString.equals(KPIConstants.CONCORD)) {
            kpiService.incrementCardProcessingRequestConcord(responseTime);
        } else if (proxyString.equals(KPIConstants.ALLIANCE)) {
            kpiService.incrementCardProcessingRequestAlliance(responseTime);
        } else if (proxyString.equals(KPIConstants.AUTHORIZENET)) {
            kpiService.incrementCardProcessingRequestAuthorizeNet(responseTime);
        } else if (proxyString.equals(KPIConstants.BLACKBOARD)) {
            kpiService.incrementCardProcessingRequestBlackBoard(responseTime);
        } else if (proxyString.equals(KPIConstants.DATAWIRE)) {
            kpiService.incrementCardProcessingRequestDatawire(responseTime);
        } else if (proxyString.equals(KPIConstants.FIRSTDATANASHVILLE)) {
            kpiService.incrementCardProcessingRequestFirstDataNashville(responseTime);
        } else if (proxyString.equals(KPIConstants.FIRSTHORIZON)) {
            kpiService.incrementCardProcessingRequestFirstHorizon(responseTime);
        } else if (proxyString.equals(KPIConstants.HEARTLAND)) {
            kpiService.incrementCardProcessingRequestHeartland(responseTime);
        } else if (proxyString.equals(KPIConstants.HEARTLAND_SPPLUS)) {
            kpiService.incrementCardProcessingRequestHeartlandSPPlus(responseTime);
        } else if (proxyString.equals(KPIConstants.LINK2GOV)) {
            kpiService.incrementCardProcessingRequestLink2Gov(responseTime);
        } else if (proxyString.equals(KPIConstants.MONERIS)) {
            kpiService.incrementCardProcessingRequestMoneris(responseTime);
        } else if (proxyString.equals(KPIConstants.NUVISION)) {
            kpiService.incrementCardProcessingRequestNuVision(responseTime);
        } else if (proxyString.equals(KPIConstants.PARADATA)) {
            kpiService.incrementCardProcessingRequestParadata(responseTime);
        } else if (proxyString.equals(KPIConstants.PAYMENTECH)) {
            kpiService.incrementCardProcessingRequestPaymentech(responseTime);
        } else if (proxyString.equals(KPIConstants.TOTALCARD)) {
            kpiService.incrementCardProcessingRequestTotalCard(responseTime);
        } else if (proxyString.equals(KPIConstants.CBORDGOLD)) {
        	kpiService.incrementCardProcessingRequestCBordGold(responseTime);
        } else if (proxyString.equals(KPIConstants.CBORDODYSSEY)) {
        	kpiService.incrementCardProcessingRequestCBordOdyssey(responseTime);
        } else if (proxyString.equals(KPIConstants.ELAVON_CONVERGE)) {
            kpiService.incrementCardProcessingRequestElavonConverge(responseTime);
        } else if (proxyString.equals(KPIConstants.ELAVON_VIACONEX)) {
            kpiService.incrementCardProcessingRequestElavonViaConex(responseTime);
        } else if (proxyString.equals(KPIConstants.TD_MERCHANT)) {
            kpiService.incrementCardProcessingRequestTDMerchantServices(responseTime);
        } else if (proxyString.equals(KPIConstants.CREDITCALL)) {
            kpiService.incrementCardProcessingRequestCreditcall(responseTime);
        } else if (proxyString.equals(KPIConstants.SMSPERMITALERTJOBSERVICEIMPL)) {
            if (methodName.equals(KPIConstants.SENDSMS)) {
                kpiService.incrementBackgroundProcessesSMSAlertSent();
            } else if (methodName.equals(KPIConstants.SENDCONFIRMATIONSMS)) {
                kpiService.incrementBackgroundProcessesSMSAlertReceive();
            }
        } else if (proxyString.equals(KPIConstants.EMAILNOTIFICATIONTHREAD)) {
            kpiService.addBackgroundProcessesEmailAlertResponseTime(responseTime);
        }
    }
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        try {
            kpiService = applicationContext.getBean(KPIService.class);
            System.out.println();
        } catch (NoSuchBeanDefinitionException e) {
            kpiService = null;
        }
    }
}
