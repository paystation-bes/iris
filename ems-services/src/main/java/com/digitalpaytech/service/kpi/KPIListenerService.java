package com.digitalpaytech.service.kpi;

import java.lang.reflect.Method;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CryptoException;

public interface KPIListenerService {
    
    Object invoke(Object proxy, Method method, Object[] args) throws Exception;
    
    Object invokeProcessPreAuth(Object proxy, Method method, Object[] args) throws CryptoException, CardTransactionException;
    
    KPIService getKPIService();
}
