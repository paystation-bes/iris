package com.digitalpaytech.service.kpi;

import java.util.Map;
import java.util.concurrent.Future;

public interface KPIService {
    
    void incrementUIFileUploadSuccessCoupon();
    
    void incrementUIFileUploadSuccessPasscard();
    
    void incrementUIFileUploadSuccessBannedCard();
    
    void incrementUIFileUploadFailCoupon();
    
    void incrementUIFileUploadFailPasscard();
    
    void incrementUIFileUploadFailBannedCard();
    
    void addDigitalAPIResponseTimeAuditInfo(long responseTime);
    
    void addDigitalAPIResponseTimePaystationInfo(long responseTime);
    
    void addDigitalAPIResponseTimePlateInfo(long responseTime);
    
    void addDigitalAPIResponseTimeStallInfo(long responseTime);
    
    void addDigitalAPIResponseTimeTransactionInfo(long responseTime);
    
    void incrementDigitalAPIRequestSuccessAuditInfo();
    
    void incrementDigitalAPIRequestSuccessPaystationInfo();
    
    void incrementDigitalAPIRequestSuccessPlateInfo();
    
    void incrementDigitalAPIRequestSuccessStallInfo();
    
    void incrementDigitalAPIRequestSuccessTransactionInfo();
    
    void incrementDigitalAPIRequestFailAuditInfo();
    
    void incrementDigitalAPIRequestFailPaystationInfo();
    
    void incrementDigitalAPIRequestFailPlateInfo();
    
    void incrementDigitalAPIRequestFailStallInfo();
    
    void incrementDigitalAPIRequestFailTransactionInfo();
    
    Map<String, Object> getKPIInfo();
    
    void incrementUIFileDownloadSuccessCoupon();
    
    void incrementUIFileDownloadSuccessPasscard();
    
    void incrementUIFileDownloadFailCoupon();
    
    void incrementUIFileDownloadFailPasscard();
    
    void incrementUIFileDownloadSuccessReport();
    
    void incrementUIFileDownloadFailReport();
    
    void incrementUIFileDownloadSuccessWidget();
    
    void incrementUIFileDownloadFailWidget();
    
    void incrementXMLRPCRequestSuccessAudit();
    
    void incrementXMLRPCRequestSuccessTransaction();
    
    void incrementXMLRPCRequestSuccessAuthorizeRequest();
    
    void incrementXMLRPCRequestSuccessEvent();
    
    void incrementXMLRPCRequestSuccessHeartBeat();
    
    void incrementXMLRPCRequestSuccessLotSettingSuccess();
    
    void incrementXMLRPCRequestSuccessPlateRptReq();
    
    void incrementXMLRPCRequestSuccessAuthorizeCardRequest();
    
    void incrementXMLRPCRequestSuccessStallInfoRequest();
    
    void incrementXMLRPCRequestSuccessStallReportRequest();
    
    void incrementXMLRPCRequestFailAudit();
    
    void incrementXMLRPCRequestFailTransaction();
    
    void incrementXMLRPCRequestFailAuthorizeRequest();
    
    void incrementXMLRPCRequestFailEvent();
    
    void incrementXMLRPCRequestFailHeartBeat();
    
    void incrementXMLRPCRequestFailLotSettingSuccess();
    
    void incrementXMLRPCRequestFailPlateRptReq();
    
    void incrementXMLRPCRequestFailAuthorizeCardRequest();
    
    void incrementXMLRPCRequestFailStallInfoRequest();
    
    void incrementXMLRPCRequestFailStallReportRequest();
    
    // Pay Station REST
    void incrementPaystationRestFailCancelTransaction();
    
    void incrementPaystationRestFailHeartBeat();
    
    void incrementPaystationRestFailToken();
    
    void incrementPaystationRestFailTransaction();
    
    void incrementPaystationRestSuccessCancelTransaction();
    
    void incrementPaystationRestSuccessHeartBeat();
    
    void incrementPaystationRestSuccessToken();
    
    void incrementPaystationRestSuccessTransaction();
    
    void incrementPaystationRestTransactionFailPaymentCash();
    
    void incrementPaystationRestTransactionFailPaymentCashSmart();
    
    void incrementPaystationRestTransactionFailPaymentCashValue();
    
    void incrementPaystationRestTransactionFailPaymentCashCredit();
    
    void incrementPaystationRestTransactionFailPaymentCredit();
    
    void incrementPaystationRestTransactionFailPaymentSmart();
    
    void incrementPaystationRestTransactionFailPaymentUnknown();
    
    void incrementPaystationRestTransactionFailPaymentValue();
    
    void incrementPaystationRestTransactionFailPermitNA();
    
    void incrementPaystationRestTransactionFailPermitPayAndDisplay();
    
    void incrementPaystationRestTransactionFailPermitPayByPlate();
    
    void incrementPaystationRestTransactionFailPermitPayBySpace();
    
    void incrementPaystationRestTransactionFailStoreAndForward();
    
    void incrementPaystationRestTransactionSuccessPaymentCash();
    
    void incrementPaystationRestTransactionSuccessPaymentCashSmart();
    
    void incrementPaystationRestTransactionSuccessPaymentCashValue();
    
    void incrementPaystationRestTransactionSuccessPaymentCashCredit();
    
    void incrementPaystationRestTransactionSuccessPaymentCredit();
    
    void incrementPaystationRestTransactionSuccessPaymentSmart();
    
    void incrementPaystationRestTransactionSuccessPaymentUnknown();
    
    void incrementPaystationRestTransactionSuccessPaymentValue();
    
    void incrementPaystationRestTransactionSuccessPermitNA();
    
    void incrementPaystationRestTransactionSuccessPermitPayAndDisplay();
    
    void incrementPaystationRestTransactionSuccessPermitPayByPlate();
    
    void incrementPaystationRestTransactionSuccessPermitPayBySpace();
    
    void incrementPaystationRestTransactionSuccessStoreAndForward();
    
    void incrementPaystationRestFailParameterError();
    
    void incrementPaystationRestFailSignatureError();
    
    void incrementPaystationRestFailSystemError();
    
    void addUIDashboardResponseTimeAlerts(long responseTime);
    
    void addUIDashboardResponseTimeRevenue(long responseTime);
    
    void addUIDashboardResponseTimeCollections(long responseTime);
    
    void addUIDashboardResponseTimeSettledCard(long responseTime);
    
    void addUIDashboardResponseTimePurchases(long responseTime);
    
    void addUIDashboardResponseTimePaidOccupancy(long responseTime);
    
    void addUIDashboardResponseTimeTurnover(long responseTime);
    
    void addUIDashboardResponseTimeUtilization(long responseTime);
    
    void addUIDashboardResponseTimeMap(long responseTime);
    
    void incrementUIDashboardQuerySuccess();
    
    void incrementUIDashboardQueryFail();
    
    void incrementCardProcessingRequestConcord(long responseTime);
    
    void incrementCardProcessingRequestAlliance(long responseTime);
    
    void incrementCardProcessingRequestAuthorizeNet(long responseTime);
    
    void incrementCardProcessingRequestBlackBoard(long responseTime);
    
    void incrementCardProcessingRequestDatawire(long responseTime);
    
    void incrementCardProcessingRequestFirstDataNashville(long responseTime);
    
    void incrementCardProcessingRequestFirstHorizon(long responseTime);
    
    void incrementCardProcessingRequestHeartland(long responseTime);
    
    void incrementCardProcessingRequestHeartlandSPPlus(long responseTime);
    
    void incrementCardProcessingRequestLink2Gov(long responseTime);
    
    void incrementCardProcessingRequestMoneris(long responseTime);
    
    void incrementCardProcessingRequestNuVision(long responseTime);
    
    void incrementCardProcessingRequestParadata(long responseTime);
    
    void incrementCardProcessingRequestPaymentech(long responseTime);
    
    void incrementCardProcessingRequestTotalCard(long responseTime);
    
    void incrementCardProcessingRequestCBordGold(long responseTime);
    
    void incrementCardProcessingRequestCBordOdyssey(long responseTime);
    
    void incrementCardProcessingRequestElavonConverge(final long responseTime);
    
    void incrementCardProcessingRequestElavonViaConex(final long responseTime);
    
    void incrementCardProcessingRequestTDMerchantServices(final long responseTime);
    
    void incrementCardProcessingRequestCreditcall(final long responseTime);
    
    void incrementUIRequestInformation();
    
    void incrementUIRequestSuccess();
    
    void incrementUIRequestRedirect();
    
    void incrementUIRequestClientError();
    
    void incrementUIRequestServerError();
    
    void incrementBackgroundProcessesSMSAlertSent();
    
    void incrementBackgroundProcessesSMSAlertReceive();
    
    void addBackgroundProcessesEmailAlertResponseTime(long responseTime);
    
    void addUIFileSize(long size);
    
    String getValue(String key);
    
    void incrementUncaughtExceptionInformation();
    
    void incrementUncaughtExceptionSuccess();
    
    void incrementUncaughtExceptionRedirect();
    
    void incrementUncaughtExceptionClientError();
    
    void incrementUncaughtExceptionServerError();
    
    void update();
    
    void updateDaily();
    
    void incrementDigitalAPIRequestCouponSuccess(long responseTime, int fileSize);
    
    void incrementDigitalAPIRequestCouponFail(long responseTime, int fileSize);
    
    void incrementXMLRPCRequestTransactionSuccessStoreAndForward();
    
    void incrementXMLRPCRequestTransactionSuccessCashCreditSwipe();
    
    void incrementXMLRPCRequestTransactionSuccessCashCreditCL();
    
    void incrementXMLRPCRequestTransactionSuccessCashCreditCH();
    
    void incrementXMLRPCRequestTransactionSuccessCashValue();
    
    void incrementXMLRPCRequestTransactionSuccessValue();
    
    void incrementXMLRPCRequestTransactionSuccessCreditSwipe();
    
    void incrementXMLRPCRequestTransactionSuccessCreditCL();
    
    void incrementXMLRPCRequestTransactionSuccessCreditCH();
    
    void incrementXMLRPCRequestTransactionSuccessCashSmart();
    
    void incrementXMLRPCRequestTransactionSuccessSmart();
    
    void incrementXMLRPCRequestTransactionSuccessUnknown();
    
    void incrementXMLRPCRequestTransactionSuccessCash();
    
    void incrementXMLRPCRequestTransactionSuccessNA();
    
    void incrementXMLRPCRequestTransactionSuccessPayAndDisplay();
    
    void incrementXMLRPCRequestTransactionSuccessPayByPlate();
    
    void incrementXMLRPCRequestTransactionSuccessPayBySpace();
    
    //    public void incrementXMLRPCRequestTransactionFailStoreAndForward();
    
    void incrementXMLRPCRequestTransactionFailCashCreditSwipe();
    
    void incrementXMLRPCRequestTransactionFailCashCreditCL();
    
    void incrementXMLRPCRequestTransactionFailCashCreditCH();
    
    void incrementXMLRPCRequestTransactionFailCashValue();
    
    void incrementXMLRPCRequestTransactionFailValue();
    
    void incrementXMLRPCRequestTransactionFailCreditSwipe();
    
    void incrementXMLRPCRequestTransactionFailCreditCL();
    
    void incrementXMLRPCRequestTransactionFailCreditCH();
    
    void incrementXMLRPCRequestTransactionFailCashSmart();
    
    void incrementXMLRPCRequestTransactionFailSmart();
    
    void incrementXMLRPCRequestTransactionFailUnknown();
    
    void incrementXMLRPCRequestTransactionFailCash();
    
    void incrementXMLRPCRequestTransactionFailNA();
    
    void incrementXMLRPCRequestTransactionFailPayAndDisplay();
    
    void incrementXMLRPCRequestTransactionFailPayByPlate();
    
    void incrementXMLRPCRequestTransactionFailPayBySpace();
    
    void incrementBOSSUploadSuccessLotSetting();
    
    void incrementBOSSUploadSuccessFileUpload();
    
    void incrementBOSSUploadSuccessPaystationQuery();
    
    void incrementBOSSUploadSuccessOfflineTransaction();
    
    void incrementBOSSDownloadSuccessLotSetting();
    
    void incrementBOSSDownloadSuccessPublicKeyRequest();
    
    void incrementBOSSDownloadSuccessBadCreditCard();
    
    void incrementBOSSDownloadSuccessBadSmartCard();
    
    void incrementBOSSUploadFailLotSetting();
    
    void incrementBOSSUploadFailFileUpload();
    
    void incrementBOSSUploadFailPaystationQuery();
    
    void incrementBOSSUploadFailOfflineTransaction();
    
    void incrementBOSSDownloadFailLotSetting();
    
    void incrementBOSSDownloadFailPublicKeyRequest();
    
    void incrementBOSSDownloadFailBadCreditCard();
    
    void incrementBOSSDownloadFailBadSmartCard();
    
    void incrementBOSSUploadSuccessTotal();
    
    void incrementBOSSDownloadSuccessTotal();
    
    void incrementBOSSUploadFailTotal();
    
    void incrementBOSSDownloadFailTotal();
    
    void incrementQueueExhaustingStoreAndForward();
    
    Future<Void> runSQL();
    
    void incrementUIRequest449();
    
    void incrementUIRequest480();
    
    void incrementUIRequest481();
    
    void incrementUIRequest482();
    
    void incrementUIRequest483();
    
    void incrementUIRequest509();
    
    void addDigitalAPICouponRecord(int records);

}
