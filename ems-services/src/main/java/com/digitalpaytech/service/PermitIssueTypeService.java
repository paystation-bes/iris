package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.PermitIssueType;

public interface PermitIssueTypeService {
    PermitIssueType findPermitIssueType(String licensePlate, String stallNumber);
    
    PermitIssueType findPermitIssueTypeById(Integer permitIssueTypeId);
    
    List<PermitIssueType> findAll();
    
}
