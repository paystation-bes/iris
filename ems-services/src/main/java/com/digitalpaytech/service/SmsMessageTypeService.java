package com.digitalpaytech.service;

import com.digitalpaytech.domain.SmsMessageType;

public interface SmsMessageTypeService {
    public SmsMessageType findSmsMessageType(Integer id);    
}
