package com.digitalpaytech.service;

import com.digitalpaytech.domain.SigningLog;

public interface SigningLogService {

    public void recordSignatureData(SigningLog signingLog);

}
