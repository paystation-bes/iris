package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.PosDate;

public interface PosDateService {

    public List<PosDate> findPosDateByPointOfSale(int pointOfSaleId, String sortOrder, String sortItem, Integer page);
    
}
