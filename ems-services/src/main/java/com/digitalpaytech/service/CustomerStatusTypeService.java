package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CustomerStatusType;

public interface CustomerStatusTypeService {
    List<CustomerStatusType> loadAll();
    
    Map<Integer, CustomerStatusType> getCustomerStatusTypesMap();
    
    String getText(int customerStatusTypeId);
    
    CustomerStatusType findCustomerStatusType(int customerStatusTypeId);
    
    CustomerStatusType findCustomerStatusType(String name);
}
