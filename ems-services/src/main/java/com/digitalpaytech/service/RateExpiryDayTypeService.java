package com.digitalpaytech.service;

import com.digitalpaytech.domain.RateExpiryDayType;

public interface RateExpiryDayTypeService {
    
    RateExpiryDayType findRateExpiryDayTypeById(Byte rateExpiryDayTypeId);
    
}
