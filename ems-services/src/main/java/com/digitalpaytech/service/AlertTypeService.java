package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.EventSeverityType;

public interface AlertTypeService {
    
    Collection<AlertType> findAlertTypeByIsUserDefined(boolean isUserDefined);
    
    Collection<AlertClassType> findAllAlertClassTypes();
    
    EventSeverityType findEventSeverityTypeById(int eventSeverityTypeId);
    
    List<AlertType> findAllAlertTypes();
    
    AlertThresholdType findAlertThresholdTypeById(int id);
    
    List<String> findAlertThresholdTypeText(Collection<Integer> ids);
}
