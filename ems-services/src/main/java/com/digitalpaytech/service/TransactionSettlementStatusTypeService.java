package com.digitalpaytech.service;

import com.digitalpaytech.domain.TransactionSettlementStatusType;

public interface TransactionSettlementStatusTypeService {
    public TransactionSettlementStatusType findById(Integer id);
}
