package com.digitalpaytech.service;

import com.digitalpaytech.domain.MobileNumber;

public interface MobileNumberService {
    public MobileNumber findMobileNumber(String number);
    
    public MobileNumber findMobileNumberById(Integer id);
}
