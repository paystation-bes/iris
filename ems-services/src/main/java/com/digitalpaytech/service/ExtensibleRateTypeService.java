package com.digitalpaytech.service;

import com.digitalpaytech.domain.ExtensibleRateType;

import java.util.Map;

public interface ExtensibleRateTypeService {
    public Map<String, ExtensibleRateType> loadAll();
    public Map<String, ExtensibleRateType> reloadAll();
}
