package com.digitalpaytech.service;

import com.digitalpaytech.domain.MerchantAccountMigrationStatus;

public interface MerchantAccountMigrationStatusService {
    
    MerchantAccountMigrationStatus find(int statusId);
}
