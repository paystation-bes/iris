package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.util.RandomKeyMapping;

public interface CreditCardTypeService {
    List<CreditCardType> loadAll();
    
    Map<Integer, CreditCardType> getCreditCardTypesMap();
    
    Map<String, CreditCardType> getCreditCardTypeLowerNamesMap();
    
    CreditCardType getCreditCardTypeByName(String name);
    
    CreditCardType getCreditCardTypeByName(String name, boolean defaultCreditCard);
    
    CreditCardType getCreditCardTypeById(Integer id);
    
    String getText(int creditCardId);
    
    List<FilterDTO> getFilters(List<FilterDTO> result, RandomKeyMapping keyMapping, boolean showOnlyValid);
}
