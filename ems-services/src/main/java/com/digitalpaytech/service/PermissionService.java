package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Permission;

public interface PermissionService {
    
    public Collection<RolePermissionInfo> findAllPermissionsByRoleId(Integer roleId);
    
    public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId);
    
    public Collection<Permission> findRolePermissionsByRoleId(Integer roleId);
    
    public List<Permission> findAllPermissions();
}
