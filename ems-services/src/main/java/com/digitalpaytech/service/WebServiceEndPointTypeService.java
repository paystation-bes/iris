package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.WebServiceEndPointType;

public interface WebServiceEndPointTypeService {
    List<WebServiceEndPointType> loadAll();
    
    List<WebServiceEndPointType> loadAllPrivate();
    
    Map<Byte, WebServiceEndPointType> getWebServiceEndPointTypesMap();
    
    String getText(byte webServiceEndPointTypeId);
    
    WebServiceEndPointType findWebServiceEndPointType(String name);
}
