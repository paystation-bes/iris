package com.digitalpaytech.service;

import com.digitalpaytech.domain.ParkingPermissionType;

import java.util.Map;

public interface ParkingPermissionTypeService {
    public Map<String, ParkingPermissionType> loadAll();
    public Map<String, ParkingPermissionType> reloadAll();
}
