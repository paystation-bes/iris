package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.dto.customeradmin.ConsumerSearchCriteria;

public interface ConsumerService {
    
    Collection<Consumer> findConsumerByCustomerId(int customerId, Integer pageNo, Integer itermsPerPage, String column, String order, Long currentTime);
    
    Consumer findConsumerById(int id);
    
    boolean deleteConsumer(Consumer consumer);
    
    void updateConsumer(Consumer consumer, String email, Collection<Integer> couponIds, Collection<Integer> cardIds);
    
    void createConsumer(Consumer consumer);
    
    Collection<Consumer> findConsumerByCustomerIdAndFilterValue(int customerId, String filterValue, Integer pageNo, Integer itermsPerPage,
        String column, String order, Long currentTime);
    
    List<Consumer> findConsumerByCriteria(ConsumerSearchCriteria criteria);
    
    int findRecordPage(Integer consumerId, ConsumerSearchCriteria criteria);
    
    Date findMaxLastModifiedGmtByCustomerId(int customerId);
    
    List<Consumer> findOtherConsumerByCustomerIdAndEmailId(int consumerId, int customerId, int emailAddressId);
    
    List<EmailAddress> findEmailAddressByEmail(String email);
    
    void saveConsumer(Consumer consumer, Collection<Integer> couponIds, Collection<Integer> customerCardIds);
    
    void saveConsumer(Consumer consumer);
    
    List<Consumer> findConsumersByFirstLastNameOrEmail(int customerId, String firstLastNameOrEmail);
}
