package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CustomerCardSearchCriteria;

public interface CustomerCardService {
    
    CustomerCard getCustomerCard(int customerCardId);
    
    CustomerCard findCustomerCardByCustomerIdAndCardNumber(int customerId, String cardNumber);
    
    List<CustomerCard> findCustomerCardByConsumerId(int consumerId);
    
    int countByCustomerId(Integer customerId);
    
    void createCustomerCard(CustomerCard customerCard);
    
    void updateCustomerCard(CustomerCard customerCard);
    
    void deleteCustomerCard(CustomerCard customerCard);
    
    Future<Integer> deleteCustomerCardsByCustomerIdAsync(int customerId, String statusKey);
    
    int deleteCustomerCardsByCustomerId(int customerId, String statusKey) throws BatchProcessingException;
    
    int deleteCustomerCardsPaginated(Integer customerId, Date maxLastUpdate, Map<String, Object> context);
    
    int deleteCustomerCardsByCustomerIdAndCardNumber(int customerId, String cardNumber);
    
    int findTotalCustomerCardsByCustomerId(int customerId);
    
    int findTotalCustomerCardsByCustomerIdAndCardNumber(int customerId, String cardNumber);
    
    Future<Integer> processBatchAddCustomerCardsAsync(String statusKey, int customerId
        , Collection<CustomerCard> customerCards, String mode
        , int userId) throws BatchProcessingException;
    
    int processBatchAddCustomerCardsWithStatus(String statusKey, int customerId
        , Collection<CustomerCard> customerCards, String mode
        , int userId) throws BatchProcessingException;
    
    int processBatchAddCustomerCards(int customerId, Collection<CustomerCard> customerCards, String mode
        , int userId, BatchProcessStatus status) throws BatchProcessingException;
    
    int findRecordPage(Integer customerCardId, CustomerCardSearchCriteria criteria);
    
    List<CustomerCard> findCustomerCard(CustomerCardSearchCriteria criteria);
    
    List<CustomerCard> findCustomerCardByCustomerId(int customerId);
    
    List<CustomerCard> findByCustomerIdAndCardIds(Integer customerId, Collection<Integer> cardIds);
    
    int countByCustomerCardType(Collection<Integer> customerCardTypeIds);
}
