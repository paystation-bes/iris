package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.PosEventCurrent;

public interface PosEventCurrentService {
    
    PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeId(Integer pointOfSaleId, byte eventTypeId);
    
    PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(Integer posId, byte eventTypeId,
        Integer customerAlertTypeId);
    
    void savePosEventCurrent(PosEventCurrent posEventCurrent);
    
    void updatePosEventCurrent(PosEventCurrent posEventCurrent);
    
    List<PosEventCurrent> findActivePosEventCurrentByTimestamp(Date timestamp);
    
    void deletePosEventCurrent(Integer posId, byte eventTypeId, Integer customerAlertTypeId);
    
    void delete(PosEventCurrent posEventCurrent);
    
    Collection<PosEventCurrent> findNextRecoveryDuplicateSerialNumberData(final int numOfRows);
    
    List<PosEventCurrent> findActivePosEventCurrentByCustomerId(Integer customerId);
    
    List<PosEventCurrent> findMaintenanceCentreDetailByPointOfSaleId(Integer pointOfSaleId);
    
    List<PosEventCurrent> findCollectionCentreDetailByPointOfSaleId(Integer pointOfSaleId);
    
    List<PosEventCurrent> findActiveDefaultPosEventCurrentByPosIdListAndAttIdList(List<Integer> pointOfSaleIdList,
        List<Integer> alertThresholdTypeIdList);
    
    PosEventCurrent findActivePosEventCurrentById(Long posEventCurrentId);
    
    PosEventCurrent findActivePosEventCurrentByEventTypeIdAndPointOfSaleId(byte eventTypeId, Integer pointOfSaleId);
    
    List<PosEventCurrent> findActivePosEventCurrentByPointOfSaleId(Integer posId);
    
    List<PosEventCurrent> findAllPosEventCurrentByPointOfSaleId(Integer posId);
    
    List<Long> findDefaultPosEventCurrentsToDisable(List<Integer> pointOfSaleIdList);
    
    List<Long> findDefaultPosEventCurrentsToEnable(List<Integer> pointOfSaleIdList);
    
    List<PosEventCurrent> findPosEventCurrentsByIdList(List<Long> pecIdList);
}
