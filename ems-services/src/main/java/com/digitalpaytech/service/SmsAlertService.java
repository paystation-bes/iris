package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.SmsAlert;

public interface SmsAlertService {
    List<SmsAlert> findByAddTimeNumCustomerIdAndSpaceNumber(Integer customerId, int spaceNumber, int addTimeNumber);
    
    List<SmsAlert> findByAddTimeNumCustomerIdPaystationSettingNameAndSpaceNumber(Integer customerId, String paystationSettingName,
        int spaceNumber, int addTimeNumber);
    
    List<SmsAlert> findByAddTimeNumLocationIdAndSpaceNumber(Integer locationId, int spaceNumber, int addTimeNumber);
    
    List<SmsAlert> findByLicencePlate(Integer customerId, String licencePlate);
    
    List<SmsAlert> findByCustomerIdPaystationSettingNameAndSpaceNumber(Integer customerId, String paystationSettingName, int spaceNumber);
    
    List<SmsAlert> findByCustomerIdAndSpaceNumber(Integer customerId, int spaceNumber);
    
    List<SmsAlert> findByLocationIdAndSpaceNumber(Integer locationId, int spaceNumber);
    
    SmsAlert findByMobileNumber(String mobileNumber);
    
    SmsAlert findSmsAlertById(Integer id);
    
    int getSmsAlertNotAlerted();
    
    SmsAlert findUnexpiredSmsAlertById(Integer id);
}
