package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.dto.TransactionSearchCriteria;
import com.digitalpaytech.exception.ApplicationException;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public interface ProcessorTransactionService {
    
    ProcessorTransaction findProcessorTransactionById(Long id, boolean isEvict);
    
    ProcessorTransaction findByPointOfSaleIdPurchasedDateTicketNumber(int pointOfSaleId, Date purchasedDate, int ticketNumber);
    
    Set<ProcessorTransaction> findRefundablePtdsByCard(short last4DigitsofCardNumber, int cardChecksum, String cardHash, String cardType,
        Date startDate, Date endDate, int merchantAccountId, Collection<Integer> psList);
    
    Set<ProcessorTransaction> findRefundablePtdsByCardAndAuthNumber(String authNumber, short last4DigitsofCardNumber, int cardChecksum,
        String cardHash, String cardType, Date startDate, Date endDate, int merchantAccountId, Collection<Integer> psList);
    
    int updateProcessorTransactionType(Long id, int typeId, int newTypeId);
    
    int markRecoverable(ProcessorTransaction pt);
    
    ProcessorTransaction getApprovedTransaction(int pointOfSaleId, Date purchasedDate, int ticketNumber);
    
    ProcessorTransactionType getProcessorTransactionType(Integer processorTransactionTypeId);
    
    void updateProcessorTransactionToBatchedSettled(ProcessorTransaction ptd);
    
    void updateProcessorTransactionToSettled(ProcessorTransaction ptd);
    
    Collection<ProcessorTransaction> findProcessorTransactionByPurchaseId(Long purchaseId);
    
    Collection<ProcessorTransaction> findProcessorTransactionByPurchaseIdAndIsApprovedAndIsRefund(Long purchaseId);
    
    Collection<ProcessorTransaction> findRefundedProcessorTransaction(Long purchaseId);
    
    ProcessorTransaction findRefundProcessorTransaction(Integer pointOfSaleId, Date purchasedDate, Integer ticketNumber);
    
    ProcessorTransaction findPartialSettlementProcessorTransaction(Integer pointOfSaleId, Date purchasedDate, Integer ticketNumber, boolean isEvict)
        throws ApplicationException;
    
    List<ProcessorTransaction> findRefundableTransaction(TransactionSearchCriteria criteria);
    
    boolean isRefundableTransaction(ProcessorTransaction ptd);
    
    ProcessorTransaction findTransactionPosPurchaseById(Long transactionId);
    
    void removePreAuthsInProcessorTransactions(List<Long> preAuthIds);
    
    void updateProcessorTransaction(ProcessorTransaction ptd);
    
    void removePreAuthInProcessorTransactions(Long preAuthIds);
    
    void saveProcessorTransaction(ProcessorTransaction ptd);
    
    ProcessorTransaction findProcTransByPurchaseIdTicketNoAndPurchasedDate(Long purchaseId, Date purchasedDate, Integer ticketNumber);
    
    boolean isIncomplete(ProcessorTransaction processorTransaction);
    
    boolean isIncomplete(MerchantAccount ma, ProcessorTransaction pt);
    
    Collection<ProcessorTransaction> findByMercAccIdAndProcTransTypeIdAndHasPurchase(Integer merchantAccountId, Integer transactionTypeId);
    
}
