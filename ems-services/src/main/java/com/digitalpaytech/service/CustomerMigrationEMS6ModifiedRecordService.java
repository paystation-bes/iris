package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;

/**
 * This service interface defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
public interface CustomerMigrationEMS6ModifiedRecordService {
    
    public void saveCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord);
    public void updateCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord);

    public void deleteCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord);

    public List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerId(Integer customerId);
    public List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerIdAndTableType(Integer customerId, String tableName);
    
    public CustomerMigrationEMS6ModifiedRecord findModifiedRecordIfExists(Integer customerId, Integer tableId, String tableName, String columnName);
}
