package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationStatusType;

/**
 * This service interface defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
public interface CustomerMigrationValidationStatusService {
    
    List<CustomerMigrationValidationStatus> findSystemWideValidations();
    
    List<CustomerMigrationValidationStatus> findUserFacingValidationsByCustomerId(Integer customerId);
    
    List<CustomerMigrationValidationStatus> findInMigrationValidationsByCustomerId(Integer customerId, boolean isDuringMigration);
    
    CustomerMigrationValidationStatusType runValidations(Integer customerId, boolean includeSystemWide, boolean isDuringMigration, boolean beforeStart);
}
