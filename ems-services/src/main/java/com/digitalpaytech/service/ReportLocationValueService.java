package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ReportLocationValue;

public interface ReportLocationValueService {
    
    public List<ReportLocationValue> findReportLocationValuesByReportDefinition(int reportDefinitionId);

    
}
