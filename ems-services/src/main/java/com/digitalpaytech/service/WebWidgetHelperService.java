package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;

public interface WebWidgetHelperService {
    
    List<Integer> getAllChildCustomerId(int parentCustomerId);
    
    String getCustomerTimeZoneByCustomerId(int customerId);
    
    WidgetData convertData(Widget widget, List<WidgetMetricInfo> rawData, UserAccount userAccount, String queryLimit);
    
    void calculateRange(Widget widget, int customerId, int rangeTypeId);
    
    void calculateRange(Widget widget, int customerId, StringBuilder query, int rangeTypeId, int tier1TypeId, boolean usePartitionedTable,
        boolean calculateWidgetRanges);
    
    void appendLabelTable(StringBuilder query, Widget widget, Map<Integer, StringBuilder> subsetMap, boolean useETLLocationDetail,
        Integer etlLocationDetailTypeId, boolean isTierTypeTime);
    
    void appendSelection(StringBuilder query, Widget widget);
    
    Map<Integer, StringBuilder> createTierSubsetMap(Widget widget);
    
    void appendGrouping(StringBuilder query, Widget widget);
    
    void appendOrdering(StringBuilder query, Widget widget);
    
    void appendLimit(StringBuilder query, Widget widget, String queryLimit);
    
    boolean appendFieldMember(StringBuilder query, Widget widget, Map<Integer, String> tierTypesMembersMap, Map<Integer, String> tierTypesAliasesMap,
        boolean isForGroupBy);
    
    void appendDataConditions(StringBuilder query, Widget widget, String customerIdField, Map<Integer, String> tierTypesFieldsMap,
        Map<Integer, StringBuilder> subsetMap);
}
