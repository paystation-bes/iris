package com.digitalpaytech.service;

import java.util.Collection;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;

public interface RawSensorDataService {
    
    public void saveRawSensorData(String serialNumber, EventData eventData);
    
    public RawSensorData getRawSensorData(Long rawSensorDataId);
    
    public void deleteRawSensorData(RawSensorData rawSensorData);
    
    public void updateRawSensorData(RawSensorData rawSensorData);
    
    public void archiveRawSensorData(RawSensorData rawSensorData);
    
    public Collection<RawSensorData> findRawSensorDataForServer(int sensorDataBatchSize);
    
    public boolean rawSensorDataExist(Long id);
    
    public int getRawSensorDataCount();
    
}
