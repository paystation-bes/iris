package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.RateType;

public interface RateTypeService {

    RateType findRateTypeById(Byte rateTypeId);

    List<RateType> findAllRateTypes();
    
    List<RateType> findAllRateTypesWithoutHoliday();
    
}
