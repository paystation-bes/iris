package com.digitalpaytech.service;

import java.util.List;
import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;

public interface TransactionDetailService {
    List<TransactionDetail> findTransactionDetailByCriteria(TransactionReceiptSearchCriteria criteria);        
}
