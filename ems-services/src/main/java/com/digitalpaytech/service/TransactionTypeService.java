package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.TransactionType;

public interface TransactionTypeService {
    public List<TransactionType> loadAll();
    public Map<Integer, TransactionType> getTransactionTypesMap();
    public String getText(int transactionId);
    public TransactionType findTransactionType(String name);
}
