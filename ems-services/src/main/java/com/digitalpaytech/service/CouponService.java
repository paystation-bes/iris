package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CouponSearchCriteria;
import com.digitalpaytech.exception.InvalidDataException;

public interface CouponService {
    
    Coupon findCouponByCustomerIdAndCouponCode(int customerId, String couponCode);
    
    Coupon findCouponWithShortestCode(int customerId, String couponCode);
    
    Coupon findDeletedCouponWithShortestCode(int customerId, String couponCode, Date minArchiveDate);
    
    Coupon findByCustomerIdCouponCodeNoRestriction(int customerId, String couponCode);
    
    Coupon findDeletedByCustomerIdCouponCodeNoRestriction(int customerId, String couponCode, Date minArchiveDate);
    
    List<Coupon> findCouponsByCustomerId(int customerId);
    
    Coupon findCouponById(int id);
    
    List<Coupon> findCouponByCriteria(CouponSearchCriteria criteria);
    
    int findRecordPage(Integer couponId, CouponSearchCriteria criteria);
    
    void saveOrUpdateCoupon(Coupon coupon);
    
    void saveCoupon(Coupon coupon);
    
    void saveOrUpdateCoupons(List<Coupon> coupons);
    
    void saveCoupons(List<Coupon> coupons);
    
    void deleteCoupon(Coupon coupon);
    
    Coupon processCouponAuthorization(PointOfSale pointOfSale, String couponCode, int stallNumber) throws InvalidDataException;
    
    void deleteAndSaveCoupons(List<Coupon> coupons) throws BatchProcessingException;
    
    int findTotalCouponSize(int customerId);
    
    Coupon modifyCoupon(Coupon coupon, Integer userId);
    
    int deleteCoupons(Integer customerId, String statusKey) throws BatchProcessingException;
    
    int deleteCouponsPaginated(Integer customerId, Date deleteTimeStamp, Map<String, Object> context) throws BatchProcessingException;
    
    Future<Integer> processBatchUpdateAsync(String statusKey, int customerId, Collection<Coupon> coupons, String mode, int userId)
        throws BatchProcessingException;
    
    int processBatchUpdateWithStatus(String statusKey, int customerId, Collection<Coupon> coupons, String mode, int userId)
        throws BatchProcessingException;
    
    int processBatchUpdate(int customerId, Collection<Coupon> coupons, String mode, int userId, BatchProcessStatus status)
        throws BatchProcessingException;
    
    List<Coupon> findCouponByConsumerId(int consumerId);
    
    List<Coupon> findByCustomerIdAndCouponIds(Integer customerId, Collection<Integer> couponIds);
    
    int findCouponCountByCustomerId(Integer customerId);
}
