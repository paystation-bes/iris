package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Reversal;

public interface ReversalService {
    
    String CUST_IS_MIG = "customer.isMigrated";
    String MERCHANT_ACCOUNT = "merchantAccount";
    String CARD_NUMBER = "cardNumber";
    
    int findTotalReversalsByCardData(String cardNumber);
    
    Collection<Reversal> findReversalsByCardData(String cardNumber, boolean isEvict);
    
    void updateReversal(Reversal reversal);
    
    void archiveReversalData();
    
    Collection<Reversal> getIncompleteReversalsForReversal(Date processedBefore);
    
    Collection<Reversal> getIncompleteReversalsForReversal(MerchantAccount merchantAccount);
    
    void saveReversal(Reversal reversal);
    
}
