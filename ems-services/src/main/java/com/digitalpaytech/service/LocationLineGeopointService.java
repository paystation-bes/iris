package com.digitalpaytech.service;

import com.digitalpaytech.domain.LocationLineGeopoint;
import com.digitalpaytech.dto.GeoLocationEntry;

public interface LocationLineGeopointService {
    
    LocationLineGeopoint getLocationLineGeopointById(long geopointId);
    
    LocationLineGeopoint getLocationLineGeopointByLocationId(int locationId);
    
    void saveOrUpdateLocationLineGeopoint(LocationLineGeopoint line);
    
    void findOccupancyForGeoPoint(GeoLocationEntry geoLocationEntry, Integer locationId);

    void deleteLocationLineGeopoint(LocationLineGeopoint line);
}
