package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;

public interface CardRetryTransactionService {
    void clearRetriesExceededCardData();
    
    void clearRetriesExceededCardDataForMigratedCustomers();
    
    Date getMaxRetryTimeForTransaction(CardRetryTransaction crt);
    
    int getMatchingTransactionsCountForCardData(String cardData);
    
    Collection<CardRetryTransaction> getCardRetryTransactionByCardData(String cardData, int howManyRows, boolean isEvict);
    
    void updateCardRetryTransaction(CardRetryTransaction cardRetryTransaction);
    
    Collection<CardRetryTransaction> findCardRetryTransactions(Collection<Integer> pointOfSaleIds, int maxRetries, int numRecords, Date retryDate);
    
    CardRetryTransaction findByPointOfSalePurchasedDateTicketNumber(Integer pointOfSaleId, Date purchasedDate, Integer ticketNumber);
    
    List<String> findBadCardHashByPointOfSaleIdsDateAndNumRetries(List<Integer> pointOfSaleIds, int yymm, int creditCardOfflineRetryLimit);

    EntityDao getEntityDao();
    
    List<Object[]> findEmptyBadCardHash();
    
    int updateBadCardHash(String cardHash, String badCardHash);
    
    void save(CardRetryTransaction crt);
    
    CardRetryTransaction findCardRetryTransaction(Long id);

    void delete(CardRetryTransaction cardRetryTransaction);
    
    CardRetryTransaction getCardRetryTransaction(List<Object> objects);
    
    CardRetryTransaction getCardRetryTransaction(PointOfSale pointOfSale, Purchase purchase, ProcessorTransaction ptd, String encryptedCardData);
}
