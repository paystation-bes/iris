package com.digitalpaytech.service;

import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;

public interface ExtensibleRateAndPermissionService {
    
    public SmsRateInfo getSmsRateInfo(SmsAlertInfo smsAlertInfo);
}
