package com.digitalpaytech.service.cps;

import java.util.List;

import com.digitalpaytech.domain.CPSData;

public interface CPSDataService {
    
    CPSData findCPSDataByChargeTokenAndRefundToken(String chargeToken, String refundToken);
    
    CPSData findCPSDataByChargeTokenAndRefundTokenIsNull(String chargeToken);
    
    List<CPSData> findListCPSDataByProcessorTransactionId(Long processorTransactionId);
    
    CPSData save(CPSData cpsData);
    
    CPSData findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(long processorTransactionId);
    
    void delete(CPSData cpsData);
}
