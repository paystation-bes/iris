package com.digitalpaytech.service.cps.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.service.cps.CPSTransactionClosedService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.ElavonTransactionDetailService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.TransactionSettlementStatusType;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.ElavonRequestType;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.dto.cps.TransactionClosedMessage;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("cpsTransactionClosedService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSTransactionClosedServiceImpl implements CPSTransactionClosedService {
    private static final Logger LOG = Logger.getLogger(CPSTransactionClosedServiceImpl.class);
    private static final int CREDIT_CARD_SALE_ID = 1;
    private static final int CREDIT_CARD_RETURN_ID = 4;
    private static final int CREDIT_CARD_REVERSAL_ID = 3;
    private static final int CREDIT_CARD_VOID_ID_INCORRECT = 2;
    private static final int[] PROCESSOR_TRANSACTION_TYPES = new int[] {
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, 
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REVERSAL,
    };
    private static final int[] ELAVON_REQUEST_TYPES = new int[] {
        CREDIT_CARD_SALE_ID, CREDIT_CARD_SALE_ID, CREDIT_CARD_RETURN_ID, CREDIT_CARD_REVERSAL_ID,
    };

    
    @Autowired
    private ElavonTransactionDetailService elavonTransactionDetailService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    
    @Override
    public Long saveElavonTransactionDetail(final TransactionClosedMessage transactionClosedMessage) {
        try {
            final ElavonTransactionDetail etd = create(transactionClosedMessage);
            this.elavonTransactionDetailService.saveElavonTransactionDetail(etd);
            if (LOG.isDebugEnabled()) {
                LOG.debug("ElavonTransactinDetail is saved, id: " + etd.getId());
            }
            return etd.getId();
        
        } catch (ApplicationException ae) {
            return new Long(WebCoreConstants.N_A);
        }
    }
    
    
    private Date convert(final String pattern, final String dateString) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.parse(dateString);
    }
    
    @Override
    public ElavonTransactionDetail create(final TransactionClosedMessage transactionClosedMessage) throws ApplicationException {
        final CPSData cpsData = this.cpsDataService.findCPSDataByChargeTokenAndRefundTokenIsNull(transactionClosedMessage.getChargeToken());
        if (cpsData == null || cpsData.getProcessorTransaction() == null) {
            final String err = "Couldn't find CPSData from transactionClosedMessage.getChargeToken(): " + transactionClosedMessage.getChargeToken()
                                    + ", TransactionClosedMessage: " + transactionClosedMessage;
            LOG.error(err);
            throw new ApplicationException(err);
        }
        final ProcessorTransaction pt = this.processorTransactionService.findProcessorTransactionById(cpsData.getProcessorTransaction().getId(), true);
        final Date currentDate = DateUtil.getCurrentGmtDate();
        Date purchasedDate;
        try {
            purchasedDate = convert(DateUtil.UTC_CPS_DATE_TIME_FORMAT, transactionClosedMessage.getPurchasedDate());
        } catch (ParseException pe) {
            LOG.error("Cannot parse purchasedDate: " + transactionClosedMessage.getPurchasedDate());
            purchasedDate = currentDate;
        }
        final ElavonTransactionDetail detail = new ElavonTransactionDetail();
        detail.setProcessorTransaction(pt);
        detail.setMerchantAccount(pt.getMerchantAccount());
        detail.setPointOfSale(pt.getPointOfSale());
        detail.setPurchasedDate(purchasedDate);
        detail.setTicketNumber(pt.getTicketNumber());
        
        final TransactionSettlementStatusType tsst = new TransactionSettlementStatusType();
        tsst.setId(transactionClosedMessage.getTransactionSettlementStatusTypeId());
        detail.setTransactionSettlementStatusType(tsst);
        
        detail.setAccountEntryMode((byte) WebCoreConstants.N_A);
        detail.setPosentryCapability((byte) WebCoreConstants.N_A);
        detail.setTransactionAmount(pt.getAmount());
        detail.setOriginalAuthAmount(pt.getAmount());
        detail.setAuthorizedAmount(pt.getAmount());
        detail.setResponseCode(StandardConstants.STRING_EMPTY_STRING);
        detail.setBatchNumber((short) WebCoreConstants.N_A);
        detail.setTraceNumber(WebCoreConstants.N_A);
        detail.setTransactionReferenceNbr(WebCoreConstants.N_A);

        final ElavonRequestType reqType = new ElavonRequestType();
        reqType.setId(getElavonRequestTypeId(pt.getProcessorTransactionType().getId()));
        detail.setElavonRequestType(reqType);
        
        detail.setNumRetries((short) WebCoreConstants.N_A);
        detail.setCreatedGmt(currentDate);
        detail.setAuthorizationDateTime(purchasedDate);
        return detail;
    }
    
    private int getElavonRequestTypeId(final int processorTransactionTypeId) {
        for (int i = 0; i < PROCESSOR_TRANSACTION_TYPES.length; i++) {
            if (processorTransactionTypeId == PROCESSOR_TRANSACTION_TYPES[i]) {
                return ELAVON_REQUEST_TYPES[i];
            }
        }
        return CREDIT_CARD_VOID_ID_INCORRECT;
    }
    
    public void setElavonTransactionDetailService(final ElavonTransactionDetailService elavonTransactionDetailService) {
        this.elavonTransactionDetailService = elavonTransactionDetailService;
    }
    public void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    public void setCPSDataService(final CPSDataService cpsDataService) {
        this.cpsDataService = cpsDataService;
    }
}
