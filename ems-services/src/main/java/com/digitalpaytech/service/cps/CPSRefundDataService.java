package com.digitalpaytech.service.cps;

import java.util.List;

import com.digitalpaytech.domain.CPSRefundData;

public interface CPSRefundDataService {
    
    List<CPSRefundData> findByChargeToken(String chargeToken);
    
    void save(CPSRefundData cpsRefundData);
    
    void delete(CPSRefundData cpsRefundData);
    
    CPSRefundData findByChargeTokenAndRefundToken(String chargeToken, String refundToken);

    void manageCPSRefundData(List<CPSRefundData> cpsRefundData);

    void manageCPSRefundData(CPSRefundData cpsRefundData);
    
}
