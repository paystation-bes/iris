package com.digitalpaytech.service.cps;

import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CPSReversalData.ReversalType;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.dto.cps.Tokens;
import com.digitalpaytech.util.dto.Pair;

public interface CoreCPSService {
    
    boolean sendPreAuthToCoreCPS(PreAuth preAuth, PointOfSale pointOfSale, MerchantAccount merchantAccount) throws CryptoException;
    
    int sendPostAuthToCoreCPS(PreAuth preAuth, ProcessorTransaction ptd);
    
    Tokens sendProcessedTransactionToCoreCPS(Purchase purchase, ProcessorTransaction processorTransaction, CardEaseData cardEaseData);
    
    Tokens sendProcessedTransactionToCoreCPS(Purchase purchase, ProcessorTransaction processorTransaction, MerchantAccount merchantAccount,
        String cardNumber);
    
    int sendStoreAndForwardToCoreCPS(ProcessorTransaction processorTransaction, CardRetryTransaction cardRetryTransaction) throws CryptoException;
    
    Pair<String, Authorization> sendStoreAndForwardToCoreCPSOrFailTransaction(ProcessorTransaction processorTransaction,
        CardRetryTransaction cardRetryTransaction, String cardTypeName) throws CryptoException;
    
    ProcessorTransaction sendRefundToCoreCPS(MerchantAccount merchantAccount, ProcessorTransaction original, ProcessorTransaction refund)
        throws CryptoException;
    
    Authorization sendChargeToCoreCPS(Track2Card track2Card, ProcessorTransaction processorTransaction) throws CryptoException;
    
    int sendReversalsToCoreCPS(String chargeToken, String refundToken, ReversalType reversalType);
    
    int sendReversalsToCoreCPS(ProcessorTransaction processorTransaction, String chargeToken, ReversalType reversalType,
        Exception exceptionFromTransaction);
    
    void updateObjectsAfterSuccessfulCoreCPSStoreAndForward(ProcessorTransaction processorTransaction, Authorization authorization,
        CardRetryTransaction cardRetryTransaction, String chargeToken);
    
    int sendReversalsToCoreCPS(ProcessorTransaction processorTransaction, String chargeToken, ReversalType reversalType, String refundToken,
        Exception exceptionFromTransaction);
    
}
