package com.digitalpaytech.service.cps.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.service.cps.CPSDataService;

@Service("cpsDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSDataServiceImpl implements CPSDataService {
    
    private static final String REFUND_TOKEN = "refundToken";
    private static final String CHARGE_TOKEN = "chargeToken";
    private static final String PROCESSOR_TRANSACTION_ID = "processorTransactionId";
    private static final String FIND_CPS_DATA_BY_PROCESSOR_TRANSACTION_ID = "CPSData.findCPSDataByProcessorTransactionId";
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public CPSData findCPSDataByChargeTokenAndRefundToken(final String chargeToken, final String refundToken) {
        if (chargeToken == null || refundToken == null) {
            return null;
        }
        return (CPSData) this.entityDao.findUniqueByNamedQueryAndNamedParam("CPSData.findCPSDataByChargeTokenAndRefundToken",
                                                                            new String[] { CHARGE_TOKEN, REFUND_TOKEN },
                                                                            new Object[] { chargeToken, refundToken }, true);
    }
    
    @Override
    public CPSData findCPSDataByChargeTokenAndRefundTokenIsNull(final String chargeToken) {
        if (chargeToken == null) {
            return null;
        }
        return (CPSData) this.entityDao.findUniqueByNamedQueryAndNamedParam("CPSData.findCPSDataByChargeTokenAndRefundTokenIsNull", CHARGE_TOKEN,
                                                                            chargeToken);
    }
    
    @Override
    public List<CPSData> findListCPSDataByProcessorTransactionId(final Long processorTransactionId) {
        if (processorTransactionId == null) {
            return new ArrayList<>();
        }
        
        @SuppressWarnings("unchecked")
        final List<CPSData> cpsDataList = this.entityDao.findByNamedQueryAndNamedParam(FIND_CPS_DATA_BY_PROCESSOR_TRANSACTION_ID,
                                                                                       PROCESSOR_TRANSACTION_ID, processorTransactionId);
        if (cpsDataList == null) {
            return new ArrayList<>();
        } else {
            return cpsDataList;
        }
    }
    
    @Override
    public CPSData findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(final long processorTransactionId) {
        
        @SuppressWarnings("unchecked")
        final List<CPSData> cpsDataList =
                this.entityDao.findByNamedQueryAndNamedParam("CPSData.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull",
                                                             new String[] { PROCESSOR_TRANSACTION_ID }, new Object[] { processorTransactionId });
        if (cpsDataList == null || cpsDataList.isEmpty()) {
            return null;
        } else {
            return cpsDataList.get(0);
        }
    }
    
    @Override
    public CPSData save(final CPSData cpsData) {
        cpsData.setId((Integer) this.entityDao.save(cpsData));
        return cpsData;
    }
    
    @Override
    public void delete(CPSData cpsData) {
        this.entityDao.delete(cpsData);
    }
}
