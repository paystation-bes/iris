package com.digitalpaytech.service.cps.impl;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.client.CoreCPSClient;
import com.digitalpaytech.client.dto.Error;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.Status;
import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.client.dto.corecps.Payment;
import com.digitalpaytech.client.dto.corecps.PaymentsList;
import com.digitalpaytech.client.dto.corecps.TransactionRequest;
import com.digitalpaytech.client.dto.corecps.TransactionResponse;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.client.util.HystrixExceptionUtil;
import com.digitalpaytech.data.PaymentParameters;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSReversalData.ReversalType;
import com.digitalpaytech.domain.CPSStoreAndForwardAttempt;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.cps.Tokens;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PaymentTypeService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.StoreAndForwardService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.cps.CPSReversalDataService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.dto.Pair;
import com.digitalpaytech.util.json.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ncipher.nfast.connect.ClientException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service("coreCPSService")
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass" })
public class CoreCPSServiceImpl implements CoreCPSService {
    private static final String RESPONSE_FROM_LINK_CPS_FOR_ONLINE_REFUND_IS_NOT_VALID_JSON =
            "Response from Link CPS for online refund is not valid JSON";
    
    private static final String FAILED_REQUEST_FOR_ONLINE_REFUND_TO_LINK_CPS = "Failed request for online refund to Link CPS";
    
    private static final String FAILED_SENDING_STORE_AND_FORWARD_TO_LINK_CPS = "Fail while sending StoreAndForward to Link CPS";
    
    private static final String FAILED_SENDING_STORE_AND_FORWARD_TO_LINK_CPS_UNABLE_GET_AUTHORIZATION =
            "Fail while sending StoreAndForward to Link CPS. Unable to get Authorization from response";
    
    private static final Integer[] REVERSAL_PROCESSOR_TRANSACTION_TYPE_IDS = new Integer[] {
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_AUTHORIZED,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND_3RD_PARTY,
    };
    
    private static final Integer[] BATCH_PROCESSOR_TRANSACTION_TYPE_IDS = new Integer[] {
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EMS_RETRY_WITH_NO_REFUNDS,
        CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS,
    };
    
    private static final Logger LOG = Logger.getLogger(CoreCPSService.class);
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private MailerService mailerService;

    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired 
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private PaymentTypeService paymentTypeService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private CPSReversalDataService cpsReversalDataService;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @Autowired
    private CcFailLogService ccFailLogService;
    
    @Autowired
    private StoreAndForwardService storeAndForwardService;
    
    private CoreCPSClient client;
    
    private JSON json;
    
    private final TypeReference<ObjectResponse<Authorization>> objectAuthType = new TypeReference<ObjectResponse<Authorization>>() {
    };
    
    @PostConstruct
    public void init() {
        this.json = new JSON();
        this.client = this.clientFactory.from(CoreCPSClient.class);
    }
    
    @Override
    public final boolean sendPreAuthToCoreCPS(final PreAuth preAuth, final PointOfSale pointOfSale, final MerchantAccount merchantAccount)
        throws CryptoException {
        
        assertLinkEncrypted(preAuth.getCardData());
        
        final UUID chargeToken = UUID.randomUUID();
        preAuth.setChargeToken(chargeToken.toString());
        
        final PaymentParameters params = new PaymentParameters(preAuth, null, pointOfSale, merchantAccount, null, chargeToken, preAuth.getCardType());
        final Payment paymentMessage = buildCoreCpsPayment(params);
        
        try {
            final String jsonResult = this.client.preAuth(paymentMessage).execute().toString(Charset.defaultCharset());
            
            final ObjectResponse<Authorization> objAuth = this.json.deserialize(jsonResult, this.objectAuthType);
            
            final Authorization authorization = objAuth.getResponse();
            
            updatePreAuth(preAuth, authorization);
        } catch (JsonException jex) {
            LOG.error("Invalid JSON returned from Link CPS", jex);
            this.sendReversalsToCoreCPS(null, chargeToken.toString(), ReversalType.PREAUTH, jex);
            throw new IllegalStateException(jex);
        } catch (HystrixRuntimeException hre) {
            LOG.error("Unable to Authorize PreAuth request", hre);
            
            this.sendReversalsToCoreCPS(null, chargeToken.toString(), ReversalType.PREAUTH, hre);            
            
            if (HystrixExceptionUtil.isCryptoCause(hre)) {
                throw new CryptoException(new ClientException("core-cps returned crypto problem for pre auth"));
            }
            
            final Throwable rootCause = ExceptionUtils.getRootCause(hre);
            if (rootCause instanceof CommunicationException) {
                handleFailedPreAuth(preAuth, (CommunicationException) rootCause);
            }
            throw hre;
        } finally {
            preAuth.setCardData(null);
            preAuth.setCardHash(StandardConstants.STRING_EMPTY_STRING);
            preAuth.setCardExpiry((short) 0);
            this.preAuthService.savePreauth(preAuth);
        }
        
        return preAuth.isIsApproved();
        
    }

    private boolean isStatusCode(final ObjectResponse<Authorization> objResp, final String expectedStatusCode) {
        return objResp != null 
                && objResp.getStatus() != null 
                && StringUtils.isNotBlank(objResp.getStatus().getResponseStatus()) 
                && objResp.getStatus().getResponseStatus().equalsIgnoreCase(expectedStatusCode);
    }
    
    private String createTokensLogString(final String chargeToken, final String refundToken) {
        return "chargeToken: " + chargeToken + ", refundToken: " + refundToken;
    }
    
    private String createLogDueToIgnored(final ProcessorTransaction processorTransaction, 
                                         final String chargeToken,
                                         final String refundToken,
                                         final String reversalType) {
        
        final String msg = "CoreCPS returned 'IGNORED - does not support operation'. " + createTokensLogString(chargeToken, refundToken)
            + ", reversalType: " + reversalType;
        if (processorTransaction == null || processorTransaction.getMerchantAccount() == null) {
            return  msg;
        } else {
            final MerchantAccount merchAcct = this.merchantAccountService.findById(processorTransaction.getMerchantAccount().getId());
            return msg + ". Processor id: " + merchAcct.getProcessor().getId() + ", terminalToken: "
                + merchAcct.getTerminalToken() + ", ProcessingDate: " + processorTransaction.getProcessingDate() + ", PointOfSale id: " 
                + processorTransaction.getPointOfSale().getId();
        }
    }
    
    /**
     * This method is called from:
     *  - CPSReversalRetryService, reversalRetry with ProcessorTransaction = null
     */
    @Override
    public final int sendReversalsToCoreCPS(final String chargeToken, final String refundToken, final ReversalType reversalType) {
        return sendReversalsToCoreCPS(null, chargeToken, reversalType, refundToken, null);
    }
    
    /**
     * This method is called from:
     * - CoreCPSService, reverseOrRetryTransaction
     * - CoreCPSService, sendPreAuthToCoreCPS with ProcessorTransaction = null
     */
    @Override
    public final int sendReversalsToCoreCPS(final ProcessorTransaction processorTransaction, 
                                            final String chargeToken, 
                                            final ReversalType reversalType,
                                            final Exception exceptionFromTransaction) {
        return sendReversalsToCoreCPS(processorTransaction, chargeToken, reversalType, null, exceptionFromTransaction);
    }
    
    /**
     * This method is call from:
     * - CoreCPSService, sendReversalsToCoreCPS with chargeToken
     * - CoreCPSService, sendRefundToCoreCPS with chargeToken and refundToken
     * 
     * @param exceptionFromTransaction
     *            it will be null if this method is called by CPSReversalRetryService.
     */
    @Override
    public final int sendReversalsToCoreCPS(final ProcessorTransaction processorTransaction, final String chargeToken,
        final ReversalType reversalType, final String refundToken, final Exception exceptionFromTransaction) {
        int status;
        try {
            final String jsonResp = this.client.reversals(chargeToken, reversalType.name(), refundToken).execute().toString(Charset.defaultCharset());
            LOG.info("CPSReversal completed, " + createTokensLogString(chargeToken, refundToken) + ", response: " + jsonResp);

            final ObjectResponse<Authorization> objResp = this.json.deserialize(jsonResp, this.objectAuthType);
            if (isStatusCode(objResp, StandardConstants.STRING_STATUS_CODE_IGNORED)) {
                LOG.info(createLogDueToIgnored(processorTransaction, chargeToken, refundToken, reversalType.toString()));
                // Set to CardProcessingConstants.STATUS_AUTHORIZED so caller CPSReversalRetryService would delete the CPSReversalData record.
                status = CardProcessingConstants.STATUS_AUTHORIZED;
            } else {
                final Authorization authorization = objResp.getResponse();
                if (authorization == null) {
                    status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
                    // Input "exception" is null because it's called from CPSReversalRetryService and data is already in database.
                    if (exceptionFromTransaction != null) {
                        this.cpsReversalDataService.createCPSReversalData(processorTransaction, 
                                                                          chargeToken, 
                                                                          refundToken, 
                                                                          reversalType, 
                                                                          exceptionFromTransaction);
                    }
                } else {
                    status = CardProcessingConstants.STATUS_AUTHORIZED;
                }
            }                    
        } catch (JsonException | HystrixRuntimeException e) {
            // Input "exception" is null because it's called from CPSReversalRetryService and data is already in database.
            if (exceptionFromTransaction != null) {
                this.cpsReversalDataService.createCPSReversalData(processorTransaction, chargeToken, refundToken, reversalType, e);
            }
            final String pid = processorTransaction == null || processorTransaction.getId() == null ? StandardConstants.STRING_EMPTY_STRING
                    : String.valueOf(processorTransaction.getId().longValue());
            LOG.error("Problem occurred while calling this.client.reversals, processorTransaction id: " + pid + ", chargeToken: " + chargeToken
                      + ", original exception: " + exceptionFromTransaction, e);
            status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
        }
        return status;
    }
    
    @Override
    public final int sendPostAuthToCoreCPS(final PreAuth preAuth, final ProcessorTransaction ptd) {
        
        preparePaymentCard(ptd);
        
        final String chargeToken = preAuth.getChargeToken();
        
        int status;
        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug("CoreCPSClient call capture with chargeToken = " + chargeToken);
            }
            
            final String jsonResult = this.client.capture(chargeToken).execute().toString(Charset.defaultCharset());
            
            if (LOG.isDebugEnabled()) {
                LOG.debug("CoreCPSClient called capture with response = " + jsonResult);
            }
            
            final ObjectResponse<Authorization> objAuth = this.json.deserialize(jsonResult, this.objectAuthType);
            
            final Authorization authorization = objAuth.getResponse();
            
            if (authorization == null) {
                status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
            } else if (updateObjects(ptd, authorization, null, chargeToken)) {
                status = CardProcessingConstants.STATUS_AUTHORIZED;
            } else {
                status = CardProcessingConstants.STATUS_DENIED;
            }
            
        } catch (JsonException | HystrixRuntimeException e) {
            LOG.error("Unable to Capture request", e);
            status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
            insertCcFailedLog(e, preAuth.getPointOfSale(), preAuth.getMerchantAccount(), ptd.getProcessingDate(), ptd.getPurchasedDate(),
                              ptd.getTicketNumber(), CardProcessingConstants.CC_TYPE_SETTLEMENT);
            
            ptd.setReferenceNumber(WebCoreConstants.N_A_STRING);
            ptd.setCardType(WebCoreConstants.N_A_STRING);
            ptd.setProcessorTransactionId(WebCoreConstants.N_A_STRING);
            this.processorTransactionService.updateProcessorTransaction(ptd);
            
            // No need to call retry and background job, cardCaptureChargeJobDetail, will process every 15 minutes.
        }
        
        return status;
    }
    
    @Override
    public final Authorization sendChargeToCoreCPS(final Track2Card track2Card, final ProcessorTransaction processorTransaction)
        throws CryptoException {
        
        preparePaymentCard(processorTransaction);
        resetIsApproved(processorTransaction);
        
        final Payment payment = new Payment();
        final UUID chargeToken = UUID.randomUUID();
        payment.setChargeToken(chargeToken);
        payment.setTerminalToken(UUID.fromString(processorTransaction.getMerchantAccount().getTerminalToken()));
        payment.setAmount(processorTransaction.getAmount());
        
        payment.setPurchaseUTC(processorTransaction.getPurchasedDate().toInstant().toString());
        
        payment.setPaymentType(processorTransaction.isIsRfid()
                ? this.paymentTypeService.getText(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS)
                : this.paymentTypeService.getText(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE));
        
        // at this point there is a chance that we only have card data if Merchant Account was switched to Link
        final String cardToken = track2Card.getCardToken();
        final Matcher matcher = CardProcessingConstants.UUID_PATTERN.matcher(cardToken);
        if (track2Card.getCardToken().length() == HibernateConstants.CHAR_LENGTH_UUID && matcher.matches()) {
            payment.setCardToken(UUID.fromString(cardToken));
        } else {
            assertLinkEncrypted(cardToken);
            payment.setCardData(cardToken);
        }
        
        try {
            final String jsonResult = this.client.charge(payment).execute().toString(Charset.defaultCharset());
            final ObjectResponse<Authorization> objResponse = this.json.deserialize(jsonResult, this.objectAuthType);
            final Authorization authorization = objResponse.getResponse();
            
            final ProcessorTransaction charge = this.processorTransactionService.findProcessorTransactionById(processorTransaction.getId(), true);
            updateObjects(charge, authorization, null, authorization.getChargeToken().toString());
            return authorization;
        } catch (HystrixRuntimeException hre) {
            LOG.error(HystrixExceptionUtil.getRootCause(hre));
            insertCcFailedLog(hre, processorTransaction.getPointOfSale(), processorTransaction.getMerchantAccount(),
                              processorTransaction.getProcessingDate(), processorTransaction.getPurchasedDate(),
                              processorTransaction.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE);
            
            reverseOrRetryTransaction(processorTransaction, ReversalType.CHARGE, hre, null);
            
            if (HystrixExceptionUtil.isCryptoCause(hre)) {
                throw new CryptoException(new ClientException("core-cps returned crypto problem for charge"));
            }
            throw hre;
        } catch (JsonException jse) {
            LOG.error(RESPONSE_FROM_LINK_CPS_FOR_ONLINE_REFUND_IS_NOT_VALID_JSON);
            throw new IllegalStateException(jse);
            
        }
        
    }
    
    private boolean isForReversal(final int processorTransactionTypeId) {
        return isPresent(REVERSAL_PROCESSOR_TRANSACTION_TYPE_IDS, processorTransactionTypeId);
    }

    private boolean isForBatchConnectionRetry(final ProcessorTransaction processorTransaction, final Exception e) {
        if (processorTransaction.getProcessorTransactionType() != null
                && isPresent(BATCH_PROCESSOR_TRANSACTION_TYPE_IDS, processorTransaction.getProcessorTransactionType().getId())
            && (WebCoreUtil.isNetworkException(e) || HystrixExceptionUtil.isConnectionClosedForcibly(e))) {
            return true;
        }
        return false;
    }

    private boolean isPresent(final Integer[] options, final int processorTransactionTypeId) {
        final Optional<Integer> result = Arrays.asList(options).stream()
                .filter(ptt -> ptt.intValue() == processorTransactionTypeId)
                .findFirst();
        return result.isPresent();
    }
    
    private void reverseOrRetryTransaction(final ProcessorTransaction processorTransaction, 
                                           final ReversalType reversalType, 
                                           final Exception e, 
                                           final String encryptedCardData) {
        final boolean forBatchConnectionRetry = isForBatchConnectionRetry(processorTransaction, e);
        if (!forBatchConnectionRetry) {
            return;
        }
        
        // Need to save processorTransact first for offline batch.        
        if (processorTransaction.getId() == null) {
            this.processorTransactionService.saveProcessorTransaction(processorTransaction);
        }
        
        final ProcessorTransaction orig = this.processorTransactionService
                .findByPointOfSaleIdPurchasedDateTicketNumber(processorTransaction.getPointOfSale().getId(), processorTransaction.getPurchasedDate(),
                                                              processorTransaction.getTicketNumber());
        
        final CPSData cpsd = this.cpsDataService.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(orig.getId());
        if (isForReversal(processorTransaction.getProcessorTransactionType().getId())) {
            if (cpsd != null && StringUtils.isNotBlank(cpsd.getChargeToken())) {
                this.sendReversalsToCoreCPS(orig, cpsd.getChargeToken(), reversalType, e);
            } else {
                final String chargeToken = UUID.randomUUID().toString();
                this.cpsDataService.save(new CPSData(orig, chargeToken));
                this.sendReversalsToCoreCPS(orig, chargeToken, reversalType, e);
            }
        } else {
            final CardRetryTransaction retry 
                = this.cardRetryTransactionService.findByPointOfSalePurchasedDateTicketNumber(orig.getPointOfSale().getId(), 
                                                                                              orig.getPurchasedDate(), 
                                                                                              orig.getTicketNumber());
            this.insertOrUpdateCardRetryTransactionAndRetry(retry == null 
                    ? createCardRetryTransaction(orig, cpsd, encryptedCardData, forBatchConnectionRetry) : retry);
            
        }
    }
    
    private CardRetryTransaction createCardRetryTransaction(final ProcessorTransaction processorTransaction, 
                                                            final CPSData cpsd, 
                                                            final String encryptedCardData,
                                                            final boolean forBatchConnectionRetry) {
        final Date now = new Date();
        final CardRetryTransaction crt = new CardRetryTransaction();
        crt.setAmount(processorTransaction.getAmount());
        crt.setBadCardHash(WebCoreConstants.N_A_STRING);
        crt.setCardData(forBatchConnectionRetry ? encryptedCardData : StandardConstants.STRING_EMPTY_STRING);
        crt.setCardExpiry(CardProcessingConstants.MAX_YEAR_MONTH_VALUE);
        crt.setCardHash(processorTransaction.getCardHash());
        crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                .findCardRetryTransactionType(CardProcessingConstants.CARD_RETRY_TX_TYPE_BATCH));
        crt.setCardType(processorTransaction.getCardType());
        crt.setChargeToken(cpsd == null ? null : cpsd.getChargeToken());
        crt.setCreationDate(now);
        crt.setIsRfid(processorTransaction.isIsRfid());
        crt.setLast4digitsOfCardNumber(processorTransaction.getLast4digitsOfCardNumber());
        crt.setLastRetryDate(now);
        crt.setPointOfSale(processorTransaction.getPointOfSale());
        crt.setPurchasedDate(processorTransaction.getPurchasedDate());
        crt.setTicketNumber(processorTransaction.getTicketNumber());
        return crt;
    }
    
    @Override
    public final ProcessorTransaction sendRefundToCoreCPS(final MerchantAccount merchantAccount, final ProcessorTransaction original,
        final ProcessorTransaction refund) throws CryptoException {
        
        this.processorTransactionService.saveProcessorTransaction(refund);
        
        final CPSData cpsData = this.cpsDataService.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(original.getId());
        
        final String chargeToken = cpsData.getChargeToken();
        final UUID refundToken = UUID.randomUUID();
        
        final Payment payment = new Payment();
        payment.setAmount(original.getAmount());
        payment.setChargeToken(UUID.fromString(chargeToken));
        payment.setRefundToken(refundToken);
        
        boolean approved = false;
        Authorization refundAuth = null;
        try {
            final CPSData refundCpsData = new CPSData(refund, cpsData.getChargeToken().toString(), refundToken.toString());
            this.cpsDataService.save(refundCpsData);
            
            final String jsonResult = this.client.onlineRefund(payment).execute().toString(Charset.defaultCharset());
            final ObjectResponse<Authorization> objResponse = this.json.deserialize(jsonResult, this.objectAuthType);
            
            refundAuth = objResponse.getResponse();
            
            if (refundAuth != null) {
                approved = Authorization.AuthotizationStatus.ACCEPTED.toString().equalsIgnoreCase(refundAuth.getStatus().getResponseStatus());
                refund.setIsApproved(approved);
                
                updateRefundProcessorTrans(refund, refundAuth);
                
                this.processorTransactionService.updateProcessorTransaction(refund);
                
                if (approved) {
                    updateChargeProcessorTransaction(original);
                    this.processorTransactionService.updateProcessorTransaction(original);
                }
            }
            
            //TODO Commented this for now until Credit Card Transaction report sql has been fixed.
            // this.paymentCardService.saveRefundPaymentCard(refund);
            
            return refund;
        } catch (HystrixRuntimeException hre) {
            LOG.error(FAILED_REQUEST_FOR_ONLINE_REFUND_TO_LINK_CPS);

            sendReversalsToCoreCPS(refund, chargeToken, ReversalType.REFUND, refundToken.toString(), hre);
            
            if (HystrixExceptionUtil.isCryptoCause(hre)) {
                throw new CryptoException(new ClientException("core-cps returned crypto problem for refund"));
            }
            throw hre;
        } catch (JsonException jse) {
            LOG.error(RESPONSE_FROM_LINK_CPS_FOR_ONLINE_REFUND_IS_NOT_VALID_JSON);

            sendReversalsToCoreCPS(refund, chargeToken, ReversalType.REFUND, refundToken.toString(), jse);
            throw new IllegalStateException(jse);
        }
    }
    
    private Tokens createTokens(final UUID cardToken, final UUID chargeToken) {
        final Tokens tokens = new Tokens();
        tokens.addToken(Tokens.TokenTypes.CARD_TOKEN, cardToken);
        tokens.addToken(Tokens.TokenTypes.CHARGE_TOKEN, chargeToken);
        return tokens;
    }
    
    @Override
    public final Tokens sendProcessedTransactionToCoreCPS(final Purchase purchase, final ProcessorTransaction processorTransaction,
        final CardEaseData cardEaseData) {
        final TransactionRequest txReq = buildCoreCpsTransactionRequest(purchase, processorTransaction, cardEaseData);
        final UUID cardToken = sendProcessedTransactionToCoreCPS(txReq);
        return createTokens(cardToken, txReq.getChargeToken());
    }
    
    @Override
    public final Tokens sendProcessedTransactionToCoreCPS(final Purchase purchase, final ProcessorTransaction processorTransaction,
        final MerchantAccount merchantAccount, final String accountNumber) {
        final UUID chargeToken;
        final TransactionRequest txReq = buildCoreCpsTransactionRequest(purchase, processorTransaction, merchantAccount, accountNumber);
        final UUID cardToken = sendProcessedTransactionToCoreCPS(txReq);
        if (cardToken != null && txReq != null && txReq.getChargeToken() != null) {
            chargeToken = txReq.getChargeToken();
        } else {
            chargeToken = null;
        }
        return createTokens(cardToken, chargeToken);
    }
    
    private UUID sendProcessedTransactionToCoreCPS(final TransactionRequest txReq) {
        int status;
        UUID cardToken = null;
        try {
            final String jsonResult = this.client.push(txReq).execute().toString(Charset.defaultCharset());
            final ObjectResponse<TransactionResponse> objResp =
                    this.json.deserialize(jsonResult, new TypeReference<ObjectResponse<TransactionResponse>>() {
                    });
            if (LOG.isDebugEnabled()) {
                LOG.debug("CoreCPSClient sendProcessedTransactionToCoreCPS returned: " + objResp);
            }
            
            cardToken = objResp.getResponse().getCardToken();
            if (cardToken == null) {
                status = CardProcessingConstants.STATUS_DENIED;
            } else {
                status = CardProcessingConstants.STATUS_SUCCESS;
            }
        } catch (JsonException | HystrixRuntimeException e) {
            LOG.error("Unable to send TransactionRequest (EMV) to CoreCPS, \r\n" + txReq, e);
            status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
            if (isEMVAndClientOrNetworkException(txReq.getProcessorType(), e)) {
                this.mailerService.sendAdminErrorAlert("Cannot access micro-services", "Check ClientException", e);
                throw new CardTransactionException(e);
            }
        }
        LOG.info("sendProcessedTransactionToCoreCPS - status: " + status + ", cardToken: " + cardToken);
        
        return cardToken == null ? null : cardToken;
    }
    
    @Override
    public final int sendStoreAndForwardToCoreCPS(final ProcessorTransaction processorTransaction, final CardRetryTransaction cardRetryTransaction)
        throws CryptoException {
        
        preparePaymentCard(processorTransaction);
        resetIsApproved(processorTransaction);
        
        int status;
        PaymentsList payments = null;
        try {
            final UUID chargeToken = StringUtils.isNotBlank(cardRetryTransaction.getChargeToken())
                    ? UUID.fromString(cardRetryTransaction.getChargeToken()) : UUID.randomUUID();
            
            final PaymentParameters params = new PaymentParameters(null, processorTransaction, processorTransaction.getPointOfSale(), null,
                    cardRetryTransaction.getCardData(), chargeToken, processorTransaction.getCardType());
            
            payments = buildCoreCpsPaymentsList(params);
            final String jsonResp = this.client.storeAndForward(payments).execute().toString(Charset.defaultCharset());
            if (LOG.isDebugEnabled()) {
                LOG.debug("Store & Forward response: " + jsonResp);
            }
            
            // Authorization would be an "acknowledgement" that responseStatus is "SUCCESS" but no transaction result.
            final Authorization authorization = this.json.deserialize(jsonResp, new TypeReference<ObjectResponse<Authorization>>() {
                }).getResponse();
            status = checkResultAndUpdateObjects(processorTransaction, authorization, cardRetryTransaction, chargeToken.toString());
            
        } catch (JsonException | HystrixRuntimeException e) {
            LOG.error("Problem for Store & Forward request, " + payments, e);
            status = CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
            
            reverseOrRetryTransaction(processorTransaction, ReversalType.CHARGE, e, cardRetryTransaction.getCardData());
            
            insertCcFailedLog(e, processorTransaction.getPointOfSale(), processorTransaction.getMerchantAccount(),
                              processorTransaction.getProcessingDate(), processorTransaction.getPurchasedDate(),
                              processorTransaction.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE);
            
            if (e instanceof HystrixRuntimeException && HystrixExceptionUtil.isCryptoCause((HystrixRuntimeException) e)) {
                throw new CryptoException(new ClientException("core-cps returned crypto problem for SnF"));
            }
        }
        return status;
    }
    
    @Override
    public final Pair<String, Authorization> sendStoreAndForwardToCoreCPSOrFailTransaction(final ProcessorTransaction processorTransaction,
        final CardRetryTransaction cardRetryTransaction, final String cardTypeName) throws CryptoException {
        
        preparePaymentCard(processorTransaction);
        resetIsApproved(processorTransaction);
        
        PaymentsList payments = null;
        
        final CPSStoreAndForwardAttempt storeAndForwardAttempt = this.storeAndForwardService.
                findByPointOfSaleIdAndPurchasedDateAndTicketNumber(processorTransaction.getPointOfSale().getId(), 
                                                                   processorTransaction.getPurchasedDate(), 
                                                                   processorTransaction.getTicketNumber());
        
        final UUID chargeToken = (storeAndForwardAttempt != null) ? UUID.fromString(storeAndForwardAttempt.getChargeToken()) : UUID.randomUUID();
        
        try {            
            final PaymentParameters params = new PaymentParameters(null, processorTransaction, processorTransaction.getPointOfSale(), null,
                    cardRetryTransaction.getCardData(), chargeToken, cardTypeName);
            
            payments = buildCoreCpsPaymentsList(params);
            final String jsonResp = this.client.storeAndForward(payments).execute().toString(Charset.defaultCharset());
            if (LOG.isDebugEnabled()) {
                LOG.debug("Store & Forward or fail response: " + jsonResp);
            }
            
            final Authorization authorization = this.json.deserialize(jsonResp, new TypeReference<ObjectResponse<Authorization>>() {
                }).getResponse();
            
            /*
             * Something went wrong w/ CoreCPS response, Iris is not sure of the success of the S&F.
             * In case it isn't already on the storeAndForwardAttempt table, add it.
             */
            if (authorization == null) {
                if (storeAndForwardAttempt == null) {
                    this.storeAndForwardService.saveCPSStoreAndForwardAttempt(new CPSStoreAndForwardAttempt(processorTransaction.getPointOfSale(), 
                                                                       processorTransaction.getPurchasedDate(), 
                                                                       processorTransaction.getTicketNumber(), chargeToken.toString()));
                }
                
                final RuntimeException exception = new RuntimeException(FAILED_SENDING_STORE_AND_FORWARD_TO_LINK_CPS_UNABLE_GET_AUTHORIZATION);
                insertCcFailedLog(exception, processorTransaction.getPointOfSale(), processorTransaction.getMerchantAccount(),
                                  processorTransaction.getProcessingDate(), processorTransaction.getPurchasedDate(),
                                  processorTransaction.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE);
                throw exception;
            }
            
            // At this point S&F was received successfully by CoreCPS. Set storeAndForwardAttempt to be deleted before responding to the PayStation.
            if (storeAndForwardAttempt != null) {
                storeAndForwardAttempt.setIsSuccessful(true);
                this.storeAndForwardService.saveCPSStoreAndForwardAttempt(storeAndForwardAttempt);
            }
            
            return new Pair<String, Authorization>(chargeToken.toString(), authorization);
            
        } catch (JsonException | HystrixRuntimeException exception) {
            LOG.error("Problem for Store & Forward or fail request, " + payments, exception);
            
            insertCcFailedLog(exception, processorTransaction.getPointOfSale(), processorTransaction.getMerchantAccount(),
                              processorTransaction.getProcessingDate(), processorTransaction.getPurchasedDate(),
                              processorTransaction.getTicketNumber(), CardProcessingConstants.CC_TYPE_SALE);
            
            if (exception instanceof HystrixRuntimeException && HystrixExceptionUtil.isCryptoCause((HystrixRuntimeException) exception)) {
                throw new CryptoException(new ClientException("core-cps returned crypto problem for SnF or fail"));
            }
            
            if (HystrixExceptionUtil.isRequestWithClientError((HystrixRuntimeException) exception)) {
                throw new RuntimeException(FAILED_SENDING_STORE_AND_FORWARD_TO_LINK_CPS, exception);
            }
            
            if (storeAndForwardAttempt == null) {
                this.storeAndForwardService.saveCPSStoreAndForwardAttempt(new CPSStoreAndForwardAttempt(processorTransaction.getPointOfSale(),
                        processorTransaction.getPurchasedDate(), processorTransaction.getTicketNumber(), chargeToken.toString()));
            }
            
            throw new RuntimeException(FAILED_SENDING_STORE_AND_FORWARD_TO_LINK_CPS, exception);
        }
    }
    
    private Payment buildCoreCpsPayment(final Integer amount, final String cardData, final boolean isRfid, final PointOfSale pointOfSale,
        final MerchantAccount merchantAccount, final UUID chargeToken, final String cardTypeName) throws CryptoException {
        final Payment payment = new Payment();
        payment.setDeviceSerialNumber(pointOfSale.getSerialNumber());
        final Integer unifiId = pointOfSale.getCustomer().getUnifiId();
        
        if (unifiId != null) {
            payment.setCustomerId(unifiId);
        }
        
        final String terminalTokenString = merchantAccount.getTerminalToken();
        if (!StringUtils.isBlank(terminalTokenString)) {
            payment.setTerminalToken(UUID.fromString(terminalTokenString));
        }
        
        payment.setCardType(cardTypeName);
        payment.setPurchaseUTC(Instant.now());
        payment.setAmount(amount);
        
        if (StringUtils.isNotBlank(cardData)) {
            assertLinkEncrypted(cardData);
        }
        
        payment.setCardData(cardData);
        payment.setChargeToken(chargeToken);
        payment.setPaymentType(isRfid ? this.paymentTypeService.getText(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS)
                : this.paymentTypeService.getText(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE));
        return payment;
    }
    
    private Payment buildCoreCpsPayment(final PaymentParameters params) throws CryptoException {
        return buildCoreCpsPayment(params.getPreAuth().getAmount(), params.getPreAuth().getCardData(), params.getPreAuth().isIsRfid(),
                                   params.getPointOfSale(), params.getMerchantAccount(), params.getChargeToken(), params.getCardTypeName());
    }
    
    private PaymentsList buildCoreCpsPaymentsList(final PaymentParameters params) throws CryptoException {
        final List<Payment> payments = new ArrayList<Payment>(1);
        payments.add(buildCoreCpsPayment(params.getProcessorTransaction().getAmount(), params.getCardData(),
                                         params.getProcessorTransaction().isIsRfid(), params.getPointOfSale(),
                                         params.getProcessorTransaction().getMerchantAccount(), params.getChargeToken(), params.getCardTypeName()));
        return new PaymentsList(payments);
    }
    
    private int checkResultAndUpdateObjects(final ProcessorTransaction processorTransaction, final Authorization authorization,
        final CardRetryTransaction cardRetryTransaction, final String chargeToken) {
        if (authorization == null) {
            insertOrUpdateCardRetryTransactionAndRetry(cardRetryTransaction);
            return CardProcessingConstants.CARD_STATUS_EMS_UNAVAILABLE;
        } else {
            final boolean accepted = updateObjects(processorTransaction, authorization, cardRetryTransaction, chargeToken);
            LOG.info("Store & Forward accepted? " + accepted);
            return CardProcessingConstants.STATUS_AUTHORIZED;
        }
    }
    
    @Override
    public final void updateObjectsAfterSuccessfulCoreCPSStoreAndForward(final ProcessorTransaction processorTransaction, final Authorization authorization,
        final CardRetryTransaction cardRetryTransaction, final String chargeToken) {
        
        final boolean approvedFlag = updateObjects(processorTransaction, authorization, cardRetryTransaction, chargeToken);
        LOG.info("Store & Forward approved? " + approvedFlag);
    }
    
    private void preparePaymentCard(final ProcessorTransaction processorTransaction) {
        final PaymentCard paymentCard = this.paymentCardService.findByPurchaseIdAndCardType(processorTransaction, WebCoreConstants.CREDIT_CARD, true);
        if (paymentCard != null) {
            if (paymentCard.getProcessorTransaction() == null) {
                paymentCard.setProcessorTransaction(processorTransaction);
                paymentCard.setProcessorTransactionType(processorTransaction.getProcessorTransactionType());
            }
            
            if (paymentCard.getMerchantAccount() == null) {
                paymentCard.setMerchantAccount(processorTransaction.getMerchantAccount());
            }
            paymentCard.setIsApproved(false);
            this.paymentCardService.savePaymentCard(paymentCard);
        }
    }
    
    private void resetIsApproved(final ProcessorTransaction processorTransaction) {
        processorTransaction.setIsApproved(false);
    }
    
    private CardRetryTransaction updateRetriesAndDate(final CardRetryTransaction crt) {
        crt.setNumRetries(crt.getNumRetries() + 1);
        crt.setLastRetryDate(new Date());
        return crt;
    }
    
    private void insertOrUpdateCardRetryTransactionAndRetry(final CardRetryTransaction crt) {
        updateRetriesAndDate(crt);
        if (StringUtils.isBlank(crt.getChargeToken())) {
            crt.setChargeToken(UUID.randomUUID().toString());
        }
        if (crt.getId() == null) {
            this.cardRetryTransactionService.save(crt);
        } else {
            this.cardRetryTransactionService.updateCardRetryTransaction(crt);
        }
    }
    
    private String formatResponseCode(final String identifier, final String message) {
        return StringUtils.left(String.format("%s - %s", identifier, message), HibernateConstants.VARCHAR_LENGTH_50);
    }
    
    private void handleFailedPreAuth(final PreAuth preAuth, final CommunicationException rootCause) {
        preAuth.setIsApproved(false);
        preAuth.setResponseCode(createRootCauseMessage(rootCause));
    }
    
    private void insertCcFailedLog(final Exception e, final PointOfSale pos, final MerchantAccount ma, final Date processingDate,
        final Date purchasedDate, final int ticketNumber, final int ccType) {
        
        final Throwable rootCause = ExceptionUtils.getRootCause(e);
        final String reason;
        if (rootCause instanceof CommunicationException) {
            reason = createRootCauseMessage((CommunicationException) rootCause);
        } else if (rootCause == null) {
            reason = WebCoreConstants.N_A_STRING;
        } else {
            reason = rootCause.toString();
        }
        LOG.error(reason);
        this.ccFailLogService.logFailedCcTransaction(pos, ma, processingDate, purchasedDate, ticketNumber, ccType, reason);
    }
    
    private String createRootCauseMessage(final CommunicationException rootCause) {
        try {
            final ObjectResponse<?> objErrorStatus =
                    this.json.deserialize(rootCause.getResponseMessage().toCompletableFuture().get(), ObjectResponse.class);
            final Status errorStatus = objErrorStatus.getStatus();
            final List<Error<String>> errors = errorStatus.getErrors();
            if (errors == null || errors.isEmpty()) {
                return formatResponseCode(CardProcessingConstants.FAILED_STRING, CardProcessingConstants.FAILED_GENERIC_ERROR);
            } else {
                final Error<String> firstError = errors.get(0);
                return formatResponseCode(CardProcessingConstants.FAILED_STRING + StandardConstants.STRING_EMPTY_SPACE + firstError.getIdentifier(),
                                          firstError.getMessage());
            }
        } catch (JsonException | InterruptedException | ExecutionException e) {
            return formatResponseCode(CardProcessingConstants.FAILED_STRING, CardProcessingConstants.FAILED_GENERIC_ERROR);
        }
    }
    
    private ProcessorTransaction updateChargeProcessorTransaction(final ProcessorTransaction processorTransaction) {
        final int updatedTypeId;
        switch (processorTransaction.getProcessorTransactionType().getId()) {
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS:
            default:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS;
                break;
        }
        processorTransaction.setProcessorTransactionType(this.processorTransactionTypeService.findProcessorTransactionType(updatedTypeId));
        return processorTransaction;
    }
    
    private boolean updatePreAuth(final PreAuth preAuth, final Authorization authorization) {
        final boolean approved =
                Authorization.AuthotizationStatus.ACCEPTED.toString().equalsIgnoreCase(authorization.getStatus().getResponseStatus());
        preAuth.setIsApproved(approved);
        preAuth.setAuthorizationNumber(authorization.getAuthorizationNumber());
        preAuth.setCardType(authorization.getCardType());
        
        preAuth.setReferenceNumber(authorization.getReferenceNumber());
        preAuth.setProcessorTransactionId(authorization.getProcessorTransactionId());
        
        if (authorization.getProcessedDate() != null) {
            preAuth.setProcessingDate(Date.from(authorization.getProcessedDate()));
        }
        
        if (StringUtils.isNotBlank(authorization.getLast4Digits())) {
            preAuth.setLast4digitsOfCardNumber(Short.parseShort(authorization.getLast4Digits()));
        }
        
        if (authorization.getChargeToken() != null) {
            preAuth.setChargeToken(authorization.getChargeToken().toString());
        }
        
        if (authorization.getCardToken() != null) {
            preAuth.setCardToken(authorization.getCardToken().toString());
        }
        
        final List<Error<String>> statusErrors = authorization.getStatus().getErrors();
        
        if (statusErrors != null && !statusErrors.isEmpty()) {
            final Error<String> firstError = statusErrors.get(0);
            preAuth.setResponseCode(formatResponseCode(firstError.getIdentifier(), firstError.getMessage()));
        }
        
        preAuth.setExtraData(authorization.getOptionalData() == null ? null : authorization.getOptionalData().asText(null));
        return approved;
    }
 
    private boolean updateObjects(final ProcessorTransaction ptd, final Authorization authorization, final CardRetryTransaction cardRetryTransaction,
        final String chargeToken) {
        final boolean approved =
                Authorization.AuthotizationStatus.ACCEPTED.toString().equalsIgnoreCase(authorization.getStatus().getResponseStatus());
        // Update ProcessorTransaction
        ptd.setAuthorizationNumber(WebCoreUtil.returnNAIfBlank(authorization.getAuthorizationNumber()));
        if (StringUtils.isNotBlank(authorization.getCardType())) {
            ptd.setCardType(authorization.getCardType());
        }
        ptd.setReferenceNumber(WebCoreUtil.returnNAIfBlank(authorization.getReferenceNumber()));
        ptd.setProcessorTransactionId(WebCoreUtil.returnNAIfBlank(authorization.getProcessorTransactionId()));
        if (authorization.getLast4Digits() != null) {
            ptd.setLast4digitsOfCardNumber(Short.parseShort(authorization.getLast4Digits()));
        }
        if (authorization.getProcessedDate() != null) {
            ptd.setProcessingDate(Date.from(authorization.getProcessedDate()));
        }
        
        // Insert new ProcessorTransaction. In daily card retry background process the ProcessorTransaction is a new object.
        if (ptd.getId() == null) {
            this.processorTransactionService.saveProcessorTransaction(ptd);
        }
        
        // Insert CPSData
        final CPSData cpsData = new CPSData(ptd, chargeToken);
        this.cpsDataService.save(cpsData);
        
        // Delete CardRetryTransaction if exists.
        if (approved && cardRetryTransaction != null) {
            this.cardRetryTransactionService.delete(cardRetryTransaction);

            LOG.info("New ProcessorTransaction, CPSData are inserted, processorTransactionId: " + ptd.getId() + ", chargeToken is: " + chargeToken
                     + ", cardRetryTransaction is deleted, crt id: " + cardRetryTransaction);

        } else if (!approved && cardRetryTransaction != null) {
            final String cardType 
                = StringUtils.isNotBlank(authorization.getCardType()) ? authorization.getCardType() : cardRetryTransaction.getCardType();
            cardRetryTransaction.setCardType(cardType);            
            
            final short last4dig 
                = StringUtils.isNotBlank(authorization.getLast4Digits()) && !authorization.getLast4Digits().equals(StandardConstants.STRING_ZERO) 
                    ? Short.valueOf(authorization.getLast4Digits()) : cardRetryTransaction.getLast4digitsOfCardNumber();
            cardRetryTransaction.setLast4digitsOfCardNumber(last4dig);
            
            updateRetriesAndDate(cardRetryTransaction);
            insertOrUpdateCardRetryTransactionAndRetry(cardRetryTransaction);            
        }
        return approved;
    }
    
    private ProcessorTransaction updateRefundProcessorTrans(final ProcessorTransaction refund, final Authorization refundAuthorization) {
        if (refundAuthorization != null) {
            refund.setProcessingDate(Date.from(refundAuthorization.getProcessedDate()));
            refund.setReferenceNumber(setSafeReferenceNumber(refundAuthorization));
            refund.setProcessorTransactionId(refundAuthorization.getProcessorTransactionId());
            refund.setAuthorizationNumber(refundAuthorization.getAuthorizationNumber() == null ? StandardConstants.STRING_EMPTY_STRING
                    : refundAuthorization.getAuthorizationNumber().trim());
        }
        return refund;
        
    }
    
    private String setSafeReferenceNumber(final Authorization refundAuthorization) {
        
        //Currently, LINK CPS will response w/cardEaseReference which is too larger for ReferenceNumber(VARCHAR(30)) also put the same value in ProcessorTransactionId.
        if (refundAuthorization.getReferenceNumber() == null
            || refundAuthorization.getReferenceNumber().equals(refundAuthorization.getProcessorTransactionId())) {
            return StandardConstants.STRING_EMPTY_STRING;
            
        } else if (refundAuthorization.getReferenceNumber().length() > StandardConstants.CONSTANT_30) {
            LOG.warn("Received value for ReferenceNumber with lenght greater than 30 and it is not the same value of ProcessorTransactionId. RefundAuthorization = "
                      + refundAuthorization.toString());
            
            return StringUtils.substring(refundAuthorization.getReferenceNumber(), 0, 29);
        }
        return refundAuthorization.getReferenceNumber();
    }
    
    private TransactionRequest buildCoreCpsTransactionRequest(final Purchase purchase, final ProcessorTransaction procTransaction,
        final CardEaseData cardEaseData) {

        final Map<String, Object> paymentDetails = cardEaseData.getPaymentDetails();
        final SortedMap<String, String> extCardTokensMap = new TreeMap<>();
        extCardTokensMap.put(CardEaseData.CARD_REFERENCE, cardEaseData.getCardReference());
        extCardTokensMap.put(CardEaseData.CARD_HASH, cardEaseData.getCardHash());
        
        final TransactionRequest txReq = new TransactionRequest();
        txReq.setAmount(procTransaction.getAmount());
        txReq.setAuthorizationNumber(procTransaction.getAuthorizationNumber());
        txReq.setCardType(procTransaction.getCardType());
        txReq.setChargeToken(UUID.randomUUID());
        txReq.setExternalCardToken(extCardTokensMap);
        
        final String terminalTokenString = procTransaction.getMerchantAccount().getTerminalToken();
        if (!StringUtils.isBlank(terminalTokenString)) {
            txReq.setTerminalToken(UUID.fromString(terminalTokenString));
        }
        
        final Integer unifiId = procTransaction.getMerchantAccount().getCustomer().getUnifiId();
        if (unifiId != null) {
            txReq.setCustomerId(unifiId);
        }
        
        txReq.setDeviceSerialNumber(procTransaction.getPointOfSale().getSerialNumber());
        txReq.setLast4Digits(String.valueOf(procTransaction.getLast4digitsOfCardNumber()));
        txReq.setPaymentDetails(paymentDetails);
        txReq.setPaymentType(purchase.getPaymentType().getName());
        txReq.setProcessorTransactionId(WebCoreUtil.returnNAIfBlank(procTransaction.getProcessorTransactionId()));
        txReq.setProcessorType(procTransaction.getMerchantAccount().getProcessor().getName());
        txReq.setPurchaseUTC(StableDateUtil.convertFromGMTDateToUTC(purchase.getPurchaseGmt()));
        txReq.setReferenceNumber(WebCoreUtil.returnNAIfBlank(procTransaction.getReferenceNumber()));
        
        return txReq;
    }
    
    private TransactionRequest buildCoreCpsTransactionRequest(final Purchase purchase, final ProcessorTransaction procTransaction,
        final MerchantAccount merchantAccount, final String accountNumber) {
        final Map<String, Object> paymentDetails = new HashMap<String, Object>(1);
        final SortedMap<String, String> extCardTokensMap = new TreeMap<>();
        final SortedMap<String, String> cardDataMap = new TreeMap<>();
        cardDataMap.put(CardEaseData.CARD_HASH, procTransaction.getCardHash());
        if (StringUtils.isNotBlank(accountNumber)) {
            try {
                cardDataMap.put("accountNumber", this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, accountNumber));
            } catch (CryptoException e) {
                LOG.error(e);
                return null;
            }
        }
        
        final TransactionRequest txReq = new TransactionRequest();
        txReq.setAmount(procTransaction.getAmount());
        txReq.setAuthorizationNumber(procTransaction.getAuthorizationNumber());
        txReq.setCardType(procTransaction.getCardType());
        txReq.setChargeToken(UUID.randomUUID());
        txReq.setExternalCardToken(extCardTokensMap);
        txReq.setExternalCardToken(cardDataMap);
        txReq.setTerminalToken(UUID.fromString(merchantAccount.getTerminalToken()));
        txReq.setCustomerId(merchantAccount.getCustomer().getUnifiId());
        txReq.setDeviceSerialNumber(procTransaction.getPointOfSale().getSerialNumber());
        txReq.setLast4Digits(String.valueOf(procTransaction.getLast4digitsOfCardNumber()));
        txReq.setPaymentDetails(paymentDetails);
        txReq.setPaymentType(purchase.getPaymentType().getName());
        txReq.setProcessorTransactionId(WebCoreUtil.returnNAIfBlank(procTransaction.getProcessorTransactionId()));
        txReq.setProcessorType(merchantAccount.getProcessor().getName());
        txReq.setPurchaseUTC(StableDateUtil.convertFromGMTDateToUTC(purchase.getPurchaseGmt()));
        txReq.setReferenceNumber(WebCoreUtil.returnNAIfBlank(procTransaction.getReferenceNumber()));
        
        return txReq;
    }
    
    public final void setCcFailLogService(final CcFailLogService ccFailLogService) {
        this.ccFailLogService = ccFailLogService;
    }
    
    public final void setCardRetryTransactionTypeService(final CardRetryTransactionTypeService cardRetryTransactionTypeService) {
        this.cardRetryTransactionTypeService = cardRetryTransactionTypeService;
    }
    
    private void assertLinkEncrypted(final String enc) throws CryptoException {
        if (!this.cryptoService.isLinkCryptoKey(enc) && !WebCoreUtil.isUUID(enc)) {
            throw new CryptoException(new ClientException("core-cps cannot process card encrypted in scala crypto service"));
        }
    }
    
    private boolean isEMVAndClientOrNetworkException(final String processorName, final Exception e) {
        boolean isCreditCall = CardProcessingUtil.isCreditcallRequest(processorName, 
                                     this.processorService.findProcessor(CardProcessingConstants.PROCESSOR_ID_CREDITCALL),
                                     this.processorService.findProcessor(CardProcessingConstants.PROCESSOR_ID_CREDITCALL_LINK));
        if (LOG.isDebugEnabled()) {
            LOG.debug("isCreditCall:" + isCreditCall + ", processorName: " + processorName);
        }
        if (isCreditCall && WebCoreUtil.causedByClientExceptionOrNetworkCommunicationException(e)) {
            return true;
        }
        return false;
    }
}
