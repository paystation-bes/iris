package com.digitalpaytech.service.cps;

import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.dto.cps.TransactionClosedMessage;
import com.digitalpaytech.exception.ApplicationException;

public interface CPSTransactionClosedService {
    Long saveElavonTransactionDetail(TransactionClosedMessage transactionClosedMessage);
    ElavonTransactionDetail create(TransactionClosedMessage msg) throws ApplicationException;
}
