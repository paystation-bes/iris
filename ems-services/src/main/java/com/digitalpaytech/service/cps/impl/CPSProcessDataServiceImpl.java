package com.digitalpaytech.service.cps.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.service.cps.CPSRefundDataService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Service("cpsProcessDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSProcessDataServiceImpl implements CPSProcessDataService {
    private static final Logger LOG = Logger.getLogger(CPSProcessDataService.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private CPSRefundDataService cpsRefundDataService;
    
    @Override
    public final void manageCPSProcessData(final CPSProcessData cpsProcessData) {
        if (cpsProcessData == null) {
            LOG.error("Unable to manage update ProcessorTransaction or CPSData, CPSProcessData must not be null");
        } else {
            final CPSData cpsData = this.cpsDataService.findCPSDataByChargeTokenAndRefundTokenIsNull(cpsProcessData.getChargeToken());
            
            if (cpsData == null) {
                CPSProcessData duplicate = find(cpsProcessData.getTerminalToken(), cpsProcessData.getChargeToken());
                
                if (duplicate == null) {
                    this.entityDao.save(cpsProcessData);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("CPSProcessData.chargeToken: " + cpsProcessData.getChargeToken()
                                  + " doesn't exist in CPSData. The incoming transaction happened "
                                  + "outside of Iris, and data is saved in CPSProcessorData table. Waiting for PS now sending the transaction info...");
                    }
                } else {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Duplicate message for cpsProcessData" + cpsProcessData.getChargeToken());
                    }
                }
            } else {
                final ProcessorTransaction procTx =
                        this.processorTransactionService.findProcessorTransactionById(cpsData.getProcessorTransaction().getId(), true);
                
                printInfoLog(cpsProcessData, cpsData.getProcessorTransaction().getId(), cpsData.getChargeToken(), procTx);
                
                if (procTx != null) {
                    
                    procTx.setIsApproved(true);
                    procTx.setCardType(cpsProcessData.getCardType());
                    procTx.setAuthorizationNumber(cpsProcessData.getAuthorizationNumber());
                    procTx.setLast4digitsOfCardNumber(cpsProcessData.getLast4Digits());
                    
                    procTx.setProcessorTransactionId(cpsProcessData.getProcessorTransactionId());
                    
                    if (cpsProcessData.getReferenceNumber() != null) {
                        procTx.setReferenceNumber(cpsProcessData.getReferenceNumber());
                    }
                    
                    if (cpsProcessData.getProcessedGMT() != null) {
                        procTx.setProcessingDate(cpsProcessData.getProcessedGMT());
                    }
                    
                    if (cpsProcessData.isExpired()) {
                        handleExpired(procTx);
                    }
                    
                    this.entityDao.saveOrUpdate(procTx);
                    
                    updatePaymentCard(procTx);
                    
                    this.entityDao.delete(cpsProcessData);
                    
                    final List<CPSRefundData> cpsRefundData = this.cpsRefundDataService.findByChargeToken(cpsData.getChargeToken());
                    if (cpsRefundData != null && !cpsRefundData.isEmpty()) {
                        this.cpsRefundDataService.manageCPSRefundData(cpsRefundData);
                    }
                    
                }
                
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Updated ProcessorTransaction and deleted CPSProcessData");
                }
                
            }
        }
        
    }
    
    private void updatePaymentCard(final ProcessorTransaction procTx) {
        final PaymentCard paymentCard = this.paymentCardService.findByPurchaseIdAndCardType(procTx, CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
        if (paymentCard != null) {
            if (paymentCard.getCardLast4digits() == 0) {
                paymentCard.setCardLast4digits(procTx.getLast4digitsOfCardNumber());
            }
            
            if (paymentCard.getCreditCardType() == null) {
                paymentCard.setCreditCardType(this.creditCardTypeService.getCreditCardTypeByName(procTx.getCardType()));
            }
            
            if (paymentCard.getMerchantAccount() == null) {
                paymentCard.setMerchantAccount(procTx.getMerchantAccount());
            }
            
            paymentCard.setProcessorTransaction(procTx);
            paymentCard.setProcessorTransactionType(procTx.getProcessorTransactionType());
            paymentCard.setIsApproved(procTx.isIsApproved());
            
            this.paymentCardService.savePaymentCard(paymentCard);
        }
    }
    
    private void handleExpired(final ProcessorTransaction procTx) {
        final ProcessorTransactionType uncloseable =
                this.processorTransactionTypeService.findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE);
        procTx.setProcessorTransactionType(uncloseable);
        procTx.setIsApproved(false);
    }
    
    private void printInfoLog(final CPSProcessData cpsProcessData, final Long processorTxId, final String chargeToken,
        final ProcessorTransaction procTx) {
        final StringBuilder bdr = new StringBuilder().append("In CPSProcessData id: ").append(cpsProcessData.getId())
                .append(" processorTransaction id: ").append(processorTxId).append(", chargeToken: ").append(chargeToken)
                .append(", do we have a ProcessorTransaction record: ").append(procTx);
        LOG.info(bdr.toString());
    }
    
    @Override
    public final CPSProcessData find(final String terminalToken, final String chargeToken) {
        return (CPSProcessData) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("CPSProcessData.findByTerminalTokenAndChargeToken",
                                                     new String[] { "terminalToken", CardProcessingConstants.CHARGE_TOKEN },
                                                     new Object[] { terminalToken, chargeToken }, false);
    }
    
    private boolean hasAuthorizationNumber(final boolean isApproved, final String authNumber, final String authNumberPlaceholder) {
        return isApproved && StringUtils.isNotBlank(authNumber) && StringUtils.isNotBlank(authNumberPlaceholder)
               && authNumberPlaceholder.contains(WebCoreConstants.NOT_APPLICABLE);
    }
    
    @Override
    public final void captureChargeResults(final boolean isApproved, final CPSProcessData cpsProcessData) {
        final CPSData cpsData = this.cpsDataService.findCPSDataByChargeTokenAndRefundTokenIsNull(cpsProcessData.getChargeToken());
        if (cpsData == null) {
            this.entityDao.saveOrUpdate(cpsProcessData);
            LOG.warn("chargeToken doesn't exist. CPSProcessData: " + cpsProcessData);
            return;
        }
        
        final ProcessorTransaction procTx =
                this.processorTransactionService.findProcessorTransactionById(cpsData.getProcessorTransaction().getId(), true);
        if (procTx == null) {
            LOG.warn("processorTransactionId doesn't exist, ProcessorTransaction: " + procTx);
            return;
        }
        
        // e.g. N/A:163 -> 7VJGY6:163
        if (hasAuthorizationNumber(isApproved, cpsProcessData.getAuthorizationNumber(), procTx.getAuthorizationNumber())) {
            procTx.setAuthorizationNumber(procTx.getAuthorizationNumber().replace(WebCoreConstants.NOT_APPLICABLE,
                                                                                  cpsProcessData.getAuthorizationNumber()));
        } else {
            procTx.setAuthorizationNumber(StringUtils.isBlank(cpsProcessData.getAuthorizationNumber()) ? WebCoreConstants.NOT_APPLICABLE
                    : cpsProcessData.getAuthorizationNumber());
        }
        procTx.setCardType(cpsProcessData.getCardType());
        
        procTx.setProcessorTransactionId(WebCoreUtil.returnNAIfBlank(cpsProcessData.getProcessorTransactionId()));
        
        procTx.setReferenceNumber(WebCoreUtil.returnNAIfBlank(cpsProcessData.getReferenceNumber()));
        procTx.setProcessingDate(cpsProcessData.getProcessedGMT() == null ? cpsProcessData.getCreatedGMT() : cpsProcessData.getProcessedGMT());
        procTx.setLast4digitsOfCardNumber(cpsProcessData.getLast4Digits());
        
        final ProcessorTransactionType ptType = cpsProcessData.isExpired()
                ? this.processorTransactionService.getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE)
                : this.processorTransactionService
                        .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS);
        procTx.setProcessorTransactionType(ptType);

        /**
         * Disabling this line because when IRIS receives an Expired transaction from CPS and 
         * isApproved == 0, Iris gets misled and shows the transaction as Offline in the reports.  
         * Iris uses a set of fields and tables to determine that a transaction, in fact, is approved.  
         */
        //        procTx.setIsApproved(isApproved);
        this.entityDao.saveOrUpdate(procTx);
        
        updatePaymentCard(procTx);
    }
    
    @Override
    public final List<ProcessorTransaction> findAuthorizedTransactionsForCaptureChargeResults(final Date maxReceiveTime) {
        final Criteria crit = buildAuthorizedTransactionsCriteria(maxReceiveTime);
        crit.add(Restrictions.eq("cardType", WebCoreConstants.N_A_STRING));
        crit.add(Restrictions.eq("processorTransactionId", WebCoreConstants.N_A_STRING));
        crit.add(Restrictions.eq("referenceNumber", WebCoreConstants.N_A_STRING));
        return crit.list();
    }
    
    @Override
    public final Criteria buildAuthorizedTransactionsCriteria(final Date maxReceiveTime) {
        Criteria crit = this.entityDao.createCriteria(ProcessorTransaction.class);
        crit = crit.createAlias(CardProcessingConstants.PROC_TRANS_TYPE, CardProcessingConstants.PROC_TRANS_TYPE);
        crit = crit.createAlias("pointOfSale", "ptPointOfSale");
        crit = crit.createAlias("ptPointOfSale.customer", CardProcessingConstants.CUSTOMER, CriteriaSpecification.LEFT_JOIN)
                .setFetchMode(CardProcessingConstants.CUSTOMER, FetchMode.JOIN);
        // 1 = Auth PlaceHolder - PreAuth Authorized but not yet settled.
        crit.add(Restrictions.eq("processorTransactionType.id", new Integer(1)));
        crit.add(Restrictions.lt("processingDate", maxReceiveTime));
        return crit;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    public final void setPaymentCardService(final PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }
    
    public final void setCreditCardTypeService(final CreditCardTypeService creditCardTypeService) {
        this.creditCardTypeService = creditCardTypeService;
    }
    
    public final void setCpsDataService(final CPSDataService cpsDataService) {
        this.cpsDataService = cpsDataService;
    }
    
    public final void setCpsRefundDataService(final CPSRefundDataService cpsRefundDataService) {
        this.cpsRefundDataService = cpsRefundDataService;
    }
}
