package com.digitalpaytech.service.cps;

import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.domain.ProcessorTransaction;

import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;

public interface CPSProcessDataService {
    
    void manageCPSProcessData(CPSProcessData cpsProcessData);
    
    CPSProcessData find(String terminalToken, String chargeToken);
    
    void captureChargeResults(boolean isApproved, CPSProcessData cpsProcessData);
    
    List<ProcessorTransaction> findAuthorizedTransactionsForCaptureChargeResults(Date maxReceiveTime);
    
    Criteria buildAuthorizedTransactionsCriteria(Date maxReceiveTime);
}
