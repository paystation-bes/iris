package com.digitalpaytech.service.cps.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.cps.CPSRefundDataService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("cpsRefundDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSRefundDataServiceImpl implements CPSRefundDataService {
    private static final Logger LOG = Logger.getLogger(CPSRefundDataServiceImpl.class);
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CPSRefundData> findByChargeToken(final String chargeToken) {
        return (List<CPSRefundData>) this.entityDao.findByNamedQueryAndNamedParam("CPSRefundData.findByChargeToken", "chargeToken", chargeToken);
        
    }
    
    @Override
    public CPSRefundData findByChargeTokenAndRefundToken(final String chargeToken, final String refundToken) {
        return (CPSRefundData) this.entityDao.findUniqueByNamedQueryAndNamedParam("CPSRefundData.findByRefundToken", "refundToken", refundToken,
                                                                                  true);
    }
    
    @Override
    public void save(final CPSRefundData cpsRefundData) {
        if (findByChargeTokenAndRefundToken(cpsRefundData.getChargeToken(), cpsRefundData.getRefundToken()) == null) {
            this.entityDao.save(cpsRefundData);
        }
    }
    
    @Override
    public void delete(final CPSRefundData cpsRefundData) {
        this.entityDao.delete(cpsRefundData);
    }
    
    @Override
    public void manageCPSRefundData(final CPSRefundData cpsRefundData) {
        if (cpsRefundData != null) {
            handleOfflineRefundMessage(cpsRefundData);
        }
    }
    
    @Override
    public void manageCPSRefundData(final List<CPSRefundData> cpsRefundData) {
        if (cpsRefundData != null && !cpsRefundData.isEmpty()) {
            cpsRefundData.forEach(refund -> handleOfflineRefundMessage(refund));
        }
    }
    
    private void handleOfflineRefundMessage(final CPSRefundData cpsRefundData) {
        
        final CPSData chargeCpsData = this.cpsDataService.findCPSDataByChargeTokenAndRefundTokenIsNull(cpsRefundData.getChargeToken());
        cpsRefundData.setCreatedGMT(new Date());
        
        if (chargeCpsData == null) {
            save(cpsRefundData);
        } else {
            final String chargeToken = chargeCpsData.getChargeToken();
            final CPSData duplicate = this.cpsDataService.findCPSDataByChargeTokenAndRefundToken(chargeToken, cpsRefundData.getRefundToken());
            if (duplicate == null) {
                final ProcessorTransaction chargeProcessorTransaction =
                        this.processorTransactionService.findProcessorTransactionById(chargeCpsData.getProcessorTransaction().getId(), true);
                if (chargeProcessorTransaction == null || isSNFNotApproved(chargeProcessorTransaction)) {
                    LOG.warn(String.format(
                                           "Processing refund for chargeToken [%s] processorTransactionId [%s] hasn't been created or is still offline",
                                           chargeToken, cpsRefundData.getRefundToken()));
                    save(cpsRefundData);
                } else {
                    if (!isRefunded(chargeProcessorTransaction)) {
                        updateChargeProcessorTransaction(chargeProcessorTransaction);
                    }
                    this.processorTransactionService.updateProcessorTransaction(chargeProcessorTransaction);
                    
                    final ProcessorTransaction refund = createRefundProcessorTransaction(cpsRefundData, chargeProcessorTransaction);
                    this.processorTransactionService.saveProcessorTransaction(refund);
                    final CPSData refundCpsData = new CPSData(refund, cpsRefundData.getChargeToken(), cpsRefundData.getRefundToken());
                    this.cpsDataService.save(refundCpsData);

                    //TODO Commented this for now until Credit Card Transaction report sql has been fixed.
                    // this.paymentCardService.saveRefundPaymentCard(refund);
                    
                    delete(cpsRefundData);
                }
            } else {
                LOG.info(String.format("Duplicate refund ignored for chargeToken [%s] refundToken [%s]", chargeToken,
                                       cpsRefundData.getRefundToken()));
            }
        }
    }
    
    private boolean isSNFNotApproved(final ProcessorTransaction processorTransaction) {
        return CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS == processorTransaction.getProcessorTransactionType()
                .getId()
               && !processorTransaction.isIsApproved() && (StringUtils.isBlank(processorTransaction.getAuthorizationNumber())
                                                           || WebCoreConstants.N_A_STRING.equals(processorTransaction.getAuthorizationNumber()));
    }
    
    private ProcessorTransaction createRefundProcessorTransaction(final CPSRefundData cpsRefundData,
        final ProcessorTransaction processorTransaction) {
        final ProcessorTransaction refund = new ProcessorTransaction(processorTransaction);
        final Date now = new Date();
        if (cpsRefundData.isExpired()) {
            refund.setProcessorTransactionType(this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_EXPIRED_REFUND));
            refund.setIsApproved(false);
        } else {
            refund.setProcessorTransactionType(this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND));
            refund.setIsApproved(true);
        }
        refund.setAuthorizationNumber(StandardConstants.STRING_EMPTY_STRING);
        refund.setProcessingDate(now);
        refund.setCreatedGmt(now);
        refund.setProcessorTransactionId(cpsRefundData.getProcessorTransactionId());
        refund.setReferenceNumber(cpsRefundData.getReferenceNumber());
        refund.setAmount(cpsRefundData.getAmount());
        return refund;
        
    }
    
    private boolean isRefunded(final ProcessorTransaction processorTransaction) {
        final int id = processorTransaction.getProcessorTransactionType().getId();
        return ArrayUtils.contains(CardProcessingConstants.REFUNDED_PROCESSOR_TRANSACTION_TYPE_IDS, id);
    }
    
    private void updateChargeProcessorTransaction(final ProcessorTransaction processorTransaction) {
        final int updatedTypeId;
        switch (processorTransaction.getProcessorTransactionType().getId()) {
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_REFUNDS;
                break;
            case CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_REFUNDS;
                break;
            default:
                updatedTypeId = CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS;
                break;
        }
        
        processorTransaction.setProcessorTransactionType(this.processorTransactionTypeService.findProcessorTransactionType(updatedTypeId));
    }
}
