package com.digitalpaytech.service.cps.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSReversalData;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.cps.CPSReversalDataService;

@Service("cpsReversalDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSReversalDataServiceImpl implements CPSReversalDataService {
    private static final Logger LOG = Logger.getLogger(CPSReversalDataServiceImpl.class);
    private static final String ID_OUTPUT = "CPSReversalData created, id: ";
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final void save(final CPSReversalData cpsReversalData) {
        this.entityDao.save(cpsReversalData);
    }
    
    @Override
    public final void delete(final CPSReversalData cpsReversalData) {
        this.entityDao.delete(cpsReversalData);
    }
    
    @Override
    public final void update(final CPSReversalData cpsReversalData) {
        this.entityDao.update(cpsReversalData);
    }
    
    @Override
    public final void createCPSReversalData(final ProcessorTransaction processorTransaction, 
                                            final String chargeToken,
                                            final String refundToken,
                                            final CPSReversalData.ReversalType reversalType, 
                                            final Exception e) {

        @SuppressWarnings("unchecked")
        final List<CPSReversalData> list = this.entityDao.findByNamedQueryAndNamedParam("CPSReversalData.findByChargeTokenAndRefundToken", 
                                                                                        new String[] { "chargeToken", "refundToken" }, 
                                                                                        new Object[] { chargeToken, refundToken });
        if (list != null && !list.isEmpty()) {
            LOG.info("CPSReversalData with chargeToken: " + chargeToken + ", refundToken: " + refundToken + " already exists!");
            return;
        }
        
        final CPSReversalData cpsReversalData 
            = new CPSReversalData(chargeToken, refundToken, this.getHTTPStatus(e), e.getMessage(), reversalType, new Date());
        this.save(cpsReversalData);
        LOG.info(ID_OUTPUT + cpsReversalData.getId() + ", chargeToken: " + chargeToken + ", refundToken: " + refundToken 
                 + ", processorTransaction: " + processorTransaction);
    }
    
    private int getHTTPStatus(final Exception ex) {
        int status = HttpStatus.INTERNAL_SERVER_ERROR.value();
        Throwable e = ex;
        while (e.getCause() != null) {
            e = e.getCause();
            if (e instanceof CommunicationException) {
                final CommunicationException communicationException = (CommunicationException) e;
                status = communicationException.getResponseStatus();
            }
        }
        return status;
    }
}
