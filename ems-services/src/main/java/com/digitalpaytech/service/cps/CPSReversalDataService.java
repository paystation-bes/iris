package com.digitalpaytech.service.cps;

import com.digitalpaytech.domain.CPSReversalData;
import com.digitalpaytech.domain.CPSReversalData.ReversalType;
import com.digitalpaytech.domain.ProcessorTransaction;

public interface CPSReversalDataService {
    
    void save(CPSReversalData cpsReversalData);
    
    void delete(CPSReversalData cpsReversalData);
    
    void update(CPSReversalData cpsReversalData);
    
    void createCPSReversalData(ProcessorTransaction processorTransaction, String chargeToken, String refundToken, ReversalType reversalType, Exception e);
}
