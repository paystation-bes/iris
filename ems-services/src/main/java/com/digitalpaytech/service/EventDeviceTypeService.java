package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.EventDeviceType;

public interface EventDeviceTypeService {
    public List<EventDeviceType> loadAll();
    public Map<Integer, EventDeviceType> getEventDeviceTypesMap();
    public String getText(int eventDeviceTypeId);
    public String getTextNoSpace(int eventDeviceTypeId);
    public EventDeviceType findEventDeviceType(String name);
}
