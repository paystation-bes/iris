package com.digitalpaytech.service;

import java.util.List;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;

public interface ThirdPartyServiceAccountService {
    public List<ThirdPartyServiceAccount> findByCustomerId(Integer customerId);
    
    public ThirdPartyServiceAccount getThirdPartyServiceAccountByCustomerAndType(Integer customerId, String type);
}
