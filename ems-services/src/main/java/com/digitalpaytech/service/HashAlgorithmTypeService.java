package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.SystemException;

public interface HashAlgorithmTypeService {
    HashAlgorithmType findHashAlgorithmType(Integer id);
    List<HashAlgorithmType> loadAll();
    Map<Integer, HashAlgorithmData> findHashAlgorithmNotSigningMap();
    Map<Integer, HashAlgorithmData> findForInternalKeyRotation();
    HashAlgorithmData findForMessageDigest(Integer hashAlgorithmTypeId);
    void reloadHashAlgorithmNotSigningMaps() throws SystemException;
    void loadHashAlgorithmNotSigningMaps() throws SystemException;
    List<String> findHashAlgorithmNames(final boolean isForSigning);
    List<String> findActiveHashAlgorithmNames(final boolean isForSigning);
    List<String> findActiveSigningHashAlgorithmNames();
    String findAlgorithmName(int hashAlgorithmTypeId);
}
