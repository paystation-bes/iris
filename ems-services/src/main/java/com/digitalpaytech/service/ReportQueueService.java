package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo;
import com.digitalpaytech.util.RandomKeyMapping;

public interface ReportQueueService {
    
    public void saveReportQueue(ReportQueue reportQueue);
    
    public void updateReportQueue(ReportQueue reportQueue);
    
    public List<ReportQueue> findAll();
    
    public List<ReportQueue> findQueuedReports();
    
    public ReportQueue findQueuedReportWithLock(String clusterName);
    
    public List<ReportQueue> findQueuedReportsByUserAccountId(int userAccountId);
    
    public List<ReportQueue> findQueuedReportsByCustomerId(int customerId);
    
    public String fixStalledReports(String clusterName, Date idleTime);
    
    public boolean cancelReport(int reportQueueId);
    
    public ReportQueue getReportQueue(int id);
    
    public int getLaunchedReportCount();
    
    public int getScheduledReportCount();
    
    public ReportQueue findRecentlyQueuedReport(int reportDefinitionId);
    
    public List<ReportQueue> findReportQueueByCustomerIdAndUserAccountId(int userAccountId, int customerId);
    
    public List<ReportQueueInfo.QueueInfo> findQueuedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone);
    
}
