package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.RestAccountType;

public interface RestAccountTypeService {
    public List<RestAccountType> loadAll();
    public Map<Integer, RestAccountType> getRestAccountTypesMap();
    public String getText(int restAccountTypeId);
    public RestAccountType findRestAccountType(String name);
}
