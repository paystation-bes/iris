package com.digitalpaytech.service;

import com.digitalpaytech.domain.PosEventHistory;

public interface PosEventHistoryService {
    
    void savePosEventHistory(PosEventHistory posEventHistory);
    
    PosEventHistory findActivePosEventHistoryById(Long posEventHistoryId);
    
    PosEventHistory findById(Long id);

    PosEventHistory findByIdWithUserName(Long id);
    
}
