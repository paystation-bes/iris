package com.digitalpaytech.service;

import com.digitalpaytech.domain.SmsTransactionLog;

public interface SmsTransactionLogService {
    
    public void saveSmsTransactionLog(SmsTransactionLog smsTransactionLog);
}
