package com.digitalpaytech.service;

import com.digitalpaytech.domain.SubscriptionType;

import java.util.List;

public interface SubscriptionTypeService {
    public List<SubscriptionType> loadAll();
    public SubscriptionType findById(Integer id);
}
