package com.digitalpaytech.service;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;

public interface ArchiveService {
    public void archiveRawSensorData(RawSensorData rawSensorData);
    public void handleRawSensorDataTransformationError(Exception e, EventData event, RawSensorData sensor, int threadId);
}
