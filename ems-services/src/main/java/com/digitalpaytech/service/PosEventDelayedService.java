package com.digitalpaytech.service;

import java.util.Collection;

import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventDelayed;

public interface PosEventDelayedService {
    
    Collection<PosEventDelayed> findByDelayedGmtPast();
    
    void delete(PosEventDelayed delayedEvent);
    
    void save(PosEventDelayed delayedEvent);
    
    Collection<PosEventDelayed> findByCustomerAlertTypeAndPointOfSaleAndEventTypeId(CustomerAlertType customerAlertType, PointOfSale pointOfSale,
        byte eventTypeId);

    void deleteByCustomerAlertType(CustomerAlertType customerAlertType);
    
}
