package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationDay;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.util.RandomKeyMapping;

public interface LocationService {
    
    List<Location> findLocationsByCustomerId(Integer customerId);
    
    List<Location> findChildLocationsByCustomerId(int customerId);
    
    List<Location> findParentLocationsByCustomerId(int customerId);
    
    List<Location> getLocationsByParentLocationId(int parentLocationId);
    
    List<LocationDay> findLocationDayByLocationId(int locationId);
    
    Location findLocationByCustomerIdAndName(int customerId, String name);
    
    Location findLocationById(int locationId);
    
    List<Integer> findAllParentIds(int locationId);
    
    Location findUnassignedLocationByCustomerId(int customerId);
    
    Location findLocationByPointOfSaleId(int pointOfSaleId);
    
    boolean locationContainsPermitsById(int locationId);
    
    void deleteLocationById(int locationId);
    
    void updateLocationOpenCloseTime(int userAccountId, int locationId, int dayOfWeek, Integer openTime, Integer closeTime,
                                            Integer closeTimeOfPreviousDay, boolean open, boolean closed);
    
    void saveOrUpdateLocation(Location location);
    
    List<List<ExtensibleRate>> getValidRatesByLocationId(Integer locationId);
    
    List<ExtensibleRateDayOfWeek> getValidRatesDayOfWeekByLocationId(Integer locationId);
    
    List<List<ParkingPermission>> getValidParkingPermissionsByLocationId(Integer locationId);
    
    List<ParkingPermissionDayOfWeek> getValidParkingPermissionsDayOfWeekByLocationId(Integer locationId);
    
    ExtensibleRate findExtensibleRate(int extensibleRateId);
    
    ParkingPermission findParkingPermission(int parkingPermissionId);
    
    void saveOrUpdateExtensibleRate(UserAccount userAccount, ExtensibleRate extensibleRate);
    
    void saveOrUpdateParkingPermission(UserAccount userAccount, ParkingPermission parkingPermission);
    
    void deleteExtensibleRateById(int extensibleRateId);
    
    void deleteLocationOpenByLocationId(int locationId);
    
    void deleteParkingPermissionById(int parkingPermissionId);
    
    ExtensibleRate evictExtensibleRate(ExtensibleRate extensibleRate);
    
    ParkingPermission evictParkingPermission(ParkingPermission parkingPermission);
    
    List<ExtensibleRate> findOverlappedExtensibleRate(ExtensibleRate extensibleRate, List<Integer> daysOfWeek);
    
    List<ParkingPermission> findOverlappedParkingPermission(ParkingPermission parkingPermission, List<Integer> daysOfWeek);
    
    List<Location> findLocationsByCustomerIdAndName(int customerId, String name);
    
    List<Location> findLocationsWithPointOfSalesByCustomerId(Integer customerId);
    
    LocationTree getLocationTreeByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly, RandomKeyMapping keyMapping);
    
    LocationTree getLocationPayStationTreeByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly, boolean showVisibleOnly,
                                                              RandomKeyMapping keyMapping);
    
    LocationTree getRoutePayStationTreeByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly, boolean showVisibleOnly,
                                                           RandomKeyMapping keyMapping);
    
    List<Location> getLowestLocationsByCustomerIdAndName(int customerId, String locationName);
    
    List<FilterDTO> getLocationFiltersByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly, RandomKeyMapping keyMapping);
    
    List<Location> getLocationByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly);
    
    List<Location> getDetachedLocationsByLowerCaseNames(int customerId, Collection<String> names);
    
    Location getParentLocationByCustomerIdLocationName(int customerId, String locationName);
}
