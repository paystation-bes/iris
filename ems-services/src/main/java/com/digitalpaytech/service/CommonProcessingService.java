package com.digitalpaytech.service;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.Reversal;

public interface CommonProcessingService {
    public String getDestinationUrlString(Processor processor);
    public String getNewReferenceNumber(MerchantAccount merchantAccount);
    public void sendCardProcessingAdminErrorAlert(String subject, String message, Exception e);
    public boolean isReversalExpired(Reversal reversal);
    public String convertWildCards(String value);
    public boolean isTestMode(int merchantAccountId);
    public boolean isProcessorTestMode(int processorId);
    public Processor getProcessor(int processorId);

}
