package com.digitalpaytech.service;

import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.dto.customeradmin.SystemNotification;

public interface CustomerAdminService {
    
    GlobalConfiguration findGlobalPreferences(Customer customer, GlobalConfiguration globalConfiguration);
    
    GlobalConfiguration findGlobalPreferencesTimeZoneOnly(Customer customer, GlobalConfiguration globalConfiguration);
    
    SystemNotification findSystemNotifications();
    
    List<CustomerProperty> getCustomerPropertyByCustomerId(int customerId);
    
    CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(int customerId, int customerPropertyType);
    
    List<CustomerPropertyType> getCustomerPropertyTypes();
    
    Set<CustomerProperty> findCustomerPropertyByCustomerId(int customerId);
    
    List<Customer> findAllChildCustomers(int parentCustomerId);
    
    List<TimezoneVId> getTimeZones();
    
    List<FilterDTO> getQuerySpacesByOptions();
    
    List<CardType> getCardTypes();
    
    List<UserAccount> getUserAccountsByCustomerId(int customerId);
    
    List<UserAccount> getUserAccountsWithAliasUsersByCustomerId(int customerId);
    
    Customer findCustomerByCustomerId(int customerId);
    
    void saveOrUpdateCustomerProperty(CustomerProperty customerProperty);
    
    void saveOrUpdatePointOfSale(PointOfSale pointOfSale);
    
    void saveOrUpdatePaystation(Paystation payStation);
    
    void saveOrUpdateCustomerCardType(CustomerCardType customerCardType);
    
    void saveOrUpdatePointOfSaleStatus(PosStatus pointOfSaleStatus);
    
    boolean hasActiveCustomerCard(CustomerCardType customerCardType);
    
    CustomerCardType findCustomerCardTypeById(int customerCardTypeId);
    
    void setCardTypeService(CardTypeService cardTypeService);
    
    void setCardTypes(List<CardType> cardTypes);
}
