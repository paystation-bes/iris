package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;

public interface CustomerBadCardService {
    Collection<CustomerBadCard> getCustomerBadCards(Integer customerId, String cardNumber, Integer cardTypeId);
    
    Collection<CustomerBadCard> findCustomerBadCards(CustomerBadCardSearchCriteria criteria);
    
    void createCustomerBadCard(CustomerBadCard newCustomerBadCard);
    
    void createCustomerBadCard(Map<String, CustomerBadCard> badCards);
    
    void deleteCustomerBadCard(CustomerBadCard customerBadCard);
    
    int deleteByCustomerId(Integer customerId, String statusKey) throws BatchProcessingException;
    
    int deleteByCustomerIdPaginated(Integer customerId, Date deleteTimeStamp, Map<String, Object> context) throws BatchProcessingException;
    
    void updateCustomerBadCard(CustomerBadCard customerBadCard);
    
    void updateLast4DigitsOfCardNumber(Integer id, Short last4DigitsOfCardNumbe);
    
    void updateCustomerBadCards(Collection<CustomerBadCard> customerBadCards);
    
    CustomerBadCard findCustomerBadCardById(Integer id, boolean isEvict);
    
    Collection<CustomerBadCard> findCustomerBadCardByCustomerId(Integer customerId);
    
    Future<Integer> processBatchUpdateAsync(String statusKey, int customerId, Collection<CustomerBadCard> badCardList, String mode, int userId)
        throws BatchProcessingException;
    
    int processBatchUpdateWithStatus(String statusKey, int customerId, Collection<CustomerBadCard> badCardList, String mode, int userId)
        throws BatchProcessingException;
    
    int processBatchUpdate(int customerId, Collection<CustomerBadCard> badCardList, String mode, int userId, BatchProcessStatus status)
        throws BatchProcessingException;
    
    int getTotalBadCreditCard(String cardData);
    
    Collection<CustomerBadCard> getBadCreditCardByCardData(String cardData, int howManyRows, boolean isEvict);
    
    Collection<CustomerBadCard> findNoExpiredBadCreditCard(Integer customerId, Integer customerCardTypeId);
    
    int countByCustomerCardType(Collection<Integer> customerCardTypeIds);
}
