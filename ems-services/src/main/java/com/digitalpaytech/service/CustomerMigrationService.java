package com.digitalpaytech.service;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEmail;
import com.digitalpaytech.dto.CustomerMigrationSearchCriteria;
import com.digitalpaytech.dto.customermigration.CustomerMigrationData;
import com.digitalpaytech.dto.customermigration.RecentBoardedMigratedCustomerList;

/**
 * This service interface defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
public interface CustomerMigrationService {
    
    Serializable updateCustomerMigration(CustomerMigration customerMigration, List<String> emailAddress);
    
    void updateCustomerMigration(CustomerMigration customerMigration);
    
    CustomerMigration findCustomerMigrationByCustomerId(Integer customerId);
    
    List<CustomerMigration> findScheduledCustomerMigration(int batchSize);
    
    boolean isMigrationInProgress();
    
    CustomerMigration findCustomerMigrationById(Integer customerMigrationId);
    
    CustomerMigration findNextAvailableCustomer();
    
    CustomerMigrationData getCurrentCustomerMigrationData();
    
    CustomerMigrationData getCurrentPaystationMigrationStatus();
    
    List<CustomerMigrationData> getCustomerMigrationData();
    
    List<CustomerMigrationData> getCustomerPaystationMigrationData();
    
    List<CustomerMigration> findThisWeeksSchedule();
    
    List<CustomerMigration> findCustomerMigrationByCriteria(final CustomerMigrationSearchCriteria criteria, final String timeZone);
    
    List<RecentBoardedMigratedCustomerList> getRecentCustomerActivityList(HttpServletRequest request, String sortOrder, String sortItem,
        Integer page, boolean isBoarded);
    
    String getMigrationStatusText(byte customerMigrationStatusTypeId);
    
    int getMigrationStatusInt(byte customerMigrationStatusTypeId);
    
    List<CustomerMigrationEmail> findCustomerMigrationEmails(Integer customerMigrationId);
    
}
