package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.PointOfSale;

public interface ActivePermitService {
    
    ActivePermit getActivePermitByCustomerAndLicencePlate(int customerId, String plateNumberNumber, int querySpaceBy);
    
    ActivePermit getActivePermitByLocationAndLicencePlate(int locationId, String plateNumberNumber);
    
    List<ActivePermit> getValidByLocationId(Integer locationId, Date reportDate, int startPosition, int recordsReturned);
    
    int getValidByLocationIdTotalCount(Integer locationId, Date reportDate);
    
    List<ActivePermit> getValidByCustomerId(Integer customerId, Date reportDate, int startPosition, int recordsReturned, int querySpaceBy);
    
    int getValidByCustomerIdTotalCount(Integer customerId, Date reportDate, int querySpaceBy);
    
    List<ActivePermit> getPaystationSettingsFromLast14Days(int customerId);
    
    List<ActivePermit> getActivePermitByCustomerAndSpaceRange(int customerId, int startSpace, int endSpace, int querySpaceBy);
    
    List<ActivePermit> getActivePermitByLocationAndSpaceRange(int customerId, int locationId, int startSpace, int endSpace);
    
    List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(int customerId, String paystationSettingName, int startSpace, int endSpace,
        int querySpaceBy);
    
    List<ActivePermit> getActivePermitByPosListAndSpaceRange(int customerId, List<PointOfSale> posList, int startSpace, int endSpace, int querySpaceBy);
    
    ActivePermit getActivePermitByCustomerAndAddTimeNumber(int customerId, int addTimeNumber);
    
    ActivePermit getActivePermitByCustomerAndAddTimeNumberOrderByLatestExpiry(int customerId, int addTimeNumber);
    
    ActivePermit getActivePermitByLocationAndAddTimeNumber(int customerId, int locationId, int addTimeNumber);
    
    ActivePermit getActivePermitByLocationAndAddTimeNumberOrderByLatestExpiry(int customerId, int locationId, int addTimeNumber);
    
    ActivePermit getActivePermitByCustomerAndSpaceNumber(int customerId, int spaceNumber);
    
    ActivePermit getActivePermitByCustomerAndSpaceNumberOrderByLatestExpiry(int customerId, int spaceNumber);
    
    ActivePermit getActivePermitByLocationAndSpaceNumber(int customerId, int locationId, int spaceNumber);
    
    ActivePermit getActivePermitByLocationAndSpaceNumberOrderByLatestExpiry(int customerId, int locationId, int spaceNumber);
    
    ActivePermit getActivePermitByPaystationSettingAndAddTimeNumber(int customerId, String paystationSettingName, int addTimeNumber);
    
    ActivePermit getActivePermitByPaystationSettingAndSpaceNumber(int customerId, String paystationSettingName, int spaceNumber);
    
    ActivePermit findActivePermitByOriginalPermitId(Long originalPermitId);
    
    ActivePermit findActivePermitByLocationIdAndLicencePlateNumber(Integer permitLocationId, String licencePlateNumber);
    
    ActivePermit findActivePermitByLocationIdAndLicencePlateNumberOrderByLatestExpiry(Integer permitLocationId, String licencePlateNumber);
    
    ActivePermit findActivePermitByCustomerIdAndLicencePlateNumber(Integer customerId, String licencePlateNumber);
    
    ActivePermit findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry(Integer customerId, String licencePlateNumber);
    
    void deleteActivePermit(ActivePermit activePermit);
}
