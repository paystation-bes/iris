package com.digitalpaytech.service;

public interface ReversalArchiveService {
    void cleanupCardData();
    
    void cleanupCardDataForMigratedCustomers();
}
