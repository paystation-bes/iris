package com.digitalpaytech.service;

import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.CustomerSubscription;

public interface CustomerSubscriptionService {
    List<CustomerSubscription> findCustomerSubscriptionsByCustomerId(int customerId, boolean cacheable);
    
    Set<CustomerSubscription> findByCustomerId(Integer customerId, boolean cacheable);
    
    List<CustomerSubscription> findActiveCustomerSubscriptionByCustomerId(int customerId, boolean cacheable);
    
    void updateLicenseUsedForCustomer(Integer customerId, Integer subscriptionTypeId, int licenseUsed);
    
    void updateLicenseCountAndUsedForCustomer(Integer customerId, Integer subscriptionTypeId, int licenseCount, int licenseUsed,
        int userAccountId);
    
    void updateLicenseCountForCustomer(Integer customerId, Integer subscriptionTypeId, int licenseCount);
    
    long countMobileLicensesForCustomer(Integer customerId);
    
    boolean hasOneOfSubscriptions(Integer customerId, Integer... subscriptionTypeId);
}
