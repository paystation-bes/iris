package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.PosSensorInfo;

public interface PosSensorInfoService {
    
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdTypeId(int pointOfSaleId, int typeId);
    
    public void archiveOldPosSensorInfoData(int archiveIntervalDays, int archiveBatchSize, String archiveServerName);
    
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndDateRange(Set<Integer> pointOfSaleIdList, int sensorInfoTypeId, Date fromDate, Date toDate, int limit);

    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(Integer pointOfSaleId, Integer sensorInfoTypeId, Date fromDate);

    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(Integer pointOfSaleId, Integer sensorInfoTypeId, Date fromDate);

}
