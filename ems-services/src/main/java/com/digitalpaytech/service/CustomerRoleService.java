package com.digitalpaytech.service;

import java.util.Collection;

import com.digitalpaytech.domain.CustomerRole;

public interface CustomerRoleService {

    public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId);
}
