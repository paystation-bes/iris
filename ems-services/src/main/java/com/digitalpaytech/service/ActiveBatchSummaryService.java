package com.digitalpaytech.service;

import com.digitalpaytech.domain.ActiveBatchSummary;

public interface ActiveBatchSummaryService {
    
    ActiveBatchSummary findByBatchNoAndMerchantId(short batchNo, int merchantId);
}
