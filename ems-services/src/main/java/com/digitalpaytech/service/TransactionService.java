package com.digitalpaytech.service;

import java.util.List;
import java.util.Properties;

import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseCollectionCancelled;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.exception.PaystationCommunicationException;

public interface TransactionService {    
    
    List<Object> processTransactionWithSmsAlert(TransactionData transactionData, EmbeddedTxObject eEmbeddedTxObject) 
            throws DuplicateTransactionRequestException, CryptoException;
    
    Properties getMappingProperties();
    
    int getMappingPropertiesValue(String key);
    
    void createTransaction(Permit permit, Purchase purchase, PaymentCard paymentCard, ProcessorTransaction processorTransaction);
    
    void cancelTransaction(Permit permit, Purchase purchase, PaymentCard paymentCard, ProcessorTransaction processorTransaction, CustomerCard customerCard,
                           Coupon coupon, PurchaseCollectionCancelled purchaseCollectionCancelled) throws PaystationCommunicationException;
    
    List<Object> processTransaction(TransactionData transactionData, boolean isManualSave,
        TransactionDto transDto, EmbeddedTxObject embeddedTxObject) throws DuplicateTransactionRequestException, CryptoException;
}
