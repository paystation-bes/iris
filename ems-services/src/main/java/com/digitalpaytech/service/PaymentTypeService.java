package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.PaymentType;

public interface PaymentTypeService {
    public List<PaymentType> loadAll();
    public Map<Byte, PaymentType> getPaymentTypesMap();
    public String getText(int paymentTypeId);
}
