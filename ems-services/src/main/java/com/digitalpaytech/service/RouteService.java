package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;

public interface RouteService {
    
    public Collection<Route> findRoutesByCustomerId(int customerId);
    
    public Collection<Route> findRoutesByCustomerIdAndName(int customerId, String name);
    
    public Collection<Route> findRoutesByCustomerIdAndNameUpperCase(int customerId, String name);
    
    public Route findRouteById(Integer id);
    
    public Route findRouteByIdAndEvict(Integer id);
    
    public void saveRouteAndRoutePOS(Route route);
    
    public void updateRouteAndRoutePOS(Route route);
    
    public List<Paystation> findPayStationsByRouteId(int routeId);
    
    public List<PointOfSale> findPointOfSalesByRouteId(int routeId, boolean includeArchived);
    
    public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId);
    
    public List<Route> findRoutesByPointOfSaleId(int pointOfSaleId);

    public List<Route> findRoutesByPointOfSaleIdAndRouteTypeId(int pointOfSaleId, int routeTypeId);

    public void deleteRoute(Integer routeId);
    
    public RoutePOS findRoutePOSByIdAndPointOfSaleId(Integer routeId, Integer pointOfSaleId);
    
    public Collection<Route> findRoutesByCustomerIdAndRouteTypeId(int customerId, int routeTypeId);
    
    public List<RoutePOS> findRoutePOSesByPointOfSaleIdAndRouteTypeId(Integer pointOfSaleId, Integer routeTypeId);
    
    public List<Route> findRoutesWithPointOfSalesByCustomerId(Integer customerId);

    public List<Route> findRoutesWithPointOfSalesByCustomerIdAndRouteType(Integer customerId, Integer routeTypeId);

    //TODO
    List<Integer> findPointOfSaleIdsByRouteId(int routeId, boolean includeArchived);
        
}
