package com.digitalpaytech.service;

import com.digitalpaytech.domain.MobileDevice;

public interface MobileDeviceService {

    public MobileDevice findMobileDeviceByUid(String uid);

    public void saveMobileDevice(MobileDevice mobileDevice);

    public void deleteMobileDevice(MobileDevice mobileDevice);

    public void saveOrUpdateMobileDevice(MobileDevice mobileDevice);

    public boolean getIsMobileDeviceBlocked(Integer customerId, Integer mobileDeviceId);

    public MobileDevice findMobileDeviceById(Integer id);

}
