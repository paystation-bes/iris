package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Processor;

public interface ProcessorService {
    List<Processor> findAll();
    
    Processor findProcessor(Integer id);
    
    Processor findByName(String name);
    
    void updateProcessor(Processor processor);
    
    boolean isProcessorReadyForMigration(Processor processor);
    
    boolean isProcessorReadyForMigration(Processor processor, boolean isLinkSuffixAttached);

    Processor findProcessorReadyForMigration(Integer processorId);
    
    String getCorrespondingLinkProcName(String processorName);
}
