package com.digitalpaytech.service;

import java.util.Set;

import com.digitalpaytech.dto.customeradmin.BannedCard;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;

public interface BannedCardService {
    Set<BannedCard> findBannedCardsFromCardRetry(CustomerBadCardSearchCriteria criteria);
}
