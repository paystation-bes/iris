package com.digitalpaytech.service;

import com.digitalpaytech.domain.MaintenanceSummaryConfig;

public interface MaintenanceSummaryConfigService {
    
    MaintenanceSummaryConfig getMaintenanceSummaryConfigByUserId(int userAccountId);
}
