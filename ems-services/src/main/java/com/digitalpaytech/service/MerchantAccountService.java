package com.digitalpaytech.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.digitalpaytech.client.dto.merchant.CardReaderConfig;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.data.InfoAndMerchantAccount;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.GatewayProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.exception.DuplicateMerchantAccountException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.util.RandomKeyMapping;

@SuppressWarnings("PMD.TooManyMethods")
public interface MerchantAccountService {
    String MERCHANT_ACCOUNT_ID = "merchantAccountId";
    String PROCESSOR_ID = "processorId";
    String CUSTOMER_ID = "customerId";
    String POINT_OF_SALE_ID = "pointOfSaleId";
    String CARD_TYPE_ID = "cardTypeId";
    String FIELD_1 = "field1";
    String FIELD_2 = "field2";
    String FIELD_3 = "field3";
    String FIELD_4 = "field4";
    String FIELD_5 = "field5";
    String FIELD_6 = "field6";
    String MERCHANT_STATUS_TYPE_ID = "merchantStatusTypeId";
    
    List<MerchantAccount> findMerchantAccounts(int customerId);
    
    List<MerchantAccount> findMerchantAccounts(int customerId, boolean forValueCard);
    
    List<MerchantAccount> findUnusedMerchantAccounts(int customerId, boolean forValueCard);
    
    List<MerchantAccount> findValidMerchantAccountsByCustomerId(Integer id);
    
    List<MerchantAccount> findAllMerchantAccountsByCustomerId(Integer id);
    
    List<MerchantAccount> findRefundableMerchantAccounts(Integer customerId);
    
    MerchantAccount findByProcessorAndFields(Map<String, String> criteriaMap) throws DuplicateMerchantAccountException;
    
    List<MerchantAccount> findByProcessorAndFields(String[] params, Object[] values);
    
    MerchantAccount findActiveByProcessorAndFields(Map<String, String> criteriaMap) throws DuplicateMerchantAccountException;
    
    MerchantAccount findByProcessorAndFieldsAndCustomer(Map<String, String> criteriaMap) throws DuplicateMerchantAccountException;
    
    void saveMerchantAccount(MerchantAccount merchantAccount);
    
    void updateMerchantAccount(MerchantAccount merchantAccount);
    
    void saveOrUpdateMerchantAccount(MerchantAccount merchantAccount);
    
    MerchantAccount findByPointOfSaleIdAndCardTypeId(Integer pointOfSaleId, Integer cardTypeId);
    
    MerchantAccount findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(Integer pointOfSaleId, Integer cardTypeId);
    
    MerchantAccount findById(int merchantAccountId);
    
    void deleteMerchantAccount(MerchantAccount merchantAccount);
    
    long updateNewReferenceNumber(MerchantAccount merchantAccount);
    
    InfoAndMerchantAccount preFillMerchantAccount(MerchantAccount merchantAccount);
    
    MerchantAccount findByDeletedProcessorFieldsAndMaxReferenceCounter(MerchantAccount merchantAccount);
    
    Collection<MerchantAccount> getValidMerchantAccountsForProcessor(Integer customerId, Integer processorId);
    
    Collection<MerchantPOS> findMerchantPosesByMerchantAccountIdAndCardTypeId(Integer merchantAccountId, int cardTypeId);
    
    Collection<MerchantPOS> findMerchantPosesByMerchantAccountId(Integer merchantAccountId);
    
    List<MerchantAccount> findAllMerchantAccountsForReportsByCustomerId(Integer customerId);
    
    List<MerchantAccount> findMerchAcctsIncludeDeleted(int customerId);
    
    List<FilterDTO> getMerchantAccountFiltersByCustomerId(List<FilterDTO> result, int customerId, RandomKeyMapping keyMapping);
    
    List<FilterDTO> getRefundableMerchantAccountFiltersByCustomerId(List<FilterDTO> result, int customerId, RandomKeyMapping keyMapping);
    
    List<String> findCustomerNamesByProcessorId(int processorId);
    
    List<CustomerProperty> findTimeZoneByProcessorId(Integer processorId);
    
    MerchantAccount findFromMechantPOSByPointOfSaleAndProcId(Integer pointOfSaleId, Integer processorId);
    
    MerchantAccount findByMerchantPOSPointOfSaleIdAndCardTypeId(Integer pointOfSaleId, Integer cardTypdId);
    
    MerchantAccount findByDeletedMerchantPOSPointOfSaleIdAndCardTypeId(Integer pointOfSaleId, Integer cardTypdId);
    
    MerchantPOS findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(int pointOfSaleId, int cardTypeId, int posId);
    
    MerchantAccount findWithProcessorById(Integer merchantAccountId) throws CommunicationException;
    
    MerchantAccount findWithProcessorById(Integer merchantAccountId, boolean forTest) throws CommunicationException;
    
    List<GatewayProcessor> findGatewayProcessors();
    
    MerchantPOS findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber(String terminalToken, String serialnumber);
    
    MerchantAccount findByTerminalTokenAndIsLink(String terminalToken, boolean isLink);
    
    MerchantAccount findByTerminalTokenAndIsLink(String terminalToken);
    
    boolean hasLinkMerchantAccount(int customerId);
    
    boolean hasIrisMerchantAccount(Integer customerId);
    
    MerchantAccount migrateMerchantAccount(Integer customerId, MerchantAccount merchantAccount) throws InvalidDataException;
    
    CardReaderConfig getCardReaderConfig(String terminalToken) throws InvalidDataException;
    
    List<MerchantAccount> populateMigrationInfo(List<MerchantAccount> merchantAccounts);
    
    String testTransaction(MerchantAccount merchantAccount);
    
    Optional<MerchantAccount> populateMerchantTerminalInformation(MerchantAccount merchantAccount, boolean forTest) throws CommunicationException;
}
