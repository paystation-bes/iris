package com.digitalpaytech.service;

import com.digitalpaytech.domain.SingleTransactionCardData;

public interface SingleTransactionCardDataService {
    
    public SingleTransactionCardData findSingleTransactionCardDataByPurchaseId(Long purchaseId);
    
    public void saveOrUpdate(SingleTransactionCardData singleTransactionCardData);
    
    public void delete(SingleTransactionCardData singleTransactionCardData);
    
    public void cleanupOldData(int maxDays);
}
