package com.digitalpaytech.service;

import com.digitalpaytech.domain.Tax;

public interface TaxService {
    public Tax findByCustomerIdAndName(Integer customerId, String name);
    
    public Tax findTaxById(Integer taxId);
}
