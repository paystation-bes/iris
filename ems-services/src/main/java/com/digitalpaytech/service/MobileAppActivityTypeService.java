package com.digitalpaytech.service;

import com.digitalpaytech.domain.MobileAppActivityType;

public interface MobileAppActivityTypeService {

    public MobileAppActivityType findById(Integer id);

    public MobileAppActivityType findByName(String name);
    
}
