package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Route;

public interface RouteProcessor {
    void saveRouteAndRoutePOS(Route route);
    
    void updateRouteAndRoutePOS(Route route);
    
    void deleteRoute(Integer routeId, List<Integer> affectedPosIds, List<Integer> affectedCatIds);
}
