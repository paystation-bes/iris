package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public interface PurchaseService {
    
    Purchase findPurchaseById(Long id, boolean isEvict);
    
    List<Purchase> findPurchasesByPointOfSaleIdLast90Days(int customerId, int pointOfSaleId, String sortOrder, String sortItem, Integer page);
    
    Purchase findUniquePurchaseForSms(Integer customerId, Integer pointOfSaleId, Date purchaseGmt, int purchaseNumber);
    
    Purchase findLatestPurchaseByPointOfSaleIdAndPurchaseNumber(int pointOfSaleId, String purchaseNumber);
    
    Purchase findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt(Integer pointOfSaleId, Integer purchaseNumber, Date purchaseGmt);
    
    Object findPurchaseByCriteria(TransactionReceiptSearchCriteria criteria);
    
}
