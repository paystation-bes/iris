package com.digitalpaytech.service.crypto.impl;

import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.util.crypto.CryptoConstants;

public class Sha256AlgorithmImpl implements CryptoAlgorithm {
    private static final byte[] SALT_BAD_CARD =                 "goe91WW0PX!0!xm#UU@iiH".getBytes();
    private static final byte[] SALT_PUBLIC_ENCRYPTION_KEY =    "NpGLEF153liPRSsWzb69Oj".getBytes();
    private static final byte[] SALT_CREDIT_CARD_PROCESSING =   "V!1+@!gJ)go-fyTW!fo#?e".getBytes();
    private static final byte[] SALT_FILE_UPLOAD =              "bn4usS077CN03OJfEmmWx9".getBytes();
    private static final byte[] SALT_IMPORT_ERROR =             "gYWxx9523YUuwF22XWjlPp".getBytes();
    private static final byte[] SALT_TRANSACTION_UPLOAD =       "TvjO23EFl#)fe*)Pw7@#5J".getBytes();
    private static final byte[] LINK_SALT_BAD_CARD =            "zz>pHj)ldM0#L%:'4B[OQLG|M=G*AJm?".getBytes();
    private static final byte[][] SALTS = new byte[][] { SALT_BAD_CARD, SALT_PUBLIC_ENCRYPTION_KEY, SALT_CREDIT_CARD_PROCESSING, SALT_FILE_UPLOAD,
        SALT_IMPORT_ERROR, SALT_TRANSACTION_UPLOAD, LINK_SALT_BAD_CARD };

    @Override
    public final String getHashAlgorithm() {
        return CryptoConstants.SHA_256;
    }

    @Override
    public final byte[][] getSalts() {
        return SALTS;
    }    
}
