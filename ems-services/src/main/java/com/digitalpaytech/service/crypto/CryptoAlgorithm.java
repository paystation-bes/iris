package com.digitalpaytech.service.crypto;

public interface CryptoAlgorithm {
    String getHashAlgorithm();
    byte[][] getSalts();
}
