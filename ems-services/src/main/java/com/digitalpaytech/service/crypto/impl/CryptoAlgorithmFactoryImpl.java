package com.digitalpaytech.service.crypto.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.HashAlgorithmTypeService;


@Service("cryptoAlgorithmFactory")
public final class CryptoAlgorithmFactoryImpl implements CryptoAlgorithmFactory {
    private static Logger logger = Logger.getLogger(CryptoAlgorithmFactoryImpl.class);
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private HashAlgorithmTypeService hashAlgorithmTypeService;


    @Override
    public String getShaHash(byte[] data, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        HashAlgorithmData algData = hashAlgorithmTypeService.findForMessageDigest(hashAlgorithmTypeId);
        
        if (hashType < 0 || algData.getSalts() == null || hashType >= algData.getSalts().length) {
            throw new CryptoException(createUnknowHashTypeErrorMessage(algData.getHashAlgorithm(), hashType));
        }
        
        try {
            MessageDigest message_digest = MessageDigest.getInstance(algData.getHashAlgorithm());
            message_digest.update(data);
            byte[] digest = message_digest.digest(algData.getSalts()[hashType]);
            return new String(Base64.encodeBase64(digest));

        } catch (Exception e) {
            throw new CryptoException(createCalculateErrorMessage(algData.getHashAlgorithm(), hashType), e);
        }
    }

    @Override    
    public String getShaHash(byte[] data, int hashType, String charSet, int hashAlgorithmTypeId) throws CryptoException {
        HashAlgorithmData algData = hashAlgorithmTypeService.findForMessageDigest(hashAlgorithmTypeId);
        
        if (hashType < 0 || algData.getSalts() == null || hashType >= algData.getSalts().length) {
            throw new CryptoException(createUnknowHashTypeErrorMessage(algData.getHashAlgorithm(), hashType));
        }
        
        try {
            MessageDigest message_digest = MessageDigest.getInstance(algData.getHashAlgorithm());
            message_digest.update(data);
            byte[] digest = message_digest.digest(algData.getSalts()[hashType]);
            
            return new String(Base64.encodeBase64(digest), charSet);
        } catch (Exception e) {
            throw new CryptoException(createCalculateErrorMessage(algData.getHashAlgorithm(), hashType), e);
        }
    }
    
    // Returns the contents of the file in a byte array.
    @Override    
    public String getShaHash(File file, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        HashAlgorithmData algData = hashAlgorithmTypeService.findForMessageDigest(hashAlgorithmTypeId);
        
        if (hashType < 0 || algData.getSalts() == null || hashType >= algData.getSalts().length) {
            throw new CryptoException(createUnknowHashTypeErrorMessage(algData.getHashAlgorithm(), hashType));
        }
        
        InputStream is;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new CryptoException("File not found: " + file.getPath(), e);
        }
        
        // Get the size of the file
        long length = file.length();
        
        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            throw new CryptoException("File is too large: " + file.getPath());
        }
        
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];
        
        // Read in the bytes
        try {
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
            
            // Ensure all the bytes have been read in
            if (offset < bytes.length) {
                throw new IOException("Could not completely read file " + file.getPath());
            }
            
            // Close the input stream and return bytes
            is.close();
        } catch (IOException e) {
            throw new CryptoException("Unable to read file: " + file.getPath(), e);
        }
        
        // Get the hash
        return (getShaHash(bytes, hashType, hashAlgorithmTypeId));
    }
    
    @Override
    public String getShaHash(String rawString, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        return (getShaHash(rawString.getBytes(), hashType, hashAlgorithmTypeId));
    }
    
    @Override
    public boolean verifyShaHash(String hash, byte[] data, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        if (hash == null) {
            return (false);
        }

        String verify_hash = getShaHash(data, hashType, hashAlgorithmTypeId);
        logger.info("+++ generated hash: '" + verify_hash + "'");
        return (hash.equals(verify_hash));
    }
    
    @Override
    public boolean verifyShaHash(String hash, byte[] data, int hashType, String charSet, int hashAlgorithmTypeId) throws CryptoException {
        if (hash == null) {
            return (false);
        }
        
        String verify_hash = getShaHash(data, hashType, charSet, hashAlgorithmTypeId);
        logger.info("+++ generated hash: '" + verify_hash + "'");
        return (hash.equals(verify_hash));
    }
    
    @Override
    public boolean verifyShaHash(String hash, String rawString, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        return (verifyShaHash(hash, rawString.getBytes(), hashType, hashAlgorithmTypeId));
    }
    
    @Override
    public boolean verifyShaHashHex(String hexHash, File file, int hashType, int hashAlgorithmTypeId) throws CryptoException {
        if (hexHash == null) {
            return (false);
        }
        
        try {
            char[] hex_chars = new char[hexHash.length()];
            hexHash.getChars(0, hexHash.length(), hex_chars, 0);
            
            String base64_hash = new String(Base64.encodeBase64(Hex.decodeHex(hex_chars)));
            String verify_hash = getShaHash(file, hashType, hashAlgorithmTypeId);
            
            return (base64_hash.equals(verify_hash));
        } catch (Exception e) {
            throw new CryptoException("Unable to verify Hex hash", e);
        }
    }

    
    // For EMS 6
    @Override
    public String getSha1Hash(byte[] data, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return getShaHash(data, 
                          hashType, 
                          this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                               EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public String getSha1Hash(byte[] data, int hashType, String charSet) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return getShaHash(data, 
                          hashType, 
                          charSet, 
                          this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                               EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public String getSha1Hash(File file, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return getShaHash(file, 
                          hashType, 
                          this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                               EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public String getSha1Hash(String rawString, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return getShaHash(rawString, 
                          hashType, 
                          this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                               EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public boolean verifySha1Hash(String hash, byte[] data, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return verifyShaHash(hash, 
                             data, 
                             hashType, 
                             this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                                  EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public boolean verifySha1Hash(String hash, byte[] data, int hashType, String charSet) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return verifyShaHash(hash, 
                             data, 
                             hashType, 
                             charSet, 
                             this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                                  EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public boolean verifySha1Hash(String hash, String rawString, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return verifyShaHash(hash, 
                             rawString, 
                             hashType, 
                             this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                                  EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    @Override
    public boolean verifySha1HashHex(String hexHash, File file, int hashType) throws CryptoException {
        // HashAlgorithmTypeId 1 is SHA-1.
        return verifyShaHashHex(hexHash, 
                                file, 
                                hashType, 
                                this.emsPropertiesService.getPropertyValueAsIntQuiet(EmsPropertiesService.HASH_ALGORITHM_TYPE_SHA_1_ID, 
                                                                                     EmsPropertiesService.DEFAULT_HASH_ALGORITHM_TYPE_SHA_1_ID));
    }
    
    
    private String createUnknowHashTypeErrorMessage(String hashAlgorithm, int hashType) {
        StringBuilder bdr = new StringBuilder();
        bdr.append("Unknown ").append(hashAlgorithm).append(" Hash Type: ").append(hashType);
        return bdr.toString();
    }
    
    private String createCalculateErrorMessage(String hashAlgorithm, int hashType) {
        StringBuilder bdr = new StringBuilder();
        bdr.append("Unable to calculate ").append(hashAlgorithm).append(" Hash Type: ").append(hashType);
        return bdr.toString();
    }

    public void setHashAlgorithmTypeService(HashAlgorithmTypeService hashAlgorithmTypeService) {
        this.hashAlgorithmTypeService = hashAlgorithmTypeService;
    }

    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
}
