package com.digitalpaytech.service.crypto;

import java.io.File;

import com.digitalpaytech.exception.CryptoException;

public interface CryptoAlgorithmFactory {
    String getShaHash(byte[] data, int hashType, int hashAlgorithmTypeId) throws CryptoException;
    
    String getShaHash(byte[] data, int hashType, String charSet, int hashAlgorithmTypeId) throws CryptoException;
    
    String getShaHash(File file, int hashType, int hashAlgorithmTypeId) throws CryptoException;
    
    String getShaHash(String rawString, int hashType, int hashAlgorithmTypeId) throws CryptoException;
    
    boolean verifyShaHash(String hash, byte[] data, int hashType, int hashAlgorithmTypeId) throws CryptoException;
    
    boolean verifyShaHash(String hash, byte[] data, int hashType, String charSet, int hashAlgorithmTypeId) throws CryptoException;
    
    boolean verifyShaHash(String hash, String rawString, int hashType, int hashAlgorithmTypeId) throws CryptoException;
    
    boolean verifyShaHashHex(String hexHash, File file, int hashType, int hashAlgorithmTypeI) throws CryptoException;
    
    // For EMS 6
    String getSha1Hash(byte[] data, int hashType) throws CryptoException;
    
    String getSha1Hash(byte[] data, int hashType, String charSet) throws CryptoException;
    
    String getSha1Hash(File file, int hashType) throws CryptoException;
    
    String getSha1Hash(String rawString, int hashType) throws CryptoException;
    
    boolean verifySha1Hash(String hash, byte[] data, int hashType) throws CryptoException;
    
    boolean verifySha1Hash(String hash, byte[] data, int hashType, String charSet) throws CryptoException;
    
    boolean verifySha1Hash(String hash, String rawString, int hashType) throws CryptoException;
    
    boolean verifySha1HashHex(String hexHash, File file, int hashType) throws CryptoException;
}
