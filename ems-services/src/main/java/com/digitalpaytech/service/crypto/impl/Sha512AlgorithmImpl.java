package com.digitalpaytech.service.crypto.impl;

import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.util.crypto.CryptoConstants;

public class Sha512AlgorithmImpl implements CryptoAlgorithm {
    private static final byte[] SALT_BAD_CARD =                 "eG683?!))eifjl)eFPE2X-".getBytes();
    private static final byte[] SALT_PUBLIC_ENCRYPTION_KEY =    "gP3FgeFE0fixbyzE3bei1F".getBytes();
    private static final byte[] SALT_CREDIT_CARD_PROCESSING =   "H?3+EF!!g03EFjle?f0-$3".getBytes();
    private static final byte[] SALT_FILE_UPLOAD =              "Be59EFYxZ38F3ffw2efP2O".getBytes();
    private static final byte[] SALT_IMPORT_ERROR =             "Hvy293Xpf2qQfex03jl40Y".getBytes();
    private static final byte[] SALT_TRANSACTION_UPLOAD =       "vT0x3fLEq_39&ef*0PJfi5".getBytes();
    private static final byte[][] SALTS = new byte[][] { SALT_BAD_CARD, SALT_PUBLIC_ENCRYPTION_KEY, SALT_CREDIT_CARD_PROCESSING, SALT_FILE_UPLOAD,
        SALT_IMPORT_ERROR, SALT_TRANSACTION_UPLOAD, };

    @Override
    public final String getHashAlgorithm() {
        return CryptoConstants.SHA_512;
    }

    @Override
    public final byte[][] getSalts() {
        return SALTS;
    }
}
