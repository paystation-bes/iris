package com.digitalpaytech.service.crypto.impl;

import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.util.crypto.CryptoConstants;

public class Sha1AlgorithmImpl implements CryptoAlgorithm {
    private static final byte[] SALT_BAD_CARD = "AsGuw8M8ew/p1X3gUDU+iA".getBytes();
    private static final byte[] SALT_PUBLIC_ENCRYPTION_KEY = "XMvZFxb1ki11OujsLyY3rG".getBytes();
    private static final byte[] SALT_CREDIT_CARD_PROCESSING = "D!9!+@1P@y+e<Hk!<K5@$s".getBytes();
    private static final byte[] SALT_FILE_UPLOAD = "kemd57fTC9NDqfrePlMwxG".getBytes();
    private static final byte[] SALT_IMPORT_ERROR = "FJGZf68emItrXHBs8T1kxj".getBytes();
    private static final byte[] SALT_TRANSACTION_UPLOAD = "VqEJaQF189$@Yn8a09b!3L".getBytes();
    private static final byte[][] SALTS = new byte[][] { SALT_BAD_CARD, SALT_PUBLIC_ENCRYPTION_KEY, SALT_CREDIT_CARD_PROCESSING, SALT_FILE_UPLOAD,
        SALT_IMPORT_ERROR, SALT_TRANSACTION_UPLOAD, };

    @Override
    public final String getHashAlgorithm() {
        return CryptoConstants.SHA_1;
    }

    @Override
    public final byte[][] getSalts() {
        return SALTS;
    }
}
