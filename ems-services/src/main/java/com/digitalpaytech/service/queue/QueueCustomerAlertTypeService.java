package com.digitalpaytech.service.queue;

import java.util.List;

public interface QueueCustomerAlertTypeService {
    
    void addCustomerAlertTypeEvent(List<Integer> addedPointOfSaleIds, List<Integer> removedPointOfSaleIds, List<Integer> updatedPointOfSaleIds);
    
    void addCustomerAlertTypeEvent(List<Integer> addedPointOfSaleIds, List<Integer> removedPointOfSaleIds, List<Integer> updatedPointOfSaleIds,
        Integer customerAlertTypeId);
    
    void addCustomerAlertTypeEvent(List<Integer> addedPointOfSaleIds, List<Integer> removedPointOfSaleIds, List<Integer> updatedPointOfSaleIds,
        List<Integer> customerAlertTypeIds);
    
    void addCustomerAlertTypeEventWithLimit(Integer alertThresholdTypeId, Integer limit);
}
