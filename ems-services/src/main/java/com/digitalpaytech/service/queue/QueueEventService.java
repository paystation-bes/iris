package com.digitalpaytech.service.queue;

import java.util.Date;

import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.dto.queue.QueueEvent;

public interface QueueEventService {
    
    @SuppressWarnings("checkstyle:parameternumber")
    void createEventQueue(Integer pointOfSaleId, Integer paystationTypeId, Integer customerAlertTypeId, EventType eventType,
        Integer eventSeverityTypeId, Integer eventActionTypeId, Date timestampGMT, String alertInfo, boolean isActive, boolean isNotification,
        Integer userId, byte queueType);
    
    void addItemToQueue(QueueEvent queueEvent, byte queueType);
}
