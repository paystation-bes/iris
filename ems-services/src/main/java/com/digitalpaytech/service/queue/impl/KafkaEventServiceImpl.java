package com.digitalpaytech.service.queue.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.service.EventExceptionService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Service("kafkaEventService")
@Transactional(propagation = Propagation.SUPPORTS)
public class KafkaEventServiceImpl implements QueueEventService {
    private static final Logger LOG = Logger.getLogger(KafkaEventServiceImpl.class);

    @Autowired
    private EventExceptionService eventExceptionService;

    @Autowired
    @Qualifier("irisMessageProducer")
    private AbstractMessageProducer processingEventsProducer;
    

    @Override
    public final void createEventQueue(final Integer pointOfSaleId, 
                                       final Integer paystationTypeId, 
                                       final Integer customerAlertTypeId,
                                       final EventType eventType, 
                                       final Integer eventSeverityTypeId, 
                                       final Integer eventActionTypeId, 
                                       final Date timestampGMT,
                                       final String alertInfo, 
                                       final boolean isActive, 
                                       final boolean isNotification, 
                                       final Integer userId, 
                                       final byte queueType) {
        QueueEvent queueEvent = null;
        
        if (paystationTypeId == null || !this.eventExceptionService.isException(paystationTypeId, eventType.getEventDeviceType().getId())) {
            queueEvent = new QueueEvent();
            queueEvent.setPointOfSaleId(pointOfSaleId);
            queueEvent.setCustomerAlertTypeId(customerAlertTypeId);
            queueEvent.setEventTypeId(eventType.getId());
            queueEvent.setEventSeverityTypeId(eventSeverityTypeId);
            queueEvent.setEventActionTypeId(eventType.isHasLevel() ? eventActionTypeId : null);
            queueEvent.setAlertInfo(alertInfo);
            queueEvent.setTimestampGMT(timestampGMT);
            queueEvent.setIsActive(isActive);
            queueEvent.setIsNotification(isNotification);
            queueEvent.setCreatedGMT(DateUtil.getCurrentGmtDate());
            queueEvent.setLastModifiedByUserId(userId);
            
            if (LOG.isDebugEnabled()) {
                LOG.debug("QueueEvent before sending to Kafka producer is: " + queueEvent);
            }
            
            addItemToQueue(queueEvent, queueType);
        }
    }
    
    @Override
    public final void addItemToQueue(final QueueEvent queueEvent, final byte queueType) {
        final String key = String.valueOf(queueEvent.getPointOfSaleId());
        try {
            if ((queueType & WebCoreConstants.QUEUE_TYPE_RECENT_EVENT) == WebCoreConstants.QUEUE_TYPE_RECENT_EVENT) {
                this.processingEventsProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_RECENT_EVENTS_TOPIC_NAME_KEY, key, queueEvent);
            }
            if ((queueType & WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT) == WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT) {
                this.processingEventsProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_PROCESSING_EVENTS_TOPIC_NAME_KEY, key, queueEvent);
            }
            if ((queueType & WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT) == WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT) {
                // If severity is 1 and is a user defined alert it means a critical alert is cleared and reduced to minor
                // History and current status should be different. (current active with severity 1, history cleared with severity 0
                if (queueEvent.getCustomerAlertTypeId() != null && queueEvent.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                    queueEvent.setEventSeverityTypeId(WebCoreConstants.SEVERITY_CLEAR);
                    queueEvent.setIsActive(false);
                }
                this.processingEventsProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_HISTORICAL_EVENTS_TOPIC_NAME_KEY, key, queueEvent);
            }
        } catch (InvalidTopicTypeException | JsonException e) {
            LOG.error("Problem occurred while sending alert to Kafka; key (posId): " + key, e);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sent 'CompletableFuture' to Kafka, key (posId): " + key);
        }
    }

    
    public final void setEventExceptionService(final EventExceptionService eventExceptionService) {
        this.eventExceptionService = eventExceptionService;
    }
    public final void setProcessingEventsProducer(final AbstractMessageProducer processingEventsProducer) {
        this.processingEventsProducer = processingEventsProducer;
    }
}
