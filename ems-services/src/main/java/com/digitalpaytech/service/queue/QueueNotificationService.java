package com.digitalpaytech.service.queue;

public interface QueueNotificationService {
    void createQueueNotificationEmail(String subject, String content, String toAddress);
}
