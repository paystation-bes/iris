package com.digitalpaytech.service.queue.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.service.queue.QueueNotificationService;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.exception.JsonException;

@Service("kafkaNotificationService")
@Transactional(propagation = Propagation.SUPPORTS)
public class KafkaNotificationServiceImpl implements QueueNotificationService {
    private static final Logger LOG = Logger.getLogger(KafkaNotificationServiceImpl.class);
    
    @Autowired
    @Qualifier("irisMessageProducer")
    private AbstractMessageProducer notificationEmailProducer;
    
        
    @Override
    public final void createQueueNotificationEmail(final String subject, final String content, final String toAddress) {
        
        final QueueNotificationEmail queueNotificationEmail = new QueueNotificationEmail();
        
        queueNotificationEmail.setSubject(subject);
        queueNotificationEmail.setContent(content);
        queueNotificationEmail.setToAddress(toAddress);
        
        try {
            this.notificationEmailProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_TOPIC_NAME_KEY, 
                                                             toAddress, 
                                                             queueNotificationEmail);
            
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sent notification to Kafka, subject: " + subject + ", toAddress: " + toAddress);
            }
        } catch (InvalidTopicTypeException | JsonException e) {
            LOG.error("Problem occurred while sending notification to Kafka, ", e);
        }
    }

    public final void setCustomerAlertTypeProducer(final AbstractMessageProducer notificationEmailProducer) {
        this.notificationEmailProducer = notificationEmailProducer;
    }
}
