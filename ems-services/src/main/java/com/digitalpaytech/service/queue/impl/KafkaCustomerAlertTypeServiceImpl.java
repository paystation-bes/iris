package com.digitalpaytech.service.queue.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;
import com.digitalpaytech.util.QueueCustomerAlertTypeKeyResolver;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Service("kafkaCustomerAlertTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class KafkaCustomerAlertTypeServiceImpl implements QueueCustomerAlertTypeService {
    private static final Logger LOG = Logger.getLogger(KafkaCustomerAlertTypeServiceImpl.class);
    private final QueueCustomerAlertTypeKeyResolver keyResolver = new QueueCustomerAlertTypeKeyResolver();
    
    @Autowired
    @Qualifier("irisMessageProducer")
    private AbstractMessageProducer customerAlertTypeProducer;
    
    
    @Override
    public final void addCustomerAlertTypeEvent(final List<Integer> addedPointOfSaleIds, 
                                                final List<Integer> removedPointOfSaleIds,
                                                final List<Integer> modifiedPointOfSaleIds) {
        this.addCustomerAlertTypeEvent(addedPointOfSaleIds, removedPointOfSaleIds, modifiedPointOfSaleIds, new ArrayList<Integer>(1));        
    }
    
    @Override
    public final void addCustomerAlertTypeEvent(final List<Integer> addedPointOfSaleIds, 
                                                final List<Integer> removedPointOfSaleIds,
                                                final List<Integer> modifiedPointOfSaleIds, 
                                                final Integer customerAlertTypeId) {
        final List<Integer> passedCustomerAlertTypeIds = new ArrayList<Integer>(1);
        passedCustomerAlertTypeIds.add(customerAlertTypeId);
        this.addCustomerAlertTypeEvent(addedPointOfSaleIds, removedPointOfSaleIds, modifiedPointOfSaleIds, passedCustomerAlertTypeIds);
    }
    
    @Override
    public final void addCustomerAlertTypeEvent(final List<Integer> addedPointOfSaleIds, 
                                                final List<Integer> removedPointOfSaleIds,
                                                final List<Integer> modifiedPointOfSaleIds, 
                                                final List<Integer> customerAlertTypeIds) {
        
        final List<Integer> passedCustomerAlertTypeIds = customerAlertTypeIds == null || customerAlertTypeIds.isEmpty() ? null : customerAlertTypeIds;
        try {
            if (removedPointOfSaleIds != null && !removedPointOfSaleIds.isEmpty()) {
                sendQueueCustomerAlertType(QueueCustomerAlertType.Type.REMOVED, removedPointOfSaleIds, passedCustomerAlertTypeIds);
            }
            if (addedPointOfSaleIds != null && !addedPointOfSaleIds.isEmpty()) {
                sendQueueCustomerAlertType(QueueCustomerAlertType.Type.ADDED, addedPointOfSaleIds, passedCustomerAlertTypeIds);            
            }
            if (modifiedPointOfSaleIds != null && !modifiedPointOfSaleIds.isEmpty()) {
                sendQueueCustomerAlertType(QueueCustomerAlertType.Type.MODIFIED, modifiedPointOfSaleIds, passedCustomerAlertTypeIds);
            }
        } catch (InvalidTopicTypeException | JsonException e) {
            final String msg = "removedPointOfSaleIds: " + removedPointOfSaleIds + ", addedPointOfSaleIds: " 
                                + addedPointOfSaleIds + ", modifiedPointOfSaleIds: " 
                                + modifiedPointOfSaleIds;
            LOG.error("Problem occurred while sending customerAlertType to Kafka, msg: " + msg, e);
        }            
    }
    
    @Override
    public final void addCustomerAlertTypeEventWithLimit(final Integer alertThresholdTypeId, final Integer limit) {
        final QueueCustomerAlertType custAlertType = new QueueCustomerAlertType(QueueCustomerAlertType.Type.COMMUNICATION, limit);
        try {
            // Use QueueCustomerAlertTypeKeyResolver to generate a randomized key from a QueueCustomerAlertType object.
            final Future<Boolean> fboolean
                = this.customerAlertTypeProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_CUSTOMER_ALERT_TYPE_TOPIC_NAME_KEY,
                                                                   this.keyResolver.resolve(custAlertType),
                                                                   custAlertType);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sent customerAlertTypeEventWithLimit, future response: " + fboolean);
            }
        } catch (InvalidTopicTypeException | JsonException e) {
            LOG.error("Problem occurred while sending customerAlertTypeWithLimit to Kafka, ", e);
        }
    }

    
    private Future<Boolean> sendQueueCustomerAlertType(final QueueCustomerAlertType.Type alertType, 
                                                       final List<Integer> posIds, 
                                                       final List<Integer> passedCustomerAlertTypeIds) 
        throws InvalidTopicTypeException, JsonException {
        
        final QueueCustomerAlertType custAlertType = new QueueCustomerAlertType(alertType, posIds, passedCustomerAlertTypeIds);
        // Use QueueCustomerAlertTypeKeyResolver to generate a randomized key from a QueueCustomerAlertType object.        
        return this.customerAlertTypeProducer.sendWithByteArray(KafkaKeyConstants.ALERT_PROCESSING_CUSTOMER_ALERT_TYPE_TOPIC_NAME_KEY,
                                                                this.keyResolver.resolve(custAlertType),
                                                                custAlertType);
    }

    public final void setCustomerAlertTypeProducer(final AbstractMessageProducer customerAlertTypeProducer) {
        this.customerAlertTypeProducer = customerAlertTypeProducer;
    }
}
