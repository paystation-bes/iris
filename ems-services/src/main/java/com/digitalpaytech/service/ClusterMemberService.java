package com.digitalpaytech.service;

import java.util.Collection;

import com.digitalpaytech.domain.Cluster;

public interface ClusterMemberService {
    
    public String getClusterName();

    public boolean isClusterMember(String name);
    
    public boolean isClusterMemberHost(String hostname);

    public Collection<Cluster> getAllClusterMembers();
    
    public int getClusterId();
}
