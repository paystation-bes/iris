package com.digitalpaytech.service.paystation.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.LicencePlateInfo;
import com.digitalpaytech.service.paystation.LicencePlateInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;

@Service("licencePlateInfoService")
@Transactional(propagation = Propagation.REQUIRED)
public class LicencePlateInfoServiceImpl implements LicencePlateInfoService {
    
    private static final String SQL_WRAPPER_BEGIN = "SELECT ap3.LicencePlateNumber, ap3.PermitBeginGMT AS PermitBeginGmt, "
                                                    + "ap3.PermitExpireGMT AS PermitExpireGmt, l.Name AS LocationName "
                                                    + "FROM ActivePermit ap3 INNER JOIN Location l ON l.Id = ap3.PermitLocationId INNER JOIN (";
    
    private static final String SQL_WRAPPER_END = ") apres ON  ap3.LicencePlateNumber = apres.LicencePlateNumber AND "
                                                  + "ap3.permitExpireGMT = apres.PermitExpireGMT AND ap3.CustomerId = apres.CustomerId";
    private static final String SQL_WRAPPER_END_MOST_RECENT = ") apres ON  ap3.LicencePlateNumber = apres.LicencePlateNumber AND "
                                                              + "ap3.permitBeginGmt = apres.PermitBeginGmt "
                                                              + "AND ap3.CustomerId = apres.CustomerId";
    
    private static final String SQL_GET_LICENCE_PLATE_INFO_CORE = "SELECT MAX(ap.PermitExpireGMT) AS PermitExpireGMT, "
                                                                  + "ap.LicencePlateNumber AS LicencePlateNumber, "
                                                                  + "ap.CustomerId AS CustomerId FROM ActivePermit ap";
    private static final String SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT = "SELECT MAX(ap.PermitBeginGmt) AS PermitBeginGmt, "
                                                                              + "ap.LicencePlateNumber AS LicencePlateNumber, "
                                                                              + "ap.CustomerId AS CustomerId FROM ActivePermit ap";
    private static final String SQL_GET_LICENCE_PLATE_INFO_CORE_NOWRAPPER = "SELECT ap.LicencePlateNumber AS LicencePlateNumber, "
                                                                            + "ap.PermitBeginGMT AS PermitBeginGmt, "
                                                                            + "ap.PermitExpireGMT AS PermitExpireGMT, l.Name AS LocationName "
                                                                            + "FROM ActivePermit ap INNER JOIN Location l ON l.Id = ap.PermitLocationId ";
    
    private static final String SQL_VALID_WHERE = " WHERE ap.CustomerId = :customerId AND ap.PermitExpireGMT > :currTime "
                                                  + "AND ap.LicencePlateNumber IS NOT NULL AND ap.LicencePlateNumber != ''";
    private static final String SQL_EXPIRED_WHERE = " WHERE ap.CustomerId = :customerId " + "AND ap.PermitExpireGMT BETWEEN :yesterday "
                                                    + "AND :currTime AND ap.LicencePlateNumber " + "IS NOT NULL AND ap.LicencePlateNumber != ''";
    private static final String SQL_SINGLE_WHERE = " WHERE ap.CustomerId = :customerId AND ap.LicencePlateNumber = :licencePlateNumber";
    private static final String SQL_WHERE_ADD_LASTMODIFIED = " AND ap.LastModifiedGMT >= :lastModifiedGMT";
    private static final String SQL_WHERE_ADD_POSLIST = " AND ap.PointOfSaleId IN (:posList)";
    private static final String SQL_WHERE_ADD_LOCATION = " AND ap.PermitLocationId = :locationId";
    private static final String SQL_WHERE_ADD_LICENCEPLATENUMBER = " AND ap.LicencePlateNumber = :licencePlateNumber";
    private static final String SQL_GROUP_BY_LICENCEPLATENUMBER = " GROUP BY ap.LicencePlateNumber";
    private static final String SQL_ORDER_BY_PERMITEXPIREGMT = " ORDER BY ap.PermitExpireGMT";
    private static final String SQL_ORDER_BY_LICENCEPLATENUMBER = " ORDER BY ap.LicencePlateNumber";
    private static final String SQL_WHERE_ADD_LICENCEPLATE_CHECK = " AND ap.LicencePlateNumber NOT IN "
                                                                   + "(SELECT DISTINCT(ap2.LicencePlateNumber) FROM ActivePermit ap2 "
                                                                   + "WHERE ap2.CustomerId = :customerId AND ap2.PermitExpireGMT> :currTime "
                                                                   + "AND ap2.LicencePlateNumber IS NOT NULL AND ap2.LicencePlateNumber != ''";
    private static final String SQL_WHERE_ADD_LOCATION2 = " AND ap2.PermitLocationId = :locationId)";
    private static final String SQL_WHERE_ADD_POSLIST2 = " AND ap2.PointOfSaleId IN (:posList))";
    private static final String SQL_WHERE_ADD_LICENCEPLATE_CHECK_CLOSE = ")";
    
    private static final String SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_POSLIST = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE
                                                                                     + SQL_VALID_WHERE + SQL_WHERE_ADD_POSLIST
                                                                                     + SQL_WHERE_ADD_LASTMODIFIED + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                                     + SQL_ORDER_BY_PERMITEXPIREGMT + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_POSLIST_MOST_RECENT = SQL_WRAPPER_BEGIN
                                                                                                 + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                                                 + SQL_VALID_WHERE + SQL_WHERE_ADD_POSLIST
                                                                                                 + SQL_WHERE_ADD_LASTMODIFIED
                                                                                                 + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                                                 + SQL_ORDER_BY_PERMITEXPIREGMT
                                                                                                 + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_LOCATION = SQL_GET_LICENCE_PLATE_INFO_CORE_NOWRAPPER + SQL_VALID_WHERE
                                                                                      + SQL_WHERE_ADD_LOCATION + SQL_WHERE_ADD_LASTMODIFIED
                                                                                      + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                                      + SQL_ORDER_BY_PERMITEXPIREGMT;
    
    private static final String SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_CUSTOMER = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE
                                                                                      + SQL_VALID_WHERE + SQL_WHERE_ADD_LASTMODIFIED
                                                                                      + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                                      + SQL_ORDER_BY_PERMITEXPIREGMT + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_CUSTOMER_MOST_RECENT = SQL_WRAPPER_BEGIN
                                                                                                  + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                                                  + SQL_VALID_WHERE + SQL_WHERE_ADD_LASTMODIFIED
                                                                                                  + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                                                  + SQL_ORDER_BY_PERMITEXPIREGMT
                                                                                                  + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String SQL_GET_VALID_PLATES_BY_POSLIST = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE + SQL_VALID_WHERE
                                                                  + SQL_WHERE_ADD_POSLIST + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                  + SQL_ORDER_BY_LICENCEPLATENUMBER + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATES_BY_POSLIST_MOST_RECENT = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                              + SQL_VALID_WHERE + SQL_WHERE_ADD_POSLIST
                                                                              + SQL_GROUP_BY_LICENCEPLATENUMBER + SQL_ORDER_BY_LICENCEPLATENUMBER
                                                                              + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String SQL_GET_VALID_PLATES_BY_CUSTOMER = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE + SQL_VALID_WHERE
                                                                   + SQL_GROUP_BY_LICENCEPLATENUMBER + SQL_ORDER_BY_LICENCEPLATENUMBER
                                                                   + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATES_BY_CUSTOMER_MOST_RECENT = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                               + SQL_VALID_WHERE + SQL_GROUP_BY_LICENCEPLATENUMBER
                                                                               + SQL_ORDER_BY_LICENCEPLATENUMBER + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String SQL_GET_VALID_PLATES_BY_LOCATION = SQL_GET_LICENCE_PLATE_INFO_CORE_NOWRAPPER + SQL_VALID_WHERE
                                                                   + SQL_WHERE_ADD_LOCATION + SQL_ORDER_BY_LICENCEPLATENUMBER;
    
    private static final String SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_CUSTOMER = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE
                                                                                      + SQL_EXPIRED_WHERE + SQL_WHERE_ADD_LICENCEPLATE_CHECK
                                                                                      + SQL_WHERE_ADD_LICENCEPLATE_CHECK_CLOSE
                                                                                      + SQL_GROUP_BY_LICENCEPLATENUMBER + SQL_WRAPPER_END;
    
    private static final String SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_LOCATION = SQL_GET_LICENCE_PLATE_INFO_CORE_NOWRAPPER + SQL_EXPIRED_WHERE
                                                                                      + SQL_WHERE_ADD_LOCATION + SQL_WHERE_ADD_LICENCEPLATE_CHECK
                                                                                      + SQL_WHERE_ADD_LOCATION2 + SQL_ORDER_BY_LICENCEPLATENUMBER;
    
    private static final String SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_POSLIST = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE
                                                                                     + SQL_EXPIRED_WHERE + SQL_WHERE_ADD_POSLIST
                                                                                     + SQL_WHERE_ADD_LICENCEPLATE_CHECK + SQL_WHERE_ADD_POSLIST2
                                                                                     + SQL_GROUP_BY_LICENCEPLATENUMBER + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE + SQL_SINGLE_WHERE
                                                                            + SQL_WHERE_ADD_LICENCEPLATENUMBER + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_MOST_RECENT = SQL_WRAPPER_BEGIN
                                                                                        + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                                        + SQL_SINGLE_WHERE + SQL_WHERE_ADD_LICENCEPLATENUMBER
                                                                                        + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_AND_LOCATION = SQL_WRAPPER_BEGIN + SQL_GET_LICENCE_PLATE_INFO_CORE
                                                                                         + SQL_SINGLE_WHERE + SQL_WHERE_ADD_LICENCEPLATENUMBER
                                                                                         + SQL_WHERE_ADD_LOCATION + SQL_WRAPPER_END;
    
    private static final String SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_AND_LOCATION_MOST_RECENT = SQL_WRAPPER_BEGIN
                                                                                                     + SQL_GET_LICENCE_PLATE_INFO_CORE_MOST_RECENT
                                                                                                     + SQL_SINGLE_WHERE
                                                                                                     + SQL_WHERE_ADD_LICENCEPLATENUMBER
                                                                                                     + SQL_WHERE_ADD_LOCATION
                                                                                                     + SQL_WRAPPER_END_MOST_RECENT
                                                                                                     + SQL_WRAPPER_END_MOST_RECENT;
    
    private static final String LICENCE_PLATE_NO = "licencePlateNumber";
    private static final String PERMIT_BEGIN_GMT = "PermitBeginGmt";
    private static final String PERMIT_EXPIRE_GMT = "PermitExpireGmt";
    private static final String LOCATION_NAME = "locationName";
    private static final String CUSTOMER_ID = "customerId";
    private static final String POST_LIST = "posList";
    private static final String LAST_MODIFIED_GMT = "lastModifiedGMT";
    private static final String CURR_TIME = "currTime";
    private static final String YESTERDAY = "yesterday";
    private static final String LOCATION_ID = "locationId";
    
    private static final int ZERO_GRACE_PERIOD = 0;
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByPosList(final int customerId, final List<PointOfSale> posList,
        final Date lastModifiedGMT, final int querySpaceBy) {
        
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_POSLIST_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_POSLIST;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameterList(POST_LIST, posList);
        q.setParameter(LAST_MODIFIED_GMT, lastModifiedGMT);
        q.setParameter(CURR_TIME, new Date());
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByLocation(final int customerId, final int locationId,
        final Date lastModifiedGMT) {
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_LOCATION);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LOCATION_ID, locationId);
        q.setParameter(LAST_MODIFIED_GMT, lastModifiedGMT);
        q.setParameter(CURR_TIME, new Date());
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByCustomer(final int customerId, final Date lastModifiedGMT,
        final int querySpaceBy) {
        
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_CUSTOMER_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATES_FROM_PURCHASE_DATE_BY_CUSTOMER;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LAST_MODIFIED_GMT, lastModifiedGMT);
        q.setParameter(CURR_TIME, new Date());
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByPosList(final int customerId, final List<PointOfSale> posList, final int querySpaceBy) {
        return this.getValidPlateListByPosList(customerId, posList, querySpaceBy, 0);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByPosList(final int customerId, final List<PointOfSale> posList, final int querySpaceBy,
        final int gracePeriod) {
        
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATES_BY_POSLIST_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATES_BY_POSLIST;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameterList(POST_LIST, posList);
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(new Date(), gracePeriod * -1));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByCustomer(final int customerId, final int querySpaceBy) {
        return this.getValidPlateListByCustomer(customerId, querySpaceBy, ZERO_GRACE_PERIOD);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByCustomer(final int customerId, final int querySpaceBy, final int gracePeriod) {
        
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATES_BY_CUSTOMER_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATES_BY_CUSTOMER;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(new Date(), gracePeriod * -1));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByLocation(final int customerId, final int locationId) {
        return this.getValidPlateListByLocation(customerId, locationId, ZERO_GRACE_PERIOD);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getValidPlateListByLocation(final int customerId, final int locationId, final int gracePeriod) {
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_VALID_PLATES_BY_LOCATION);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LOCATION_ID, locationId);
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(new Date(), gracePeriod * -1));
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByPosList(final int customerId, final List<PointOfSale> posList,
        final int gracePeriod) {
        
        final Calendar now = Calendar.getInstance();
        final Calendar yesterday = (Calendar) now.clone();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_POSLIST);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameterList(POST_LIST, posList);
        q.setParameter(YESTERDAY, yesterday.getTime());
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(now.getTime(), gracePeriod));
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByCustomer(final int customerId, final int gracePeriod) {
        
        final Calendar now = Calendar.getInstance();
        final Calendar yesterday = (Calendar) now.clone();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_CUSTOMER);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(YESTERDAY, yesterday.getTime());
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(now.getTime(), gracePeriod));
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getExpiredPlateListByCustomerWithinXMinutes(final int customerId, final int minutesBack) {
        
        final Calendar now = Calendar.getInstance();
        final Calendar yesterday = (Calendar) now.clone();
        final int minuteBackNeg = minutesBack * -1;
        yesterday.add(Calendar.MINUTE, minuteBackNeg);
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_CUSTOMER);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(YESTERDAY, yesterday.getTime());
        q.setParameter(CURR_TIME, now.getTime());
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByLocation(final int customerId, final int locationId, final int gracePeriod) {
        
        final Calendar now = Calendar.getInstance();
        final Calendar yesterday = (Calendar) now.clone();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_LOCATION);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LOCATION_ID, locationId);
        q.setParameter(YESTERDAY, yesterday.getTime());
        q.setParameter(CURR_TIME, DateUtil.addMinutesToDate(now.getTime(), gracePeriod));
        return q.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<LicencePlateInfo> getExpiredPlateListByLocationWithinXMinutes(final int customerId, final int locationId, final int minutesBack) {
        
        final Calendar now = Calendar.getInstance();
        final Calendar yesterday = (Calendar) now.clone();
        final int minuteBackNeg = minutesBack * -1;
        yesterday.add(Calendar.MINUTE, minuteBackNeg);
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXPIRED_PLATES_FROM_EXPIRE_DATE_BY_LOCATION);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LOCATION_ID, locationId);
        q.setParameter(YESTERDAY, yesterday.getTime());
        q.setParameter(CURR_TIME, now.getTime());
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final LicencePlateInfo getValidPlateByCustomerAndPlate(final int customerId, final String licencePlateNumber) {
        return getValidPlateByCustomerAndPlate(customerId, licencePlateNumber,
                                               PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final LicencePlateInfo getValidPlateByCustomerAndPlate(final int customerId, final String licencePlateNumber, final int querySpaceBy) {
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LICENCE_PLATE_NO, licencePlateNumber);
        final List<LicencePlateInfo> plateList = q.list();
        if (plateList != null && !plateList.isEmpty()) {
            return plateList.get(0);
        } else {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final LicencePlateInfo getValidPlateByLocationAndPlate(final int customerId, final String licencePlateNumber, final int locationId,
        final int querySpaceBy) {
        String query;
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            query = SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_AND_LOCATION_MOST_RECENT;
        } else {
            query = SQL_GET_VALID_PLATE_BY_CUSTOMER_AND_PLATE_AND_LOCATION;
        }
        final SQLQuery q = this.entityDao.createSQLQuery(query);
        q.addScalar(LICENCE_PLATE_NO, new StringType());
        q.addScalar(PERMIT_BEGIN_GMT, new TimestampType());
        q.addScalar(PERMIT_EXPIRE_GMT, new TimestampType());
        q.addScalar(LOCATION_NAME, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(LicencePlateInfo.class));
        q.setParameter(LOCATION_ID, locationId);
        q.setParameter(CUSTOMER_ID, customerId);
        q.setParameter(LICENCE_PLATE_NO, licencePlateNumber);
        final List<LicencePlateInfo> plateList = q.list();
        if (plateList != null && !plateList.isEmpty()) {
            return plateList.get(0);
        } else {
            return null;
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
