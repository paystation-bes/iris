package com.digitalpaytech.service.paystation.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.domain.SensorInfoType;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosBatteryInfoService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.SensorInfoTypeService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.EventSensorService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("eventSensorService")
@Transactional(propagation = Propagation.REQUIRED)
public class EventSensorServiceImpl implements EventSensorService {
    
    private static Logger logger = Logger.getLogger(EventSensorServiceImpl.class);
    private final static String PRESENT = "Present";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private SensorInfoTypeService sensorInfoTypeService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private EventAlertService eventAlertService;
    
    @Autowired
    private PosBatteryInfoService posBatteryInfoService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private RawSensorDataService rawSensorDataService;
    
    @Override
    public void processEvent(PointOfSale pointOfSale, PosSensorState sensorState, PosServiceState serviceState, EventData event) {
        boolean serviceStateChange = false;
        boolean sensorStateChange = false;
        boolean batteryInfoUpdate = false;
        
        PosSensorInfo db_s_log = null;
        
        Map<String, Float> updateState = new HashMap<String, Float>(3);
        
        PosSensorInfo posSensorInfo = new PosSensorInfo();
        posSensorInfo.setPointOfSale(pointOfSale);
        posSensorInfo.setSensorGmt(event.getTimeStamp());
        
        // set type and update service state
        String action = event.getAction();
        if (WebCoreConstants.ACTION_CONTROLLER_TEMPERATURE.equals(action)) {
            // parse value
            float value = Float.parseFloat(event.getInformation());
            
            posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                    .getMappingPropertiesValue("sensorInfoType.controller.temperature.id")));
            value = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), value);
            posSensorInfo.setAmount(value);
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                sensorState.setControllerTemperature(value);
                updateState.put(WebCoreConstants.CONTROLLER_TEMPERATURE, value);
                sensorStateChange = true;
            }
        } else if (WebCoreConstants.ACTION_AMBIENT_TEMPERATURE.equals(action)) {
            float value = Float.parseFloat(event.getInformation());
            
            posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                    .getMappingPropertiesValue("sensorInfoType.ambient.temperature.id")));
            value = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), value);
            posSensorInfo.setAmount(value);
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                sensorState.setAmbientTemperature(value);
                updateState.put(WebCoreConstants.AMBIENT_TEMPERATURE, value);
                sensorStateChange = true;
            }
        } else if (WebCoreConstants.ACTION_RELATIVE_HUMIDITY.equals(action)) {
            float value = Float.parseFloat(event.getInformation());
            
            posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                    .getMappingPropertiesValue("sensorInfoType.relative.humidity.id")));
            value = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), value);
            posSensorInfo.setAmount(value);
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                sensorState.setRelativeHumidity(value);
                updateState.put(WebCoreConstants.RELATIVE_HUMIDITY, value);
                sensorStateChange = true;
            }
        } else if (WebCoreConstants.ACTION_INPUT_CURRENT.equals(action)) {
            float value = Float.parseFloat(event.getInformation());
            
            posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                    .getMappingPropertiesValue("sensorInfoType.input.current.id")));
            value = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), value);
            posSensorInfo.setAmount(value);
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                sensorState.setInputCurrent(value);
                updateState.put(WebCoreConstants.INPUT_CURRENT, value);
                sensorStateChange = true;
            }
            batteryInfoUpdate = true;
        } else if (WebCoreConstants.ACTION_SYSTEM_LOAD.equals(action)) {
            float value = Float.parseFloat(event.getInformation());
            
            posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                    .getMappingPropertiesValue("sensorInfoType.system.load.id")));
            value = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), value);
            posSensorInfo.setAmount(value);
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                sensorState.setSystemLoad(value);
                updateState.put(WebCoreConstants.SYSTEM_LOAD, value);
                sensorStateChange = true;
            }
            batteryInfoUpdate = true;
        } else if (WebCoreConstants.ACTION_VOLTAGE.equals(action)) {
            
            // for Info String with multiple values, eg <BatteryNumber:VoltageValue>
            StringTokenizer info_tokens = new StringTokenizer(event.getInformation(), WebCoreConstants.COLON);
            
            int battery = Integer.parseInt(info_tokens.nextToken());
            if (battery < 1 || battery > 2) {
                logger.error("Unknown BatteryNumber: " + battery);
                return;
            }
            
            /*
             * For Luke/Shelby, if voltages for battery 2 are received, they shall be discarded, as battery 1 and battery 2 are connected in parallel and have
             * the same voltage.
             * http://confluence:8080/display/EMS/Pitstop+-+EMS+-+6.2+Build+02+-+Engineering+-+Technical+Specification
             */
            pointOfSale = reloadPointOfSale(pointOfSale.getId());
            if (pointOfSale.getPaystation().getPaystationType().getId() != WebCoreConstants.PAY_STATION_TYPE_TEST_INTELLEPAY && battery == 2) {
                return;
            }
            
            // Ignore NO_BATTERY messages
            String voltage_string = info_tokens.nextToken();
            if ("NOBATTERY".equals(voltage_string)) {
                return;
            }
            
            float voltage = Float.parseFloat(voltage_string);
            
            String ss_field;
            if (battery == 1) {
                posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                        .getMappingPropertiesValue("sensorInfoType.battery.voltage.id")));
                ss_field = WebCoreConstants.BATTERY_1_VOLTAGE;
                sensorState.setBattery1Voltage(voltage);
            } else {
                posSensorInfo.setSensorInfoType(sensorInfoTypeService.findSensorInfoType(transactionService
                        .getMappingPropertiesValue("sensorInfoType.battery.2.voltage.id")));
                ss_field = WebCoreConstants.BATTERY_2_VOLTAGE;
                sensorState.setBattery2Voltage(voltage);
            }
            
            // Adjust battery voltages that don't make sense
            float newVoltage = adjustSensorData(posSensorInfo.getSensorInfoType().getId(), voltage);
            posSensorInfo.setAmount(newVoltage);
            
            // Create alert if necessary
            
            if ((battery == 1 && ((sensorState.getBattery1Level() == WebCoreConstants.LEVEL_NORMAL && newVoltage < WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD) || (sensorState
                    .getBattery1Level() == WebCoreConstants.LEVEL_LOW && newVoltage > WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD)))
                || (battery == 2 && ((sensorState.getBattery2Level() == WebCoreConstants.LEVEL_NORMAL && newVoltage < WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD) || (sensorState
                        .getBattery2Level() == WebCoreConstants.LEVEL_LOW && newVoltage > WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD)))) {
                try {
                    if (battery == 1) {
                        event.setType(WebCoreConstants.EVENT_ACTION_BATTERY_1_STRING);
                    } else {
                        event.setType(WebCoreConstants.EVENT_ACTION_BATTERY_2_STRING);
                    }
                    if (newVoltage < WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD) {
                        event.setAction(WebCoreConstants.EVENT_ACTION_LOW_STRING);
                        if (battery == 1) {
                            sensorState.setBattery1Level((byte) WebCoreConstants.LEVEL_LOW);
                        } else {
                            sensorState.setBattery2Level((byte) WebCoreConstants.LEVEL_LOW);
                        }
                    } else {
                        event.setAction(WebCoreConstants.EVENT_ACTION_NORMAL_STRING);
                        if (battery == 1) {
                            sensorState.setBattery1Level((byte) WebCoreConstants.LEVEL_NORMAL);
                        } else {
                            sensorState.setBattery2Level((byte) WebCoreConstants.LEVEL_NORMAL);
                        }
                    }
                    
                    this.eventAlertService.processEvent(pointOfSale, event);
                } catch (InvalidDataException ide) {
                    
                }
            }
            
            db_s_log = findMostRecentPosSensorInfoAndEvict(pointOfSale.getId(), posSensorInfo.getSensorInfoType().getId());
            if (db_s_log == null || db_s_log.getSensorGmt().compareTo(posSensorInfo.getSensorGmt()) < 0) {
                updateState.put(ss_field, newVoltage);
                sensorStateChange = true;
            }
            batteryInfoUpdate = true;
        } else if (WebCoreConstants.EVENT_TYPE_BILL_ACCEPTOR.equalsIgnoreCase(event.getType())) {
            boolean isPresent = isPresent(event.getAction());
            if (serviceState.isIsBillAcceptor() != isPresent) {
                try {
                    serviceState.setIsBillAcceptor(isPresent);
                    posServiceStateService.updatePosServiceState(serviceState);
                } catch (Exception e) {
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("4. Unable to update PosServiceState object or table for PointOfSale id: ").append(serviceState.getPointOfSaleId());
                    bdr.append(". 'updateState' table values: ").append(updateState.toString());
                    logger.error(bdr.toString());
                }
            }
            return;
        } else if (WebCoreConstants.EVENT_TYPE_COIN_ACCEPTOR.equalsIgnoreCase(event.getType())) {
            boolean isPresent = isPresent(event.getAction());
            if (serviceState.isIsCoinAcceptor() != isPresent) {
                try {
                    serviceState.setIsCoinAcceptor(isPresent);
                    posServiceStateService.updatePosServiceState(serviceState);
                } catch (Exception e) {
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("5. Unable to update PosServiceState object or table for PointOfSale id: ").append(serviceState.getPointOfSaleId());
                    bdr.append(". 'updateState' table values: ").append(updateState.toString());
                    logger.error(bdr.toString());
                }
            }
            return;
        } else if (WebCoreConstants.EVENT_TYPE_CARD_READER.equalsIgnoreCase(event.getType())) {
            boolean isPresent = isPresent(event.getAction());
            if (serviceState.isIsCardReader() != isPresent) {
                try {
                    serviceState.setIsCardReader(isPresent);
                    posServiceStateService.updatePosServiceState(serviceState);
                } catch (Exception e) {
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("6. Unable to update PosServiceState object or table for PointOfSale id: ").append(serviceState.getPointOfSaleId());
                    bdr.append(". 'updateState' table values: ").append(updateState.toString());
                    logger.error(bdr.toString());
                }
            }
            return;
            
        } else {
            logger.warn("Received unsupported Sensor Event: " + event);
        }
        
        // Check if duplicate
        if (!posSensorInfo.equals(db_s_log)) {
            // Copy the state of the given object onto the persistent object with the same identifier..
            entityDao.merge(posSensorInfo);
            if (batteryInfoUpdate) {
                this.posBatteryInfoService.addBatteryInfo(posSensorInfo.getPointOfSale().getId(), posSensorInfo.getSensorInfoType().getId(),
                                                          posSensorInfo.getSensorGmt(), posSensorInfo.getAmount());
            }
            if (serviceStateChange) {
                updatePosServiceState(serviceState, updateState);
            }
            if (sensorStateChange) {
                updatePosSensorState(sensorState, updateState);
            }
        }
        
    }
    
    /**
     * @param updateState
     *            Map<String, Float> The String value could be 'SystemLoad', 'InputCurrent', etc.
     * @param ss
     */
    private void updatePosServiceState(PosServiceState posServiceState, Map<String, Float> updateState) {
        if (!updateState.isEmpty()) {
            Set<String> keySets = updateState.keySet();
            String[] keys = keySets.toArray(new String[keySets.size()]);
            try {
                for (String fieldName : keys) {
                    posServiceState = updateValue(posServiceState, fieldName, updateState.get(fieldName));
                }
                posServiceStateService.updatePosServiceState(posServiceState);
                
            } catch (InvalidDataException ide) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("1. Unable to update PosServiceState object or table for PointOfSale id: ").append(posServiceState.getPointOfSaleId());
                bdr.append(". 'updateState' table values: ").append(updateState.toString());
                logger.error(bdr.toString(), ide);
                mailerService.sendAdminErrorAlert("Unable to update PosServiceState", bdr.toString(), ide);
            } catch (Exception e) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("2. Unable to update PosServiceState object or table for PointOfSale id: ").append(posServiceState.getPointOfSaleId());
                bdr.append(". 'updateState' table values: ").append(updateState.toString());
                logger.error(bdr.toString());
            }
        }
    }

    private void updatePosSensorState(PosSensorState posSensorState, Map<String, Float> updateState) {
        if (!updateState.isEmpty()) {
            Set<String> keySets = updateState.keySet();
            String[] keys = keySets.toArray(new String[keySets.size()]);
            try {
                for (String fieldName : keys) {
                    posSensorState = updateValue(posSensorState, fieldName, updateState.get(fieldName));
                }
                pointOfSaleService.updatePosSensorState(posSensorState);
                
            } catch (InvalidDataException ide) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("1. Unable to update PosServiceState object or table for PointOfSale id: ").append(posSensorState.getPointOfSaleId());
                bdr.append(". 'updateState' table values: ").append(updateState.toString());
                logger.error(bdr.toString(), ide);
                mailerService.sendAdminErrorAlert("Unable to update PosServiceState", bdr.toString(), ide);
            } catch (Exception e) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("2. Unable to update PosServiceState object or table for PointOfSale id: ").append(posSensorState.getPointOfSaleId());
                bdr.append(". 'updateState' table values: ").append(updateState.toString());
                logger.error(bdr.toString());
            }
        }
    }

    /**
     * Use Java Reflection to set new values in PosServiceState object.
     * 
     * @param posServiceState
     *            PosServiceState object is ready for update.
     * @param fieldName
     *            String fieldName is placed in the map (updateState) in 'processEvent' method. fieldName is identical to domain object private property name.
     * @param value
     *            Float value which is stored in 'updateState' Map.
     * @return PosServiceState updated PosServiceState object.
     */
    @Override
    public PosServiceState updateValue(PosServiceState posServiceState, String fieldName, Float value) throws InvalidDataException {
        // fieldName is identical to domain object private property name.
        StringBuilder bdr = new StringBuilder();
        bdr.append("set").append(fieldName);
        String methodName = bdr.toString().toLowerCase();
        
        Method[] methods = posServiceState.getClass().getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().toLowerCase().indexOf(methodName) != -1) {
                m.setAccessible(true);
                try {
                    // First parameter is the object you want to invoke the method on.
                    m.invoke(posServiceState, new Object[] { value });
                    
                } catch (IllegalArgumentException iae) {
                    throw new InvalidDataException(iae);
                } catch (IllegalAccessException iace) {
                    throw new InvalidDataException(iace);
                } catch (InvocationTargetException ite) {
                    throw new InvalidDataException(ite);
                }
            }
        }
        return posServiceState;
    }

    /**
     * Use Java Reflection to set new values in PosSensorState object.
     * 
     * @param PosSensorState
     *            PosSensorState object is ready for update.
     * @param fieldName
     *            String fieldName is placed in the map (updateState) in 'processEvent' method. fieldName is identical to domain object private property name.
     * @param value
     *            Float value which is stored in 'updateState' Map.
     * @return PosServiceState updated PosServiceState object.
     */
    @Override
    public PosSensorState updateValue(PosSensorState posSensorState, String fieldName, Float value) throws InvalidDataException {
        // fieldName is identical to domain object private property name.
        StringBuilder bdr = new StringBuilder();
        bdr.append("set").append(fieldName);
        String methodName = bdr.toString().toLowerCase();
        
        Method[] methods = posSensorState.getClass().getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().toLowerCase().indexOf(methodName) != -1) {
                m.setAccessible(true);
                try {
                    // First parameter is the object you want to invoke the method on.
                    m.invoke(posSensorState, new Object[] { value });
                    
                } catch (IllegalArgumentException iae) {
                    throw new InvalidDataException(iae);
                } catch (IllegalAccessException iace) {
                    throw new InvalidDataException(iace);
                } catch (InvocationTargetException ite) {
                    throw new InvalidDataException(ite);
                }
            }
        }
        return posSensorState;
    }

    @Override
    public PosSensorInfo findMostRecentPosSensorInfoAndEvict(Integer pointOfSaleId, int sensorInfoTypeId) {
        Query q = entityDao.getNamedQuery("PosSensorInfo.findMostRecentPosSensorInfo");
        q.setParameter("pointOfSaleId", pointOfSaleId);
        q.setParameter("sensorInfoTypeId", sensorInfoTypeId);
        q.setMaxResults(1);
        
        List<PosSensorInfo> list = q.list();
        if (list != null && list.size() > 0) {
            PosSensorInfo sl = list.get(0);
            entityDao.evict(sl);
            return (sl);
        }
        return null;
    }
    
    @Override
	public void processRawSensorData(EventData event, RawSensorData sensor, int threadId) {
    	PointOfSale ps = pointOfSaleService.findPointOfSaleBySerialNumber(sensor.getSerialNumber());
    	if (ps != null) {
            PosServiceState serviceState = pointOfSaleService.findPosServiceStateByPointOfSaleId(ps.getId());
            PosSensorState sensorState= pointOfSaleService.findPosSensorStateByPointOfSaleId(ps.getId());
            
            event.setPointOfSaleId(ps.getId());
            if (logger.isDebugEnabled()) {
                logger.debug("Thread " + threadId + " processing sensor data: " + event.toString());
            }
            
            this.processEvent(ps, sensorState, serviceState, event);
            rawSensorDataService.deleteRawSensorData(sensor);
    	}
	}

	private float adjustSensorData(int typeId, float value) {
        StringBuilder bdr = new StringBuilder();
        float fixedValue = (Math.round(value * 10)) / (float) 10.0;
        SensorInfoType sensorType = sensorInfoTypeService.findSensorInfoType(typeId);
        if (sensorType.getMinValue() > value) {
            bdr.append("Invalid ").append(sensorType.getName()).append(" sensor data : ").append(fixedValue).append(", adjusted to ")
                    .append(sensorType.getMinValue());
            logger.warn(bdr.toString());
            fixedValue = sensorType.getMinValue();
        } else if (sensorType.getMaxValue() < fixedValue) {
            bdr.append("Invalid ").append(sensorType.getName()).append(" sensor data : ").append(", adjusted to ").append(sensorType.getMaxValue());
            logger.warn(bdr.toString());
            fixedValue = sensorType.getMaxValue();
            
        }
        return fixedValue;
    }
    
    private boolean isPresent(String data) {
        if (StringUtils.isNotBlank(data) && data.equalsIgnoreCase(PRESENT)) {
            return true;
        }
        return false;
    }
    
    private PointOfSale reloadPointOfSale(Integer id) {
        return (PointOfSale) entityDao.get(PointOfSale.class, id);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setMailerService(MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public void setSensorInfoTypeService(SensorInfoTypeService sensorInfoTypeService) {
        this.sensorInfoTypeService = sensorInfoTypeService;
    }
    
    public void setPosServiceStateService(PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    public void setEventAlertService(EventAlertService eventAlertService) {
        this.eventAlertService = eventAlertService;
    }
    
    public void setPosBatteryInfoService(PosBatteryInfoService posBatteryInfoService) {
        this.posBatteryInfoService = posBatteryInfoService;
    }
    
}
