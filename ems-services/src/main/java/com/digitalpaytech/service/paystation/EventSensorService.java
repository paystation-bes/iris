package com.digitalpaytech.service.paystation;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;

public interface EventSensorService {
    void processEvent(PointOfSale pointOfSale, PosSensorState sensorState, PosServiceState serviceState, EventData eventData);
    
    PosSensorInfo findMostRecentPosSensorInfoAndEvict(Integer pointOfSaleId, int sensorInfoTypeId);
    
    PosServiceState updateValue(PosServiceState posServiceState, String fieldName, Float value) throws InvalidDataException;

    PosSensorState updateValue(PosSensorState sensorState, String fieldName, Float value) throws InvalidDataException;

    void processRawSensorData(EventData event, RawSensorData sensor, int threadId);
}
