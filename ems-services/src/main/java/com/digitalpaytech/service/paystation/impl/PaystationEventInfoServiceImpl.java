package com.digitalpaytech.service.paystation.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dto.paystation.PaystationEventInfo;
import com.digitalpaytech.service.paystation.PaystationEventInfoService;

@Service("paystationEventInfoService")
@Transactional(propagation = Propagation.REQUIRED)
public class PaystationEventInfoServiceImpl implements PaystationEventInfoService { 
    private static final String POINT_OF_SALE_ID = "PointOfSaleId";
    private static final String EVENT_DEVICE_TYPE_ID = "EventDeviceTypeId";
    private static final String DEVICE_NAME = "DeviceName";
    private static final String EVENT_STATUS_TYPE_ID = "EventTypeId";
    private static final String STATUS_NAME = "StatusName";
    private static final String EVENT_ACTION_TYPE_ID = "EventActionTypeId";
    private static final String ACTION_NAME = "ActionName";
    private static final String ALERT_GMT = "AlertGmt";
    private static final String SEVERITY_NAME = "SeverityName";
    private static final String DATE_FROM = "dateFrom";
    private static final String DATE_TO = "dateTo";
    private static final String ALERT_INFO = "AlertInfo";
    private static final String EVENT_SEVERITY_TYPE_ID = "EventSeverityTypeId";
    
    private static final String SQL_EVENTS_CORE =  "SELECT DISTINCT peh.id, peh.PointOfSaleId AS PointOfSaleId,    et.EventDeviceTypeId AS EventDeviceTypeId," 
                                                   + " edt.name AS DeviceName,   peh.EventTypeId AS EventTypeId," 
                                                   + " esatt.Name AS StatusName,   peh.EventActionTypeId AS EventActionTypeId," 
                                                   + " eat.Name AS ActionName,    peh.TimestampGMT AS AlertGmt,   esevt.Name AS SeverityName," 
                                                   + " peh.EventSeverityTypeId AS EventSeverityTypeId,"  
                                                   + " peh.AlertInfo AS AlertInfo," 
                                                   + " peh.IsActive as IsActive" 
                                                   + " FROM POSEventHistory peh" 
                                                   + " INNER JOIN EventType et ON peh.EventTypeId = et.id" 
                                                   + " INNER JOIN EventDeviceType edt ON et.EventDeviceTypeId = edt.id" 
                                                   + " INNER JOIN EventSeverityType esevt ON peh.EventSeverityTypeId = esevt.Id" 
                                                   + " INNER JOIN EventDefinition ed ON ed.EventTypeId =  peh.EventTypeId"
                                                   + " LEFT JOIN EventStatusType esatt ON ed.EventStatusTypeId = esatt.Id"
                                                   + " LEFT JOIN EventActionType eat ON peh.EventActionTypeId = eat.Id";
                                                  
    private static final String SQL_WHERE_LIST = " WHERE peh.PointOfSaleId IN (:posList) AND et.EventDeviceTypeId IN (:deviceList)";
    private static final String SQL_WHERE_UNIQUE = " WHERE peh.PointOfSaleId = :pointOfSaleId AND et.EventDeviceTypeId = :deviceId";
    private static final String SQL_WHERE_ADD_EVENTTYPE = " AND peh.EventTypeId >= :statusId";
    private static final String SQL_WHERE_ADD_DATE_FROM = " AND peh.TimestampGMT >= :dateFrom";
    private static final String SQL_WHERE_ADD_DATE_TO = " AND peh.TimestampGMT <= :dateTo";
    private static final String SQL_WHERE_ADD_DATE_RANGE = " AND peh.TimestampGMT BETWEEN :dateFrom AND :dateTo";
    private static final String SQL_ORDER_BY_ALERTGMT = " ORDER BY peh.TimestampGMT DESC";
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @SuppressWarnings("PMD.ConfusingTernary")
    public final List<PaystationEventInfo> findEventsforPosList(final Collection<Integer> posList, final Collection<Integer> deviceList,
        final Date dateFrom, final Date dateTo, final int limit) {
        final StringBuilder bld = new StringBuilder(SQL_EVENTS_CORE);
        bld.append(SQL_WHERE_LIST);
        if (dateFrom != null && dateTo != null) {
            bld.append(SQL_WHERE_ADD_DATE_RANGE);
        } else if (dateTo != null) {
            bld.append(SQL_WHERE_ADD_DATE_TO);
        } else if (dateFrom != null) {
            bld.append(SQL_WHERE_ADD_DATE_FROM);
        }
        bld.append(SQL_ORDER_BY_ALERTGMT);
        
        final SQLQuery q = this.entityDao.createSQLQuery(bld.toString());
        q.addScalar(POINT_OF_SALE_ID, new IntegerType());
        q.addScalar(EVENT_DEVICE_TYPE_ID, new IntegerType());
        q.addScalar(DEVICE_NAME, new StringType());
        q.addScalar(EVENT_STATUS_TYPE_ID, new IntegerType());
        q.addScalar(STATUS_NAME, new StringType());
        q.addScalar(EVENT_ACTION_TYPE_ID, new IntegerType());
        q.addScalar(ACTION_NAME, new StringType());
        q.addScalar(ALERT_GMT, new TimestampType());
        q.addScalar(SEVERITY_NAME, new StringType());
        q.addScalar(EVENT_SEVERITY_TYPE_ID, new IntegerType());
        q.addScalar(ALERT_INFO, new StringType());
        q.addScalar("IsActive", new IntegerType());
        q.setResultTransformer(Transformers.aliasToBean(PaystationEventInfo.class));
        q.setParameterList("posList", posList);
        q.setParameterList("deviceList", deviceList);
        if (dateFrom != null) {
            q.setParameter(DATE_FROM, dateFrom);
        }
        if (dateTo != null) {
            q.setParameter(DATE_TO, dateTo);
        }
        q.setMaxResults(limit);
        return q.list();
    }
    
    @Override
    @SuppressWarnings("PMD.ConfusingTernary")
    public final List<PaystationEventInfo> findEventsforPos(final int pointOfSaleId, final int deviceId, final int statusId, final Date dateFrom,
        final Date dateTo, final int limit) {
        final StringBuilder bld = new StringBuilder(SQL_EVENTS_CORE);
        bld.append(SQL_WHERE_UNIQUE).append(SQL_WHERE_ADD_EVENTTYPE);
        if (dateFrom != null && dateTo != null) {
            bld.append(SQL_WHERE_ADD_DATE_RANGE);
        } else if (dateTo != null) {
            bld.append(SQL_WHERE_ADD_DATE_TO);
        } else if (dateFrom != null) {
            bld.append(SQL_WHERE_ADD_DATE_FROM);
        }
        bld.append(SQL_ORDER_BY_ALERTGMT);
        
        final SQLQuery q = this.entityDao.createSQLQuery(bld.toString());
        q.addScalar(POINT_OF_SALE_ID, new IntegerType());
        q.addScalar(EVENT_DEVICE_TYPE_ID, new IntegerType());
        q.addScalar(DEVICE_NAME, new StringType());
        q.addScalar(EVENT_STATUS_TYPE_ID, new IntegerType());
        q.addScalar(STATUS_NAME, new StringType());
        q.addScalar(EVENT_ACTION_TYPE_ID, new IntegerType());
        q.addScalar(ACTION_NAME, new StringType());
        q.addScalar(ALERT_GMT, new TimestampType());
        q.addScalar(SEVERITY_NAME, new StringType());
        q.addScalar(EVENT_SEVERITY_TYPE_ID, new IntegerType());
        q.addScalar(ALERT_INFO, new StringType());
        q.setResultTransformer(Transformers.aliasToBean(PaystationEventInfo.class));
        q.setParameter("pointOfSaleId", pointOfSaleId);
        q.setParameter("deviceId", deviceId);
        q.setParameter("statusId", statusId);
        if (dateFrom != null) {
            q.setParameter(DATE_FROM, dateFrom);
        }
        if (dateTo != null) {
            q.setParameter(DATE_TO, dateTo);
        }
        q.setMaxResults(limit);
        return q.list();
    }
    
}
