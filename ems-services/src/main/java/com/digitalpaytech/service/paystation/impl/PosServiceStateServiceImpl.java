package com.digitalpaytech.service.paystation.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.paystation.PosServiceStateInitInfo;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.DateUtil;

@Service("posServiceStateService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PosServiceStateServiceImpl implements PosServiceStateService {

    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;

    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }

    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }    
    
    @Override
    public PosServiceState findPosServiceStateById(final Integer id, final boolean isEvicted) {
        final PosServiceState result = (PosServiceState) this.entityDao.get(PosServiceState.class, id);
        if (isEvicted) {
            this.entityDao.evict(result);
        }
        return result;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePosServiceState(final PosServiceState posServiceState) {
        this.entityDao.update(this.entityDao.merge(posServiceState));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePosServiceStates(final Collection<PosServiceState> posServiceStates) {
        this.entityDao.saveOrUpdateAll(posServiceStates);
    }
    
    @Override
    public Collection<PosServiceState> findAllPosServiceStates(final boolean isEvicted) {
        final Collection<PosServiceState> resultList = this.entityDao.loadAll(PosServiceState.class);
        if (isEvicted) {
            for (PosServiceState posServiceState : resultList) {
                this.entityDao.evict(posServiceState);
            }
        }
        return resultList;
    }
    
    /**
     * Calls from InitializationHandler.java and processInitialization updates the following:
     * - bbserialNumber
     * - paystationSetting
     * - primaryVersion
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void processInitialization(final Integer id, final PosServiceStateInitInfo posServiceStateInitInfo) {
        final PosServiceState result = (PosServiceState) this.entityDao.get(PosServiceState.class, id);
        result.setBbserialNumber(posServiceStateInitInfo.getBbSerialNumber());
        result.setPaystationSettingName(posServiceStateInitInfo.getPaystationSetting());
        result.setPrimaryVersion(posServiceStateInitInfo.getPrimaryVersion());
        updatePosServiceState(result);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void checkforEMVMerchantAccount(final MerchantAccount merchantAccount, final List<Integer> posIds) {
        final Date now = DateUtil.getCurrentGmtDate();
        final List<PosServiceState> posServiceStates 
            = this.entityDao.getNamedQuery("PosServiceState.findPosServiceStateByPointOfSaleIds").setParameterList("pointOfSaleIds", posIds).list();
        
        for (PosServiceState posServiceState : posServiceStates) {
            if (merchantAccount != null && merchantAccount.getProcessor() != null && merchantAccount.getProcessor().getIsEMV()) {
                posServiceState.setIsNewMerchantAccount(true);
                posServiceState.setMerchantAccountChangedGMT(now);
            } else {
                posServiceState.setMerchantAccountChangedGMT(null);
            }
            posServiceState.setMerchantAccountRequestedGMT(null);
            posServiceState.setMerchantAccountUploadedGMT(null);
            
            this.entityDao.update(posServiceState);
        }
    }
    
    @Override
    public List<PosServiceState> findPosServiceStateByNextSettingsFileId(final Integer nextSettingsFileId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosServiceState.findPosServiceStateWaitingForPaystationSettingUpdate", "settingFileId",
                                                            nextSettingsFileId);
    }

    @Override
    public Collection<PosServiceState> findLinuxPosServiceStatesByLinkCustomerId(final Integer linkCustomerId) {
        final Customer customer = this.customerService.findCustomerByUnifiId(linkCustomerId);
        if (customer != null) {
            final List<PointOfSale> posList = this.pointOfSaleService.findLinuxPointOfSalesByCustomerId(customer.getId());
            if (posList != null && !posList.isEmpty()) {
                final List<Integer> posIds = posList.stream().map(PointOfSale::getId).collect(Collectors.toList());
                return this.entityDao.getNamedQuery("PosServiceState.findPosServiceStateByPointOfSaleIds").setParameterList("pointOfSaleIds", posIds).list();
            }
        }
        return new ArrayList<PosServiceState>();
    }
    
    
    @Override
    public PosServiceState findLinuxPosServiceStateBySerialNumber(final String serialNumber) {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
        return this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pos.getId());
    }

    @Override
    public Collection<PosServiceState> findAllLinuxPosServiceStates() {
        return this.entityDao.findByNamedQuery("PosServiceState.findAllLinuxPosServiceStates", true);
    }
}
