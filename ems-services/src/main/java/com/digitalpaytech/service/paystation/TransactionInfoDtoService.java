package com.digitalpaytech.service.paystation;

import java.util.List;

import com.digitalpaytech.dto.paystation.TransactionInfoDto;

public interface TransactionInfoDtoService {
    
    public List<TransactionInfoDto> getTransactionList(int reqType, Object[] values);
    
    public int getTransactionListCount(int reqType, Object[] values);
}
