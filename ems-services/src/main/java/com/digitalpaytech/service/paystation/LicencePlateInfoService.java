package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.LicencePlateInfo;

public interface LicencePlateInfoService {
    
    List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByPosList(int customerId, List<PointOfSale> posList, Date lastModifiedGMT,
                                                                             int querySpaceBy);
    
    List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByCustomer(int customerId, Date lastModifiedGMT, int querySpaceBy);
    
    List<LicencePlateInfo> getValidPlateListFromPurchaseTimeByLocation(int customerId, int locationId, Date permitBeginGmt);
    
    List<LicencePlateInfo> getValidPlateListByPosList(int customerId, List<PointOfSale> posList, int querySpaceBy);
    
    List<LicencePlateInfo> getValidPlateListByPosList(int customerId, List<PointOfSale> posList, int querySpaceBy, int gracePeriod);
    
    List<LicencePlateInfo> getValidPlateListByCustomer(int customerId, int querySpaceBy);
    
    List<LicencePlateInfo> getValidPlateListByCustomer(int customerId, int querySpaceBy, int gracePeriod);
    
    List<LicencePlateInfo> getValidPlateListByLocation(int customerId, int locationId);
    
    List<LicencePlateInfo> getValidPlateListByLocation(int customerId, int locationId, int gracePeriod);
    
    List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByPosList(int customerId, List<PointOfSale> posList, int gracePeriod);
    
    List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByCustomer(int customerId, int gracePeriod);
    
    List<LicencePlateInfo> getExpiredPlateListFromExpireTimeByLocation(int customerId, int locationId, int gracePeriod);
    
    LicencePlateInfo getValidPlateByCustomerAndPlate(int customerId, String licencePlateNumber);
    
    LicencePlateInfo getValidPlateByCustomerAndPlate(int customerId, String licencePlateNumber, int querySpaceBy);
    
    List<LicencePlateInfo> getExpiredPlateListByCustomerWithinXMinutes(int customerId, int minutesBack);
    
    List<LicencePlateInfo> getExpiredPlateListByLocationWithinXMinutes(int customerId, int locationId, int minutesBack);
    
    LicencePlateInfo getValidPlateByLocationAndPlate(int customerId, String licencePlateNumber, int locationId, int querySpaceBy);
    
}
