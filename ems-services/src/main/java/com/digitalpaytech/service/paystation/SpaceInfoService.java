package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.SpaceInfo;

public interface SpaceInfoService {
    
    public SpaceInfo getSpaceInfoByCustomerAndPlateNumber(int customerId, Date currentPosDate, String plateNumber, int querySpaceby);
    public SpaceInfo getSpaceInfoByLocationAndPlateNumber(int customerId, int locationId, Date currentPosDate, String plateNumber);
    
    @Deprecated
    public SpaceInfo getValidByPaystationSettingPurchaseExpiryAddTime(String paystationSettingName, int addTimeNumber, Date currentPosDate, int customerId);
    
    public SpaceInfo getValidByLocationPurchaseExpiryAddTime(int locationId, int addTimeNumber, Date currentPosDate, int customerId);
    public SpaceInfo getValidByLocationPurchaseExpiryAddTimeOrderByLatestExpiry(int locationId, int addTimeNumber, Date currentPosDate, int customerId);
    
    public SpaceInfo getValidByCustomerPurchaseExpiryAddTime(int addTimeNumber, Date currentPosDate, int customerId);
    public SpaceInfo getValidByCustomerPurchaseExpiryAddTimeOrderByLatestExpiry(int addTimeNumber, Date currentPosDate, int customerId);
    
    @Deprecated
    public SpaceInfo getValidByPaystationSettingPurchaseExpirySpaceNumber(String paystationSettingName, int spaceNumber, Date currentPosDate, int customerId);
    
    public SpaceInfo getValidByLocationPurchaseExpirySpaceNumber(int locationId, int spaceNumber, Date currentPosDate, int customerId);
    public SpaceInfo getValidByLocationPurchaseExpirySpaceNumberOrderByLatestExpiry(int locationId, int spaceNumber, Date currentPosDate, int customerId);
    
    public SpaceInfo getValidByCustomerPurchaseExpirySpaceNumber(int spaceNumber, Date currentPosDate, int customerId);
    public SpaceInfo getValidByCustomerPurchaseExpirySpaceNumberOrderByLatestExpiry(int spaceNumber, Date currentPosDate, int customerId);

    public List<SpaceInfo> getSpaceInfoListByCustomerAndSpaceRangeForPosDate(int customerId, int startSpace, int endSpace, int stallType, Date currentPosDate,
                                                                             boolean isActive);
    
    public List<SpaceInfo> getSpaceInfoListByLocationAndSpaceRangeForPosDate(int customerId, int locationId, int startSpace, int endSpace, int stallType,
                                                                             Date currentPosDate, boolean isActive);
    
    @Deprecated
    public List<SpaceInfo> getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDate(int customerId, String paystationSettingName, int startSpace, int endSpace,
                                                                               int stallType, Date currentPosDate, boolean isActive);
    
    public List<SpaceInfo> getSpaceInfoListByPosListAndSpaceRangeForPosDate(int customerId, List<PointOfSale> posList, int startSpace, int endSpace,
                                                                            int stallType, Date currentPosDate, boolean isActive);
    
    public List<SpaceInfo> getSpaceInfoListByLocationAndSpaceRangeForPosDatePDA(int customerId, int locationId, int startSpace, int endSpace, int stallType,
            Date posDate, boolean isActive);
    
    public List<SpaceInfo> getSpaceInfoListByCustomerAndSpaceRangeForPosDatePDA(int customerId, int startSpace, int endSpace, int stallType, Date posDate,
            boolean isActive);
    
    @Deprecated
    public List<SpaceInfo> getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDatePDA(int customerId, String paystationSettingName, int startSpace, int endSpace,
            int stallType, Date posDate, boolean isActive);
    
}
