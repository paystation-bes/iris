package com.digitalpaytech.service.paystation.impl;

import java.io.IOException;
import java.io.InputStream;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventStatusType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("eventAlertService")
@Transactional(propagation = Propagation.REQUIRED)
public class EventAlertServiceImpl implements EventAlertService {
    
    private static final long NUM_DAYS_FUTURE = 2;
    
    private static Logger LOG = Logger.getLogger(EventAlertServiceImpl.class);
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    @Qualifier(value = "kafkaEventService")
    private QueueEventService queueEventService;    
    
    @Autowired
    private EventTypeService eventTypeService;
    
    @Autowired
    private EntityDao entityDao;
    
    private Set<String> positiveActions;
    
    private Properties eventsAlertsProp;
    private Map<String, EventDefinition> eventDefinitionsMap;
    private Map<String, EventDeviceType> eventDeviceTypesMap;
    
    @PostConstruct
    public final void loadPositiveEventActions() {
        // Loads events_alerts.properties.
        this.eventsAlertsProp = new Properties();
        final String msg = "EventAlertServiceImpl, cannot load events_alerts.properties file !";
        final InputStream is = EventAlertServiceImpl.class.getResourceAsStream(WebCoreConstants.EVENTS_ALERTS_PROPERTIES);
        try {
            this.eventsAlertsProp.load(is);
        } catch (IOException ioe) {
            LOG.error(msg, ioe);
            throw new RuntimeException(msg, ioe);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe2) {
                    LOG.error(ioe2);
                }
            }
        }
        if (this.eventsAlertsProp.size() == 0) {
            throw new RuntimeException("EventAlertServiceImpl, events_alerts.properties is empty.");
        }
        
        // Uses Set object to prevent duplication of positive keywords.
        this.positiveActions = new HashSet<String>();
        String key = null;
        String[] values = null;
        
        // Gets all properties names, e.g. event.paystation.positive.keywords.
        final Enumeration<?> e = this.eventsAlertsProp.propertyNames();
        while (e.hasMoreElements()) {
            key = (String) e.nextElement();
            // Iterates each property name and checks if's positive keys. If it is, changes content to lower case and adds to positiveActions.
            if (key.indexOf("positive.keywords") != -1) {
                values = this.eventsAlertsProp.getProperty(key).toLowerCase().split(StandardConstants.STRING_COMMA);
                this.positiveActions.addAll(Arrays.asList(values));
            }
            
        }
        
        LOG.info("EventAlertServiceImpl, loadPositiveEventActions completed, size: " + this.positiveActions.size());
    }
    
    /**
     * Processes incoming events & alerts that could contain new issues, duplicate of existing issues or cleared events.
     * - If event is an alert event send it to the alert framework
     * 
     * @param pointOfSale
     *            PointOfSale object for corresponding pay station.
     * @param event
     *            EventData object contains all data in event xml.
     */
    @Override
    public final void processEvent(final PointOfSale pointOfSale, final EventData event) throws InvalidDataException {
        
        final boolean isCleared = this.positiveActions.contains(event.getAction().toLowerCase());
        boolean isNewEvent = true;
        boolean isNewSensorEvent = false;
        
        final Paystation paystation = this.pointOfSaleService.findPayStationByPointOfSaleId(pointOfSale.getId());
        final int paystationTypeId = paystation.getPaystationType().getId();
        
        final EventDeviceType eventDeviceType = loadEventDeviceTypes().get(event.getType());
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(eventDeviceType.getId()).append(StandardConstants.STRING_UNDERSCORE).append(event.getAction());
        // It could be null because eventDefinitionsMap doesn't contain the data. 
        final EventDefinition eventDefinition = loadEventDefinitions().get(bdr.toString());
        
        if (WebCoreConstants.EVENT_TYPE_BILL_ACCEPTOR.equals(event.getType()) || WebCoreConstants.EVENT_TYPE_BILL_STACKER.equals(event.getType())
            || WebCoreConstants.EVENT_TYPE_CARD_READER.equals(event.getType()) || WebCoreConstants.EVENT_TYPE_COIN_ACCEPTOR.equals(event.getType())
            || WebCoreConstants.EVENT_TYPE_COIN_CANISTER.equals(event.getType()) || WebCoreConstants.EVENT_TYPE_COIN_CHANGER.equals(event.getType())
            || WebCoreConstants.EVENT_TYPE_COIN_ESCROW.equals(event.getType()) || WebCoreConstants.EVENT_TYPE_PRINTER.equals(event.getType())
            || WebCoreConstants.EVENT_TYPE_PAYSTATION.equals(event.getType()) || WebCoreConstants.EVENT_TYPE_RFIDCARDREADER.equals(event.getType())) {
            
            isNewEvent = updateServiceStatus(pointOfSale, event);
            isNewSensorEvent = updateSensorStatus(pointOfSale, event);
            if (!isNewEvent && isNewSensorEvent) {
                isNewEvent = isNewSensorEvent;
            }
        } else if ((WebCoreConstants.EVENT_TYPE_COIN_HOPPER + 1).equals(event.getType())
                   || (WebCoreConstants.EVENT_TYPE_COIN_HOPPER + 2).equals(event.getType())) {
            if (paystationTypeId == WebCoreConstants.PAY_STATION_TYPE_SHELBY) {
                isNewEvent = updateServiceStatus(pointOfSale, event);
            }
        }
        
        if (eventDefinition != null) {
            final ZoneId gmtZone = ZoneId.of(StableDateUtil.GMT);
            final Date timeStamp;
            
            if (event.getTimeStamp() != null && StableDateUtil.isDateNDaysInTheFuture(NUM_DAYS_FUTURE, event.getTimeStamp(), gmtZone)) {
                timeStamp = StableDateUtil.getCurrentGmtDate();
                LOG.info(String.format("+++ Received Event with timeStamp in the Future. TimeStamp was correct to [%s]. Event is: [%s]", timeStamp,
                                       event));
            } else {
                timeStamp = event.getTimeStamp();
            }
            
            this.queueEventService.createEventQueue(pointOfSale.getId(), paystationTypeId, null, eventDefinition.getEventType(), eventDefinition
                    .getEventSeverityType().getId(), eventDefinition.getEventActionType().getId(), timeStamp, event.getInformation(),
                                                    !isCleared, eventDefinition.getEventType().isIsNotification(),
                                                    WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID, (byte) (eventDefinition.getEventType()
                                                            .isIsAlert() ? WebCoreConstants.QUEUE_TYPE_RECENT_EVENT
                                                                           | WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT : 0));
        } else {
            LOG.info("+++ Received unsupported Event no Alert is created: " + event);
        }
    }
    
    // Set Present/NotPresent values
    private boolean updateServiceStatus(final PointOfSale pos, final EventData event) {
        final PosServiceState posServiceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pos.getId());
        boolean isNewEvent = false;
        
        if (WebCoreConstants.EVENT_ACTION_UID_CHANGED_STRING.equals(event.getAction())
            || WebCoreConstants.EVENT_ACTION_UID_ALERT_STRING.equals(event.getAction())
            || WebCoreConstants.EVENT_ACTION_UID_CLEARED_STRING.equals(event.getAction())) {
            return isNewEvent;
        } else if (WebCoreConstants.EVENT_TYPE_BILL_ACCEPTOR.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsBillAcceptor()) {
                    posServiceState.setIsBillAcceptor(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsBillAcceptor()) {
                    posServiceState.setIsBillAcceptor(false);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_BILL_STACKER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_BILLS_ACCEPTED_STRING.equals(event.getAction())) {
                // posServiceState.setBillStackerCount((short) (posServiceState.getBillStackerCount() + Short.parseShort(event.getInformation())));
                // isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsBillStacker()) {
                    posServiceState.setIsBillStacker(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsBillStacker()) {
                    posServiceState.setIsBillStacker(false);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_RESET_STRING.equals(event.getAction())) {
                posServiceState.setBillStackerCount((short) 0);
                isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_FULL_STRING.equals(event.getAction())) {
                if (posServiceState.getBillStackerLevel() != (byte) WebCoreConstants.LEVEL_FULL) {
                    posServiceState.setBillStackerLevel((byte) WebCoreConstants.LEVEL_FULL);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_FULL_CLEARED_STRING.equals(event.getAction())) {
                if (posServiceState.getBillStackerLevel() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                    posServiceState.setBillStackerLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_CARD_READER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCardReader()) {
                    posServiceState.setIsCardReader(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCardReader()) {
                    posServiceState.setIsCardReader(false);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_COIN_ACCEPTOR.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_COINS_ACCEPTED_STRING.equals(event.getAction())) {
                // posServiceState.setCoinBagCount((short) (posServiceState.getCoinBagCount() + Short.parseShort(event.getInformation())));
                // isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinAcceptor()) {
                    posServiceState.setIsCoinAcceptor(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinAcceptor()) {
                    posServiceState.setIsCoinAcceptor(false);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_RESET_STRING.equals(event.getAction())) {
                posServiceState.setCoinBagCount((short) 0);
                isNewEvent = true;
            }
        } else if (WebCoreConstants.EVENT_TYPE_COIN_CANISTER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinCanister()) {
                    posServiceState.setIsCoinCanister(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinCanister()) {
                    posServiceState.setIsCoinCanister(false);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_INSERTED_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinCanister()) {
                    posServiceState.setIsCoinCanister(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_REMOVED_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinCanister()) {
                    posServiceState.setIsCoinCanister(false);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_COIN_CHANGER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.getIsCoinChanger() == 0) {
                    posServiceState.setIsCoinChanger((byte) 1);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.getIsCoinChanger() == 1) {
                    posServiceState.setIsCoinChanger((byte) 0);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_EMPTY_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinChangerLevel() != (byte) WebCoreConstants.LEVEL_EMPTY) {
                    posServiceState.setCoinChangerLevel((byte) WebCoreConstants.LEVEL_EMPTY);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_LOW_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinChangerLevel() != (byte) WebCoreConstants.LEVEL_LOW) {
                    posServiceState.setCoinChangerLevel((byte) WebCoreConstants.LEVEL_LOW);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NORMAL_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinChangerLevel() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                    posServiceState.setCoinChangerLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_COIN_ESCROW.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinEscrow()) {
                    posServiceState.setIsCoinEscrow(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinEscrow()) {
                    posServiceState.setIsCoinEscrow(false);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_DISABLED_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinEscrow()) {
                    posServiceState.setIsCoinEscrow(false);
                    isNewEvent = true;
                }
             }
            
        } else if ((WebCoreConstants.EVENT_TYPE_COIN_HOPPER + 1).equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_COINS_DISPENSED_STRING.equals(event.getAction())) {
                // posServiceState
                //  .setCoinHopper1dispensedCount((short) (posServiceState.getCoinHopper1dispensedCount() + Short.parseShort(event.getInformation())));
                // isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinHopper1()) {
                    posServiceState.setIsCoinHopper1(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinHopper1()) {
                    posServiceState.setIsCoinHopper1(false);
                    posServiceState.setCoinHopper1dispensedCount((short) 0);
                    posServiceState.setCoinHopper1level((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_EMPTY_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinHopper1level() != (byte) WebCoreConstants.LEVEL_EMPTY) {
                    posServiceState.setCoinHopper1level((byte) WebCoreConstants.LEVEL_EMPTY);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_EMPTY_CLEARED_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinHopper1level() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                    posServiceState.setCoinHopper1level((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            }
        } else if ((WebCoreConstants.EVENT_TYPE_COIN_HOPPER + 2).equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_COINS_DISPENSED_STRING.equals(event.getAction())) {
                // posServiceState
                //  .setCoinHopper2dispensedCount((short) (posServiceState.getCoinHopper2dispensedCount() + Short.parseShort(event.getInformation())));
                // isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsCoinHopper2()) {
                    posServiceState.setIsCoinHopper2(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsCoinHopper2()) {
                    posServiceState.setIsCoinHopper2(false);
                    posServiceState.setCoinHopper2dispensedCount((short) 0);
                    posServiceState.setCoinHopper2level((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_EMPTY_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinHopper2level() != (byte) WebCoreConstants.LEVEL_EMPTY) {
                    posServiceState.setCoinHopper2level((byte) WebCoreConstants.LEVEL_EMPTY);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_EMPTY_CLEARED_STRING.equals(event.getAction())) {
                if (posServiceState.getCoinHopper2level() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                    posServiceState.setCoinHopper2level((byte) WebCoreConstants.LEVEL_NORMAL);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_RFIDCARDREADER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsRfidCardReader()) {
                    posServiceState.setIsRfidCardReader(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsRfidCardReader()) {
                    posServiceState.setIsRfidCardReader(false);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_PRINTER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsPrinter()) {
                    posServiceState.setIsPrinter(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsPrinter()) {
                    posServiceState.setIsPrinter(false);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_PAPER_TICKET_DID_NOT_PRINT_STRING.equals(event.getAction())
                       || WebCoreConstants.EVENT_ACTION_PAPER_PRINTING_FAILURE_STRING.equals(event.getAction())) {
                isNewEvent = true;
            }
        } else if (WebCoreConstants.EVENT_TYPE_RFIDCARDREADER.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PRESENT_STRING.equals(event.getAction())) {
                if (!posServiceState.isIsRfidCardReader()) {
                    posServiceState.setIsRfidCardReader(true);
                    isNewEvent = true;
                }
            } else if (WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING.equals(event.getAction())) {
                if (posServiceState.isIsRfidCardReader()) {
                    posServiceState.setIsRfidCardReader(false);
                    isNewEvent = true;
                }
            }
        } else if (WebCoreConstants.EVENT_TYPE_PAYSTATION.equals(event.getType())) {
            if (WebCoreConstants.EVENT_ACTION_PUBLIC_KEY_UPDATE_STRING.equals(event.getAction())) {
                posServiceState.setIsNewPublicKey(false);
                isNewEvent = true;
                
            } else if (WebCoreConstants.EVENT_ACTION_TICKET_NOT_TAKEN_STRING.equals(event.getAction())
                       || WebCoreConstants.EVENT_ACTION_TICKET_NOT_TAKEN_CLEARED_STRING.equals(event.getAction())) {
                isNewEvent = true;
            } else if (WebCoreConstants.EVENT_ACTION_UPGRADE_STRING.equals(event.getAction())) {
                final String[] versions = event.getInformation().split(StandardConstants.STRING_COLON);
                if (versions.length == 2) {
                    posServiceState.setPrimaryVersion(versions[0]);
                    posServiceState.setSecondaryVersion(versions[1]);
                }
            }
        }
        
        this.posServiceStateService.updatePosServiceState(posServiceState);
        return isNewEvent;
    }
    
    // Set Present/NotPresent values
    private boolean updateSensorStatus(final PointOfSale pos, final EventData event) {
        final PosSensorState posSensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pos.getId());
        boolean isNewEvent = false;
        
        if (WebCoreConstants.EVENT_TYPE_PRINTER.equals(event.getType())) {
            if (posSensorState.getLastPaperLevelGmt() == null || posSensorState.getLastPaperLevelGmt().before(event.getTimeStamp())) {
                if (WebCoreConstants.EVENT_ACTION_PAPER_OUT_STRING.equals(event.getAction())) {
                    if (posSensorState.getPrinterPaperLevel() != (byte) WebCoreConstants.LEVEL_EMPTY) {
                        posSensorState.setPrinterPaperLevel((byte) WebCoreConstants.LEVEL_EMPTY);
                    }
                    posSensorState.setLastPaperLevelGmt(event.getTimeStamp());
                    isNewEvent = true;
                } else if (WebCoreConstants.EVENT_ACTION_PAPER_LOW_STRING.equals(event.getAction())) {
                    if (posSensorState.getPrinterPaperLevel() != (byte) WebCoreConstants.LEVEL_LOW) {
                        posSensorState.setPrinterPaperLevel((byte) WebCoreConstants.LEVEL_LOW);
                    }
                    posSensorState.setLastPaperLevelGmt(event.getTimeStamp());
                    isNewEvent = true;
                } else if (WebCoreConstants.EVENT_ACTION_PAPER_OUT_CLEARED_STRING.equals(event.getAction())
                           || WebCoreConstants.EVENT_ACTION_PAPER_LOW_CLEARED_STRING.equals(event.getAction())
                           || WebCoreConstants.EVENT_ACTION_PAPER_NORMAL_STRING.equals(event.getAction())
                           || WebCoreConstants.EVENT_ACTION_PAPER_TICKET_DID_NOT_PRINT_CLEARED_STRING.equals(event.getAction())
                           || WebCoreConstants.EVENT_ACTION_PAPER_PRINTING_FAILURE_CLEARED_STRING.equals(event.getAction())) {
                    if (posSensorState.getPrinterPaperLevel() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                        posSensorState.setPrinterPaperLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                    }
                    posSensorState.setLastPaperLevelGmt(event.getTimeStamp());
                    isNewEvent = true;
                }
            }
            if (posSensorState.getLastPrinterStatusGmt() == null || posSensorState.getLastPrinterStatusGmt().before(event.getTimeStamp())) {
                if (WebCoreConstants.EVENT_ACTION_PAPER_JAM_STRING.equals(event.getAction())) {
                    if (posSensorState.getPrinterStatus() != (byte) WebCoreConstants.LEVEL_JAM) {
                        posSensorState.setPrinterStatus((byte) WebCoreConstants.LEVEL_JAM);
                    }
                    posSensorState.setLastPrinterStatusGmt(event.getTimeStamp());
                    isNewEvent = true;
                } else if (WebCoreConstants.EVENT_ACTION_PAPER_JAM_CLEARED_STRING.equals(event.getAction())) {
                    if (posSensorState.getPrinterStatus() != (byte) WebCoreConstants.LEVEL_NORMAL) {
                        posSensorState.setPrinterStatus((byte) WebCoreConstants.LEVEL_NORMAL);
                    }
                    posSensorState.setLastPrinterStatusGmt(event.getTimeStamp());
                    isNewEvent = true;
                }
            }
        }
        if (isNewEvent) {
            this.pointOfSaleService.updatePosSensorState(posSensorState);
        }
        return isNewEvent;
    }
    
    private boolean hasSubscription(final int subscriptionType, final List<CustomerSubscription> customerSubscriptions) {
        final Iterator<CustomerSubscription> iter = customerSubscriptions.iterator();
        while (iter.hasNext()) {
            if (iter.next().getSubscriptionType().getId() == subscriptionType) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Set<String> getPositiveActions() {
        return this.positiveActions;
    }
    
    /**
     * Performs lazy-loading for EventDefinition table and ehcache is configured.
     * 
     * @return Map<String, EventDefinition> key is combination of eventDeviceType.id + "_" + eventDefinition.descriptionNoSpace, e.g. 2_Jam.
     */
    private Map<String, EventDefinition> loadEventDefinitions() {
        if (this.eventDefinitionsMap == null) {
            final StringBuilder bdr = new StringBuilder();
            final List<EventDefinition> eventDef = this.entityDao.findByNamedQuery("EventDefinition.findAll");
            // TODO keep or kill??
            //        eventDef.addAll(createPaystationDoorEvents());
            //TODO has to be added in UI because has the same property as PaperOutCleared and cannot be added to the DB
            eventDef.add(createPaperLowClearedEvent());
            eventDef.addAll(createAlarmOnOffEvent());
            
            this.eventDefinitionsMap = new HashMap<String, EventDefinition>(eventDef.size());
            final Iterator<EventDefinition> iter2 = eventDef.iterator();
            while (iter2.hasNext()) {
                final EventDefinition ed = iter2.next();
                bdr.append(ed.getEventDeviceType().getId()).append(StandardConstants.STRING_UNDERSCORE).append(ed.getDescriptionNoSpace());
                this.eventDefinitionsMap.put(bdr.toString(), ed);
                bdr.replace(0, bdr.length(), "");
            }
        }
        return this.eventDefinitionsMap;
    }
    
    private EventDefinition createPaperLowClearedEvent() {
        EventStatusType eventStatusType = new EventStatusType(13, "Paper");
        EventActionType eventActionType = new EventActionType(2, "Cleared");
        EventSeverityType eventSeverityType = new EventSeverityType(0, "Clear");
        EventDeviceType eventDeviceType = new EventDeviceType(13, "Printer", "Printer");
        
        final EventDefinition eventDefinition = new EventDefinition();
        eventDefinition.setEventStatusType(eventStatusType);
        eventDefinition.setEventActionType(eventActionType);
        eventDefinition.setEventSeverityType(eventSeverityType);
        eventDefinition.setEventDeviceType(eventDeviceType);
        eventDefinition.setEventType(this.eventTypeService.findEventTypeById(WebCoreConstants.EVENT_TYPE_PRINTER_PAPERSTATUS));
        eventDefinition.setDescriptionNoSpace("PaperLowCleared");
        
        return eventDefinition;
    }
    
    private List<EventDefinition> createAlarmOnOffEvent() {
        final List<EventDefinition> eventDefinitionList = new ArrayList<EventDefinition>();
        final EventDeviceType eventDeviceType = new EventDeviceType(14, "Shock Alarm", "ShockAlarm");
        final EventStatusType eventStatusType = new EventStatusType(16, "On");
        EventActionType eventActionType = new EventActionType(1, "Alerted");
        EventSeverityType eventSeverityType = new EventSeverityType(3, "Critical");
        
        final EventDefinition eventDefinitionOn = new EventDefinition();
        eventDefinitionOn.setEventStatusType(eventStatusType);
        eventDefinitionOn.setEventActionType(eventActionType);
        eventDefinitionOn.setEventSeverityType(eventSeverityType);
        eventDefinitionOn.setEventDeviceType(eventDeviceType);
        eventDefinitionOn.setEventType(this.eventTypeService.findEventTypeById(WebCoreConstants.EVENT_TYPE_SHOCK_ALARM));
        eventDefinitionOn.setDescriptionNoSpace("AlarmOn");
        
        eventDefinitionList.add(eventDefinitionOn);
        
        eventActionType = new EventActionType(2, "Cleared");
        eventSeverityType = new EventSeverityType(0, "Clear");
        
        final EventDefinition eventDefinitionOff = new EventDefinition();
        eventDefinitionOff.setEventStatusType(eventStatusType);
        eventDefinitionOff.setEventActionType(eventActionType);
        eventDefinitionOff.setEventSeverityType(eventSeverityType);
        eventDefinitionOff.setEventDeviceType(eventDeviceType);
        eventDefinitionOff.setEventType(this.eventTypeService.findEventTypeById(WebCoreConstants.EVENT_TYPE_SHOCK_ALARM));
        eventDefinitionOff.setDescriptionNoSpace("AlarmOff");
        
        eventDefinitionList.add(eventDefinitionOff);
        
        return eventDefinitionList;
    }
    
    private List<EventDefinition> createPaystationDoorEvents() {
        
        final EventStatusType openStatusType = new EventStatusType(4, "Open");
        
        final EventActionType alertedAction = new EventActionType(1, "Alerted");
        final EventActionType clearedAction = new EventActionType(2, "Cleared");
        
        final EventSeverityType clearSeverity = new EventSeverityType(0, "Clear");
        final EventSeverityType minorSeverity = new EventSeverityType(0, "Minor");
        
        final EventDeviceType psDevice = new EventDeviceType(12, "Paystation", "Paystation");
        
        final EventDefinition openDef = new EventDefinition();
        openDef.setEventStatusType(openStatusType);
        openDef.setEventActionType(alertedAction);
        openDef.setEventSeverityType(minorSeverity);
        openDef.setEventDeviceType(psDevice);
        openDef.setDescriptionNoSpace("DoorOpened");
        openDef.setEventType(this.eventTypeService.findEventTypeById(WebCoreConstants.EVENT_TYPE_DOOR_OPEN));
        
        final EventDefinition closeDef = new EventDefinition();
        closeDef.setEventStatusType(openStatusType);
        closeDef.setEventActionType(clearedAction);
        closeDef.setEventSeverityType(clearSeverity);
        closeDef.setEventDeviceType(psDevice);
        closeDef.setDescriptionNoSpace("DoorClosed");
        closeDef.setEventType(this.eventTypeService.findEventTypeById(WebCoreConstants.EVENT_TYPE_DOOR_OPEN));
        
        final List<EventDefinition> list = new ArrayList<EventDefinition>(2);
        list.add(openDef);
        list.add(closeDef);
        return list;
    }
    
    /**
     * Performs lazy-loading EventDeviceType table and ehcache is configured.
     * 
     * @return Map<String, EventDeviceType> key is <type> value in the event xml.
     */
    private Map<String, EventDeviceType> loadEventDeviceTypes() {
        if (this.eventDeviceTypesMap == null) {
            final List<EventDeviceType> eventDevs = this.entityDao.loadAll(EventDeviceType.class);
            this.eventDeviceTypesMap = new HashMap<String, EventDeviceType>(eventDevs.size());
            final Iterator<EventDeviceType> iter = eventDevs.iterator();
            while (iter.hasNext()) {
                final EventDeviceType edt = iter.next();
                this.eventDeviceTypesMap.put(edt.getNameNoSpace(), edt);
                this.entityDao.evict(edt);
            }
        }
        return this.eventDeviceTypesMap;
    }
    
    /**
     * @param action
     *            String event action in xml (also in EventDefinition, descriptionNoSpace).
     * @return EventActionType If action is in 'positiveActions' list then returns EventActionType with id = 2 for Cleared.
     *         Otherwise returns EventActionType with id = 1 for Alerted.
     */
    private EventActionType getEventActionType(final String action) {
        if (this.positiveActions.contains(action.toLowerCase())) {
            return new EventActionType(2);
        }
        return new EventActionType(1);
    }
    
    /**
     * EventDefinition, descriptionNoSpace = EventData, action
     * 
     * @param action
     *            'action' property of EventData.
     * @return EventSeverityType EventSeverityType object with corresponding id and name.
     */
    private EventSeverityType getEventSeverityType(final String action) {
        if (isCorrectLevel("event.alert.level.critical", action)) {
            // Critical    
            return new EventSeverityType(3);
        } else if (isCorrectLevel("event.alert.level.major", action)) {
            // Major
            return new EventSeverityType(2);
        } else if (isCorrectLevel("event.alert.level.minor", action)) {
            // Minor
            return new EventSeverityType(1);
        } else {
            // Clear
            return new EventSeverityType(0);
        }
    }
    
    private boolean isCorrectLevel(final String propName, final String action) {
        final List<String> list = Arrays.asList(this.eventsAlertsProp.getProperty(propName).split(StandardConstants.STRING_COMMA));
        if (list.contains(action)) {
            return true;
        }
        return false;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setPosServiceStateService(final PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setQueueEventService(final QueueEventService queueEventService) {
        this.queueEventService = queueEventService;
    }
    
    public final EventDefinition getEventDefinitionByDeviceTypeIdAndDescriptionNoSpace(final Integer deviceTypeId, final String descriptionNoSpace) {
        @SuppressWarnings("unchecked")
        final List<EventDefinition> result = (List<EventDefinition>)
                this.entityDao.findByNamedQueryAndNamedParam("EventDefinition.findEventDefinitionByDeviceTypeIdAndDescriptionNoSpace",
                                                             new String[] { "eventDeviceTypeId", "descriptionNoSpace" },
                                                             new Object[] {deviceTypeId, descriptionNoSpace});
        if (result.size() < 1) {
            return null;
        } else {
            return result.get(0);
        }
            
        
    }
}
