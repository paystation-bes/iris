package com.digitalpaytech.service.paystation;


import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosTelemetry;
import com.digitalpaytech.domain.TelemetryType;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.OutOfOrderException;
import com.digitalpaytech.util.KeyValuePair;

public interface PosTelemetryService {
    
    List<KeyValuePair<String, String>> findActiveByPosId(Integer pointOfSaleId, boolean includePrivate);
    
    void processPosTelemetry(PointOfSale pointOfSale, String timeStamp, Map<String, String> telemetryData, String type, Integer referenceCounter) throws InvalidDataException, OutOfOrderException;

    TelemetryType findTelemetryTypeByName(String telemetryTypeName);

    TelemetryType findTelemetryTypeByLabel(String telemetryTypeLabel);
    
    List<PosTelemetry> findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType(Integer pointOfSaleId, Integer telemetryTypeId);

    List<PosTelemetry> findAllCurrentTelemetryForPointOfSaleByPointOfSaleId(Integer pointOfSaleId);
    
}
