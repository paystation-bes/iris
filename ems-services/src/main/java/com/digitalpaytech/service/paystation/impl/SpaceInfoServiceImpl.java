package com.digitalpaytech.service.paystation.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.paystation.SpaceInfoService;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("spaceInfoService")
@Transactional(propagation = Propagation.REQUIRED)
public class SpaceInfoServiceImpl implements SpaceInfoService {
    private static final Logger LOG = Logger.getLogger(SpaceInfoServiceImpl.class);
    
    @Autowired
    private ActivePermitService activePermitService;
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public void setActivePermitService(ActivePermitService activePermitService) {
        this.activePermitService = activePermitService;
    }
    
    public void setPermitService(PermitService permitService) {
        this.permitService = permitService;
    }
    
    public void setCustomerAdminService(CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    @Override
    public SpaceInfo getSpaceInfoByCustomerAndPlateNumber(int customerId, Date currentPsDate, String licencePlateNumber, int querySpaceBy) {
        
        if (licencePlateNumber != null && !licencePlateNumber.isEmpty()) {
            ActivePermit activePermit = this.activePermitService.getActivePermitByCustomerAndLicencePlate(customerId, licencePlateNumber, querySpaceBy);
            if (activePermit != null) {
                CustomerProperty customerProperty = (CustomerProperty) this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                return convertActivePermitToSpaceInfo(activePermit, currentPsDate, customerProperty.getPropertyValue());
            }
        }
        // No stall info in database.
        return null;
    }
    
    @Override
    public SpaceInfo getSpaceInfoByLocationAndPlateNumber(int customerId, int locationId, Date currentPsDate, String licencePlateNumber) {
        
        if (licencePlateNumber != null && !licencePlateNumber.isEmpty()) {
            ActivePermit activePermit = this.activePermitService.getActivePermitByLocationAndLicencePlate(locationId, licencePlateNumber);
            if (activePermit != null) {
                CustomerProperty customerProperty = (CustomerProperty) this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                return convertActivePermitToSpaceInfo(activePermit, currentPsDate, customerProperty.getPropertyValue());
            }
        }
        // No stall info in database.
        return null;
    }
    
    // Copied from StallStatusAppService
    /**
     * If multiple transactions match the license plate provided but with
     * different stall or addtime numbers, the most recent transactions of the
     * same <plate, stall, addtime> combination will be returned.
     */
    private SpaceInfo convertActivePermitToSpaceInfo(ActivePermit activePermit, Date currentPosDate, String timeZone) {
        SpaceInfo stall = null;
        if (activePermit != null) {
            Date expiryDate = activePermit.getPermitExpireGmt();
            if (expiryDate.after(currentPosDate)) {
                stall = new SpaceInfo();
                stall.setStallNumber(activePermit.getSpaceNumber());
                stall.setAddTimeNumber(activePermit.getAddTimeNumber());
                stall.setPaystationSettingName(activePermit.getPaystationSettingName());
                stall.setStartDate(activePermit.getPermitBeginGmt());
                stall.setEndDate(expiryDate);
                stall.setTimeZone(timeZone);
            }
        }
        return stall;
    }
    
    @Override
    @Deprecated
    public SpaceInfo getValidByPaystationSettingPurchaseExpiryAddTime(String paystationSettingName, int addTimeNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByPaystationSettingAndAddTimeNumber(customerId, paystationSettingName,
                                                                                                                addTimeNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    @Override
    public SpaceInfo getValidByCustomerPurchaseExpiryAddTime(int addTimeNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByCustomerAndAddTimeNumber(customerId, addTimeNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    //TODO: refactor getValidByCustomerPurchaseExpiryAddTimeOrderByLatestExpiry to remove duplicate code
    @Override
    public SpaceInfo getValidByCustomerPurchaseExpiryAddTimeOrderByLatestExpiry(int addTimeNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByCustomerAndAddTimeNumberOrderByLatestExpiry(customerId, addTimeNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    @Override
    public SpaceInfo getValidByLocationPurchaseExpiryAddTime(int locationId, int addTimeNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByLocationAndAddTimeNumber(customerId, locationId, addTimeNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    //TODO: refactor getValidByLocationPurchaseExpiryAddTimeOrderByLatestExpiry to remove duplicate code
    @Override
    public SpaceInfo getValidByLocationPurchaseExpiryAddTimeOrderByLatestExpiry(int locationId, int addTimeNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService
                .getActivePermitByLocationAndAddTimeNumberOrderByLatestExpiry(customerId, locationId, addTimeNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    @Override
    @Deprecated
    public SpaceInfo getValidByPaystationSettingPurchaseExpirySpaceNumber(String paystationSettingName, int spaceNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByPaystationSettingAndSpaceNumber(customerId, paystationSettingName, spaceNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    @Override
    public SpaceInfo getValidByCustomerPurchaseExpirySpaceNumber(int spaceNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByCustomerAndSpaceNumber(customerId, spaceNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    //TODO: refactor getValidByCustomerPurchaseExpirySpaceNumberOrderByLatestExpiry to remove duplicate code
    @Override
    public SpaceInfo getValidByCustomerPurchaseExpirySpaceNumberOrderByLatestExpiry(int spaceNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByCustomerAndSpaceNumberOrderByLatestExpiry(customerId, spaceNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    @Override
    public SpaceInfo getValidByLocationPurchaseExpirySpaceNumber(int locationId, int spaceNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByLocationAndSpaceNumber(customerId, locationId, spaceNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    //TODO: refactor getValidByLocationPurchaseExpirySpaceNumberOrderByLatestExpiry to remove duplicate code
    @Override
    public SpaceInfo getValidByLocationPurchaseExpirySpaceNumberOrderByLatestExpiry(int locationId, int spaceNumber, Date currentPosDate, int customerId) {
        ActivePermit activePermit = this.activePermitService.getActivePermitByLocationAndSpaceNumberOrderByLatestExpiry(customerId, locationId, spaceNumber);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        SpaceInfo stall = convertActivePermitToSpaceInfo(activePermit, currentPosDate, customerProperty.getPropertyValue());
        return (stall);
    }
    
    // Copied from StallStatusAppServiceImpl
    
    @Override
    public List<SpaceInfo> getSpaceInfoListByCustomerAndSpaceRangeForPosDate(int customerId, int startSpace, int endSpace, int stallType, Date posDate,
                                                                             boolean isActive) {
        List<ActivePermit> stallList = null;
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        stallList = this.activePermitService.getActivePermitByCustomerAndSpaceRange(customerId, startSpace, endSpace, querySpaceBy);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByCustomerAndSpaceRange(customerId, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByCustomerAndSpaceRange(customerId, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = extractActivePermitIntoSpaceInfo(customerId, stallList, startSpace, endSpace, posDate, stallType,
                                                                  customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    public List<SpaceInfo> getSpaceInfoListByCustomerAndSpaceRangeForPosDatePDA(int customerId, int startSpace, int endSpace, int stallType, Date posDate,
                                                                                boolean isActive) {
        List<ActivePermit> stallList = null;
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        stallList = this.activePermitService.getActivePermitByCustomerAndSpaceRange(customerId, startSpace, endSpace, querySpaceBy);
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = convertActivePermitIntoSpaceInfo(customerId, stallList, posDate, stallType, customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    public List<SpaceInfo> getSpaceInfoListByLocationAndSpaceRangeForPosDate(int customerId, int locationId, int startSpace, int endSpace, int stallType,
                                                                             Date posDate, boolean isActive) {
        List<ActivePermit> stallList = null;
        
        stallList = this.activePermitService.getActivePermitByLocationAndSpaceRange(customerId, locationId, startSpace, endSpace);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByLocationAndSpaceRange(customerId, locationId, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByLocationAndSpaceRange(locationId, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = extractActivePermitIntoSpaceInfo(customerId, stallList, startSpace, endSpace, posDate, stallType,
                                                                  customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    public List<SpaceInfo> getSpaceInfoListByLocationAndSpaceRangeForPosDatePDA(int customerId, int locationId, int startSpace, int endSpace, int stallType,
                                                                                Date posDate, boolean isActive) {
        List<ActivePermit> stallList = null;
        
        stallList = this.activePermitService.getActivePermitByLocationAndSpaceRange(customerId, locationId, startSpace, endSpace);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByLocationAndSpaceRange(customerId, locationId, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByLocationAndSpaceRange(locationId, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = convertActivePermitIntoSpaceInfo(customerId, stallList, posDate, stallType, customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    @Deprecated
    public List<SpaceInfo> getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDate(int customerId, String paystationSettingName, int startSpace,
                                                                                      int endSpace, int stallType, Date posDate, boolean isActive) {
        List<ActivePermit> stallList = null;
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        stallList = this.activePermitService.getActivePermitByPaystationSettingAndSpaceRange(customerId, paystationSettingName, startSpace, endSpace,
                                                                                             querySpaceBy);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByPaystationSettingAndSpaceRange(customerId, paystationSettingName, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByPaystationSettingAndSpaceRange(paystationSettingName, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = extractActivePermitIntoSpaceInfo(customerId, stallList, startSpace, endSpace, posDate, stallType,
                                                                  customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    @Deprecated
    public List<SpaceInfo> getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDatePDA(int customerId, String paystationSettingName, int startSpace,
                                                                                         int endSpace, int stallType, Date posDate, boolean isActive) {
        List<ActivePermit> stallList = null;
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        stallList = this.activePermitService.getActivePermitByPaystationSettingAndSpaceRange(customerId, paystationSettingName, startSpace, endSpace,
                                                                                             querySpaceBy);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByPaystationSettingAndSpaceRange(customerId, paystationSettingName, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByPaystationSettingAndSpaceRange(paystationSettingName, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = convertActivePermitIntoSpaceInfo(customerId, stallList, posDate, stallType, customerProperty.getPropertyValue());
        return stalls;
    }
    
    @Override
    public List<SpaceInfo> getSpaceInfoListByPosListAndSpaceRangeForPosDate(int customerId, List<PointOfSale> posList, int startSpace, int endSpace,
                                                                            int stallType, Date posDate, boolean isActive) {
        List<ActivePermit> stallList = null;
        
        CustomerProperty custProp = customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        stallList = this.activePermitService.getActivePermitByPosListAndSpaceRange(customerId, posList, startSpace, endSpace, querySpaceBy);
        
        //        if (isActive || posDate.equals(WebCoreConstants.EMPTY_DATE)) {
        //            stallList = this.activePermitService.getActivePermitByPosListAndSpaceRange(customerId, posList, startSpace, endSpace);
        //        } else {
        //            stallList = this.permitService.getActivePermitByPosListAndSpaceRange(posList, startSpace, endSpace, posDate);
        //        }
        
        CustomerProperty customerProperty = (CustomerProperty) customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        List<SpaceInfo> stalls = extractActivePermitIntoSpaceInfo(customerId, stallList, startSpace, endSpace, posDate, stallType,
                                                                  customerProperty.getPropertyValue());
        return stalls;
    }
    
    private List<SpaceInfo> extractActivePermitIntoSpaceInfo(int customerId, List<ActivePermit> results, int startSpace, int endSpace, Date date,
                                                             int stallType, String timeZone) {
        List<SpaceInfo> stalls = new ArrayList<SpaceInfo>();
        if (results.size() == 0) {
            return stalls;
        }
        Iterator<ActivePermit> iterator = results.iterator();
        ActivePermit data = null;
        
        // get first item
        if (iterator.hasNext()) {
            data = iterator.next();
        }
        
        for (int i = startSpace; i <= endSpace; i++) {
            SpaceInfo stall = null;
            
            // If 'rs' row is valid
            if (data != null) {
                int curr_rs_stall = (Integer) data.getSpaceNumber();
                
                // Current valid stall in record set is higher than stall 'i' ->
                // stall 'i' has no record
                // -> stall 'i' is expired
                if (curr_rs_stall > i) {
                    if (stallType != PaystationConstants.STALL_TYPE_VALID) {
                        stall = new SpaceInfo();
                        stall.setStallNumber(Integer.toString(i));
                        if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                            stall.setEndDate(PaystationConstants.INVALID_EXPIRED_STALL);
                        }
                        stalls.add(stall);
                    }
                }
                
                // Current valid stall in record set is equal to stall 'i' ->
                // stall 'i' has a record
                // and may be valid or expired
                else if (curr_rs_stall == i) {
                    Date curr_expiry_date = data.getPermitExpireGmt();
                    // change current time to customer local time
                    if (date.equals(WebCoreConstants.EMPTY_DATE)) {
                        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
                        cal.setTimeInMillis(System.currentTimeMillis());
                        date = cal.getTime();
                    }
                    if (stallType == PaystationConstants.STALL_TYPE_ALL) {
                        stall = new SpaceInfo();
                        stall.setStallNumber(Integer.toString(i));
                        stall.setPaystationSettingName(data.getPaystationSettingName());
                        stall.setStartDate(data.getPermitBeginGmt());
                        stall.setEndDate(curr_expiry_date);
                        stall.setTimeZone(timeZone);
                        stalls.add(stall);
                    } else if (stallType == PaystationConstants.STALL_TYPE_VALID && curr_expiry_date.after(date)) {
                        stall = new SpaceInfo();
                        stall.setStallNumber(Integer.toString(i));
                        stall.setPaystationSettingName(data.getPaystationSettingName());
                        stall.setStartDate(data.getPermitBeginGmt());
                        stall.setEndDate(curr_expiry_date);
                        stall.setTimeZone(timeZone);
                        stalls.add(stall);
                    } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED && curr_expiry_date.before(date)) {
                        stall = new SpaceInfo();
                        stall.setStallNumber(Integer.toString(i));
                        stall.setPaystationSettingName(data.getPaystationSettingName());
                        stall.setStartDate(data.getPermitBeginGmt());
                        stall.setEndDate(curr_expiry_date);
                        stall.setTimeZone(timeZone);
                        stalls.add(stall);
                    }
                    data = (iterator.hasNext() ? iterator.next() : null);
                }
                
                // Current valid stall in record set is lower than stall 'i' ->
                // cannot happen
                else {
                    LOG.warn("Code is incorrect - stall in record set is lower than loop stall # " + curr_rs_stall 
                             + ", start/end stall # " + startSpace + "/" + endSpace
                             + ", current stall # " + i);
                    data = (iterator.hasNext() ? iterator.next() : null);
                    i--;
                }
            }
            
            // If current 'rs' row is not valid(ie. no more rows) -> stall 'i'
            // has no record
            // stall 'i' is expired
            else {
                if (stallType != PaystationConstants.STALL_TYPE_VALID) {
                    stall = new SpaceInfo();
                    stall.setStallNumber(Integer.toString(i));
                    if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                        stall.setEndDate(PaystationConstants.INVALID_EXPIRED_STALL);
                    }
                    stalls.add(stall);
                }
            }
        }
        return stalls;
    }
    
    private List<SpaceInfo> convertActivePermitIntoSpaceInfo(int customerId, List<ActivePermit> results, Date date, int stallType, String timeZone) {
        
        List<SpaceInfo> stalls = new ArrayList<SpaceInfo>();
        if (results.size() == 0) {
            return stalls;
        }
        
        for (ActivePermit ap : results) {
            SpaceInfo stall = null;
            
            if (date.equals(WebCoreConstants.EMPTY_DATE)) {
                Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
                cal.setTimeInMillis(System.currentTimeMillis());
                date = cal.getTime();
            }
            
            if (stallType == PaystationConstants.STALL_TYPE_ALL) {
                stall = new SpaceInfo();
                stall.setStallNumber(ap.getSpaceNumber());
                stall.setPaystationSettingName(ap.getPaystationSettingName());
                stall.setStartDate(ap.getPermitBeginGmt());
                stall.setEndDate(ap.getPermitExpireGmt());
                stall.setTimeZone(timeZone);
                stalls.add(stall);
            } else if (stallType == PaystationConstants.STALL_TYPE_VALID && ap.getPermitExpireGmt().after(date)) {
                stall = new SpaceInfo();
                stall.setStallNumber(ap.getSpaceNumber());
                stall.setPaystationSettingName(ap.getPaystationSettingName());
                stall.setStartDate(ap.getPermitBeginGmt());
                stall.setEndDate(ap.getPermitExpireGmt());
                stall.setTimeZone(timeZone);
                stalls.add(stall);
            } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED && ap.getPermitExpireGmt().before(date)) {
                stall = new SpaceInfo();
                stall.setStallNumber(ap.getSpaceNumber());
                stall.setPaystationSettingName(ap.getPaystationSettingName());
                stall.setStartDate(ap.getPermitBeginGmt());
                stall.setEndDate(ap.getPermitExpireGmt());
                stall.setTimeZone(timeZone);
                stalls.add(stall);
            }
            
        }
        
        return stalls;
    }
    
}
