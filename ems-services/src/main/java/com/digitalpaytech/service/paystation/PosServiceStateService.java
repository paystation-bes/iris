package com.digitalpaytech.service.paystation;

import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.paystation.PosServiceStateInitInfo;

public interface PosServiceStateService {
    
    PosServiceState findPosServiceStateById(Integer id, boolean isEvicted);
    
    void updatePosServiceState(PosServiceState posServiceState);
    
    void updatePosServiceStates(Collection<PosServiceState> posServiceStates);
    
    Collection<PosServiceState> findAllPosServiceStates(boolean isEvicted);
    
    void processInitialization(Integer id, PosServiceStateInitInfo posServiceStateInitInfo);
    
    List<PosServiceState> findPosServiceStateByNextSettingsFileId(Integer nextSettingsFileId);

    void checkforEMVMerchantAccount(MerchantAccount merchantAccount, List<Integer> posId);
    
    Collection<PosServiceState> findAllLinuxPosServiceStates();
    Collection<PosServiceState> findLinuxPosServiceStatesByLinkCustomerId(Integer linkCustomerId);
    PosServiceState findLinuxPosServiceStateBySerialNumber(String serialNumber);
}
