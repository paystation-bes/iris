package com.digitalpaytech.service.paystation.impl;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.ByteType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dto.paystation.TransactionInfoDto;
import com.digitalpaytech.service.paystation.TransactionInfoDtoService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("transactionInfoDtoService")
@Transactional(propagation = Propagation.SUPPORTS)
public class TransactionInfoDtoServiceImpl implements TransactionInfoDtoService {
    
    private static final Logger logger = Logger.getLogger(TransactionInfoDtoServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    private final static String SQL_SELECT_COUNT = "SELECT COUNT(*)";
    
    private final static String SQL_SELECT = "SELECT pos.SerialNumber AS SerialNumber, pos.Name AS PaystationName, pst.Name AS PaystationType, ls.Name AS SettingName, l.Id AS LocationId, l.Name AS LocationName, pur.PurchaseNumber AS TicketNumber,"
                                             + " per.SpaceNumber , pur.PurchaseGMT, per.PermitExpireGMT, pur.PaymentTypeId AS PaymentType, pur.TransactionTypeId AS TransactionType, proctrans.CardType AS CardType,"
                                             + " proctrans.Last4DigitsOfCardNumber, proctrans.AuthorizationNumber, proctrans.ProcessingDate, proctrans.ProcessorTransactionTypeId AS ProcessorTransactionType,"
                                             + " proctrans.ReferenceNumber, IF(ma.TerminalName IS NULL OR ma.TerminalName = '', ma.Name, ma.TerminalName) AS MerchantAccountName, "
                                             + " crt.NumRetries, proctrans.ProcessorTransactionId, c.Coupon AS CouponNumber, c.PercentDiscount, c.DollarDiscountAmount,"
                                             + " pur.ExcessPaymentAmount, pur.ChargedAmount, pur.CashPaidAmount, pur.CardPaidAmount, pur.ChangeDispensedAmount, pur.IsRefundSlip,"
                                             + " cc.CardNumber AS CustomCardData, cct.Name AS CustomCardType, ur.Id AS RateId, ur.Name as RateName, pur.RateAmount , lp.Number AS LicencePlateNumber, route.Name As GroupName";
    
    private final static String SQL_FROM_PURCHASE = " FROM Purchase pur"
                                                    + " LEFT JOIN Permit per ON pur.Id = per.PurchaseId"
                                                    + " INNER JOIN PointOfSale pos ON pur.PointOfSaleId = pos.Id"
                                                    + " INNER JOIN Paystation ps ON pos.PaystationId = ps.Id"
                                                    + " INNER JOIN PaystationType pst ON ps.PaystationTypeId = pst.Id"
                                                    + " LEFT JOIN ProcessorTransaction proctrans ON pur.Id = proctrans.PurchaseId AND proctrans.IsApproved=1 AND proctrans.ProcessorTransactionTypeId IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,21,23)"
                                                    + " LEFT JOIN UnifiedRate ur ON pur.UnifiedRateId = ur.Id"
                                                    + " LEFT JOIN Location l On pur.LocationId = l.Id"
                                                    + " LEFT JOIN PaystationSetting ls On pur.PaystationSettingId = ls.Id"
                                                    + " LEFT JOIN LicencePlate lp On per.LicencePlateId = lp.Id"
                                                    + " LEFT JOIN MerchantAccount ma ON proctrans.MerchantAccountId = ma.Id"
                                                    + " LEFT JOIN Coupon c ON pur.CouponId = c.Id"
                                                    + " LEFT JOIN CardRetryTransaction crt ON pur.PointOfSaleId = crt.PointOfSaleId AND pur.PurchaseGMT = crt.PurchasedDate AND pur.PurchaseNumber = crt.TicketNumber"
                                                    + " LEFT JOIN PaymentCard pcard ON pur.Id=pcard.PurchaseId"
                                                    + " LEFT JOIN CustomerCard cc ON pcard.CustomerCardId=cc.Id"
                                                    + " LEFT JOIN CustomerCardType cct ON cc.CustomerCardTypeId=cct.Id"                                                    
                                                    + " LEFT JOIN RoutePOS rpos ON pos.id=rpos.PointOfSaleId"
                                                    + " LEFT JOIN Route route ON rpos.RouteId=route.id";
    
    private final static String SQL_FROM_PROCESSORTRANSACTION = " FROM ProcessorTransaction proctrans"
                                                                + ReportingConstants.SQL_USE_IDX_PROCESSOR_TRANSACTION_PROCESSING_DATE
                                                                + " INNER JOIN Purchase pur ON proctrans.PurchaseId = pur.Id AND proctrans.IsApproved=1 AND proctrans.ProcessorTransactionTypeId IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,21,23)"
                                                                + " LEFT JOIN Permit per ON pur.Id = per.PurchaseId"
                                                                + " INNER JOIN PointOfSale pos ON pur.PointOfSaleId = pos.Id"
                                                                + " INNER JOIN Paystation ps ON pos.PaystationId = ps.Id"
                                                                + " INNER JOIN PaystationType pst ON ps.PaystationTypeId = pst.Id"
                                                                + " LEFT JOIN UnifiedRate ur ON pur.UnifiedRateId = ur.Id"
                                                                + " LEFT JOIN Location l On pur.LocationId = l.Id"
                                                                + " LEFT JOIN PaystationSetting ls On pur.PaystationSettingId = ls.Id"
                                                                + " LEFT JOIN LicencePlate lp On per.LicencePlateId = lp.Id"
                                                                + " LEFT JOIN MerchantAccount ma ON proctrans.MerchantAccountId = ma.Id"
                                                                + " LEFT JOIN Coupon c ON pur.CouponId = c.Id"
                                                                + " LEFT JOIN CardRetryTransaction crt ON pur.PointOfSaleId = crt.PointOfSaleId AND pur.PurchaseGMT = crt.PurchasedDate AND pur.PurchaseNumber = crt.TicketNumber"
                                                                + " LEFT JOIN PaymentCard pcard ON pur.Id=pcard.PurchaseId"
                                                                + " LEFT JOIN CustomerCard cc ON pcard.CustomerCardId=cc.Id"
                                                                + " LEFT JOIN CustomerCardType cct ON cc.CustomerCardTypeId=cct.Id"                                                                
                                                                + " LEFT JOIN RoutePOS rpos ON pos.id=rpos.PointOfSaleId"
                                                                + " LEFT JOIN Route route ON rpos.RouteId=route.id";
    
    private final static String SQL_FROM_PURCHASE_COUNT = " FROM Purchase pur"
                                                          + " LEFT JOIN Permit per ON pur.Id = per.PurchaseId"
                                                          + " LEFT JOIN ProcessorTransaction proctrans ON pur.Id = proctrans.PurchaseId AND proctrans.IsApproved=1 AND proctrans.ProcessorTransactionTypeId IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,21,23)";
    
    private final static String SQL_FROM_PROCESSORTRANSACTION_COUNT = " FROM ProcessorTransaction proctrans"
                                                                      + ReportingConstants.SQL_USE_IDX_PROCESSOR_TRANSACTION_PROCESSING_DATE
                                                                      + " INNER JOIN Purchase pur ON proctrans.PurchaseId = pur.Id AND proctrans.IsApproved=1 AND proctrans.ProcessorTransactionTypeId IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,21,23)"
                                                                      + " LEFT JOIN Permit per ON pur.Id = per.PurchaseId";
    
    private final static String SQL_WHERE_CORE = " WHERE pur.CustomerId = :customerId";
    private final static String SQL_WHERE_ADD_PURCHASEDATERANGE = " AND pur.PurchaseGMT BETWEEN :fromDate AND :toDate";
    private final static String SQL_WHERE_ADD_UPDATEDATERANGE = " AND pur.CreatedGMT BETWEEN :fromDate AND :toDate";
    private final static String SQL_WHERE_ADD_PROCESSINGDATERANGE = " AND proctrans.ProcessingDate BETWEEN :fromDate AND :toDate AND proctrans.IsApproved=1";
    private final static String SQL_WHERE_ADD_SPACERANGE = " AND per.SpaceNumber BETWEEN :fromSpace AND :toSpace";
    private final static String SQL_WHERE_ADD_LOCATIONID = " AND pur.LocationId = :locationId";
    private final static String SQL_WHERE_ADD_POINTOFSALEID = " AND pur.PointOfSaleId = :pointOfSaleId";
    private final static String SQL_WHERE_ADD_PAYSTATIONSETTINGID = " AND pur.PaystationSettingId = :paystationSettingId";
    private final static String SQL_WHERE_ADD_POSLIST = " AND pur.PointOfSaleId IN (:posList)";
    private final static String SQL_WHERE_ADD_ROUTENAME = " AND route.Name = :routeName";
    private final static String SQL_GROUP_BY_PURCHASEID = " GROUP BY pur.Id";

    @Override
    public List<TransactionInfoDto> getTransactionList(int reqType, Object[] values) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT);
        // Use index if it's 'by settlement date'
        builderStr.append(reqType == WebCoreConstants.GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE ? SQL_FROM_PROCESSORTRANSACTION : SQL_FROM_PURCHASE);
        getTransactionInfoWhereSQLByType(builderStr, reqType, false);
        builderStr.append(reqType == WebCoreConstants.GETTRANSACTIONINFOBYGROUP_TYPE ? WebCoreConstants.EMPTY_STRING : SQL_GROUP_BY_PURCHASEID);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addTransactionInfoSQLScalars(q);
        
        String[] parameters = getTransactionInfoWhereSQLNameByType(reqType, false);
        for (int i = 0; i < parameters.length; i++) {
            if (values[i] instanceof Collection)
                q.setParameterList(parameters[i], (Collection) values[i]);
            else
                q.setParameter(parameters[i], values[i]);
        }
        return q.list();
    }
    
    @Override
    public int getTransactionListCount(int reqType, Object[] values) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_COUNT);
        builderStr.append(reqType == WebCoreConstants.GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE ? SQL_FROM_PROCESSORTRANSACTION_COUNT : SQL_FROM_PURCHASE_COUNT);
        getTransactionInfoWhereSQLByType(builderStr, reqType, true);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        
        String[] parameters = getTransactionInfoWhereSQLNameByType(reqType, true);
        for (int i = 0; i < parameters.length; i++) {
            if (values[i] instanceof Collection)
                q.setParameterList(parameters[i], (Collection) values[i]);
            else
                q.setParameter(parameters[i], values[i]);
        }
        Object response = q.uniqueResult();
        return ((BigInteger) response).intValue();
    }
    
    private void getTransactionInfoWhereSQLByType(StringBuilder builderStr, int reqType, boolean isCount) {
        switch (reqType) {
            case WebCoreConstants.GETTRANSACTIONINFOBYSTALL_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                builderStr.append(SQL_WHERE_ADD_SPACERANGE);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYPURCHASEDDATE_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PROCESSINGDATERANGE);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYREGION_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                builderStr.append(SQL_WHERE_ADD_LOCATIONID);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYSERIALNUMBER_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                builderStr.append(SQL_WHERE_ADD_POINTOFSALEID);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYSETTING_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                builderStr.append(SQL_WHERE_ADD_PAYSTATIONSETTINGID);
                break;
            case WebCoreConstants.GETTRANSACTIONINFOBYGROUP_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_PURCHASEDATERANGE);
                builderStr.append(SQL_WHERE_ADD_POSLIST);
                if (!isCount) {
                    builderStr.append(SQL_WHERE_ADD_ROUTENAME);
                }
                break;
            case WebCoreConstants.GETTRANSACTIONDATABYUPDATEDATE_TYPE:
                builderStr.append(SQL_WHERE_CORE);
                builderStr.append(SQL_WHERE_ADD_UPDATEDATERANGE);
                break;
        }
    }
    
    private void addTransactionInfoSQLScalars(SQLQuery q) {
        q.addScalar("SerialNumber", new StringType());
        q.addScalar("PaystationName", new StringType());
        q.addScalar("SettingName", new StringType());
        q.addScalar("PaystationType", new StringType());
        q.addScalar("LocationId", new IntegerType());
        q.addScalar("LocationName", new StringType());
        q.addScalar("TicketNumber", new IntegerType());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PurchaseGMT", new TimestampType());
        q.addScalar("PermitExpireGMT", new TimestampType());
        q.addScalar("PaymentType", new ByteType());
        q.addScalar("TransactionType", new ByteType());
        q.addScalar("CardType", new StringType());
        q.addScalar("Last4DigitsOfCardNumber", new IntegerType());
        q.addScalar("AuthorizationNumber", new StringType());
        q.addScalar("ProcessingDate", new TimestampType());
        q.addScalar("ProcessorTransactionType", new IntegerType());
        q.addScalar("ReferenceNumber", new StringType());
        q.addScalar("MerchantAccountName", new StringType());
        q.addScalar("NumRetries", new IntegerType());
        q.addScalar("ProcessorTransactionId", new StringType());
        q.addScalar("CouponNumber", new StringType());
        q.addScalar("PercentDiscount", new ByteType());
        q.addScalar("DollarDiscountAmount", new FloatType());
        q.addScalar("ExcessPaymentAmount", new FloatType());
        q.addScalar("ChargedAmount", new FloatType());
        q.addScalar("CashPaidAmount", new FloatType());
        q.addScalar("CardPaidAmount", new FloatType());
        q.addScalar("ChangeDispensedAmount", new FloatType());
        q.addScalar("IsRefundSlip", new ByteType());
        q.addScalar("CustomCardData", new StringType());
        q.addScalar("CustomCardType", new StringType());
        q.addScalar("RateId", new IntegerType());
        q.addScalar("RateName", new StringType());
        q.addScalar("RateAmount", new FloatType());
        q.addScalar("LicencePlateNumber", new StringType());
        q.addScalar("GroupName", new StringType());
        q.setResultTransformer(Transformers.aliasToBean(TransactionInfoDto.class));
    }
    
    private String[] getTransactionInfoWhereSQLNameByType(int reqType, boolean isCount) {
        switch (reqType) {
            case WebCoreConstants.GETTRANSACTIONINFOBYSTALL_TYPE:
                return new String[] { "customerId", "fromDate", "toDate", "fromSpace", "toSpace" };
            case WebCoreConstants.GETTRANSACTIONINFOBYPURCHASEDDATE_TYPE:
                return new String[] { "customerId", "fromDate", "toDate" };
            case WebCoreConstants.GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE:
                return new String[] { "customerId", "fromDate", "toDate" };
            case WebCoreConstants.GETTRANSACTIONINFOBYREGION_TYPE:
                return new String[] { "customerId", "fromDate", "toDate", "locationId" };
            case WebCoreConstants.GETTRANSACTIONINFOBYSERIALNUMBER_TYPE:
                return new String[] { "customerId", "fromDate", "toDate", "pointOfSaleId" };
            case WebCoreConstants.GETTRANSACTIONINFOBYSETTING_TYPE:
                return new String[] { "customerId", "fromDate", "toDate", "paystationSettingId" };
            case WebCoreConstants.GETTRANSACTIONINFOBYGROUP_TYPE:
                if (isCount) {
                    return new String[] { "customerId", "fromDate", "toDate", "posList" };
                } else {
                    return new String[] { "customerId", "fromDate", "toDate", "posList", "routeName" };
                }
            case WebCoreConstants.GETTRANSACTIONDATABYUPDATEDATE_TYPE:
                return new String[] { "customerId", "fromDate", "toDate" };
        }
        return new String[] {};
    }
    
}
