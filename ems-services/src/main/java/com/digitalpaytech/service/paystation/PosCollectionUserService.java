package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosCollectionUser;

public interface PosCollectionUserService {
    
    Set<PosCollectionUser> findExistingPosCollectionByPosCollection(final PosCollection posCollection);
    
    List<PosCollectionUser> findPosCollectionUserExistingPosCollectionListByCollectionType(Integer pointOfSaleId, Integer collectionTypeId, Date startTime,
                                                                                     Date endTime);
    
    void savePosCollectionUserList(List<PosCollectionUser> posCollectionUserList);
    
}
