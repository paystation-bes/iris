package com.digitalpaytech.service.paystation.impl;


import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;


import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.LinuxConfigurationFile;
import com.digitalpaytech.service.paystation.LinuxConfigurationFileService;
import com.digitalpaytech.util.StandardConstants;

@Service("linuxConfigurationFileService")
@Transactional(propagation = Propagation.REQUIRED)
public class LinuxConfigurationFileServiceImpl implements LinuxConfigurationFileService {
    private static final String FIND_BY_POS_ID_AND_SNAPSHOT_ID = "LinuxConfigurationFile.findLinuxConfigurationFilesByPointOfSaleIdAndSnapShotId";

    private static final String DELETE_BY_POS_ID_AND_SNAPSHOT_ID_ORDINAL =
            "LinuxConfigurationFile.deleteLinuxConfigurationFilesByPointOfSaleIdAndSnapShotIdOrdinal";
    
    private static final String DELETE_BY_LINUX_CONFIGURATION_POS_ID = "LinuxConfigurationFile.deleteLinuxConfigurationFilesByPointOfSaleId";

    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final int deleteByPointOfSaleIdAndSnaphotId(final int pointOfSaleId, final String snapShotId) {
        return this.entityDao.deleteAllByNamedQuery(DELETE_BY_POS_ID_AND_SNAPSHOT_ID_ORDINAL, new Object[] { pointOfSaleId, snapShotId });
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<LinuxConfigurationFile> findByPointOfSaleIdAndSnapshotId(final int pointOfSaleId, final String snapshotId) {
        return this.entityDao.findByNamedQueryAndNamedParam(FIND_BY_POS_ID_AND_SNAPSHOT_ID, new String[] { "pointOfSaleId", "snapShotId" },
                                                            new Object[] { pointOfSaleId, snapshotId });
    }
    
    @Override
    public final LinuxConfigurationFile findByPointOfSaleIdAndFileIdAndSnapShotId(final int pointOfSaleId, final String fileId,
        final String snapshotId) {
        @SuppressWarnings("unchecked")
        final List<LinuxConfigurationFile> resultsList = this.entityDao
                .findByNamedQueryAndNamedParam("LinuxConfigurationFile.findLinuxConfigurationFilesByPointOfSaleIdAndSnapShotIdAndFileId",
                                               new String[] { "fileId", "pointOfSaleId", "snapShotId" },
                                               new Object[] { fileId, pointOfSaleId, snapshotId });
        
        if (resultsList != null && resultsList.size() > 0) {
            return resultsList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public final boolean fileExistsByFileIdAndSnapShotIdAndPointOfSaleId(final String fileId, final int pointOfSaleId, final String snapshotId) {
        final LinuxConfigurationFile result = findByPointOfSaleIdAndFileIdAndSnapShotId(pointOfSaleId, fileId, snapshotId);
        return result != null;
    }
    
    @Override
    public final void save(LinuxConfigurationFile file) {
        this.entityDao.saveOrUpdate(file);
    }
    
    @Override
    public final int deleteByPointOfSaleId(final int pointOfSaleId) {
        return this.entityDao.deleteAllByNamedQuery(DELETE_BY_LINUX_CONFIGURATION_POS_ID, new Object[] { pointOfSaleId });
    }
    
    @Override
    public final Optional<LinuxConfigurationFile> findLatestByPointOfSaleId(final int pointOfSaleId) {
        final List<LinuxConfigurationFile> resultList 
            = this.entityDao.getCurrentSession().getNamedQuery("LinuxConfigurationFile.findLatestLinuxConfigurationFilesByPointOfSaleId")
                .setParameter("pointOfSaleId", pointOfSaleId).setMaxResults(StandardConstants.CONSTANT_1).list();
        
        if (resultList == null || resultList.size() == StandardConstants.CONSTANT_0) {
            return Optional.empty();
        }
        return Optional.of(resultList.get(StandardConstants.CONSTANT_0));
    }
}
