package com.digitalpaytech.service.paystation;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.EventData;

public interface EventModemService {
    public void saveModemSetting(PointOfSale pointOfSale, EventData eventData);
}
