package com.digitalpaytech.service.paystation.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.CollectionSearchCriteria;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePosInfo;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.paystation.EventSensorService;
import com.digitalpaytech.service.paystation.PosCollectionService;
import com.digitalpaytech.service.paystation.PosCollectionUserService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ItemLocatorCriterionAggregator;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;

@Service("posCollectionService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosCollectionServiceImpl implements PosCollectionService {
    private static final String EVENT_ACTION_RESET_STRING = "Reset";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CollectionTypeService collectionTypeService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private EventSensorService eventSensorService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private PosCollectionUserService posCollectionUserService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private MailerService mailerService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public CollectionTypeService getCollectiontypeService() {
        return this.collectionTypeService;
    }
    
    public EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public MailerService getMailerService() {
        return mailerService;
    }
    
    public void setMailerService(MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public void setEventSensorService(EventSensorService eventSensorService) {
        this.eventSensorService = eventSensorService;
    }
    
    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setPosCollectionUserService(PosCollectionUserService posCollectionUserService) {
        this.posCollectionUserService = posCollectionUserService;
    }
    
    public void setRouteService(RouteService routeService) {
        this.routeService = routeService;
    }
    
    @SuppressWarnings("unchecked")
    public List<PosCollection> findPosCollectionByCustomerIdAndDateRange(final int customerId, final long dateBoundary, final int pageNumber,
                                                                         final boolean isAll, final boolean isBill, final boolean isCoin, final boolean isCard) {
        
        // First page should start from 0 following pages should start from  (startingResult-1)*25  
        final int startingRow = (pageNumber <= 1 ? 0 : (pageNumber - 1)) * WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
        
        final Date date = new Date();
        date.setTime(dateBoundary);
        
        final Calendar maxDate = Calendar.getInstance();
        maxDate.setTimeInMillis(dateBoundary);
        // TODO: retrieve '90' from some configuration file or emsproperties table
        maxDate.add(Calendar.DAY_OF_YEAR, -90);
        
        final Criteria crit = this.entityDao.createCriteria(PosCollection.class);
        crit.add(Restrictions.eq("customer.id", customerId));
        if (!(isBill == isCoin == isCard == isAll)) {
            final List<Integer> collectionTypeList = new ArrayList<Integer>();
            if (isAll) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_ALL);
            }
            if (isBill) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_BILL);
            }
            if (isCoin) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_COIN);
            }
            if (isCard) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_CARD);
            }
            crit.add(Restrictions.in("collectionType.id", collectionTypeList));
        }
        crit.add(Restrictions.le("endGmt", date));
        crit.add(Restrictions.ge("endGmt", maxDate.getTime()));
        crit.addOrder(Order.desc("endGmt"));
        crit.setFirstResult(startingRow);
        crit.setMaxResults(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
        return crit.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PosCollection> findPosCollectionByPointOfSaleListIdAndDateRange(final List<PointOfSale> posList, final long dateBoundary, final int pageNumber,
                                                                                final boolean isAll, final boolean isBill, final boolean isCoin,
                                                                                final boolean isCard) {
        
        // First page should start from 0 following pages should start from  (startingResult-1)*25  
        final int startingRow = (pageNumber <= 1 ? 0 : (pageNumber - 1)) * WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
        
        final Date date = new Date();
        date.setTime(dateBoundary);
        
        final Calendar maxDate = Calendar.getInstance();
        maxDate.setTimeInMillis(dateBoundary);
        //TODO: retrieve '90' from some configuration file or emsproperties table
        maxDate.add(Calendar.DAY_OF_YEAR, -90);
        
        final Criteria crit = this.entityDao.createCriteria(PosCollection.class);
        final List<Integer> posIdList = new ArrayList<Integer>();
        for (PointOfSale pos : posList) {
            posIdList.add(pos.getId());
        }
        crit.add(Restrictions.in("pointOfSale.id", posIdList));
        if (!(isBill && isCoin && isCard && isAll) && !(!isBill && !isCoin && !isCard && !isAll)) {
            final List<Integer> collectionTypeList = new ArrayList<Integer>();
            if (isAll) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_ALL);
            }
            if (isBill) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_BILL);
            }
            if (isCoin) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_COIN);
            }
            if (isCard) {
                collectionTypeList.add(WebCoreConstants.COLLECTION_TYPE_CARD);
            }
            crit.add(Restrictions.in("collectionType.id", collectionTypeList));
        }
        crit.add(Restrictions.le("endGmt", date));
        crit.add(Restrictions.ge("endGmt", maxDate.getTime()));
        crit.addOrder(Order.desc("endGmt"));
        crit.setFirstResult(startingRow);
        crit.setMaxResults(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
        return crit.list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<PosCollection> findPosCollectionByPointOfSaleIdLast90Days(final int pointOfSaleId, final String sortOrder, final String sortItem,
                                                                          final Integer page) {
        
        final Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.DAY_OF_YEAR, -90);
        
        final Criteria crit = this.entityDao.createCriteria(PosCollection.class);
        crit.createAlias("collectionType", "ct");
        crit.add(Restrictions.eq("pointOfSale.id", pointOfSaleId));
        crit.add(Restrictions.ge("endGmt", maxDate.getTime()));
        crit.setFetchMode("ct", FetchMode.JOIN);
        
        crit.setCacheable(true);
        
        crit.setMaxResults(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        crit.setFirstResult(((page != null ? page.intValue() : 1) - 1) * WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
        if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("ASC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.asc("endGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("type")) {
                crit.addOrder(Order.asc("ct.name"));
            } else {
                crit.addOrder(Order.asc("endGmt"));
            }
        } else if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("DESC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.desc("endGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("type")) {
                crit.addOrder(Order.desc("ct.name"));
            } else {
                crit.addOrder(Order.desc("endGmt"));
            }
        } else {
            crit.addOrder(Order.desc("endGmt"));
        }
        
        return crit.list();
    }
    
    @Override
    public PosCollection findPosCollectionByPosCollectionId(final int collectionId) {
        
        return (PosCollection) this.entityDao.get(PosCollection.class, collectionId);
    }
    
    @Override
    public Date findLastCollectionDate(final PosCollection collection) {
        final StringBuilder queryStr = new StringBuilder();
        
        queryStr.append("SELECT MAX(posc.EndGMT) ");
        queryStr.append("FROM POSCollection posc ");
        queryStr.append("WHERE posc.PointOfSaleId = " + collection.getPointOfSale().getId() + " ");
        queryStr.append("AND posc.CollectionTypeId = " + collection.getCollectionType().getId() + " ");
        queryStr.append("AND posc.EndGMT < '" + DateUtil.createDatabaseDateString(collection.getEndGmt()) + "' ");
        final SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        
        @SuppressWarnings("unchecked")
        final List<Object> results = query.list();
        if (results != null && !results.isEmpty()) {
            return (Date) results.get(0);
        }
        
        return null;
    }
    
    @Override
    public void processPosCollection(final PointOfSale pointOfSale, final PosCollection posCollection) {
        final PosServiceState serviceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSale.getId());
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSale.getId());
        posCollection.setCreatedGmt(DateUtil.getCurrentGmtDate());
        posCollection.setPosCollectionUsers(this.posCollectionUserService.findExistingPosCollectionByPosCollection(posCollection));
        this.entityDao.save(posCollection);
        // reset NumberBillsAccepted & NumberCoinsAccepted in PosServiceState.
        this.eventSensorService.processEvent(pointOfSale,
                                             sensorState,
                                             serviceState,
                                             createEventData(pointOfSale.getId(), EVENT_ACTION_RESET_STRING, WebCoreConstants.EVENT_TYPE_BILL_ACCEPTOR,
                                                             "PosCollection, reset bill acceptor", "0", posCollection.getEndGmt()));
        this.eventSensorService.processEvent(pointOfSale,
                                             sensorState,
                                             serviceState,
                                             createEventData(pointOfSale.getId(), EVENT_ACTION_RESET_STRING, WebCoreConstants.EVENT_TYPE_COIN_ACCEPTOR,
                                                             "PosCollection, reset coin acceptor", "0", posCollection.getEndGmt()));
    }
    
    private EventData createEventData(final int pointOfSaleId, final String action, final String type, final String severity, final String information,
                                      final Date timestamp) {
        final EventData event = new EventData();
        event.setPointOfSaleId(pointOfSaleId);
        event.setTimeStamp(timestamp);
        event.setAction(action);
        event.setInformation(information);
        event.setType(type);
        event.setSeverity(severity);
        return event;
    }
    
    @Override
    public boolean isDuplicateRecord(final PosCollection posCollection) {
        final PosCollection posCol = findByPosIdCollectionTypeIdStartEndGmt(posCollection);
        if (posCol != null) {
            return true;
        }
        return false;
    }
    
    @Override
    public PosCollection findByPosIdCollectionTypeIdStartEndGmt(final PosCollection posCollection) {
        final String[] params = { "pointOfSaleId", "collectionTypeId", "startGmt", "endGmt" };
        final Object[] values = new Object[] { posCollection.getPointOfSale().getId(), posCollection.getCollectionType().getId(), posCollection.getStartGmt(),
                posCollection.getEndGmt() };
        @SuppressWarnings("unchecked")
        final List<PosCollection> list = this.entityDao.findByNamedQueryAndNamedParam("PointOfSaleCollection.findByPosIdCollectionTypeIdStartEndGmt", params,
                                                                                      values, true);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    @Override
    public CollectionsCentrePosInfo findPosInfoByCriteria(final CollectionSearchCriteria criteria, final TimeZone timeZone) {
        final Criteria query = this.entityDao.createCriteria(PosCollection.class);
        query.createAlias("customer", "c");
        query.createAlias("pointOfSale", "pos");
        query.createAlias("pos.paystation", "ps");
        query.createAlias("collectionType", "ct");
        query.createAlias("posCollectionUsers", "cu", Criteria.LEFT_JOIN);
        query.createAlias("cu.userAccount", "ua", Criteria.LEFT_JOIN);
        //posCollection.getCoinTotalAmount() + posCollection.getBillTotalAmount() + posCollection.getCardTotalAmount()
        query.setProjection(Projections.projectionList().add(Projections.property("id")).add(Projections.property("collectionType"))
                .add(Projections.property("pos.id")).add(Projections.property("ps.paystationType.id")).add(Projections.property("coinTotalAmount"))
                .add(Projections.property("billTotalAmount")).add(Projections.property("cardTotalAmount")).add(Projections.property("endGmt"))
                .add(Projections.property("ua.fullName")));
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("c.id", criteria.getCustomerId()));
        }
        
        if (criteria.getPointOfSaleId() != null) {
            query.add(Restrictions.eq("pos.id", criteria.getPointOfSaleId()));
        } else {
            if (criteria.getLocationId() != null) {
                query.add(Restrictions.eq("pos.location.id", criteria.getLocationId()));
            }
            if (criteria.getRouteId() != null) {
                query.createAlias("pos.routePOSes", "posRoute").createAlias("posRoute.route", "route").add(Restrictions.eq("route.id", criteria.getRouteId()));
            }
        }
        
        if ((criteria.getCollectionTypeIds() != null) && (!criteria.getCollectionTypeIds().isEmpty())) {
            query.add(Restrictions.in("ct.id", criteria.getCollectionTypeIds()));
        }
        
        if (criteria.getMinCollectionDate() != null) {
            query.add(Restrictions.ge("endGmt", criteria.getMinCollectionDate()));
        }
        if (criteria.getMaxCollectionDate() != null) {
            query.add(Restrictions.le("endGmt", criteria.getMaxCollectionDate()));
        }
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult(criteria.getItemsPerPage() * (criteria.getPage() - 1));
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        query.addOrder(Order.desc("endGmt"));
        
        // remove multiples performance can be increased by changing the sql
        @SuppressWarnings("unchecked")
        final List<Object[]> objectList = query.list();
        for (int i = 0; i < objectList.size() - 1; i++) {
            if (objectList.get(i)[0].equals(objectList.get(i + 1)[0])) {
                if (objectList.get(i)[8] != null) {
                    objectList.remove(i + 1);
                } else {
                    objectList.remove(i);
                }
                i = -1;
            }
        }
        
        return this.prepareCollectionInfo(objectList, timeZone);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CollectionsCentreCollectionInfo> findPosCollectionByCriteria(final CollectionSearchCriteria criteria, final TimeZone timeZone) {
        final Criteria query = createPosCollectionQuery(criteria);
        //posCollection.getCoinTotalAmount() + posCollection.getBillTotalAmount() + posCollection.getCardTotalAmount()
        query.setProjection(Projections.projectionList().add(Projections.property("id")).add(Projections.property("collectionType"))
                .add(Projections.property("pos.id")).add(Projections.property("ps.paystationType.id")).add(Projections.property("coinTotalAmount"))
                .add(Projections.property("billTotalAmount")).add(Projections.property("cardTotalAmount")).add(Projections.property("endGmt")));
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult(criteria.getItemsPerPage() * (criteria.getPage() - 1));
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        addPosCollectionOrder(query, criteria, (PosCollection) null);
        
        return prepareCollectionInfoList(query.list(), timeZone);
    }
    
    @Override
    public Integer locatePosCollectionPageByCriteria(final CollectionSearchCriteria criteria) {
        Integer page = null;
        
        final PosCollection targetObj = this.entityDao.get(PosCollection.class, criteria.getTargetId());
        if ((targetObj != null) && (criteria.getItemsPerPage() != null)) {
            final Criteria query = createPosCollectionQuery(criteria);
            addPosCollectionOrder(query, criteria, targetObj);
            
            query.setProjection(Projections.count("id"));
            
            final double itemIdx = ((Number) query.uniqueResult()).doubleValue();
            if (itemIdx <= 0) {
                page = -1;
            } else {
                page = (int) Math.floor(itemIdx / criteria.getItemsPerPage().doubleValue());
                page += 1;
            }
        }
        
        return page;
    }
    
    private void addPosCollectionOrder(final Criteria query, final CollectionSearchCriteria criteria, final PosCollection targetObj) {
        ItemLocatorCriterionAggregator aggregator = new ItemLocatorCriterionAggregator();
        
        query.addOrder(Order.desc("endGmt")).addOrder(Order.desc("id"));
        if (targetObj != null) {
            aggregator.descending("endGmt", targetObj.getEndGmt(), new Date(0)).descending("id", targetObj.getId(), -1);
            
            query.add(aggregator.aggregateCriterion());
        }
    }
    
    private Criteria createPosCollectionQuery(final CollectionSearchCriteria criteria) {
        final Criteria query = this.entityDao.createCriteria(PosCollection.class);
        query.createAlias("customer", "c");
        query.createAlias("pointOfSale", "pos");
        query.createAlias("pos.paystation", "ps");
        query.createAlias("collectionType", "ct");
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("c.id", criteria.getCustomerId()));
        }
        
        if (criteria.getPointOfSaleId() != null) {
            query.add(Restrictions.eq("pos.id", criteria.getPointOfSaleId()));
        } else {
            if (criteria.getLocationId() != null) {
                query.add(Restrictions.eq("pos.location.id", criteria.getLocationId()));
            }
            if (criteria.getRouteId() != null) {
                query.createAlias("pos.routePOSes", "posRoute").createAlias("posRoute.route", "route").add(Restrictions.eq("route.id", criteria.getRouteId()));
            }
        }
        
        if ((criteria.getCollectionTypeIds() != null) && (!criteria.getCollectionTypeIds().isEmpty())) {
            query.add(Restrictions.in("ct.id", criteria.getCollectionTypeIds()));
        }
        
        if (criteria.getMinCollectionDate() != null) {
            query.add(Restrictions.ge("endGmt", criteria.getMinCollectionDate()));
        }
        if (criteria.getMaxCollectionDate() != null) {
            query.add(Restrictions.le("endGmt", criteria.getMaxCollectionDate()));
        }
        
        return query;
    }
    
    private CollectionsCentrePosInfo prepareCollectionInfo(final List<Object[]> raw, final TimeZone timeZone) {
        final CollectionsCentrePosInfo posInfo = new CollectionsCentrePosInfo();
        Map<Integer, PointOfSale> posHash = null;
        
        final Set<Integer> posIds = new HashSet<Integer>();
        for (Object[] placeholder : raw) {
            posIds.add((Integer) placeholder[2]);
        }
        
        if (posIds.size() <= 0) {
            posHash = new HashMap<Integer, PointOfSale>(1);
        } else {
            posHash = this.pointOfSaleService.findPosHashByPosIds(posIds);
        }
        
        for (Object[] placeholder : raw) {
            final CollectionsCentreCollectionInfo info = new CollectionsCentreCollectionInfo();
            
            info.setCollectionRandomId(new WebObjectId(PosCollection.class, placeholder[0]));
            
            final CollectionType type = (CollectionType) placeholder[1];
            info.setRevenueType(type.getName());
            
            final PointOfSale pos = posHash.get(placeholder[2]);
            if (pos != null) {
                posInfo.setRandomId(new WebObjectId(PointOfSale.class, pos.getId()));
                posInfo.setPointOfSaleName(pos.getName());
                posInfo.setSerial(pos.getSerialNumber());
                posInfo.setLocation(pos.getLocation().getName());
                posInfo.setLatitude(pos.getLatitude());
                posInfo.setLongitude(pos.getLongitude());
                posInfo.setLastSeen(new RelativeDateTime((Date) placeholder[7], timeZone));
                
                final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(pos.getId(), WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
                
                posInfo.setRoute(new ArrayList<String>(routeList.size()));
                for (Route route : routeList) {
                    posInfo.addRoute(route.getName());
                }
                
                posInfo.setPayStationType((Integer) placeholder[3]);
                
                final Integer coinTotalAmount = (Integer) placeholder[4];
                final Integer billTotalAmount = (Integer) placeholder[5];
                final Integer cardTotalAmount = (Integer) placeholder[6];
                
                switch (type.getId()) {
                    case WebCoreConstants.COLLECTION_TYPE_ALL:
                        info.setAmount(coinTotalAmount + billTotalAmount + cardTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_COIN:
                        info.setAmount(coinTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_BILL:
                        info.setAmount(billTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_CARD:
                        info.setAmount(cardTotalAmount);
                        break;
                    default:
                        break;
                }
                
                final String userName = (String) placeholder[8];
                if (userName != null) {
                    try {
                        info.setCollectionUserName(URLDecoder.decode((String) placeholder[8], WebSecurityConstants.URL_ENCODING_LATIN1));
                    } catch (UnsupportedEncodingException uee) {
                        info.setCollectionUserName((String) placeholder[8]);
                    }
                }
                posInfo.addCollectionInfo(info);
            }
        }
        
        return posInfo;
    }
    
    private List<CollectionsCentreCollectionInfo> prepareCollectionInfoList(final List<Object[]> raw, final TimeZone timeZone) {
        final List<CollectionsCentreCollectionInfo> result = new ArrayList<CollectionsCentreCollectionInfo>(raw.size());
        
        Map<Integer, PointOfSale> posHash = null;
        
        final Set<Integer> posIds = new HashSet<Integer>();
        for (Object[] placeholder : raw) {
            posIds.add((Integer) placeholder[2]);
        }
        
        if (posIds.size() <= 0) {
            posHash = new HashMap<Integer, PointOfSale>(1);
        } else {
            posHash = this.pointOfSaleService.findPosHashByPosIds(posIds);
        }
        
        for (Object[] placeholder : raw) {
            final CollectionsCentreCollectionInfo info = new CollectionsCentreCollectionInfo();
            
            info.setCollectionRandomId(new WebObjectId(PosCollection.class, placeholder[0]));
            
            final CollectionType type = (CollectionType) placeholder[1];
            info.setRevenueType(type.getName());
            
            final PointOfSale pos = posHash.get(placeholder[2]);
            if (pos != null) {
                info.setRandomId(new WebObjectId(PointOfSale.class, pos.getId()));
                info.setPointOfSaleName(pos.getName());
                info.setSerial(pos.getSerialNumber());
                info.setLocation(pos.getLocation().getName());
                info.setLatitude(pos.getLatitude());
                info.setLongitude(pos.getLongitude());
                info.setLastSeen(new RelativeDateTime((Date) placeholder[7], timeZone));
                
                final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(pos.getId(), WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
                
                info.setRoute(new ArrayList<String>(routeList.size()));
                for (Route route : routeList) {
                    info.getRoute().add(route.getName());
                }
                
                info.setPayStationType((Integer) placeholder[3]);
                
                final Integer coinTotalAmount = (Integer) placeholder[4];
                final Integer billTotalAmount = (Integer) placeholder[5];
                final Integer cardTotalAmount = (Integer) placeholder[6];
                
                switch (type.getId()) {
                    case WebCoreConstants.COLLECTION_TYPE_ALL:
                        info.setAmount(coinTotalAmount + billTotalAmount + cardTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_COIN:
                        info.setAmount(coinTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_BILL:
                        info.setAmount(billTotalAmount);
                        break;
                    case WebCoreConstants.COLLECTION_TYPE_CARD:
                        info.setAmount(cardTotalAmount);
                        break;
                    default:
                        break;
                }
                
                result.add(info);
            }
        }
        
        return result;
    }
    
}
