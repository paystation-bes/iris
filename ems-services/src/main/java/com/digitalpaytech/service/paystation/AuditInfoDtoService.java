package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.dto.paystation.AuditInfoDto;

public interface AuditInfoDtoService {
    
    public List<AuditInfoDto> getAuditListByDate(int customerId, Date fromDate, Date toDate);
    
    public List<AuditInfoDto> getAuditListBySerialNumber(int customerId, Date fromDate, Date toDate, String serialNumber);
    
    public List<AuditInfoDto> getAuditListByLocationId(int customerId, Date fromDate, Date toDate, int locationId);
    
    public List<AuditInfoDto> getAuditListByRouteId(int customerId, Date fromDate, Date toDate, int routeId);
}
