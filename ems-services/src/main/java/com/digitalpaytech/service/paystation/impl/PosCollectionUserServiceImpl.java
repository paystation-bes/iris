package com.digitalpaytech.service.paystation.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosCollectionUser;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.paystation.PosCollectionUserService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posCollectionUserService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosCollectionUserServiceImpl implements PosCollectionUserService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CollectionTypeService collectionTypeService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final CollectionTypeService getCollectiontypeService() {
        return this.collectionTypeService;
    }
    
    @Override
    public final Set<PosCollectionUser> findExistingPosCollectionByPosCollection(final PosCollection posCollection) {
        int interval = EmsPropertiesService.DEFAULT_COLLECTION_USER_INTERVAL;
        try {
            interval = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.COLLECTION_USER_INTERVAL,
                                                                       EmsPropertiesService.DEFAULT_COLLECTION_USER_INTERVAL);
        } catch (Exception e) {
            // Don't need to do anything use default value
        }
        
        final Calendar startCal = Calendar.getInstance();
        startCal.setTime(posCollection.getEndGmt());
        startCal.add(Calendar.MINUTE, -interval);
        final Date startTime = startCal.getTime();
        final Calendar endCal = (Calendar) startCal.clone();
        endCal.add(Calendar.MINUTE, 2 * interval);
        final Date endTime = endCal.getTime();
        
        final List<Integer> collectionTypeIdList = new ArrayList<Integer>();
        switch (posCollection.getCollectionType().getId()) {
            case WebCoreConstants.COLLECTION_TYPE_BILL:
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_BILL);
                break;
            case WebCoreConstants.COLLECTION_TYPE_COIN:
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_COIN);
                break;
            case WebCoreConstants.COLLECTION_TYPE_CARD:
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_CARD);
                break;
            default:
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_BILL);
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_COIN);
                collectionTypeIdList.add(WebCoreConstants.COLLECTION_TYPE_CARD);
                break;
        }
        
        final String[] params = new String[] { "pointOfSaleId", "collectionTypeIdList", "startTime", "endTime" };
        
        final Object[] values = new Object[] { posCollection.getPointOfSale().getId(), collectionTypeIdList, startTime, endTime };
        
        final List<PosCollectionUser> posCollectionUserList = this.entityDao
                .findByNamedQueryAndNamedParam("PosCollectionUser.findPosCollectionUserExistingUserListByCollectionType", params, values, true);
        
        final Set<PosCollectionUser> returnCollectionUsers = new HashSet<PosCollectionUser>();
        
        for (Integer collectionTypeId : collectionTypeIdList) {
            if (posCollectionUserList != null && !posCollectionUserList.isEmpty()) {
                boolean isUserFound = false;
                for (PosCollectionUser collectionUser : posCollectionUserList) {
                    if (collectionUser.getCollectionType().getId().intValue() == collectionTypeId.intValue()) {
                        collectionUser.setCollectionStartGmt(posCollection.getStartGmt());
                        collectionUser.setCollectionEndGmt(posCollection.getEndGmt());
                        collectionUser.setPosCollection(posCollection);
                        this.entityDao.update(collectionUser);
                        returnCollectionUsers.add(collectionUser);
                        isUserFound = true;
                        break;
                    }
                }
                if (!isUserFound) {
                    final PosCollectionUser collectionUser = new PosCollectionUser();
                    collectionUser.setCollectionStartGmt(posCollection.getStartGmt());
                    collectionUser.setCollectionEndGmt(posCollection.getEndGmt());
                    collectionUser.setPosCollection(posCollection);
                    collectionUser.setCollectionType(this.collectionTypeService.findCollectionType(collectionTypeId));
                    collectionUser.setPointOfSale(posCollection.getPointOfSale());
                    this.entityDao.save(collectionUser);
                    returnCollectionUsers.add(collectionUser);
                }
            } else {
                final PosCollectionUser collectionUser = new PosCollectionUser();
                collectionUser.setCollectionStartGmt(posCollection.getStartGmt());
                collectionUser.setCollectionEndGmt(posCollection.getEndGmt());
                collectionUser.setPosCollection(posCollection);
                collectionUser.setCollectionType(this.collectionTypeService.findCollectionType(collectionTypeId));
                collectionUser.setPointOfSale(posCollection.getPointOfSale());
                this.entityDao.save(collectionUser);
                returnCollectionUsers.add(collectionUser);
            }
        }
        return returnCollectionUsers;
    }
    
    @Override
    public final List<PosCollectionUser> findPosCollectionUserExistingPosCollectionListByCollectionType(final Integer pointOfSaleId,
                                                                                                        final Integer collectionTypeId, final Date startTime,
                                                                                                        final Date endTime) {
        
        final String[] params = new String[] { "pointOfSaleId", "collectionTypeId", "startTime", "endTime" };
        final Object[] values = new Object[] { pointOfSaleId, collectionTypeId, startTime, endTime };
        
        return this.entityDao.findByNamedQueryAndNamedParam("PosCollectionUser.findPosCollectionUserExistingPosCollectionListByCollectionType", params, values,
                                                            true);
        
    }
    
    @Override
    public final void savePosCollectionUserList(final List<PosCollectionUser> posCollectionUserList) {
        for (PosCollectionUser collectionUser : posCollectionUserList) {
            
            final String[] params = new String[] { "pointOfSaleId", "userAccountId", "collectionTypeId", "mobileEndGmt" };
            final Object[] values = new Object[] { collectionUser.getPointOfSale().getId(), collectionUser.getUserAccount().getId(),
                    collectionUser.getCollectionType().getId(), collectionUser.getMobileEndGmt() };
            final PosCollectionUser existingUser = (PosCollectionUser) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("PosCollectionUser.findExistingPosCollectionUserForMobile", params, values, true);
            if (existingUser == null) {
                this.entityDao.saveOrUpdate(collectionUser);
            }
        }
    }
}
