package com.digitalpaytech.service.paystation;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.dto.paystation.PaystationEventInfo;

public interface PaystationEventInfoService {
    
    public List<PaystationEventInfo> findEventsforPosList(Collection<Integer> posList, Collection<Integer> deviceList, Date dateFrom, Date dateTo, int limit);

    public List<PaystationEventInfo> findEventsforPos(int pointOfSaleId, int deviceId, int statusId, Date dateFrom, Date dateTo, int limit);

}
