package com.digitalpaytech.service.paystation.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dto.paystation.CollectionInfoDto;
import com.digitalpaytech.service.paystation.CollectionInfoDtoService;

@Service("collectionInfoDtoService")
@Transactional(propagation = Propagation.REQUIRED)
public class CollectionInfoDtoServiceImpl implements CollectionInfoDtoService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    private final static String SQL_SELECT_COLLECTION = "SELECT pos.SerialNumber AS PaystationSerialNumber, pos.Name AS PaystationName, l.Name AS PaystationRegion, pc.StartGMT AS CollectionPeriodStartDate,"
                                                        + " pc.EndGMT AS CollectionPeriodEndDate, pc.ReportNumber AS CollectionReportNumber, ct.Name AS CollectionTypeName, pc.StartTransactionNumber AS StartPermitNumber,"
                                                        + " pc.EndTransactionNumber AS EndPermitNumber, pc.TicketsSold AS NumberPermitsSold, ROUND(pc.BillAmount1 / 100.00, 2) AS BillStacker1, ROUND(pc.BillAmount2 / 100.00, 2) AS BillStacker2,"
                                                        + " ROUND(pc.BillAmount5 / 100.00, 2) AS BillStacker5, ROUND(pc.BillAmount10 / 100.00, 2) AS BillStacker10, ROUND(pc.BillAmount20 / 100.00, 2) AS BillStacker20,"
                                                        + " ROUND(pc.BillAmount50 / 100.00, 2) AS BillStacker50, ROUND(pc.VisaAmount / 100.00, 2) AS CreditCardVisa, ROUND(pc.MasterCardAmount / 100.00, 2) AS CreditCardMasterCard,"
                                                        + " ROUND(pc.AmexAmount / 100.00, 2) AS CreditCardAMEX, ROUND(pc.DiscoverAmount / 100.00, 2) AS CreditCardDiscover, ROUND(pc.DinersAmount / 100.00, 2) AS CreditCardDiners,"
                                                        + " ROUND((pc.CardTotalAmount - (pc.VisaAmount+pc.MasterCardAmount+pc.AmexAmount+pc.DiscoverAmount+pc.DinersAmount)) / 100.00, 2) AS CreditCardOther,"
                                                        + " ROUND(pc.SmartCardAmount / 100.00, 2) AS SmartCard, ROUND(pc.CoinCount05 * 0.05, 2) AS CoinCanister005, ROUND(pc.CoinCount10 * 0.10, 2) AS CoinCanister010,"
                                                        + " ROUND(pc.CoinCount25 * 0.25, 2) AS CoinCanister025, ROUND(pc.CoinCount100, 2) AS CoinCanister100, ROUND(pc.CoinCount200 * 2.00, 2) AS CoinCanister200";
    
    private final static String SQL_FROM_COLLECTION = " FROM POSCollection pc INNER JOIN PointOfSale pos ON pc.PointOfSaleId = pos.Id INNER JOIN Location l on l.Id = pos.LocationId INNER JOIN CollectionType ct ON pc.CollectionTypeId = ct.Id";
    
    private final static String SQL_FROM_ADD_ROUTE = " LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId" + " LEFT JOIN Route r ON rpos.RouteId=r.Id";
    
    private final static String SQL_WHERE_CORE = " WHERE pc.CustomerId = :customerId AND pc.CollectionTypeId != 0 ";
    
    private final static String SQL_WHERE_ADD_ENDGMTRANGE = " AND pc.EndGMT BETWEEN :fromDate AND :toDate";
    private final static String SQL_WHERE_ADD_SERILNUMBER = " AND pos.SerialNumber = :serialNumber";
    private final static String SQL_WHERE_ADD_LOCATIONID = " AND pos.LocationId = :locationId";
    private final static String SQL_WHERE_ADD_ROUTEID = " AND r.Id = :routeId";
    
    private final static String SQL_ORDER_BY_ENDGMT = " ORDER BY pc.EndGMT";
    
    @Override
    public List<CollectionInfoDto> getCollectionListByDate(int customerId, Date fromDate, Date toDate) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_COLLECTION);
        builderStr.append(SQL_FROM_COLLECTION);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
        
    }
    
    @Override
    public List<CollectionInfoDto> getCollectionListBySerialNumber(int customerId, Date fromDate, Date toDate, String serialNumber) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_COLLECTION);
        builderStr.append(SQL_FROM_COLLECTION);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_SERILNUMBER);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);

        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("serialNumber", serialNumber);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    @Override
    public List<CollectionInfoDto> getCollectionListByLocationId(int customerId, Date fromDate, Date toDate, int locationId) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_COLLECTION);
        builderStr.append(SQL_FROM_COLLECTION);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_LOCATIONID);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);

        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("locationId", locationId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    @Override
    public List<CollectionInfoDto> getCollectionListByRouteId(int customerId, Date fromDate, Date toDate, int routeId) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_COLLECTION);
        builderStr.append(SQL_FROM_COLLECTION);
        builderStr.append(SQL_FROM_ADD_ROUTE);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_ROUTEID);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);

        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("routeId", routeId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    private void addAuditInfoSQLScalars(SQLQuery q) {
        q.addScalar("PaystationSerialNumber", new StringType());
        q.addScalar("PaystationName", new StringType());
        q.addScalar("PaystationRegion", new StringType());
        q.addScalar("CollectionPeriodStartDate", new TimestampType());
        q.addScalar("CollectionPeriodEndDate", new TimestampType());
        q.addScalar("CollectionReportNumber", new IntegerType());
        q.addScalar("CollectionTypeName", new StringType());
        q.addScalar("StartPermitNumber", new IntegerType());
        q.addScalar("EndPermitNumber", new IntegerType());
        q.addScalar("NumberPermitsSold", new IntegerType());
        q.addScalar("BillStacker1", new BigDecimalType());
        q.addScalar("BillStacker2", new BigDecimalType());
        q.addScalar("BillStacker5", new BigDecimalType());
        q.addScalar("BillStacker10", new BigDecimalType());
        q.addScalar("BillStacker20", new BigDecimalType());
        q.addScalar("BillStacker50", new BigDecimalType());
        q.addScalar("CreditCardVisa", new BigDecimalType());
        q.addScalar("CreditCardMasterCard", new BigDecimalType());
        q.addScalar("CreditCardAMEX", new BigDecimalType());
        q.addScalar("CreditCardDiscover", new BigDecimalType());
        q.addScalar("CreditCardDiners", new BigDecimalType());
        q.addScalar("CreditCardOther", new BigDecimalType());
        q.addScalar("SmartCard", new BigDecimalType());
        q.addScalar("CoinCanister005", new BigDecimalType());
        q.addScalar("CoinCanister010", new BigDecimalType());
        q.addScalar("CoinCanister025", new BigDecimalType());
        q.addScalar("CoinCanister100", new BigDecimalType());
        q.addScalar("CoinCanister200", new BigDecimalType());
        q.setResultTransformer(Transformers.aliasToBean(CollectionInfoDto.class));
    }
    
}
