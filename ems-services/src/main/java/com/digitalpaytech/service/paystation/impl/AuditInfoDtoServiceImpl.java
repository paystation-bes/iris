package com.digitalpaytech.service.paystation.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dto.paystation.AuditInfoDto;
import com.digitalpaytech.service.paystation.AuditInfoDtoService;

@Service("auditInfoDtoService")
@Transactional(propagation = Propagation.REQUIRED)
public class AuditInfoDtoServiceImpl implements AuditInfoDtoService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    private final static String SQL_SELECT_AUDIT = "SELECT pos.SerialNumber AS PaystationSerialNumber, pos.Name AS PaystationName, l.Name AS PaystationLocation,"
                                                   + " pc.StartGMT AS AuditPeriodStartDate, pc.EndGMT AS AuditPeriodEndDate, pc.ReportNumber AS AuditReportNumber, pc.TicketsSold AS NumberPermitsSold,"
                                                   + " CASE WHEN p.PaystationTypeId = 0 THEN ROUND((pc.CoinTotalAmount + pc.BillTotalAmount + pc.CardTotalAmount) / 100, 2)"
                                                   + " ELSE ROUND((pc.AcceptedFloatAmount + pc.CoinTotalAmount + pc.BillTotalAmount + pc.CardTotalAmount - pc.OverfillAmount) / 100, 2) END AS TotalCollection,"
                                                   + " CASE WHEN p.PaystationTypeId = 0 THEN ROUND((pc.CoinTotalAmount + pc.BillTotalAmount + pc.CardTotalAmount + pc.SmartCardAmount - pc.SmartCardRechargeAmount"
                                                   + " + pc.AttendantTicketsAmount - pc.AttendantDepositAmount - pc.RefundIssuedAmount - pc.ChangeIssuedAmount) / 100, 2)"
                                                   + " ELSE ROUND((pc.AcceptedFloatAmount + pc.CoinTotalAmount + pc.BillTotalAmount + pc.CardTotalAmount - pc.OverfillAmount + pc.SmartCardAmount - pc.SmartCardRechargeAmount"
                                                   + " - pc.RefundIssuedAmount - pc.ChangeIssuedAmount) / 100, 2) END AS TotalRevenue,"
                                                   + " ROUND(pc.ExcessPaymentAmount / 100, 2) AS OverPayment, ROUND(pc.ChangeIssuedAmount / 100, 2) AS ChangeIssued , ROUND(pc.RefundIssuedAmount / 100, 2) AS RefundsIssued, ROUND(pc.CoinTotalAmount / 100, 2) AS TotalCoinBag,"
                                                   + " ROUND(pc.BillTotalAmount / 100, 2) AS TotalBillStacker, ROUND(pc.BillAmount1 / 100, 2) AS BillStacker1, ROUND(pc.BillAmount2 / 100, 2) AS BillStacker2, ROUND(pc.BillAmount5 / 100, 2) AS BillStacker5, ROUND(pc.BillAmount10 / 100, 2) AS BillStacker10,"
                                                   + " ROUND(pc.BillAmount20 / 100, 2) AS BillStacker20, ROUND(pc.BillAmount50 / 100, 2) AS BillStacker50, ROUND(pc.CardTotalAmount / 100, 2) AS TotalCreditCard, ROUND(pc.VisaAmount / 100, 2) AS CreditCardVisa, ROUND(pc.MasterCardAmount / 100, 2) AS CreditCardMasterCard,"
                                                   + " ROUND(pc.AmexAmount / 100, 2) AS CreditCardAMEX, ROUND(pc.DiscoverAmount / 100, 2) AS CreditCardDiscover, ROUND(pc.DinersAmount / 100, 2) AS CreditCardDiners,"
                                                   + " ROUND((pc.CardTotalAmount - (pc.VisaAmount+pc.MasterCardAmount+pc.AmexAmount+pc.DiscoverAmount+pc.DinersAmount)) / 100, 2) AS CreditCardOther,"
                                                   + " ROUND((pc.SmartCardAmount + pc.SmartCardRechargeAmount) / 100, 2) AS TotalSmartCard, ROUND(pc.SmartCardAmount / 100, 2) AS SmartCardCharge, ROUND(pc.SmartCardRechargeAmount / 100, 2) AS SmartCardRecharge,"
                                                   + " ROUND(pc.ReplenishedAmount / 100, 2) AS CoinChangerReplenished, ROUND(pc.OverfillAmount / 100, 2) AS CoinChangerOverfillBag, ROUND(pc.AcceptedFloatAmount / 100, 2) AS CoinChangerAcceptedFloat,"
                                                   + " ROUND((pc.ChangeIssuedAmount - pc.Hopper1Dispensed - pc.Hopper2Dispensed) / 100, 2) AS CoinChangerDispensed, ROUND(pc.TestDispensedChanger / 100, 2) AS CoinChangerTestDispensed,"
                                                   + " ROUND(pc.Hopper1Type / 100, 2) AS CoinHopper1Type, ROUND(pc.Hopper1Replenished / 100, 2) AS CoinHopper1Replenished, ROUND(pc.Hopper1Dispensed / 100, 2) AS CoinHopper1Dispensed, ROUND(pc.TestDispensedHopper1 / 100, 2) AS CoinHopper1TestDispensed,"
                                                   + " ROUND(pc.Hopper2Type / 100, 2) AS CoinHopper2Type, ROUND(pc.Hopper2Replenished / 100, 2) AS CoinHopper2Replenished, ROUND(pc.Hopper2Dispensed / 100, 2) AS CoinHopper2Dispensed, ROUND(pc.TestDispensedHopper2 / 100, 2) AS CoinHopper2TestDispensed";
    
    private final static String SQL_FROM_AUDIT = " FROM POSCollection pc" + " INNER JOIN PointOfSale pos ON pc.PointOfSaleId = pos.Id"
                                                 + " INNER JOIN Paystation p ON pos.PaystationId = p.Id"
                                                 + " INNER JOIN Location l on l.Id = pos.LocationId";
    
    private final static String SQL_FROM_ADD_ROUTE = " LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId"
                                                     + " LEFT JOIN Route r ON rpos.RouteId=r.Id";
    
    // All collection types are added to Audit requests according to SUPP-1074
    //    private final static String SQL_WHERE_CORE = " WHERE pc.CustomerId = :customerId AND pc.CollectionTypeId = 0 ";
    private final static String SQL_WHERE_CORE = " WHERE pc.CustomerId = :customerId ";
    
    private final static String SQL_WHERE_ADD_ENDGMTRANGE = " AND pc.EndGMT BETWEEN :fromDate AND :toDate";
    private final static String SQL_WHERE_ADD_SERILNUMBER = " AND pos.SerialNumber = :serialNumber";
    private final static String SQL_WHERE_ADD_LOCATIONID = " AND pos.LocationId = :locationId";
    private final static String SQL_WHERE_ADD_ROUTEID = " AND r.Id = :routeId";
    
    private final static String SQL_ORDER_BY_ENDGMT = " ORDER BY pc.EndGMT";
    
    @Override
    public List<AuditInfoDto> getAuditListByDate(int customerId, Date fromDate, Date toDate) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_AUDIT);
        builderStr.append(SQL_FROM_AUDIT);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
        
    }
    
    @Override
    public List<AuditInfoDto> getAuditListBySerialNumber(int customerId, Date fromDate, Date toDate, String serialNumber) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_AUDIT);
        builderStr.append(SQL_FROM_AUDIT);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_SERILNUMBER);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("serialNumber", serialNumber);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    @Override
    public List<AuditInfoDto> getAuditListByLocationId(int customerId, Date fromDate, Date toDate, int locationId) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_AUDIT);
        builderStr.append(SQL_FROM_AUDIT);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_LOCATIONID);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("locationId", locationId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    @Override
    public List<AuditInfoDto> getAuditListByRouteId(int customerId, Date fromDate, Date toDate, int routeId) {
        StringBuilder builderStr = new StringBuilder();
        builderStr.append(SQL_SELECT_AUDIT);
        builderStr.append(SQL_FROM_AUDIT);
        builderStr.append(SQL_FROM_ADD_ROUTE);
        builderStr.append(SQL_WHERE_CORE);
        builderStr.append(SQL_WHERE_ADD_ROUTEID);
        builderStr.append(SQL_WHERE_ADD_ENDGMTRANGE);
        builderStr.append(SQL_ORDER_BY_ENDGMT);
        
        SQLQuery q = entityDao.createSQLQuery(builderStr.toString());
        addAuditInfoSQLScalars(q);
        
        q.setParameter("customerId", customerId);
        q.setParameter("routeId", routeId);
        q.setParameter("fromDate", fromDate);
        q.setParameter("toDate", toDate);
        
        return q.list();
    }
    
    private void addAuditInfoSQLScalars(SQLQuery q) {
        q.addScalar("PaystationSerialNumber", new StringType());
        q.addScalar("PaystationName", new StringType());
        q.addScalar("PaystationLocation", new StringType());
        q.addScalar("AuditPeriodStartDate", new TimestampType());
        q.addScalar("AuditPeriodEndDate", new TimestampType());
        q.addScalar("AuditReportNumber", new IntegerType());
        q.addScalar("NumberPermitsSold", new IntegerType());
        q.addScalar("TotalCollection", new BigDecimalType());
        q.addScalar("TotalRevenue", new BigDecimalType());
        q.addScalar("Overpayment", new BigDecimalType());
        q.addScalar("ChangeIssued", new BigDecimalType());
        q.addScalar("RefundsIssued", new BigDecimalType());
        q.addScalar("TotalCoinBag", new BigDecimalType());
        q.addScalar("TotalBillStacker", new BigDecimalType());
        q.addScalar("BillStacker1", new BigDecimalType());
        q.addScalar("BillStacker2", new BigDecimalType());
        q.addScalar("BillStacker5", new BigDecimalType());
        q.addScalar("BillStacker10", new BigDecimalType());
        q.addScalar("BillStacker20", new BigDecimalType());
        q.addScalar("BillStacker50", new BigDecimalType());
        q.addScalar("TotalCreditCard", new BigDecimalType());
        q.addScalar("CreditCardVisa", new BigDecimalType());
        q.addScalar("CreditCardMasterCard", new BigDecimalType());
        q.addScalar("CreditCardAMEX", new BigDecimalType());
        q.addScalar("CreditCardDiscover", new BigDecimalType());
        q.addScalar("CreditCardDiners", new BigDecimalType());
        q.addScalar("CreditCardOther", new BigDecimalType());
        q.addScalar("TotalSmartCard", new BigDecimalType());
        q.addScalar("SmartCardCharge", new BigDecimalType());
        q.addScalar("SmartCardRecharge", new BigDecimalType());
        q.addScalar("CoinChangerReplenished", new BigDecimalType());
        q.addScalar("CoinChangerOverfillBag", new BigDecimalType());
        q.addScalar("CoinChangerAcceptedFloat", new BigDecimalType());
        q.addScalar("CoinChangerDispensed", new BigDecimalType());
        q.addScalar("CoinChangerTestDispensed", new BigDecimalType());
        q.addScalar("CoinHopper1Type", new BigDecimalType());
        q.addScalar("CoinHopper1Replenished", new BigDecimalType());
        q.addScalar("CoinHopper1Dispensed", new BigDecimalType());
        q.addScalar("CoinHopper1TestDispensed", new BigDecimalType());
        q.addScalar("CoinHopper2Type", new BigDecimalType());
        q.addScalar("CoinHopper2Replenished", new BigDecimalType());
        q.addScalar("CoinHopper2Dispensed", new BigDecimalType());
        q.addScalar("CoinHopper2TestDispensed", new BigDecimalType());
        q.setResultTransformer(Transformers.aliasToBean(AuditInfoDto.class));
    }
    
}
