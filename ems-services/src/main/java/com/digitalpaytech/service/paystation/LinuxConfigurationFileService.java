package com.digitalpaytech.service.paystation;

import java.util.List;
import java.util.Optional;

import com.digitalpaytech.domain.LinuxConfigurationFile;

public interface LinuxConfigurationFileService {
    int deleteByPointOfSaleIdAndSnaphotId(int pointOfSaleId, String snapshotId);
    
    List<LinuxConfigurationFile> findByPointOfSaleIdAndSnapshotId(int pointOfSaleId, String snapshotId);
    
    LinuxConfigurationFile findByPointOfSaleIdAndFileIdAndSnapShotId(int pointOfSaleId, String fileId, String snapshotId);
    
    boolean fileExistsByFileIdAndSnapShotIdAndPointOfSaleId(String fileId, int pointOfSaleId, String snapshotId);

    void save(LinuxConfigurationFile file);

    int deleteByPointOfSaleId(int pointOfSaleId);
    
    Optional<LinuxConfigurationFile> findLatestByPointOfSaleId(int pointOfSaleId);
}
