package com.digitalpaytech.service.paystation;

import java.util.Set;

import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.exception.InvalidDataException;

public interface EventAlertService {
    public Set<String> getPositiveActions();
    
    public void processEvent(PointOfSale pointOfSale, EventData eventData) throws InvalidDataException;
    
    public EventDefinition getEventDefinitionByDeviceTypeIdAndDescriptionNoSpace(Integer deviceTypeId, String descriptionNoSpace);
}
