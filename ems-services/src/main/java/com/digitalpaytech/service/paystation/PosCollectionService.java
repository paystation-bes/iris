package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.dto.CollectionSearchCriteria;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePosInfo;

public interface PosCollectionService {
    
    List<PosCollection> findPosCollectionByCustomerIdAndDateRange(int customerId, long dateBoundary, int pageNumber, boolean isAll, boolean isBill,
                                                                  boolean isCoin, boolean isCard);
    
    List<PosCollection> findPosCollectionByPointOfSaleListIdAndDateRange(List<PointOfSale> posList, long dateBoundary, int pageNumber, boolean isAll,
                                                                         boolean isBill, boolean isCoin, boolean isCard);
    
    List<PosCollection> findPosCollectionByPointOfSaleIdLast90Days(int pointOfSaleId, String sortOrder, String sortItem, Integer page);
    
    PosCollection findPosCollectionByPosCollectionId(int collectionId);
    
    PosCollection findByPosIdCollectionTypeIdStartEndGmt(PosCollection posCollection);
    
    Date findLastCollectionDate(PosCollection collection);
    
    boolean isDuplicateRecord(PosCollection posCollection);
    
    void processPosCollection(PointOfSale pointOfSale, PosCollection posCollection);
    
    List<CollectionsCentreCollectionInfo> findPosCollectionByCriteria(CollectionSearchCriteria criteria, TimeZone timeZone);
    
    Integer locatePosCollectionPageByCriteria(CollectionSearchCriteria criteria);
    
    CollectionsCentrePosInfo findPosInfoByCriteria(CollectionSearchCriteria criteria, TimeZone timeZone);
    
}
