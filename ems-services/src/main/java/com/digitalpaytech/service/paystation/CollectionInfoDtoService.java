package com.digitalpaytech.service.paystation;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.dto.paystation.CollectionInfoDto;

public interface CollectionInfoDtoService {
    
    public List<CollectionInfoDto> getCollectionListByDate(int customerId, Date fromDate, Date toDate);
    
    public List<CollectionInfoDto> getCollectionListBySerialNumber(int customerId, Date fromDate, Date toDate, String serialNumber);
    
    public List<CollectionInfoDto> getCollectionListByLocationId(int customerId, Date fromDate, Date toDate, int locationId);
    
    public List<CollectionInfoDto> getCollectionListByRouteId(int customerId, Date fromDate, Date toDate, int routeId);
}
