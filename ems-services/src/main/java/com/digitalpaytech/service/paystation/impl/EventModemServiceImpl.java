package com.digitalpaytech.service.paystation.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AccessPoint;
import com.digitalpaytech.domain.Carrier;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.ModemType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.AccessPointService;
import com.digitalpaytech.service.CarrierService;
import com.digitalpaytech.service.ModemTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.EventModemService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("eventModemService")
@Transactional(propagation = Propagation.REQUIRED)
public class EventModemServiceImpl implements EventModemService {
    
    private static final Logger LOGGER = Logger.getLogger(EventModemServiceImpl.class);
    private static final int MEIDSIZE = 18;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private ModemTypeService modemTypeService;
    
    @Autowired
    private CarrierService carrierService;
    
    @Autowired
    private AccessPointService accessPointService;
    
    @Override
    public final void saveModemSetting(final PointOfSale pointOfSale, final EventData eventData) {
        
        final Date now = DateUtil.getCurrentGmtDate();
        
        final ModemSetting modemSetting = setModemSetting(pointOfSale);
        
        final String action = eventData.getAction();
        
        if (action.equals(WebCoreConstants.EVENT_ACTION_TYPE_STRING)) {
            
            final ModemType modemType = setModemType(eventData, now);
            
            modemSetting.setModemType(modemType);
            if (modemType.getId() == WebCoreConstants.MODEM_TYPE_CDMA) {
                modemSetting.setMeid(setMeidSetting(modemSetting.getCcid()));
            } else {
                modemSetting.setMeid(null);
            }
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_CCID_STRING)) {
            
            modemSetting.setCcid(eventData.getInformation());
            if (modemSetting.getModemType().getId() == WebCoreConstants.MODEM_TYPE_CDMA) {
                modemSetting.setMeid(setMeidSetting(eventData.getInformation()));
            }
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_CARRIER_STRING)) {
            final Carrier carrier = setCarrierSetting(eventData, now);
            
            modemSetting.setCarrier(carrier);
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_APN_STRING)) {
            final AccessPoint accessPoint = setAccessPointSetting(eventData, now);
            modemSetting.setAccessPoint(accessPoint);
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_MODEM_HARDWARE_MANUFACTURE_STRING)) {
            modemSetting.setHardwareManufacturer(eventData.getInformation() == null ? "" : eventData.getInformation());
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_MODEM_HARDWARE_MODEL_STRING)) {
            modemSetting.setHardwareModel(eventData.getInformation() == null ? "" : eventData.getInformation());
            
        } else if (action.equals(WebCoreConstants.EVENT_ACTION_MODEM_HARDWARE_FIRMWARE_STRING)) {
            modemSetting.setHardwareFirmware(eventData.getInformation() == null ? "" : eventData.getInformation());
        } else {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("+++ unknown modem action: " + action + " for ps: " + pointOfSale.getSerialNumber() + " +++");
            }
        }
        
        modemSetting.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        modemSetting.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        
        this.entityDao.saveOrUpdate(modemSetting);
    }
    
    private AccessPoint setAccessPointSetting(final EventData eventData, final Date now) {
        if (StringUtils.isNotBlank(eventData.getInformation())) {
            AccessPoint accessPoint = this.accessPointService.findAccessPoint(eventData.getInformation());
            if (accessPoint == null) {
                // table is not defined as autoincrement and making autoincrement will use up the Ids pretty fast 
                final int newId = this.accessPointService.loadAll().size() + 1;
                accessPoint = new AccessPoint();
                accessPoint.setId((byte) newId);
                accessPoint.setName(eventData.getInformation());
                accessPoint.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                accessPoint.setLastModifiedGmt(now);
                this.entityDao.save(accessPoint);
            }
            return accessPoint;
        }
        return null;
    }
    
    private Carrier setCarrierSetting(final EventData eventData, final Date now) {
        if (StringUtils.isNotBlank(eventData.getInformation())) {
            Carrier carrier = this.carrierService.findCarrier(eventData.getInformation());
            if (carrier == null) {
                carrier = new Carrier();
                carrier.setName(eventData.getInformation());
                carrier.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                carrier.setLastModifiedGmt(now);
                this.entityDao.save(carrier);
            }
            return carrier;
        }
        return null;
    }
    
    private String setMeidSetting(final String data) {
        if (data != null && data.length() == MEIDSIZE) {
            final Long first10digits = Long.valueOf(data.substring(0, 10));
            final Long last8digits = Long.valueOf(data.substring(10));
            return Long.toHexString(first10digits).toUpperCase() + Long.toHexString(last8digits).toUpperCase();
            
        }
        
        return null;
    }
    
    private ModemType setModemType(final EventData eventData, final Date now) {
        if (StringUtils.isNotBlank(eventData.getInformation())) {
            ModemType modemType = this.modemTypeService.findModemType(eventData.getInformation());
            if (modemType == null) {
                // table is not defined as autoincrement and making autoincrement will use up the Ids pretty fast 
                // 0 is used as a valid id.
                final int newId = this.modemTypeService.loadAll().size() + 1;
                modemType = new ModemType();
                modemType.setId((byte) newId);
                modemType.setName(eventData.getInformation());
                modemType.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                modemType.setLastModifiedGmt(now);
                this.entityDao.save(modemType);
            }
            return modemType;
        }
        return null;
    }
    
    private ModemSetting setModemSetting(final PointOfSale pointOfSale) {
        
        ModemSetting modemSetting = this.pointOfSaleService.findModemSettingByPointOfSaleid(pointOfSale.getId());
        if (modemSetting == null) {
            modemSetting = new ModemSetting();
            modemSetting.setPointOfSale(pointOfSale);
            modemSetting.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            modemSetting.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        }
        
        return modemSetting;
    }
    
    @SuppressWarnings("unused")
    private void insertNewPosDate(final PointOfSale pointOfSale, final int posDateTypeId, final UserAccount userAccount, final boolean setting) {
        final PosDate posDate = new PosDate();
        posDate.setPointOfSale(pointOfSale);
        posDate.setPosDateType(this.pointOfSaleService.findPosDateTypeById(posDateTypeId));
        posDate.setCurrentSetting(setting);
        posDate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        posDate.setLastModifiedByUserId(userAccount);
        posDate.setChangedGmt(DateUtil.getCurrentGmtDate());
        this.pointOfSaleService.saveOrUpdatePOSDate(posDate);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setModemTypeService(final ModemTypeService modemTypeService) {
        this.modemTypeService = modemTypeService;
    }
    
    public final void setCarrierService(final CarrierService carrierService) {
        this.carrierService = carrierService;
    }
    
    public final void setAccessPointService(final AccessPointService accessPointService) {
        this.accessPointService = accessPointService;
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final ModemTypeService getModemTypeService() {
        return this.modemTypeService;
    }
    
    public final CarrierService getCarrierService() {
        return this.carrierService;
    }
    
    public final AccessPointService getAccessPointService() {
        return this.accessPointService;
    }
    
}
