package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.RateProfileLocation;

public interface RateProfileLocationService {
    List<RateProfileLocation> findByRateProfileId(Integer rateProfileId);
    
    List<RateProfileLocation> findPublishedByLocationId(Integer locationId, Integer customerId);
    
    RateProfileLocation findById(Integer rateProfileLocationId);
    
    int saveAll(List<RateProfileLocation> locationsList, Integer customerId);
    
    int deleteByRateProfileId(Integer rateProfileId);
}
