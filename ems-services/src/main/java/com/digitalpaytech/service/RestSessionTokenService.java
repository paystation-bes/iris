package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.RestSessionToken;

public interface RestSessionTokenService {
    
    public List<RestSessionToken> findRestSessionTokenBySessionToken(String sessionToken);
    
    public RestSessionToken findRestSessionTokenByRestAccountId(int restAccountId);
    
    public int findCountBySessionToken(String sessionToken);
    
    public void saveRestSessionToken(RestSessionToken restSessionToken);
    
    public void updateRestSessionToken(RestSessionToken restSessionToken);
}