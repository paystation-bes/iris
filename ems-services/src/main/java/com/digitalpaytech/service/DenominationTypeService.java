package com.digitalpaytech.service;

import com.digitalpaytech.domain.DenominationType;

public interface DenominationTypeService {
    public DenominationType findDenominationType(boolean isCoin, Integer denominationAmount);
}
