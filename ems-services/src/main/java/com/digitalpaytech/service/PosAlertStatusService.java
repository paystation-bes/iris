package com.digitalpaytech.service;

import java.util.Map;

import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;

public interface PosAlertStatusService {
    
    int getCommunicationAlertsSum();
    
    int getCollectionAlertsSum();
    
    int getPaystationAlertsSum();
    
    Map<Integer, PosAlertStatusDetailSummary> findPosAlertDetailSummaryByPointOfSaleId(int pointOfSaleId);
    
    void updateAlertCount(PosEventCurrent posEventCurrent, int originalSeverity, boolean isSeverityRecalc);
    
}
