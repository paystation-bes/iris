package com.digitalpaytech.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.dto.customeradmin.ActivityLogDetail;

public interface ActivityLogService {
    
    Collection<ActivityLog> findActivityLogsByUserAccountId(Integer userAccountId);
    
    List<ActivityLog> findActivityLogWithCriteria(Calendar start, Calendar end, List<Integer> userIds, List<Integer> logTypes, String order,
        String column, int page, int numOfRecord);
    
    ActivityType findActivityTypeByActivityId(int activityTypeId);
    
    List<ActivityType> findActivityTypesByParentActivityTypeId(Integer parentActivityTypeId);
    
    List<ActivityType> findAllChildActivityTypes();
    
    ActivityLogDetail getActivityLogDetailsByActivityLogId(String activityLogId);
    
}
