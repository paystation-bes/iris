package com.digitalpaytech.service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.TransactionFileUpload;

public interface TransactionFileUploadService {
    TransactionFileUpload findTransactionFileUpload(Customer customer, String fileName, String checksum);
    void saveTransactionFileUpload(Customer customer, String fullPathFileName);
    void updateTransactionFileUpload(TransactionFileUpload transactionFileUpload);
}
