package com.digitalpaytech.service;

import com.digitalpaytech.domain.RateFlatType;

public interface RateFlatTypeService {
    
    RateFlatType findRateFlatTypeById(Byte rateFlatTypeId);
    
}
