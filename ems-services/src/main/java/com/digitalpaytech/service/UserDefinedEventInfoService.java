package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.data.UserDefinedEventInfo;

public interface UserDefinedEventInfoService {
    
    List<UserDefinedEventInfo> findEventsClassTypeAndLimit(Class<? extends UserDefinedEventInfo> classType, Integer limit);
    
    List<UserDefinedEventInfo> findEventsByNamedQueryAndPointOfSaleIdList(String queryName, List<Integer> pointOfSaleIdList);
    
    List<UserDefinedEventInfo> findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(String queryName, List<Integer> pointOfSaleIdList,
        Integer customerAlertTypeId);
}
