package com.digitalpaytech.service;

import com.digitalpaytech.domain.Replenish;

public interface ReplenishService {
    public void processReplenish(Replenish replenish);
}
