package com.digitalpaytech.service;

import java.util.Date;

import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;

public interface CcFailLogService {
    void logFailedCcTransaction(CcFailLog ccFailLog);
    void logFailedCcTransaction(PointOfSale pointOfSale, 
                                MerchantAccount merchantAccount, 
                                Date procDate,
                                Date purchasedDate, 
                                int ticketNumber, 
                                int ccType, 
                                String failedReason);
}
