package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ReportStatusType;

public interface ReportStatusTypeService {
    public List<ReportStatusType> loadAll();
}
