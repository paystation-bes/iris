package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.util.RandomKeyMapping;

public interface RateProfileService {
    
    RateProfile findRateProfileById(Integer rateProfileId);
    
    RateProfile findRateProfileByCustomerIdAndPermitIssueTypeIdAndName(Integer customerId, Integer permitIssueTypeId, String name);
    
    List<RateProfile> findRateProfilesByCustomerId(Integer customerId);
    
    List<FilterDTO> findPublishedRateProfileFiltersByCustomerId(Integer customerId, RandomKeyMapping keyMapping);
    
    List<RateProfile> findRateProfilesByCustomerIdAndPermitIssueTypeId(Integer customerId, Integer permitIssueTypeId);
    
    List<RateProfile> findPagedRateProfileList(Integer customerId, Integer permitIssueTypeId, Boolean isPublished, Date maxUpdateTime, int pageNumber);
    
    void saveOrUpdateRateProfile(RateProfile rateProfile, Collection<RateRateProfile> rateRateProfileList,
                                 Collection<RateProfileLocation> rateProfileLocationList);
    
    void deleteRateProfile(RateProfile rateProfile, Integer userAccountId);
    
    List<RateProfile> searchRateProfiles(Integer customerId, String keyword);
    
    int findRateProfilePage(Integer rateProfileId, Integer customerId, Integer permitIssueTypeId, Boolean isPublished, Date maxUpdateTime);
    
}
