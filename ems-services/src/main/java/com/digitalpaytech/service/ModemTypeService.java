package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ModemType;

public interface ModemTypeService {
    public List<ModemType> loadAll();
    public ModemType findModemType(String name);
    public void save(ModemType modemType);
}
