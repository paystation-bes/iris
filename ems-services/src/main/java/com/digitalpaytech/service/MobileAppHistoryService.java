package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileAppHistory;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.UserAccount;

public interface MobileAppHistoryService {

    public List<MobileAppHistory> getMobileAppHistoryByCustomer(Integer customerId);

    public List<MobileAppHistory> getMobileAppHistoryByDeviceUid(String uid);

    public void logActivity(Customer customer, UserAccount userAccount, MobileApplicationType appType, CustomerMobileDevice device, Integer activityTypeInt,
                     boolean isSuccessful, String result);

    public List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceId(Integer customerMobileDeviceId, Integer page, Integer pageSize);

    public List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceIdAndOrder(Integer customerMobileDeviceId, Integer page, Integer pageSize, String column,
                                                                               String order);
    
}
