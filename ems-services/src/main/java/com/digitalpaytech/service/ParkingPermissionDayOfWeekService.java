package com.digitalpaytech.service;

import java.util.Set;

import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;

public interface ParkingPermissionDayOfWeekService {
    public Set<ParkingPermissionDayOfWeek> findByParkingPermissionId(Integer parkingPermissionId);
    public void deleteByParkingPermissionId(Integer parkingPermissionId);
}
