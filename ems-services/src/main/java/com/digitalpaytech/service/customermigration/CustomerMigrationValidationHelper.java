package com.digitalpaytech.service.customermigration;

import com.digitalpaytech.domain.CustomerMigrationValidationStatus;

public interface CustomerMigrationValidationHelper {
    
    Boolean checkEMS6Connection(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkEMS6SlaveConnection(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkMigrationQueueStatus(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId,
        Boolean beforeStart);
    
    Boolean checkCardRetry(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkPayStationVersion(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkRestAccounts(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkPayStationCount(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkCouponCount(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkUnusedMerchantAccounts(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId,
        Boolean beforeStart);
    
    Boolean checkLocationsWithDash(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkLongRateNames(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkMerchantAccounts(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkReversals(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkCardTransactions(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
    
    Boolean checkLastSuccess(CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);

    Boolean checkEnforcementMode (CustomerMigrationValidationStatus validation, Integer irisCustomerId, Integer ems6CustomerId, Boolean beforeStart);
}
