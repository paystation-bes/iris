package com.digitalpaytech.service.customermigration;

public interface CustomerMigrationIRISService {
    Integer findIRISPaystationId(Integer ems6PayStationId);
    
    Integer findEMS6CustomerId(Integer customerId);
    
    Integer findIRISMerchantAccountId(Integer ems6MerchantAccountId);
    
}
