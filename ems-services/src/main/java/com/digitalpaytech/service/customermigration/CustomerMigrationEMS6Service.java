package com.digitalpaytech.service.customermigration;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;

@SuppressWarnings("PMD.TooManyMethods")
public interface CustomerMigrationEMS6Service {
    
    int REST_ACCOUNT_ACCOUNT_NAME = 0;
    int REST_ACCOUNT_TYPE_ID = 1;
    int REST_ACCOUNT_SECRET_KEY = 2;
    int REST_ACCOUNT_VIRTUAL_PS = 3;
    
    int MERCHANT_ACCOUNT_ID = 0;
    int MERCHANT_ACCOUNT_NAME = 1;
    int MERCHANT_ACCOUNT_REFERENCE_COUNTER = 2;
    
    void disableEMS6Customer(Integer customerId);
    
    List<Object[]> findUnlockedPaystationsByCustomer(Integer customerId);
    
    List<Object[]> findAllPaystationsByCustomer(Integer customerId);
    
    List<Object[]> findNAPaystations(Integer customerId);
    
    void lockPaystationById(Integer paystationId);
    
    void fixNAPaystationById(Integer paystationId);
    
    List<Object[]> findEnabledUserAccountsByCustomer(Integer customerId);
    
    void disableUserAccountById(Integer userAccountId);
    
    List<Object[]> findActiveAlertsByCustomer(Integer customerId);
    
    void disableAndDeleteAlertById(Integer alertId);
    
    boolean checkMigrationQueue(Date migrationWaitTime);
    
    List<Object[]> findMerchantAccounts(Integer customerId);
    
    void rollbackRecord(CustomerMigrationEMS6ModifiedRecord modifiedRecord, boolean addCustomer, boolean addPaystation);
    
    Object[] findCardRetryProperties(Integer customerId);
    
    void disableCardRetryByCustomerId(Integer customerId);
    
    Byte findURLRerouteIDByCustomerId(Integer customerId);
    
    void setPaystationURLRerouteIds(Integer customerId, Byte urlRerouteId);
    
    List<Date> findLatestCardRetryTransaction();
    
    boolean checkReversalsForCustomer(Integer customerId);
    
    boolean checkCardTransactionsForCustomer(Integer customerId);
    
    List<Object[]> findRestAccounts(Integer customerId);
    
    int findPayStationCount(Integer customerId);
    
    int findCouponCount(Integer customerId);
    
    boolean checkForSettingsEnforcementMode(final Integer customerId);
    
}
