package com.digitalpaytech.service.customermigration.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoEMS6;
import com.digitalpaytech.dao.EntityDaoEMS6Slave;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.MigrationReadyPayStationVersionService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.service.customermigration.CustomerMigrationEMS6Service;
import com.digitalpaytech.service.customermigration.CustomerMigrationIRISService;
import com.digitalpaytech.service.customermigration.CustomerMigrationValidationHelper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 *         
 */
@Service("customerMigrationValidationHelper")
@Transactional(propagation = Propagation.SUPPORTS)
//Instantiated objects are StringBuilder objects with log messages.
//Temporary class, for simplicity not refractured 
//Import count cannot be reduced without refracturing class
@SuppressWarnings({ "PMD.GodClass", "PMD.AvoidInstantiatingObjectsInLoops", "PMD.ExcessiveImports" })
public class CustomerMigrationValidationHelperImpl implements CustomerMigrationValidationHelper {
    
    private static final Logger LOGGER = Logger.getLogger(CustomerMigrationValidationHelperImpl.class);
    
    private static final String ERROR_REST_ACCOUNT_START = "VALIDATION ERROR:Rest Account in EMS6 with Account_Name: ";
    private static final String ERROR_MERCHANT_ACCOUNT_START = "VALIDATION ERROR:Merchant Account with Name: ";
    private static final String ERROR_CUSTOMER_WITH_BY_SETTING_ENFORCEMENT_MODE =
            "Paystation present in EMS6 with enforcement mode \"by setting\" for Customer: ";
    private static final String ERROR_MERCHANT_ACCOUNT_MORE_IN_IRIS =
            "VALIDATION ERROR:There are Merchant Accounts that don't exist in EMS6 for Customer: ";
    private static final String ERROR_DOES_NOT_EXIST = " doesn't exist in Iris for Customer: ";
    
    private static final String TEST_ACCESS_TO_EMS6_QUERY = "SELECT Id FROM Customer LIMIT 1";
    private static boolean isEms6Accessible;
    private static boolean isEms6SlaveAccessible;
    
    @Autowired
    private transient EntityDaoEMS6 entityDaoEMS6;
    @Autowired
    private transient EntityDaoEMS6Slave entityDaoEMS6Slave;
    @Autowired
    private transient PointOfSaleService pointOfSaleService;
    @Autowired
    private transient LocationService locationService;
    @Autowired
    private transient UnifiedRateService unifiedRateService;
    @Autowired
    private transient MigrationReadyPayStationVersionService migrationReadyPayStationVersionService;
    @Autowired
    private transient CustomerMigrationEMS6Service customerMigrationEMS6Service;
    @Autowired
    private transient CustomerMigrationIRISService customerMigrationIRISService;
    @Autowired
    private transient RestAccountService restAccountService;
    @Autowired
    private transient CouponService couponService;
    @Autowired
    private transient MerchantAccountService merchantAccountService;
    
    @Override
    public final Boolean checkEMS6Connection(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        
        try {
            final SQLQuery q = this.entityDaoEMS6.createSQLQuery(TEST_ACCESS_TO_EMS6_QUERY);
            q.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
            q.list();
        } catch (JDBCException sqle) {
            isEms6Accessible = false;
            LOGGER.error("VALIDATION ERROR:No access to EMS DB");
            return false;
        }
        isEms6Accessible = true;
        return true;
    }
    
    @Override
    public final Boolean checkEMS6SlaveConnection(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        
        try {
            final SQLQuery q = this.entityDaoEMS6Slave.createSQLQuery(TEST_ACCESS_TO_EMS6_QUERY);
            q.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
            q.list();
        } catch (JDBCException sqle) {
            isEms6SlaveAccessible = false;
            LOGGER.error("VALIDATION ERROR:No access to EMS Slave DB");
            return false;
        }
        isEms6SlaveAccessible = true;
        return true;
    }
    
    @Override
    public final Boolean checkMigrationQueueStatus(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6SlaveAccessible) {
            return false;
        }
        
        final Calendar migrationWaitTime = Calendar.getInstance();
        
        migrationWaitTime.add(Calendar.MINUTE, -StandardConstants.CONSTANT_5);
        
        if (this.customerMigrationEMS6Service.checkMigrationQueue(migrationWaitTime.getTime())) {
            return true;
        }
        LOGGER.error("VALIDATION ERROR:Incremental Migration is lagging.");
        
        return false;
    }
    
    @Override
    public final Boolean checkCardRetry(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        final List<Date> latestCardRetryDateList = this.customerMigrationEMS6Service.findLatestCardRetryTransaction();
        if (latestCardRetryDateList == null || latestCardRetryDateList.isEmpty()) {
            return true;
        }
        
        final Date now = DateUtil.getCurrentGmtDate();
        final Date lastDate = latestCardRetryDateList.get(0);
        
        long timeDifference = now.getTime() - lastDate.getTime();
        
        if (StandardConstants.MINUTES_IN_MILLIS_2 > timeDifference) {
            
            final Date firstDate = latestCardRetryDateList.get(latestCardRetryDateList.size() - 1);
            timeDifference = lastDate.getTime() - firstDate.getTime();
            
            if (StandardConstants.MINUTES_IN_MILLIS_2 > timeDifference) {
                LOGGER.warn("VALIDATION WARNING:Nightly card retry is running don't start a migration now.");
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final Boolean checkPayStationVersion(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        
        final boolean result =
                this.pointOfSaleService.isPointOfSalesReadyForMigration(validation.getCustomerMigration().getCustomer(),
                                                                        this.migrationReadyPayStationVersionService.getVersionSet(), beforeStart);
                                                                        
        if (!result) {
            LOGGER.error(new StringBuilder("VALIDATION ERROR:Pay stations don't have correct versions for Customer: ").append(irisCustomerId)
                    .toString());
        }
        
        return result;
    }
    
    @Override
    public final Boolean checkRestAccounts(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        boolean result = true;
        final List<Object[]> restAccountList = this.customerMigrationEMS6Service.findRestAccounts(ems6CustomerId);
        if (restAccountList == null || restAccountList.isEmpty()) {
            return result;
        } else {
            for (Object[] ems6RestAccount : restAccountList) {
                final RestAccount restAccount = this.restAccountService
                        .findRestAccountByAccountName((String) ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_ACCOUNT_NAME]);
                if (restAccount == null) {
                    LOGGER.error(new StringBuilder(ERROR_REST_ACCOUNT_START)
                            .append(ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_ACCOUNT_NAME]).append(ERROR_DOES_NOT_EXIST)
                            .append(irisCustomerId).toString());
                    result = false;
                } else {
                    if (!restAccount.getSecretKey().equals((String) ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_SECRET_KEY])) {
                        LOGGER.error(new StringBuilder(ERROR_REST_ACCOUNT_START)
                                .append(ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_ACCOUNT_NAME])
                                .append(" has different SecretKey in Iris for Customer: ").append(irisCustomerId).toString());
                        result = false;
                    }
                    if (restAccount.getRestAccountType().getId() != ((Integer) ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_TYPE_ID])
                            .intValue()) {
                        LOGGER.error(new StringBuilder(ERROR_REST_ACCOUNT_START)
                                .append(ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_ACCOUNT_NAME])
                                .append(" has different TypeId in Iris for Customer: ").append(irisCustomerId).toString());
                        result = false;
                    }
                    
                    final PointOfSale virtualPos = this.pointOfSaleService.findPointOfSaleById(restAccount.getPointOfSale().getId());
                    if (!virtualPos.getSerialNumber().equals((String) ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_VIRTUAL_PS])) {
                        LOGGER.error(new StringBuilder(ERROR_REST_ACCOUNT_START)
                                .append(ems6RestAccount[CustomerMigrationEMS6Service.REST_ACCOUNT_ACCOUNT_NAME])
                                .append(" has different VirtualPS in Iris for Customer: ").append(irisCustomerId).toString());
                        result = false;
                    }
                }
            }
        }
        return result;
    }
    
    @Override
    public final Boolean checkPayStationCount(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        final int ems6Count = this.customerMigrationEMS6Service.findPayStationCount(ems6CustomerId);
        final int irisCount = this.pointOfSaleService.findPayStationCountByCustomerId(validation.getCustomerMigration().getCustomer().getId());
        final boolean result = ems6Count == irisCount;
        if (!result) {
            LOGGER.error(new StringBuilder("VALIDATION ERROR:Pay station counts don't match for Customer: ").append(irisCustomerId).toString());
        }
        return result;
    }
    
    @Override
    public final Boolean checkCouponCount(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        final int ems6Count = this.customerMigrationEMS6Service.findCouponCount(ems6CustomerId);
        final int irisCount = this.couponService.findCouponCountByCustomerId(validation.getCustomerMigration().getCustomer().getId());
        final boolean result = ems6Count == irisCount;
        if (!result) {
            LOGGER.error(new StringBuilder("VALIDATION ERROR:Coupon counts don't match for Customer: ").append(irisCustomerId).toString());
        }
        return result;
    }
    
    @Override
    public final Boolean checkUnusedMerchantAccounts(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        final List<Object[]> merchantAccountList = this.customerMigrationEMS6Service.findMerchantAccounts(ems6CustomerId);
        if (merchantAccountList == null || merchantAccountList.isEmpty()) {
            final List<MerchantAccount> irisMerchantAccountList = this.merchantAccountService.findAllMerchantAccountsByCustomerId(irisCustomerId);
            if (irisMerchantAccountList != null && !irisMerchantAccountList.isEmpty()) {
                LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_MORE_IN_IRIS).append(irisCustomerId).toString());
                return false;
            }
        }
        
        return checkMerchantAccountList(merchantAccountList, irisCustomerId, false);
    }
    
    @Override
    public final Boolean checkLocationsWithDash(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        boolean result = true;
        final List<Location> locationList = this.locationService.findLocationsByCustomerId(irisCustomerId);
        for (Location location : locationList) {
            if (location.getName().indexOf(StandardConstants.STRING_DASH) > -1) {
                LOGGER.error(new StringBuilder("VALIDATION WARNING:Location: ").append(location.getName()).append(" contains '-' for Customer: ")
                        .append(irisCustomerId).toString());
                result = false;
            }
        }
        
        return result;
    }
    
    @Override
    public final Boolean checkLongRateNames(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        boolean result = true;
        final List<UnifiedRate> unifiedRateList = this.unifiedRateService.findRatesByCustomerId(irisCustomerId);
        for (UnifiedRate unifiedRate : unifiedRateList) {
            if (unifiedRate.getName().trim().length() == StandardConstants.LIMIT_20) {
                LOGGER.error(new StringBuilder("VALIDATION WARNING:Unified Rate: ").append(unifiedRate.getName())
                        .append(" may have a name longer than 20 chars for Customer: ").append(irisCustomerId).toString());
                result = false;
            }
        }
        
        return result;
    }
    
    @Override
    public final Boolean checkMerchantAccounts(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        if (!isEms6Accessible) {
            return false;
        }
        
        final List<Object[]> merchantAccountList = this.customerMigrationEMS6Service.findMerchantAccounts(ems6CustomerId);
        if (merchantAccountList == null || merchantAccountList.isEmpty()) {
            final List<MerchantAccount> irisMerchantAccountList = this.merchantAccountService.findAllMerchantAccountsByCustomerId(irisCustomerId);
            if (irisMerchantAccountList != null && !irisMerchantAccountList.isEmpty()) {
                LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_MORE_IN_IRIS).append(irisCustomerId).toString());
                return false;
            }
        }
        return checkMerchantAccountList(merchantAccountList, irisCustomerId, true);
    }
    
    @Override
    public final Boolean checkReversals(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        
        if (!isEms6Accessible) {
            return false;
        }
        
        if (!this.customerMigrationEMS6Service.checkReversalsForCustomer(ems6CustomerId)) {
            LOGGER.error(new StringBuilder("VALIDATION ERROR:There are unprocessed reversals for Customer : ").append(irisCustomerId).toString());
            return false;
        }
        return true;
    }
    
    @Override
    public final Boolean checkCardTransactions(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        
        if (!isEms6Accessible) {
            return false;
        }
        
        if (!this.customerMigrationEMS6Service.checkCardTransactionsForCustomer(ems6CustomerId)) {
            LOGGER.info(new StringBuilder("VALIDATION ERROR:There are unprocessed card transactions for Customer : ").append(irisCustomerId)
                    .toString());
            return false;
        }
        return true;
    }
    
    @Override
    public final Boolean checkLastSuccess(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId,
        final Integer ems6CustomerId, final Boolean beforeStart) {
        final Date lastSuccessGmt = validation.getLastSuccessGmt();
        
        if (lastSuccessGmt != null && DateUtil.getCurrentGmtDate().getTime() - lastSuccessGmt.getTime() < StandardConstants.MINUTES_IN_MILLIS_2) {
            return true;
        }
        return false;
    }
    
    private boolean checkMerchantAccountList(final List<Object[]> merchantAccountList, final Integer irisCustomerId,
        final Boolean isDuringMigration) {
        boolean result = true;
        final Date oneDayAgo = new Date(DateUtil.getCurrentGmtDate().getTime() - StandardConstants.DAYS_IN_MILLIS_1);
        for (Object[] merchantAccount : merchantAccountList) {
            final Integer ems7MerchantAccountId = this.customerMigrationIRISService
                    .findIRISMerchantAccountId((Integer) merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_ID]);
            if (ems7MerchantAccountId == null) {
                LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_START)
                        .append(merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_NAME]).append(ERROR_DOES_NOT_EXIST)
                        .append(irisCustomerId).toString());
                result = false;
            } else {
                final MerchantAccount ems7MerchantAccount = this.merchantAccountService.findById(ems7MerchantAccountId);
                if (ems7MerchantAccount == null) {
                    LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_START)
                            .append(merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_NAME]).append(ERROR_DOES_NOT_EXIST)
                            .append(irisCustomerId).toString());
                    result = false;
                } else {
                    final boolean compareCount =
                            (long) merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_REFERENCE_COUNTER] == ems7MerchantAccount
                                    .getReferenceCounter();
                    if (isDuringMigration && !compareCount) {
                        LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_START)
                                .append(merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_NAME])
                                .append(" reference counts do not match for CustomerId: ").append(irisCustomerId).toString());
                        result = false;
                    } else if (!isDuringMigration && !compareCount && ems7MerchantAccount.getLastModifiedGmt().before(oneDayAgo)) {
                        LOGGER.error(new StringBuilder(ERROR_MERCHANT_ACCOUNT_START)
                                .append(merchantAccount[CustomerMigrationEMS6Service.MERCHANT_ACCOUNT_NAME])
                                .append(" reference counts do not match and not used for CustomerId: ").append(irisCustomerId).toString());
                        result = false;
                    }
                }
            }
        }
        return result;
    }
    
    @Override
    public final Boolean checkEnforcementMode(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId, final Integer ems6CustomerId,
        final Boolean beforeStart) {
        
        if (!isEms6Accessible || !this.customerMigrationEMS6Service.checkForSettingsEnforcementMode(ems6CustomerId)) {
            LOGGER.info(new StringBuilder(ERROR_CUSTOMER_WITH_BY_SETTING_ENFORCEMENT_MODE).append(irisCustomerId).toString());
            return false;
        }
        
        return true;
        
    }
}
