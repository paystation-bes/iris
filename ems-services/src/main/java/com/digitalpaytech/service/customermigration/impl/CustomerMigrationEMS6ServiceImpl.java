package com.digitalpaytech.service.customermigration.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.SQLQuery;
import org.hibernate.type.BooleanType;
import org.hibernate.type.ByteType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoEMS6;
import com.digitalpaytech.dao.EntityDaoEMS6Slave;
import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.customermigration.CustomerMigrationEMS6Service;

@Service("customerMigrationEMS6Service")
@Transactional(propagation = Propagation.SUPPORTS)
//Temporary class for simplicity and all EMS6 related methods are kept here.
@SuppressWarnings({ "PMD.GodClass", "PMD.TooManyMethods" })
public class CustomerMigrationEMS6ServiceImpl implements CustomerMigrationEMS6Service {
    
    private static final String EMS6_WHERE_CUSTOMER_ID = "WHERE CustomerId = :customerId";
    private static final String DISABLE_EMS6_CUSTOMER_BY_ID_QUERY = "UPDATE Customer SET AccountStatus = 'DISABLED' WHERE Id = :customerId";
    private static final String FIND_EMS6_UNLOCKED_PAYSTATION_QUERY =
            "SELECT Id, CommAddress FROM Paystation WHERE LockState = 'UNLOCKED'" + " AND CustomerId = :customerId ";
    private static final String FIND_EMS6_ALL_PAYSTATION_QUERY = "SELECT Id, CommAddress FROM Paystation WHERE CustomerId = :customerId ";
    private static final String FIND_EMS6_ALL_PAYSTATION_COUNT_QUERY = "SELECT COUNT(Id) AS Count FROM Paystation WHERE CustomerId = :customerId ";
    private static final String FIND_EMS6_NA_PAYSTATION_QUERY =
            "SELECT ps.Id, ps.CommAddress, ss.SecondaryVersion FROM ServiceState ss " + "INNER JOIN Paystation ps ON (ps.Id = ss.PaystationId) "
                                                                + EMS6_WHERE_CUSTOMER_ID + " AND ss.SecondaryVersion = 'n/a' ";
    private static final String LOCK_PAYSTATION_BY_ID_QUERY = "UPDATE Paystation SET LockState = 'LOCKED' WHERE Id = :paystationId";
    private static final String FIX_NA_PAYSTATION_BY_ID_QUERY =
            "UPDATE ServiceState SET SecondaryVersion = 'offline' WHERE PaystationId = :paystationId";
    private static final String FIND_EMS6_ENABLED_USERACCOUNT_QUERY =
            "SELECT Id, Name FROM UserAccount " + "WHERE AccountStatus = 'ENABLED' AND CustomerId = :customerId ";
    private static final String DISABLE_USERACCOUNT_BY_ID_QUERY = "UPDATE UserAccount SET AccountStatus = 'DISABLED' WHERE Id = :userAccountId";
    private static final String FIND_EMS6_ENABLED_ALERT_QUERY =
            "SELECT Id, AlertName, IsEnabled, IsDeleted FROM Alert " + EMS6_WHERE_CUSTOMER_ID + " AND (IsEnabled = 1 OR IsDeleted = 0)";
    private static final String DISABLE_AND_DELETE_ALERT_BY_ID_QUERY = "UPDATE Alert SET IsEnabled = 0, IsDeleted = 1 WHERE Id = :alertId";
    private static final String FIND_EMS6_CARD_RETRY_PROPERTIES_QUERY =
            "SELECT CustomerId, CCOfflineRetry, MaxOfflineRetry FROM CustomerProperties " + EMS6_WHERE_CUSTOMER_ID;
    private static final String DISABLE_CARD_RETRY_BY_CUSTOMERID_QUERY =
            "UPDATE CustomerProperties SET CCOfflineRetry = 0, MaxOfflineRetry = 0 " + EMS6_WHERE_CUSTOMER_ID;
            
    private static final String CHECK_MIGRATION_QUEUE_SELECT_QUERY = "SELECT LowerLimit, UpperLimit, MaxId FROM MigrationQueueLimit ";
    private static final String CHECK_MIGRATION_QUEUE_TRANSACTION_DATA_QUERY =
            CHECK_MIGRATION_QUEUE_SELECT_QUERY + "WHERE ModuleName = 'TransactionData' ORDER BY Id DESC LIMIT 1";
    private static final String CHECK_MIGRATION_QUEUE_MASTER_DATA_QUERY =
            CHECK_MIGRATION_QUEUE_SELECT_QUERY + "WHERE ModuleName = 'MasterData' ORDER BY Id DESC LIMIT 1";
    private static final String CHECK_MIGRATION_QUEUE_SENSOR_DATA_QUERY =
            CHECK_MIGRATION_QUEUE_SELECT_QUERY + "WHERE ModuleName = 'SensorData' ORDER BY Id DESC LIMIT 1";
    private static final String CHECK_MIGRATION_QUEUE_CARD_RETRY_QUERY =
            CHECK_MIGRATION_QUEUE_SELECT_QUERY + "WHERE ModuleName = 'CardRetry' ORDER BY Id DESC LIMIT 1";
            
    private static final String CHECK_LAST_MIGRATED_DATA_DATE_QUERY = "SELECT CreatedDate FROM MigrationQueue WHERE Id = :id";
    
    private static final String FIND_EMS6_MERCHANT_ACCOUNT_QUERY =
            "SELECT Id, Name, ReferenceCounter FROM MerchantAccount " + "WHERE CustomerId = :customerId AND IsDeleted = 0";
            
    private static final String FIND_URLREROUTE_ID_BY_CUSTOMERID_QUERY =
            "SELECT EMSPaystationURLRerouteId FROM CustomerURLRErouteMapping " + EMS6_WHERE_CUSTOMER_ID;
    private static final String SET_PAYSTATION_URLREROUTEID_QUERY =
            "UPDATE Paystation SET EMSPaystationURLRerouteId = :urlRerouteId " + EMS6_WHERE_CUSTOMER_ID;
            
    private static final String VALIDATE_CARDRETRY_QUERY = "SELECT LastRetryDate FROM CardRetryTransaction ORDER BY LastRetryDate DESC LIMIT 12";
    private static final String VALIDATE_REVERSAL_QUERY =
            "SELECT r.LastRetryTime FROM Reversal r WHERE r.Succeeded = 0 " + "AND r.MerchantAccountId IN(SELECT ma.Id from MerchantAccount ma "
                                                          + "WHERE ma.CustomerId = :customerId) LIMIT 1";
    private static final String VALIDATE_CARDTRANSACTION_QUERY = "SELECT pt.ProcessingDate from ProcessorTransaction pt WHERE pt.TypeId = 1 "
                                                                 + "AND pt.Approved = 0 AND pt.MerchantAccountId IN(SELECT ma.Id "
                                                                 + "FROM MerchantAccount ma WHERE ma.CustomerId = :customerId) LIMIT 1";
                                                                 
    private static final String FIND_EMS6_RESTACCOUNTS_QUERY =
            "SELECT AccountName, TypeId, SecretKey, VirtualPS FROM RESTAccount " + EMS6_WHERE_CUSTOMER_ID;
    private static final String FIND_EMS6_ALL_COUPON_COUNT_QUERY = "SELECT COUNT(Id) AS Count FROM Coupon WHERE CustomerId = :customerId ";
    private static final String GET_PAYSTATIONS_WITH_ENFORECEMENT_MODE_BY_SETTINGS =
            "SELECT COUNT(Id) as Count FROM Paystation where CustomerId = :customerId and QueryStallsBy = 0";
            
    @Autowired
    private EntityDaoEMS6 entityDaoEMS6;
    
    @Autowired
    private EntityDaoEMS6Slave entityDaoEMS6Slave;
    
    public final void setEntityDaoEMS6(final EntityDaoEMS6 entityDaoEMS6) {
        this.entityDaoEMS6 = entityDaoEMS6;
    }
    
    public final void setEntityDaoEMS6Slave(final EntityDaoEMS6Slave entityDaoEMS6Slave) {
        this.entityDaoEMS6Slave = entityDaoEMS6Slave;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void disableEMS6Customer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(DISABLE_EMS6_CUSTOMER_BY_ID_QUERY);
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        query.executeUpdate();
    }
    
    @Override
    public final List<Object[]> findUnlockedPaystationsByCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_UNLOCKED_PAYSTATION_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_COMM_ADDRESS, new StringType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    public final List<Object[]> findAllPaystationsByCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_ALL_PAYSTATION_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_COMM_ADDRESS, new StringType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    public final List<Object[]> findNAPaystations(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_NA_PAYSTATION_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_COMM_ADDRESS, new StringType());
        query.addScalar("SecondaryVersion", new StringType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void lockPaystationById(final Integer paystationId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(LOCK_PAYSTATION_BY_ID_QUERY);
        query.setParameter(HibernateConstants.PAYSTATION_ID, paystationId);
        query.executeUpdate();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void fixNAPaystationById(final Integer paystationId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIX_NA_PAYSTATION_BY_ID_QUERY);
        query.setParameter(HibernateConstants.PAYSTATION_ID, paystationId);
        query.executeUpdate();
    }
    
    @Override
    public final List<Object[]> findEnabledUserAccountsByCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_ENABLED_USERACCOUNT_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_NAME, new StringType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void disableUserAccountById(final Integer userAccountId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(DISABLE_USERACCOUNT_BY_ID_QUERY);
        query.setParameter(HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        query.executeUpdate();
    }
    
    @Override
    public final List<Object[]> findActiveAlertsByCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_ENABLED_ALERT_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar("AlertName", new StringType());
        query.addScalar("IsEnabled", new BooleanType());
        query.addScalar("IsDeleted", new BooleanType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void disableAndDeleteAlertById(final Integer alertId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(DISABLE_AND_DELETE_ALERT_BY_ID_QUERY);
        query.setParameter("alertId", alertId);
        query.executeUpdate();
    }
    
    @Override
    //Temporary class for simplicity and maintainability method is not refactured
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity" })
    public final boolean checkMigrationQueue(final Date migrationWaitTime) {
        
        SQLQuery query = this.entityDaoEMS6Slave.createSQLQuery(CHECK_MIGRATION_QUEUE_TRANSACTION_DATA_QUERY);
        query.addScalar(HibernateConstants.COLUMN_LOWER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_UPPER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MAX_ID, new LongType());
        final List<Object[]> transactionDataList = query.list();
        Object[] transactionData = null;
        if (transactionDataList == null || transactionDataList.isEmpty()) {
            return false;
        } else {
            transactionData = transactionDataList.get(0);
        }
        
        query = this.entityDaoEMS6Slave.createSQLQuery(CHECK_MIGRATION_QUEUE_MASTER_DATA_QUERY);
        query.addScalar(HibernateConstants.COLUMN_LOWER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_UPPER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MAX_ID, new LongType());
        final List<Object[]> masterDataList = query.list();
        Object[] masterData = null;
        if (masterDataList == null || masterDataList.isEmpty()) {
            return false;
        } else {
            masterData = masterDataList.get(0);
        }
        
        query = this.entityDaoEMS6Slave.createSQLQuery(CHECK_MIGRATION_QUEUE_SENSOR_DATA_QUERY);
        query.addScalar(HibernateConstants.COLUMN_LOWER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_UPPER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MAX_ID, new LongType());
        final List<Object[]> sensorDataList = query.list();
        Object[] sensorData = null;
        if (sensorDataList == null || sensorDataList.isEmpty()) {
            return false;
        } else {
            sensorData = sensorDataList.get(0);
        }
        
        query = this.entityDaoEMS6Slave.createSQLQuery(CHECK_MIGRATION_QUEUE_CARD_RETRY_QUERY);
        query.addScalar(HibernateConstants.COLUMN_LOWER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_UPPER_LIMIT, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MAX_ID, new LongType());
        final List<Object[]> cardRetryList = query.list();
        Object[] cardRetry = null;
        if (cardRetryList == null || cardRetryList.isEmpty()) {
            return false;
        } else {
            cardRetry = cardRetryList.get(0);
        }
        
        final long transId =
                ((Long) transactionData[0]).longValue() == ((Long) transactionData[1]).longValue() ? Long.MAX_VALUE : ((Long) transactionData[0]);
        final long masterId = ((Long) masterData[0]).longValue() == ((Long) masterData[1]).longValue() ? Long.MAX_VALUE : ((Long) masterData[0]);
        final long sensorId = ((Long) sensorData[0]).longValue() == ((Long) sensorData[1]).longValue() ? Long.MAX_VALUE : ((Long) sensorData[0]);
        final long cardRetryId = ((Long) cardRetry[0]).longValue() == ((Long) cardRetry[1]).longValue() ? Long.MAX_VALUE : ((Long) cardRetry[0]);
        
        final long minId = Math.min(transId, Math.min(masterId, Math.min(sensorId, cardRetryId)));
        
        if (minId != Long.MAX_VALUE) {
            query = this.entityDaoEMS6Slave.createSQLQuery(CHECK_LAST_MIGRATED_DATA_DATE_QUERY);
            query.addScalar("CreatedDate", new TimestampType());
            query.setParameter(HibernateConstants.ID, minId);
            final List<Date> latestMigrationList = query.list();
            
            if (latestMigrationList == null || latestMigrationList.isEmpty()) {
                return false;
            }
            
            //Date is in localtime should be changed to UTC
            final Calendar latestMigrationCal = Calendar.getInstance();
            latestMigrationCal.setTime(latestMigrationList.get(0));
            latestMigrationCal.add(Calendar.MILLISECOND, -TimeZone.getTimeZone("Canada/Pacific").getOffset(latestMigrationCal.getTimeInMillis()));
            
            if (migrationWaitTime.after(latestMigrationCal.getTime())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final List<Object[]> findMerchantAccounts(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_MERCHANT_ACCOUNT_QUERY);
        query.addScalar(HibernateConstants.COLUMN_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_NAME, new StringType());
        query.addScalar("ReferenceCounter", new LongType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        return query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void rollbackRecord(final CustomerMigrationEMS6ModifiedRecord modifiedRecord, final boolean addCustomer,
        final boolean addPaystation) {
        
        final StringBuilder queryString = new StringBuilder("UPDATE ").append(modifiedRecord.getTableName()).append(" SET ")
                .append(modifiedRecord.getColumnName()).append(" = ").append(modifiedRecord.getValue()).append(" WHERE ");
        if (addCustomer) {
            queryString.append("Customer");
        }
        if (addPaystation) {
            queryString.append("Paystation");
        }
        queryString.append("Id = ").append(modifiedRecord.getTableId());
        
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(queryString.toString());
        query.executeUpdate();
        
    }
    
    @Override
    public final Object[] findCardRetryProperties(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_CARD_RETRY_PROPERTIES_QUERY);
        query.addScalar("CustomerId", new IntegerType());
        query.addScalar("CCOfflineRetry", new ShortType());
        query.addScalar("MaxOfflineRetry", new ShortType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Object[]> idList = query.list();
        
        if (idList == null || idList.isEmpty() || idList.size() != 1) {
            return null;
        } else {
            return idList.get(0);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void disableCardRetryByCustomerId(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(DISABLE_CARD_RETRY_BY_CUSTOMERID_QUERY);
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        query.executeUpdate();
    }
    
    @Override
    public final Byte findURLRerouteIDByCustomerId(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_URLREROUTE_ID_BY_CUSTOMERID_QUERY);
        query.addScalar("EMSPaystationURLRerouteId", new ByteType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Byte> idList = query.list();
        
        if (idList == null || idList.isEmpty() || idList.size() != 1) {
            
            return null;
        } else {
            return idList.get(0);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void setPaystationURLRerouteIds(final Integer customerId, final Byte urlRerouteId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(SET_PAYSTATION_URLREROUTEID_QUERY);
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        query.setParameter("urlRerouteId", urlRerouteId);
        query.executeUpdate();
    }
    
    @Override
    public final List<Date> findLatestCardRetryTransaction() {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(VALIDATE_CARDRETRY_QUERY);
        query.addScalar("LastRetryDate", new TimestampType());
        return query.list();
    }
    
    @Override
    public final boolean checkReversalsForCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(VALIDATE_REVERSAL_QUERY);
        query.addScalar("LastRetryTime", new TimestampType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Date> dateList = query.list();
        return dateList == null || dateList.isEmpty();
    }
    
    @Override
    public final boolean checkCardTransactionsForCustomer(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(VALIDATE_CARDTRANSACTION_QUERY);
        query.addScalar("ProcessingDate", new TimestampType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Date> dateList = query.list();
        return dateList == null || dateList.isEmpty();
    }
    
    public final EntityDaoEMS6 getEntityDaoEMS6() {
        return this.entityDaoEMS6;
    }
    
    public final EntityDaoEMS6Slave getEntityDaoEMS6Slave() {
        return this.entityDaoEMS6Slave;
    }
    
    @Override
    public final List<Object[]> findRestAccounts(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_RESTACCOUNTS_QUERY);
        query.addScalar("AccountName", new StringType());
        query.addScalar("TypeId", new IntegerType());
        query.addScalar("SecretKey", new StringType());
        query.addScalar("VirtualPS", new StringType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Object[]> idList = query.list();
        
        return idList;
    }
    
    @Override
    public final int findPayStationCount(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_ALL_PAYSTATION_COUNT_QUERY);
        query.addScalar(HibernateConstants.COLUMN_COUNT, new IntegerType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Integer> countList = query.list();
        
        if (countList == null || countList.isEmpty() || countList.size() != 1) {
            return 0;
        } else {
            return countList.get(0);
        }
    }
    
    @Override
    public final int findCouponCount(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(FIND_EMS6_ALL_COUPON_COUNT_QUERY);
        query.addScalar(HibernateConstants.COLUMN_COUNT, new IntegerType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Integer> countList = query.list();
        
        if (countList == null || countList.isEmpty() || countList.size() != 1) {
            return 0;
        } else {
            return countList.get(0);
        }
    }
    
    public final boolean checkForSettingsEnforcementMode(final Integer customerId) {
        final SQLQuery query = this.entityDaoEMS6.createSQLQuery(GET_PAYSTATIONS_WITH_ENFORECEMENT_MODE_BY_SETTINGS);
        query.addScalar(HibernateConstants.COLUMN_COUNT, new IntegerType());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        final List<Integer> countList = query.list();
        if (countList == null || countList.isEmpty() || countList.get(0) > 0) {
            return false;
        } else {
            return true;
        }    
    }
}
