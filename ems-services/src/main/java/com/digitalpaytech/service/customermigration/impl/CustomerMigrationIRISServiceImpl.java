package com.digitalpaytech.service.customermigration.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoIRIS;
import com.digitalpaytech.service.customermigration.CustomerMigrationIRISService;

@Service("customerMigrationIRISService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerMigrationIRISServiceImpl implements CustomerMigrationIRISService {
    
    private static final String FIND_EMS6_CUSTOMER_ID_QUERY = "SELECT EMS6CustomerId FROM CustomerMapping WHERE EMS7CustomerId = :customerId";
    private static final String FIND_IRIS_MERCHANT_ACCOUNT_ID_QUERY = "SELECT EMS7MerchantAccountId FROM MerchantAccountMapping "
                                                                      + "WHERE EMS6MerchantAccountId = :ems6MerchantAccountId";
    private static final String FIND_IRIS_PAYSTATION_ID_QUERY = "SELECT EMS7PaystationId FROM PaystationMapping WHERE EMS6PaystationId = :paystationId";
    
    @Autowired
    private EntityDaoIRIS entityDaoIRIS;
    
    public final EntityDaoIRIS getEntityDaoIRIS() {
        return this.entityDaoIRIS;
    }
    
    public final void setEntityDaoIRIS(final EntityDaoIRIS entityDaoIRIS) {
        this.entityDaoIRIS = entityDaoIRIS;
    }
    
    @Override
    public final Integer findIRISPaystationId(final Integer ems6PayStationId) {
        
        final SQLQuery q = this.entityDaoIRIS.createSQLQuery(FIND_IRIS_PAYSTATION_ID_QUERY);
        q.addScalar("EMS7PaystationId", new IntegerType());
        q.setParameter("paystationId", ems6PayStationId);
        final List<Integer> idList = q.list();
        
        if (idList == null || idList.isEmpty() || idList.size() != 1) {
            return null;
        } else {
            return idList.get(0);
        }
        
    }
    
    @Override
    public final Integer findEMS6CustomerId(final Integer customerId) {
        
        final SQLQuery q = this.entityDaoIRIS.createSQLQuery(FIND_EMS6_CUSTOMER_ID_QUERY);
        q.addScalar("EMS6CustomerId", new IntegerType());
        q.setParameter("customerId", customerId);
        final List<Integer> idList = q.list();
        
        if (idList == null || idList.isEmpty() || idList.size() != 1) {
            return null;
        } else {
            return idList.get(0);
        }
        
    }
    
    @Override
    public final Integer findIRISMerchantAccountId(final Integer ems6MerchantAccountId) {
        final SQLQuery q = this.entityDaoIRIS.createSQLQuery(FIND_IRIS_MERCHANT_ACCOUNT_ID_QUERY);
        q.addScalar("EMS7MerchantAccountId", new IntegerType());
        q.setParameter("ems6MerchantAccountId", ems6MerchantAccountId);
        final List<Integer> idList = q.list();
        
        if (idList == null || idList.isEmpty() || idList.size() != 1) {
            return null;
        } else {
            return idList.get(0);
        }
    }
    
}
