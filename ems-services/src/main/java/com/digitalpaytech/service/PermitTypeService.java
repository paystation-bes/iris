package com.digitalpaytech.service;

import com.digitalpaytech.domain.PermitType;

public interface PermitTypeService {
    public PermitType findPermitType(String name);
    public PermitType findPermitTypeById(Integer id);
}
