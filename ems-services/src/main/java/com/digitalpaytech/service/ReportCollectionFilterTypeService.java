package com.digitalpaytech.service;

import com.digitalpaytech.domain.ReportCollectionFilterType;

public interface ReportCollectionFilterTypeService {
    public ReportCollectionFilterType getReportCollectionFilterType(short reportCollectionFilterTypeId);
}
