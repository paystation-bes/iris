package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.util.RandomKeyMapping;

public interface ReportHistoryService {
    
    public void saveReportHistory(ReportHistory reportHistory, ReportQueue reportQueue, byte[] fileContent, String fileName, boolean isWithEmail);
    
    public void updateReportHistory(ReportHistory reportHistory);
    
    public List<ReportHistory> findFailedAndCancelledReportsByUserAccountId(int userAccountId);

    public List<ReportHistory> findFailedAndCancelledReportsByCustomerId(int customerId);

    public List<ReportHistory> findFinishedReportsByUserAccountId(int userAccountId);
    
    public ReportHistory getReportHistory(int id);
    
    public void deleteReportHistory(int id);
    
    public int getCompletedReports();
    
    public int getCancelledReports();
    
    public int getFailedReports();

    public float getReportResponseTime();
    
    public ReportHistory findRecentlyExecutedReport(int reportDefinitionId, Date minExecutionEndGmt);
    
    public ReportHistory findRecentlyExecutedReport(AdhocReportInfo adhocReportInfo);

    public List<ReportHistory> findFailedAndCancelledReportsByCustomerIdAndUserAccountId(int userAccountId, int customerId);
    
    public List<QueueInfo> findFailedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone);
}
