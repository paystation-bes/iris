package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.UserAccount;

public interface RoleService {
    
    void saveRoleAndRolePermission(Role role);
    
    void updateRoleAndRolePermission(Role role);
    
    Role findRoleByRoleIdAndEvict(Integer roleId);
    
    void updateRole(Role role);
    
    void deleteRole(Role role);
    
    Role findRoleByName(Integer customerId, String roleName);
    
    List<UserAccount> findUserAccountByRoleId(Integer roleId);
    
    void saveCustomerRoles(Set<CustomerRole> customerRoles);
    
    Map<Integer, Boolean> findNumberOfUserRolesPerRoleIdByCustomerId(final Integer customerId);
    
}
