package com.digitalpaytech.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.util.RandomKeyMapping;

public interface CustomerService {
    
    Customer findCustomer(Integer id);
    
    Customer findCustomerDetached(Integer id);
    
    Customer findCustomerNoCache(Integer id);
    
    Serializable saveCustomerAgreement(CustomerAgreement customerAgreement);
    
    Collection<Customer> findAllParentAndChildCustomers();
    
    Collection<Customer> findAllParentCustomers();
    
    Collection<Customer> findAllChildCustomers();
    
    Collection<Customer> findAllParentAndChildCustomersBySearchKeyword(String keyword, Integer... customerTypeIds);
    
    boolean update(Customer customer);
    
    LocationTree getCustomerTree(int customerId, RandomKeyMapping keyMapping);
    
    List<Customer> findAllChildCustomers(int parentCustomerId);
    
    LocationTree getChildCustomerTree(RandomKeyMapping keyMapping);
    
    CustomerURLReroute findURLRerouteByCustomerId(Integer customerId);
    
    List<CustomerSubscription> findCustomerSubscriptions(Integer customerId);
    
    List<Customer> findCustomerBySubscriptionTypeId(Integer subscriptionTypeId);
    
    Customer findCustomerByUnifiId(Integer unifiId);    
}
