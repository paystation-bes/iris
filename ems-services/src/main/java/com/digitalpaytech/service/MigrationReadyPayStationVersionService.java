package com.digitalpaytech.service;

import java.util.Set;

public interface MigrationReadyPayStationVersionService {
    public Set<String> getVersionSet();
}
