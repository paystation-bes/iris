package com.digitalpaytech.service;

public interface ElavonSemaphoreService {
    
    boolean bookConnection(String tidmid);
    void releaseConnection(String tidmid);
    boolean bookConnectionWithExpire(String tidmid, int milliseconds);
}
