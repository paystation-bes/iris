package com.digitalpaytech.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.digitalpaytech.domain.CustomerAgreement;

public interface CustomerAgreementService {

    public CustomerAgreement findCustomerAgreementByCustomerId(
            Integer customerId);

    public void sendServiceAgreementEmail(Integer customerId, String name,
            String title, String organization) throws AddressException,
            MessagingException;
}
