package com.digitalpaytech.service;

import java.util.Map;

import com.digitalpaytech.domain.EventException;

public interface EventExceptionService {
    Map<String, EventException> loadAll();
    
    boolean isException(Integer paystationTypeId, Integer eventDeviceTypeId);
}
