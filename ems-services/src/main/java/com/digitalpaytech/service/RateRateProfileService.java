package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.RateRateProfile;

public interface RateRateProfileService {
    
    List<RateRateProfile> findPublishedRatesByRateId(Integer rateId);
 
    List<RateRateProfile> findActiveRatesByRateProfileIdAndDateRange(Integer rateProfileId, Date startDate, Date endDate, boolean isSingleDay);
    
    List<RateRateProfile> findPublishedRatesByLocationIdAndDateRange(Integer locationId, Integer customerId, Date startDate, Date endDate);
    
    RateRateProfile findRateRateProfileById(Integer rateRateProfileId);
}
