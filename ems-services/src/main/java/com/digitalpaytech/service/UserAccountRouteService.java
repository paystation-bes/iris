package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.UserAccountRoute;

public interface UserAccountRouteService {

    public long getUserCountForRoute(int routeId);
    
    public long getUserCountForRouteAppIdAndCurrentSession(int routeId, int mobileApplicationTypeId, Date currentDate);

    void saveOrUpdate(UserAccountRoute userAccountRoute);

    void delete(UserAccountRoute userAccountRoute);

    List<UserAccountRoute> findUserAccountRouteByUserAccountId(int userAccountId);

    List<UserAccountRoute> findUserAccountRouteByRouteId(int routeId);

    List<UserAccountRoute> findUserAccountRouteByUserAccountIdAndRouteId(
            int userAccountId, int routeId);

    public UserAccountRoute findUserAccountRouteByUserAccountIdAndApplicationId(int userAccountId, int applicationTypeId);
}
