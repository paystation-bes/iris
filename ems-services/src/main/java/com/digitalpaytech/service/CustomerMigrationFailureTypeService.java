package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CustomerMigrationFailureType;

public interface CustomerMigrationFailureTypeService {
    List<CustomerMigrationFailureType> loadAll();
    
    Map<Byte, CustomerMigrationFailureType> getCustomerMigrationFailureTypesMap();
    
    String getText(byte customerMigrationFailureTypeId);
    
    CustomerMigrationFailureType findCustomerMigrationFailureType(byte customerMigrationFailureTypeId);
    
    CustomerMigrationFailureType findCustomerMigrationFailureType(String name);
}
