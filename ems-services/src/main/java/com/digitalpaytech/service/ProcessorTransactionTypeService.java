package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.ProcessorTransactionType;

public interface ProcessorTransactionTypeService {
    ProcessorTransactionType findProcessorTransactionType(Integer id);
    
    List<ProcessorTransactionType> loadAll();
    
}
