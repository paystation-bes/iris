package com.digitalpaytech.service;

import com.digitalpaytech.domain.PosMacAddress;

public interface PosMacAddressService {
    
    public PosMacAddress findPosMacAddressByPointOfSaleId(Integer pointOfSaleId);
    
    public PosMacAddress findPosMacAddressByMacAddress(String macAddress);
    
    public void saveOrUpdate(PosMacAddress posMacAddress, boolean isUpdate);
    
    public void delete(PosMacAddress posMacAddress);
}
