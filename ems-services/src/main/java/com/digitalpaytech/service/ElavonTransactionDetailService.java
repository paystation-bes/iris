package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;

import com.digitalpaytech.domain.ElavonTransactionDetail;

public interface ElavonTransactionDetailService {
    
    void saveElavonTransactionDetail(ElavonTransactionDetail elavonTransactionDetail);
    
    ElavonTransactionDetail findByPreAuthId(long preAuthId);
    
    ElavonTransactionDetail findByProcessorTransactionId(final long processorTransactionId);
    
    ElavonTransactionDetail findByTransactionReferenceNbr(int transactionReferenceNbr);
    
    ElavonTransactionDetail findByPurchasedDatePointOfSaleIdAndTicketNo(final Date purchasedDate, final int pointOfSaleId, final int ticketNumber);

    void updateForPreauthTransferToHolding(Collection<Long> preAuthIds);
}
