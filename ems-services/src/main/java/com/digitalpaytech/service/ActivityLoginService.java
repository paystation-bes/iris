package com.digitalpaytech.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.ActivityLogin;

public interface ActivityLoginService {
    
    Serializable saveActivityLogin(ActivityLogin activityLogin);
    
    int countSuccessfulLoginWithinLastMinute(Date endTime);
    
    List<ActivityLogin> findInvalidActivityLoginByLastModifiedGMT(String username, int maxLockTimeMinutes);
    
    int findCountInvalidActivityLoginByActivityGMT(Integer userAccountId, int loginResultTypeId, int maxLockTimeMinutes);
    
    Date findLastSuccessByUserAccountId(Integer userAccountId);
    
    List<ActivityLogin> findAllActivityForUsageCalculation(Integer customerId);

}
