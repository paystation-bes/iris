package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.CustomerMigrationStatusType;

public interface CustomerMigrationStatusTypeService {
    List<CustomerMigrationStatusType> loadAll();
    
    Map<Byte, CustomerMigrationStatusType> getCustomerMigrationStatusTypesMap();
    
    String getText(byte customerMigrationStatusTypeId);
    
    CustomerMigrationStatusType findCustomerMigrationStatusType(byte customerMigrationStatusTypeId);
    
    CustomerMigrationStatusType findCustomerMigrationStatusType(String name);
}
