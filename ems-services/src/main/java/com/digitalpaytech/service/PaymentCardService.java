package com.digitalpaytech.service;

import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.ProcessorTransaction;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public interface PaymentCardService {
    
    PaymentCard findPaymentCardByPurchaseIdAndProcessorTransactionId(Long purchaseId, Long processorTransactionId);
    
    PaymentCard findByPurchaseIdAndCardType(ProcessorTransaction processorTransaction, Integer cardTypeId);
    
    PaymentCard findByPurchaseIdAndCardType(ProcessorTransaction processorTransaction, Integer cardTypeId, boolean cacheable);
    
    PaymentCard findByPurchaseId(Long purchaseId);

    PaymentCard findLatestByCustomerCardId(Integer customerCardId);
    
    void saveRefundPaymentCard(ProcessorTransaction refund);

    void updateProcessorTransactionToBatchedSettled(ProcessorTransaction processorTransaction, Integer cardTypeId);
    
    void updateProcessorTransactionTypeToSettled(Long purchaseId, Long processorTransactionId);
    
    void savePaymentCard(PaymentCard paymentCard);
}
