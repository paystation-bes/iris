package com.digitalpaytech.service;

import com.digitalpaytech.domain.Transaction2Push;

public interface Transaction2PushService {
    public void saveTransaction2Push(Transaction2Push transaction2Push);
}
