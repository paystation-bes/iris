package com.digitalpaytech.service;

import java.util.List;

public interface CreditCardTestNumberService {
    
    String getDefaultTestCreditCardNumber();
    
    String getDefaultTestFirstDataNashvilleCreditcardNumber();
    
    String getDefaultTestLink2GovCreditCardNumber();
    
    String getDefaultTestParadataCreditCardNumber();
    
    String getDefaultTestMonerisCreditCardNumber();
    
    String getDefaultTestElavonViaconexCreditCardNumberDeclined();
    
    String getDefaultTestElavonCreditCardNumberDeclined();
    
    String getDefaultTestTDMerchantCardNumberDeclined();
    
    List<String> getDefaultTestTDMerchantCardNumberApproved();
    
    String getDefaultTestTDMerchantCardNumberemsunavailable();
    
    String getDefaultTestHeartlandCardNumberApproved();
    
    String getDefaultTestHeartlandExpiryYYMM();
}
