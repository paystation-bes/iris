package com.digitalpaytech.service;

public interface StagingPurchaseService {
    public int getRecordsToProcessByCount();

    public Object getCustomersToProcessByCount();
}
