package com.digitalpaytech.service;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;

public interface WebWidgetService {
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount);
}
