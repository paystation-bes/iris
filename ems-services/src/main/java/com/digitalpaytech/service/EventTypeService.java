package com.digitalpaytech.service;

import com.digitalpaytech.domain.EventType;

public interface EventTypeService {
    
    EventType findEventTypeById(byte eventTypeId);
}
