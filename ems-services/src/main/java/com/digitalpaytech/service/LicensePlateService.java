package com.digitalpaytech.service;

import java.util.concurrent.Future;

import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.client.dto.llps.PreferredParkerFile;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.PlateCheck;
import com.digitalpaytech.dto.PlateCheckResponse;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.JsonException;

public interface LicensePlateService {
    LicencePlate findByNumber(String number);
    
    LicencePlate findById(Integer id);
    
    PreferredParkerFile getCurrentStatus(String fileId, Integer unifiId) throws JsonException;
    
    PlateCheckResponse checkPlateLookup(PlateCheck plateCheck) throws JsonException;
    
    PreferredParkerFile uploadLicensePlates(Integer unifiId, boolean isByLocation, MultipartFile multipartFile) throws JsonException;
    
    boolean compareJurisdictionType(int paystationJurisdictionType, CustomerProperty jurisdictionTypeProperty);

    Future<Boolean> updateLicensePlateService(TransactionDto txDto, PointOfSale pointOfSale, Purchase purchase);
    
}
