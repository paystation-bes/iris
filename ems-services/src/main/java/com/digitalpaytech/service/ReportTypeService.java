package com.digitalpaytech.service;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.ReportType;

public interface ReportTypeService {
    public List<ReportType> loadAll();
    public Map<Integer, ReportType> getReportTypesMap();
    public String getText(int transactionId);
}
