package com.digitalpaytech.service;

import com.digitalpaytech.domain.EventActionType;

public interface EventActionTypeService {
    EventActionType findEventActionType(Integer id);
}
