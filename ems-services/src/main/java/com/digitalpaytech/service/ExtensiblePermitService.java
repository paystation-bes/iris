package com.digitalpaytech.service;

import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.dto.SmsAlertInfo;

import java.util.Collection;
import java.util.Date;

public interface ExtensiblePermitService {
    ExtensiblePermit findByMobileNumber(String mobileNumber);
    
    @SuppressWarnings("PMD.AvoidUsingShortType")
    void updateLatestExpiryDate(Date permitExpireGmt, String cardData, short last4digit, String mobileNumber);
    
    void updateExtensiblePermit(ExtensiblePermit extensiblePermit);
    
    int deleteExpiredSmsAlert();
    
    int deleteExpiredEMSExtensiblePermit();
    
    Collection<SmsAlertInfo> loadSmsAlertFromDB(int countOfRows);
    
    void deleteExtensiblePermitAndSMSAlert(String mobileNumber);
    
    SmsAlertInfo getSmsAlertByParkerReply(String mobileNumber);
    
    void updateSmsAlert(boolean isAlerted, int numOfRetry, boolean isLocked, SmsAlertInfo smsAlertInfo);
    
    void updateSmsAlert(boolean isAlerted, int numOfRetry, boolean isLocked, SmsAlertInfo smsAlertInfo, String callbackId);
    
    void updateSmsAlert(boolean isAlerted, int numOfRetry, boolean isLocked, SmsAlertInfo smsAlertInfo, String callbackId, boolean isFreeParking);
    
    void updateSmsAlertAndLatestExpiryDate(boolean isAlerted, int numOfRetry, boolean isLocked, Date permitBeginGmt, Date permitExpireGmt,
        boolean isFreeParkingExtended, SmsAlertInfo smsAlertInfo);
    
    int getExtensiblePermitByCount();
    
}
