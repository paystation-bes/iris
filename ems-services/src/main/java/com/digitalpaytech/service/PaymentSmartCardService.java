package com.digitalpaytech.service;

import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.Purchase;

public interface PaymentSmartCardService {
    public PaymentSmartCard findByPurchaseId(Purchase purchase);
}
