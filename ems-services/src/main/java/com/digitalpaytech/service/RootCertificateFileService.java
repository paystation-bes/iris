package com.digitalpaytech.service;

import com.digitalpaytech.domain.RootCertificateFile;

public interface RootCertificateFileService {
    
    RootCertificateFile findByMaxId();
    
    void saveOrUpdate(RootCertificateFile file);
    
}
