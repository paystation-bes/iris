package com.digitalpaytech.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.BadCardData;
import com.digitalpaytech.dto.customeradmin.BannedCard;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.SystemException;

public interface LinkBadCardService {
    
    Set<BannedCard> findBannedCards(Integer unifiId, CustomerBadCardSearchCriteria criteria) throws CryptoException;
    
    boolean addBadCard(BadCardData badCard);
    
    void addBadCard(final Map<String, BadCardData> badCards, final Integer unifiId);
    
    boolean deleteBadCard(BadCardData badCard);
    
    void deleteBadCardsbyCustomerId(Integer unifiId);
    
    CustomerCardType determineCustomerCardType(String cardNumber, Integer customerCardTypeId, String cardTypeName, int customerId);
    
    void buildBadCardCreditCardFile(ByteArrayOutputStream baos, Customer customer, int cardType) throws IOException;
    
    boolean activateNotificationIfRequired(Customer customer) throws SystemException;
}
