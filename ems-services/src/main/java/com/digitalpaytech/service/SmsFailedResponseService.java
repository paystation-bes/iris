package com.digitalpaytech.service;

import com.digitalpaytech.domain.SmsFailedResponse;

public interface SmsFailedResponseService {
    
    public void saveSmsFailedResponse(SmsFailedResponse smsFailedResponse);
}
