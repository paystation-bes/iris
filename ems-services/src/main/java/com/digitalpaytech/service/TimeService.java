package com.digitalpaytech.service;

import java.util.Calendar;

import com.digitalpaytech.domain.Time;

public interface TimeService {
    int MINUTES_IN_BUCKET = 15;
    
    Time findBucket(Calendar time);
    
    Time findBucket(Short year, Byte month, Byte day, Byte hour, Byte quaterOfHour);
    
    Time findBucket(Integer timeId);
    
    Time findHour(Calendar time);
    
    /**
     * 
     * @param year
     * @param month
     *            is from 1-12
     * @param day
     * @param hour
     * @return
     */
    Time findHour(Short year, Byte month, Byte day, Byte hour);
    
    Time findDay(Calendar time);
    
    /**
     * 
     * @param year
     * @param month
     *            is from 1-12
     * @param day
     * @return
     */
    Time findDay(Short year, Byte month, Byte day);
    
    Time findMonth(Calendar time);
    
    /**
     * 
     * @param year
     * @param month
     *            is from 1-12
     * @return
     */
    Time findMonth(Short year, Byte month);
}
