package com.digitalpaytech.service;

import com.digitalpaytech.domain.WebServiceEndPoint;


public interface WebServiceCallLogService {
    public boolean logWsCall(WebServiceEndPoint token, int customerId, String timeZone);
}
