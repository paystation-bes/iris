package com.digitalpaytech.service;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.domain.PosBatteryInfo;

public interface PosBatteryInfoService {
    
    public void archiveOldPosBatteryInfoData(int archiveIntervalDays, int archiveBatchSize, String archiveServerName);
    public List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDate(Integer pointOfSaleId, Date fromDate);
    public void addBatteryInfo(Integer pointOfSaleId, Integer sensorTypeId, Date sensorDateGmt, Float value);
    public List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDateNoZeroes(Integer pointOfSaleId, Date fromDate);
}
