package com.digitalpaytech.service;


public interface RestPropertyService {
    
    String REST_DOMAIN_NAME = "DomainName";
    String REST_TOKEN_URI = "TokenURI";
    String REST_PERMIT_URI = "PermitURI";
    String REST_REGION_URI = "RegionURI";
    String REST_COUPON_URI = "CouponURI";
    String REST_SIGNATURE_VERSION = "SignatureVersion";
    String REST_SESSION_TOKEN_EXPIRY_INTERVAL = "RESTSessionTokenExpiryInterval";
    
    String REST_DOMAIN_NAME_DEFAULT = "www.digitalpaytech.com";
    String REST_TOKEN_URI_DEFAULT = "/REST/Token";
    String REST_PERMIT_URI_DEFAULT = "/REST/Permit";
    String REST_REGION_URI_DEFAULT = "/REST/Region";
    String REST_COUPONS_URI_DEFAULT = "/REST/Coupons";
    String REST_SIGNATURE_VERSION_DEFAULT = "1";

    // License Plate Service     
    String REST_LICENSE_PLATE_SERVICE_DOMAIN_NAME = "LicensePlateServiceDomainName";
    String LICENSE_PLATE_SERVICE_CONNECTION_TIMEOUT_TIME_MS = "LicensePlateTimeoutMs";
    int DEFAULT_LICENSE_PLATE_SERVICE_CONNECTION_TIMEOUT_TIME_MS = 5000;    
    
    
    String getPropertyValue(String propertyName);
    
    String getPropertyValue(String propertyName, String defaultValue);
    
    String getPropertyValue(String propertyName, String defaultValue, boolean forceGet);
    
    String getPropertyValue(String propertyName, boolean forceGet);
    
    int getPropertyValueAsInt(String propertyName, int defaultValue) throws Exception;
    
    int getPropertyValueAsInt(String propertyName, int defaultValue, boolean forceGet) throws Exception;
    
    boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue);
    
    boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue, boolean forceGet);
    
    void findPropertiesFile();
    
    void loadPropertiesFile();
}
