package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.Carrier;

public interface CarrierService {
    public List<Carrier> loadAll();
    public Carrier findCarrier(String name);
    public void save(Carrier carrier);
}