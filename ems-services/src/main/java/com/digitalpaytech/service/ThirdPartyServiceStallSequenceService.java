package com.digitalpaytech.service;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ThirdPartyServiceStallSequence;

public interface ThirdPartyServiceStallSequenceService {
    public long findSequenceNumber(Location location, int spaceNumber);
    public ThirdPartyServiceStallSequence insertThirdPartyServiceStallSequence(ThirdPartyServiceStallSequence thirdPartyServiceStallSequence);
    public void updateThirdPartyServiceStallSequence(ThirdPartyServiceStallSequence thirdPartyServiceStallSequence);
}
