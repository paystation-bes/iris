package com.digitalpaytech.service;

import java.util.List;

import com.digitalpaytech.domain.RestAccount;

public interface RestAccountService {
    public List<RestAccount> findRestAccounts(int customerId);
    
    public RestAccount findRestAccountByAccountName(String accountName);
    
    public List<RestAccount> findNonUniqueRestAccountByAccountName(String accountName);

    public RestAccount findRestAccountBySecretKey(String secretKey);

    public RestAccount findRestAccountBySessionToken(String sessionToken);

    public RestAccount findRestAccountByPointOfSaleId(int pointOfSaleId);
    
    public RestAccount findRestAccountById(int restAccountId);
    
    public void saveOrUpdate(RestAccount restAccount);

    public void delete(RestAccount restAccount);

}
