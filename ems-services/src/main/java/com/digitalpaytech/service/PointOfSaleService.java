package com.digitalpaytech.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosDateType;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleAlertInfo;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryDTO;

public interface PointOfSaleService {
    
    List<PointOfSale> findPointOfSalesByRouteId(int routeId);
    
    List<PointOfSale> findPointOfSalesByLocationId(int locationId);
    
    List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int locationId);
    
    List<PointOfSale> findPointOfSalesByCustomerId(int customerId);
    
    List<PointOfSale> findPointOfSalesByCustomerId(int customerId, boolean all);
    
    List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId);
    
    List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdNoCache(int customerId);
    
    PointOfSale findPointOfSaleBySerialNumber(String serialNumber);
    
    PointOfSale findPointOfSaleAndPosStatusBySerialNumber(String serialNumber);
    
    PointOfSale findTxablePointOfSaleBySerialNumber(String serialNumber);
    
    List<PointOfSale> findPointOfSaleBySerialNumberNoConditions(String serialNumber);
    
    PointOfSale findPointOfSaleByPaystationId(int payStationId);
    
    Collection<PointOfSale> findAllPointOfSalesOrderByName();
    
    PointOfSale findPointOfSaleById(Integer id);
    
    List<Paystation> findPayStationsByLocationId(int locationId);
    
    List<Paystation> findPayStationsByCustomerId(int customerId);
    
    Paystation findPayStationById(int payStationId);
    
    Paystation findPayStationByPointOfSaleId(int pointOfSaleId);
    
    PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId, boolean useCache);
    
    PaystationType findPayStationTypeByPointOfSaleId(int pointOfSaleId);
    
    PaystationType findPayStationTypeById(int payStationTypeId);
    
    PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId);
    
    PosServiceState findPosServiceStateByPointOfSaleId(int pointOfSaleId);
    
    PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId);
    
    List<MerchantPOS> findMerchantPOSbyPointOfSaleId(int pointOfSaleId);
    
    List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(int pointOfSaleId);
    
    PosBalance findPOSBalanceByPointOfSaleId(int pointOfSaleId);
    
    PosBalance findPOSBalanceByPointOfSaleIdNoCache(int pointOfSaleId);
    
    Date findLastCollectionDateByPointOfSaleId(int pointOfSaleId);
    
    ModemSetting findModemSettingByPointOfSaleid(int pointOfSaleId);
    
    void saveOrUpdatePointOfSaleHeartbeat(PosHeartbeat posHeartbeat);
    
    void saveOrUpdatePOSDate(PosDate posDate);
    
    PosDateType findPosDateTypeById(int posDateTypeId);
    
    List<PointOfSale> findPointOfSalesBySettingsFileId(int settingsFileId);
    
    List<PointOfSale> findPointOfSalesBySerialNumberAndCustomerId(int customerId, List<String> serialNumberList);
    
    Collection<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(String keyword);
    
    /** 
     * @deprecated Use findPointOfSalesBySerialNumberKeywordWithLimit instead.
     */
    List<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(String keyword, int customerId);
    
    List<PointOfSale> findPointOfSalesBySerialNumberKeywordWithLimit(String keyword, int customerId);
    
    Collection<PointOfSale> findAllPointOfSalesByPosNameKeyword(String keyword);

    /** 
     * @deprecated Use findPointOfSalesByPosNameKeywordWithLimit instead.
     */    
    List<PointOfSale> findAllPointOfSalesByPosNameKeyword(String keyword, int customerId);
    
    List<PointOfSale> findPointOfSalesByPosNameKeywordWithLimit(String keyword, int customerId);
    
    List<PointOfSale> findVirtualPointOfSalesByCustomerId(int customerId);
    
    List<PointOfSale> findNoneVirtualPointOfSalesByCustomerId(int customerId);
    
    void update(PointOfSale pointOfSale);
    
    List<PointOfSale> findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer settingsFileId);
    
    List<PointOfSale> findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer settingsFileId);
    
    List<PointOfSale> findUnAssignedPointOfSalesForPayStationSettingsPage(Integer customerId);
    
    PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId);
    
    List<PointOfSale> findByCustomerIdAndLocationName(Integer customerId, String locationName);
    
    List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(int customerId);
    
    List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdOrderByName(int customerId);
    
    int countUnPlacedByCustomerIds(Collection<Integer> customerIds);
    
    int countUnPlacedByLocationIds(Collection<Integer> locationIds);
    
    int countUnPlacedByRouteIds(Collection<Integer> routeIds);
    
    List<MapEntry> findMapEntriesByCustomerIds(Collection<Integer> customerIds);
    
    List<MapEntry> findMapEntriesByLocationIds(Collection<Integer> locationIds);
    
    List<MapEntry> findMapEntriesByRouteIds(Collection<Integer> routeIds);
    
    Map<Integer, MapEntry> findPointOfSaleMapEntryDetails(Collection<Integer> pointOfSaleIds, TimeZone timeZone);
    
    Map<Integer, PointOfSale> findPosHashByCustomerIds(Collection<Integer> customerIds);
    
    Map<Integer, PointOfSale> findPosHashByLocationIds(Collection<Integer> locationIds);
    
    Map<Integer, PointOfSale> findPosHashByRouteIds(Collection<Integer> routeIds);
    
    Map<Integer, PointOfSale> findPosHashByPosIds(Collection<Integer> posIds);
    
    List<Integer> findPosIdsByCustomerId(Integer customerId);
    
    List<Integer> findAllIds(List<Integer> customerId, List<Integer> locationIds, List<Integer> routeIds);
    
    List<PointOfSale> findAllPointOfSales(List<Integer> customerId, List<Integer> locationIds, List<Integer> routeIds, List<Integer> pointOfSaleIds);
    
    PointOfSale findPointOfSaleBySerialNumberAndCustomerId(String serialNumber, Integer customerId);
    
    boolean isPointOfSalesReadyForMigration(Customer customer, Set<String> versionSet, boolean isForMigration);
    
    List<PointOfSale> findPointOfSaleNotReadyForMigration(Integer customerId, Set<String> versionSet);
    
    List<PayStationSelectorDTO> findPayStationSelector(PointOfSaleSearchCriteria criteria);
    
    List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria);
    
    int findPayStationPage(Integer posId, PointOfSaleSearchCriteria criteria);
    
    void updatePosServicState(PosServiceState posServiceState);
    
    void updatePosSensorState(PosSensorState posSensorState);
    
    void updatePaystationActivityTime(PointOfSale pos, boolean isUpdated);
    
    List<PointOfSale> getDetachedPointOfSaleByLowerCaseNames(int customerId, Collection<String> names, String locPosNameSeparator);
    
    long countPointOfSalesRequiringCollections(Integer routeId, int severity);
    
    List<PointOfSaleAlertInfo> findActivePointOfSalesWithCollectionAlertsInfo(Integer customerId, Integer routeId, Date date, Integer severity,
        boolean onlyActive, boolean onlyInactive);
    
    List<CollectPaystationSummaryDTO> findValidAndActivatedPointOfSalesByCustomerIdAndRouteType(int customerId, int routeTypeId);
    
    List<PointOfSaleAlertInfo> findActivePointOfSalesWithRecentHeartbeatOrCollection(Integer customerId, Integer routeId, Date sinceDate);
    
    int findPayStationCountByCustomerId(Integer customerId);
    
    List<PointOfSale> findLinuxPointOfSalesByCustomerId(Integer customerId);    
	
	List<PointOfSale> findAllPointOfSalesByPosNameForCustomer(String name, Integer customerId);
}
