package com.digitalpaytech.service.impl;

import java.util.Set;
import java.util.HashSet;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.service.ParkingPermissionDayOfWeekService;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.dao.EntityDao;

@Service("parkingPermissionDayOfWeekService")
@Transactional(propagation=Propagation.REQUIRED)
public class ParkingPermissionDayOfWeekServiceImpl implements ParkingPermissionDayOfWeekService {

    @Autowired
    private EntityDao entityDao;

    @Override
    public Set<ParkingPermissionDayOfWeek> findByParkingPermissionId(Integer parkingPermissionId) {
        return new HashSet<ParkingPermissionDayOfWeek>(entityDao.findByNamedQueryAndNamedParam("ParkingPermissionDayOfWeek.findByParkingPermissionId", "parkingPermissionId", parkingPermissionId));
    }
    
    @Override
    public void deleteByParkingPermissionId(Integer parkingPermissionId) {
        entityDao.updateByNamedQuery("ParkingPermissionDayOfWeek.deleteByParkingPermissionId", new String[] { "parkingPermissionId" }, new Object[] { parkingPermissionId });
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
