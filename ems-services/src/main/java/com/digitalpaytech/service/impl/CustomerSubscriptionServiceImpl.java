package com.digitalpaytech.service.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

@Service("customerSubscriptionService")
public class CustomerSubscriptionServiceImpl implements CustomerSubscriptionService {
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final List<CustomerSubscription> findCustomerSubscriptionsByCustomerId(final int customerId, final boolean cacheable) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerSubscription.findCustomerSubscriptionByCustomerId", "customerId", customerId,
            cacheable);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Set<CustomerSubscription> findByCustomerId(final Integer customerId, final boolean cacheable) {
        final List<CustomerSubscription> list = findCustomerSubscriptionsByCustomerId(customerId, cacheable);
        final Set<CustomerSubscription> set = new HashSet<CustomerSubscription>(list.size());
        final Iterator<CustomerSubscription> iter = list.iterator();
        while (iter.hasNext()) {
            set.add(iter.next());
        }
        return set;
    }
    
    @Override
    public List<CustomerSubscription> findActiveCustomerSubscriptionByCustomerId(final int customerId, final boolean cacheable) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerSubscription.findActiveCustomerSubscriptionByCustomerId", "customerId",
            customerId, cacheable);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final void updateLicenseCountForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseCount) {
        final CustomerSubscription subscription = (CustomerSubscription) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "CustomerSubscription.findCustomerSubscriptionByCustomerAndSubscriptionType", new String[] { "customerId", "subscriptionTypeId" },
            new Object[] { customerId, subscriptionTypeId }, true);
        if (subscription != null) {
            subscription.setLicenseCount(licenseCount);
            this.entityDao.update(subscription);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final void updateLicenseCountAndUsedForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseCount,
        final int licenseUsed, final int userAccountId) {
        final CustomerSubscription subscription = (CustomerSubscription) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "CustomerSubscription.findCustomerSubscriptionByCustomerAndSubscriptionType", new String[] { "customerId", "subscriptionTypeId" },
            new Object[] { customerId, subscriptionTypeId }, true);
        if (subscription != null) {
            subscription.setLicenseCount(licenseCount);
            subscription.setLicenseUsed(licenseUsed);
            subscription.setLastModifiedByUserId(userAccountId);
            subscription.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            this.entityDao.update(subscription);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final void updateLicenseUsedForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseUsed) {
        final CustomerSubscription subscription = (CustomerSubscription) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "CustomerSubscription.findCustomerSubscriptionByCustomerAndSubscriptionType", new String[] { "customerId", "subscriptionTypeId" },
            new Object[] { customerId, subscriptionTypeId }, true);
        if (subscription != null) {
            subscription.setLicenseUsed(licenseUsed);
            this.entityDao.update(subscription);
        }
    }
    
    @Override
    public final long countMobileLicensesForCustomer(final Integer customerId) {
        final List<Integer> subscriptionTypeIds = new LinkedList<Integer>();
        subscriptionTypeIds.add(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT);
        subscriptionTypeIds.add(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_MAINTAIN);
        subscriptionTypeIds.add(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_PATROL);
        final List<Object> queryResult = this.entityDao.findByNamedQueryAndNamedParam(
            "CustomerSubscription.countActiveCustomerSubscriptionByCustomerAndSubscriptionTypes",
            new String[] { "customerId", "subscriptionTypeIds" }, new Object[] { customerId, subscriptionTypeIds }, true);
        if (queryResult == null || queryResult.size() == 0) {
            return 0;
        }
        final long result = (Long) queryResult.get(0);
        return result;
    }
    
    @Override
    public final boolean hasOneOfSubscriptions(final Integer customerId, final Integer... subscriptionTypeIds) {
        final Number count = (Number) this.entityDao
                .getNamedQuery("CustomerSubscription.countActiveCustomerSubscriptionByCustomerAndSubscriptionTypes")
                .setParameter("customerId", customerId).setParameterList("subscriptionTypeIds", subscriptionTypeIds).uniqueResult();
        
        return count.intValue() > 0;
    }
}
