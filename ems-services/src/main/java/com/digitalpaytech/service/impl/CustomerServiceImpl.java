package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.LocationTreeCustomerTransformer;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.RandomKeyMapping;

@Component("customerService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Customer findCustomer(final Integer id) {
        return (Customer) this.entityDao.findUniqueByNamedQueryAndNamedParam("Customer.findById", "customerId", id);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Customer findCustomerDetached(final Integer id) {
        final Customer result = this.findCustomer(id);
        if (result != null) {
            this.entityDao.evict(result);
        }
        
        return result;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Customer findCustomerNoCache(final Integer id) {
        return (Customer) this.entityDao.findUniqueByNamedQueryAndNamedParam("Customer.findByIdNoCache", "customerId", id);
    }
    
    @Override
    public final Serializable saveCustomerAgreement(final CustomerAgreement customerAgreement) {
        return this.entityDao.save(customerAgreement);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Customer> findAllParentAndChildCustomers() {
        return this.entityDao.findByNamedQuery("Customer.findAllParentAndChildCustomers", true);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Customer> findAllParentCustomers() {
        return this.entityDao.findByNamedQuery("Customer.findAllParentCustomers", false);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<Customer> findAllChildCustomers() {
        return this.entityDao.findByNamedQuery("Customer.findAllChildCustomers", true);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<Customer> findAllParentAndChildCustomersBySearchKeyword(final String keyword, final Integer... customerIds) {
        Integer[] customerIdsList = customerIds;
        boolean ignoreCustomerType = false;
        if ((customerIds == null) || (customerIds.length <= 0)) {
            customerIdsList = new Integer[] { -1 };
            ignoreCustomerType = true;
        }
        
        return this.entityDao.getNamedQuery("Customer.findAllParentAndChildCustomersBySearchKeyword").setParameter("keyword", "%" + keyword + "%")
                .setParameter("ignoreCustomerType", ignoreCustomerType).setParameterList("customerTypeIds", customerIdsList).list();
    }
    
    @Override
    public final boolean update(final Customer customer) {
        this.entityDao.update(customer);
        return true;
    }
    
    @Override
    public final LocationTree getCustomerTree(final int customerId, final RandomKeyMapping keyMapping) {
        final LocationTree root = new LocationTree();
        
        final LocationTreeCustomerTransformer customerTransformer = new LocationTreeCustomerTransformer(keyMapping);
        customerTransformer.getParentNodes().put(customerId, root);
        while (customerTransformer.getParentNodes().size() > 0) {
            final Set<Integer> orgIds = customerTransformer.getParentNodes().keySet();
            customerTransformer.cleareLevel();
            
            this.entityDao.getNamedQuery("Customer.findChildCustomersTreeByCustomerIds").setParameterList("customerIds", orgIds)
                    .setResultTransformer(customerTransformer).list();
        }
        
        return root;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final LocationTree getChildCustomerTree(final RandomKeyMapping keyMapping) {
        final LocationTree root = new LocationTree();
        final List<LocationTree> childCustomerList = this.entityDao.getNamedQuery("Customer.findChildCustomersTree")
                .setResultTransformer(Transformers.aliasToBean(LocationTree.class)).list();
        for (LocationTree locationTree : childCustomerList) {
            locationTree.setOrganization(true);
            locationTree.setRandomId(keyMapping.getRandomString(Customer.class, locationTree.getId()));
        }
        root.setChildren(childCustomerList);
        return root;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Customer> findAllChildCustomers(final int parentCustomerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Customer.findAllChildCustomersByParentCustomerId", "parentCustomerId", parentCustomerId);
    }
    
    @Override
    public final CustomerURLReroute findURLRerouteByCustomerId(final Integer customerId) {
        return (CustomerURLReroute) this.entityDao.findUniqueByNamedQueryAndNamedParam("CustomerURLReroute.findCustomerURLRerouteByCustomerId",
                                                                                       "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CustomerSubscription> findCustomerSubscriptions(final Integer customerId) {
        return this.entityDao.getNamedQuery("CustomerSubscription.findActiveCustomerSubscriptionByCustomerId").setParameter("customerId", customerId)
                .list();
    }
    
    //TODO
    @SuppressWarnings("unchecked")
    @Override
    public final List<Customer> findCustomerBySubscriptionTypeId(final Integer subscriptionTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Customer.findActiveChildCustomerBySubscriptionTypeId",
                                                            HibernateConstants.SUBSCRIPTION_TYPE_ID, subscriptionTypeId);
    }
    
    @Override
    public final Customer findCustomerByUnifiId(final Integer unifiId) {
        if (unifiId == null) {
            return null;
        }
        @SuppressWarnings("unchecked")
        final List<Customer> customerList = this.entityDao.findByNamedQueryAndNamedParam("Customer.findByUnifiId", "unifiId", unifiId);
        if (customerList == null || customerList.isEmpty()) {
            return null;
        }
        return customerList.get(0);
    }
}
