package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.WebServiceEndPointType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.WebServiceEndPointTypeService;

@Service("webServiceEndPointTypeService")
public class WebServiceEndPointTypeServiceImpl implements WebServiceEndPointTypeService {
    
    @Autowired
    private EntityService entityService;
    
    private List<WebServiceEndPointType> webServiceEndPointTypes;
    private Map<Byte, WebServiceEndPointType> webServiceEndPointTypesMap;
    private Map<String, WebServiceEndPointType> webServiceEndPointTypeNamesMap;
    
    private List<WebServiceEndPointType> privateWebServiceEndPointTypes;
    
    @Override
    public final List<WebServiceEndPointType> loadAll() {
        if (this.webServiceEndPointTypes == null) {
            final List<WebServiceEndPointType> allTypes = this.entityService.loadAll(WebServiceEndPointType.class);
            this.webServiceEndPointTypes = new ArrayList<WebServiceEndPointType>();
            this.webServiceEndPointTypeNamesMap = new HashMap<String, WebServiceEndPointType>();
            this.webServiceEndPointTypesMap = new HashMap<Byte, WebServiceEndPointType>();
            this.privateWebServiceEndPointTypes = new ArrayList<WebServiceEndPointType>();
            for (WebServiceEndPointType type : allTypes) {
                if (type.getIsPrivate()) {
                    this.privateWebServiceEndPointTypes.add(type);
                } else {
                    this.webServiceEndPointTypes.add(type);
                }
                this.webServiceEndPointTypeNamesMap.put(type.getName().toLowerCase(), type);
                this.webServiceEndPointTypesMap.put(type.getId(), type);
            }
        }
        return this.webServiceEndPointTypes;
    }
    
    @Override
    public final List<WebServiceEndPointType> loadAllPrivate() {
        if (this.privateWebServiceEndPointTypes == null) {
            loadAll();
        }
        return this.privateWebServiceEndPointTypes;
    }
    
    @Override
    public final Map<Byte, WebServiceEndPointType> getWebServiceEndPointTypesMap() {
        if (this.webServiceEndPointTypes == null) {
            loadAll();
        }
        return this.webServiceEndPointTypesMap;
    }
    
    @Override
    public final WebServiceEndPointType findWebServiceEndPointType(final String name) {
        WebServiceEndPointType returnValue = null;
        if (this.webServiceEndPointTypes == null) {
            loadAll();
        }
        if (this.webServiceEndPointTypeNamesMap.containsKey(name.toLowerCase())) {
            returnValue = this.webServiceEndPointTypeNamesMap.get(name.toLowerCase());
        }
        return returnValue;
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    @Override
    public final String getText(final byte webServiceEndPointTypeId) {
        String returnValue = null;
        if (this.webServiceEndPointTypesMap == null) {
            loadAll();
        }
        if (this.webServiceEndPointTypesMap.containsKey(webServiceEndPointTypeId)) {
            returnValue = this.webServiceEndPointTypesMap.get(webServiceEndPointTypeId).getName();
        }
        return returnValue;
    }
}
