package com.digitalpaytech.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.CaseCustomerFacility;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.CaseOccupancyHour;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.CaseLocationLotSearchCriteria;
import com.digitalpaytech.dto.CaseLocationLotSelectorDTO;
import com.digitalpaytech.exception.ApplicationException;
//import com.digitalpaytech.helper.CaseRESTLotOccupancy;
//import com.digitalpaytech.service.CaseRESTService;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("caseService")
@Transactional(propagation = Propagation.SUPPORTS)
//PMD.TooManyMethods for simple maintenance all autocount integration related DB service methods are here
@SuppressWarnings({ "PMD.TooManyMethods", "PMD.GodClass", "PMD.ExcessiveImports" })
public class CaseServiceImpl implements CaseService {
    
    private static final Logger LOGGER = Logger.getLogger(CaseServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final ClusterMemberService getClusterMemberService() {
        return this.clusterMemberService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Location> findActiveCaseLocationLots() {
        return (List<Location>) this.entityDao.findByNamedQuery("Location.findLocationByActiveCaseLocationLots");
        
    }
    
    @Override
    public final CaseCustomerAccount findCaseCustomerAccountById(final Integer caseCustomerAccountId) {
        return this.entityDao.get(CaseCustomerAccount.class, caseCustomerAccountId);
    }
    
    @Override
    public final CaseCustomerAccount findCaseCustomerAccountByCaseAccountId(final Integer caseAccountId) {
        return (CaseCustomerAccount) this.entityDao.findUniqueByNamedQueryAndNamedParam("CaseCustomerAccount.findCaseCustomerAccountByCaseAccountId",
                                                                                        HibernateConstants.CASE_ACCOUNT_ID, caseAccountId);
    }
    
    //TODO
    //    @Override
    //    public final Time findLatestCaseOccupancyHour() {
    //        return (Time) this.entityDao.getNamedQuery("CaseOccupancyHour.findLatestOccupancyHour").uniqueResult();
    //    }
    
    @Override
    public final Integer findLatestCaseOccupancyHourTimeIdGMTByLocationId(final Integer locationId) {
        return (Integer) this.entityDao.findUniqueByNamedQueryAndNamedParam("CaseOccupancyHour.findLatestCaseOccupancyHourTimeIdGMTByLocationId",
                                                                            HibernateConstants.LOCATION_ID, locationId);
    }
    
    @Override
    public final CaseCustomerAccount findCaseCustomerAccountByCustomerId(final Integer customertId) {
        return (CaseCustomerAccount) this.entityDao.findUniqueByNamedQueryAndNamedParam("CaseCustomerAccount.findCaseCustomerAccountByCustomerId",
                                                                                        HibernateConstants.CUSTOMER_ID, customertId);
    }
    
    @Override
    public final List<CaseCustomerAccount> findAssignedCaseCustomerAccount() {
        return (List<CaseCustomerAccount>) this.entityDao.findByNamedQuery("CaseCustomerAccount.findAssignedCaseCustomerAccount");
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final List<CaseCustomerAccount> findUnassignedCaseCustomerAccountByNameKeyword(final Integer customerId, final String keyword) {
        return this.entityDao.findByNamedQueryAndNamedParam("CaseCustomerAccount.findUnassignedCaseCustomerAccountByNameKeyword", new String[] {
            HibernateConstants.CUSTOMER_ID, HibernateConstants.KEYWORD, }, new Object[] { customerId,
            new StringBuilder(StandardConstants.STRING_PERCENTAGE).append(keyword).append(StandardConstants.STRING_PERCENTAGE).toString(), });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final List<CaseCustomerAccount> findCaseCustomerAccountByNameKeyword(final String keyword) {
        return this.entityDao.findByNamedQueryAndNamedParam("CaseCustomerAccount.findCaseCustomerAccountByNameKeyword",
                                                            HibernateConstants.KEYWORD,
                                                            new StringBuilder(StandardConstants.STRING_PERCENTAGE).append(keyword)
                                                                    .append(StandardConstants.STRING_PERCENTAGE).toString());
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveCaseCustomerAccount(final CaseCustomerAccount caseCustomerAccount) {
        this.entityDao.save(caseCustomerAccount);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCaseCustomerAccount(final CaseCustomerAccount caseCustomerAccount) {
        this.entityDao.update(caseCustomerAccount);
    }
    
    @Override
    public final List<CaseLocationLot> findCaseLocationLotByCustomerId(final Integer customerId) {
        return (List<CaseLocationLot>) this.entityDao.findByNamedQueryAndNamedParam("CaseLocationLot.findCaseLocationLotByCustomerId",
                                                                                    HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    public final List<CaseLocationLot> findUnassignedLocationLotByCustomerId(final Integer customerId) {
        return (List<CaseLocationLot>) this.entityDao.findByNamedQueryAndNamedParam("CaseLocationLot.findUnassignedCaseLocationLotByCustomerId",
                                                                                    HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    public final List<CaseLocationLot> findCaseLocationLotByLocationId(final Integer locationId) {
        return (List<CaseLocationLot>) this.entityDao.findByNamedQueryAndNamedParam("CaseLocationLot.findCaseLocationLotByLocationId",
                                                                                    HibernateConstants.LOCATION_ID, locationId);
    }
    
    @Override
    public final CaseLocationLot findCaseLocationLotByLotId(final Integer caseLotId) {
        return (CaseLocationLot) this.entityDao.findUniqueByNamedQueryAndNamedParam("CaseLocationLot.findCaseLocationLotByLotId",
                                                                                    HibernateConstants.CASE_LOT_ID, caseLotId);
    }
    
    @Override
    public final CaseLocationLot findCaseLocationLotById(final Integer caseLocationLotId) {
        return this.entityDao.get(CaseLocationLot.class, caseLocationLotId);
    }
    
    @Override
    public final boolean isCaseDataExtractionServer() {
        try {
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CASE_DATA_EXTRACTION_PROCESS_SERVER);
            
            if (processServer == null || processServer.equals(WebCoreConstants.BLANK)) {
                throw new ApplicationException("Couldnot get Server to process Flex Data Extraction process");
            }
            
            if (clusterName.equals(processServer)) {
                LOGGER.info(clusterName + " configured for Flex Data Extraction Task");
                return true;
            } else {
                LOGGER.info(clusterName + " not configured for Flex Data Extraction Task");
                return false;
            }
        } catch (ApplicationException e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e1) {
                LOGGER.error("UnExpected UnknownHost! This shouldn't happened", e1);
            }
            
            final String err = new StringBuilder("Unable to read memeber name from cluster.properties for cluster member ").append(addr).toString();
            LOGGER.error(err);
            
            return false;
        }
    }
    
    @Override
    public final CaseCustomerFacility findCaseCustomerFacilityByCaseFacilityId(final Integer caseFacilityId) {
        return (CaseCustomerFacility) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("CaseCustomerFacility.findCaseCustomerFacilityByCaseFacilityId",
                                                     HibernateConstants.CASE_FACILITY_ID, caseFacilityId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CaseCustomerFacility> findCaseCustomerFacilityByCustomerId(final Integer customerId) {
        return (List<CaseCustomerFacility>) this.entityDao.findByNamedQueryAndNamedParam("CaseCustomerFacility.findCaseCustomerFacilityByCustomerId",
                                                                                         HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveCaseCustomerFacility(final CaseCustomerFacility caseCustomerFacility) {
        this.entityDao.save(caseCustomerFacility);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCaseCustomerFacility(final CaseCustomerFacility caseCustomerFacility) {
        this.entityDao.update(caseCustomerFacility);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveCaseLocationLot(final CaseLocationLot caseLocationLot) {
        this.entityDao.save(caseLocationLot);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCaseLocationLot(final CaseLocationLot caseLocationLot) {
        this.entityDao.update(caseLocationLot);
    }
    
    @Override
    //PMD.AvoidInstantiatingObjectsInLoops required
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final List<CaseLocationLotSelectorDTO> findCaseLocationLotSelector(final CaseLocationLotSearchCriteria criteria,
        final RandomKeyMapping keyMapping) {
        final Criteria query = createCaseLocationLotSearchQuery(criteria);
        //        flfQuery.addOrder(Order.asc("facCode").ignoreCase());
        query.addOrder(Order.asc("lotName").ignoreCase());
        
        final List<CaseLocationLot> caseLocationLotList = query.list();
        
        final List<CaseLocationLotSelectorDTO> result = new ArrayList<CaseLocationLotSelectorDTO>(caseLocationLotList.size());
        for (CaseLocationLot caseLocationLot : caseLocationLotList) {
            final CaseLocationLotSelectorDTO caseLocationLotSelectorDTO = new CaseLocationLotSelectorDTO();
            caseLocationLotSelectorDTO.setLotName(caseLocationLot.getLotName());
            caseLocationLotSelectorDTO.setRandomId(keyMapping.getRandomString(CaseLocationLot.class, caseLocationLot.getId()));
            caseLocationLotSelectorDTO.setStatus(caseLocationLot.getLocation() != null && criteria.getLocationId() != null
                                                 && caseLocationLot.getLocation().getId().intValue() == criteria.getLocationId().intValue()
                    ? WebCoreConstants.SELECTED : WebCoreConstants.ACTIVE);
            
            result.add(caseLocationLotSelectorDTO);
        }
        
        return result;
    }
    
    private Criteria createCaseLocationLotSearchQuery(final CaseLocationLotSearchCriteria criteria) {
        final Criteria query = this.entityDao.createCriteria(CaseLocationLot.class).setCacheable(true);
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq(HibernateConstants.HQL_CUSTOMER_ID, criteria.getCustomerId()));
        }
        
        query.add(Restrictions.eq(HibernateConstants.HQL_IS_ACTIVE, true));
        
        if (criteria.getPage() != null && criteria.getItemsPerPage() != null) {
            final int page = criteria.getPage();
            final int itemsPerPage = criteria.getItemsPerPage();
            final int firstResult = (page - 1) * itemsPerPage;
            if (criteria.getMaxUpdatedTime() == null) {
                criteria.setMaxUpdatedTime(new Date());
            }
            
            query.add(Restrictions.lt(HibernateConstants.PARAMETER_LAST_MODIFIED_GMT, criteria.getMaxUpdatedTime()));
            query.setFirstResult(firstResult);
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query;
    }
    
    @Override
    public final boolean isCaseCustomerAccountListEmpty() {
        @SuppressWarnings("unchecked")
        final List<CaseCustomerAccount> caseCustomerAccountList = this.entityDao.findByNamedQuery("CaseCustomerAccount.findFirstCaseCustomerAccount",
                                                                                                  true, 1, 1);
        
        return caseCustomerAccountList == null || caseCustomerAccountList.isEmpty();
    }
    
    @Override
    public final CaseCustomerFacility findCaseCustomerFacilityByCaseLotId(final Integer caseLotId) {
        // TODO Auto-generated method stub
        @SuppressWarnings("unchecked")
        final List<CaseCustomerFacility> facilities = this.entityDao
                .findByNamedQueryAndNamedParam("CaseCustomerFacility.findCaseCustomerFacilityByCaseLotId", HibernateConstants.CASE_LOT_ID, caseLotId);
        return (CaseCustomerFacility) facilities.get(0);
    }
    
    @Override
    public final Integer findLatestCaseOccupancyHourTimeIdLocalByLocationId(final Integer locationId) {
        return (Integer) this.entityDao.findUniqueByNamedQueryAndNamedParam("CaseOccupancyHour.findLatestCaseOccupancyHourTimeIdLocalByLocationId",
                                                                            HibernateConstants.LOCATION_ID, locationId);
        
    }
    
    @Override
    public final CaseOccupancyHour findCaseOccupancyHourByLocationIdAndTimeIdGMT(final Integer locationId, final Integer timeId) {
        return (CaseOccupancyHour) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("CaseOccupancyHour.findCaseOccupancyHourByLocationIdAndTimeIdGMT", new String[] {
                    HibernateConstants.LOCATION_ID, HibernateConstants.TIME_ID, }, new Object[] { locationId, timeId, }, true);
    }
    
    @Override
    public final void saveOrUpdateCaseOccupancyHour(final CaseOccupancyHour caseOccupancyHour) {
        this.entityDao.saveOrUpdate(caseOccupancyHour);
    }
    
}
