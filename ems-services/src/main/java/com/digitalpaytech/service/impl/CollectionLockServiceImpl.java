package com.digitalpaytech.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.service.CollectionLockService;

@Service("collectionLockService")
public class CollectionLockServiceImpl implements CollectionLockService {
    
    @Autowired
    private EntityDao entityDao;
    
    private static final String SQL_GET_FAILED_RECALCS_BY_COUNT = "SELECT SUM(LockedCount) FROM CollectionLock WHERE LockGMT >= UTC_TIMESTAMP() - INTERVAL 1 HOUR AND  RecalcBeginGMT IS NULL";
    
    @Override
    @Transactional
    public int getFailedRecalcs() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_FAILED_RECALCS_BY_COUNT);
        if (q.list().get(0) == null) {
            return 0;
        } else {
            return Integer.parseInt(q.list().get(0).toString());
        }
    }
    
}
