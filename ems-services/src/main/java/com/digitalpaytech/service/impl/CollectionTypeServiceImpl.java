package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.service.CollectionTypeService;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
@Service("collectionTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CollectionTypeServiceImpl implements CollectionTypeService {
    public static final String SQL_POSBALANCE_RECALCABLE_COUNT = "SELECT COUNT(*) FROM POSBalance posb WHERE posb.IsRecalcable = 1";
    
    private static Logger log = Logger.getLogger(CollectionTypeServiceImpl.class);
    
    private Map<String, CollectionType> collectionTypeMap;
    private Map<Integer, CollectionType> collectionTypeIntMap;
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    @Autowired
    private EntityDao entityDao;
    
    public void setOpenSessionDao(final OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @PostConstruct
    public void init() {
        final Collection<CollectionType> collectionTypes = findAllCollectionTypes();
        this.collectionTypeMap = new HashMap<String, CollectionType>();
        this.collectionTypeIntMap = new HashMap<Integer, CollectionType>();
        
        for (CollectionType type : collectionTypes) {
            this.collectionTypeMap.put(type.getName(), type);
            this.collectionTypeIntMap.put(type.getId(), type);
        }
    }
    
    @Override
    public Collection<CollectionType> findAllCollectionTypes() {
        return this.openSessionDao.loadAll(CollectionType.class);
    }
    
    @Override
    public CollectionType findCollectionType(final String name) {
        return this.collectionTypeMap.get(name);
    }
    
    @Override
    public CollectionType findCollectionType(final int id) {
        return this.collectionTypeIntMap.get(id);
    }
    
    private SQLQuery addCollectionSynchronizedEntity(final SQLQuery query) {
        return query.addSynchronizedQuerySpace("");
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    @Transactional(propagation = Propagation.REQUIRED)
    public int setLock(final int clusterId, final int numberOfPaystionToBeExecuted) {
        
        final SQLQuery query = this.entityDao.createSQLQuery("{ call sp_CollectionSetLock(:clusterId, :numberOfPaystionToBeExecuted) }");
        
        addCollectionSynchronizedEntity(query);
        
        query.setParameter("clusterId", clusterId);
        query.setParameter("numberOfPaystionToBeExecuted", numberOfPaystionToBeExecuted);
        query.addScalar("CollectionLockId", new IntegerType());
        
        final List result = query.list();
        if (result == null) {
            return 0;
        }
        
        return (Integer) result.get(0);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Integer> calculatePaystationBalance(final int collectionLockId, final int timeInterval, final int preAuthDelayTime) {
        
        //        log.debug("RECALC DEBUG LOG: BEFORE calling Recalc() collectionLockId: " + collectionLockId);
        //        log.debug("RECALC DEBUG LOG: BEFORE calling Recalc() timeInterval: " + timeInterval);
        //        log.debug("RECALC DEBUG LOG: BEFORE calling Recalc() preAuthDelayTime: " + preAuthDelayTime);
        
        final SQLQuery query = this.entityDao.createSQLQuery("{ call sp_CollectionRecalcBalance(:collectionLockId, :timeInterval, :preAuthDelayTime) }");
        
        addCollectionSynchronizedEntity(query);
        
        query.setParameter("collectionLockId", collectionLockId);
        query.setParameter("timeInterval", timeInterval);
        query.setParameter("preAuthDelayTime", preAuthDelayTime);
        
        @SuppressWarnings("unchecked")
        final List<String> result = (List<String>) query.list();
        
        //        log.debug("RECALC DEBUG LOG: AFTER calling Recalc() result: " + result == null ? "isNull" : (result.isEmpty() ? "isEmpty" : result.get(0)));
        final List<Integer> idList = new ArrayList<Integer>();
        if (result != null && !result.isEmpty() && result.get(0) != null) {
            final String ids = result.get(0);
            final String[] idListStr = ids.split(",");
            for (int i = 0; i < idListStr.length; i++) {
                if (NumberUtils.isDigits(idListStr[i].trim()) && Integer.parseInt(idListStr[i].trim()) > 0) {
                    idList.add(Integer.parseInt(idListStr[i].trim()));
                }
            }
            if (idList != null && !idList.isEmpty()) {
                this.entityDao.clearFromCache(this.entityDao.getNamedQuery("POSBalance.refreshAfterRecalc").setParameterList("pointOfSaleIds", idList));
                this.entityDao.bringToCache(this.entityDao.getNamedQuery("POSBalance.refreshAfterRecalc").setParameterList("pointOfSaleIds", idList));
            }
        }
        //        log.debug("RECALC DEBUG LOG: AFTER calling Recalc() cache cleared");
        
        if (log.isDebugEnabled()) {
            Object obj = null;
            if (result != null && result.size() > 0) {
                obj = result.get(0);
            }
            log.debug("calculatePaystationBalance returns: " + obj);
        }
        return idList;
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    @Transactional(propagation = Propagation.REQUIRED)
    public void processCollectionFlagUnsettledPreAuth(final int preAuthDelayTime) {
        //        log.debug("UNSETTLED DEBUG LOG: BEFORE calling Recalc() preAuthDelayTime: " + preAuthDelayTime);
        final SQLQuery query = this.entityDao.createSQLQuery("{ call sp_CollectionFlagUnsettledPreAuth(:preAuthDelayTime) }");
        
        addCollectionSynchronizedEntity(query);
        
        query.setParameter("preAuthDelayTime", preAuthDelayTime);
        
        final List result = query.list();
        //        log.debug("UNSETTLED DEBUG LOG: AFTER calling Recalc() preAuthDelayTime: " + preAuthDelayTime);
        if (log.isDebugEnabled()) {
            if (!result.isEmpty()) {
                log.debug("sp_CollectionFlagUnsettledPreAuth execution completed and returned: " + result.get(0));
            }
        }
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    @Transactional(propagation = Propagation.REQUIRED)
    public void processCollectionReleaseLock(final int clusterId) {
        final SQLQuery query = this.entityDao.createSQLQuery("{ call sp_CollectionReleaseLock(:clusterId) }");
        
        addCollectionSynchronizedEntity(query);
        
        query.setParameter("clusterId", clusterId);
        
        final List result = query.list();
        if (log.isDebugEnabled()) {
            if (!result.isEmpty()) {
                log.debug("sp_CollectionReleaseLock execution completed and returned: " + result.get(0));
            }
        }
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    public void processDeleteCollectionLock() {
        final SQLQuery query = this.entityDao.createSQLQuery("{ call sp_CollectionDeleteLock() }");
        final List result = query.list();
        if (log.isDebugEnabled()) {
            if (!result.isEmpty()) {
                log.debug("sp_CollectionDeleteLock execution completed and returned: " + result.get(0));
            }
        }
    }
    
    @Override
    public int getPOSBalanceRecalcableCount() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_POSBALANCE_RECALCABLE_COUNT);
        final int ret = Integer.parseInt(q.list().get(0).toString());
        return ret;
    }
    
}