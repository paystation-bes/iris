package com.digitalpaytech.service.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.CryptoClient;
import com.digitalpaytech.client.LinkCryptoClient;
import com.digitalpaytech.client.dto.KeyIdentity;
import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.client.util.HystrixExceptionUtil;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CryptoKey;
import com.digitalpaytech.domain.CryptoKeyHash;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.HashAlgorithm;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.dto.systemadmin.CryptoKeyInfo;
import com.digitalpaytech.dto.systemadmin.CryptoKeyListInfo;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.MissingCryptoKeyException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.Crypto;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.KeyValuePair;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.impl.EasyAES128;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.utils.DPTWSUtil;
import com.digitalpaytech.ws.signingservice.GetSignStringFaultMessage;
import com.digitalpaytech.ws.signingservice.GetSignStringType;
import com.digitalpaytech.ws.signingservice.SigningService;
import com.digitalpaytech.ws.signingservicev2.GetUnsupportedHashRequestFaultMessage;
import com.digitalpaytech.ws.signingservicev2.SigningServiceV2;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.client.ClientException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.netflix.ribbon.ServerError;

@Service("cryptoService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass", "PMD.TooManyMethods" })
public class CryptoServiceImpl implements CryptoService {
    public static final int CURR_PUBLIC_KEY_RSA_MODULUS = 0;
    public static final int CURR_PUBLIC_KEY_RSA_EXPONENT = 1;
    
    private static final int KEY_DISPLAY_INDEX_START = 3;
    
    private static final String FAILURE = "FAILURE";
    private static final int RSA_2048_BIT_CREDIT_CARD_TYPE_ID = 0;
    private static final int AES_256_BIT_CREDIT_CARD_TYPE_ID_0 = 1;
    private static final int AES_256_BIT_CREDIT_CARD_TYPE_ID_1 = 2;
    private static final int SIGNING_SERVER_CRYPTO_ID = 99;
    
    private static final int CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID = AES_256_BIT_CREDIT_CARD_TYPE_ID_1;
    private static final int CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID = RSA_2048_BIT_CREDIT_CARD_TYPE_ID;
    private static final int CURRENT_LINK_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID = 4;
    
    private static final boolean ALLOW_EMPTY_KEYSTORE = false;
    
    private static final String REGEX_ENCRYPTED_DATA = "[0-9]{6}[0-9a-zA-Z|=|+|/]{8,}";
    
    private static final String RSA_TRANSFORMATION_IP = "RSA/ECB/PKCS1Padding";
    private static final String RSA_TRANSFORMATION_0 = "RSA/ECB/PKCS1Padding";
    // BC was originally set to this
    private static final String AES_TRANSFORMATION_0 = "AES/ECB/PKCS7Padding";
    // Ncipher can't do PKCS7
    private static final String AES_TRANSFORMATION_1 = "AES/ECB/PKCS5Padding";
    
    private static final String CRYPOTO_KEY_PROCESSING_ADMIN_ALERT_NAME = "Crypto Key Processing - ";
    
    private static final int KEY_INFO_LENGTH = 6;
    
    private static final int KEY_PREFIX_LENGTH = 3;
    
    private static final int RSA_KEY_SIZE = 2048;
    
    private static final int RSA_SIGNING_KEY_SIZE = 4096;
    
    private static final int AES_KEY_SIZE = 256;
    
    private static final int CURRENT_HASH = 0;
    
    private static final int NEXT_HASH = 1;
    
    private static final int KEY_TYPE_EXT = 0;
    private static final Pattern[] PTTRN_KEYS = { CryptoConstants.PTTRN_INTERNAL_KEY, CryptoConstants.PTTRN_EXTERNAL_KEY };
    
    private static final String FMT_KEY_INFO = "<Key><Modulus>{1}</Modulus><Exponent>{2}</Exponent><Identifier>{0}</Identifier></Key>\n";
    private static final String FMT_KEY_PACKAGE = "{0}<Expiration>{1,date,yyyy-MM-dd}</Expiration>\n<NextKeyHash>{2}</NextKeyHash>\n";
    
    private static final String DEVICE_PASSWORD_SALT = "t&E5@4%g!Lr9e2$ap*nF";
    private static final int DEVICE_PASSWORD_ENCODE_STRENGTH = 10;
    
    private static final Logger LOG = Logger.getLogger(CryptoServiceImpl.class);
    
    private static final String OPEN_BRACE = "<";
    private static final String CLOSE_BRACE = ">";
    private static final String SLASH = "/";
    private static final String NEWLINE = "\n";
    
    private static final String KEY = "Key";
    private static final String KEY_OPEN_TAG = OPEN_BRACE + KEY + CLOSE_BRACE;
    private static final String KEY_CLOSE_TAG = OPEN_BRACE + SLASH + KEY + CLOSE_BRACE + NEWLINE;
    
    private static final String MODULUS = "Modulus";
    private static final String MODULUS_OPEN_TAG = OPEN_BRACE + MODULUS + CLOSE_BRACE;
    private static final String MODULUS_CLOSE_TAG = OPEN_BRACE + SLASH + MODULUS + CLOSE_BRACE;
    
    private static final String EXPONENT = "Exponent";
    private static final String EXPONENT_OPEN_TAG = OPEN_BRACE + EXPONENT + CLOSE_BRACE;
    private static final String EXPONENT_CLOSE_TAG = OPEN_BRACE + SLASH + EXPONENT + CLOSE_BRACE;
    
    private static final String IDENTIFIER = "Identifier";
    private static final String IDENTIFIER_OPEN_TAG = OPEN_BRACE + IDENTIFIER + CLOSE_BRACE;
    private static final String IDENTIFIER_CLOSE_TAG = OPEN_BRACE + SLASH + IDENTIFIER + CLOSE_BRACE;
    
    private static final String EXPIRATION = "Expiration";
    private static final String EXPIRATION_OPEN_TAG = OPEN_BRACE + EXPIRATION + CLOSE_BRACE;
    private static final String EXPIRATION_CLOSE_TAG = OPEN_BRACE + SLASH + EXPIRATION + CLOSE_BRACE + NEWLINE;
    
    private static final String NEXT_KEY_HASH = "NextKeyHash";
    private static final String NEXT_KEY_HASH_OPEN_TAG = OPEN_BRACE + NEXT_KEY_HASH + CLOSE_BRACE;
    private static final String NEXT_KEY_HASH_CLOSE_TAG = OPEN_BRACE + SLASH + NEXT_KEY_HASH + CLOSE_BRACE + NEWLINE;
    
    private static final String ERROR_ENCRYPTION = "Not able to encrypt data";
    
    private Map<Integer, CryptoKey> currentKeyMap = new Hashtable<Integer, CryptoKey>();
    
    private Map<Integer, Collection<CryptoKey>> deprecatedKeysMap = new Hashtable<Integer, Collection<CryptoKey>>();
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private JSON json;
    
    private CryptoClient cryptoClient;
    
    private LinkCryptoClient linkCryptoClient;
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private void init() throws IOException {
        this.cryptoClient = this.clientFactory.from(CryptoClient.class);
        this.linkCryptoClient = this.clientFactory.from(LinkCryptoClient.class);
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setPosServiceStateService(final PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    public final void setHashAlgorithmTypeService(final HashAlgorithmTypeService hashAlgorithmTypeService) {
        this.hashAlgorithmTypeService = hashAlgorithmTypeService;
    }
    
    public void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    // For JUnit
    public final void setLinkCryptoClient(final LinkCryptoClient linkCryptoClient) {
        this.linkCryptoClient = linkCryptoClient;
    }
    
    public final void setCryptoClient(final CryptoClient cryptoClient) {
        this.cryptoClient = cryptoClient;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setJson(final JSON json) {
        this.json = json;
    }
    
    public final String decryptData(final String data) throws CryptoException {
        if (data == null || data.length() <= KEY_INFO_LENGTH) {
            return data;
        }
        
        if (!data.matches(REGEX_ENCRYPTED_DATA)) {
            throw new CryptoException("Unable to decrypt data as it has invalid characters", new ClientException("Card data invalid"));
        }
        
        String result = null;
        try {
            if (useCryptoServer()) {
                result = sendToCryptoService(data, data.substring(0, KEY_PREFIX_LENGTH));
            } else {
                
                final String keyInfo = data.substring(0, KEY_INFO_LENGTH);
                if (!hasCipher(keyInfo)) {
                    throw new MissingCryptoKeyException(keyInfo);
                }
                
                final Cipher cipher = getCipher(keyInfo, Cipher.DECRYPT_MODE);
                
                final byte[] encodedEncryptedBytes = data.substring(KEY_INFO_LENGTH).getBytes();
                
                final byte[] decodedEncryptedBytes = Base64.decodeBase64(encodedEncryptedBytes);
                final byte[] decryptedBytes = cipher.doFinal(decodedEncryptedBytes);
                result = new String(decryptedBytes);
            }
        } catch (Exception e) {
            throw new CryptoException("Unable to decrypt data", e);
        }
        return result;
    }
    
    @Override
    public final String encryptData(final String data) throws CryptoException {
        
        String result = null;
        if (data == null || data.length() == 0) {
            result = data;
        } else {
            if (useCryptoServer()) {
                final boolean useLinkFlag = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.USE_LINK_CRYPTO,
                                                                                                EmsPropertiesService.DEFAULT_USE_LINK_CRYPTO);
                if (useLinkFlag) {
                    try {
                        result = this.linkCryptoClient.encryptInternal(data).execute().toString(Charset.defaultCharset());
                    } catch (HystrixRuntimeException hre) {
                        final String errMsg = "Failed to encrypt data";
                        LOG.error(errMsg, hre);
                        
                        if (HystrixExceptionUtil.isCryptoCause(hre)) {
                            throw new CryptoException(new ClientException(result, hre));
                        } else {
                            throw new CryptoException(new ServerError(result, hre));
                        }
                    }
                } else {
                    try {
                        result = this.cryptoClient.encrypt(data).execute().toString(Charset.defaultCharset());
                    } catch (Exception e) {
                        throw new CryptoException("Failed to encrypt data", new ServerError(result, e));
                    }
                    if (result == null || result.startsWith(FAILURE)) {
                        throw new CryptoException("Unable to ecrypt data due to clent error", new ClientException(result));
                    }
                }
            } else {
                final int cryptoTypeId = getCurrentCryptoTypeIdForPurpose(PURPOSE_CREDIT_CARD_LOCAL);
                final int cryptoIndex = getCurrentCryptoIndexForTypeId(cryptoTypeId);
                String keyInfo = createKeyInfo(cryptoTypeId, cryptoIndex);
                if (cryptoTypeId == CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID && !hasCipher(keyInfo)) {
                    // Using previous key instead!
                    keyInfo = createKeyInfo(cryptoTypeId, cryptoIndex - 1);
                }
                
                final Cipher cipher = getCipher(keyInfo, Cipher.ENCRYPT_MODE);
                final byte[] decryptedBytes = data.getBytes();
                
                try {
                    final byte[] encryptedBytes = cipher.doFinal(decryptedBytes);
                    final byte[] encodedEncryptedBytes = Base64.encodeBase64(encryptedBytes);
                    result = keyInfo + new String(encodedEncryptedBytes);
                } catch (Exception e) {
                    throw new CryptoException(ERROR_ENCRYPTION, e);
                }
            }
        }
        
        return result;
    }
    
    @Override
    public String encryptData(final int purpose, final String data) throws CryptoException {
        if (purpose != PURPOSE_CREDIT_CARD_LOCAL) {
            throw new CryptoException("Iris does not support encryption for purpose: " + purpose);
        }
        
        return encryptData(data);
    }
    
    private String sendToCryptoService(final String data, final String prefix) throws ServerError, ClientException {
        
        String result = null;
        
        if (CryptoConstants.IRIS_INTERNAL_KEY_PREFIX.equals(prefix) || CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX.equals(prefix)) {
            try {
                result = this.cryptoClient.decrypt(data).execute().toString(Charset.defaultCharset());
            } catch (Exception e) {
                throw new ServerError(result, e);
            }
            if (result.startsWith(FAILURE)) {
                throw new ClientException(result);
            }
            
        } else if (CryptoConstants.LINK_INTERNAL_KEY_PREFIX.equals(prefix) || CryptoConstants.LINK_EXTERNAL_KEY_PREFIX.equals(prefix)) {
            try {
                result = this.linkCryptoClient.decrypt(data).execute().toString(Charset.defaultCharset());
            } catch (HystrixRuntimeException hre) {
                
                if (HystrixExceptionUtil.isCryptoCause(hre)) {
                    throw new ClientException(result, hre);
                } else {
                    throw new ServerError(result, hre);
                }
            }
        } else {
            throw new ClientException(String.format("Unrecognized prefix %s", prefix));
        }
        return result;
    }
    
    private CryptoKey getCurrentCryptKey(final int purpose) throws CryptoException {
        final int cryptoTypeId = getCurrentCryptoTypeIdForPurpose(purpose);
        final CryptoKey cryptoKey = this.findCurrentCryptoKeyByType(cryptoTypeId);
        if (cryptoKey != null) {
            this.currentKeyMap.put(cryptoTypeId, cryptoKey);
        }
        return cryptoKey;
    }
    
    private CryptoKey getPrevCryptKey(final int purpose) throws CryptoException {
        final int cryptoTypeId = getCurrentCryptoTypeIdForPurpose(purpose);
        final Collection<CryptoKey> cryptoKeys = this.findDeprecatedKeysByType(cryptoTypeId);
        if (cryptoKeys.size() < 1) {
            return null;
        }
        final Object[] cryptoKeysArray = cryptoKeys.toArray();
        return (CryptoKey) cryptoKeysArray[cryptoKeysArray.length - 1];
    }
    
    private Collection<CryptoKey> getDeprecatedKeys(final int purpose) throws CryptoException {
        final Collection<CryptoKey> cryptoKeys;
        final int cryptoTypeId = getCurrentCryptoTypeIdForPurpose(purpose);
        
        cryptoKeys = this.findDeprecatedKeysByType(cryptoTypeId);
        if (cryptoKeys != null) {
            this.deprecatedKeysMap.put(cryptoTypeId, cryptoKeys);
        }
        return cryptoKeys;
    }
    
    // This will return the data needed for the next key hash
    private String createXmlKeyInformation(final Key key, final String keyInfo) throws CryptoException {
        RSAKeyParameters param = null;
        try {
            final SubjectPublicKeyInfo subPkInfo = new SubjectPublicKeyInfo((ASN1Sequence) ASN1Object.fromByteArray(key.getEncoded()));
            param = (RSAKeyParameters) PublicKeyFactory.createKey(subPkInfo);
        } catch (IOException e) {
            throw new CryptoException("Invalid key", e);
        }
        
        final StringBuilder sb = new StringBuilder(500);
        sb.append(KEY_OPEN_TAG);
        
        sb.append(MODULUS_OPEN_TAG);
        sb.append(convertToBase64EncodedString(param.getModulus()));
        sb.append(MODULUS_CLOSE_TAG);
        
        sb.append(EXPONENT_OPEN_TAG);
        sb.append(convertToBase64EncodedString(param.getExponent()));
        sb.append(EXPONENT_CLOSE_TAG);
        
        sb.append(IDENTIFIER_OPEN_TAG);
        sb.append(keyInfo);
        sb.append(IDENTIFIER_CLOSE_TAG);
        
        sb.append(KEY_CLOSE_TAG);
        
        return sb.toString();
    }
    
    // this will create the key information for signature
    private String createXmlKeyBundle(final CryptoKey cryptoKey, final CryptoKeyHash cryptoKeyHash) throws CryptoException {
        // get key info
        final String keyInfo = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex());
        
        // get public key
        final Key key = getCrypto(cryptoKey.getKeyType()).getKey(keyInfo, Crypto.PUBLIC_KEY_OBJECT_ID);
        
        final StringBuilder sb = new StringBuilder(800);
        
        // Key Information
        final String xmlKeyInfo = createXmlKeyInformation(key, keyInfo);
        sb.append(xmlKeyInfo);
        
        // Expiration
        sb.append(EXPIRATION_OPEN_TAG);
        // get from db crypto record
        sb.append(DateUtil.createDateOnlyString(cryptoKey.getExpiryGmt()));
        sb.append(EXPIRATION_CLOSE_TAG);
        
        // Next Key Hash
        sb.append(NEXT_KEY_HASH_OPEN_TAG);
        if (cryptoKey.getId() == null && cryptoKeyHash != null && cryptoKeyHash.getId() == null) {
            sb.append(cryptoKeyHash.getNextHash());
        } else {
            sb.append(getNextHash(cryptoKey, cryptoKeyHash.getHashAlgorithmType().getId()));
        }
        sb.append(NEXT_KEY_HASH_CLOSE_TAG);
        
        return sb.toString();
    }
    
    // This will return the public key information
    private String getPublicKeyInformation(final Key key) throws CryptoException {
        RSAKeyParameters param = null;
        try {
            final SubjectPublicKeyInfo subPkInfo = new SubjectPublicKeyInfo((ASN1Sequence) ASN1Object.fromByteArray(key.getEncoded()));
            param = (RSAKeyParameters) PublicKeyFactory.createKey(subPkInfo);
        } catch (IOException e) {
            throw new CryptoException("Invalid key", e);
        }
        
        final StringBuilder sb = new StringBuilder(500);
        sb.append(convertToBase64EncodedString(param.getModulus()));
        sb.append(PUBLIC_KEY_DILIMITER);
        sb.append(convertToBase64EncodedString(param.getExponent()));
        return sb.toString();
    }
    
    private String convertToBase64EncodedString(final BigInteger bigInt) {
        String base16String = bigInt.toString(16);
        
        // If the hex string has an odd # of digits, it means there is a leading
        // '0' that is missing
        if (base16String.length() % 2 == 1) {
            base16String = "0" + base16String;
        }
        
        // Convert the hex string to a byte array
        final byte[] byteArray = new byte[base16String.length() / 2];
        int i = 0;
        
        for (i = 0; i < base16String.length() - 2; i = i + 2) {
            byteArray[i / 2] = (byte) Integer.parseInt(base16String.substring(i, i + 2), 16);
        }
        byteArray[i / 2] = (byte) Integer.parseInt(base16String.substring(i), 16);
        
        // Base 64 encode and encapsulate in a string
        return new String(Base64.encodeBase64(byteArray));
    }
    
    @Override
    public String createKeyInfo(final int cryptoTypeId, final int cryptoIndex) {
        String keyType = Integer.toString(cryptoTypeId);
        keyType = THREE_ZERO.substring(keyType.length()) + keyType;
        
        String keyIndex = null;
        if (cryptoIndex < 0) {
            keyIndex = "0";
        } else {
            keyIndex = Integer.toString(cryptoIndex);
        }
        keyIndex = THREE_ZERO.substring(keyIndex.length()) + keyIndex;
        
        return keyType + keyIndex;
    }
    
    private boolean hasCipher(final String keyInfo) {
        boolean result = false;
        try {
            final int keyType = Integer.parseInt(keyInfo.substring(0, 3));
            final int keyIndex = Integer.parseInt(keyInfo.substring(3));
            
            final Crypto crypto = getCrypto(keyType);
            result = crypto.isExistKey(createKeyInfo(keyType, keyIndex));
        } catch (CryptoException ce) {
            // DO NOTHING.
        }
        
        return result;
    }
    
    private Cipher getCipher(final String keyInfo, final int cipherMode) throws CryptoException {
        if (keyInfo == null) {
            throw new CryptoException("Invalid key info: null");
        } else if (keyInfo.length() != KEY_INFO_LENGTH) {
            throw new CryptoException("Invalid key info: length should be " + KEY_INFO_LENGTH + ", but is: " + keyInfo.length());
        } else if (!keyInfo.matches(WebCoreConstants.REGEX_INTEGER)) {
            throw new CryptoException("Invalid key info: not an integer");
        }
        
        try {
            final int cryptoTypeId = Integer.parseInt(keyInfo.substring(0, 3));
            final Crypto crypto = getCrypto(cryptoTypeId);
            
            final Key key = crypto.getKey(keyInfo, getKeyObjectType(cryptoTypeId, cipherMode));
            final String transformation = getCryptoTransformation(cryptoTypeId);
            
            final Cipher cipher = Cipher.getInstance(transformation, crypto.getJceProvider());
            cipher.init(cipherMode, key);
            return cipher;
        } catch (GeneralSecurityException e) {
            throw new CryptoException("Unable to init cipher", e);
        }
    }
    
    private Crypto getCrypto(final int cryptoTypeId) throws CryptoException {
        // to expand this to multiple crypto types(ie. creditcard, other) check
        // on cryptoTypeId
        return CryptoUtil.getCreditCardCrypto();
    }
    
    private int getKeyObjectType(final int cryptoTypeId, final int cipherMode) throws CryptoException {
        final int cryptoKeyObjectType;
        
        if (cipherMode == Cipher.DECRYPT_MODE) {
            switch (cryptoTypeId) {
                case RSA_2048_BIT_CREDIT_CARD_TYPE_ID:
                    cryptoKeyObjectType = Crypto.PRIVATE_KEY_OBJECT_ID;
                    break;
                
                case AES_256_BIT_CREDIT_CARD_TYPE_ID_0:
                case AES_256_BIT_CREDIT_CARD_TYPE_ID_1:
                    cryptoKeyObjectType = Crypto.SECRET_KEY_OBJECT_ID;
                    break;
                
                default:
                    throw new CryptoException("Unknown Crypto Type: " + cryptoTypeId);
            }
        } else if (cipherMode == Cipher.ENCRYPT_MODE) {
            // only doing secret key encryption on EMS
            cryptoKeyObjectType = Crypto.SECRET_KEY_OBJECT_ID;
        } else {
            throw new CryptoException("Unknown Cipher Mode: " + cipherMode);
        }
        
        return cryptoKeyObjectType;
    }
    
    private String getCryptoTransformation(final int cryptoTypeId) throws CryptoException {
        switch (cryptoTypeId) {
            case RSA_2048_BIT_CREDIT_CARD_TYPE_ID:
                return RSA_TRANSFORMATION_0;
            
            case AES_256_BIT_CREDIT_CARD_TYPE_ID_0:
                return AES_TRANSFORMATION_0;
            
            case AES_256_BIT_CREDIT_CARD_TYPE_ID_1:
                return AES_TRANSFORMATION_1;
            default:
                break;
        }
        
        throw new CryptoException("Unknown Crypto Type: " + cryptoTypeId);
    }
    
    private int getCurrentCryptoTypeIdForPurpose(final int purpose) throws CryptoException {
        switch (purpose) {
            case PURPOSE_CREDIT_CARD_LOCAL:
                return CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID;
            case PURPOSE_CREDIT_CARD_REMOTE:
                return CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID;
            default:
                break;
        }
        
        throw new CryptoException("Unknown Crypto Purpose: " + purpose);
    }
    
    private int getCurrentCryptoIndexForTypeId(final int cryptoTypeId) throws CryptoException {
        if ((cryptoTypeId == CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID) || (cryptoTypeId == CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID)) {
            final CryptoKey cryptoKey = this.findCurrentCryptoKeyByType(cryptoTypeId);
            if (cryptoKey != null) {
                this.currentKeyMap.put(cryptoTypeId, cryptoKey);
                return cryptoKey.getKeyIndex();
            } else {
                // means not current key using
                return 0;
            }
        } else {
            throw new CryptoException("Unknown Crypto Type: " + cryptoTypeId);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void generateExternalCryptoKey(final String comment, final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) throws CryptoException {
        String tmpComment = comment;
        if (useCryptoServer()) {
            try {
                this.cryptoClient.rotate().execute();
            } catch (Exception e) {
                throw new CryptoException("Failed to rotate External Key!", e);
            }
        } else {
            final int cryptoTypeId = CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID;
            int cryptoIndex = getCurrentCryptoIndexForTypeId(cryptoTypeId);
            String currentkeyInfo;
            final Crypto crypto = getCrypto(cryptoTypeId);
            // no record in crypto DB
            if (cryptoIndex == 0) {
                currentkeyInfo = createKeyInfo(cryptoTypeId, cryptoIndex + 1);
                // check whether have Initial key in keystore
                if (!crypto.isExistKey(currentkeyInfo)) {
                    if (ALLOW_EMPTY_KEYSTORE) {
                        crypto.createRsaKey(currentkeyInfo, RSA_KEY_SIZE,
                                            this.hashAlgorithmTypeService.findAlgorithmName(CryptoConstants.CURRENT_LATEST_SIGNING_ALGO));
                    } else {
                        throw new CryptoException("Can't find initial key for type:" + cryptoTypeId);
                    }
                }
                
                tmpComment = "Initial RSA Key";
            }
            cryptoIndex++;
            final CryptoKey cryptoKey = new CryptoKey();
            cryptoKey.setKeyType(cryptoTypeId);
            cryptoKey.setKeyIndex(cryptoIndex);
            cryptoKey.setComment(tmpComment);
            
            final Date createDate = Calendar.getInstance().getTime();
            cryptoKey.setCreatedGmt(createDate);
            
            final Date expiryDate = getExpirationDateForTypeId(createDate, cryptoTypeId);
            cryptoKey.setExpiryGmt(expiryDate);
            currentkeyInfo = createKeyInfo(cryptoTypeId, cryptoIndex);
            final Key currentKey = crypto.getKey(currentkeyInfo, Crypto.PUBLIC_KEY_OBJECT_ID);
            
            final String currentXmlKeyInfo = createXmlKeyInformation(currentKey, currentkeyInfo);
            
            final String currentInfo = this.getPublicKeyInformation(currentKey);
            cryptoKey.setInfo(currentInfo);
            
            // Insert the next key
            final String nextKeyInfo = createKeyInfo(cryptoTypeId, cryptoIndex + 1);
            
            crypto.createRsaKey(nextKeyInfo, RSA_KEY_SIZE,
                                this.hashAlgorithmTypeService.findAlgorithmName(CryptoConstants.CURRENT_LATEST_SIGNING_ALGO));
            final Key nextKey = crypto.getKey(nextKeyInfo, Crypto.PUBLIC_KEY_OBJECT_ID);
            
            final String nextKeyXmlInfo = createXmlKeyInformation(nextKey, nextKeyInfo);
            
            /*
             * Prepare CryptoKeyHash objects.
             */
            Integer algId = null;
            HashAlgorithmData hashAlgorithmData = null;
            final Set<CryptoKeyHash> cryptoKeyHashes = new HashSet<CryptoKeyHash>(hashAlgorithmDataMap.size());
            final Iterator<Integer> algIdsIter = hashAlgorithmDataMap.keySet().iterator();
            while (algIdsIter.hasNext()) {
                algId = algIdsIter.next();
                hashAlgorithmData = hashAlgorithmDataMap.get(algId);
                
                final CryptoKeyHash cryptoKeyHash = new CryptoKeyHash();
                
                // get hash
                final String hash = this.cryptoAlgorithmFactory.getShaHash(currentXmlKeyInfo, CryptoConstants.HASH_PUBLIC_ENCRYPTION_KEY,
                                                                           hashAlgorithmData.getHashAlgorithmType().getId());
                cryptoKeyHash.setHash(hash);
                
                // get hash
                final String nextHash = this.cryptoAlgorithmFactory.getShaHash(nextKeyXmlInfo, CryptoConstants.HASH_PUBLIC_ENCRYPTION_KEY,
                                                                               hashAlgorithmData.getHashAlgorithmType().getId());
                cryptoKeyHash.setNextHash(nextHash);
                
                // Add CryptoKeyHash
                cryptoKeyHash.setCryptoKey(cryptoKey);
                cryptoKeyHash.setHashAlgorithmType(hashAlgorithmData.getHashAlgorithmType());
                cryptoKeyHash.setCreatedGmt(createDate);
                
                // creat xml key bundle
                final String xmlKeyBundle = createXmlKeyBundle(cryptoKey, cryptoKeyHash);
                
                // get signature
                // String signature = generateSignatureV2(keyInfo, xml_key_bundle,
                // hashAlgorithmData.getHashAlgorithmType().getSignatureId());
                final String signature;
                
                if (hashAlgorithmData.getHashAlgorithmType().getId() == CryptoConstants.HASH_ALGORITHM_TYPE_SHA1) {
                    signature = generateSignature(currentkeyInfo, xmlKeyBundle);
                } else {
                    signature = generateSignatureV2(currentkeyInfo, xmlKeyBundle, hashAlgorithmData.getHashAlgorithmType().getSignatureId());
                }
                
                cryptoKeyHash.setSignature(signature);
                
                cryptoKeyHashes.add(cryptoKeyHash);
            }
            cryptoKey.setCryptoKeyHashs(cryptoKeyHashes);
            
            // Save those info to CryptoKey table
            // Trigger to set service state flag, notice PS a new key created
            saveCryptoKey(cryptoKey);
            
            // Clean the currentkey in map
            this.currentKeyMap.remove(cryptoTypeId);
            
            // Clean deprecated key collection in maps
            this.deprecatedKeysMap.remove(cryptoTypeId);
        }
    }
    
    // This function is supposed to automatically bridge the migration gap as a
    // result of EMS-2510 change.
    // This is supposed to be called once only by crypto key generate server to
    // generate the initial next
    // internal key and store in key store.
    private void generateNextInternalKeyInKeyStore() {
        final int cryptoTypeId = CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID;
        String keyInfo = null;
        try {
            final int cryptoIndex = getCurrentCryptoIndexForTypeId(cryptoTypeId);
            
            final Crypto crypto = getCrypto(cryptoTypeId);
            keyInfo = createKeyInfo(cryptoTypeId, cryptoIndex + 1);
            // no record in crypto DB
            if (cryptoIndex == 0) {
                if (!crypto.isExistKey(keyInfo)) {
                    // key in keystore
                    throw new CryptoException("Can't find initial key for type:" + cryptoTypeId);
                }
            } else {
                // generate the next key in key store
                crypto.createAesKey(keyInfo, AES_KEY_SIZE);
                LOG.info("Next internal key " + keyInfo + " generated in key store.");
            }
        } catch (CryptoException e) {
            final String msg = "Unable to retrieve and generate the next internal key " + keyInfo;
            this.mailerService.sendAdminAlert("Unable to retrieve and generate next internal key.", msg, e);
        }
    }
    
    /**
     * After key rotation, The key alias of the old next key is stored in
     * database and use as the current key. And the new generated next key will
     * be in key store only.
     */
    @Override
    public void generateInternalCryptoKey(final String comment, final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) throws CryptoException {
        if (useCryptoServer()) {
            throw new IllegalStateException("Iris is configured to use CryptoServer, rotatin internal key on schedule is not allowed!");
        }
        
        final int cryptoTypeId = CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID;
        int cryptoIndex = getCurrentCryptoIndexForTypeId(cryptoTypeId);
        // = createKeyInfo(crypto_type_id, crypto_index);
        final String keyInfo;
        
        final Crypto crypto = getCrypto(cryptoTypeId);
        keyInfo = createKeyInfo(cryptoTypeId, cryptoIndex + 1 + 1);
        // no record in crypto DB
        if (cryptoIndex == 0) {
            // check whether have Initial key in keystore
            if (!crypto.isExistKey(keyInfo)) {
                if (ALLOW_EMPTY_KEYSTORE) {
                    crypto.createAesKey(createKeyInfo(cryptoTypeId, cryptoIndex + 1), AES_KEY_SIZE);
                    crypto.createAesKey(createKeyInfo(cryptoTypeId, cryptoIndex + 1 + 1), AES_KEY_SIZE);
                } else {
                    throw new CryptoException("Can't find initial key for type:" + cryptoTypeId);
                }
            }
        } else {
            // generate the next key in key store
            crypto.createAesKey(keyInfo, AES_KEY_SIZE);
            LOG.info("Next internal key " + keyInfo + " generated in key store.");
        }
        
        // use the old next key as the current key
        cryptoIndex++;
        final CryptoKey cryptoKey = new CryptoKey();
        cryptoKey.setKeyType(cryptoTypeId);
        cryptoKey.setKeyIndex(cryptoIndex);
        // Create Date
        final Date createDate = Calendar.getInstance().getTime();
        cryptoKey.setCreatedGmt(createDate);
        // Get Expiry
        final Date expiryDate = getExpirationDateForTypeId(createDate, cryptoTypeId);
        cryptoKey.setExpiryGmt(expiryDate);
        
        cryptoKey.setComment(comment);
        
        /*
         * Prepare CryptoKeyHash objects.
         */
        final Set<CryptoKeyHash> cryptoKeyHashes = new HashSet<CryptoKeyHash>();
        Integer algId = null;
        HashAlgorithmData hashAlgorithmData = null;
        final Iterator<Integer> algIdsIter = hashAlgorithmDataMap.keySet().iterator();
        while (algIdsIter.hasNext()) {
            algId = algIdsIter.next();
            hashAlgorithmData = hashAlgorithmDataMap.get(algId);
            
            final CryptoKeyHash cryptoKeyHash = new CryptoKeyHash();
            cryptoKeyHash.setCryptoKey(cryptoKey);
            cryptoKeyHash.setCreatedGmt(createDate);
            cryptoKeyHash.setHashAlgorithmType(hashAlgorithmData.getHashAlgorithmType());
            cryptoKeyHashes.add(cryptoKeyHash);
        }
        cryptoKey.setCryptoKeyHashs(cryptoKeyHashes);
        
        // Save those info to CryptoKey table
        saveCryptoKey(cryptoKey);
        
        // Clean the currentkey in map
        this.currentKeyMap.remove(cryptoTypeId);
        // Clean deprecated key collection in maps
        this.deprecatedKeysMap.remove(cryptoTypeId);
        
    }
    
    private Date getExpirationDateForTypeId(final Date createDate, final int cryptoTypeId) throws CryptoException {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(createDate);
        switch (cryptoTypeId) {
            case CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID:
                cal.add(Calendar.MONTH, 12);
                return cal.getTime();
            case CURRENT_LOCAL_CREDIT_CARD_CRYPTO_TYPE_ID:
                cal.add(Calendar.MONTH, 12);
                return cal.getTime();
            default:
                break;
        }
        throw new CryptoException("Unknown Crypto Type: " + cryptoTypeId);
    }
    
    @Override
    public void checkAndProcessExternalCryptoKey(final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) {
        if (useCryptoServer()) {
            throw new IllegalStateException("External Key expiration check is omitted because Iris is using CryptoServer");
        } else {
            final Date expiryDate;
            final GregorianCalendar validCal;
            final GregorianCalendar curCal;
            final CryptoKey cryptoKey;
            try {
                LOG.info("Checking external crypto key is in use for 6 months.");
                cryptoKey = this.getCurrentCryptKey(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
                if (cryptoKey == null) {
                    throw new CryptoException("Can't find current external key in db.");
                }
                expiryDate = cryptoKey.getExpiryGmt();
                validCal = new GregorianCalendar();
                validCal.setTime(expiryDate);
                validCal.add(Calendar.MONTH, -6);
                curCal = new GregorianCalendar();
                if (curCal.after(validCal)) {
                    // key expiried, create new key
                    generateExternalCryptoKey("External Key is expired, automatically change new key.", hashAlgorithmDataMap);
                    
                } else {
                    LOG.info("Valid external crypto key.");
                }
            } catch (Exception e) {
                this.sendKeyProcessingAdminWarnAlert(MailerService.EXCEPTION, "Unable to check and process external cryptokey", e);
                LOG.error("Unable to check and process external cryptokey", e);
            }
        }
    }
    
    @Override
    public void checkAndRemoveDeprecatedExternalCryptoKey() {
        if (useCryptoServer()) {
            throw new IllegalStateException("External Key removal check is omitted because Iris is using CryptoServer");
        } else {
            Date createDate;
            GregorianCalendar validCal;
            GregorianCalendar curCal;
            CryptoKey cryptoKey;
            String keyInfo;
            try {
                LOG.info("Checking deprecated external crypto key is in use for 12 months.");
                final Collection<CryptoKey> cryptoKeys = this.getDeprecatedKeys(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
                if ((cryptoKeys == null) || (cryptoKeys.size() < 1)) {
                    return;
                }
                final Iterator<CryptoKey> it = cryptoKeys.iterator();
                while (it.hasNext()) {
                    cryptoKey = (CryptoKey) (it.next());
                    createDate = cryptoKey.getCreatedGmt();
                    validCal = new GregorianCalendar();
                    validCal.setTime(createDate);
                    validCal.add(Calendar.MONTH, 12);
                    curCal = new GregorianCalendar();
                    if (curCal.after(validCal)) {
                        // deperated key expiried, remove from keystore
                        final Crypto crypto = getCrypto(cryptoKey.getKeyType());
                        keyInfo = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex());
                        
                        try {
                            crypto.deleteKey(keyInfo);
                            
                        } catch (CryptoException ce) {
                            /*
                             * If CryptoException has no cause or message doesn't
                             * exist or message is not about 'Alias not found...',
                             * throw the exception to caller. Otherwise since the
                             * key no longer exist in keystore, continue the process
                             * to clean up CryptoKey record.
                             */
                            if (ce.getCause() == null || StringUtils.isBlank(ce.getCause().getMessage())
                                || ce.getCause().getMessage().indexOf("KeyStoreException: Alias not found") == -1) {
                                throw ce;
                            }
                        }
                        final Date date = new Date();
                        cryptoKey.setInfo(WebCoreConstants.EMPTY_STRING);
                        cryptoKey.setComment("deleted on " + date);
                        cryptoKey.setStatus(CryptoConstants.CRYPTOKEY_STATUS_DELETED);
                        updateCryptoKey(cryptoKey);
                        final Set<CryptoKeyHash> cryptoKeyHashes = cryptoKey.getCryptoKeyHashs();
                        for (CryptoKeyHash cryptoKeyHash : cryptoKeyHashes) {
                            cryptoKeyHash.setHash(WebCoreConstants.EMPTY_STRING);
                            cryptoKeyHash.setNextHash(WebCoreConstants.EMPTY_STRING);
                            cryptoKeyHash.setSignature(WebCoreConstants.EMPTY_STRING);
                            this.entityDao.update(cryptoKeyHash);
                        }
                        
                        final StringBuilder bdr = new StringBuilder();
                        bdr.append("!!! external key: (").append(cryptoKey.getKeyType()).append(", ").append(cryptoKey.getKeyIndex());
                        bdr.append(") expired! Deleted on ").append(date).append(" !!!");
                        LOG.info(bdr.toString());
                    }
                }
            } catch (Exception e) {
                this.sendKeyProcessingAdminWarnAlert(MailerService.EXCEPTION, "Unable to check and remove deperated external cryptokey", e);
                LOG.error("Unable to check and remove deperated external cryptokey", e);
            }
        }
    }
    
    @Override
    public boolean checkAndProcessInternalCryptoKey(final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) {
        if (useCryptoServer()) {
            throw new IllegalStateException("Internal Key expiration check is omitted because Iris is using CryptoServer");
        } else {
            final Date expiryDate;
            final GregorianCalendar validCal;
            final GregorianCalendar curCal;
            CryptoKey cryptoKey;
            
            try {
                cryptoKey = this.getCurrentCryptKey(CryptoService.PURPOSE_CREDIT_CARD_LOCAL);
                if (cryptoKey == null) {
                    if (ALLOW_EMPTY_KEYSTORE) {
                        this.generateInternalCryptoKey("Initial Internal Key.", hashAlgorithmDataMap);
                        
                        cryptoKey = this.getCurrentCryptKey(CryptoService.PURPOSE_CREDIT_CARD_LOCAL);
                    } else {
                        throw new CryptoException("Can't find current internal key in db.");
                    }
                }
                
                LOG.info("Checking if next internal key exists in key store.");
                final String nextKeyAlias = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex() + 1);
                try {
                    final Cipher cipher = getCipher(nextKeyAlias, Cipher.ENCRYPT_MODE);
                    if (cipher == null) {
                        // generate the next key and store in key store
                        generateNextInternalKeyInKeyStore();
                    }
                } catch (Exception e) {
                    if (e instanceof CryptoException) {
                        // generate the next key and store in key store
                        generateNextInternalKeyInKeyStore();
                    } else {
                        final String msg = "Unable to retrieve next internal key " + nextKeyAlias + " from key store.";
                        this.mailerService.sendAdminAlert("Unable to retrieve next internal key.", msg, e);
                    }
                }
                
                LOG.info("Checking internal crypto key is in use for 6 months");
                expiryDate = cryptoKey.getExpiryGmt();
                validCal = new GregorianCalendar();
                validCal.setTime(expiryDate);
                validCal.add(Calendar.MONTH, -6);
                curCal = new GregorianCalendar();
                if (curCal.after(validCal)) {
                    // key expiried, create new key
                    this.generateInternalCryptoKey("Internal Key is expiried, Automatically change new key.", hashAlgorithmDataMap);
                    // Auto reencrypt bad credit card in BadCreditCardProcessor's
                    // reencryptBadCreditCardData
                    // Auto reencrypt preauths in CardProcessingService's
                    // processOutstandingTransactionSettlementsAndCleanupCardData
                    return true;
                } else {
                    LOG.info("Valid internal crypto key.");
                }
            } catch (Exception e) {
                this.sendKeyProcessingAdminWarnAlert(MailerService.EXCEPTION, "Unable to check and process cryptokey", e);
                LOG.error("Unable to check and process cryptokey", e);
            }
        }
        return false;
    }
    
    /**
     * Remove Internal key from keystore if the date is exceed expiry + 7 days
     */
    @Override
    public void checkAndRemoveInternalCryptoKey() {
        if (useCryptoServer()) {
            throw new IllegalStateException("Internal Key removal check is omitted because Iris is using CryptoServer");
        } else {
            final Date expiryDate;
            final GregorianCalendar validCal;
            final GregorianCalendar curCal;
            final CryptoKey cryptoKey;
            final String keyInfo;
            try {
                LOG.info("Checking internal crypto key is in use for 12 months.");
                cryptoKey = this.getPrevCryptKey(CryptoService.PURPOSE_CREDIT_CARD_LOCAL);
                if (cryptoKey == null) {
                    return;
                }
                
                expiryDate = cryptoKey.getExpiryGmt();
                validCal = new GregorianCalendar();
                validCal.setTime(expiryDate);
                // validCal.add(Calendar.DAY_OF_YEAR,
                // DEFAULT_DELAY_INTERVAL_TO_REMOVE_INTERNAL_KEY); // Delay 7
                // days
                // after expiry
                curCal = new GregorianCalendar();
                if (curCal.after(validCal)) {
                    // internal key expiried, remove from keystore
                    final Crypto crypto = getCrypto(cryptoKey.getKeyType());
                    keyInfo = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex());
                    crypto.deleteKey(keyInfo);
                    // Change the Status flag of the cryptoKey;
                    cryptoKey.setStatus(CryptoConstants.CRYPTOKEY_STATUS_DELETED);
                    updateCryptoKey(cryptoKey);
                }
            } catch (Exception e) {
                this.sendKeyProcessingAdminWarnAlert(MailerService.EXCEPTION, "Unable to check and remove internal cryptokey", e);
                LOG.error("Unable to check and remove internal external cryptokey", e);
            }
        }
    }
    
    @Override
    public boolean isKeyStoreUpToDate() {
        if (useCryptoServer()) {
            return true;
        }
        
        CryptoKey cryptoKey;
        boolean nextInternalExist = false;
        boolean nextExternalExist = false;
        Exception firstException = null;
        
        // Refresh Crypto Directory this is to skip tomcat restart when keystore
        // get replaced.
        CryptoUtil.reloadCryptoDirectory(this.emsPropertiesService);
        
        try {
            // check internal key
            cryptoKey = this.getCurrentCryptKey(CryptoService.PURPOSE_CREDIT_CARD_LOCAL);
            if (cryptoKey == null) {
                final String msg = "Unable to retrieve current internal key.";
                this.mailerService.sendAdminAlert(msg, msg, null);
                return false;
            }
            
            final String nextInternalKeyAlias = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex() + 1);
            LOG.info("Checking if internal key " + nextInternalKeyAlias + " exists in key store.");
            Cipher cipher;
            try {
                cipher = getCipher(nextInternalKeyAlias, Cipher.DECRYPT_MODE);
                if (cipher == null) {
                    nextInternalExist = false;
                } else {
                    nextInternalExist = true;
                }
            } catch (Exception e) {
                firstException = e;
            }
            
            // check external key
            cryptoKey = this.getCurrentCryptKey(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
            if (cryptoKey == null) {
                final String msg = "Unable to retrieve current external key.";
                this.mailerService.sendAdminAlert(msg, msg, null);
                return false;
            }
            
            final String nextExternalKeyAlias = createKeyInfo(cryptoKey.getKeyType(), cryptoKey.getKeyIndex() + 1);
            LOG.info("Checking if external key " + nextExternalKeyAlias + " exists in key store.");
            try {
                cipher = getCipher(nextExternalKeyAlias, Cipher.DECRYPT_MODE);
                if (cipher == null) {
                    nextExternalExist = false;
                } else {
                    nextExternalExist = true;
                }
            } catch (Exception e) {
                if (firstException == null) {
                    firstException = e;
                }
            }
        } catch (CryptoException e) {
            final String msg = "Unable to retrieve next internal or external key.";
            this.mailerService.sendAdminAlert("msg", msg, e);
            return false;
        }
        
        if (!nextInternalExist || !nextExternalExist) {
            final String subject = "Unable to retrieve next internal or external key.";
            final String msg = subject + " Next internal key in key store: " + nextInternalExist + ". Next external key in key store: "
                               + nextExternalExist + ".";
            this.mailerService.sendAdminAlert(subject, msg, firstException);
        } else {
            LOG.info("Next internal key in key store: " + nextInternalExist + ". Next external key in keystore: " + nextExternalExist + ".");
        }
        return nextInternalExist && nextExternalExist;
    }
    
    @Override
    public boolean isNewKeyForData(final String data) throws CryptoException {
        if (data == null || data.length() <= KEY_INFO_LENGTH) {
            throw new CryptoException("Invalid data: Incorrect length");
        }
        
        final String keyInfo = data.substring(0, KEY_INFO_LENGTH);
        if (!CryptoConstants.PTTRN_INTERNAL_KEY.matcher(keyInfo).matches()) {
            throw new UnsupportedOperationException("This method only support Internal Key!");
        }
        
        final String currentKey = currentInternalKey();
        
        return keyInfo.compareTo(currentKey) < 0;
    }
    
    private String generateSignature(final String keyName, final String keyBundle) throws CryptoException {
        final String emsName = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_ACCOUNT_NAME);
        final SigningService signingService =
                DPTWSUtil.getSigningService(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SS_LOCATION), emsName,
                                            this.emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_ACCOUNT_TOKEN));
        final GetSignStringType signStringType = new GetSignStringType();
        signStringType.setSignString(keyBundle);
        signStringType.setComments("Signing " + keyName + "'s public key by " + emsName);
        final String signature;
        try {
            signature = signingService.getSignString(signStringType);
        } catch (GetSignStringFaultMessage gex) {
            throw new CryptoException(gex.getMessage(), gex);
        }
        return signature;
    }
    
    private String generateSignatureV2(final String keyName, final String keyBundle, final int hashAlgorithmTypeId) throws CryptoException {
        final String signingServiceUrl = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SIGNING_SERVICE_LOCATION);
        final String signingServiceAcctName = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SIGNING_SERVICE_ACCOUNT_NAME);
        final String signingServiceToken = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SIGNING_SERVICE_TOKEN);
        if (StringUtils.isBlank(signingServiceUrl) || StringUtils.isBlank(signingServiceAcctName) || StringUtils.isBlank(signingServiceToken)) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("signingServiceAcctName: ").append(signingServiceAcctName).append(", signingServiceToken: ");
            bdr.append(signingServiceToken).append(", or signingServiceUrl: ").append(signingServiceUrl).append(", is missing.");
            LOG.error(bdr.toString());
            throw new CryptoException(bdr.toString());
        }
        
        final EasyAES128 aes = new EasyAES128();
        final String plainPasswd = aes.decryptBase64(signingServiceToken);
        
        final SigningServiceV2 signingServiceV2 = DPTWSUtil.getSigningServiceV2(signingServiceUrl, signingServiceAcctName, plainPasswd);
        
        final com.digitalpaytech.ws.signingservicev2.GetSignStringType signStringType =
                new com.digitalpaytech.ws.signingservicev2.GetSignStringType();
        signStringType.setSignString(keyBundle);
        signStringType.setType(hashAlgorithmTypeId);
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Signing ").append(keyName).append("'s public key by ").append(signingServiceAcctName);
        signStringType.setComments(bdr.toString());
        final String signature;
        try {
            signature = signingServiceV2.getSignString(signStringType);
        } catch (com.digitalpaytech.ws.signingservicev2.GetSignStringFaultMessage gex) {
            throw new CryptoException(gex.getMessage(), gex);
        } catch (GetUnsupportedHashRequestFaultMessage guhrfm) {
            throw new CryptoException(guhrfm.getMessage(), guhrfm);
        }
        return signature;
    }
    
    private void sendKeyProcessingAdminWarnAlert(final String subject, final String message, final Exception e) {
        this.mailerService.sendAdminWarnAlert(CRYPOTO_KEY_PROCESSING_ADMIN_ALERT_NAME + subject, message, e);
    }
    
    @Override
    public String decryptData(final String data, final int pointOfSaleId, final Date purchasedDate) throws CryptoException {
        final String decrypted = decryptData(data);
        
        final String psKeyAlias = data.substring(0, KEY_INFO_LENGTH);
        isUseOldKey(psKeyAlias, pointOfSaleId, purchasedDate);
        
        return decrypted;
    }
    
    /*
     * @param psKeyAlias e.g. "000033" for Scala, "004002" for Link.
     */
    private boolean isUseOldKey(final String psKeyAlias, final int pointOfSaleId, final Date purchasedDate) throws CryptoException {
        boolean isUseOldKey = false;
        final int keyType = Integer.parseInt(StringUtils.substring(psKeyAlias, 2, 3));
        if (keyType == CURRENT_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID
                || keyType == CURRENT_LINK_REMOTE_CREDIT_CARD_CRYPTO_TYPE_ID) {
            
            /*
             * currentExternalKey method retrieves key from Link or Scala crypto service depending on EmsProperties.UseLinkCrypto.
             */
            final PosServiceState posServiceState = this.posServiceStateService.findPosServiceStateById(pointOfSaleId, true);
            final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
            
            final String currKeyAlias = currentExternalKey(posServiceState.isIsSwitchLinkCrypto(), pointOfSale.getSerialNumber());
            // Starting 8.0 all customers have been migrated to Link Crypto.
            final boolean isPsKeyLink = true;
            
            if (psKeyAlias.equals(currKeyAlias)) {
                LOG.info("+++ +++ PointOfSale(Id: " + pointOfSaleId + ") uses the latest key: " + currKeyAlias + ", keyAlias from track2: "
                         + psKeyAlias);
                
                if (updateIsUsingLinkCryptoIfNeeded(posServiceState, isPsKeyLink, pointOfSale.getSerialNumber())) {
                    this.posServiceStateService.updatePosServiceState(posServiceState);
                }
            } else {
                /*
                 * Pay station might use an old key but customer has migrated to Link (Customer.IsSwitchLinkCrypto = 1).
                 */
                final RSAKeyInfo keyInfo = retrieveKeyInfo(psKeyAlias);
                if (keyInfo == null) {
                    // the old key is not in database
                    final String subject = "Old public key detected for PointOfSaleId: " + pointOfSaleId;
                    final String message = "PointOfSale(Id: " + pointOfSaleId + ") does not use latest key: " + currKeyAlias + ", but use "
                                           + psKeyAlias + ". Key " + psKeyAlias + " is not found in database";
                    LOG.warn("+++ +++ " + message + " +++");
                    this.mailerService.sendAdminWarnAlert(subject, message, null);
                    isUseOldKey = true;
                    
                    if (psKeyAlias.startsWith(CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX)
                        || psKeyAlias.startsWith(CryptoConstants.IRIS_INTERNAL_KEY_PREFIX)) {
                        LOG.info("+++ +++ Cannot retrieve RSAKeyInfo using psKeyAlias: " + psKeyAlias + ", POSServiceState: " + posServiceState);
                        /*
                         * Set POSServiceState.IsUsingLinkCrypto to "false" if the key is from Scala, to "true" if the key is from Link.
                         * Only save if the value is changing.
                         */
                        updateIsUsingLinkCryptoIfNeeded(posServiceState, isPsKeyLink, pointOfSale.getSerialNumber());
                        posServiceState.setIsNewPublicKey(true);
                        this.posServiceStateService.updatePosServiceState(posServiceState);
                        
                        LOG.info("+++ +++ Updated database table POSServiceState: " + posServiceState);
                    }
                } else {
                    /*
                     * EMS-2302 Set 'new encryption key' flag for any pay
                     * station that uses a key older than 8.5 months
                     */
                    final Calendar creationCal = Calendar.getInstance();
                    final Date now = creationCal.getTime();
                    
                    creationCal.setTime(keyInfo.getCreateTime());
                    creationCal.add(Calendar.HOUR_OF_DAY, 48);
                    if (now.after(creationCal.getTime())) {
                        LOG.info("+++ +++ now after creationCal: " + creationCal + ", POSServiceState: " + posServiceState);
                        
                        updateIsUsingLinkCryptoIfNeeded(posServiceState, isPsKeyLink, pointOfSale.getSerialNumber());
                        posServiceState.setIsNewPublicKey(true);
                        this.posServiceStateService.updatePosServiceState(posServiceState);
                        
                        LOG.info("+++ +++ Updated database table POSServiceState: " + posServiceState);
                    }
                    
                    creationCal.setTime(keyInfo.getCreateTime());
                    creationCal.add(Calendar.MONTH, 9);
                    LOG.info("+++ +++ old key creation date(+9 month): " + creationCal.getTime() + ", current date: " + now + " +++");
                    if (now.after(creationCal.getTime())) {
                        final String subject = "Old public key detected for PointOfSale:" + pointOfSaleId;
                        final String message = "PointOfSale(Id: " + pointOfSaleId + ") does not use latest key, but use " + psKeyAlias
                                               + " which is more than 9 months old.";
                        LOG.warn("+++ +++ " + message + " +++");
                        this.mailerService.sendAdminWarnAlert(subject, message, null);
                        isUseOldKey = true;
                    } else {
                        LOG.warn("+++ +++ PointOfSale(Id: " + pointOfSaleId + ") does not use latest key, but use " + psKeyAlias + ".");
                    }
                }
            }
        }
        
        return isUseOldKey;
    }
    
    /*
     * If POSServiceState isUsingLinkCrypto is false and since isPsKeyLink is now always true, the logic will call setIsUsingLinkCrypto to true.
     * @param isPsKeyLink Starting from 8.0 isPsKeyLink is true.
     */
    private boolean updateIsUsingLinkCryptoIfNeeded(final PosServiceState posServiceState, final boolean isPsKeyLink, final String serialNumber) {
        if (isPsKeyLink != posServiceState.isIsUsingLinkCrypto()) {
            posServiceState.setIsUsingLinkCrypto(isPsKeyLink);
            LOG.info("+++ +++ Pay station with serialnumber: " + serialNumber + " setIsUsingLinkCrypto(true)");
            return true;
        }
        return false;
    }
    
    private CryptoKey findCurrentCryptoKeyByType(final int type) {
        final List<CryptoKey> keys = findCryptoKeysWithCryptoKeyHashesByType(type);
        if (keys.size() > 0) {
            // Get the first element which has latest data in the list.
            return keys.get(0);
        }
        return null;
    }
    
    /**
     * Search CryptoKey and associated CryptoKeyHash.
     * 
     * @param type
     *            key type
     * @return Collection<CryptoKey> a collection of CryptoKey objects which
     *         have associated CryptoKeyHash object.
     */
    private List<CryptoKey> findCryptoKeysWithCryptoKeyHashesByType(final int type) {
        final String[] params = new String[] { "keyType" };
        final Object[] values = new Object[] { type };
        final List<Object[]> list = this.entityDao.findByNamedQuery("CryptoKey.findCurrentWithCryptoKeyHashByType", params, values, 0, 0);
        return groupCryptoKeyAndCryptoKeyHash(list);
    }
    
    @SuppressWarnings("unchecked")
    private Collection<CryptoKey> findDeprecatedKeysByType(final int type) {
        final Collection<CryptoKey> keys = this.entityDao.findByNamedQuery("CryptoKey.findDeprecatedByType", new Object[] { type, (byte) 1 }, 0, 0);
        return findDeprecatedKeysByType(keys);
    }
    
    private Collection<CryptoKey> findDeprecatedKeysByType(final Collection<CryptoKey> keys) {
        final Collection<CryptoKey> retKeys = new ArrayList<CryptoKey>();
        
        if (keys.size() > 0) {
            final Iterator<CryptoKey> it = keys.iterator();
            // remove the current key (the last one)
            CryptoKey key = it.next();
            while (key != null) {
                if (it.hasNext()) {
                    retKeys.add(key);
                    key = it.next();
                } else {
                    key = null;
                }
            }
        }
        
        return retKeys;
    }
    
    private List<CryptoKey> groupCryptoKeyAndCryptoKeyHash(final List<Object[]> list) {
        final List<CryptoKey> keys = new ArrayList<CryptoKey>(list.size());
        final Iterator<Object[]> iter = list.iterator();
        while (iter.hasNext()) {
            final Object[] objs = iter.next();
            final CryptoKey key = (CryptoKey) objs[0];
            final CryptoKeyHash keyHash = (CryptoKeyHash) objs[1];
            
            if (addToSameCryptoKey(keys, key, keyHash)) {
                continue;
            } else {
                // Set CryptoKey to CryptoHashSet.
                keyHash.setCryptoKey(key);
                
                // Place CryptoKeyHash into a HashSet and set in CryptoKey
                final Set<CryptoKeyHash> set = new HashSet<CryptoKeyHash>(1);
                set.add(keyHash);
                key.setCryptoKeyHashs(set);
                
                keys.add(key);
            }
        }
        return keys;
    }
    
    private boolean addToSameCryptoKey(final List<CryptoKey> list, final CryptoKey cryptoKey, final CryptoKeyHash keyHash) {
        for (CryptoKey key : list) {
            if (key.getId().intValue() == cryptoKey.getId().intValue()) {
                key.getCryptoKeyHashs().add(keyHash);
                return true;
            }
        }
        return false;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCryptoKey(final CryptoKey cryptoKey) {
        if (useCryptoServer()) {
            throw new IllegalStateException("Cannot save CryptoKey because Iris is configured to use CryptoServer!");
        }
        
        // Save those info to CryptoKey table and manually insert CryptoKeyHash
        // records.
        this.entityDao.save(cryptoKey);
        
        // It's an external key
        if (cryptoKey.getKeyType() == RSA_2048_BIT_CREDIT_CARD_TYPE_ID) {
            // Trigger to set service state flag, notice PS a new key created
            final Collection<PosServiceState> posServiceStates = this.posServiceStateService.findAllPosServiceStates(true);
            if (posServiceStates != null && !posServiceStates.isEmpty()) {
                for (PosServiceState posServiceState : posServiceStates) {
                    posServiceState.setIsNewPublicKey(true);
                    this.posServiceStateService.updatePosServiceState(posServiceState);
                }
            }
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCryptoKey(final CryptoKey cryptoKey) {
        if (useCryptoServer()) {
            throw new IllegalStateException("Cannot update CryptoKey because Iris is configured to use CryptoServer!");
        }
        
        this.entityDao.update(cryptoKey);
    }
    
    @Override
    public int countSpareKey(final CryptoKey key) throws CryptoException {
        int result = 0;
        if (!useCryptoServer()) {
            int keyIdx = key.getKeyIndex() + 1;
            final Crypto crypto = getCrypto(key.getKeyType());
            if (crypto == null) {
                LOG.error("Could not locate Crypto !");
            } else {
                while (crypto.isExistKey(createKeyInfo(key.getKeyType(), keyIdx))) {
                    ++keyIdx;
                    ++result;
                }
            }
        }
        
        return result;
    }
    
    @Override
    public int getNearestKeyIndex(final CryptoKey maxKey) throws CryptoException {
        int keyIdx = 0;
        if ((useCryptoServer()) && (maxKey != null)) {
            keyIdx = maxKey.getKeyIndex();
            
            final Crypto crypto = getCrypto(maxKey.getKeyType());
            if (crypto == null) {
                LOG.error("Could not locate Crypto !");
            } else {
                while (!crypto.isExistKey(createKeyInfo(maxKey.getKeyType(), keyIdx))) {
                    --keyIdx;
                }
            }
        }
        
        return keyIdx;
    }
    
    private String getNextHash(final CryptoKey cryptoKey, final int hashAlgorithmTypeId) {
        // 1 is next hash.
        return getCurrentOrNextHash(cryptoKey, hashAlgorithmTypeId, NEXT_HASH);
    }
    
    /**
     * @param currentOrNext
     *            int 0 is current, 1 is next.
     */
    private String getCurrentOrNextHash(final CryptoKey cryptoKey, final int hashAlgorithmTypeId, final int currentOrNext) {
        CryptoKeyHash ckh;
        final Iterator<CryptoKeyHash> iter = cryptoKey.getCryptoKeyHashs().iterator();
        while (iter.hasNext()) {
            ckh = iter.next();
            if (ckh.getHashAlgorithmType().getId() == hashAlgorithmTypeId && currentOrNext == CURRENT_HASH) {
                return ckh.getHash();
            } else if (ckh.getHashAlgorithmType().getId() == hashAlgorithmTypeId && currentOrNext == NEXT_HASH) {
                return ckh.getNextHash();
            }
        }
        return WebCoreConstants.EMPTY_STRING;
    }
    
    @Override
    public void checkAndUpdateSigningKeyStore(final List<String> activeSignAlgorithms) {
        if (useCryptoServer()) {
            throw new IllegalStateException("Generating signing key is not allow because Iris is configured to use CryptoServer!");
        }
        
        Crypto crypto = null;
        try {
            crypto = getCrypto(SIGNING_SERVER_CRYPTO_ID);
            LOG.info("Got" + crypto.getJceProvider() + "successfully");
            final boolean isSigningKeyExpiring = crypto.checkAndGenerateSigningKey(activeSignAlgorithms, RSA_SIGNING_KEY_SIZE);
            if (isSigningKeyExpiring) {
                
            }
        } catch (CryptoException e) {
            LOG.error(e);
        }
        
    }
    
    @Override
    public CryptoKeyListInfo listActiveKeys(final int purpose) throws CryptoException {
        
        if (useCryptoServer()) {
            return buildCryptoKeyList();
            
        } else {
            final CryptoKeyListInfo result = new CryptoKeyListInfo();
            // *** Order of Key Index of these two HQLs must always be the same.
            // Otherwise, the logic to construct KeyInfo will go wrong ***
            @SuppressWarnings("unchecked")
            final List<CryptoKey> cryptoKeysList =
                    this.entityDao.getNamedQuery("CryptoKey.findActiveKeysByType").setInteger("keyType", KEY_TYPE_EXT).list();
            
            @SuppressWarnings("unchecked")
            final List<CryptoKeyHash> hashesList =
                    this.entityDao.getNamedQuery("CryptoKeyHash.findActiveHashesByKeyType").setInteger("keyType", KEY_TYPE_EXT).list();
            
            final ArrayList<CryptoKeyInfo> buffer = new ArrayList<CryptoKeyInfo>(hashesList.size());
            final Iterator<CryptoKey> cryptoKeysItr = cryptoKeysList.iterator();
            final Iterator<CryptoKeyHash> hashesItr = hashesList.iterator();
            
            CryptoKeyInfo current = null;
            CryptoKeyInfo future = null;
            
            CryptoKey currentKey = null;
            CryptoKeyHash currentHash = (hashesItr.hasNext()) ? hashesItr.next() : null;
            
            int displayIndex = KEY_DISPLAY_INDEX_START;
            
            while (cryptoKeysItr.hasNext()) {
                currentKey = cryptoKeysItr.next();
                current = new CryptoKeyInfo();
                current.setCryptoKeyId(currentKey.getId());
                current.setKeyType(currentKey.getKeyType());
                current.setKeyIndex(currentKey.getKeyIndex());
                
                current.setSource(CryptoConstants.IRIS_KEY_SOURCE);
                current.setDisplayIndex(displayIndex);
                displayIndex++;
                
                if (!cryptoKeysItr.hasNext()) {
                    future = new CryptoKeyInfo();
                    future.setKeyType(currentKey.getKeyType());
                    future.setKeyIndex(currentKey.getKeyIndex() + 1);
                }
                
                while ((currentHash != null) && (currentHash.getCryptoKey().getKeyIndex() <= currentKey.getKeyIndex())) {
                    if (currentHash.getCryptoKey().getKeyIndex() == currentKey.getKeyIndex()) {
                        current.setHash(currentHash.getHashAlgorithmType().getId(), currentHash.getHash());
                        if (future != null) {
                            future.setHash(currentHash.getHashAlgorithmType().getId(), currentHash.getNextHash());
                        }
                    }
                    
                    if (hashesItr.hasNext()) {
                        currentHash = hashesItr.next();
                    } else {
                        currentHash = null;
                    }
                }
                
                if (cryptoKeysItr.hasNext()) {
                    buffer.add(current);
                }
            }
            
            result.setPreviousKeys(buffer);
            result.setCurrentScalaKey(current);
            result.setFutureScalaKey(future);
            return result;
        }
        
    }
    
    private void setFutureAndCurrentKeys(final CryptoKeyListInfo result, final List<KeyIdentity> linkKeyIdentities,
        final Consumer<CryptoKeyInfo> futureSetter, final Consumer<CryptoKeyInfo> currentSetter) {
        if (CollectionUtils.isNotEmpty(linkKeyIdentities)) {
            if (linkKeyIdentities.size() > 1) {
                futureSetter.accept(transform(linkKeyIdentities.remove(linkKeyIdentities.size() - 1), true, 0));
            }
            currentSetter.accept(transform(linkKeyIdentities.remove(linkKeyIdentities.size() - 1), true, 0));
        }
    }
    
    @Override
    public KeyValuePair<Integer, Integer> splitKeyInfo(final String keyName) {
        KeyValuePair<Integer, Integer> result = null;
        
        int pttrnIdx = -1;
        while ((result == null) && (++pttrnIdx < PTTRN_KEYS.length)) {
            final Matcher m = PTTRN_KEYS[pttrnIdx].matcher(keyName);
            if (m.matches()) {
                result = new KeyValuePair<Integer, Integer>(Integer.valueOf(m.group(1)), Integer.valueOf(m.group(2)));
            }
        }
        
        return result;
    }
    
    @Override
    public RSAKeyInfo retrieveKeyInfo(final String keyAlias) throws CryptoException {
        RSAKeyInfo result = null;
        
        if (useCryptoServer()) {
            String jsonPackage = null;
            
            if (StringUtils.isNotBlank(keyAlias) && isLinkCryptoKey(keyAlias)) {
                try {
                    jsonPackage = this.linkCryptoClient.keyInfo(keyAlias).execute().toString(Charset.defaultCharset());
                } catch (Exception ce) {
                    LOG.error("Marked key '" + keyAlias
                              + "' as non-existing key, we are not sure if there is other problem while reaching for LINK crypto-server", ce);
                }
                
                if (jsonPackage != null) {
                    try {
                        result = this.json.deserialize(jsonPackage, RSAKeyInfo.class);
                    } catch (JsonException ioe) {
                        throw new CryptoException("Failed to deserialize RSAKeyInfo JSON for LINK Crypto", ioe);
                    }
                }
            } else if (StringUtils.isNotBlank(keyAlias) && !isLinkCryptoKey(keyAlias)) {
                try {
                    jsonPackage = this.cryptoClient.keyInfo(keyAlias).execute().toString(Charset.defaultCharset());
                } catch (Exception ce) {
                    LOG.error("Marked key '" + keyAlias
                              + "' as non-existing key, we are not sure if there is other problem while reaching for Scala crypto-server", ce);
                }
                
                if (jsonPackage != null) {
                    try {
                        result = this.json.deserialize(jsonPackage, RSAKeyInfo.class);
                    } catch (JsonException ioe) {
                        throw new CryptoException("Failed to deserialize RSAKeyInfo JSON for Scala Crypto", ioe);
                    }
                }
            } else {
                // key is invalid.
                return null;
            }
        } else {
            final KeyPackage pkg = retrieveKey(keyAlias);
            if (pkg != null) {
                result = pkg.getKey();
            }
        }
        
        return result;
    }
    
    @Override
    public KeyPackage retrieveKey(final String keyAlias) throws CryptoException {
        return retrieveKey(keyAlias, HashAlgorithm.SHA256);
    }
    
    @Override
    public final KeyPackage retrieveKey(final String keyAlias, final HashAlgorithm hashAlg) throws CryptoException {
        KeyPackage result = null;
        String jsonPackage = null;
        final String hashAlgorithm = formatSignatureAlgorithm(hashAlg);
        if (useCryptoServer()
            && (keyAlias.startsWith(CryptoConstants.LINK_EXTERNAL_KEY_PREFIX) || keyAlias.startsWith(CryptoConstants.LINK_INTERNAL_KEY_PREFIX))) {
            try {
                jsonPackage = this.linkCryptoClient.export(keyAlias, hashAlgorithm).execute().toString(Charset.defaultCharset());
            } catch (Exception ce) {
                throw new CryptoException("Could not retrieve key package from linkCryptoClient for: " + keyAlias + ", hashAlgorithm: " + hashAlgorithm, ce);
            }
            
            try {
                result = this.json.deserialize(jsonPackage, KeyPackage.class);
            } catch (JsonException ioe) {
                throw new CryptoException("Failed to deserialize key package JSON for link crypto ", ioe);
            }
            
        } else if (useCryptoServer() && (keyAlias.startsWith(CryptoConstants.IRIS_EXTERNAL_KEY_PREFIX)
                                         || keyAlias.startsWith(CryptoConstants.IRIS_INTERNAL_KEY_PREFIX))) {
            try {
                jsonPackage = this.cryptoClient.export(keyAlias, hashAlgorithm).execute().toString(Charset.defaultCharset());
            } catch (Exception ce) {
                throw new CryptoException("Could not retrieve key package from cryptoClient for: " + keyAlias, ce);
            }
            
            try {
                result = this.json.deserialize(jsonPackage, KeyPackage.class);
            } catch (JsonException ioe) {
                throw new CryptoException("Failed to deserialize key package JSON for scala crypto ", ioe);
            }
            
        } else {
            final KeyValuePair<Integer, Integer> targetKey = splitKeyInfo(keyAlias);
            final CryptoKey ck = (CryptoKey) this.entityDao.getNamedQuery("CryptoKey.findCryptoKeyByKeyTypeAndKeyIndex")
                    .setParameter("keyType", targetKey.getKey()).setParameter("keyIndex", targetKey.getValue()).uniqueResult();
            final RSAKeyInfo key = new RSAKeyInfo();
            key.setCreateTime(ck.getCreatedGmt());
            key.setExpiryTime(ck.getExpiryGmt());
            
            final CryptoKeyHash hash = (CryptoKeyHash) this.entityDao.getNamedQuery("CryptoKeyHash.findByCryptoKeyIdTypeAndHashAlgorithmTypeId")
                    .setParameter("cryptoKeyId", ck.getId()).setParameter("hashAlgorithmTypeId", hashAlg.getId()).uniqueResult();
            
            result = new KeyPackage();
            result.setKey(key);
            result.setHash(hash.getHash());
            result.setNextHash(hash.getNextHash());
            result.setSignature(hash.getSignature());
            
            final String[] modEx = ck.getInfo().split(":");
            result.setSignatureSource(MessageFormat.format(FMT_KEY_PACKAGE, MessageFormat.format(FMT_KEY_INFO, keyAlias, modEx[0], modEx[1]),
                                                           key.getExpiryTime(), result.getNextHash()));
        }
        
        return result;
    }
    
    /**
     * For BOSS, EncryptionServiceImpl.getKeyPresentInfo (Iris)
     */
    @Override
    public final String currentExternalKey() throws CryptoException {
        String result = null;
        final boolean useLinkFlag = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.USE_LINK_CRYPTO,
                                                                                        EmsPropertiesService.DEFAULT_USE_LINK_CRYPTO);
        
        if (this.useCryptoServer() && useLinkFlag) {
            try {
                result = this.linkCryptoClient.currentExternalKey().execute().toString(Charset.defaultCharset());
            } catch (Exception e) {
                throw new CryptoException("Unable to determine current ExternalKey from Link CryptoService!", e);
            }
        } else if (this.useCryptoServer() && !useLinkFlag) {
            try {
                result = this.cryptoClient.currentExternalKey().execute().toString(Charset.defaultCharset());
            } catch (HystrixRuntimeException hre) {
                throw new CryptoException("Unable to retrieve currentExternalKey from Scala CryptoService!", hre);
            }
        } else {
            final CryptoKey currKey = getCurrentCryptKey(PURPOSE_CREDIT_CARD_REMOTE);
            if (currKey != null) {
                result = createKeyInfo(currKey.getKeyType(), currKey.getKeyIndex());
            }
        }
        return result;
    }
    
    @Override
    public final String currentExternalKey(final boolean isCryptoLink, final String serialNumber) throws CryptoException {
        String result = null;
        if (this.useCryptoServer()) {
            if (isCryptoLink) {
                try {
                    result = this.linkCryptoClient.currentExternalKey().execute().toString(Charset.defaultCharset());
                } catch (Exception e) {
                    throw new CryptoException("Unable to determine current ExternalKey from Link CryptoService! Serial Number: " + serialNumber, e);
                }
            } else {
                try {
                    result = this.cryptoClient.currentExternalKey().execute().toString(Charset.defaultCharset());
                } catch (HystrixRuntimeException hre) {
                    throw new CryptoException("Unable to retrieve currentExternalKey from Scala CryptoService! Serial Number: " + serialNumber, hre);
                }
            }
        } else {
            final CryptoKey currKey = getCurrentCryptKey(PURPOSE_CREDIT_CARD_REMOTE);
            if (currKey != null) {
                result = createKeyInfo(currKey.getKeyType(), currKey.getKeyIndex());
            }
        }
        return result;
    }
    
    @Override
    public final String currentInternalKey() throws CryptoException {
        String result = null;
        final boolean useLinkFlag = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.USE_LINK_CRYPTO,
                                                                                        EmsPropertiesService.DEFAULT_USE_LINK_CRYPTO);
        
        if (this.useCryptoServer() && useLinkFlag) {
            try {
                result = this.linkCryptoClient.currentInternalKey().execute().toString(Charset.defaultCharset());
            } catch (HystrixRuntimeException hre) {
                throw new CryptoException("Unable to retrieve current Internal Key from Link CryptoService", hre);
            }
        } else if (this.useCryptoServer() && !useLinkFlag) {
            try {
                result = this.cryptoClient.currentInternalKey().execute().toString(Charset.defaultCharset());
            } catch (Exception e) {
                throw new CryptoException("Unable to determine current InternalKey from Scala CryptoService!", e);
            }
        } else {
            final CryptoKey currKey = getCurrentCryptKey(PURPOSE_CREDIT_CARD_LOCAL);
            if (currKey != null) {
                result = createKeyInfo(currKey.getKeyType(), currKey.getKeyIndex());
            }
        }
        return result;
    }
    
    @Override
    public boolean isLinkCryptoKey(final String encryptedData) {
        return StringUtils.isNotBlank(encryptedData) && (encryptedData.startsWith(CryptoConstants.LINK_INTERNAL_KEY_PREFIX)
                                                         || encryptedData.startsWith(CryptoConstants.LINK_EXTERNAL_KEY_PREFIX));
    }
    
    private boolean useCryptoServer() {
        return EncryptionService.CRYPTO_SERVER.equals(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.ENCRYPTION_MODE, null, false));
    }
    
    private String formatSignatureAlgorithm(final HashAlgorithm hashAlg) {
        final StringBuilder result = new StringBuilder(hashAlg.getName());
        if (this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.ENABLE_SIGNING_PROXY, false)) {
            result.append(EncryptionService.SIGNING_PROXY_SUFFIX);
        }
        
        return result.toString();
    }
    
    private CryptoKeyListInfo buildCryptoKeyList() throws CryptoException {
        final CryptoKeyListInfo result = new CryptoKeyListInfo();
        
        final TypeReference<List<KeyIdentity>> listKeyType = new TypeReference<List<KeyIdentity>>() {
        };
        
       
        final List<KeyIdentity> scalaKeyIdentities = loadKeyIdentities(listKeyType, true);
        
        final List<KeyIdentity> linkKeyIdentities = loadKeyIdentities(listKeyType, false);
        
        
        setFutureAndCurrentKeys(result, scalaKeyIdentities, result::setFutureScalaKey, result::setCurrentScalaKey);
        setFutureAndCurrentKeys(result, linkKeyIdentities, result::setFutureLinkKey, result::setCurrentLinkKey);
        
        final List<CryptoKeyInfo> prev = new ArrayList<>();
        
        // this interleaves the key lists and keeps an individual index the UI can display
        int scalaIndex = KEY_DISPLAY_INDEX_START;
        int linkIndex = KEY_DISPLAY_INDEX_START;
        final int linkKeyIdentitiesSize = linkKeyIdentities == null ? 0 : linkKeyIdentities.size();
        final int scalaKeyIdentitiesSize = scalaKeyIdentities == null ? 0 : scalaKeyIdentities.size();
        
        for (int i = 0; i < Math.max(linkKeyIdentitiesSize, scalaKeyIdentitiesSize); i++) {
            if (linkKeyIdentitiesSize > i) {
                prev.add(transform(linkKeyIdentities.get(i), true, linkIndex));
                linkIndex++;
            }
            if (scalaKeyIdentitiesSize > i) {
                prev.add(transform(scalaKeyIdentities.get(i), false, scalaIndex));
                scalaIndex++;
            }
        }
        result.setPreviousKeys(prev);
        return result;
         
    }
    
    /** This method load Link Key Identities, in case of exception, list will be null
     * @param listKeyType
     * @param isLink - true if Scala, false if Link
     * @return
     */
    private List<KeyIdentity> loadKeyIdentities(final TypeReference<List<KeyIdentity>> listKeyType, final boolean isScala)  {
        List<KeyIdentity> keyIdentities = null;
        try {
            keyIdentities = isScala ? this.json.deserialize(this.cryptoClient.listExternalKey().execute().toString(Charset.defaultCharset()), listKeyType) :
                            this.json.deserialize(this.linkCryptoClient.listExternalKeys().execute().toString(Charset.defaultCharset()), listKeyType) ;
        } catch (Exception e) {
            LOG.error("Failed to load Crypto Key Identities", e);
        }
        return keyIdentities;
    }
    private CryptoKeyInfo transform(final KeyIdentity keyId, final boolean isLink, final int displayIndex) {
        final CryptoKeyInfo result = new CryptoKeyInfo();
        result.setKeyType(keyId.getKeyType());
        result.setKeyIndex(keyId.getKeyIndex());
        result.setHash(1, keyId.getHashes().get(HashAlgorithm.SHA1.getName()));
        result.setHash(2, keyId.getHashes().get(HashAlgorithm.SHA256.getName()));
        result.setSource(isLink ? CryptoConstants.LINK_KEY_SOURCE : CryptoConstants.IRIS_KEY_SOURCE);
        result.setDisplayIndex(displayIndex);
        return result;
    }
    
    @Override
    public final String generateDevicePassword(final String serialNumber) {
        final String passwordString = serialNumber + DEVICE_PASSWORD_SALT;
        byte[] hash;
        try {
            final MessageDigest digest = MessageDigest.getInstance(HashAlgorithm.SHA256.getName());
            hash = digest.digest(passwordString.getBytes(StandardCharsets.UTF_8));
            
        } catch (NoSuchAlgorithmException nsae) {
            LOG.error(nsae);
            return null;
        }
        final String password = Base64.encodeBase64String(hash).toString().substring(0, 30).concat("!").replaceAll("\\/", "0");
        final PasswordEncoder encoder = new BCryptPasswordEncoder(DEVICE_PASSWORD_ENCODE_STRENGTH);
        return encoder.encode(password);
    }
}
