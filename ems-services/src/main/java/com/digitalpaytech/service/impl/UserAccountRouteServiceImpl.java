package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.service.UserAccountRouteService;
import com.digitalpaytech.domain.UserAccountRoute;

@Service("userAccountRouteService")
@Transactional(propagation = Propagation.REQUIRED)
public class UserAccountRouteServiceImpl implements UserAccountRouteService {

    @Autowired
    private EntityDao entityDao;
    
	@Override
	public long getUserCountForRoute(int routeId) {
		return (Long)entityDao.findUniqueByNamedQueryAndNamedParam("UserAccountRoute.findUserCountForRouteId", "routeId", routeId);
	}
	
	@Override
	public long getUserCountForRouteAppIdAndCurrentSession(int routeId, int mobileApplicationTypeId, Date currentDate) {
		return (Long)entityDao.findUniqueByNamedQueryAndNamedParam("UserAccountRoute.findUserCountByRouteIdAndApplicationIdAndExpiryDate",  new String[]{"routeId", "mobileApplicationTypeId", "currentDate"}, new Object[]{routeId, mobileApplicationTypeId, currentDate}, true);
	}	
	
	@Override
	public List<UserAccountRoute> findUserAccountRouteByUserAccountId(int userAccountId){
		return entityDao.findByNamedQueryAndNamedParam("UserAccountRoute.findUserAccountRouteByUserAccountId", "userAccountId", userAccountId);
	}
	
	@Override
	public List<UserAccountRoute> findUserAccountRouteByRouteId(int routeId){
		return entityDao.findByNamedQueryAndNamedParam("UserAccountRoute.findUserAccountRouteByRouteId", "routeId", routeId);
	}
	
	@Override
	public List<UserAccountRoute> findUserAccountRouteByUserAccountIdAndRouteId(int userAccountId, int routeId){
		return entityDao.findByNamedQueryAndNamedParam("UserAccountRoute.findUserAccountRouteByUserAccountIdAndRouteId", new String[]{"userAccountId", "routeId"}, new Object[]{userAccountId, routeId});
	}
	
	@Override
	public UserAccountRoute findUserAccountRouteByUserAccountIdAndApplicationId(int userAccountId, int applicationTypeId){
	    return (UserAccountRoute)entityDao.findUniqueByNamedQueryAndNamedParam("UserAccountRoute.findUserAccountRouteByUserAccountAndApplication", new String[]{"userAccountId", "applicationId"}, new Object[]{userAccountId, applicationTypeId}, true);
	}
	
	@Override
	public void saveOrUpdate(UserAccountRoute userAccountRoute){
		entityDao.saveOrUpdate(userAccountRoute);
	}
	
	@Override
	public void delete(UserAccountRoute userAccountRoute){
		entityDao.delete(userAccountRoute);
	}
}
