package com.digitalpaytech.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.FlushMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CustomerAlertTypeProcessor;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("customerAlertTypeProcessor")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerAlertTypeProcessorImpl implements CustomerAlertTypeProcessor {
    private static final String CUSTOMER_EMAIL_BY_CUSTOMER_ID_AND_EMAIL = "CustomerEmail.findCustomerEmailByCustomerIdAndEmail";
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final List<Integer> save(final CustomerAlertType customerAlertType) {
        this.entityDao.save(customerAlertType);
        final Set<CustomerAlertEmail> customerAlertEmails = customerAlertType.getCustomerAlertEmails();
        for (CustomerAlertEmail customerAlertEmail : customerAlertEmails) {
            final CustomerEmail email = customerAlertEmail.getCustomerEmail();
            final CustomerEmail existingEmail = (CustomerEmail) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam(CUSTOMER_EMAIL_BY_CUSTOMER_ID_AND_EMAIL, new String[] { HibernateConstants.CUSTOMER_ID,
                        HibernateConstants.EMAIL, }, new Object[] { email.getCustomer().getId(), email.getEmail(), }, true);
            if (existingEmail == null) {
                this.entityDao.save(email);
            } else {
                customerAlertEmail.setCustomerEmail(existingEmail);
            }
            this.entityDao.save(customerAlertEmail);
        }
        
        final List<Integer> posIdList = new ArrayList<Integer>();
        
        if (customerAlertType.getRoute() == null) {
            /* all pay stations. */
            final Customer customer = (Customer) this.entityDao.get(Customer.class, customerAlertType.getCustomer().getId());
            final Set<PointOfSale> sales = customer.getPointOfSales();
            for (PointOfSale pos : sales) {
                posIdList.add(pos.getId());
            }
        } else {
            
            final List<RoutePOS> routePos = this.entityDao.findByNamedQueryAndNamedParam("RoutePOS.findRoutePOSByRouteId",
                                                                                         HibernateConstants.ROUTE_ID, customerAlertType.getRoute()
                                                                                                 .getId(), true);
            for (RoutePOS pos : routePos) {
                posIdList.add(pos.getPointOfSale().getId());
            }
        }
        
        return posIdList;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:npathcomplexity", "checkstyle:cyclomaticcomplexity" })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void update(final CustomerAlertType customerAlertType, final Collection<String> emailAddresses
        , final int oldRouteId, final int newRouteId) {
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final Date now = customerAlertType.getLastModifiedGmt();
        final Integer userId = customerAlertType.getLastModifiedByUserId();
        final AlertThresholdType newType = this.entityDao.get(AlertThresholdType.class, customerAlertType.getAlertThresholdType().getId());
        
        final CustomerAlertType existing = this.entityDao.get(CustomerAlertType.class, customerAlertType.getId());
        if (existing == null) {
            throw new IllegalStateException("Could not locate CustomerAlertType: " + customerAlertType.getId());
        }
        
        final int oldTypeId = existing.getAlertThresholdType().getId();
        final int newTypeId = customerAlertType.getAlertThresholdType().getId();
        
        final boolean allow = isAlertThresholdSwitchAllowed(oldTypeId, newTypeId);
        
        if (!allow) {
            throw new IllegalStateException("Switching AlertThresholdType from " + oldTypeId + " to " + newTypeId + " is not allowed! (ID: "
                                            + customerAlertType.getId() + ")!");
        }
        
        // Modify email addresses
        if ((emailAddresses != null) && (emailAddresses.size() > 0)) {
            String emailStr = null;
            final Set<String> emailsSet = new HashSet<String>(emailAddresses.size());
            try {
                final Iterator<String> emailStrItr = emailAddresses.iterator();
                while (emailStrItr.hasNext()) {
                    emailStr = emailStrItr.next();
                    emailsSet.add(URLEncoder.encode(emailStr, WebSecurityConstants.URL_ENCODING_LATIN1));
                }
            } catch (UnsupportedEncodingException uee) {
                throw new RuntimeException("Unable to encode email '" + emailStr + "' !");
            }
            
            final Set<CustomerAlertEmail> existingEmailsSet = existing.getCustomerAlertEmails();
            for (CustomerAlertEmail existingEmail : existingEmailsSet) {
                if (!emailsSet.remove(existingEmail.getCustomerEmail().getEmail())) {
                    // Found deleted email
                    this.entityDao.delete(existingEmail);
                }
            }
            
            this.entityDao.flush();
            
            if (emailsSet.size() > 0) {
                // Create new alert email addresses for existing ones in CustomerEmail
                final List<CustomerEmail> custEmails = this.entityDao.getNamedQuery(CUSTOMER_EMAIL_BY_CUSTOMER_ID_AND_EMAIL)
                        .setParameter(HibernateConstants.CUSTOMER_ID, existing.getCustomer().getId())
                        .setParameterList(HibernateConstants.EMAIL, emailsSet).list();
                for (CustomerEmail custEmail : custEmails) {
                    emailsSet.remove(custEmail.getEmail());
                    createAndSaveCustomerAlertEmail(existing, custEmail, now, userId);
                }
                
                // Create new alert email addresses for non-existings in CustomerEmail
                for (String newEmail : emailsSet) {
                    final CustomerEmail custEmail = createAndSaveCustomerEmail(newEmail, existing.getCustomer(), now, userId);
                    createAndSaveCustomerAlertEmail(existing, custEmail, now, userId);
                }
                
                this.entityDao.flush();
            }
        } else {
            final Set<CustomerAlertEmail> existingEmailsSet = existing.getCustomerAlertEmails();
            for (CustomerAlertEmail existingEmail : existingEmailsSet) {
                this.entityDao.delete(existingEmail);
            }
            
        }
        
        // Update existing CustomerAlertType
        existing.setName(customerAlertType.getName());
        existing.setIsActive(customerAlertType.isIsActive());
        existing.setAlertThresholdType(newType);
        existing.setThreshold(customerAlertType.getThreshold());
        existing.setIsDelayed(customerAlertType.getIsDelayed());
        existing.setDelayedByMinutes(customerAlertType.getDelayedByMinutes());
        
        if (oldRouteId != newRouteId) {
            existing.setRoute((newRouteId == -1) ? (Route) null : this.entityDao.get(Route.class, newRouteId));
        }
        
        existing.setLastModifiedGmt(now);
        existing.setLastModifiedByUserId(userId);
        
        this.entityDao.update(existing);
        this.entityDao.flush();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final CustomerAlertType delete(final CustomerAlertType customerAlertType, final Integer userAccountId) {
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final Date now = new Date();
        
        final CustomerAlertType existing = this.entityDao.get(CustomerAlertType.class, customerAlertType.getId());
        existing.setIsDeleted(true);
        existing.setIsActive(false);
        existing.setLastModifiedGmt(now);
        existing.setLastModifiedByUserId(userAccountId);
        
        this.entityDao.update(existing);
        this.entityDao.flush();
        
        return existing;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity" })
    public final boolean isAlertThresholdSwitchAllowed(final int oldTypeId, final int newTypeId) {
        boolean allow = true;
        if (oldTypeId != newTypeId) {
            switch (oldTypeId) {
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION:
                    allow = false;
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                    allow = (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT)
                            || (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                    allow = (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT)
                            || (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                    allow = (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT)
                            || (newTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
                    break;
                default:
                    break;
            }
            
        }
        return allow;
    }
    
    private CustomerAlertEmail createAndSaveCustomerAlertEmail(final CustomerAlertType alertType, final CustomerEmail email, final Date createGmt,
        final Integer createdBy) {
        final CustomerAlertEmail cae = new CustomerAlertEmail();
        cae.setCustomerAlertType(alertType);
        cae.setCustomerEmail(email);
        cae.setLastModifiedGmt(createGmt);
        cae.setLastModifiedByUserId(createdBy);
        
        this.entityDao.save(cae);
        
        return cae;
    }
    
    private CustomerEmail createAndSaveCustomerEmail(final String emailAddress, final Customer customer, final Date createGmt, final Integer createdBy) {
        final CustomerEmail custEmail = new CustomerEmail();
        custEmail.setCustomer(customer);
        custEmail.setEmail(emailAddress);
        custEmail.setLastModifiedGmt(createGmt);
        custEmail.setLastModifiedByUserId(createdBy);
        
        this.entityDao.save(custEmail);
        
        return custEmail;
    }
    
}
