package com.digitalpaytech.service.impl;

import org.apache.log4j.Logger;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Service;

import com.digitalpaytech.service.ElavonSemaphoreService;
import com.digitalpaytech.util.semaphore.RedisSemaphore;

@Service("elavonSemaphoreService")
public class ElavonSemaphoreServiceImpl extends RedisSemaphore implements ElavonSemaphoreService {
    private static final Logger LOG = Logger.getLogger(ElavonSemaphoreServiceImpl.class);
    
    protected ElavonSemaphoreServiceImpl(final JedisConnectionFactory c) {
        super(c);
    }
    
    public ElavonSemaphoreServiceImpl() {
    }

    /**
     * This method is used by ElavonViaConexProcessor, sendOnlineToServer.
     */    
    @Override
    public final boolean bookConnection(final String tidmid) {
        return bookConnection(tidmid, 0);
    }

    /**
     * This method is used by ElavonViaConexCloseBatchService, processBatch. 
     */
    @Override
    public final boolean bookConnectionWithExpire(final String tidmid, final int milliseconds) {
        return bookConnection(tidmid, milliseconds);
    }
    
    private boolean bookConnection(final String tidmid, final int milliseconds) {
        final int txnCounter;
        if (milliseconds <= 0) {
            txnCounter = super.incrementAndGetCounter(tidmid);
        } else {
            txnCounter = super.incrementSetExpireAndGetCounter(tidmid, milliseconds);
        }
        LOG.info(createSemaphoreLogMessage(tidmid, txnCounter, milliseconds));
        if (txnCounter == 1) {
            return true;
        }
        // if transaction counter > # then some measures need be taken, like sending an email ,etc.
        return false;
    }

    @Override
    public final void releaseConnection(final String tidmid) {
        super.resetCounter(tidmid);
    }
    
    private String createSemaphoreLogMessage(final String tidmid, final int counter, final int milliseconds) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("TID_MID: ").append(tidmid).append(", semaphore counter: ").append(counter);
        bdr.append(", expire value in milliseconds: ").append(milliseconds);
        return bdr.toString();
    }

}
