package com.digitalpaytech.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PosSensorInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageCompressor;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Service("posSensorInfoService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosSensorInfoServiceImpl implements PosSensorInfoService {
    
    private static final Logger logger = Logger.getLogger(PosSensorInfoServiceImpl.class);
    private static Boolean isArchivingSensor = Boolean.FALSE;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdTypeId(int pointOfSaleId, int sensorInfoTypeId) {
        
        String[] paramNames = { "pointOfSaleId", "sensorInfoTypeId" };
        Object[] values = { pointOfSaleId, sensorInfoTypeId };
        
        List<PosSensorInfo> sensorLogs = entityDao.findByNamedQueryAndNamedParam("SensorLog.findSensorLogsByPointOfSaleIdTypeIdLastSevenDays",
            paramNames, values);
        return sensorLogs;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public void archiveOldPosSensorInfoData(int archiveIntervalDays, int archiveBatchSize, String archiveServerName) {
        
        if (isArchivingSensor) {
            return;
        } else {
            isArchivingSensor = true;
        }
        
        Date currentDate = new Date();
        Date olderThanDate = DateUtils.addDays(new Date(), -archiveIntervalDays);
        try {
            String dirName = WebCoreConstants.DEFAULT_ARCHIVE_LOCATION;
            File dir = new File(dirName);
            if (!dir.exists()) {
                dir.mkdir(); // create directory {tomcat.home}/archives if doesn't exist
            }
            
            // e.g. SensorLog_Archive_2011_01_05_11_30_57_PST.csv
            String fileName = WebCoreConstants.ARCHIVE_FILE_NAME_PREFIX_FOR_POS_SENSOR_INFO
                              + DateUtil.createColonDelimitedDateString(currentDate).replace(':', '_') + WebCoreConstants.CSV_FILE_NAME_EXTENSION;
            
            // e.g. SensorLog_Archive_2011_01_05_11_30_57_PST.zip
            String zipFileName = WebCoreConstants.ARCHIVE_FILE_NAME_PREFIX_FOR_POS_SENSOR_INFO
                                 + DateUtil.createColonDelimitedDateString(currentDate).replace(':', '_') + ".zip";
            
            FileWriter file = new FileWriter(dirName + fileName);
            
            int minPaystationId = 1;
            int exportRecordCount = 0;
            int deleteRecordCount = 0;
            
            List<PointOfSale> poses = this.entityDao.findByNamedQuery("PointOfSale.findPointOfSalesOrderById", new String[] { "minPointOfSaleId" },
                new Object[] { minPaystationId }, 0, WebCoreConstants.PAYSTATION_ID_RETRIEVE_BATCH_SIZE);
            while (poses != null && !poses.isEmpty()) {
                List<Integer> currArchiveBatch = new ArrayList<Integer>(archiveBatchSize);
                for (PointOfSale pos : poses) {
                    minPaystationId = pos.getId().intValue();
                    currArchiveBatch.add(pos.getId());
                    if (currArchiveBatch.size() == archiveBatchSize) {
                        // archive this batch to file
                        Collection<PosSensorInfo> posSensorInfos = this.entityDao.findByNamedQueryAndNamedParam(
                            "PosSensorInfo.findAllByPointOfSaleIdsAndSensorGmt", new String[] { "psIdList", "olderThanDate" }, new Object[] {
                                currArchiveBatch, olderThanDate });
                        exportRecordCount = this.exportOldPosSensorInfoData(posSensorInfos, file, exportRecordCount);
                        logger.debug("Successfully exported old PosSensorInfo for paystations " + currArchiveBatch);
                        // delete this batch
                        for (PosSensorInfo info : posSensorInfos) {
                            this.entityDao.delete(info);
                            deleteRecordCount++;
                        }
                        logger.debug("Successfully deleted old PosSensorInfo for paystations " + currArchiveBatch);
                        // clear array list
                        currArchiveBatch.clear();
                    }
                }
                // the last batch may be < archiveBatchSize entries
                if (!currArchiveBatch.isEmpty()) {
                    // archive last batch to file
                    Collection<PosSensorInfo> posSensorInfos = this.entityDao.findByNamedQueryAndNamedParam(
                        "PosSensorInfo.findAllByPointOfSaleIdsAndSensorGmt", new String[] { "psIdList", "olderThanDate" }, new Object[] {
                            currArchiveBatch, olderThanDate });
                    exportRecordCount = this.exportOldPosSensorInfoData(posSensorInfos, file, exportRecordCount);
                    logger.debug("Successfully exported old PosSensorInfo for paystations " + currArchiveBatch);
                    // delete last batch
                    for (PosSensorInfo info : posSensorInfos) {
                        this.entityDao.delete(info);
                        deleteRecordCount++;
                    }
                    logger.debug("Successfully deleted old PosSensorInfo for paystations " + currArchiveBatch);
                }
                
                poses = this.entityDao.findByNamedQuery("PointOfSale.findPointOfSalesOrderById", new String[] { "minPointOfSaleId" },
                    new Object[] { minPaystationId + 1 }, 0, WebCoreConstants.PAYSTATION_ID_RETRIEVE_BATCH_SIZE);
            }
            
            file.close();
            
            // Compress archive file
            MessageCompressor.doFileZip(dirName + fileName, dirName + zipFileName);
            
            // Remove csv file after Compress
            File csvFile = new File(dirName + fileName);
            boolean success = WebCoreUtil.secureDelete(csvFile);
            logger.info("+++ delete csv file " + fileName + (success ? " successed" : " failed"));
            
            String successInfo = "Copied " + exportRecordCount + " records to archive file. \n\n" + "Deleted " + deleteRecordCount
                                 + " records from table PosSensorInfo. \n\n" + "You can find archive files at directory "
                                 + WebCoreConstants.DEFAULT_ARCHIVE_LOCATION + "\n" + "on application server: " + archiveServerName + "\n";
            
            if (exportRecordCount == deleteRecordCount) {
                emailResult("Archiving PosSensorInfo successfully", successInfo);
            } else {
                emailResult("WARNING: Archiving PosSensorInfo finished. BUT got copy/delete count mismatch result", successInfo);
            }
        } catch (Exception ex) {
            logger.debug("+++ Failed to archive PosSensorInfo. +++ " + ex.getMessage());
            
            // build email body
            StringBuffer content = new StringBuffer("");
            content.append("Error: " + ex.getMessage() + "\n\n");
            
            // build Stack Trace
            StringWriter string_writer = new StringWriter();
            ex.printStackTrace(new PrintWriter(string_writer));
            content.append(string_writer.toString() + "\n");
            
            emailResult("Failed to archive PosSensorInfo", content.toString());
        } finally {
            isArchivingSensor = false;
        }
    }
    
    private void emailResult(String subject, String body) {
        String toAddresses = emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_ADMIN_ALERT_TO_EMAIL_ADDRESS,
            EmsPropertiesService.EMS_ADDRESS_STRING);
        
        mailerService.sendMessage(toAddresses, subject, body, null);
        logger.debug("Successfully sent email about archive status to : " + toAddresses);
    }
    
    private int exportOldPosSensorInfoData(Collection<PosSensorInfo> posSensorInfos, FileWriter file, int count) throws IOException {
        int newCount = count;
        if (posSensorInfos != null && posSensorInfos.size() >= 0) {
            newCount = newCount + posSensorInfos.size();
            for (PosSensorInfo info : posSensorInfos) {
                file.write(this.toArchiveString(info));
            }
            file.flush();
        }
        
        return newCount;
    }
    
    private String toArchiveString(PosSensorInfo info) {
        StringBuilder str = new StringBuilder("");
        
        str.append(info.getPointOfSale().getId());
        str.append(",");
        
        str.append(info.getSensorInfoType().getId());
        str.append(",");
        
        str.append(info.getSensorGmt().toString());
        str.append(",");
        
        str.append(info.getAmount());
        str.append("\n");
        
        return (str.toString());
    }
    
    @Override
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndDateRange(Set<Integer> pointOfSaleIdList, int sensorInfoTypeId,
        Date fromDate, Date toDate, int limit) {
        String[] paramNames = { "pointOfSaleIdList", "sensorInfoTypeId", "fromDate", "toDate" };
        Object[] values = { pointOfSaleIdList, sensorInfoTypeId, fromDate, toDate };
        
        List<PosSensorInfo> sensorLogs = entityDao.findByNamedQueryAndNamedParamWithLimit(
            "PosSensorInfo.findPosSensorInfoByPointOfSaleIdsAndTypeIdAndDateRange", paramNames, values, false, limit);
        return sensorLogs;
    }
    
    @Override
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(Integer pointOfSaleId, Integer sensorInfoTypeId, Date fromDate) {
        String[] paramNames = { "pointOfSaleId", "sensorInfoTypeId", "fromDate" };
        Object[] values = { pointOfSaleId, sensorInfoTypeId, fromDate };
        
        List<PosSensorInfo> sensorLogs = entityDao.findByNamedQueryAndNamedParam(
            "PosSensorInfo.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate", paramNames, values, false);
        return sensorLogs;
    }
    
    @Override
    public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(Integer pointOfSaleId,
        Integer sensorInfoTypeId, Date fromDate) {
        String[] paramNames = { "pointOfSaleId", "sensorInfoTypeId", "fromDate" };
        Object[] values = { pointOfSaleId, sensorInfoTypeId, fromDate };
        
        List<PosSensorInfo> sensorLogs = entityDao.findByNamedQueryAndNamedParam(
            "PosSensorInfo.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc", paramNames, values, false);
        return sensorLogs;
    }
}
