package com.digitalpaytech.service.impl;

import com.digitalpaytech.service.ExtensibleRateService;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.SpecialRateDate;
import com.digitalpaytech.dao.EntityDao;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service("extensibleRateService")
@Transactional(propagation=Propagation.REQUIRED)
public class ExtensibleRateServiceImpl implements ExtensibleRateService {

    @Autowired
    private EntityDao entityDao;
    
    
    @Override
    public ExtensibleRate findByNameLocationIdAndUnifiedRateId(String name, Integer locationId, Integer unifiedRateId) {
        String[] params = { "name", "locationId", "unifiedRateId" };
        Object[] values = { name, locationId, unifiedRateId };

        List<?> list = entityDao.findByNamedQueryAndNamedParam("ExtensibleRate.findByNameLocationIdAndUnifiedRateId", params, values);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (ExtensibleRate) list.get(0);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.SUPPORTS)
    public Collection<ExtensibleRate> getCurrentExtensibleRate(Integer locationId, Date targetDateLocal) {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("SELECT r.* FROM ExtensibleRate r ")
                .append("INNER JOIN ExtensibleRateDayOfWeek dow ON r.Id = dow.ExtensibleRateId ")
                .append("LEFT OUTER JOIN SpecialRateDate sr ON sr.Id = r.SpecialRateDateId AND sr.StartDateLocal <= :targetDateLocal AND sr.EndDateLocal > :targetDateLocal ")
                .append("WHERE r.LocationId = :locationId ")
                .append("AND dow.DayOfWeekId = DAYOFWEEK(:targetDateLocal) ")
                .append("AND r.BeginHourLocal * 100 + r.BeginMinuteLocal <= HOUR(:targetDateLocal) * 100 + MINUTE(:targetDateLocal) ")
                .append("AND (HOUR(:targetDateLocal) * 100 + MINUTE(:targetDateLocal) < (r.EndHourLocal * 100 + r.EndMinuteLocal) ")
                .append("OR 0 = (r.EndHourLocal * 100 + r.EndMinuteLocal)) ")
                .append("AND r.IsActive = 1");
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        query.setParameter("locationId", locationId);
        query.setParameter("targetDateLocal", targetDateLocal);
        query.addEntity(ExtensibleRate.class);
        return query.list();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public List getEmsRatesForSMSMessage(Date localExpiryDate, Date localMaxExtendedDate, Integer locationId) {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("SELECT r.*, edow.*, sr.*, edow.dayOfWeekId as edowDayOfWeekId FROM ExtensibleRate r ")
                .append("INNER JOIN ExtensibleRateDayOfWeek edow ON r.Id = edow.ExtensibleRateId ")                
                .append("LEFT OUTER JOIN SpecialRateDate sr ON sr.Id = r.SpecialRateDateId AND sr.StartDateLocal<= :maxExtendedDate AND sr.EndDateLocal >= :date ")
                .append("WHERE edow.DayOfWeekId in (DAYOFWEEK(:date), DAYOFWEEK(:maxExtendedDate)) ")
                .append("AND r.LocationId = :locationId ")
                .append("AND edow.DayOfWeekId * 10000 + r.BeginHourLocal * 100 + r.BeginMinuteLocal <= DAYOFWEEK(:maxExtendedDate) * 10000 + HOUR(:maxExtendedDate) * 100 + MINUTE(:maxExtendedDate) ")
                .append("AND (DAYOFWEEK(:date) * 10000 + HOUR(:date) * 100 + MINUTE(:date) < (edow.DayOfWeekId * 10000 + r.EndHourLocal * 100 + r.EndMinuteLocal) OR 0 = (r.EndHourLocal * 100 + r.EndMinuteLocal)) ")
                .append("AND r.IsActive = 1 ")
                .append("ORDER BY edow.DayOfWeekId, r.BeginHourLocal, r.BeginMinuteLocal");
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        query.setParameter("date", localExpiryDate);
        query.setParameter("maxExtendedDate", localMaxExtendedDate);
        query.setParameter("locationId", locationId);  
        query.addEntity("r", ExtensibleRate.class);
        query.addEntity("edow", ExtensibleRateDayOfWeek.class);   
        query.addEntity("sr", SpecialRateDate.class);        
        query.addScalar("edowDayOfWeekId");
        return query.list();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public List getWeekendEmsRatesForSMSMessage(Date localExpiryDate, Date localMaxExtendedDate, Integer locationId) {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("SELECT r.*, edow.*, sr.*, edow.dayOfWeekId as edowDayOfWeekId FROM ExtensibleRate r ")
                .append("INNER JOIN ExtensibleRateDayOfWeek edow ON r.Id = edow.ExtensibleRateId ")
                .append("LEFT OUTER JOIN SpecialRateDate sr ON sr.Id = r.SpecialRateDateId AND sr.StartDateLocal <= :maxExtendedDate AND sr.EndDateLocal >= :date ")
                .append("WHERE edow.DayOfWeekId in (7, 1) ")
                .append("AND r.LocationId = :locationId ")
                .append("AND If (edow.DayOfWeekId = 7, 1,  r.BeginHourLocal * 100 + r.BeginMinuteLocal <= HOUR(:maxExtendedDate) * 100 + MINUTE(:maxExtendedDate) ) = 1 ")
                .append("AND ( If (edow.DayOfWeekId = 7, r.EndHourLocal * 100 + r.EndMinuteLocal > HOUR(:date) * 100 + MINUTE(:date), 1) = 1 OR 0 = (r.EndHourLocal * 100 + r.EndMinuteLocal) ) ")
                .append("AND r.IsActive = 1 ")
                .append("ORDER BY edow.DayOfWeekId desc, r.BeginHourLocal, r.BeginMinuteLocal ");
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        query.setParameter("date", localExpiryDate);
        query.setParameter("maxExtendedDate", localMaxExtendedDate);
        query.setParameter("locationId", locationId);   
        query.addEntity("r", ExtensibleRate.class);
        query.addEntity("edow", ExtensibleRateDayOfWeek.class);
        query.addEntity("sr", SpecialRateDate.class);      
        query.addScalar("edowDayOfWeekId");
        return query.list();
    }    
}
