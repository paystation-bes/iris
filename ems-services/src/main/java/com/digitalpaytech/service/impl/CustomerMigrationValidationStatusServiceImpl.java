package com.digitalpaytech.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationStatusType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;
import com.digitalpaytech.service.customermigration.CustomerMigrationIRISService;
import com.digitalpaytech.service.customermigration.CustomerMigrationValidationHelper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
@Service("customerMigrationValidationStatusService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerMigrationValidationStatusServiceImpl implements CustomerMigrationValidationStatusService {
    
    private static final Logger LOGGER = Logger.getLogger(CustomerMigrationValidationStatusServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private CustomerMigrationValidationHelper customerMigrationValidationHelper;
    @Autowired
    private CustomerMigrationIRISService customerMigrationIRISService;
    
    @Override
    public final List<CustomerMigrationValidationStatus> findSystemWideValidations() {
        return this.entityDao.findByNamedQuery("CustomerMigrationValidationStatus.findSystemWideValidations", false);
        
    }
    
    @Override
    public final List<CustomerMigrationValidationStatus> findUserFacingValidationsByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerMigrationValidationStatus.findUserFacingValidationsByCustomerId",
                                                            HibernateConstants.CUSTOMER_ID, customerId, false);
    }
    
    @Override
    public final List<CustomerMigrationValidationStatus> findInMigrationValidationsByCustomerId(final Integer customerId,
        final boolean isDuringMigration) {
        final String[] parameters = { HibernateConstants.CUSTOMER_ID, HibernateConstants.IS_DURING_MIGRATION };
        final Object[] values = { customerId, isDuringMigration };
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerMigrationValidationStatus.findInMigrationValidationsByCustomerId", parameters,
                                                            values, false);
    }

    @Override
    public final CustomerMigrationValidationStatusType runValidations(final Integer customerId, final boolean includeSystemWide,
        final boolean isDuringMigration, final boolean beforeStart) {
        
        final List<CustomerMigrationValidationStatus> validationList = new ArrayList<CustomerMigrationValidationStatus>();
        if (includeSystemWide) {
            validationList.addAll(findSystemWideValidations());
        }
        byte validationStatus = WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_PASSED;
        validationList.addAll(findInMigrationValidationsByCustomerId(customerId, isDuringMigration));
        
        final Integer ems6CustomerId = this.customerMigrationIRISService.findEMS6CustomerId(customerId);
        
        for (CustomerMigrationValidationStatus validation : validationList) {
            validationStatus = updateValidation(validation, validationStatus, customerId, ems6CustomerId, beforeStart);
        }
        
        return this.entityDao.get(CustomerMigrationValidationStatusType.class, validationStatus);
    }
    
    private byte updateValidation(final CustomerMigrationValidationStatus validation, final byte existingValidationStatus,
        final Integer irisCustomerId, final Integer ems6CustomerId, final boolean beforeStart) {
        byte validationStatus = existingValidationStatus;
        
        final boolean status = checkValidation(validation, irisCustomerId, ems6CustomerId, beforeStart);
        
        final Date now = DateUtil.getCurrentGmtDate();
        validation.setIsPassed(status);
        validation.setLastTryGmt(now);
        if (status) {
            validation.setLastSuccessGmt(now);
        } else {
            validation.setLastFailureGmt(now);
            if (validation.getCustomerMigrationValidationType().isIsBlocking()) {
                validationStatus = WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED;
            } else if (existingValidationStatus != WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED) {
                validationStatus = WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_WARNED;
            }
        }
        
        this.entityDao.update(validation);
        return validationStatus;
    }
    
    // Each validation type requires a different logic 
    private boolean checkValidation(final CustomerMigrationValidationStatus validation, final Integer irisCustomerId, final Integer ems6CustomerId,
        final Boolean beforeStart) {
        final String methodName = validation.getCustomerMigrationValidationType().getMethodToCall();
        try {
            final Method method = this.customerMigrationValidationHelper.getClass().getDeclaredMethod(methodName,
                                                                                                      CustomerMigrationValidationStatus.class,
                                                                                                      Integer.class, Integer.class, Boolean.class);
            return (Boolean) method.invoke(this.customerMigrationValidationHelper, validation, irisCustomerId, ems6CustomerId, beforeStart);
        } catch (NoSuchMethodException nsme) {
            LOGGER.error(methodName + " doesn't exist in CustomerMigrationValidationStatusServiceImpl.", nsme);
            return false;
        } catch (IllegalAccessException iae) {
            LOGGER.error(methodName + " could not be accessed in CustomerMigrationValidationStatusServiceImpl.", iae);
            return false;
        } catch (InvocationTargetException ite) {
            LOGGER.error(methodName + " could not be called in CustomerMigrationValidationStatusServiceImpl.", ite);
            return false;
        } catch (Exception e) {
            LOGGER.error(methodName + " Unknown exception: ", e);
            return false;
        }
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final CustomerMigrationValidationHelper getCustomerMigrationValidationHelper() {
        return this.customerMigrationValidationHelper;
    }
    
    public final void setCustomerMigrationValidationHelper(final CustomerMigrationValidationHelper customerMigrationValidationHelper) {
        this.customerMigrationValidationHelper = customerMigrationValidationHelper;
    }
    
    public final CustomerMigrationIRISService getCustomerMigrationIRISService() {
        return this.customerMigrationIRISService;
    }
    
    public final void setCustomerMigrationIRISService(final CustomerMigrationIRISService customerMigrationIRISService) {
        this.customerMigrationIRISService = customerMigrationIRISService;
    }
    
}
