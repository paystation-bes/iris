package com.digitalpaytech.service.impl;

import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.dao.EntityDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("mobileNumberService")
@Transactional(propagation=Propagation.SUPPORTS)
public class MobileNumberServiceImpl implements MobileNumberService {

    @Autowired
    private EntityDao entityDao;

    
    @Override
    public MobileNumber findMobileNumber(String number) {
        String[] paramNames = { "number" };
        Object[] values = { number };
        
        List<?> list = entityDao.findByNamedQueryAndNamedParam("MobileNumber.findByNumber", paramNames, values);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (MobileNumber) list.get(0);
    }

    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public MobileNumber findMobileNumberById(Integer id) {
        List<MobileNumber> mobileNumberList = entityDao.findByNamedQueryAndNamedParam("MobileNumber.findByNumberById", "id", id);
        if (mobileNumberList == null || mobileNumberList.isEmpty()) {
            return null;
        }
        return mobileNumberList.get(0);
    }
}
