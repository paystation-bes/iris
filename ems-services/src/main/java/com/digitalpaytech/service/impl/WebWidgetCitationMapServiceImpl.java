package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.CitationMap;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.service.CitationMapService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetCitationMapService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetCitationMapServiceImpl implements WebWidgetService {
    
    @Autowired
    private CitationMapService citationMapService;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public final void setCitationMapService(final CitationMapService citationMapService) {
        this.citationMapService = citationMapService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final WidgetData getWidgetData(final Widget widget, final UserAccount userAccount) {
        final WidgetMapInfo widgetMapInfo = new WidgetMapInfo();
        
        List<CitationMap> citationList = null;
        
        final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        
        final int customerId = userAccount.getCustomer().getId();
        final boolean checkTier = widget.isIsSubsetTier1();
        
        final CustomerProperty timezoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        this.webWidgetHelperService.calculateRange(widget, customerId, widget.getWidgetRangeType().getId());
        
        if (checkTier) {
            final List<WidgetSubsetMember> subsetMembers = widget.getWidgetSubsetMembers();
            if (tier1Id == WidgetConstants.TIER_TYPE_CITATION_TYPE) {
                /* Organization */
                final List<Integer> citationTypeSubsetIds = new ArrayList<Integer>();
                for (WidgetSubsetMember subsetMember : subsetMembers) {
                    if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_CITATION_TYPE)) {
                        citationTypeSubsetIds.add(subsetMember.getMemberId());
                    }
                }
                
                citationList = this.citationMapService.findByDateRangeAndCitationType(customerId, widget.getDateRangeStart().getTime(), widget.getDateRangeEnd()
                        .getTime(), citationTypeSubsetIds);
            }
        } else {
            citationList = this.citationMapService.findByDateRange(customerId, widget.getDateRangeStart().getTime(), widget.getDateRangeEnd().getTime());
        }
        
        List<MapEntry> mapEntries;
        if ((citationList == null) || (citationList.size() <= 0)) {
            mapEntries = new ArrayList<MapEntry>(0);
        } else {
            mapEntries = new ArrayList<MapEntry>(citationList.size());
            for (CitationMap citationMap : citationList) {
                final MapEntry mapEntry = new MapEntry();
                mapEntry.setLatitude(citationMap.getLatitude());
                mapEntry.setLongitude(citationMap.getLongitude());
                mapEntry.setLastSeen(DateUtil.getRelativeTimeString(citationMap.getIssueDateGmt(), timezoneProperty.getPropertyValue()));
                mapEntry.setName(citationMap.getCitationType().getName());
                mapEntry.setSerialNumber(citationMap.getTicketId());
                mapEntries.add(mapEntry);
            }
        }
        
        widgetMapInfo.setMapInfo(mapEntries);
        
        final WidgetData widgetData = new WidgetData();
        widgetData.setWidgetMapInfo(widgetMapInfo);
        widgetData.setIsZeroValues(false);
        widgetData.setIsEmpty(mapEntries.size() <= 0);
        widgetData.setWidgetName(widget.getName());
        widgetData.setChartType(widget.getWidgetChartType().getId());
        widgetData.setRangeTypeId(widget.getWidgetRangeType().getId());
        widgetData.setMetricTypeId(widget.getWidgetMetricType().getId());
        
        return widgetData;
    }
}
