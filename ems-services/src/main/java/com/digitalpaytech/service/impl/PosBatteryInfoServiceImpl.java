package com.digitalpaytech.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBatteryInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PosBatteryInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageCompressor;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posBatteryInfoService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosBatteryInfoServiceImpl implements PosBatteryInfoService {
    
    private static Logger logger = Logger.getLogger(PosBatteryInfoServiceImpl.class);
    
    private static Boolean isArchivingBattery = Boolean.FALSE;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MailerService mailerService;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public void archiveOldPosBatteryInfoData(final int archiveIntervalDays, final int archiveBatchSize, final String archiveServerName) {
        if (isArchivingBattery) {
            return;
        } else {
            isArchivingBattery = true;
        }
        
        final Date currentDate = new Date();
        final Date olderThanDate = DateUtils.addDays(currentDate, -archiveIntervalDays);
        try {
            final String dirName = WebCoreConstants.DEFAULT_ARCHIVE_LOCATION;
            final File dir = new File(dirName);
            if (!dir.exists()) {
             // create directory {tomcat.home}/archives if doesn't exist
                dir.mkdir();
            }
            
            // e.g. BatteryInfo_Archive_2011_01_05_11_30_57_PST.csv
            final String fileName = WebCoreConstants.ARCHIVE_FILE_NAME_PREFIX_FOR_POS_BATTERY_INFO
                              + DateUtil.createColonDelimitedDateString(currentDate).replace(':', '_') + WebCoreConstants.CSV_FILE_NAME_EXTENSION;
            
            // e.g. BatteryInfo_Archive_2011_01_05_11_30_57_PST.zip
            final String zipFileName = WebCoreConstants.ARCHIVE_FILE_NAME_PREFIX_FOR_POS_BATTERY_INFO
                                 + DateUtil.createColonDelimitedDateString(currentDate).replace(':', '_') + ".zip";
            
            final FileWriter file = new FileWriter(dirName + fileName);
            
            int minPaystationId = 1;
            int exportRecordCount = 0;
            int deleteRecordCount = 0;
            
            Collection<PointOfSale> poses = this.entityDao.findByNamedQuery("PointOfSale.findPointOfSalesOrderById",
                new String[] { "minPointOfSaleId" }, new Object[] { minPaystationId }, 0, WebCoreConstants.PAYSTATION_ID_RETRIEVE_BATCH_SIZE);
            
            while (poses != null && !poses.isEmpty()) {
                final List<Integer> currArchiveBatch = new ArrayList<Integer>(archiveBatchSize);
                for (PointOfSale pos : poses) {
                    minPaystationId = pos.getId();
                    currArchiveBatch.add(pos.getId());
                    if (currArchiveBatch.size() == archiveBatchSize) {
                        // archive this batch to file
                        final Collection<PosBatteryInfo> batteryInfos = this.entityDao.findByNamedQueryAndNamedParam(
                            "PosBatteryInfo.findAllPosBatteryInfoByPointOfSaleIdsAndSensorGmt", new String[] { "psIdList", "olderThanDate" },
                            new Object[] { currArchiveBatch, olderThanDate });
                        exportRecordCount = exportOldPosBatteryInfoData(batteryInfos, file, exportRecordCount);
                        logger.debug("Successfully exported old PosBatteryInfo for paystations " + currArchiveBatch);
                        // delete this batch
                        for (PosBatteryInfo info : batteryInfos) {
                            this.entityDao.delete(info);
                            deleteRecordCount++;
                        }
                        logger.debug("Successfully deleted old PosBatteryInfo for paystations " + currArchiveBatch);
                        // clear array list
                        currArchiveBatch.clear();
                    }
                }
                // the last batch may be < archiveBatchSize entries
                if (!currArchiveBatch.isEmpty()) {
                    // archive last batch to file
                    final Collection<PosBatteryInfo> batteryInfos = this.entityDao.findByNamedQueryAndNamedParam(
                        "PosBatteryInfo.findAllPosBatteryInfoByPointOfSaleIdsAndSensorGmt", new String[] { "psIdList", "olderThanDate" },
                        new Object[] { currArchiveBatch, olderThanDate });
                    exportRecordCount = exportOldPosBatteryInfoData(batteryInfos, file, exportRecordCount);
                    logger.debug("Successfully exported old PosBatteryInfo for paystations " + currArchiveBatch);
                    // delete last batch
                    for (PosBatteryInfo info : batteryInfos) {
                        this.entityDao.delete(info);
                        deleteRecordCount++;
                    }
                    logger.debug("Successfully deleted old PosBatteryInfo for paystations " + currArchiveBatch);
                }
                
                poses = this.entityDao.findByNamedQuery("PointOfSale.findPointOfSalesOrderById", new String[] { "minPointOfSaleId" },
                    new Object[] { minPaystationId + 1 }, 0, WebCoreConstants.PAYSTATION_ID_RETRIEVE_BATCH_SIZE);
            }
            
            file.close();
            
            // Compress archive file
            MessageCompressor.doFileZip(dirName + fileName, dirName + zipFileName);
            
            // Remove csv file after Compress
            final File csvFile = new File(dirName + fileName);
            final boolean success = csvFile.delete();
            logger.info("+++ delete csv file " + fileName + (success ? " successed" : " failed"));
            
            final StringBuilder successInfo = new StringBuilder("Copied ");
            successInfo.append(exportRecordCount);
            successInfo.append(" records to archive file. \n\n");
            successInfo.append("Deleted ");
            successInfo.append(deleteRecordCount);
            successInfo.append(" records from table PosBatteryInfo. \n\n");
            successInfo.append("You can find archive files at directory ");
            successInfo.append(WebCoreConstants.DEFAULT_ARCHIVE_LOCATION);
            successInfo.append("\n");
            successInfo.append("on application server: ");
            successInfo.append(archiveServerName);
            successInfo.append("\n");
            
            if (exportRecordCount == deleteRecordCount) {
                emailResult("Archiving BatteryInfo successfully", successInfo.toString());
            } else {
                emailResult("WARNING: Archiving PosBatteryInfo finished. BUT got copy/delete count mismatch result", successInfo.toString());
            }
        } catch (Exception ex) {
            logger.debug("+++ Failed to archive PosBatteryInfo. +++ " + ex.getMessage());
            
            // build email body
            final StringBuffer content = new StringBuffer("");
            content.append("Error: ");
            content.append(ex.getMessage());
            content.append("\n\n");
            
            // build Stack Trace
            final StringWriter stringWriter = new StringWriter();
            ex.printStackTrace(new PrintWriter(stringWriter));
            content.append(stringWriter.toString());
            content.append("\n");
            
            emailResult("Failed to archive PosBatteryInfo", content.toString());
        } finally {
            isArchivingBattery = false;
        }
    }
    
    private void emailResult(final String subject, final String body) {
        final String toAddresses = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_ADMIN_ALERT_TO_EMAIL_ADDRESS,
            EmsPropertiesService.EMS_ADDRESS_STRING);
        
        this.mailerService.sendMessage(toAddresses, subject, body, null);
        logger.debug("Successfully sent email about archive status to : " + toAddresses);
    }
    
    private int exportOldPosBatteryInfoData(final Collection<PosBatteryInfo> batteryInfos, final FileWriter file, final int count) throws IOException {
        int newCount = count;
        if (batteryInfos != null && batteryInfos.size() >= 0) {
            newCount = newCount + batteryInfos.size();
            for (PosBatteryInfo info : batteryInfos) {
                file.write(this.toArchiveString(info));
            }
            file.flush();
        }
        
        return newCount;
    }
    
    private String toArchiveString(final PosBatteryInfo info) {
        final StringBuilder str = new StringBuilder("");
        
        str.append(info.getPointOfSale().getId());
        str.append(",");
        
        str.append(info.getSensorGmt().toString());
        str.append(",");
        
        str.append(info.getSystemLoad());
        str.append(",");
        
        str.append(info.getInputCurrent());
        str.append(",");
        
        str.append(info.getBatteryVoltage());
        str.append("\n");
        
        return str.toString();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDate(final Integer pointOfSaleId, final Date fromDate) {
        final String[] paramNames = { "pointOfSaleId", "fromDate" };
        final Object[] values = { pointOfSaleId, fromDate };
        
        return this.entityDao.findByNamedQueryAndNamedParam("PosBatteryInfo.findAllPosBatteryInfoByPointOfSaleIdsAndFromDate", paramNames, values, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDateNoZeroes(final Integer pointOfSaleId, final Date fromDate) {
        final String[] paramNames = { "pointOfSaleId", "fromDate" };
        final Object[] values = { pointOfSaleId, fromDate };
        
        return this.entityDao
                .findByNamedQueryAndNamedParam("PosBatteryInfo.findAllPosBatteryInfoByPointOfSaleIdsAndFromDateNoZeroes"
                                               , paramNames, values, false);
    }
    
    @Override
    public void addBatteryInfo(final Integer pointOfSaleId, final Integer sensorTypeId, final Date sensorDateGmt, final Float value) {
        final SQLQuery query = this.entityDao
                .createSQLQuery("{ call sp_ProcessBatteryInfo(:p_Value, :p_PointOfSaleId, :p_SensorTypeId, :p_SensorDateGMT) }");
        query.addSynchronizedEntityClass(PosBatteryInfo.class);
        query.setParameter("p_Value", value);
        query.setParameter("p_PointOfSaleId", pointOfSaleId);
        query.setParameter("p_SensorTypeId", sensorTypeId);
        
        query.setParameter("p_SensorDateGMT", sensorDateGmt);
        
        query.list();
        
    }
}
