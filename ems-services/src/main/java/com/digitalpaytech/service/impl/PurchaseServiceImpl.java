package com.digitalpaytech.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
@Service("purchaseService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PurchaseServiceImpl implements PurchaseService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public Purchase findPurchaseById(Long id, boolean isEvict) {
        Purchase purchase = (Purchase) entityDao.get(Purchase.class, id);
        if (isEvict) {
            entityDao.evict(purchase);
        }
        return purchase;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Purchase> findPurchasesByPointOfSaleIdLast90Days(int customerId, int pointOfSaleId, String sortOrder, String sortItem, Integer page) {
        
        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.DAY_OF_YEAR, -90);
        
        Criteria crit = entityDao.createCriteria(Purchase.class);
        crit.createAlias("transactionType", "tt");
        crit.createAlias("paymentType", "pt");
        crit.add(Restrictions.eq("customer.id", customerId));
        crit.add(Restrictions.eq("pointOfSale.id", pointOfSaleId));
        crit.add(Restrictions.ge("purchaseGmt", maxDate.getTime()));
        crit.setFetchMode("tt", FetchMode.JOIN);
        crit.setFetchMode("pt", FetchMode.JOIN);
        
        crit.setCacheable(true);
        
        crit.setMaxResults(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        crit.setFirstResult(((page != null ? page.intValue() : 1) - 1) * WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
        if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("ASC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.asc("purchaseGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("type")) {
                crit.addOrder(Order.asc("tt.name"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("paymentType")) {
                crit.addOrder(Order.asc("pt.name"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("amount")) {
                crit.addOrder(Order.asc("chargedAmount"));
            } else {
                crit.addOrder(Order.asc("purchaseGmt"));
            }
        } else if (!StringUtils.isBlank(sortOrder) && sortOrder.equals("DESC")) {
            if (!StringUtils.isBlank(sortItem) && sortItem.equals("date")) {
                crit.addOrder(Order.desc("purchaseGmt"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("type")) {
                crit.addOrder(Order.desc("tt.name"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("paymentType")) {
                crit.addOrder(Order.desc("pt.name"));
            } else if (!StringUtils.isBlank(sortItem) && sortItem.equals("amount")) {
                crit.addOrder(Order.desc("chargedAmount"));
            } else {
                crit.addOrder(Order.desc("purchaseGmt"));
            }
        } else {
            crit.addOrder(Order.desc("purchaseGmt"));
        }
        
        return crit.list();
    }
    
    @Override
    public Purchase findUniquePurchaseForSms(Integer customerId, Integer pointOfSaleId, Date purchaseGmt, int purchaseNumber) {
        return (Purchase) this.entityDao.findUniqueByNamedQueryAndNamedParam("Purchase.findUniquePurchaseForSms", new String[] { "customerId", "pointOfSaleId",
                "purchaseGmt", "purchaseNumber" }, new Object[] { customerId, pointOfSaleId, purchaseGmt, purchaseNumber }, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Purchase findLatestPurchaseByPointOfSaleIdAndPurchaseNumber(int pointOfSaleId, String purchaseNumber) {
        List<Purchase> purchaseList = this.entityDao.findByNamedQueryAndNamedParam("Purchase.findLatestPurchaseByPointOfSaleIdAndPurchaseNumber", new String[] {
                "pointOfSaleId", "purchaseNumber" }, new Object[] { pointOfSaleId, purchaseNumber }, false);
        
        if (purchaseList == null || purchaseList.isEmpty()) {
            return null;
        } else {
            return purchaseList.get(0);
        }
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    public Purchase findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt(Integer pointOfSaleId, Integer purchaseNumber, Date purchaseGmt) {
        
        Purchase purchase = (Purchase) this.entityDao.findUniqueByNamedQueryAndNamedParam("Purchase.findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt", new String[] {
                "pointOfSaleId", "purchaseNumber", "purchaseGmt" }, new Object[] { pointOfSaleId, purchaseNumber, purchaseGmt }, true);
        
        return purchase;
    }
    
    
    @Override
    public Object findPurchaseByCriteria(TransactionReceiptSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }

}
