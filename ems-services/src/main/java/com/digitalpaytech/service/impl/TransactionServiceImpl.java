package com.digitalpaytech.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.orm.hibernate4.HibernateObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.mysql.jdbc.exceptions.jdbc4.MySQLTransactionRollbackException;
import com.netflix.client.ClientException;

import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MobileNumber;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseCollectionCancelled;
import com.digitalpaytech.domain.PurchaseTax;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.SmsTransactionLog;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;
import com.digitalpaytech.domain.ThirdPartyServicePos;
import com.digitalpaytech.domain.Transaction2Push;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.dto.cps.Tokens;
import com.digitalpaytech.dto.paystation.CardEaseData;
import com.digitalpaytech.dto.paystation.CardTransaction;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CardTransactionException;
import com.digitalpaytech.exception.CcProcessingRestrictionException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DataNotFoundException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.service.SmsAlertService;
import com.digitalpaytech.service.SmsMessageTypeService;
import com.digitalpaytech.service.ThirdPartyServiceAccountService;
import com.digitalpaytech.service.ThirdPartyServiceStallSequenceService;
import com.digitalpaytech.service.Transaction2PushService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.service.cps.CPSRefundDataService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;

@Service("transactionService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass", "PMD.TooManyFields", "PMD.TooManyMethods" })
public class TransactionServiceImpl implements TransactionService {
    
    private static final Logger LOG = Logger.getLogger(TransactionServiceImpl.class);
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private ThirdPartyServiceStallSequenceService thirdPartyServiceStallSequenceService;
    
    @Autowired
    private ThirdPartyServiceAccountService thirdPartyServiceAccountService;
    
    @Autowired
    private Transaction2PushService transaction2PushService;
    
    @Autowired
    private RawSensorDataService rawSensorDataService;
    
    @Autowired
    private SmsAlertService smsAlertService;
    
    @Autowired
    private ExtensiblePermitService extensiblePermitService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private SmsMessageTypeService smsMessageTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private MobileNumberService mobileNumberService;
    
    @Autowired
    private ActivePermitService activePermitService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    @Autowired
    private CPSProcessDataService cpsProcessDataService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private CPSRefundDataService cpsRefundDataService;
    
    @Autowired
    private CoreCPSService coreCPSService;
    
    private Properties mappingProperties;
    private JSON json;
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    public void loadMappingProperties() throws SystemException {
        try {
            this.mappingProperties = PropertiesLoaderUtils.loadAllProperties(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            
            if (this.mappingProperties == null || this.mappingProperties.isEmpty()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("TransactionServiceImpl, loadMappingProperties, ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME)
                        .append(" is missing!");
                throw new SystemException(bdr.toString());
            }
            final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
            this.json = new JSON(jsonConfig);
        } catch (IOException ioe) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("TransactionServiceImpl, loadMappingProperties, error loading ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            LOG.error(bdr.toString(), ioe);
            throw new SystemException(bdr.toString(), ioe);
        }
    }
    
    private boolean createCancelled(final boolean coreCPSFlag, final String authNumber) {
        if (coreCPSFlag) {
            return true;
        } else {
            return authNumber == null ? false : authNumber.trim().length() > 0;
        }
    }
    
    private void logAndSendError(final String tokenString, final CardEaseData cardEaseData, final String cardTransactionJson) {
        if (StringUtils.isBlank(tokenString) || tokenString.toLowerCase(Locale.getDefault()).indexOf(this.messageHelper
                .getMessageWithDefault("cps.error.incorrect.json", "Incorrect JSON").toLowerCase(Locale.getDefault())) != -1) {
            final StringBuilder bdr = new StringBuilder("CPS returned ERROR response - ");
            if (StringUtils.isNotBlank(cardTransactionJson)) {
                bdr.append("JSON sent over is: ").append(cardTransactionJson).append(StandardConstants.STRING_NEXT_LINE).append("Ref is: ")
                        .append(cardEaseData.getRef()).append(", error message is: ").append(tokenString);
            } else if (StringUtils.isBlank(tokenString) && StringUtils.isBlank(cardTransactionJson)) {
                bdr.append("CoreCPS TransactionResponse is blank, cardEaseData: ").append(cardEaseData);
            }
            LOG.error(bdr.toString());
            this.mailerService.sendAdminErrorAlert(tokenString, bdr.toString(), null);
        }
    }
    
    @Override
    public final List<Object> processTransaction(final TransactionData transactionData, final boolean isManualSave, final TransactionDto transDto,
        final EmbeddedTxObject embeddedTxObject) throws DuplicateTransactionRequestException, CryptoException {
        
        final PointOfSale pointOfSale = this.entityDao.get(PointOfSale.class, transactionData.getPointOfSale().getId());
        
        if (pointOfSale.getCustomer().getCustomerStatusType().getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED
            || pointOfSale.getCustomer().getCustomerStatusType().getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DELETED) {
            throw new CcProcessingRestrictionException(
                    "Customer does NOT have correct CustomerStatusType: " + pointOfSale.getCustomer().getCustomerStatusType().getName());
        }
        
        boolean createCancelledProcTransRecord = false;
        
        Purchase purchase = transactionData.getPurchase();
        final ProcessorTransaction ptd = transactionData.getProcessorTransaction();
        
        if (transactionData.isCancelledTransaction()) {
            createCancelledProcTransRecord = createCancelled(transactionData.isCoreCPS(), ptd.getAuthorizationNumber());
            purchase = updateForCancellation(purchase, transactionData.isCoreCPS());
        }
        
        List<Object> needSavedObjects = new ArrayList<Object>();
        determineTransactionExcessPayment(purchase);
        
        try {
            if (createCancelledProcTransRecord) {
                ptd.setPreAuth(null);
                ptd.setIsApproved(true);
                ptd.setAuthorizationNumber(StandardConstants.STRING_EMPTY_STRING);
                if (transactionData.isCoreCPS()) {
                    ptd.setProcessorTransactionType(isSettled(embeddedTxObject));
                } else {
                    ptd.setAmount(0);
                    ptd.setCardType(StandardConstants.STRING_EMPTY_STRING);
                    ptd.setLast4digitsOfCardNumber((short) 0);
                    ptd.setProcessorTransactionType(this.processorTransactionTypeService
                            .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_CANCELLED_ID)));
                }
                if (transactionData.getPreAuth() != null) {
                    needSavedObjects.add(transactionData.getPreAuth());
                }
                if (embeddedTxObject != null && transactionData.isCoreCPS()) {
                    populateDataFromCPSResponse(embeddedTxObject, ptd, purchase, needSavedObjects);
                }
            } else {
                needSavedObjects = determineTransactionCardFields(pointOfSale, purchase, ptd, needSavedObjects, isManualSave);
                // PaymentType is already determined in TransactionHandler.
            }
            
            if (transactionData.getSingleTransactionCardData() != null) {
                needSavedObjects.add(transactionData.getSingleTransactionCardData());
            }
            
            if (transDto != null) {
                adjustElavonRecoverable(transactionData, transDto);
            }
            
            if (transactionData.isEMVOrCoreCPS() && purchase.getCardPaidAmount() > 0 && !createCancelledProcTransRecord) {
                ptd.setAuthorizationNumber(transactionData.getAuthorizationNumber());
                ptd.setMerchantAccount(this.merchantAccountService.findByPointOfSaleIdAndCardTypeId(transactionData.getPointOfSale().getId(),
                                                                                                    WebCoreConstants.CARD_TYPE_CREDIT_CARD));
                
                if (transactionData.isEMV() && embeddedTxObject != null) {
                    ptd.setIsApproved(true);
                    ptd.setReferenceNumber(WebCoreConstants.N_A_STRING);
                    ptd.setProcessorTransactionType(isSettled(embeddedTxObject));
                    final CardEaseData cardEaseData = (CardEaseData) embeddedTxObject;
                    if (purchase.getPaymentCards() != null && !purchase.getPaymentCards().isEmpty()) {
                        PaymentCard pc;
                        final Iterator<PaymentCard> iter = purchase.getPaymentCards().iterator();
                        while (iter.hasNext()) {
                            pc = iter.next();
                            if (pc.getCreditCardType() != null) {
                                ptd.setCardType(pc.getCreditCardType().getName());
                            } else {
                                ptd.setCardType(StringUtils.isBlank(cardEaseData.getAcquirer()) ? WebCoreConstants.N_A_STRING : cardEaseData.getAcquirer());
                            }
                            break;
                        }
                    } else {
                        ptd.setCardType(StringUtils.isBlank(cardEaseData.getAcquirer()) ? WebCoreConstants.N_A_STRING : cardEaseData.getAcquirer());
                    }
                    
                    ptd.setProcessorTransactionId(cardEaseData.getRef());
                    ptd.setLast4digitsOfCardNumber(StringUtils.isBlank(cardEaseData.getPan()) 
                                                   ? StandardConstants.CONSTANT_0 : Short.parseShort(cardEaseData.getPan()));
                    // Switch the data from "Application" to "APL".
                    cardEaseData.switchApplicationToApl();
                    
                    final String chargeTokenString;
                    final String cardTokenString;
                    if (CardProcessingUtil.isNotNullAndIsLink(ptd.getMerchantAccount())) {
                        final Tokens tokens = this.coreCPSService.sendProcessedTransactionToCoreCPS(purchase, ptd, cardEaseData);
                        final UUID cardToken = tokens.getToken(Tokens.TokenTypes.CARD_TOKEN);
                        if (cardToken == null) {
                            logAndSendError(null, cardEaseData, null);
                            cardTokenString = null;
                        } else {
                            cardTokenString = cardToken.toString();
                            purchase.getPermits().forEach(p -> {
                                    p.getExtensiblePermits().forEach(e -> { 
                                            e.setCardData(cardTokenString);
                                            e.setLast4digit(StringUtils.isBlank(cardEaseData.getPan()) 
                                                            ? StandardConstants.CONSTANT_0 : Short.parseShort(cardEaseData.getPan()));
                                        }); 
                                });                            
                        }
                        chargeTokenString = tokens.getToken(Tokens.TokenTypes.CHARGE_TOKEN) == null ? null
                                : tokens.getToken(Tokens.TokenTypes.CHARGE_TOKEN).toString();
                    } else {
                        final CardTransaction cardTransactionData = createCardTransactionData(ptd, cardEaseData);
                        final String cardTransactionJson = createCardTransactionJson(cardTransactionData);
                        chargeTokenString = pushEMVDataForToken(cardTransactionJson);
                        cardTokenString = null;
                        
                        // e.g. CPS returned 'Incorrect JSON format for EMV transaction request'
                        logAndSendError(chargeTokenString, cardEaseData, cardTransactionJson);
                    }
                    
                    // Continue saving the information to CPSData table even though it might be a problem.
                    final CPSData cpsData = new CPSData(transactionData.getProcessorTransaction(), chargeTokenString, cardTokenString,
                            cardEaseData.getRef(), cardEaseData.getCvm(), cardEaseData.getApl(), cardEaseData.getAid());
                    needSavedObjects.add(cpsData);
                    
                } else if (transactionData.isCoreCPS() && embeddedTxObject != null) {
                    ptd.setProcessorTransactionType(isSettled(embeddedTxObject));
                    populateDataFromCPSResponse(embeddedTxObject, ptd, purchase, needSavedObjects);
                } else {
                    ptd.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
                }
            }
            
            // SinglePhaseTransaction card Data
            saveTransaction(pointOfSale, purchase, ptd, transactionData.getPosServiceState(), needSavedObjects, createCancelledProcTransRecord);
            
            // These must be done after saveTransaction
            processLinkCPSRefund(transactionData, embeddedTxObject);
            addCardRetryIfLink(ptd.getMerchantAccount(), pointOfSale, purchase, ptd, needSavedObjects);
            
        } catch (Exception e) {
            LOG.error(e);
            if (WebCoreUtil.checkIfExistingTransactionException(e)) {
                throw new DuplicateTransactionRequestException(e);
            } else if (WebCoreUtil.checkIfDuplicateException(e)) {
                
                final Integer[] perms = { WebSecurityConstants.PERMISSION_CC_SMARTCARD_PROCESSING_MANAGEMENT };
                
                final Number permsCount = (Number) this.entityDao.getNamedQuery("RolePermission.checkCustomerPermissions")
                        .setParameter("customerId", pointOfSale.getCustomer().getId()).setParameterList("permissionIds", perms).uniqueResult();
                
                final boolean okFlag = permsCount != null && permsCount.intValue() > 0;
                if (okFlag) {
                    final CardRetryTransaction crt = new CardRetryTransaction();
                    crt.setPointOfSale(pointOfSale);
                    crt.setPurchasedDate(ptd.getPurchasedDate());
                    crt.setTicketNumber(ptd.getTicketNumber());
                    crt.setAmount(purchase.getCardPaidAmount());
                    crt.setCreationDate(new Date());
                    crt.setLastRetryDate(crt.getCreationDate());
                    crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                            .findCardRetryTransactionType(getMappingPropertiesValue(LabelConstants.CARD_RETRY_TRANSACTION_TYPE_IMPORTED_STORED_FORWARD_ID)));
                    crt.setIsRfid(ptd.isIsRfid());
                    // Set track 2 data as object will remain in memory so first
                    // attempt is with track2
                    // Safe to store in memory
                    if (StringUtils.isNotBlank(purchase.getCardData())) {
                        final String decryptedData =
                                this.cryptoService.decryptData(purchase.getCardData(), pointOfSale.getId(), purchase.getPurchaseGmt());
                        final CustomerCardType customerCardType =
                                this.customerCardTypeService.getCardTypeByTrack2Data(pointOfSale.getCustomer().getId(), decryptedData);
                        final Track2Card track2Card = new Track2Card(customerCardType, pointOfSale.getCustomer().getId(), decryptedData);
                        // Card Data fields
                        crt.setCardData(this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                                                       track2Card.getCreditCardPanAndExpiry()));
                        crt.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
                        crt.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
                        crt.setBadCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getPrimaryAccountNumber(),
                                                                                   CryptoConstants.HASH_BAD_CREDIT_CARD));
                        crt.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                                CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
                        crt.setCardType(track2Card.getDisplayName());
                    }
                    this.entityDao.save(crt);
                    
                } else {
                    throw new CcProcessingRestrictionException("Customer does not have permission to process online credit card transaction.", e);
                }
            } else if (CardProcessingUtil.isCreditcallRequest(ptd.getMerchantAccount().getProcessor().getName(), 
                                                              this.processorService.findProcessor(CardProcessingConstants.PROCESSOR_ID_CREDITCALL),
                                                              this.processorService.findProcessor(CardProcessingConstants.PROCESSOR_ID_CREDITCALL_LINK)) 
                       && WebCoreUtil.causedByClientExceptionOrNetworkCommunicationException(e)) {
                LOG.error("Cannot access micro-services for storing Creditcall transaction details.");
                throw new CardTransactionException(e);
            } else if (e instanceof CryptoException) {
                throw (CryptoException) e;
            } else {
                throw new CardTransactionException(e);
            }
        }
        return needSavedObjects;
    }
    
    private void processLinkCPSRefund(final TransactionData transactionData, final EmbeddedTxObject embeddedTxObject) {
        if (transactionData.isCoreCPS() && embeddedTxObject.isCPSStoreForward()) {
            
            final CPSResponse cpsResp = (CPSResponse) embeddedTxObject;
            
            final List<CPSRefundData> cpsRefundData = this.cpsRefundDataService.findByChargeToken(cpsResp.getChargeToken());
            if (cpsRefundData != null && !cpsRefundData.isEmpty()) {
                this.cpsRefundDataService.manageCPSRefundData(cpsRefundData);
            }
        }
    }
    
    private void addCardRetryIfLink(final MerchantAccount merchantAccount, final PointOfSale pointOfSale, final Purchase purchase,
        final ProcessorTransaction ptd, final List<Object> returnObjectList) {
        if (merchantAccount != null && merchantAccount.getIsLink()) {
            returnObjectList.add(createCardRetryTransaction(pointOfSale, purchase, ptd, purchase.getCardData()));
        }
    }
    
    private void populateDataFromCPSResponse(final EmbeddedTxObject embeddedTxObject, final ProcessorTransaction ptd, final Purchase purchase,
        final List<Object> needSavedObjects) throws DataNotFoundException {
        
        final CPSResponse cpsResp = (CPSResponse) embeddedTxObject;
        
        if (!StringUtils.isBlank(cpsResp.getTerminalToken())) {
            final MerchantAccount timeOfTransactionMerchantAccount =
                    this.merchantAccountService.findByTerminalTokenAndIsLink(cpsResp.getTerminalToken());
            if (timeOfTransactionMerchantAccount != null) {
                ptd.setMerchantAccount(timeOfTransactionMerchantAccount);
            } else {
                final StringBuilder bdr = new StringBuilder().append("Merchant Accounts does not contain terminal Token: ")
                        .append(cpsResp.getTerminalToken()).append(". ");
                throw new DataNotFoundException(bdr.toString());
            }
        }
        
        if (embeddedTxObject.isCPSStoreForward()) {
            final CPSProcessData cpsProcessData =
                    this.cpsProcessDataService.find(ptd.getMerchantAccount().getTerminalToken(), cpsResp.getChargeToken());
            populateWithCPSProcessDataAndDelete(ptd, CardProcessingUtil.getFirstCardTypeOrNA(purchase.getPaymentCards()), cpsProcessData);
        } else {
            populateWithCPSResponse(ptd, cpsResp);
        }
        needSavedObjects.add(new CPSData(ptd, cpsResp.getChargeToken(), cpsResp.getReferenceNumber(), null, null, null));
        
        if (purchase.getPaymentCards() != null && !purchase.getPaymentCards().isEmpty()) {
            purchase.getPaymentCards().forEach(pc -> {
                pc.setProcessorTransactionType(ptd.getProcessorTransactionType());
                pc.setProcessorTransaction(ptd);
                pc.setMerchantAccount(ptd.getMerchantAccount());
                pc.setCardLast4digits(ptd.getLast4digitsOfCardNumber());
                pc.setCreditCardType(this.creditCardTypeService.getCreditCardTypeByName(ptd.getCardType()));
                pc.setIsApproved(ptd.isIsApproved());
            });
        }
        
    }
    
    private ProcessorTransactionType isSettled(final EmbeddedTxObject txObj) {
        if (txObj.isCPSStoreForward()) {
            return this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS);
        } else {
            return this.processorTransactionTypeService
                    .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS);
        }
    }
    
    private ProcessorTransaction populateWithCPSProcessDataAndDelete(final ProcessorTransaction ptd, final String cardType,
        final CPSProcessData cpsProcessData) {
        if (cpsProcessData == null) {
            populateProcessorTransactionProps(ptd, cardType, WebCoreConstants.N_A_STRING, WebCoreConstants.N_A_STRING, WebCoreConstants.N_A_STRING,
                                              (short) 0, DateUtil.getCurrentGmtDate());
            ptd.setIsApproved(false);
        } else {
            
            populateProcessorTransactionProps(ptd, cpsProcessData.getCardType(), cpsProcessData.getProcessorTransactionId(),
                                              cpsProcessData.getAuthorizationNumber(), cpsProcessData.getReferenceNumber(),
                                              cpsProcessData.getLast4Digits(), cpsProcessData.getProcessedGMT());
            
            if (cpsProcessData.isExpired()) {
                ptd.setIsApproved(false);
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE));
            }
            
            this.entityDao.delete(cpsProcessData);
        }
        return ptd;
    }
    
    private ProcessorTransaction populateWithCPSResponse(final ProcessorTransaction ptd, final CPSResponse cpsResp) {
        populateProcessorTransactionProps(ptd, cpsResp.getCardType() == null ? CardProcessingConstants.NAME_CREDIT_CARD : cpsResp.getCardType(),
                                          cpsResp.getProcessorTransactionId(), cpsResp.getAuthorizationNumber(), cpsResp.getReferenceNumber(),
                                          cpsResp.getLast4Digits() == null ? 0 : Short.parseShort(cpsResp.getLast4Digits()),
                                          DateUtil.convertUTCToGMT(cpsResp.getProcessedDate()));
        return ptd;
    }
    
    private ProcessorTransaction populateProcessorTransactionProps(final ProcessorTransaction ptd, final String cardType,
        final String processorTransactionId, final String authorizationNumber, final String referenceNumber, final Short last4digitsOfCardNumber,
        final Date processingDate) {
        ptd.setCardType(cardType);
        ptd.setProcessorTransactionId(processorTransactionId);
        ptd.setAuthorizationNumber(authorizationNumber);
        ptd.setReferenceNumber(referenceNumber);
        ptd.setLast4digitsOfCardNumber(last4digitsOfCardNumber);
        ptd.setProcessingDate(processingDate);
        ptd.setIsApproved(true);
        return ptd;
    }
    
    private CardTransaction createCardTransactionData(final ProcessorTransaction ptd, final CardEaseData cardEase) {
        final CardTransaction cardTransaction = new CardTransaction();
        cardTransaction.setAmount(ptd.getAmount());
        cardTransaction.setAuthorizationNumber(ptd.getAuthorizationNumber());
        cardTransaction.setLast4Digits(String.valueOf(ptd.getLast4digitsOfCardNumber()));
        cardTransaction.setCardType(ptd.getCardType());
        final ProcessorTransactionType processorTransactionType =
                this.processorTransactionTypeService.findProcessorTransactionType(ptd.getProcessorTransactionType().getId());
        cardTransaction.setTransactionType(processorTransactionType.getName());
        final String processorName = ptd.getMerchantAccount().getProcessor().getName().replace("processor.", "");
        final String output = processorName.substring(0, 1).toUpperCase() + processorName.substring(1);
        cardTransaction.setProcessorType(output);
        cardTransaction.setProcessorSpecificData(cardEase);
        cardTransaction.setProcessedDate(DateUtil.format(DateUtil.getCurrentGmtDate(), DateUtil.UTC_TRANSACTION_DATE_TIME_FORMAT));
        cardTransaction.setReferenceNumber(ptd.getReferenceNumber());
        cardTransaction.setProcessorTransactionId(ptd.getProcessorTransactionId());
        cardTransaction.setTerminalToken(ptd.getMerchantAccount().getTerminalToken());
        return cardTransaction;
    }
    
    private String createCardTransactionJson(final CardTransaction cardTransactionData) {
        String cardEaseJson = null;
        try {
            cardEaseJson = this.json.serialize(cardTransactionData);
        } catch (JsonException e) {
            final StringBuilder bdr = new StringBuilder().append("Unable to read JSON: ").append(cardTransactionData).append(". ");
            LOG.error(bdr.toString(), e);
            return null;
        }
        return cardEaseJson;
    }
    
    private String pushEMVDataForToken(final String cardEaseData) throws Exception {
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        return payService.pushEMVData(cardEaseData).execute().toString(Charset.defaultCharset());
    }
    
    /*
     * EMS-10037 Recoverable for Elavon Direct
     * Data Flow for Reversal:
     * Scenario:
     * PreAuth record is moved to PreAuthHolding after 1 day.
     * SET PreAuthId to NULL in ElavonTransaction Table during Reversal.
     * Send Reversal to Elavon to reverse the charge.
     * After 1 day, Iris receive PostAuth
     * PostAuth XML does not contain CardData but it contains PreAuthId.
     * Logic:
     * Iris when receives PostAuth, checks for PreAuthId in ElavonTransaction Table. It does not find as it was SET to NULL during Reversal.
     * At this point, Iris will create Purchase, Permit, ProcessorTransaction records. But not ElavonTransaction record.
     * Here are the values for ProcessorTransaction record:
     * ProcessorTransactionTypeId = 20 (Recoverable)
     * AuthorizationNumber = PreAuthId in PostAuthXML
     * IsApproved = 0
     * Need to ensure the incoming Transaction request is for Elavon Direct, and contains EmsPreAuthId, CardAuthorizationId,
     * PreAuth record has moved to PreAuthHolding, not store forward and not cancelled.
     * If qualified, this method will:
     * - put emsPreAuthId to processorTransaction authorizationNumber field
     * - set to RECOVERABLE (id = 20)
     * - set is not approved
     * - set cardType to blank string
     */
    private void adjustElavonRecoverable(final TransactionData transactionData, final TransactionDto txDto) {
        PreAuth preAuth = null;
        if (isEmsPreAuthId(txDto.getEmsPreAuthId())) {
            try {
                preAuth = this.entityDao.get(PreAuth.class, Long.parseLong(txDto.getEmsPreAuthId()));
            } catch (HibernateObjectRetrievalFailureException e) {
                // do nothing
            }
        }
        if (isElavon(transactionData.getProcessorTransaction().getPointOfSale()) && qualifyForRecoverable(preAuth, transactionData, txDto)) {
            
            transactionData.getProcessorTransaction().setAuthorizationNumber(txDto.getEmsPreAuthId());
            transactionData.getProcessorTransaction().setProcessorTransactionType(this.entityDao
                    .get(ProcessorTransactionType.class, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_RECOVERABLE));
            transactionData.getProcessorTransaction().setIsApproved(false);
            if (transactionData.getProcessorTransaction().getCardType() == null) {
                transactionData.getProcessorTransaction().setCardType("");
            }
        }
    }
    
    /*
     * Return true if:
     * - emsPreAuthId is not blank and not null
     * - it's all digits
     */
    private boolean isEmsPreAuthId(final String emsPreAuthId) {
        if (StringUtils.isNotBlank(emsPreAuthId) && WebCoreUtil.checkIfAllDigits(emsPreAuthId)) {
            return true;
        }
        return false;
    }
    
    /*
     * Return true only if:
     * - cardAuthorizationId exists (e.g. <Transaction>...<CardAuthorizationId>191871</CardAuthorizationId>...</Transaction>)
     * - emsPreAuthId exists (e.g. <Transaction>...<EmsPreAuthId>299557</EmsPreAuthId>...</Transaction>)
     * - preAuth is null (data is in PreAuthHolding table)
     * - not Store & Forward
     * - not Cancelled Transaction
     */
    private boolean qualifyForRecoverable(final PreAuth preAuth, final TransactionData transactionData, final TransactionDto txDto) {
        if (StringUtils.isNotBlank(txDto.getCardAuthorizationId()) && isEmsPreAuthId(txDto.getEmsPreAuthId()) && preAuth == null
            && !transactionData.isStoreForward() && !transactionData.isCancelledTransaction()) {
            return true;
        }
        return false;
    }
    
    private boolean isElavon(final PointOfSale pointOfSale) {
        if (pointOfSale == null) {
            return false;
        }
        final MerchantAccount ma = this.merchantAccountService
                .findFromMechantPOSByPointOfSaleAndProcId(pointOfSale.getId(), CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX);
        if (ma != null) {
            return true;
        }
        return false;
    }
    
    private void saveTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final PosServiceState posServiceState, final List<Object> needSavedObjects, final boolean deletePreAuthIfExists) {
        
        // Coupon and Purchase is not a parent-child relationship, need to save
        // it independently.
        if (purchase.getCoupon() != null) {
            this.entityDao.saveOrUpdate(purchase.getCoupon());
        }
        
        savePurchaseAndProcessorTransaction(pointOfSale, purchase, ptd);
        
        // Need to insert Purchase object first then populate PaymentSmartCard
        // with it.
        for (Object obj : needSavedObjects) {
            if (obj instanceof PreAuth) {
                if (deletePreAuthIfExists) {
                    this.entityDao.delete(obj);
                }
                continue;
            }
            if (obj instanceof PaymentSmartCard) {
                ((PaymentSmartCard) obj).setPurchase(purchase);
            }
            this.entityDao.saveOrUpdate(obj);
        }
        
        processTransactionEvents(pointOfSale, purchase);
        
        // Searches SmsAlert & ExtensiblePermit and deletes the records if
        // exist.
        final Permit permit = getFirstPermit(purchase.getPermits());
        if (permit != null) {
            final SmsAlert smsAlert = findSmsAlert(pointOfSale, purchase, permit);
            if (smsAlert != null) {
                this.entityDao.delete(smsAlert);
                
                final ExtensiblePermit oldPermit = this.extensiblePermitService.findByMobileNumber(smsAlert.getMobileNumber().getNumber());
                if (oldPermit != null) {
                    this.entityDao.delete(oldPermit);
                }
            }
        }
        
        // Retrieve PosServiceState from database and update values.
        savePosServiceStateAndPos(pointOfSale, posServiceState, purchase.getPaystationSetting());
    }
    
    @Override
    public List<Object> processTransactionWithSmsAlert(final TransactionData transactionData, final EmbeddedTxObject embeddedTxObject)
        throws DuplicateTransactionRequestException, CryptoException {
        
        // Saves all objects
        // Suppress because of side effects
        @SuppressWarnings("PMD.PrematureDeclaration")
        final List<Object> objs = processTransaction(transactionData, false, null, embeddedTxObject);
        
        final PointOfSale pointOfSale = transactionData.getPointOfSale();
        final Purchase purchase = transactionData.getPurchase();
        final ProcessorTransaction ptd = transactionData.getProcessorTransaction();
        
        final ExtensiblePermit extensiblePermit = transactionData.getExtensiblePermit();
        
        // Manages SmsAlert & ExtensiblePermit.
        boolean createCancelledProcTransRecord = false;
        if (purchase.getTransactionType().getId() == getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_CANCELLED_ID)) {
            final String authNumber = ptd.getAuthorizationNumber();
            createCancelledProcTransRecord = (authNumber == null) ? false : authNumber.length() > 0;
        }
        if (!createCancelledProcTransRecord) {
            
            final MerchantAccount merchantAccount = this.merchantAccountService
                    .findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSale.getId(), CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
            if (CardProcessingUtil.isNotNullAndIsLink(merchantAccount)) {
                final PreAuth preAuth = transactionData.getPreAuth();
                if (preAuth != null) {
                    extensiblePermit.setCardData(preAuth.getCardToken());
                    extensiblePermit.setLast4digit(preAuth.getLast4digitsOfCardNumber());
                } else if (!transactionData.isEMVOrCoreCPS()) {
                    throw new IllegalArgumentException(
                            String.format("No PreAuth exists for EbP transaction for PointOfSale %s", pointOfSale.getSerialNumber()));
                
                } else if (embeddedTxObject != null 
                        && embeddedTxObject instanceof CPSResponse
                        && transactionData.isCoreCPS()) {
                    final CPSResponse cpsResp = (CPSResponse) embeddedTxObject;
                    extensiblePermit.setCardData(cpsResp.getCardToken());
                    extensiblePermit.setLast4digit(Short.parseShort(cpsResp.getLast4Digits()));
                }
            } else {
                /* decrypt track2data */
                setExtensiblePermitCardData(pointOfSale, purchase, extensiblePermit);
            }
            extensiblePermit.setIsRfid(ptd.isIsRfid());
        }
        SmsAlert smsAlert = transactionData.getSmsAlert();
        SmsAlert storedSmsAlert = this.smsAlertService.findByMobileNumber(smsAlert.getMobileNumber().getNumber());
        if (storedSmsAlert == null) {
            final MobileNumber existing = this.mobileNumberService.findMobileNumber(smsAlert.getMobileNumber().getNumber());
            if (existing != null) {
                smsAlert.setMobileNumber(existing);
            }
        } else {
            storedSmsAlert = updateSmsAlertProperties(storedSmsAlert, smsAlert);
            this.entityDao.update(storedSmsAlert);
            
            smsAlert = storedSmsAlert;
        }
        
        // It's extend-by-phone "add time".
        if (purchase.getTransactionType().getId() == getMappingPropertiesValue(LabelConstants.TRANSACTION_TYPE_ADD_TIME_EBP_ID)) {
            // Extends using same mobile number.
            if (storedSmsAlert != null) {
                // Copies data back from input SMSAlert and updates record in
                // the database.
                this.extensiblePermitService.updateLatestExpiryDate(extensiblePermit.getPermitExpireGmt(), extensiblePermit.getCardData(),
                                                                    extensiblePermit.getLast4digit(), extensiblePermit.getMobileNumber().getNumber());
            } else {
                // Searches SMSAlert & EMSExtensiblePermit and deletes the
                // records if exist.
                final Permit permit = getFirstPermit(purchase.getPermits());
                /*
                 * In TransactionHelperImpl, locateOriginalPermit method, if it's extend-by-phone this method will
                 * load original Permit record into the new Permit's "originalPermit" property. Use the first permit
                 * in the latest ExtensiblePermit.
                 */
                if (permit.getPermit() != null) {
                    extensiblePermit.setOriginalPermit(permit.getPermit());
                }
                
                final SmsAlert oldSmsAlert = findSmsAlert(pointOfSale, purchase, permit);
                if (oldSmsAlert != null) {
                    this.entityDao.delete(oldSmsAlert);
                }
                final ExtensiblePermit oldPermit = this.extensiblePermitService.findByMobileNumber(smsAlert.getMobileNumber().getNumber());
                if (oldPermit != null) {
                    this.entityDao.delete(oldPermit);
                }
                
                // Saves SmsAlert & ExtensiblePermit.
                this.entityDao.save(smsAlert);
                this.entityDao.save(extensiblePermit);
                
                // Saves sms transaction. */
                this.entityDao.save(setUpSmsTransactionLog(purchase, permit, smsAlert));
            }
        } else if (purchase.getTransactionType().getId() == getMappingPropertiesValue(LabelConstants.TRANSACTION_TYPE_REGULAR_EBP_ID)) {
            ExtensiblePermit storedExtensiblePermit = this.extensiblePermitService.findByMobileNumber(extensiblePermit.getMobileNumber().getNumber());
            if (storedExtensiblePermit != null) {
                storedExtensiblePermit = updateExtensiblePermitProperties(storedExtensiblePermit, extensiblePermit);
                this.entityDao.update(storedExtensiblePermit);
            } else {
                // It's extend-by-phone first time.
                this.entityDao.save(extensiblePermit);
            }
            
            if (storedSmsAlert == null) {
                this.entityDao.save(smsAlert);
            }
            
        }
        return objs;
    }
    
    private void setExtensiblePermitCardData(final PointOfSale pointOfSale, final Purchase purchase, final ExtensiblePermit extensiblePermit)
        throws CryptoException {
        final String decryptedData = StringUtils.isNotBlank(extensiblePermit.getCardData())
                ? this.cryptoService.decryptData(extensiblePermit.getCardData(), pointOfSale.getId(), purchase.getPurchaseGmt())
                : StandardConstants.STRING_EMPTY_STRING;
        
        if (StringUtils.isNotBlank(decryptedData)) {
            final CustomerCardType customerCardType =
                    this.customerCardTypeService.getCardTypeByTrack2DataOrAccountNumber(pointOfSale.getCustomer().getId(), decryptedData);
            final Track2Card track2Card = new Track2Card(customerCardType, pointOfSale.getCustomer().getId(), decryptedData);
            
            /*
             * from the track2data, retrieve PAN + ExpiryDate only and store
             * it in DB.
             */
            extensiblePermit
                    .setCardData(this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry()));
            extensiblePermit.setLast4digit(track2Card.getLast4DigitsOfAccountNumber());
        }
    }
    
    private List<Object> determineTransactionCardFields(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final List<Object> returnObjectList, final boolean isManualSave) throws CryptoException {
        Purchase updatedPurchase = purchase;
        String cardData = updatedPurchase.getCardData() == null ? "" : updatedPurchase.getCardData();
        
        /*
         * TransactionHandler populatePaymentSmartCard method can place a
         * PaymentSmartCard object to Purchase under transient method
         * 'setPaymentSmartCard'.
         */
        if (updatedPurchase.getPaymentSmartCard() != null) {
            returnObjectList.add(updatedPurchase.getPaymentSmartCard());
            updatedPurchase.setPaymentSmartCard(null);
        }
        
        int cardTypeId = WebCoreConstants.CREDIT_CARD;
        final CardType cardType = findCardType(updatedPurchase);
        if (cardType != null && cardType.getId() == WebCoreConstants.VALUE_CARD) {
            cardTypeId = cardType.getId();
        }
        
        /*
         * If no magstripe card was used for payment -> exit. CardPaidAmount is
         * in cents (base 100) Or when Pay Station has moved card data onto Boss
         * Key and only contains minimum information
         */
        final int oneCent = 1;
        if (cardTypeId != WebCoreConstants.VALUE_CARD
            && (updatedPurchase.getCardPaidAmount() < oneCent || isCardPaidNoCardDataAuthNum(updatedPurchase))) {
            removeObjectReferencesAndTransactionType(updatedPurchase, ptd);
            updatedPurchase.setCardData(null);
            
            return returnObjectList;
        }
        
        final MerchantAccount macct =
                this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(pointOfSale.getId(), cardTypeId);
        if (macct == null) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("POS serial number: ").append(pointOfSale.getSerialNumber());
            bdr.append(" doesn't have a merchant account for cardTypeId: ").append(cardTypeId);
            LOG.error(bdr.toString());
            this.mailerService.sendAdminErrorAlert("S&F merchant account is null, ", bdr.toString(), null);
        }
        ptd.setMerchantAccount(macct);
        
        // No card data -> assume credit card.
        if (StringUtils.isBlank(cardData) && StringUtils.isNotBlank(ptd.getAuthorizationNumber())) {
            ptd.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
            ptd.setLast4digitsOfCardNumber((short) 0);
            ptd.setIsApproved(true);
            if (ptd.getProcessorTransactionType() == null || ptd.getProcessorTransactionType()
                    .getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS) {
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_PREAUTH_ID)));
            }
            updatedPurchase = updatePaymentCardsMerchantAccount(updatedPurchase, macct, ptd.getProcessorTransactionType());
            return returnObjectList;
        }
        
        if (CardProcessingUtil.isNotNullAndIsLink(macct)) {
            if (ptd.isIsOffline() && !isManualSave) {
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_N_A_ID)));
                
                updatedPurchase = updatePaymentCardsMerchantAccount(updatedPurchase, macct, ptd.getProcessorTransactionType(), true);
            } else {
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_STORE_FORWARD_ID)));
                
            }
            
            populateProcessorTransactionProps(ptd, CardProcessingUtil.getFirstCardTypeOrNA(purchase.getPaymentCards()), WebCoreConstants.N_A_STRING,
                                              WebCoreConstants.N_A_STRING, WebCoreConstants.N_A_STRING, (short) 0, DateUtil.getCurrentGmtDate());
            return returnObjectList;
        }
        
        // ---------------------------------------------------------------------------------------------------
        // The following logic is when card data is present.
        // ---------------------------------------------------------------------------------------------------
        boolean isVersion620OrAbove = false;
        
        // Decrypt card data if length > 40 (max track2 length is 37) AND it's not Link
        if (cardData.length() > 40) {
            try {
                cardData = this.cryptoService.decryptData(cardData, pointOfSale.getId(), updatedPurchase.getPurchaseGmt());
                isVersion620OrAbove = true;
                
            } catch (CryptoException e) {
                if (e.getCause() instanceof ClientException) {
                    // clear card data
                    updatedPurchase.setCardData(null);
                    
                    // mark as invalid data
                    ptd.setProcessorTransactionType(this.processorTransactionTypeService
                            .findProcessorTransactionType(getMappingPropertiesValue("processorTransactionType.invalidData.id")));
                    ptd.setCardType(CardProcessingConstants.NAME_CREDIT_CARD);
                    ptd.setIsApproved(true);
                    
                    final StringBuilder bdr = new StringBuilder();
                    bdr.append(ptd.toString()).append("---").append(cardData);
                    // log
                    bdr.replace(0, bdr.length(), "");
                    bdr.append("Unable to decrypt S&F card data for PointOfSale serialNumber: ").append(pointOfSale.getSerialNumber());
                    LOG.error(bdr.toString(), e);
                    this.mailerService.sendAdminErrorAlert("S&F Decryption error", bdr.toString(), null);
                    
                    // return
                    return returnObjectList;
                } else {
                    throw e;
                }
            }
        }
        // public Track2Card(CustomerCardType customerCardType, int customerId,
        // String cardData)
        final Track2Card track2Card =
                new Track2Card(this.customerCardTypeService.getCardTypeByTrack2Data(pointOfSale.getCustomer().getId(), cardData),
                        pointOfSale.getCustomer().getId(), cardData);
        
        // Determine card type
        final String cardTypeName = track2Card.getDisplayName();
        
        // If card type is still unknown, log warning
        if (cardTypeName.equals(CardProcessingConstants.NAME_UNKNOWN) && WebCoreUtil.isUnknowCardType(cardTypeId)) {
            LOG.error("Invalid Card Data -> unable to determine card type for PointOfSale serialNumber: " + pointOfSale.getSerialNumber());
        }
        
        // Is online?
        if (ptd.getAuthorizationNumber() != null && ptd.getAuthorizationNumber().length() > 0) {
            // Clear credit card data -> credit card data shouldn't happen but
            // still have to check
            if (track2Card.isCreditCard()) {
                updatedPurchase.setCardData(null);
            } else {
                // Keep value card data
                updatedPurchase.setCardData(track2Card.getPrimaryAccountNumber());
            }
            
            ptd.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                    CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
            ptd.setCardType(cardTypeName);
            
            // Set last 4 digits
            try {
                ptd.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
            } catch (NumberFormatException e) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Invalid Card Data -> non-numeric characters in last 4 digits: ").append(track2Card.getLast4DigitsOfAccountNumber())
                        .append(" for PointOfSale serialNumber: ");
                bdr.append(pointOfSale.getSerialNumber());
                LOG.error(bdr.toString());
                this.mailerService.sendAdminAlert("Invalid last 4 digits", bdr.toString(), e);
                
                ptd.setLast4digitsOfCardNumber((short) 0);
            }
            
            ptd.setIsApproved(true);
            if (ptd.getProcessorTransactionType() == null || ptd.getProcessorTransactionType()
                    .getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_CHARGE_WITH_NO_REFUNDS) {
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_PREAUTH_ID)));
            }
            updatedPurchase = updatePaymentCardsMerchantAccount(updatedPurchase, macct, ptd.getProcessorTransactionType());
            return returnObjectList;
        }
        
        // Card is offline or store & forward.
        
        // Is credit card? -> Shouldn't happen prior to 6.2.0, but still have to
        // check.
        if (track2Card.isCreditCard()) {
            /*
             * Create and save CardRetryTransaction for Store & Forward and Offline.
             * In 'cardProcessingManager.processTransactionCharge' it will delete approved
             * CardRetryTransaction record.
             */
            final CardRetryTransaction crt = createCardRetryTransaction(pointOfSale, updatedPurchase, ptd, track2Card);
            if (!isManualSave) {
                ptd.setProcessorTransactionType(this.processorTransactionTypeService
                        .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_N_A_ID)));
            }
            updatedPurchase = updatePaymentCardsMerchantAccount(updatedPurchase, macct, ptd.getProcessorTransactionType(), true);
            
            // If the PS version is 6.2.0 or above, create store and forward
            // transaction.
            if (isVersion620OrAbove && updatedPurchase.isIsOffline()) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("+++ recognized as offline Batch CC transaction ++++");
                }
                
            } else if (isVersion620OrAbove && !updatedPurchase.isIsOffline()) {
                
                if (StringUtils.isBlank(ptd.getCardHash())) {
                    ptd.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(),
                                                                            CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
                }
                if (LOG.isDebugEnabled()) {
                    LOG.debug("+++ recognized as online S&F CC transaction ++++");
                }
            }
            
            final Set<CardRetryTransaction> set = new HashSet<CardRetryTransaction>(1);
            set.add(crt);
            pointOfSale.setCardRetryTransactions(set);
            returnObjectList.add(crt);
        } else {
            
            // Passcard & Smart Card. Note: Patroller cards are an offline only "custom" card
            updatedPurchase.setCardData(track2Card.getPrimaryAccountNumber());
            removeObjectReferencesAndTransactionType(updatedPurchase, ptd);
        }
        return returnObjectList;
    }
    
    private void determineTransactionExcessPayment(final Purchase purchase) {
        // Removed EMS 6.3.11 RpcPaystationAppServiceImpl,
        // determineTransactionExcessPayment, 'replenish transactions' section.
        
        // Excess is $0 for transactions with a refund ticket
        if (purchase.isIsRefundSlip()) {
            purchase.setExcessPaymentAmount(0);
        } else {
            final int excessPayment =
                    purchase.getCardPaidAmount() + purchase.getCashPaidAmount() - purchase.getChangeDispensedAmount() - purchase.getChargedAmount();
            purchase.setExcessPaymentAmount(excessPayment <= 0 ? 0 : excessPayment);
        }
    }
    
    private void savePurchaseAndProcessorTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd) {
        /*
         * 1. Insert Purchase first. It contains other objects which should be
         * inserted automatically by Hibernate cascade.
         */
        this.entityDao.saveOrUpdate(purchase);
        
        /*
         * 2. Insert ProcessorTransaction.
         */
        if (ptd.getProcessorTransactionType().getId() != getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_N_A_ID)) {
            ptd.setPurchase(purchase);
            
            final ProcessorTransactionType preauthPtd = this.processorTransactionTypeService
                    .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_PREAUTH_ID));
            // Sets processing date to purchased date to avoid duplicated
            // processor transaction records being created with different
            // processing date for the same transaction.
            if (ptd.getProcessorTransactionType().getId() == preauthPtd.getId()) {
                ptd.setProcessingDate(ptd.getPurchasedDate());
            } else {
                ptd.setProcessingDate(DateUtil.getCurrentGmtDate());
            }
            this.entityDao.saveOrUpdate(ptd);
        }
        
        /*
         * 3. Save third party transaction push if exist.
         */
        saveTransaction2Push(purchase, getThirdPartyServiceAccount(pointOfSale.getCustomer().getId()));
        
        /*
         * 4. Save ActivePermit.
         */
        saveActivePermit(purchase);
    }
    
    private Purchase updateForCancellation(final Purchase purchase, final boolean coreCPSFlag) {
        
        if (purchase.getCoupon() != null) {
            if (purchase.getCoupon().getNumberOfUsesRemaining() != null
                && purchase.getCoupon().getNumberOfUsesRemaining().shortValue() != WebCoreConstants.COUPON_USES_UNLIMITED) {
                purchase.getCoupon().setNumberOfUsesRemaining((short) (purchase.getCoupon().getNumberOfUsesRemaining() + 1));
            }
        }
        
        if (purchase.getPurchaseTaxes() != null && !coreCPSFlag) {
            PurchaseTax pTax;
            final Iterator<PurchaseTax> pTaxIter = purchase.getPurchaseTaxes().iterator();
            while (pTaxIter.hasNext()) {
                pTax = pTaxIter.next();
                pTax.setTaxAmount((short) 0);
            }
        }
        
        if (purchase.getPermits() != null) {
            Permit permit;
            final Iterator<Permit> perIter = purchase.getPermits().iterator();
            while (perIter.hasNext()) {
                permit = perIter.next();
                permit.setPermitExpireGmt(purchase.getPurchaseGmt());
                permit.setPermitOriginalExpireGmt(purchase.getPurchaseGmt());
            }
        }
        
        if (purchase.getPaymentType().getId() != WebCoreConstants.PAYMENT_TYPE_CASH_SMART
            && purchase.getPaymentType().getId() != WebCoreConstants.PAYMENT_TYPE_SMART_CARD && !coreCPSFlag) {
            purchase.setCardPaidAmount(0);
            purchase.setCardData(null);
            if (purchase.getPaymentCards() != null) {
                PaymentCard payCard;
                final Iterator<PaymentCard> pcIter = purchase.getPaymentCards().iterator();
                while (pcIter.hasNext()) {
                    payCard = pcIter.next();
                    payCard.setAmount(0);
                    payCard.setCardLast4digits((short) 0);
                }
            }
        }
        return purchase;
    }
    
    private void saveActivePermit(final Purchase purchase) {
        final Permit permit = getFirstPermit(purchase.getPermits());
        if (permit != null) {
            saveActivePermit(permit, purchase);
        }
    }
    
    /**
     * There will be only ONE permit in Purchase if request is coming from
     * TransactionHandler, see TransactionHandler,
     * populatePermitAndSetPurchase() method.
     */
    private Permit getFirstPermit(final Set<Permit> permits) {
        if (permits != null && !permits.isEmpty()) {
            final Iterator<Permit> permitsIter = permits.iterator();
            if (permitsIter.hasNext()) {
                return permitsIter.next();
            }
        }
        return null;
    }
    
    private void saveTransaction2Push(final Purchase purchase, final Collection<ThirdPartyServiceAccount> accounts) {
        final Permit permit = getFirstPermit(purchase.getPermits());
        boolean isPushed = false;
        if (!accounts.isEmpty()) {
            for (ThirdPartyServiceAccount account : accounts) {
                if (account.isIsPaystationBased()) {
                    if (containsPaystation(purchase.getPointOfSale().getId(), account)) {
                        isPushed = true;
                    }
                } else {
                    isPushed = true;
                }
                
                if (isPushed) {
                    final Transaction2Push push = new Transaction2Push();
                    push.setPointOfSale(purchase.getPointOfSale());
                    push.setPurchasedDate(purchase.getPurchaseGmt());
                    push.setTicketNumber(purchase.getPurchaseNumber());
                    push.setThirdPartyServiceAccount(account);
                    push.setPaystationSettingName(purchase.getPaystationSetting().getName());
                    if (permit.getLicencePlate() != null) {
                        push.setPlateNumber(permit.getLicencePlate().getNumber());
                    }
                    push.setExpiryDate(permit.getPermitExpireGmt());
                    push.setChargedAmount(purchase.getChargedAmount());
                    push.setStallNumber(permit.getSpaceNumber());
                    push.setLocation(permit.getLocation());
                    push.setRetryCount((short) 0);
                    push.setLastRetryTime(new Date());
                    push.setIsOffline(false);
                    
                    final int sequenceNumber =
                            (int) this.thirdPartyServiceStallSequenceService.findSequenceNumber(permit.getLocation(), permit.getSpaceNumber());
                    push.setField1(Integer.toString(sequenceNumber));
                    
                    this.transaction2PushService.saveTransaction2Push(push);
                }
            }
        }
    }
    
    private Collection<ThirdPartyServiceAccount> getThirdPartyServiceAccount(final Integer customerId) {
        final Collection<ThirdPartyServiceAccount> result = new ArrayList<ThirdPartyServiceAccount>();
        final Collection<ThirdPartyServiceAccount> accounts = this.thirdPartyServiceAccountService.findByCustomerId(customerId);
        for (ThirdPartyServiceAccount account : accounts) {
            /*
             * If the Verrus push is integrated with this module, then this part
             * should be modified accordingly.
             */
            if (account.getType().equals(WebCoreConstants.VERRUS_STRING)) {
                continue;
            } else if (account.getType().equals(WebCoreConstants.STREETLINE_TXN_PUSH_STRING)) {
                result.add(account);
            }
        }
        
        return result;
    }
    
    private boolean containsPaystation(final int pointOfSaleId, final ThirdPartyServiceAccount account) {
        boolean result = false;
        final Collection<ThirdPartyServicePos> thirdPartyServicePoses = account.getThirdPartyServicePoses();
        for (ThirdPartyServicePos thirdPartyServicePos : thirdPartyServicePoses) {
            if (pointOfSaleId == thirdPartyServicePos.getPointOfSale().getId()) {
                result = true;
                break;
            }
        }
        
        return result;
    }
    
    private void processTransactionEvents(final PointOfSale pointOfSale, final Purchase purchase) {
        final EventData event = new EventData();
        event.setTimeStamp(purchase.getPurchaseGmt());
        event.setPointOfSaleId(pointOfSale.getId());
        event.setAction(WebCoreConstants.EVENT_ACTION_PRESENT_STRING);
        if (purchase.getBillCount() > 0) {
            event.setType(WebCoreConstants.EVENT_TYPE_BILL_ACCEPTOR);
            event.setInformation(Integer.toString(purchase.getBillCount()));
            this.rawSensorDataService.saveRawSensorData(pointOfSale.getSerialNumber(), event);
        }
        if (purchase.getCoinCount() > 0) {
            event.setType(WebCoreConstants.EVENT_TYPE_COIN_ACCEPTOR);
            event.setInformation(Integer.toString(purchase.getCoinCount()));
            this.rawSensorDataService.saveRawSensorData(pointOfSale.getSerialNumber(), event);
        }
    }
    
    /**
     * Searches SMSAlert & EMSExtensiblePermit using plate number, addTimeNumber
     * or stallNumber and deletes the records if exist. The search priority is:
     * 1st by plate number, 2nd add time number, 3rd stall number.
     * 
     * @param queryStallsBy
     *            int QueryStallsBy is a property of Paystation class and in
     *            database table PayStation.
     */
    private SmsAlert findSmsAlert(final PointOfSale pointOfSale, final Purchase purchase, final Permit permit) {
        final CustomerProperty queryBySpaceProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        final int querySpacesBy = Integer.parseInt(queryBySpaceProperty.getPropertyValue());
        
        final int locationId = purchase.getLocation().getId();
        final int customerId = pointOfSale.getCustomer().getId();
        final String paystationSettingName = purchase.getPaystationSetting().getName();
        final int addTimeNumber = permit.getAddTimeNumber();
        String licencePlate = null;
        if (permit.getLicencePlate() != null) {
            licencePlate = permit.getLicencePlate().getNumber();
        }
        final int spaceNumber = permit.getSpaceNumber();
        
        SmsAlert oldSmsAlert = null;
        if (StringUtils.isNotBlank(licencePlate)) {
            oldSmsAlert = findSmsAlertByPlateNumber(customerId, licencePlate);
        } else if (addTimeNumber > 0) {
            oldSmsAlert = findSmsAlertByAddTimeNum(querySpacesBy, locationId, customerId, paystationSettingName, addTimeNumber, spaceNumber);
        } else if (spaceNumber > 0) {
            oldSmsAlert = findSmsAlertBySpaceNumber(querySpacesBy, locationId, customerId, paystationSettingName, spaceNumber);
        }
        return oldSmsAlert;
    }
    
    /**
     * The steps are: 1. Depends on input 'queryStallsBy' value if it's
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_LOT_NUMBER (0), uses query with
     * lotNumber and addTimeNum. 2. If it's
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_REGION_ID (1), uses query with
     * regionId and addTimeNum. 3. If 'queryStallsBy' value is
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER (2), uses query with
     * customerId and addTimeNum. 4. Returns SMSAlert result or null.
     * 
     * The searching logic is mimic StallInfoRequest & StallInfoService, it
     * needs to be updated if StallInfo logic is changed.
     * 
     * @param queryStallsBy
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_LOT_NUMBER (0),
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_REGION_ID (1) or
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER (2)
     * @param locationId
     * @param customerId
     * @param paystationSettingName
     *            String From Transaction or SMSAlert object.
     * @param addTimeNum
     *            A column in SMSAlert table.
     * @param spaceNumber
     * @return SMSAlert SMSAlert object matches search criteria, otherwise it
     *         would be 'null'.
     */
    private SmsAlert findSmsAlertByAddTimeNum(final int queryStallsBy, final int locationId, final int customerId, final String paystationSettingName,
        final int addTimeNum, final int spaceNumber) {
        if (addTimeNum <= 0) {
            return null;
        }
        
        List<SmsAlert> alerts = null;
        switch (queryStallsBy) {
            case WebCoreConstants.POS_SPACE_QUERY_BY_LOCATION_ID:
                alerts = this.smsAlertService.findByAddTimeNumLocationIdAndSpaceNumber(locationId, spaceNumber, addTimeNum);
                break;
            case WebCoreConstants.POS_SPACE_QUERY_BY_CUSTOMER:
                alerts = this.smsAlertService.findByAddTimeNumCustomerIdAndSpaceNumber(customerId, spaceNumber, addTimeNum);
                break;
            default:
                return null;
        }
        if (alerts == null || alerts.isEmpty()) {
            return null;
        }
        return alerts.get(0);
    }
    
    /**
     * The steps are: 1. Searches SMSAlert table with input 'customerId' and
     * 'plateNumber'. 2. Returns SMSAlert result or null.
     * 
     * @param customerId
     *            customer id
     * @param licencePlate
     *            plate number.
     * @return SmsAlert SmsAlert object matches paystationId, purchasedDate and
     *         ticketNumber, otherwise it would be 'null'.
     */
    private SmsAlert findSmsAlertByPlateNumber(final int customerId, final String licencePlate) {
        if (StringUtils.isBlank(licencePlate)) {
            return null;
        }
        final List<SmsAlert> alerts = this.smsAlertService.findByLicencePlate(customerId, licencePlate);
        if (alerts.isEmpty()) {
            return null;
        }
        return alerts.get(0);
    }
    
    /**
     * The steps are: 1. Depends on input 'queryStallsBy' value if it's
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_LOT_NUMBER (0), uses query with
     * lotNumber and stallNumber. 2. if it's
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_REGION_ID: (1), uses query with
     * regionId and stallNumber. 3. If 'queryStallsBy' value is
     * EMSConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER (2), uses query with
     * customerId and stallNumber. 4. Returns SMSAlert result or null.
     * 
     * The searching logic is mimic StallInfoRequest & StallInfoService, it
     * needs to be updated if StallInfo logic is changed.
     * 
     * @param queryStallsBy
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_LOT_NUMBER (0),
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_REGION_ID (1) or
     *            EMSConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER (2)
     * @param locationId
     * @param customerId
     * @param paystationSettingName
     *            String From Transaction or SMSAlert object.
     * @param customerId
     *            customer id
     * @param spaceNumber
     *            A column in Transaction table.
     * @return SmsAlert SmsAlert object matches search criteria, otherwise it
     *         would be 'null'.
     */
    private SmsAlert findSmsAlertBySpaceNumber(final int queryStallsBy, final int locationId, final int customerId,
        final String paystationSettingName, final int spaceNumber) {
        if (spaceNumber <= 0) {
            return null;
        }
        
        List<SmsAlert> alerts = null;
        switch (queryStallsBy) {
            case WebCoreConstants.POS_SPACE_QUERY_BY_LOT_SETTING_NAME:
                if (StringUtils.isBlank(paystationSettingName)) {
                    return null;
                }
                alerts = this.smsAlertService.findByCustomerIdPaystationSettingNameAndSpaceNumber(customerId, paystationSettingName, spaceNumber);
                break;
            case WebCoreConstants.POS_SPACE_QUERY_BY_LOCATION_ID:
                alerts = this.smsAlertService.findByLocationIdAndSpaceNumber(locationId, spaceNumber);
                break;
            case WebCoreConstants.POS_SPACE_QUERY_BY_CUSTOMER:
                alerts = this.smsAlertService.findByCustomerIdAndSpaceNumber(customerId, spaceNumber);
                break;
            default:
                return null;
        }
        if (alerts == null || alerts.isEmpty()) {
            return null;
        }
        return alerts.get(0);
    }
    
    private SmsAlert updateSmsAlertProperties(final SmsAlert storedSmsAlert, final SmsAlert smsAlert) {
        storedSmsAlert.setPermitBeginGmt(smsAlert.getPermitBeginGmt());
        storedSmsAlert.setPermitExpireGmt(smsAlert.getPermitExpireGmt());
        storedSmsAlert.setIsAlerted(smsAlert.isIsAlerted());
        storedSmsAlert.setIsAutoExtended(smsAlert.isIsAutoExtended());
        storedSmsAlert.setIsLocked(smsAlert.isIsLocked());
        storedSmsAlert.setNumberOfRetries(smsAlert.getNumberOfRetries());
        if (smsAlert.getLicencePlate() != null) {
            storedSmsAlert.setLicencePlate(smsAlert.getLicencePlate());
        }
        if (smsAlert.getPaystationSettingName() != null) {
            storedSmsAlert.setPaystationSettingName(smsAlert.getPaystationSettingName());
        }
        storedSmsAlert.setLocation(smsAlert.getLocation());
        storedSmsAlert.setSpaceNumber(smsAlert.getSpaceNumber());
        storedSmsAlert.setTicketNumber(smsAlert.getTicketNumber());
        storedSmsAlert.setPointOfSale(smsAlert.getPointOfSale());
        storedSmsAlert.setCustomer(smsAlert.getCustomer());
        storedSmsAlert.setAddTimeNumber(smsAlert.getAddTimeNumber());
        storedSmsAlert.setCallbackId(smsAlert.getCallbackId());
        return storedSmsAlert;
    }
    
    private ExtensiblePermit updateExtensiblePermitProperties(final ExtensiblePermit storedExtensiblePermit,
        final ExtensiblePermit extensiblePermit) {
        storedExtensiblePermit.setOriginalPermit(extensiblePermit.getOriginalPermit());
        storedExtensiblePermit.setCardData(extensiblePermit.getCardData());
        storedExtensiblePermit.setLast4digit(extensiblePermit.getLast4digit());
        storedExtensiblePermit.setPermitBeginGmt(extensiblePermit.getPermitBeginGmt());
        storedExtensiblePermit.setPermitExpireGmt(extensiblePermit.getPermitExpireGmt());
        storedExtensiblePermit.setIsRfid(extensiblePermit.getIsRfid());
        return storedExtensiblePermit;
    }
    
    private SmsTransactionLog setUpSmsTransactionLog(final Purchase purchase, final Permit permit, final SmsAlert smsAlert) {
        final SmsTransactionLog smsLog = new SmsTransactionLog();
        smsLog.setPointOfSale(smsAlert.getPointOfSale());
        smsLog.setPurchaseGmt(purchase.getPurchaseGmt());
        smsLog.setExpiryDateGmt(permit.getPermitExpireGmt());
        smsLog.setTicketNumber(smsAlert.getTicketNumber());
        smsLog.setSmsMessageType(this.smsMessageTypeService
                .findSmsMessageType(getMappingPropertiesValue(LabelConstants.SMS_MESSAGE_TYPE_TRANSACTION_RECEIVED_ID)));
        smsLog.setMobileNumber(smsAlert.getMobileNumber());
        smsLog.setTimeStampGmt(DateUtil.getCurrentGmtDate());
        return smsLog;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void createTransaction(final Permit permit, final Purchase purchase, final PaymentCard paymentCard,
        final ProcessorTransaction processorTransaction) {
        final Date nowGmt = DateUtil.changeTimeZone(new Date(), DateUtil.GMT);
        purchase.setCreatedGmt(nowGmt);
        this.entityDao.saveOrUpdate(purchase);
        this.entityDao.saveOrUpdate(permit);
        if (CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_NA != processorTransaction.getProcessorTransactionType().getId()) {
            this.entityDao.saveOrUpdate(paymentCard);
            this.entityDao.saveOrUpdate(processorTransaction);
        }
        
        saveActivePermit(permit, purchase);
        
        PosHeartbeat posHeartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(purchase.getPointOfSale().getId());
        if (posHeartbeat == null) {
            posHeartbeat = new PosHeartbeat();
            posHeartbeat.setPointOfSale(purchase.getPointOfSale());
        }
        posHeartbeat.setLastHeartbeatGmt(nowGmt);
        this.pointOfSaleService.saveOrUpdatePointOfSaleHeartbeat(posHeartbeat);
        
    }
    
    private void saveActivePermit(final Permit permit, final Purchase purchase) {
        
        Long originalPermitId = 0L;
        if (permit.getPermit() != null) {
            originalPermitId = permit.getPermit().getId();
        } else {
            originalPermitId = permit.getId();
        }
        ActivePermit activePermit = (ActivePermit) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByOriginalPermitId", "originalPermitId", originalPermitId);
        if (activePermit == null) {
            // Not Add time
            if (permit.getSpaceNumber() > 0) {
                activePermit = (ActivePermit) this.entityDao
                        .findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByLocationIdAndSpaceNumber",
                                                             new String[] { "locationId", "spaceNumber", },
                                                             new Object[] { permit.getLocation().getId(), permit.getSpaceNumber(), }, false);
                // This part is required for permits that have both a license plate and space number. The license plate should exist only once in a location.
                if (permit.getLicencePlate() != null) {
                    final ActivePermit licencePlateActivePermit = this.activePermitService
                            .findActivePermitByLocationIdAndLicencePlateNumber(permit.getLocation().getId(), permit.getLicencePlate().getNumber());
                    if (licencePlateActivePermit != null) {
                        this.activePermitService.deleteActivePermit(licencePlateActivePermit);
                    }
                    if (activePermit != null && activePermit.getPermitBeginGmt().before(permit.getPermitBeginGmt())) {
                        activePermit.setLicencePlateNumber(permit.getLicencePlate() != null ? permit.getLicencePlate().getNumber()
                                : WebCoreConstants.EMPTY_STRING);
                    }
                }
            } else if (permit.getLicencePlate() != null) {
                // Keep one ActivePermit per Location & LicencePlate pair independent of query by property.
                activePermit = this.activePermitService.findActivePermitByLocationIdAndLicencePlateNumber(permit.getLocation().getId(),
                                                                                                          permit.getLicencePlate().getNumber());
            }
            
            if (activePermit == null) {
                activePermit = new ActivePermit();
                activePermit.setLicencePlateNumber(permit.getLicencePlate() != null ? permit.getLicencePlate().getNumber()
                        : WebCoreConstants.EMPTY_STRING);
                activePermit.setCustomer(purchase.getCustomer());
                activePermit.setPermitBeginGmt(permit.getPermitBeginGmt());
                activePermit.setPermitExpireGmt(permit.getPermitExpireGmt());
                activePermit.setPermitOriginalExpireGmt(permit.getPermitOriginalExpireGmt());
                activePermit.setSpaceNumber(permit.getSpaceNumber());
                activePermit.setLocation(permit.getLocation());
                activePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            }
            if (!activePermit.getPermitBeginGmt().after(permit.getPermitBeginGmt())) {
                activePermit.setAddTimeNumber(permit.getAddTimeNumber());
                activePermit.setNumberOfExtensions((byte) 0);
                activePermit.setPaystationSettingName(purchase.getPaystationSetting().getName());
                activePermit.setPermit(permit);
                activePermit.setPointOfSale(purchase.getPointOfSale());
            }
            
        } else {
            // Add time
            if (purchase.getTransactionType().getId() == ReportingConstants.TRANSACTION_TYPE_ADDTIME) {
                activePermit.setNumberOfExtensions((byte) (activePermit.getNumberOfExtensions() + 1));
            } else if (purchase.getTransactionType().getId() == ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY) {
                activePermit.setPermitExpireGmt(permit.getPermitExpireGmt());
            }
        }
        
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(purchase.getCustomer().getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Default to PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME (1).
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME
            || querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME) {
            if (activePermit.getPermitExpireGmt().before(permit.getPermitExpireGmt())) {
                activePermit.setPermitBeginGmt(permit.getPermitBeginGmt());
                activePermit.setPermitExpireGmt(permit.getPermitExpireGmt());
                activePermit.setPermitOriginalExpireGmt(permit.getPermitOriginalExpireGmt());
                activePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            }
        } else {
            if (!activePermit.getPermitBeginGmt().after(permit.getPermitBeginGmt())) {
                activePermit.setPermitBeginGmt(permit.getPermitBeginGmt());
                activePermit.setPermitExpireGmt(permit.getPermitExpireGmt());
                activePermit.setPermitOriginalExpireGmt(permit.getPermitOriginalExpireGmt());
                activePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            }
        }
        
        this.entityDao.saveOrUpdate(activePermit);
    }
    
    private Purchase updatePaymentCardsMerchantAccount(final Purchase purchase, final MerchantAccount merchantAccount,
        final ProcessorTransactionType processorTransactionType) {
        return updatePaymentCardsMerchantAccount(purchase, merchantAccount, processorTransactionType, false);
    }
    
    private Purchase updatePaymentCardsMerchantAccount(final Purchase purchase, final MerchantAccount merchantAccount,
        final ProcessorTransactionType processorTransactionType, final boolean offlineFlag) {
        if (purchase.getPaymentCards() != null && !purchase.getPaymentCards().isEmpty()) {
            final Set<PaymentCard> set = new HashSet<PaymentCard>(purchase.getPaymentCards().size());
            for (PaymentCard paymentCard : purchase.getPaymentCards()) {
                paymentCard.setMerchantAccount(merchantAccount);
                paymentCard.setProcessorTransactionType(processorTransactionType);
                if (offlineFlag) {
                    paymentCard.setProcessorTransaction(null);
                }
                paymentCard.setPurchase(purchase);
                set.add(paymentCard);
            }
            purchase.setPaymentCards(set);
        }
        return purchase;
    }
    
    private CardType findCardType(final Purchase purchase) {
        final Set<PaymentCard> payCards = purchase.getPaymentCards();
        if (payCards != null && payCards.size() > 0) {
            final Iterator<PaymentCard> iter = payCards.iterator();
            if (iter.hasNext()) {
                return iter.next().getCardType();
            }
        }
        return null;
    }
    
    private void removeObjectReferences(final Purchase purchase, final ProcessorTransaction ptd) {
        final Set<PaymentCard> cards = new HashSet<PaymentCard>(purchase.getPaymentCards().size());
        final Iterator<PaymentCard> iter = purchase.getPaymentCards().iterator();
        while (iter.hasNext()) {
            final PaymentCard card = iter.next();
            card.setProcessorTransaction(null);
            cards.add(card);
        }
        purchase.setPaymentCards(cards);
        ptd.setPurchase(null);
        ptd.setPaymentCards(null);
    }
    
    private void removeObjectReferencesAndTransactionType(final Purchase purchase, final ProcessorTransaction ptd) {
        removeObjectReferences(purchase, ptd);
        ptd.setProcessorTransactionType(this.processorTransactionTypeService
                .findProcessorTransactionType(getMappingPropertiesValue(LabelConstants.PROCESSOR_TRANSACTION_TYPE_N_A_ID)));
    }
    
    /**
     * This happens when Pay Station has moved card data onto Boss Key and only
     * contains minimum information. E.g. <Transaction MessageNumber="Tc7">
     * <PaystationCommAddress>570002400091</PaystationCommAddress>
     * <Number>10</Number> <LotNumber>June 4 2013</LotNumber>
     * <AddTimeNum>914164</AddTimeNum> <Type>Regular</Type>
     * <PurchasedDate>2013:06:11:22:10:23:GMT</PurchasedDate>
     * <ParkingTimePurchased>3600</ParkingTimePurchased>
     * <OriginalAmount>100</OriginalAmount> <ChargedAmount>1</ChargedAmount>
     * <CashPaid>0</CashPaid> <CardPaid>11</CardPaid>
     * <HopperDispensed>0-0:0-0</HopperDispensed>
     * <ExpiryDate>2013:06:11:23:10:23:GMT</ExpiryDate>
     * <RateName>Hourly</RateName> <RateID>27</RateID>
     * <RateValue>100</RateValue> <TaxType>N/A</TaxType> </Transaction>
     * 
     * @param purchase
     * @return boolean Return 'true' if CardPaid is not null but paymentCards &
     *         paymentCashs are null which are set in TransactionHandler,
     *         'populatePaymentInfo' method.
     */
    private boolean isCardPaidNoCardDataAuthNum(final Purchase purchase) {
        if (purchase.getCardPaidAmount() > 0 && purchase.getPaymentCards() != null && purchase.getPaymentCards().isEmpty()
            && (purchase.getPaymentCashs() == null || purchase.getPaymentCashs().isEmpty()) && StringUtils.isBlank(purchase.getCardData())) {
            return true;
        }
        return false;
    }
    
    /**
     * 1. Get billStackerCount, coinBagCount, coinHopper1dispensedCount and
     * coinHopper2dispensedCount from temporary holder PosServiceState first, 2.
     * Retrieve PosServiceState record using pointOfSaleId, 3. Populate with
     * these data in step 1 4. Update the table.
     * 
     * @param pointOfSaleId
     * @param tmpPosServiceState
     *            Temporary data holder created in TransactionHandler.
     * @param paystationSetting
     *            PaystationSetting object comes from Purchase.
     */
    private void savePosServiceStateAndPos(final PointOfSale pointOfSale, final PosServiceState tmpPosServiceState,
        final PaystationSetting paystationSetting) {
        boolean updatePosServiceState = false;
        
        final PosServiceState posServiceStateInDb = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSale.getId());
        final SettingsFile settingsFile = posServiceStateInDb.getSettingsFile();
        this.entityDao.evict(posServiceStateInDb);
        
        /*
         * Update both PointOfSale and PosServiceState with the new SettingsFile
         * information and change PosServiceState.isNewPaystationSetting back to
         * 0 if: - PosServiceState isNewPaystationSetting is true. - incoming
         * xml-rpc uses the latest setting. - paystationSetting.Name equals to
         * posServiceState.NextSettingsFile.Name.
         */
        if (posServiceStateInDb.isIsNewPaystationSetting() && settingsFile != null && settingsFile.getName() != null
            && settingsFile.getName().equalsIgnoreCase(WebCoreUtil.returnEmptyIfBlank(paystationSetting.getName()))) {
            
            pointOfSale.setSettingsFile(posServiceStateInDb.getSettingsFile());
            this.entityDao.update(pointOfSale);
            
            posServiceStateInDb.setPaystationSettingName(posServiceStateInDb.getSettingsFile().getName());
            updatePosServiceState = true;
        }
        
        if (updatePosServiceState) {
            /*
             * Copy the state of the given object (posServiceStateInDb) onto the
             * persistent object (mergedPSS) with the same identifier. If the
             * given instance is unsaved, save a copy of and return it as a
             * newly persistent instance. The given instance does not become
             * associated with the session.
             */
            try {
                this.entityDao.merge(posServiceStateInDb);
                
            } catch (Exception e) {
                // Confirm it's Hibernate or MySQL deadlock exception.
                if (e instanceof StaleObjectStateException && e.getMessage().indexOf("Row was updated or deleted by another transaction") != -1
                    || e instanceof MySQLTransactionRollbackException && e.getMessage().indexOf("Deadlock found when trying to get lock") != -1) {
                    
                    LOG.warn("savePosServiceStateAndPos, StaleObjectStateException or MySQLTransactionRollbackException occurred !", e);
                    performRetry(e, pointOfSale, tmpPosServiceState, paystationSetting);
                }
            }
        }
    }
    
    /**
     * Increase updateRetryCount by 1 and call savePosServiceStateAndPos again
     * to update the record in database. Let any exception to be thrown if error
     * occurred again (updateRetryCount > 0).
     */
    private void performRetry(final Exception e, final PointOfSale pointOfSale, final PosServiceState tmpPosServiceState,
        final PaystationSetting paystationSetting) {
        int maxRetryCount = EmsPropertiesService.DEFAULT_TRANSACTION_MAX_RETRY_COUNT;
        try {
            maxRetryCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.TRANSACTION_MAX_RETRY_COUNT,
                                                                            EmsPropertiesService.DEFAULT_TRANSACTION_MAX_RETRY_COUNT);
        } catch (Exception ex) {
            final StringBuilder bdr = new StringBuilder().append("Unable to retrieve value from database EmsProperties table, column ")
                    .append(EmsPropertiesService.TRANSACTION_MAX_RETRY_COUNT).append(". ").append("Use default value: ")
                    .append(EmsPropertiesService.DEFAULT_TRANSACTION_MAX_RETRY_COUNT);
            LOG.error(bdr.toString(), ex);
        }
        
        if (tmpPosServiceState.getUpdateRetryCount() > maxRetryCount) {
            throw new CardTransactionException(e);
        }
        
        int c = tmpPosServiceState.getUpdateRetryCount();
        c++;
        tmpPosServiceState.setUpdateRetryCount(c);
        savePosServiceStateAndPos(pointOfSale, tmpPosServiceState, paystationSetting);
    }
    
    @Override
    public final void cancelTransaction(final Permit permit, final Purchase purchase, final PaymentCard paymentCard,
        final ProcessorTransaction processorTransaction, final CustomerCard customerCard, final Coupon coupon,
        final PurchaseCollectionCancelled purchaseCollectionCancelled) throws PaystationCommunicationException {
        
        final ActivePermit originalActivePermit = (ActivePermit) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByOriginalPermitId", "originalPermitId", permit.getId());
        if (originalActivePermit != null) {
            originalActivePermit.setPermitExpireGmt(permit.getPermitBeginGmt());
            originalActivePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            this.entityDao.update(originalActivePermit);
        }
        permit.setPermitExpireGmt(permit.getPermitBeginGmt());
        final Permit originalPermit = permit.getPermit();
        if (originalPermit != null) {
            originalPermit.setPermitExpireGmt(permit.getPermitBeginGmt());
            
            final ActivePermit activePermit =
                    (ActivePermit) this.entityDao.findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByOriginalPermitId",
                                                                                      "originalPermitId", originalPermit.getId());
            if (activePermit != null) {
                activePermit.setPermitExpireGmt(permit.getPermitBeginGmt());
                activePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                activePermit.setNumberOfExtensions((byte) (activePermit.getNumberOfExtensions() != 0 ? activePermit.getNumberOfExtensions() - 1 : 0));
                this.entityDao.update(activePermit);
            }
            this.entityDao.update(originalPermit);
            
        }
        this.entityDao.update(permit);
        
        if (coupon != null) {
            this.entityDao.update(coupon);
        }
        
        if (customerCard != null) {
            this.entityDao.update(customerCard);
        }
        
        if (purchaseCollectionCancelled != null) {
            this.entityDao.save(purchaseCollectionCancelled);
        }
        
        if (purchase.getPurchaseTaxes() != null && !purchase.getPurchaseTaxes().isEmpty()) {
            PurchaseTax pTax;
            final Iterator<PurchaseTax> pTaxIter = purchase.getPurchaseTaxes().iterator();
            while (pTaxIter.hasNext()) {
                pTax = pTaxIter.next();
                pTax.setTaxAmount((short) 0);
            }
        }
        this.entityDao.update(purchase);
    }
    
    @Override
    public final Properties getMappingProperties() {
        return this.mappingProperties;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final int getMappingPropertiesValue(final String key) {
        return Integer.parseInt(this.mappingProperties.getProperty(key).trim());
    }
    
    private CardRetryTransaction createCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd) {
        final CardRetryTransaction crt = new CardRetryTransaction();
        crt.setPointOfSale(pointOfSale);
        crt.setPurchasedDate(ptd.getPurchasedDate());
        crt.setTicketNumber(ptd.getTicketNumber());
        crt.setAmount(purchase.getCardPaidAmount());
        crt.setNumRetries(0);
        crt.setCreationDate(DateUtil.getCurrentGmtDate());
        crt.setLastRetryDate(crt.getCreationDate());
        crt.setIsRfid(ptd.isIsRfid());
        if (purchase.isIsOffline()) {
            crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                    .findCardRetryTransactionType(getMappingPropertiesValue(LabelConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED_AUTO_EMS_RETRY_ID)));
        } else {
            crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                    .findCardRetryTransactionType(getMappingPropertiesValue(LabelConstants.CARD_RETRY_TRANSACTION_TYPE_IMPORTED_STORED_FORWARD_ID)));
        }
        return crt;
    }
    
    private CardRetryTransaction createCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final String encryptedCardData) {
        final CardRetryTransaction crt = createCardRetryTransaction(pointOfSale, purchase, ptd);
        crt.setCardData(encryptedCardData);
        crt.setCardExpiry(CardProcessingConstants.MAX_YEAR_MONTH_VALUE);
        crt.setBadCardHash(WebCoreConstants.N_A_STRING);
        crt.setCardHash(WebCoreConstants.N_A_STRING);
        crt.setCardType(WebCoreConstants.N_A_STRING);
        return crt;
    }
    
    private CardRetryTransaction createCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final Track2Card track2Card) throws CryptoException {
        final CardRetryTransaction crt = createCardRetryTransaction(pointOfSale, purchase, ptd);
        crt.setCardData(this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL, track2Card.getCreditCardPanAndExpiry()));
        crt.setCardExpiry(Short.parseShort(track2Card.getExpirationYear() + track2Card.getExpirationMonth()));
        crt.setLast4digitsOfCardNumber(track2Card.getLast4DigitsOfAccountNumber());
        crt.setBadCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getPrimaryAccountNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD));
        crt.setCardHash(this.cryptoAlgorithmFactory.getSha1Hash(track2Card.getCreditCardPanAndExpiry(), CryptoConstants.HASH_CREDIT_CARD_PROCESSING));
        crt.setCardType(track2Card.getDisplayName());
        crt.setTrack2(track2Card.getDecryptedCardData());
        return crt;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final void setCardRetryTransactionTypeService(final CardRetryTransactionTypeService cardRetryTransactionTypeService) {
        this.cardRetryTransactionTypeService = cardRetryTransactionTypeService;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setThirdPartyServiceStallSequenceService(final ThirdPartyServiceStallSequenceService thirdPartyServiceStallSequenceService) {
        this.thirdPartyServiceStallSequenceService = thirdPartyServiceStallSequenceService;
    }
    
    public final void setTransaction2PushService(final Transaction2PushService transaction2PushService) {
        this.transaction2PushService = transaction2PushService;
    }
    
    public final void setThirdPartyServiceAccountService(final ThirdPartyServiceAccountService thirdPartyServiceAccountService) {
        this.thirdPartyServiceAccountService = thirdPartyServiceAccountService;
    }
    
    public final void setRawSensorDataService(final RawSensorDataService rawSensorDataService) {
        this.rawSensorDataService = rawSensorDataService;
    }
    
    public final void setSmsAlertService(final SmsAlertService smsAlertService) {
        this.smsAlertService = smsAlertService;
    }
    
    public final void setExtensiblePermitService(final ExtensiblePermitService extensiblePermitService) {
        this.extensiblePermitService = extensiblePermitService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setSmsMessageTypeService(final SmsMessageTypeService smsMessageTypeService) {
        this.smsMessageTypeService = smsMessageTypeService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    public final void setCreditCardTypeService(final CreditCardTypeService creditCardTypeService) {
        this.creditCardTypeService = creditCardTypeService;
    }
    
    public final void setCPSProcessDataService(final CPSProcessDataService cpsProcessDataService) {
        this.cpsProcessDataService = cpsProcessDataService;
    }
    
    public final void setCoreCPSService(final CoreCPSService coreCPSService) {
        this.coreCPSService = coreCPSService;
    }
    
    public final void setProcessorService(final ProcessorService processorService) {
        this.processorService = processorService;
    }
}
