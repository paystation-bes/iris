package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.FlushMode;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.comparator.CouponComparator;
import com.digitalpaytech.dto.customeradmin.CouponSearchCriteria;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.util.CouponUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ReflectionUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Service("couponService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CouponServiceImpl implements CouponService {
    
    private static Logger log = Logger.getLogger(CouponServiceImpl.class);
    
    private static final String COUPON_QUERY_PART_1 = "SELECT Coupon FROM Coupon WHERE CustomerId = :customerId AND Coupon IN ( ";
    private static final String COUPON_QUERY_PART_2 = " ) AND IsDeleted = 0 AND IsOffline = 0 ORDER BY LENGTH(Coupon) LIMIT 1";
    
    private static final String CTX_KEY_DUPLICATION_IDX = "dupIdx";
    private static final String CTX_KEY_PREVIOUS = "prev";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private MessageHelper messageHelper;
    
    private CouponService self() {
        return this.serviceHelper.bean("couponService", CouponService.class);
    }
    
    @Override
    public final Coupon findCouponByCustomerIdAndCouponCode(final int customerId, final String couponCode) {
        return findByCustomerIdAndCouponCode("Coupon.findCouponByCustomerIdAndCouponCode", customerId, couponCode);
    }
    
    @Override
    public final Coupon findByCustomerIdCouponCodeNoRestriction(final int customerId, final String couponCode) {
        return findByCustomerIdAndCouponCode("Coupon.findByCustomerIdAndCouponCodeNoRestriction", customerId, couponCode);
    }
    
    @Override
    public final Coupon findDeletedByCustomerIdCouponCodeNoRestriction(final int customerId, final String couponCode, final Date minArchiveDate) {
        return (Coupon) this.entityDao.getNamedQuery("Coupon.findDeletedByCustomerIdAndCouponCodeNoRestriction")
                .setParameter("customerId", customerId).setParameter("couponCode", couponCode).setParameter("minArchiveDate", minArchiveDate)
                .setMaxResults(1).uniqueResult();
    }
    
    private Coupon findByCustomerIdAndCouponCode(final String namedQuery, final int customerId, final String couponCode) {
        final String[] params = { "customerId", "couponCode" };
        final Object[] args = { customerId, couponCode };
        
        @SuppressWarnings("unchecked")
        final List<Coupon> couponList = this.entityDao.findByNamedQueryAndNamedParam(namedQuery, params, args);
        if (couponList != null && couponList.size() > 0) {
            return couponList.get(0);
        }
        return null;
    }
    
    /**
     * e.g.
     * select id from coupon
     * where customerid = 1 and id in ('07','007','0007','00007','00007','000000000000000000000007')
     * and IsDeleted = 0 order by length(id) limit 1;
     * 
     * From the forum 'Hibernate doesn't support binding collection to IN (...) in SQL queries.
     * <<< But Hibernate do support this in HQL !!!!!!
     */
    @Override
    public Coupon findCouponWithShortestCode(final int customerId, final String couponCode) {
        String actualCouponCode = couponCode;
        final String ids = WebCoreUtil.prefixZeroesForSql(actualCouponCode, WebCoreConstants.COUPON_LENGTH_MAX);
        final StringBuilder bdr = new StringBuilder();
        bdr.append(COUPON_QUERY_PART_1).append(ids).append(COUPON_QUERY_PART_2);
        
        final SQLQuery sqlquery = this.entityDao.createSQLQuery(bdr.toString());
        sqlquery.setParameter("customerId", customerId);
        @SuppressWarnings("unchecked")
        final List<Long> list = sqlquery.list();
        if (list != null && !list.isEmpty()) {
            actualCouponCode = String.valueOf(list.get(0));
        }
        return findCouponByCustomerIdAndCouponCode(customerId, actualCouponCode);
    }
    
    @Override
    public Coupon findDeletedCouponWithShortestCode(final int customerId, final String couponCode, final Date minArchiveDate) {
        return (Coupon) this.entityDao.getNamedQuery("Coupon.findDeletedShortestCodeByCustomerIdAndCouponCode")
                .setParameter("customerId", customerId).setParameter("couponCode", couponCode).setParameter("minArchiveDate", minArchiveDate)
                .setMaxResults(1).uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Coupon> findCouponsByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Coupon.findCouponsByCustomerId", "customerId", customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateCoupon(final Coupon coupon) {
        this.entityDao.saveOrUpdate(coupon);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCoupon(final Coupon coupon) {
        this.entityDao.saveOrUpdate(coupon);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCoupon(final Coupon coupon) {
        coupon.setIsDeleted(true);
        coupon.setArchiveDate(DateUtil.getCurrentGmtDate());
        this.entityDao.update(coupon);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteCoupons(final Integer customerId, final String statusKey) throws BatchProcessingException {
        int deleted = 0;
        
        final BatchProcessStatus status = (statusKey == null) ? null : this.serviceHelper.getProcessStatus(statusKey);
        try {
            int retried = 0;
            
            final Map<String, Object> context = new HashMap<String, Object>(4);
            context.put(CTX_KEY_DUPLICATION_IDX, 0);
            
            final Date now = new Date();
            int currDeleted = 0;
            do {
                try {
                    currDeleted = self().deleteCouponsPaginated(customerId, now, context);
                    deleted += currDeleted;
                } catch (Exception e) {
                    ++retried;
                    if (retried < 1000) {
                        currDeleted = 1;
                        log.warn("Encountered error while trying to delete Coupons. Will retry rightaway", e);
                    } else {
                        currDeleted = -1;
                        log.error("Gave up because there are too many retries to delete Coupons. Some of the records have been deleted but not all of them. "
                                          + "(CustomerId" + customerId + ")", e);
                        throw new RuntimeException("Too many retries to delete Coupons", e);
                    }
                }
            } while (currDeleted > 0);
            
            if (status == null) {
                // DO NOTHING.
            } else if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail", "label.coupon"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.deleteAllMsg", "label.coupon"));
            }
        } catch (Throwable t) {
            if (status != null) {
                status.fail((String) null, t);
            }
            
            log.error("Failed to process Coupon batch deleteAll", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return deleted;
    }
    
    // These logic are extracted to different method just in case in the future there are more than 500 MB of records to be deleted (which will break cluster-db)
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteCouponsPaginated(final Integer customerId, final Date deleteTimeStamp, final Map<String, Object> context)
        throws BatchProcessingException {
        int deleted = 0;
        
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final ScrollableResults scroll = this.entityDao.getNamedQuery("Coupon.findLowerUpdatesCouponsByCustomerId")
                .setParameter("customerId", customerId).setParameter("lastModifiedGmt", deleteTimeStamp).setFirstResult(0)
                .setMaxResults(WebCoreConstants.BATCH_SIZE).scroll(ScrollMode.FORWARD_ONLY);
        
        int dupCnt = (Integer) context.get(CTX_KEY_DUPLICATION_IDX);
        String prev = (String) context.get(CTX_KEY_PREVIOUS);
        while (scroll.next()) {
            final Coupon coupon = (Coupon) scroll.get(0);
            coupon.setIsDeleted(true);
            if ((prev == null) || (!prev.equals(coupon.getCoupon()))) {
                dupCnt = 0;
                coupon.setArchiveDate(deleteTimeStamp);
            } else {
                ++dupCnt;
                coupon.setArchiveDate(new Date(deleteTimeStamp.getTime() + (dupCnt * 1000)));
            }
            
            prev = coupon.getCoupon();
            
            ++deleted;
        }
        
        context.put(CTX_KEY_DUPLICATION_IDX, dupCnt);
        context.put(CTX_KEY_PREVIOUS, prev);
        
        this.entityDao.getCurrentSession().flush();
        this.entityDao.getCurrentSession().clear();
        
        return deleted;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAndSaveCoupons(final List<Coupon> coupons) throws BatchProcessingException {
        deleteCoupons(coupons.get(0).getCustomer().getId(), (String) null);
        for (Coupon c : coupons) {
            this.entityDao.save(c);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateCoupons(final List<Coupon> coupons) {
        for (Coupon c : coupons) {
            saveOrUpdateCoupon(c);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCoupons(final List<Coupon> coupons) {
        for (Coupon c : coupons) {
            saveCoupon(c);
        }
    }
    
    @Override
    public Coupon processCouponAuthorization(final PointOfSale pointOfSale, final String couponCode, final int stallNumber)
        throws InvalidDataException {
        final Coupon cd = findCouponByCustomerIdAndCouponCode(pointOfSale.getCustomer().getId(), couponCode);
        final int result = validateCoupon(pointOfSale, couponCode, stallNumber, cd);
        
        if (result != WebCoreConstants.COUPON_INVALID && result != WebCoreConstants.COUPON_INACTIVE && result != WebCoreConstants.COUPON_EXPIRED) {
            return cd;
        }
        throw new InvalidDataException(String.valueOf(result));
    }
    
    /**
     * @return int returns INVALID = -1, INACTIVE = -2, or EXPIRED = -3 if it's invalid. Otherwise returns customerId.
     */
    private int validateCoupon(final PointOfSale pos, final String couponNumber, final int stallNumber, final Coupon cd) {
        final String timeZone = this.webWidgetHelperService.getCustomerTimeZoneByCustomerId(pos.getCustomer().getId());
        if (cd == null) {
            log.error("Unable to find coupon: " + couponNumber);
            return WebCoreConstants.COUPON_INVALID;
        }
        
        // Reset 'SingleUseDate' if it's before current day.
        if (cd.getSingleUseDate() != null && CouponUtil.isFromDayBefore(cd.getSingleUseDate(), timeZone)) {
            cd.setSingleUseDate(null);
        }
        
        // Get current time and convert to local PS time
        long currentDateMillis = System.currentTimeMillis();
        
        currentDateMillis += TimeZone.getTimeZone(timeZone).getOffset(currentDateMillis);
        final Date currentDate = new Date(currentDateMillis);
        
        if (currentDate.before(cd.getStartDateLocal())) {
            printInfoLog("Inactive coupon: " + couponNumber);
            return WebCoreConstants.COUPON_INACTIVE;
            
        } else if (currentDate.after(cd.getEndDateLocal())) {
            printInfoLog("Expired coupon: " + couponNumber);
            return WebCoreConstants.COUPON_EXPIRED;
            
        } else if (cd.getNumberOfUsesRemaining() == 0) {
            printInfoLog("No more uses left for coupon: " + couponNumber);
            return WebCoreConstants.COUPON_EXPIRED;
            
        } else if (!isValidCouponLocation(cd, pos)) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Invalid Location id ").append(pos.getLocation().getId()).append(" for coupon: ").append(couponNumber);
            printInfoLog(bdr.toString());
            return WebCoreConstants.COUPON_INVALID;
            
        } else if (stallNumber == 0 && !cd.isIsPndEnabled()) {
            printInfoLog("PND not valid for coupon: " + couponNumber);
            return WebCoreConstants.COUPON_INVALID;
            
        } else if (stallNumber > 0 && !cd.isIsPbsEnabled()) {
            printInfoLog("PBS not valid for coupon: " + couponNumber);
            return WebCoreConstants.COUPON_INVALID;
            
        } else if (stallNumber > 0 && cd.isIsPbsEnabled() && !isValidStall(cd.getSpaceRange(), stallNumber)) {
            printInfoLog("Stall " + stallNumber + " is not valid for coupon: " + couponNumber);
            return WebCoreConstants.COUPON_INVALID;
            
        } else if (cd.getValidForNumOfDay() > 0 && cd.getSingleUseDate() != null) {
            printInfoLog("Single Day Use coupon has applied today, coupon number: " + couponNumber);
            return WebCoreConstants.COUPON_EXPIRED;
        }
        return cd.getCustomer().getId();
    }
    
    private boolean isValidCouponLocation(final Coupon cd, final PointOfSale pos) {
        int couponLocationId = 0;
        if (cd.getLocation() == null || cd.getLocation().getId() == 0 || couponLocationId == pos.getLocation().getId()) {
            return true;
        }
        couponLocationId = cd.getLocation().getId();
        /*
         * Valid for an entire region / location by finding sub-location first then finding the parent-location.
         */
        Location location = this.locationService.findLocationById(pos.getLocation().getId());
        if (location == null) {
            return false;
        }
        
        // Loop through regions / location to root or until we find a match
        while (location != null) {
            if (log.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Comparing coupon region/location: '").append(couponLocationId).append("' with current region/location: '")
                        .append(location.getId());
                log.debug(bdr.toString());
            }
            if (couponLocationId == location.getId()) {
                // We pass
                return true;
            }
            if (location.getLocation() != null) {
                location = this.locationService.findLocationById(location.getLocation().getId());
            } else {
                break;
            }
        }
        
        // We hit the root region -> no match
        return false;
    }
    
    private boolean isValidStall(final String stallRange, final int stallNumber) {
        if (stallRange == null || (stallRange.trim().length() <= 0)) {
            return true;
        }
        
        final String[] stallGroups = stallRange.split(",");
        for (int i = 0; i < stallGroups.length; i++) {
            final String[] stallValues = stallGroups[i].split("-");
            switch (stallValues.length) {
                case 1:
                    final int number = Integer.parseInt(stallValues[0].trim());
                    if (stallNumber == number) {
                        return true;
                    }
                    break;
                
                case 2:
                    final int firstNumber = Integer.parseInt(stallValues[0].trim());
                    final int secondNumber = Integer.parseInt(stallValues[1].trim());
                    
                    if (stallNumber >= firstNumber && stallNumber <= secondNumber) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
        return false;
    }
    
    @Override
    public Coupon findCouponById(final int id) {
        return (Coupon) this.entityDao.get(Coupon.class, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Coupon> findCouponByCriteria(final CouponSearchCriteria criteria) {
        final Criteria query = createSearchQuery(criteria);
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query.list();
    }
    
    @Override
    public int findRecordPage(final Integer couponId, final CouponSearchCriteria criteria) {
        int result = -1;
        
        criteria.setEndCouponId(couponId);
        final Criteria query = createSearchQuery(criteria);
        criteria.setEndCouponId((Integer) null);
        
        query.setProjection(Projections.count("id"));
        
        final double recordNumber = ((Number) query.uniqueResult()).doubleValue();
        if ((recordNumber > 0) && (criteria.getItemsPerPage() != null)) {
            result = (int) Math.ceil(recordNumber / criteria.getItemsPerPage().doubleValue());
        }
        
        return result;
    }
    
    private Criteria createSearchQuery(final CouponSearchCriteria criteria) {
        Coupon endCoupon = null;
        if (criteria.getEndCouponId() != null) {
            endCoupon = this.entityDao.get(Coupon.class, criteria.getEndCouponId());
        }
        
        final Criteria query = this.entityDao.createCriteria(Coupon.class).setCacheable(criteria.getMaxUpdatedTime() == null);
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        }
        
        if ((criteria.getCouponIds() != null) && (criteria.getCouponIds().size() > 0)) {
            query.add(Restrictions.in("id", criteria.getCouponIds()));
        } else {
            Criterion discountConditions = null;
            if (criteria.getDiscountValue() != null) {
                discountConditions = CouponServiceHelper.createDiscountCondition(criteria.getDiscountValue().trim());
            }
            
            Criterion filterConditions = null;
            if (criteria.getFilterValue() == null) {
                if (discountConditions != null) {
                    filterConditions = discountConditions;
                }
            } else {
                query.createAlias("location", "location", CriteriaSpecification.LEFT_JOIN).setFetchMode("location", FetchMode.JOIN);
                
                final List<CouponServiceHelper.SubSearchCriteria> subs = CouponServiceHelper.splitAndCombine(criteria.getFilterValue());
                for (CouponServiceHelper.SubSearchCriteria ssc : subs) {
                    Criterion codeDiscLocCond;
                    if (criteria.isDisableFilterValueAsDiscount()) {
                        codeDiscLocCond = CouponServiceHelper.createCondition((ssc.getCodeOrDiscount1() != null) ? ssc.getCodeOrDiscount1() : ssc
                                                                                      .getCodeOrDiscount2(), discountConditions, ssc.getLocation());
                    } else {
                        codeDiscLocCond = CouponServiceHelper.createCodeOrDiscountCondition(ssc.getCodeOrDiscount1(), ssc.getCodeOrDiscount2(),
                                                                                            ssc.getLocation());
                    }
                    
                    filterConditions = CouponServiceHelper.nullableOr(filterConditions, codeDiscLocCond);
                }
            }
            
            if (filterConditions != null) {
                query.add(filterConditions);
            }
        }
        
        if (criteria.isUsedCoupon() == criteria.isUnusedCoupon()) {
            if (criteria.getIgnoredConsumerId() != null) {
                query.add(Restrictions.not(Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId())));
            }
        } else {
            Criterion sub;
            if (criteria.isUsedCoupon()) {
                sub = Restrictions.isNotNull("consumer");
                if (criteria.getIgnoredConsumerId() != null) {
                    sub = Restrictions.and(sub, Restrictions.not(Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId())));
                }
            } else {
                sub = Restrictions.isNull("consumer");
                if (criteria.getIgnoredConsumerId() != null) {
                    sub = Restrictions.or(sub, Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId()));
                }
            }
            
            query.add(sub);
        }
        
        if (criteria.getValidForDate() != null) {
            query.add(Restrictions.or(Restrictions.isNull("numberOfUsesRemaining"), Restrictions.gt("numberOfUsesRemaining", (short) 0)));
            
            query.add(Restrictions.ge("endDateLocal", criteria.getValidForDate()));
        }
        
        if (criteria.getLastValidDate() != null) {
            query.add(Restrictions.lt("endDateLocal", criteria.getLastValidDate()));
        }
        
        query.add(Restrictions.eq("isDeleted", false));
        query.add(Restrictions.isNull("archiveDate"));
        
        if (criteria.isHideOfflineCoupon()) {
            query.add(Restrictions.eq("isOffline", false));
        }
        
        if ((criteria.getIgnoredCouponIds() != null) && (criteria.getIgnoredCouponIds().size() > 0)) {
            query.add(Restrictions.not(Restrictions.in("id", criteria.getIgnoredCouponIds())));
        }
        
        Criterion endCouponRes = null;
        Criterion prevEndCouponEq = null;
        if ((criteria.getOrderColumn() != null) && (criteria.getOrderColumn().length > 0)) {
            
            for (int i = 0; i < criteria.getOrderColumn().length; i++) {
                
                final String orderCol = criteria.getOrderColumn()[i];
                if (criteria.isOrderDesc()) {
                    query.addOrder(Property.forName(orderCol).desc());
                } else {
                    query.addOrder(Property.forName(orderCol).asc());
                }
                
                if (endCoupon != null) {
                    final Object endCouponVal = ReflectionUtil.getAttribute(endCoupon, orderCol);
                    
                    endCouponRes = this.entityDao.nullableOr(endCouponRes, this.entityDao.nullableAnd(prevEndCouponEq, (criteria.isOrderDesc())
                            ? Restrictions.gt(orderCol, endCouponVal) : Restrictions.lt(orderCol, endCouponVal)));
                    prevEndCouponEq = this.entityDao.nullableAnd(prevEndCouponEq, Restrictions.eq(orderCol, endCouponVal));
                }
            }
        }
        
        query.addOrder(Property.forName("id").asc());
        if (endCoupon != null) {
            endCouponRes = this.entityDao.nullableOr(endCouponRes,
                                                     this.entityDao.nullableAnd(prevEndCouponEq, Restrictions.le("id", endCoupon.getId())));
            query.add(endCouponRes);
        }
        
        if (criteria.getMaxUpdatedTime() != null) {
            query.add(Restrictions.le("lastModifiedGmt", criteria.getMaxUpdatedTime()));
        }
        
        return query;
    }
    
    @Override
    public int findTotalCouponSize(final int customerId) {
        @SuppressWarnings("unchecked")
        final List<Long> list = this.entityDao.findByNamedQueryAndNamedParam("Coupon.findTotalCouponSize", "id", customerId);
        if (list == null) {
            return 0;
        } else {
            final Long lo = list.get(0);
            return lo.intValue();
        }
    }
    
    @Override
    @Async
    public Future<Integer> processBatchUpdateAsync(final String statusKey, final int customerId, final Collection<Coupon> coupons, final String mode,
        final int userId) throws BatchProcessingException {
        return new AsyncResult<Integer>(self().processBatchUpdateWithStatus(statusKey, customerId, coupons, mode, userId));
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public int processBatchUpdateWithStatus(final String statusKey, final int customerId, final Collection<Coupon> coupons, final String mode,
        final int userId) throws BatchProcessingException {
        final BatchProcessStatus status = this.serviceHelper.getProcessStatus(statusKey);
        try {
            self().processBatchUpdate(customerId, coupons, mode, userId, status);
            if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail", "label.coupon"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.bulkupdate.successMsg", "label.coupon"));
            }
        } catch (Throwable t) {
            status.fail((String) null, t);
            log.error("Failed to process Coupon batch update", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return 0;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int processBatchUpdate(final int customerId, final Collection<Coupon> coupons, final String mode, final int userId,
        final BatchProcessStatus status) throws BatchProcessingException {
        int updated = 0;
        
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        if ("replace".equalsIgnoreCase(mode)) {
            this.deleteCoupons(customerId, (String) null);
            this.entityDao.flush();
        }
        
        Coupon couponStart = null;
        if (coupons.size() > 0) {
            couponStart = coupons.iterator().next();
        }
        
        if (couponStart != null) {
            final ScrollableResults scroll = this.entityDao.getNamedQuery("Coupon.findCouponsByCustomerIdOrderByCode")
                    .setParameter("customerId", customerId).setParameter("couponStart", couponStart.getCoupon())
                    .setParameter("couponStartLength", couponStart.getCoupon().length()).scroll(ScrollMode.FORWARD_ONLY);
            
            final Date updateTime = new Date();
            final CouponComparator cmprtr = new CouponComparator();
            Coupon current = null;
            int compareResult = -1;
            for (Coupon coupon : coupons) {
                compareResult = (current == null) ? -1 : cmprtr.compare(current, coupon);
                while (compareResult < 0) {
                    current = (!scroll.next()) ? null : (Coupon) scroll.get(0);
                    compareResult = (current == null) ? 1 : cmprtr.compare(current, coupon);
                }
                
                final Coupon existingCoupon = (compareResult != 0) ? null : current;
                if (existingCoupon != null) {
                    modifyCoupon(existingCoupon, coupon, userId, updateTime);
                } else {
                    if (coupon.getId() != null) {
                        throw new IllegalStateException("Detected mismatches coupon ID: " + coupon.getId());
                    }
                    
                    coupon.setLastModifiedByUserId(userId);
                    coupon.setLastModifiedGmt(updateTime);
                    
                    this.entityDao.save(coupon);
                }
                
                ++updated;
                
                if ((updated % WebCoreConstants.BATCH_SIZE) == 0) {
                    this.entityDao.getCurrentSession().flush();
                    this.entityDao.getCurrentSession().clear();
                }
            }
            
            this.entityDao.getCurrentSession().flush();
            this.entityDao.getCurrentSession().clear();
        }
        
        return updated;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Coupon modifyCoupon(final Coupon coupon, final Integer userId) {
        Coupon result = null;
        if (coupon.getId() == null) {
            coupon.setLastModifiedByUserId(userId);
            coupon.setLastModifiedGmt(new Date());
            
            this.entityDao.save(coupon);
            result = coupon;
        } else {
            final Coupon existingCoupon = findCouponById(coupon.getId());
            if (existingCoupon == null) {
                throw new IllegalStateException("Could not locate Coupon: " + coupon.getId());
            } else if (existingCoupon.isIsOffline()) {
                throw new IllegalStateException("Could not modify offline coupon: " + coupon.getId());
            }
            
            result = modifyCoupon(existingCoupon, coupon, userId, new Date());
        }
        
        return result;
    }
    
    private Coupon modifyCoupon(final Coupon existingCoupon, final Coupon coupon, final Integer userId, final Date updateTime) {
        Coupon savingCoupon = existingCoupon;
        final boolean shouldArchive = CouponUtil.isDiscountValueChanged(savingCoupon, coupon.getPercentDiscount(), coupon.getDollarDiscountAmount());
        if (shouldArchive) {
            final String couponCode = savingCoupon.getCoupon();
            final Consumer assignedConsumer = savingCoupon.getConsumer();
            
            deleteCoupon(savingCoupon);
            
            savingCoupon = new Coupon();
            savingCoupon.setCoupon(couponCode);
            savingCoupon.setConsumer(assignedConsumer);
        }
        
        savingCoupon.setCustomer(coupon.getCustomer());
        savingCoupon.setDescription(coupon.getDescription());
        savingCoupon.setDollarDiscountAmount(coupon.getDollarDiscountAmount());
        savingCoupon.setPercentDiscount(coupon.getPercentDiscount());
        savingCoupon.setStartDateLocal(coupon.getStartDateLocal());
        savingCoupon.setEndDateLocal(coupon.getEndDateLocal());
        savingCoupon.setIsPbsEnabled(coupon.isIsPbsEnabled());
        savingCoupon.setIsPndEnabled(coupon.isIsPndEnabled());
        savingCoupon.setLocation(coupon.getLocation());
        savingCoupon.setMaxNumberOfUses(coupon.getMaxNumberOfUses());
        savingCoupon.setNumberOfUsesRemaining(coupon.getNumberOfUsesRemaining());
        savingCoupon.setSpaceRange(coupon.getSpaceRange());
        savingCoupon.setValidForNumOfDay(coupon.getValidForNumOfDay());
        savingCoupon.setLocation(coupon.getLocation());
        savingCoupon.setLastModifiedByUserId(userId);
        savingCoupon.setLastModifiedGmt(updateTime);
        
        if (shouldArchive) {
            this.entityDao.save(savingCoupon);
        } else {
            this.entityDao.update(savingCoupon);
        }
        
        return savingCoupon;
    }
    
    private void printInfoLog(final String msg) {
        if (log.isInfoEnabled()) {
            log.info(msg);
        }
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    public void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Coupon> findCouponByConsumerId(final int consumerId) {
        return this.entityDao.getNamedQuery("Coupon.findByConsumerId").setParameter("consumerId", consumerId).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Coupon> findByCustomerIdAndCouponIds(final Integer customerId, final Collection<Integer> couponIds) {
        return this.entityDao.getNamedQuery("Coupon.findByCustomerIdAndCouponIds").setParameter("customerId", customerId)
                .setParameterList("couponIds", couponIds).list();
    }
    
    @Override
    public final int findCouponCountByCustomerId(final Integer customerId) {
        final List<Object> response = this.entityDao.findByNamedQueryAndNamedParam("Coupon.findAllCouponsByCustomerId", "customerId", customerId);
        
        if (response == null || response.isEmpty()) {
            return 0;
        }
        final int result = ((Long) response.get(0)).intValue();
        return result;
    }
}
