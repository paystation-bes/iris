package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetCollectionsService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetCollectionsServiceImpl implements WebWidgetService {
    public static final HashMap<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(12);
    static {
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, "rp.RouteId, tc.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PAYSTATION, "tc.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_COLLECTION_TYPE, "tc.CollectionTypeId ");
    }
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount) {
        
        StringBuilder queryStr = new StringBuilder();
        
        String queryLimit = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                  EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        
        // needed to set the widgetRunDate
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), new StringBuilder(), widget.getWidgetRangeType().getId(), widget
                .getWidgetTierTypeByWidgetTier1Type().getId(), false, true);
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        queryStr.append(", IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        /* "wData" SELECT expression */
        queryStr.append("SELECT tc.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, false);
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        if (tier1Id == WidgetConstants.TIER_TYPE_COLLECTION_TYPE || tier2Id == WidgetConstants.TIER_TYPE_COLLECTION_TYPE) {
            queryStr.append(", SUM( ");
            queryStr.append("CASE WHEN tc.CollectionTypeId = 0 THEN tc.TotalAmount ");
            queryStr.append("WHEN tc.CollectionTypeId = 1 THEN tc.BillTotalAmount ");
            queryStr.append("WHEN tc.CollectionTypeId = 2 THEN tc.CoinTotalAmount ");
            queryStr.append("WHEN tc.CollectionTypeId = 3 THEN tc.CardTotalAmount ");
            queryStr.append("ELSE 0 ");
            queryStr.append("END ");
            queryStr.append(") AS WidgetMetricValue ");
        } else {
            queryStr.append(", IFNULL(SUM(tc.TotalAmount), 0) AS WidgetMetricValue ");
        }
        
        queryStr.append("FROM TotalCollection tc ");
        queryStr.append("INNER JOIN Time t ON(tc.EndTimeIdLocal = t.Id ");

        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(), widget
                .getWidgetTierTypeByWidgetTier1Type().getId(), false, false);
        
        queryStr.append(") ");
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "tc.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY tc.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, false, null, false);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(webWidgetHelperService, widget, query, userAccount, false);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        @SuppressWarnings("unchecked")
        List<WidgetMetricInfo> rawWidgetData = query.list();
        
        widgetData = webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        widgetData.setTrendValue(getWidgetTrendValues(widget, widgetData, userAccount));
        
        return widgetData;
    }
    
    protected List<String> getWidgetTrendValues(Widget widget, WidgetData widgetData, UserAccount userAccount) {
        
        List<String> trendValue = new ArrayList<String>();
        
        if (!(widget.getTrendAmount() == null || widget.getTrendAmount() == 0)) {
            Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
            trendValue.add(dollarValue.toString());
            return trendValue;
        }
        
        return null;
    }
}
