package com.digitalpaytech.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("serviceAgreementService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ServiceAgreementServiceImpl implements ServiceAgreementService {
	private static final Logger logger = Logger.getLogger(ServiceAgreementServiceImpl.class);
	
	@Autowired
	private OpenSessionDao openSessionDao;
	
	@Autowired
	private EntityDao entityDao;
	
	public void setOpenSessionDao(OpenSessionDao openSessionDao) {
		this.openSessionDao = openSessionDao;
	}

	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}

	@Transactional(propagation=Propagation.SUPPORTS)
	public ServiceAgreement findLatestServiceAgreement()
	{
		List<ServiceAgreement> list = openSessionDao.findByNamedQuery("ServiceAgreement.findLatestAgreement");
		if (list.isEmpty())
		{
			return null;
		}

		return list.get(0);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Serializable saveServiceAgreement(ServiceAgreement serviceAgreement) {
		return openSessionDao.save(serviceAgreement);
	}
	
	@PostConstruct
	private void uploadNewServiceAgreement() {
		logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		logger.info("++++++++++++++++ loading new service agreement  ++++++++++++++++++");
		logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		try {
			String dirName = WebSecurityConstants.DEFAULT_SERVICE_AGREEMENT_LOCATION;
			java.io.File dir = new java.io.File(dirName);
			if (!dir.exists()) {
				dir.mkdir();
				logger.info("+++ no new service agreement available ++++");
			} else {
				File[] files = dir.listFiles();
				String filePath;
				if (files.length > 0) {
					ServiceAgreement oldAgreement = findLatestServiceAgreement();
					filePath = files[0].getAbsolutePath();
					logger.debug("++++ service agreement file name: "
							+ filePath);
					ServiceAgreement agreement = new ServiceAgreement();
					File file = new File(filePath);
					byte[] bytes = getBytesFromFile(file);
					if (bytes == null) {
						logger.error("!!! read service agreement file error !!!");
						return;
					}
					agreement.setContent(bytes);
					Date currentTime = new Date();
					agreement.setUploadedGmt(currentTime);
					agreement.setLastModifiedGmt(currentTime);
					// Tomcat modified this service agreement, therefore set to 1, which means DPT admin for now.
					UserAccount ua = new UserAccount();
					ua.setId(1);
					agreement.setUserAccount(ua);
										
					if (oldAgreement == null) {
						agreement.setContentVersion(1);
					} else {
						agreement.setContentVersion(oldAgreement
								.getContentVersion() + 1);
					}

					saveServiceAgreement(agreement);
					deleteFile(filePath);
					logger.info("+++ new service agreement has been updated ++++");
				} else {
					logger.info("+++ no new service agreement available ++++");
				}
			}
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException: ", e);
		} catch (IOException e) {
			logger.error("IOException: ", e);
		}
	}
	
	private byte[] getBytesFromFile(File file) throws FileNotFoundException, IOException {
		FileInputStream ins = new FileInputStream(file);
		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			logger.error("!!! service agreement file too large !!!");
			return null;
		}
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = ins.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		if (offset < bytes.length) {
			logger.error("!!! Could not completely read file " + file.getName() + " !!!");
			return null;
		}
		ins.close();

		return bytes;
	}

	private void deleteFile(String serviceAgreementFile) {
		File file = new java.io.File(serviceAgreementFile);
		if (file.exists() && file.isFile()) {
			file.delete();
			logger.info("+++ file " + serviceAgreementFile + " is deleted +++");
		} else {
			logger.warn("!!! file " + serviceAgreementFile + " is directory or does not exist !!!");
		}
	}

	@Override
	public ServiceAgreement findLatestServiceAgreementByCustomerId(int customerId) {
		return (ServiceAgreement) entityDao.findUniqueByNamedQueryAndNamedParam(
						"ServiceAgreement.findLatestAgreementByCustomerId", "customerId", customerId, true);
	}
}
