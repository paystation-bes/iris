package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.digitalpaytech.service.CreditCardTestNumberService;

@Service("CreditCardTestNumberService")
public class CreditCardTestNumberServiceImpl implements CreditCardTestNumberService {
    
    @Value("${default.test.credit.card.number:5405010100000016}")
    private String defaultTestCreditCardNumber;
    
    @Value("${default.test.first.data.nashville.credit.card.number:4012000033330026}")
    private String defaultTestFirstDataNashvilleCreditcardNumber;
    
    @Value("${default.test.link2gov.credit.card.number:5405222222222226}")
    private String defaultTestLink2GovCreditCardNumber;
    
    @Value("${default.test.paradata.credit.card.number:4217651111111119}")
    private String defaultTestParadataCreditCardNumber;
    
    @Value("${default.test.moneris.mc.credit.card.number:5454545454545454}")
    private String defaultTestMonerisCreditCardNumber;
    
    @Value("${default.test.elavon.via.conex.credit.card.number.declined:4124939999999990}")
    private String defaultTestElavonViaconexCreditCardNumberDeclined;
    
    @Value("${default.test.elavon.credit.card.number.decline:4124939999999990}")
    private String defaultTestElavonCreditCardNumberDeclined;
    
    @Value("${default.test.td.merchant.card.number.declined:4003050500040005}")
    private String defaultTestTDMerchantCardNumberDeclined;
    
    @Value("${default.test.td.merchant.card.number.approved}:4030000010001234,2223000010262100}")
    private List<String> defaultTestTDMerchantCardNumberApproved;
    
    @Value("${default.test.td.merchant.card.number.ems.unavailable:371100001000131}")
    private String defaultTestTDMerchantCardNumberemsunavailable;
    
    @Value("${default.test.heartland.card.number.approved:4012002000060016}")
    private String defaultTestHeartlandCardNumberApproved;
    
    @Value("${default.test.heartland.expiry.yymm.visa:2512}")
    private String defaultTestHeartlandExpiryYYMM;
    
    @Override
    public String getDefaultTestCreditCardNumber() {
        return this.defaultTestCreditCardNumber;
    }
    
    @Override
    public String getDefaultTestFirstDataNashvilleCreditcardNumber() {
        return this.defaultTestFirstDataNashvilleCreditcardNumber;
    }
    
    @Override
    public String getDefaultTestLink2GovCreditCardNumber() {
        return this.defaultTestLink2GovCreditCardNumber;
    }
    
    @Override
    public String getDefaultTestParadataCreditCardNumber() {
        return this.defaultTestParadataCreditCardNumber;
    }
    
    @Override
    public String getDefaultTestMonerisCreditCardNumber() {
        return this.defaultTestMonerisCreditCardNumber;
    }
    
    @Override
    public String getDefaultTestElavonViaconexCreditCardNumberDeclined() {
        return this.defaultTestElavonViaconexCreditCardNumberDeclined;
    }
    
    @Override
    public String getDefaultTestElavonCreditCardNumberDeclined() {
        return this.defaultTestElavonCreditCardNumberDeclined;
    }
    
    @Override
    public String getDefaultTestTDMerchantCardNumberDeclined() {
        return this.defaultTestTDMerchantCardNumberDeclined;
    }
    
    @Override
    public List<String> getDefaultTestTDMerchantCardNumberApproved() {
        return this.defaultTestTDMerchantCardNumberApproved;
    }
    
    @Override
    public String getDefaultTestTDMerchantCardNumberemsunavailable() {
        return this.defaultTestTDMerchantCardNumberemsunavailable;
    }
    
    @Override
    public String getDefaultTestHeartlandCardNumberApproved() {
        return this.defaultTestHeartlandCardNumberApproved;
    }
    
    @Override
    public String getDefaultTestHeartlandExpiryYYMM() {
        return this.defaultTestHeartlandExpiryYYMM;
    }
}
