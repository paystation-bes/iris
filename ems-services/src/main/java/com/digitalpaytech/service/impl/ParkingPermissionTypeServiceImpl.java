package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ParkingPermissionType;
import com.digitalpaytech.service.ParkingPermissionTypeService;
import com.digitalpaytech.util.WebCoreUtil;

@Service("parkingPermissionTypeService")
@Transactional(propagation=Propagation.SUPPORTS)

public class ParkingPermissionTypeServiceImpl implements ParkingPermissionTypeService {

    @Autowired
    private EntityDao entityDao;
    
    private Map<String, ParkingPermissionType> map;

    
    @Override
    public Map<String, ParkingPermissionType> loadAll() {
        if (map == null || map.isEmpty()) {
            List<ParkingPermissionType> list = entityDao.loadAll(ParkingPermissionType.class);
            map = new HashMap<String, ParkingPermissionType>(list.size());
            Iterator<ParkingPermissionType> iter = list.iterator();
            while (iter.hasNext()) {
                ParkingPermissionType type = iter.next();
                map.put(WebCoreUtil.combineWordsAndChangeCase(" ", WebCoreUtil.LOWER_UPPER_CASES.LOWERCASE, type.getName()), type);
            }
        }
        return map;
    }
    
    
    @Override
    public Map<String, ParkingPermissionType> reloadAll() {
        if (map != null && !map.isEmpty()) {
            String name;
            Iterator<String> iter = map.keySet().iterator();
            while (iter.hasNext()) {
                name = iter.next();
                map.remove(name);
            }
        }
        return loadAll();
    }

    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }    
}
