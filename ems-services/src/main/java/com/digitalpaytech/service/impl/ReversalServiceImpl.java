package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.domain.ReversalArchive;
import com.digitalpaytech.service.ReversalService;
import com.digitalpaytech.util.CardProcessingConstants;

@Service("reversalService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReversalServiceImpl implements ReversalService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final int findTotalReversalsByCardData(final String cardNumber) {
        return this.entityDao.findTotalResultByNamedQuery("Reversal.findTotalReversalsByCardData", new String[] { CARD_NUMBER },
                                                          new Object[] { cardNumber });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<Reversal> findReversalsByCardData(final String cardNumber, final boolean isEvict) {
        final List<Reversal> lists = this.entityDao.findByNamedQueryAndNamedParam("Reversal.findReversalsByCardData", CARD_NUMBER, cardNumber);
        if (isEvict) {
            for (Reversal reversal : lists) {
                this.entityDao.evict(reversal);
            }
        }
        return lists;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateReversal(final Reversal reversal) {
        this.entityDao.update(reversal);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<Reversal> getIncompleteReversalsForReversal(final Date processedBefore) {
        return createIncompleteReversals(processedBefore, null).setCacheable(false).list();
    }
    
    @Override 
    @SuppressWarnings("unchecked")
    public final Collection<Reversal> getIncompleteReversalsForReversal(final MerchantAccount merchantAccount) {
        return createIncompleteReversals(null, merchantAccount).setCacheable(false).list();
    }
    
    private Criteria createIncompleteReversals(final Date processedBefore, final MerchantAccount merchantAccount) {
        /*
         * Moved from CardProcessingManagerImpl.java
         */
        Criteria crit = this.entityDao.createCriteria(Reversal.class);
        crit = crit.createAlias(MERCHANT_ACCOUNT, "revMerchantAccount");
        crit = crit.createAlias("revMerchantAccount.customer", CardProcessingConstants.CUSTOMER, CriteriaSpecification.LEFT_JOIN)
                .setFetchMode(CardProcessingConstants.CUSTOMER, FetchMode.JOIN);
        crit.add(Restrictions.eq("isSucceeded", Boolean.FALSE));
        crit.add(Restrictions.eq("isExpired", Boolean.FALSE));
        if (merchantAccount == null) {
            crit.add(Restrictions.le("lastRetryTime", processedBefore));
        } else {
            crit.add(Restrictions.eq(MERCHANT_ACCOUNT, merchantAccount));
        }
        crit.add(Restrictions.eq(CUST_IS_MIG, Boolean.TRUE));
        return crit;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    public final void archiveReversalData() {
        final List<Reversal> reversals = this.entityDao.findByNamedQuery("Reversal.findReversalsForArchiveForMigratedCustomers");
        for (Reversal reversal : reversals) {
            this.entityDao.save(this.convertReversalToArchive(reversal));
            this.entityDao.delete(reversal);
        }
    }
    
    private ReversalArchive convertReversalToArchive(final Reversal reversal) {
        final ReversalArchive reversalArchive = new ReversalArchive();
        reversalArchive.setMerchantAccount((MerchantAccount) this.entityDao.get(MerchantAccount.class, reversal.getMerchantAccount().getId()));
        if (reversal.getCardData() != null) {
            reversalArchive.setCardNumber(reversal.getCardData());
        }
        if (reversal.getExpiryDate() != null) {
            reversalArchive.setExpiryDate(reversal.getExpiryDate());
        }
        reversalArchive.setOriginalMessageType(reversal.getOriginalMessageType());
        reversalArchive.setOriginalProcessingCode(reversal.getOriginalProcessingCode());
        reversalArchive.setOriginalReferenceNumber(reversal.getOriginalReferenceNumber());
        reversalArchive.setOriginalTime(reversal.getOriginalTime());
        reversalArchive.setOriginalTransactionAmount(reversal.getOriginalTransactionAmount());
        reversalArchive.setLastResponseCode(reversal.getLastResponseCode());
        reversalArchive.setPurchasedDate(reversal.getPurchasedDate());
        reversalArchive.setTicketNumber(reversal.getTicketNumber());
        final PointOfSale p = (PointOfSale) this.entityDao.get(PointOfSale.class, reversal.getPointOfSaleId());
        if (p != null) {
            reversalArchive.setPointOfSale(p);
        }
        reversalArchive.setRetryAttempts(reversal.getRetryAttempts());
        reversalArchive.setLastRetryTime(reversal.getLastRetryTime());
        reversalArchive.setSucceeded(reversal.isIsSucceeded());
        reversalArchive.setExpired(reversal.isIsExpired());
        
        return reversalArchive;
    }
    
    @Override
    public final void saveReversal(final Reversal reversal) {
        this.entityDao.save(reversal);
    }
    
}
