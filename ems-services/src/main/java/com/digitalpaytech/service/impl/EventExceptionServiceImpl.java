package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EventException;
import com.digitalpaytech.service.EventExceptionService;
import com.digitalpaytech.util.StandardConstants;

@Service("eventExceptionService")
@Transactional(propagation = Propagation.SUPPORTS)
public class EventExceptionServiceImpl implements EventExceptionService {
    
    private Map<String, EventException> eventExceptionMap;
    
    @Autowired
    private EntityDao entityDao;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final Map<String, EventException> getEventExceptionMap() {
        return this.eventExceptionMap;
    }
    
    public final void setEventExceptionMap(final Map<String, EventException> eventExceptionMap) {
        this.eventExceptionMap = eventExceptionMap;
    }
    
    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public Map<String, EventException> loadAll() {
        if (this.eventExceptionMap == null) {
            final List<EventException> eventExceptionList = this.entityDao.loadAll(EventException.class);
            if (eventExceptionList != null) {
                this.eventExceptionMap = new HashMap<String, EventException>();
                for (EventException eventException : eventExceptionList) {
                    this.eventExceptionMap.put(new StringBuilder(String.valueOf(eventException.getPaystationType().getId()))
                                                       .append(StandardConstants.STRING_UNDERSCORE)
                                                       .append(eventException.getEventDeviceType().getId()).toString(), eventException);
                }
            }
        }
        return this.eventExceptionMap;
    }
    
    @Override
    public boolean isException(final Integer paystationTypeId, final Integer eventDeviceTypeId) {
        if (loadAll() != null) {
            return loadAll().containsKey(new StringBuilder(String.valueOf(paystationTypeId)).append(StandardConstants.STRING_UNDERSCORE)
                                                 .append(eventDeviceTypeId).toString());
        }
        return false;
    }
}
