package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.RateRateProfileService;

@Service("rateRateProfileService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateRateProfileServiceImpl implements RateRateProfileService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private LocationService locationService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<RateRateProfile> findPublishedRatesByRateId(final Integer rateId) {
        return this.entityDao.findByNamedQueryAndNamedParam("RateRateProfile.findPublishedRatesByRateId", "rateId", rateId);
    }
    
    @Override
    public final List<RateRateProfile> findActiveRatesByRateProfileIdAndDateRange(final Integer rateProfileId, final Date startDate, final Date endDate,
                                                                                  final boolean isSingleDay) {
        
        final String[] params = new String[] { "rateProfileId", "startDate", "endDate" };
        final Object[] values = new Object[] { rateProfileId, startDate, endDate };
        
        @SuppressWarnings("unchecked")
        final List<RateRateProfile> rateRateProfileList = this.entityDao
                .findByNamedQueryAndNamedParam("RateRateProfile.findActiveRatesByRateProfileIdAndDateRange", params, values, true);
        
        if (rateRateProfileList != null && !rateRateProfileList.isEmpty()) {
            
            if (isSingleDay) {
                final Calendar date = Calendar.getInstance();
                date.setTime(startDate);
                final Integer dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
                final List<RateRateProfile> ratesToRemove = new ArrayList<RateRateProfile>();
                for (RateRateProfile rateRateProfile : rateRateProfileList) {
                    if (!hasDay(dayOfWeek, rateRateProfile)) {
                        ratesToRemove.add(rateRateProfile);
                    }
                }
                rateRateProfileList.removeAll(ratesToRemove);
            }
        }
        return rateRateProfileList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RateRateProfile> findPublishedRatesByLocationIdAndDateRange(final Integer locationId, final Integer customerId, final Date startDate,
                                                                            final Date endDate) {
        final List<Integer> locationIds = this.locationService.findAllParentIds(locationId);
        final List<Integer> rateProfileIds = this.entityDao.getNamedQuery("RateProfileLocation.findRateProfileIdsPublishedToLocation")
                .setParameterList("locationIds", locationIds).setParameter("customerId", customerId).list();
        return this.entityDao.getNamedQuery("RateRateProfile.findPublishedRatesByRateProfileIdsAndDateRange")
                .setParameterList("rateProfileIds", rateProfileIds).setParameter("startDate", startDate).setParameter("endDate", endDate).list();
    }
    
    @Override
    public final RateRateProfile findRateRateProfileById(Integer rateRateProfileId) {
        return (RateRateProfile) this.entityDao.getNamedQuery("RateRateProfile.findById").setInteger("id", rateRateProfileId).uniqueResult();
    }
    
    private boolean hasDay(final Integer dayOfWeek, final RateRateProfile rateRateProfile) {
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                return rateRateProfile.getIsMonday();
            case Calendar.TUESDAY:
                return rateRateProfile.getIsTuesday();
            case Calendar.WEDNESDAY:
                return rateRateProfile.getIsWednesday();
            case Calendar.THURSDAY:
                return rateRateProfile.getIsThursday();
            case Calendar.FRIDAY:
                return rateRateProfile.getIsFriday();
            case Calendar.SATURDAY:
                return rateRateProfile.getIsSaturday();
            case Calendar.SUNDAY:
                return rateRateProfile.getIsSunday();
            default:
                return false;
        }
    }
}
