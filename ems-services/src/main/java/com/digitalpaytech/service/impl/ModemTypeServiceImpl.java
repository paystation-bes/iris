package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ModemType;
import com.digitalpaytech.service.ModemTypeService;

@Service("modemTypeService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ModemTypeServiceImpl implements ModemTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public List<ModemType> loadAll() {
        return entityDao.loadAll(ModemType.class);
    }
    
    @Override
    public ModemType findModemType(String name) {
        return (ModemType) this.entityDao.findUniqueByNamedQueryAndNamedParam("ModemType.findByName", "name", name);
    }
    
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(ModemType modemType) {
        this.entityDao.save(modemType);
    }
}
