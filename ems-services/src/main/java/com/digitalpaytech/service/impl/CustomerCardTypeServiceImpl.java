package com.digitalpaytech.service.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.AuthorizationType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.FilterDTOTransformer;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.Sorts;
import com.digitalpaytech.util.WebCoreConstants;

@Service("customerCardTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerCardTypeServiceImpl implements CustomerCardTypeService {
    
    private static final String SINGLE_QUOTE_STRING = "'";
    private static final String CUSTOMER_CARD_TYPE_IDS = "customerCardTypeIds";
    private static final String NAME = "name";
    private static final String CUSTOMER_CARD_TYPE_FIND_BY_CUSTOMER_ID_AND_NAME = "CustomerCardType.findByCustomerIdAndName";
    private static final String CUSTOMER_CARD_TYPE_ID = "customerCardTypeId";
    private static final String CUSTOMER_CARD_TYPE_COUNT_SECONDARY_BY_CUSTOMER_CARD_TYPE = "CustomerCardType.countSecondaryByCustomerCardType";
    private static final String CUSTOMER_ID = "customerId";
    private static final String CARD_TYPE_ID = "cardTypeId";
    private static final String IS_LOCKED = "isLocked";
    private static final String CUSTOMER_CARD_TYPE_FIND_PRIMARY_BY_CUSTOMER_ID_AND_NAME = "CustomerCardType.findPrimaryByCustomerIdAndName";
    private static final String NEW_CUSTOMER_CARD_TYPE_ID = "newCustomerCardTypeId";
    private static final String OLD_CUSTOMER_CARD_TYPE_IDS = "oldCustomerCardTypeIds";
    private static final String CUSTOMER_CARD_CHANGE_CUSTOMER_CARD_TYPE = "CustomerCard.changeCustomerCardType";
    private static final String CUSTOMER_CARD_TYPE_NAMES = "customerCardTypeNames";
    private static final String IGNORE_DELETED_FLAG = "ignoreDeletedFlag";
    private static Logger log = Logger.getLogger(CustomerCardTypeServiceImpl.class);
    private static Set<String> nonValueCardTypes = new HashSet<String>(4);
    
    static {
        nonValueCardTypes.add(Integer.toString(WebCoreConstants.CARD_TYPE_CREDIT_CARD));
        nonValueCardTypes.add(Integer.toString(WebCoreConstants.CARD_TYPE_SMART_CARD));
    }
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private CustomerBadCardService customerBadCardService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    private CustomerCardType unknownCustomerCardType;
    private List<CustomerCardType> creditCustomerCardTypes;
    private List<CustomerCardType> defaultCreditCustomerCardTypes;
    
    @PostConstruct
    public void init() {
        createCreditCustomerCardTypes();
        buildDefaultCreditCardPatterns();
        
    }
    
    @Override
    public CustomerCardType getUnknownCustomerCardType() {
        if (this.unknownCustomerCardType == null) {
            
            final CardType naCardType = this.cardTypeService.getCardTypesMap().get(0);
            this.unknownCustomerCardType =
                    new CustomerCardType(CardProcessingConstants.NAME_UNKNOWN, CardProcessingConstants.TRACK_2_UNKNOWN, false, naCardType);
        }
        return this.unknownCustomerCardType;
    }
    
    // *********************************************************************
    // Credit Card Type, copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl 
    // *********************************************************************
    @Override
    public CustomerCardType getCreditCardTypeByAccountNumber(final String accountNumber) {
        return this.getCardTypeByAccountNumber(accountNumber, getCreditCustomerCardTypes());
    }
    
    @Override
    public CustomerCardType getCreditCardTypeByTrack2Data(final String track2Data) {
        return this.getCardTypeByTrack2Data(track2Data, getCreditCustomerCardTypes());
    }
    
    @Override
    public CustomerCardType getCardTypeByAccountNumber(final int customerId, final String accountNumber) {
        CustomerCardType customerCardType = getCreditCardTypeByAccountNumber(accountNumber);
        if (customerCardType == null && customerId > 0) {
            customerCardType = getCardTypeByAccountNumber(accountNumber, getAllCustomerCardType(customerId));
        }
        if (customerCardType == null) {
            return getUnknownCustomerCardType();
        }
        return customerCardType;
    }
    
    private void logCustomerCardType(final String info, final CustomerCardType customerCardType) {
        log.debug(info + " CustomerCardType: " + customerCardType);
        log.debug(info + " customerCardType.getCardType(): " + (customerCardType == null ? null : customerCardType.getCardType()));
    }
    
    @Override
    public CustomerCardType getCardTypeByTrack2Data(final int customerId, final String track2Data) {
        CustomerCardType customerCardType = getCreditCardTypeByTrack2Data(track2Data);
        logCustomerCardType("getCreditCardTypeByTrack2Data", customerCardType);
        if (customerCardType == null) {
            customerCardType = findCustomerCardTypeForTransaction(customerId, WebCoreConstants.VALUE_CARD, track2Data);
            logCustomerCardType("findCustomerCardTypeForTransaction", customerCardType);
        }
        if (customerCardType == null) {
            customerCardType = getUnknownCustomerCardType();
            logCustomerCardType("getUnknownCustomerCardType", customerCardType);
        }
        return customerCardType;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    public CustomerCardType getCardTypeByTrack2Data(final String track2Data, List<CustomerCardType> customerCardTypes) {
        customerCardTypes = setDataPatterns(customerCardTypes);
        
        CustomerCardType cardType = null;
        boolean algorithmFlag = false;
        final Iterator<CustomerCardType> iterator = customerCardTypes.iterator();
        while (iterator.hasNext()) {
            cardType = iterator.next();
            // check if pattern matches
            if (cardType.getDataPattern().getTrack2Pattern().matcher(track2Data.trim()).matches()) {
                algorithmFlag = cardType.isIsDigitAlgorithm();
                
                // if the pattern matches check for Mod10
                if (algorithmFlag == CardProcessingConstants.CHECK_DIGIT_ALG_MOD10) {
                    if (performLuhnCheck(CardProcessingUtil.extractAccountNumber(track2Data, CardProcessingConstants.TRACK_2_DELIMITER))) {
                        return cardType;
                    }
                } else {
                    return cardType;
                }
            }
        }
        return null;
        
    }
    
    // *********************************************************************
    // Similar logic as EMS 6.3.11 com.digitalpioneer.util.Track2Card -> constructor -> determineCardNameAndType(...)
    // *********************************************************************    
    @Override
    public CustomerCardType getCardTypeByTrack2DataOrAccountNumber(final int customerId, final String track2Data) {
        CustomerCardType cardType = getCardTypeByTrack2Data(customerId, track2Data);
        
        if (cardType == null || cardType.getName().equals(CardProcessingConstants.NAME_UNKNOWN)) {
            int accountNumberLength = track2Data.indexOf(Track2Card.TRACK_2_DELIMITER);
            if (accountNumberLength == -1) {
                accountNumberLength = track2Data.length();
            }
            final String accountNumber = track2Data.substring(0, accountNumberLength);
            cardType = getCardTypeByAccountNumber(customerId, accountNumber);
        }
        return cardType;
    }
    
    // *********************************************************************
    // Customer Card Type, copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl
    // *********************************************************************
    @SuppressWarnings("unchecked")
    @Override
    public List<CustomerCardType> getAllCustomerCardType(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerCardType.findByCustomerId", CUSTOMER_ID, customerId);
    }
    
    @Override
    public CustomerCardType getCardType(final int id) {
        return (CustomerCardType) this.entityDao.get(CustomerCardType.class, id);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createCustomerCardType(final CustomerCardType cardType) throws DuplicateObjectException {
        validateIfIsExistTrack2Pattern(cardType);
        if ((cardType.getCardType() != null) && (cardType.getCardType().getId() == WebCoreConstants.CARD_TYPE_VALUE_CARD)) {
            try {
                cardType.setParentCustomerCardType(self().createPrimaryTypeIfAbsent(cardType.getCustomer().getId(), cardType.getName()));
            } catch (Exception e) {
                cardType.setParentCustomerCardType(this.getPrimaryType(cardType.getCustomer().getId(), cardType.getName()));
            }
        }
        
        this.entityDao.save(cardType);
        this.entityDao.flush();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCardType(final CustomerCardType cardType) throws DuplicateObjectException {
        validateIfIsExistTrack2Pattern(cardType);
        CustomerCardType existing = this.entityDao.get(CustomerCardType.class, cardType.getId());
        if (existing == null) {
            throw new IllegalStateException("CustomerCardType '" + cardType.getId() + "' does not existed !");
        }
        
        existing = (CustomerCardType) this.entityDao.merge(existing);
        
        final Integer customerId = existing.getCustomer().getId();
        final Date now = new Date();
        if (cardType.getCardType().getId() == WebCoreConstants.CARD_TYPE_VALUE_CARD) {
            CustomerCardType primary = null;
            if (existing.getParentCustomerCardType() != null) {
                if (cardType.getName().equals(existing.getName())) {
                    primary = existing.getParentCustomerCardType();
                } else {
                    boolean changePrimaryName = false;
                    if (cardType.getName().equalsIgnoreCase(existing.getParentCustomerCardType().getName())) {
                        changePrimaryName = true;
                    } else {
                        final long secondariesCount = ((Number) this.entityDao.getNamedQuery(CUSTOMER_CARD_TYPE_COUNT_SECONDARY_BY_CUSTOMER_CARD_TYPE)
                                .setParameter(CUSTOMER_CARD_TYPE_ID, existing.getId()).uniqueResult()).longValue();
                        if (secondariesCount <= 1) {
                            // Only referenced by one secondary (which probably "this" one), just updating the Primary CustomerCardType's name is enough
                            changePrimaryName = this.getPrimaryType(customerId, cardType.getName()) == null;
                        }
                    }
                    
                    if (changePrimaryName) {
                        primary = existing.getParentCustomerCardType();
                        primary.setName(cardType.getName());
                        primary.setLastModifiedGmt(now);
                        
                        this.entityDao.update(primary);
                    }
                }
            }
            
            if (primary == null) {
                try {
                    primary = self().createPrimaryTypeIfAbsent(customerId, cardType.getName());
                } catch (Exception e) {
                    primary = this.getPrimaryType(customerId, cardType.getName());
                }
            }
            
            existing.setParentCustomerCardType(primary);
        }
        
        existing.setAuthorizationType(cardType.getAuthorizationType());
        existing.setName(cardType.getName());
        existing.setTrack2pattern(cardType.getTrack2pattern());
        existing.setDescription(cardType.getDescription());
        existing.setLastModifiedGmt(now);
        existing.setLastModifiedByUserId(cardType.getLastModifiedByUserId());
        
        this.entityDao.update(existing);
        this.entityDao.flush();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCardType(final CustomerCardType customerCardType) {
        customerCardType.setIsDeleted(true);
        customerCardType.setArchiveDate(new Date());
        this.entityDao.update(customerCardType);
        
        this.deletePrimaryIfNoChild(customerCardType.getParentCustomerCardType());
        this.entityDao.flush();
    }
    
    @Override
    public boolean isExistCardType(final CustomerCardType cardType) {
        final int totalCustomercardTypeSize = getTotalCustomerCardType(cardType);
        if (totalCustomercardTypeSize > 0) {
            return true;
        }
        return false;
    }
    
    @Override
    public int getTotalCustomerCardType(final CustomerCardType cardType) {
        final String[] params = { CUSTOMER_ID, NAME };
        final Object[] values = { cardType.getCustomer().getId(), cardType.getName() };
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> list =
                this.entityDao.findByNamedQueryAndNamedParam(CUSTOMER_CARD_TYPE_FIND_BY_CUSTOMER_ID_AND_NAME, params, values);
        return list.size();
    }
    
    @Override
    public List<CustomerCardType> getCustomerCardTypesByCustomerAndName(final int customerId, final String name) {
        final String[] params = { CUSTOMER_ID, NAME };
        final Object[] values = { customerId, name };
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> list =
                this.entityDao.findByNamedQueryAndNamedParam(CUSTOMER_CARD_TYPE_FIND_BY_CUSTOMER_ID_AND_NAME, params, values);
        return list;
    }
    
    private void validateIfIsExistTrack2Pattern(final CustomerCardType cardType) throws DuplicateObjectException {
        boolean isOk = true;
        if (!CardProcessingUtil.isExistTrack2Pattern(cardType.getTrack2pattern())) {
            isOk = false;
        }
        
        if (isOk) {
            final String[] params = { CUSTOMER_ID, "track2pattern", "id" };
            final Object[] values = { cardType.getCustomer().getId(), cardType.getTrack2pattern(), cardType.getId() };
            @SuppressWarnings("unchecked")
            final List<CustomerCardType> cardTypes =
                    this.entityDao.findByNamedQueryAndNamedParam("CustomerCardType.findByCustomerIdAndTrack2Pattern", params, values);
            if (cardTypes.size() > 0) {
                isOk = false;
            }
        }
        
        if (!isOk) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("CustomerCardType, createCustomerCardType, existing pattern matched with CustomerCardType with data pattern='");
            bdr.append(cardType.getTrack2pattern()).append("' for customerId = ").append(cardType.getCustomer().getId());
            throw new DuplicateObjectException(bdr.toString());
        }
    }
    
    @Override
    public boolean isExistTrack2Pattern(final Integer customerId, final String track2pattern) {
        final Customer cust = new Customer();
        cust.setId(customerId);
        final CustomerCardType cct = new CustomerCardType();
        cct.setCustomer(cust);
        cct.setTrack2pattern(track2pattern);
        cct.setId(0);
        try {
            validateIfIsExistTrack2Pattern(cct);
        } catch (DuplicateObjectException doe) {
            return true;
        }
        return false;
    }
    
    private List<CustomerCardType> setDataPatterns(final List<CustomerCardType> customerCardTypes) {
        final Iterator<CustomerCardType> iter = customerCardTypes.iterator();
        while (iter.hasNext()) {
            iter.next().createDataPattern();
        }
        return customerCardTypes;
    }
    
    private CustomerCardType getCardTypeByAccountNumberOrTrack2Data(final String accountNumber, final List<CustomerCardType> customerCardTypes) {
        CustomerCardType custCardType = null;
        // If account number doesn't have '=', call 'getCardTypeByAccountNumber'
        if (accountNumber.indexOf(Track2Card.TRACK_2_DELIMITER) == -1) {
            custCardType = getCardTypeByAccountNumber(accountNumber, customerCardTypes);
        }
        // Try to use 'getCardTypeByTrack2Data' if couldn't find CustomerCardType.
        if (custCardType == null) {
            custCardType = getCardTypeByTrack2Data(accountNumber, customerCardTypes);
        }
        return custCardType;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    public CustomerCardType getCardTypeByAccountNumber(final String accountNumber, List<CustomerCardType> customerCardTypes) {
        customerCardTypes = setDataPatterns(customerCardTypes);
        
        CustomerCardType cardType = null;
        boolean algorithmFlag = false;
        final Iterator<CustomerCardType> iterator = customerCardTypes.iterator();
        while (iterator.hasNext()) {
            cardType = iterator.next();
            // check if pattern matches
            if (cardType.getTrack2pattern().indexOf(Track2Card.TRACK_2_DELIMITER) == -1
                && cardType.getDataPattern().getAccountNumberPattern().matcher(accountNumber.trim()).matches()) {
                
                algorithmFlag = cardType.isIsDigitAlgorithm();
                // if the pattern matches check for Mod10
                if (algorithmFlag == CardProcessingConstants.CHECK_DIGIT_ALG_MOD10) {
                    if (performLuhnCheck(accountNumber)) {
                        return cardType;
                    }
                } else {
                    return cardType;
                }
            }
        }
        return null;
    }
    
    // This method intended to be call from UI ONLY !
    @Override
    public boolean isCreditCardData(final String cardNumber) {
        CustomerCardType cardType = null;
        boolean algorithmFlag = false;
        final Iterator<CustomerCardType> iterator = getCreditCustomerCardTypes().iterator();
        while (iterator.hasNext() && (cardType == null)) {
            final CustomerCardType candidateCT = iterator.next();
            // check if pattern matches
            if (candidateCT.getDataPattern().getAccountNumberPattern().matcher(cardNumber).matches()) {
                
                algorithmFlag = candidateCT.isIsDigitAlgorithm();
                // if the pattern matches check for Mod10
                if (algorithmFlag == CardProcessingConstants.CHECK_DIGIT_ALG_MOD10) {
                    if (performLuhnCheck(cardNumber)) {
                        cardType = candidateCT;
                    }
                } else {
                    cardType = candidateCT;
                }
            }
        }
        
        return cardType != null
               && cardType.getCardType().getId() == this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD).getId();
    }
    
    // FOR UI ONLY !
    @Override
    public CustomerCardType getCreditCardTypeByCardNumber(final int customerId, final String cardNumber) {
        CustomerCardType cardType = null;
        boolean algorithmFlag = false;
        final Iterator<CustomerCardType> iterator = getCreditCustomerCardTypes().iterator();
        while (iterator.hasNext() && (cardType == null)) {
            final CustomerCardType candidateCT = iterator.next();
            // check if pattern matches
            if (candidateCT.getDataPattern().getAccountNumberPattern().matcher(cardNumber).matches()) {
                
                algorithmFlag = candidateCT.isIsDigitAlgorithm();
                // if the pattern matches check for Mod10
                if (algorithmFlag == CardProcessingConstants.CHECK_DIGIT_ALG_MOD10) {
                    if (performLuhnCheck(cardNumber)) {
                        cardType = candidateCT;
                    }
                } else {
                    cardType = candidateCT;
                }
            }
        }
        
        if (cardType == null && customerId > 0) {
            cardType = getCardTypeByAccountNumber(cardNumber, getAllCustomerCardType(customerId));
        }
        if (cardType == null) {
            cardType = getUnknownCustomerCardType();
        }
        
        return cardType;
    }
    
    @Override
    public boolean isCreditCardTrack2(final String track2Data) {
        final CustomerCardType cardType = this.getCardTypeByTrack2Data(track2Data, getCreditCustomerCardTypes());
        return cardType != null
               && cardType.getCardType().getId() == this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD).getId();
    }
    
    @Override
    public boolean isCreditCardAccountNumber(final String accountNumber) {
        final CustomerCardType cardType = getCardTypeByAccountNumber(accountNumber, getCreditCustomerCardTypes());
        return cardType != null
               && cardType.getCardType().getId() == this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD).getId();
    }
    
    /*
     * 'IsForBadcard' column is now removed and only search bad cards from CustomerBadCard table.
     */
    @Override
    public CustomerCardType findCustomerCardTypeForBadCard(final Integer customerId, final int cardTypeId) {
        return (CustomerCardType) this.entityDao.findUniqueByNamedQueryAndNamedParam("CustomerCardType.findByCustomerIdCardTypeId",
                                                                                     new String[] { CUSTOMER_ID, CARD_TYPE_ID },
                                                                                     new Object[] { customerId, cardTypeId }, true);
    }
    
    @Override
    public final boolean generateBadCardList(final int customerId, final int cardTypeId, final ZipOutputStream zipfos) {
        final SortedSet<String> badCCHashList = new TreeSet<String>();
        PrintStream fout = null;
        try {
            // get rid of expried credit card
            final Collection<CustomerBadCard> badCcCards = this.customerBadCardService.findNoExpiredBadCreditCard(customerId, cardTypeId);
            
            final Iterator<CustomerBadCard> it = badCcCards.iterator();
            while (it.hasNext()) {
                final CustomerBadCard badCCard = it.next();
                badCCHashList.add(badCCard.getCardNumberOrHash());
            }
            
            final Customer customer = (Customer) this.entityDao.get(Customer.class, customerId);
            
            int badCCRetry = 0;
            CustomerProperty prop;
            final Iterator<CustomerProperty> iter = customer.getCustomerProperties().iterator();
            while (iter.hasNext()) {
                prop = iter.next();
                if (prop.getCustomerPropertyType().getId() == WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY) {
                    badCCRetry = Integer.parseInt(prop.getPropertyValue());
                    break;
                }
            }
            
            final Collection<PointOfSale> poss = this.pointOfSaleService.findPointOfSalesByCustomerId(customerId);
            if (poss == null || poss.isEmpty()) {
                return false;
            }
            
            // Only for credit cards and value cards.
            if (cardTypeId == CardProcessingConstants.CARD_TYPE_CREDIT_CARD || cardTypeId == CardProcessingConstants.CARD_TYPE_VALUE_CARD) {
                // get rid of expired credit card and check if greater than bad card retry count for customer
                final Collection<String> badCardHashs = this.cardRetryTransactionService
                        .findBadCardHashByPointOfSaleIdsDateAndNumRetries(getPointOfSaleIds(poss), DateUtil.createCurrentYYMM(), badCCRetry);
                final Iterator<String> iter2 = badCardHashs.iterator();
                while (iter2.hasNext()) {
                    final String badCardHash = iter2.next();
                    if (StringUtils.isNotBlank(badCardHash)) {
                        badCCHashList.add(badCardHash);
                    }
                }
            }
            
            fout = new PrintStream(zipfos);
            final Iterator<String> its = badCCHashList.iterator();
            
            while (its.hasNext()) {
                fout.print(its.next());
                fout.print('\n');
            }
            
            return true;
        } finally {
            if (fout != null) {
                fout.close();
            }
        }
        
    }
    
    private List<Integer> getPointOfSaleIds(final Collection<PointOfSale> pointOfSales) {
        final List<Integer> list = new ArrayList<Integer>(pointOfSales.size());
        final Iterator<PointOfSale> iter = pointOfSales.iterator();
        while (iter.hasNext()) {
            list.add(iter.next().getId());
        }
        return list;
    }
    
    @Override
    public List<CustomerCardType> findByCustomerIdCardTypeIdIsLocked(final Integer customerId, final Integer cardTypeId, final boolean isLocked) {
        final String[] params = { CUSTOMER_ID, CARD_TYPE_ID, IS_LOCKED };
        final Object[] values = new Object[] { customerId, cardTypeId, isLocked };
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> list =
                this.entityDao.findByNamedQueryAndNamedParam("CustomerCardType.findByCustomerIdCardTypeIdIsLocked", params, values, true);
        if (list != null && !list.isEmpty()) {
            return list;
        }
        return null;
    }
    
    @Override
    public List<CustomerCardType> findByCustomerIdIsLocked(final Integer customerId, final boolean isLocked) {
        final String[] params = { CUSTOMER_ID, IS_LOCKED, "isDeleted" };
        final Object[] values = new Object[] { customerId, isLocked, false };
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> list =
                this.entityDao.findByNamedQueryAndNamedParam("CustomerCardType.findByCustomerIdIsLockedIsDeleted", params, values, true);
        if (list != null && !list.isEmpty()) {
            return list;
        }
        return null;
    }
    
    @Override
    public CustomerCardType findCustomerCardTypeForTransaction(final Integer customerId, final Integer cardTypeId, final String cardNumber) {
        CustomerCardType ccType = null;
        List<CustomerCardType> ccTypes = findByCustomerIdCardTypeIdIsLocked(customerId, cardTypeId, false);
        ccTypes = removeDeletedCustomerCardTypes(ccTypes);
        
        if (ccTypes == null || ccTypes.isEmpty()) {
            return null;
        } else if (ccTypes.size() == 1 && StringUtils.isBlank(cardNumber)) {
            ccType = ccTypes.get(0);
        } else if (StringUtils.isNotBlank(cardNumber)) {
            // Check getCardTypeByAccountNumber and/or getCardTypeByTrack2Data.  
            ccType = getCardTypeByAccountNumberOrTrack2Data(cardNumber, ccTypes);
        }
        if (ccType != null) {
            ccType.setAuthorizationType((AuthorizationType) this.entityDao.get(AuthorizationType.class, ccType.getAuthorizationType().getId()));
        }
        return ccType;
    }
    
    private List<CustomerCardType> removeDeletedCustomerCardTypes(final List<CustomerCardType> ccTypes) {
        if (ccTypes == null || ccTypes.isEmpty()) {
            return ccTypes;
        }
        
        final List<CustomerCardType> list = new ArrayList<CustomerCardType>();
        final Iterator<CustomerCardType> iter = ccTypes.iterator();
        while (iter.hasNext()) {
            final CustomerCardType cct = iter.next();
            if (!cct.isIsDeleted()) {
                list.add(cct);
            }
        }
        return list;
    }
    
    // LUHN Formula (Mod 10) for Validation of Primary Account Number
    // The following steps are required to validate the primary account number:
    //
    // Step 1: Double the value of alternate digits of the primary account number beginning with the second
    // digit from the right (the first right--hand digit is the check digit.)
    //
    // Step 2: Add the individual digits comprising the products obtained in Step 1 to each of the unaffected
    // digits in the original number.
    //
    // Step 3: The total obtained in Step 2 must be a number ending in zero (30, 40, 50, etc.) for the account
    // number to be validated.
    //
    // For example, to validate the primary account number 49927398716:
    //
    // Step 1:
    //
    // 4 9 9 2 7 3 9 8 7 1 6
    // x2 x2 x2 x2 x2
    // ------------------------------
    // 18 4 6 16 2
    //
    // Step 2: 4 +(1+8)+ 9 + (4) + 7 + (6) + 9 +(1+6) + 7 + (2) + 6
    //
    // Step 3: Sum = 70 : Card number is validated
    //
    // Note: Card is valid because the 70/10 yields no remainder.
    
    private boolean performLuhnCheck(final String primaryAccountNumber) {
        int sum = 0;
        int alternatingFactor = 1;
        
        if (primaryAccountNumber != null) {
            for (int i = primaryAccountNumber.length() - 1; i >= 0; i--) {
                final int currentInt = Integer.parseInt(primaryAccountNumber.substring(i, i + 1));
                int subSum = alternatingFactor * currentInt;
                
                if (subSum >= 10) {
                    final String intString = Integer.toString(subSum);
                    
                    subSum = Integer.parseInt(intString.substring(0, 1)) + Integer.parseInt(intString.substring(1, 2));
                }
                sum += subSum;
                alternatingFactor = alternatingFactor == 2 ? 1 : 2;
            }
            // The remainder of dividing by 10 should be 0.
            if ((sum % 10) == 0) {
                return true;
            }
        }
        return false;
    }
    
    public List<CustomerCardType> getCreditCustomerCardTypes() {
        
        if (this.creditCustomerCardTypes == null || this.creditCustomerCardTypes.isEmpty()) {
            createCreditCustomerCardTypes();
            if (this.creditCustomerCardTypes == null || this.creditCustomerCardTypes.isEmpty()) {
                return this.defaultCreditCustomerCardTypes;
            } else {
                return this.creditCustomerCardTypes;
            }
        } else {
            return this.creditCustomerCardTypes;
        }
        
    }
    
    public void setCreditCustomerCardTypes(final List<CustomerCardType> creditCustomerCardTypes) {
        this.creditCustomerCardTypes = creditCustomerCardTypes;
    }
    
    public void setCardTypeService(final CardTypeService cardTypeService) {
        this.cardTypeService = cardTypeService;
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setCustomerBadCardService(final CustomerBadCardService customerBadCardService) {
        this.customerBadCardService = customerBadCardService;
    }
    
    public void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    @Override
    public CustomerCardType getCustomerCardTypeById(final int id) {
        return (CustomerCardType) this.entityDao.getCurrentSession().load(CustomerCardType.class, id);
    }
    
    @Override
    public CustomerCardType getPrimaryType(final Integer customerId, final String name) {
        CustomerCardType result = null;
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> candidates = this.entityDao.getNamedQuery(CUSTOMER_CARD_TYPE_FIND_PRIMARY_BY_CUSTOMER_ID_AND_NAME)
                .setParameter(CUSTOMER_ID, customerId).setParameter(NAME, name).list();
        if (candidates.size() > 0) {
            result = candidates.get(0);
        }
        
        return result;
    }
    
    @Override
    @Async
    public Future<Boolean> ensurePrimaryReferenceAsync(final Integer... customerCardTypeIds) {
        self().ensurePrimaryReference(customerCardTypeIds);
        return new AsyncResult<Boolean>(true);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void ensurePrimaryReference(final Integer... customerCardTypeIds) {
        try {
            @SuppressWarnings("unchecked")
            final List<CustomerCardType> cardTypesList = this.entityDao.getNamedQuery("CustomerCardType.findMigratedTypeByIds")
                    .setParameterList(CUSTOMER_CARD_TYPE_IDS, customerCardTypeIds).list();
            
            for (CustomerCardType cardType : cardTypesList) {
                if ((!cardType.isIsDeleted()) && (!cardType.isIsPrimary()) && (cardType.getParentCustomerCardType() == null)) {
                    final CustomerCardType primary = this.createPrimaryTypeIfAbsent(cardType.getCustomer().getId(), cardType.getName());
                    cardType.setParentCustomerCardType(primary);
                    
                    this.entityDao.update(primary);
                    if (log.isDebugEnabled()) {
                        log.debug("Refering CustomerCardType '" + cardType.getId() + "' to Primary CustomerCardType '" + primary.getId()
                                  + SINGLE_QUOTE_STRING);
                    }
                }
            }
            
        } catch (Exception e) {
            log.warn("Could not associate Primary CustomerCardType: " + e.getMessage());
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CustomerCardType createPrimaryTypeIfAbsent(final Integer customerId, final String name) throws Exception {
        final Date now = new Date();
        CustomerCardType primeType = null;
        
        final List<CustomerCardType> candidates = this.entityDao.getNamedQuery(CUSTOMER_CARD_TYPE_FIND_PRIMARY_BY_CUSTOMER_ID_AND_NAME)
                .setParameter(CUSTOMER_ID, customerId).setParameter(NAME, name).list();
        if (candidates.size() <= 0) {
            primeType = new CustomerCardType();
            primeType.setCustomer(new Customer(customerId));
            primeType.setName(name);
            primeType.setTrack2pattern("PRIMARY VALUE CARD");
            primeType.setIsPrimary(true);
            
            primeType.setCardType(new CardType());
            primeType.getCardType().setId(3);
            
            primeType.setAuthorizationType(new AuthorizationType(0));
            primeType.setLastModifiedGmt(now);
            primeType.setLastModifiedByUserId(1);
            
            this.entityDao.save(primeType);
            if (log.isDebugEnabled()) {
                log.debug("Created Primary CustomerCardType '" + primeType.getId() + "' for name '" + name + SINGLE_QUOTE_STRING);
            }
        } else {
            if (candidates.size() == 1) {
                primeType = candidates.get(0);
                if (log.isDebugEnabled()) {
                    log.debug("Matched exactly 1 Primary CustomerCardType with name '" + name + SINGLE_QUOTE_STRING);
                }
            } else {
                final ArrayList<Integer> redundants = new ArrayList<Integer>(candidates.size());
                if (log.isDebugEnabled()) {
                    log.debug("Matched " + candidates.size() + " Primary CustomerCardTypes with name '" + name
                              + "'. Will prune the Primary CustomerCardType to just one.");
                }
                
                Iterator<CustomerCardType> primeItr = candidates.iterator();
                primeType = primeItr.next();
                while (primeItr.hasNext()) {
                    final CustomerCardType cct = primeItr.next();
                    redundants.add(cct.getId());
                }
                
                moveCustomerCardType(primeType.getId(), redundants);
                if (log.isDebugEnabled()) {
                    log.debug("Re-referenced all chid records for all Redundant Primaries to the actual Primary CustomerCardType '"
                              + primeType.getId());
                }
                
                primeItr = candidates.iterator();
                primeItr.next();
                while (primeItr.hasNext()) {
                    final CustomerCardType redundance = primeItr.next();
                    this.deleteCardType(redundance);
                    if (log.isDebugEnabled()) {
                        log.debug("Deleted Redundant Primary CustomerCardType '" + redundance.getId() + SINGLE_QUOTE_STRING);
                    }
                }
            }
            
            // This is for changing letter case
            if (!name.equals(primeType.getName())) {
                primeType.setName(name);
                primeType.setLastModifiedGmt(now);
                this.entityDao.update(primeType);
            }
        }
        
        final List<CustomerCardType> secondaries = this.entityDao.getNamedQuery("CustomerCardType.findMigratedByCustomerIdAndName")
                .setParameter(CUSTOMER_ID, customerId).setParameter(NAME, name).list();
        if (secondaries.size() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("Found " + secondaries.size() + " Migrated CustomerCardType to be assigned as Secondary for the Primary CustomerCardType '"
                          + primeType.getId() + SINGLE_QUOTE_STRING);
            }
            
            final ArrayList<Integer> oldIds = new ArrayList<Integer>(secondaries.size());
            for (CustomerCardType sec : secondaries) {
                oldIds.add(sec.getId());
            }
            
            this.entityDao.getNamedQuery("CustomerCardType.assignPrimary").setParameter("primaryCardTypeId", primeType.getId())
                    .setParameterList(CUSTOMER_CARD_TYPE_IDS, oldIds).executeUpdate();
            if (log.isTraceEnabled()) {
                log.trace("Secondary assignment completed");
            }
            
            moveCustomerCardType(primeType.getId(), oldIds);
            if (log.isDebugEnabled()) {
                log.debug("Re-referenced all chid records for all Secondaries to the actual Primary CustomerCardType '" + primeType.getId());
            }
        }
        
        this.entityDao.flush();
        this.entityDao.evict(primeType);
        
        return primeType;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CustomerCardType createPrimaryTypeIfAbsentFor(final Integer customerCardTypeId) throws Exception {
        CustomerCardType result = null;
        if (customerCardTypeId != null) {
            final CustomerCardType cct = this.getCardType(customerCardTypeId);
            if (cct.isIsPrimary() || (WebCoreConstants.CARD_TYPE_VALUE_CARD != cct.getCardType().getId())) {
                result = cct;
            } else {
                result = createPrimaryTypeIfAbsent(cct.getCustomer().getId(), cct.getName());
            }
        }
        
        return result;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deletePrimaryIfNoChild(final CustomerCardType primary) {
        CustomerCardType target = primary;
        if (target != null) {
            try {
                target = self().createPrimaryTypeIfAbsent(target.getCustomer().getId(), target.getName());
                target = (CustomerCardType) this.entityDao.merge(target);
            } catch (Exception e) {
                target = primary;
            }
        }
        
        if (target != null) {
            final Number count = (Number) this.entityDao.getNamedQuery(CUSTOMER_CARD_TYPE_COUNT_SECONDARY_BY_CUSTOMER_CARD_TYPE)
                    .setParameter(CUSTOMER_CARD_TYPE_ID, target.getId()).uniqueResult();
            if ((count == null) || (count.longValue() <= 1)) {
                this.deleteCardType(target);
            }
        }
        
        return 0;
    }
    
    private int moveCustomerCardType(final Integer newCardTypeIds, final Collection<Integer> oldCardTypeIds) {
        int updated = 0;
        updated += this.entityDao.getNamedQuery(CUSTOMER_CARD_CHANGE_CUSTOMER_CARD_TYPE).setParameter(NEW_CUSTOMER_CARD_TYPE_ID, newCardTypeIds)
                .setParameterList(OLD_CUSTOMER_CARD_TYPE_IDS, oldCardTypeIds).executeUpdate();
        
        updated += this.entityDao.getNamedQuery(CUSTOMER_CARD_CHANGE_CUSTOMER_CARD_TYPE).setParameter(NEW_CUSTOMER_CARD_TYPE_ID, newCardTypeIds)
                .setParameterList(OLD_CUSTOMER_CARD_TYPE_IDS, oldCardTypeIds).executeUpdate();
        
        return updated;
    }
    
    @Override
    public int countChildType(final Integer customerCardTypeId) {
        int result = 0;
        
        final Number count = (Number) this.entityDao.getNamedQuery(CUSTOMER_CARD_TYPE_COUNT_SECONDARY_BY_CUSTOMER_CARD_TYPE)
                .setParameter(CUSTOMER_CARD_TYPE_ID, customerCardTypeId).uniqueResult();
        if (count != null) {
            result = count.intValue();
        }
        
        return result;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    public List<FilterDTO> getBannableCardTypeFilters(List<FilterDTO> result, final Integer customerId, final RandomKeyMapping keyMapping) {
        final int originalSize = result.size();
        
        final ValueCardNameFilter filter = new ValueCardNameFilter();
        
        final FilterDTOTransformer transformer = new FilterDTOTransformer(keyMapping, CustomerCardType.class, result, filter);
        this.entityDao.getNamedQuery("CustomerCardType.findPrimaryBannableTypeFiltersByCustomerId").setParameter(CUSTOMER_ID, customerId)
                .setResultTransformer(transformer).list();
        
        final int includedPrimarySize = result.size();
        
        this.entityDao.getNamedQuery("CustomerCardType.findMigratedBannableTypeFiltersByCustomerId").setParameter(CUSTOMER_ID, customerId)
                .setResultTransformer(transformer).list();
        
        final int totalSize = result.size();
        if (totalSize > includedPrimarySize) {
            result = Sorts.sortSeparateSortedList(result, new FilterDTO.LabelComparator(), originalSize, includedPrimarySize);
        }
        
        return result;
    }
    
    @Override
    public List<CustomerCardType> getDetachedBannableCardTypes(final Integer customerId, final Collection<String> names) {
        
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> result = this.entityDao.getNamedQuery("CustomerCardType.findPrimaryBannableTypesByCustomerIdAndTypeNames")
                .setParameter(CUSTOMER_ID, customerId).setParameterList(CUSTOMER_CARD_TYPE_NAMES, names).list();
        
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> migrated = this.entityDao.getNamedQuery("CustomerCardType.findMigratedBannableTypesByCustomerIdAndTypeNames")
                .setParameter(CUSTOMER_ID, customerId).setParameterList(CUSTOMER_CARD_TYPE_NAMES, names).list();
        
        consolidatePrimaryAndMigrated(result, migrated, true);
        
        this.entityDao.getCurrentSession().clear();
        
        return result;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<CustomerCardType> getDetachedBannableCardTypes(final int customerId) {
        final List<CustomerCardType> result =
                this.entityDao.getNamedQuery("CustomerCardType.findCreditAndSmartCardByCustomerId").setParameter(CUSTOMER_ID, customerId).list();
        // Evict All
        this.entityDao.getCurrentSession().clear();
        
        return result;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    public List<FilterDTO> getValueCardTypeFilters(List<FilterDTO> result, final Integer customerId, final boolean showDeleted,
        final RandomKeyMapping keyMapping) {
        final int originalSize = result.size();
        
        final ValueCardNameFilter filter = new ValueCardNameFilter();
        
        final FilterDTOTransformer transformer = new FilterDTOTransformer(keyMapping, CustomerCardType.class, result, filter);
        this.entityDao.getNamedQuery("CustomerCardType.findPrimaryValueCardTypeFiltersByCustomerId").setParameter(CUSTOMER_ID, customerId)
                .setParameter(IGNORE_DELETED_FLAG, showDeleted).setResultTransformer(transformer).list();
        
        final int includedPrimarySize = result.size();
        
        this.entityDao.getNamedQuery("CustomerCardType.findMigratedValueCardTypeFiltersByCustomerId").setParameter(CUSTOMER_ID, customerId)
                .setParameter(IGNORE_DELETED_FLAG, showDeleted).setResultTransformer(transformer).list();
        
        final int totalSize = result.size();
        if (totalSize > includedPrimarySize) {
            result = Sorts.sortSeparateSortedList(result, new FilterDTO.LabelComparator(), originalSize, includedPrimarySize);
        }
        
        return result;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    public final List<FilterDTO> getValueCardTypeFiltersWithDeletedFlag(List<FilterDTO> result, final Integer customerId, final boolean showDeleted,
        final RandomKeyMapping keyMapping) {
        final int originalSize = result.size();
        
        final FilterDTOTransformer transformer = new FilterDTOTransformer(keyMapping, CustomerCardType.class, result);
        this.entityDao.getNamedQuery("CustomerCardType.findPrimaryValueCardTypeFiltersWithDeletedFlagByCustomerId")
                .setParameter(CUSTOMER_ID, customerId).setParameter(IGNORE_DELETED_FLAG, showDeleted).setResultTransformer(transformer).list();
        
        final int includedPrimarySize = result.size();
        
        this.entityDao.getNamedQuery("CustomerCardType.findMigratedValueCardTypeFiltersWithDeletedFlagByCustomerId")
                .setParameter(CUSTOMER_ID, customerId).setParameter(IGNORE_DELETED_FLAG, showDeleted).setResultTransformer(transformer).list();
        
        final int totalSize = result.size();
        if (totalSize > includedPrimarySize) {
            result = Sorts.sortSeparateSortedList(result, new FilterDTO.LabelComparator(), originalSize, includedPrimarySize);
        }
        
        return result;
    }
    
    @Override
    public List<CustomerCardType> getDetachedValueCardTypes(final Integer customerId, final Collection<String> names) {
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> result = this.entityDao.getNamedQuery("CustomerCardType.findPrimaryValueCardTypesByCustomerIdAndTypeNames")
                .setParameter(CUSTOMER_ID, customerId).setParameterList(CUSTOMER_CARD_TYPE_NAMES, names).list();
        
        @SuppressWarnings("unchecked")
        final List<CustomerCardType> migrated = this.entityDao.getNamedQuery("CustomerCardType.findMigratedValueCardTypesByCustomerIdAndTypeNames")
                .setParameter(CUSTOMER_ID, customerId).setParameterList(CUSTOMER_CARD_TYPE_NAMES, names).list();
        
        consolidatePrimaryAndMigrated(result, migrated, true);
        
        this.entityDao.getCurrentSession().clear();
        
        return result;
    }
    
    private void consolidatePrimaryAndMigrated(final List<CustomerCardType> primary, final List<CustomerCardType> migrated,
        final boolean forBatchProcessing) {
        final Map<String, Integer> valueCardNamesIdsMap = new HashMap<String, Integer>(primary.size() * 2);
        for (CustomerCardType cct : primary) {
            if ((cct.getCardType().getId() != WebCoreConstants.CARD_TYPE_CREDIT_CARD)
                && (cct.getCardType().getId() != WebCoreConstants.CARD_TYPE_SMART_CARD)) {
                valueCardNamesIdsMap.put(cct.getName().toLowerCase(), cct.getId());
            }
        }
        
        for (CustomerCardType cct : migrated) {
            final String name = cct.getName().toLowerCase();
            final Integer id = valueCardNamesIdsMap.get(name);
            if (id != null) {
                if (forBatchProcessing) {
                    this.ensurePrimaryReference(cct.getId());
                } else {
                    self().ensurePrimaryReferenceAsync(cct.getId());
                }
            } else {
                primary.add(cct);
                valueCardNamesIdsMap.put(name, cct.getId());
                if (forBatchProcessing) {
                    this.ensurePrimaryReference(cct.getId());
                } else {
                    self().ensurePrimaryReferenceAsync(cct.getId());
                }
            }
        }
    }
    
    private CustomerCardTypeService self() {
        return this.serviceHelper.bean("customerCardTypeService", CustomerCardTypeService.class);
    }
    
    private void createCreditCustomerCardTypes() {
        final Map<String, List<String>> binRanges = this.linkCardTypeService.getBinRanges();
        
        if (binRanges != null && !binRanges.entrySet().isEmpty()) {
            final CardType cardType = this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
            
            final List<CustomerCardType> customerCardTypes = binRanges.entrySet().stream()
                    .flatMap(entry -> entry.getValue().stream()
                            .map(pat -> new CustomerCardType(entry.getKey(), pat, CardProcessingConstants.CHECK_DIGIT_ALG_MOD10, cardType)))
                    .collect(Collectors.toList());
            this.creditCustomerCardTypes = customerCardTypes;
        }
    }
    
    private void buildDefaultCreditCardPatterns() {
        final Map<String, Collection<String>> creditCardPatterns = new HashMap<>();
        
        // MasterCard
        creditCardPatterns.put(CardProcessingConstants.NAME_MASTERCARD,
                               Arrays.asList("5[1-5]\\d{14}=\\d{7,20}", "5[1-5]\\d{14}",
                                             "(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9][0-9]\\d{10}=\\d{7,20}",
                                             "(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9][0-9]\\d{10}"));
        
        // VISA
        creditCardPatterns.put(CardProcessingConstants.NAME_VISA, Arrays.asList("4\\d{15}=\\d{7,20}", "4\\d{12}=\\d{7,23}", "4\\d{15}", "4\\d{12}"));
        
        // AMEX
        creditCardPatterns.put(CardProcessingConstants.NAME_AMEX,
                               Arrays.asList("34\\d{13}=\\d{7,21}", "37\\d{13}=\\d{7,21}", "34\\d{13}", "37\\d{13}"));
        
        //Diners
        creditCardPatterns.put(CardProcessingConstants.NAME_DINERS,
                               Arrays.asList("30[0-5]\\d{11}=\\d{7,22}", "309\\d{11}=\\d{7,22}", "36\\d{12}=\\d{7,22}", "38\\d{14,17}=\\d{7,22}",
                                             "39\\d{14,17}=\\d{7,22}", "30[0-5]\\d{11}", "309\\d{11}", "36\\d{12}", "38\\d{14,17}", "39\\d{14,17}"));
        
        //enRoute
        creditCardPatterns.put(CardProcessingConstants.NAME_ENROUTE, Arrays.asList("(2014|2149)\\d{11}=\\d{7,20}", "(2014|2149)\\d{11}"));
        
        //Discover
        creditCardPatterns.put(CardProcessingConstants.NAME_DISCOVER,
                               Arrays.asList("6011\\d{12}=\\d{7,20}", "622\\d{13}=\\d{7,20}", "62[4-6]\\d{13}=\\d{7,20}", "628[2-8]\\d{12}=\\d{7,20}",
                                             "64[4-9]\\d{13}=\\d{7,20}", "65\\d{14}=\\d{7,20}", "6011\\d{12}", "622\\d{13}", "62[4-6]\\d{13}",
                                             "628[2-8]\\d{12}", "64[4-9]\\d{13}", "65\\d{14}"));
        
        // JCB
        creditCardPatterns
                .put(CardProcessingConstants.NAME_JCB,
                     Arrays.asList("(?:2131|1800|35\\d{3})\\d{11}=\\d{7,20}", "(?:2131|1800|35\\d{3})\\d{11}", "30(8[8-9]|9[0-4])\\d{12}=\\d{7,20}",
                                   "30(8[8-9]|9[0-4])\\d{12}", "3(09[6-9]|10[0-2])\\d{12}=\\d{7,20}", "3(09[6-9]|10[0-2])\\d{12}",
                                   "31(1[2-9]|20)\\d{12}=\\d{7,20}", "31(1[2-9]|20)\\d{12}", "315[8-9]\\d{12}=\\d{7,20}", "315[8-9]\\d{12}",
                                   "33(3[7-9]|4[0-9])\\d{12}=\\d{7,20}", "33(3[7-9]|4[0-9])\\d{12}"));
        
        // UnionPay
        creditCardPatterns.put(CardProcessingConstants.NAME_UNION_PAY,
                               Arrays.asList("62\\d{14}=\\d{7,20}", "62\\d{14}", "81([0-2][0-9]|3[0-1])\\d{12}=\\d{7,20}",
                                             "81([0-2][0-9]|3[0-1])\\d{12}", "81(3[2-9]|[4-5][0-9]|6[0-3])\\d{12}=\\d{7,20}",
                                             "81(3[2-9]|[4-5][0-9]|6[0-3])\\d{12}", "81(6[4-9]|[7][0-1])\\d{12}=\\d{7,20}"));
        
        final List<CustomerCardType> customerCardTypes = new ArrayList<CustomerCardType>();
        final CardType cardType = this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
        
        creditCardPatterns.forEach((name, pats) -> pats
                .forEach(pat -> customerCardTypes.add(new CustomerCardType(name, pat, CardProcessingConstants.CHECK_DIGIT_ALG_MOD10, cardType))));
        
        this.defaultCreditCustomerCardTypes = customerCardTypes;
    }
    
    private class ValueCardNameFilter implements FilterDTOTransformer.Filter {
        private Map<String, Integer> namesIdsMap = new HashMap<String, Integer>();
        private Set<Integer> ids = new HashSet<Integer>();
        
        @Override
        public boolean filter(final FilterDTO dto) {
            boolean valid = false;
            if (dto != null) {
                valid = (dto.getExtraData() != null) && (nonValueCardTypes.contains(dto.getExtraData()));
                if (!valid) {
                    final String name = dto.getLabel().toLowerCase();
                    final int thisId = dto.getId().intValue();
                    
                    final Integer id = this.namesIdsMap.get(name);
                    if (id != null) {
                        this.ids.add(thisId);
                    } else {
                        valid = true;
                        
                        this.namesIdsMap.put(name, thisId);
                        this.ids.add(thisId);
                    }
                }
            }
            
            return valid;
        }
        
    }
    
}
