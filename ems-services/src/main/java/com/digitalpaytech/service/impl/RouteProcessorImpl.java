package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.RouteProcessor;

@Service("routeProcessor")
@Transactional(propagation = Propagation.REQUIRED)
public class RouteProcessorImpl implements RouteProcessor {
    private static final String POS_UPDATE_NOTHING = "PointOfSale.updateNothing";
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public void saveRouteAndRoutePOS(final Route route) {
        this.entityDao.save(route);
        final Set<RoutePOS> sets = route.getRoutePOSes();
        for (RoutePOS pos : sets) {
            this.entityDao.save(pos);
        }
        // This is to refresh query cache of every PointOfSale Queries that is using RoutePOS as it sub criteria.
        this.entityDao.getNamedQuery(POS_UPDATE_NOTHING).executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateRouteAndRoutePOS(final Route route) {
        final Route existingRoute = (Route) this.entityDao.get(Route.class, route.getId());
        if (existingRoute == null) {
            throw new IllegalStateException("Could not locate existing Route: " + route.getId());
        }
        
        existingRoute.setRouteType(route.getRouteType());
        existingRoute.setName(route.getName());
        
        existingRoute.setLastModifiedByUserId(route.getLastModifiedByUserId());
        existingRoute.setLastModifiedGmt(route.getLastModifiedGmt());
        
        this.entityDao.deleteAllByNamedQuery("RoutePOS.deleteAllByRouteId", new Object[] { route.getId() });
        
        // This is to refresh query cache of every PointOfSale Queries that is using RoutePOS as it sub criteria.
        this.entityDao.getNamedQuery(POS_UPDATE_NOTHING).executeUpdate();
        
        final Set<RoutePOS> sets = route.getRoutePOSes();
        for (RoutePOS pos : sets) {
            this.entityDao.save(pos);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteRoute(final Integer routeId, final List<Integer> affectedPosIds, final List<Integer> affectedCatIds) {
        final Route route = (Route) this.entityDao.get(Route.class, routeId);
        
        final Set<RoutePOS> routePOSes = route.getRoutePOSes();
        for (RoutePOS routePOS : routePOSes) {
            affectedPosIds.add(routePOS.getPointOfSale().getId());
            this.entityDao.delete(routePOS);
        }
        
        @SuppressWarnings("unchecked")
        final List<CustomerAlertType> customerAlertTypes =
                this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdAndRouteIdAllAlertThresholdTypes",
                                                             new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.ROUTE_ID, },
                                                             new Object[] { route.getCustomer().getId(), route.getId(), });
        for (CustomerAlertType customerAlertType : customerAlertTypes) {
            customerAlertType.setRoute(null);
            customerAlertType.setIsActive(false);
            affectedCatIds.add(customerAlertType.getId());
            this.entityDao.saveOrUpdate(customerAlertType);
            
        }
        
        this.entityDao.delete(route);
    }
    
}
