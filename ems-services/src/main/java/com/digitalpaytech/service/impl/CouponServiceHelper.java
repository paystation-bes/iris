package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@SuppressWarnings({ "PMD.GodClass" })
public final class CouponServiceHelper {
    private static final String PERCENT_DISCOUNT = "percentDiscount";
    private static final String DOLLAR_DISCOUNT_AMOUNT = "dollarDiscountAmount";
    private static final Integer TENTH = 10;
    private static final Integer ONE_HUNDRED = 100;
    
    private static Pattern dollarPatternFront = Pattern.compile(WebCoreConstants.REGEX_DOLLAR_VALUE_FRONT);
    private static Pattern dollarPatternEnd = Pattern.compile(WebCoreConstants.REGEX_DOLLAR_VALUE_END);
    private static Pattern percentagePatternFront = Pattern.compile(WebCoreConstants.REGEX_PERCENTAGE_VALUE_FRONT);
    private static Pattern percentagePatternEnd = Pattern.compile(WebCoreConstants.REGEX_PERCENTAGE_VALUE_END);
    private static Pattern dblPattern = Pattern.compile(WebCoreConstants.REGEX_TWO_DIGIT_FLOAT);
    private static Pattern intPattern = Pattern.compile(WebCoreConstants.REGEX_INTEGER);
    
    private CouponServiceHelper() {
        
    }
    
    public static Criterion nullableAnd(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.and(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    public static Criterion nullableOr(final Criterion mainCondition, final Criterion additionalCondition) {
        Criterion result = null;
        if (mainCondition == null) {
            result = additionalCondition;
        } else if (additionalCondition == null) {
            result = mainCondition;
        } else {
            result = Restrictions.or(mainCondition, additionalCondition);
        }
        
        return result;
    }
    
    public static Criterion createCondition(final String code, final Criterion discount, final String location) {
        Criterion condition = discount;
        if (location != null) {
            condition = nullableAnd(createLocationCondition(location), condition);
        }
        if (code != null) {
            condition = nullableAnd(createCodeCondition(code), condition);
        }
        
        return condition;
    }
    
    private static String generateCouponCodeExpression(final String coupon) {
        final StringBuilder result = new StringBuilder(WebCoreConstants.PERCENT_SIGN);
        
        boolean containSpecialChars = false;
        for (char c : coupon.toCharArray()) {
            switch (c) {
                case '?':
                    result.append('_');
                    containSpecialChars = true;
                    break;
                case '*':
                    result.append(WebCoreConstants.PERCENT_SIGN);
                    containSpecialChars = true;
                    break;
                case '%':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT_PERCENT_SIGN);
                    break;
                case '_':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT_UNDERSCORE);
                    break;
                case '!':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT).append(StandardConstants.STRING_EXCLAMATION_POINT);
                    break;
                default:
                    result.append(c);
                    break;
            }
        }
        
        if (!containSpecialChars) {
            result.append(WebCoreConstants.PERCENT_SIGN);
        }
        
        return containSpecialChars ? result.substring(1) : result.toString();
    }
    
    private static String generateLocationExpression(final String location) {
        final StringBuilder result = new StringBuilder();
        
        result.append(WebCoreConstants.PERCENT_SIGN);
        for (char c : location.toCharArray()) {
            switch (c) {
                case '%':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT_PERCENT_SIGN);
                    break;
                case '_':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT_UNDERSCORE);
                    break;
                case '!':
                    result.append(StandardConstants.STRING_EXCLAMATION_POINT).append(StandardConstants.STRING_EXCLAMATION_POINT);
                    break;
                default:
                    result.append(c);
                    break;
            }
        }
        
        result.append(WebCoreConstants.PERCENT_SIGN);
        
        return result.toString();
    }
    
    @SuppressWarnings("PMD.NullAssignment")
    public static Criterion createCodeCondition(final String code) {
        return (code == null) ? null : Restrictions.ilike("coupon", generateCouponCodeExpression(code));
    }
    
    @SuppressWarnings("PMD.NullAssignment")
    public static Criterion createLocationCondition(final String location) {
        return (location == null) ? null : Restrictions.ilike("location.name", generateLocationExpression(location));
    }
    
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:emptycatchblock", "PMD.AvoidUsingShortType", "PMD.EmptyCatchBlock" })
    public static Criterion createDiscountCondition(final String discountStr) {
        Criterion condition = null;
        if (percentagePatternFront.matcher(discountStr).matches()) {
            condition = Restrictions.eq(PERCENT_DISCOUNT, Short.parseShort(discountStr.substring(1).trim(), TENTH));
        } else if (percentagePatternEnd.matcher(discountStr).matches()) {
            condition = Restrictions.eq(PERCENT_DISCOUNT, Short.parseShort(discountStr.substring(0, discountStr.length() - 1).trim(), TENTH));
        } else if (dollarPatternFront.matcher(discountStr).matches()) {
            condition = Restrictions.eq(DOLLAR_DISCOUNT_AMOUNT, WebCoreUtil.convertToBase100IntValue(discountStr.substring(1).trim()));
        } else if (dollarPatternEnd.matcher(discountStr).matches()) {
            condition = Restrictions.eq(DOLLAR_DISCOUNT_AMOUNT,
                                        WebCoreUtil.convertToBase100IntValue(discountStr.substring(0, discountStr.length() - 1).trim()));
        } else if (intPattern.matcher(discountStr).matches()) {
            try {
                final int parsedInt = Integer.parseInt(discountStr);
                if (parsedInt > 0) {
                    if (parsedInt <= ONE_HUNDRED) {
                        condition = Restrictions.or(Restrictions.eq(PERCENT_DISCOUNT, (short) parsedInt),
                                                    Restrictions.eq(DOLLAR_DISCOUNT_AMOUNT, WebCoreUtil.convertToBase100IntValue(discountStr)));
                    } else if (parsedInt > ONE_HUNDRED) {
                        condition = Restrictions.eq(DOLLAR_DISCOUNT_AMOUNT, WebCoreUtil.convertToBase100IntValue(discountStr));
                        
                    }
                }
            } catch (NumberFormatException e) {
                
            }
        } else if (dblPattern.matcher(discountStr).matches()) {
            try {
                final Double parsedDouble = Double.parseDouble(discountStr);
                if (parsedDouble > 0) {
                    condition = Restrictions.eq(DOLLAR_DISCOUNT_AMOUNT, WebCoreUtil.convertToBase100IntValue(discountStr));
                }
            } catch (NumberFormatException e) {
                
            }
        }
        
        return condition;
    }
    
    public static Criterion createCodeOrDiscountCondition(final String codeOrDiscount1, final String codeOrDiscount2, final String location) {
        Criterion condition = null;
        if (codeOrDiscount1 == null || codeOrDiscount2 == null) {
            Criterion codeOrDiscountCondition = null;
            
            String code = (codeOrDiscount1 == null) ? codeOrDiscount2 : codeOrDiscount1;
            if (code == null) {
                condition = nullableOr(condition, createLocationCondition(location));
            } else {
                code = code.trim();
                codeOrDiscountCondition = nullableOr(createDiscountCondition(code), createCodeCondition(code));
                condition = nullableOr(condition, nullableAnd(codeOrDiscountCondition, createLocationCondition(location)));
            }
        } else {
            String code = codeOrDiscount1;
            Criterion discount = createDiscountCondition(codeOrDiscount2);
            if (discount != null) {
                condition = nullableOr(condition, createCondition(code, discount, location));
            }
            
            code = codeOrDiscount2;
            discount = createDiscountCondition(codeOrDiscount1);
            if (discount != null) {
                condition = nullableOr(condition, createCondition(code, discount, location));
            }
        }
        
        return condition;
    }
    
    @SuppressWarnings("checkstyle:magicnumber")
    public static List<SubSearchCriteria> splitAndCombine(final String filterValue) {
        final List<SubSearchCriteria> result = new ArrayList<SubSearchCriteria>();
        
        final String trimmedFilterVal = filterValue.trim();
        
        // It is always possible that everything are location name
        result.add(new SubSearchCriteria(trimmedFilterVal, (String) null));
        
        final Matcher m = Pattern.compile("\\s+").matcher(trimmedFilterVal);
        if (m.find()) {
            int idxesIdx = 0;
            final int[][] idxes = new int[4][2];
            
            idxes[idxesIdx][0] = 0;
            do {
                idxes[idxesIdx][1] = m.start();
                if (idxesIdx < 3) {
                    ++idxesIdx;
                } else {
                    idxes[2][0] = idxes[3][0];
                    idxes[2][1] = idxes[3][1];
                }
                
                idxes[idxesIdx][0] = m.end();
            } while (m.find());
            
            idxes[idxesIdx][1] = trimmedFilterVal.length();
            
            final String first = trimmedFilterVal.substring(idxes[0][0], idxes[0][1]);
            final String second = trimmedFilterVal.substring(idxes[1][0], idxes[1][1]);
            if (idxesIdx <= 1) {
                result.add(new SubSearchCriteria(first, second));
                result.add(new SubSearchCriteria(second, first));
            } else if (idxesIdx <= 2) {
                final String third = trimmedFilterVal.substring(idxes[2][0], idxes[2][1]);
                
                result.add(new SubSearchCriteria(first, second, third));
                result.add(new SubSearchCriteria(second, first, third));
                result.add(new SubSearchCriteria(third, second, first));
            } else {
                final String third = trimmedFilterVal.substring(idxes[2][0], idxes[2][1]);
                final String fourth = trimmedFilterVal.substring(idxes[3][0], idxes[3][1]);
                
                result.add(new SubSearchCriteria(trimmedFilterVal.substring(idxes[1][0]), first));
                result.add(new SubSearchCriteria(trimmedFilterVal.substring(idxes[1][1]).trim(), first, second));
                result.add(new SubSearchCriteria(trimmedFilterVal.substring(idxes[1][0], idxes[2][1]), first, fourth));
                result.add(new SubSearchCriteria(trimmedFilterVal.substring(idxes[0][0], idxes[2][0]).trim(), third, fourth));
                result.add(new SubSearchCriteria(trimmedFilterVal.substring(idxes[0][0], idxes[3][0]).trim(), fourth));
            }
        } else {
            result.add(new SubSearchCriteria((String) null, trimmedFilterVal));
        }
        
        return result;
    }
    
    public static class SubSearchCriteria {
        private String location;
        private String codeOrDiscount1;
        private String codeOrDiscount2;
        
        public SubSearchCriteria(final String location, final String codeOrDiscount) {
            this(location, codeOrDiscount, (String) null);
        }
        
        public SubSearchCriteria(final String location, final String codeOrDiscount1, final String codeOrDiscount2) {
            this.location = location;
            this.codeOrDiscount1 = codeOrDiscount1;
            this.codeOrDiscount2 = codeOrDiscount2;
        }
        
        public final void setLocation(final String location) {
            this.location = location;
        }
        
        public final void setCodeOrDiscount1(final String codeOrDiscount1) {
            this.codeOrDiscount1 = codeOrDiscount1;
        }
        
        public final void setCodeOrDiscount2(final String codeOrDiscount2) {
            this.codeOrDiscount2 = codeOrDiscount2;
        }
        
        public final String getLocation() {
            return this.location;
        }
        
        public final String getCodeOrDiscount1() {
            return this.codeOrDiscount1;
        }
        
        public final String getCodeOrDiscount2() {
            return this.codeOrDiscount2;
        }
        
        @Override
        
        public final String toString() {
            final StringBuilder result = new StringBuilder();
            result.append("{ ")
            .append((this.codeOrDiscount1 == null) ? "" : this.codeOrDiscount1)
            .append(StandardConstants.STRING_COMMA_SPACE)
            .append((this.codeOrDiscount2 == null) ? "" : this.codeOrDiscount2)
            .append(StandardConstants.STRING_COMMA_SPACE)
            .append(this.location)
            .append(" }");
            
            return result.toString();
        }
    }
}
