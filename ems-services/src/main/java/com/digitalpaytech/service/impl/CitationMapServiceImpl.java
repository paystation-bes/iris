package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CitationMap;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CitationMapService;

@Service("CitationMapService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CitationMapServiceImpl implements CitationMapService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CitationMap> findByDateRange(final Integer customerId, final Date startDate, final Date endDate) {
        return this.entityDao
                .findByNamedQueryAndNamedParam("CitationMap.findByDateRange"
                                               , new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.START_DATE, HibernateConstants.END_DATE, }
                                               , new Object[] { customerId, startDate, endDate, });
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CitationMap> findByDateRangeAndCitationType(final Integer customerId, final Date startDate, final Date endDate, final List<Integer> citationTypeIdList) {
        return this.entityDao
                .findByNamedQueryAndNamedParam("CitationMap.findByDateRangeAndCitationType"
                                               , new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.START_DATE, HibernateConstants.END_DATE, "citationTypeIdList", }
                                               , new Object[] { customerId, startDate, endDate, citationTypeIdList, });
    }
    
}
