package com.digitalpaytech.service.impl;

import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.dao.EntityDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("webServiceEndPointService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebServiceEndPointServiceImpl implements WebServiceEndPointService {
    @Autowired
    private EntityDao entityDao;
    
    /**
     * Retrieves web service endpoints.
     */
    @Override
    public final List<WebServiceEndPoint> findWebServiceEndPointsByCustomer(final int customerId) {
        final List<WebServiceEndPoint> result = this.entityDao.findByNamedQueryAndNamedParam("WebServiceEndPoint.findWebServiceEndPointByCustomerId",
                                                                                             HibernateConstants.CUSTOMER_ID, customerId, true);
        return result;
    }
    
    @Override
    public final List<WebServiceEndPoint> findPrivateWebServiceEndPointsByCustomer(final int customerId) {
        final List<WebServiceEndPoint> result = this.entityDao
                .findByNamedQueryAndNamedParam("WebServiceEndPoint.findPrivateWebServiceEndPointByCustomerId", HibernateConstants.CUSTOMER_ID,
                                               customerId, true);
        return result;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final List<WebServiceEndPoint> findWebServiceEndPointByCustomerAndType(final int customerId, final byte typeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("WebServiceEndPoint.findWebServiceEndPointByCustomerIdAndTypeId", new String[] {
            HibernateConstants.CUSTOMER_ID, HibernateConstants.TYPE_ID, }, new Object[] { customerId, typeId, }, false);
    }
    
    @Override
    public final WebServiceEndPoint findWebServiceEndPointByToken(final String token) {
        return (WebServiceEndPoint) this.entityDao.findUniqueByNamedQueryAndNamedParam("WebServiceEndPoint.findWebServiceEndPointByToken", "token",
                                                                                       token, false);
    }
    
    @Override
    public final WebServiceEndPoint findWebServiceEndById(final int webServiceEndPointId) {
        return (WebServiceEndPoint) this.entityDao.get(WebServiceEndPoint.class, webServiceEndPointId);
    }
    
    @Override
    public final void saveOrUpdate(final WebServiceEndPoint webServiceEndPoint) {
        this.entityDao.saveOrUpdate(webServiceEndPoint);
    }
    
    @Override
    public final void delete(final WebServiceEndPoint webServiceEndPoint) {
        webServiceEndPoint.setIsDeleted(true);
        this.entityDao.update(webServiceEndPoint);
        
    }
    
}
