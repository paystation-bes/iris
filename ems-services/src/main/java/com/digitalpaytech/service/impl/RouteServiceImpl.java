package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.RouteProcessor;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;

@Service("routeService")
@Transactional(propagation = Propagation.REQUIRED)
public class RouteServiceImpl implements RouteService {
    
    private static final String ROUTE_POS_BY_ROUTE_ID = "RoutePOS.findRoutePOSByRouteId";
    private static final String SQL_APPEND_JOIN_ROUTE_AND_ROUTE_POS = "SELECT r.* from Route r INNER JOIN RoutePOS rp on r.Id = rp.RouteId ";
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    @Autowired
    private RouteProcessor routeProcessor;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Route> findRoutesByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByCustomerId", HibernateConstants.CUSTOMER_ID, customerId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Route> findRoutesByCustomerIdAndName(final int customerId, final String name) {
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByCustomerIdAndName", new String[] { HibernateConstants.CUSTOMER_ID,
            HibernateConstants.NAME, }, new Object[] { customerId, name, }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Route> findRoutesByCustomerIdAndNameUpperCase(final int customerId, final String name) {
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByCustomerIdAndNameUpperCase", new String[] {
            HibernateConstants.CUSTOMER_ID, HibernateConstants.NAME, }, new Object[] { customerId, name, }, true);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Route findRouteById(final Integer id) {
        return (Route) this.entityDao.get(Route.class, id);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Route findRouteByIdAndEvict(final Integer id) {
        final Route route = (Route) this.entityDao.get(Route.class, id);
        this.entityDao.evict(route);
        return route;
    }
    
    public final void saveRouteAndRoutePOS(final Route route) {
        this.routeProcessor.saveRouteAndRoutePOS(route);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void updateRouteAndRoutePOS(final Route route) {
        
        final List<Integer> oldRoutePosList = this.findPointOfSaleIdsByRouteId(route.getId(), false);
        
        final List<Integer> addedPosList = new ArrayList<Integer>();
        final List<Integer> removedPosList = new ArrayList<Integer>();
        
        for (RoutePOS routePos : route.getRoutePOSes()) {
            if (!oldRoutePosList.contains(routePos.getPointOfSale().getId())) {
                addedPosList.add(routePos.getPointOfSale().getId());
            } else {
                oldRoutePosList.remove(routePos.getPointOfSale().getId());
            }
        }
        
        //oldRoutePosList only contains deleted POS's
        for (Integer oldRoutePos : oldRoutePosList) {
            removedPosList.add(oldRoutePos);
        }
        
        this.routeProcessor.updateRouteAndRoutePOS(route);
        
        /* retrieve CustomerAlertType based on routeId */
        final List<RoutePOS> allRoutesPos = this.entityDao.findByNamedQueryAndNamedParam(ROUTE_POS_BY_ROUTE_ID, HibernateConstants.ROUTE_ID,
                                                                                         route.getId());
        final List<Integer> allPosIds = new ArrayList<Integer>();
        for (RoutePOS r : allRoutesPos) {
            allPosIds.add(r.getPointOfSale().getId());
        }
        
        final List<CustomerAlertType> customerAlertTypes = this.entityDao
                .findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdAndRouteId", new String[] {
                    HibernateConstants.CUSTOMER_ID, HibernateConstants.ROUTE_ID, }, new Object[] { route.getCustomer().getId(), route.getId(), });
        final List<Integer> customerAlertTypeIds = new ArrayList<Integer>();
        
        for (CustomerAlertType customerAlertType : customerAlertTypes) {
            customerAlertTypeIds.add(customerAlertType.getId());
        }
        
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(addedPosList, removedPosList, null, customerAlertTypeIds);
        
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Paystation> findPayStationsByRouteId(final int routeId) {
        final List<RoutePOS> routePOSList = this.entityDao.findByNamedQueryAndNamedParam(ROUTE_POS_BY_ROUTE_ID, HibernateConstants.ROUTE_ID, routeId,
                                                                                         true);
        final List<Paystation> payStations = new ArrayList<Paystation>();
        for (RoutePOS routePOS : routePOSList) {
            payStations.add(routePOS.getPointOfSale().getPaystation());
        }
        return payStations;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesByRouteId(final int routeId, final boolean includeArchived) {
        List<PointOfSale> pointOfSaleLIst = null;
        if (includeArchived) {
            pointOfSaleLIst = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByRouteIdIncludingArchived",
                                                                           HibernateConstants.ROUTE_ID, routeId, true);
        } else {
            pointOfSaleLIst = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByRouteId", HibernateConstants.ROUTE_ID,
                                                                           routeId, true);
        }
        return pointOfSaleLIst;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Integer> findPointOfSaleIdsByRouteId(final int routeId, final boolean includeArchived) {
        List<Integer> pointOfSaleIdsList = null;
        if (includeArchived) {
            pointOfSaleIdsList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleIdsByRouteIdIncludingArchived",
                                                                              HibernateConstants.ROUTE_ID, routeId, true);
        } else {
            pointOfSaleIdsList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleIdsByRouteId", HibernateConstants.ROUTE_ID,
                                                                              routeId, true);
        }
        return pointOfSaleIdsList;
    }
    
    //
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<RoutePOS> findRoutePOSesByPointOfSaleId(final Integer pointOfSaleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("RoutePOS.findRoutePOSByPosId", HibernateConstants.POINTOFSALE_ID, pointOfSaleId, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void deleteRoute(final Integer routeId) {
        final List<Integer> routePosIds = new ArrayList<Integer>();
        final List<Integer> customerAlertTypeIds = new ArrayList<Integer>();
        
        this.routeProcessor.deleteRoute(routeId, routePosIds, customerAlertTypeIds);
        
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, new ArrayList<Integer>(routePosIds), null, customerAlertTypeIds);
    }
    
    @Override
    public final RoutePOS findRoutePOSByIdAndPointOfSaleId(final Integer routeId, final Integer pointOfSaleId) {
        return (RoutePOS) this.entityDao.findUniqueByNamedQueryAndNamedParam("RoutePOS.findRoutePOSByIdAndPointOfSaleId", new String[] {
            HibernateConstants.ROUTE_ID, HibernateConstants.POINTOFSALE_ID, }, new Object[] { routeId, pointOfSaleId, }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Route> findRoutesByPointOfSaleId(final int pointOfSaleId) {
        
        return this.entityDao
                .findByNamedQueryAndNamedParam("Route.findRoutesByPointOfSaleId", HibernateConstants.POINTOFSALE_ID, pointOfSaleId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Route> findRoutesByPointOfSaleIdAndRouteTypeId(final int pointOfSaleId, final int routeTypeId) {
        
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByPointOfSaleIdAndRouteTypeId", new String[] {
            HibernateConstants.POINTOFSALE_ID, HibernateConstants.ROUTE_TYPE_ID, }, new Object[] { pointOfSaleId, routeTypeId, }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final Collection<Route> findRoutesByCustomerIdAndRouteTypeId(final int customerId, final int routeTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByCustomerIdAndRouteTypeIdOrderByNameAsc", new String[] {
            HibernateConstants.CUSTOMER_ID, "filterType", }, new Object[] { customerId, routeTypeId, }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<RoutePOS> findRoutePOSesByPointOfSaleIdAndRouteTypeId(final Integer pointOfSaleId, final Integer routeTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("RoutePOS.findRoutePOSByPosIdAndRouteTypeId", new String[] {
            HibernateConstants.POINTOFSALE_ID, HibernateConstants.ROUTE_TYPE_ID, }, new Object[] { pointOfSaleId, routeTypeId, }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Route> findRoutesWithPointOfSalesByCustomerId(final Integer customerId) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(SQL_APPEND_JOIN_ROUTE_AND_ROUTE_POS);
        bdr.append("WHERE r.CustomerId = :customerId GROUP BY r.Id");
        final SQLQuery query = this.entityDao.createSQLQuery(bdr.toString());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        query.addEntity(Route.class);
        query.setCacheable(true);
        final List<Route> routeList = query.list();
        
        return routeList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Route> findRoutesWithPointOfSalesByCustomerIdAndRouteType(final Integer customerId, final Integer routeTypeId) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(SQL_APPEND_JOIN_ROUTE_AND_ROUTE_POS);
        bdr.append("WHERE r.CustomerId = :customerId AND r.RouteTypeId = :routeTypeId GROUP BY r.Id ORDER BY r.Name+0<>0 DESC, r.Name+0, r.Name");
        final SQLQuery query = this.entityDao.createSQLQuery(bdr.toString());
        query.setParameter(HibernateConstants.CUSTOMER_ID, customerId);
        query.setParameter(HibernateConstants.ROUTE_TYPE_ID, routeTypeId);
        query.addEntity(Route.class);
        query.setCacheable(true);
        final List<Route> routeList = query.list();
        
        return routeList;
    }
}
