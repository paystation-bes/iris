package com.digitalpaytech.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.WebServiceCallLog;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.service.WebServiceCallLogService;
import com.digitalpaytech.util.DateUtil;

@Service("webServiceCallLogService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebServiceCallLogServiceImpl implements WebServiceCallLogService {
    
    private static final Logger logger = Logger.getLogger(WebServiceCallLogServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public boolean logWsCall(WebServiceEndPoint wsEndPoint, int customerId, String timeZone) {
        try {
            Date currDateLocal = DateUtil.createCurrentWebServiceCallLogDate(timeZone);
            WebServiceCallLog wsCallLog = (WebServiceCallLog) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("WebServiceCallLog.findWSCallLogByWSEndPointIdAndDateLocal", new String[] { "wsEndPointId",
                            "dateLocal" }, new Object[] { wsEndPoint.getId(), currDateLocal }, false);
            if (wsCallLog == null) {
                wsCallLog = new WebServiceCallLog(wsEndPoint, currDateLocal, 1);
                this.entityDao.save(wsCallLog);
            } else {
                wsCallLog.setTotalCalls(wsCallLog.getTotalCalls() + 1);
                this.entityDao.update(wsCallLog);
            }
        } catch (Exception ex) {
            logger.error("Can't log WebServic Call", ex);
            return false;
        }
        return true;
    }
    
}
