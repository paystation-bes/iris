package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.RestProperty;
import com.digitalpaytech.service.RestPropertyService;

@Service("restPropertyService")
@Transactional(propagation = Propagation.REQUIRED)
public class RestPropertyServiceImpl implements RestPropertyService {
    private final static Logger logger = Logger.getLogger(RestPropertyServiceImpl.class);
    private final static String OSType_ = System.getProperty("os.name");
    private final static String WINDOWS_STRING = "Windows";
    private Properties properties = null;
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    @Autowired
    private EntityDao entityDao;
    
    public void setOpenSessionDao(OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    
    @Override
    public String getPropertyValue(String propertyName) {
        return (properties.getProperty(propertyName));
    }
    
    @Override
    public String getPropertyValue(String propertyName, String defaultValue) {
        return getPropertyValue(propertyName, defaultValue, false);
    }
    
    @Override
    public String getPropertyValue(String propertyName, String defaultValue, boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        
        if (value == null)
            return defaultValue;
        return (value);
    }
    
    @Override
    public String getPropertyValue(String propertyName, boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        return (value);
    }
    
    @Override
    public int getPropertyValueAsInt(String propertyName, int defaultValue) throws Exception {
        return getPropertyValueAsInt(propertyName, defaultValue, false);
    }
    
    @Override
    public int getPropertyValueAsInt(String propertyName, int defaultValue, boolean forceGet) throws Exception {
        String value = null;
        if (!forceGet) {
            value = properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        
        if (value == null) {
            return (defaultValue);
        }
        
        try {
            return (Integer.parseInt(value));
        } catch (Exception e) {
            throw new Exception("Property (" + propertyName + ", " + value + ") is not an integer");
        }
    }
    
    @PostConstruct
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public void findPropertiesFile() {
        properties = new Properties();
        properties.clear();
        try {
            Iterator iterator = openSessionDao.findByNamedQuery("RestProperty.findAll").iterator();
            
            while (iterator.hasNext()) {
                RestProperty property = (RestProperty) iterator.next();
                properties.put(property.getName(), property.getValue());
            }
        } catch (Exception e) {
            logger.error("Unable to load properties from the database: ", e);
        }
        
        printProperties();
    }
    
    private void printProperties() {
        if (!logger.isDebugEnabled()) {
            return;
        }
        
        properties.list(System.out);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    private String findByName(String name) {
        RestProperty restProperty = (RestProperty) openSessionDao.findUniqueByNamedQueryAndNamedParam("RestProperty.findByName", "name", name);
        if (restProperty == null)
            return null;
        else
            return restProperty.getValue();
    }
    
    @Override
    public void loadPropertiesFile() {
        properties = new Properties();
        properties.clear();
        try {
            Collection<RestProperty> restPropertyList = entityDao.findByNamedQuery("RestProperty.findAll");
            
            for (RestProperty property : restPropertyList) {
                properties.put(property.getName(), property.getValue());
            }
        } catch (Exception e) {
            logger.error("Unable to load properties from the database: ", e);
        }
        
        printProperties();
    }
    
    @Override
    public boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue) {
        return getPropertyValueAsBoolean(propertyName, defaultValue, false);
    }
    
    @Override
    public boolean getPropertyValueAsBoolean(String propertyName, boolean defaultValue, boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        
        if (value == null) {
            return (defaultValue);
        }
        
        return (value.equals("1") ? true : false);
    }
}
