package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEmail;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationStatusType;
import com.digitalpaytech.dto.CustomerMigrationSearchCriteria;
import com.digitalpaytech.dto.customermigration.CustomerMigrationData;
import com.digitalpaytech.dto.customermigration.RecentBoardedMigratedCustomerList;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
@Service("customerMigrationService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerMigrationServiceImpl implements CustomerMigrationService {
    private static final String SQL_START_TIME = "'2014-05-01'";
    
    private static final int CUSTOMER_ID = 0;
    private static final int CUSTOMER_NAME = 1;
    private static final int DATE = 2;
    private static final int VALIDATION_STATUS = 3;
    private static final int PAYSTATION_COUNT = 4;
    
    private static final String STATUS_REQUESTED = "Requested";
    private static final String STATUS_SCHEDULED = "Scheduled";
    private static final String STATUS_RUNNING = "Running";
    private static final String STATUS_MIGRATED = "Migrated";
    private static final String STATUS_FAILED = "Failed";
    private static final String STATUS_BOARDED = "Boarded";
    private static final String STATUS_NOT_BOARDED = "Not Boarded Yet";
    
    private static final int BOARDED = 1;
    private static final int REQUESTED = 4;
    private static final int SCHEDULED = 5;
    private static final int NOT_BOARDED = 6;
    private static final int RUNNING = 7;
    private static final int MIGRATED = 8;
    private static final int FAILED = 9;
    
    private static final String SQL_CUSTOMER_STATUS = "SELECT CustomerMigrationStatusTypeId, COUNT(Id)"
                                                      + " FROM CustomerMigration GROUP BY CustomerMigrationStatusTypeId";
    
    private static final String SQL_PAYSTATION_STATUS = "SELECT cm.CustomerMigrationStatusTypeId, COUNT(p.Id)"
                                                        + " FROM CustomerMigration cm INNER JOIN PointOfSale p ON p.CustomerId = cm.CustomerId"
                                                        + " INNER JOIN POSStatus ps ON ps.PointOfSaleId = p.Id"
                                                        + " WHERE p.ProvisionedGMT < UTC_TIMESTAMP() AND ps.IsDecommissioned = 0"
                                                        + " GROUP BY CustomerMigrationStatusTypeId";
    
    private static final String SQL_CUSTOMER_DATA = "SELECT t1.DateTime, t1.Migrated, t2.Boarded, t3.EMS6"
                                                    + " FROM (SELECT t.Id, t.DateTime, COUNT(c.id) AS 'Migrated'"
                                                    + " FROM Time t LEFT JOIN CustomerMigration c ON (c.MigrationEndGMT < t.DateTime)"
                                                    + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0 AND t.Date > " + SQL_START_TIME
                                                    + " AND t.Date < UTC_TIMESTAMP() GROUP BY t.Id order by t.Id) t1"
                                                    + " LEFT JOIN  (SELECT t.Id, t.DateTime, COUNT(c2.id) AS 'Boarded'" + " FROM Time t "
                                                    + " LEFT JOIN CustomerMigration c2 ON (c2.CustomerBoardedGMT < t.DateTime)"
                                                    + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0 AND t.Date > " + SQL_START_TIME
                                                    + " AND t.Date < UTC_TIMESTAMP() GROUP BY t.Id ORDER BY t.Id) t2 ON (t1.Id = t2.Id) "
                                                    + " LEFT JOIN  (Select t.Id, t.DateTime, COUNT(c2.id) AS 'EMS6' FROM Time t"
                                                    + " LEFT JOIN CustomerMigration c2 ON (c2.CreatedGMT < t.DateTime)"
                                                    + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0 AND t.Date > " + SQL_START_TIME
                                                    + " AND t.Date < UTC_TIMESTAMP() " + "GROUP BY t.Id ORDER BY t.Id) t3 ON (t1.Id = t3.Id)";
    
    private static final String SQL_PAYSTATION_DATA = "SELECT t1.DateTime, t1.Migrated, t2.Boarded, t3.EMS6"
                                                      + " FROM (SELECT t.id, t.Datetime, IFNULL(COUNT(pos.id), 0) AS 'Migrated'"
                                                      + " FROM Time t LEFT JOIN CustomerMigration c ON (c.MigrationEndGMT < t.DateTime)"
                                                      + " LEFT JOIN PointOfSale pos ON (pos.CustomerId = c.CustomerId)"
                                                      + " LEFT JOIN POSStatus posstatus ON (pos.Id = posstatus.PointOfSaleId"
                                                      + " AND posstatus.IsDecommissioned = 0)"
                                                      + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0  AND t.Date > " + SQL_START_TIME
                                                      + " AND t.Date < UTC_TIMESTAMP()" + " GROUP BY t.Id order by t.Id) t1 "
                                                      + " LEFT JOIN (Select t.Id, t.DateTime, IFNULL(COUNT(pos.id), 0) AS 'Boarded'"
                                                      + " FROM Time t LEFT JOIN CustomerMigration c2 ON (c2.CustomerBoardedGMT < t.DateTime) "
                                                      + " LEFT JOIN PointOfSale pos ON (pos.CustomerId = c2.CustomerId)"
                                                      + " LEFT JOIN POSStatus posstatus ON (pos.Id = posstatus.PointOfSaleId"
                                                      + " AND posstatus.IsDecommissioned = 0)"
                                                      + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0 AND t.Date > " + SQL_START_TIME
                                                      + " AND t.Date < UTC_TIMESTAMP()" + " GROUP BY t.Id order by t.Id) t2 ON (t1.id = t2.id)"
                                                      + " LEFT JOIN (Select t.Id, t.DateTime, IFNULL(COUNT(pos.id), 0) AS 'EMS6'"
                                                      + " FROM Time t LEFT JOIN CustomerMigration c3 ON (c3.CreatedGMT < t.DateTime)"
                                                      + " LEFT JOIN PointOfSale pos ON (pos.CustomerId = c3.CustomerId)"
                                                      + " LEFT JOIN POSStatus posstatus ON (pos.Id = posstatus.PointOfSaleId"
                                                      + " AND posstatus.IsDecommissioned = 0)"
                                                      + " WHERE t.DayOfWeek = 1 AND t.QuarterOfDay = 0  AND t.Date > " + SQL_START_TIME
                                                      + " AND t.Date < UTC_TIMESTAMP()" + " GROUP BY t.Id order by t.Id) t3 ON (t1.id = t3.id)";
    
    private static final String SQL_BOARDED_SELECT = "SELECT c.Id AS CustomerId, c.Name AS CustomerName, cm.CustomerBoardedGMT AS CustomerDate,";
    private static final String SQL_MIGRATED_SELECT = "SELECT c.Id AS CustomerId, c.Name AS CustomerName, cm.MigrationEndGMT AS CustomerDate,";
    
    private static final String SQL_AVTIVITY_REMAINING = " cm.CustomerMigrationValidationStatusTypeId AS ValidationStatus, COUNT(pos.Id) AS PayStationCount"
                                                         + " FROM CustomerMigration cm"
                                                         + " LEFT JOIN Customer c ON cm.CustomerId =  c.Id"
                                                         + " LEFT JOIN PointOfSale pos ON pos.CustomerId =  cm.CustomerId"
                                                         + " LEFT JOIN POSStatus posstatus ON posstatus.PointOfSaleId = pos.Id"
                                                         + " AND posstatus.IsDecommissioned = 0";
    
    private static final String SQL_BOARDED_WHERE = " WHERE cm.CustomerBoardedGMT IS NOT NULL"
                                                    + " AND cm.CustomerBoardedGMT BETWEEN UTC_TIMESTAMP() - INTERVAL 30 DAY AND UTC_TIMESTAMP()";
    private static final String SQL_MIGRATED_WHERE = " WHERE cm.MigrationEndGMT IS NOT NULL"
                                                     + " AND cm.MigrationEndGMT BETWEEN UTC_TIMESTAMP() - INTERVAL 30 DAY AND UTC_TIMESTAMP()";
    
    private static final String SQL_GROUP_BY_CUSTOMER_ID = " GROUP BY cm.CustomerId";
    
    private static final String SQL_RECENT_CUSTOMERS_ORDER_BY_CUSTOMER_NAME = " ORDER BY CustomerName";
    private static final String SQL_RECENT_CUSTOMERS_ORDER_BY_CUSTOMER_DATE = " ORDER BY CustomerDate";
    private static final String SQL_RECENT_CUSTOMERS_ORDER_BY_PAYSTATION_COUNT = " ORDER BY PayStationCount";
    private static final String SQL_RECENT_CUSTOMERS_LIMIT = " LIMIT ";
    
    private static final TimeZone SYSTEM_ADMIN_TIMEZONE = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
    
    private static Map<Byte, String> statusMappingText;
    private static Map<Byte, Integer> statusMappingInt;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CustomerService customerService;
    
    @PostConstruct
    private void createStatusMapping() {
        statusMappingText = new HashMap<Byte, String>();
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED, STATUS_REQUESTED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED, STATUS_SCHEDULED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED, STATUS_RUNNING);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED, STATUS_RUNNING);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION, STATUS_RUNNING);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED, STATUS_RUNNING);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED, STATUS_MIGRATED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED, STATUS_MIGRATED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED, STATUS_MIGRATED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED, STATUS_FAILED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED, STATUS_FAILED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED, STATUS_BOARDED);
        statusMappingText.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED, STATUS_NOT_BOARDED);
        
        statusMappingInt = new HashMap<Byte, Integer>();
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED, REQUESTED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED, SCHEDULED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED, RUNNING);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED, RUNNING);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION, RUNNING);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED, RUNNING);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED, MIGRATED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED, MIGRATED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED, MIGRATED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED, FAILED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED, FAILED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED, BOARDED);
        statusMappingInt.put(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED, NOT_BOARDED);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final Serializable updateCustomerMigration(final CustomerMigration customerMigration, final List<String> emailAddressList) {
        final Date currentTime = DateUtil.getCurrentGmtDate();
        this.entityDao.update(customerMigration);
        for (String emailAddress : emailAddressList) {
            CustomerEmail customerEmail = (CustomerEmail) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("CustomerEmail.findCustomerEmailByCustomerIdAndEmail",
                                                         new String[] { "customerId", "email" }, new Object[] {
                                                             customerMigration.getCustomer().getId(), emailAddress }, true);
            //TODO 
            if (customerEmail == null) {
                customerEmail = new CustomerEmail();
                customerEmail.setCustomer(customerMigration.getCustomer());
                customerEmail.setEmail(emailAddress);
                customerEmail.setIsDeleted(false);
                customerEmail.setLastModifiedByUserId(customerMigration.getUserAccount().getId());
                customerEmail.setLastModifiedGmt(currentTime);
                this.entityDao.save(customerEmail);
            }
            
            CustomerMigrationEmail customerMigrationEmail = (CustomerMigrationEmail) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("CustomerMigrationEmail.findCustomerMigrationEmailByCustomerIdAndEmailId", new String[] {
                        "customerEmailId", "customerMigrationId", }, new Object[] { customerEmail.getId(), customerMigration.getId(), }, false);
            if (customerMigrationEmail == null) {
                customerMigrationEmail = new CustomerMigrationEmail();
                customerMigrationEmail.setCustomerEmail(customerEmail);
                customerMigrationEmail.setCustomerMigration(customerMigration);
                customerMigrationEmail.setLastModifiedByUserId(customerMigration.getUserAccount().getId());
                customerMigrationEmail.setLastModifiedGmt(currentTime);
                this.entityDao.save(customerMigrationEmail);
            }
        }
        final Set<CustomerMigrationEmail> finalEmailList = customerMigration.getCustomerMigrationEmails();
        for (CustomerMigrationEmail customerMigrationEmail : finalEmailList) {
            boolean isFound = false;
            for (String emailAddress : emailAddressList) {
                if (emailAddress.equals(customerMigrationEmail.getCustomerEmail().getEmail())) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                this.entityDao.delete(customerMigrationEmail);
            }
        }
        return customerMigration.getId();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCustomerMigration(final CustomerMigration customerMigration) {
        customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        
        this.entityDao.update(customerMigration);
    }
    
    @Override
    public final CustomerMigration findCustomerMigrationByCustomerId(final Integer customerId) {
        return (CustomerMigration) this.entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMigration.findCustomerMigrationByCustomerId",
                                                                                      "customerId", customerId, false);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final List<CustomerMigration> findScheduledCustomerMigration(final int batchSize) {
        final Calendar processWaitTime = Calendar.getInstance();
        
        int processWaitHours = EmsPropertiesService.CUSTOMER_MIGRATION_PROCESS_DELAY_IN_HOURS_DEFAULT;
        try {
            processWaitHours = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_PROCESS_DELAY_IN_HOURS,
                                           EmsPropertiesService.CUSTOMER_MIGRATION_PROCESS_DELAY_IN_HOURS_DEFAULT, true);
        } catch (Exception e) {
            // Ignore exception and continue with default value;
        }
        processWaitTime.add(Calendar.HOUR_OF_DAY, -processWaitHours);
        
        return this.entityDao.findByNamedQueryAndNamedParamWithLimit("CustomerMigration.findScheduledCustomerMigration",
                                                                     new String[] { "processWaitTime" }, new Object[] { processWaitTime.getTime() },
                                                                     false, batchSize);
    }
    
    @Override
    public final CustomerMigration findCustomerMigrationById(final Integer customerMigrationId) {
        return this.entityDao.get(CustomerMigration.class, customerMigrationId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final boolean isMigrationInProgress() {
        final List<Long> countList = this.entityDao.findByNamedQuery("CustomerMigration.countMigratingCustomers", false);
        if (countList == null || countList.isEmpty()) {
            return false;
        }
        final Long count = countList.get(0);
        if (count > 0) {
            return true;
        }
        
        return false;
    }
    
    @Override
    public final CustomerMigration findNextAvailableCustomer() {
        
        final List<CustomerMigration> customerList = this.entityDao.findByNamedQueryAndNamedParam("CustomerMigration.findNextAvailableCustomer",
                                                                                                  "currentTime", DateUtil.getCurrentGmtDate(), false);
        
        if (customerList != null && !customerList.isEmpty()) {
            return customerList.get(0);
        }
        return null;
    }
    
    @Override
    public final CustomerMigrationData getCurrentCustomerMigrationData() {
        
        int boardedCustomers = 0;
        int migratedCustomers = 0;
        int scheduledCustomers = 0;
        int remainingCustomers = 0;
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_CUSTOMER_STATUS);
        final List<Object> migrationStatus = q.list();
        
        for (Object status : migrationStatus) {
            final Object[] item = (Object[]) status;
            final byte migrationStatusCode = (byte) item[0];
            final BigInteger numberOfCustomers = (BigInteger) item[1];
            switch (migrationStatusCode) {
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED:
                    boardedCustomers = boardedCustomers + numberOfCustomers.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED:
                    scheduledCustomers = scheduledCustomers + numberOfCustomers.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED:
                    migratedCustomers = migratedCustomers + numberOfCustomers.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED:
                    remainingCustomers = remainingCustomers + numberOfCustomers.intValue();
                    break;
                default:
                    break;
            }
            
        }
        
        final CustomerMigrationData migrationData = new CustomerMigrationData();
        migrationData.setBoardedCount(boardedCustomers);
        migrationData.setMigratedCount(migratedCustomers);
        migrationData.setRemainingCount(remainingCustomers);
        migrationData.setScheduledCount(scheduledCustomers);
        
        return migrationData;
        
    }
    
    @Override
    public final CustomerMigrationData getCurrentPaystationMigrationStatus() {
        int boardedPaystations = 0;
        int migratedPaystations = 0;
        int scheduledPaystations = 0;
        int remainingPaystations = 0;
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_PAYSTATION_STATUS);
        final List<Object> migrationStatus = q.list();
        for (Object status : migrationStatus) {
            final Object[] item = (Object[]) status;
            final byte migrationStatusCode = (byte) item[0];
            final BigInteger numberOfPaystations = (BigInteger) item[1];
            switch (migrationStatusCode) {
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED:
                    boardedPaystations += numberOfPaystations.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED:
                    scheduledPaystations += numberOfPaystations.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED:
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED:
                    migratedPaystations += numberOfPaystations.intValue();
                    break;
                case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED:
                    remainingPaystations += numberOfPaystations.intValue();
                    break;
                default:
                    break;
            }
        }
        
        final CustomerMigrationData migrationData = new CustomerMigrationData();
        migrationData.setBoardedCount(boardedPaystations);
        migrationData.setMigratedCount(migratedPaystations);
        migrationData.setRemainingCount(remainingPaystations);
        migrationData.setScheduledCount(scheduledPaystations);
        
        return migrationData;
        
    }
    
    @Override
    public final List<CustomerMigrationData> getCustomerMigrationData() {
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_CUSTOMER_DATA);
        final List<Object[]> migrationData = q.list();
        
        final List<CustomerMigrationData> customerMigrationData = parseSQLQuery(migrationData);
        
        return customerMigrationData;
    }
    
    @Override
    public final List<CustomerMigrationData> getCustomerPaystationMigrationData() {
        
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_PAYSTATION_DATA);
        final List<Object[]> migrationData = q.list();
        
        final List<CustomerMigrationData> customerMigrationData = parseSQLQuery(migrationData);
        
        return customerMigrationData;
        
    }
    
    private List<CustomerMigrationData> parseSQLQuery(final List<Object[]> migrationData) {
        final int size = migrationData.size();
        final List<CustomerMigrationData> customerMigrationDataList = new ArrayList<CustomerMigrationData>(size);
        for (Object[] migrationDataWithTime : migrationData) {
            final Date dateTime = (Date) migrationDataWithTime[0];
            final int migratedCustomers = ((BigInteger) migrationDataWithTime[1]).intValue();
            final int boardedCustomers = ((BigInteger) migrationDataWithTime[2]).intValue();
            final int emsCustomers = ((BigInteger) migrationDataWithTime[3]).intValue();
            
            final CustomerMigrationData customerMigrationData = new CustomerMigrationData();
            customerMigrationData.setBoardedCount(boardedCustomers);
            customerMigrationData.setMigratedCount(migratedCustomers);
            customerMigrationData.setRemainingCount(emsCustomers);
            customerMigrationDataList.add(customerMigrationData);
            
        }
        
        return customerMigrationDataList;
        
    }
    
    public final List<CustomerMigration> findThisWeeksSchedule() {
        final TimeZone timeZone = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
        final Calendar startTime = Calendar.getInstance(timeZone);
        startTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        startTime.set(Calendar.HOUR_OF_DAY, 0);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        startTime.set(Calendar.MILLISECOND, 0);
        
        final Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
        
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerMigration.findThisWeeksSchedule", new String[] { "startTime", "endTime", },
                                                            new Object[] { startTime.getTime(), endTime.getTime(), }, false);
    }
    
    @Override
    public final List<CustomerMigration> findCustomerMigrationByCriteria(final CustomerMigrationSearchCriteria criteria, final String timeZone) {
        
        @SuppressWarnings("unchecked")
        List<CustomerMigration> result = new ArrayList<CustomerMigration>();
        if (criteria.getCustomerMigrationStatusTypeIds() != null && criteria.getCustomerMigrationStatusTypeIds().size() > 0) {
            result = createCustomerMigrationScheduleQuery(criteria).list();
        }
        
        return result;
    }
    
    private Criteria createCustomerMigrationScheduleQuery(final CustomerMigrationSearchCriteria criteria) {
        
        final Criteria query = this.entityDao.createCriteria(CustomerMigration.class);
        query.setFetchMode("customer", FetchMode.JOIN).createAlias("customer", "c");
        
        addExclusiveOrderBy(criteria, query);
        
        if (criteria.getCustomerMigrationStatusTypeIds() != null) {
            query.add(Restrictions.in("customerMigrationStatusType.id", criteria.getCustomerMigrationStatusTypeIds().toArray()));
        }
        
        if ((criteria.getItemsPerPage() != null) && (criteria.getPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        return query;
    }
    
    private void addExclusiveOrderBy(final CustomerMigrationSearchCriteria criteria, final Criteria query) {
        if (!StringUtils.isEmpty(criteria.getExclusiveOrderBy())) {
            String orderColumn = null;
            
            switch (criteria.getExclusiveOrderBy()) {
                case "customer":
                    orderColumn = "c.name";
                    break;
                case "status":
                    orderColumn = "customerMigrationStatusType.id";
                    break;
                case "boardedDate":
                    orderColumn = "customerBoardedGmt";
                    break;
                case "migratedDate":
                    orderColumn = "migrationScheduledGmt";
                    break;
                case "contacted":
                    orderColumn = "isContacted";
                    break;
                default:
                    break;
            }
            
            if (orderColumn != null) {
                if (criteria.getExclusiveOrderDir() != null) {
                    if ("desc".equals(criteria.getExclusiveOrderDir().toLowerCase())) {
                        query.addOrder(Order.desc(orderColumn));
                    } else {
                        query.addOrder(Order.asc(orderColumn));
                    }
                }
            }
        }
    }
    
    @Override
    public final List<RecentBoardedMigratedCustomerList> getRecentCustomerActivityList(final HttpServletRequest request, final String sortOrder,
        final String sortItem, final Integer page, final boolean isBoarded) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String orderByString = SQL_RECENT_CUSTOMERS_ORDER_BY_CUSTOMER_NAME;
        if ("date".equals(sortItem)) {
            orderByString = SQL_RECENT_CUSTOMERS_ORDER_BY_CUSTOMER_DATE;
        } else if ("payStationCount".equals(sortItem)) {
            orderByString = SQL_RECENT_CUSTOMERS_ORDER_BY_PAYSTATION_COUNT;
        }
        
        final StringBuilder query = new StringBuilder();
        query.append(isBoarded ? SQL_BOARDED_SELECT : SQL_MIGRATED_SELECT);
        query.append(SQL_AVTIVITY_REMAINING);
        query.append(isBoarded ? SQL_BOARDED_WHERE : SQL_MIGRATED_WHERE);
        query.append(SQL_GROUP_BY_CUSTOMER_ID);
        
        query.append(orderByString).append(StandardConstants.STRING_EMPTY_SPACE).append(sortOrder);
        query.append(SQL_RECENT_CUSTOMERS_LIMIT);
        query.append((page - 1) * WebCoreConstants.ITEMS_PER_PAGE);
        query.append(StandardConstants.STRING_COMMA);
        query.append(WebCoreConstants.ITEMS_PER_PAGE);
        
        final SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("CustomerId", new IntegerType());
        q.addScalar("CustomerName", new StringType());
        q.addScalar("CustomerDate", new TimestampType());
        q.addScalar("ValidationStatus", new IntegerType());
        q.addScalar("PayStationCount", new IntegerType());
        
        final List<Object[]> customerActivityResList = q.list();
        final int size = customerActivityResList.size();
        final List<RecentBoardedMigratedCustomerList> customerActivityList = new ArrayList<RecentBoardedMigratedCustomerList>(size);
        
        for (Object[] customerActivityRes : customerActivityResList) {
            final int customerId = (int) customerActivityRes[CUSTOMER_ID];
            final String customerName = (String) customerActivityRes[CUSTOMER_NAME];
            final Date customerDate = (Date) customerActivityRes[DATE];
            final Integer validationStatus = (Integer) customerActivityRes[VALIDATION_STATUS];
            final int paystationCount = (int) customerActivityRes[PAYSTATION_COUNT];
            
            final RecentBoardedMigratedCustomerList customerActivity = new RecentBoardedMigratedCustomerList();
            customerActivity.setRandomId(keyMapping.getRandomString(Customer.class, customerId));
            customerActivity.setCustomerName(customerName);
            customerActivity.setValidationStatus(validationStatus);
            customerActivity.setPaystationsCount(paystationCount);
            customerActivity.setCustomerDate(customerDate != null ? DateUtil.createUIDateOnlyString(customerDate, SYSTEM_ADMIN_TIMEZONE)
                    : WebCoreConstants.N_A_STRING);
            customerActivityList.add(customerActivity);
            
        }
        
        return customerActivityList;
        
    }
    
    @Override
    public final String getMigrationStatusText(final byte customerMigrationStatusTypeId) {
        return statusMappingText.get(customerMigrationStatusTypeId);
    }
    
    @Override
    public final int getMigrationStatusInt(final byte customerMigrationStatusTypeId) {
        return statusMappingInt.get(customerMigrationStatusTypeId);
    }
    
    @Override
    public final List<CustomerMigrationEmail> findCustomerMigrationEmails(final Integer customerMigrationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerMigrationEmail.findCustomerMigrationEmailByCustomerId", "customerMigrationId",
                                                            customerMigrationId, false);
    }
}
