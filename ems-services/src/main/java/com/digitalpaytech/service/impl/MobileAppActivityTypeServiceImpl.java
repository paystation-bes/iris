package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MobileAppActivityType;
import com.digitalpaytech.service.MobileAppActivityTypeService;

@Service("MobileAppActivityTypeService")
public class MobileAppActivityTypeServiceImpl implements MobileAppActivityTypeService{
    @Autowired
    EntityDao entityDao;
    
    @Override
    public MobileAppActivityType findById(Integer id){
        return (MobileAppActivityType)entityDao.findUniqueByNamedQueryAndNamedParam("MobileAppActivityType.findById", new String[]{"id"}, new Object[]{id}, true);
    }
    
    @Override
    public MobileAppActivityType findByName(String name){
        return (MobileAppActivityType)entityDao.findUniqueByNamedQueryAndNamedParam("MobileAppActivityType.findByName", new String[]{"name"}, new Object[]{name}, true);
    }
}
