package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MaintenanceSummaryConfig;
import com.digitalpaytech.service.MaintenanceSummaryConfigService;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
@Service("MaintenanceSummaryConfigService")
@Transactional(propagation = Propagation.SUPPORTS)
public class MaintenanceSummaryConfigServiceImpl implements MaintenanceSummaryConfigService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final MaintenanceSummaryConfig getMaintenanceSummaryConfigByUserId(final int userAccountId) {
        return (MaintenanceSummaryConfig) this.entityDao.findUniqueByNamedQueryAndNamedParam("MaintenanceSummaryConfig.findColumnShowByUserAccountId",
                                                                                             "userAccountId", userAccountId);
    }
}
