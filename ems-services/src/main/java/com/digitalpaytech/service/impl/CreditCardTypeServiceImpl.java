package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.FilterDTOTransformer;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.CardProcessingConstants;

@Service("creditCardTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CreditCardTypeServiceImpl implements CreditCardTypeService {
    
    @Autowired
    private transient EntityDao entityDao;
    
    private transient List<CreditCardType> creditCardTypes;
    private transient Map<Integer, CreditCardType> creditCardTypesMap;
    private transient Map<String, CreditCardType> creditCardTypeLowerNamesMap;
    
    private void init() {
        if (this.creditCardTypes == null) {
            this.creditCardTypes = this.entityDao.loadAll(CreditCardType.class);
            
            final Map<Integer, CreditCardType> cctsMap = new HashMap<Integer, CreditCardType>(this.creditCardTypes.size());
            final Map<String, CreditCardType> cctlnsMap = new HashMap<String, CreditCardType>(this.creditCardTypes.size());
            for (CreditCardType cct : this.creditCardTypes) {
                this.entityDao.evict(cct);
                
                cctsMap.put(cct.getId(), cct);
                cctlnsMap.put(cct.getName().trim().toLowerCase(), cct);
            }
            
            final CreditCardType dinersCreditCardType = cctlnsMap.get(CardProcessingConstants.NAME_DINERS.toLowerCase());
            if (dinersCreditCardType != null) {
                cctlnsMap.put(CardProcessingConstants.NAME_DINERS_WO_CLUB.toLowerCase(), dinersCreditCardType);   
            }
            
            this.creditCardTypesMap = cctsMap;
            this.creditCardTypeLowerNamesMap = cctlnsMap;
        }
    }
    
    @Override
    public final List<CreditCardType> loadAll() {
        init();
        return this.creditCardTypes;
    }
    
    @Override
    public final Map<Integer, CreditCardType> getCreditCardTypesMap() {
        init();
        return this.creditCardTypesMap;
    }
    
    @Override
    public final Map<String, CreditCardType> getCreditCardTypeLowerNamesMap() {
        init();
        return this.creditCardTypeLowerNamesMap;
    }
    
    @Override
    public final CreditCardType getCreditCardTypeByName(final String name) {
        final Map<String, CreditCardType> cctlnsMap = getCreditCardTypeLowerNamesMap();
        return (name == null) ? (CreditCardType) null : cctlnsMap.get(name.trim().toLowerCase());
    }

    @Override
    public final CreditCardType getCreditCardTypeByName(final String name, final boolean defaultCreditCard) {
        final CreditCardType cct = getCreditCardTypeByName(name);
        if (defaultCreditCard && cct == null) {
            return getCreditCardTypeByName(CardProcessingConstants.NAME_CREDIT_CARD);
        }
        return cct;
    }    
    
    @Override
    public final CreditCardType getCreditCardTypeById(final Integer id) {
        final Map<Integer, CreditCardType> cctlnsMap = getCreditCardTypesMap();
        return cctlnsMap.get(id);
    }

    @Override
    public final String getText(final int creditCardId) {
        if (this.creditCardTypesMap == null) {
            getCreditCardTypesMap();
        }
        
        if (this.creditCardTypesMap.containsKey(creditCardId)) {
            return this.creditCardTypesMap.get(creditCardId).getName();
        } else {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<FilterDTO> getFilters(final List<FilterDTO> result, final RandomKeyMapping keyMapping, final boolean showOnlyValid) {
        return this.entityDao.getNamedQuery("CreditCardType.findFilters")
                .setResultTransformer(new FilterDTOTransformer(keyMapping, CreditCardType.class, result)).setParameter("isValid", true)
                .setParameter("isInvalid", !showOnlyValid).list();
    }
    
    // For JUnit
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
