package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.RateType;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("permitIssueTypeService")
public class PermitIssueTypeServiceImpl implements PermitIssueTypeService {
    
    private static Logger log = Logger.getLogger(PermitIssueTypeServiceImpl.class);
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private TransactionService transactionService;
    
    private Map<Integer, PermitIssueType> map;
    
    @PostConstruct
    private void loadAllToMap() {
        List<PermitIssueType> list = openSessionDao.loadAll(PermitIssueType.class);
        map = new HashMap<Integer, PermitIssueType>(list.size());
        
        Iterator<PermitIssueType> iter = list.iterator();
        while (iter.hasNext()) {
            PermitIssueType issType = iter.next();
            map.put(issType.getId(), issType);
        }
    }
    
    @Override
    public PermitIssueType findPermitIssueType(String licensePlate, String stallNumber) {
        if (StringUtils.isNotBlank(licensePlate) && StringUtils.isNotBlank(stallNumber)) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("PermitIssueTypeServiceImpl, findPermitIssueType, <Transaction> xml has both licensePlate and stallNumber, licensePlate: ")
                    .append(licensePlate);
            bdr.append(", stallNumber: ").append(stallNumber).append(", find License Plate first.");
            log.warn(bdr.toString());
        }
        
        if (StringUtils.isNotBlank(licensePlate)) {
            return map.get(transactionService.getMappingPropertiesValue("permitIssueType.pay.plate.id"));
        }
        try {
            if (StringUtils.isNotBlank(stallNumber) && Integer.parseInt(stallNumber) > 0) {
                return map.get(transactionService.getMappingPropertiesValue("permitIssueType.pay.space.id"));
            }
        } catch (NumberFormatException nfe) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("Cannot parse stallNumber: ").append(stallNumber).append(", return default permitIssueType - Pay & Display.");
            log.error(nfe, nfe);
        }
        return map.get(transactionService.getMappingPropertiesValue("permitIssueType.pay.display.id"));
    }
    
    public void setOpenSessionDao(OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
    
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    @Override
    public List<PermitIssueType> findAll() {
        List<PermitIssueType> permitIssueTypeList = this.entityDao.loadAll(PermitIssueType.class);
        for (PermitIssueType permitIssueType : permitIssueTypeList) {
            if (permitIssueType.getId() == WebCoreConstants.PERMIT_ISSUE_TYPE_NA) {
                permitIssueTypeList.remove(permitIssueType);
                break;
            }
        }
        return permitIssueTypeList;
    }
    
    @Override
    public PermitIssueType findPermitIssueTypeById(Integer permitIssueTypeId) {
        return this.entityDao.get(PermitIssueType.class, permitIssueTypeId);
    }
}
