package com.digitalpaytech.service.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;

import com.digitalpaytech.service.BannedCardService;
import com.digitalpaytech.util.LicensePlateConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.dto.customeradmin.BannedCard;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;

@Service("bannedCardService")
public class BannedCardServiceImpl implements BannedCardService {
    private static final Logger LOG = Logger.getLogger(BannedCardServiceImpl.class);
    private static final String PARAM_CARD_TYPE_ID = "cardTypeId";
    private static final String PARAM_CARD_NUMBER = "cardNumber";
    private static final String PARAM_LAST_4_DIGITS = "last4DigitsOfCardNumber";
    private static final int RESULT_POSITION_ID = 0;
    private static final int RESULT_POSITION_ADDED_GMT = 1;
    private static final int RESULT_POSITION_CARD_EXPIRY = 2;
    private static final int RESULT_POSITION_LAST_4_DIGITS = 3;
    private static final int RESULT_POSITION_CARD_NUMBER = 4;
    private static final int RESULT_POSITION_COMMENT = 5;
    private static final int RESULT_POSITION_CARD_DATA = 6;
    private static final int RESULT_POSITION_SOURCE = 7;
    private static final int RESULT_POSITION_CARD_TYPE = 8;
    private static final int RESULT_POSITION_CARD_TYPE_ID = 9;
    private static final int RESULT_POSITION_CUSTOMER_ID = 10;
    private static final int RESULT_POSITION_NUM_RETRIES = 11;
    
    private static final String SQL_FIND_CARD_RETRY_TRANSACTION_NO_FILTER =
            "SELECT cardRetry.Id AS Id, cardRetry.CreationDate AS AddedGMT, cardRetry.CardExpiry AS CardExpiry,"
                                                                            + " cardRetry.Last4DigitsOfCardNumber AS Last4DigitsOfCardNumber,"
                                                                            + " cardRetry.BadCardHash AS CardNumber,"
                                                                            + " 'FROM CARD RETRY TRANSACTION' AS Comment,"
                                                                            + " cardRetry.CardData AS CardData, 'import' AS Source,"
                                                                            + " cardRetry.CardType AS CardType, 1 AS CardTypeId, customer.id AS CustomerId,"
                                                                            + " cardRetry.NumRetries AS NumRetries"
                                                                            + " FROM CardRetryTransaction cardRetry"
                                                                            + " INNER JOIN PointOfSale pos ON cardRetry.PointOfSaleId=pos.Id"
                                                                            + " INNER JOIN Customer customer ON pos.CustomerId=customer.Id"
                                                                            + " WHERE";
    
    private static final String SQL_FIND_CARD_RETRY_TRANSACTION_CUSTOMER_ID =
            " cardRetry.IgnoreBadCard=0 AND customer.id = :customerId AND cardRetry.NumRetries >= :numRetries";
    
    private static final String SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_TYPE_ID = " 1 = :cardTypeId AND ";
    
    private static final String SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_NUMBER_LIKE = " AND cardRetry.BadCardHash LIKE :cardNumber ";
    
    private static final String SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_LAST_4_DIGITS =
            " AND cardRetry.Last4DigitsOfCardNumber = :last4DigitsOfCardNumber";
    
    private static final String SQL_ORDER_BY = " ORDER BY ";
    
    private enum SQLOrderBy {
        ASC, DESC
    }
    
    @Autowired
    private EntityDao entityDao;
    
    private boolean exists(final String sql, final String paramName) {
        return sql.contains(paramName);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final Set<BannedCard> findBannedCardsFromCardRetry(final CustomerBadCardSearchCriteria criteria) {
        final String sql = buildQueryWithParameters(criteria);
        final SQLQuery query = this.entityDao.createSQLQuery(sql);
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("SQL query with parameters: " + sql);
        }
        
        query.setParameter(LicensePlateConstants.PARAM_CUSTOMER_ID, criteria.getCustomerId());
        query.setParameter("numRetries", criteria.getMaxRetry());
        
        if (exists(sql, PARAM_CARD_TYPE_ID)) {
            query.setParameter(PARAM_CARD_TYPE_ID, criteria.getCardType());
        }
        
        if (exists(sql, PARAM_CARD_NUMBER)) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(WebCoreConstants.SQL_WILDCARD_PERCENT).append(criteria.getCardNumber()).append(WebCoreConstants.SQL_WILDCARD_PERCENT);
            query.setParameter(PARAM_CARD_NUMBER, bdr.toString());
        }
        
        if (exists(sql, PARAM_LAST_4_DIGITS)) {
            final String last4 = StringUtils.stripStart(criteria.getCardNumber(), String.valueOf(StandardConstants.CONSTANT_0));
            final Short num = last4.isEmpty() ? StandardConstants.CONSTANT_0 : Short.valueOf(last4);
            query.setParameter(PARAM_LAST_4_DIGITS, num);
        }
        
        if (criteria.getPage() != null && criteria.getItemsPerPage() != null) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        final List<Object[]> objArrList = query.list();
        final Set<BannedCard> bannedCardsSet = new LinkedHashSet<>(objArrList.size());
        for (Object[] objArr : objArrList) {
            bannedCardsSet.add(createBannedCard(objArr));
        }
        return bannedCardsSet;
        
    }
    
    private BannedCard createBannedCard(final Object[] objArr) {
        final BannedCard bc = new BannedCard();
        bc.setId(((BigInteger) objArr[RESULT_POSITION_ID]).intValue());
        bc.setAddedGMT((Timestamp) objArr[RESULT_POSITION_ADDED_GMT]);
        bc.setCardExpiry((Short) objArr[RESULT_POSITION_CARD_EXPIRY]);
        bc.setLast4DigitsOfCardNumber((Short) objArr[RESULT_POSITION_LAST_4_DIGITS]);
        bc.setCardNumber((String) objArr[RESULT_POSITION_CARD_NUMBER]);
        bc.setComment((String) objArr[RESULT_POSITION_COMMENT]);
        bc.setCardData((String) objArr[RESULT_POSITION_CARD_DATA]);
        bc.setSource((String) objArr[RESULT_POSITION_SOURCE]);
        bc.setCardType((String) objArr[RESULT_POSITION_CARD_TYPE]);
        bc.setCardTypeId(((BigInteger) objArr[RESULT_POSITION_CARD_TYPE_ID]).intValue());
        bc.setCustomer(new Customer((Integer) objArr[RESULT_POSITION_CUSTOMER_ID]));
        bc.setNumRetries(Integer.valueOf((short) objArr[RESULT_POSITION_NUM_RETRIES]));
        return bc;
    }
    
    private String buildQueryWithParameters(final CustomerBadCardSearchCriteria criteria) {
        
        final StringBuilder cardRetryBuilder = new StringBuilder();
        cardRetryBuilder.append(SQL_FIND_CARD_RETRY_TRANSACTION_NO_FILTER);
        
        if (criteria.getCustomerCardType() != null && criteria.getCustomerCardType() > 0) {
            cardRetryBuilder.append(SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_TYPE_ID);
        }
        
        cardRetryBuilder.append(SQL_FIND_CARD_RETRY_TRANSACTION_CUSTOMER_ID);
        
        if (!StringUtils.isBlank(criteria.getCardNumber())) {
            if (criteria.getCardNumber().length() != StandardConstants.CONSTANT_4) {
                cardRetryBuilder.append(SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_NUMBER_LIKE);
            } else {
                cardRetryBuilder.append(SQL_FILTER_CARD_RETRY_TRANSACTION_CARD_LAST_4_DIGITS);
            }
        }
        
        if (criteria.getOrderColumn() != null && criteria.getOrderColumn().length > 0) {
            cardRetryBuilder.append(SQL_ORDER_BY).append(criteria.getOrderColumn()[0]).append(StandardConstants.STRING_EMPTY_SPACE);
            if (criteria.isOrderDesc()) {
                cardRetryBuilder.append(SQLOrderBy.DESC);
            } else {
                cardRetryBuilder.append(SQLOrderBy.ASC);
            }
            
        }
        return cardRetryBuilder.toString();
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
