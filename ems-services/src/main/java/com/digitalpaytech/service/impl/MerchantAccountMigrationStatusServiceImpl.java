package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MerchantAccountMigrationStatus;
import com.digitalpaytech.service.MerchantAccountMigrationStatusService;

@Service("MerchantAccountMigrationStatusService")
@Transactional(propagation = Propagation.REQUIRED)
public class MerchantAccountMigrationStatusServiceImpl implements MerchantAccountMigrationStatusService {
    private static final Logger LOG = Logger.getLogger(MerchantAccountMigrationStatusService.class);
    
    private Map<Integer, MerchantAccountMigrationStatus> statuses;
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public MerchantAccountMigrationStatus find(final int statusId) {
        if (this.statuses == null || this.statuses.isEmpty()) {
            init();
        }
        
        if (this.statuses == null || this.statuses.isEmpty()) {
            LOG.error("Unable to find MerchantAccountMigrationStatus, they were not loaded from the db properly");
            return null;
        } else {
            return this.statuses.get(statusId);
        }
        
    }
    
    private void init() {
        this.statuses = new ConcurrentHashMap<>();
        final List<MerchantAccountMigrationStatus> statusList = this.entityDao.loadAll(MerchantAccountMigrationStatus.class);
        statusList.forEach(status -> this.statuses.put(status.getId(), status));
    }
    
}
