package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CitationTypeService;

@Component("citationTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CitationTypeServiceImpl implements CitationTypeService {
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings({ "unchecked", "checkstyle:designforextension" })
    @Override
    public List<CitationType> findCitationTypeByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CitationType.findByCustomerId", HibernateConstants.CUSTOMER_ID, customerId, true);
    }
    
    @SuppressWarnings({ "unchecked", "checkstyle:designforextension" })
    @Override
    public List<CitationType> findActiveCitationTypeByCustomerId(final Integer customerId) {
        return this.entityDao.getNamedQuery("CitationType.findActiveByCustomerId")
                .setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .list();
    }
    
    @Override
    public final CitationType findCitationTypeByCustomerIdAndFlexCitationTypeId(final Integer customerId, final Integer flexCitationTypeId) {
        return (CitationType) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("CitationType.findByCustomerIdAndFlexCitationTypeId"
                                                     , new String[] { HibernateConstants.CUSTOMER_ID, "flexCitationTypeId", }
                                                     , new Object[] { customerId, flexCitationTypeId, }, true);
    }
}
