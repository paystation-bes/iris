package com.digitalpaytech.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.util.CardProcessingConstants;

@Service("commonProcessingService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CommonProcessingServiceImpl implements CommonProcessingService {
    
    private static Logger log = Logger.getLogger(CommonProcessingServiceImpl.class);
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private MailerService mailerService;
    
    @Override
    public String getDestinationUrlString(Processor processor) {
        Processor dbProc = getProcessor(processor.getId());
        
        String destinationUrlString;
        if (dbProc.isIsTest()) {
            destinationUrlString = dbProc.getTestUrl();
        } else {
            destinationUrlString = dbProc.getProductionUrl();
        }
        return destinationUrlString;
    }
    
    //------------------------------------------------------------------------------------------------------
    // Copied from EMS 6.3.12 - com.digitalpioneer.cardprocessing.CardProcessor
    //------------------------------------------------------------------------------------------------------
    /**
     * Generate a new reference number based on merchant account and return it as
     * a String that is 12 characters long and zero-padded on the left.
     */
    @Override
    public String getNewReferenceNumber(MerchantAccount merchantAccount) {
        /*
         * Use a synchronized block to call 'merchantAccountService.updateNewReferenceNumber' and push the new value
         * to the database right away.
         */
        long reference_number_long = -1;
        synchronized (this) {
            reference_number_long = merchantAccountService.updateNewReferenceNumber(merchantAccount);
        }
        
        String reference_number_string = Long.toString(reference_number_long);
        int len = reference_number_string.length();
        
        StringBuffer reference_number_string_buffer = new StringBuffer();
        for (int i = 0; i < (12 - len); i++) {
            // length of reference number is 12 (as per Concord)
            reference_number_string_buffer.append("0");
        }
        reference_number_string_buffer.append(reference_number_string);
        
        if (log.isDebugEnabled()) {
            log.debug("REF COUNTER:  " + reference_number_string_buffer.toString());
        }
        return reference_number_string_buffer.toString();
    }
    
    @Override
    public void sendCardProcessingAdminErrorAlert(String subject, String message, Exception e) {
        mailerService.sendAdminErrorAlert(subject, message, e);
    }
    
    @Override
    public boolean isReversalExpired(Reversal reversal) {
        return (reversal.getRetryAttempts() >= CardProcessingConstants.DEFAULT_MAX_REVERSAL_RETRIES);
    }
    
    /**
     * isTestMode method is called from Processor classes which are out of transactional boundary and not able to get Processor object
     * from MerchantAccount. Thus this method will look up the merchant account and processor.
     * 
     * @param merchantAccountId
     *            Merchant account id.
     * @return boolean true if associated Processor object has isiTest equals to true (1).
     */
    @Override
    public boolean isTestMode(int merchantAccountId) {
        return this.merchantAccountService.findById(merchantAccountId).getProcessor().isIsTest();
    }
    
    //------------------------------------------------------------------------------------------------------
    // Copied from EMS 6.3.12 - com.digitalpioneer.appservice.common.Impl.BaseAppServiceImpl
    //------------------------------------------------------------------------------------------------------
    @Override
    public String convertWildCards(String value) {
        if (value != null && value.length() > 0) {
            StringBuilder sb = new StringBuilder(value);
            // add escape character for MySQL wildcard characters % and _
            addEscapeCharacter(CardProcessingConstants.MYSQL_MULTIPLE_WILD_CARD, sb, 0);
            addEscapeCharacter(CardProcessingConstants.MYSQL_SINGLE_WILD_CARD, sb, 0);
            substituteWildCard(CardProcessingConstants.MULTIPLE_WILD_CARD, CardProcessingConstants.MYSQL_MULTIPLE_WILD_CARD, sb, 0);
            substituteWildCard(CardProcessingConstants.SINGLE_WILD_CARD, CardProcessingConstants.MYSQL_SINGLE_WILD_CARD, sb, 0);
            value = sb.toString();
        }
        return value;
    }
    
    private void addEscapeCharacter(String searchValue, StringBuilder sb, int startIndex) {
        int index = sb.indexOf(searchValue, startIndex);
        if (index >= 0) {
            sb.insert(index, CardProcessingConstants.ESCAPE_CHARACTER);
            
            // calculate new start position
            int newIndex = index + searchValue.length() + 1;
            if (newIndex < sb.length()) {
                addEscapeCharacter(searchValue, sb, newIndex);
            }
        }
    }
    
    private void substituteWildCard(String wildCardValue, String replaceWildCardValue, StringBuilder sb, int startIndex) {
        int index = sb.indexOf(wildCardValue, startIndex);
        if (index >= 0) {
            sb.replace(index, index + wildCardValue.length(), replaceWildCardValue);
            
            // calculate new start position
            int newIndex = index + replaceWildCardValue.length();
            if (newIndex < sb.length()) {
                substituteWildCard(wildCardValue, replaceWildCardValue, sb, newIndex);
            }
        }
    }
    
    public void setMerchantAccountService(MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public void setMailerService(MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public void setProcessorService(ProcessorService processorService) {
        this.processorService = processorService;
    }
    
    @Override
    public boolean isProcessorTestMode(int processorId) {
        return this.processorService.findProcessor(processorId).isIsTest();
    }

    @Override
    public Processor getProcessor(int processorId) {
        return this.processorService.findProcessor(processorId);
    }
}
