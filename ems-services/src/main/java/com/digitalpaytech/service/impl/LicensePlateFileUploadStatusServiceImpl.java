package com.digitalpaytech.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.dto.llps.PreferredParkerFile;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.LicensePlateFileUploadStatus;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LicensePlateFileUploadStatusService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.util.LicensePlateConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service
public class LicensePlateFileUploadStatusServiceImpl implements LicensePlateFileUploadStatusService {
    
    private static final Logger LOG = Logger.getLogger(LicensePlateFileUploadStatusService.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private LicensePlateService licensePlateService;
    
    @Autowired
    private MessageHelper messages;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void save(final LicensePlateFileUploadStatus licensePlateFileUploadStatus) {
        this.entityDao.save(licensePlateFileUploadStatus);
    }
    
    @Override
    public void saveOrUpdate(final LicensePlateFileUploadStatus licensePlateFileUploadStatus) {
        this.entityDao.saveOrUpdate(licensePlateFileUploadStatus);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void update(final LicensePlateFileUploadStatus licensePlateFileUploadStatus) {
        this.entityDao.update(licensePlateFileUploadStatus);
    }
    
    @Override
    public final LicensePlateFileUploadStatus findByCustomerIdAndStatus(final Integer customerId, final String status) {
        
        @SuppressWarnings("unchecked")
        final List<LicensePlateFileUploadStatus> statuses =
                this.entityDao.findByNamedQueryAndNamedParam("LicensePlateFileUploadStatus.findByCustomerIdAndStatus",
                                                             new String[] { LicensePlateConstants.PARAM_CUSTOMER_ID, "status" },
                                                             new Object[] { customerId, status });
        return checkResult(statuses);
    }
    
    @Override
    public final LicensePlateFileUploadStatus findLatestByCustomerId(final Integer customerId) {
        
        final Query q = this.entityDao.getNamedQuery("LicensePlateFileUploadStatus.findByCustomerIdOrderByLastUpdatedGMTDesc");
        q.setParameter(LicensePlateConstants.PARAM_CUSTOMER_ID, customerId);
        q.setMaxResults(1);
        return checkResult(q.list());
    }
    
    private LicensePlateFileUploadStatus checkResult(final List<LicensePlateFileUploadStatus> statuses) {
        if (statuses == null || statuses.isEmpty()) {
            return null;
        } else {
            return statuses.get(0);
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void setPreferredParkingsStatus(final GlobalConfiguration globalConfiguration, final Customer customer) {
        
        final LicensePlateFileUploadStatus licenseStatus = findLatestByCustomerId(customer.getId());
        final FilterDTO dto = new FilterDTO();
        if (licenseStatus == null) {
            dto.setProcessStatusString(this.messages.getMessage(CustomerAdminServiceImpl.LABEL_SETTINGS_GLOBAL_PREF_NO_PREFERRED_PARKERS));
        } else if (!licenseStatus.getStatus().equalsIgnoreCase(LicensePlateFileUploadStatus.ProcessStatus.IN_PROCESS.toString()) || licenseStatus.getFileId() == null) {
            dto.setProcessStatusString(this.messages
                    .getMessage(combineKeysAndLowerCase(CustomerAdminServiceImpl.LABEL_SETTINGS_GLOBAL_PREF_PROCESS_STATUS_KEY_PREFIX,
                                                        licenseStatus.getStatus())));
            dto.setLastUpdatedDateTime(createCustomerLocalDateTime(licenseStatus));
            if (StringUtils.isNotBlank(licenseStatus.getError())) {
                dto.setErrorRows(Arrays.asList(licenseStatus.getError().split(StandardConstants.STRING_SEMICOLON)));
            }
        } else {
            try {
                
                final PreferredParkerFile prefFile = this.licensePlateService
                        .getCurrentStatus(licenseStatus.getFileId(), licenseStatus.getCustomer().getUnifiId());
                updateLicensePlateFileUploadStatusInDb(licenseStatus, prefFile);
                
                dto.setProcessStatusString(this.messages
                        .getMessage(combineKeysAndLowerCase(CustomerAdminServiceImpl.LABEL_SETTINGS_GLOBAL_PREF_PROCESS_STATUS_KEY_PREFIX,
                                                            prefFile.getProcessStatus().toString())));
                dto.setLastUpdatedDateTime(createCustomerLocalDateTime(licenseStatus));
                
                if (prefFile.getErrorRows() != null && !prefFile.getErrorRows().isEmpty()) {
                    dto.setErrorRows(prefFile.getErrorRows());
                }
            } catch (JsonException | HystrixRuntimeException e) {
                LOG.error("Cannot call License Plate Service - 'getCurrentStatus'", e);
                dto.setProcessStatusString(this.messages
                        .getMessage(CustomerAdminServiceImpl.LABEL_SETTINGS_GLOBAL_PREF_PROCESS_STATUS_KEY_PREFIX + "error_out"));
            }
            
        }
        globalConfiguration.setPreferredParkersStatus(dto);
    }
    
    private LicensePlateFileUploadStatus updateLicensePlateFileUploadStatusInDb(final LicensePlateFileUploadStatus licenseStatus,
        final PreferredParkerFile preferredParkerFile) {
        licenseStatus.setError(WebCoreUtil.concatErrors(preferredParkerFile.getErrorRows(), LicensePlateFileUploadStatus.ERROR_STRING_MAX_LEN));
        licenseStatus.setStatus(preferredParkerFile.getProcessStatus().toString());
        licenseStatus.setLastUpdatedGMT(new Date());
        saveOrUpdate(licenseStatus);
        return licenseStatus;
    }
    
    private String combineKeysAndLowerCase(final String keyPrefix, final String status) {
        return keyPrefix + status.toLowerCase();
    }
    
    private String createCustomerLocalDateTime(final LicensePlateFileUploadStatus licenseStatus) {
        if (licenseStatus.getLastUpdatedGMT() == null) {
            return this.messages.getMessage(CustomerAdminServiceImpl.LABEL_SETTINGS_GLOBAL_PREF_NO_PREFERRED_PARKERS);
        }
        return StableDateUtil
                .getDateFormattedWithChronoUnitDays(StableDateUtil.DATE_ONLY_FORMAT,
                                                    this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(licenseStatus
                                                            .getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                                                            .getPropertyValue(),
                                                    licenseStatus.getLastUpdatedGMT());
    }
    
}
