package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.service.AlertTypeService;

@Service("alertTypeService")
@Transactional(propagation = Propagation.REQUIRED)
public class AlertTypeServiceImpl implements AlertTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<AlertType> findAlertTypeByIsUserDefined(final boolean isUserDefined) {
        return this.entityDao.findByNamedQueryAndNamedParam("AlertType.findAlertTypeByIsUserDefined", "isUserDefined", isUserDefined, true);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<AlertClassType> findAllAlertClassTypes() {
        return this.entityDao.loadAll(AlertClassType.class);
    }
    
    @Override
    public final EventSeverityType findEventSeverityTypeById(final int eventSeverityTypeId) {
        return (EventSeverityType) this.entityDao.getNamedQuery("EventSeverityType.findEventSeverityTypeById")
                .setParameter("id", eventSeverityTypeId).uniqueResult();
    }
    
    @Override
    public final List<AlertType> findAllAlertTypes() {
        return this.entityDao.loadAll(AlertType.class);
    }
    
    @Override
    public final AlertThresholdType findAlertThresholdTypeById(final int id) {
        return (AlertThresholdType) this.entityDao.get(AlertThresholdType.class, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<String> findAlertThresholdTypeText(final Collection<Integer> ids) {
        return this.entityDao.getNamedQuery("AlertThresholdType.findByIds").setParameterList("alertThresholdTypeIds", ids).list();
    }
}
