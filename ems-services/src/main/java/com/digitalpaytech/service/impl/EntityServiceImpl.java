package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.dao.EntityDao;

@Service("entityService")
@Transactional(propagation=Propagation.SUPPORTS)
public class EntityServiceImpl implements EntityService {
	
	@Autowired
	private EntityDao entityDao;
	
	/**
	 * Copy the state of the given object onto the persistent object with the same identifier. 
	 */
	@Override
	public Object merge(Object entity) {
		return entityDao.merge(entity);
	}
	
	@Override
	public <T> List<T> loadAll(Class<T> entityClass) {
		return entityDao.loadAll(entityClass);
	}

	@Override
	public void evict(Object entity) {
		this.entityDao.evict(entity);
	}
}
