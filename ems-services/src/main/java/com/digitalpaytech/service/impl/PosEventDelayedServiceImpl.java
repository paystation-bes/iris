package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventDelayed;
import com.digitalpaytech.service.PosEventDelayedService;

@Service("posEventDelayedService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosEventDelayedServiceImpl implements PosEventDelayedService {
    
    private static final String CUSTOMER_ALERT_TYPE_ID = "customerAlertTypeId";
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public Collection<PosEventDelayed> findByDelayedGmtPast() {
        @SuppressWarnings("unchecked")
        final List<PosEventDelayed> events =
                this.entityDao.findByNamedQueryAndNamedParam("POSEventDelayed.findDelayedGMTPast", "currentTime", new Date());
        
        if (events == null) {
            return new ArrayList<>();
        } else {
            return events;
        }
    }
    
    @Override
    public void delete(final PosEventDelayed delayedEvent) {
        if (delayedEvent != null) {
            this.entityDao.delete(delayedEvent);
        }
    }
    
    @Override
    public void save(final PosEventDelayed delayedEvent) {
        if (delayedEvent != null) {
            this.entityDao.save(delayedEvent);
        }
    }
    
    @Override
    public void deleteByCustomerAlertType(final CustomerAlertType customerAlertType) {
        if (customerAlertType != null && customerAlertType.getId() != null) {
            this.entityDao.deleteByNamedQueryAndNamedParams("POSEventDelayed.deleteByCustomerAlertType", new String[] { CUSTOMER_ALERT_TYPE_ID },
                                                            new Object[] { customerAlertType.getId() });
        }
    }
    
    @Override
    public Collection<PosEventDelayed> findByCustomerAlertTypeAndPointOfSaleAndEventTypeId(final CustomerAlertType customerAlertType,
        final PointOfSale pointOfSale, final byte eventTypeId) {
        if (customerAlertType == null || customerAlertType.getId() == null || pointOfSale == null || pointOfSale.getId() == null) {
            return new ArrayList<>();
        }
        
        @SuppressWarnings("unchecked")
        final List<PosEventDelayed> events =
                this.entityDao.findByNamedQueryAndNamedParam("POSEventDelayed.findByCustomerAlertTypeAndPointOfSaleAndEventType",
                                                             new String[] { CUSTOMER_ALERT_TYPE_ID, "pointOfSaleId", "eventTypeId" }, new Object[] {
                                                                 customerAlertType.getId(), pointOfSale.getId(), eventTypeId },
                                                             true);
        
        if (events == null) {
            return new ArrayList<>();
        } else {
            return events;
        }
    }
}
