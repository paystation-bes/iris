package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.CustomerStatusTypeService;

@Service("customerStatusTypeService")
public class CustomerStatusTypeServiceImpl implements CustomerStatusTypeService {
    
    @Autowired
    private EntityService entityService;
    
    private List<CustomerStatusType> CustomerStatusTypes;
    private Map<Integer, CustomerStatusType> CustomerStatusTypesMap;
    private Map<String, CustomerStatusType> CustomerStatusTypeNamesMap;
    
    @Override
    public List<CustomerStatusType> loadAll() {
        if (CustomerStatusTypes == null) {
            CustomerStatusTypes = entityService.loadAll(CustomerStatusType.class);
        }
        return CustomerStatusTypes;
    }
    
    @Override
    public Map<Integer, CustomerStatusType> getCustomerStatusTypesMap() {
        if (CustomerStatusTypes == null) {
            loadAll();
        }
        if (CustomerStatusTypesMap == null) {
            CustomerStatusTypesMap = new HashMap<Integer, CustomerStatusType>(CustomerStatusTypes.size());
            Iterator<CustomerStatusType> iter = CustomerStatusTypes.iterator();
            while (iter.hasNext()) {
                CustomerStatusType CustomerStatusType = iter.next();
                CustomerStatusTypesMap.put(CustomerStatusType.getId(), CustomerStatusType);
            }
        }
        return CustomerStatusTypesMap;
    }
    
    @Override
    public CustomerStatusType findCustomerStatusType(String name) {
        if (CustomerStatusTypes == null) {
            loadAll();
            
            CustomerStatusTypeNamesMap = new HashMap<String, CustomerStatusType>(CustomerStatusTypes.size());
            Iterator<CustomerStatusType> iter = CustomerStatusTypes.iterator();
            while (iter.hasNext()) {
                CustomerStatusType CustomerStatusType = iter.next();
                CustomerStatusTypeNamesMap.put(CustomerStatusType.getName().toLowerCase(), CustomerStatusType);
            }
        }
        return CustomerStatusTypeNamesMap.get(name.toLowerCase());
    }
    
    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }
    
    @Override
    public String getText(int CustomerStatusTypeId) {
        if (CustomerStatusTypesMap == null)
            getCustomerStatusTypesMap();
        
        if (!CustomerStatusTypesMap.containsKey(CustomerStatusTypeId))
            return null;
        else
            return CustomerStatusTypesMap.get(CustomerStatusTypeId).getName();
    }
    
    @Override
    public CustomerStatusType findCustomerStatusType(int CustomerStatusTypeId) {
        if (CustomerStatusTypesMap == null)
            getCustomerStatusTypesMap();
        
        if (!CustomerStatusTypesMap.containsKey(CustomerStatusTypeId))
            return null;
        else
            return CustomerStatusTypesMap.get(CustomerStatusTypeId);
    }
}
