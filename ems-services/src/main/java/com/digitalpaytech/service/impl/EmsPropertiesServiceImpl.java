package com.digitalpaytech.service.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.EmsProperties;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.WebCoreConstants;

@Component("emsPropertiesService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class EmsPropertiesServiceImpl implements EmsPropertiesService {
    
    private static final Logger LOGGER = Logger.getLogger(EmsPropertiesServiceImpl.class);
    private static final String OS_TYPE = System.getProperty("os.name");
    private static final String WINDOWS_STRING = "Windows";
    private Properties properties;
    private Properties versionProperties;
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setOpenSessionDao(final OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setProperties(final Properties properties) {
        this.properties = properties;
    }
    
    public final String getPropertyValue(final String propertyName) {
        return this.properties.getProperty(propertyName);
    }
    
    public final String getPropertyValue(final String propertyName, final String defaultValue) {
        return getPropertyValue(propertyName, defaultValue, false);
    }
    
    public String getPropertyValue(final String propertyName, final String defaultValue, final boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = this.properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
    
    public final String getPropertyValue(final String propertyName, final boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = this.properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        return value;
    }
    
    public final int getPropertyValueAsInt(final String propertyName, final int defaultValue) {
        return getPropertyValueAsInt(propertyName, defaultValue, false);
    }
    
    public int getPropertyValueAsInt(final String propertyName, final int defaultValue, final boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = this.properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException nfe) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
        
    }

    public final long getPropertyValueAsLong(final String propertyName, final long defaultValue) {
        return getPropertyValueAsLong(propertyName, defaultValue, false);
    }
    
    public long getPropertyValueAsLong(final String propertyName, final long defaultValue, final boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = this.properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        if (value != null) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException nfe) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
        
    }

    /**
     * If unable to parse the database value or it doesn't exist, returns the default value without throwing exception.
     * 
     * @param propertyName
     *            EmsProperties record name.
     * @param defaultValue
     *            Value to return if property value doesn't exist or couldn't parse data.
     * @return int recorded value in database or default value.
     */
    public int getPropertyValueAsIntQuiet(final String propertyName, final int defaultValue) {
        return getPropertyValueAsInt(propertyName, defaultValue, false);
    }
    
    @PostConstruct
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public final void findPropertiesFile() {
        this.properties = new Properties();
        this.properties.clear();
        final Iterator iterator = this.openSessionDao.findByNamedQuery("EmsProperties.findAll").iterator();
        
        while (iterator.hasNext()) {
            final EmsProperties property = (EmsProperties) iterator.next();
            this.properties.put(property.getName(), property.getValue());
        }
        
        if (WebCoreConstants.MAPPING_URL == null) {
            WebCoreConstants.MAPPING_URL = (String) this.properties.get(MAPPING_URL_PROPERTY_NAME);
        }
        
        this.versionProperties = new Properties();
        try {
            this.versionProperties.load(this.getClass().getResourceAsStream(WebCoreConstants.STATIC_CONTENT_PATHS_PROPERTIES_FILE_NAME));
        } catch (IOException e) {
            LOGGER.error("Unable to load version properties (" + WebCoreConstants.STATIC_CONTENT_PATHS_PROPERTIES_FILE_NAME
                         + "). Iris's UI might not be useable !", e);
        }
        
        printProperties();
    }
    
    private void printProperties() {
        if (!LOGGER.isDebugEnabled()) {
            return;
        }
        
        this.properties.list(System.out);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    private String findByName(final String name) {
        final EmsProperties emsProperties = (EmsProperties) this.openSessionDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName",
                                                                                                                    "name", name);
        if (emsProperties == null) {
            return null;
        } else {
            return emsProperties.getValue();
        }
    }
    
    public final boolean isWindowsOS() {
        return OS_TYPE.indexOf(WINDOWS_STRING) >= 0;
    }
    
    public void updatePrimaryServer(final String newPrimaryServer) {
        final Map<String, String> updateList = new HashMap<String, String>();
        updateList.put(EmsPropertiesService.SCHEDULED_CARD_PROCESS_SERVER, newPrimaryServer);
        updateList.put(EmsPropertiesService.AUTO_CRYPTO_KEY_PROCESS_SERVER, newPrimaryServer);
        
        final Set<String> keys = updateList.keySet();
        for (String key : keys) {
            final EmsProperties p = (EmsProperties) this.entityDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", key);
            if (p == null) {
                this.entityDao.save(new EmsProperties(key, updateList.get(key)));
            } else {
                p.setValue(updateList.get(key));
                this.entityDao.update(p);
            }
        }
        loadPropertiesFile();
    }
    
    @Override
    public void updatePatchExecution(final boolean execute) {
        final String key = EXECUTE_PATCH;
        final String value = execute ? "1" : "0";
        
        final EmsProperties p = (EmsProperties) this.entityDao.findUniqueByNamedQueryAndNamedParam("EmsProperties.findByName", "name", key);
        if (p == null) {
            this.entityDao.save(new EmsProperties(key, value));
        } else {
            p.setValue(value);
            this.entityDao.update(p);
        }
        
        loadPropertiesFile();
    }

    public final void loadPropertiesFile() {
        this.properties = new Properties();
        this.properties.clear();
        final Collection<EmsProperties> emsPropertiesList = this.entityDao.findByNamedQuery("EmsProperties.findAll");
        
        for (EmsProperties property : emsPropertiesList) {
            this.properties.put(property.getName(), property.getValue());
        }
        
        printProperties();
    }
    
    public final boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue) {
        return getPropertyValueAsBoolean(propertyName, defaultValue, false);
    }
    
    public boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue, final boolean forceGet) {
        String value = null;
        if (!forceGet) {
            value = this.properties.getProperty(propertyName);
        } else {
            value = findByName(propertyName);
        }
        if (value != null) {
            return "1".equals(value);
        } else {
            return defaultValue;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final <V> V getProperty(final String key, final Class<V> valueClass, final V defaultValue) {
        V result = null;
        
        String buffer = null;
        
        buffer = this.getPropertyValue(key);
        if (buffer == null) {
            result = defaultValue;
        } else {
            result = (V) ConvertUtils.convert(buffer, valueClass);
        }
        
        return result;
    }
    
    @Override
    public final String getIrisVersion() {
        return this.versionProperties.getProperty(WebCoreConstants.VERSION);
    }
    
    @Override
    public final String getIrisBuildNumber() {
        return this.versionProperties.getProperty(WebCoreConstants.BUILD_NUMBER);
    }
    
    @Override
    public final String getIrisBuildDate() {
        return this.versionProperties.getProperty(WebCoreConstants.BUILD_DATE);
    }
    
    @Override
    public final String getStaticContentPath() {
        return this.versionProperties.getProperty(WebCoreConstants.HASHED_VERSION);
    }
    
    @Override
    public final String getStaticContentPatam() {
        return this.versionProperties.getProperty(WebCoreConstants.HASHED_BUILD_DATE);
    }

    @Override
    public final String getIrisDeploymentMode() {
        return this.versionProperties.getProperty(WebCoreConstants.DEPLOYMENT_MODE);
    }
}
