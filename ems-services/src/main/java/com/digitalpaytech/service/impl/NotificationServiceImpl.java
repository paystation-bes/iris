package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.NotificationService;

@Component("notificationService")
@Transactional(propagation=Propagation.REQUIRED)
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private EntityDao entityDao;
	
	@Autowired
	private EmsPropertiesService emsPropertiesService;
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}

	public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
		this.emsPropertiesService = emsPropertiesService;
	}
	
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Notification> findNotificationByEffectiveDate() {
		return entityDao.findByNamedQueryAndNamedParam("Notification.findNotificationByEffectiveDate", "currentTime", new Date(), true);
	}

    @Override
    public List<Notification> findNotificationAllCurrentAndFuture() {
        return entityDao.findByNamedQueryAndNamedParam("Notification.findNotificationAllCurrentAndFuture", "currentTime", new Date(), true);
    }


	/**
	 * Number of past notifications are limited which is defined in database EmsProperties table, name 'MaximumPastCustomerNotifications'.
	 * @return List<Notification> List of past notification objects.  
	 */
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Notification> findNotificationInPast() {

		String limitValue = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_PAST_CUSTOMER_ADMIN_NOTIFICATIONS_LIMIT, 
				String.valueOf(EmsPropertiesService.MAX_DEFAULT_PAST_CUSTOMER_ADMIN_NOTIFICATIONS_LIMIT), false);
		
		return entityDao.findPageResultByNamedQuery(
				"Notification.findNotificationInPast",
				new String[] { "currentTime" }, new Object[] { new Date() }, true, 0,
				Integer.parseInt(limitValue));
	}
	
	@Transactional(propagation=Propagation.SUPPORTS)
	public Notification findNotificationById(Integer id, boolean doEvict) {
		Notification notification = (Notification) entityDao.get(Notification.class, id);
		if (doEvict) {
			entityDao.evict(notification);
		}
		return notification;
	}
	
	public void saveNotification(Notification notification) {
		entityDao.save(notification);
	}

	public void updateNotification(Notification notification) {
		entityDao.update(notification);
	}

}
