package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MobileDevice;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileLicenseStatusType;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.MobileDeviceService;
import com.digitalpaytech.util.WebCoreConstants;

import org.springframework.transaction.annotation.Propagation;
@Service("mobileDeviceService")
public class MobileDeviceServiceImpl implements MobileDeviceService {
	@Autowired
	EntityDao entityDao;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	MobileLicenseService mobileAppService;
	
	@Override
	public MobileDevice findMobileDeviceByUid(String uid){
		return (MobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("MobileDevice.findMobileDeviceByUid", "uid", uid);
	}
	
	@Override
	public MobileDevice findMobileDeviceById(Integer id){
	    return (MobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("MobileDevice.findMobileDevicebyId", "id", id);
	}
	
	@Override
	public boolean getIsMobileDeviceBlocked(Integer customerId, Integer mobileDeviceId){
	    long count = (Long) entityDao.findUniqueByNamedQueryAndNamedParam("MobileDevice.CountBlockedByCustomerAndDevice", new String[]{"customerId", "deviceId"}, new Object[]{customerId, mobileDeviceId}, true);
	    if(count > 0){
	        return true;
	    }
	    return false;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveMobileDevice(MobileDevice mobileDevice){
	    entityDao.save(mobileDevice);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteMobileDevice(MobileDevice mobileDevice) {
        entityDao.delete(mobileDevice);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateMobileDevice(MobileDevice mobileDevice) {
        entityDao.saveOrUpdate(mobileDevice);
    }
}
