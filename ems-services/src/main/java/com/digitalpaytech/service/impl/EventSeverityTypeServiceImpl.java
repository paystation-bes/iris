package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.service.EventSeverityTypeService;

@Service("eventSeverityTypeService")
public class EventSeverityTypeServiceImpl implements EventSeverityTypeService {
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    private Map<Integer, EventSeverityType> map;
    
    @PostConstruct
    private void loadAllToMap() {
        final List<EventSeverityType> list = this.openSessionDao.loadAll(EventSeverityType.class);
        this.map = new HashMap<Integer, EventSeverityType>(list.size());
        
        final Iterator<EventSeverityType> iter = list.iterator();
        while (iter.hasNext()) {
            final EventSeverityType esType = iter.next();
            this.map.put(esType.getId(), esType);
        }
    }
    
    @Override
    public final EventSeverityType findEventSeverityType(final Integer id) {
        return this.map.get(id);
    }
}
