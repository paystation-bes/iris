package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.ActiveAlertSearchCriteria;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.AlertMapEntryMinimalTransformer;
import com.digitalpaytech.dto.AlertMapEntryTransformer;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.AlertSummaryEntryTransformer;
import com.digitalpaytech.dto.CollectionSummaryEntryTransformer;
import com.digitalpaytech.dto.OrderBySqlFormula;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntryTransformer;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ItemLocatorCriterionAggregator;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.ValueProxy;

@Service("alertsService")
@Transactional(propagation = Propagation.REQUIRED)
public class AlertsServiceImpl implements AlertsService {
    /*
     * This part of SQL is for filter out non-interested records. This will later union with SQL_HISTORY_TEMINATOR to create a full list of POSEventHistory.
     * The purpose of having the SQL conditions here is to improve performance by eliminate non-interested records.
     * 
     * PS 1: All other conditions that is not standard will be injected via {2}. The join expressions required for those conditions should be included in {1}.
     * 
     * PS 2: {5} is the BoundaryState value which is used to track whether we hit the end of the data set.
     * 
     */
    private static final String SQLFMT_HISTORY_FILTER = "         SELECT peh.Id "
                                                      + "            , peh.PointOfSaleId "
                                                      + "            , peh.EventTypeId "
                                                      + "            , peh.CustomerAlertTypeId "
                                                      + "            , peh.EventSeverityTypeId "
                                                      + "            , peh.EventActionTypeId "
                                                      + "            , peh.IsActive "
                                                      + "            , peh.LastModifiedByUserId "
                                                      + "            , peh.CreatedGMT "
                                                      + "            , peh.TimestampGMT  "
                                                      + "            {5} "
                                                      + "         FROM POSEventHistory peh "
                                                      + "            LEFT JOIN CustomerAlertType cat ON(peh.CustomerAlertTypeId = cat.Id) "
                                                      + "            LEFT JOIN PointOfSale pos ON(peh.PointOfSaleId = pos.Id) "
                                                      + "            LEFT JOIN POSStatus pStat ON(pos.Id = pStat.PointOfSaleId) "
                                                      + "            {1} "
                                                      + "         WHERE (pStat.IsDeleted = 0) AND (pStat.IsDecommissioned = 0) AND (pStat.IsActivated = 1) AND (pStat.IsVisible = 1) "
                                                      + "            AND (peh.CreatedGMT < :maxCreatedGMT) AND (peh.TimestampGMT >= :minTimestampGMT) "
                                                      + "            {2} ";
    
    /* 
     * This part of SQL is to create a blank record to mark the end of the records.
     * The reason behind this is because the active alerts are matched with cleared alerts in the next row.
     * For example, if we have active -> clear -> active, the last active will be left un-matched because there is no other record after it.
     * So, if we insert this terminator to make it active -> clear -> active -> active (terminator), we can dump out the last status on this terminator record.
     * 
     */
    private static final String SQL_HISTORY_TERMINATOR = "         SELECT NULL AS Id "
                                                       + "            , :maxInt AS PointOfSaleId "
                                                       + "            , :maxInt AS EventTypeId "
                                                       + "            , :maxInt AS CustomerAlertTypeId "
                                                       + "            , 3 AS EventSeverityTypeId "
                                                       + "            , :maxInt AS EventActionTypeId "
                                                       + "            , 1 AS IsActive "
                                                       + "            , 1 AS LastModifiedByUserId "
                                                       + "            , DATE_ADD(:maxCreatedGMT, INTERVAL -1 MICROSECOND) AS CreatedGMT "
                                                       + "            , DATE_ADD(:maxCreatedGMT, INTERVAL -1 MICROSECOND) AS TimestampGMT "
                                                       + "            , NULL AS BoundaryState ";
    
    /*
     * This part of SQL is to match between active alert to its corresponding cleared alert. This will also tract the severity elevation (changes in EventActionType).
     * The idea here is that if we order the whole data set by PointOfSale, EventType, CustomerAlertType, Timestamp
     * , then we can step through each record to determine whether the alert is clear or the severity get elevated.
     * 
     * To be able to compare previous record to the current record as we step through each records, we defined variables (@XXX) to track previous record.
     * 
     * There are some special variable to track the state of the matchings.
     *  - @complete: if this is 1, it means that the current event is completed (alerted and cleared).
     *  - @changedACT: this is to indicate thate EventActionType has been changed.
     *  - @typeCntr: this is the counter that keep increasing when we step through more records with the same event.
     *  The severity elavations will not increase this value because they considered to be the same event.
     *  - @selectCntr: this is basically the "rownum"
     *  - @alertId: is the last active alert (POSEventHistory's ID).
     *  - @activeAlertId: is the last active alert before we start the new type. This is basically the id of the current active alert.
     * 
     * Some notes on previous record variable with special values.
     *  - @prevIsActive: Normally, this will either be 1 or 0 according to POSEventHistory.IsActive.
     *  However, it will become 2 to indicate that we have reached the "BoundaryState".
     * 
     */
    private static final String SQLFMT_HISTORY_MATCH_MAKER = "   SELECT peh.Id AS RecordId "
                                                           + "      , @complete |= 0 "
                                                           + "      , @curCAT |= IFNULL(peh.CustomerAlertTypeId, -1) "
                                                           + "      , @curACT |= IFNULL(peh.EventActionTypeId, -1) "
                                                           + "      , CASE WHEN @selectCntr = 1 THEN (@alertId |= peh.Id) + (@prevPOS |= peh.PointOfSaleId) + (@prevET |= peh.EventTypeId) + (@prevCAT |= @curCAT) + (@prevACT |= @curACT) + (@prevIsActive |= peh.IsActive) END "
                                                           + "      , CASE WHEN @prevIsActive = 0 THEN @prevACT |= @curACT END "
                                                           + "      , CASE "
                                                           + "         WHEN (@prevET <> peh.EventTypeId) OR (@prevCAT <> @curCAT) OR (@prevPOS <> peh.PointOfSaleId) "
                                                           + "         THEN @typeCntr |= 0 "
                                                           + "         ELSE @typeCntr |= @typeCntr + 1 "
                                                           + "         END AS countPerType "
                                                           + "      , CASE WHEN (@typeCntr = 0) AND (@prevIsActive = 1) THEN @activeAlertId |= @alertId ELSE @activeAlertId |= NULL END "
                                                           + "      , CASE "
                                                           + "         WHEN @typeCntr = 0 "
                                                           + "         THEN CASE "
                                                           + "            WHEN (BoundaryState IS NOT NULL) AND (peh.IsActive) AND (SUBSTRING(BoundaryState, -1) = '1') "
                                                           + "            THEN @prevIsActive |= 2 "
                                                           + "            ELSE (@prevIsActive |= peh.IsActive) + CASE WHEN peh.IsActive = 1 THEN @alertId |= peh.Id END "
                                                           + "            END "
                                                           + "         ELSE CASE  "
                                                           + "            WHEN @prevIsActive = 2 "
                                                           + "            THEN CASE WHEN peh.IsActive = 0 THEN @prevIsActive |= 0 END "
                                                           + "            ELSE CASE "
                                                           + "               WHEN @prevIsActive <> peh.IsActive "
                                                           + "               THEN (@prevIsActive |= peh.IsActive) + CASE WHEN peh.IsActive = 0 THEN @complete |= 1 ELSE (@complete |= 0) + (@alertId |= peh.Id) END "
                                                           + "               END "
                                                           + "            END "
                                                           + "         END AS PrevActiveFlag "
                                                           + "      , CASE WHEN (@typeCntr <> 0) AND (@complete = 0) AND (peh.IsActive = 1) AND (@prevACT <> @curACT) THEN (@changedACT |= 1) + (@complete |= 1) ELSE @changedACT |= 0 END"
                                                           + "      , CAST(CASE WHEN @complete = 1 THEN @alertId ELSE @activeAlertId END AS UNSIGNED) AS AlertId "
                                                           + "      , CAST(CASE WHEN @complete = 1 THEN peh.TimestampGMT ELSE NULL END AS DATETIME) AS ClearGMT "
                                                           + "      , CASE WHEN @complete = 1 THEN peh.LastModifiedByUserId END AS ResolvedByUserId "
                                                           + "      , CAST(CASE WHEN @complete = 1 THEN 0 ELSE 1 END AS UNSIGNED) IsActive "
                                                           + "      , CASE WHEN @complete = 1 THEN CASE WHEN @changedACT = 0 THEN @alertId |= NULL ELSE @alertId |= peh.Id END END "
                                                           + "      , CASE WHEN @typeCntr = 0 THEN @prevPOS |= peh.PointOfSaleId END "
                                                           + "      , CASE WHEN @typeCntr = 0 THEN @prevET |= peh.EventTypeId END "
                                                           + "      , CASE WHEN @typeCntr = 0 THEN @prevCAT |= @curCAT END "
                                                           + "      , CASE WHEN (@typeCntr = 0) OR (@changedACT = 1) THEN @prevACT |= @curACT END "
                                                           + "      , @selectCntr |= @selectCntr + 1 "
                                                           + "   FROM ( "
                                                           + SQLFMT_HISTORY_FILTER
                                                           + "         UNION ALL "
                                                           + SQL_HISTORY_TERMINATOR
                                                           + "      ) peh "
                                                           + "      , (SELECT @typeCntr |= 0, @selectCntr |= 1) tmp "
                                                           + "   ORDER BY peh.PointOfSaleId, peh.EventTypeId, peh.CustomerAlertTypeId, peh.TimestampGMT, peh.IsActive DESC ";
    
    /*
     * This is the main SQL. All the partial SQLs are aggregated here.
     * 
     * PS: This is start with '|' to tell hibernate that we want to trigger SQLInterceptor to replace all "|=" to ":=".
     * 
     */
    private static final String SQLFMT_HISTORY_MAIN = "|SELECT alert.Id AS Id "
                                                    + "   , alert.PointOfSaleId AS PointOfSaleId "
                                                    + "   , att.AlertTypeId AS AlertTypeId "
                                                    + "   , alertType.Name AS AlertTypeName "
                                                    + "   , alert.EventTypeId AS EventTypeId "
                                                    + "   , et.Description AS EventTypeName "
                                                    + "   , et.EventDeviceTypeId AS DeviceId "
                                                    + "   , edt.Name AS DeviceName "
                                                    + "   , alert.CustomerAlertTypeId AS CustomerAlertId "
                                                    + "   , cat.Name AS CustomerAlertName "
                                                    + "   , alert.EventSeverityTypeId AS SeverityId "
                                                    + "   , est.Name AS SeverityName "
                                                    + "   , alert.EventActionTypeId AS ActionTypeId "
                                                    + "   , act.Name AS ActionTypeName "
                                                    + "   , history.IsActive AS IsActive "
                                                    + "   , alert.TimestampGMT AS AlertGMT "
                                                    + "   , history.RecordId AS ClearId "
                                                    + "   , history.ClearGMT AS ClearGMT "
                                                    + "   , CASE WHEN ua.Id = 1 THEN ''System'' ELSE CONCAT_WS('' '', ua.FirstName, ua.LastName) END AS LastModifiedByUserName "
                                                    + "   {0} "
                                                    + "FROM ( "
                                                    + SQLFMT_HISTORY_MATCH_MAKER
                                                    + ") history "
                                                    + "   INNER JOIN POSEventHistory alert ON((history.AlertId IS NOT NULL) AND (history.AlertId = alert.Id)) "
                                                    + "   LEFT JOIN EventType et ON(alert.EventTypeId = et.Id) "
                                                    + "   LEFT JOIN EventDeviceType edt ON(et.EventDeviceTypeId = edt.Id) "
                                                    + "   LEFT JOIN CustomerAlertType cat ON(alert.CustomerAlertTypeId = cat.Id) "
                                                    + "   LEFT JOIN AlertThresholdType att ON(cat.AlertThresholdTypeId = att.Id) "
                                                    + "   LEFT JOIN AlertType alertType ON(att.AlertTypeId = alertType.Id) "
                                                    + "   LEFT JOIN EventSeverityType est ON(alert.EventSeverityTypeId = est.Id) "
                                                    + "   LEFT JOIN EventActionType act ON(alert.EventActionTypeId = act.Id)  "
                                                    + "   LEFT JOIN PointOfSale pos ON(alert.PointOfSaleId = pos.Id) "
                                                    + "   LEFT JOIN Paystation ps ON(pos.PaystationId = ps.Id) "
                                                    + "   LEFT JOIN UserAccount ua ON (history.ResolvedByUserId = ua.Id) "
                                                    + "WHERE (history.AlertId IS NOT NULL) " + "{3} " + "{4} ";
    
    /*
     * For things that need BoundaryState. This is no longer needed.
     * 
     */
    private static final String SQLFMT_HISTORY_BOUNDARY_SEL = ", bound.BoundaryState AS BoundaryState ";
    
    /*
     * For things that do not need BoundaryState.
     * 
     */
    private static final String SQLFMT_HISTORY_BOUNDARY_SEL_EMPTY = ", NULL AS BoundaryState ";
    
    /*
     * This is the actual join for BoundaryState. This is basically to catch the last record of each type of Event.
     * This query will select data before 90 days. So, with archive process, this is not needed
     * 
     * PS: I am not sure if we still need to include EventActionTypeId here.
     * 
     */
    private static final String SQLFMT_HISTORY_BOUNDARY_JOIN = "      LEFT JOIN ( "
                                                              + "         SELECT peh.PointOfSaleId, peh.EventTypeId, peh.CustomerAlertTypeId, peh.EventActionTypeId, MAX(CONCAT(peh.TimestampGMT, peh.IsActive)) AS BoundaryState "
                                                              + "         FROM POSEventHistory peh "
                                                              + "            LEFT JOIN CustomerAlertType cat ON(peh.CustomerAlertTypeId = cat.Id) "
                                                              + "            LEFT JOIN POSStatus pStat ON(peh.PointOfSaleId = pStat.PointOfSaleId) "
                                                              + "            LEFT JOIN PointOfSale pos ON(peh.PointOfSaleId = pos.Id) "
                                                              + "            {0} "
                                                              + "         WHERE (pStat.IsDeleted = 0) AND (pStat.IsDecommissioned = 0) AND (pStat.IsActivated = 1) AND (pStat.IsVisible = 1) "
                                                              + "            AND (peh.CreatedGMT < :maxCreatedGMT) AND (peh.TimestampGMT < :minTimestampGMT) "
                                                              + "            {1} "
                                                              + "         GROUP BY peh.PointOfSaleId, peh.EventTypeId, peh.CustomerAlertTypeId, peh.EventActionTypeId "
                                                              + "      ) bound ON(peh.PointOfSaleId = bound.PointOfSaleId "
                                                              + "         AND peh.EventTypeId = bound.EventTypeId "
                                                              + "         AND ((peh.CustomerAlertTypeId = bound.CustomerAlertTypeId) OR ((peh.CustomerAlertTypeId IS NULL) AND (bound.CustomerAlertTypeId IS NULL))) "
                                                              + "         AND ((peh.EventActionTypeId = bound.EventActionTypeId) OR ((peh.EventActionTypeId IS NULL) AND (bound.EventActionTypeId IS NULL)))) ";
    
    /*
     * This part of SQL is to add fields for alert center.
     * 
     */
    private static final String SQL_SEL_ALERT_CENTER = "   , pos.Name AS PointOfSaleName "
                                                     + "   , pos.SerialNumber AS SerialNumber "
                                                     + "   , pos.Latitude AS Latitude " + "   , pos.Longitude AS Longitude "
                                                     + "   , ps.PaystationTypeId AS PayStationType ";
    
    /*
     * The purpose of this SQL is for pagination performance improvements.
     * The idea here is if we know the number of records and the first and latest time we have data, we can calculate the average number of records distributed per time range.
     * So, we can translate from page number to the point in time instead of number of records. This will reduce the number of SQL call that java has to make to fill the current page.
     * 
     */
    private static final String SQLFMT_HISTORY_SCOPE = "SELECT MAX(peh.TimestampGMT) AS dataMaxTS, MIN(peh.TimestampGMT) AS dataMinTS, COUNT(peh.Id) AS dataCount "
                                                     + "FROM POSEventHistory peh "
                                                     + "   LEFT JOIN CustomerAlertType cat ON(peh.CustomerAlertTypeId = cat.Id) "
                                                     + "   LEFT JOIN PointOfSale pos ON(peh.PointOfSaleId = pos.Id) "
                                                     + "   LEFT JOIN POSStatus pStat ON(pos.Id = pStat.PointOfSaleId) "
                                                     + "   {0} "
                                                     + "WHERE (pStat.IsDeleted = 0) AND (pStat.IsDecommissioned = 0) AND (pStat.IsActivated = 1) AND (pStat.IsVisible = 1) "
                                                     + "   AND (peh.CreatedGMT < :maxCreatedGMT) AND (peh.TimestampGMT < :currentTimestampGMT) "
                                                     + "   {1} ";
    
    private static final String ORDER_SEVERITY = "severity";
    private static final String ORDER_ALERT_DATE = "alertDate";
    private static final String ORDER_RESOLVED_DATE = "resolvedDate";
    private static final String ORDER_ALERT_STATUS = "alertStatus";
    private static final String ORDER_TIME = "time";
    
    private static final String ORDER_ASC = "ASC";
    private static final String ORDER_DESC = "DESC";
    
    private static final Map<String, String> HISTORY_ORDER_BY_MAP = new HashMap<String, String>();
    static {
        HISTORY_ORDER_BY_MAP.put(WebCoreConstants.EMPTY_STRING,
                                 " ORDER BY history.IsActive DESC, history.ClearGMT DESC, alert.EventSeverityTypeId DESC, alert.TimestampGMT DESC ");
        HISTORY_ORDER_BY_MAP.put(ORDER_SEVERITY, " ORDER BY alert.EventSeverityTypeId {0}, history.IsActive DESC, alert.TimestampGMT DESC ");
        HISTORY_ORDER_BY_MAP.put(ORDER_ALERT_DATE, " ORDER BY alert.TimestampGMT {0}, alert.EventSeverityTypeId DESC ");
        HISTORY_ORDER_BY_MAP.put(ORDER_RESOLVED_DATE,
                                 " ORDER BY history.ClearGMT {0}, alert.TimestampGMT DESC, alert.EventSeverityTypeId DESC, history.IsActive DESC ");
        HISTORY_ORDER_BY_MAP.put(ORDER_ALERT_STATUS,
                                 " ORDER BY history.IsActive {0}, history.ClearGMT DESC, alert.EventSeverityTypeId DESC, alert.TimestampGMT DESC ");
        HISTORY_ORDER_BY_MAP.put(ORDER_TIME, " ORDER BY history.ClearGMT {0}, alert.Id DESC ");
    }
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    @Override
    public final EventDefinition findEventDefinitionByDeviceStatusActionSeverityId(final int deviceTypeId, final int statusTypeId,
        final int actionTypeId, final int severityTypeId) {
        
        final String[] params = { HibernateConstants.DEVICE_TYPE_ID, HibernateConstants.STATUS_TYPE_ID, HibernateConstants.ACTION_TYPE_ID,
            HibernateConstants.SEVERITY_TYPE_ID, };
        final Object[] args = { deviceTypeId, statusTypeId, actionTypeId, severityTypeId };
        
        @SuppressWarnings("unchecked")
        final List<EventDefinition> eventDefinitions = this.entityDao
                .findByNamedQueryAndNamedParam("EventDefinition.findEventDefinitionByDeviceStatusActionSeverityId", params, args, true);
        if (eventDefinitions != null && eventDefinitions.size() > 0) {
            return eventDefinitions.get(0);
        }
        return null;
    }
    
    @Override
    public final EventDefinition findEventDefinitionById(final int eventDefinitionId) {
        
        return (EventDefinition) this.entityDao.findUniqueByNamedQueryAndNamedParam("EventDefinition.findEventDefinitionById", "eventDefinitionId",
                                                                                    eventDefinitionId, true);
    }
    
    @Override
    public final List<EventDeviceType> findAllEventDeviceTypes() {
        return this.entityDao.loadAll(EventDeviceType.class);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<EventDeviceType> findAllEventDeviceTypesExceptPCM() {
        return this.entityDao.findByNamedQuery("EventDeviceType.fildAllTypesExceptPCM", true);
    }
    
    @Override
    public final EventDeviceType findEventDeviceTypeById(final int deviceTypeId) {
        return (EventDeviceType) this.entityDao.get(EventDeviceType.class, deviceTypeId);
    }
    
    @Override
    public final List<EventSeverityType> findAllEventSeverityTypes() {
        return this.entityDao.loadAll(EventSeverityType.class);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<AlertMapEntry> findPointOfSalesWithAlertMap(final ActiveAlertSearchCriteria criteria) {
        final ProjectionList selections = Projections.projectionList().add(Projections.property("pos.serialNumber").as("psSerialNumber"))
                .add(Projections.property("psType.id").as("psType")).add(Projections.property("pos.latitude").as("latitude"))
                .add(Projections.property("pos.longitude").as("longitude")).add(Projections.property("pos.name").as("posName"))
                .add(Projections.property("pos.name").as("posName"));
        return createFindPayStationsOnMapWithAlertQuery(criteria, selections, (AlertEntry) null)
                .setResultTransformer(new AlertMapEntryMinimalTransformer()).list();
    }
    
    @Override
    public final List<CollectionSummaryEntry> findPointOfSalesWithBalanceAndAlertSeverity(final SummarySearchCriteria criteria, final String timeZone) {
        final ProjectionList selections = Projections.projectionList();
        selections.add(Projections.property("serialNumber").as("psSerialNumber"));
        selections.add(Projections.property("name").as("pointOfSaleName"));
        selections.add(Projections.property("location.id").as("locationId"));
        selections.add(Projections.property("location.name").as("locationName"));
        
        selections.add(Projections.property("posBal.billAmount").as("billAmount"));
        selections.add(Projections.property("posBal.coinAmount").as("coinAmount"));
        selections.add(Projections.property("posBal.unsettledCreditCardAmount").as("cardAmount"));
        selections.add(Projections.property("posBal.totalAmount").as("totalAmount"));
        
        selections.add(Projections.property("posBal.billCount").as("billCount"));
        selections.add(Projections.property("posBal.coinCount").as("coinCount"));
        selections.add(Projections.property("posBal.unsettledCreditCardCount").as("cardCount"));
        
        selections.add(Projections.property("posBal.lastCollectionGmt").as("collectionDate"));
        
        final Map<Integer, CollectionSummaryEntry> mapEntriesHash = new HashMap<Integer, CollectionSummaryEntry>();
        final CollectionSummaryEntryTransformer transformer = new CollectionSummaryEntryTransformer(mapEntriesHash);
        
        @SuppressWarnings("unchecked")
        final List<CollectionSummaryEntry> result = createFindPayStationsWithBalanceAndAlertQuery(criteria, selections)
                .setResultTransformer(transformer).list();
        
        if (result.size() > 0) {
            
            for (CollectionSummaryEntry cse : result) {
                final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId((Integer) cse.getPosRandomId().getId(),
                                                                                                        WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
                cse.setRoutes(routeList);
            }
            
            //            @SuppressWarnings("unchecked")
            //            final List<PosEventCurrent> alertDetails = this.createFindAlertsQuery(criteria, mapEntriesHash.keySet()).list();
            //            
            //            final Iterator<PosEventCurrent> alertItr = alertDetails.iterator();
            //            while (alertItr.hasNext()) {
            //                final PosEventCurrent posEventCurrent= alertItr.next();
            //                @SuppressWarnings("unused")
            //                final CollectionSummaryEntry entry = mapEntriesHash.get(posEventCurrent.getPointOfSale().getId());
            //                /*
            //                 * if (entry != null) {
            //                 * // MATCH!
            //                 * if (entry.getAlertGMT() != null) {
            //                 * entry.setAlertDate(DateUtil.getRelativeTimeString(entry.getAlertGMT(), timeZone));
            //                 * }
            //                 * mapEntriesHash.remove(alert.getPosAlertStatus().getPointOfSaleId());
            //                 * }
            //                 */
            //            }
        }
        
        return result;
    }
    
    @Override
    public final Integer locatePageContainsPointOfSaleWithAlert(final ActiveAlertSearchCriteria criteria) {
        final ProjectionList targetProjections = Projections.projectionList().add(Projections.property("pos.name").as("psName"));
        
        final AlertEntry targetObj = (AlertEntry) createFindPayStationsOnMapWithAlertQuery(criteria, targetProjections, (AlertEntry) null)
                .add(Restrictions.eq("pos.id", criteria.getTargetId())).setResultTransformer(new AliasToBeanResultTransformer(AlertEntry.class))
                .uniqueResult();
        final double itemIdx = ((Number) createFindPayStationsOnMapWithAlertQuery(criteria, (ProjectionList) null, targetObj)
                .setProjection(Projections.projectionList().add(Projections.countDistinct("pos.id"))).uniqueResult()).doubleValue();
        
        return (itemIdx <= 0) ? -1 : (int) Math.floor(itemIdx / criteria.getItemsPerPage().doubleValue()) + 1;
    }
    
    @Override
    public final List<AlertMapEntry> findPointOfSalesWithAlertDetail(final ActiveAlertSearchCriteria criteria, final String timeZone) {
        final ProjectionList selections = Projections.projectionList().add(Projections.property("pos.serialNumber").as("psSerialNumber"))
                .add(Projections.property("pos.name").as("pointOfSaleName")).add(Projections.property("psType.id").as("psType"));
        
        final Map<Integer, AlertMapEntry> mapEntriesHash = new HashMap<Integer, AlertMapEntry>();
        final AlertMapEntryTransformer transformer = new AlertMapEntryTransformer(mapEntriesHash);
        
        @SuppressWarnings("unchecked")
        final List<AlertMapEntry> result = createFindPayStationsOnMapWithAlertQuery(criteria, selections, (AlertEntry) null)
                .setResultTransformer(transformer).list();
        
        if (result.size() > 0) {
            final Set<Integer> targetRouteTypes = new HashSet<Integer>();
            if ((criteria.getAlertThresholdTypeIds() == null) || (criteria.getAlertThresholdTypeIds().size() <= 0)) {
                targetRouteTypes.add(WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
                targetRouteTypes.add(WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
            } else {
                for (Integer thresholdTypeId : criteria.getAlertThresholdTypeIds()) {
                    if ((thresholdTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR)
                        || (thresholdTypeId == WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION)) {
                        targetRouteTypes.add(WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
                    } else {
                        targetRouteTypes.add(WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
                    }
                }
            }
            
            Collections.sort(transformer.getPosRouteNamesProxy());
            
            @SuppressWarnings("unchecked")
            final List<RoutePOS> routePOSes = this.entityDao.getNamedQuery("RoutePOS.findRoutePOSByPosId")
                    .setParameterList(HibernateConstants.POINTOFSALE_ID, mapEntriesHash.keySet()).list();
            for (ValueProxy<StringBuilder, Integer> routeNameProxy : transformer.getPosRouteNamesProxy()) {
                boolean matched = false;
                RoutePOS current = null;
                final Iterator<RoutePOS> rpItr = routePOSes.iterator();
                while ((!matched) && (rpItr.hasNext())) {
                    current = rpItr.next();
                    matched = routeNameProxy.getId().equals(current.getPointOfSale().getId());
                }
                
                while (matched) {
                    rpItr.remove();
                    if (targetRouteTypes.contains(current.getRoute().getRouteType().getId())) {
                        if ((routeNameProxy.getValue() == null) || (routeNameProxy.getValue().length() <= 0)) {
                            routeNameProxy.setValue(new StringBuilder(current.getRoute().getName()));
                        }
                    }
                    
                    if (!rpItr.hasNext()) {
                        matched = false;
                    } else {
                        current = rpItr.next();
                        matched = routeNameProxy.getId().equals(current.getPointOfSale().getId());
                    }
                }
            }
            
            @SuppressWarnings("unchecked")
            final List<PosEventCurrent> alertDetails = createFindAlertsQuery(criteria, mapEntriesHash.keySet()).list();
            
            final Iterator<PosEventCurrent> alertItr = alertDetails.iterator();
            while (alertItr.hasNext()) {
                final PosEventCurrent alert = alertItr.next();
                final AlertMapEntry entry = mapEntriesHash.get(alert.getPointOfSale().getId());
                if (entry != null) {
                    // MATCH!
                    if (alert.getCustomerAlertType() != null) {
                        if (alert.getCustomerAlertType().getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR) {
                            entry.setModule(alert.getCustomerAlertType().getName());
                        } else {
                            entry.setModule(alert.getCustomerAlertType().getAlertThresholdType().getAlertType().getName());
                        }
                    } else {
                        entry.setModule(this.messageHelper.getMessage(alert.getEventType().getEventDeviceType().getName()));
                        
                    }
                    if (entry.getAlertGMT() != null) {
                        entry.setAlertDate(DateUtil.getRelativeTimeString(entry.getAlertGMT(), timeZone));
                    }
                    mapEntriesHash.remove(alert.getPointOfSale().getId());
                }
            }
        }
        
        return result;
    }
    
    private Criteria createFindAlertsQuery(final ActiveAlertSearchCriteria criteria, final Collection<Integer> pointOfSaleIds) {
        final Criteria query = this.entityDao.createCriteria(PosEventCurrent.class);
        query.createAlias("pointOfSale", "pos");
        query.setFetchMode("pointOfSale", FetchMode.JOIN);
        query.createAlias("pos.posAlertStatus", "alertStatus");
        query.setFetchMode("pos.posAlertStatus", FetchMode.JOIN);
        query.createAlias("pos.posStatus", "posStatus");
        query.createAlias("eventType", "eventType");
        query.setFetchMode("eventType", FetchMode.JOIN);
        query.createAlias("eventType.eventDeviceType", "eventDeviceType");
        query.setFetchMode("eventType.eventDeviceType", FetchMode.JOIN);
        query.createAlias("customerAlertType", "cat", Criteria.LEFT_JOIN);
        query.createAlias("pos.paystation", "pstation");
        query.createAlias("pstation.paystationType", "psType");
        
        //        query.createAlias("alertThresholdType", "alertThresholdType");
        //        query.setFetchMode("alertThresholdType", FetchMode.JOIN);
        //        query.createAlias("alertThresholdType.alertType", "alertType");
        //        query.setFetchMode("alertThresholdType.alertType", FetchMode.JOIN);
        
        final ActiveAlertSearchCriteria.Severity translatedSeverities = criteria.translateSeverities();
        if (criteria.getMaxAlertGmt() != null) {
            query.add(Restrictions.le("alertGmt", criteria.getMaxAlertGmt()));
        }
        query.addOrder(Order.desc("alertGmt"));
        
        //        if (translatedSeverities.isCritical()) {
        //            query.createAlias("criticalAlert", "criticalAlert", Criteria.LEFT_JOIN);
        //            query.setFetchMode("criticalAlert", FetchMode.JOIN);
        //            query.createAlias("criticalAlert.customerAlertType", "critcalCustomerAlertType", Criteria.LEFT_JOIN);
        //            query.setFetchMode("criticalAlert.customerAlertType", FetchMode.JOIN);
        //            
        //            query.addOrder(Order.desc("criticalAlert.alertGmt"));
        //            
        //        }
        //        if (translatedSeverities.isMajor()) {
        //            query.createAlias("majorAlert", "majorAlert", Criteria.LEFT_JOIN);
        //            query.setFetchMode("majorAlert", FetchMode.JOIN);
        //            query.createAlias("majorAlert.customerAlertType", "majorCustomerAlertType", Criteria.LEFT_JOIN);
        //            query.setFetchMode("majorAlert.customerAlertType", FetchMode.JOIN);
        //            
        //            query.addOrder(Order.desc("majorAlert.alertGmt"));
        //        }
        //        if (translatedSeverities.isMinor()) {
        //            query.createAlias("minorAlert", "minorAlert", Criteria.LEFT_JOIN);
        //            query.setFetchMode("minorAlert", FetchMode.JOIN);
        //            query.createAlias("minorAlert.customerAlertType", "minorCustomerAlertType", Criteria.LEFT_JOIN);
        //            query.setFetchMode("minorAlert.customerAlertType", FetchMode.JOIN);
        //            
        //            query.addOrder(Order.desc("minorAlert.alertGmt"));
        //            
        //        }
        
        addPayStationsWithAlertRestrictions(query, criteria, translatedSeverities);
        
        query.addOrder(Order.asc("pos.name"));
        
        query.add(Restrictions.in("pos.id", pointOfSaleIds));
        
        return query;
    }
    
    private Criteria createFindPayStationsOnMapWithAlertQuery(final ActiveAlertSearchCriteria criteria, final ProjectionList projections,
        final AlertEntry targetObj) {
        final Criteria query = this.entityDao.createCriteria(PosEventCurrent.class);
        query.createAlias("pointOfSale", "pos");
        query.createAlias("pos.posAlertStatus", "alertStatus");
        query.createAlias("pos.posStatus", "posStatus");
        query.createAlias("pos.paystation", "pStation");
        query.createAlias("pStation.paystationType", "psType");
        query.createAlias("eventType", "eventType");
        query.setFetchMode("eventType", FetchMode.JOIN);
        query.createAlias("eventType.eventDeviceType", "eventDeviceType");
        query.setFetchMode("eventType.eventDeviceType", FetchMode.JOIN);
        query.createAlias("customerAlertType", "cat", Criteria.LEFT_JOIN);
        
        final ActiveAlertSearchCriteria.Severity translatedSeverities = criteria.translateSeverities();
        
        final ItemLocatorCriterionAggregator aggregator = new ItemLocatorCriterionAggregator();
        final Date epoc = new Date(0);
        
        if (projections != null) {
            projections.add(Projections.groupProperty("pos.id").as("psId"));
        }
        
        if (criteria.getSeverityIds() != null) {
            query.add(Restrictions.in("eventSeverityType.id", criteria.getSeverityIds()));
        }
        if (criteria.getMaxAlertGmt() != null) {
            query.add(Restrictions.le("alertGmt", criteria.getMaxAlertGmt()));
        }
        if (projections != null) {
            projections.add(Projections.count("id").as("sum")).add(Projections.max("eventSeverityType.id").as("maxSeverity"))
                    .add(Projections.min("alertGmt").as("minAlertGmt"));
        }
        
        if (targetObj != null) {
            aggregator.descending("alertGmt", targetObj.getAlertDate(), epoc);
        }
        
        //        if (translatedSeverities.isCritical()) {
        //            query.createAlias("criticalAlert", "criticalAlert", Criteria.LEFT_JOIN);
        //            if (criteria.getMaxAlertGmt() != null) {
        //                query.add(Restrictions.or(Restrictions.isNull("criticalAlert"), Restrictions.le("criticalAlert.alertGmt", criteria.getMaxAlertGmt())));
        //            }
        //            
        //            if (projections != null) {
        //                projections.add(Projections.sum("critical").as("sumCritical")).add(Projections.max("criticalAlert.alertGmt").as("criticalAlertDate"));
        //            }
        //            
        //            if ((targetObj == null) && (projections != null)) {
        //                query.addOrder(Order.desc("criticalAlertDate"));
        //            } else {
        //                query.addOrder(Order.desc("criticalAlert.alertGmt"));
        //            }
        //            
        //            if (targetObj != null) {
        //                aggregator.descending("criticalAlert.alertGmt", targetObj.getCriticalAlertDate(), epoc);
        //            }
        //        }
        //        if (translatedSeverities.isMajor()) {
        //            query.createAlias("majorAlert", "majorAlert", Criteria.LEFT_JOIN);
        //            if (criteria.getMaxAlertGmt() != null) {
        //                query.add(Restrictions.or(Restrictions.isNull("majorAlert"), Restrictions.le("majorAlert.alertGmt", criteria.getMaxAlertGmt())));
        //            }
        //            
        //            if (projections != null) {
        //                projections.add(Projections.sum("major").as("sumMajor")).add(Projections.max("majorAlert.alertGmt").as("majorAlertDate"));
        //            }
        //            
        //            if ((targetObj == null) && (projections != null)) {
        //                query.addOrder(Order.desc("majorAlertDate"));
        //            } else {
        //                query.addOrder(Order.desc("majorAlert.alertGmt"));
        //            }
        //            
        //            if (targetObj != null) {
        //                aggregator.descending("majorAlert.alertGmt", targetObj.getMajorAlertDate(), epoc);
        //            }
        //        }
        //        if (translatedSeverities.isMinor()) {
        //            query.createAlias("minorAlert", "minorAlert", Criteria.LEFT_JOIN);
        //            if (criteria.getMaxAlertGmt() != null) {
        //                query.add(Restrictions.or(Restrictions.isNull("minorAlert"), Restrictions.le("minorAlert.alertGmt", criteria.getMaxAlertGmt())));
        //            }
        //            
        //            if (projections != null) {
        //                projections.add(Projections.sum("minor").as("sumMinor")).add(Projections.max("minorAlert.alertGmt").as("minorAlertDate"));
        //            }
        //            
        //            if ((targetObj == null) && (projections != null)) {
        //                query.addOrder(Order.desc("minorAlertDate"));
        //            } else {
        //                query.addOrder(Order.desc("minorAlert.alertGmt"));
        //            }
        //            
        //            if (targetObj != null) {
        //                aggregator.descending("minorAlert.alertGmt", targetObj.getMinorAlertDate(), epoc);
        //            }
        //        }
        
        query.addOrder(Order.asc("pos.name"));
        if (targetObj != null) {
            aggregator.ascending("pos.name", targetObj.getPsName(), "");
        }
        
        addPayStationsWithAlertRestrictions(query, criteria, translatedSeverities);
        if (criteria.isPlaced()) {
            query.add(Restrictions.isNotNull("pos.latitude")).add(Restrictions.isNotNull("pos.longitude"));
        }
        
        if (projections != null) {
            query.setProjection(projections);
        }
        
        if ((criteria.getItemsPerPage() != null) && (criteria.getPage() != null) && (targetObj == null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        if (targetObj != null) {
            query.add(aggregator.aggregateCriterion());
        }
        
        return query;
    }
    
    private Criteria createFindPayStationsWithBalanceAndAlertQuery(final SummarySearchCriteria criteria, final ProjectionList projections) {
        
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class);
        final Object[] alertThresholdTypes = new Object[] { WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT,
            WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS, WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT,
            WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS, WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT,
            WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS, WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR,
            WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION };
        query.createAlias("posAlertStatus", "alertStatus", Criteria.LEFT_JOIN);
        query.createAlias("posEventCurrents", "pec", Criteria.LEFT_JOIN);
        query.createAlias("pec.customerAlertType",
                          "cat",
                          Criteria.LEFT_JOIN,
                          Restrictions.and(Restrictions.isNotNull("pec.customerAlertType"),
                                           Restrictions.in("cat.alertThresholdType.id", alertThresholdTypes)));
        
        query.createAlias("posStatus", "posStatus");
        query.createAlias("posBalance", "posBal");
        query.createAlias("location", "location");
        query.createAlias("routePOSes", "routePos", Criteria.LEFT_JOIN);
        
        this.addExclusiveOrderBy(criteria, query);
        query.addOrder(Order.asc("pointOfSaleName"));
        
        query.add(Restrictions.isNotNull("cat.id"));
        
        if (criteria.getSeverityIds() != null) {
            query.add(Restrictions.in("pec.eventSeverityType.id", criteria.getSeverityIds()));
            query.add(Restrictions.eq("pec.isActive", true));
        }
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        }
        
        query.add(Restrictions.eq("posStatus.isDeleted", false));
        query.add(Restrictions.eq("posStatus.isVisible", true));
        query.add(Restrictions.eq("posStatus.isActivated", true));
        query.add(Restrictions.eq("posStatus.isDecommissioned", false));
        
        if ((criteria.getLocationIds() != null) && (criteria.getLocationIds().size() > 0)) {
            query.add(Restrictions.in("location.id", criteria.getLocationIds()));
        }
        
        if ((criteria.getRouteIds() != null) && (criteria.getRouteIds().size() > 0)) {
            query.add(Subqueries.propertyIn(HibernateConstants.ID,
                                            DetachedCriteria.forClass(RoutePOS.class).setProjection(Projections.property("pointOfSale.id"))
                                                    .add(Restrictions.in("route.id", criteria.getRouteIds()))));
        }
        
        projections.add(Projections.groupProperty(HibernateConstants.ID).as("psId"));
        query.setProjection(projections);
        
        if ((criteria.getItemsPerPage() != null) && (criteria.getPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query;
    }
    
    private void addExclusiveOrderBy(final SummarySearchCriteria criteria, final Criteria query) {
        if (!StringUtils.isEmpty(criteria.getExclusiveOrderBy())) {
            String aliasName = null;
            String associationPath = null;
            String orderColumn = null;
            boolean isPaperLevel = false;
            
            switch (criteria.getExclusiveOrderBy()) {
                case "pointOfSaleName":
                    orderColumn = "name";
                    break;
                case "routeName":
                    aliasName = "route";
                    associationPath = "routePos.route";
                    orderColumn = "route.name";
                    break;
                case "locationName":
                    orderColumn = "location.name";
                    break;
                case "coinCount":
                    orderColumn = "posBal.coinCount";
                    break;
                case "coinAmount":
                    orderColumn = "posBal.coinAmount";
                    break;
                case "billCount":
                    orderColumn = "posBal.billCount";
                    break;
                case "billAmount":
                    orderColumn = "posBal.billAmount";
                    break;
                case "unsettledCreditCardCount":
                    orderColumn = "posBal.unsettledCreditCardCount";
                    break;
                case "unsettledCreditCardAmount":
                    orderColumn = "posBal.unsettledCreditCardAmount";
                    break;
                case "totalAmount":
                    orderColumn = "posBal.totalAmount";
                    break;
                case "lastSeen":
                    orderColumn = "posHeartbeat.lastHeartbeatGmt";
                    break;
                case "batteryVolt":
                    orderColumn = "posSensorState.battery1Voltage";
                    break;
                case "paperStatus":
                    isPaperLevel = true;
                    orderColumn = "CASE PrinterPaperLevel WHEN 0 THEN 4 WHEN 1 THEN 2 WHEN 2 THEN 0 WHEN 3 THEN 0 WHEN 4 THEN 3 END";
                    break;
                case "collectionDate":
                    orderColumn = "posBal.lastCollectionGmt";
                    break;
                default:
                    break;
            }
            
            if (aliasName != null) {
                query.createAlias(associationPath, aliasName, Criteria.LEFT_JOIN);
            }
            
            if (orderColumn != null) {
                if (isPaperLevel) {
                    if ("desc".equals(criteria.getExclusiveOrderDir().toLowerCase())) {
                        query.addOrder(Order.desc("posSensorState.printerStatus"));
                    } else {
                        query.addOrder(Order.asc("posSensorState.printerStatus"));
                    }
                    final StringBuilder sqlFormula = new StringBuilder(orderColumn);
                    sqlFormula.append(" ");
                    sqlFormula.append(criteria.getExclusiveOrderDir().toLowerCase());
                    query.addOrder(OrderBySqlFormula.sqlFormula(sqlFormula.toString()));
                } else {
                    if (criteria.getExclusiveOrderDir() != null) {
                        if ("desc".equals(criteria.getExclusiveOrderDir().toLowerCase())) {
                            query.addOrder(Order.desc(orderColumn));
                        } else {
                            query.addOrder(Order.asc(orderColumn));
                        }
                    }
                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<AlertMapEntry> findActivePointOfSalesByRecentCollections(final ActiveAlertSearchCriteria criteria,
        final Object[] collectionTypes, final Date minDate, final Date maxDate) {
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class);
        
        final ProjectionList selections = Projections.projectionList();
        selections.add(Projections.property("serialNumber").as("psSerialNumber"));
        selections.add(Projections.property("latitude").as("latitude"));
        selections.add(Projections.property("longitude").as("longitude"));
        selections.add(Projections.property("paystation.paystationType.id").as("psType"));
        selections.add(Projections.property("name").as("posName"));
        selections.add(Projections.groupProperty(HibernateConstants.ID).as("psId"));
        
        query.setProjection(selections);
        query.createAlias("posCollections", "posCollection");
        query.createAlias("customer", "customer");
        query.createAlias("paystation", "paystation");
        query.createAlias("posCollection.collectionType", "collectionType");
        query.createAlias("posStatus", "posStatus");
        
        query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        query.add(Restrictions.between("posCollection.endGmt", minDate, maxDate));
        if (collectionTypes != null && collectionTypes.length != 0) {
            query.add(Restrictions.in("collectionType.id", collectionTypes));
        }
        query.add(Restrictions.eq("posStatus.isDeleted", false));
        query.add(Restrictions.eq("posStatus.isVisible", true));
        query.add(Restrictions.eq("posStatus.isActivated", true));
        query.add(Restrictions.eq("posStatus.isDecommissioned", false));
        
        if ((criteria.getLocationIds() != null) && (criteria.getLocationIds().size() > 0)) {
            query.add(Restrictions.in("location.id", criteria.getLocationIds()));
        }
        
        if ((criteria.getRouteIds() != null) && (criteria.getRouteIds().size() > 0)) {
            query.add(Subqueries.propertyIn(HibernateConstants.ID,
                                            DetachedCriteria.forClass(RoutePOS.class).setProjection(Projections.property(HibernateConstants.ID))
                                                    .add(Restrictions.in("route.id", criteria.getRouteIds()))));
        }
        //
        query.setResultTransformer(new AlertMapEntryMinimalTransformer());
        return query.list();
    }
    
    private void addPayStationsWithAlertRestrictions(final Criteria query, final ActiveAlertSearchCriteria criteria,
        final ActiveAlertSearchCriteria.Severity severityRestrictions) {
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("pos.customer.id", criteria.getCustomerId()));
        }
        query.add(Restrictions.eq("isActive", true));
        query.add(Restrictions.eq("posStatus.isDeleted", false));
        query.add(Restrictions.eq("posStatus.isVisible", true));
        query.add(Restrictions.eq("posStatus.isActivated", true));
        query.add(Restrictions.eq("posStatus.isDecommissioned", false));
        
        if ((criteria.getLocationIds() != null) && (criteria.getLocationIds().size() > 0)) {
            query.add(Restrictions.in("pos.location.id", criteria.getLocationIds()));
        }
        
        if ((criteria.getRouteIds() != null) && (criteria.getRouteIds().size() > 0)) {
            query.add(Subqueries.propertyIn("pos.id", DetachedCriteria.forClass(RoutePOS.class).setProjection(Projections.property("pointOfSale.id"))
                    .add(Restrictions.in("route.id", criteria.getRouteIds()))));
        }
        
        if ((criteria.getAlertThresholdTypeIds() != null) && (criteria.getAlertThresholdTypeIds().size() > 0)) {
            if (criteria.getIsPaystationAlertSelected()) {
                query.add(Restrictions.or(Restrictions.isNull("customerAlertType"),
                                          Restrictions.in("cat.alertThresholdType.id", criteria.getAlertThresholdTypeIds())));
            } else {
                query.add(Restrictions.in("cat.alertThresholdType.id", criteria.getAlertThresholdTypeIds()));
            }
        }
        
        if ((criteria.getDeviceTypeIds() != null) && (criteria.getDeviceTypeIds().size() > 0)) {
            query.add(Restrictions.in("eventDeviceType.id", criteria.getDeviceTypeIds()));
        }
        
        //        query.add(getNoHopperRestrictions());
        //        query.add(getNoCoinChanger());
        
        Criterion severityCond = null;
        if (severityRestrictions.isCritical()) {
            severityCond = this.entityDao.nullableOr(severityCond, Restrictions.eq("eventSeverityType.id", WebCoreConstants.SEVERITY_CRITICAL));
        }
        if (severityRestrictions.isMajor()) {
            severityCond = this.entityDao.nullableOr(severityCond, Restrictions.eq("eventSeverityType.id", WebCoreConstants.SEVERITY_MAJOR));
        }
        if (severityRestrictions.isMinor()) {
            severityCond = this.entityDao.nullableOr(severityCond, Restrictions.eq("eventSeverityType.id", WebCoreConstants.SEVERITY_MINOR));
        }
        
        query.add(severityCond);
    }
    
    // Returns restriction that excludes hopper events unless the paystation is of type Shelby
    //    private Criterion getNoHopperRestrictions() {
    //        final Criterion equalHopper1 = Restrictions.eq("eventDeviceType.id", WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1);
    //        final Criterion equalHopper2 = Restrictions.eq("eventDeviceType.id", WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2);
    //        final Criterion equalHopper1ORHopper2 = Restrictions.or(equalHopper1, equalHopper2);
    //        final Criterion notequalShelby = Restrictions.ne("psType.id", WebCoreConstants.PAY_STATION_TYPE_SHELBY);
    //        return Restrictions.not(Restrictions.and(equalHopper1ORHopper2, notequalShelby));
    //    }
    
    // Returns restriction that excludes coin changer events if the paystation is of type LukeII
    //    private Criterion getNoCoinChanger() {
    //        final Criterion equalChanger = Restrictions.eq("eventDeviceType.id", WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER);
    //        final Criterion equalLukeII = Restrictions.eq("psType.id", WebCoreConstants.PAY_STATION_TYPE_LUKE_2);
    //        return Restrictions.not(Restrictions.and(equalChanger, equalLukeII));
    //    }
    
    @Override
    public final List<AlertSummaryEntry> findPointOfSalesForAlertSummary(final SummarySearchCriteria criteria, final String timeZone) {
        final Map<Integer, AlertSummaryEntry> mapEntriesHash = new HashMap<Integer, AlertSummaryEntry>();
        final AlertSummaryEntryTransformer transformer = new AlertSummaryEntryTransformer(mapEntriesHash);
        
        @SuppressWarnings("unchecked")
        final List<AlertSummaryEntry> result = createFindPointOfSalesForAlertSummaryQuery(criteria).setResultTransformer(transformer).list();
        
        if (result.size() > 0) {
            
            for (AlertSummaryEntry alertSummaryEntry : result) {
                final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId((Integer) alertSummaryEntry.getPosRandomId()
                        .getId(), WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
                alertSummaryEntry.setRoutes(routeList);
                
                alertSummaryEntry.setLastSeenDate(DateUtil.getRelativeTimeString(alertSummaryEntry.getLastSeenGMT(), timeZone));
            }
        }
        return result;
    }
    
    private Criteria createFindPointOfSalesForAlertSummaryQuery(final SummarySearchCriteria criteria) {
        final ProjectionList projections = Projections.projectionList();
        projections.add(Projections.distinct(Projections.property(HibernateConstants.ID).as("posId")));
        projections.add(Projections.property("name").as("pointOfSaleName"));
        projections.add(Projections.property("serialNumber").as("psSerialNumber"));
        projections.add(Projections.property("location.id").as("locationId"));
        projections.add(Projections.property("location.name").as("locationName"));
        projections.add(Projections.property("posHeartbeat.lastHeartbeatGmt").as("lastSeenGMT"));
        projections.add(Projections.property("posSensorState.printerPaperLevel").as("paperLevel"));
        projections.add(Projections.property("posSensorState.printerStatus").as("printerStatus"));
        projections.add(Projections.property("posSensorState.battery1Level").as("batteryLevel"));
        projections.add(Projections.property("posSensorState.battery1Voltage").as("batteryVoltage"));
        projections.add(Projections.property("alertStatus.payStationMinor").as("payStationMinor"));
        projections.add(Projections.property("alertStatus.payStationMajor").as("payStationMajor"));
        projections.add(Projections.property("alertStatus.payStationCritical").as("payStationCritical"));
        projections.add(Projections.property("alertStatus.communicationMinor").as("communicationMinor"));
        projections.add(Projections.property("alertStatus.communicationMajor").as("communicationMajor"));
        projections.add(Projections.property("alertStatus.communicationCritical").as("communicationCritical"));
       
        
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class);
        
        query.createAlias("posAlertStatus", "alertStatus", Criteria.LEFT_JOIN);
        query.createAlias("posStatus", "posStatus");
        query.createAlias("posSensorState", "posSensorState");
        query.createAlias("posHeartbeat", "posHeartbeat");
        query.createAlias("location", "location");
        query.createAlias("routePOSes", "routePos", Criteria.LEFT_JOIN);
        
        this.addExclusiveOrderBy(criteria, query);
        query.addOrder(Order.asc("pointOfSaleName"));
        
        if (criteria.getSeverityIds() != null) {
            query.createAlias("posEventCurrents", "pec", Criteria.LEFT_JOIN);
            query.createAlias("pec.customerAlertType", "cat", Criteria.LEFT_JOIN);
            query.add(Restrictions.and(Restrictions.eq("pec.isActive", true), Restrictions.and(Restrictions.or(Restrictions
                    .isNull("pec.customerAlertType"), Restrictions.eq("cat.alertThresholdType.id",
                                                                      WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR)), Restrictions
                    .in("pec.eventSeverityType.id", criteria.getSeverityIds()))));
        }
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        }
        
        query.add(Restrictions.eq("posStatus.isDeleted", false));
        query.add(Restrictions.eq("posStatus.isVisible", true));
        query.add(Restrictions.eq("posStatus.isActivated", true));
        query.add(Restrictions.eq("posStatus.isDecommissioned", false));
        
        if ((criteria.getLocationIds() != null) && (criteria.getLocationIds().size() > 0)) {
            query.add(Restrictions.in("location.id", criteria.getLocationIds()));
        }
        
        if ((criteria.getRouteIds() != null) && (criteria.getRouteIds().size() > 0)) {
            query.add(Subqueries.propertyIn(HibernateConstants.ID,
                                            DetachedCriteria.forClass(RoutePOS.class).setProjection(Projections.property("pointOfSale.id"))
                                                    .add(Restrictions.in("route.id", criteria.getRouteIds()))));
        }
        
        query.setProjection(projections);
        
        if ((criteria.getItemsPerPage() != null) && (criteria.getPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query;
    }
    
    @Override
    public final List<AlertThresholdType> findAlertThresholdTypeByAlertClassTypeId(final Byte alertClassTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("AlertThresholdType.findByAlertClassTypeId", HibernateConstants.ALERT_CLASS_TYPE_ID,
                                                            alertClassTypeId);
    }
    
    @SuppressWarnings({ "unchecked", "checkstyle:designforextension" })
    @Override
    public List<AlertInfoEntry> findHistory(final AlertSearchCriteria criteria, final TimeZone timezone, final String sortItem,
        final String sortOrder) {
        String selectClauseStr;
        final StringBuilder joinClause = new StringBuilder();
        final StringBuilder whereClause = new StringBuilder();
        final StringBuilder outerWhereClause = new StringBuilder();
        final Map<String, Object> namedParams = new HashMap<String, Object>();
        
        boolean isAlertCenter = false;
        if ((criteria.getIsAlertCentre() == null) || (!criteria.getIsAlertCentre().booleanValue())) {
            isAlertCenter = false;
            selectClauseStr = WebCoreConstants.EMPTY_STRING;
        } else {
            isAlertCenter = true;
            selectClauseStr = SQL_SEL_ALERT_CENTER;
        }
        
        namedParams.put("isAlertCenter", isAlertCenter);
        namedParams.put("maxInt", Integer.MAX_VALUE);
        
        applyPOSCriteria(criteria, joinClause, whereClause, outerWhereClause, namedParams);
        applyAlertCriteria(criteria, joinClause, whereClause, outerWhereClause, namedParams);
        
        final Calendar cal = Calendar.getInstance();
        if (criteria.getMaxClearedTime() == null) {
            criteria.setMaxClearedTime(new Date());
        }
        
        cal.setTime(criteria.getMaxClearedTime());
        namedParams.put("maxCreatedGMT", cal.getTime());
        
        if (criteria.getMinClearedTime() == null) {
            cal.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT);
            final Date timeLimit = cal.getTime();
            
            criteria.setMinClearedTime(timeLimit);
        }
        
        namedParams.put("minTimestampGMT", criteria.getMinClearedTime());
        
        String scopeSQL = null;
        final String whereClauseStr = whereClause.toString();
        String joinClauseStr = joinClause.toString();
        
        List<AlertInfoEntry> result;
        String orderBy = HISTORY_ORDER_BY_MAP.get((isAlertCenter || (sortItem == null)) ? WebCoreConstants.EMPTY_STRING : sortItem);
        orderBy = MessageFormat.format(orderBy, (!StringUtils.isBlank(sortOrder) && sortOrder.equals(ORDER_ASC)) ? ORDER_ASC : ORDER_DESC);
        
        final AlertInfoEntryTransformer resultTransformer = new AlertInfoEntryTransformer(this.messageHelper, timezone);
        final String sql = MessageFormat.format(SQLFMT_HISTORY_MAIN, selectClauseStr, joinClauseStr, whereClauseStr, outerWhereClause.toString(),
                                                orderBy, SQLFMT_HISTORY_BOUNDARY_SEL_EMPTY);
        
        int callCnt = 0;
        SQLQuery query = null;
        if (criteria.isFindAll() || (criteria.getPage() == null) || (criteria.getItemsPerPage() == null)) {
            query = this.entityDao.createSQLQuery(sql);
            query.setProperties(namedParams);
            addHistoryScalar(query, isAlertCenter);
            
            query.setResultTransformer(resultTransformer);
            query.setCacheable(false);
            
            query.setMaxResults(WebCoreConstants.NUM_RECORDS_FOR_EVENT_HISTORY_EXPORT);
            
            result = query.list();
        } else {
            result = new ArrayList<AlertInfoEntry>();
            
            boolean completed = false;
            while (!completed) {
                ++callCnt;
                query = this.entityDao.createSQLQuery(sql);
                
                if (criteria.getCurrentDataTime() != null) {
                    namedParams.put("minTimestampGMT", criteria.getCurrentDataTime());
                }
                
                query.setProperties(namedParams);
                
                addHistoryScalar(query, isAlertCenter);
                
                query.setResultTransformer(resultTransformer);
                query.setCacheable(false);
                
                query.setFirstResult(((criteria.getPage() - 1) * criteria.getItemsPerPage()) + result.size());
                query.setMaxResults(criteria.getItemsPerPage() - result.size());
                
                List<AlertInfoEntry> currentResult = query.list();
                result.addAll(currentResult);
                
                completed = (criteria.getCurrentDataTime() == null) || (criteria.getMinTimeWithData() == null)
                            || (result.size() >= criteria.getItemsPerPage()) || criteria.getMinTimeWithData().after(criteria.getCurrentDataTime());
                if (!completed && (result.size() < criteria.getItemsPerPage())) {
                    if (callCnt > 1) {
                        // This will break the loop. It is still here just in case we need another thing to provide the timestamp criteria.
                        criteria.setCurrentDataTime(new Date(criteria.getMinClearedTime().getTime() - StandardConstants.DAYS_IN_MILLIS_1));
                    } else if (isAlertCenter && (currentResult.size() <= 0)) {
                        determineHistoryScope(scopeSQL, criteria, namedParams);
                    } else {
                        criteria.setCurrentDataTime(new Date(criteria.getCurrentDataTime().getTime() - criteria.getMillisecsPerPage()));
                    }
                    
                    completed = criteria.getCurrentDataTime().before(criteria.getMinClearedTime());
                }
            }
        }
        
        return result;
    }
    
    @Override
    public List<AlertMapEntry> findPointOfSaleWithClearedPSAlert(final AlertSearchCriteria criteria, final TimeZone timezone) {
        final StringBuilder queryStr = new StringBuilder();
        final Map<String, Object> namedParams = new HashMap<String, Object>();
        
        queryStr.append(" SELECT DISTINCT pos.id AS psId ");
        queryStr.append("  , pos.serialNumber AS psSerialNumber ");
        queryStr.append("  , pos.name AS pointOfSaleName ");
        queryStr.append("  , pos.latitude AS Latitude ");
        queryStr.append("  , pos.longitude AS Longitude ");
        queryStr.append("  , ps.paystationType.id AS psType ");
        queryStr.append(" FROM PosEventHistory cPeh, PosEventHistory peh ");
        queryStr.append("  JOIN peh.pointOfSale pos JOIN pos.paystation ps ");
        queryStr.append("  JOIN pos.posStatus pStat WITH (pStat.isDeleted = false) AND (pStat.isDecommissioned = false) AND (pStat.isActivated = true) AND (pStat.isVisible = true) ");
        if (criteria.getRouteId() != null) {
            queryStr.append("  JOIN pos.routePOSes rPOS WITH rPOS.route.id = :routeId ");
            namedParams.put("routeId", criteria.getRouteId());
        }
        
        if (criteria.getEventDeviceId() != null) {
            queryStr.append(" JOIN peh.eventType et WITH et.eventDeviceType.id = :eventDeviceTypeId ");
            namedParams.put("eventDeviceTypeId", criteria.getEventDeviceId());
        }
        
        queryStr.append(" WHERE (cPeh.isActive = false) AND (peh.isActive = true) AND (cPeh.timestampGmt > peh.timestampGmt) ");
        queryStr.append("  AND (cPeh.pointOfSale.id = peh.pointOfSale.id) AND (cPeh.eventType.id = peh.eventType.id) ");
        
        // Apply POS criterias
        if (criteria.getPointOfSaleId() != null) {
            queryStr.append("  AND (peh.pointOfSale.id IN(:pointOfSaleIds)) ");
            namedParams.put("pointOfSaleIds", criteria.getPointOfSaleId());
        }
        
        if (criteria.getLocationId() != null) {
            queryStr.append("  AND (pos.location.id = :locationId) ");
            namedParams.put("locationId", criteria.getLocationId());
        }
        
        if (criteria.getCustomerId() != null) {
            queryStr.append("  AND (pos.customer.id = :customerId) ");
            namedParams.put("customerId", criteria.getCustomerId());
        }
        
        // Apply Alert Criterias
        if (criteria.getSeverityId() != null) {
            queryStr.append("  AND (peh.eventSeverityType.id = :eventSeverityTypeId) ");
            namedParams.put("eventSeverityTypeId", criteria.getSeverityId());
        }
        
        // Apply Time Criterias
        queryStr.append("  AND (peh.createdGmt < :maxCreatedGMT) AND (cPeh.createdGmt < :maxCreatedGMT) AND (cPeh.timestampGmt >= :minTimestampGMT) ");
        final Calendar cal = Calendar.getInstance();
        if (criteria.getMaxClearedTime() == null) {
            criteria.setMaxClearedTime(new Date());
        }
        
        cal.setTime(criteria.getMaxClearedTime());
        namedParams.put("maxCreatedGMT", cal.getTime());
        
        if (criteria.getMinClearedTime() == null) {
            cal.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT_MAP);
            final Date timeLimit = cal.getTime();
            
            criteria.setMinClearedTime(timeLimit);
        }
        
        namedParams.put("minTimestampGMT", criteria.getMinClearedTime());
        
        Query query = this.entityDao.getCurrentSession().createQuery(queryStr.toString());
        query.setProperties(namedParams);
        query.setResultTransformer(new AlertMapEntryTransformer());
        
        return query.list();
    }

    private void applyPOSCriteria(final AlertSearchCriteria criteria, final StringBuilder joinClause, final StringBuilder whereClause,
        final StringBuilder outerWhereClause, final Map<String, Object> namedParams) {
        if (criteria.getPointOfSaleId() != null) {
            whereClause.append(" AND (peh.PointOfSaleId IN(:pointOfSaleIds)) ");
            namedParams.put("pointOfSaleIds", criteria.getPointOfSaleId());
        }
        
        if (criteria.getLocationId() != null) {
            whereClause.append(" AND (pos.LocationId = :locationId) ");
            namedParams.put("locationId", criteria.getLocationId());
        }
        
        if (criteria.getRouteId() != null) {
            joinClause.append(" JOIN RoutePOS rPOS ON((rPOS.RouteId = :routeId) AND (pos.Id = rPOS.PointOfSaleId)) ");
            namedParams.put("routeId", criteria.getRouteId());
        }
        
        if (criteria.getCustomerId() != null) {
            whereClause.append(" AND (pos.CustomerId = :customerId) ");
            namedParams.put("customerId", criteria.getCustomerId());
        }
    }
    
    private void applyAlertCriteria(final AlertSearchCriteria criteria, final StringBuilder joinClause, final StringBuilder whereClause,
        final StringBuilder outerWhereClause, final Map<String, Object> namedParams) {
        if (criteria.isActive() != null) {
            outerWhereClause.append(" AND (history.IsActive = :isActive) ");
            namedParams.put("isActive", criteria.isActive());
        }
        
        if (criteria.isOnlyPayStation()) {
            whereClause.append(" AND ((peh.CustomerAlertTypeId IS NULL) OR (cat.AlertThresholdTypeId = "
                    + WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR + ")) ");
        }
        
        if ((criteria.getAlertThresholdTypeIds() != null) && (criteria.getAlertThresholdTypeIds().size() > 0)) {
            whereClause.append(" AND (cat.AlertThresholdTypeId IN(:alertThreshodTypeIds)) ");
            namedParams.put("alertThreshodTypeIds", criteria.getAlertThresholdTypeIds());
        }
        
        if (criteria.getEventDeviceId() != null) {
            joinClause.append(" JOIN EventType et ON(peh.EventTypeId = et.Id) ");
            whereClause.append(" AND (et.EventDeviceTypeId = :eventDeviceTypeId) ");
            namedParams.put("eventDeviceTypeId", criteria.getEventDeviceId());
        }
        
        if (criteria.getSeverityId() != null) {
            whereClause.append(" AND ((peh.IsActive = 0) OR (peh.EventSeverityTypeId = :eventSeverityTypeId)) ");
            namedParams.put("eventSeverityTypeId", criteria.getSeverityId());
        }
    }
    
    private void determineHistoryScope(final String sql, final AlertSearchCriteria criteria, final Map<String, Object> namedParams) {
        namedParams.put("currentTimestampGMT", criteria.getCurrentDataTime());
        final SQLQuery scopeQuery = this.entityDao.createSQLQuery(sql);
        scopeQuery.setProperties(namedParams);
        
        scopeQuery.addScalar("dataMaxTS", HibernateConstants.TYPE_DATETIME);
        scopeQuery.addScalar("dataMinTS", HibernateConstants.TYPE_DATETIME);
        scopeQuery.addScalar("dataCount", HibernateConstants.TYPE_INTEGER);
        
        final Object[] buff = (Object[]) scopeQuery.uniqueResult();
        if (buff != null) {
            final int count = (Integer) buff[2];
            if (count > 0) {
                criteria.setMinTimeWithData((Date) buff[1]);
                criteria.setCurrentDataTime((Date) buff[0]);
                
                criteria.setMillisecsPerPage((int) Math.ceil((criteria.getCurrentDataTime().getTime() - criteria.getMinTimeWithData().getTime())
                                             / (count / (double) criteria.getItemsPerPage())));
                if (criteria.getMillisecsPerPage() <= 0) {
                    criteria.setMillisecsPerPage(1L);
                }
                
                criteria.setCurrentDataTime(new Date(criteria.getCurrentDataTime().getTime() - criteria.getMillisecsPerPage()));
            }
        }
    }
    
    private void addHistoryScalar(final SQLQuery query, boolean isAlertCenter) {
        query.addScalar(AlertInfoEntryTransformer.ATTR_ID, HibernateConstants.TYPE_LONG);
        query.addScalar(AlertInfoEntryTransformer.ATTR_CLEAR_ID, HibernateConstants.TYPE_LONG);
        query.addScalar(AlertInfoEntryTransformer.ATTR_POS_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_ALERT_TYPE_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_ALERT_TYPE_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_EVENT_TYPE_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_EVENT_TYPE_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_DEVICE_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_DEVICE_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_CAT_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_CAT_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_SEVERITY_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_SEVERITY_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_ACTION_TYPE_ID, HibernateConstants.TYPE_INTEGER);
        query.addScalar(AlertInfoEntryTransformer.ATTR_ACTION_TYPE_NAME, HibernateConstants.TYPE_STRING);
        query.addScalar(AlertInfoEntryTransformer.ATTR_IS_ACTIVE, HibernateConstants.TYPE_BOOLEAN);
        query.addScalar(AlertInfoEntryTransformer.ATTR_ALERT_GMT, HibernateConstants.TYPE_DATETIME);
        query.addScalar(AlertInfoEntryTransformer.ATTR_CLEAR_GMT, HibernateConstants.TYPE_DATETIME);
        query.addScalar(AlertInfoEntryTransformer.ATTR_LAST_MODIFIED_BY_USER_NAME, HibernateConstants.TYPE_STRING);
        
        if (isAlertCenter) {
            query.addScalar(AlertInfoEntryTransformer.ATTR_POS_NAME, HibernateConstants.TYPE_STRING);
            query.addScalar(AlertInfoEntryTransformer.ATTR_SERIAL_NUMBER, HibernateConstants.TYPE_STRING);
            query.addScalar(AlertInfoEntryTransformer.ATTR_LATITUDE, HibernateConstants.TYPE_DECIMAL);
            query.addScalar(AlertInfoEntryTransformer.ATTR_LONGITUDE, HibernateConstants.TYPE_DECIMAL);
            query.addScalar(AlertInfoEntryTransformer.ATTR_PAY_STATION_TYPE, HibernateConstants.TYPE_INTEGER);
        }
    }
    
    public static class AlertEntry implements Serializable {
        private static final long serialVersionUID = -4302978254906236362L;
        
        private Integer psId;
        private String psName;
        
        //        private Number sumCritical;
        //        private Number sumMajor;
        //        private Number sumMinor;
        
        private Number sum;
        private Date alertDate;
        
        //        private Date criticalAlertDate;
        //        private Date majorAlertDate;
        //        private Date minorAlertDate;
        
        public AlertEntry() {
            
        }
        
        public final Integer getPsId() {
            return this.psId;
        }
        
        public final void setPsId(final Integer psId) {
            this.psId = psId;
        }
        
        public final String getPsName() {
            return this.psName;
        }
        
        public final void setPosName(final String newPsName) {
            this.psName = newPsName;
        }
        
        public final Number getSum() {
            return this.sum;
        }
        
        public final void setSum(final Number sum) {
            this.sum = sum;
        }
        
        public final Date getAlertDate() {
            return this.alertDate;
        }
        
        public final void setAlertDate(final Date alertDate) {
            this.alertDate = alertDate;
        }
        
    }
    
}
