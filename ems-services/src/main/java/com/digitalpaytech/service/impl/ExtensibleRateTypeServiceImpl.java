package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import com.digitalpaytech.service.ExtensibleRateTypeService;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ExtensibleRateType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("extensibleRateTypeService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ExtensibleRateTypeServiceImpl implements ExtensibleRateTypeService {

    @Autowired
    private EntityDao entityDao;
    
    private Map<String, ExtensibleRateType> map;
    
    
    @Override
    public Map<String, ExtensibleRateType> loadAll() {
        if (map == null || map.isEmpty()) {
            List<ExtensibleRateType> list = entityDao.loadAll(ExtensibleRateType.class);
            map = new HashMap<String, ExtensibleRateType>(list.size());
            Iterator<ExtensibleRateType> iter = list.iterator();
            while (iter.hasNext()) {
                ExtensibleRateType type = iter.next();
                map.put(type.getName().toLowerCase(), type);
            }
        }
        return map;
    }
    
    @Override
    public Map<String, ExtensibleRateType> reloadAll() {
        if (map != null && !map.isEmpty()) {
            String name;
            Iterator<String> iter = map.keySet().iterator();
            while (iter.hasNext()) {
                name = iter.next();
                map.remove(name);
            }
        }
        return loadAll();
    }

    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}