package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.crypto.CryptoAlgorithm;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.SystemException;

@Service("hashAlgorithmTypeService")
public class HashAlgorithmTypeServiceImpl implements HashAlgorithmTypeService {
    
    private static Logger log = Logger.getLogger(HashAlgorithmTypeServiceImpl.class);
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    private List<HashAlgorithmType> hashAlgorithmsTypes;
    
    private Map<Integer, HashAlgorithmType> map;
    
    private Map<Integer, HashAlgorithmData> hashAlgorithmNotSigningMap;
    
    private Map<Integer, HashAlgorithmData> hashAlgorithmNotSigningAllMap;
    
    @PostConstruct
    private void loadAllToMap() throws SystemException {
        this.hashAlgorithmsTypes = this.openSessionDao.loadAll(HashAlgorithmType.class);
        this.map = new HashMap<Integer, HashAlgorithmType>(this.hashAlgorithmsTypes.size());
        this.hashAlgorithmNotSigningMap = new HashMap<Integer, HashAlgorithmData>();
        this.hashAlgorithmNotSigningAllMap = new HashMap<Integer, HashAlgorithmData>();
        
        final Iterator<HashAlgorithmType> iter = this.hashAlgorithmsTypes.iterator();
        while (iter.hasNext()) {
            final HashAlgorithmType hashAlgorithmType = iter.next();
            this.map.put(hashAlgorithmType.getId(), hashAlgorithmType);
        }
        loadHashAlgorithmNotSigningMaps();
    }
    
    @Override
    public final HashAlgorithmType findHashAlgorithmType(final Integer id) {
        return this.map.get(id);
    }
    
    @Override
    public List<HashAlgorithmType> loadAll() {
        if (this.hashAlgorithmsTypes == null) {
            this.hashAlgorithmsTypes = this.openSessionDao.loadAll(HashAlgorithmType.class);
        }
        return this.hashAlgorithmsTypes;
    }
    
    @Override
    public HashAlgorithmData findForMessageDigest(final Integer hashAlgorithmTypeId) {
        return this.hashAlgorithmNotSigningAllMap.get(hashAlgorithmTypeId);
    }
    
    @Override
    public final void reloadHashAlgorithmNotSigningMaps() throws SystemException {
        this.hashAlgorithmNotSigningMap.clear();
        this.hashAlgorithmNotSigningAllMap.clear();
        this.hashAlgorithmsTypes = null;
        this.hashAlgorithmsTypes = loadAll();
        this.loadHashAlgorithmNotSigningMaps();
    }
    
    @Override
    public final void loadHashAlgorithmNotSigningMaps() throws SystemException {
        if (this.hashAlgorithmNotSigningMap != null && !this.hashAlgorithmNotSigningMap.isEmpty()) {
            this.hashAlgorithmNotSigningMap.clear();
        }
        if (this.hashAlgorithmNotSigningAllMap != null && !this.hashAlgorithmNotSigningAllMap.isEmpty()) {
            this.hashAlgorithmNotSigningAllMap.clear();
        }
        final String blank = "";
        final String title = "Class: ";

        final StringBuilder bdr = new StringBuilder();
        try {
            for (HashAlgorithmType algorithmType : this.hashAlgorithmsTypes) {
                if (validHashAlgorithmType(algorithmType)) {
                    bdr.append(CryptoConstants.SERVICE_CRYPTO_PACKAGE_PREFIX).append(algorithmType.getClassName());
                    bdr.append(CryptoConstants.CRYPTO_ALGORITHM_FILE_SUFFIX);
                    
                    final Class<?> clz = Class.forName(bdr.toString());
                    final CryptoAlgorithm cryptoAlgorithm = (CryptoAlgorithm) clz.newInstance();
                    
                    final HashAlgorithmData data = new HashAlgorithmData(algorithmType, cryptoAlgorithm);
                    this.hashAlgorithmNotSigningAllMap.put(algorithmType.getId(), data);
                    if (!algorithmType.isIsDeleted()) {
                        this.hashAlgorithmNotSigningMap.put(algorithmType.getId(), data);
                    }
                    // Reset the buffer.
                    bdr.replace(0, bdr.length(), blank);
                }
            }
        } catch (ClassNotFoundException cnfe) {
            final StringBuilder err = new StringBuilder();
            err.append(title).append(bdr.toString()).append(" doesn't exist. ");
            log.error(err.toString(), cnfe);
            throw new SystemException(err.toString(), cnfe);
            
        } catch (IllegalAccessException iae) {
            final StringBuilder err = new StringBuilder();
            err.append(title).append(bdr.toString()).append(" cannot be accessed. ");
            log.error(err.toString(), iae);
            throw new SystemException(err.toString(), iae);
            
        } catch (InstantiationException ie) {
            final StringBuilder err = new StringBuilder();
            err.append(title).append(bdr.toString()).append(" cannot be instantiated. ");
            log.error(err.toString(), ie);
            throw new SystemException(err.toString(), ie);
        }
    }
    
    @Override
    public Map<Integer, HashAlgorithmData> findHashAlgorithmNotSigningMap() {
        return this.hashAlgorithmNotSigningMap;
    }
    
    @Override
    public final List<String> findHashAlgorithmNames(final boolean isForSigning) {
        List<String> notDeletedList = findHashAlgorithmNames(isForSigning, false);
        List<String> deletedList = findHashAlgorithmNames(isForSigning, true);
        List<String> total = new ArrayList<String>(notDeletedList);
        total.addAll(deletedList);
        return total;
    }
    
    @Override
    public List<String> findActiveHashAlgorithmNames(final boolean isForSigning) {
        return findHashAlgorithmNames(isForSigning, false);
    }
    
    @Override
    public List<String> findActiveSigningHashAlgorithmNames() {
        return findHashAlgorithmNames(true, false);
    }
    
    private List<String> findHashAlgorithmNames(final boolean isForSigning, final boolean isDeleted) {
        final List<String> list = new ArrayList<String>();
        for (HashAlgorithmType type : hashAlgorithmsTypes) {
            if (type.isIsForSigning() == isForSigning && type.isIsDeleted() == isDeleted) {
                list.add(type.getName());
            }
        }
        return list;
    }
    
    /**
     * Retrieve algorithm name (e.g. SHA-1, SHA-256, SHA-512) by database table HashAlgorithmType id.
     * 
     * @param hashAlgorithmTypeId
     *            HashAlgorithmType id.
     * @return String algorithm name.
     *         If input id is invalid, "unsupported" is returned.
     *         If algorithm's status is "deleted", "deprecated" is returned.
     */
    @Override
    public final String findAlgorithmName(final int hashAlgorithmTypeId) {
        if (this.map.get(hashAlgorithmTypeId) == null) {
            return "unsupported";
        }
        final HashAlgorithmType algType = this.map.get(hashAlgorithmTypeId);
        if (algType.isIsDeleted()) {
            return "deprecated";
        }
        return algType.getName();
    }
    
    /**
     * Internal keys do not use SHA256.
     */
    @Override
    public Map<Integer, HashAlgorithmData> findForInternalKeyRotation() {
        int sha1Id = Byte.valueOf(CryptoConstants.HASH_ALGORITHM_TYPE_SHA1).intValue();
        Map<Integer, HashAlgorithmData> map = new HashMap<Integer, HashAlgorithmData>(1);
        map.put(sha1Id, hashAlgorithmNotSigningAllMap.get(sha1Id));
        return map;
    }
    
    
    /**
     * It's valid if:
     * - has 'ClassName' value
     * - has 'SignatureId' value not 0
     * 
     * @param algorithmType
     *            HashAlgorithmType record/object.
     * @return boolean true if it's valid.
     */
    private boolean validHashAlgorithmType(HashAlgorithmType algorithmType) {
        if (StringUtils.isNotBlank(algorithmType.getClassName()) && algorithmType.getSignatureId() != null && algorithmType.getSignatureId().intValue() > 0) {
            return true;
        }
        return false;
    }
    
    // For JUnit test.
    public void setHashAlgorithmsTypes(List<HashAlgorithmType> hashAlgorithmsTypes) {
        this.hashAlgorithmsTypes = hashAlgorithmsTypes;
    }
    
    // For JUnit test.
    public void setHashAlgorithmNotSigningMap(Map<Integer, HashAlgorithmData> hashAlgorithmNotSigningMap) {
        this.hashAlgorithmNotSigningMap = hashAlgorithmNotSigningMap;
    }

    // For JUnit test.
    public void setHashAlgorithmNotSigningAllMap(Map<Integer, HashAlgorithmData> hashAlgorithmNotSigningMap) {
        this.hashAlgorithmNotSigningAllMap = hashAlgorithmNotSigningMap;
    }    
}
