package com.digitalpaytech.service.impl;

import java.util.Collection;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.service.CustomerRoleService;

@Component("customerRoleService")
@Transactional(propagation=Propagation.SUPPORTS)
public class CustomerRoleServiceImpl implements CustomerRoleService {
	
	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
		return this.entityDao.getNamedQuery("CustomerRole.findCustomerRoleByCustomerId")
				.setParameter("customerId", customerId)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
	}
}
