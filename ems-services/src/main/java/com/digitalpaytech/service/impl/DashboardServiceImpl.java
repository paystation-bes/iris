package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.TabsGroup;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WidgetConstants;

@Service("dashboardService")
@Transactional(propagation = Propagation.REQUIRED)
public class DashboardServiceImpl implements DashboardService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Tab> findTabsByUserAccountId(final int userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("findTabsByUserAccountId", HibernateConstants.ID, userAccountId, true);
    }
    
    /**
     * findTabByIdEager searches Tab, Section and Widget tables at first. If nothing is returned it would find Tab and Section only.
     * Hibernate session would be evict at the end of the method.
     * 
     * @param id
     *            Tab id number.
     * @return Tab All associated classes with Tab are retrieved. Return 'null' if couldn't find any result.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Tab findTabByIdEager(final int id) {
        Tab tab = findTabWithCriteria(id, true);
        if (tab == null) {
            tab = findTabWithCriteria(id, false);
        }
        
        // Cannot find a Tab with or without widgets.
        if (tab == null) {
            return null;
        }
        
        final List<Section> list = new LinkedList<Section>();
        
        final Iterator<Section> iter = tab.getSections().iterator();
        while (iter.hasNext()) {
            final Section sec = iter.next();
            sec.prepareSectionColumns();
            list.add(sec);
        }
        tab.setSections(list);
        
        //evict does not evict inner classes - cascading evict
        for (Section sec : tab.getSections()) {
            for (Widget wid : sec.getWidgets()) {
                this.entityDao.evict(wid);
            }
            this.entityDao.evict(sec);
        }
        this.entityDao.evict(tab);
        
        return tab;
    }
    
    /**
     * Depending on searchWidgetsFlag flag this method would search Tab, Section or with Widget tables.
     * 
     * @param id
     *            Tab id number.
     * @param searchWidgetsFlag
     *            'true' for searching Widget table as well, otherwise only including Tab and Section.
     * @return Tab All associated classes with Tab are retrieved. Return 'null' if couldn't find any result.
     */
    @SuppressWarnings("unchecked")
    private Tab findTabWithCriteria(final int id, final boolean searchWidgetsFlag) {
        final Tab tab = (Tab) this.entityDao.getNamedQuery("findTabById").setParameter(HibernateConstants.ID, id).uniqueResult();
        if (tab != null) {
            tab.setSections((List<Section>) this.entityDao.getNamedQuery("findSectionsByTabId").setParameter("tabId", id).list());
            
            if (searchWidgetsFlag && (tab.getSections().size() > 0)) {
                final Map<Integer, Section> sectionsMap = new HashMap<Integer, Section>(tab.getSections().size() * 2);
                for (Section s : tab.getSections()) {
                    sectionsMap.put(s.getId(), s);
                    
                    s.setTab(tab);
                    s.setWidgets(new ArrayList<Widget>());
                }
                
                final List<Widget> sectionsWidgets = this.entityDao.getNamedQuery("findWidgetsBySectionId")
                        .setParameterList("sectionId", sectionsMap.keySet()).list();
                for (Widget w : sectionsWidgets) {
                    final Section s = sectionsMap.get(w.getSection().getId());
                    
                    w.setSection(s);
                    s.getWidgets().add(w);
                }
            }
        }
        
        return tab;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Tab> findDefaultTabs() {
        final int id = WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID;
        return findTabsByUserAccountId(id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<Tab> findDefaultTabs(final boolean parentFlag) {
        if (!parentFlag) {
            return findDefaultTabs();
        }
        return this.entityDao.findByNamedQueryAndNamedParam("Tab.findParentAdminTabsByUserAccountId", HibernateConstants.ID,
                                                            WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID, true);
    }
    
    @Override
    public final void deleteTabsByUserId(final int userId) {
        final List<Tab> tabCollection = (List<Tab>) findTabsByUserAccountId(userId);
        for (int t = 0; t < tabCollection.size(); t++) {
            /*
             * List<Section> sectionCollection = tabCollection.get(t).getSections();
             * for(int s = 0; s < sectionCollection.size(); s++)
             * {
             * List<Widget> widgetCollection = sectionCollection.get(s).getWidgets();
             * for(int w = 0; w < widgetCollection.size(); w++)
             * {
             * deleteWidgetById(widgetCollection.get(w).getId());
             * }
             * deleteSectionById(sectionCollection.get(s).getId());
             * }
             */
            delete(tabCollection.get(t));
        }
    }
    
    @Override
    public final void delete(final Object object) {
        this.entityDao.delete(object);
    }
    
    private void deleteSectionsWidgets(final List<Integer> sectionIds, final List<Integer> widgetIds, final List<Integer> subsetMemberIds) {
        // Updates widgetIds and deletes sections.
        List<Integer> updatedWidgetIds = new ArrayList<Integer>();
        List<Integer> updatedWidgetSusbetIds = new ArrayList<Integer>();
        updatedWidgetIds = widgetIds;
        updatedWidgetSusbetIds = subsetMemberIds;
        
        Integer sectionId;
        final Iterator<Integer> secIdsIter = sectionIds.iterator();
        while (secIdsIter.hasNext()) {
            sectionId = secIdsIter.next();
            final Section secObj = (Section) this.entityDao.get(Section.class, sectionId);
            if ((secObj == null) || (secObj.getUserAccountId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID)) {
                continue;
            }
            // Ensures widgets in a Section don't exist in widgetIds.
            for (Widget widget : secObj.getWidgets()) {
                if (updatedWidgetIds.contains(widget.getId())) {
                    updatedWidgetIds.remove(widget.getId());
                }
            }
            delete(secObj);
        }
        
        deleteWidgets(updatedWidgetIds, updatedWidgetSusbetIds);
        deleteSubsetMembers(updatedWidgetSusbetIds);
    }
    
    private void deleteWidgets(final List<Integer> updatedWidgetIds, final List<Integer> updatedWidgetSusbetIds) {
        // Deletes widgets from 'updatedWidgetIds'.
        Integer widgetId;
        final Iterator<Integer> wigIdIter = updatedWidgetIds.iterator();
        while (wigIdIter.hasNext()) {
            widgetId = wigIdIter.next();
            final Widget widObj = (Widget) this.entityDao.get(Widget.class, widgetId);
            if ((widObj == null) || (widObj.getUserAccountId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID)) {
                continue;
            }
            // Ensures susbetMembers in a Widget don't exist in widgetSubsetIds.
            for (WidgetSubsetMember subsetMember : widObj.getWidgetSubsetMembers()) {
                if (updatedWidgetSusbetIds.contains(subsetMember.getId())) {
                    updatedWidgetSusbetIds.remove(subsetMember.getId());
                }
            }
            delete(widObj);
        }
    }
    
    private void deleteSubsetMembers(final List<Integer> updatedWidgetSusbetIds) {
        // Deletes subset members from 'subsetMemberIds'.
        Integer subsetMemberId;
        final Iterator<Integer> subsetIdIter = updatedWidgetSusbetIds.iterator();
        while (subsetIdIter.hasNext()) {
            subsetMemberId = subsetIdIter.next();
            final WidgetSubsetMember subObj = (WidgetSubsetMember) this.entityDao.get(WidgetSubsetMember.class, subsetMemberId);
            if (subObj == null) {
                continue;
            }
            delete(subObj);
        }
    }
    
    /**
     * If widget id is already in Section's widgets, ignore the id and won't be in the result list.
     * 
     * @param widgetsInSection
     *            List of Widget objects that exist in Section.
     * @param widgetIds
     *            List of widget ids need to be removed.
     * @return List<Integer> list of widget ids that do NOT exist in input variable widgetsInSection.
     */
    private List<Integer> getWidgetIdsForDelete(final List<Widget> widgetsInSection, final List<Integer> widgetIds) {
        final List<Integer> widgetIdsInSection = new ArrayList<Integer>(widgetsInSection.size());
        final Iterator<Widget> wIter = widgetsInSection.iterator();
        while (wIter.hasNext()) {
            widgetIdsInSection.add(wIter.next().getId());
        }
        
        final List<Integer> updatedIds = new ArrayList<Integer>();
        final Iterator<Integer> iter = widgetIds.iterator();
        while (iter.hasNext()) {
            final Integer wid = iter.next();
            if (!widgetIdsInSection.contains(wid)) {
                updatedIds.add(wid);
            }
        }
        return updatedIds;
    }
    
    @Override
    public final void saveOrUpdateTab(final Tab tab, final UserAccount userAccount) {
        if (tab.getId() == null || tab.getId() == 0 || userAccount.getIsDefaultDashboard()) {
            this.entityDao.save(tab);
        } else {
            //Tab mergedTab = (Tab) this.entityDao.merge(tab);           
            //entityDao.saveOrUpdate(mergedTab);
            this.entityDao.saveOrUpdate(tab);
        }
    }
    
    @Override
    public final void saveTabs(final TabsGroup tabsGroup, final UserAccount mergedUserAccount) {
        final Map<String, Tab> tabTreeMap = tabsGroup.getTabTreeMap();
        final UserAccount userAccount = tabsGroup.getUserAccount();
        
        final TabsGroup newTabsGroup = removeRedundantDeletedEntities(tabsGroup);
        
        for (Map.Entry<String, Tab> entry : tabTreeMap.entrySet()) {
            final Tab tab = entry.getValue();
            if (tab.isToBeDeleted() && tab.getId() != null) {
                delete(this.entityDao.get(Tab.class, tab.getId()));
                
            } else if (tab.isToBeDeleted()) {
                // Tabs marked for deletion that are part of the default list
                // will not have null Id's. We do not want to delete these
                continue;
            } else {
                saveOrUpdateTab(tab, userAccount);
            }
        }
        
        //entityDao.flush();    
        deleteSectionsWidgets(newTabsGroup.getSectionIds(), newTabsGroup.getWidgetIds(), newTabsGroup.getSubsetMemberIds());
        
        updateUserAccount(userAccount, mergedUserAccount);
    }
    
    /**
     * Removes from the TabsGroup object any subset member, widget, or section
     * Id's that would already have been deleted through the removal of any tab
     * that is 'isToBeDeleted'.
     */
    private TabsGroup removeRedundantDeletedEntities(final TabsGroup tabsGroup) {
        
        final Iterator<Entry<String, Tab>> iter = tabsGroup.getTabTreeMap().entrySet().iterator();
        while (iter.hasNext()) {
            Tab tab = (Tab) iter.next().getValue();
            if (tab.getId() == null || !tab.isToBeDeleted()) {
                continue;
            }
            tab = findTabByIdEager(tab.getId());
            for (Section section : tab.getSections()) {
                if (tabsGroup.getSectionIds().contains(section.getId())) {
                    tabsGroup.getSectionIds().remove(section.getId());
                }
                for (Widget widget : section.getWidgets()) {
                    if (tabsGroup.getWidgetIds().contains(widget.getId())) {
                        tabsGroup.getWidgetIds().remove(widget.getId());
                    }
                    for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
                        if (tabsGroup.getSubsetMemberIds().contains(subsetMember.getId())) {
                            tabsGroup.getSubsetMemberIds().remove(subsetMember.getId());
                        }
                    }
                }
            }
        }
        return tabsGroup;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Widget findWidgetById(final int widgetId) {
        return (Widget) this.entityDao.get(Widget.class, widgetId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Widget findDefaultWidgetByIdAndEvict(final int widgetId) {
        final Widget widget = (Widget) this.entityDao.get(Widget.class, widgetId);
        this.entityDao.evict(widget);
        return widget;
    }
    
    @Override
    public final void updateUserAccount(final UserAccount userAccount, final UserAccount mergedUserAccount) {
        //update user account isDefaultDashboard boolean
        userAccount.setLastModifiedByUserId(mergedUserAccount.getLastModifiedByUserId());
        userAccount.setIsDefaultDashboard(false);
        this.entityDao.update(userAccount);
    }
    
    // This method must return the "Unassigned" Location. 
    // Because if there is only one location and it is unassigned, we have to display it on the Widget Settings !
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Location> findLocationsByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByCustomerId", HibernateConstants.CUSTOMER_ID, customerId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Customer> findAllChildCustomers(final int parentCustomerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Customer.findAllChildCustomersByParentCustomerId", "parentCustomerId", parentCustomerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Route> findRoutesByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Route.findRoutesByCustomerId", HibernateConstants.CUSTOMER_ID, customerId, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final WidgetListType getWidgetListTypeById(final int widgetListTypeId) {
        @SuppressWarnings("unchecked")
        final List<WidgetListType> list = this.entityDao.findByNamedQueryAndNamedParam("findWidgetListTypeById", HibernateConstants.ID,
                                                                                       widgetListTypeId, true);
        if (list == null) {
            return null;
        }
        final WidgetListType widgetListType = list.get(0);
        return widgetListType;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Widget> findWidgetMasterList(final UserAccount user) {
        @SuppressWarnings("unchecked")
        final List<Widget> list = this.entityDao.findByNamedQueryAndNamedParam("findWidgetMasterList", "widgetListTypeId",
                                                                               determineWidgetListTypeId(user.getCustomer().isIsParent()));
        
        final List<CustomerSubscription> customerSubscriptionList = this.customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(user
                .getCustomer().getId(), true);
        
        final Set<Integer> activeSubscriptions = new HashSet<Integer>();
        for (CustomerSubscription customerSubscription : customerSubscriptionList) {
            if (customerSubscription.isIsEnabled()) {
                activeSubscriptions.add(customerSubscription.getSubscriptionType().getId());
            }
        }
        
        final Iterator<Widget> iter = list.iterator();
        while (iter.hasNext()) {
            final Widget widget = iter.next();
            
            if (widget.getWidgetMetricType().getSubscriptionType() != null
                && !activeSubscriptions.contains(widget.getWidgetMetricType().getSubscriptionType().getId())) {
                iter.remove();
            }
            // If it's not parent AND (widget tier 1 type is ALL_ORG, ORG, or PARENT_ORG), remove item from iterator.
            if (!user.getCustomer().isIsParent()
                && (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ALL_ORG || widget
                        .getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ORG)) {
                
                iter.remove();
            } else if (user.getCustomer().isIsParent() && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_REVENUE
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_PURCHASES
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_MAP
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_CITATIONS
                       && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_CITATION_MAP) {
                // If it's parent only retains WidgetMetricType is Revenue, Purchases, Map or card processing.
                iter.remove();
            }
        }
        
        return list;
    }
    
    private Integer determineWidgetListTypeId(final boolean parentFlag) {
        if (parentFlag) {
            return WidgetConstants.LIST_TYPE_PARENT_MASTER;
        }
        return WidgetConstants.LIST_TYPE_MASTER;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<UnifiedRate> findUnifiedRatesByCustomerId(final int customerId, final int pastMonths) {
        final Calendar cal = new GregorianCalendar();
        cal.add(Calendar.MONTH, pastMonths * -1);
        
        final String[] params = { HibernateConstants.CUSTOMER_ID, "pastGmt" };
        final Object[] values = { customerId, DateUtil.changeTimeZone(cal.getTime(), "GMT") };
        
        return this.entityDao.findByNamedQueryAndNamedParam("UnifiedRate.findRatesByCustomerIdPastMonths", params, values, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<WidgetSubsetMember> findSubsetMemberByWidgetIdTierTypeId(final Integer widgetId, final int widgetTierType) {
        final String[] params = { "widgetId", "tierTypeId" };
        final Object[] values = { widgetId, widgetTierType };
        
        return this.entityDao.findByNamedQueryAndNamedParam("WidgetSubsetMember.findWidgetSubsetMemberByWidgetIdTierTypeId", params, values, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final UnifiedRate findUnifiedRateById(final int rateId) {
        return (UnifiedRate) this.entityDao.get(UnifiedRate.class, rateId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final RevenueType findRevenueTypeById(final byte revenueTypeId) {
        return (RevenueType) this.entityDao.get(RevenueType.class, revenueTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Route findRouteById(final int routeId) {
        return (Route) this.entityDao.get(Route.class, routeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final TransactionType findTransactionTypeById(final int transactionTypeId) {
        return (TransactionType) this.entityDao.get(TransactionType.class, transactionTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Location findLocationById(final int locationId) {
        return (Location) this.entityDao.get(Location.class, locationId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final WidgetTierType findWidgetTierTypeByTierTypeId(final int tierTypeId) {
        return (WidgetTierType) this.entityDao.get(WidgetTierType.class, tierTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final <T> T findObjectByClassTypeAndId(final Class<T> clazz, final int id) {
        return (T) this.entityDao.get(clazz, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<Widget> findMapWidgetsByUserAccountId(final int userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("findMapWidgetsByUserAccountId", "userAccountId", userAccountId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Customer findCustomerById(final int customerId) {
        return (Customer) this.entityDao.get(Customer.class, customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CollectionType findCollectionTypeById(final int collectionTypeId) {
        return (CollectionType) this.entityDao.get(CollectionType.class, collectionTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final MerchantAccount findMerchantAccountById(final int merchantAccountId) {
        return (MerchantAccount) this.entityDao.get(MerchantAccount.class, merchantAccountId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final AlertClassType findAlertClassTypeById(final byte alertClassTypeId) {
        return (AlertClassType) this.entityDao.get(AlertClassType.class, alertClassTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CardProcessMethodType findCardProcessMethodTypeById(final int cardProcessMethodTypeId) {
        return (CardProcessMethodType) this.entityDao.get(CardProcessMethodType.class, cardProcessMethodTypeId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<CardProcessMethodType> findSettledCardProcessMethodTypes() {
        return this.entityDao.findByNamedQuery("CardProcessMethodType.findSettledTypes", true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CitationType findCitationTypeById(final int citationTypeId) {
        return (CitationType) this.entityDao.get(CitationType.class, citationTypeId);
    }
}
