package com.digitalpaytech.service.impl;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.service.DenominationTypeService;
import com.digitalpaytech.domain.DenominationType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import javax.annotation.PostConstruct;

@Service("denominationTypeService")
public class DenominationTypeServiceImpl implements DenominationTypeService {
    @Autowired
    private OpenSessionDao openSessionDao;

    private Map<String, DenominationType> map;

    @PostConstruct
    private void loadAllToMap() {
        List<DenominationType> list = openSessionDao.loadAll(DenominationType.class);
        map = new HashMap<String, DenominationType>(list.size());
        
        Iterator<DenominationType> iter = list.iterator();
        while (iter.hasNext()) {
            DenominationType denType = iter.next();
            StringBuilder bdr = new StringBuilder();
            bdr.append(denType.getIsCoinOrBill()).append("_").append(denType.getDenominationAmount());
            map.put(bdr.toString(), denType);
        }
    }
    
    @Override
    public DenominationType findDenominationType(boolean isCoin, Integer denominationAmount) {
        StringBuilder bdr = new StringBuilder();
        if (isCoin) {
            bdr.append("1_");
        } else {
            bdr.append("2_");
        }
        bdr.append(denominationAmount);
        return map.get(bdr.toString());
    }
}
