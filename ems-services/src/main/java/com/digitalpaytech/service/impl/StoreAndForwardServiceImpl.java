package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSStoreAndForwardAttempt;
import com.digitalpaytech.service.StoreAndForwardService;

@Service("storeAndForwardService")
@Transactional(propagation = Propagation.SUPPORTS)
public class StoreAndForwardServiceImpl implements StoreAndForwardService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public CPSStoreAndForwardAttempt findByPointOfSaleIdAndPurchasedDateAndTicketNumber(final int pointOfSaleId, final Date purchasedDate,
        final int ticketNumber) {
        final String[] params = { "pointOfSale", "purchasedDate", "ticketNumber" };
        final Object[] values = { pointOfSaleId, purchasedDate, ticketNumber };
        
        final List<CPSStoreAndForwardAttempt> attempts = this.entityDao
                .findByNamedQueryAndNamedParam("CPSStoreAndForwardAttempt.findByPointOfSaleIdAndPurchasedDateAndTicketNumber", params, values);
        
        if (attempts != null && attempts.size() > 0) {
            return attempts.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public CPSStoreAndForwardAttempt findByChargeToken(final String chargeToken) {
        final List<CPSStoreAndForwardAttempt> attempts =
                this.entityDao.findByNamedQueryAndNamedParam("CPSStoreAndForwardAttempt.findByChargeToken", "chargeToken", chargeToken);
        
        if (attempts != null && attempts.size() > 0) {
            return attempts.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public void saveCPSStoreAndForwardAttempt(final CPSStoreAndForwardAttempt cPSStoreAndForwardAttempt) {
        this.entityDao.save(cPSStoreAndForwardAttempt);
    }
    
    @Override
    public void deleteCPSStoreAndForwardAttempt(final CPSStoreAndForwardAttempt cPSStoreAndForwardAttempt) {
        this.entityDao.delete(cPSStoreAndForwardAttempt);
    }
}