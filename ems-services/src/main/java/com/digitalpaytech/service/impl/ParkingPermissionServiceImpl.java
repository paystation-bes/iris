package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.service.ParkingPermissionService;

@Service("parkingPermissionService")
@Transactional(propagation=Propagation.REQUIRED)
public class ParkingPermissionServiceImpl implements ParkingPermissionService {

    @Autowired
    private EntityDao entityDao;
    
    
    @Override
    public ParkingPermission findByNameAndLocationId(String name, Integer locationId) {
        String[] params = { "name", "id" };
        Object[] values = { name, locationId };

        List<?> list = entityDao.findByNamedQueryAndNamedParam("ParkingPermission.findByNameAndLocationId", params, values);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (ParkingPermission) list.get(0);
    }
    
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }


    @Override
    public List getWeekendParkingPermissionsForSMSMessage(Date localExpiryDate, Date maxPermitDate, Integer locationId) {
        StringBuilder qry = new StringBuilder();
        qry.append("SELECT p.Id AS pId, pow.Id AS powId, sp.id AS spId FROM ParkingPermission p ")
           .append("INNER JOIN ParkingPermissionDayOfWeek pow ON p.Id = pow.ParkingPermissionId ")
           .append("LEFT OUTER JOIN SpecialPermissionDate sp ON sp.Id = p.SpecialPermissionDateId AND sp.StartDateLocal<= :maxPermitDate AND sp.EndDateLocal >= :date ")
           .append("WHERE pow.DayOfWeekId in (7, 1) ")
           .append("AND p.LocationId = :locationId ")
           .append("AND If (pow.DayOfWeekId = 7, 1,  p.BeginHourLocal * 100 + p.BeginMinuteLocal <= HOUR(:maxPermitDate) * 100 + MINUTE(:maxPermitDate) ) = 1 ")
           .append("AND (If (pow.DayOfWeekId = 7, p.EndHourLocal * 100 + p.EndMinuteLocal > HOUR(:date) * 100 + MINUTE(:date), 1) = 1 OR 0 = (p.EndHourLocal * 100 + p.EndMinuteLocal)) ") 
           .append("AND p.IsActive = 1 ")
           .append("ORDER BY pow.DayOfWeekId desc, p.BeginHourLocal, p.BeginMinuteLocal");
        SQLQuery query = this.entityDao.createSQLQuery(qry.toString());
        query.setParameter("date", localExpiryDate);
        query.setParameter("maxPermitDate", maxPermitDate);
        query.setParameter("locationId", locationId);
        
        query.addScalar("pId", new IntegerType());
        query.addScalar("powId", new IntegerType());
        query.addScalar("spId", new IntegerType());
        //PermissionTuples will be a list of object[3] arrays each corresponding to a trio of data objects, 
        //where object[0] = ParkingPermission.id, object[1] = ParkingPermissionDayOfWeek.id, object[2] = specialPermissionDate
        List permissionTuples = query.list();
        //result will be a list of object[3] arrays containing the ParkingPermission, ParkingPermissionDayOfWeek, and SpecialPermissionDate objects.
        List result = retrievePermissionObjectsWithIdTuples(permissionTuples);
        return result;
    }


	@Override
    public List getParkingPermissionsForSMSMessage(Date localExpiryDate, Date maxPermitDate, Integer locationId) {
        StringBuilder qry = new StringBuilder();
        qry.append("SELECT p.Id AS pId, pow.Id AS powId, sp.Id AS spId FROM ParkingPermission p ")
           .append("INNER JOIN ParkingPermissionDayOfWeek pow ON p.Id = pow.ParkingPermissionId ")
           .append("LEFT OUTER JOIN SpecialPermissionDate sp ON sp.Id = p.SpecialPermissionDateId AND sp.StartDateLocal<= :maxPermitDate AND sp.EndDateLocal >= :date ")
           .append("WHERE pow.DayOfWeekId in (DAYOFWEEK(:date), DAYOFWEEK(:maxPermitDate)) ")
           .append("AND p.LocationId = :locationId ")
           .append("AND pow.DayOfWeekId * 10000 + p.BeginHourLocal * 100 + p.BeginMinuteLocal <= DAYOFWEEK(:maxPermitDate) * 10000 + HOUR(:maxPermitDate) * 100 + MINUTE(:maxPermitDate) ")
           .append("AND (DAYOFWEEK(:date) * 10000 + HOUR(:date) * 100 + MINUTE(:date) < (pow.DayOfWeekId * 10000 + p.EndHourLocal * 100 + p.EndMinuteLocal) OR 0 = (p.EndHourLocal * 100 + p.EndMinuteLocal)) ") 
           .append("AND p.IsActive = 1 ")
           .append("ORDER BY pow.DayOfWeekId, p.BeginHourLocal, p.BeginMinuteLocal");
        SQLQuery query = this.entityDao.createSQLQuery(qry.toString());
        query.setParameter("date", localExpiryDate);
        query.setParameter("maxPermitDate", maxPermitDate);
        query.setParameter("locationId", locationId);
        
        query.addScalar("pId", new IntegerType());
        query.addScalar("powId", new IntegerType());
        query.addScalar("spId", new IntegerType());
        //PermissionTuples will be a list of object[3] arrays each corresponding to a trio of data objects, 
        //where object[0] = ParkingPermission.id, object[1] = ParkingPermissionDayOfWeek.id, object[2] = specialPermissionDate
        List permissionTuples = query.list();
        //result will be a list of object[3] arrays containing the ParkingPermission, ParkingPermissionDayOfWeek, and SpecialPermissionDate objects.
        List result = retrievePermissionObjectsWithIdTuples(permissionTuples);
        return result;
    }
    
	/**
	 * Given a list of size-3 object arrays (containing Integer ids for parkingpermission.id, parkingpermissiondayofweek.id, specialpermissiondate.id),
	 * retrieves a list populated with size-3 object arrays containing the corresponding objects.
	 * @param permissionTuples: a list of size-3 object arrays containing the integer ids.
	 * @return: a list of size-3 object arrays containing the corresponding objects to those ids.
	 */
    private List retrievePermissionObjectsWithIdTuples(List permissionTuples){
    	List result = new LinkedList();
    	for(Object o : permissionTuples){
        	Object[] permissionTupleIds = (Object[])o;
        	Object tuple[] = new Object[3];
        	// if both ParkingPermissionDayOfWeek and SpecialPermissionDate are NULL
        	if(permissionTupleIds[1] == null && permissionTupleIds[2] == null){
        		ParkingPermission p = (ParkingPermission)entityDao.findUniqueByNamedQueryAndNamedParam("ParkingPermission.findParkingPermissionById", "pId", permissionTupleIds[0]);
        		tuple[0] = p;
        		tuple[1] = null;
        		tuple[2] = null;
        	//if ParkingPermissionDayOfWeek is null
        	}else if(permissionTupleIds[1] == null){
        		Object[] incompleteTuple = (Object[])entityDao.findUniqueByNamedQueryAndNamedParam("ParkingPermission.findParkingPermissionDetailTuplesWithIdsNoDayOfWeek", new String[]{"pId", "spId"}, new Object[]{permissionTupleIds[0], permissionTupleIds[2]}, true);
        		tuple[0] = incompleteTuple[0];
        		tuple[1] = null;
        		tuple[2] = incompleteTuple[1];
        	//if SpecialPermissionDate is null
        	}else if(permissionTupleIds[2] == null){
				Object[] incompleteTuple = (Object[])entityDao.findUniqueByNamedQueryAndNamedParam("ParkingPermission.findParkingPermissionDetailTuplesWithIdsNoSpecial", new String[]{"pId", "powId"}, new Object[]{permissionTupleIds[0], permissionTupleIds[1]}, true);
        		tuple[0] = incompleteTuple[0];
        		tuple[1] = incompleteTuple[1];
        		tuple[2] = null;
        	//if all three fields are non-null
        	}else{
        		Object[] incompleteTuple = (Object[])entityDao.findUniqueByNamedQueryAndNamedParam("ParkingPermission.findParkingPermissionDetailTuplesWithIds", new String[]{"pId", "powId", "spId"}, permissionTupleIds, true);
        		tuple = incompleteTuple;
        	}
        	result.add(tuple);
        }
        return result;
    }
}
