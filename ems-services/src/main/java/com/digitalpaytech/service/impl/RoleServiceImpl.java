package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.service.RoleService;

@Component("roleService")
@Transactional(propagation = Propagation.REQUIRED)
public class RoleServiceImpl implements RoleService {
    
    private static final int ROLE_ID_IDX = 0;
    private static final int USER_ROLE_COUNT_ID_IDX = 1;
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void saveRoleAndRolePermission(final Role role) {
        
        this.entityDao.save(role);
        final Set<RolePermission> rolePermissions = role.getRolePermissions();
        for (RolePermission rp : rolePermissions) {
            this.entityDao.save(rp);
        }
        final Set<CustomerRole> customerRoles = role.getCustomerRoles();
        for (CustomerRole customerRole : customerRoles) {
            this.entityDao.save(customerRole);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public Role findRoleByRoleIdAndEvict(final Integer roleId) {
        
        final Role role = (Role) this.entityDao.get(Role.class, roleId);
        if (role != null) {
            this.entityDao.evict(role);
        }
        return role;
    }
    
    public void updateRoleAndRolePermission(final Role role) {
        
        this.entityDao.update(role);
        this.entityDao.deleteAllByNamedQuery("RolePermission.deleteAllByRoleId", new Object[] { role.getId() });
        final Set<RolePermission> rolePermissions = role.getRolePermissions();
        for (RolePermission rp : rolePermissions) {
            this.entityDao.save(rp);
        }
    }
    
    public void updateRole(final Role role) {
        this.entityDao.update(role);
    }
    
    @Override
    public void saveCustomerRoles(final Set<CustomerRole> customerRoles) {
        for (CustomerRole customerRole : customerRoles) {
            final CustomerRole existingCustomerRole = (CustomerRole) this.entityDao
                    .findUniqueByNamedQueryAndNamedParam("CustomerRole.findCustomerRoleByCustomerIdAndRoleId",
                                                         new String[] { "customerId", "roleId" },
                                                         new Object[] { customerRole.getCustomer().getId(), customerRole.getRole().getId() }, true);
            if (existingCustomerRole == null) {
                this.entityDao.saveOrUpdate(customerRole);
            }
        }
    }
    
    @Override
    public void deleteRole(final Role role) {
        
        final Role newRole = (Role) this.entityDao.merge(role);
        final Set<UserRole> userRoleList = newRole.getUserRoles();
        final Set<CustomerRole> customerRoleList = newRole.getCustomerRoles();
        final Set<RolePermission> rolePermissionList = newRole.getRolePermissions();
        
        if (userRoleList != null && !userRoleList.isEmpty()) {
            for (UserRole userRole : userRoleList) {
                this.entityDao.delete(userRole);
            }
        }
        if (customerRoleList != null && !customerRoleList.isEmpty()) {
            for (CustomerRole customerRole : customerRoleList) {
                this.entityDao.delete(customerRole);
            }
        }
        if (rolePermissionList != null && !rolePermissionList.isEmpty()) {
            for (RolePermission rolePermission : rolePermissionList) {
                this.entityDao.delete(rolePermission);
            }
        }
        this.entityDao.delete(newRole);
        
    }
    
    @Override
    public Role findRoleByName(final Integer customerId, final String roleName) {
        return (Role) this.entityDao.findUniqueByNamedQueryAndNamedParam("Role.findByNameAndCustomerId", new String[] { "customerId", "roleName" },
                                                                         new Object[] { customerId, roleName }, true);
    }
    
    @Override
    public List<UserAccount> findUserAccountByRoleId(final Integer roleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserRole.findUserAccountByRoleId", "roleId", roleId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Map<Integer, Boolean> findNumberOfUserRolesPerRoleIdByCustomerId(final Integer customerId) {
        final List<Object[]> userRoleCountMapList =
                this.entityDao.findByNamedQueryAndNamedParam("UserRole.findNumberOfUserRolesPerRoleIdByCustomerId", "customerId", customerId);
        
        final Map<Integer, Boolean> roleSettingMap = new HashMap<Integer, Boolean>();
        boolean hasUserRoles;
        for (Object[] tuple : userRoleCountMapList) {
            hasUserRoles = (Long) tuple[USER_ROLE_COUNT_ID_IDX] > 0 ? true : false;
            roleSettingMap.put((Integer) tuple[ROLE_ID_IDX], hasUserRoles);
        }
        
        return roleSettingMap;
    }
    
}
