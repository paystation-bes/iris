package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.FilterDTOTransformer;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

@Service("rateProfileService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateProfileServiceImpl implements RateProfileService {
    private static final String NQ_RATE_PROFILE_NAME = "RateProfile";
    private static final String NQ_METHOD_TYPE_FIND = ".find";
    private static final String NQ_METHOD_TYPE_COUNT = ".count";
    private static final String NQ_BASIC_WITH_CUSTOMERID = "RateProfilesByCustomerId";
    private static final String NQ_AND_ISPUBLISHED = "AndIsPublished";
    private static final String NQ_AND_PERMITISSUETYPEID = "AndPermitIssueTypeId";
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final RateProfile findRateProfileById(final Integer rateProfileId) {
        return (RateProfile) this.entityDao.findUniqueByNamedQueryAndNamedParam("RateProfile.findRateProfileById", "rateProfileId", rateProfileId);
    }
    
    @Override
    public final RateProfile findRateProfileByCustomerIdAndPermitIssueTypeIdAndName(final Integer customerId, final Integer permitIssueTypeId, final String name) {
        final List<RateProfile> rateProfileList = this.entityDao
                .findByNamedQueryAndNamedParam("RateProfile.findRateProfileByCustomerIdAndPermitIssueTypeIdAndName", new String[] { "customerId",
                        "permitIssueTypeId", "name" }, new Object[] { customerId, permitIssueTypeId, name }, true);
        
        if (rateProfileList != null && !rateProfileList.isEmpty()) {
            return rateProfileList.get(0);
        } else {
            return null;
        }
        
    }
    
    @Override
    public final List<RateProfile> findRateProfilesByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("RateProfile.findRateProfilesByCustomerId", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FilterDTO> findPublishedRateProfileFiltersByCustomerId(final Integer customerId, final RandomKeyMapping keyMapping) {
        return this.entityDao.getNamedQuery("RateProfile.findPublishedRateProfileFiltersByCustomerId").setParameter("customerId", customerId)
                .setResultTransformer(new FilterDTOTransformer(keyMapping, RateProfile.class)).list();
    }
    
    @Override
    public final List<RateProfile> findRateProfilesByCustomerIdAndPermitIssueTypeId(final Integer customerId, final Integer permitIssueTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("RateProfile.findRateProfilesByCustomerIdAndRateProfileTypeId", new String[] { "customerId",
                "permitIssueTypeId", }, new Object[] { customerId, permitIssueTypeId, });
    }
    
    @Override
    public final List<RateProfile> findPagedRateProfileList(final Integer customerId, final Integer permitIssueTypeId, final Boolean isPublished,
                                                            final Date maxUpdateTime, final int pageNumber) {
        final List<String> parameters = new ArrayList<String>();
        final List<Object> values = new ArrayList<Object>();
        final StringBuilder namedQuery = new StringBuilder(NQ_RATE_PROFILE_NAME).append(NQ_METHOD_TYPE_FIND).append(NQ_BASIC_WITH_CUSTOMERID);
        parameters.add("customerId");
        parameters.add("maxUpdateTime");
        values.add(customerId);
        values.add(maxUpdateTime);
        if (isPublished != null) {
            parameters.add("isPublished");
            values.add(isPublished);
            namedQuery.append(NQ_AND_ISPUBLISHED);
        }
        if (permitIssueTypeId != null) {
            parameters.add("permitIssueTypeId");
            values.add(permitIssueTypeId);
            namedQuery.append(NQ_AND_PERMITISSUETYPEID);
            
        }
        return this.entityDao.findPageResultByNamedQuery(namedQuery.toString(), parameters.toArray(new String[1]), values.toArray(), true, pageNumber,
                                                         WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveOrUpdateRateProfile(final RateProfile rateProfile, final Collection<RateRateProfile> rateRateProfileList,
                                              final Collection<RateProfileLocation> rateProfileLocationList) {
        
        this.entityDao.merge(rateProfile);
        for (RateRateProfile rateRateProfile : rateRateProfileList) {
            this.entityDao.merge(rateRateProfile);
        }
        for (RateProfileLocation rateProfileLocation : rateProfileLocationList) {
            rateProfileLocation.setNextPublisGmt(rateProfileLocation.getStartGmt());
            this.entityDao.merge(rateProfileLocation);
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteRateProfile(final RateProfile rateProfile, final Integer userAccountId) {
        final Date currentTime = new Date();
        
        rateProfile.setLastModifiedGmt(currentTime);
        rateProfile.setLastModifiedByUserId(userAccountId);
        rateProfile.setIsDeleted(true);
        
        final List<RateRateProfile> rateRateProfileList = this.entityDao.findByNamedQueryAndNamedParam("RateRateProfile.findActiveRatesByRateProfileId",
                                                                                                       "rateProfileId", rateProfile.getId());
        for (RateRateProfile rateRateProfile : rateRateProfileList) {
            rateRateProfile.setIsDeleted(true);
            rateRateProfile.setLastModifiedGmt(currentTime);
            rateRateProfile.setLastModifiedByUserId(userAccountId);
            this.entityDao.saveOrUpdate(rateRateProfile);
        }
        this.entityDao.saveOrUpdate(rateProfile);
    }
    
    @Override
    public final List<RateProfile> searchRateProfiles(final Integer customerId, final String keyword) {
        final String keywordString = new StringBuilder("%").append(keyword).append("%").toString();
        
        return this.entityDao.findByNamedQueryAndNamedParam("RateProfile.findAllRateProfilesByNameKeywordAndCustomerId",
                                                            new String[] { "customerId", "keyword" }, new Object[] { customerId, keywordString });
        
    }
    
    @Override
    public final int findRateProfilePage(final Integer rateProfileId, final Integer customerId, final Integer permitIssueTypeId, final Boolean isPublished,
                                         final Date maxUpdateTime) {
        
        int result = -1;
        
        final RateProfile endRateProfile = this.entityDao.get(RateProfile.class, rateProfileId);
        double recordNumber = 0;
        
        final List<String> parameters = new ArrayList<String>();
        final List<Object> values = new ArrayList<Object>();
        final StringBuilder namedQuery = new StringBuilder(NQ_RATE_PROFILE_NAME).append(NQ_METHOD_TYPE_COUNT).append(NQ_BASIC_WITH_CUSTOMERID);
        parameters.add("customerId");
        parameters.add("name");
        parameters.add("maxUpdateTime");
        values.add(customerId);
        values.add(endRateProfile.getName());
        values.add(maxUpdateTime);
        if (isPublished != null) {
            parameters.add("isPublished");
            values.add(isPublished);
            namedQuery.append(NQ_AND_ISPUBLISHED);
        }
        if (permitIssueTypeId != null) {
            parameters.add("permitIssueTypeId");
            values.add(permitIssueTypeId);
            namedQuery.append(NQ_AND_PERMITISSUETYPEID);
            
        }
        recordNumber = ((Number) this.entityDao.findUniqueByNamedQueryAndNamedParam(namedQuery.toString(), parameters.toArray(new String[1]), values.toArray(),
                                                                                    true)).doubleValue();
        if (recordNumber > 0) {
            result = (int) Math.ceil(recordNumber / WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        }
        
        return result;
        
    }
}
