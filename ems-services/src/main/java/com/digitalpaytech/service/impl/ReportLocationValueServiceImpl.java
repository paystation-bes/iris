package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.service.ReportLocationValueService;

@Service("reportLocationValueService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ReportLocationValueServiceImpl implements ReportLocationValueService {

	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
    	this.entityDao = entityDao;
    }

	@Override
	public List<ReportLocationValue> findReportLocationValuesByReportDefinition(int reportDefinitionId) {
		return entityDao.findByNamedQueryAndNamedParam("ReportLocationValue.findReportLocationValuesByReportDefinition", "reportDefinitionId", reportDefinitionId);
	}

}
