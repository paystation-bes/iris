package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SmsTransactionLog;
import com.digitalpaytech.service.SmsTransactionLogService;

@Service("smsTransactionLogService")
@Transactional(propagation=Propagation.REQUIRED)
public class SmsTransactionLogServiceImpl implements SmsTransactionLogService {

    @Autowired
    private EntityDao entityDao;
    
    @Override
    public void saveSmsTransactionLog(SmsTransactionLog smsTransactionLog) {
        this.entityDao.save(smsTransactionLog);
    }
    
}
