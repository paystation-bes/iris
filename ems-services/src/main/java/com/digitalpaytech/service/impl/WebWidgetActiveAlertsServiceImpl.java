package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetActiveAlertsService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings("PMD.UseConcurrentHashMap")
public class WebWidgetActiveAlertsServiceImpl implements WebWidgetService {

    public static final Map<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(14);
    public static final Map<Integer, String> TIER_TYPES_ALIASES_MAP = new HashMap<Integer, String>(2);
    private static final String POS_LOCATION_ID = "pos.LocationId ";
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    static {
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, "rp.RouteId, pos.Id ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, POS_LOCATION_ID);
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, POS_LOCATION_ID);
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ALERT_TYPE, "IFNULL(at.AlertClassTypeId, 3) ");
        
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, "PointOfSaleId");
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_ALERT_TYPE, "AlertClassTypeId");
    }
    
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final WebWidgetHelperService getWebWidgetHelperService() {
        return this.webWidgetHelperService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings({"PMD.ConsecutiveAppendsShouldReuse", "PMD.ConsecutiveLiteralAppends", "PMD.InsufficientStringBufferDeclaration"})
    public final WidgetData getWidgetData(final Widget widget, final UserAccount userAccount) {
        
        final StringBuilder queryStr = new StringBuilder();
        
        final String queryLimit = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                  EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        
        // needed to set the widgetRunDate
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), new StringBuilder(), widget.getWidgetRangeType()
                .getId(), widget.getWidgetTierTypeByWidgetTier1Type().getId(), false, true);
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        queryStr.append(", SUM(IFNULL(wData.WidgetMetricValue, 0)) AS WidgetMetricValue ");
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        final Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        /* "wData" SELECT expression */
        queryStr.append("SELECT pos.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, TIER_TYPES_ALIASES_MAP, false);
        
        queryStr.append(", COUNT(DISTINCT pec.PointOfSaleId) AS WidgetMetricValue ");
        
        queryStr.append("FROM POSEventCurrent pec ");
        queryStr.append("RIGHT JOIN PointOfSale pos ON (pec.PointOfSaleId = pos.id AND pec.IsActive = 1) ");
        queryStr.append("INNER JOIN POSStatus poss ON (poss.PointOfSaleId = pos.id AND poss.IsVisible = 1 AND "
                + "poss.IsDecommissioned = 0 AND poss.IsDeleted = 0) ");
        queryStr.append("LEFT JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id) ");
        queryStr.append("LEFT JOIN AlertThresholdType att ON (cat.AlertThresholdTypeId = att.Id) ");
        queryStr.append("LEFT JOIN AlertType at ON (att.AlertTypeId = at.Id) ");
        queryStr.append("LEFT JOIN AlertClassType act ON (at.AlertClassTypeId = act.Id) ");
        
        if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ROUTE) {
            queryStr.append("INNER JOIN RoutePOS rp ON (rp.PointOfSaleId = pos.Id) ");
        }
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "pos.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY pos.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, false, null, false);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(this.webWidgetHelperService, widget, query, userAccount, false);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        @SuppressWarnings("unchecked")
        final List<WidgetMetricInfo> rawWidgetData = query.list();
        
        widgetData = this.webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        
        return widgetData;
    }
}
