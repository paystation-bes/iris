package com.digitalpaytech.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.service.EmailService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.dto.Email;

@Component("emailService")
public class EmailServiceImpl implements EmailService {
    private static final String IRIS_NAME_STRING = "Digital Iris";
    
    private static final String MAIL_HOST_KEY = "mail.smtp.host";
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    private Session session;
    
    @Override
    public final void sendNow(final Email email) throws MessagingException {
        final MimeMessage message = email.buildMimeMessage(session());
        if ((message.getFrom() == null) || (message.getFrom().length <= 0)) {
            message.setFrom(defaultSender());
        }
        
        Transport.send(message);
    }
    
    private Session session() {
        if (this.session == null) {
            final Properties props = System.getProperties();
            props.put(MAIL_HOST_KEY, resolveMailServer());
            this.session = Session.getDefaultInstance(props);
        }
        
        return this.session;
    }
    
    private String resolveMailServer() {
        String host = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAIL_SERVER_URL);
        if ((host == null) || (host.trim().length() <= 0)) {
            if (this.emsPropertiesService.isWindowsOS()) {
                host = "192.168.0.1";
            } else {
                host = "localhost";
            }
        }
        return host;
    }
    
    private Address defaultSender() throws MessagingException {
        Address result = null;
        
        final String myAddress = this.emsPropertiesService
                .getPropertyValue(EmsPropertiesService.EMS_ALERT_FROM_EMAIL_ADDRESS, EmsPropertiesService.EMS_ADDRESS_STRING);
        try {
            result = new InternetAddress(myAddress, IRIS_NAME_STRING);
        } catch (UnsupportedEncodingException e) {
            throw new MessagingException("Invalid default sender: " + myAddress, e);
        }
        
        return result;
    }
    
}
