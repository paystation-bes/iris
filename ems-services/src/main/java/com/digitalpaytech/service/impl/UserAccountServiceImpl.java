package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserDefaultDashboard;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;

@Service("userAccountService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings("checkstyle:designforextension")
public class UserAccountServiceImpl implements UserAccountService {
    private static final String HQL_DELETE_ALL_ROLE_BY_USER_ACCOUNT = "UserRole.deleteAllByUserAccountId";
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    /**
     * Find list of roles first from UerAccount id, then get roles for each role.
     * 
     * @param userAccountId
     *            Id for a particular UserAccount record
     * @return List<Permission> All permissions for this UserAccount.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Permission> findPermissions(final int userAccountId) {
        final List<Permission> finalPermissions = new ArrayList<Permission>();
        
        Role role;
        final List<Role> roles = findRoles(userAccountId);
        final Iterator<Role> iter = roles.iterator();
        while (iter.hasNext()) {
            role = iter.next();
            finalPermissions.addAll(findPermissions(role.getRolePermissions().iterator()));
        }
        return finalPermissions;
    }
    
    /**
     * Find list of Permissions from RolePermission iterator.
     * 
     * @param iter
     *            Iterator object contains RolePermission.
     * @return List<Permission> List of permission objects belong to all related RolePermission.
     */
    private List<Permission> findPermissions(final Iterator<RolePermission> iter) {
        final List<Permission> permissions = new ArrayList<Permission>();
        while (iter.hasNext()) {
            final RolePermission rolePerm = iter.next();
            permissions.add(rolePerm.getPermission());
        }
        return permissions;
    }
    
    /**
     * Find list of Roles belong to input UserAccount id.
     * 
     * @param userAccountId
     *            id for UserAccount table.
     * @return List<Role> List of Roles belong to this UserAccount.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Role> findRoles(final int userAccountId) {
        final List<Role> roles = new ArrayList<Role>();
        
        UserRole userRole;
        final UserAccount ua = findUserAccount(userAccountId);
        final Iterator<UserRole> iter = ua.getUserRoles().iterator();
        while (iter.hasNext()) {
            userRole = iter.next();
            roles.add(userRole.getRole());
        }
        return roles;
    }
    
    /**
     * Find UserAccount from id.
     * 
     * @param userAccountId
     *            Id for a particular UserAccount record
     * @return UserAccount selected UserAccount record.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public UserAccount findUserAccount(final int userAccountId) {
        return (UserAccount) this.entityDao.get(UserAccount.class, userAccountId);
    }
    
    /**
     * Find UserAccount from id and evict the object
     * 
     * @param userAccountId
     *            Id for a particular UserAccount record
     * @return UserAccount evicted UserAccount record.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public UserAccount findUserAccountAndEvict(final int userAccountId) {
        final UserAccount userAccount = (UserAccount) this.entityDao.get(UserAccount.class, userAccountId);
        this.entityDao.evict(userAccount);
        return userAccount;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public UserAccount findUserAccount(final String userName) {
        final List<UserAccount> list = this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountByUserName",
                                                                                    HibernateConstants.USER_NAME, (userName == null) ? null
                                                                                            : userName.toLowerCase(), true);
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public UserAccount findUndeletedUserAccount(final String userName) {
        final List<UserAccount> list = this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserName",
                                                                                    HibernateConstants.USER_NAME, (userName == null) ? null
                                                                                            : userName.toLowerCase(), true);
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePassword(final UserAccount userAccount) {
        this.entityDao.update(userAccount);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerIdAndUserAccountTypeId",
                                                            new String[] { HibernateConstants.CUSTOMER_ID }, new Object[] { customerId }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<UserAccount> findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountType(final Integer customerId,
        final Integer controllerCustomerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountTypeId",
                                                            new String[] { HibernateConstants.CUSTOMER_ID, "controllerCustomerId", }, new Object[] {
                                                                customerId, controllerCustomerId, }, true);
    }
    
    @Override
    public void updateUserAccount(final UserAccount userAccount) {
        this.entityDao.update(userAccount);
    }
    
    @Override
    public void updateUserAccountAndUserRoles(final UserAccount userAccount) {
        this.entityDao.update(userAccount);
        this.entityDao.deleteAllByNamedQuery(HQL_DELETE_ALL_ROLE_BY_USER_ACCOUNT, new Object[] { userAccount.getId() });
        
        final Set<UserRole> userRoles = userAccount.getUserRoles();
        for (UserRole userRole : userRoles) {
            this.entityDao.save(userRole);
        }
    }
    
    @Override
    public void saveUserAccountAndUserRole(final UserAccount userAccount) {
        this.entityDao.save(userAccount);
        
        final Set<UserRole> userRoles = userAccount.getUserRoles();
        for (UserRole userRole : userRoles) {
            this.entityDao.save(userRole);
        }
        
        final UserDefaultDashboard defaultDashboard = new UserDefaultDashboard();
        defaultDashboard.setId(userAccount.getId());
        defaultDashboard.setIsDefaultDashboard(true);
        defaultDashboard.setLastModifiedByUserId(userAccount.getLastModifiedByUserId());
        defaultDashboard.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        defaultDashboard.setUserAccount(userAccount);
        this.entityDao.save(defaultDashboard);
        
    }
    
    @Override
    public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
        return (Collection<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserAccountId",
                                                                                      HibernateConstants.USER_ACCOUNT_ID, userAccountId, true);
    }
    
    @Override
    public Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(final Integer customerId, final Integer roleId) {
        return (Collection<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerIdAndRoleId",
                                                                                      new String[] { HibernateConstants.CUSTOMER_ID,
                                                                                          HibernateConstants.ROLE_ID, }, new Object[] { customerId,
                                                                                          roleId, }, true);
        
    }
    
    @Override
    public Collection<UserAccount> findUserAccountsByCustomerId(final int customerId) {
        return (Collection<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerId",
                                                                                      HibernateConstants.CUSTOMER_ID, customerId, true);
        
    }
    
    @Override
    public void saveOrUpdateUserAccount(final UserAccount userAccount) {
        this.entityDao.saveOrUpdate(userAccount);
    }
    
    @Override
    public UserAccount findAdminUserAccountByChildCustomerId(final int customerId) {
        final List<UserRole> list = this.entityDao.findByNamedQueryAndNamedParam("UserRole.findAdminUserRoleByChildCustomerId",
                                                                                 HibernateConstants.CUSTOMER_ID, customerId, true);
        if (list.isEmpty()) {
            return null;
        } else {
            return this.entityDao.get(UserAccount.class, list.get(0).getUserAccount().getId());
        }
    }
    
    @Override
    public UserAccount findAdminUserAccountByParentCustomerId(final int customerId) {
        final List<UserRole> list = this.entityDao.findByNamedQueryAndNamedParam("UserRole.findAdminUserRoleByParentCustomerId",
                                                                                 HibernateConstants.CUSTOMER_ID, customerId, true);
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0).getUserAccount();
        }
    }
    
    @Override
    public UserAccount findUserRoleByUserAccountIdAndRoleId(final Integer roleId, final Integer userAccountId) {
        final List<UserRole> list = this.entityDao.findByNamedQueryAndNamedParam("UserRole.findUserRoleByUserAccountIdAndRoleId", new String[] {
            HibernateConstants.ROLE_ID, HibernateConstants.USER_ACCOUNT_ID, }, new Object[] { roleId, userAccountId });
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0).getUserAccount();
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RolePermission> findUserRoleAndPermission(final Integer userAccountId) {
        return (List<RolePermission>) this.entityDao.getNamedQuery("RolePermission.findUserPermissions")
                .setParameter(HibernateConstants.USER_ACCOUNT_ID, userAccountId).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
        return (List<RolePermission>) this.entityDao.findByNamedQueryAndNamedParam("RolePermission.findUserPermissionsForCustomerType", new String[] {
            HibernateConstants.USER_ACCOUNT_ID, "customerTypeId", }, new Object[] { userAccountId, customerTypeId, });
    }
    
    @Override
    public UserAccount findUserAccountForLogin(final int userAccountId) {
        final UserAccount account = (UserAccount) this.entityDao.get(UserAccount.class, userAccountId);
        Hibernate.initialize(account.getCustomer());
        Hibernate.initialize(account.getCustomer().getCustomerSubscriptions());
        Hibernate.initialize(account.getCustomer().getCustomerProperties());
        
        return account;
    }
    
    @Override
    public void deleteUserRoles(final UserAccount userAccount) {
        this.entityDao.deleteAllByNamedQuery(HQL_DELETE_ALL_ROLE_BY_USER_ACCOUNT, new Object[] { userAccount.getId() });
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<UserAccount> findUserAccountsByCustomerIdAndMobileApplicationTypeId(final int customerId, final int mobileApplicationTypeId) {
        return (List<UserAccount>) this.entityDao
                .findByNamedQueryAndNamedParam("UserAccount.findUserAccountsByCustomerIdAndMobileApplicationTypeIdAndDate", new String[] {
                    HibernateConstants.CUSTOMER_ID, "mobileApplicationTypeId", "date", }, new Object[] { customerId, mobileApplicationTypeId,
                    new Date(), }, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<UserAccount> findChildUserAccountsByParentUserId(final Integer parentUserAccountId) {
        return (List<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findChildUserAccountsByParentUserId",
                                                                                "parentUserAccountId", parentUserAccountId);
    }
    
    @Override
    public List<UserAccount> findAdminUserAccountsForParent(final Integer customerId, final Integer parentCustomerId) {
        return (List<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findAdminUserAccountsForParent", new String[] {
            HibernateConstants.CUSTOMER_ID, HibernateConstants.PARENT_CUSTOMER_ID, }, new Object[] { customerId, parentCustomerId, });
    }
    
    @Override
    public List<UserAccount> findAdminUserAccountWithIsAllChilds(final Integer customerId) {
        return (List<UserAccount>) this.entityDao.findByNamedQueryAndNamedParam("UserAccount.findAdminUserAccountWithIsAllChilds",
                                                                                HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    public UserAccount findAliasUserAccountByUserAccountIdAndCustomerId(final Integer userAccountId, final Integer customerId) {
        return (UserAccount) this.entityDao.findUniqueByNamedQueryAndNamedParam("UserAccount.findUndeletedUserAccountByUserAccountIdAndCustomerId",
                                                                                new String[] { HibernateConstants.USER_ACCOUNT_ID,
                                                                                    HibernateConstants.CUSTOMER_ID, }, new Object[] { userAccountId,
                                                                                    customerId, }, true);
    }
    
    @Override
    public List<UserRole> findActiveUserRoleByUserAccountId(final Integer userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserRole.findActiveUserRoleByUserAccountId", HibernateConstants.USER_ACCOUNT_ID,
                                                            userAccountId, true);
    }
    
    @Override
    public List<UserAccount> findUserAccountByRoleIdAndCustomerIdList(final Integer roleId, final List<Integer> customerIdList) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserRole.findUserAccountByRoleIdAndCustomerIdList", new String[] {
            HibernateConstants.ROLE_ID, "customerIdList", }, new Object[] { roleId, customerIdList, }, true);
    }
    
    @Override
    public List<UserAccount> findUserAccountByRoleIdAndCustomerId(final Integer roleId, final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("UserRole.findUserAccountByRoleIdAndCustomerId", new String[] {
            HibernateConstants.ROLE_ID, HibernateConstants.CUSTOMER_ID, }, new Object[] { roleId, customerId, }, true);
    }
    
    @Override
    public Customer findChildCustomer(final Integer parentCustomerId, final String childCustomerName) {
        return (Customer) this.entityDao.getNamedQuery("Customer.findActiveChildCustomerByParentIdAndName")
                .setInteger(HibernateConstants.PARENT_CUSTOMER_ID, parentCustomerId).setString("childCustomerName", childCustomerName.toLowerCase())
                .uniqueResult();
    }
    
    @Override
    public UserAccount findUserAccountByLinkUserName(final String linkUserName) {
        final List<UserAccount> accounts = this.entityDao.getNamedQuery("UserAccount.findUserAccountByLinkUserName")
                .setString("linkUserName", linkUserName.toLowerCase())
                .list();
        if (accounts.isEmpty()) {
            return null;
        } else {
            return accounts.get(0);
        }
    }
}
