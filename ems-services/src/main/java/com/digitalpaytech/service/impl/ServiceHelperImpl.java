package com.digitalpaytech.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.util.cache.ObjectCache;

@Service("SpringServiceHelper")
public class ServiceHelperImpl implements ApplicationContextAware, ServiceHelper {
    private ApplicationContext context;
    
    @Autowired
    @Qualifier("ObjectCache")
    private ObjectCache cache;
    
    @Override
    public final <B> B bean(final Class<B> beanClass) {
        return this.context.getBean(beanClass);
    }
    
    @Override
    public final <B> B bean(final String beanName, final Class<B> beanClass) {
        return this.context.getBean(beanName, beanClass);
    }
    
    @Override
    public final void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
    
    @Override
    public final BatchProcessStatus getProcessStatus(final String key) {
        BatchProcessStatus status = null;
        if (this.cache != null) {
            status = this.cache.get(BatchProcessStatus.class, key);
            if (status != null) {
                status.reconnect(this.cache,  key);
            }
        }
        
        if (status == null) {
            status = new BatchProcessStatus();
        }
        
        return status;
    }
}
