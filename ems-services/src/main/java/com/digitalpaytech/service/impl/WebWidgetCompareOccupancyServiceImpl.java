package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetCompareOccupancyService")
@Transactional(propagation = Propagation.REQUIRED)
// PMD.ConsecutiveAppendsShouldReuse and ConsecutiveLiteralAppends ignored for maintainability
// PMD.UseConcurrentHashMap ignored for performance, map is static and read only 
@SuppressWarnings({ "PMD.ConsecutiveAppendsShouldReuse", "PMD.ConsecutiveLiteralAppends", "PMD.UseConcurrentHashMap" })
public class WebWidgetCompareOccupancyServiceImpl implements WebWidgetService {
    
    public static final Map<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(10);
    
    private static final String OCCUPANCY_LOCATIONID = "o.LocationId ";
    private static final String PHYSICAL_OCCUPANCY = "Counted Occupancy";
    private static final String PAID_OCCUPANCY = "Paid Occupancy";
    private static final String WDATA = "wData";
    private static final String WLABEL = "wLabel";
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    static {
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay, t.Id ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, OCCUPANCY_LOCATIONID);
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, OCCUPANCY_LOCATIONID);
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final WebWidgetHelperService getWebWidgetHelperService() {
        return this.webWidgetHelperService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final WidgetData getWidgetData(final Widget widget, final UserAccount userAccount) {
        
        int tableRangeType = 0;
        
        final String queryLimit = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                             EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        final int timeRange = widget.getWidgetRangeType().getId();
        
        switch (timeRange) {
            case WidgetConstants.RANGE_TYPE_NOW:
            case WidgetConstants.RANGE_TYPE_TODAY:
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                break;
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                break;
            default:
                break;
        }
        
        WidgetData widgetData = new WidgetData();
        
        final List<WidgetMetricInfo> rawWidgetData = createPartialList(widget, tableRangeType, userAccount, queryLimit, true);
        rawWidgetData.addAll(createPartialList(widget, tableRangeType, userAccount, queryLimit, false));
        
        widgetData = this.webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        widgetData.setTrendValue(getWidgetTrendValues(widget, widgetData));
        
        return widgetData;
    }
    
    //Complexity is a common problem with widget query creation methods
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity" })
    private List<WidgetMetricInfo> createPartialList(final Widget widget, final int tableRangeType, final UserAccount userAccount,
        final String queryLimit, final boolean isPhysical) {
        
        final StringBuilder queryStr = new StringBuilder(StandardConstants.CONSTANT_2000);
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        // Specific code to cover closed/open intervals within the day
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            queryStr.append(", IFNULL(SUM(CASE WHEN ").append(isPhysical ? WDATA : WLABEL).append(".TotalNumberOfSpaces = 0 THEN 0 ELSE ")
                    .append("wData.TotalNumberOfPermits END)/SUM(");
        } else {
            queryStr.append(", IFNULL(SUM(wData.TotalNumberOfPermits)/SUM(");
        }
        queryStr.append(isPhysical ? WDATA : WLABEL).append(".TotalNumberOfSpaces), 0) * 100 AS WidgetMetricValue ");
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        final Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        String tableType = null;
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_DAY) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_DAY_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_MONTH) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING;
        }
        
        /* "wData" SELECT expression */
        queryStr.append("SELECT o.CustomerId");
        final boolean isTierTypeTime = this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP,
                                                                                     (Map<Integer, String>) null, false);
        queryStr.append(", SUM(o.NoOfPermits) AS TotalNumberOfPermits ");
        queryStr.append(", SUM(o.NumberOfSpaces) AS TotalNumberOfSpaces ");
        
        queryStr.append("FROM ").append(isPhysical ? "Case" : StandardConstants.STRING_EMPTY_STRING).append("Occupancy");
        queryStr.append(tableType);
        queryStr.append(" o ");
        
        queryStr.append("INNER JOIN Time t ON(");
        
        final int timeRange = widget.getWidgetRangeType().getId();
        
        if (timeRange == WidgetConstants.RANGE_TYPE_NOW && userAccount.getCustomer().isIsParent()) {
            queryStr.append("o.TimeIdGMT = t.Id ");
        } else {
            queryStr.append("o.TimeIdLocal = t.Id ");
        }
        
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(), widget
                .getWidgetTierTypeByWidgetTier1Type().getId(), true, true);
        
        queryStr.append(") ");
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "o.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY o.CustomerId ");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, !isPhysical, tableRangeType, isTierTypeTime);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        
        final boolean isNowAndParent = userAccount.getCustomer().isIsParent()
                                       && widget.getWidgetRangeType().getId() == WidgetConstants.RANGE_TYPE_NOW;
        
        query = WebWidgetUtil.querySetParameter(this.webWidgetHelperService, widget, query, userAccount, isNowAndParent);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        final List<WidgetMetricInfo> responseList = query.list();
        // Replace 2. tier name with occupancy type
        for (WidgetMetricInfo metricInfo : responseList) {
            metricInfo.setTier2TypeName(isPhysical ? PHYSICAL_OCCUPANCY : PAID_OCCUPANCY);
        }
        return responseList;
    }
    
    protected final List<String> getWidgetTrendValues(final Widget widget, final WidgetData widgetData) {
        
        final List<String> trendValue = new ArrayList<String>();
        
        if (!(widget.getTrendAmount() == null || widget.getTrendAmount() == 0)) {
            final Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(StandardConstants.BIGDECIMAL_100, 2, BigDecimal.ROUND_HALF_UP)
                    .floatValue();
            trendValue.add(dollarValue.toString());
            return trendValue;
        }
        
        return null;
    }
}
