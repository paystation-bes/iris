package com.digitalpaytech.service.impl;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.domain.CardRetryTransactionType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import javax.annotation.PostConstruct;

@Service("cardRetryTransactionTypeService")
public class CardRetryTransactionTypeServiceImpl implements CardRetryTransactionTypeService {

    @Autowired
    private OpenSessionDao openSessionDao;

    private Map<Integer, CardRetryTransactionType> map;

    @PostConstruct
    private void loadAllToMap() {
        final List<CardRetryTransactionType> list = this.openSessionDao.loadAll(CardRetryTransactionType.class);
        this.map = new HashMap<Integer, CardRetryTransactionType>(list.size());
        
        final Iterator<CardRetryTransactionType> iter = list.iterator();
        while (iter.hasNext()) {
            final CardRetryTransactionType crtType = iter.next();
            this.map.put(crtType.getId(), crtType);
        }
    }
    
    
    @Override
    public final CardRetryTransactionType findCardRetryTransactionType(final Integer id) {
        return this.map.get(id);
    }
}
