package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.annotation.PostConstruct;

import com.digitalpaytech.service.PermitTypeService;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.PermitType;

@Service("permitTypeService")
public class PermitTypeServiceImpl implements PermitTypeService {

    @Autowired
    private OpenSessionDao openSessionDao;

    private Map<String, PermitType> map;
    private Map<Integer, PermitType> mapByIds;
    
    @PostConstruct
    private void loadAllToMap() {
        List<PermitType> list = openSessionDao.loadAll(PermitType.class);
        map = new HashMap<String, PermitType>(list.size());
        mapByIds = new HashMap<Integer, PermitType>(list.size());
        
        Iterator<PermitType> iter = list.iterator();
        while (iter.hasNext()) {
            PermitType perType = iter.next();
            map.put(perType.getName().toLowerCase(), perType);
            mapByIds.put(perType.getId(), perType);
        }
    }

    
    @Override
    public PermitType findPermitTypeById(Integer id) {
        return mapByIds.get(id);
    }

    @Override
    public PermitType findPermitType(String name) {
        return map.get(name.toLowerCase());
    }
        

    public void setOpenSessionDao(OpenSessionDao openSessionDao) {
        this.openSessionDao = openSessionDao;
    }
}
