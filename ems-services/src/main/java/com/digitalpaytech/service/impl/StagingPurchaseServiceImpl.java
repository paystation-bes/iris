package com.digitalpaytech.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.service.StagingPurchaseService;

@Service("stagingPurchaseService")
public class StagingPurchaseServiceImpl implements StagingPurchaseService {
    
    @Autowired
    private EntityDao entityDao;
    
    private static final String SQL_GET_RECORDS_TO_PROCESS_BY_COUNT = "SELECT COUNT(*) FROM StagingPurchase";
    
    @Override
    @Transactional
    public int getRecordsToProcessByCount() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_RECORDS_TO_PROCESS_BY_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    private static final String SQL_GET_CUSTOMERS_TO_PROCESS_BY_COUNT = "SELECT COUNT(DISTINCT CustomerId) FROM StagingPurchase";
    
    @Override
    @Transactional
    public Object getCustomersToProcessByCount() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_CUSTOMERS_TO_PROCESS_BY_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
}
