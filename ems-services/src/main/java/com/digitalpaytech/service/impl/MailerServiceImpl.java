package com.digitalpaytech.service.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.DefaultEmailNotificationCallback;
import com.digitalpaytech.util.EmailNotification;
import com.digitalpaytech.util.EmailNotificationCallback;
import com.digitalpaytech.util.EmailNotificationThread;
import com.digitalpaytech.util.WebCoreConstants;

@Component("mailerService")
@Transactional(propagation = Propagation.SUPPORTS)
public class MailerServiceImpl implements MailerService {
    private final static String EMS_NAME_STRING = "Digital Iris";
    private final static String EMS_NOTIFICATION_STRING = "Iris Alert";
    
    private final static String MAIL_HOST_KEY = "mail.smtp.host";
    private Session mailSession_;
    
    public final static String NEWLINE = System.getProperty("line.separator");
    
    private final static Logger logger__ = Logger.getLogger(MailerServiceImpl.class);
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setClusterMemberService(ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    private MailerServiceImpl() {
        // Empty constructor
    }
    
    private String getMailServer() {
        String mail_host = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAIL_SERVER_URL);
        if (mail_host == null || mail_host.equals("")) {
            if (emsPropertiesService.isWindowsOS()) {
                mail_host = "192.168.0.1";
            } else {
                mail_host = "localhost";
            }
        }
        return (mail_host);
    }
    
    private String getEmsErrorAlertAddress() {
        return emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_ADMIN_ALERT_TO_EMAIL_ADDRESS, EmsPropertiesService.EMS_ADDRESS_STRING);
    }
    
    private Address getEmsAlertFromAddress() {
        String email_address = emsPropertiesService
                .getPropertyValue(EmsPropertiesService.EMS_ALERT_FROM_EMAIL_ADDRESS, EmsPropertiesService.EMS_ADDRESS_STRING);
        
        try {
            return (new InternetAddress(email_address, EMS_NAME_STRING));
        } catch (UnsupportedEncodingException e) {
            logger__.error("***\n\nInvalid 'From' field for mail notification!\n\n***", e);
        }
        
        return (null);
    }
    
    //	private String formatNotificationMessage(Paystation paystationData,
    //			EventData eventData) {
    //		String timeZone = paystationService.getCustomerTimeZone(paystationData
    //				.getId());
    //		// SMS short-format
    //		String content = "PS Name: "
    //				+ paystationData.getName()
    //				+ NEWLINE
    //				+ "Region: "
    //				+ (paystationData.getRegion() == null ? "" : paystationData
    //						.getRegion().getName())
    //				+ NEWLINE
    //				+ "Lot:"
    //				+ (paystationData.getServiceState().getLotNumber() == null ? ""
    //						: paystationData.getServiceState().getLotNumber())
    //				+ NEWLINE
    //				+ "Machine:"
    //				+ (paystationData.getServiceState().getMachineNumber() == null ? ""
    //						: paystationData.getServiceState().getMachineNumber())
    //				+ NEWLINE
    //				+ "Severity:"
    //				+ eventData.getSeverity()
    //				+ NEWLINE
    //				+ "Type:"
    //				+ eventData.getType()
    //				+ "=>"
    //				+ eventData.getAction()
    //				+ NEWLINE
    //				+ "Details:"
    //				+ eventData.getInformation()
    //				+ NEWLINE
    //				+ "Time:"
    //				+ DateFactory.createDateString(timeZone,
    //						eventData.getTimeStamp());
    //
    //		return content;
    //	}
    
    //	public void sendMessage(Paystation paystationData, EventData eventData) {
    //		String content = formatNotificationMessage(paystationData, eventData);
    //		String to_addresses = paystationData.getContactUrl();
    //		if (to_addresses == null || to_addresses.length() == 0
    //				|| to_addresses.equals("n/a")) {
    //			logger__.info("Unable to send message as paystation: '"
    //					+ paystationData.getCommAddress()
    //					+ "' has no email address.");
    //			return;
    //		}
    //		sendMessage(to_addresses, content, null);
    //	}
    
    /**
     * Plain Text message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param content
     *            the MIME message CONTENT
     */
    public void sendMessage(String toAddresses, String content) {
        sendMessage(getEmsAlertFromAddress(), toAddresses, EMS_NOTIFICATION_STRING, content, null);
    }
    
    /**
     * Generic message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, Object content, String type) {
        sendMessage(getEmsAlertFromAddress(), toAddresses, EMS_NOTIFICATION_STRING, content, type);
    }
    
    /**
     * Generic message sender with default FROM & SUBJECT fields
     * 
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param ccAddresses
     *            comma separated RFC822 InternetAddresses for CC
     * @param bccAddresses
     *            comma separated RFC822 InternetAddresses for BCC
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, String ccAddresses, String bccAddresses, Object content, String type) {
        sendMessage(getEmsAlertFromAddress(), toAddresses, ccAddresses, bccAddresses, EMS_NOTIFICATION_STRING, content, type);
    }
    
    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(Address fromAddress, String toAddresses, String subject, Object content, String type) {
        sendMessage(fromAddress, toAddresses, null, null, subject, content, type);
    }
    
    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(String toAddresses, String subject, Object content, String type) {
        sendMessage(getEmsAlertFromAddress(), toAddresses, null, null, subject, content, type);
    }
    
    /**
     * Generic message sender
     * 
     * @param fromAddress
     *            the MIME message FROM Address
     * @param toAddresses
     *            comma separated RFC822 InternetAddresses for TO
     * @param ccAddresses
     *            comma separated RFC822 InternetAddresses for CC
     * @param bccAddresses
     *            comma separated RFC822 InternetAddresses for BCC
     * @param subject
     *            the MIME message SUBJECT
     * @param content
     *            the MIME message CONTENT
     * @param type
     *            the MIME message TYPE
     */
    public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses, String bccAddresses, String subject, Object content, String type) {
        MimeMessage message = createBaseMessage(fromAddress, toAddresses, ccAddresses, bccAddresses, subject);
        
        try {
            if ((type == null) && (content instanceof String)) {
                // assume MIME type of "text/plain"
                message.setText((String) content);
            } else {
                message.setContent(content, type);
            }
            
            sendMessage(message);
        } catch (MessagingException e) {
            logger__.warn("Error occurred while setting mail notification content!", e);
        }
    }
    
    public void sendAdminErrorAlert(String subject, String content, Exception exception) {
        sendAdminAlert("ERROR - " + subject, content, exception);
    }
    
    public void sendAdminWarnAlert(String subject, String content, Exception exception) {
        sendAdminAlert("WARN - " + subject, content, exception);
    }
    
    public void sendAdminAlert(String subject, String content, Exception exception) {
        
        MimeMessage message = createBaseMessage(getEmsAlertFromAddress(), getEmsErrorAlertAddress(), null, null, subject);
        
        if (exception != null) {
            StringBuilder content_sb = new StringBuilder(content);
            content_sb.append("\n*****************************************************\n\n");
            content_sb.append("Error occured at: " + new java.sql.Timestamp(System.currentTimeMillis()) + "\n");
            String serverName = "";
            String clusterName = clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.trim().isEmpty()) {
                String hostname;
                try {
                    hostname = InetAddress.getLocalHost().getHostName();
                } catch (Exception e) {
                    hostname = WebCoreConstants.UNKNOWN;
                }
                serverName = hostname;
            } else {
                serverName = clusterName;
            }
            content_sb.append("App Server: " + serverName + "\n");
            
            StringWriter string_writer = new StringWriter();
            exception.printStackTrace(new PrintWriter(string_writer));
            content_sb.append(string_writer.toString() + "\n");
            
            content_sb.append("\n*****************************************************\n\n");
            content = content_sb.toString();
        }
        
        try {
            // assume MIME type of "text/plain"
            message.setText(content);
            
            // sendMessage(message);
            // log email alert to a seperate file. log analyzer will send the
            // email
            Logger emailLogger = Logger.getLogger("EmailAlert");
            emailLogger.error(content);
        } catch (MessagingException e) {
            logger__.warn("Error occurred while setting mail notification content", e);
        }
    }
    
    private void sendMessage(MimeMessage message) {
        EmailNotification notification = new EmailNotification(message);
        EmailNotificationCallback callback = new DefaultEmailNotificationCallback(notification);
        EmailNotificationThread thread = new EmailNotificationThread(null, null);
        ArrayList<EmailNotification> notifications = new ArrayList<EmailNotification>();
        notifications.add(notification);
        ArrayList<EmailNotificationCallback> callbacks = new ArrayList<EmailNotificationCallback>();
        callbacks.add(callback);
        thread.setNotifications(notifications);
        thread.setCallbacks(callbacks);
        
        try {
            Method method = EmailNotificationThread.class.getDeclaredMethod("run", null);
            kpiListenerService.invoke(thread, method, null);
        } catch (Throwable e) {
            thread.start();
        }
    }
    
    private MimeMessage createBaseMessage(Address fromAddress, String toAddresses, String ccAddresses, String bccAddresses, String subject) {
        MimeMessage message = new MimeMessage(getSession());
        
        try {
            message.setFrom(fromAddress);
            message.setRecipients(Message.RecipientType.TO, toAddresses);
            if (ccAddresses != null) {
                message.setRecipients(Message.RecipientType.CC, ccAddresses);
            }
            if (bccAddresses != null) {
                message.setRecipients(Message.RecipientType.BCC, bccAddresses);
            }
            message.setSubject(subject);
        } catch (MessagingException e) {
            logger__.warn("Error occurred while setting a mail notification parameter!", e);
        }
        
        return message;
    }
    
    private Session getSession() {
        if (mailSession_ == null) {
            Properties props = System.getProperties();
            props.put(MAIL_HOST_KEY, getMailServer());
            mailSession_ = Session.getDefaultInstance(props);
        }
        
        return (mailSession_);
    }
    
    public void sendServiceAgreementAccepted(CustomerAgreement customerAgreement, String customerName) {
        if (customerAgreement == null) {
            logger__.error("!!! sending service agreement acceptance fail due to serviceAgreedCustomer object is null !!!");
            return;
        }
        
        String subject = "Terms Accepted - " + customerName;
        String content = customerAgreement.getName() + ", " + customerAgreement.getTitle() + ", from " + customerAgreement.getOrganization()
                         + " has accepted the IRIS Terms of Service.";
        try {
            MimeMessage message = createBaseMessage(getEmsAlertFromAddress(), getServiceAgreementToAddress(), null, null, subject);
            message.setText(content);
            logger__.debug("+++ service agreement email sending to " + getServiceAgreementToAddress() + " +++");
            ;
            sendMessage(message);
        } catch (MessagingException e) {
            logger__.warn("Error occurred while setting mail notification content", e);
        }
    }
    
    private String getServiceAgreementToAddress() {
        return emsPropertiesService.getPropertyValue(EmsPropertiesService.SERVICE_AGREEMENT_TO_EMAIL_ADDRESS,
                                                     EmsPropertiesService.EMS_SERVICE_AGREEMENT_TO_ADDRESS_STRING);
    }
    
    @Override
    public void sendMessage(Address fromAddress, String toAddresses, String ccAddresses, String bccAddresses, String subject, Object content, String type,
                            byte[] attachment, String filename) {
        MimeMessage message = createBaseMessage(fromAddress, toAddresses, ccAddresses, bccAddresses, subject);
        
        try {
            Multipart multipart = new MimeMultipart();
            
            BodyPart attachmentPart = new MimeBodyPart();
            attachmentPart.setFileName(filename);
            attachmentPart.setHeader("Content-Type", "application/pdf");
            attachmentPart.setHeader("Content-ID", "PDF");
            
            DataSource ds = new ByteArrayDataSource(attachment, "application/pdf");
            DataHandler dh = new DataHandler(ds);
            attachmentPart.setDataHandler(dh);
            
            BodyPart contentPart = new MimeBodyPart();
            if ((type == null) && (content instanceof String)) {
                // assume MIME type of "text/plain"
                contentPart.setText((String) content);
            } else {
                contentPart.setContent(content, type);
            }

            multipart.addBodyPart(contentPart);
            multipart.addBodyPart(attachmentPart);
            
            message.setContent(multipart);
            sendMessage(message);
        } catch (MessagingException e) {
            logger__.warn("Error occurred while setting mail notification content!", e);
        }
        
    }

    @Override
    public void sendMessage(String toAddresses, String subject, Object content, String type, byte[] attachment, String filename) {
        sendMessage(getEmsAlertFromAddress(), toAddresses, null, null, subject, content, type, attachment, filename);
        
    }
}