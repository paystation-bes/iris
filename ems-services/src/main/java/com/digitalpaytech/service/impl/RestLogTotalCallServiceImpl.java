package com.digitalpaytech.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestLogTotalCall;
import com.digitalpaytech.service.RestLogTotalCallService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("restLogTotalCallService")
@Transactional(propagation = Propagation.REQUIRED)
public class RestLogTotalCallServiceImpl implements RestLogTotalCallService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public int saveRestLogTotalCall(RestAccount restAccount) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        
        String[] parameters = { "restAccountId", "loggingDate" };
        Object[] values = { restAccount.getId(), today.getTime() };
        RestLogTotalCall rltc = (RestLogTotalCall) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("RestLogTotalCall.findRestLogByRestAccountIdAndLoggingDate", parameters, values, false);
        if (rltc == null) {
            rltc = new RestLogTotalCall();
            rltc.setRestAccount(restAccount);
            rltc.setLoggingDate(today.getTime());
            rltc.setTotalCalls(0);
        }
        rltc.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        rltc.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        rltc.setTotalCalls(rltc.getTotalCalls() + 1);
        
        this.entityDao.saveOrUpdate(rltc);
        return rltc.getTotalCalls();
    }
}
