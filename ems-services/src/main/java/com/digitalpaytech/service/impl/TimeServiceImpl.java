package com.digitalpaytech.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Time;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.TimeService;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class TimeServiceImpl implements TimeService {
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final Time findBucket(final Calendar time) {
        return findBucket((short) time.get(Calendar.YEAR), (byte) (time.get(Calendar.MONTH) + 1), (byte) time.get(Calendar.DAY_OF_MONTH),
                          (byte) time.get(Calendar.HOUR_OF_DAY), (byte) Math.floor(time.get(Calendar.MINUTE) / MINUTES_IN_BUCKET));
    }
    
    @Override
    public final Time findBucket(final Short year, final Byte month, final Byte day, final Byte hour, final Byte quaterOfHour) {
        return (Time) this.entityDao.findUniqueByNamedQueryAndNamedParam("Time.findBucket", new String[] { HibernateConstants.YEAR,
            HibernateConstants.MONTH, HibernateConstants.DAY, HibernateConstants.HOUR, HibernateConstants.QUATER_OF_HOUR, }, new Object[] { year,
                month, day, hour, quaterOfHour, }, true);
    }
    
    @Override
    public final Time findBucket(final Integer timeId) {
        return (Time) this.entityDao.findUniqueByNamedQueryAndNamedParam("Time.findBucketByTimeId", HibernateConstants.TIME_ID, timeId);
    }
    
    @Override
    public final Time findHour(final Calendar time) {
        return findHour((short) time.get(Calendar.YEAR), (byte) (time.get(Calendar.MONTH) + 1), (byte) time.get(Calendar.DAY_OF_MONTH),
                        (byte) time.get(Calendar.HOUR_OF_DAY));
    }
    
    @Override
    public final Time findHour(final Short year, final Byte month, final Byte day, final Byte hour) {
        return (Time) this.entityDao.findUniqueByNamedQueryAndNamedParam("Time.findHour", new String[] { HibernateConstants.YEAR,
            HibernateConstants.MONTH, HibernateConstants.DAY, HibernateConstants.HOUR, }, new Object[] { year, month, day, hour, }, true);
    }
    
    @Override
    public final Time findDay(final Calendar time) {
        return findDay((short) time.get(Calendar.YEAR), (byte) (time.get(Calendar.MONTH) + 1), (byte) time.get(Calendar.DAY_OF_MONTH));
    }
    
    @Override
    public final Time findDay(final Short year, final Byte month, final Byte day) {
        return (Time) this.entityDao.findUniqueByNamedQueryAndNamedParam("Time.findDay", new String[] { HibernateConstants.YEAR,
            HibernateConstants.MONTH, HibernateConstants.DAY, }, new Object[] { year, month, day, }, true);
    }
    
    @Override
    public final Time findMonth(final Calendar time) {
        return findMonth((short) time.get(Calendar.YEAR), (byte) (time.get(Calendar.MONTH) + 1));
    }
    
    @Override
    public final Time findMonth(final Short year, final Byte month) {
        return (Time) this.entityDao.findUniqueByNamedQueryAndNamedParam("Time.findMonth", new String[] { HibernateConstants.YEAR,
            HibernateConstants.MONTH, }, new Object[] { year, month, }, true);
    }
}
