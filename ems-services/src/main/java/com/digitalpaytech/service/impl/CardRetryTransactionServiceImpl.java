package com.digitalpaytech.service.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("cardRetryTransactionService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class CardRetryTransactionServiceImpl implements CardRetryTransactionService {
    
    private static final Logger LOG = Logger.getLogger(CardRetryTransactionServiceImpl.class);
    
    private static final String HQL_SEL_EMPTY_BAD_CARD_HASH =
            "SELECT distinct cardHash, cardData FROM CardRetryTransaction " + "WHERE (badCardHash = '') OR (badCardHash IS NULL)";
    
    private static final String HQL_UPD_BAD_CARD_HASH =
            "UPDATE CardRetryTransaction crt SET crt.badCardHash = :badCardHash WHERE crt.cardHash = :cardHash";
    private static final String LAST_FOUR_DIGITS_CARD_NUMBER = "last4digitsOfCardNumber";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    private Properties mappingProperties;
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    public void loadMappingProperties() throws SystemException {
        try {
            this.mappingProperties = PropertiesLoaderUtils.loadAllProperties(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            
            if (this.mappingProperties == null || this.mappingProperties.isEmpty()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("TransactionServiceImpl, loadMappingProperties, ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME)
                        .append(" is missing!");
                throw new SystemException(bdr.toString());
            }
        } catch (IOException ioe) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("TransactionServiceImpl, loadMappingProperties, error loading ").append(WebCoreConstants.MAPPING_PROPERTIES_FILE_NAME);
            LOG.error(bdr.toString(), ioe);
            throw new SystemException(bdr.toString(), ioe);
        }
    }
    
    @Override
    public void clearRetriesExceededCardData() {
        this.entityDao.updateByNamedQuery("CardRetryTransaction.clearRetriesExceededCardData", null, null);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Date getMaxRetryTimeForTransaction(final CardRetryTransaction crt) {
        final List<Date> dates = this.entityDao
                .findByNamedQueryAndNamedParam("CardRetryTransaction.findMaxRetryTimeForTransaction",
                                               new String[] { "pointOfSaleId", "purchasedDate", "ticketNumber", },
                                               new Object[] { crt.getPointOfSale().getId(), crt.getPurchasedDate(), crt.getTicketNumber(), });
        
        if (dates != null && dates.size() > 0) {
            return dates.get(0);
        }
        return null;
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public int getMatchingTransactionsCountForCardData(final String cardData) {
        return this.entityDao.findTotalResultByNamedQuery("CardRetryTransaction.findCountForCardData", new String[] { "cardData" },
                                                          new Object[] { cardData });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Collection<CardRetryTransaction> getCardRetryTransactionByCardData(final String cardData, final int howManyRows, final boolean isEvict) {
        final List<CardRetryTransaction> lists =
                this.entityDao.findByNamedQuery("CardRetryTransaction.getCardRetryTransactionByCardData", new Object[] { cardData }, 0, howManyRows);
        
        if (isEvict) {
            for (CardRetryTransaction crt : lists) {
                this.entityDao.evict(crt);
            }
        }
        return lists;
    }
    
    @Override
    public void updateCardRetryTransaction(final CardRetryTransaction cardRetryTransaction) {
        this.entityDao.update(cardRetryTransaction);
    }
    
    @Override
    public final CardRetryTransaction findCardRetryTransaction(final Long id) {
        return this.entityDao.get(CardRetryTransaction.class, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Collection<CardRetryTransaction> findCardRetryTransactions(final Collection<Integer> pointOfSaleIds, final int maxRetries,
        final int numRecords, final Date retryDate) {
        final Criteria query = this.entityDao.createCriteria(CardRetryTransaction.class).createAlias("pointOfSale", "pointOfSale");
        
        query.createAlias("pointOfSale.customer", "customer", CriteriaSpecification.INNER_JOIN).setFetchMode("customer", FetchMode.JOIN);
        
        if (pointOfSaleIds != null && !pointOfSaleIds.isEmpty()) {
            query.add(Restrictions.in("pointOfSale.id", pointOfSaleIds));
        }
        
        query.add(Restrictions.lt("numRetries", maxRetries));
        
        final short cardExpiry = (short) DateUtil.createCurrentYYMM();
        query.add(Restrictions.ge(CardRetryTransaction.COLUMN_NAME_CARD_EXPIRY, cardExpiry));
        
        query.add(Restrictions.lt("lastRetryDate", retryDate));
        
        final Date creationDate = new Date(new Date().getTime() - 30 * 60 * 1000);
        query.add(Restrictions.or(Restrictions.neProperty("lastRetryDate", "creationDate"), Restrictions.lt("creationDate", creationDate)));
        
        return query.list();
    }
    
    @Override
    public CardRetryTransaction findByPointOfSalePurchasedDateTicketNumber(final Integer pointOfSaleId, final Date purchasedDate,
        final Integer ticketNumber) {
        final String[] params = { "pointOfSaleId", "purchasedDate", "ticketNumber" };
        final Object[] values = { pointOfSaleId, purchasedDate, ticketNumber };
        final List<CardRetryTransaction> list =
                this.entityDao.findByNamedQueryAndNamedParam("CardRetryTransaction.findByPointOfSalePurchasedDateTicketNumber", params, values);
        if (list != null && !list.isEmpty()) {
            return (CardRetryTransaction) list.get(0);
        }
        return null;
    }
    
    @Override
    public List<String> findBadCardHashByPointOfSaleIdsDateAndNumRetries(final List<Integer> pointOfSaleIds, final int yymm,
        final int creditCardOfflineRetryLimit) {
        
        final SQLQuery q = this.entityDao.createSQLQuery("SELECT DISTINCT crt.BadCardHash FROM CardRetryTransaction crt "
                                                         + "WHERE crt.PointOfSaleId IN (:posIds) AND crt.CardExpiry >= :curDate "
                                                         + "and crt.NumRetries >= :ccOfflineRetryLimit AND crt.IgnoreBadCard = false");
        q.setParameterList("posIds", pointOfSaleIds);
        q.setParameter("curDate", yymm);
        q.setParameter("ccOfflineRetryLimit", creditCardOfflineRetryLimit);
        
        return q.list();
    }
    
    @Override
    public EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    /**
     * TODO Replace with original logic AFTER migration is completed.
     */
    @Override
    public void clearRetriesExceededCardDataForMigratedCustomers() {
        this.entityDao.updateByNamedQuery("CardRetryTransaction.clearRetriesExceededCardDataForMigratedCustomers", null, null);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Object[]> findEmptyBadCardHash() {
        return this.entityDao.getCurrentSession().createQuery(HQL_SEL_EMPTY_BAD_CARD_HASH).list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int updateBadCardHash(final String cardHash, final String badCardHash) {
        return this.entityDao.getCurrentSession().createQuery(HQL_UPD_BAD_CARD_HASH).setParameter("cardHash", cardHash)
                .setParameter("badCardHash", badCardHash).executeUpdate();
    }
    
    @Override
    public final void save(final CardRetryTransaction crt) {
        this.entityDao.save(crt);
    }
    
    @Override
    public final void delete(final CardRetryTransaction cardRetryTransaction) {
        this.entityDao.delete(cardRetryTransaction);
    }
    
    @Override
    public final CardRetryTransaction getCardRetryTransaction(final List<Object> objects) {
        if (objects == null || objects.isEmpty()) {
            return null;
        }
        final Iterator<Object> iter = objects.iterator();
        while (iter.hasNext()) {
            final Object obj = iter.next();
            if (obj instanceof CardRetryTransaction) {
                return (CardRetryTransaction) obj;
            }
        }
        return null;
    }
    
    @Override
    public final CardRetryTransaction getCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final String encryptedCardData) {
        return createCardRetryTransaction(pointOfSale, purchase, ptd, encryptedCardData);
    }
    
    private CardRetryTransaction createCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd) {
        final CardRetryTransaction crt = new CardRetryTransaction();
        crt.setPointOfSale(pointOfSale);
        crt.setPurchasedDate(ptd.getPurchasedDate());
        crt.setTicketNumber(ptd.getTicketNumber());
        crt.setAmount(purchase.getCardPaidAmount());
        crt.setNumRetries(0);
        crt.setCreationDate(DateUtil.getCurrentGmtDate());
        crt.setLastRetryDate(crt.getCreationDate());
        crt.setIsRfid(ptd.isIsRfid());
        if (purchase.isIsOffline()) {
            crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                    .findCardRetryTransactionType(getMappingPropertiesValue(LabelConstants.CARD_RETRY_TRANSACTION_TYPE_BATCHED_AUTO_EMS_RETRY_ID)));
        } else {
            crt.setCardRetryTransactionType(this.cardRetryTransactionTypeService
                    .findCardRetryTransactionType(getMappingPropertiesValue(LabelConstants.CARD_RETRY_TRANSACTION_TYPE_IMPORTED_STORED_FORWARD_ID)));
        }
        return crt;
    }
    
    private CardRetryTransaction createCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final String encryptedCardData) {
        final CardRetryTransaction crt = createCardRetryTransaction(pointOfSale, purchase, ptd);
        crt.setCardData(encryptedCardData);
        crt.setCardExpiry(CardProcessingConstants.MAX_YEAR_MONTH_VALUE);
        crt.setBadCardHash(WebCoreConstants.N_A_STRING);
        crt.setCardHash(WebCoreConstants.N_A_STRING);
        crt.setCardType(WebCoreConstants.N_A_STRING);
        return crt;
    }
    
    public final int getMappingPropertiesValue(final String key) {
        return Integer.parseInt(this.mappingProperties.getProperty(key).trim());
    }
}
