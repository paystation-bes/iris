package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.service.ProcessorTransactionTypeService;

@Service("processorTransactionTypeService")
public class ProcessorTransactionTypeServiceImpl implements ProcessorTransactionTypeService {
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    private List<ProcessorTransactionType> processorTransactionTypes;
    
    private Map<Integer, ProcessorTransactionType> map;
    
    @PostConstruct
    private void loadAllToMap() {
        List<ProcessorTransactionType> list = openSessionDao.loadAll(ProcessorTransactionType.class);
        map = new HashMap<Integer, ProcessorTransactionType>(list.size());
        
        Iterator<ProcessorTransactionType> iter = list.iterator();
        while (iter.hasNext()) {
            ProcessorTransactionType ptType = iter.next();
            map.put(ptType.getId(), ptType);
        }
    }
    
    @Override
    public ProcessorTransactionType findProcessorTransactionType(Integer id) {
        return map.get(id);
    }
    
    @Override
    public List<ProcessorTransactionType> loadAll() {
        if (processorTransactionTypes == null) {
            processorTransactionTypes = openSessionDao.loadAll(ProcessorTransactionType.class);
        }
        return processorTransactionTypes;
    }
}
