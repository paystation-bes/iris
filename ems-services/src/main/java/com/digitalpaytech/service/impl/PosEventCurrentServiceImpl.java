package com.digitalpaytech.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posEventCurrentService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PosEventCurrentServiceImpl implements PosEventCurrentService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    
    private static final String SQL_DISABLE_DEFAULT_ALERTS = "SELECT A.Id AS pecId FROM (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId "
                                                             + "FROM POSEventCurrent pec "
                                                             + "INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id) "
                                                             + "WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 1 AND pec.IsHidden = 0 AND pec.PointOfSaleId IN (:pointOfSaleIdList)) A "
                                                             + "INNER JOIN (SELECT pec.PointOfSaleId, cat.AlertThresholdTypeId FROM POSEventCurrent pec "
                                                             + "INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id) "
                                                             + "WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 0) B ON (A.PointOfSaleId = B.PointOfSaleId AND A.AlertThresholdTypeId = B.AlertThresholdTypeId) "
                                                             + "GROUP BY A.Id";
    
    private static final String SQL_ENABLE_DEFAULT_ALERTS = "SELECT A.Id AS pecId FROM (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId "
                                                            + "FROM POSEventCurrent pec "
                                                            + "INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id) "
                                                            + "WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 1 AND pec.IsHidden = 1 AND pec.PointOfSaleId IN (:pointOfSaleIdList)) A "
                                                            + "LEFT JOIN (SELECT pec.Id, pec.PointOfSaleId, cat.AlertThresholdTypeId FROM POSEventCurrent pec "
                                                            + "INNER JOIN CustomerAlertType cat ON (pec.CustomerAlertTypeId = cat.Id) "
                                                            + "WHERE cat.AlertThresholdTypeId <> 12 AND cat.IsDefault = 0) B ON (A.PointOfSaleId = B.PointOfSaleId AND A.AlertThresholdTypeId = B.AlertThresholdTypeId) "
                                                            + "WHERE B.Id IS NULL " + "GROUP BY A.Id";
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeId(final Integer pointOfSaleId, final byte eventTypeId) {
        return (PosEventCurrent) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("PosEventCurrent.findPosEventCurrentByPointOfSaleIdAndEventTypeId", new String[] {
                    HibernateConstants.POINTOFSALE_ID, HibernateConstants.EVENT_TYPE_ID, }, new Object[] { pointOfSaleId, eventTypeId, }, true);
    }
    
    @Override
    public final PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(final Integer pointOfSaleId,
        final byte eventTypeId, final Integer customerAlertTypeId) {
        final List<PosEventCurrent> posEventCurrentList = this.entityDao
                .findByNamedQueryAndNamedParam("PosEventCurrent.findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId",
                                               new String[] { HibernateConstants.POINTOFSALE_ID, HibernateConstants.EVENT_TYPE_ID,
                                                   HibernateConstants.CUSTOMER_ALERT_TYPE_ID, }, new Object[] { pointOfSaleId, eventTypeId,
                                                   customerAlertTypeId, }, true);
        
        PosEventCurrent posEventCurrent = null;
        if (posEventCurrentList != null && !posEventCurrentList.isEmpty()) {
            if (posEventCurrentList.size() == 1) {
                posEventCurrent = posEventCurrentList.get(0);
            } else {
                Date latestDate = null;
                for (PosEventCurrent pec : posEventCurrentList) {
                    final Date latestCurrent = pec.getClearedGmt() != null ? pec.getClearedGmt() : pec.getAlertGmt();
                    if (posEventCurrent == null) {
                        posEventCurrent = pec;
                        latestDate = latestCurrent;
                    } else {
                        if (latestDate.equals(latestCurrent) && !pec.isIsActive() || latestDate.before(latestCurrent)) {
                            posEventCurrent = pec;
                            latestDate = latestCurrent;
                        }
                    }
                }
                posEventCurrentList.remove(posEventCurrent);
                for (PosEventCurrent pec : posEventCurrentList) {
                    if (pec.isIsActive()) {
                        final PosEventCurrent clearEvent = new PosEventCurrent(pec.getPointOfSale(), new EventSeverityType(
                                WebCoreConstants.SEVERITY_CLEAR), null, pec.getEventType(), null, false, false);
                        this.posAlertStatusService.updateAlertCount(clearEvent, pec.getEventSeverityType().getId(), false);
                    }
                    this.entityDao.delete(pec);
                }
            }
        }
        
        return posEventCurrent;
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void savePosEventCurrent(final PosEventCurrent posEventCurrent) {
        // TODO
        this.entityDao.save(posEventCurrent);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updatePosEventCurrent(final PosEventCurrent posEventCurrent) {
        // TODO
        this.entityDao.update(posEventCurrent);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PosEventCurrent> findActivePosEventCurrentByTimestamp(final Date timestamp) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findActivePosEventCurrentByTimestamp", HibernateConstants.TIMESTAMP,
                                                            timestamp);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PosEventCurrent> findActivePosEventCurrentByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findActivePosEventCurrentByCustomerId", HibernateConstants.CUSTOMER_ID,
                                                            customerId);
    }
    
    @Override
    public final void deletePosEventCurrent(final Integer posId, final byte eventTypeId, final Integer customerAlertTypeId) {
        // TODO
        final PosEventCurrent dbPosEventCurrent = this.findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(posId, eventTypeId,
                                                                                                                              customerAlertTypeId);
        this.entityDao.delete(dbPosEventCurrent);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<PosEventCurrent> findNextRecoveryDuplicateSerialNumberData(final int numOfRows) {
        final Calendar oneDayAgo = Calendar.getInstance();
        oneDayAgo.add(Calendar.DAY_OF_YEAR, -1);
        return this.entityDao.findByNamedQueryAndNamedParamWithLimit("PosEventCurrent.findNextRecoveryDuplicateSerialNumberData",
                                                                     new String[] { "oneDayAgo" }, new Object[] { oneDayAgo.getTime() }, false,
                                                                     numOfRows);
    }
    
    @Override
    public final List<PosEventCurrent> findMaintenanceCentreDetailByPointOfSaleId(final Integer pointOfSaleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findMaintenanceCentreDetailByPointOfSaleId",
                                                            HibernateConstants.POINTOFSALE_ID, pointOfSaleId);
    }
    
    @Override
    public final List<PosEventCurrent> findCollectionCentreDetailByPointOfSaleId(final Integer pointOfSaleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findCollectionCentreDetailByPointOfSaleId",
                                                            HibernateConstants.POINTOFSALE_ID, pointOfSaleId);
    }
    
    @Override
    public final List<PosEventCurrent> findActiveDefaultPosEventCurrentByPosIdListAndAttIdList(final List<Integer> pointOfSaleIdList,
        final List<Integer> alertThresholdTypeIdList) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findActiveDefaultPosEventCurrentByPosIdListAndAttIdList", new String[] {
            HibernateConstants.POINTOFSALE_ID_LIST, HibernateConstants.ALERT_THRESHOLD_TYPE_ID_LIST, }, new Object[] { pointOfSaleIdList,
            alertThresholdTypeIdList, });
    }
    
    @Override
    public final PosEventCurrent findActivePosEventCurrentById(final Long posEventCurrentId) {
        return (PosEventCurrent) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosEventCurrent.findActivePosEventCurrentById",
                                                                                    HibernateConstants.POS_EVENT_CURRENT_ID, posEventCurrentId);
    }
    
    @Override
    public final PosEventCurrent findActivePosEventCurrentByEventTypeIdAndPointOfSaleId(final byte eventTypeId, final Integer pointOfSaleId) {
        
        return (PosEventCurrent) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("PosEventCurrent.findActivePosEventCurrentByEventTypeIdAndPointOfSaleId", new String[] {
                    HibernateConstants.EVENT_TYPE_ID, HibernateConstants.POINTOFSALE_ID, }, new Object[] { eventTypeId, pointOfSaleId, }, true);
        
    }
    
    @Override
    public final List<PosEventCurrent> findActivePosEventCurrentByPointOfSaleId(final Integer posId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findActivePosEventCurrentByPointOfSaleId",
                                                            new String[] { HibernateConstants.POINTOFSALE_ID, }, new Object[] { posId }, true);
        
    }
    
    @Override
    public final void delete(final PosEventCurrent posEventCurrent) {
        this.entityDao.delete(posEventCurrent);
    }
    
    @Override
    public final List<PosEventCurrent> findAllPosEventCurrentByPointOfSaleId(final Integer posId) {
        
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findPosEventCurrentByPointOfSaleId",
                                                            new String[] { HibernateConstants.POINTOFSALE_ID, }, new Object[] { posId }, true);
    }
    
    @Override
    public final List<Long> findDefaultPosEventCurrentsToDisable(final List<Integer> pointOfSaleIdList) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_DISABLE_DEFAULT_ALERTS);
        q.addScalar(HibernateConstants.PEC_ID, new LongType());
        q.setParameterList(HibernateConstants.POINTOFSALE_ID_LIST, pointOfSaleIdList);
        return q.list();
    }
    
    @Override
    public final List<Long> findDefaultPosEventCurrentsToEnable(final List<Integer> pointOfSaleIdList) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_ENABLE_DEFAULT_ALERTS);
        q.addScalar(HibernateConstants.PEC_ID, new LongType());
        q.setParameterList(HibernateConstants.POINTOFSALE_ID_LIST, pointOfSaleIdList);
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final List<PosEventCurrent> findPosEventCurrentsByIdList(final List<Long> pecIdList) {
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findPosEventCurrentsByIdList",
                                                            new String[] { HibernateConstants.PEC_ID_LIST, }, new Object[] { pecIdList }, true);
    }
    
}
