package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.CitationWidgetBase;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.FlexDataWSCallStatus;
import com.digitalpaytech.domain.FlexETLDataType;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.domain.FlexWSUserAccount.Blocker;
import com.digitalpaytech.domain.Time;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.FlexLocationFacilitySearchCriteria;
import com.digitalpaytech.dto.FlexLocationFacilitySelectorDTO;
import com.digitalpaytech.dto.FlexLocationPropertySearchCriteria;
import com.digitalpaytech.dto.FlexLocationPropertySelectorDTO;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet.ViolationFlexRecord;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet.FacilityFlexRecord;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet.PropertyFlexRecord;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.WebObjectId;

// This class seem to have inconsistent transactional behavior when some methods are declared final.
// So, I have suppress warning here to make it easier to remove later when we have figured out it is 100% safe to remove.
@SuppressWarnings({ "checkstyle:designforextension", "PMD.GodClass", "PMD.TooManyMethods" })
@Service("flexWSService")
@Transactional(propagation = Propagation.SUPPORTS)
/*
 * "PMD.GodClass" - the fix invloves too many code changes
 * "PMD.TooManyMethods" - the fix invloves many code changes, as well as might increase the cyclomatic
 * complexity of existing methods
 */
public class FlexWSServiceImpl implements FlexWSService {
    
    private static final Logger LOG = Logger.getLogger(FlexWSServiceImpl.class);
    
    private static final String FLEX_IDS = "flexIds";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    @Override
    public FlexWSUserAccount findById(final Integer id) {
        return (FlexWSUserAccount) this.entityDao.getNamedQuery("FlexWSUserAccount.findById").setParameter("id", id).uniqueResult();
    }
    
    @Override
    public FlexWSUserAccount findFlexWSUserAccountByCustomerId(final Integer customerId) {
        return (FlexWSUserAccount) this.entityDao.findUniqueByNamedQueryAndNamedParam("FlexWSUserAccount.findFlexWSUserAccountByCustomerId",
                                                                                      HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public FlexDataWSCallStatus findLastCallByCustomerIdAndTypeId(final Integer customerId, final Byte flexETLDataTypeId) {
        return (FlexDataWSCallStatus) this.entityDao.getNamedQuery("FlexDataWSCallStatus.findLastCallByCustomerIdAndTypeId")
                .setParameter(HibernateConstants.CUSTOMER_ID, customerId).setParameter(HibernateConstants.FLEX_ETL_DATA_TYPE_ID, flexETLDataTypeId)
                .setMaxResults(1).uniqueResult();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveFlexDataWSCallStatus(final FlexDataWSCallStatus flexDataWSCallStatus) {
        this.entityDao.save(flexDataWSCallStatus);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateFlexDataWSCallStatus(final FlexDataWSCallStatus flexDataWSCallStatus) {
        this.entityDao.update(flexDataWSCallStatus);
    }
    
    @Override
    public FlexETLDataType getFlexETLDataType(final byte flexETLDataTypeId) {
        return this.entityDao.get(FlexETLDataType.class, flexETLDataTypeId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveFlexLocationFacility(final FlexLocationFacility flexLocationFacility) {
        this.entityDao.save(flexLocationFacility);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateFlexLocationFacility(final FlexLocationFacility flexLocationFacility) {
        this.entityDao.update(flexLocationFacility);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationFacility> findAllFlexLocationFacilitiesByCustomer(final int customerId) {
        return (List<FlexLocationFacility>) this.entityDao
                .findByNamedQueryAndNamedParam("FlexLocationFacility.findFlexLocationFacilitiesByCustomerId", HibernateConstants.CUSTOMER_ID,
                                               customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationProperty> findAllFlexLocationPropertiesByCustomer(final int customerId) {
        return (List<FlexLocationProperty>) this.entityDao
                .findByNamedQueryAndNamedParam("FlexLocationProperty.findFlexLocationPropertiesByCustomerId", HibernateConstants.CUSTOMER_ID,
                                               customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveFlexLocationProperty(final FlexLocationProperty flexLocationProperty) {
        this.entityDao.save(flexLocationProperty);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateFlexLocationProperty(final FlexLocationProperty flexLocationProperty) {
        this.entityDao.update(flexLocationProperty);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CitationType> findCitationTypeByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CitationType.findByCustomerId", HibernateConstants.CUSTOMER_ID, customerId, true);
    }
    
    @Override
    public CitationType findCitationTypeByCustomerIdAndFlexCitationTypeId(final Integer customerId, final Integer flexCitationTypeId) {
        return (CitationType) this.entityDao.findUniqueByNamedQueryAndNamedParam("CitationType.findByCustomerIdAndFlexCitationTypeId", new String[] {
            HibernateConstants.CUSTOMER_ID, "flexCitationTypeId", }, new Object[] { customerId, flexCitationTypeId, }, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCitationType(final CitationType flexLocationProperty) {
        this.entityDao.save(flexLocationProperty);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCitationType(final CitationType flexLocationProperty) {
        this.entityDao.update(flexLocationProperty);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public FlexLocationFacility findFlexLocationFacilityById(final Integer id) {
        return (FlexLocationFacility) this.entityDao.get(FlexLocationFacility.class, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationFacility> findFlexLocationFacilityByLocationId(final Integer locationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("FlexLocationFacility.findFlexLocationFacilityByLocationIds",
                                                            HibernateConstants.PARAMETER_LOCATION_ID, locationId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationFacility> findUnselectedFlexLocationFacilityByCustomerId(final Integer customerId, final List<Integer> locationIds) {
        final String[] params = { HibernateConstants.CUSTOMER_ID, HibernateConstants.PARAMETER_LOCATION_IDS };
        final Object[] values = { customerId, locationIds };
        
        return this.entityDao.findByNamedQueryAndNamedParam("FlexLocationFacility.findActiveFlexLocationFacilityByCustomerId", params, values);
    }
    
    @SuppressWarnings({ "unchecked", "PMD.AvoidInstantiatingObjectsInLoops" })
    // "PMD.AvoidInstantiatingObjectsInLoops" - cannot be avoided, must instantiate a new object on every loop iteration
    @Override
    public List<FlexLocationFacilitySelectorDTO> findFlexLocationFacilitySelector(final FlexLocationFacilitySearchCriteria criteria) {
        List<Integer> selectedFlfIdsByLoc = new ArrayList<Integer>(1);
        if (criteria.getSelectedLocationId() != null) {
            selectedFlfIdsByLoc = (List<Integer>) this.entityDao.getNamedQuery("FlexLocationFacility.findFlexLocationFacilityIdByLocationIds")
                    .setParameter("locationIds", criteria.getSelectedLocationId()).list();
        }
        
        List<Integer> selectedFlfIdsByCustomer = new ArrayList<Integer>(1);
        if (criteria.getCustomerId() != null) {
            selectedFlfIdsByCustomer = (List<Integer>) this.entityDao
                    .getNamedQuery("FlexLocationFacility.findActiveFlexLocationFacilityIdByCustomerId")
                    .setParameter(HibernateConstants.CUSTOMER_ID, criteria.getCustomerId())
                    .setParameter(HibernateConstants.PARAMETER_LOCATION_IDS, criteria.getSelectedLocationId()).list();
        }
        
        final Set<Integer> selectedFlf = new HashSet<Integer>(selectedFlfIdsByLoc.size() * 2);
        selectedFlf.addAll(selectedFlfIdsByLoc);
        selectedFlf.addAll(selectedFlfIdsByCustomer);
        
        final Criteria flfQuery = createFlexLocationFacilitySearchQuery(criteria);
        //        flfQuery.addOrder(Order.asc("facCode").ignoreCase());
        flfQuery.addOrder(Order.asc("facDescription").ignoreCase());
        
        final List<FlexLocationFacility> flfList = flfQuery.list();
        
        final List<FlexLocationFacilitySelectorDTO> result = new ArrayList<FlexLocationFacilitySelectorDTO>(flfList.size());
        for (FlexLocationFacility flf : flfList) {
            final FlexLocationFacilitySelectorDTO dto = new FlexLocationFacilitySelectorDTO();
            dto.setFacCode(flf.getFacCode());
            dto.setFacDescription(flf.getFacDescription());
            dto.setRandomId(new WebObjectId(FlexLocationFacility.class, flf.getId()));
            dto.setStatus(selectedFlfIdsByLoc.contains(flf.getId()) ? WebCoreConstants.SELECTED : WebCoreConstants.ACTIVE);
            
            result.add(dto);
        }
        
        return result;
    }
    
    private Criteria createFlexLocationFacilitySearchQuery(final FlexLocationFacilitySearchCriteria criteria) {
        final Criteria flfQuery = this.entityDao.createCriteria(FlexLocationFacility.class).setCacheable(true);
        //flfQuery.createCriteria("location", "loc").setFetchMode("location", FetchMode.SELECT);
        
        if (criteria.getCustomerId() != null) {
            flfQuery.add(Restrictions.eq(HibernateConstants.HQL_CUSTOMER_ID, criteria.getCustomerId()));
        }
        
        //        if (criteria.getFlexLocationFacilityId() != null) {
        //            flfQuery.add(Restrictions.eq("id", criteria.getFlexLocationFacilityId()));
        //        } else {
        //            if (criteria.getLocationId() != null) {
        //                flfQuery.add(Restrictions.eq("location.id", criteria.getLocationId()));
        //            }            
        //        }         
        
        flfQuery.add(Restrictions.eq("facIsActive", true));
        
        if (criteria.getPage() != null && criteria.getItemsPerPage() != null) {
            final int page = criteria.getPage();
            final int itemsPerPage = criteria.getItemsPerPage();
            final int firstResult = (page - 1) * itemsPerPage;
            if (criteria.getMaxUpdatedTime() == null) {
                criteria.setMaxUpdatedTime(new Date());
            }
            
            flfQuery.add(Restrictions.lt(HibernateConstants.PARAMETER_LAST_MODIFIED_GMT, criteria.getMaxUpdatedTime()));
            flfQuery.setFirstResult(firstResult);
            flfQuery.setMaxResults(criteria.getItemsPerPage());
        }
        
        return flfQuery;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationProperty> findFlexLocationPropertyByLocationId(final Integer locationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("FlexLocationProperty.findFlexLocationPropertyByLocationIds",
                                                            HibernateConstants.PARAMETER_LOCATION_ID, locationId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FlexLocationProperty> findUnselectedFlexLocationPropertyByCustomerId(final Integer customerId, final List<Integer> locationIds) {
        final String[] params = { HibernateConstants.CUSTOMER_ID, HibernateConstants.PARAMETER_LOCATION_IDS };
        final Object[] values = { customerId, locationIds };
        
        return this.entityDao.findByNamedQueryAndNamedParam("FlexLocationProperty.findActiveFlexLocationPropertyByCustomerId", params, values);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public FlexLocationProperty findFlexLocationPropertyById(final Integer id) {
        return (FlexLocationProperty) this.entityDao.get(FlexLocationProperty.class, id);
    }
    
    // "PMD.AvoidInstantiatingObjectsInLoops" - cannot be avoided, must instantiate a new object on every loop iteration
    @SuppressWarnings({ "unchecked", "PMD.AvoidInstantiatingObjectsInLoops" })
    @Override
    public List<FlexLocationPropertySelectorDTO> findFlexLocationPropertySelector(final FlexLocationPropertySearchCriteria criteria) {
        List<Integer> selectedFlpIdsByLoc = new ArrayList<Integer>(1);
        if (criteria.getSelectedLocationId() != null) {
            selectedFlpIdsByLoc = (List<Integer>) this.entityDao.getNamedQuery("FlexLocationProperty.findFlexLocationPropertyIdByLocationIds")
                    .setParameter(HibernateConstants.PARAMETER_LOCATION_IDS, criteria.getSelectedLocationId()).list();
        }
        
        List<Integer> selectedFlpIdsByCustomer = new ArrayList<Integer>(1);
        if (criteria.getCustomerId() != null) {
            selectedFlpIdsByCustomer = (List<Integer>) this.entityDao
                    .getNamedQuery("FlexLocationProperty.findActiveFlexLocationPropertyIdByCustomerId")
                    .setParameter(HibernateConstants.CUSTOMER_ID, criteria.getCustomerId())
                    .setParameter(HibernateConstants.PARAMETER_LOCATION_IDS, criteria.getSelectedLocationId()).list();
        }
        
        final Set<Integer> selectedFlp = new HashSet<Integer>(selectedFlpIdsByLoc.size() * 2);
        selectedFlp.addAll(selectedFlpIdsByLoc);
        selectedFlp.addAll(selectedFlpIdsByCustomer);
        
        final Criteria flpQuery = createFlexLocationPropertySearchQuery(criteria);
        
        flpQuery.addOrder(Order.asc("proName").ignoreCase());
        
        final List<FlexLocationProperty> flpList = flpQuery.list();
        
        final List<FlexLocationPropertySelectorDTO> result = new ArrayList<FlexLocationPropertySelectorDTO>(flpList.size());
        for (FlexLocationProperty flp : flpList) {
            final FlexLocationPropertySelectorDTO dto = new FlexLocationPropertySelectorDTO();
            dto.setProUid(flp.getProUid());
            dto.setProName(flp.getProName());
            dto.setRandomId(new WebObjectId(FlexLocationProperty.class, flp.getId()));
            dto.setStatus(selectedFlpIdsByLoc.contains(flp.getId()) ? WebCoreConstants.SELECTED : WebCoreConstants.ACTIVE);
            
            result.add(dto);
        }
        
        return result;
    }
    
    private Criteria createFlexLocationPropertySearchQuery(final FlexLocationPropertySearchCriteria criteria) {
        final Criteria flpQuery = this.entityDao.createCriteria(FlexLocationProperty.class).setCacheable(true);
        //flpQuery.createCriteria("location", "loc").setFetchMode("location", FetchMode.SELECT);
        
        if (criteria.getCustomerId() != null) {
            flpQuery.add(Restrictions.eq(HibernateConstants.HQL_CUSTOMER_ID, criteria.getCustomerId()));
        }
        
        //        if (criteria.getFlexLocationFacilityId() != null) {
        //            flpQuery.add(Restrictions.eq("id", criteria.getFlexLocationPropertyId()));
        //        } else {
        //            if (criteria.getLocationId() != null) {
        //                flpQuery.add(Restrictions.eq("location.id", criteria.getLocationId()));
        //            }            
        //        }         
        
        flpQuery.add(Restrictions.eq("proIsActive", true));
        
        if (criteria.getPage() != null && criteria.getItemsPerPage() != null) {
            final int page = criteria.getPage();
            final int itemsPerPage = criteria.getItemsPerPage();
            final int firstResult = (page - 1) * itemsPerPage;
            if (criteria.getMaxUpdatedTime() == null) {
                criteria.setMaxUpdatedTime(new Date());
            }
            
            flpQuery.add(Restrictions.lt(HibernateConstants.PARAMETER_LAST_MODIFIED_GMT, criteria.getMaxUpdatedTime()));
            flpQuery.setFirstResult(firstResult);
            flpQuery.setMaxResults(criteria.getItemsPerPage());
        }
        
        return flpQuery;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveFlexAccount(final FlexWSUserAccount user) {
        FlexWSUserAccount existing = null;
        if (user.getId() != null) {
            existing = this.entityDao.get(FlexWSUserAccount.class, user.getId());
            if (existing == null) {
                throw new IllegalStateException("Could not locate FlexWSUserAccount: " + user.getId() + ", this account might have been deleted!");
            }
        }
        
        if (existing == null) {
            user.setBlockingStatus(0);
            this.entityDao.save(user);
        } else {
            existing.setUrl(user.getUrl());
            existing.setUsername(user.getUsername());
            existing.setTimeZone(user.getTimeZone());
            
            if (user.getPassword() != null) {
                existing.setPassword(user.getPassword());
            }
            
            existing.setLastModifiedByUserId(user.getLastModifiedByUserId());
            existing.setLastModifiedGmt(user.getLastModifiedGmt());
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void blockFlexAccount(final Integer flexAccountId, final Blocker blocker) {
        final FlexWSUserAccount existing = this.entityDao.get(FlexWSUserAccount.class, flexAccountId);
        blocker.block(existing);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void unblockFlexAccount(final Integer flexAccountId, final Blocker blocker) {
        final FlexWSUserAccount existing = this.entityDao.get(FlexWSUserAccount.class, flexAccountId);
        blocker.unblock(existing);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void summarizeCitationDayData(final Integer customerId, final int dayOfMonth, final int monthOfYear, final int year) {
        final Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth, 0, 0);
        
        final Time endTime = (Time) this.entityDao.getNamedQuery("Time.findDay")
                .setParameter(HibernateConstants.DAY, (byte) cal.get(Calendar.DAY_OF_MONTH))
                .setParameter(HibernateConstants.MONTH, (byte) (cal.get(Calendar.MONTH) + 1))
                .setParameter(HibernateConstants.YEAR, (short) cal.get(Calendar.YEAR)).uniqueResult();
        
        cal.add(Calendar.DAY_OF_MONTH, -1);
        
        final Time startTime = (Time) this.entityDao.getNamedQuery("Time.findDay")
                .setParameter(HibernateConstants.DAY, (byte) cal.get(Calendar.DAY_OF_MONTH))
                .setParameter(HibernateConstants.MONTH, (byte) (cal.get(Calendar.MONTH) + 1))
                .setParameter(HibernateConstants.YEAR, (short) cal.get(Calendar.YEAR)).uniqueResult();
        
        this.entityDao.getNamedQuery("CitationTotalDay.purge").setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .setParameter(HibernateConstants.START_TIME_ID, startTime.getId()).setParameter(HibernateConstants.END_TIME_ID, endTime.getId())
                .executeUpdate();
        
        this.entityDao.getNamedQuery("CitationTotalDay.summarize").setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .setParameter(HibernateConstants.START_TIME_ID, startTime.getId()).setParameter(HibernateConstants.END_TIME_ID, endTime.getId())
                .executeUpdate();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void summarizeCitationMonthData(final Integer customerId, final int monthOfYear, final int year) {
        final Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, 1, 0, 0);
        
        final Time endTime = (Time) this.entityDao.getNamedQuery("Time.findMonth")
                .setParameter(HibernateConstants.MONTH, (byte) (cal.get(Calendar.MONTH) + 1))
                .setParameter(HibernateConstants.YEAR, (short) cal.get(Calendar.YEAR)).uniqueResult();
        
        cal.add(Calendar.MONTH, -1);
        
        final Time startTime = (Time) this.entityDao.getNamedQuery("Time.findMonth")
                .setParameter(HibernateConstants.MONTH, (byte) (cal.get(Calendar.MONTH) + 1))
                .setParameter(HibernateConstants.YEAR, (short) cal.get(Calendar.YEAR)).uniqueResult();
        
        this.entityDao.getNamedQuery("CitationTotalMonth.purge").setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .setParameter(HibernateConstants.START_TIME_ID, startTime.getId()).setParameter(HibernateConstants.END_TIME_ID, endTime.getId())
                .executeUpdate();
        
        this.entityDao.getNamedQuery("CitationTotalMonth.summarize").setParameter(HibernateConstants.CUSTOMER_ID, customerId)
                .setParameter(HibernateConstants.START_TIME_ID, startTime.getId()).setParameter(HibernateConstants.END_TIME_ID, endTime.getId())
                .executeUpdate();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveWidgetData(final Collection<? extends Serializable> widgetData, final Integer lastId, final Integer callStatusId) {
        for (Serializable data : widgetData) {
            this.entityDao.saveOrUpdate(data);
        }
        completeCallStatus(lastId, callStatusId);
        this.entityDao.flush();
    }
    
    @Override
    public boolean isFlexDataExtractionServer() {
        try {
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.FLEX_DATA_EXTRACTION_PROCESS_SERVER,
                                                                                    EmsPropertiesService.DEFAULT_FLEX_DATA_EXTRACTION_PROCESS_SERVER,
                                                                                    true);
            
            if (processServer == null || processServer.equals(WebCoreConstants.BLANK)) {
                throw new ApplicationException("Couldnot get Server to process Flex Data Extraction process");
            }
            
            if (clusterName.equals(processServer)) {
                LOG.info("Server " + clusterName + " configured for Flex Data Extraction Task");
                return true;
            } else {
                LOG.info("Server " + clusterName + " not configured for Flex Data Extraction Task");
                return false;
            }
        } catch (ApplicationException e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e1) {
                LOG.error("UnExpected UnknownHost! This shouldn't happened", e1);
            }
            
            final String err = new StringBuilder("Unable to read memeber name from cluster.properties for cluster member ").append(addr).toString();
            LOG.error(err);
            
            return false;
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncFacilities(final List<FacilityFlexRecord> modifiedFacilities, final Customer customer, final Integer callStatusId) {
        if (modifiedFacilities.size() > 0) {
            final Date now = new Date();
            
            final Map<Integer, FacilityFlexRecord> flexFacilities = new HashMap<Integer, FacilityFlexRecord>(modifiedFacilities.size() * 2);
            for (FacilityFlexRecord ffr : modifiedFacilities) {
                flexFacilities.put(ffr.getUid(), ffr);
            }
            
            @SuppressWarnings("unchecked")
            final List<FlexLocationFacility> irisFacilities = this.entityDao.getNamedQuery("FlexLocationFacility.findByCustomerIdAndFlexIds")
                    .setParameter(HibernateConstants.CUSTOMER_ID, customer.getId()).setParameterList(FLEX_IDS, flexFacilities.keySet()).list();
            
            for (FlexLocationFacility irisRecord : irisFacilities) {
                final FacilityFlexRecord flexRecord = flexFacilities.remove(irisRecord.getFacUid());
                if (flexRecord != null) {
                    copyProperties(irisRecord, flexRecord, now, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    
                    this.entityDao.update(irisRecord);
                }
            }
            
            for (Map.Entry<Integer, FacilityFlexRecord> flexRecordEntry : flexFacilities.entrySet()) {
                final FacilityFlexRecord flexRecord = flexRecordEntry.getValue();
                final FlexLocationFacility irisRecord = copyProperties(new FlexLocationFacility(), flexRecord, now,
                                                                       WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                irisRecord.setCustomer(customer);
                
                this.entityDao.save(irisRecord);
            }
        }
        
        completeCallStatus(null, callStatusId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncProperties(final List<PropertyFlexRecord> modifiedProperties, final Customer customer, final Integer callStatusId) {
        if (modifiedProperties.size() > 0) {
            final Date now = new Date();
            
            final Map<Integer, PropertyFlexRecord> flexProperties = new HashMap<Integer, PropertyFlexRecord>(modifiedProperties.size() * 2);
            for (PropertyFlexRecord pfr : modifiedProperties) {
                flexProperties.put(pfr.getUid(), pfr);
            }
            
            @SuppressWarnings("unchecked")
            final List<FlexLocationProperty> irisProperties = this.entityDao.getNamedQuery("FlexLocationProperty.findByCustomerIdAndFlexIds")
                    .setParameter(HibernateConstants.CUSTOMER_ID, customer.getId()).setParameterList(FLEX_IDS, flexProperties.keySet()).list();
            
            for (FlexLocationProperty irisRecord : irisProperties) {
                final PropertyFlexRecord flexRecord = flexProperties.remove(irisRecord.getProUid());
                if (flexRecord != null) {
                    copyProperties(irisRecord, flexRecord, now, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    
                    this.entityDao.update(irisRecord);
                }
            }
            
            for (Map.Entry<Integer, PropertyFlexRecord> flexRecordEntry : flexProperties.entrySet()) {
                final PropertyFlexRecord flexRecord = flexRecordEntry.getValue();
                final FlexLocationProperty irisRecord = copyProperties(new FlexLocationProperty(), flexRecord, now,
                                                                       WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                irisRecord.setCustomer(customer);
                
                this.entityDao.save(irisRecord);
            }
        }
        
        completeCallStatus(null, callStatusId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncCitationTypes(final List<ViolationFlexRecord> modifiedCitationTypes, final Customer customer, final Integer callStatusId) {
        if (modifiedCitationTypes.size() > 0) {
            final Date now = new Date();
            
            final Map<Integer, ViolationFlexRecord> flexCitationTypes = new HashMap<Integer, ViolationFlexRecord>(modifiedCitationTypes.size() * 2);
            for (ViolationFlexRecord vfr : modifiedCitationTypes) {
                flexCitationTypes.put(vfr.getViolationUid(), vfr);
            }
            
            @SuppressWarnings("unchecked")
            final List<CitationType> irisCitationTypes = this.entityDao.getNamedQuery("CitationType.findByCustomerIdAndFlexIds")
                    .setParameter(HibernateConstants.CUSTOMER_ID, customer.getId()).setParameterList(FLEX_IDS, flexCitationTypes.keySet()).list();
            
            for (CitationType irisRecord : irisCitationTypes) {
                final ViolationFlexRecord flexRecord = flexCitationTypes.remove(irisRecord.getFlexCitationTypeId());
                if (flexRecord != null) {
                    copyProperties(irisRecord, flexRecord, now, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                    
                    this.entityDao.update(irisRecord);
                }
            }
            
            for (Map.Entry<Integer, ViolationFlexRecord> flexRecordEntry : flexCitationTypes.entrySet()) {
                final ViolationFlexRecord flexRecord = flexRecordEntry.getValue();
                final CitationType irisRecord = copyProperties(new CitationType(), flexRecord, now, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                irisRecord.setCustomer(customer);
                
                this.entityDao.save(irisRecord);
            }
            
            final FlexWSUserAccount flexAccount = this.findFlexWSUserAccountByCustomerId(customer.getId());
            if (flexAccount.isBlocked()) {
                this.unblockFlexAccount(flexAccount.getId(), FlexWSUserAccount.Blocker.CITATION_TYPE_MISSING);
            }
        }
        
        completeCallStatus(null, callStatusId);
    }
    
    private FlexLocationFacility copyProperties(final FlexLocationFacility irisRecord, final FacilityFlexRecord flexRecord, final Date modifyGmt,
        final int modifyUser) {
        irisRecord.setFacUid(flexRecord.getUid());
        irisRecord.setFacCode(flexRecord.getCode());
        irisRecord.setFacDescription(flexRecord.getDescription());
        irisRecord.setFacIsActive(flexRecord.isActive());
        
        irisRecord.setLastModifiedGmt(modifyGmt);
        irisRecord.setLastModifiedByUserId(modifyUser);
        
        return irisRecord;
    }
    
    private FlexLocationProperty copyProperties(final FlexLocationProperty irisRecord, final PropertyFlexRecord flexRecord, final Date modifyGmt,
        final int modifyUser) {
        irisRecord.setProUid(flexRecord.getUid());
        irisRecord.setProName(flexRecord.getName());
        irisRecord.setProCity(flexRecord.getCity());
        irisRecord.setProIsActive(flexRecord.isActive());
        
        irisRecord.setLastModifiedGmt(modifyGmt);
        irisRecord.setLastModifiedByUserId(modifyUser);
        
        return irisRecord;
    }
    
    private CitationType copyProperties(final CitationType irisRecord, final ViolationFlexRecord flexRecord, final Date modifyGmt,
        final int modifyUser) {
        irisRecord.setFlexCitationTypeId(flexRecord.getViolationUid());
        irisRecord.setName(flexRecord.getViolationDescription());
        irisRecord.setIsActive(flexRecord.isActive());
        
        irisRecord.setLastModifiedGmt(modifyGmt);
        irisRecord.setLastModifiedByUserId(modifyUser);
        
        return irisRecord;
    }
    
    private void completeCallStatus(final Integer lastId, final Integer callStatusId) {
        if (callStatusId != null) {
            final FlexDataWSCallStatus status = this.entityDao.get(FlexDataWSCallStatus.class, callStatusId);
            if (status != null) {
                status.setLastUniqueIdentifier(lastId);
                status.setStatus((byte) 2);
                this.entityDao.update(status);
            }
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public FlexDataWSCallStatus findFlexDataWSCallStatusById(final Integer callStatusId) {
        
        return this.entityDao.get(FlexDataWSCallStatus.class, callStatusId);
    }
    
    @Override
    public CitationWidgetBase findCitationWidgetData(final String className, final Integer customerId, final Integer flexCitationTypeId,
        final Integer timeId) {
        return (CitationWidgetBase) this.entityDao.findUniqueByNamedQueryAndNamedParam(className + ".find", new String[] {
            HibernateConstants.CUSTOMER_ID, HibernateConstants.FLEX_CITATION_TYPE_ID, HibernateConstants.TIME_ID, }, new Object[] { customerId,
            flexCitationTypeId, timeId, }, false);
    }
    
}
