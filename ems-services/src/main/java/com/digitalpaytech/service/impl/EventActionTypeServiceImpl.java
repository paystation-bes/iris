package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.service.EventActionTypeService;

@Service("EventActionTypeService")
public class EventActionTypeServiceImpl implements EventActionTypeService {
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    private Map<Integer, EventActionType> map;
    
    @PostConstruct
    private void loadAllToMap() {
        final List<EventActionType> list = this.openSessionDao.loadAll(EventActionType.class);
        this.map = new HashMap<Integer, EventActionType>(list.size());
        
        final Iterator<EventActionType> iter = list.iterator();
        while (iter.hasNext()) {
            final EventActionType esType = iter.next();
            this.map.put(esType.getId(), esType);
        }
    }
    
    @Override
    public final EventActionType findEventActionType(final Integer id) {
        return this.map.get(id);
    }
}
