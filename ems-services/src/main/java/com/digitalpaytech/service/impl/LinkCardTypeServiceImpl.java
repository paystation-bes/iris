package com.digitalpaytech.service.impl;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.corecps.CardConfiguration;
import com.digitalpaytech.client.util.HystrixExceptionUtil;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.dto.systemadmin.PosTerminalAssignments;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.dto.Pair;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.impl.LinkMessageProducer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.client.ClientException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service("linkCardTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class LinkCardTypeServiceImpl implements LinkCardTypeService {
    
    private static final Logger LOG = Logger.getLogger(LinkCardTypeServiceImpl.class);
    private static final String CUSTOM_CARDS = "Custom Cards";
    private static List<String> cardNames;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    @Qualifier("pciLinkMessageProducer")
    private AbstractMessageProducer pciLinkMessageProducer;
   
    private CardTypeClient cardTypeClient;
    
    private JSON json;
    
    private Collection<Pair<Pattern, String>> collapsedBinRanges;
    
    private Map<String, List<String>> binRanges;
    
    @Override
    public Collection<Pair<Pattern, String>> getCollapsedBinRanges() {
        if (this.collapsedBinRanges == null || this.collapsedBinRanges.isEmpty()) {
            this.collapsedBinRanges = new ArrayList<>();
            getBinRangesFromCardTypeService();
        }
        return this.collapsedBinRanges;
    }
    
    @Override
    public Map<String, List<String>> getBinRanges() {
        if (this.binRanges == null || this.binRanges.isEmpty()) {
            this.binRanges = new LinkedHashMap<>();
            getBinRangesFromCardTypeService();
        }
        return this.binRanges;
    }
    
    @Override
    public final String getCardType(final int customerId, final String encryptedCardData) throws CryptoException {
        
        if (!this.cryptoService.isLinkCryptoKey(encryptedCardData)) {
            final String err = "Call to link card-type service with scala crypto key";
            LOG.error(err);
            throw new CryptoException(new ClientException(err));
        }
        
        try {
            final String stringResult = this.cardTypeClient.getCardType(customerId, encryptedCardData).execute().toString(Charset.defaultCharset());
            
            final TypeReference<ObjectResponse<String>> objectResponseTypeRef = new TypeReference<ObjectResponse<String>>() {
            };
            
            final ObjectResponse<String> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
            
            return objectResponse.getResponse();
        } catch (HystrixRuntimeException hre) {
            LOG.error(hre);
            if (HystrixExceptionUtil.isCryptoCause(hre)) {
                throw new CryptoException(new ClientException("card-type service returns unable to decrypt card data"));
            } else {
                throw hre;
            }
        } catch (JsonException jex) {
            final String err = "unable to decrypt using Card Type Service";
            LOG.error(err, jex);
            throw new IllegalStateException(err);
        }
    }
    
    @Override
    public String getCreditCardTypeName(final String cardNumber) {
        
        if (this.collapsedBinRanges == null || this.collapsedBinRanges.isEmpty()) {
            getCollapsedBinRanges();
        }
        
        if (cardNumber != null && this.collapsedBinRanges != null) {
            for (Pair<Pattern, String> binRangePair : this.collapsedBinRanges) {
                if (binRangePair.getLeft().matcher(cardNumber).matches()) {
                    return binRangePair.getRight();
                }
            }
        }
        return null;
    }
    
    @PostConstruct
    public final void init() {
        this.json = new JSON();
        
        this.cardTypeClient = this.clientFactory.from(CardTypeClient.class);
        this.collapsedBinRanges = new ArrayList<>();
        this.binRanges = new LinkedHashMap<>();
        getCollapsedBinRanges();
        getBinRanges();
    }
    
    // Public only for testing
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public void getBinRangesFromCardTypeService() {
        try {
            final String stringResult = this.cardTypeClient.getBinRanges().execute().toString(Charset.defaultCharset());
            
            final TypeReference<ObjectResponse<List<CardConfiguration>>> objectResponseTypeRef =
                    new TypeReference<ObjectResponse<List<CardConfiguration>>>() { };
            
            final ObjectResponse<List<CardConfiguration>> objectResponse;
            
            objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
            
            final List<CardConfiguration> configurations = objectResponse.getResponse();
            if (configurations != null && !configurations.isEmpty()) {
                final List<CardConfiguration> customConfigs = new ArrayList<>();
                for (CardConfiguration configuration : configurations) {
                    if (StringUtils.isNotBlank(configuration.getCardCategory()) && configuration.getCardCategory().equalsIgnoreCase(CUSTOM_CARDS)) {
                        customConfigs.add(configuration);
                    } else if (configuration.getPatterns() != null) {
                        populateBinRangesCollapsedBinRanges(configuration);
                    }
                }
                for (CardConfiguration customConfig : customConfigs) {
                    populateBinRangesCollapsedBinRanges(customConfig);
                }
            }
        } catch (Exception e) {
            LOG.error("Unable to get BinRanges from CardType Service, Iris will try again next call", e);
        }
        
    }
    
    private void populateBinRangesCollapsedBinRanges(final CardConfiguration configuration) {
        this.binRanges.put(configuration.getCardType(), Arrays.asList(configuration.getPatterns()));
        for (String pattern : configuration.getPatterns()) {
            this.collapsedBinRanges.add(new Pair<Pattern, String>(Pattern.compile(pattern), configuration.getCardType()));
        }
    }
    
    
    @Override
    public List<String> getAllCardNames() {
        
        if (LinkCardTypeServiceImpl.cardNames == null) {
            try {
                final String stringResult = this.cardTypeClient.getAllCardNames().execute().toString(Charset.defaultCharset());
                
                final TypeReference<ObjectResponse<List<String>>> objectResponseTypeRef = new TypeReference<ObjectResponse<List<String>>>() {
                };
                
                final ObjectResponse<List<String>> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
                
                if (objectResponse != null && objectResponse.getResponse() != null && !objectResponse.getResponse().isEmpty()) {
                    LinkCardTypeServiceImpl.cardNames = objectResponse.getResponse();
                }
                
            } catch (JsonException | HystrixRuntimeException e) {
                LOG.error("Unable to getAllCardNames from CardTypeService", e);
            }
        }
        
        return LinkCardTypeServiceImpl.cardNames == null ? CardProcessingConstants.DEFAULT_CREDIT_CARD_LIST : LinkCardTypeServiceImpl.cardNames;
    }
    
    
    @Override
    public boolean pushPosTerminalAssignments(final Integer unifiCustId, final String deviceSerialNumber, final CardType cardType,
        final UUID terminalToken) {
        
        if (unifiCustId == null) {
            LOG.error("Unifi Id is null, deviceSerialNumber: " + deviceSerialNumber);
            return false;
        }

        final String unifiId = String.valueOf(unifiCustId);
        try {
            if (WebCoreConstants.CARD_TYPE_CREDIT_CARD == cardType.getId()) {
                final List<String> cardNames = getAllCardNames();
                for (String cardName : cardNames) {
                    this.pciLinkMessageProducer.sendWithByteArray(KafkaKeyConstants.LINK_KAFKA_POS_TERMINAL_ASSIGNMENTS_TOPIC_NAME, unifiId,
                                                                  buildPosTerminalAssignments(unifiCustId, deviceSerialNumber,
                                                                                              cardName, terminalToken));
                }
            } else {
                this.pciLinkMessageProducer.sendWithByteArray(KafkaKeyConstants.LINK_KAFKA_POS_TERMINAL_ASSIGNMENTS_TOPIC_NAME, unifiId,
                                                              buildPosTerminalAssignments(unifiCustId, deviceSerialNumber,
                                                                                          cardType.getName(), terminalToken));
            }
        } catch (JsonException | NumberFormatException e) {
            final String err = "JSON serialization error, linkCustomerId: " + unifiCustId + ", deviceSerialNumber: " + deviceSerialNumber
                               + ", cardType: " + cardType.getName() + ", terminalToken: " + terminalToken;
            LOG.error(err, e);
            this.mailerService.sendAdminErrorAlert("Push data to topic " + KafkaKeyConstants.LINK_KAFKA_POS_TERMINAL_ASSIGNMENTS_TOPIC_NAME, err, e);
            return false;
        }
        return true;
    }
    
    private PosTerminalAssignments buildPosTerminalAssignments(final Integer unifiCustId, final String deviceSerialNumber, final String cardType,
        final UUID terminalToken) {
        final PosTerminalAssignments assign = new PosTerminalAssignments();
        assign.setCustomerId(unifiCustId);
        assign.setDeviceSerialNumber(deviceSerialNumber);
        assign.setCardType(cardType);
        assign.setTerminalToken(terminalToken);
        return assign;
    }
    
    
    public final void setMessageProducer(final LinkMessageProducer producer) {
        this.pciLinkMessageProducer = producer;
    }
    
    // For TESTING only
    public static final void setEmptyCardNames() {
        LinkCardTypeServiceImpl.cardNames = new ArrayList<String>();
    }
}
