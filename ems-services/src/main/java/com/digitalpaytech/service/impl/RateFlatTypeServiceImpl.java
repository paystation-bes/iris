package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RateFlatType;
import com.digitalpaytech.service.RateFlatTypeService;

@Service("rateFlatTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateFlatTypeServiceImpl implements RateFlatTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final RateFlatType findRateFlatTypeById(final Byte rateFlatTypeId) {
        return this.entityDao.get(RateFlatType.class, rateFlatTypeId);
    }
    
}
