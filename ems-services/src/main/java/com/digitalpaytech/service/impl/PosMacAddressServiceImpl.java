package com.digitalpaytech.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.domain.PosMacAddressLog;
import com.digitalpaytech.service.PosMacAddressService;

@Service("posMacAddressService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PosMacAddressServiceImpl implements PosMacAddressService {
    
    private final static Logger logger = Logger.getLogger(PosMacAddressServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public PosMacAddress findPosMacAddressByPointOfSaleId(Integer pointOfSaleId) {
        return (PosMacAddress) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosMacAddress.findPosMacAddressByPointOfSaleId", "pointOfSaleId",
                                                                                  pointOfSaleId, true);
    }
    
    @Override
    public PosMacAddress findPosMacAddressByMacAddress(String macAddress) {
        return (PosMacAddress) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("PosMacAddress.findPosMacAddressByMacAddress", "macAddress", macAddress, true);
    }
    
    @Override
    public void saveOrUpdate(PosMacAddress posMacAddress, boolean isUpdate) {
        this.entityDao.saveOrUpdate(posMacAddress);
        
        if (isUpdate) {
            PosMacAddressLog posMacAddressLog = new PosMacAddressLog();
            posMacAddressLog.setPointOfSale(posMacAddress.getPointOfSale());
            posMacAddressLog.setMacAddress(posMacAddress.getMacAddress());
            posMacAddressLog.setCreatedGmt(posMacAddress.getLastModifiedGmt());
            this.entityDao.saveOrUpdate(posMacAddressLog);
        }
    }
    
    @Override
    public void delete(PosMacAddress posMacAddress) {
        this.entityDao.delete(posMacAddress);
    }

}
