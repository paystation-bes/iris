package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationFailureType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.CustomerMigrationFailureTypeService;

@Service("customerMigrationFailureTypeService")
public class CustomerMigrationFailureTypeServiceImpl implements CustomerMigrationFailureTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<CustomerMigrationFailureType> CustomerMigrationFailureTypes;
	private Map<Byte, CustomerMigrationFailureType> CustomerMigrationFailureTypesMap;
	private Map<String, CustomerMigrationFailureType> CustomerMigrationFailureTypeNamesMap;
	
	
	@Override
	public List<CustomerMigrationFailureType> loadAll() {
		if (CustomerMigrationFailureTypes == null) {
			CustomerMigrationFailureTypes = entityService.loadAll(CustomerMigrationFailureType.class);
		}
		return CustomerMigrationFailureTypes;
	}

	@Override
	public Map<Byte, CustomerMigrationFailureType> getCustomerMigrationFailureTypesMap() {
		if (CustomerMigrationFailureTypes == null) {
			loadAll();
		}
		if (CustomerMigrationFailureTypesMap == null) {
			CustomerMigrationFailureTypesMap = new HashMap<Byte, CustomerMigrationFailureType>(CustomerMigrationFailureTypes.size());
			Iterator<CustomerMigrationFailureType> iter = CustomerMigrationFailureTypes.iterator();
			while (iter.hasNext()) {
				CustomerMigrationFailureType CustomerMigrationFailureType = iter.next();
				CustomerMigrationFailureTypesMap.put(CustomerMigrationFailureType.getId(), CustomerMigrationFailureType);
			}
		}
		return CustomerMigrationFailureTypesMap;
	}
	
	@Override
	public CustomerMigrationFailureType findCustomerMigrationFailureType(String name) {
        if (CustomerMigrationFailureTypes == null) {
            loadAll();
            
            CustomerMigrationFailureTypeNamesMap = new HashMap<String, CustomerMigrationFailureType>(CustomerMigrationFailureTypes.size());
            Iterator<CustomerMigrationFailureType> iter = CustomerMigrationFailureTypes.iterator();
            while (iter.hasNext()) {
                CustomerMigrationFailureType CustomerMigrationFailureType = iter.next();
                CustomerMigrationFailureTypeNamesMap.put(CustomerMigrationFailureType.getName().toLowerCase(), CustomerMigrationFailureType);
            }
        }
        return CustomerMigrationFailureTypeNamesMap.get(name.toLowerCase());
	}

	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	@Override
	public String getText(byte CustomerMigrationFailureTypeId) {
		if (CustomerMigrationFailureTypesMap == null)
			getCustomerMigrationFailureTypesMap();
		
		if (!CustomerMigrationFailureTypesMap.containsKey(CustomerMigrationFailureTypeId))
			return null;
		else
			return CustomerMigrationFailureTypesMap.get(CustomerMigrationFailureTypeId).getName();
	}

    @Override
    public CustomerMigrationFailureType findCustomerMigrationFailureType(byte CustomerMigrationFailureTypeId) {
        if (CustomerMigrationFailureTypesMap == null)
            getCustomerMigrationFailureTypesMap();
        
        if (!CustomerMigrationFailureTypesMap.containsKey(CustomerMigrationFailureTypeId))
            return null;
        else
            return CustomerMigrationFailureTypesMap.get(CustomerMigrationFailureTypeId);
    }
}
