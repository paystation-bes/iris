package com.digitalpaytech.service.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.domain.PurchaseTax;
import com.digitalpaytech.dto.customeradmin.EmailAddressHistoryDetail;
import com.digitalpaytech.dto.customeradmin.TaxDetail;
import com.digitalpaytech.dto.customeradmin.TaxReceiptInfo;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PaymentSmartCardService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.PurchaseTaxService;
import com.digitalpaytech.service.TaxService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TemplateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("purchaseEmailAddressService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PurchaseEmailAddressServiceImpl implements PurchaseEmailAddressService {
    
    private static Logger log = Logger.getLogger(PurchaseEmailAddressServiceImpl.class);
    
    private static final String MESSAGE_TYPE = "text/html;charset=UTF-8";
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private PaymentSmartCardService paymentSmartCardService;
    
    @Autowired
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private LicensePlateService licencePlateService;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PurchaseTaxService purchaseTaxService;
    
    @Autowired
    private TaxService taxService;
    
    @Autowired
    private MobileNumberService mobileNumberService;
    
    @Autowired
    private TemplateUtil templateUtil;
    
    @Autowired
    private ElavonViaConexService elavonViaConexService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @SuppressWarnings("unchecked")
    @Override
    public Collection<PurchaseEmailAddress> findPurchaseEmailAddress(final int numOfRows, final boolean isEvict) {
        final Query query = this.entityDao.getNamedQuery("PurchaseEmailAddress.findPurchaseEmailAddress");
        query.setMaxResults(numOfRows);
        final Collection<PurchaseEmailAddress> purchaseEmailAddresses = query.list();
        
        if (isEvict) {
            for (PurchaseEmailAddress purchaseEmailAddress : purchaseEmailAddresses) {
                this.entityDao.evict(purchaseEmailAddress);
            }
        }
        return purchaseEmailAddresses;
    }
    
    @Override
    public void sendReceipt(final PurchaseEmailAddress purchaseEmailAddress, final boolean isMigrated) {
        final Purchase purchase = this.purchaseService.findPurchaseById(purchaseEmailAddress.getPurchase().getId(), false);
        final TransactionReceiptDetails detail = getTransactionReceiptDetails(purchase);
        
        final String content = createEmailReceipt(detail);
        
        try {
            this.mailerService
                    .sendMessage(URLDecoder.decode(purchaseEmailAddress.getEmailAddress().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                 "Receipt", content, MESSAGE_TYPE);
        } catch (UnsupportedEncodingException e) {
            log.error("Unable to send email receipt to email address: " + purchaseEmailAddress.getEmailAddress().getEmail() + " with purchaseId: "
                      + purchase.getId(), e);
            e.printStackTrace();
        }
        purchaseEmailAddress.setSentEmailGMT(new Date());
        
        purchaseEmailAddress.setIsEmailSent(true);
        this.entityDao.update(purchaseEmailAddress);
    }
    
    private String createEmailReceipt(final TransactionReceiptDetails detail) {
        final Map<String, Object> ctx = new HashMap<String, Object>(2);
        ctx.put("txDtl", detail);
        
        return this.templateUtil.applyTemplate("templates/ereceipt.vm", ctx);
    }
    
    public void savePurchaseEmail(final PurchaseEmailAddress purchaseEmailAddress) {
        this.entityDao.save(purchaseEmailAddress);
    }
    
    public void saveEmailAddress(final EmailAddress emailAddress) {
        this.entityDao.save(emailAddress);
    }
    
    private Float formatToDollars(final int i) {
        final Float dollars = new BigDecimal(i).divide(new BigDecimal(StandardConstants.CONSTANT_100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
        return dollars;
    }
    
    private boolean isPaidByCashBillCoin(final Purchase purchase) {
        if (purchase.getCashPaidAmount() > 0 || purchase.getBillPaidAmount() > 0 || purchase.getCoinPaidAmount() > 0) {
            return true;
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PurchaseEmailAddress> findPurchaseEmailAddressByPurchaseId(final Long purchaseId) {
        final List<PurchaseEmailAddress> purchaseEmailAddressList =
                this.entityDao.findByNamedQueryAndNamedParam("PurchaseEmailAddress.findPurchaseEmailAddressByPurchaseId",
                                                             new String[] { "purchaseId" }, new Object[] { purchaseId }, false);
        
        return purchaseEmailAddressList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public EmailAddress findEmailById(final Integer emailAddressId) {
        final List<EmailAddress> emailAddressList =
                this.entityDao.findByNamedQueryAndNamedParam("EmailAddress.findEmailById", new String[] { "emailAddressId" },
                                                             new Object[] { emailAddressId }, false);
        
        return emailAddressList == null || emailAddressList.isEmpty() ? null : emailAddressList.get(0);
    }
    
    public final TransactionReceiptDetails getTransactionReceiptDetails(final Purchase purchase) {
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(purchase.getCustomer().getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final TransactionReceiptDetails detail = new TransactionReceiptDetails();
        
        final List<PurchaseTax> purchaseTaxList = this.purchaseTaxService.findPurchaseTaxByPurchaseId(purchase.getId());
        
        final TaxReceiptInfo taxDetails = buildTaxDetails(purchaseTaxList);
        
        detail.setTaxDetailList(taxDetails.getTaxDetails());
        detail.setOriginalAmount(formatToDollars(purchase.getOriginalAmount()));
        detail.setTotalPaid(formatToDollars(purchase.getCashPaidAmount() + purchase.getCardPaidAmount()));
        final PosServiceState posServiceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(purchase.getPointOfSale().getId());
        detail.setPaystationSetting(posServiceState.getPaystationSettingName());
        detail.setRateName(purchase.getUnifiedRate().getName());
        
        detail.setLocationName(purchase.getLocation().getName());
        detail.setPayStationName(purchase.getPointOfSale().getName());
        detail.setPayStationSerial(purchase.getPointOfSale().getSerialNumber());
        detail.setTransactionNumber(String.valueOf(purchase.getPurchaseNumber()));
        detail.setLotNumber(purchase.getPaystationSetting().getName());
        detail.setIsPANNeeded(true);
        detail.setIsRefundable(false);
        
        detail.setTransactionType(purchase.getTransactionType().getName());
        
        // Will probably need to ba changed to match pay station
        if (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE) {
            detail.setPaymentType("Cash/Card Swipe");
        } else if (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL) {
            detail.setPaymentType("Cash/Card Tap");
        } else if (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH) {
            detail.setPaymentType("Cash/Card Chip");
        } else {
            detail.setPaymentType(purchase.getPaymentType().getName());
        }
        
        final SimpleDateFormat format = new SimpleDateFormat(WebCoreConstants.DATE_FORMAT);
        final SimpleDateFormat formatNoTime = new SimpleDateFormat(WebCoreConstants.DATE_NO_TIME_FORMAT);
        final SimpleDateFormat formatOnlyTime = new SimpleDateFormat(WebCoreConstants.TIME_UI_FORMAT_TODAY);
        
        detail.setPurchasedDate(formatNoTime.format(DateUtil.changeTimeZone(purchase.getPurchaseGmt(), timeZone)));
        detail.setPurchasedTime(formatOnlyTime.format(DateUtil.changeTimeZone(purchase.getPurchaseGmt(), timeZone)));
        
        final Permit permit = this.permitService.findLatestExpiredPermitByPurchaseId(purchase.getId());
        if (permit != null) {
            detail.setExpiryDate(formatNoTime.format(DateUtil.changeTimeZone(permit.getPermitOriginalExpireGmt(), timeZone)));
            detail.setExpiryTime(formatOnlyTime.format(DateUtil.changeTimeZone(permit.getPermitOriginalExpireGmt(), timeZone)));
            
            final int permitIssueType = permit.getPermitIssueType().getId();
            detail.setIsPND(permitIssueType == WebCoreConstants.PERMIT_ISSUE_TYPE_PND ? true : false);
            detail.setIsPBL(permitIssueType == WebCoreConstants.PERMIT_ISSUE_TYPE_PBL ? true : false);
            detail.setIsPBS(permitIssueType == WebCoreConstants.PERMIT_ISSUE_TYPE_PBS ? true : false);
            
            detail.setStallNumber(permit.getSpaceNumber());
            detail.setAddTimeNumber(permit.getAddTimeNumber());
            if (permit.getLicencePlate() != null) {
                final LicencePlate licencePlate = this.licencePlateService.findById(permit.getLicencePlate().getId());
                detail.setLicensePlateNumber(licencePlate.getNumber());
            } else {
                detail.setLicensePlateNumber(WebCoreConstants.NOT_APPLICABLE);
            }
            if (purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_MONTHLY) {
                final Date startDate = DateUtil.createUTCStartOfMonthDate(permit.getPermitExpireGmt(), timeZone);
                detail.setMonthlyStartDate(format.format(DateUtil.changeTimeZone(startDate, timeZone)));
            }
            if (permit.getMobileNumber() != null) {
                detail.setMobileNumber(this.mobileNumberService.findMobileNumberById(permit.getMobileNumber().getId()).getNumber());
            }
        } else if (purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_CANCELLED
                   || purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_SC_RECHARGE) {
            // There is no Permit record for cancellation or SC Recharge, set expiry date to purchase date.
            detail.setExpiryDate(detail.getPurchasedDate());
        }
        
        detail.setChargedAmount(formatToDollars(purchase.getChargedAmount()));
        
        detail.setCashPaid(formatToDollars(purchase.getCashPaidAmount()));
        
        switch (purchase.getPaymentType().getId()) {
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                detail.setCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                detail.setValueCardPaid(formatToDollars(0));
                detail.setSmartCardPaid(formatToDollars(0));
                break;
            case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
            case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                final PaymentCard paymentCard = this.paymentCardService.findByPurchaseId(purchase.getId());
                detail.setCardType(this.messageHelper.getMessage("label.other"));
                detail.setCardNumber(WebCoreUtil.createStarredCardNumber(paymentCard.getCustomerCard().getCardNumber(), false));
                detail.setCardPaid(formatToDollars(0));
                detail.setValueCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                detail.setSmartCardPaid(formatToDollars(0));
                break;
            case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
            case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                detail.setCardPaid(formatToDollars(0));
                detail.setValueCardPaid(formatToDollars(0));
                detail.setSmartCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                break;
            default:
                break;
        }
        
        detail.setExcessPayment(formatToDollars(purchase.getExcessPaymentAmount()));
        detail.setChangeDispensed(formatToDollars(purchase.getChangeDispensedAmount()));
        detail.setRefundSlip(purchase.isIsRefundSlip());
        
        int cardRefundAmount = 0;
        if (purchase.getCardPaidAmount() > 0) {
            final Collection<ProcessorTransaction> pts = this.processorTransactionService.findProcessorTransactionByPurchaseId(purchase.getId());
            if (pts != null && !pts.isEmpty()) {
                final ProcessorTransaction pt = pts.iterator().next();
                final PaymentCard paymentCard =
                        this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(purchase.getId(), pt.getId());
                detail.setProcessorTransactionTypeId(pt.getProcessorTransactionType().getId());
                final CPSData cpsData = this.cpsDataService.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(pt.getId());
                if (cpsData != null) {
                    detail.setCvm(cpsData.getCvm());
                    detail.setAid(cpsData.getAid());
                    detail.setApl(cpsData.getApl());
                }
                if (paymentCard.getCardType().getId() == WebCoreConstants.CREDIT_CARD) {
                    detail.setIsCreditCard(true);
                    
                    if (pt.getCardType().equals(CardProcessingConstants.NAME_AMEX)) {
                        detail.setCardType(this.messageHelper.getMessage("label.americanExpress"));
                    } else {
                        detail.setCardType(pt.getCardType());
                    }
                    detail.setCardNumber(WebCoreUtil.createStarredCardNumber(String.valueOf(pt.getLast4digitsOfCardNumber()), false));
                    detail.setAuthorizationNumber(pt.getSanitizedAuthorizationNumber());
                    detail.setReferenceNumber(pt.getReferenceNumber());
                    // if a token is found for the processor transaction id then it must be an emv transaction
                    final List<CPSData> transactionCardTokens = this.cpsDataService.findListCPSDataByProcessorTransactionId(pt.getId());
                    
                    detail.setIsPANNeeded(transactionCardTokens.isEmpty());
                } else if (paymentCard.getCardType().getId() == WebCoreConstants.VALUE_CARD) {
                    detail.setCardType(this.messageHelper.getMessage("label.other"));
                    detail.setCardNumber(WebCoreUtil.createStarredCardNumber(String.valueOf(paymentCard.getCustomerCard().getCardNumber()), false));
                    detail.setAuthorizationNumber(pt.getSanitizedAuthorizationNumber());
                    detail.setReferenceNumber(pt.getReferenceNumber());
                } else if (paymentCard.getCardType().getId() == WebCoreConstants.SMART_CARD) {
                    detail.setCardType(paymentCard.getCustomerCard().getCustomerCardType().getName());
                    detail.setCardNumber(paymentCard.getCustomerCard().getCardNumber());
                } else {
                    detail.setCardType(pt.getCardType());
                }
                
                final ProcessorTransactionType processorTransactionType =
                        this.processorTransactionTypeService.findProcessorTransactionType(pt.getProcessorTransactionType().getId());
                if (processorTransactionType.isIsRefund()) {
                    final Collection<ProcessorTransaction> refundPts =
                            this.processorTransactionService.findRefundedProcessorTransaction(purchase.getId());
                    if (refundPts != null && !refundPts.isEmpty()) {
                        final ProcessorTransaction refundPt = refundPts.iterator().next();
                        final ProcessorTransactionType rptt =
                                this.processorTransactionTypeService.findProcessorTransactionType(refundPt.getProcessorTransactionType().getId());
                        detail.setStatus(rptt.getName());
                        detail.setProcessingDate(format.format(DateUtil.changeTimeZone(refundPt.getProcessingDate(), timeZone)));
                        detail.setAmountRefundedCard(formatToDollars(refundPt.getAmount()));
                        detail.setRefundAuthorizationNumber(refundPt.getAuthorizationNumber());
                        detail.setRefundReferenceNumber(refundPt.getReferenceNumber());
                        cardRefundAmount = refundPt.getAmount();
                    }
                } else {
                    if (detail.getIsCreditCard() && (pt.getMerchantAccount() != null)) {
                        final MerchantAccount ma = this.entityDao.get(MerchantAccount.class, pt.getMerchantAccount().getId());
                        if (ma != null && ma.getProcessor().getId() == CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX) {
                            final ElavonTransactionDetail elavonTransactionDetail = this.elavonViaConexService
                                    .findSaleDetail(pt.getId(), pt.getPointOfSale().getId(), pt.getMerchantAccount().getId());
                            
                            if (elavonTransactionDetail != null && elavonTransactionDetail.getTransactionSettlementStatusType()
                                    .getId() == CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID) {
                                detail.setIsRefundable(true);
                            } else {
                                detail.setIsRefundable(false);
                            }
                        } else if (this.processorTransactionService.isIncomplete(ma, pt)) {
                            detail.setIsRefundable(false);
                        } else {
                            detail.setIsRefundable(this.processorTransactionService.isRefundableTransaction(pt));
                        }
                    }
                    
                    detail.setStatus(processorTransactionType.getName());
                    detail.setProcessingDate(format.format(DateUtil.changeTimeZone(pt.getProcessingDate(), timeZone)));
                }
            } else {
                final PaymentCard paymentCard = this.paymentCardService.findByPurchaseId(purchase.getId());
                if (paymentCard != null) {
                    if (paymentCard.getCardType().getId() == WebCoreConstants.CARD_TYPE_VALUE_CARD
                        && paymentCard.getCustomerCard().getCustomerCardType().getAuthorizationType()
                                .getId() == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST
                        && purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_CANCELLED) {
                        cardRefundAmount = paymentCard.getAmount();
                        detail.setAmountRefundedCard(formatToDollars(paymentCard.getAmount()));
                    }
                }
                final PaymentSmartCard psc = this.paymentSmartCardService.findByPurchaseId(purchase);
                if (psc != null && (purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_SMART_CARD
                                    || purchase.getPaymentType().getId() == WebCoreConstants.PAYMENT_TYPE_CASH_SMART)) {
                    detail.setCardNumber(psc.getSmartCardData());
                }
            }
        }
        
        // Refund cash and total refund amount.
        if ((purchase.isIsRefundSlip() && purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_CANCELLED)
            && isPaidByCashBillCoin(purchase)) {
            
            final int cardAmountToAdd = purchase.getCardPaidAmount() - cardRefundAmount;
            final int totalRefundAmt = (purchase.getCashPaidAmount() + cardAmountToAdd) - purchase.getChargedAmount()
                                       - purchase.getChangeDispensedAmount() - purchase.getExcessPaymentAmount();
            
            detail.setAmountRefundedSlip(formatToDollars(totalRefundAmt));
        } else {
            detail.setAmountRefundedSlip(0f);
        }
        if (purchase.getTransactionType().getId().intValue() != ReportingConstants.TRANSACTION_TYPE_CANCELLED) {
            if (detail.getAmountRefundedSlip() == null) {
                detail.setAmountRefundedSlip(0f);
            }
            if (detail.getAmountRefundedCard() == null) {
                detail.setAmountRefundedCard(0f);
            }
        }
        
        boolean isTotalParking = taxDetails.getTotalParking();
        if (purchase.getCoupon() == null) {
            detail.setIsCouponOffline((Boolean) null);
            detail.setCouponNumber((String) null);
        } else {
            detail.setIsCouponOffline(purchase.getCoupon().isIsOffline());
            if (!purchase.getCoupon().isIsOffline()) {
                detail.setCouponNumber(purchase.getCoupon().getCoupon());
            } else if (purchase.getCoupon().getCoupon().endsWith("+")) {
                detail.setCouponNumber(purchase.getCoupon().getCoupon().substring(1, purchase.getCoupon().getCoupon().length() - 1));
            } else {
                detail.setCouponNumber(purchase.getCoupon().getCoupon().substring(1));
            }
            isTotalParking = true;
        }
        if (isTotalParking) {
            detail.setTotalParking(formatToDollars(purchase.getChargedAmount() - taxDetails.getTaxTotal()));
        }
        return detail;
    }
    
    private TaxReceiptInfo buildTaxDetails(final List<PurchaseTax> purchaseTaxList) {
        short taxTotal = 0;
        boolean isTotalParking = false;
        final List<TaxDetail> variableRateDetails = new ArrayList<>();
        final List<TaxDetail> flatFeeDetail = new ArrayList<>();
        
        for (PurchaseTax purchaseTax : purchaseTaxList) {
            final String rateName = this.taxService.findTaxById(purchaseTax.getTax().getId()).getName();
            
            final TaxDetail taxDetail = new TaxDetail();
            taxDetail.setName(rateName);
            taxDetail.setAmount(String.format(StandardConstants.FORMAT_TWO_DECIMALS, formatToDollars(purchaseTax.getTaxAmount())));
            
            if (purchaseTax.getTaxRate() != 0) {
                taxDetail.setRate(String.format(StandardConstants.FORMAT_TWO_DECIMALS, formatToDollars(purchaseTax.getTaxRate())));
                variableRateDetails.add(taxDetail);
            } else {
                flatFeeDetail.add(taxDetail);
            }
            
            taxTotal += purchaseTax.getTaxAmount();
            isTotalParking = true;
        }
        
        // Ensure that flat fee taxes are the first tax in the list
        final List<TaxDetail> taxDetails = new ArrayList<>();
        taxDetails.addAll(flatFeeDetail);
        taxDetails.addAll(variableRateDetails);
        
        return new TaxReceiptInfo(taxDetails, taxTotal, isTotalParking);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<EmailAddressHistoryDetail> findEmailAddressHistoryDetail(final Long purchaseId) {
        final List<PurchaseEmailAddress> purchaseEmailAddressList =
                this.entityDao.findByNamedQueryAndNamedParam("PurchaseEmailAddress.findPurchaseEmailAddressByPurchaseId",
                                                             new String[] { "purchaseId" }, new Object[] { purchaseId }, false);
        
        final List<EmailAddressHistoryDetail> emailAddressHistoryDetailList = new ArrayList<EmailAddressHistoryDetail>();
        for (PurchaseEmailAddress purchaseEmailAddress : purchaseEmailAddressList) {
            final EmailAddressHistoryDetail emailAddressHistoryDetail =
                    new EmailAddressHistoryDetail(purchaseEmailAddress.getEmailAddress().getEmail(),
                            purchaseEmailAddress.getSentEmailGMT() == null ? "" : purchaseEmailAddress.getSentEmailGMT().toString(),
                            purchaseEmailAddress.isIsEmailSent());
            emailAddressHistoryDetailList.add(emailAddressHistoryDetail);
        }
        
        return emailAddressHistoryDetailList;
    }
}
