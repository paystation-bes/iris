package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.EventDeviceTypeService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;

@Service("eventDeviceTypeService")
public class EventDeviceTypeServiceImpl implements EventDeviceTypeService {
    
    @Autowired
    private EntityService entityService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    private List<EventDeviceType> eventDeviceTypes;
    private Map<Integer, EventDeviceType> eventDeviceTypesMap;
    private Map<String, EventDeviceType> eventDeviceTypeNamesMap;
    
    public void setMessageHelper(MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    @Override
    public List<EventDeviceType> loadAll() {
        if (eventDeviceTypes == null) {
            eventDeviceTypes = entityService.loadAll(EventDeviceType.class);
        }
        return eventDeviceTypes;
    }
    
    @Override
    public Map<Integer, EventDeviceType> getEventDeviceTypesMap() {
        if (eventDeviceTypes == null) {
            loadAll();
        }
        if (eventDeviceTypesMap == null) {
            eventDeviceTypesMap = new HashMap<Integer, EventDeviceType>(eventDeviceTypes.size());
            Iterator<EventDeviceType> iter = eventDeviceTypes.iterator();
            while (iter.hasNext()) {
                EventDeviceType eventDeviceType = iter.next();
                eventDeviceTypesMap.put(eventDeviceType.getId(), eventDeviceType);
            }
        }
        return eventDeviceTypesMap;
    }
    
    @Override
    public EventDeviceType findEventDeviceType(String name) {
        if (eventDeviceTypes == null) {
            loadAll();
            
            eventDeviceTypeNamesMap = new HashMap<String, EventDeviceType>(eventDeviceTypes.size());
            Iterator<EventDeviceType> iter = eventDeviceTypes.iterator();
            while (iter.hasNext()) {
                EventDeviceType eventDeviceType = iter.next();
                eventDeviceTypeNamesMap.put(messageHelper.getMessage(eventDeviceType.getName()).toLowerCase(), eventDeviceType);
            }
        }
        return eventDeviceTypeNamesMap.get(name.toLowerCase());
    }
    
    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }
    
    @Override
    public String getText(int eventDeviceId) {
        if (eventDeviceTypesMap == null)
            getEventDeviceTypesMap();
        
        if (!eventDeviceTypesMap.containsKey(eventDeviceId))
            return null;
        else
            return messageHelper.getMessage(eventDeviceTypesMap.get(eventDeviceId).getName());
    }
    
    @Override
    public String getTextNoSpace(int eventDeviceId) {
        if (eventDeviceTypesMap == null)
            getEventDeviceTypesMap();
        
        if (!eventDeviceTypesMap.containsKey(eventDeviceId))
            return null;
        else
            return WebCoreConstants.EMPTY_STRING.equals(eventDeviceTypesMap.get(eventDeviceId).getNameNoSpace()) ? "Paystation" : eventDeviceTypesMap
                    .get(eventDeviceId).getNameNoSpace();
    }
    
}
