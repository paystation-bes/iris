package com.digitalpaytech.service.impl;

import java.util.Set;
import java.util.HashSet;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.service.ExtensibleRateDayOfWeekService;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.dao.EntityDao;

@Service("extensibleRateDayOfWeekService")
@Transactional(propagation=Propagation.REQUIRED)
public class ExtensibleRateDayOfWeekServiceImpl implements ExtensibleRateDayOfWeekService {

    @Autowired
    private EntityDao entityDao;
    
    @Override
    public Set<ExtensibleRateDayOfWeek> findByExtensibleRateId(Integer extensibleRateId) {
        return new HashSet<ExtensibleRateDayOfWeek>(entityDao.findByNamedQueryAndNamedParam("ExtensibleRateDayOfWeek.findByExtensibleRateId", "extensibleRateId", extensibleRateId));
    }

    @Override
    public void deleteByExtensibleRateId(Integer extensibleRateId) {
        entityDao.updateByNamedQuery("ExtensibleRateDayOfWeek.deleteByExtensibleRateId", new String[] { "extensibleRateId" }, new Object[] { extensibleRateId });
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
