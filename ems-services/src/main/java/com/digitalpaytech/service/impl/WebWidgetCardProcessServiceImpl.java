package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetCardProcessingService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetCardProcessServiceImpl implements WebWidgetService {
    private static Logger log = Logger.getLogger(WebWidgetCardProcessServiceImpl.class);
    
    private static final Map<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(20);
    private static final Map<Integer, String> TIER_TYPES_ALIASES_MAP = new HashMap<Integer, String>(4);
    static {
        // Only time types are allowed to have more than one field under the same type.
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "ctd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "ctd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, " rp.RouteId, ctd.PointOfSaleId ");
        
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "LocationId");
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "PurchaseLocationId");
    }
    
    private static Map<String, Integer> cardProcessMethodTypesMap;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final WidgetData getWidgetData(final Widget widget, final UserAccount userAccount) {
        
        final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        final int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        final int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        
        /*
         * Define table depth type, <Hour | Day | Month>
         */
        int tableDepthType = 0;
        if (tier1Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier1Id == WidgetConstants.TIER_TYPE_LOCATION
            || tier2Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier2Id == WidgetConstants.TIER_TYPE_LOCATION
            || tier3Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier3Id == WidgetConstants.TIER_TYPE_LOCATION
            || tier1Id == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE || tier2Id == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE
            || tier3Id == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE) {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_DETAIL;
        } else {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_TOTAL;
        }
        
        /*
         * Define table range type, <YESTERDAY | YEAR_TO_DATE | LAST_24HOURS...>
         */
        final int timeRange = widget.getWidgetRangeType().getId();
        int tableRangeType = 0;
        switch (timeRange) {
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                if (tier1Id == WidgetConstants.TIER_TYPE_HOUR) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                }
                break;
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
            case WidgetConstants.RANGE_TYPE_TODAY:
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                break;
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
            case WidgetConstants.RANGE_TYPE_THIS_MONTH:
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                break;
            default:
                log.error(timeRange + ", this range id is NOT valid");
        }
        
        /*
         * Define table type, <HOUR | DAY | MONTH>
         * Sub JOIN Temp Table "wData"
         */
        String tableType = null;
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_DAY) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_DAY_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_MONTH) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING;
        }
        
        /*
         * Prepare subsetMap
         */
        final StringBuilder labelBdr = new StringBuilder();
        labelBdr.append(":cardProcessMethodTypeIds");
        final Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        subsetMap.put(new Integer(WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE), labelBdr);
        
        String column;
        if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV) {
            column = "Amount";
        } else {
            column = "Count";
        }
        
        /*
         * Define main SELECT expression. "wData" SELECT expression & queryStr ends at 'wLabel.Tier1TypeHidden AS Tier1TypeHidden '.
         */
        final StringBuilder queryStr = new StringBuilder();
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        queryStr.append(", IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
        /* Main FROM expression */
        queryStr.append("FROM (");
        queryStr.append("SELECT ctt.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, TIER_TYPES_ALIASES_MAP, false);
        
        queryStr.append(", ctt.CardProcessMethodTypeId");
        
        if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_DETAIL) {
            queryStr.append(", SUM(ctd.Total");
        } else {
            queryStr.append(", SUM(ctt.Total");
        }
        queryStr.append(column).append(") AS WidgetMetricValue ");
        
        /* "wData" FROM expression */
        queryStr.append("FROM CardTransactionTotal");
        queryStr.append(tableType);
        queryStr.append(" ctt ");
        if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_DETAIL) {
            queryStr.append("INNER JOIN CardTransactionDetail");
            queryStr.append(tableType);
            queryStr.append(" ctd ON ctd.CardTransactionTotal");
            queryStr.append(tableType);
            queryStr.append("Id = ctt.Id ");
        }
        
        /* Time table */
        queryStr.append("INNER JOIN Time t ON (ctt.TimeIdLocal = t.Id ");
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(), tier1Id,
                                                   false, true);
        queryStr.append(") ");
        
        if (tier1Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier2Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
            queryStr.append("INNER JOIN Location pl ON ctd.PurchaseLocationId = pl.Id  ");
        }
        
        if (tier2Id == WidgetConstants.TIER_TYPE_ROUTE) {
            queryStr.append("INNER JOIN RoutePOS rp ON ctd.PointOfSaleId = rp.PointOfSaleId  ");
        }
        
        /* "wData" WHERE expression to speed things up */
        queryStr.append("WHERE ctt.CustomerId IN ( :customerId ) AND ");
        queryStr.append("ctt.CardProcessMethodTypeId IN (:cardProcessMethodTypeIds) ");
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY ctt.CustomerId, ");
        queryStr.append("ctt.CardProcessMethodTypeId ");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, false, null, false);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        final String queryLimit = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                             EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = this.entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(this.webWidgetHelperService, widget, query, userAccount, false);
        query.setParameterList("cardProcessMethodTypeIds", getSettledTypeIds(widget.getWidgetSubsetMembers()));
        
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        if (log.isDebugEnabled()) {
            log.debug("---------->\r\n\r\n" + query.getQueryString());
        }
        
        @SuppressWarnings("unchecked")
        final List<WidgetMetricInfo> rawWidgetData = query.list();
        widgetData = this.webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        
        return widgetData;
    }
    
    private List<Integer> getSettledTypeIds(final List<WidgetSubsetMember> widgetSubsetMembers) {
        // Lazy-loading
        if (cardProcessMethodTypesMap == null || cardProcessMethodTypesMap.isEmpty()) {
            @SuppressWarnings("unchecked")
            final List<CardProcessMethodType> cardProcessMethodTypes = this.entityDao
                    .findByNamedQuery("CardProcessMethodType.findSettledTypes", true);
            CardProcessMethodType type;
            cardProcessMethodTypesMap = new HashMap<String, Integer>(cardProcessMethodTypes.size());
            final Iterator<CardProcessMethodType> iter = cardProcessMethodTypes.iterator();
            while (iter.hasNext()) {
                type = iter.next();
                cardProcessMethodTypesMap.put(type.getName().toUpperCase(), Integer.valueOf(type.getId()));
            }
        }
        
        final List<Integer> ids = new ArrayList<Integer>();
        if (widgetSubsetMembers == null || widgetSubsetMembers.isEmpty()) {
            ids.addAll(cardProcessMethodTypesMap.values());
        } else {
            WidgetSubsetMember subMem;
            final Iterator<WidgetSubsetMember> iterSub = widgetSubsetMembers.iterator();
            while (iterSub.hasNext()) {
                subMem = iterSub.next();
                final Integer id = cardProcessMethodTypesMap.get(subMem.getMemberName().toUpperCase());
                if (id != null) {
                    ids.add(id);
                }
            }
            if (ids.isEmpty()) {
                ids.addAll(cardProcessMethodTypesMap.values());
            }
        }
        return ids;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setWebWidgetHelperService(final WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
}
