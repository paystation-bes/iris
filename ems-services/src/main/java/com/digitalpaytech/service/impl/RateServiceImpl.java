package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.service.RateService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("rateService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateServiceImpl implements RateService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final Rate findRateById(final Integer rateId) {
        return (Rate) this.entityDao.findUniqueByNamedQueryAndNamedParam("Rate.findRateById", "rateId", rateId);
    }
    
    @Override
    public final Rate findRateByCustomerIdAndName(final Integer customerId, final Byte rateTypeId, final String name) {
        List<Rate> rateList = this.entityDao.findByNamedQueryAndNamedParam("Rate.findRateByCustomerIdAndRateTypeIdAndName", new String[] { "customerId",
                "rateTypeId", "name" }, new Object[] { customerId, rateTypeId, name }, true);
        
        if (rateList != null && !rateList.isEmpty()) {
            return rateList.get(0);
        } else {
            return null;
        }
        
    }
    
    @Override
    public final List<Rate> findRatesByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Rate.findRatesByCustomerId", "customerId", customerId);
    }
    
    @Override
    public final List<Rate> findRatesByCustomerIdAndRateTypeId(final Integer customerId, final Byte rateTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Rate.findRatesByCustomerIdAndRateTypeId", new String[] { "customerId", "rateTypeId" },
                                                            new Object[] { customerId, rateTypeId });
    }
    
    @Override
    public final List<Rate> findPagedRateList(final Integer customerId, final Byte rateTypeId, final Date maxUpdateTime, final int pageNumber) {
        List<Rate> rateList = null;
        if (rateTypeId == null) {
            rateList = this.entityDao.findPageResultByNamedQuery("Rate.findRatesByCustomerId", new String[] { "customerId", "maxUpdateTime" }, new Object[] {
                    customerId, maxUpdateTime }, true, pageNumber, WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        } else {
            rateList = this.entityDao.findPageResultByNamedQuery("Rate.findRatesByCustomerIdAndRateTypeId", new String[] { "customerId", "rateTypeId",
                    "maxUpdateTime" }, new Object[] { customerId, rateTypeId, maxUpdateTime }, true, pageNumber, WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        }
        return rateList;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateRate(final Rate rate) {
        this.entityDao.saveOrUpdate(rate);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteRate(final Rate rate, final Integer userAccountId) {
        final Date currentTime = new Date();
        
        rate.setLastModifiedGmt(currentTime);
        rate.setLastModifiedByUserId(userAccountId);
        rate.setIsDeleted(true);
        
        final List<RateRateProfile> rateRateProfileList = this.entityDao.findByNamedQueryAndNamedParam("RateRateProfile.findActiveRatesByRateId", "rateId",
                                                                                                       rate.getId());
        for (RateRateProfile rateRateProfile : rateRateProfileList) {
            RateProfile rateProfile = rateRateProfile.getRateProfile();
            if (rateProfile.isIsPublished()) {
                rateProfile.setIsPublished(false);
                rateProfile.setLastModifiedGmt(currentTime);
                rateProfile.setLastModifiedByUserId(userAccountId);
                this.entityDao.saveOrUpdate(rateProfile);
            }
            rateRateProfile.setIsDeleted(true);
            rateRateProfile.setLastModifiedGmt(currentTime);
            rateRateProfile.setLastModifiedByUserId(userAccountId);
            this.entityDao.saveOrUpdate(rateRateProfile);
        }
        this.entityDao.saveOrUpdate(rate);
    }
    
    @Override
    public final List<Rate> searchRates(final Integer customerId, final String keyword) {
        final String keywordString = new StringBuilder("%").append(keyword).append("%").toString();
        
        return entityDao.findByNamedQueryAndNamedParam("Rate.findAllRatesByNameKeywordAndCustomerId", new String[] { "customerId", "keyword" }, new Object[] {
                customerId, keywordString });
        
    }
    
    @Override
    public final int findRatePage(final Integer rateId, final Integer customerId, final Byte rateTypeId, final Date maxUpdateTime) {
        
        int result = -1;
        
        final Rate endRate = this.entityDao.get(Rate.class, rateId);
        double recordNumber = 0;
        if (rateTypeId == null) {
            recordNumber = ((Number) this.entityDao.findUniqueByNamedQueryAndNamedParam("Rate.countRatesByCustomerId", new String[] { "customerId", "name",
                    "maxUpdateTime" }, new Object[] { customerId, endRate.getName(), maxUpdateTime }, true)).doubleValue();
        } else {
            recordNumber = ((Number) this.entityDao.findUniqueByNamedQueryAndNamedParam("Rate.countRatesByCustomerIdAndRateTypeId", new String[] {
                    "customerId", "rateTypeId", "name", "maxUpdateTime" }, new Object[] { customerId, rateTypeId, endRate.getName(), maxUpdateTime }, true))
                    .doubleValue();
        }
        
        if (recordNumber > 0) {
            result = (int) Math.ceil(recordNumber / WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        }
        
        return result;
        
    }
}
