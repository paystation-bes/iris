package com.digitalpaytech.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.client.util.ServiceDiscovery;
import com.digitalpaytech.dto.EncryptionInfo;
import com.digitalpaytech.dto.mobile.rest.MobileCollectCollectedQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileCollectStatusQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileCollectSummaryQueryParamList;
import com.digitalpaytech.dto.mobile.rest.MobileLoginParamList;
import com.digitalpaytech.dto.mobile.rest.MobileLogoutParamList;
import com.digitalpaytech.dto.rest.RESTPermitQueryParamList;
import com.digitalpaytech.dto.rest.RESTQueryParamList;
import com.digitalpaytech.dto.rest.RESTRegionQueryParamList;
import com.digitalpaytech.dto.rest.RESTTokenQueryParamList;
import com.digitalpaytech.dto.rest.RESTCouponQueryParamList;
import com.digitalpaytech.dto.rest.paystation.PaystationQueryParamList;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.util.Crypto;
import com.digitalpaytech.util.CryptoUtil;
import com.digitalpaytech.util.MobileConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.RestCoreConstants;

@Service("encryptionService")
public class EncryptionServiceImpl implements EncryptionService {
    private static final Logger LOG = Logger.getLogger(EncryptionServiceImpl.class);
    
    private static final String OPERATION_MODE_BC = "Development - No HSM services provided";
    private static final String OPERATION_MODE_HSM = "Production";
    private static final String OPERATION_MODE_CRYPTO_SERVER = "Crypto Server";
    
    private static final String PROVIDER_BC = "Bouncy Castle";
    private static final String PROVIDER_HSM = "nCipher";
    private static final String PROVIDER_EUREKA_VIP = "cryptoserver.ribbon.DeploymentContextBasedVipAddresses";
    
    private static final String KEY_PRESENT_YES = "Yes";
    private static final String KEY_PRESENT_NO = "No";
    private static final String N_A = "N/A";
    private static String keyPresent = null;
    private static String cardSerialNumber = null;
    private static EncryptionInfo encryptionInfo = null;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private RestPropertyService restPropertyService;
    
    @Autowired
    private ServiceDiscovery eurekaClient;
    
    private static Mac mac = null;
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setCryptoService(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public void setRestPropertyService(RestPropertyService restPropertyService) {
        this.restPropertyService = restPropertyService;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpioneer.appservice.encryption.EncryptionAppService#
     * isForce2LicensePage()
     */
    public boolean isForce2LicensePage() {
        boolean forced2License = true;
        String mode = CryptoUtil.getOperationMode();
        if (mode != null && mode.trim().equals(EncryptionService.PRODUCTION)) {
            forced2License = !this.isHSMInstalled();
        } else if (mode != null && mode.trim().equals(EncryptionService.DEVELOPMENT)) {
            forced2License = false;
        }
        LOG.debug("+++ encrypt mode: " + mode + ", forced2License: " + forced2License);
        return forced2License;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpioneer.appservice.encryption.EncryptionAppService#
     * getEncryptionInfo()
     */
    public EncryptionInfo getEncryptionInfo() {
        if (encryptionInfo == null) {
            String mode = CryptoUtil.getOperationMode();
            if (mode != null) {
                mode = mode.trim();
                if (mode.equals(EncryptionService.PRODUCTION)) {
                    encryptionInfo = new EncryptionInfo();
                    if (isHSMInstalled()) {
                        encryptionInfo.setOperationMode(OPERATION_MODE_HSM);
                        encryptionInfo.setKeyPresent(getKeyPresentInfo());
                        encryptionInfo.setProvider(PROVIDER_HSM);
                        encryptionInfo.setCardSerialNumber(getCardSerialNumber());
                    } else {
                        encryptionInfo.setOperationMode("");
                    }
                } else if (mode.equals(EncryptionService.CRYPTO_SERVER)) {
                    encryptionInfo = new EncryptionInfo();
                    encryptionInfo.setOperationMode(OPERATION_MODE_CRYPTO_SERVER);
                    encryptionInfo.setKeyPresent(getKeyPresentInfo());
                    //                    encryptionInfo.setProvider(this.eurekaClient.getConfiguration().getString(PROVIDER_EUREKA_VIP));
                }
            }
            
            // Fallback to BouncyCastle
            if (encryptionInfo == null) {
                encryptionInfo = new EncryptionInfo();
                encryptionInfo.setOperationMode(OPERATION_MODE_BC);
                encryptionInfo.setKeyPresent(getKeyPresentInfo());
                encryptionInfo.setProvider(PROVIDER_BC);
                encryptionInfo.setCardSerialNumber(N_A);
            }
        }
        LOG.info("---------------------------------------");
        LOG.info("Operation Mode: " + (encryptionInfo.getOperationMode() != "" ? encryptionInfo.getOperationMode() : EncryptionService.PRODUCTION)
                 + ", HSM installed: " + isHSMInstalled() + ", Keys Present: " + encryptionInfo.getKeyPresent() + ", Provider: "
                 + encryptionInfo.getProvider() + ", Serial Number: " + encryptionInfo.getCardSerialNumber());
        LOG.info("---------------------------------------");
        return encryptionInfo;
    }
    
    public boolean isHSMInstalled() {
        return CryptoUtil.isHSMMode();
    }
    
    private String getKeyPresentInfo() {
        if (keyPresent == null) {
            try {
                final String currExtKey = this.cryptoService.currentExternalKey();
                final RSAKeyInfo currentExtKey = this.cryptoService.retrieveKeyInfo(currExtKey);
                if (currentExtKey == null || currentExtKey.getExpiryTime().before(new Date())) {
                    keyPresent = KEY_PRESENT_NO;
                    LOG.debug("!!! key expired !!!");
                } else {
                    LOG.debug("!!! current key: " + currExtKey);
                    Crypto crypto = CryptoUtil.getCreditCardCrypto();
                    if (crypto == null) {
                        keyPresent = KEY_PRESENT_NO;
                    } else if (crypto.isExistKey(currExtKey)) {
                        keyPresent = KEY_PRESENT_YES;
                    } else {
                        keyPresent = KEY_PRESENT_NO;
                    }
                }
            } catch (CryptoException e) {
                LOG.error("!!! " + e.getMessage());
                keyPresent = KEY_PRESENT_NO;
            }
        }
        
        return keyPresent;
    }
    
    private String getCardSerialNumber() {
        if (cardSerialNumber == null) {
            final String hsmDir =
                    this.emsPropertiesService.getPropertyValue(EmsPropertiesService.HSM_DIR, EmsPropertiesService.DEFAULT_HSM_DIRECTORY, true);
            try {
                final Process p = Runtime.getRuntime()
                        .exec(hsmDir + System.getProperty("file.separator") + "bin" + System.getProperty("file.separator") + "enquiry --module=1");
                final BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = null;
                while ((line = stdInput.readLine()) != null) {
                    if (line.indexOf("serial number") != -1) {
                        final String[] tmp = line.trim().split(" ");
                        final String model = tmp[tmp.length - 1];
                        LOG.debug("Card model: " + model);
                        cardSerialNumber = model;
                        break;
                    }
                }
            } catch (IOException e) {
                LOG.error("getCardSerialNumber error: ", e);
                cardSerialNumber = null;
            }
        }
        
        return cardSerialNumber;
    }
    
    @PostConstruct
    public void initEncryptionMode() {
        try {
            CryptoUtil.initEncryptionMode(emsPropertiesService);
            getEncryptionInfo();
            if (needShutdownServer() || !dptSigningKeyStoreExist()) {
                LOG.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                LOG.warn("!!! Critical Error - HSM not correctly installed in Production Mode !!!");
                LOG.warn("!!!                        Shutting down IRIS                       !!!");
                LOG.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
                System.exit(-1);
            }
        } catch (CryptoException e) {
            LOG.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            LOG.error("!!! Can not init Encryption Mode, tomcat will be shutdown  !!!");
            LOG.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.exit(-1);
        }
    }
    
    /**
     * Check if production 'dptsigning.keystore' file exist.
     * 
     * @return boolean false if dptsigning.keystore doesn't exist
     */
    private boolean dptSigningKeyStoreExist() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CRYPTO_DIRECTORY));
        bdr.append("/dptsigning.keystore");
        final File f = new File(bdr.toString());
        if (!f.exists() && isHSMInstalled()) {
            return false;
        }
        return true;
    }
    
    private boolean needShutdownServer() {
        if (!encryptionInfo.getOperationMode().equals(OPERATION_MODE_HSM) || isHSMInstalled()) {
            return false;
        } else {
            return true;
        }
    }
    
    public String createHMACSecretKey() {
        String encodedKey = null;
        try {
            final KeyGenerator kg = KeyGenerator.getInstance(EncryptionService.HMAC_SHA256_ALGORITHM);
            final SecretKey secretKey = kg.generateKey();
            final byte[] keyBytes = secretKey.getEncoded();
            encodedKey = new String(Base64.encodeBase64(keyBytes), EncryptionService.HMAC_SHA256_CHARSET);
            if (LOG.isDebugEnabled()) {
                Provider provider = kg.getProvider();
                LOG.debug("+++ HMACSecretKey provider: " + provider.toString());
                LOG.debug("+++ encoded secretKey: " + encodedKey);
                LOG.debug("+++ encoded secretKey size: " + encodedKey.length());
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOG.error("+++ createHMACSecretKey error", e);
        }
        
        return encodedKey;
    }
    
    public boolean verifyHMACSignature(String hostName, String httpMethod, String passInSignature, Object queryParamList, String xmlString,
        String encodedKey) {
        try {
            String signData = buildSignatureString(hostName, httpMethod, queryParamList, xmlString);
            if (signData == null) {
                return false;
            }
            byte[] key = encodedKey.getBytes(EncryptionService.HMAC_SHA256_CHARSET);
            LOG.debug("+++ encodedKey           : " + encodedKey);
            byte[] decodedKey = Base64.decodeBase64(key);
            SecretKey secretKey = new SecretKeySpec(decodedKey, EncryptionService.HMAC_SHA256_ALGORITHM);
            mac = getMac();
            mac.init(secretKey);
            byte[] result = mac.doFinal(signData.getBytes(EncryptionService.HMAC_SHA256_CHARSET));
            String calculatedSignature = new String(Base64.encodeBase64(result), EncryptionService.HMAC_SHA256_CHARSET);
            boolean isSame = calculatedSignature.equals(passInSignature);
            LOG.debug("+++ original Signature           : " + passInSignature);
            LOG.debug("+++ calculatedSignature Signature: " + calculatedSignature);
            LOG.info("+++ HMACSignature is same? " + isSame + " +++");
            return isSame;
        } catch (InvalidKeyException e) {
            LOG.error("+++ InvalidKeyException error", e);
        } catch (UnsupportedEncodingException e) {
            LOG.error("+++ UnsupportedEncodingException error", e);
        } catch (IllegalStateException e) {
            LOG.error("+++ IllegalStateException error", e);
        }
        return false;
    }
    
    private String buildSignatureString(String hostName, String httpMethod, Object queryParamList, String xmlInput)
        throws UnsupportedEncodingException {
        String SLASH = "/";
        StringBuilder sb = new StringBuilder();
        sb.append(httpMethod);
        sb.append(hostName);
        
        if (queryParamList instanceof RESTTokenQueryParamList) {
            sb.append(this.restPropertyService.getPropertyValue(RestPropertyService.REST_TOKEN_URI, RestPropertyService.REST_TOKEN_URI_DEFAULT));
            sb.append(((RESTQueryParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof RESTPermitQueryParamList) {
            sb.append(this.restPropertyService.getPropertyValue(RestPropertyService.REST_PERMIT_URI, RestPropertyService.REST_PERMIT_URI_DEFAULT));
            sb.append(((RESTQueryParamList) queryParamList).getFormatParamString());
            if (!httpMethod.equals(RestCoreConstants.HTTP_METHOD_GET)) {
                sb.append(xmlInput.trim());
            }
        } else if (queryParamList instanceof RESTRegionQueryParamList) {
            sb.append(this.restPropertyService.getPropertyValue(RestPropertyService.REST_REGION_URI, RestPropertyService.REST_REGION_URI_DEFAULT));
            if (httpMethod.equals(RestCoreConstants.HTTP_METHOD_GET)) {
                sb.append(((RESTQueryParamList) queryParamList).getFormatParamString());
            } else {
                LOG.error("++++ not recognized region HTTP Method +++");
                return null;
            }
        } else if (queryParamList instanceof RESTCouponQueryParamList) {
            sb.append(this.restPropertyService.getPropertyValue(RestPropertyService.REST_COUPON_URI, RestPropertyService.REST_COUPONS_URI_DEFAULT));
            
            String couponCode = ((RESTCouponQueryParamList) queryParamList).getCouponCode();
            if (StringUtils.isNotBlank(couponCode) && couponCode.startsWith(SLASH)) {
                sb.append(couponCode);
            } else if (StringUtils.isNotBlank(couponCode) && !couponCode.startsWith(SLASH)) {
                sb.append(SLASH).append(couponCode);
            }
            sb.append(((RESTQueryParamList) queryParamList).getFormatParamString());
            if ((!httpMethod.equalsIgnoreCase(RestCoreConstants.HTTP_METHOD_GET)
                 && !httpMethod.equalsIgnoreCase(RestCoreConstants.HTTP_METHOD_DELETE))
                || (httpMethod.equalsIgnoreCase(RestCoreConstants.HTTP_METHOD_DELETE) && StringUtils.isNotBlank(xmlInput))) {
                sb.append(xmlInput.trim().replaceAll("[\r\n]+", ""));
            }
        } else if (queryParamList instanceof MobileLoginParamList) {
            sb.append(MobileConstants.DEFAULT_MOBILE_LOGIN_URI);
            sb.append(((MobileLoginParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof MobileCollectStatusQueryParamList) {
            sb.append(MobileConstants.DEFAULT_MOBILE_COLLECT_STATUS_URI);
            sb.append(((MobileCollectStatusQueryParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof MobileCollectSummaryQueryParamList) {
            sb.append(MobileConstants.DEFAULT_MOBILE_COLLECT_SUMMARY_URI);
            sb.append(((MobileCollectSummaryQueryParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof MobileLogoutParamList) {
            sb.append(MobileConstants.DEFAULT_MOBILE_LOGOUT_URI);
            sb.append(((MobileLogoutParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof MobileCollectCollectedQueryParamList) {
            sb.append(MobileConstants.DEFAULT_MOBILE_COLLECT_COLLECTED_URI);
            sb.append(((MobileCollectCollectedQueryParamList) queryParamList).getFormatParamString());
        } else if (queryParamList instanceof PaystationQueryParamList) {
            if (xmlInput.indexOf("<Token ") >= 0) {
                sb.append(PaystationConstants.PAYSTATION_PATH_TOKEN_URI);
            } else if (xmlInput.indexOf("<Transaction ") >= 0) {
                sb.append(PaystationConstants.PAYSTATION_PATH_TRANSACTION_URI);
            } else if (xmlInput.indexOf("<CancelTransaction ") >= 0) {
                sb.append(PaystationConstants.PAYSTATION_PATH_CANCELTRANSACTION_URI);
            } else if (xmlInput.indexOf("<HeartBeat ") >= 0) {
                sb.append(PaystationConstants.PAYSTATION_PATH_HEARTBEAT_URI);
            }
            sb.append(((PaystationQueryParamList) queryParamList).getFormatParamString());
            sb.append(xmlInput.trim());
        } else {
            LOG.error("++++ not recognized parameter list +++");
            return null;
        }
        
        LOG.debug("++++ data to sign:\n" + sb.toString());
        return sb.toString();
    }
    
    private Mac getMac() {
        Mac mc = null;
        if (mac == null) {
            try {
                mc = Mac.getInstance(EncryptionService.HMAC_SHA256_ALGORITHM);
                return mc;
            } catch (NoSuchAlgorithmException e) {
                LOG.error("+++ getMac error", e);
            }
        }
        return mac;
        
    }
}
