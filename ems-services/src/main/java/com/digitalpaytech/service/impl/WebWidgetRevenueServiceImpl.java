package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.PropertyAccessException;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetRevenueService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetRevenueServiceImpl implements WebWidgetService {
    
    public static final HashMap<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(20);
    public static final HashMap<Integer, String> TIER_TYPES_ALIASES_MAP = new HashMap<Integer, String>(4);
    static {
        // Only time types are allowed to have more than one field under the same type.
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, "rp.RouteId, pppd.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PAYSTATION, "pppd.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "pppd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "pppd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_REVENUE_TYPE, "pppr.RevenueTypeId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_TRANSACTION_TYPE, "pppd.TransactionTypeId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_RATE, "pppd.UnifiedRateId ");
        
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "LocationId");
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "LocationId");
    }
    
    private static Logger LOG = Logger.getLogger(WebWidgetService.class);
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount) {
        
        StringBuilder queryStr = new StringBuilder();
        
        String queryLimit = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                  EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        int tableDepthType = 0;
        
        if (tier1Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE || tier2Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE
            || tier3Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_REVENUE;
        } else if (tier1Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier1Id == WidgetConstants.TIER_TYPE_LOCATION
                   || tier1Id == WidgetConstants.TIER_TYPE_ROUTE || tier1Id == WidgetConstants.TIER_TYPE_PAYSTATION
                   || tier1Id == WidgetConstants.TIER_TYPE_RATE || tier1Id == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE
                   || tier2Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier2Id == WidgetConstants.TIER_TYPE_LOCATION
                   || tier2Id == WidgetConstants.TIER_TYPE_ROUTE || tier2Id == WidgetConstants.TIER_TYPE_RATE
                   || tier2Id == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE || tier3Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION
                   || tier3Id == WidgetConstants.TIER_TYPE_LOCATION || tier3Id == WidgetConstants.TIER_TYPE_ROUTE
                   || tier3Id == WidgetConstants.TIER_TYPE_RATE || tier3Id == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_DETAIL;
        } else {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_TOTAL;
        }
        
        int timeRange = widget.getWidgetRangeType().getId();
        int tableRangeType = 0;
        switch (timeRange) {
            case WidgetConstants.RANGE_TYPE_TODAY:
                if (tier1Id == WidgetConstants.TIER_TYPE_HOUR) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                }
                break;
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
                if (tier1Id == WidgetConstants.TIER_TYPE_HOUR) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                break;
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                //            case WidgetConstants.RANGE_TYPE_THIS_WEEK:
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                break;
            //            case WidgetConstants.RANGE_TYPE_THIS_MONTH:
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                if (tier1Id == WidgetConstants.TIER_TYPE_DAY) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                //            case WidgetConstants.RANGE_TYPE_THIS_YEAR:
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                break;
        }
        
        boolean isTopOrBottom = widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP
                                || widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM;
        
        /* Main SELECT expression */
        if (isTopOrBottom) {
            envelopeStartForTopBottom(widget, queryStr);
        } else {
            this.webWidgetHelperService.appendSelection(queryStr, widget);
            queryStr.append(", IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
        }
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
        
        /* Sub JOIN Temp Table "wData" */
        String tableType = null;
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_DAY) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_DAY_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_MONTH) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING;
        }
        
        /* "wData" SELECT expression */
        queryStr.append("SELECT pppt.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, TIER_TYPES_ALIASES_MAP, false);
        
        if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_REVENUE) {
            queryStr.append(", SUM(pppr.TotalAmount) AS WidgetMetricValue ");
        } else if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_DETAIL) {
            queryStr.append(", SUM(pppd.TotalAmount) AS WidgetMetricValue ");
        } else {
            queryStr.append(", SUM(pppt.TotalAmount) AS WidgetMetricValue ");
        }
        
        /* "wData" FROM expression */
        queryStr.append("FROM PPPTotal");
        queryStr.append(tableType);
        queryStr.append(" pppt ");
        if (tableDepthType != WidgetConstants.TABLE_DEPTH_TYPE_TOTAL) {
            queryStr.append("INNER JOIN PPPDetail");
            queryStr.append(tableType);
            queryStr.append(" pppd ON pppd.PPPTotal");
            queryStr.append(tableType);
            queryStr.append("Id = pppt.Id ");
        }
        if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_REVENUE) {
            queryStr.append("INNER JOIN PPPRevenue");
            queryStr.append(tableType);
            queryStr.append(" pppr ON pppr.PPPDetail");
            queryStr.append(tableType);
            queryStr.append("Id = pppd.Id ");
        }
        
        queryStr.append("INNER JOIN Time t ON(pppt.TimeIdLocal = t.Id ");
        
        this.webWidgetHelperService.calculateRange(widget, userAccount.getCustomer().getId(), queryStr, widget.getWidgetRangeType().getId(),
                                                   widget.getWidgetTierTypeByWidgetTier1Type().getId(), true, true);
        
        queryStr.append(") ");
        
        /* "wData" WHERE expression to speed things up */
        this.webWidgetHelperService.appendDataConditions(queryStr, widget, "pppt.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
        
        /* "wData" GROUP BY expression */
        queryStr.append("GROUP BY pppt.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
        queryStr.append(") AS wData ");
        
        /* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, false, null, false);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        if (isTopOrBottom) {
            envelopeEndForTopBottom(widget, queryStr, userAccount);
        }
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(webWidgetHelperService, widget, query, userAccount, false);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        List<WidgetMetricInfo> rawWidgetData = new ArrayList<>();
        try {
            rawWidgetData = query.list();
        } catch (PropertyAccessException pae) {
            LOG.error(pae);
            if (LOG.isDebugEnabled()) {
                LOG.debug("The following query failed: " + query.toString());
            }
        }
        //TODO: Do we want bottom to be ordered ASC or DESC?
        /*
         * if(widget.getWidgetFilterType().getId() == WebCoreConstants.WIDGET_FILTER_TYPE_BOTTOM){
         * Collections.reverse(rawWidgetData);
         * }
         */
        
        widgetData = webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        widgetData.setTrendValue(getWidgetTrendValues(widget, widgetData, userAccount));
        
        return widgetData;
    }
    
    @SuppressWarnings("unchecked")
    protected List<String> getWidgetTrendValues(Widget widget, WidgetData widgetData, UserAccount userAccount) {
        
        List<String> trendValue = new ArrayList<String>();
        
        if (!(widget.getTrendAmount() == null || widget.getTrendAmount() == 0)) {
            Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
            trendValue.add(dollarValue.toString());
            return trendValue;
        }
        
        if (widget.isIsTrendByLocation()) {
            Boolean toBeRemoved;
            
            Collection<Location> locations = new ArrayList<Location>();
            
            if (userAccount.getCustomer().isIsParent()) {
                for (Integer customerId : webWidgetHelperService.getAllChildCustomerId(userAccount.getCustomer().getId())) {
                    locations.addAll(entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByCustomerId", "customerId", customerId, true));
                }
            } else {
                locations = entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByCustomerId", "customerId",
                                                                    userAccount.getCustomer().getId(), true);
            }
            
            //removes locations from the location list which are not relevant (not returned in the SQL)
            Iterator<Location> itr = locations.iterator();
            while (itr.hasNext()) {
                Location location = (Location) itr.next();
                toBeRemoved = true;
                for (String locationName : widgetData.getTier1Names()) {
                    if (locationName.equals(location.getName())) {
                        toBeRemoved = false;
                    }
                }
                if (toBeRemoved) {
                    itr.remove();
                }
            }
            
            //add to trendValue in order of location names found in the widget data
            for (String locationName : widgetData.getTier1Names()) {
                for (Location location : locations) {
                    if (locationName.equals(location.getName())) {
                        Float dollarValue = new BigDecimal(location.getTargetMonthlyRevenueAmount())
                                .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
                        trendValue.add(dollarValue.toString());
                        break;
                    }
                }
            }
            
            /*
             * Widget by Parent location is out of scope for 7.0
             * //if widget is grouped by parent location, need to sum the target revenues of the locations children
             * if(widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ORG
             * || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_ORG ){
             * List<Customer> childCustomers =
             * entityDao.findByNamedQueryAndNamedParam("Customer.findAllChildCustomersByParentCustomerId","parentCustomerId",userAccount.getCustomer().getId());
             * for(Customer customer: childCustomers){
             * List<Location> children = entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByCustomerId", "customerId", customer.getId(), true);
             * int trend = 0;
             * for(Location child: children){
             * trend += child.getTargetMonthlyRevenueAmount();
             * }
             * trendValue.add(Integer.toString(trend));
             * }
             * }
             */
            
            BigDecimal months = new BigDecimal(12);
            BigDecimal weeks = new BigDecimal(52);
            BigDecimal days = new BigDecimal(365);
            
            ListIterator<String> iter = trendValue.listIterator();
            while (iter.hasNext()) {//String target: trendValue){				
                BigDecimal trend = new BigDecimal(iter.next());//location.getTargetMonthlyRevenueAmount());
                switch (widget.getWidgetRangeType().getId()) {
                    case WidgetConstants.RANGE_TYPE_TODAY:
                        trend = trend.multiply(months).divide(days, 2, RoundingMode.HALF_UP);
                        break;
                    case WidgetConstants.RANGE_TYPE_YESTERDAY:
                        trend = trend.multiply(months).divide(days, 2, RoundingMode.HALF_UP);
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                        trend = trend.multiply(months).divide(days, 2, RoundingMode.HALF_UP);
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
                        trend = trend.multiply(months).divide(weeks, 2, RoundingMode.HALF_UP);
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
                        //trend = trend;
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
                        trend = trend.multiply(months);
                        break;
                    //                    case WidgetConstants.RANGE_TYPE_THIS_WEEK:
                    //                        trend = trend.multiply(months).divide(weeks, 2, RoundingMode.HALF_UP);
                    //                        break;
                    //                    case WidgetConstants.RANGE_TYPE_THIS_MONTH:
                    //                        //trend = trend;
                    //                        break;
                    //                    case WidgetConstants.RANGE_TYPE_THIS_YEAR:
                    //                        trend = trend.multiply(months);
                    //                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                        trend = trend.multiply(months).divide(weeks, 2, RoundingMode.HALF_UP);
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                        //trend = trend;
                        break;
                    case WidgetConstants.RANGE_TYPE_LAST_YEAR:
                        trend = trend.multiply(months);
                        break;
                    case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                        trend = trend.multiply(months);
                        break;
                    default:
                        break;
                }
                if (trend.compareTo(new BigDecimal(0)) == 0) {
                    iter.set("0"); //trendValue.add("null");
                } else {
                    iter.set(trend.toString()); //trendValue.add(trend.toString());					
                }
            }
            return trendValue;
        }
        List<String> defaultValue = new ArrayList<String>();
        defaultValue.add("null");
        return defaultValue;
    }
    
    protected void envelopeStartForTopBottom(Widget widget, StringBuilder query) {
        query.append("SELECT ");
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        switch (tier1Id) {
            case WidgetConstants.TIER_TYPE_LOCATION:
                /* Location */
                query.append("l2.CustomerId, l2.Id AS Tier1TypeId, l2.Name AS Tier1TypeName, 0 AS Tier1TypeHidden, ");
                query.append("IFNULL(sub.WidgetMetricValue, 0) AS WidgetMetricValue ");
                query.append("FROM Location l2 ");
                query.append("LEFT JOIN (SELECT wLabel.Tier1TypeId AS SubId, IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
                break;
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                /* Parent Location */
                query.append("l2.CustomerId, l2.Id AS Tier1TypeId, l2.Name AS Tier1TypeName, 0 AS Tier1TypeHidden, ");
                query.append("IFNULL(sub.WidgetMetricValue, 0) AS WidgetMetricValue ");
                query.append("FROM Location l2 ");
                query.append("LEFT JOIN (SELECT wLabel.Tier1TypeId AS SubId, IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                /* Route */
                query.append("ro2.CustomerId, ro2.Id AS Tier1TypeId, ro2.Name AS Tier1TypeName, 0 AS Tier1TypeHidden, ");
                query.append("IFNULL(sub.WidgetMetricValue, 0) AS WidgetMetricValue ");
                query.append("FROM Route ro2 ");
                query.append("LEFT JOIN (SELECT wLabel.Tier1TypeId AS SubId, IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
                break;
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                /* Pay Station */
                query.append("pos2.CustomerId, pos2.Id AS Tier1TypeId, pos2.Name AS Tier1TypeName, 0 AS Tier1TypeHidden, pos2.Serialnumber, ");
                query.append("IFNULL(sub.WidgetMetricValue, 0) AS WidgetMetricValue ");
                query.append("FROM PointOfSale pos2 ");
                query.append("LEFT JOIN (SELECT wLabel.Tier1TypeId AS SubId, IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
                break;
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
                query.append("c2.ParentCustomerId as CustomerId, c2.Id AS Tier1TypeId, c2.Name AS Tier1TypeName, 0 AS Tier1TypeHidden, ");
                query.append("IFNULL(sub.WidgetMetricValue, 0) AS WidgetMetricValue ");
                query.append("FROM Customer c2 ");
                query.append("LEFT JOIN (SELECT wLabel.Tier1TypeId AS SubId, IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
                break;
        }
    }
    
    protected void envelopeEndForTopBottom(Widget widget, StringBuilder query, UserAccount userAccount) {
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        switch (tier1Id) {
            case WidgetConstants.TIER_TYPE_LOCATION:
                /* Location */
                query.append(") sub ON sub.SubId = l2.Id ");
                if (userAccount.getCustomer().isIsParent()) {
                    query.append("WHERE l2.CustomerId IN (:customerId) ");
                } else {
                    query.append("WHERE l2.CustomerId = :customerId ");
                }
                query.append("AND l2.IsParent = 0 ");
                query.append("AND ((sub.WidgetMetricValue IS NOT NULL AND sub.WidgetMetricValue > 0) ");
                query.append("OR (l2.IsDeleted = 0 AND l2.IsUnassigned = 0))");
                break;
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                /* Parent Location */
                query.append(") sub ON sub.SubId = l2.Id ");
                if (userAccount.getCustomer().isIsParent()) {
                    query.append("WHERE l2.CustomerId IN (:customerId) ");
                } else {
                    query.append("WHERE l2.CustomerId = :customerId ");
                }
                query.append("AND l2.IsParent = 1 ");
                query.append("AND ((sub.WidgetMetricValue IS NOT NULL AND sub.WidgetMetricValue > 0) ");
                query.append("OR (l2.IsDeleted = 0 AND l2.IsUnassigned = 0))");
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                /* Route */
                query.append(") sub ON sub.SubId = ro2.Id ");
                if (userAccount.getCustomer().isIsParent()) {
                    query.append("WHERE ro2.CustomerId IN (:customerId) ");
                } else {
                    query.append("WHERE ro2.CustomerId = :customerId ");
                }
                break;
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                /* Pay Station */
                query.append(") sub ON sub.SubId = pos2.Id ");
                query.append("INNER JOIN POSStatus poss ON pos2.Id = poss.PointOfSaleId ");
                if (userAccount.getCustomer().isIsParent()) {
                    query.append("WHERE pos2.CustomerId IN (:customerId) ");
                } else {
                    query.append("WHERE pos2.CustomerId = :customerId ");
                }
                query.append("AND ((sub.WidgetMetricValue IS NOT NULL AND sub.WidgetMetricValue > 0) ");
                query.append("OR (sub.WidgetMetricValue IS NULL AND poss.IsDeleted = 0 AND poss.IsDecommissioned = 0 AND poss.IsVisible = 1))");
                break;
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
                query.append(") sub ON sub.SubId = c2.Id ");
                if (userAccount.getCustomer().isIsParent()) {
                    query.append("WHERE c2.Id IN (:customerId) ");
                } else {
                    query.append("WHERE c2.Id = :customerId ");
                }
                
                break;
        }
    }
    
    protected void appendWhere(StringBuilder query, Widget widget, UserAccount userAccount) {
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        if (userAccount.getCustomer().isIsParent()) {
            if ((tier1Id == WidgetConstants.TIER_TYPE_ORG) && ((widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP)
                                                               || (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM))) {
                query.append("WHERE (1=1) ");
            } else {
                query.append("WHERE wLabel.CustomerId IN (:customerId) ");
            }
        } else {
            query.append("WHERE wLabel.CustomerId = :customerId ");
        }
        
        switch (tier1Id) {
            case WidgetConstants.TIER_TYPE_HOUR:
                //query.append("AND t.QuarterOfHour = 0 ");
                break;
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
                query.append("AND wLabel.ParentCustomerId IS NOT NULL ");
                break;
            default:
                break;
        }
        
        //Following 3 cases are to ignore null values for bottom N queries
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
            switch (tier1Id) {
                case WidgetConstants.TIER_TYPE_LOCATION:
                    query.append("AND wLabel.Tier1TypeId IS NOT NULL ");
                    break;
                case WidgetConstants.TIER_TYPE_ROUTE:
                    query.append("AND wLabel.Tier1TypeId IS NOT NULL ");
                    break;
                case WidgetConstants.TIER_TYPE_PAYSTATION:
                    query.append("AND wLabel.Tier1TypeId IS NOT NULL ");
                    break;
            }
        }
        
        //        int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        //        int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        
        //If a breakdown of transaction types or specific type is not specified, remove smart card recharge and TestPurchase
        // TODO will not be added to the sums
        //
        //        if (tier1Id != WidgetConstants.TIER_TYPE_TRANSACTION_TYPE && tier2Id != WidgetConstants.TIER_TYPE_TRANSACTION_TYPE
        //            && tier3Id != WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
        //            //pi.TransactionTypeId = 4 is a `Smart Card Recharge` type of Transaction
        //            //pi.TransactionTypeId = 6 is a `Test Purchase` type of Transaction
        //            query.append("AND (pi.TransactionTypeId NOT IN (4,6) OR pi.TransactionTypeId IS NULL) ");
        //        }
        
        //If a breakdown of revenue types or specific type is not specified, only allow Total
        // TODO Not necessary with the new design
        //        if (tier1Id != WidgetConstants.TIER_TYPE_REVENUE_TYPE && tier2Id != WidgetConstants.TIER_TYPE_REVENUE_TYPE
        //            && tier3Id != WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
        //            //pid.RevenueTypeId = 1 is a `Total` type of Revenue
        //            query.append("AND (pid.RevenueTypeId = 1 OR pid.RevenueTypeId IS NULL) ");
        //        }
        
        /*
         * if (tier1Id == WidgetConstants.TIER_TYPE_ROUTE ||
         * tier2Id == WidgetConstants.TIER_TYPE_ROUTE ||
         * tier3Id == WidgetConstants.TIER_TYPE_ROUTE)
         * {
         * //RouteType.Id = 1 is a `Collections` type of Route
         * //TODO: may change
         * query.append("AND (ro.RouteTypeId = 1 OR ro.RouteTypeId is null) ");
         * }
         */
    }
    
}