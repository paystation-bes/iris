package com.digitalpaytech.service.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ShortType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ReportContent;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo.RepositoryInfo;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.support.RelativeDateTime;

@Service("reportRepositoryService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class ReportRepositoryServiceImpl implements ReportRepositoryService {
    private static final String HQL_ADDED_GMT = "addedGmt";
    private static final String HQL_LIMIT_DAY = "limitDay";
    
    private static final String QN_DELETE_BY_REPO_ID = "ReportContent.deleteByReportRepositoryId";
    private static final String QN_SEL_OLD_REPORTS = "ReportRepository.findOldReports";
    
    private static final String ALIAS_RR = "rr";
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateReportRepository(final ReportRepository reportRepository) {
        this.entityDao.saveOrUpdate(reportRepository);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteReportRepository(final int reportRepositoryId) {
        
        final ReportRepository reportRepository = this.entityDao.get(ReportRepository.class, reportRepositoryId);
        if (reportRepository != null) {
            final ReportContent reportContent = reportRepository.getReportContent();
            if (reportContent != null) {
                this.entityDao.delete(reportContent);
            }
            this.entityDao.delete(reportRepository);
        }
    }
    
    @Override
    public ReportRepository getReportRepository(final int id) {
        return (ReportRepository) this.entityDao.get(ReportRepository.class, id);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteOldReportsByDay(final Date limitDay) {
        final List<ReportRepository> repositoryList = this.entityDao.findByNamedQueryAndNamedParamWithLock(QN_SEL_OLD_REPORTS,
            HQL_LIMIT_DAY, limitDay, ALIAS_RR);
        if (repositoryList != null && repositoryList.size() > 0) {
            final Object[] reportRepositoryIdParameter = new Object[1];
            for (ReportRepository repository : repositoryList) {
                reportRepositoryIdParameter[0] = repository.getId();
                this.entityDao.deleteAllByNamedQuery(QN_DELETE_BY_REPO_ID, reportRepositoryIdParameter);
                final ReportHistory reportHistory = (ReportHistory) this.entityDao.get(ReportHistory.class, repository.getReportHistory().getId());
                final ReportStatusType reportStatusType = new ReportStatusType();
                reportStatusType.setId(ReportingConstants.REPORT_STATUS_DELETED);
                reportHistory.setReportStatusType(reportStatusType);
                this.entityDao.update(reportHistory);
                this.entityDao.delete(repository);
                this.entityDao.flush();
            }
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteOldSystemAdminReports(final Date limitDay) {
        final List<ReportRepository> repositoryList = this.entityDao.findByNamedQueryAndNamedParamWithLock(
            "ReportRepository.findOldSystemAdminReports", HQL_LIMIT_DAY, limitDay, ALIAS_RR);
        if (repositoryList != null && repositoryList.size() > 0) {
            final Object[] reportRepositoryIdParameter = new Object[1];
            for (ReportRepository repository : repositoryList) {
                reportRepositoryIdParameter[0] = repository.getId();
                this.entityDao.deleteAllByNamedQuery(QN_DELETE_BY_REPO_ID, reportRepositoryIdParameter);
                final ReportHistory reportHistory = (ReportHistory) this.entityDao.get(ReportHistory.class, repository.getReportHistory().getId());
                final ReportStatusType reportStatusType = new ReportStatusType();
                reportStatusType.setId(ReportingConstants.REPORT_STATUS_DELETED);
                reportHistory.setReportStatusType(reportStatusType);
                this.entityDao.update(reportHistory);
                this.entityDao.delete(repository);
            }
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void addWarningToOldReportsByDay(final Date limitDay) {
        final List<ReportRepository> listReports = this.entityDao.findByNamedQueryAndNamedParamWithLock(QN_SEL_OLD_REPORTS,
            HQL_LIMIT_DAY, limitDay, ALIAS_RR);
        if (listReports != null && listReports.size() > 0) {
            for (ReportRepository reports : listReports) {
                reports.setIsSoonToBeDeleted(true);
                this.entityDao.saveOrUpdate(reports);
                this.entityDao.flush();
            }
        }
    }
    
    @Override
    public List<Object[]> getUserAccountFileSizeTotals() {
        final SQLQuery sqlQuery = this.entityDao
                .createSQLQuery("SELECT rr.UserAccountId, SUM(rr.FileSizeKb) AS FileSizeKb FROM ReportRepository rr "
                        + "INNER JOIN ReportHistory rh ON rr.ReportHistoryId = rh.Id INNER JOIN ReportDefinition rd ON rh.ReportDefinitionId = rd.Id "
                        + "WHERE rd.IsSystemAdmin = 0 GROUP BY UserAccountId");
        return sqlQuery.list();
        
    }
    
    @Override
    public final List<Object[]> getUserAccountUnWarnedFileSizeTotals() {
        final SQLQuery sqlQuery = this.entityDao
                .createSQLQuery(" SELECT rr.UserAccountId, SUM(rr.FileSizeKb) AS FileSizeKb FROM ReportRepository rr "
                        + " INNER JOIN ReportHistory rh ON rr.ReportHistoryId = rh.Id INNER JOIN ReportDefinition rd ON rh.ReportDefinitionId = rd.Id "
                        + "WHERE rr.IsSoonToBeDeleted = 0 AND rd.IsSystemAdmin = 0 GROUP BY UserAccountId");
        return sqlQuery.list();
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteOldestReports(final int userAccountId, final int maxSize, final int totalSize) {
        final List<ReportRepository> reportRepositoryList = this.entityDao.findByNamedQueryAndNamedParam(
            "ReportRepository.findByUserAccountIdOrderByAddedGMTAsc", HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        if (reportRepositoryList != null) {
            final Object[] reportRepositoryIdParameter = new Object[1];
            int currentTotalSize = totalSize;
            while (currentTotalSize > maxSize && reportRepositoryList.size() > 0) {
                final ReportRepository reportRepository = reportRepositoryList.remove(0);
                final int sizeOfDeletedReport = reportRepository.getFileSizeKb();
                reportRepositoryIdParameter[0] = reportRepository.getId();
                this.entityDao.deleteAllByNamedQuery(QN_DELETE_BY_REPO_ID, reportRepositoryIdParameter);
                final ReportHistory reportHistory = (ReportHistory) this.entityDao.get(ReportHistory.class, reportRepository.getReportHistory()
                        .getId());
                final ReportStatusType reportStatusType = new ReportStatusType();
                reportStatusType.setId(ReportingConstants.REPORT_STATUS_DELETED);
                reportHistory.setReportStatusType(reportStatusType);
                this.entityDao.update(reportHistory);
                this.entityDao.delete(this.entityDao.merge(reportRepository));
                currentTotalSize -= sizeOfDeletedReport;
                this.entityDao.flush();
            }
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void addWarningToOldestReports(final int userAccountId, final int maxSize, final int totalSize) {
        final List<ReportRepository> reportRepositoryList = this.entityDao.findByNamedQueryAndNamedParam(
            "ReportRepository.findUnWarnedByUserAccountIdOrderByAddedGMTAsc", HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        
        int currentTotalSize = totalSize;
        while (currentTotalSize > maxSize && reportRepositoryList.size() > 0) {
            final ReportRepository reportRepository = reportRepositoryList.remove(0);
            final int sizeOfWarnedReport = reportRepository.getFileSizeKb();
            reportRepository.setIsSoonToBeDeleted(true);
            this.entityDao.saveOrUpdate(reportRepository);
            currentTotalSize -= sizeOfWarnedReport;
            this.entityDao.flush();
        }
    }
    
    @SuppressWarnings("unused")
    // "ReportContent.findByUserAccountIdOrderByAddedGMTDesc" used instead
    private ReportRepository findOldestByUserAccountId(final int userAccountId) {
        
        final StringBuilder sqlText = new StringBuilder("SELECT rr.Id, rr.FileSizeKb FROM ReportRepository rr ");
        sqlText.append(" RIGHT JOIN (SELECT UserAccountId, MIN(AddedGmt) AS AddedGmt FROM ReportRepository Group By UserAccountId) rr2");
        sqlText.append(" ON rr.AddedGMT = rr2.AddedGMT AND rr.UserAccountId = rr2.UserAccountId");
        sqlText.append(" INNER JOIN ReportHistory rh ON rr.ReportHistoryId = rh.Id ");
        sqlText.append(" INNER JOIN ReportDefinition rd ON rh.ReportDefinitionId = rd.Id ");
        sqlText.append(" WHERE rr.UserAccountId = :userAccountId AND rd.IsSystemAdmin = 0 ");
        
        final SQLQuery sqlQuery = this.entityDao.createSQLQuery(sqlText.toString());
        sqlQuery.addScalar("Id", new IntegerType());
        sqlQuery.addScalar("FileSizeKb", new ShortType());
        sqlQuery.setParameter(HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(ReportRepository.class));
        final List<ReportRepository> repositoryList = sqlQuery.list();
        if (repositoryList != null && repositoryList.size() > 0) {
            return repositoryList.get(0);
        }
        return null;
    }
    
    @Override
    public ReportRepository findByReportHistoryId(final int reportHistoryId) {
        return (ReportRepository) this.entityDao.findUniqueByNamedQueryAndNamedParam("ReportRepository.findByReportHistoryId", "reportHistoryId",
            reportHistoryId);
        
    }
    
    @Override
    public List<ReportRepository> findByUserAccountId(final int userAccountId) {
        return this.entityDao
                .findByNamedQueryAndNamedParam("ReportRepository.findByUserAccountIdOrderByAddedGMTDesc", HibernateConstants.USER_ACCOUNT_ID, userAccountId);
    }
    
    @Override
    public List<ReportRepository> findByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportRepository.findByCustomerIdOrderByAddedGMTDesc", HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    public List<ReportRepository> findByUserAccountIdForSystemAdmin(final int userAccountId, final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportRepository.findByUserAccountIdForSystemAdminOrderByAddedGMTDesc", new String[] {
            HibernateConstants.USER_ACCOUNT_ID, HibernateConstants.CUSTOMER_ID, }, new Integer[] { userAccountId, customerId, });
    }
    
    @Override
    public int countUnreadReportsByUserAccountId(final int userAccountId) {
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT COUNT(*) FROM ReportRepository rr ");
        bdr.append(" INNER JOIN ReportHistory rh ON rr.ReportHistoryId = rh.Id");
        bdr.append(" INNER JOIN ReportDefinition rd ON rh.ReportDefinitionId = rd.Id");
        bdr.append(" WHERE rr.IsReadByUser = 0 AND rr.UserAccountId = :userAccountId AND rd.IsSystemAdmin = 0 ");
        
        final SQLQuery query = this.entityDao.createSQLQuery(bdr.toString());
        query.setParameter(HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        final BigInteger numberOfReports = (BigInteger) query.uniqueResult();
        return numberOfReports.intValue();
    }
    
    @Override
    public final ReportContent findReportContentByReportRepositoryId(final int reportRepositoryId) {
        return (ReportContent) this.entityDao.findUniqueByNamedQueryAndNamedParam("ReportContent.findByReportRepositoryId", "reportRepositoryId",
            reportRepositoryId);
    }
    
    @Override
    public final int deleteOlderAdhocReport(final Date targetTime) {
        int deleted = 0;
        final ScrollableResults scroll = this.entityDao.getNamedQuery("ReportRepository.findOlderAdhocReportId")
                .setParameter("targetTime", targetTime).scroll(ScrollMode.FORWARD_ONLY);
        while (scroll.next()) {
            deleteReportRepository(scroll.getInteger(0));
            ++deleted;
        }
        
        return deleted;
    }

    @Override
    @SuppressWarnings({ "checkstyle:magicnumber" })
    // I suppress magic number here becuase it easier to read when those magic numbers are just index of array.
    public List<RepositoryInfo> findCompletedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        final Criteria query = this.entityDao.createCriteria(ReportRepository.class);
        query.createAlias("reportHistory", "history");
        query.createAlias("history.reportDefinition", "definition");
        
        query.setProjection(Projections.projectionList()
                            .add(Projections.property(HibernateConstants.ID))
                            .add(Projections.property("definition.title"))
                            .add(Projections.property(HQL_ADDED_GMT))
                            .add(Projections.property("history.reportStatusType.id"))
                            .add(Projections.property("isSoonToBeDeleted"))
                            .add(Projections.property("isReadByUser")));
        
        query.setResultTransformer(new ResultTransformer() {
            private static final long serialVersionUID = -6392102159415075621L;

            @Override
            public Object transformTuple(final Object[] tuple, final String[] aliases) {
                final RepositoryInfo result = new RepositoryInfo();
                result.setRandomId(keyMapping.getRandomString(ReportQueue.class, tuple[0]));
                result.setTitle((String) tuple[1]);
                result.setCreatedTime(new RelativeDateTime((Date) tuple[2], timeZone).toString());
                result.setCreatedMs(((Date) tuple[2]).getTime());
                result.setStatus((Integer) tuple[3]);
                result.setToBeDeleted((Boolean) tuple[4]);
                result.setReadByUser((Boolean) tuple[5]);
                
                return result;
            }

            @SuppressWarnings("rawtypes")
            @Override
            public List transformList(final List collection) {
                return collection;
            }
        });
        
        query.add(Restrictions.eq("definition.isSystemAdmin", criteria.isSysadmin()));
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("history.customer.id", criteria.getCustomerId()));
        }
        
        if (criteria.getUserAccountId() != null) {
            query.add(Restrictions.eq("userAccount.id", criteria.getUserAccountId()));
        }
        
        query.addOrder(Order.desc(HQL_ADDED_GMT));
        
        return query.list();
    }
}
