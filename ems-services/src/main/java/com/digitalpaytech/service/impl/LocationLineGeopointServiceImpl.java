package com.digitalpaytech.service.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.LocationLineGeopoint;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.GeoLocationEntry;
import com.digitalpaytech.service.LocationLineGeopointService;

@Service("locationLineGeopointService")
@Transactional(propagation = Propagation.SUPPORTS)
public class LocationLineGeopointServiceImpl implements LocationLineGeopointService {
    
    private static final String SQL_OCCUPANCY = "SELECT l.Id AS LocationId, IFNULL(SUM(o.NoOfPermits)/l.NumberOfSpaces * 100, 0) AS PaidOccupancy, "
                                                + "IFNULL(SUM(co.NoOfPermits)/co.NumberOfSpaces * 100, NULL) AS PhysicalOccupancy " + "FROM Location l "
                                                + "LEFT JOIN OccupancyHour o ON (o.LocationId = l.Id AND o.TimeIdGMT = "
                                                + "(SELECT MAX(Id) FROM `Time` t3 WHERE t3.DateTime <= UTC_TIMESTAMP())) "
                                                + "LEFT JOIN CaseOccupancyHour co ON (co.LocationId = l.Id AND co.Id = "
                                                + "(SELECT MAX(Id) FROM CaseOccupancyHour WHERE LocationId = :locationId)) "
                                                + "WHERE l.Id = :locationId GROUP BY o.LocationId;";
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final LocationLineGeopoint getLocationLineGeopointByLocationId(final int locationId) {
        return (LocationLineGeopoint) this.entityDao.findUniqueByNamedQueryAndNamedParam("LocationLineGeopoint.findLocationLineGeopointByLocationId",
                                                                                         "locationId", locationId);
    }
    
    @Override
    public final LocationLineGeopoint getLocationLineGeopointById(final long geopointId) {
        return this.entityDao.get(LocationLineGeopoint.class, geopointId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveOrUpdateLocationLineGeopoint(final LocationLineGeopoint line) {
        if (line.getLocation() != null && line.getLocation().getId() != null) {
            this.entityDao.saveOrUpdate(line);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteLocationLineGeopoint(final LocationLineGeopoint line) {
        if (line.getLocation() != null && line.getLocation().getId() != null) {
            this.entityDao.delete(line);
        } 
    }
    
    public final void findOccupancyForGeoPoint(final GeoLocationEntry geoLocationEntry, final Integer locationId) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_OCCUPANCY);
        q.addScalar("LocationId", new IntegerType());
        q.addScalar("PaidOccupancy", new FloatType());
        q.addScalar("PhysicalOccupancy", new FloatType());
        q.setParameter(HibernateConstants.LOCATION_ID, locationId);
        final List<Object[]> occupancyList = q.list();
        if (occupancyList != null && occupancyList.get(0) != null) {
            final Object[] occupancies = occupancyList.get(0);
            geoLocationEntry.setPaidOccupancy((Float) occupancies[1]);
            geoLocationEntry.setPhysicalOccupancy((Float) occupancies[2]);
        }
    }
}
