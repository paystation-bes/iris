package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.service.MobileApplicationTypeService;

@Service("mobileApplicationTypeService")
public class MobileApplicationTypeServiceImpl implements MobileApplicationTypeService{

	@Autowired
	EntityDao entityDao;
	
	@Override
	public MobileApplicationType getMobileApplicationTypeById(int id){
		return (MobileApplicationType) entityDao.findUniqueByNamedQueryAndNamedParam("MobileApplicationType.findMobileApplicationTypeById", "id", id);
	}
}
