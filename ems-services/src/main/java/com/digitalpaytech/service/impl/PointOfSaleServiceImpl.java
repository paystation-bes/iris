package com.digitalpaytech.service.impl;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosDateType;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.MapEntryDetailsTransformer;
import com.digitalpaytech.dto.MapEntryMinimalTransformer;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleAlertInfo;
import com.digitalpaytech.dto.PointOfSaleAlertInfoTransformer;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.PosHashTransformer;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryDTO;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryTransformer;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Service("pointOfSaleService")
@Transactional(propagation = Propagation.REQUIRED)
public class PointOfSaleServiceImpl implements PointOfSaleService {
    private static final String MOBILESUMMARY_SQL = "SELECT pos.Name AS Name, pos.Id AS Id, r.Id AS RouteId, pos.Latitude AS Latitude, "
                                                    + "pos.Longitude AS Longitude, l.Name AS Location, pl.Name AS ParentLocation, "
                                                    + "pos.SerialNumber AS SerialNumber, ps.PaystationTypeId AS PaystationType, "
                                                    + "posb.LastCollectionGMT AS LastCollected, posb.LastBillCollectionGMT AS LastCollectedBill, "
                                                    + "posb.LastCoinCollectionGMT AS LastCollectedCoin, posb.LastCardCollectionGMT AS LastCollectedCard "
                                                    + "FROM PointOfSale pos INNER JOIN Paystation ps ON (pos.PaystationId = ps.Id) "
                                                    + "INNER JOIN POSStatus poss ON (pos.Id = poss.PointOfSaleId AND poss.IsProvisioned = 1 "
                                                    + "AND poss.IsDecommissioned = 0 AND poss.isVisible = 1 AND poss.IsActivated = 1 AND poss.IsDeleted = 0) "
                                                    + "INNER JOIN POSBalance posb ON (pos.Id = posb.PointOfSaleId) "
                                                    + "INNER JOIN Location l ON (pos.LocationId = l.Id) "
                                                    + "INNER JOIN RoutePOS rPOS ON (pos.Id = rPOS.PointOfSaleId) "
                                                    + "INNER JOIN Route r ON (rPOS.RouteId = r.Id) "
                                                    + "LEFT JOIN Location pl ON (l.ParentLocationId = pl.Id) "
                                                    + "WHERE pos.CustomerId = :customerId AND r.RouteTypeId = :routeTypeId";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesByLocationId(final int locationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByLocationId", "locationId", locationId);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesByLocationIdOrderByName(final int locationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByLocationIdOrderByName", "locationId", locationId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesByCustomerId(final int customerId) {
        return findPointOfSalesByCustomerId(customerId, false);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesByCustomerId(final int customerId, final boolean all) {
        return this.entityDao.findByNamedQueryAndNamedParam(all ? "PointOfSale.findPointOfSaleByCustomerId"
                : "PointOfSale.findValidPointOfSaleByCustomerId", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findValidPointOfSaleByCustomerIdOrderByName", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @Override
    public final List<CollectPaystationSummaryDTO> findValidAndActivatedPointOfSalesByCustomerIdAndRouteType(final int customerId,
        final int routeTypeId) {
        final CollectPaystationSummaryTransformer transformer = new CollectPaystationSummaryTransformer();
        final SQLQuery q = this.entityDao.createSQLQuery(MOBILESUMMARY_SQL);
        q.addScalar("Name", new StringType());
        q.addScalar("Id", new IntegerType());
        q.addScalar("RouteId", new IntegerType());
        q.addScalar("Latitude", new BigDecimalType());
        q.addScalar("Longitude", new BigDecimalType());
        q.addScalar("Location", new StringType());
        q.addScalar("ParentLocation", new StringType());
        q.addScalar("SerialNumber", new StringType());
        q.addScalar("PaystationType", new IntegerType());
        q.addScalar("LastCollected", new TimestampType());
        q.addScalar("LastCollectedBill", new TimestampType());
        q.addScalar("LastCollectedCoin", new TimestampType());
        q.addScalar("LastCollectedCard", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("routeTypeId", routeTypeId);
        q.setResultTransformer(transformer).list();
        final List<CollectPaystationSummaryDTO> result = new ArrayList<CollectPaystationSummaryDTO>(transformer.getResult().values());
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdNoCache(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleAndPosStatusByCustomerIdNoCache", "customerId", customerId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public PointOfSale findPointOfSaleBySerialNumber(final String serialNumber) {
        return findPosAndPosStatus("PointOfSale.findPointOfSaleBySerialNumber", serialNumber);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final PointOfSale findPointOfSaleAndPosStatusBySerialNumber(final String serialNumber) {
        return findPosAndPosStatus("PointOfSale.findPointOfSaleAndPosStatusBySerialNumber", serialNumber);
    }

    private PointOfSale findPosAndPosStatus(final String hqlName, final String serialNumber) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = this.entityDao.findByNamedQueryAndNamedParam(hqlName, "serialNumber", serialNumber);
        if (posList != null && posList.size() > 0) {
            return posList.get(0);
        }
        return null;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final PointOfSale findTxablePointOfSaleBySerialNumber(final String serialNumber) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findTxablePointOfSaleBySerialNumber", "serialNumber",
                                                                                       serialNumber);
        if (posList != null && posList.size() > 0) {
            return posList.get(0);
        }
        return null;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final PointOfSale findPointOfSaleBySerialNumberAndCustomerId(final String serialNumber, final Integer customerId) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleBySerialNumberAndCustomerId",
                                                                                       new String[] { "serialNumber", "customerId" }, new Object[] {
                                                                                           serialNumber, customerId });
        if (posList != null && posList.size() > 0) {
            return posList.get(0);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<PointOfSale> findAllPointOfSalesOrderByName() {
        return this.entityDao.findByNamedQuery("PointOfSale.findAllPointOfSalesOrderByName", true);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final PointOfSale findPointOfSaleById(final Integer id) {
        return (PointOfSale) this.entityDao.get(PointOfSale.class, id);
    }
    
    @Override
    public final PosHeartbeat findPosHeartbeatByPointOfSaleId(final int pointOfSaleId) {
        return (PosHeartbeat) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosHeartbeat.findPosHeartbeatForPointOfSaleById", "pointOfSaleId",
                                                                                 pointOfSaleId, true);
    }
    
    @Override
    public final PosServiceState findPosServiceStateByPointOfSaleId(final int pointOfSaleId) {
        @SuppressWarnings("unchecked")
        final List<PosServiceState> posServiceStates = this.entityDao
                .findByNamedQueryAndNamedParam("PosServiceState.findPosServiceStateByPointOfSaleId", "pointOfSaleId", pointOfSaleId);
        if (posServiceStates.size() > 0) {
            return posServiceStates.get(0);
        }
        return null;
    }
    
    @Override
    public final PosSensorState findPosSensorStateByPointOfSaleId(final int pointOfSaleId) {
        return (PosSensorState) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosSensorState.findPosSensorStateByPointOfSaleId",
                                                                                   "pointOfSaleId", pointOfSaleId);
    }
    
    @Override
    public final PointOfSale findPointOfSaleByPaystationId(final int paystationId) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByPaystationId", "paystationId",
                                                                                       paystationId);
        if (posList != null && posList.size() > 0) {
            return posList.get(0);
        }
        return null;
        
    }
    
    @Override
    public final List<Paystation> findPayStationsByCustomerId(final int paystationId) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = (List<PointOfSale>) this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByCustomerId",
                                                                                                           "paystationId", paystationId);
        final List<Paystation> payStations = new ArrayList<Paystation>();
        for (PointOfSale pos : posList) {
            payStations.add((Paystation) this.entityDao.get(Paystation.class, pos.getPaystation().getId()));
        }
        return payStations;
    }
    
    @Override
    public final List<Paystation> findPayStationsByLocationId(final int locationId) {
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleByLocationId", "locationId",
                                                                                       locationId);
        final List<Paystation> payStations = new ArrayList<Paystation>();
        for (PointOfSale pos : posList) {
            payStations.add((Paystation) this.entityDao.get(Paystation.class, pos.getPaystation().getId()));
        }
        return payStations;
    }
    
    @Override
    public final Paystation findPayStationById(final int payStationId) {
        return (Paystation) this.entityDao.get(Paystation.class, payStationId);
    }
    
    @Override
    public final Paystation findPayStationByPointOfSaleId(final int pointOfSaleId) {
        final PointOfSale pointOfSale = (PointOfSale) this.entityDao.get(PointOfSale.class, pointOfSaleId);
        return pointOfSale.getPaystation();
    }
    
    @Override
    public final PosStatus findPointOfSaleStatusByPOSId(final int pointOfSaleId, final boolean useCache) {
        @SuppressWarnings("unchecked")
        final List<PosStatus> posStatusList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSaleStatus.findPointOfSaleStatusByPOSId",
                                                                                           "pointOfSaleId", pointOfSaleId, useCache);
        if (posStatusList != null && posStatusList.size() > 0) {
            return posStatusList.get(0);
        }
        return null;
        
    }
    
    @Override
    public final PaystationType findPayStationTypeByPointOfSaleId(final int pointOfSaleId) {
        final PointOfSale pointOfSale = (PointOfSale) this.entityDao.get(PointOfSale.class, pointOfSaleId);
        return pointOfSale.getPaystation().getPaystationType();
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public final List<MerchantPOS> findMerchantPOSbyPointOfSaleId(final int pointOfSaleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantPOS.findMerchantPOSByPointOfSaleId", "pointOfSaleId", pointOfSaleId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(final int pointOfSaleId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MerchantPOS.findMerchPOSByPOSIdNoDeleted", "pointOfSaleId", pointOfSaleId, true);
    }
    
    @Override
    public final PosBalance findPOSBalanceByPointOfSaleId(final int pointOfSaleId) {
        return (PosBalance) this.entityDao.get(PosBalance.class, pointOfSaleId);
    }
    
    @Override
    public final PosBalance findPOSBalanceByPointOfSaleIdNoCache(final int pointOfSaleId) {
        return (PosBalance) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("POSBalance.findPOSBalanceById", "pointOfSaleId", pointOfSaleId, false);
        
    }
    
    @Override
    public final Date findLastCollectionDateByPointOfSaleId(final int pointOfSaleId) {
        final Query query = this.entityDao.getCurrentSession().getNamedQuery("PointOfSaleCollection.findLastCollectionDateByPointOfSaleId");
        query.setInteger("pointOfSaleId", pointOfSaleId);
        
        return (Date) query.uniqueResult();
    }
    
    @Override
    public final List<PointOfSale> findPointOfSalesByRouteId(final int routeId) {
        
        @SuppressWarnings("unchecked")
        final List<RoutePOS> routePOSes = this.entityDao.findByNamedQueryAndNamedParam("RoutePOS.findRoutePOSByRouteId", "routeId", routeId, true);
        final List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
        
        for (RoutePOS routePOS : routePOSes) {
            if (routePOS.getPointOfSale().getPosStatus().isIsVisible()) {
                pointOfSales.add(routePOS.getPointOfSale());
            }
        }
        
        return pointOfSales;
    }
    
    @Override
    public final ModemSetting findModemSettingByPointOfSaleid(final int pointOfSaleId) {
        
        @SuppressWarnings("unchecked")
        final List<ModemSetting> modemSettings = this.entityDao.findByNamedQueryAndNamedParam("ModemSetting.findModemSettingByPointOfSaleId",
                                                                                              "pointOfSaleId", pointOfSaleId);
        if (modemSettings == null || modemSettings.isEmpty() || modemSettings.get(0) == null) {
            return null;
        }
        return modemSettings.get(0);
    }
    
    @Override
    public final void saveOrUpdatePointOfSaleHeartbeat(final PosHeartbeat posHeartbeat) {
        this.entityDao.saveOrUpdate(posHeartbeat);
    }
    
    @Override
    public final void updatePaystationActivityTime(final PointOfSale pos, final boolean isUpdated) {
        if (pos != null && !isUpdated) {
            final PosHeartbeat posHeartbeat = findPosHeartbeatByPointOfSaleId(pos.getId());
            if (posHeartbeat != null) {
                posHeartbeat.setLastHeartbeatGmt(DateUtil.convertTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT, TimeZone.getDefault().getID(),
                                                                          new Date()));
                saveOrUpdatePointOfSaleHeartbeat(posHeartbeat);
            }
        }
    }
    
    @Override
    public final void updatePosServicState(final PosServiceState posServiceState) {
        this.entityDao.saveOrUpdate(this.entityDao.merge(posServiceState));
    }
    
    @Override
    public final void updatePosSensorState(final PosSensorState posSensorState) {
        this.entityDao.saveOrUpdate(this.entityDao.merge(posSensorState));
    }
    
    @Override
    public final void saveOrUpdatePOSDate(final PosDate posDate) {
        this.entityDao.saveOrUpdate(posDate);
    }
    
    @Override
    public final PosDateType findPosDateTypeById(final int posDateTypeId) {
        return (PosDateType) this.entityDao.get(PosDateType.class, posDateTypeId);
    }
    
    @Override
    public final PaystationType findPayStationTypeById(final int payStationTypeId) {
        return (PaystationType) this.entityDao.get(PaystationType.class, payStationTypeId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findPointOfSalesBySettingsFileId(final int settingsFileId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleBySettingsFileId", "settingsFileId", settingsFileId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSalesBySerialNumberAndCustomerId(final int customerId, final List<String> serialNumberList) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSalesBySerialNumberAndCustomerId", new String[] {
            "serialNumberList", "customerId" }, new Object[] { serialNumberList, customerId });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(final String keyword) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findAllPointOfSalesBySerialNumberKeyword", "keyword", new StringBuilder("%")
                .append(keyword).append("%").toString());
        
    }
    
    /** 
     * @deprecated Use findPointOfSalesBySerialNumberKeywordWithLimit instead.
     */    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(final String keyword, final int customerId) {
        return createPointOfSalesBySerialNumberKeyword(keyword, customerId).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSalesBySerialNumberKeywordWithLimit(final String keyword, final int customerId) {
        final int limitSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE,
                                                                              EmsPropertiesService.DEFAULT_PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE);
        
        return createPointOfSalesBySerialNumberKeyword(keyword, customerId).setMaxResults(limitSize).list();
    }

    private Query createPointOfSalesBySerialNumberKeyword(final String keyword, final int customerId) {
        return this.entityDao.getNamedQuery("PointOfSale.findAllPointOfSalesBySerialNumberKeywordAndCustomer")
                .setParameter("keyword", new StringBuilder("%").append(keyword).append("%").toString()).setParameter("customerId", customerId);        
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<PointOfSale> findAllPointOfSalesByPosNameKeyword(final String keyword) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findAllPointOfSalesByPosNameKeyword", "keyword", new StringBuilder("%")
                .append(keyword).append("%").toString());
    }
    
    /** 
     * @deprecated Use findPointOfSalesByPosNameKeywordWithLimit instead.
     */     
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findAllPointOfSalesByPosNameKeyword(final String keyword, final int customerId) {
        return createPointOfSalesByPosNameKeyword(keyword, customerId).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSalesByPosNameKeywordWithLimit(final String keyword, final int customerId) {
        final int limitSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE,
                                                                              EmsPropertiesService.DEFAULT_PAY_STATION_SEARCH_RESULTS_LIMIT_SIZE);
        
        return createPointOfSalesByPosNameKeyword(keyword, customerId).setMaxResults(limitSize).list();
    }
    
    private Query createPointOfSalesByPosNameKeyword(final String keyword, final int customerId) {
        return this.entityDao.getNamedQuery("PointOfSale.findAllPointOfSalesByPosNameKeywordAndCustomer")
                .setParameter("keyword", new StringBuilder("%").append(keyword).append("%").toString()).setParameter("customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findVirtualPointOfSalesByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findVirtualPointOfSalesByCustomerId", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findNoneVirtualPointOfSalesByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findNoneVirtualPointOfSalesByCustomerId", "customerId", customerId);
    }
    
    @Override
    public final void update(final PointOfSale pointOfSale) {
        this.entityDao.update(pointOfSale);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage(final Integer settingsFileId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage",
                                                            "settingsFileId", settingsFileId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage(final Integer settingsFileId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage",
                                                            "settingsFileId", settingsFileId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<PointOfSale> findUnAssignedPointOfSalesForPayStationSettingsPage(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findUnAssignedPointOfSalesForPayStationSettingsPage", "customerId",
                                                            customerId);
    }
    
    @Override
    public final PlacementMapBordersInfo findMapBordersByCustomerId(final Integer customerId) {
        final SQLQuery q = this.entityDao
                .createSQLQuery("SELECT MIN(pos.Latitude) AS MinLatitude, Max(pos.Latitude) AS MaxLatitude, MIN(pos.Longitude) AS MinLongitude"
                                + ", MAX(longitude) AS MaxLongitude FROM PointOfSale pos where pos.CustomerId = :customerId");
        q.addScalar("MinLatitude", new BigDecimalType());
        q.addScalar("MaxLatitude", new BigDecimalType());
        q.addScalar("MinLongitude", new BigDecimalType());
        q.addScalar("MaxLongitude", new BigDecimalType());
        q.setParameter("customerId", customerId);
        q.setResultTransformer(Transformers.aliasToBean(PlacementMapBordersInfo.class));
        @SuppressWarnings("unchecked")
        final List<PlacementMapBordersInfo> list = q.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findByCustomerIdAndLocationName(final Integer customerId, final String locationName) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findByCustomerIdAndLocationName", new String[] { "customerId",
            "locationName" }, new Object[] { customerId, locationName }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleAndPosStatusByCustomerId", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdOrderByName(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleAndPosStatusByCustomerIdOrderByName", "customerId",
                                                            customerId);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final int countUnPlacedByCustomerIds(final Collection<Integer> customerIds) {
        int result = 0;
        final Number buffer = (Number) this.entityDao.getNamedQuery("PointOfSale.countUnPlacedPosMapEntryByCustomerIds")
                .setParameterList("customerIds", customerIds).uniqueResult();
        if (buffer != null) {
            result = buffer.intValue();
        }
        
        return result;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final int countUnPlacedByLocationIds(final Collection<Integer> locationIds) {
        int result = 0;
        if ((locationIds == null) || (!locationIds.isEmpty())) {
            final Number buffer = (Number) this.entityDao.getNamedQuery("PointOfSale.countUnPlacedPosMapEntryByLocationIds")
                    .setParameterList("locationIds", locationIds).uniqueResult();
            if (buffer != null) {
                result = buffer.intValue();
            }
        }
        
        return result;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final int countUnPlacedByRouteIds(final Collection<Integer> routeIds) {
        int result = 0;
        final Number buffer = (Number) this.entityDao.getNamedQuery("PointOfSale.countUnPlacedPosMapEntryByRouteIds")
                .setParameterList("routeIds", routeIds).uniqueResult();
        if (buffer != null) {
            result = buffer.intValue();
        }
        
        return result;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("unchecked")
    @Override
    public final List<MapEntry> findMapEntriesByCustomerIds(final Collection<Integer> customerIds) {
        return this.entityDao.getNamedQuery("PointOfSale.findPlacedPosMapEntryByCustomerIds").setParameterList("customerIds", customerIds)
                .setResultTransformer(new MapEntryMinimalTransformer()).list();
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("unchecked")
    @Override
    public final List<MapEntry> findMapEntriesByLocationIds(final Collection<Integer> locationIds) {
        return this.entityDao.getNamedQuery("PointOfSale.findPlacedPosMapEntryByLocationIds").setParameterList("locationIds", locationIds)
                .setResultTransformer(new MapEntryMinimalTransformer()).list();
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("unchecked")
    @Override
    public final List<MapEntry> findMapEntriesByRouteIds(final Collection<Integer> routeIds) {
        return this.entityDao.getNamedQuery("PointOfSale.findPlacedPosMapEntryByRouteIds").setParameterList("routeIds", routeIds)
                .setResultTransformer(new MapEntryMinimalTransformer()).list();
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public final Map<Integer, MapEntry> findPointOfSaleMapEntryDetails(final Collection<Integer> pointOfSaleIds, final TimeZone timeZone) {
        Map<Integer, MapEntry> result = null;
        @SuppressWarnings("unchecked")
        final List<MapEntry> mapEntries = this.entityDao.getNamedQuery("PointOfSale.findPointOfSaleMapEntryDetailByIds")
                .setParameterList("pointOfSaleIds", pointOfSaleIds).setResultTransformer(new MapEntryDetailsTransformer(timeZone)).list();
        
        result = new HashMap<Integer, MapEntry>(mapEntries.size() * 2, 0.6F);
        final Iterator<MapEntry> itr = mapEntries.iterator();
        while (itr.hasNext()) {
            final MapEntry entry = itr.next();
            result.put((Integer) entry.getRandomId().getId(), entry);
        }
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat("MMM dd, yyyy h:mm a", timeZone);
        
        @SuppressWarnings("unchecked")
        final List<Object[]> lastCollections = this.entityDao.getNamedQuery("PointOfSaleCollection.findLastCollectionDateByAllPointOfSaleIds")
                .setParameterList("pointOfSaleIds", pointOfSaleIds).list();
        for (Object[] col : lastCollections) {
            final MapEntry entry = result.get(col[0]);
            entry.setLastCollection(dateFmt.get().format(col[1]));
        }
        
        dateFmt.close();
        
        return result;
    }
    
    @Override
    public final Map<Integer, PointOfSale> findPosHashByCustomerIds(final Collection<Integer> customerIds) {
        final PosHashTransformer transformer = new PosHashTransformer();
        this.entityDao.getNamedQuery("PointOfSale.findPosHashByCustomerIds").setParameterList("customerIds", customerIds)
                .setResultTransformer(transformer).list();
        
        return transformer.getResult();
    }
    
    @Override
    public final Map<Integer, PointOfSale> findPosHashByLocationIds(final Collection<Integer> locationIds) {
        final PosHashTransformer transformer = new PosHashTransformer();
        this.entityDao.getNamedQuery("PointOfSale.findPosHashByLocationIds").setParameterList("locationIds", locationIds)
                .setResultTransformer(transformer).list();
        
        return transformer.getResult();
    }
    
    @Override
    public final Map<Integer, PointOfSale> findPosHashByRouteIds(final Collection<Integer> routeIds) {
        final PosHashTransformer transformer = new PosHashTransformer();
        this.entityDao.getNamedQuery("PointOfSale.findPosHashByRouteIds").setParameterList("routeIds", routeIds).setResultTransformer(transformer)
                .list();
        
        return transformer.getResult();
    }
    
    @Override
    public final Map<Integer, PointOfSale> findPosHashByPosIds(final Collection<Integer> posIds) {
        final PosHashTransformer transformer = new PosHashTransformer();
        this.entityDao.getNamedQuery("PointOfSale.findPosHashByPOSIds").setParameterList("posIds", posIds).setResultTransformer(transformer).list();
        
        return transformer.getResult();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Integer> findPosIdsByCustomerId(final Integer customerId) {
        return this.entityDao.getNamedQuery("PointOfSale.findPosIdsByCustomerId").setParameter("customerId", customerId).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findAllPointOfSales(final List<Integer> customerIds, final List<Integer> locationIds,
        final List<Integer> routeIds, final List<Integer> pointOfSaleIds) {
        final Criteria query = createAllPosCriteria(customerIds, locationIds, routeIds, pointOfSaleIds);
        query.setFetchMode("posStatus", FetchMode.JOIN);
        
        return (List<PointOfSale>) query.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Integer> findAllIds(final List<Integer> customerIds, final List<Integer> locationIds, final List<Integer> routeIds) {
        return (List<Integer>) createAllPosCriteria(customerIds, locationIds, routeIds, (List<Integer>) null).setProjection(Projections
                                                                                                                                    .property("id"))
                .list();
    }
    
    private Criteria createAllPosCriteria(final List<Integer> customerIds, final List<Integer> locationIds, final List<Integer> routeIds,
        final List<Integer> pointOfSaleIds) {
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class).setCacheable(true);
        
        query.createAlias("posStatus", "posStatus").add(Restrictions.eq("posStatus.isDeleted", false));
        
        Criterion containerConditions = null;
        if ((customerIds != null) && (!customerIds.isEmpty())) {
            containerConditions = Restrictions.in("customer.id", customerIds);
        }
        
        if ((locationIds != null) && (!locationIds.isEmpty())) {
            final Criterion locCondition = Restrictions.in("location.id", locationIds);
            if (containerConditions == null) {
                containerConditions = locCondition;
            } else {
                containerConditions = Restrictions.or(containerConditions, locCondition);
            }
        }
        
        if ((routeIds != null) && (!routeIds.isEmpty())) {
            final Criterion routeCondition = Subqueries.propertyIn("id",
                                                                   DetachedCriteria.forClass(RoutePOS.class)
                                                                           .setProjection(Projections.property("pointOfSale.id"))
                                                                           .add(Restrictions.in("route.id", routeIds)));
            if (containerConditions == null) {
                containerConditions = routeCondition;
            } else {
                containerConditions = Restrictions.or(containerConditions, routeCondition);
            }
        }
        
        if ((pointOfSaleIds != null) && (!pointOfSaleIds.isEmpty())) {
            final Criterion posIdCondition = Restrictions.in("id", pointOfSaleIds);
            if (containerConditions == null) {
                containerConditions = posIdCondition;
            } else {
                containerConditions = Restrictions.or(containerConditions, posIdCondition);
            }
        }
        
        if (containerConditions != null) {
            query.add(containerConditions);
        }
        
        return query;
    }
    
    @Override
    public final boolean isPointOfSalesReadyForMigration(final Customer customer, final Set<String> versionSet, final boolean isForMigration) {
        final Calendar last2Weeks = Calendar.getInstance();
        last2Weeks.add(Calendar.DAY_OF_YEAR, -14);
        
        @SuppressWarnings("unchecked")
        final List<PosServiceState> posssList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleForVersionControl",
                                                                                             new String[] { "customerId", "last2Weeks" },
                                                                                             new Object[] { customer.getId(), last2Weeks.getTime() });
        
        if (posssList != null) {
            for (PosServiceState posss : posssList) {
                if (!versionSet.contains(posss.getPrimaryVersion())) {
                    return false;
                }
            }
        }
        if (isForMigration) {
            for (PosServiceState posss : posssList) {
                CustomerMigrationOnlinePOS customerMigrationOnlinePOS = (CustomerMigrationOnlinePOS) this.entityDao
                        .findUniqueByNamedQueryAndNamedParam("CustomerMigrationOnlinePOS.findByPointOfSaleId", "pointOfSaleId", posss
                                .getPointOfSale().getId());
                if (customerMigrationOnlinePOS == null) {
                    customerMigrationOnlinePOS = new CustomerMigrationOnlinePOS();
                    customerMigrationOnlinePOS.setCustomer(customer);
                    customerMigrationOnlinePOS.setPointOfSale(posss.getPointOfSale());
                    this.entityDao.save(customerMigrationOnlinePOS);
                }
            }
        }
        return true;
    }
    
    @Override
    public final List<PointOfSale> findPointOfSaleNotReadyForMigration(final Integer customerId, final Set<String> versionSet) {
        final Calendar last2Weeks = Calendar.getInstance();
        last2Weeks.add(Calendar.DAY_OF_YEAR, -14);
        @SuppressWarnings("unchecked")
        final List<PosServiceState> posssList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleForVersionControl",
                                                                                             new String[] { "customerId", "last2Weeks" },
                                                                                             new Object[] { customerId, last2Weeks.getTime() });
        
        final List<PointOfSale> pointOfSaleList = new ArrayList<PointOfSale>();
        if (posssList != null) {
            for (PosServiceState posss : posssList) {
                if (!versionSet.contains(posss.getPrimaryVersion())) {
                    pointOfSaleList.add(posss.getPointOfSale());
                }
            }
        }
        return pointOfSaleList;
    }
    
    @Override
    public final List<PaystationListInfo> findPaystation(final PointOfSaleSearchCriteria criteria) {
        final Criteria posQuery = createPayStationSearchQuery(criteria);
        posQuery.addOrder(Order.desc("pStatus.isActivated")).addOrder(Order.asc("name").ignoreCase());
        
        @SuppressWarnings("unchecked")
        final List<PointOfSale> posList = posQuery.list();
        
        final Map<Integer, PaystationListInfo> posMap = new HashMap<Integer, PaystationListInfo>(posList.size() * 2);
        final List<PaystationListInfo> result = new ArrayList<PaystationListInfo>(posList.size());
        for (PointOfSale pos : posList) {
            final PaystationListInfo posInfo = new PaystationListInfo();
            posInfo.setRandomPosId(new WebObjectId(PointOfSale.class, pos.getId()));
            posInfo.setRandomLocationId(new WebObjectId(Location.class, pos.getLocation().getId()));
            posInfo.setName(pos.getName());
            posInfo.setSerialNumber(pos.getSerialNumber());
            
            posInfo.setIsPosActivated(pos.getPosStatus().isIsActivated());
            posInfo.setIsPosDecommissioned(pos.getPosStatus().isIsDecommissioned());
            posInfo.setIsPosHidden(!pos.getPosStatus().isIsVisible());
            posInfo.setPayStationType(pos.getPaystation().getPaystationType().getId());
            
            final int criticalCount = pos.getPosAlertStatus().getCommunicationCritical() + pos.getPosAlertStatus().getCollectionCritical()
                                      + pos.getPosAlertStatus().getPayStationCritical();
            final int majorCount = pos.getPosAlertStatus().getCommunicationMajor() + pos.getPosAlertStatus().getCollectionMajor()
                                   + pos.getPosAlertStatus().getPayStationMajor();
            final int minorCount = pos.getPosAlertStatus().getCommunicationMinor() + pos.getPosAlertStatus().getCollectionMinor()
                                   + pos.getPosAlertStatus().getPayStationMinor();
            
            posInfo.setAlertCount(criticalCount + majorCount + minorCount);
            posInfo.setAlertSeverity(criticalCount > 0 ? WebCoreConstants.SEVERITY_CRITICAL : majorCount > 0 ? WebCoreConstants.SEVERITY_MAJOR
                    : minorCount > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR);
            
            posMap.put(pos.getId(), posInfo);
            result.add(posInfo);
        }
        
        final Criteria routePosQuery = this.entityDao.createCriteria(RoutePOS.class).setCacheable(true);
        routePosQuery.createCriteria("pointOfSale", "pos").createCriteria("pos.posStatus", "pStatus");
        if (criteria.getCustomerId() != null) {
            routePosQuery.add(Restrictions.eq("pos.customer.id", criteria.getCustomerId()));
        }
        
        if (criteria.getLocationId() != null) {
            routePosQuery.add(Restrictions.eq("pos.location.id", criteria.getLocationId()));
        }
        
        if (criteria.getRouteId() != null) {
            routePosQuery.add(Restrictions.eq("route.id", criteria.getRouteId()));
        }
        
        if (!criteria.isShowDeactivated()) {
            routePosQuery.add(Restrictions.eq("pStatus.isActivated", true));
        }
        
        if (!criteria.isShowDecommissioned()) {
            routePosQuery.add(Restrictions.eq("pStatus.isDecommissioned", false));
        }
        
        if (!criteria.isShowHidden()) {
            routePosQuery.add(Restrictions.eq("pStatus.isVisible", true));
        }
        
        @SuppressWarnings("unchecked")
        final List<RoutePOS> routePosList = routePosQuery.list();
        for (RoutePOS routePos : routePosList) {
            final PaystationListInfo posInfo = posMap.get(routePos.getPointOfSale().getId());
            if (posInfo != null) {
                posInfo.addRandomRouteId(new WebObjectId(Route.class, routePos.getRoute().getId()));
            }
        }
        
        posMap.clear();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PayStationSelectorDTO> findPayStationSelector(final PointOfSaleSearchCriteria criteria) {
        List<Integer> selectedPosIdsByLoc = new ArrayList<Integer>(1);
        if (criteria.getSelectedLocationId() != null) {
            selectedPosIdsByLoc = (List<Integer>) this.entityDao.getNamedQuery("PointOfSale.findPosIdsByLocationIds")
                    .setParameter("locationIds", criteria.getSelectedLocationId()).list();
        }
        
        List<Integer> selectedPosIdsByRoute = new ArrayList<Integer>(1);
        if (criteria.getSelectedRouteId() != null) {
            selectedPosIdsByRoute = (List<Integer>) this.entityDao.getNamedQuery("PointOfSale.findPosIdsByRouteIds")
                    .setParameter("routeIds", criteria.getSelectedRouteId()).list();
        }
        
        final Set<Integer> selectedPOS = new HashSet<Integer>((selectedPosIdsByLoc.size() + selectedPosIdsByRoute.size()) * 2);
        selectedPOS.addAll(selectedPosIdsByLoc);
        selectedPOS.addAll(selectedPosIdsByRoute);
        
        final Criteria posQuery = createPayStationSearchQuery(criteria);
        posQuery.addOrder(Order.asc("name").ignoreCase());
        
        final List<PointOfSale> posList = posQuery.list();
        
        final List<PayStationSelectorDTO> result = new ArrayList<PayStationSelectorDTO>(posList.size());
        for (PointOfSale pos : posList) {
            final PayStationSelectorDTO dto = new PayStationSelectorDTO();
            dto.setName(pos.getName());
            dto.setSerial(pos.getSerialNumber());
            dto.setRandomId(new WebObjectId(PointOfSale.class, pos.getId()));
            dto.setStatus(selectedPOS.contains(pos.getId()) ? "selected" : "active");
            
            result.add(dto);
        }
        
        return result;
    }
    
    @Override
    public final int findPayStationPage(final Integer posId, final PointOfSaleSearchCriteria criteria) {
        int result = -1;
        
        final PointOfSale endPos = this.entityDao.get(PointOfSale.class, posId);
        
        final Criteria query = createPayStationSearchQuery(criteria);
        query.addOrder(Order.desc("pStatus.isActivated")).addOrder(Order.asc("name").ignoreCase());
        query.add(Restrictions.or(Restrictions.gt("pStatus.isActivated", endPos.getPosStatus().isIsActivated()),
                                  Restrictions.and(Restrictions.eq("pStatus.isActivated", endPos.getPosStatus().isIsActivated()),
                                                   Restrictions.le("name", endPos.getName()).ignoreCase())));
        
        query.setProjection(Projections.count("id"));
        
        final double recordNumber = ((Number) query.uniqueResult()).doubleValue();
        if ((recordNumber > 0) && (criteria.getItemsPerPage() != null)) {
            result = (int) Math.ceil(recordNumber / criteria.getItemsPerPage().doubleValue());
        }
        
        return result;
    }
    
    private Criteria createPayStationSearchQuery(final PointOfSaleSearchCriteria criteria) {
        final Criteria posQuery = this.entityDao.createCriteria(PointOfSale.class).setCacheable(true);
        posQuery.createCriteria("location", "loc").setFetchMode("location", FetchMode.JOIN);
        posQuery.createCriteria("posStatus", "pStatus").setFetchMode("posStatus", FetchMode.JOIN);
        posQuery.createCriteria("posAlertStatus", "pAlertStatus").setFetchMode("posAlertStatus", FetchMode.JOIN);
        posQuery.createCriteria("posBalance", "pBal").setFetchMode("posBalance", FetchMode.JOIN);
        posQuery.createCriteria("posHeartbeat", "pHeartbeat").setFetchMode("posHeartbeat", FetchMode.JOIN);
        posQuery.createCriteria("posServiceState", "pServState").setFetchMode("posServiceState", FetchMode.JOIN);
        posQuery.createAlias("posMacAddress", "pMac", Criteria.LEFT_JOIN);
        
        if (criteria.getCustomerId() != null) {
            posQuery.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        }
        
        if (criteria.getPointOfSaleId() != null) {
            posQuery.add(Restrictions.eq("id", criteria.getPointOfSaleId()));
        } else {
            if (criteria.getLocationId() != null) {
                posQuery.add(Restrictions.eq("location.id", criteria.getLocationId()));
            }
            
            if (criteria.getRouteId() != null) {
                posQuery.add(Subqueries.propertyIn("id",
                                                   DetachedCriteria.forClass(RoutePOS.class).setProjection(Projections.property("pointOfSale.id"))
                                                           .add(Restrictions.eq("route.id", criteria.getRouteId()))));
            }
        }
        
        if (!criteria.isShowDeactivated()) {
            posQuery.add(Restrictions.eq("pStatus.isActivated", true));
        }
        
        if (!criteria.isShowDecommissioned()) {
            posQuery.add(Restrictions.eq("pStatus.isDecommissioned", false));
        }
        
        if (!criteria.isShowHidden()) {
            posQuery.add(Restrictions.eq("pStatus.isVisible", true));
        }
        
        posQuery.add(Restrictions.eq("pStatus.isDeleted", false));
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            final int page = criteria.getPage();
            final int itemsPerPage = criteria.getItemsPerPage();
            final int firstResult = (page - 1) * itemsPerPage;
            if (criteria.getMaxUpdatedTime() == null) {
                criteria.setMaxUpdatedTime(new Date());
            }
            
            posQuery.add(Restrictions.lt("lastModifiedGmt", criteria.getMaxUpdatedTime()));
            posQuery.setFirstResult(firstResult);
            posQuery.setMaxResults(criteria.getItemsPerPage());
        }
        
        return posQuery;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> findPointOfSaleBySerialNumberNoConditions(final String serialNumber) {
        return this.entityDao.getNamedQuery("PointOfSale.findPointOfSaleBySerialNumberNoConditions").setParameter("serialNumber", serialNumber)
                .list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSale> getDetachedPointOfSaleByLowerCaseNames(final int customerId, final Collection<String> names,
        final String locPosNameSeparator) {
        List<PointOfSale> result = null;
        
        if (locPosNameSeparator == null) {
            result = this.entityDao.getNamedQuery("PointOfSale.findPointOfSaleByNames").setParameter("customerId", customerId)
                    .setParameterList("pointOfSaleNames", names).list();
        } else {
            result = this.entityDao.getNamedQuery("PointOfSale.findPointOfSaleByLocPosNames").setParameter("customerId", customerId)
                    .setParameterList("pointOfSaleNames", names).setParameter("locPosSeparator", locPosNameSeparator).list();
        }
        
        this.entityDao.getCurrentSession().clear();
        
        return result;
    }
    
    /**
     * Returns information about a pay station which has had a heartbeat or a collection since the provided date.
     */
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSaleAlertInfo> findActivePointOfSalesWithRecentHeartbeatOrCollection(final Integer customerId, final Integer routeId,
        final Date sinceDate) {
        //TODO method under development for Mobile Digital Collect API
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class).createAlias("posStatus", "posStatus")
                .createAlias("posHeartbeat", "heartBeat").createAlias("posBalance", "posBalance");
        
        if (routeId != null && routeId != 0) {
            query.createAlias("routePOSes", "rPOS");
            query.add(Restrictions.eq("rPOS.route.id", routeId));
        }
        query.add(Restrictions.eq("customer.id", customerId));
        if (sinceDate != null) {
            final Criterion lastCollectionCriterion = Restrictions.disjunction().add(Restrictions.ge("posBalance.lastCoinCollectionGmt", sinceDate))
                    .add(Restrictions.ge("posBalance.lastBillCollectionGmt", sinceDate))
                    .add(Restrictions.ge("posBalance.lastCardCollectionGmt", sinceDate));
            
            query.add(Restrictions.or(lastCollectionCriterion, Restrictions.ge("heartBeat.lastHeartbeatGmt", sinceDate)));
        }
        
        query.setProjection(Projections.projectionList().add(Projections.property("id").as("id")).add(Projections.property("name").as("name"))
                .add(Projections.property("serialNumber").as("serialNumber")).add(Projections.property("heartBeat.lastHeartbeatGmt").as("heartBeat"))
                .add(Projections.property("posBalance.lastCoinCollectionGmt").as("lastCoinCollection"))
                .add(Projections.property("posBalance.lastBillCollectionGmt").as("lastBillCollection"))
                .add(Projections.property("posBalance.lastCardCollectionGmt").as("lastCardCollection")));
        
        query.setResultTransformer(new PointOfSaleAlertInfoTransformer());
        return query.list();
    }
    
    /**
     * Returns information about a paystation and related alerts that meet these conditions:
     * 1) customerId matches, route matches, paystation is active and in use by the customer.
     * 2) there is a collection alert that has been created or cleared since the provided date.
     * 
     * @param sinceDate
     *            - If null, will look for any eligible alert updates regardless of time.
     *            Otherwise will restrict the search to alerts which have changed status since that time.
     * @param onlyActive
     *            - set to true if only looking for new, active alerts.
     * @param onlyInActive
     *            - set to true if only looking for newly cleared, inactive alerts.
     */
    @SuppressWarnings("unchecked")
    @Override
    public final List<PointOfSaleAlertInfo> findActivePointOfSalesWithCollectionAlertsInfo(final Integer customerId, final Integer routeId,
        final Date sinceDate, final Integer severity, final boolean onlyActive, final boolean onlyInactive) {
        //TODO method under development for Mobile Collect API - Status
        final List<Integer> alertTypes = getCollectionAlertTypes();
        final Criteria query = this.entityDao.createCriteria(PointOfSale.class).createAlias("posEventCurrents", "alerts")
                .createAlias("alerts.customerAlertType", "alertType").createAlias("alerts.eventSeverityType", "severity")
                .createAlias("posStatus", "posStatus").createAlias("alertType.alertThresholdType", "thresholdType")
                .createAlias("posHeartbeat", "heartBeat").createAlias("posBalance", "posBalance").createAlias("routePOSes", "rPOS");
        if (routeId != null && routeId != 0) {
            query.add(Restrictions.eq("rPOS.route.id", routeId));
        }
        addPosStatusRestrictions(query);
        query.add(Restrictions.eq("customer.id", customerId));
        final Junction alertCriterion = Restrictions.conjunction().add(Restrictions.in("thresholdType.id", alertTypes))
                .add(Restrictions.ge("severity.id", severity)).add(Restrictions.eq("alertType.isDeleted", false));
        
        Criterion alertActiveCriterion = null;
        if (onlyActive) {
            alertActiveCriterion = Restrictions.eq("alerts.isActive", true);
            if (sinceDate != null) {
                alertActiveCriterion = Restrictions.and(alertActiveCriterion, Restrictions.ge("alerts.alertGmt", sinceDate));
            }
        } else if (onlyInactive) {
            alertActiveCriterion = Restrictions.eq("alerts.isActive", false);
            if (sinceDate != null) {
                alertActiveCriterion = Restrictions.and(alertActiveCriterion, Restrictions.ge("alerts.clearedGmt", sinceDate));
            }
        } else {
            alertActiveCriterion = Restrictions.or(Restrictions.ge("alerts.alertGmt", sinceDate), Restrictions.ge("alerts.clearedGmt", sinceDate));
        }
        alertCriterion.add(alertActiveCriterion);
        
        query.add(alertCriterion);
        
        query.setProjection(Projections.projectionList().add(Projections.property("id").as("id")).add(Projections.property("name").as("name"))
                .add(Projections.property("serialNumber").as("serialNumber")).add(Projections.property("heartBeat.lastHeartbeatGmt").as("heartBeat"))
                .add(Projections.property("thresholdType.alertType.id").as("typeId")).add(Projections.property("severity.id").as("severity"))
                .add(Projections.property("alerts.alertGmt").as("created")).add(Projections.property("alerts.clearedGmt").as("cleared"))
                .add(Projections.property("alerts.isActive").as("isActive"))
                .add(Projections.property("posBalance.lastCoinCollectionGmt").as("lastCoinCollection"))
                .add(Projections.property("posBalance.lastBillCollectionGmt").as("lastBillCollection"))
                .add(Projections.property("posBalance.lastCardCollectionGmt").as("lastCardCollection")));
        query.setResultTransformer(new PointOfSaleAlertInfoTransformer());
        return query.list();
    }
    
    @Override
    public final long countPointOfSalesRequiringCollections(final Integer routeId, final int severity) {
        long result = 0;
        final List<Integer> alertTypes = getCollectionAlertTypes();
        @SuppressWarnings("unchecked")
        final List<Object> queryResult = this.entityDao
                .findByNamedQueryAndNamedParam("PointOfSale.countPointOfSaleWithActiveAlertByAlertTypeAndSeverityAndRouteId", new String[] {
                    "alertTypes", "routeId", "severity" }, new Object[] { alertTypes, routeId, severity }, true);
        if (queryResult == null || queryResult.size() == 0) {
            return 0;
        }
        result = (Long) queryResult.get(0);
        return result;
    }
    
    /**
     * Adds restrictions to a query such that the only points of sale that are visible and active (etc) are included in the result.
     * This method requires that the query has an association to "posStatus" aliased as "posStatus"
     * 
     * @param query
     */
    private void addPosStatusRestrictions(final Criteria query) {
        query.add(Restrictions.eq("posStatus.isDeleted", false)).add(Restrictions.eq("posStatus.isDecommissioned", false))
                .add(Restrictions.eq("posStatus.isVisible", true)).add(Restrictions.eq("posStatus.isActivated", true))
                .add(Restrictions.eq("posStatus.isProvisioned", true));
    }
    
    private List<Integer> getCollectionAlertTypes() {
        final List<Integer> alertTypes = new LinkedList<Integer>();
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
        alertTypes.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
        return alertTypes;
    }
    
    @Override
    public final int findPayStationCountByCustomerId(final Integer customerId) {
        final List<Object> response = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.countPointOfSaleByCustomerId", "customerId",
                                                                                   customerId);
        
        if (response == null || response.isEmpty()) {
            return 0;
        }
        final int result = ((Long) response.get(0)).intValue();
        return result;
    }

    @Override
    public final List<PointOfSale> findLinuxPointOfSalesByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findLinuxPointOfSalesByCustomerId", 
                                                            new String[] { "customerId" },
                                                            new Object[] { customerId });
    }
	
	@SuppressWarnings("unchecked")
    @Override
	public  List<PointOfSale> findAllPointOfSalesByPosNameForCustomer(String name, Integer customerId) {
    	return this.entityDao.getNamedQuery("PointOfSale.findAllPointOfSalesByPosNameKeywordAndCustomer")
	                .setParameter("keyword", name).setParameter("customerId", customerId).list();
	}
}
