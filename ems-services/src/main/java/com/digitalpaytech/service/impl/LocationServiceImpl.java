package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.DayOfWeek;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationDay;
import com.digitalpaytech.domain.LocationOpen;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.QuarterHour;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.FilterDTOTransformer;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.LocationTreeCustomerTransformer;
import com.digitalpaytech.dto.customeradmin.LocationTreePointOfSaleTransformer;
import com.digitalpaytech.dto.customeradmin.LocationTreeRouteTransformer;
import com.digitalpaytech.dto.customeradmin.LocationTreeTransformer;
import com.digitalpaytech.service.ExtensibleRateDayOfWeekService;
import com.digitalpaytech.service.ExtensibleRateService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.ParkingPermissionDayOfWeekService;
import com.digitalpaytech.service.ParkingPermissionService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;

@Service("locationService")
@Transactional(propagation = Propagation.REQUIRED)
public class LocationServiceImpl implements LocationService {
    private static Logger log = Logger.getLogger(LocationServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private UnifiedRateService unifiedRateService;
    
    @Autowired
    private ExtensibleRateService extensibleRateService;
    
    @Autowired
    private ExtensibleRateDayOfWeekService extensibleRateDayOfWeekService;
    
    @Autowired
    private ParkingPermissionService parkingPermissionService;
    
    @Autowired
    private ParkingPermissionDayOfWeekService parkingPermissionDayOfWeekService;
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Location> findLocationsByCustomerId(final Integer customerId) {
        
        final SQLQuery query = this.entityDao
                .createSQLQuery("SELECT * FROM Location WHERE CustomerId = :customerId AND IsDeleted = 0 ORDER BY IsUnassigned, Name+0<>0 DESC, Name+0, Name");
        query.setParameter("customerId", customerId);
        query.addEntity(Location.class);
        query.setCacheable(true);
        return query.list();
    }
    
    @Override
    public void deleteLocationById(final int locationId) {
        final Location location = (Location) entityDao.get(Location.class, locationId);
        location.setIsDeleted(true);
        entityDao.save(location);
        //entityDao.delete(entityDao.get(Location.class, locationId));
        
    }
    
    @Override
    public Location findLocationById(final int locationId) {
        return (Location) entityDao.get(Location.class, locationId);
    }
    
    @Override
    public List<Integer> findAllParentIds(final int locationId) {
        final ArrayList<Integer> result = new ArrayList<Integer>();
        
        Location loc = null;
        Integer parentLocId = locationId;
        while(parentLocId != null) {
            loc = (Location) this.entityDao.getNamedQuery("Location.findByIdFetchParent")
                    .setParameter("locationId", locationId)
                    .uniqueResult();
            if(loc != null) {
                result.add(loc.getId());
                if(loc.getLocation() == null) {
                    parentLocId = null;
                }
                else {
                    parentLocId = loc.getLocation().getId();
                    result.add(parentLocId);
                    if(loc.getLocation().getLocation() == null) {
                        parentLocId = null;
                    }
                    else {
                        parentLocId = loc.getLocation().getLocation().getId();
                        result.add(parentLocId);
                    }
                }
            }
        }
        
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Location> getLocationsByParentLocationId(final int parentLocationId) {
        return entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByParentId", "parentLocationId", parentLocationId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public boolean locationContainsPermitsById(final int locationId) {
        
        final List<ActivePermit> permitList = entityDao.findByNamedQueryAndNamedParam("ActivePermit.findLatestExpiredPermitByLocationId", "locationId",
                                                                                      locationId);
        if (permitList == null || permitList.isEmpty() || permitList.get(0).getPermitExpireGmt().before(new Date())) {
            return false;
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void updateLocationOpenCloseTime(final int userAccountId, final int locationId, int dayOfWeek, Integer openTime, Integer closeTime,
                                            Integer closeTimeOfPreviousDay, final boolean open, final boolean closed) {
        
        final String[] paramNames = { "locationId", "dayOfWeekId" };
        final Integer[] values = { locationId, dayOfWeek };
        
        LocationDay locationDay;
        final List<LocationDay> locationDays = entityDao.findByNamedQueryAndNamedParam("LocationDay.findLocationDayByLocationIdAndDayOfWeek", paramNames,
                                                                                       values);
        if (locationDays == null || locationDays.isEmpty()) {
            //locationDays will be null for new locations
            locationDay = new LocationDay();
            locationDay.setLocation((Location) entityDao.get(Location.class, locationId));
            locationDay.setDayOfWeek((DayOfWeek) entityDao.get(DayOfWeek.class, dayOfWeek));
        } else {
            locationDay = locationDays.get(0);
        }
        
        if (openTime == null || openTime == -1) {
            openTime = 0;
        }
        if (closeTime == null || closeTime == -1) {
            closeTime = 0;
        }
        if (closeTimeOfPreviousDay == null || closeTimeOfPreviousDay < 96) {
            closeTimeOfPreviousDay = 0;
        } else {
            closeTimeOfPreviousDay = ++closeTimeOfPreviousDay % 96;
        }
        locationDay.setOpenQuarterHourNumber(openTime);
        locationDay.setCloseQuarterHourNumber(closeTime);
        locationDay.setIsOpen24hours(open);
        locationDay.setIsClosed(closed);
        
        locationDay.setLastModifiedGmt(new Date());
        locationDay.setLastModifiedByUserId(userAccountId);
        entityDao.saveOrUpdate(locationDay);
        
        List<LocationOpen> locationOpen = entityDao.findByNamedQueryAndNamedParam("LocationOpen.findLocationOpenByLocationIdDayOfWeekId", paramNames, values);
        if (locationOpen == null || locationOpen.isEmpty()) {
            locationOpen = createLocationOpen(locationId, dayOfWeek);
        }
        
        for (int i = closeTimeOfPreviousDay; i < locationOpen.size(); i++) {
            LocationOpen locationOpenInterval = locationOpen.get(i);
            if (open) {
                locationOpenInterval.setIsOpen(true);
            } else if (closed) {
                locationOpenInterval.setIsOpen(false);
            } else if (locationOpenInterval.getQuarterhour().getId() >= openTime && locationOpenInterval.getQuarterhour().getId() <= closeTime) {
                locationOpenInterval.setIsOpen(true);
            } else {
                locationOpenInterval.setIsOpen(false);
            }
        }
        
        for (LocationOpen locationOpenInterval : locationOpen) {
            locationOpenInterval.setLastModifiedGmt(new Date());
            locationOpenInterval.setLastModifiedByUserId(userAccountId);
            entityDao.saveOrUpdate(locationOpenInterval);
        }
        
        //If the closeTime is past midnight
        if (closeTime > 95) {
            closeTime = closeTime % 96;
            openTime = 0;
            dayOfWeek = (dayOfWeek) % 7 + 1;
            Integer[] values2 = { locationId, dayOfWeek };
            locationOpen = entityDao.findByNamedQueryAndNamedParam("LocationOpen.findLocationOpenByLocationIdDayOfWeekId", paramNames, values2);
            if (locationOpen == null || locationOpen.isEmpty()) {
                locationOpen = createLocationOpen(locationId, dayOfWeek);
            }
            
            for (int i = 0; i <= closeTime; i++) {
                LocationOpen locationOpenInterval = locationOpen.get(i);
                if (locationOpenInterval.getQuarterhour().getId() >= openTime && locationOpenInterval.getQuarterhour().getId() <= closeTime) {
                    locationOpenInterval.setIsOpen(true);
                } else {
                    locationOpenInterval.setIsOpen(false);
                }
            }
            
            for (LocationOpen locationOpenInterval : locationOpen) {
                locationOpenInterval.setLastModifiedGmt(new Date());
                locationOpenInterval.setLastModifiedByUserId(userAccountId);
                entityDao.saveOrUpdate(locationOpenInterval);
            }
            
        }
    }
    
    private List<LocationOpen> createLocationOpen(final Integer locationId, final Integer dayOfWeek) {
        //locationOpen will be null for new locations
        final List<LocationOpen> locationOpen = new ArrayList<LocationOpen>();
        int quarterHourId = 0;
        while (quarterHourId < 96) {
            final LocationOpen locationOpenEntry = new LocationOpen();
            locationOpenEntry.setLocation((Location) entityDao.get(Location.class, locationId));
            locationOpenEntry.setDayOfWeek((DayOfWeek) entityDao.get(DayOfWeek.class, dayOfWeek));
            locationOpenEntry.setQuarterhour((QuarterHour) entityDao.get(QuarterHour.class, quarterHourId));
            locationOpen.add(locationOpenEntry);
            quarterHourId++;
        }
        return locationOpen;
    }
    
    @Override
    public void saveOrUpdateLocation(final Location location) {
        entityDao.saveOrUpdate(location);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<LocationDay> findLocationDayByLocationId(final int id) {
        return entityDao.findByNamedQueryAndNamedParam("LocationDay.findLocationDayByLocationId", "locationId", id);
    }
    
    @Override
    public Location findUnassignedLocationByCustomerId(final int customerId) {
        return (Location) entityDao.findByNamedQueryAndNamedParam("Location.findUnassignedLocationByCustomerId", "customerId", customerId).get(0);
    }
    
    @Override
    public Location findLocationByPointOfSaleId(final int pointOfSaleId) {
        final PointOfSale pointOfSale = (PointOfSale) entityDao.get(PointOfSale.class, pointOfSaleId);
        return pointOfSale.getLocation();
    }
    
    /*
     * @see EMS6.3.11 EmsRateAndPermissionAppServiceImpl#getValidDayOfWeekEMSNormalRatesByRegionId
     */
    @Override
    public List<List<ExtensibleRate>> getValidRatesByLocationId(final Integer locationId) {
        final List<ExtensibleRate> sunday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> monday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> tuesday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> wednesday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> thursday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> friday = new ArrayList<ExtensibleRate>();
        final List<ExtensibleRate> saturday = new ArrayList<ExtensibleRate>();
        final List<List<ExtensibleRate>> dayOfWeeks = new ArrayList<List<ExtensibleRate>>();
        
        dayOfWeeks.add(0, sunday);
        dayOfWeeks.add(1, monday);
        dayOfWeeks.add(2, tuesday);
        dayOfWeeks.add(3, wednesday);
        dayOfWeeks.add(4, thursday);
        dayOfWeeks.add(5, friday);
        dayOfWeeks.add(6, saturday);
        
        final List<ExtensibleRateDayOfWeek> extRateDayOfWeeks = getValidRatesDayOfWeekByLocationId(locationId);
        final Iterator<ExtensibleRateDayOfWeek> iter = extRateDayOfWeeks.iterator();
        while (iter.hasNext()) {
            ExtensibleRateDayOfWeek extensibleRateDayOfWeek = iter.next();
            int dayOfWeek = extensibleRateDayOfWeek.getDayOfWeek().getDayOfWeek();
            switch (dayOfWeek) {
                case WebCoreConstants.SUNDAY:
                    sunday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("SUNDAY rate ");
                    break;
                case WebCoreConstants.MONDAY:
                    monday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("MONDAY rate ");
                    break;
                case WebCoreConstants.TUESDAY:
                    tuesday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("TUESDAY rate ");
                    break;
                case WebCoreConstants.WEDNESDAY:
                    wednesday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("WEDNESDAY rate ");
                    break;
                case WebCoreConstants.THURSDAY:
                    thursday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("THURSDAY rate ");
                    break;
                case WebCoreConstants.FRIDAY:
                    friday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("FRIDAY rate ");
                    break;
                case WebCoreConstants.SATURDAY:
                    saturday.add(extensibleRateDayOfWeek.getExtensibleRate());
                    showDebug("SATURDAY rate ");
                    break;
                default:
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("!!! ExtensibleRate id (").append(extensibleRateDayOfWeek.getExtensibleRate().getId()).append(") error defined !!!");
                    log.warn(bdr.toString());
            }
        }
        return dayOfWeeks;
    }
    
    private void showDebug(final String msg) {
        if (log.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Found ").append(msg).append("---");
            log.debug(bdr.toString());
        }
    }
    
    /*
     * @see EMS6.3.11 com.digitalpioneer.service.EmsRateService#getValidEMSNormalRateDayOfWeekByRegionId(int)
     */
    @Override
    public List<ExtensibleRateDayOfWeek> getValidRatesDayOfWeekByLocationId(final Integer locationId) {
        // ExtensibleRateDayOfWeek.findAllValidRateDayOfWeekByLocationId HQL returns 2 objects - ExtensibleRateDayOfWeek & ExtensibleRate
        final List<?> ratesDayOfWeekAndRateObjs = entityDao.findByNamedQueryAndNamedParam("ExtensibleRateDayOfWeek.findAllValidRateDayOfWeekByLocationId",
                                                                                          "locationId", locationId);
        final List<ExtensibleRateDayOfWeek> list = new ArrayList<ExtensibleRateDayOfWeek>(ratesDayOfWeekAndRateObjs.size());
        for (int i = 0; i < ratesDayOfWeekAndRateObjs.size(); i++) {
            addExtensibleRateDayOfWeeksToList((Object[]) ratesDayOfWeekAndRateObjs.get(i), list);
        }
        return list;
    }
    
    private List<ExtensibleRateDayOfWeek> addExtensibleRateDayOfWeeksToList(final Object[] ratesDayOfWeekAndRateObjects,
                                                                            final List<ExtensibleRateDayOfWeek> list) {
        for (int i = 0; i < ratesDayOfWeekAndRateObjects.length; i++) {
            if (ratesDayOfWeekAndRateObjects[i] instanceof ExtensibleRateDayOfWeek) {
                list.add((ExtensibleRateDayOfWeek) ratesDayOfWeekAndRateObjects[i]);
            }
        }
        return list;
    }
    
    /*
     * @see EMS6.3.11 com.digitalpioneer.appservice.customer.EmsRateAndPermissionAppService#getValidDayOfWeekEMSNormalParkingPermissionByRegionId(int)
     */
    @Override
    public List<List<ParkingPermission>> getValidParkingPermissionsByLocationId(final Integer locationId) {
        final List<ParkingPermission> sunday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> monday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> tuesday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> wednesday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> thursday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> friday = new ArrayList<ParkingPermission>();
        final List<ParkingPermission> saturday = new ArrayList<ParkingPermission>();
        final List<List<ParkingPermission>> dayOfWeeks = new ArrayList<List<ParkingPermission>>();
        
        dayOfWeeks.add(0, sunday);
        dayOfWeeks.add(1, monday);
        dayOfWeeks.add(2, tuesday);
        dayOfWeeks.add(3, wednesday);
        dayOfWeeks.add(4, thursday);
        dayOfWeeks.add(5, friday);
        dayOfWeeks.add(6, saturday);
        
        final List<ParkingPermissionDayOfWeek> permissionDayOfWeeks = getValidParkingPermissionsDayOfWeekByLocationId(locationId);
        final Iterator<ParkingPermissionDayOfWeek> iter = permissionDayOfWeeks.iterator();
        while (iter.hasNext()) {
            final ParkingPermissionDayOfWeek permissionDayOfWeek = iter.next();
            final int dayOfWeek = permissionDayOfWeek.getDayOfWeek().getDayOfWeek();
            switch (dayOfWeek) {
                case WebCoreConstants.SUNDAY:
                    sunday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("SUNDAY permission");
                    break;
                case WebCoreConstants.MONDAY:
                    monday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("MONDAY permission");
                    break;
                case WebCoreConstants.TUESDAY:
                    tuesday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("TUESDAY permission +++");
                    break;
                case WebCoreConstants.WEDNESDAY:
                    wednesday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("WEDNESDAY permission +++");
                    break;
                case WebCoreConstants.THURSDAY:
                    thursday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("THURSDAY permission +++");
                    break;
                case WebCoreConstants.FRIDAY:
                    friday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("FRIDAY permission +++");
                    break;
                case WebCoreConstants.SATURDAY:
                    saturday.add(permissionDayOfWeek.getParkingPermission());
                    showDebug("SATURDAY permission");
                    break;
                default:
                    StringBuilder bdr = new StringBuilder();
                    bdr.append("!!! permission(").append(permissionDayOfWeek.getParkingPermission().getId()).append(") error defined !!!");
                    log.warn(bdr.toString());
            }
            
        }
        return dayOfWeeks;
    }
    
    /*
     * @see EMS6.3.11 com.digitalpioneer.service.Impl.EmsParkingPermissionServiceImpl#getValidEMSNormalParkingPermissionsByRegionId(int)
     */
    @Override
    public List<ParkingPermissionDayOfWeek> getValidParkingPermissionsDayOfWeekByLocationId(final Integer locationId) {
        // ParkingPermissionDayOfWeek.findAllValidParkingPermissionDayOfWeekByLocationId returns 2 objects - ParkingPermissionDayOfWeek & ParkingPermission
        final List<?> permDayOfWeekAndParkPermObjs = entityDao
                .findByNamedQueryAndNamedParam("ParkingPermissionDayOfWeek.findAllValidParkingPermissionDayOfWeekByLocationId", "locationId", locationId);
        final List<ParkingPermissionDayOfWeek> list = new ArrayList<ParkingPermissionDayOfWeek>(permDayOfWeekAndParkPermObjs.size());
        for (int i = 0; i < permDayOfWeekAndParkPermObjs.size(); i++) {
            addParkingPermissionDayOfWeeksToList((Object[]) permDayOfWeekAndParkPermObjs.get(i), list);
        }
        return list;
    }
    
    private List<ParkingPermissionDayOfWeek> addParkingPermissionDayOfWeeksToList(final Object[] permDayOfWeekAndParkPermObjs,
                                                                                  final List<ParkingPermissionDayOfWeek> list) {
        for (int i = 0; i < permDayOfWeekAndParkPermObjs.length; i++) {
            if (permDayOfWeekAndParkPermObjs[i] instanceof ParkingPermissionDayOfWeek) {
                list.add((ParkingPermissionDayOfWeek) permDayOfWeekAndParkPermObjs[i]);
            }
        }
        return list;
    }
    
    @Override
    public ExtensibleRate findExtensibleRate(final int extensibleRateId) {
        return (ExtensibleRate) entityDao.get(ExtensibleRate.class, extensibleRateId);
    }
    
    @Override
    public ExtensibleRate evictExtensibleRate(final ExtensibleRate extensibleRate) {
        entityDao.evict(extensibleRate);
        return extensibleRate;
    }
    
    @Override
    public ParkingPermission findParkingPermission(final int parkingPermissionId) {
        return (ParkingPermission) entityDao.get(ParkingPermission.class, parkingPermissionId);
    }
    
    @Override
    public ParkingPermission evictParkingPermission(final ParkingPermission parkingPermission) {
        entityDao.evict(parkingPermission);
        return parkingPermission;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateExtensibleRate(final UserAccount userAccount, final ExtensibleRate extensibleRate) {
        UnifiedRate uRate = unifiedRateService.findRateByNameAndCustomerId(extensibleRate.getName().toUpperCase(), userAccount.getCustomer().getId());
        if (uRate == null) {
            uRate = unifiedRateService.insert(userAccount, extensibleRate.getName());
            uRate.setExtensibleRates(new HashSet<ExtensibleRate>(2));
        }
        
        extensibleRate.setUnifiedRate(uRate);
        //		uRate.getExtensibleRates().add(extensibleRate);
        
        final HashMap<Integer, ExtensibleRateDayOfWeek> dowMap = new HashMap<Integer, ExtensibleRateDayOfWeek>(extensibleRate.getExtensibleRateDayOfWeeks()
                .size() * 2);
        for (ExtensibleRateDayOfWeek dow : extensibleRate.getExtensibleRateDayOfWeeks()) {
            dowMap.put(dow.getDayOfWeek().getId(), dow);
        }
        
        ExtensibleRate existing = null;
        final Date today = new Date();
        if (extensibleRate.getId() == null) {
            existing = extensibleRate;
            existing.setIsActive(true);
            existing.setCreatedGmt(today);
            existing.setLastModifiedGmt(today);
            existing.setLastModifiedByUserId(userAccount.getId());
            
            entityDao.save(existing);
        } else {
            existing = entityDao.get(ExtensibleRate.class, extensibleRate.getId());
            if (existing == null) {
                throw new IllegalStateException("Could not locate ExtensibleRate with ID: " + extensibleRate.getId());
            }
            
            existing.setIsActive(true);
            existing.setLastModifiedGmt(today);
            existing.setLastModifiedByUserId(userAccount.getId());
            
            existing.setName(extensibleRate.getName());
            
            existing.setExtensibleRateType(extensibleRate.getExtensibleRateType());
            existing.setBeginHourLocal(extensibleRate.getBeginHourLocal());
            existing.setBeginMinuteLocal(extensibleRate.getBeginMinuteLocal());
            existing.setEndHourLocal(extensibleRate.getEndHourLocal());
            existing.setEndMinuteLocal(extensibleRate.getEndMinuteLocal());
            
            existing.setRateAmount(extensibleRate.getRateAmount());
            existing.setServiceFeeAmount(extensibleRate.getServiceFeeAmount());
            existing.setMinExtensionMinutes(extensibleRate.getMinExtensionMinutes());
            
            for (ExtensibleRateDayOfWeek olddow : existing.getExtensibleRateDayOfWeeks()) {
                final ExtensibleRateDayOfWeek newdow = dowMap.remove(olddow.getDayOfWeek().getId());
                if (newdow == null) {
                    entityDao.delete(olddow);
                }
            }
        }
        
        for (Integer dowId : dowMap.keySet()) {
            final ExtensibleRateDayOfWeek newdow = dowMap.get(dowId);
            newdow.setExtensibleRate(existing);
            newdow.setLastModifiedGmt(today);
            newdow.setLastModifiedByUserId(userAccount.getId());
            entityDao.save(newdow);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateParkingPermission(final UserAccount userAccount, final ParkingPermission parkingPermission) {
        final HashMap<Integer, ParkingPermissionDayOfWeek> dowMap = new HashMap<Integer, ParkingPermissionDayOfWeek>(parkingPermission
                .getParkingPermissionDayOfWeeks().size() * 2);
        for (ParkingPermissionDayOfWeek dow : parkingPermission.getParkingPermissionDayOfWeeks()) {
            dowMap.put(dow.getDayOfWeek().getId(), dow);
        }
        
        ParkingPermission existing;
        final Date today = new Date();
        if (parkingPermission.getId() == null) {
            existing = parkingPermission;
            parkingPermission.setIsActive(true);
            parkingPermission.setCreatedGmt(today);
            parkingPermission.setLastModifiedGmt(today);
            parkingPermission.setLastModifiedByUserId(userAccount.getId());
            
            entityDao.save(parkingPermission);
        } else {
            existing = entityDao.get(ParkingPermission.class, parkingPermission.getId());
            if (existing == null) {
                throw new IllegalStateException("Could not locate ParkingPermission with ID: " + parkingPermission.getId());
            }
            
            existing.setIsActive(true);
            existing.setLastModifiedGmt(today);
            existing.setLastModifiedByUserId(userAccount.getId());
            
            existing.setName(parkingPermission.getName());
            existing.setIsActive(parkingPermission.isIsActive());
            existing.setBeginHourLocal(parkingPermission.getBeginHourLocal());
            existing.setBeginMinuteLocal(parkingPermission.getBeginMinuteLocal());
            existing.setEndHourLocal(parkingPermission.getEndHourLocal());
            existing.setEndMinuteLocal(parkingPermission.getEndMinuteLocal());
            existing.setIsLimitedOrUnlimited(parkingPermission.isIsLimitedOrUnlimited());
            existing.setMaxDurationMinutes(parkingPermission.getMaxDurationMinutes());
            existing.setParkingPermissionType(parkingPermission.getParkingPermissionType());
            existing.setSpecialPermissionDate(parkingPermission.getSpecialPermissionDate());
            
            for (ParkingPermissionDayOfWeek olddow : existing.getParkingPermissionDayOfWeeks()) {
                final ParkingPermissionDayOfWeek newdow = dowMap.remove(olddow.getDayOfWeek().getId());
                if (newdow == null) {
                    entityDao.delete(olddow);
                }
            }
        }
        
        for (Integer dowId : dowMap.keySet()) {
            final ParkingPermissionDayOfWeek newdow = dowMap.get(dowId);
            newdow.setParkingPermission(existing);
            newdow.setLastModifiedGmt(today);
            newdow.setLastModifiedByUserId(userAccount.getId());
            entityDao.save(newdow);
        }
    }
    
    @Override
    public void deleteExtensibleRateById(final int extensibleRateId) {
        final ExtensibleRate er = findExtensibleRate(extensibleRateId);
        er.setIsActive(false);
        entityDao.update(er);
    }
    
    @Override
    public void deleteLocationOpenByLocationId(final int locationId) {
        entityDao.deleteAllByNamedQuery("LocationOpen.deleteAllByLocationId", new Object[] { locationId });
    }
    
    @Override
    public void deleteParkingPermissionById(final int parkingPermissionId) {
        entityDao.delete(findParkingPermission(parkingPermissionId));
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<ExtensibleRate> findOverlappedExtensibleRate(final ExtensibleRate extensibleRate, final List<Integer> daysOfWeek) {
        final SQLQuery query = entityDao.createSQLQuery(createOverlappedExtensibleRateSQL());
        query.addEntity(ExtensibleRate.class);
        query.setParameter("locationId", extensibleRate.getLocation().getId());
        query.setParameterList("daysOfWeekIds", daysOfWeek);
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<ParkingPermission> findOverlappedParkingPermission(final ParkingPermission parkingPermission, final List<Integer> daysOfWeek) {
        final SQLQuery query = entityDao.createSQLQuery(createOverlappedParkingPermissionSQL());
        query.addEntity(ParkingPermission.class);
        query.setParameter("locationId", parkingPermission.getLocation().getId());
        query.setParameterList("daysOfWeekIds", daysOfWeek);
        return query.list();
    }
    
    /*
     * e.g.
     * select r.*
     * from ExtensibleRate r
     * inner join ExtensibleRateDayOfWeek exdow on r.Id = exdow.ExtensibleRateId
     * where r.LocationId = :locationId
     * and r.ExtensibleRateTypeId = 1
     * and r.IsActive = 1
     * and r.Id != 0
     * and exdow.DayOfWeekId in (:dayOfWeeks_list)
     */
    private String createOverlappedExtensibleRateSQL() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("select  r.* ");
        bdr.append("from    ExtensibleRate r ");
        bdr.append("inner   join ExtensibleRateDayOfWeek exdow on r.Id = exdow.ExtensibleRateId ");
        bdr.append("where   r.LocationId = :locationId  ");
        bdr.append("and     r.ExtensibleRateTypeId = 1 ");
        bdr.append("and     r.IsActive = 1 ");
        bdr.append("and     r.Id != 0 ");
        bdr.append("and     exdow.DayOfWeekId in (:daysOfWeekIds) ");
        return bdr.toString();
    }
    
    /*
     * select p.*
     * from ParkingPermission p
     * inner join ParkingPermissionDayOfWeek dow on p.Id = dow.ParkingPermissionId
     * where p.LocationId =:locationId
     * and p.Id != 0
     * and p.PermissionTypeId in (1, 2)
     * and p.IsActive = 1
     * and dow.DayOfWeekId in (:dayOfWeekIds)
     */
    private String createOverlappedParkingPermissionSQL() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("select p.* ");
        bdr.append("from ParkingPermission p ");
        bdr.append("inner join ParkingPermissionDayOfWeek dow on p.Id = dow.ParkingPermissionId ");
        bdr.append("where p.LocationId =:locationId  ");
        bdr.append("and p.Id != 0 ");
        bdr.append("and p.ParkingPermissionTypeId in (1, 2) ");
        bdr.append("and p.IsActive = 1 ");
        bdr.append("and dow.DayOfWeekId in (:daysOfWeekIds) ");
        return bdr.toString();
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setUnifiedRateService(final UnifiedRateService unifiedRateService) {
        this.unifiedRateService = unifiedRateService;
    }
    
    public void setExtensibleRateService(final ExtensibleRateService extensibleRateService) {
        this.extensibleRateService = extensibleRateService;
    }
    
    public void setParkingPermissionService(final ParkingPermissionService parkingPermissionService) {
        this.parkingPermissionService = parkingPermissionService;
    }
    
    public void setExtensibleRateDayOfWeekService(final ExtensibleRateDayOfWeekService extensibleRateDayOfWeekService) {
        this.extensibleRateDayOfWeekService = extensibleRateDayOfWeekService;
    }
    
    public void setParkingPermissionDayOfWeekService(final ParkingPermissionDayOfWeekService parkingPermissionDayOfWeekService) {
        this.parkingPermissionDayOfWeekService = parkingPermissionDayOfWeekService;
    }
    
    @Override
    public Location findLocationByCustomerIdAndName(final int customerId, final String name) {
        return (Location) entityDao.findUniqueByNamedQueryAndNamedParam("Location.findLocationsByCustomerIdAndName", new String[] { "customerId", "name" },
                                                                        new Object[] { customerId, name }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> findLocationsByCustomerIdAndName(final int customerId, final String name) {
        return entityDao.findByNamedQueryAndNamedParam("Location.findLocationsByCustomerIdAndName", new String[] { "customerId", "name" }, new Object[] {
                customerId, name }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> findChildLocationsByCustomerId(final int customerId) {
        return entityDao.findByNamedQueryAndNamedParam("Location.findChildLocationsByCustomerId", "customerId", customerId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> findParentLocationsByCustomerId(final int customerId) {
        return entityDao.findByNamedQueryAndNamedParam("Location.findParentLocationsByCustomerId", "customerId", customerId, true);
    }
    
    @SuppressWarnings("unchecked")
    protected List<Location> getOrderedChildLocationsByParentLocationAndCustomer(final Integer customerId, final Integer locationId) {
        final SQLQuery query = this.entityDao
                .createSQLQuery("SELECT * FROM Location WHERE CustomerId = :customerId AND ParentLocationId = :locationId AND IsDeleted = 0 ORDER BY IsUnassigned, Name+0<>0 DESC, Name+0, Name");
        query.setParameter("customerId", customerId);
        query.setParameter("locationId", locationId);
        query.setCacheable(true);
        query.addEntity(Location.class);
        return query.list();
    }
    
    @Override
    public List<Location> findLocationsWithPointOfSalesByCustomerId(final Integer customerId) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("SELECT l.* from Location l LEFT JOIN Location l2 on l.Id = l2.ParentLocationId ");
        bdr.append("INNER JOIN PointOfSale pos ON l.Id = pos.LocationId OR l2.Id = pos.LocationId ");
        bdr.append("WHERE l.CustomerId = :customerId AND l.IsDeleted = 0 GROUP BY l.Id ORDER BY l.IsUnassigned, l.Name+0<>0 DESC, l.Name+0, l.Name");
        final SQLQuery query = this.entityDao.createSQLQuery(bdr.toString());
        query.setParameter("customerId", customerId);
        query.addEntity(Location.class);
        @SuppressWarnings("unchecked")
        final List<Location> locationList = query.list();
        
        return locationList;
    }
    
    @Override
    public LocationTree getLocationTreeByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
                                                    final RandomKeyMapping keyMapping) {
        final LocationTree result = new LocationTree();
        
        final Map<Integer, LocationTree> leaveOrganizations = new HashMap<Integer, LocationTree>();
        final Customer customer = this.entityDao.get(Customer.class, customerId);
        buildOrganizationTree(result, leaveOrganizations, keyMapping, customer);
        
        /* The whole method's body is duplicated with "getLocationPayStationTreeByCustomerId". The reason for this duplication is the "locHash". */
        final LocationTreeTransformer transformer = new LocationTreeTransformer(keyMapping, leaveOrganizations);
        
        this.entityDao.getNamedQuery("Location.findLocationTreeByCustomerId").setResultTransformer(transformer)
                .setParameterList("customerId", leaveOrganizations.keySet()).setParameter("showActive", true).setParameter("showDeleted", !showActiveOnly)
                .setParameter("showAssigned", true).setParameter("showUnassigned", !showAssignedOnly).list();
        
        return result;
    }
    
    @Override
    public LocationTree getRoutePayStationTreeByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
                                                           final boolean showVisibleOnly, final RandomKeyMapping keyMapping) {
        final LocationTree route = new LocationTree();
        
        final Map<Integer, LocationTree> leaveOrganizations = new HashMap<Integer, LocationTree>();
        final Customer customer = this.entityDao.get(Customer.class, customerId);
        buildOrganizationTree(route, leaveOrganizations, keyMapping, customer);
        
        final LocationTreeRouteTransformer routeTransformer = new LocationTreeRouteTransformer(keyMapping, leaveOrganizations);
        
        this.entityDao.getNamedQuery("Route.findRouteLocationTreeByCustomerId").setResultTransformer(routeTransformer)
                .setParameterList("customerId", leaveOrganizations.keySet()).list();
        
        this.entityDao.getNamedQuery("RoutePOS.findLocationTreeByCustomerId")
                .setResultTransformer(new LocationTreePointOfSaleTransformer(keyMapping, routeTransformer.getRoutesMap()))
                .setParameterList("customerId", leaveOrganizations.keySet()).setParameter("showActive", true).setParameter("showDeleted", !showActiveOnly)
                .setParameter("showAssigned", true).setParameter("showUnassigned", !showAssignedOnly).setParameter("showVisible", true)
                .setParameter("showHidden", !showVisibleOnly).list();
        
        return route;
    }
    
    @Override
    public LocationTree getLocationPayStationTreeByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
                                                              final boolean showVisibleOnly, final RandomKeyMapping keyMapping) {
        final LocationTree result = new LocationTree();
        
        final Map<Integer, LocationTree> leaveOrganizations = new HashMap<Integer, LocationTree>();
        final Customer customer = this.entityDao.get(Customer.class, customerId);
        buildOrganizationTree(result, leaveOrganizations, keyMapping, customer);
        
        final LocationTreeTransformer transformer = new LocationTreeTransformer(keyMapping, leaveOrganizations);
        
        this.entityDao.getNamedQuery("Location.findLocationTreeByCustomerId").setResultTransformer(transformer)
                .setParameterList("customerId", leaveOrganizations.keySet()).setParameter("showActive", true).setParameter("showDeleted", !showActiveOnly)
                .setParameter("showAssigned", true).setParameter("showUnassigned", !showAssignedOnly).list();
        
        this.entityDao.getNamedQuery("PointOfSale.findLocationTreeByCustomerId")
                .setResultTransformer(new LocationTreePointOfSaleTransformer(keyMapping, transformer.getLocationsMap()))
                .setParameterList("customerId", leaveOrganizations.keySet()).setParameter("showActive", true).setParameter("showDeleted", !showActiveOnly)
                .setParameter("showAssigned", true).setParameter("showUnassigned", !showAssignedOnly).setParameter("showVisible", true)
                .setParameter("showHidden", !showVisibleOnly).list();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> getLowestLocationsByCustomerIdAndName(final int customerId, final String locationName) {
        return entityDao.findByNamedQueryAndNamedParam("Location.findLowestLocationsByCustomerIdAndName", new String[] { "customerId", "locationName" },
                                                       new Object[] { customerId, locationName });
    }
    
    private void buildOrganizationTree(final LocationTree root, final Map<Integer, LocationTree> leaveOrganizations, final RandomKeyMapping keyMapping,
                                       final Customer rootOrganization) {
        if (WebCoreConstants.CUSTOMER_TYPE_PARENT != rootOrganization.getCustomerType().getId()) {
            leaveOrganizations.put(rootOrganization.getId(), root);
        } else {
            final LocationTreeCustomerTransformer customerTransformer = new LocationTreeCustomerTransformer(keyMapping, leaveOrganizations);
            customerTransformer.getParentNodes().put(rootOrganization.getId(), root);
            while (customerTransformer.getParentNodes().size() > 0) {
                final Set<Integer> orgIds = customerTransformer.getParentNodes().keySet();
                customerTransformer.cleareLevel();
                
                this.entityDao.getNamedQuery("Customer.findChildCustomersTreeByCustomerIds").setParameterList("customerIds", orgIds)
                        .setResultTransformer(customerTransformer).list();
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<FilterDTO> getLocationFiltersByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
                                                          final RandomKeyMapping keyMapping) {
        return this.entityDao.getNamedQuery("Location.findLocationFilterByCustomerId")
                .setResultTransformer(new FilterDTOTransformer(keyMapping, Location.class)).setParameter("customerIds", customerId)
                .setParameter("showActive", true).setParameter("showDeleted", !showActiveOnly).setParameter("showAssigned", true)
                .setParameter("showUnassigned", !showAssignedOnly).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> getLocationByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly) {
        return this.entityDao.getNamedQuery("Location.findLocationByCustomerId").setParameter("customerIds", customerId).setParameter("showActive", true)
                .setParameter("showDeleted", !showActiveOnly).setParameter("showAssigned", true).setParameter("showUnassigned", !showAssignedOnly).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Location> getDetachedLocationsByLowerCaseNames(final int customerId, final Collection<String> names) {
        final List<Location> result = this.entityDao.getNamedQuery("Location.findByLowerCaseNames").setParameter("customerId", customerId)
                .setParameterList("locationNames", names).list();
        
        this.entityDao.getCurrentSession().clear();
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Location getParentLocationByCustomerIdLocationName(final int customerId, final String locationName) {
        final List<Location> result = this.entityDao.getNamedQuery("Location.findParentLocationByCustomerIdLocationName")
                .setParameter("customerId", customerId).setParameter("locationName", locationName).list();
        
        // A customer cannot have more than one parent location with same name.
        if (result != null && result.size() == 1) {
            return result.get(0);
        }
        return null;
    }
}
