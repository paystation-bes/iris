package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RestAccountType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.RestAccountTypeService;

@Service("restAccountTypeService")
public class RestAccountTypeServiceImpl implements RestAccountTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<RestAccountType> restAccountTypes;
	private Map<Integer, RestAccountType> restAccountTypesMap;
	private Map<String, RestAccountType> restAccountTypeNamesMap;
	
	
	@Override
	public List<RestAccountType> loadAll() {
		if (restAccountTypes == null) {
			restAccountTypes = entityService.loadAll(RestAccountType.class);
		}
		return restAccountTypes;
	}

	@Override
	public Map<Integer, RestAccountType> getRestAccountTypesMap() {
		if (restAccountTypes == null) {
			loadAll();
		}
		if (restAccountTypesMap == null) {
			restAccountTypesMap = new HashMap<Integer, RestAccountType>(restAccountTypes.size());
			Iterator<RestAccountType> iter = restAccountTypes.iterator();
			while (iter.hasNext()) {
				RestAccountType restAccountType = iter.next();
				restAccountTypesMap.put(restAccountType.getId(), restAccountType);
			}
		}
		return restAccountTypesMap;
	}
	
	@Override
	public RestAccountType findRestAccountType(String name) {
        if (restAccountTypes == null) {
            loadAll();
            
            restAccountTypeNamesMap = new HashMap<String, RestAccountType>(restAccountTypes.size());
            Iterator<RestAccountType> iter = restAccountTypes.iterator();
            while (iter.hasNext()) {
                RestAccountType restAccountType = iter.next();
                restAccountTypeNamesMap.put(restAccountType.getName().toLowerCase(), restAccountType);
            }
        }
        return restAccountTypeNamesMap.get(name.toLowerCase());
	}

	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	@Override
	public String getText(int restAccountTypeId) {
		if (restAccountTypesMap == null)
			getRestAccountTypesMap();
		
		if (!restAccountTypesMap.containsKey(restAccountTypeId))
			return null;
		else
			return restAccountTypesMap.get(restAccountTypeId).getName();
	}
}
