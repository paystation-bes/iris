package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.EntityService;

@Service("cardTypeService")
public class CardTypeServiceImpl implements CardTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<CardType> cardTypes;
	private Map<Integer, CardType> cardTypesMap;
	private Map<String, CardType> cardTypeNamesMap;
	
	
	@Override
	public List<CardType> loadAll() {
		if (cardTypes == null) {
			cardTypes = entityService.loadAll(CardType.class);
		}
		return cardTypes;
	}

	@Override
	public Map<Integer, CardType> getCardTypesMap() {
		if (cardTypes == null) {
			loadAll();
		}
		if (cardTypesMap == null) {
			cardTypesMap = new HashMap<Integer, CardType>(cardTypes.size());
			Iterator<CardType> iter = cardTypes.iterator();
			while (iter.hasNext()) {
				CardType cardType = iter.next();
				cardTypesMap.put(cardType.getId(), cardType);
			}
		}
		return cardTypesMap;
	}

	
	@Override
	public CardType findByName(String name) {
        if (cardTypes == null) {
            loadAll();
        }
	    if (cardTypeNamesMap == null) {
	        cardTypeNamesMap = new HashMap<String, CardType>(cardTypes.size());
            Iterator<CardType> iter = cardTypes.iterator();
            while (iter.hasNext()) {
                CardType cardType = iter.next();
                cardTypeNamesMap.put(cardType.getName(), cardType);
            }
	    }
	    return cardTypeNamesMap.get(name);
	}
	
	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }
}
