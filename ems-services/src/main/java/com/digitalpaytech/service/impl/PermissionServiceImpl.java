package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.service.PermissionService;

@Service("permissionService")
@Transactional(propagation = Propagation.REQUIRED)
public class PermissionServiceImpl implements PermissionService {
    
    @Autowired
    private EntityDao entityDao;
    
    private static final String FIND_ALL_PERMISSION_BY_ROLE_ID_SQL = "SELECT r.Id as RoleId, r.Name AS RoleName, rst.Id as Status, p1.Id as ParentPermissionId, p1.Name as ParentPermissionName, p.Id as ChildPermissionId, p.Name as ChildPermissionName from Role r INNER JOIN RoleStatusType rst ON rst.Id = r.RoleStatusTypeId INNER JOIN RolePermission rp ON r.Id = rp.RoleId INNER JOIN Permission p ON p.Id = rp.PermissionId INNER JOIN Permission p1 ON p1.Id = p.ParentPermissionId WHERE r.Id = :roleId AND rst.Id != 2 ORDER BY r.Name, p1.Id, p.Id";
    private static final String FIND_ALL_PERMISSION_BY_CUSTOMER_TYPE_ID_SQL = "SELECT DISTINCT p1.Id as ParentPermissionId, p1.Name as ParentPermissionName, p.Id as ChildPermissionId, p.Name as ChildPermissionName from Role r INNER JOIN RolePermission rp on r.Id = rp.RoleId INNER JOIN Permission p on p.Id = rp.PermissionId INNER JOIN Permission p1 on p1.Id = p.ParentPermissionId WHERE r.CustomerTypeId = :customerTypeId ORDER BY p1.Id, p.Id";
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<RolePermissionInfo> findAllPermissionsByRoleId(Integer roleId) {
        
        SQLQuery query = entityDao.createSQLQuery(FIND_ALL_PERMISSION_BY_ROLE_ID_SQL);
        query.setParameter("roleId", roleId);
        query.addScalar("RoleId", new IntegerType());
        query.addScalar("RoleName", new StringType());
        query.addScalar("Status", new IntegerType());
        query = setCommonRolePermissionInfoScalar(query);
        
        return query.list();
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
        
        SQLQuery query = entityDao.createSQLQuery(FIND_ALL_PERMISSION_BY_CUSTOMER_TYPE_ID_SQL);
        query.setParameter("customerTypeId", customerTypeId);
        query = setCommonRolePermissionInfoScalar(query);
        
        return query.list();
    }
    
    private SQLQuery setCommonRolePermissionInfoScalar(SQLQuery query) {
        
        query.addScalar("ParentPermissionId", new IntegerType());
        query.addScalar("ParentPermissionName", new StringType());
        query.addScalar("ChildPermissionId", new IntegerType());
        query.addScalar("ChildPermissionName", new StringType());
        query.setResultTransformer(Transformers.aliasToBean(RolePermissionInfo.class));
        query.setCacheable(false);
        
        return query;
    }
    
    @Override
    public Collection<Permission> findRolePermissionsByRoleId(Integer roleId) {
        return entityDao.findByNamedQueryAndNamedParam("Permission.findByRoleId", "roleId", roleId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Permission> findAllPermissions() {
        return (List<Permission>) entityDao.getNamedQuery("Permission.findAll").list();
    }
}
