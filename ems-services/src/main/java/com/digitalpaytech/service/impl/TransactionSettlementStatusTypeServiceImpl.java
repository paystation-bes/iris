package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.TransactionSettlementStatusType;
import com.digitalpaytech.service.TransactionSettlementStatusTypeService;

@Service("transactionSettlementStatusTypeService")
public class TransactionSettlementStatusTypeServiceImpl implements TransactionSettlementStatusTypeService {
    
    @Autowired
    EntityDao entityDao;    
    
    @Override
    public TransactionSettlementStatusType findById(Integer id) {
        return (TransactionSettlementStatusType) entityDao.findUniqueByNamedQueryAndNamedParam("TransactionSettlementStatusType.findById", new String[]{"id"}, new Object[]{id}, true);
    }
    
}
