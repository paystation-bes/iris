package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.CardIdentity;
import com.digitalpaytech.dto.comparator.CustomerCardComparator;
import com.digitalpaytech.dto.customeradmin.CustomerCardSearchCriteria;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ReflectionUtil;
import com.digitalpaytech.util.WebCoreConstants;

@Service("customerCardService")
public class CustomerCardServiceImpl implements CustomerCardService {
    private static final String CTX_KEY_DUPLICATION_IDX = "dupIdx";
    private static final String CTX_KEY_PREVIOUS = "prev";
    
    private static Logger log = Logger.getLogger(CustomerCard.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private MessageHelper messageHelper;
    
    private CustomerCardService self() {
        return this.serviceHelper.bean("customerCardService", CustomerCardService.class);
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, getValidCustomCard
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public CustomerCard getCustomerCard(final int customerCardId) {
        return (CustomerCard) this.entityDao.get(CustomerCard.class, customerCardId);
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, createValidCustomCard
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createCustomerCard(final CustomerCard customerCard) {
        try {
            customerCard.setCustomerCardType(this.customerCardTypeService.createPrimaryTypeIfAbsentFor(customerCard.getCustomerCardType().getId()));
        } catch (Exception e) {
            // DO NOTHING.
        }
        
        this.entityDao.save(customerCard);
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, updateValidCustomCard
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCustomerCard(final CustomerCard customerCard) {
        try {
            customerCard.setCustomerCardType(this.customerCardTypeService.createPrimaryTypeIfAbsentFor(customerCard.getCustomerCardType().getId()));
        } catch (Exception e) {
            // DO NOTHING.
        }
        
        this.entityDao.update(customerCard);
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, deleteValidCustomCard
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCustomerCard(final CustomerCard customerCard) {
        final CustomerCard po = (CustomerCard) this.entityDao.getCurrentSession().load(CustomerCard.class, customerCard.getId());
        if (po == null) {
            throw new IllegalStateException("Could not locate CustomerCard:" + customerCard.getId());
        }
        
        po.setIsDeleted(true);
        po.setArchiveDate(new Date());
        
        this.entityDao.update(po);
    }
    
    @Async
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Future<Integer> deleteCustomerCardsByCustomerIdAsync(final int customerId, final String statusKey) {
        Future<Integer> result = null;
        try {
            result = new AsyncResult<Integer>(self().deleteCustomerCardsByCustomerId(customerId, statusKey));
        } catch (BatchProcessingException bpe) {
            log.error("Error while trying to delete CustomerCard for Customer: " + customerId, bpe);
        }
        
        return result;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteCustomerCardsByCustomerId(final int customerId, final String statusKey) throws BatchProcessingException {
        int deleted = 0;
        
        final BatchProcessStatus status = (statusKey == null) ? null : this.serviceHelper.getProcessStatus(statusKey);
        try {
            int retried = 0;
            
            final Map<String, Object> context = new HashMap<String, Object>(4);
            context.put(CTX_KEY_DUPLICATION_IDX, 0);
            
            final Date now = new Date();
            int currDeleted = 0;
            do {
                try {
                    currDeleted = self().deleteCustomerCardsPaginated(customerId, now, context);
                    deleted += currDeleted;
                } catch (Exception e) {
                    ++retried;
                    if (retried < 1000) {
                        currDeleted = 1;
                        log.warn("Encountered error while trying to delete CustomerCards. Will retry rightaway", e);
                    } else {
                        currDeleted = -1;
                        log.error("Gave up because there are too many retries to delete CustomerCards. "
                                + "Some of the records have been deleted but not all of them. (CustomerId"
                                + customerId + ")", e);
                        throw new RuntimeException("Too many retries to delete CustomerCards", e);
                    }
                }
            } while (currDeleted > 0);
            
            if (status == null) {
                // DO NOTHING.
            } else if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail", "label.customerCard"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.deleteAllMsg", "label.customerCard"));
            }
        } catch (Throwable t) {
            if (status != null) {
                status.fail((String) null, t);
            }
            
            log.error("Failed to process CustomerCard batch deleteAll", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return deleted;
    }
    
    // These logic are extracted to different method just in case in the future there are more than 500 MB of records to be deleted (which will break cluster-db)
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteCustomerCardsPaginated(final Integer customerId, final Date maxLastUpdate, final Map<String, Object> context) {
        int deleted = 0;
        
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final ScrollableResults scroll = this.entityDao.getNamedQuery("CustomerCard.findLowerUpdatesByCustomerId")
                .setParameter("customerId", customerId).setParameter("lastModifiedGmt", maxLastUpdate).setFirstResult(0)
                .setMaxResults(WebCoreConstants.BATCH_SIZE).scroll(ScrollMode.FORWARD_ONLY);
        
        int dupCnt = (Integer) context.get(CTX_KEY_DUPLICATION_IDX);
        CardIdentity prev = (CardIdentity) context.get(CTX_KEY_PREVIOUS);
        while (scroll.next()) {
            final CustomerCard card = (CustomerCard) scroll.get(0);
            final CardIdentity curr = new CardIdentity(card);
            
            card.setIsDeleted(true);
            if ((prev == null) || (!prev.equals(curr))) {
                dupCnt = 0;
                card.setArchiveDate(maxLastUpdate);
            } else {
                ++dupCnt;
                card.setArchiveDate(new Date(maxLastUpdate.getTime() + (dupCnt * 1000)));
            }
            
            prev = curr;
            
            ++deleted;
        }
        
        context.put(CTX_KEY_DUPLICATION_IDX, dupCnt);
        context.put(CTX_KEY_PREVIOUS, prev);
        
        this.entityDao.getCurrentSession().flush();
        this.entityDao.getCurrentSession().clear();
        
        return deleted;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteCustomerCardsByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        return this.entityDao.getNamedQuery("CustomerCard.deleteByCustomerIdAndCardNumber").setParameter("customerId", customerId)
                .setParameter("cardNumber", cardNumber).setParameter("deleteDate", new Date()).executeUpdate();
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, findTotalValidCustomCardByPrimaryKey
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public int findTotalCustomerCardsByCustomerId(final int customerId) {
        final String[] params = { "customerId" };
        final Object[] values = { customerId };
        final List<Long> list = this.entityDao
                .findByNamedQueryAndNamedParam("CustomerCard.findTotalCustomerCardsByCustomerId", params, values, false);
        if (list != null) {
            return list.get(0).intValue();
        }
        return 0;
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, findTotalValidCustomCardByPrimaryKey
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public int findTotalCustomerCardsByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        final String[] params = { "customerId", "cardNumber" };
        final Object[] values = { customerId, cardNumber };
        final List<Long> list = this.entityDao.findByNamedQueryAndNamedParam("CustomerCard.findTotalCustomerCardsByCustomerIdAndCardNumber", params,
                                                                        values, false);
        if (list != null) {
            return list.get(0).intValue();
        }
        return 0;
        
    }
    
    @Override
    public int countByCustomerId(final Integer customerId) {
        return ((Number) this.entityDao.getNamedQuery("CustomerCard.countByCustomerId").setParameter("customerId", customerId).uniqueResult())
                .intValue();
    }
    
    @Override
    @Async
    public Future<Integer> processBatchAddCustomerCardsAsync(final String statusKey, final int customerId,
        final Collection<CustomerCard> customerCards, final String mode, final int userId) throws BatchProcessingException {
        return new AsyncResult<Integer>(self().processBatchAddCustomerCardsWithStatus(statusKey, customerId, customerCards, mode, userId));
    }
    
    /**
     * Inserts CustomerCard objects to database.
     * 
     * @param customerCards
     * @return List<CustomerCard> invalid CustomerCard objects that couldn't be saved to database.
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public int processBatchAddCustomerCardsWithStatus(final String statusKey, final int customerId, final Collection<CustomerCard> customerCards,
        final String mode, final int userId) throws BatchProcessingException {
        int updated = 0;
        final BatchProcessStatus status = this.serviceHelper.getProcessStatus(statusKey);
        try {
            updated = self().processBatchAddCustomerCards(customerId, customerCards, mode, userId, status);
            if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail", "label.customerCard"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.bulkupdate.successMsg", "label.customerCard"));
            }
        } catch (Throwable t) {
            status.fail((String) null, t);
            log.error("Failed to process CustomerCard batch update", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return updated;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int processBatchAddCustomerCards(final int customerId, final Collection<CustomerCard> customerCards, final String mode, final int userId,
        final BatchProcessStatus status) throws BatchProcessingException {
        int updated = 0;
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        if ("replace".equalsIgnoreCase(mode)) {
            this.deleteCustomerCardsByCustomerId(customerId, (String) null);
        }
        
        if (customerCards.size() > 0) {
            final CustomerCard first = customerCards.iterator().next();
            
            final ScrollableResults scroll = this.entityDao.getNamedQuery("CustomerCard.findByCustomerIdOrderByTypeAndNumber")
                    .setParameter("customerId", customerId).setParameter("startCardNumber", first.getCardNumber()).scroll(ScrollMode.FORWARD_ONLY);
            
            final Date today = new Date();
            final CustomerCardComparator cmprtr = new CustomerCardComparator();
            CustomerCard current = null;
            int compareResult = -1;
            for (CustomerCard card : customerCards) {
                compareResult = (current == null) ? -1 : cmprtr.compare(current, card);
                while (compareResult < 0) {
                    current = (!scroll.next()) ? null : (CustomerCard) scroll.get(0);
                    compareResult = (current == null) ? 1 : cmprtr.compare(current, card);
                }
                
                final CustomerCard existingCard = (compareResult != 0) ? null : current;
                if (existingCard == null) {
                    card.setAddedGmt(today);
                    card.setLastModifiedGmt(today);
                    card.setLastModifiedByUserId(userId);
                    
                    this.entityDao.save(card);
                } else {
                    existingCard.setComment(card.getComment());
                    existingCard.setCardBeginGmt(card.getCardBeginGmt());
                    existingCard.setCardExpireGmt(card.getCardExpireGmt());
                    existingCard.setGracePeriodMinutes(card.getGracePeriodMinutes());
                    existingCard.setIsForNonOverlappingUse(card.isIsForNonOverlappingUse());
                    existingCard.setLocation(card.getLocation());
                    existingCard.setPointOfSale(card.getPointOfSale());
                    existingCard.setMaxNumberOfUses(card.getMaxNumberOfUses());
                    
                    existingCard.setLastModifiedByUserId(userId);
                    existingCard.setLastModifiedGmt(today);
                    
                    this.entityDao.update(existingCard);
                }
                
                ++updated;
                if ((updated % WebCoreConstants.BATCH_SIZE) == 0) {
                    this.entityDao.getCurrentSession().flush();
                    this.entityDao.getCurrentSession().clear();
                }
            }
            
            this.entityDao.getCurrentSession().flush();
            this.entityDao.getCurrentSession().clear();
        }
        
        return updated;
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<CustomerCard> findCustomerCard(final CustomerCardSearchCriteria criteria) {
        final Criteria query = createSearchQuery(criteria);
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query.list();
    }
    
    @Override
    public int findRecordPage(final Integer customerCardId, final CustomerCardSearchCriteria criteria) {
        int result = -1;
        
        criteria.setEndCustomerCardId(customerCardId);
        final Criteria query = createSearchQuery(criteria);
        criteria.setEndCustomerCardId((Integer) null);
        
        query.setProjection(Projections.count("id"));
        
        final double recordNumber = ((Number) query.uniqueResult()).doubleValue();
        if ((recordNumber > 0) && (criteria.getItemsPerPage() != null)) {
            result = (int) Math.ceil(recordNumber / criteria.getItemsPerPage().doubleValue());
        }
        
        return result;
    }
    
    private Criteria createSearchQuery(final CustomerCardSearchCriteria criteria) {
        CustomerCard endCard = null;
        if (criteria.getEndCustomerCardId() != null) {
            endCard = this.entityDao.get(CustomerCard.class, criteria.getEndCustomerCardId());
        }
        
        final Criteria query = this.entityDao.createCriteria(CustomerCard.class).setCacheable(criteria.getMaxUpdatedTime() == null)
                .add(Restrictions.eq("isDeleted", false)).add(Restrictions.isNull("archiveDate"));
        
        query.add(Subqueries.propertyIn("customerCardType.id",
                                        DetachedCriteria.forClass(CustomerCardType.class).setProjection(Projections.property("id"))
                                                .add(Restrictions.eq("customer.id", criteria.getCustomerId()))
                                                .add(Restrictions.isNull("archiveDate")).add(Restrictions.eq("isDeleted", false))));
        
        if ((criteria.getCardNumber() != null) && (criteria.getCardNumber().size() > 0)) {
            final Iterator<String> itr = criteria.getCardNumber().iterator();
            Criterion condition = Restrictions.like("cardNumber", itr.next(), MatchMode.ANYWHERE);
            while (itr.hasNext()) {
                condition = Restrictions.or(condition, Restrictions.like("cardNumber", itr.next(), MatchMode.ANYWHERE));
            }
            
            query.add(condition);
        }
        
        if (criteria.getCardType() != null) {
            query.add(Restrictions.eq("customerCardType.id", criteria.getCardType()));
        } else if ((criteria.getCardTypeName() != null) && (criteria.getCardTypeName().size() > 0)) {
            query.createAlias("customerCardType", "cct");
            
            final Iterator<String> itr = criteria.getCardTypeName().iterator();
            
            Criterion condition = Restrictions.ilike("cct.name", itr.next(), MatchMode.ANYWHERE);
            while (itr.hasNext()) {
                condition = Restrictions.or(condition, Restrictions.ilike("cct.name", itr.next(), MatchMode.ANYWHERE));
            }
            
            query.add(condition);
        }
        
        if (criteria.getCardIds() != null) {
            query.add(Restrictions.in("id", criteria.getCardIds()));
        }
        
        if (criteria.isUsedCard() == criteria.isUnusedCard()) {
            if (criteria.getIgnoredConsumerId() != null) {
                query.add(Restrictions.not(Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId())));
            }
        } else {
            Criterion sub;
            if (criteria.isUsedCard()) {
                sub = Restrictions.isNotNull("consumer");
                if (criteria.getIgnoredConsumerId() != null) {
                    sub = Restrictions.and(sub, Restrictions.not(Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId())));
                }
            } else {
                sub = Restrictions.isNull("consumer");
                if (criteria.getIgnoredConsumerId() != null) {
                    sub = Restrictions.or(sub, Restrictions.eq("consumer.id", criteria.getIgnoredConsumerId()));
                }
            }
            
            query.add(sub);
        }
        
        if (criteria.getValidForDate() != null) {
            query.add(Restrictions.or(Restrictions.or(Restrictions.isNull("numberOfUses"), Restrictions.isNull("maxNumberOfUses")),
                                      Restrictions.ltProperty("numberOfUses", "maxNumberOfUses")));
            
            query.add(Restrictions.le("cardBeginGmt", criteria.getValidForDate()));
            query.add(Restrictions.ge("cardExpireGmt", criteria.getValidForDate()));
        }
        
        Criterion endCardRes = null;
        Criterion prevEndCardEq = null;
        if ((criteria.getOrderColumn() != null) && (criteria.getOrderColumn().length > 0)) {
            for (int i = -1; ++i < criteria.getOrderColumn().length;) {
                final String orderCol = criteria.getOrderColumn()[i];
                if (criteria.isOrderDesc()) {
                    query.addOrder(Order.desc(orderCol));
                } else {
                    query.addOrder(Order.asc(orderCol));
                }
                
                if (endCard != null) {
                    final Object endCardVal = ReflectionUtil.getAttribute(endCard, orderCol);
                    
                    endCardRes = this.entityDao.nullableOr(endCardRes, this.entityDao.nullableAnd(prevEndCardEq, (criteria.isOrderDesc())
                            ? Restrictions.gt(orderCol, endCardVal) : Restrictions.lt(orderCol, endCardVal)));
                    prevEndCardEq = this.entityDao.nullableAnd(prevEndCardEq, Restrictions.eq(orderCol, endCardVal));
                }
            }
        }
        
        if ((criteria.getIgnoredCardIds() != null) && (!criteria.getIgnoredCardIds().isEmpty())) {
            query.add(Restrictions.not(Restrictions.in("id", criteria.getIgnoredCardIds())));
        }
        
        query.addOrder(Order.asc("id"));
        if (endCard != null) {
            endCardRes = this.entityDao.nullableOr(endCardRes, this.entityDao.nullableAnd(prevEndCardEq, Restrictions.le("id", endCard.getId())));
            query.add(endCardRes);
        }
        
        if (criteria.getMaxUpdatedTime() != null) {
            query.add(Restrictions.le("lastModifiedGmt", criteria.getMaxUpdatedTime()));
        }
        
        return query;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<CustomerCard> findCustomerCardByConsumerId(final int consumerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerCard.findCustomerCardByConsumerId", "consumerId", consumerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<CustomerCard> findCustomerCardByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerCard.findByCustomerId", "customerId", customerId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<CustomerCard> findByCustomerIdAndCardIds(final Integer customerId, final Collection<Integer> cardIds) {
        return this.entityDao.getNamedQuery("CustomerCard.findByCustomerIdAndCardIds").setParameter("customerId", customerId)
                .setParameterList("cardIds", cardIds).list();
    }
    
    @Override
    public int countByCustomerCardType(final Collection<Integer> customerCardTypeIds) {
        return ((Number) this.entityDao.getNamedQuery("CustomerCard.countByCustomerCardTypes")
                .setParameterList("customerCardTypeIds", customerCardTypeIds).uniqueResult()).intValue();
    }
    
    /**
     * @param customerId
     *            CustomerId
     * @param cardNumber
     *            Value card number.
     * @return CustomerCard For a customer there should be only 1 CustomerCard record. Returns null if there are more than 1 record.
     */
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public CustomerCard findCustomerCardByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        final String[] params = { "customerId", "cardNumber" };
        final Object[] values = new Object[] { customerId, cardNumber };
        final List<CustomerCard> list = this.entityDao.findByNamedQueryAndNamedParam("CustomerCard.findByCustomerIdAndCardNumber", params, values, true);
        
        if (list == null || list.isEmpty() || list.size() > 1) {
            return null;
        }
        this.entityDao.evict(list.get(0));
        return list.get(0);
    }
}
