package com.digitalpaytech.service.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserAccountRoute;
import com.digitalpaytech.dto.MobileTokenMapEntry;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;

@Service("mobileSessionTokenService")
@Transactional(propagation = Propagation.REQUIRED)
public class MobileSessionTokenServiceImpl implements MobileSessionTokenService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public MobileSessionToken findMobileSessionTokenById(final int id) {
        return (MobileSessionToken) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileSessionToken.findById"
                                                                                       , new String[] { "id" }
                                                                                       , new Object[] { id }
                                                                                       , true);
    }
    
    @Override
    public MobileSessionToken findMobileSessionTokenByIdJoin(final int id) {
        return (MobileSessionToken) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileSessionToken.findByIdJoinUserAndLicenseAndRoute"
                                                                                       , new String[] { "id" }
                                                                                       , new Object[] { id }
                                                                                       , true);
    }
    
    @Override
    public MobileSessionToken findMobileSessionTokenBySessionToken(final String sessionToken) {
        return (MobileSessionToken) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenBySessionToken",
                                                                                       "sessionToken", sessionToken, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<MobileSessionToken> findMobileSessionTokenByUserAccountId(final int userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByUserAccountId", "userAccountId",
                                                            userAccountId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<MobileSessionToken> findMobileSessionTokenByUserAccountIdAndApplicationId(final int userAccountId, final int applicationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByUserAccountIdAndApplicationId"
                                                            , new String[] { "userAccountId", "applicationId" }
                                                            , new Object[] { userAccountId, applicationId });
    }
    
    @Override
    public MobileSessionToken findMobileSessionTokenByUserAccountAndMobileLicense(final int userAccountId, final int mobileLicenseId) {
        return (MobileSessionToken) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByUserAccountIdAndMobileLicenseId"
                                                     , new String[] { "userAccountId", "mobileLicenseId" }
                                                     , new Object[] { userAccountId, mobileLicenseId }
                                                     , true);
    }
    
    @Override
    public List<MobileTokenMapEntry> findMobileSessionTokensMap(final int customerId, final int mobileApplicationId, final Date expiryDate,
        final String timeZone, final Map<Integer, String> statusNames) {
        @SuppressWarnings("unchecked")
        final List<MobileSessionToken> tokens = this.entityDao
                .findByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByCustomerIdAndApplicationIdAndExpiryDate"
                                               , new String[] { "customerId", "mobileApplicationTypeId", "date" }
                                               , new Object[] { customerId, mobileApplicationId, expiryDate }
                                               , true);
        final List<MobileTokenMapEntry> result = new LinkedList<MobileTokenMapEntry>();
        MobileTokenMapEntry entry;
        for (MobileSessionToken token : tokens) {
            entry = new MobileTokenMapEntry();
            entry.setDeviceName(token.getMobileLicense().getCustomerMobileDevice().getName());
            entry.setLatitude(token.getLatitude());
            entry.setLongitude(token.getLongitude());
            try {
                entry.setUserName(URLDecoder.decode(token.getUserAccount().getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            } catch (UnsupportedEncodingException e) {
                entry.setUserName(token.getUserAccount().getUserName());
            }
            entry.setFirstName(token.getUserAccount().getFirstName());
            entry.setLastName(token.getUserAccount().getLastName());
            final WebObjectId objectId = new WebObjectId(MobileSessionToken.class, token.getId());
            entry.setRandomId(objectId.toString());
            entry.setStatus(statusNames.get(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN));
            entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN);
            for (UserAccountRoute uar : token.getUserAccount().getUserAccountRoutes()) {
                if (uar.getMobileApplicationType().getId().intValue() == WebCoreConstants.MOBILE_APP_TYPE_COLLECT) {
                    entry.setStatus(statusNames.get(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE));
                    entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE);
                    entry.setAssignedRouteName(uar.getRoute().getName());
                    break;
                }
            }
            entry.setLastCommunicated(new RelativeDateTime(token.getLastModifiedGmt(), timeZone));
            result.add(entry);
        }
        return result;
    }
    
    @Override
    public List<MobileTokenMapEntry> findRecentUsersForTokens(final List<MobileSessionToken> mobileSessionTokens, final String timeZone,
        final Map<Integer, String> statusNames) {
        Date lastActivity;
        MobileSessionToken lastUserToken;
        MobileSessionToken loggedInUserToken;
        // user can have tokens on different devices with same app
        // only show one token per user
        final Map<Integer, MobileTokenMapEntry> entriesByUser = new HashMap<Integer, MobileTokenMapEntry>();
        final List<MobileTokenMapEntry> userList = new LinkedList<MobileTokenMapEntry>();
        MobileTokenMapEntry entry;
        BigDecimal latitude;
        BigDecimal longitude;
        for (MobileSessionToken token : mobileSessionTokens) {
            latitude = null;
            longitude = null;
            lastActivity = null;
            lastUserToken = null;
            loggedInUserToken = null;
            
            if (token.getExpiryDate().after(new Date())) {
                
                // There should only be one or zero unexpired tokens per
                // license.
                // If unexpired token is found it is the logged-in user
                loggedInUserToken = token;
                lastActivity = token.getLastModifiedGmt();
                latitude = token.getLatitude();
                longitude = token.getLongitude();
            }
            if (lastActivity == null || token.getLastModifiedGmt().after(lastActivity)) {
                lastActivity = token.getLastModifiedGmt();
                lastUserToken = token;
                latitude = token.getLatitude();
                longitude = token.getLongitude();
            }
            
            if (loggedInUserToken == null) {
                if (lastUserToken == null) {
                    // don't add to list - nobody has used this license
                    continue;
                } else {
                    final UserAccount userAccount = lastUserToken.getUserAccount();
                    final int userAccountId = userAccount.getId();
                    final MobileTokenMapEntry existingEntry = entriesByUser.get(userAccountId);
                    if (existingEntry != null) {
                        if (existingEntry.getStatusId() != WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT) {
                            // don't add to list - this user is already being displayed for another license as logged in or active
                            continue;
                        }
                        if (existingEntry.getLastCommunicated().getDate().after(lastUserToken.getLastModifiedGmt())) {
                            // don't add to list - this user has used another device more recently than this device
                            continue;
                        }
                    }
                    entry = new MobileTokenMapEntry();
                    entry.setDeviceName(token.getMobileLicense().getCustomerMobileDevice().getName());
                    
                    try {
                        entry.setUserName(URLDecoder.decode(userAccount.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
                    } catch (UnsupportedEncodingException e) {
                        entry.setUserName(userAccount.getUserName());
                    }
                    entry.setLastName(lastUserToken.getUserAccount().getLastName());
                    entry.setFirstName(lastUserToken.getUserAccount().getFirstName());
                    // not showing lat / lon when user is not logged in
                    entry.setStatus(statusNames.get(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT));
                    entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT);
                    entry.setLastCommunicated(new RelativeDateTime(lastActivity, timeZone));
                    final WebObjectId webObjectId = new WebObjectId(MobileSessionToken.class, lastUserToken.getId());
                    entry.setRandomId(webObjectId.toString());
                    entriesByUser.put(userAccount.getId(), entry);
                    userList.add(entry);
                }
            } else {
                final UserAccount userAccount = loggedInUserToken.getUserAccount();
                entry = new MobileTokenMapEntry();
                entry.setDeviceName(token.getMobileLicense().getCustomerMobileDevice().getName());
                try {
                    entry.setUserName(URLDecoder.decode(userAccount.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
                } catch (UnsupportedEncodingException e) {
                    entry.setUserName(userAccount.getUserName());
                }
                entry.setLastName(userAccount.getLastName());
                entry.setFirstName(userAccount.getFirstName());
                entry.setLatitude(latitude);
                entry.setLongitude(longitude);
                entry.setLastCommunicated(new RelativeDateTime(lastActivity, timeZone));
                entry.setStatus(statusNames.get(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN));
                entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN);
                
                for (UserAccountRoute uar : userAccount.getUserAccountRoutes()) {
                    if (uar.getMobileApplicationType().getId() == WebCoreConstants.MOBILE_APP_TYPE_COLLECT) {
                        entry.setStatus(statusNames.get(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE));
                        entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE);
                        entry.setAssignedRouteName(uar.getRoute().getName());
                        break;
                    }
                }
                final WebObjectId webObjectId = new WebObjectId(MobileSessionToken.class, loggedInUserToken.getId());
                entry.setRandomId(webObjectId.toString());
                // overwrite any other entries for this user - this is the only logged in instance (user can only be logged in on one device per app)
                entriesByUser.put(userAccount.getId(), entry);
                userList.add(entry);
            }
        }
        
        return userList;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<MobileSessionToken> findMobileSessionTokensByMobileLicense(final Integer mobileLicenseId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByMobileLicenseIds", "mobileLicenseId",
                                                            mobileLicenseId);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<MobileSessionToken> findMobileSessionTokensByMobileLicenses(final List<Integer> mobileLicenseIds) {
        final Integer[] ids = new Integer[mobileLicenseIds.size()];
        final Query query = this.entityDao.getNamedQuery("MobileSessionToken.findMobileSessionTokenByMobileLicenseId");
        query.setParameterList("mobileLicenseId", ids);
        return query.list();
    }
    
    @Override
    public void deleteMobileSessionTokensByMobileLicenses(final List<Integer> mobileLicenseIds) {
        final Query query = this.entityDao.getNamedQuery("MobileSessionToken.deleteMobileSessionTokenByMobileLicenseId");
        query.setParameterList("mobileLicenseId", mobileLicenseIds);
        query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<MobileSessionToken> findProvisionedMobileSessionTokenByCustomerAndApplicationByPage(final int customerId, final int applicationId,
        final int pageNo, final Date maxUpdatedTime) {
        
        // Pagination is omitted on purpose to return the full list of digital collect users everytime this method is invoked.
        // This is to prevent inconsistency of the element order in the list. Please refer ti "EMS-6087"
        return this.entityDao
                .findByNamedQueryAndNamedParam("MobileSessionToken.findMobileSessionTokenByCustomerIdAndApplicationIdAndStatusOrderByExpiry"
                                               , new String[] { "customerId", "mobileApplicationTypeId", "status", "curDate" }
                                               , new Object[] { customerId, applicationId, WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED, maxUpdatedTime }
                                               , true);
    }
    
    @Override
    public long getTokenCount(final String sessionToken) {
        return (Long) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileSessionToken.findCountBySessionToken", "sessionToken", sessionToken);
    }
    
    @Override
    public void saveMobileSessionToken(final MobileSessionToken mobileSessionToken) {
        this.entityDao.saveOrUpdate(mobileSessionToken);
    }
    
    @Override
    public void updateMobileSessionToken(final MobileSessionToken mobileSessionToken) {
        this.entityDao.update(mobileSessionToken);
    }
    
    @Override
    public void delete(final MobileSessionToken mobileSessionToken) {
        this.entityDao.delete(mobileSessionToken);
    }
}
