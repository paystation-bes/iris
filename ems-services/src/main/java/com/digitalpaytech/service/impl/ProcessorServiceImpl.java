package com.digitalpaytech.service.impl;

import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Processor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("processorService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ProcessorServiceImpl implements ProcessorService {
    private static final String MSG_LINK_PROCESSOR_SUFFIX = "link.processor.suffix";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Override
    public final List<Processor> findAll() {
        return this.entityDao.loadAll(Processor.class);
    }
    
    @Override
    public Processor findProcessor(final Integer id) {
        return (Processor) this.entityDao.get(Processor.class, id);
    }
    
    @Override
    public void updateProcessor(final Processor processor) {
        this.entityDao.update(processor);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public Processor findByName(final String name) {
        @SuppressWarnings("unchecked")
        final List<Processor> processorList = this.entityDao.findByNamedQueryAndNamedParam("Processor.findProcessorByName", "name", name);
        if (processorList == null || processorList.isEmpty()) {
            return null;
        } else {
            return processorList.get(0);
        }
    }
    
    @Override
    public boolean isProcessorReadyForMigration(final Processor processor, boolean isLinkProc) {
        if (processor == null) {
            return false;
        }
        Processor processorLink = processor;
        if (!isLinkProc) {
            processorLink = findByName(getCorrespondingLinkProcName(processor.getName()));
        }
        return processorLink.getIsLink();
    }
    
    @Override
    public boolean isProcessorReadyForMigration(final Processor processor) {
        return isProcessorReadyForMigration(processor, false);
    }

    @Override
    public String getCorrespondingLinkProcName(final String processorName) {
        if (processorName == null) {
            return null;
        }
        return processorName + this.messageHelper.getMessage(MSG_LINK_PROCESSOR_SUFFIX);
    }
    
    @Override
    public Processor findProcessorReadyForMigration(final Integer processorId) {
        final Processor oriProcessor = this.findProcessor(processorId);
        final Processor processorLinkSuffix = findByName(oriProcessor.getName() 
                                                         + this.messageHelper.getMessage(MSG_LINK_PROCESSOR_SUFFIX));
        if (processorLinkSuffix == null || !processorLinkSuffix.getIsLink()) {
            return null;
        }
        return processorLinkSuffix;
    }
}
