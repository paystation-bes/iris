package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.FlushMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.CustomerAlertTypeProcessor;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.PosEventDelayedService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("customerAlertTypeService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerAlertTypeServiceImpl implements CustomerAlertTypeService {
    
    private static final String POS_IDS_BY_CUSTOMER_ID_NO_RESTRICTION = "PointOfSale.findIdsByCustomerIdNoRestiction";
    private static final String POS_IDS_BY_ROUTE_ID = "RoutePOS.findPointOfSaleIdByRoute";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    
    @Autowired
    private CustomerAlertTypeProcessor catProcessor;
    
    @Autowired
    private PosEventDelayedService posEventDelayedService;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameAsc(final Integer customerId, final Integer filterType) {
        if (filterType == 0) {
            return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByNameAsc",
                                                                HibernateConstants.CUSTOMER_ID, customerId, true);
        } else {
            return this.entityDao.findByNamedQueryAndNamedParam(
                                                                "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByNameAsc",
                                                                new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.FILTER_TYPE, },
                                                                new Object[] { customerId, filterType, }, true);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameDesc(final Integer customerId, final Integer filterType) {
        if (filterType == 0) {
            return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByNameDesc",
                                                                HibernateConstants.CUSTOMER_ID, customerId, true);
        } else {
            return this.entityDao.findByNamedQueryAndNamedParam(
                                                                "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByNameDesc",
                                                                new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.FILTER_TYPE, },
                                                                new Object[] { customerId, filterType, }, true);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(final Integer customerId,
        final Integer filterType) {
        if (filterType == 0) {
            return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc",
                                                                HibernateConstants.CUSTOMER_ID, customerId, true);
        } else {
            return this.entityDao.findByNamedQueryAndNamedParam(
                                                                "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByAlertTypeAsc",
                                                                new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.FILTER_TYPE, },
                                                                new Object[] { customerId, filterType, }, true);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(final Integer customerId,
        final Integer filterType) {
        if (filterType == 0) {
            return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc",
                                                                HibernateConstants.CUSTOMER_ID, customerId, true);
        } else {
            return this.entityDao.findByNamedQueryAndNamedParam(
                                                                "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByAlertTypeDesc",
                                                                new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.FILTER_TYPE, },
                                                                new Object[] { customerId, filterType, }, true);
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CustomerAlertType findCustomerAlertTypeById(final Integer id) {
        return (CustomerAlertType) this.entityDao.get(CustomerAlertType.class, id);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final CustomerAlertType findCustomerAlertTypeByIdAndEvict(final Integer id) {
        final CustomerAlertType type = (CustomerAlertType) this.entityDao.get(CustomerAlertType.class, id);
        this.entityDao.evict(type);
        return (CustomerAlertType) this.entityDao.merge(type);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    // False positive
    @SuppressWarnings("checkstyle:indentation")
    public final CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(final Integer customerId,
        final int alertThresholdTypeId) {
        return (CustomerAlertType) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("CustomerAlertType.findDefaultCustomerAlertTypeByCustomerIdAlertThresholdTypeId",
                                                     new String[] { HibernateConstants.CUSTOMER_ID, HibernateConstants.ALERT_THRESHOLD_TYPE_ID, },
                                                     new Object[] { customerId, alertThresholdTypeId, }, true);
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void saveCustomerAlertType(final CustomerAlertType customerAlertType) {
        final List<Integer> posIdList = this.catProcessor.save(customerAlertType);
        if (customerAlertType.getAlertThresholdType().getId() != WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION) {
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(posIdList, null, null, customerAlertType.getId());
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings({ "unchecked", "checkstyle:npathcomplexity", "checkstyle:methodlength", "checkstyle:cyclomaticcomplexity" })
    public final void updateCustomerAlertType(final CustomerAlertType customerAlertType, final Collection<String> emailAddresses) {
        final CustomerAlertType existing = this.entityDao.get(CustomerAlertType.class, customerAlertType.getId());
        
        final int oldRouteId = ((existing.getRoute() == null) || (existing.getRoute().getId() == null)) ? -1 : existing.getRoute().getId();
        final int newRouteId = ((customerAlertType.getRoute() == null) || (customerAlertType.getRoute().getId() == null)) ? -1
                : customerAlertType.getRoute().getId();
        
        List<Integer> dbPosIdOld = new ArrayList<Integer>();
        List<Integer> inputPosIdNew = new ArrayList<Integer>();
        List<Integer> samePos = new ArrayList<Integer>();
        
        if (existing.getRoute() == null) {
            
            dbPosIdOld.addAll(this.entityDao.findByNamedQueryAndNamedParam(POS_IDS_BY_CUSTOMER_ID_NO_RESTRICTION, HibernateConstants.CUSTOMER_ID,
                                                                           existing.getCustomer().getId()));
        } else {
            dbPosIdOld.addAll(this.entityDao.findByNamedQueryAndNamedParam(POS_IDS_BY_ROUTE_ID, HibernateConstants.ROUTE_IDS,
                                                                           existing.getRoute().getId()));
        }
        
        if (customerAlertType.getRoute() == null) {
            inputPosIdNew.addAll(this.entityDao.findByNamedQueryAndNamedParam(POS_IDS_BY_CUSTOMER_ID_NO_RESTRICTION, HibernateConstants.CUSTOMER_ID,
                                                                              customerAlertType.getCustomer().getId()));
        } else {
            inputPosIdNew.addAll(this.entityDao.findByNamedQueryAndNamedParam(POS_IDS_BY_ROUTE_ID, HibernateConstants.ROUTE_IDS,
                                                                              customerAlertType.getRoute().getId()));
        }
        
        boolean recalculateAlerts = false;
        if (!customerAlertType.isIsActive()) {
            if (existing.isIsActive()) {
                inputPosIdNew = null;
                samePos = null;
                recalculateAlerts = true;
            }
        } else {
            if (!existing.isIsActive()) {
                dbPosIdOld = null;
                samePos = null;
                recalculateAlerts = true;
            } else {
                if (oldRouteId != newRouteId) {
                    for (Integer posId : dbPosIdOld) {
                        if (inputPosIdNew.contains(posId)) {
                            samePos.add(posId);
                        }
                    }
                    for (Integer posId : samePos) {
                        inputPosIdNew.remove(posId);
                        dbPosIdOld.remove(posId);
                    }
                    recalculateAlerts = true;
                } else {
                    samePos = inputPosIdNew;
                    inputPosIdNew = null;
                    dbPosIdOld = null;
                }
                if ((existing.getThreshold() != customerAlertType.getThreshold())
                    || (existing.getAlertThresholdType() != customerAlertType.getAlertThresholdType())) {
                    recalculateAlerts = true;
                }
                
            }
        }
        
        this.catProcessor.update(customerAlertType, emailAddresses, oldRouteId, newRouteId);
        
        if (recalculateAlerts && customerAlertType.getAlertThresholdType().getId() != WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION) {
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(inputPosIdNew, dbPosIdOld, samePos, customerAlertType.getId());
        }
        
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void deleteCustomerAlertType(final CustomerAlertType customerAlertType, final Integer userAccountId) {
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final CustomerAlertType existing = this.catProcessor.delete(customerAlertType, userAccountId);
        final boolean isCurrentPSAlert = existing.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION;
        if (!isCurrentPSAlert) {
            
            final ScrollableResults oldRouteScroll;
            if (existing.getRoute() == null) {
                oldRouteScroll = this.entityDao.getNamedQuery(POS_IDS_BY_CUSTOMER_ID_NO_RESTRICTION)
                        .setParameter(HibernateConstants.CUSTOMER_ID, existing.getCustomer().getId()).scroll(ScrollMode.FORWARD_ONLY);
            } else {
                oldRouteScroll = this.entityDao.getNamedQuery(POS_IDS_BY_ROUTE_ID)
                        .setParameter(HibernateConstants.ROUTE_IDS, existing.getRoute().getId()).scroll(ScrollMode.FORWARD_ONLY);
            }
            
            final List<Integer> posIdsToDelete = new ArrayList<Integer>();
            
            while (oldRouteScroll.next()) {
                posIdsToDelete.add(oldRouteScroll.getInteger(0));
                
            }
            
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, posIdsToDelete, null, customerAlertType.getId());
            
        } else {
            this.posEventDelayedService.deleteByCustomerAlertType(customerAlertType);
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(final Integer customerId,
        final Integer alertThresholdTypeId, final boolean isActive) {
        
        final String[] params = { HibernateConstants.CUSTOMER_ID, "alertThresholdTypeId", "isActive" };
        final Object[] values = { customerId, alertThresholdTypeId, isActive };
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive",
                                                            params, values, true);
    }
    
    @Override
    public final int countByRouteId(final Integer... routeIds) {
        final Number result = (Number) this.entityDao.getNamedQuery("CustomerAlertType.countByRouteIds")
                .setParameterList(HibernateConstants.ROUTE_IDS, routeIds).uniqueResult();
        
        return (result == null) ? 0 : result.intValue();
    }
    
    @Override
    public final void disableNonDefaultAlert(final Integer customerId, final Date date, final Integer userId) {
        @SuppressWarnings("unchecked")
        final List<CustomerAlertType> catsList = this.entityDao.getNamedQuery("CustomerAlertType.findNonDefaultByCustomerIds")
                .setParameter(HibernateConstants.CUSTOMER_ID, customerId).list();
        final List<Integer> customerAlertTypeIds = new ArrayList<Integer>();
        for (CustomerAlertType cat : catsList) {
            if (WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION != cat.getAlertThresholdType().getId()) {
                customerAlertTypeIds.add(cat.getId());
            }
            
            cat.setIsActive(false);
        }
        if (!customerAlertTypeIds.isEmpty()) {
            final List<PointOfSale> pointOfSaleList = this.entityDao.findByNamedQueryAndNamedParam("PointOfSale.findPointOfSaleForAlertsByCustomerId",
                                                                                                   HibernateConstants.CUSTOMER_ID, customerId, true);
            final Set<Integer> pointOfSaleIds = new HashSet<Integer>();
            if (pointOfSaleList != null && !pointOfSaleList.isEmpty()) {
                for (PointOfSale pointOfSale : pointOfSaleList) {
                    pointOfSaleIds.add(pointOfSale.getId());
                }
            }
            
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, new ArrayList<Integer>(pointOfSaleIds), null, customerAlertTypeIds);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<CustomerAlertEmail> findCustomerAlertEmailByCustomerAlertTypeId(final Integer customerAlertTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertEmail.findByCustomerAlertTypeId", HibernateConstants.CUSTOMER_ALERT_TYPE_ID,
                                                            customerAlertTypeId, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<CustomerAlertType> findCustomerAlertTypesByRouteId(final Integer routeId) {
        List<CustomerAlertType> customerAlertTypes = null;
        if (routeId == null) {
            customerAlertTypes = this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByRouteIdNull",
                                                                              HibernateConstants.ROUTE_ID, routeId);
        } else {
            customerAlertTypes = this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypesByRouteIdNotNull",
                                                                              HibernateConstants.ROUTE_ID, routeId);
        }
        return customerAlertTypes;
    }
    
    //
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Integer> findCustomerAlertTypeIdsByRouteId(final Integer routeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerAlertType.findCustomerAlertTypeIdsByRouteId", HibernateConstants.ROUTE_ID,
                                                            routeId);
    }
    
    @Override
    public final List<CustomerAlertType> findCustomerAlertTypeByIdList(final List<Integer> customerAlertTypeIdList) {
        return this.entityDao.getNamedQuery("CustomerAlertType.findByIds")
                .setParameterList(HibernateConstants.CUSTOMER_ALERT_TYPE_IDS, customerAlertTypeIdList).list();
    }
    
}
