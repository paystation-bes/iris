package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.RateProfileLocationService;

@Service("rateProfileLocationService")
@Transactional(propagation = Propagation.SUPPORTS)
public class RateProfileLocationServiceImpl implements RateProfileLocationService {
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private LocationService locationService;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RateProfileLocation> findByRateProfileId(Integer rateProfileId) {
        return (List<RateProfileLocation>) this.entityDao.getNamedQuery("RateProfileLocation.findByRateProfileId").setParameter("rateProfileId", rateProfileId)
                .list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RateProfileLocation> findPublishedByLocationId(Integer locationId, Integer customerId) {
        if (locationId == null) {
            throw new IllegalStateException("Please specify at least a locationId !");
        }
        
        final List<Integer> locationIds = this.locationService.findAllParentIds(locationId);
        
        return (List<RateProfileLocation>) this.entityDao.getNamedQuery("RateProfileLocation.findPublishedByLocationIds").setParameterList("locationIds", locationIds)
                .setParameter("customerId", customerId).list();
    }
    
    @Override
    public RateProfileLocation findById(Integer rateProfileLocationId) {
        return this.entityDao.get(RateProfileLocation.class, rateProfileLocationId);
    }
    
    @Override
    public int saveAll(List<RateProfileLocation> locationsList, Integer customerId) {
        int updated = 0;
        
        final HashMap<Integer, RateProfileLocation> map = new HashMap<Integer, RateProfileLocation>(locationsList.size() * 2);
        for (RateProfileLocation rpl : locationsList) {
            if (rpl.getId() != null) {
                map.put(rpl.getId(), rpl);
            } else {
                this.entityDao.save(rpl);
                ++updated;
            }
        }
        
        List<Location> allAssignableLocs = null;
        
        if (map.keySet().size() > 0) {
            @SuppressWarnings("unchecked")
            List<RateProfileLocation> dbsList = this.entityDao.getNamedQuery("RateProfileLocation.findAllByIds").setParameterList("ids", map.keySet()).list();
            for (RateProfileLocation rpl : dbsList) {
                RateProfileLocation updatedRpl = map.remove(rpl.getId());
                if (rpl.getLocation() != null) {
                    if (rpl.getLocation().getIsParent() && (!updatedRpl.getLocation().getIsParent())) {
                        updated += assignToChildLocations(rpl, updatedRpl.getLocation(),
                                                          this.locationService.getLocationsByParentLocationId(rpl.getLocation().getId()));
                    }
                } else {
                    if (allAssignableLocs == null) {
                        allAssignableLocs = this.getAssignableLocation(customerId);
                    }
                    
                    updated += assignToChildLocations(rpl, updatedRpl.getLocation(), allAssignableLocs);
                }
                
                populate(rpl, updatedRpl);
                rpl.setNextPublisGmt(rpl.getStartGmt());
                this.entityDao.update(rpl);
                ++updated;
            }
            
            if (map.size() > 0) {
                throw new IllegalStateException("Could not locate " + map.size() + " RateProfileLocation(s)!");
            }
        }
        
        return updated;
    }
    
    @Override
    public int deleteByRateProfileId(Integer rateProfileId) {
        return this.entityDao.getNamedQuery("RateProfileLocation.deleteByRateProfileId").setParameter("rateProfileId", rateProfileId).executeUpdate();
    }
    
    private List<Location> getAssignableLocation(Integer customerId) {
        final List<Location> result = this.locationService.findParentLocationsByCustomerId(customerId);
        final Iterator<Location> itr = result.iterator();
        while (itr.hasNext()) {
            final Location loc = itr.next();
            if (loc.getIsUnassigned()) {
                itr.remove();
            }
        }
        
        return result;
    }
    
    private void populate(RateProfileLocation existingRate, RateProfileLocation newRate) {
        existingRate.setIsDeleted(newRate.isIsDeleted());
        existingRate.setIsPublished(newRate.isIsPublished());
        
        existingRate.setRateProfile(newRate.getRateProfile());
        existingRate.setLocation(newRate.getLocation());
        
        existingRate.setStartGmt(newRate.getStartGmt());
        existingRate.setEndGmt(newRate.getEndGmt());
        existingRate.setSpaceRange(newRate.getSpaceRange());
        
        existingRate.setNextPublisGmt(newRate.getNextPublisGmt());
    }
    
    private int assignToChildLocations(RateProfileLocation parentRate, Location newLoc, List<Location> availableLocs) {
        int updated = 0;
        
        for (Location loc : availableLocs) {
            if ((newLoc == null) || (!newLoc.getId().equals(loc.getId()))) {
                final RateProfileLocation child = new RateProfileLocation(parentRate);
                child.setId((Integer) null);
                child.setLocation(loc);
                
                this.entityDao.save(child);
                ++updated;
            }
        }
        
        return updated;
    }
}
