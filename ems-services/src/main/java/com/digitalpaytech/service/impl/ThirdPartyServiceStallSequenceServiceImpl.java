package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.digitalpaytech.domain.ThirdPartyServiceStallSequence;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.service.ThirdPartyServiceStallSequenceService;
import com.digitalpaytech.dao.EntityDao;

@Service("thirdPartyServiceStallSequenceService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ThirdPartyServiceStallSequenceServiceImpl implements ThirdPartyServiceStallSequenceService {
    
    @Autowired
    private EntityDao entityDao;

    @Override
    public long findSequenceNumber(Location location, int spaceNumber) {
        String[] params = { "locationId", "spaceNumber" };
        Object[] values = { location.getId(), spaceNumber };
        
        ThirdPartyServiceStallSequence thrSeq = new ThirdPartyServiceStallSequence();
        thrSeq.setLocation(location);
        thrSeq.setSpaceNumber(spaceNumber);
        
        long seqNum = 1;
        List<ThirdPartyServiceStallSequence> list = entityDao.findByNamedQueryAndNamedParam("ThirdPartyServiceStallSequence.findByLocationIdSequenceNumber", params, values);
        if (list == null || list.isEmpty()) {
            thrSeq.setSequenceNumber(seqNum);
            insertThirdPartyServiceStallSequence(thrSeq);            
            return seqNum;
        } else {
            seqNum = list.get(0).getSequenceNumber() + 1;
            thrSeq.setSequenceNumber(seqNum);
            updateThirdPartyServiceStallSequence(thrSeq);
            return seqNum;
        }
    }
    
    
    @Override
    public ThirdPartyServiceStallSequence insertThirdPartyServiceStallSequence(ThirdPartyServiceStallSequence thirdPartyServiceStallSequence) {
        ThirdPartyServiceStallSequence thrSeq = (ThirdPartyServiceStallSequence) entityDao.save(thirdPartyServiceStallSequence);
        return thrSeq;
    }
    
    
    @Override
    public void updateThirdPartyServiceStallSequence(ThirdPartyServiceStallSequence thirdPartyServiceStallSequence) {
        String[] params = { "sequenceNumber", "locationId", "spaceNumber" };
        Object[] values = { thirdPartyServiceStallSequence.getSequenceNumber(), thirdPartyServiceStallSequence.getLocation().getId(), thirdPartyServiceStallSequence.getSpaceNumber() };
        entityDao.updateByNamedQuery("ThirdPartyServiceStallSequence.updateThirdPartyServiceStallSequence", params, values);
    }
    
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }    
}
