package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.service.ElavonTransactionDetailService;

@Service("elavonTransactionDetailService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ElavonTransactionDetailServiceImpl implements ElavonTransactionDetailService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveElavonTransactionDetail(final ElavonTransactionDetail elavonTransactionDetail) {
        this.entityDao.save(elavonTransactionDetail);
    }
    
    @Override
    public final ElavonTransactionDetail findByPreAuthId(final long preAuthId) {
        return (ElavonTransactionDetail) this.entityDao.findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findByPreAuthId", "preAuthId",
                                                                                            preAuthId, true);
    }
    
    @Override
    public ElavonTransactionDetail findByProcessorTransactionId(final long processorTransactionId) {
        return (ElavonTransactionDetail) this.entityDao.findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findByProcessorTransactionId",
                                                                                            "processorTransactionId", processorTransactionId, true);
    }
    
    @Override
    public ElavonTransactionDetail findByTransactionReferenceNbr(final int transactionReferenceNbr) {
        return (ElavonTransactionDetail) this.entityDao.findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findByTransactionReferenceNbr",
                                                                                            "transactionReferenceNbr", transactionReferenceNbr, true);
    }
    
    @Override
    public final ElavonTransactionDetail findByPurchasedDatePointOfSaleIdAndTicketNo(final Date purchasedDate, final int pointOfSaleId,
        final int ticketNumber) {
        return (ElavonTransactionDetail) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("ElavonTransactionDetail.findByPurchasedDatePointOfSaleIdAndTicketNumber", new String[] {
                    "purchasedDate", "pointOfSaleId", "ticketNumber", }, new Object[] { purchasedDate, pointOfSaleId, ticketNumber, }, true);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateForPreauthTransferToHolding(final Collection<Long> preAuthIds) {
        @SuppressWarnings("unchecked")
        final List<ElavonTransactionDetail> etds = this.entityDao.findByNamedQueryAndNamedParam("ElavonTransactionDetail.findByPreAuthIds",
                                                                                                new String[] { "preAuthIds" },
                                                                                                new Object[] { preAuthIds }, false);
        for (ElavonTransactionDetail etd : etds) {
            etd.setPreAuth(null);
            this.entityDao.update(etd);
        }
    }
    
}
