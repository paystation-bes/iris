package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.service.EventTypeService;

@Service("eventTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class EventTypeServiceImpl implements EventTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final EventType findEventTypeById(final byte eventTypeId) {
        return this.entityDao.get(EventType.class, eventTypeId);
    }
    
}
