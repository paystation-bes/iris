package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import javax.annotation.PostConstruct;

import com.digitalpaytech.dao.OpenSessionDao;

import com.digitalpaytech.service.SmsMessageTypeService;
import com.digitalpaytech.domain.SmsMessageType;

@Service("smsMessageTypeService")
public class SmsMessageTypeServiceImpl implements SmsMessageTypeService {
    
    @Autowired
    private OpenSessionDao openSessionDao;

    private Map<Integer, SmsMessageType> map;

    @PostConstruct
    private void loadAllToMap() {
        List<SmsMessageType> list = openSessionDao.loadAll(SmsMessageType.class);
        map = new HashMap<Integer, SmsMessageType>(list.size());
        
        Iterator<SmsMessageType> iter = list.iterator();
        while (iter.hasNext()) {
            SmsMessageType smType = iter.next();
            map.put(smType.getId(), smType);
        }
    }
    
    
    @Override
    public SmsMessageType findSmsMessageType(Integer id) {
        return map.get(id);
    }
}
