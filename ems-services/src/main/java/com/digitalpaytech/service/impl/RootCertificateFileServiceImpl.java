package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RootCertificateFile;
import com.digitalpaytech.service.RootCertificateFileService;

@Service("rootCertificateFileService")
@Transactional(propagation = Propagation.REQUIRED)
public class RootCertificateFileServiceImpl implements RootCertificateFileService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public RootCertificateFile findByMaxId() {
        @SuppressWarnings("unchecked")
        final List<RootCertificateFile> rootCertificateFiles = this.entityDao.findByNamedQuery("RootCertificateFile.findByMaxId");
        if (rootCertificateFiles == null || rootCertificateFiles.isEmpty()) {
            return null;
        } else {
            return rootCertificateFiles.get(0);
        }
    }
    
    @Override
    public void saveOrUpdate(final RootCertificateFile file) {
        this.entityDao.saveOrUpdate(file);
        
    }
    
}
