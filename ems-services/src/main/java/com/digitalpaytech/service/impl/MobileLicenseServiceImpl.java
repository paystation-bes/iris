package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileLicenseStatusType;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.dto.MobileLicenseInfo;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MobileApplicationTypeService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Service("mobileLicenseService")
@Transactional(propagation = Propagation.REQUIRED)
public class MobileLicenseServiceImpl implements MobileLicenseService {
    
    protected static Logger log = Logger.getLogger(MobileLicenseServiceImpl.class);
    
    @Autowired
    private MobileApplicationTypeService mobileApplicationTypeService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MobileLicense> findMobileLicenseByCustomer(final Integer customerId) {
        final Criteria query = this.entityDao.createCriteria(MobileLicense.class).setCacheable(true);
        query.createAlias("customerMobileDevice", "customerMobileDevice");
        query.createAlias("mobileLicenseStatusType", "mobileLicenseStatusType");
        query.add(Restrictions.eq("customer.id", customerId));
        
        query.addOrder(Order.asc("customerMobileDevice.name"));
        query.addOrder(Order.asc("mobileLicenseStatusType.name"));
        
        return query.list();
    }
    
    @Override
    public final List<MobileDeviceInfo> findMobileDevicesByCustomerAndApplication(final Integer customerId, final Integer applicationId,
        final Integer pageNumber, final Integer pageSize) {
        // TODO try to remember what this was even for
        
        // ... if nothing compelling jumps to mind delete it
        final Criteria query = this.entityDao.createCriteria(MobileLicense.class).setCacheable(true);
        
        query.createAlias("customerMobileDevice", "customerMobileDevice");
        query.createAlias("mobileApplicationType", "mobileApplicationType");
        
        query.add(Restrictions.eq("customer.id", customerId));
        if (applicationId != null) {
            query.add(Restrictions.eq("mobileApplicationType.id", applicationId));
        }
        
        query.setProjection(Projections.projectionList().add(Projections.property("customerMobileDevice.name").as("deviceName"))
                .add(Projections.property("customerMobileDevice.description").as("description"))
                .add(Projections.property("mobileApplicationType.name").as("appType")).add(Projections.property("customerMobileDevice.id").as("id"))
                .add(Projections.count("customerMobileDevice.mobileLicenses").as("licenseCount")));
        
        query.addOrder(Order.asc("customerMobileDevice.name"));
        query.addOrder(Order.desc("mobileSessionTokens.expiryDate"));
        MobileDeviceInfo info;
        final List<MobileDeviceInfo> infos = new ArrayList<MobileDeviceInfo>();
        for (Object o : query.list()) {
            final Object[] tuple = (Object[]) o;
            info = new MobileDeviceInfo();
            info.setName((String) tuple[0]);
            info.setDescription((String) tuple[1]);
            info.setApplication((String) tuple[2]);
            info.setRandomId(new WebObjectId(CustomerMobileDevice.class, ((Integer) tuple[3]).intValue()).toString());
            infos.add(info);
        }
        return infos;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void updateLicenseCount(final Integer customerId, final Integer applicationId, final int count) {
        if (count < 0) {
            return;
        }
        // if (count > existing count)
        // {add licenses}
        // else {
        // find available licenses and remove them
        // then if necessary, find licenses that have not been used for the
        // greatest amount of time and remove them}
        final int keySize = (int) this.countTotalLicensesForCustomerAndType(customerId, applicationId);
        int countToRemove = keySize - count;
        if (countToRemove == 0) {
            // no change to license count - no action necessary
            return;
        } else if (countToRemove > 0) {
            final List<MobileLicense> candidates = this.entityDao.findByNamedQueryAndNamedParam(
                "MobileLicense.findMobileLicenseByCustomerAndApplicationAndLicenseStatus", new String[] { "customerId", "applicationId", "status" },
                new Object[] { customerId, applicationId, WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE });
            final Iterator<MobileLicense> iter = candidates.iterator();
            MobileLicense license;
            
            while (countToRemove > 0 && iter.hasNext()) {
                license = iter.next();
                this.entityDao.delete(license);
                countToRemove--;
            }
            
            if (countToRemove > 0) {
                final List<MobileLicense> provisionedCandidates = this.findProvisionedKeysOrderByTokenExpiry(customerId, applicationId);
                final Iterator<MobileLicense> iterator = provisionedCandidates.iterator();
                while (countToRemove > 0 && iterator.hasNext()) {
                    final MobileLicense nextLicense = iterator.next();
                    if (nextLicense.getMobileSessionTokens() != null && !nextLicense.getMobileSessionTokens().isEmpty()) {
                        for (MobileSessionToken sessionToken : nextLicense.getMobileSessionTokens()) {
                            this.entityDao.delete(sessionToken);
                        }
                    }
                    this.entityDao.delete(nextLicense);
                    countToRemove--;
                }
            }
        } else if (countToRemove < 0) {
            // actually adding licenses if this is a negative number
            MobileLicense newKey;
            while (countToRemove < 0) {
                newKey = new MobileLicense();
                newKey.setCustomer(this.customerService.findCustomer(customerId));
                newKey.setMobileApplicationType(this.mobileApplicationTypeService.getMobileApplicationTypeById(applicationId));
                newKey.setMobileLicenseStatusType(this.findMobileLicenseStatusTypeById(WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE));
                this.entityDao.save(newKey);
                countToRemove++;
            }
        }
    }
    
    /**
     * Finds all of the provisioned keys, sorted by token expiry. Keys that are
     * unused will be first in the list, followed by the rest of the keys sorted
     * such that keys that have been used most recently will be at the end of
     * the list.
     * 
     * @param customerId
     * @param applicationId
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<MobileLicense> findProvisionedKeysOrderByTokenExpiry(final Integer customerId, final Integer applicationId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseRankedByTokens", new String[] { "customerId",
            "applicationId", "status" }, new Object[] { customerId, applicationId, WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED });
    }
    
    @Override
    public final List<MobileLicenseInfo> findMobileLicenseInfoForCustomerAndApplication(final Integer customerId, final Integer applicationId,
        final String timeZone) {
        // get a list of LicenseInfo{appType, deviceName, lastUsed,
        // customerdevice_randomId}
        final Criteria query = this.entityDao.createCriteria(MobileLicense.class).setCacheable(true);
        query.createAlias("customerMobileDevice", "customerMobileDevice", Criteria.LEFT_JOIN)
                .createAlias("customerMobileDevice.mobileDevice", "mobileDevice").createAlias("mobileApplicationType", "mobileApplicationType")
                .createAlias("mobileSessionTokens", "mobileSessionTokens", Criteria.LEFT_JOIN);
        
        query.add(Restrictions.eq("customer.id", customerId));
        if (applicationId != null) {
            query.add(Restrictions.eq("mobileApplicationType.id", applicationId));
        }
        
        query.setProjection(Projections.projectionList().add(Projections.property("mobileApplicationType.name").as("appType"))
                .add(Projections.property("customerMobileDevice.name").as("deviceName"))
                .add(Projections.max("mobileSessionTokens.lastModifiedGmt").as("lastModifiedGmt"))
                .add(Projections.property("customerMobileDevice.id").as("deviceId")).add(Projections.property("mobileDevice.uid").as("uid"))
                .add(Projections.groupProperty("id").as("id")));
        
        final List<MobileLicenseInfo> infos = new ArrayList<MobileLicenseInfo>();
        MobileLicenseInfo info;
        for (Object o : query.list()) {
            final Object[] tuple = (Object[]) o;
            info = new MobileLicenseInfo();
            info.setAppType((String) tuple[0]);
            info.setDeviceName((String) tuple[1]);
            info.setLastUsed(DateUtil.getRelativeTimeString((Date) tuple[2], timeZone));
            info.setRandomId(new WebObjectId(CustomerMobileDevice.class, tuple[3]).toString());
            info.setUid((String) tuple[4]);
            infos.add(info);
        }
        return infos;
    }
    
    @Override
    public final MobileLicense findMobileLicense(final int mobileLicenseId) {
        return (MobileLicense) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseById", "id", mobileLicenseId);
    }
    
    @Override
    public final MobileLicense findMobileLicenseByCustomerAndApplicationAndDevice(final int customerId, final int applicationId,
        final int customerDeviceId) {
        return (MobileLicense) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseByCustomerAndApplicationAndDevice",
            new String[] { "customerId", "applicationId", "customerDeviceId" }, new Object[] { customerId, applicationId, customerDeviceId }, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MobileLicense> findProvisionedMobileLicensesByCustomerAndDevice(final int customerId, final int customerDeviceId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseByCustomerAndDeviceAndLicenseStatus", new String[] {
            "customerId", "customerDeviceId", "status" }, new Object[] { customerId, customerDeviceId,
            WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED }, true);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final List<MobileLicense> findMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId,
        final Integer pageNumber, final Integer pageSize) {
        if (pageNumber != null && pageSize != null) {
            return this.entityDao.findPageResultByNamedQuery("MobileLicense.findMobileLicenseByCustomerAndApplication", new String[] { "customerId",
                "applicationId" }, new Object[] { customerId, applicationId }, true, pageNumber, pageSize);
        }
        return this.entityDao.findByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseByCustomerAndApplication", new String[] { "customerId",
            "applicationId" }, new Object[] { customerId, applicationId });
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MobileLicense> findProvisionedMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId) {
        return this.entityDao
                .findByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseByCustomerAndApplicationAndLicenseStatus", new String[] {
                    "customerId", "applicationId", "status" }, new Object[] { customerId, applicationId,
                    WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED });
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final MobileLicense findAvailableMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId) {
        final List<MobileLicense> keys = this.entityDao.findByNamedQueryAndNamedParam(
            "MobileLicense.findMobileLicenseByCustomerAndApplicationAndLicenseStatus", new String[] { "customerId", "applicationId", "status" },
            new Object[] { customerId, applicationId, WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE });
        if (keys == null || keys.size() == 0) {
            return null;
        }
        return keys.get(0);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<MobileLicense> findProvisionedMobileLicensesAndActiveTokensByCustomerIdAndDeviceId(final int customerId,
        final int customerDeviceId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileLicense.findMobileLicenseAndTokensByCustomerAndDeviceAndLicenseStatusAndDate",
            new String[] { "customerId", "customerDeviceId", "status", "date" }, new Object[] { customerId, customerDeviceId,
                WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED, new Date() }, true);
        
    }
    
    @Override
    public final MobileLicense findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(final int customerId, final int applicationId,
        final int customerDeviceId) {
        return (MobileLicense) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "MobileLicense.findMobileLicenseByCustomerAndApplicationAndDeviceAndLicenseStatus", new String[] { "customerId", "applicationId",
                "customerDeviceId", "status" }, new Object[] { customerId, applicationId, customerDeviceId,
                WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED }, true);
    }
    
    @Override
    public final MobileLicenseStatusType findMobileLicenseStatusTypeById(final int id) {
        return (MobileLicenseStatusType) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "MobileLicenseStatusType.findMobileLicenseStatusTypeById", "id", id, true);
    }
    
    @Override
    public final MobileLicenseStatusType findMobileLicenseStatusTypeByName(final String name) {
        return (MobileLicenseStatusType) this.entityDao.findUniqueByNamedQueryAndNamedParam(
            "MobileLicenseStatusType.findMobileLicenseStatusTypeByName", "name", name, true);
    }
    
    @Override
    public final long countProvisionedLicensesForCustomerAndType(final Integer customerId, final Integer applicationId) {
        return (Long) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileLicense.countLicenseByCustomerAndApplicationAndStatus", new String[] {
            "customerId", "applicationId", "status" },
            new Object[] { customerId, applicationId, WebCoreConstants.MOBILE_LICENSE_STATUS_PROVISIONED }, true);
    }
    
    @Override
    public final long countTotalLicensesForCustomerAndType(final Integer customerId, final Integer applicationId) {
        return (Long) this.entityDao.findUniqueByNamedQueryAndNamedParam("MobileLicense.countLicenseByCustomerAndApplication", new String[] {
            "customerId", "applicationId" }, new Object[] { customerId, applicationId }, true);
    }
    
    @Override
    public final List<MobileApplicationType> getMobileAppsForCustomer(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("MobileLicense.findAppTypesByCustomer", "customerId", customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveOrUpdateMobileLicense(final MobileLicense mobileKey) {
        this.entityDao.saveOrUpdate(mobileKey);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteMobileLicense(final MobileLicense mobileKey) {
        this.entityDao.delete(mobileKey);
    }
}
