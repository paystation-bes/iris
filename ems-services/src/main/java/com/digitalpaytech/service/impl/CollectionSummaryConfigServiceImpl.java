package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CollectionSummaryConfig;
import com.digitalpaytech.service.CollectionSummaryConfigService;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
@Service("collectionSummaryConfigService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CollectionSummaryConfigServiceImpl implements CollectionSummaryConfigService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public CollectionSummaryConfig getCollectionSummaryConfigByUserId(final int userAccountId) {
        return (CollectionSummaryConfig) this.entityDao.findUniqueByNamedQueryAndNamedParam("CollectionSummaryConfig.findColumnShowByUserAccountId",
                                                                                            "userAccountId", userAccountId);
    }
}
