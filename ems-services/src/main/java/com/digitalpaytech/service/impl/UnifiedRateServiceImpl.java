package com.digitalpaytech.service.impl;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.util.DateUtil;

@Service("unifiedRateService")
@Transactional(propagation = Propagation.REQUIRED)
public class UnifiedRateServiceImpl implements UnifiedRateService {
    
    private static Logger log = Logger.getLogger(UnifiedRateServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public List<UnifiedRate> findRatesByCustomerId(Integer customerId) {
        return entityDao.findByNamedQueryAndNamedParam("UnifiedRate.findRatesByCustomerId", "customerId", customerId);
    }
    
    @Override
    public void insertWithNewExtensibleRate(UserAccount userAccount, ExtensibleRate extensibleRate) {
        UnifiedRate uRate = createUnifiedRate(userAccount, extensibleRate.getName());
        extensibleRate.setUnifiedRate(uRate);
        Set<ExtensibleRate> set = new HashSet<ExtensibleRate>(1);
        set.add(extensibleRate);
        uRate.setExtensibleRates(set);
        entityDao.save(uRate);
    }
    
    @Override
    public UnifiedRate insert(UserAccount userAccount, String name) {
        UnifiedRate uRate = createUnifiedRate(userAccount, name);
        Integer id = (Integer) entityDao.save(uRate);
        uRate = (UnifiedRate) entityDao.get(UnifiedRate.class, id);
        return uRate;
    }
    
    @Override
    public UnifiedRate findRateByNameAndCustomerId(String unifiedRateName, Integer customerId) {
        String[] params = { "name", "customerId" };
        Object[] values = new Object[] { unifiedRateName.toUpperCase(), customerId };
        
        List<?> list = entityDao.findByNamedQueryAndNamedParam("UnifiedRate.findRateByNameAndCustomerId", params, values, true);
        if (list == null || list.isEmpty()) {
            return null;
        }
        
        if (log.isDebugEnabled()) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("UnifiedRateServiceImpl, findRateByNameAndCustomerId, unifiedRateName: ").append(unifiedRateName).append(", customerId: ")
                    .append(customerId);
            if (list != null) {
                bdr.append(", result list size: " + list.size());
                log.debug(bdr.toString());
                for (int i = 0; i < list.size(); i++) {
                    log.debug("Class: " + list.get(i));
                }
            } else {
                bdr.append(", result is null.");
                log.debug(bdr.toString());
            }
        }
        
        UnifiedRate uRate = (UnifiedRate) list.get(0);
        return uRate;
    }
    
    private UnifiedRate createUnifiedRate(UserAccount userAccount, String name) {
        UnifiedRate uRate = new UnifiedRate();
        uRate.setCustomer(userAccount.getCustomer());
        uRate.setName(name);
        uRate.setLastModifiedByUserId(userAccount.getId());
        uRate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        return uRate;
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public UnifiedRate findUnifiedRateById(Integer unifiedRateId) {
        List<UnifiedRate> UnifiedRateList = entityDao.findByNamedQueryAndNamedParam("UnifiedRate.findUnifiedRateById", "unifiedRateId", unifiedRateId);
        return UnifiedRateList.get(0);
    }
}
