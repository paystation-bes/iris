package com.digitalpaytech.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.MigrationReadyPayStationVersion;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.MigrationReadyPayStationVersionService;

@Service("migrationReadyPayStationVersionService")
@Transactional(propagation = Propagation.REQUIRED)
public class MigrationReadyPayStationVersionServiceImpl implements MigrationReadyPayStationVersionService {
    @Autowired
    private EntityService entityService;
    
    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }
    
    @Override
    public Set<String> getVersionSet() {
        List<MigrationReadyPayStationVersion> versionList = entityService.loadAll(MigrationReadyPayStationVersion.class);
        Set<String> versionSet = new HashSet<String>();
        for (MigrationReadyPayStationVersion version : versionList) {
            versionSet.add(version.getVersionName());
        }
        return versionSet;
    }
    
}
