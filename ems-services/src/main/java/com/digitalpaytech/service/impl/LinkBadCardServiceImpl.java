package com.digitalpaytech.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.digitalpaytech.client.BadCardClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.corecps.CustomPageImpl;
import com.digitalpaytech.client.util.HystrixExceptionUtil;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.BadCardData;
import com.digitalpaytech.dto.BadCardFileInfo;
import com.digitalpaytech.dto.BadCardMessage;
import com.digitalpaytech.dto.customeradmin.BannedCard;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.dto.Pair;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Service("linkBadCardService")
public class LinkBadCardServiceImpl implements LinkBadCardService {
    private static final Logger LOG = Logger.getLogger(LinkBadCardServiceImpl.class);
    private static final String CUSTOMER_ID = "Customer id: ";
    
    @Autowired
    @Qualifier("pciLinkMessageProducer")
    private AbstractMessageProducer pciLinkMessageProducer;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    private BadCardClient badCardClient;
    
    private JSON json;
    
    @PostConstruct
    public final void init() {
        this.json = new JSON();
        
        this.badCardClient = this.clientFactory.from(BadCardClient.class);
    }
    
    @Override
    public final Set<BannedCard> findBannedCards(final Integer customerId, final CustomerBadCardSearchCriteria criteria) throws CryptoException {
        
        try {
            
            if (StringUtils.isBlank(criteria.getCardNumber())) {
                
                final String stringResult = this.badCardClient.getBadCards(customerId, criteria.getPage() - 1, criteria.getItemsPerPage()).execute()
                        .toString(Charset.defaultCharset());
                
                final TypeReference<ObjectResponse<CustomPageImpl<BadCardMessage>>> objectResponseTypeRef =
                        new TypeReference<ObjectResponse<CustomPageImpl<BadCardMessage>>>() {
                };
                
                final ObjectResponse<CustomPageImpl<BadCardMessage>> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
                
                final List<BadCardMessage> objArrList = objectResponse.getResponse().getContent();
                
                final Set<BannedCard> bannedCardsSet = new LinkedHashSet<>(objArrList.size());
                for (BadCardMessage badCardMessage : objArrList) {
                    bannedCardsSet.add(createBannedCard(badCardMessage));
                }
                
                return bannedCardsSet;
                
            } else if (criteria.getCardNumber().length() == StandardConstants.CONSTANT_4) {
                
                final String stringResult = this.badCardClient
                        .getFilteredBadCards(customerId, criteria.getCardNumber(), criteria.getPage() - 1, criteria.getItemsPerPage()).execute()
                        .toString(Charset.defaultCharset());
                
                final TypeReference<ObjectResponse<CustomPageImpl<BadCardMessage>>> objectResponseTypeRef =
                        new TypeReference<ObjectResponse<CustomPageImpl<BadCardMessage>>>() {
                };
                
                final ObjectResponse<CustomPageImpl<BadCardMessage>> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
                
                final List<BadCardMessage> objArrList = objectResponse.getResponse().getContent();
                
                final Set<BannedCard> bannedCardsSet = new LinkedHashSet<>(objArrList.size());
                for (BadCardMessage badCardMessage : objArrList) {
                    bannedCardsSet.add(createBannedCard(badCardMessage));
                }
                
                return bannedCardsSet;
                
            } else {
                /*
                 * In case is searching by specific Card Number, Make sure that it'll only call the Bad Card Service just in the first time. The second time
                 * will send empty collection making UI understand that should stop asking for data.
                 */
                if (criteria.getPage() == 1) {
                    
                    final String cardHash =
                            this.cryptoAlgorithmFactory.getShaHash(criteria.getCardNumber(), CryptoConstants.HASH_LINK_BAD_CREDIT_CARD,
                                                                   CryptoConstants.HASH_ALGORITHM_TYPE_SHA256);
                    
                    final String encodedCardHash = URLEncoder.encode(cardHash, StandardConstants.HTTP_UTF_8_ENCODE_CHARSET);
                    
                    final String stringResult =
                            this.badCardClient.getBadCard(customerId, encodedCardHash).execute().toString(Charset.defaultCharset());
                    
                    final TypeReference<ObjectResponse<BadCardMessage>> objectResponseTypeRef = new TypeReference<ObjectResponse<BadCardMessage>>() {
                    };
                    
                    final ObjectResponse<BadCardMessage> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
                    
                    final BadCardMessage badCardMessage = objectResponse.getResponse();
                    
                    final Set<BannedCard> bannedCardsSet = new LinkedHashSet<>(1);
                    bannedCardsSet.add(createBannedCard(badCardMessage));
                    
                    return bannedCardsSet;
                    
                } else {
                    return Collections.emptySet();
                }
                
            }
            
        } catch (JsonException e) {
            LOG.error("unable to convert to BadCard object", e);
            return Collections.emptySet();
        } catch (HystrixRuntimeException e) {
            LOG.error("Unable to get successful response from " + BadCardClient.ALIAS, e);
            return Collections.emptySet();
        } catch (UnsupportedEncodingException uee) {
            LOG.error("URL encoding failed", uee);
            return Collections.emptySet();
        }
    }
    
    private BannedCard createBannedCard(final BadCardMessage badCardMessage) {
        final BannedCard bc = new BannedCard();
        
        bc.setUuid(badCardMessage.getId());
        bc.setCardNumber(badCardMessage.getSha1CardHash());
        bc.setAddedGMT(Date.from(badCardMessage.getAddedUTCInstant()));
        bc.setCardExpiry(Short.parseShort(badCardMessage.getCardExpiry()));
        bc.setLast4DigitsOfCardNumber(Short.parseShort(badCardMessage.getLast4Digits()));
        bc.setComment(badCardMessage.getComment());
        bc.setSource(badCardMessage.getSource());
        bc.setCardType(badCardMessage.getCardType());
        bc.setCustomer(new Customer(badCardMessage.getCustomerId()));
        
        final CardType cardType = this.cardTypeService.findByName(badCardMessage.getCardParentType());
        final Integer cardTypeId = cardType == null ? WebCoreConstants.CARD_TYPE_NA : cardType.getId();
        bc.setCardTypeId(cardTypeId);
        return bc;
    }
    
    @Override
    public boolean addBadCard(final BadCardData badCard) {
        
        if (badCard.getCustomer() == null) {
            throw new IllegalArgumentException("BadCardData must have a customer");
        }
        
        if (badCard.getCustomer().getUnifiId() == null) {
            throw new IllegalArgumentException("Customer UnifiId is not defined");
        }
        
        try {
            return BooleanUtils.isTrue(this.pciLinkMessageProducer
                    .sendWithByteArray(KafkaKeyConstants.LINK_BAD_CARD_SERVICE_ADD, String.valueOf(badCard.getCustomer().getUnifiId()), 
                                       createBadCardMessage(badCard)).get());
        } catch (InvalidTopicTypeException | JsonException | InvalidDataException | CryptoException | InterruptedException | ExecutionException e) {
            LOG.error("Unable to send bad card message to kafka", e);
            return false;
        }
    }
    
    @Async
    @Override
    public void addBadCard(final Map<String, BadCardData> badCards, final Integer unifiId) {
        if (unifiId == null) {
            throw new IllegalArgumentException("Customer UnifiId is not defined");
        }
        
        for (BadCardData badCard : badCards.values()) {
            try {
                this.pciLinkMessageProducer.sendWithByteArray(KafkaKeyConstants.LINK_BAD_CARD_SERVICE_ADD, String.valueOf(unifiId),
                                                              createBadCardMessage(badCard))
                        .get();
            } catch (InvalidTopicTypeException | JsonException | InvalidDataException | CryptoException | InterruptedException
                    | ExecutionException e) {
                LOG.error("Unable to send bad card message to kafka", e);
            }
        }
    }
    
    @Override
    public boolean deleteBadCard(final BadCardData badCard) {
        
        if (badCard.getCustomer() == null) {
            throw new IllegalArgumentException("BadCardData must have a customer");
        }
        
        if (badCard.getCustomer().getUnifiId() == null) {
            throw new IllegalArgumentException("Customer UnifiId is not defined");
        }
        
        try {
            return BooleanUtils.isTrue(this.pciLinkMessageProducer
                    .sendWithByteArray(KafkaKeyConstants.LINK_BAD_CARD_SERVICE_DELETE, String.valueOf(badCard.getCustomer().getUnifiId()),
                                       createDeleteBadCardMessage(badCard))
                    .get());
        } catch (InvalidTopicTypeException | JsonException | InvalidDataException | CryptoException | InterruptedException | ExecutionException e) {
            LOG.error("Unable to send bad card message to kafka", e);
            return false;
        }
    }
    
    @Override
    public void deleteBadCardsbyCustomerId(final Integer customerId) {
        
        try {
            final String stringResult = this.badCardClient.deleteBadCards(customerId).execute().toString(Charset.defaultCharset());
            LOG.debug(String.format("Result call of Deleted bad cards: ", stringResult));
            
        } catch (HystrixRuntimeException e) {
            LOG.error("Unable to get successful response from " + BadCardClient.ALIAS, e);
        }
    }
    
    private BadCardMessage createBadCardMessage(final BadCardData badCard) throws CryptoException, InvalidDataException {
        
        final Integer unifiId = badCard.getCustomer().getUnifiId();
        
        if (unifiId == null) {
            throw new IllegalArgumentException(
                    String.format("BadCard Data could not be sent to BadCardService, customer %d has no UnifiId", badCard.getCustomer().getId()));
        }
        
        final BadCardMessage badCardMessage = new BadCardMessage();
        
        final CustomerCardType customerCardType = determineCustomerCardType(badCard.getCardNumber(), badCard.getCustomerCardTypeId(),
                                                                            badCard.getParentCardType(), badCard.getCustomer().getId());
        
        if (customerCardType == null || StringUtils.isBlank(customerCardType.getName())) {
            
            // In this case we don't know what the card is and we just set both card types to credit card
            final String creditCard = this.cardTypeService.getCardTypesMap().get(WebCoreConstants.CARD_TYPE_CREDIT_CARD).getName();
            badCardMessage.setCardType(creditCard);
            badCardMessage.setCardParentType(creditCard);
        } else {
            badCardMessage.setCardType(customerCardType.getName());
            badCardMessage.setCardParentType(this.cardTypeService.getCardTypesMap().get(customerCardType.getCardType().getId()).getName());
        }
        
        badCardMessage.setCardExpiry(badCard.getExpiry());
        
        if (badCard.getCardNumber().length() >= CardProcessingConstants.LAST_4_DIGITS_LENGTH) {
            badCardMessage.setLast4Digits(badCard.getCardNumber()
                    .substring(badCard.getCardNumber().length() - CardProcessingConstants.LAST_4_DIGITS_LENGTH, badCard.getCardNumber().length()));
        } else {
            badCardMessage.setLast4Digits(badCard.getCardNumber());
        }
        
        badCardMessage.setSha1CardHash(this.cryptoAlgorithmFactory.getSha1Hash(badCard.getCardNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD));
        badCardMessage.setSha256CardHash(this.cryptoAlgorithmFactory.getShaHash(badCard.getCardNumber(), CryptoConstants.HASH_LINK_BAD_CREDIT_CARD,
                                                                                CryptoConstants.HASH_ALGORITHM_TYPE_SHA256));
        badCardMessage.setSource(badCard.getSource());
        badCardMessage.setComment(badCard.getComment());
        badCardMessage.setAddedUTCInstant(Instant.now());
        badCardMessage.setCustomerId(unifiId);
        
        return badCardMessage;
    }
    
    private BadCardMessage createDeleteBadCardMessage(final BadCardData badCard) throws CryptoException, InvalidDataException {
        
        final BadCardMessage badCardMessage = new BadCardMessage();
        badCardMessage.setId(badCard.getId());
        badCardMessage.setCustomerId(badCard.getCustomer().getUnifiId());
        badCardMessage.setSource(badCard.getSource());
        badCardMessage.setAddedUTCInstant(Instant.now());
        
        return badCardMessage;
    }
    
    public AbstractMessageProducer getLinkMessageProducer() {
        return this.pciLinkMessageProducer;
    }
    
    public void setLinkMessageProducer(final AbstractMessageProducer linkMessageProducer) {
        this.pciLinkMessageProducer = linkMessageProducer;
    }
    
    @Override
    public CustomerCardType determineCustomerCardType(final String cardNumber, final Integer customerCardTypeId, final String cardTypeName,
        final int customerId) {
        
        CustomerCardType customerCardType = null;
        
        if (cardNumber != null) {
            // check if the card is a credit card
            final String creditCardTypeName = this.linkCardTypeService.getCreditCardTypeName(cardNumber);
            
            if (StringUtils.isBlank(creditCardTypeName)) {
                
                // provided by UI
                if (customerCardTypeId == null) {
                    
                    // needed for CSV
                    final List<CustomerCardType> cardTypes = this.customerCardTypeService
                            .getDetachedBannableCardTypes(customerId, Arrays.asList(cardTypeName.toLowerCase(Locale.getDefault())));
                    
                    if (cardTypes != null && !cardTypes.isEmpty()) {
                        customerCardType = cardTypes.get(0);
                    }
                } else {
                    customerCardType = this.customerCardTypeService.getCustomerCardTypeById(customerCardTypeId);
                }
            } else {
                // valid credit card
                final CardType cardType = this.cardTypeService.getCardTypesMap().get(WebCoreConstants.CARD_TYPE_CREDIT_CARD);
                customerCardType = new CustomerCardType();
                customerCardType.setCardType(cardType);
                customerCardType.setName(creditCardTypeName);
            }
        }
        return customerCardType;
    }
        
    @Override
    public void buildBadCardCreditCardFile(final ByteArrayOutputStream baos, final Customer customer, final int cardType) throws IOException {
        try (ZipOutputStream zipfos = new ZipOutputStream(baos)) {
            
            final CustomerProperty cardRetryProp = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(),
                                                                            WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY);
            
            final int cardRetries = cardRetryProp == null ? 0 : Integer.parseInt(cardRetryProp.getPropertyValue());
            
            final int unifiId = customer.getUnifiId();
            
            final String jsonResponse = this.badCardClient.getBadCardsCompact(unifiId, CryptoConstants.SHA_1).execute().toString(Charset.defaultCharset());
            final ObjectResponse<SortedSet<String>> hashesObj =
                    this.json.deserialize(jsonResponse, new TypeReference<ObjectResponse<SortedSet<String>>>() {
                    });
            final SortedSet<String> hashes = hashesObj.getResponse();
            
            zipfos.putNextEntry(new ZipEntry(CardProcessingConstants.ZIP_ENTRY_BAD_CREDITCARD));
            
            if (cardType == WebCoreConstants.CARD_TYPE_CREDIT_CARD && this.merchantAccountService.hasIrisMerchantAccount(customer.getId())) {
                appendCardRetryHashes(customer, cardRetries, hashes);
            }
            
            for (String hash : hashes) {
                zipfos.write((hash + WebCoreConstants.NIX_NEXT_LINE_SYMBOL).getBytes());
            }
            
            zipfos.closeEntry();
            zipfos.finish();
            
        } catch (IOException | JsonException ioe) {
            LOG.error("Unable to write BadCard file response", ioe);
            throw new IOException(ioe);
        } catch (HystrixRuntimeException hre) {
            LOG.error("Error communicating with BadCardService", hre);
            throw new IOException(hre);
        }
    }
    
    private void appendCardRetryHashes(final Customer customer, final int cardRetries, final SortedSet<String> hashes) {
        final List<Integer> pointOfSaleIds = this.pointOfSaleService.findPosIdsByCustomerId(customer.getId());
        if (pointOfSaleIds != null && !pointOfSaleIds.isEmpty()) {
            final List<String> cardRetryHashes = this.cardRetryTransactionService
                    .findBadCardHashByPointOfSaleIdsDateAndNumRetries(pointOfSaleIds, StableDateUtil.createCurrentYYMM(), cardRetries);
            if (cardRetryHashes != null && !cardRetryHashes.isEmpty() && !hashes.addAll(cardRetryHashes)) {
                LOG.warn("Duplicate hash found when processing card retry bad cards for customer " + customer.getId());
            }
        }
    }

    @Override
    public boolean activateNotificationIfRequired(final Customer customer) throws SystemException {
        final BadCardFileInfo badCardFileInfo;
        try {
            final String jsonResp = this.badCardClient.getBadCardFileInfo(customer.getUnifiId()).execute().toString(Charset.defaultCharset());
            final ObjectResponse<BadCardFileInfo> objResp = this.json.deserialize(jsonResp, new TypeReference<ObjectResponse<BadCardFileInfo>>() { 
            });
            badCardFileInfo = objResp.getResponse();
            if (badCardFileInfo == null) {
                final String err = CUSTOMER_ID + customer.getId() + ", unifi id: " + customer.getUnifiId()
                                   + " - unable to retrieve BadCardFileInfo (hash & file creation date";
                LOG.error(err);
                throw new SystemException(err);
            }
        } catch (JsonException je) {
            LOG.error("Unable to write BadCardFileInfo response", je);
            throw new SystemException(je);
            
        } catch (HystrixRuntimeException hre) {
            
            LOG.error("Error communicating with BadCardService, root cause:\r\n" 
                      + HystrixExceptionUtil.getRootCause(hre), hre);
            throw new SystemException(hre);
        }

        final boolean isNewBadCardListFlag = compareWithCustomer(customer, badCardFileInfo);
        LOG.info(CUSTOMER_ID + customer.getId() + ", did Iris set isNewBadCardListFlag? " + isNewBadCardListFlag);
        
        return isNewBadCardListFlag;
    }
    
    private boolean compareWithCustomer(final Customer customer, final BadCardFileInfo badCardFileInfo) {
        // The initial usage of BadCardFile collection would be empty.
        if (StringUtils.isBlank(badCardFileInfo.getHash()) || StringUtils.isBlank(badCardFileInfo.getFileCreationUTC())) {
            return false;
        }

        // Compare BadCardFileInfo with Customer NewBadCardListHash and NewBadCardListCreationDate.
        boolean hasNewValue = false;
        final Date fileCreationDate 
            = DateUtil.parse(badCardFileInfo.getFileCreationUTC(), DateUtil.UTC_CPS_DATE_TIME_FORMAT, WebCoreConstants.GMT, true);
        final GregorianCalendar fileCreationCal = new GregorianCalendar();
        fileCreationCal.setTime(fileCreationDate);

        if (customer.getNewBadCardListCreationDate() == null && fileCreationDate != null) {
            final Pair<Boolean, Collection<PosServiceState>> hasPosStates = getPosServiceStates(customer);
            if (hasPosStates.getLeft().booleanValue()) {
                hasNewValue = setCreationHashAndIsNew(customer, hasPosStates.getRight(), fileCreationDate, badCardFileInfo.getHash());
            }
        }
        
        if (!hasNewValue && customer.getNewBadCardListCreationDate() != null) {
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(customer.getNewBadCardListCreationDate());
            if (fileCreationCal.after(cal)) {
                final Pair<Boolean, Collection<PosServiceState>> hasPosStates = getPosServiceStates(customer);
                if (hasPosStates.getLeft().booleanValue()) {
                    hasNewValue = setCreationHashAndIsNew(customer, hasPosStates.getRight(), fileCreationDate, badCardFileInfo.getHash());
                }
            }
        }
        return hasNewValue;
    }
    
    private boolean setCreationHashAndIsNew(final Customer customer,
                                            final Collection<PosServiceState> posStates,
                                            final Date fileCreationDate, 
                                            final String hash) {
        customer.setNewBadCardListCreationDate(fileCreationDate);
        customer.setNewBadCardListHash(hash);
        customer.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        posStates.forEach(posState -> {
                posState.setIsNewBadCardList(true);
                posState.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            });
        
        this.customerService.update(customer);
        this.posServiceStateService.updatePosServiceStates(posStates);
        return true;
    }
    
    private Pair<Boolean, Collection<PosServiceState>> getPosServiceStates(final Customer customer) {
        final Collection<PosServiceState> posStates = this.posServiceStateService.findLinuxPosServiceStatesByLinkCustomerId(customer.getUnifiId());
        if (posStates == null || posStates.isEmpty()) {
            LOG.error("Cannot retrieve POSServiceState records by calling findLinuxPosServiceStatesByLinkCustomerId, unifi id: " + customer.getUnifiId());
            return new Pair<>(Boolean.FALSE, null);
        }
        return new Pair<>(Boolean.TRUE, posStates);
    }

    // ----------- For integration testing only -----------
    public final void setBadCardClient(final BadCardClient badCardClient) {
        this.badCardClient = badCardClient;
    }
}
