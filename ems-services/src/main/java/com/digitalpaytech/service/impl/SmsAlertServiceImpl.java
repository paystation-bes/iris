package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.service.SmsAlertService;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("smsAlertService")
@Transactional(propagation = Propagation.SUPPORTS)
public class SmsAlertServiceImpl implements SmsAlertService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public List<SmsAlert> findByAddTimeNumCustomerIdAndSpaceNumber(Integer customerId, int spaceNumber, int addTimeNumber) {
        String[] qParams = new String[] { "customerId", "twoWeeksBefore", "addTimeNum", "spaceNumber" };
        Object[] qValues = new Object[] { customerId, DateUtils.addDays(new Date(), -14), addTimeNumber, spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByAddTimeNumCustomerIdAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public List<SmsAlert> findByAddTimeNumCustomerIdPaystationSettingNameAndSpaceNumber(Integer customerId, String paystationSettingName, int spaceNumber,
                                                                                        int addTimeNumber) {
        String[] qParams = new String[] { "customerId", "paystationSettingName", "twoWeeksBefore", "addTimeNum", "spaceNumber" };
        Object[] qValues = new Object[] { customerId, paystationSettingName, DateUtils.addDays(new Date(), -14), addTimeNumber, spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByAddTimeNumCustomerIdPaystationSettingNameAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public List<SmsAlert> findByAddTimeNumLocationIdAndSpaceNumber(Integer locationId, int spaceNumber, int addTimeNumber) {
        String[] qParams = new String[] { "locationId", "twoWeeksBefore", "addTimeNum", "spaceNumber" };
        Object[] qValues = new Object[] { locationId, DateUtils.addDays(new Date(), -14), addTimeNumber, spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByAddTimeNumLocationIdAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public List<SmsAlert> findByLicencePlate(Integer customerId, String licencePlate) {
        String[] qParams = new String[] { "customerId", "twoWeeksBefore", "licencePlate" };
        Object[] qValues = new Object[] { customerId, DateUtils.addDays(new Date(), -14), licencePlate };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByLicencePlate", qParams, qValues);
        
    }
    
    @Override
    public List<SmsAlert> findByCustomerIdPaystationSettingNameAndSpaceNumber(Integer customerId, String paystationSettingName, int spaceNumber) {
        String[] qParams = new String[] { "customerId", "paystationSettingName", "twoWeeksBefore", "spaceNumber" };
        Object[] qValues = new Object[] { customerId, paystationSettingName, DateUtils.addDays(new Date(), -14), spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByCustomerIdPaystationSettingNameAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public List<SmsAlert> findByCustomerIdAndSpaceNumber(Integer customerId, int spaceNumber) {
        String[] qParams = new String[] { "customerId", "twoWeeksBefore", "spaceNumber" };
        Object[] qValues = new Object[] { customerId, DateUtils.addDays(new Date(), -14), spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByCustomerIdAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public List<SmsAlert> findByLocationIdAndSpaceNumber(Integer locationId, int spaceNumber) {
        String[] qParams = new String[] { "locationId", "twoWeeksBefore", "spaceNumber" };
        Object[] qValues = new Object[] { locationId, DateUtils.addDays(new Date(), -14), spaceNumber };
        return entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByLocationIdAndSpaceNumber", qParams, qValues);
    }
    
    @Override
    public SmsAlert findByMobileNumber(String mobileNumber) {
        List<SmsAlert> list = entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByMobileNumber", "mobileNumber", mobileNumber);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public SmsAlert findSmsAlertById(Integer id) {
        return (SmsAlert) this.entityDao.get(SmsAlert.class, id);
    }
    
    @Override
    public SmsAlert findUnexpiredSmsAlertById(Integer id) {
        String[] qParams = new String[] { "smsAlertId", "currentTime" };
        Object[] qValues = new Object[] { id, new Date() };
        return (SmsAlert) entityDao.findUniqueByNamedQueryAndNamedParam("SmsAlert.findUnExpiredById", qParams, qValues,
                                            true);
    }    
    
    public static final String SQL_GET_SMSALERT_NOTALERTED = "SELECT COUNT(*) FROM SmsAlert sa WHERE sa.IsAlerted = 0";
    
    @Override
    public int getSmsAlertNotAlerted() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_SMSALERT_NOTALERTED);
        int ret = Integer.parseInt(q.list().get(0).toString());
        return ret;
    }
}
