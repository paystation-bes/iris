package com.digitalpaytech.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.service.WebWidgetHelperService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetPurchasesService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetPurchasesServiceImpl implements WebWidgetService {

    public static final HashMap<Integer, String> TIER_TYPES_FIELDS_MAP = new HashMap<Integer, String>(20);
    public static final HashMap<Integer, String> TIER_TYPES_ALIASES_MAP = new HashMap<Integer, String>(4);
    static {
        // Only time types are allowed to have more than one field under the same type.
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_HOUR, "t.Date, t.DayOfYear, t.HourOfDay ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_DAY, "t.Date, t.DayOfYear ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_MONTH, "t.Year, t.Month, t.MonthNameAbbrev ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_ROUTE, "rp.RouteId, pppd.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PAYSTATION, "pppd.PointOfSaleId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "pppd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "pppd.PurchaseLocationId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_REVENUE_TYPE, "pppr.RevenueTypeId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_TRANSACTION_TYPE, "pppd.TransactionTypeId ");
        TIER_TYPES_FIELDS_MAP.put(WidgetConstants.TIER_TYPE_RATE, "pppd.UnifiedRateId ");
        
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_LOCATION, "LocationId");
        TIER_TYPES_ALIASES_MAP.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "LocationId");
    }

    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private WebWidgetHelperService webWidgetHelperService;
    @Autowired
    private CustomerAdminService customerAdminService;
    @Autowired
    private UnifiedRateService unifiedRateService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setWebWidgetHelperService(WebWidgetHelperService webWidgetHelperService) {
        this.webWidgetHelperService = webWidgetHelperService;
    }
    
    public void setCustomerAdminService(CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public void setUnifiedRateService(UnifiedRateService unifiedRateService) {
        this.unifiedRateService = unifiedRateService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount) {
        
        StringBuilder queryStr = new StringBuilder();
        
        String queryLimit = emsPropertiesService.getPropertyValue(EmsPropertiesService.MAX_WIDGET_DATA_QUERY_ROW_LIMIT,
                                                                  EmsPropertiesService.MAX_DEFAULT_WIDGET_DATA_QUERY_ROW_LIMIT);
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        int tableDepthType = 0;
        if (tier1Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE || tier2Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE
            || tier3Id == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_REVENUE;
        } else if (tier1Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier1Id == WidgetConstants.TIER_TYPE_LOCATION
                   || tier1Id == WidgetConstants.TIER_TYPE_ROUTE || tier1Id == WidgetConstants.TIER_TYPE_PAYSTATION
                   || tier1Id == WidgetConstants.TIER_TYPE_RATE || tier2Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION
                   || tier2Id == WidgetConstants.TIER_TYPE_LOCATION || tier2Id == WidgetConstants.TIER_TYPE_ROUTE || tier2Id == WidgetConstants.TIER_TYPE_RATE
                   || tier3Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier3Id == WidgetConstants.TIER_TYPE_LOCATION
                   || tier3Id == WidgetConstants.TIER_TYPE_ROUTE || tier3Id == WidgetConstants.TIER_TYPE_RATE
                   || tier3Id == WidgetConstants.TIER_TYPE_PARENT_LOCATION || tier3Id == WidgetConstants.TIER_TYPE_LOCATION) {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_DETAIL;
        } else {
            tableDepthType = WidgetConstants.TABLE_DEPTH_TYPE_TOTAL;
        }
        
        int timeRange = widget.getWidgetRangeType().getId();
        int tableRangeType = 0;
        switch (timeRange) {
            case WidgetConstants.RANGE_TYPE_TODAY:
            case WidgetConstants.RANGE_TYPE_YESTERDAY:
                if (tier1Id == WidgetConstants.TIER_TYPE_HOUR) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_24HOURS:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_HOUR;
                break;
            case WidgetConstants.RANGE_TYPE_LAST_7DAYS:
            case WidgetConstants.RANGE_TYPE_LAST_30DAYS:
//            case WidgetConstants.RANGE_TYPE_THIS_WEEK:
            case WidgetConstants.RANGE_TYPE_LAST_WEEK:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                break;
//            case WidgetConstants.RANGE_TYPE_THIS_MONTH:
            case WidgetConstants.RANGE_TYPE_LAST_MONTH:
                if (tier1Id == WidgetConstants.TIER_TYPE_DAY) {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_DAY;
                } else {
                    tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                }
                break;
            case WidgetConstants.RANGE_TYPE_LAST_12MONTHS:
//            case WidgetConstants.RANGE_TYPE_THIS_YEAR:
            case WidgetConstants.RANGE_TYPE_LAST_YEAR:
            case WidgetConstants.RANGE_TYPE_YEAR_TO_DATE:
                tableRangeType = WidgetConstants.TABLE_RANGE_TYPE_MONTH;
                break;
        }
        
        /* Main SELECT expression */
        this.webWidgetHelperService.appendSelection(queryStr, widget);
        queryStr.append(", IFNULL(SUM(wData.WidgetMetricValue), 0) AS WidgetMetricValue ");
        
        /* Main FROM expression */
        queryStr.append("FROM (");
        
        /* Prepare subsetMap */
        Map<Integer, StringBuilder> subsetMap = this.webWidgetHelperService.createTierSubsetMap(widget);
    	
        /* Sub JOIN Temp Table "wData" */
    	String tableType = null;
        if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_HOUR) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_HOUR_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_DAY) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_DAY_STRING;
        } else if (tableRangeType == WidgetConstants.TABLE_RANGE_TYPE_MONTH) {
            tableType = WidgetConstants.TABLE_RANGE_TYPE_MONTH_STRING;
        }
        
        /* "wData" SELECT expression */
        queryStr.append("SELECT pppt.CustomerId");
        this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, TIER_TYPES_ALIASES_MAP, false);
        
        if(tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_REVENUE) {
        	queryStr.append(", SUM(pppr.TotalCount) AS WidgetMetricValue ");
        }
        else if(tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_DETAIL) {
        	queryStr.append(", SUM(pppd.TotalCount) AS WidgetMetricValue ");
        }
        else {
        	queryStr.append(", SUM(pppt.TotalCount) AS WidgetMetricValue ");
        }
        
        /* "wData" FROM expression */
        queryStr.append("FROM PPPTotal");
        queryStr.append(tableType);
        queryStr.append(" pppt ");
        if (tableDepthType != WidgetConstants.TABLE_DEPTH_TYPE_TOTAL) {
        	queryStr.append("INNER JOIN PPPDetail");
        	queryStr.append(tableType);
        	queryStr.append(" pppd ON pppd.PPPTotal");
        	queryStr.append(tableType);
        	queryStr.append("Id = pppt.Id ");
        }
        if (tableDepthType == WidgetConstants.TABLE_DEPTH_TYPE_REVENUE) {
        	queryStr.append("INNER JOIN PPPRevenue");
        	queryStr.append(tableType);
        	queryStr.append(" pppr ON pppr.PPPDetail");
        	queryStr.append(tableType);
        	queryStr.append("Id = pppd.Id ");
        }
    	
        queryStr.append("INNER JOIN Time t ON(pppt.TimeIdLocal = t.Id ");
        
    	this.webWidgetHelperService.calculateRange(widget
        		, userAccount.getCustomer().getId()
        		, queryStr
        		, widget.getWidgetRangeType().getId(), widget.getWidgetTierTypeByWidgetTier1Type().getId(), true, true);
        
    	queryStr.append(") ");
        
    	/* "wData" WHERE expression to speed things up */
    	this.webWidgetHelperService.appendDataConditions(queryStr, widget, "pppt.CustomerId", TIER_TYPES_FIELDS_MAP, subsetMap);
    	
    	/* "wData" GROUP BY expression */
    	queryStr.append("GROUP BY pppt.CustomerId");
    	this.webWidgetHelperService.appendFieldMember(queryStr, widget, TIER_TYPES_FIELDS_MAP, (Map<Integer, String>) null, true);
        
    	queryStr.append(") AS wData ");
        
    	/* Sub JOIN Temp Table "wLabel */
        this.webWidgetHelperService.appendLabelTable(queryStr, widget, subsetMap, false, null, false);
        
        /* Main GROUP BY */
        this.webWidgetHelperService.appendGrouping(queryStr, widget);
        
        /* Main ORDER BY */
        this.webWidgetHelperService.appendOrdering(queryStr, widget);
        
        /* Main LIMIT */
        this.webWidgetHelperService.appendLimit(queryStr, widget, queryLimit);
        
        SQLQuery query = entityDao.createSQLQuery(queryStr.toString());
        
        query = WebWidgetUtil.querySetParameter(webWidgetHelperService, widget, query, userAccount, false);
        query = WebWidgetUtil.queryAddScalar(widget, query);
        
        query.setResultTransformer(Transformers.aliasToBean(WidgetMetricInfo.class));
        query.setCacheable(true);
        
        WidgetData widgetData = new WidgetData();
        
        @SuppressWarnings("unchecked")
        List<WidgetMetricInfo> rawWidgetData = query.list();
        
        widgetData = webWidgetHelperService.convertData(widget, rawWidgetData, userAccount, queryLimit);
        widgetData.setTrendValue(getWidgetTrendValues(widget, widgetData, userAccount));
        
        return widgetData;
    }
    
    protected List<String> getWidgetTrendValues(Widget widget, WidgetData widgetData, UserAccount userAccount) {
        
        List<String> trendValue = new ArrayList<String>();
        
        if (!(widget.getTrendAmount() == null || widget.getTrendAmount() == 0)) {
            Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
            trendValue.add(dollarValue.toString());
            return trendValue;
        }
        
        return null;
    }
    
    protected void appendWhere(StringBuilder query, Widget widget, UserAccount userAccount) {
        if (userAccount.getCustomer().isIsParent()) {
            query.append("WHERE x.CustomerId IN (:customerId) ");
        } else {
            query.append("WHERE x.CustomerId = :customerId ");
        }
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        switch (tier1Id) {
            case WidgetConstants.TIER_TYPE_HOUR:
                //query.append("AND t.QuarterOfHour = 0 ");
                break;
            case WidgetConstants.TIER_TYPE_ORG:
                /* Organization */
                query.append("AND x.ParentCustomerId IS NOT NULL ");
                break;
            default:
                break;
        }
        
        // TODO will not be added to the sums
        //query.append("AND (pi.TransactionTypeId NOT IN (4,6) OR pi.TransactionTypeId IS NULL) ");
        
        /*
         * int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
         * int tier3Id = widget.getWidgetTierTypeByWidgetTier3Type().getId();
        
         * if (tier1Id == WidgetConstants.TIER_TYPE_ROUTE ||
         * tier2Id == WidgetConstants.TIER_TYPE_ROUTE ||
         * tier3Id == WidgetConstants.TIER_TYPE_ROUTE)
         * {
         * //RouteType.Id = 1 is a `Collections` type of Route
         * query.append("AND (ro.RouteTypeId = 1 OR ro.RouteTypeId is null) ");
         * }
         */
    }
}
