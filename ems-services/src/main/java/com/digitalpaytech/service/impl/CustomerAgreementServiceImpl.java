package com.digitalpaytech.service.impl;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.EmsPropertiesService;

@Component("customerAgreementService")
@Transactional(propagation=Propagation.SUPPORTS)
public class CustomerAgreementServiceImpl implements CustomerAgreementService {

	@Autowired
	private EmsPropertiesService emsPropertiesService;
	
	@Autowired
	private EntityDao entityDao;
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}

	public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
		this.emsPropertiesService = emsPropertiesService;
	}

	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public CustomerAgreement findCustomerAgreementByCustomerId(
			Integer customerId) {
		
		List<CustomerAgreement> results = this.entityDao
				.findByNamedQueryAndNamedParam(
						"CustomerAgreement.findCustomerAgreementByCustomerId",
						"customerId", customerId, true);
		if (results.isEmpty()) {
			return null;
		}
		
		return results.get(0);
	}
	
	public void sendServiceAgreementEmail(Integer customerId, String name,
			String title, String organization) throws AddressException,
			MessagingException {

		// Get system properties
		Properties properties = System.getProperties();
		// Setup mail server
		properties.setProperty("mail.smtp.host", getMailServerUrl());
		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);

		// Set From: header field of the header.
		message.setFrom(new InternetAddress(getEmsAlertFromEmailAddress()));

		// Set To: header field of the header.
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(getServiceAgreementToEmailAddress()));

		String subject = "Terms Accepted - (CustomerId:" + customerId + ")";
		String content = name + ", " + title + ", from " + organization
				+ " has accepted the IRIS Terms of Service.";
		
		// Set Subject: header field
		message.setSubject(subject);

		// Now set the actual message
		message.setText(content);

		// Send message
		Transport.send(message);
	}
	
	public String getEmsAlertFromEmailAddress() {
		return emsPropertiesService.getPropertyValue(
				EmsPropertiesService.EMS_ALERT_FROM_EMAIL_ADDRESS,
				EmsPropertiesService.EMS_ADDRESS_STRING);
	}

	public String getServiceAgreementToEmailAddress() {
		return emsPropertiesService.getPropertyValue(
				EmsPropertiesService.SERVICE_AGREEMENT_TO_EMAIL_ADDRESS,
				EmsPropertiesService.EMS_SERVICE_AGREEMENT_TO_ADDRESS_STRING);
	}

	public String getMailServerUrl() {
		return emsPropertiesService
				.getPropertyValue(EmsPropertiesService.MAIL_SERVER_URL);
	}
}
