package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ReportTypeService;


@Service("reportTypeService")
public class ReportTypeServiceImpl implements ReportTypeService {

	@Autowired
	private EntityService entityService;

	private List<ReportType> reportTypes;
    private Map<Integer, ReportType> reportTypesMap;

	   public void setEntityService(EntityService entityService) {
	        this.entityService = entityService;
	    }


	@Override
	public List<ReportType> loadAll() {
		return entityService.loadAll(ReportType.class);
	}
    @Override
    public Map<Integer, ReportType> getReportTypesMap() {
        if (reportTypes == null) {
        	reportTypes = loadAll();
        }
        if (reportTypesMap == null) {
            reportTypesMap = new HashMap<Integer, ReportType>(reportTypes.size());
            Iterator<ReportType> iter = reportTypes.iterator();
            while (iter.hasNext()) {
                ReportType reportType = iter.next();
                reportTypesMap.put(new Integer(reportType.getId()), reportType);
            }
        }
        return reportTypesMap;
    }

    @Override
    public String getText(int reportId) {
        if (reportTypesMap == null)
            getReportTypesMap();
        
        if (!reportTypesMap.containsKey(reportId))
            return null;
        else
            return reportTypesMap.get(reportId).getName();
    }

}
