package com.digitalpaytech.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.dto.customeradmin.ActivityLogDetail;
import com.digitalpaytech.service.ActivityLogService;

@Service("activityLogService")
@Transactional(propagation = Propagation.REQUIRED)
public class ActivityLogServiceImpl implements ActivityLogService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<ActivityLog> findActivityLogsByUserAccountId(final Integer userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ActivityLog.findActivityLogByUserAccountId", "userAccountId", userAccountId, true);
    }
    
    @Override
    public final List<ActivityType> findActivityTypesByParentActivityTypeId(final Integer parentActivityTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ActivityType.findActivityTypeByParentActivityTypeId", "parentActivityTypeId",
                                                            parentActivityTypeId, true);
    }
    
    @Override
    public List<ActivityLog> findActivityLogWithCriteria(final Calendar start, final Calendar end, final List<Integer> userIds,
        final List<Integer> logTypes, final String order, final String column, final int page, final int numOfRecord) {
        
        Criteria crit = this.entityDao.createCriteria(ActivityLog.class);
        
        crit = crit.createAlias("userAccount", "activityLogUserAccount");
        crit = crit.createAlias("activityType", "activityLogActivityType");
        //crit.add(Restrictions.between("activityGmt",start,end));
        if (start != null) {
            crit.add(Restrictions.ge("activityGmt", start.getTime()));
        }
        if (end != null) {
            crit.add(Restrictions.le("activityGmt", end.getTime()));
        }
        
        Criterion userCr;
        Criterion logCr;
        Criterion userOR = null;
        Criterion logOR = null;
        
        for (int i = 0; i < userIds.size(); i++) {
            userCr = Restrictions.eq("activityLogUserAccount.id", userIds.get(i));
            if (userOR != null) {
                userOR = Restrictions.or(userOR, userCr);
            } else {
                userOR = userCr;
            }
        }
        
        for (int i = 0; i < logTypes.size(); i++) {
            logCr = Restrictions.eq("activityLogActivityType.id", logTypes.get(i));
            if (logOR != null) {
                logOR = Restrictions.or(logOR, logCr);
            } else {
                logOR = logCr;
            }
        }
        
        crit = crit.add(userOR);
        crit = crit.add(logOR);
        
        if (!StringUtils.isBlank(order) && order.equals("ASC")) {
            if (!StringUtils.isBlank(column) && column.equals("date")) {
                crit.addOrder(Order.asc("activityGmt")).addOrder(Order.asc("activityLogUserAccount.userName"));
            } else if (!StringUtils.isBlank(column) && column.equals("username")) {
                crit.addOrder(Order.asc("activityLogUserAccount.userName")).addOrder(Order.asc("activityGmt"));
            } else {
                crit.addOrder(Order.asc("activityGmt"));
            }
        } else if (!StringUtils.isBlank(order) && order.equals("DESC")) {
            if (!StringUtils.isBlank(column) && column.equals("date")) {
                crit.addOrder(Order.desc("activityGmt")).addOrder(Order.desc("activityLogUserAccount.userName"));
            } else if (!StringUtils.isBlank(column) && column.equals("username")) {
                crit.addOrder(Order.desc("activityLogUserAccount.userName")).addOrder(Order.desc("activityGmt"));
            } else {
                crit.addOrder(Order.desc("activityGmt"));
            }
        } else {
            crit.addOrder(Order.desc("activityGmt")).addOrder(Order.asc("activityLogUserAccount.userName"));
        }
        
        crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        if (page == 1) {
            crit.setFirstResult(0);
        } else {
            crit.setFirstResult(numOfRecord * (page - 1));
        }
        
        crit.setMaxResults(numOfRecord);
        
        @SuppressWarnings("unchecked")
        final List<ActivityLog> list = crit.setCacheable(true).list();
        return list;
    }
    
    @Override
    public final ActivityLogDetail getActivityLogDetailsByActivityLogId(final String activityLogId) {
        
        //System.out.println(activityLogId);
        
        final List<ActivityLog> list = this.entityDao.findByNamedQueryAndNamedParam("ActivityLog.findActivityLogById", "activityLogId",
                                                                                    Integer.parseInt(activityLogId), true);
        final ActivityLog activityLog = list.get(0);
        
        final String logTableName = activityLog.getEntityType().getName().replaceAll("\\s", "") + "_log";
        
        final Integer entityId = activityLog.getEntityId();
        final Integer entityLogId = activityLog.getEntityLogId();
        
        final StringBuilder query = new StringBuilder();
        query.append("SELECT * ");
        query.append("FROM " + logTableName + " ");
        query.append("WHERE " + logTableName + ".Id = " + entityId + " ");
        query.append("AND " + logTableName + ".LogId <= " + entityLogId + " ");
        query.append("ORDER BY " + logTableName + ".LogId DESC LIMIT 2;");
        
        //System.out.println(query.toString());	
        
        final SQLQuery sqlQuery = this.entityDao.createSQLQuery(query.toString());
        
        final List<Object[]> data = sqlQuery.list();
        
        for (Object[] entry : data) {
            for (int i = 0; i < entry.length; i++) {
                System.out.print(entry[i] + ", ");
            }
            //System.out.println();
        }
        //System.out.println();
        
        return null;
    }
    
    @Override
    public final List<ActivityType> findAllChildActivityTypes() {
        return this.entityDao.findByNamedQuery("ActivityType.findAllChildActivityTypes");
    }
    
    @Override
    public ActivityType findActivityTypeByActivityId(final int activityTypeId) {
        return (ActivityType) this.entityDao.get(ActivityType.class, activityTypeId);
    }
    
}
