package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ReportCollectionFilterType;
import com.digitalpaytech.service.ReportCollectionFilterTypeService;

@Service("reportCollectionFilterTypeService")
public class ReportCollectionFilterTypeServiceImpl implements ReportCollectionFilterTypeService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    @Override
    public ReportCollectionFilterType getReportCollectionFilterType(short reportCollectionFilterTypeId) {
        return this.entityDao.get(ReportCollectionFilterType.class, reportCollectionFilterTypeId);
    }
    
}
