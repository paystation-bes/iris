package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileAppActivityType;
import com.digitalpaytech.domain.MobileAppHistory;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.MobileAppActivityTypeService;
import com.digitalpaytech.service.MobileAppHistoryService;
import com.digitalpaytech.util.WebCoreConstants;
@Service("MobileAppHistoryService")
public class MobileAppHistoryServiceImpl implements MobileAppHistoryService{
    @Autowired
    EntityDao entityDao;
    
    @Autowired
    MobileAppActivityTypeService mobileAppActivityTypeService;
    
    @Override
    public List<MobileAppHistory> getMobileAppHistoryByCustomer(Integer customerId){
        return entityDao.findByNamedQueryAndNamedParam("MobileAppHistory.findByCustomerId", new String[]{"customerId"}, new Object[]{customerId}, true);
    }
    
    @Override
    public List<MobileAppHistory> getMobileAppHistoryByDeviceUid(String uid){
        return entityDao.findByNamedQueryAndNamedParam("MobileAppHistory.findByUid", new String[]{"uid"}, new Object[]{uid}, true);
    }
    
    @Override
    public List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceId(Integer customerMobileDeviceId, Integer page, Integer pageSize){
        return entityDao.findPageResultByNamedQuery("MobileAppHistory.findByCustomerMobileDeviceId", new String[]{"customerMobileDeviceId"}, new Object[]{customerMobileDeviceId}, true, page, pageSize);
        //return entityDao.findByNamedQueryAndNamedParam("MobileAppHistory.findByCustomerMobileDeviceId", new String[]{"customerMobileDeviceId"}, new Object[]{customerMobileDeviceId}, true);
    }
    
    @Override
    public List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceIdAndOrder(Integer customerMobileDeviceId, Integer page, Integer pageSize, String column, String order){
        Criteria query = entityDao.createCriteria(MobileAppHistory.class);
        query.createAlias("userAccount", "userAccount");
        
        query.add(Restrictions.eq("customerMobileDevice.id", customerMobileDeviceId));
        if("ASC".equalsIgnoreCase(order)){
            if("username".equalsIgnoreCase(column)){
                query.addOrder(Order.asc("userAccount.userName")).addOrder(Order.asc("timestamp"));
            }else if("date".equalsIgnoreCase(column)){
                query.addOrder(Order.asc("timestamp")).addOrder(Order.asc("userAccount.userName"));
            }
        }else if("DESC".equalsIgnoreCase(order)){
            if("username".equalsIgnoreCase(column)){
                query.addOrder(Order.desc("userAccount.userName")).addOrder(Order.desc("timestamp"));
            }else if("date".equalsIgnoreCase(column)){
                query.addOrder(Order.desc("timestamp")).addOrder(Order.desc("userAccount.userName"));
            }
        }
        if(pageSize == null){pageSize = WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;}
        if (page == 1) {
            query.setFirstResult(0);
        } else {
            query.setFirstResult(pageSize * (page - 1));
        }
        query.setMaxResults(pageSize);
        List<MobileAppHistory> list = query.setCacheable(true).list();
        return list;
    }
    
    @Override
    public void logActivity(Customer customer, UserAccount userAccount, MobileApplicationType appType, CustomerMobileDevice device, Integer activityTypeInt, boolean isSuccessful, String result){
        MobileAppActivityType type = mobileAppActivityTypeService.findById(activityTypeInt);
        //TODO determine whether all behaviour should be logged:
        // The requirements indicate only registration (provisioning) attempts should be logged - but this may change.
        if(activityTypeInt != WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_PROVISION){
            return;
        }
        if(type == null){
            return;
        }
        MobileAppHistory history = new MobileAppHistory();
        history.setCustomerMobileDevice(device);
        history.setIsSuccessful(isSuccessful? (byte) 1 : 0);
        history.setMobileAppActivityType(type);
        history.setMobileApplicationType(appType);
        history.setResult(result);
        history.setTimestamp(new Date());
        history.setUserAccount(userAccount);
        entityDao.save(history);
    }
}
