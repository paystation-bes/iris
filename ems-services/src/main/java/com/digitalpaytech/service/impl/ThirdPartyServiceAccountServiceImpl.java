package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;
import com.digitalpaytech.service.ThirdPartyServiceAccountService;

@Service("thirdPartyServiceAccountService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ThirdPartyServiceAccountServiceImpl implements ThirdPartyServiceAccountService {

    @Autowired
    private EntityDao entityDao;

    
    @Override
    public List<ThirdPartyServiceAccount> findByCustomerId(Integer customerId) {
        String[] params = { "customerId" };
        Object[] values = { customerId };
        return entityDao.findByNamedQueryAndNamedParam("ThirdPartyServiceAccount.findByCustomerId", params, values, true);
    }


    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ThirdPartyServiceAccount getThirdPartyServiceAccountByCustomerAndType(Integer customerId, String type) {
        List<ThirdPartyServiceAccount> col = this.entityDao.findByNamedQueryAndNamedParam("ThirdPartyServiceAccount.getThirdPartyAccountByCustomerIdAndType",
                                                                                          new String[] { "customerId", "type" }, new Object[] { customerId, type });
        if (col != null && !col.isEmpty()) {
            return col.get(0);
        }
        return null;
    }  
}
