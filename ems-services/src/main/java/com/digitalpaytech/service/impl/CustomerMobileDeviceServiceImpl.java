package com.digitalpaytech.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileLicenseStatusType;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.service.CustomerMobileDeviceService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MobileAppHistoryService;
import com.digitalpaytech.service.MobileApplicationTypeService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Service("customerMobileDeviceService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerMobileDeviceServiceImpl implements CustomerMobileDeviceService {
    @Autowired
    EntityDao entityDao;
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    MobileLicenseService mobileLicenseService;
    
    @Autowired
    MobileApplicationTypeService mobileApplicationTypeService;
    
    @Autowired
    MobileAppHistoryService mobileAppHistoryService;
    
    @Autowired
    CustomerSubscriptionService customerSubscriptionService;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CustomerMobileDevice> findBlockedMobileDevicesByCustomer(Integer customerId, Integer pageNumber, Integer pageSize){
        if(pageNumber != null && pageSize != null){
            return entityDao.findPageResultByNamedQuery("CustomerMobileDevice.findBlockedByCustomer", new String[]{"customerId"}, new Object[]{customerId}, true, pageNumber, pageSize);
        }
        return entityDao.findByNamedQueryAndNamedParam("CustomerMobileDevice.findBlockedByCustomer", "customerId", customerId);
    }
    
    @Override
    public CustomerMobileDevice findCustomerMobileDeviceById(Integer customerMobileDeviceId){
        return (CustomerMobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMobileDevice.findCustomerMobileDeviceById", new String[]{"customerMobileDeviceId"}, new Object[]{customerMobileDeviceId}, true);
    }
    
    @Override
    public CustomerMobileDevice findBlockedMobileDevice(Integer customerId, Integer mobileDeviceId){
        return (CustomerMobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMobileDevice.findBlockedByCustomerAndDevice", new String[]{"customerId", "deviceId"}, new Object[]{customerId, mobileDeviceId}, true);
    }
    
    @Override
    public void unBlockDevice(Integer customerId, Integer customerMobileDeviceId){
        CustomerMobileDevice customerMobileDevice = findCustomerMobileDeviceById(customerMobileDeviceId);
      //TODO determine whether to log this activity
//        if(customerMobileDevice.getIsBlocked() == 1){
//            Customer customer = customerService.findCustomer(customerId);
//            
//            mobileAppHistoryService.logActivity(customer, null, null, customerMobileDevice, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_BLOCK, true, "Unblocked device");
//        }
        customerMobileDevice.setIsBlocked((byte) 0);
        entityDao.saveOrUpdate(customerMobileDevice);
    }
    
    @Override
    public void blockDevice(Integer customerId, Integer customerMobileDeviceId, int userAccountId){
        //1: make available any licenses assigned to this device (for any app)
        //2: expire any session tokens associated with those licenses
        //Set<Integer> appTypesToRefresh = new HashSet<Integer>();
        Set<MobileApplicationType> appsToRefresh = new HashSet<MobileApplicationType>();
        List<MobileLicense> licenses = mobileLicenseService.findProvisionedMobileLicensesByCustomerAndDevice(customerId, customerMobileDeviceId);
        MobileLicenseStatusType availableType = mobileLicenseService.findMobileLicenseStatusTypeById(WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE);
        for(MobileLicense license : licenses){
            license.setMobileLicenseStatusType(availableType);
            license.setCustomerMobileDevice(null);
            license.setSecretKey(null);
            appsToRefresh.add(license.getMobileApplicationType());
            for(MobileSessionToken token : license.getMobileSessionTokens() ){
                token.setExpiryDate(new Date());
                entityDao.saveOrUpdate(token);
            }
            entityDao.saveOrUpdate(license);
        }
        for(MobileApplicationType appType : appsToRefresh){
            int total = (int)mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appType.getId());
            int used = (int)mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appType.getId());
            Integer subscriptionTypeId = appType.getSubscriptionType().getId();
            customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customerId, subscriptionTypeId, total, used, userAccountId);
        }
        //3 block the device
        CustomerMobileDevice customerMobileDevice = findCustomerMobileDeviceById(customerMobileDeviceId);
        //TODO determine whether to save this in the app history
//        if(customerMobileDevice.getIsBlocked() == 0){
//            Customer customer = customerService.findCustomer(customerId);
//            mobileAppHistoryService.logActivity(customer, null, null, customerMobileDevice, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_BLOCK, true, "Blocked device");
//        }
        customerMobileDevice.setIsBlocked((byte)1);
        save(customerMobileDevice);
    }
    
    @Override
    public void releaseDevice(Integer customerId, Integer customerMobileDeviceId, int userAccountId){
        List<MobileLicense> keys = mobileLicenseService.findProvisionedMobileLicensesByCustomerAndDevice(customerId, customerMobileDeviceId);
        MobileLicenseStatusType availableType = mobileLicenseService.findMobileLicenseStatusTypeById(WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE);
        for(MobileLicense key : keys){
            //TODO determine whether to log this activity
            //mobileAppHistoryService.logActivity(key.getCustomer(), null, key.getMobileApplicationType(), key.getCustomerMobileDevice(), WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_RELEASE, true, "Released device");
            MobileApplicationType appType = key.getMobileApplicationType();
            int total = (int)mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appType.getId());
            int used = (int)mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appType.getId());
            Integer subscriptionTypeId = appType.getSubscriptionType().getId();
            customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customerId, subscriptionTypeId, total, used, userAccountId);
            key.setMobileLicenseStatusType(availableType);
            key.setCustomerMobileDevice(null);
            for(MobileSessionToken token : key.getMobileSessionTokens() ){
                token.setExpiryDate(new Date());
                entityDao.saveOrUpdate(token);
            }
            
            entityDao.saveOrUpdate(key);
        }
    }
    
    @Override
    public void releaseDevice(Integer customerId, Integer customerMobileDeviceId, Integer applicationTypeId, int userAccountId){
        MobileLicense license = mobileLicenseService.findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(customerId, applicationTypeId, customerMobileDeviceId);
        MobileLicenseStatusType availableType = mobileLicenseService.findMobileLicenseStatusTypeById(WebCoreConstants.MOBILE_LICENSE_STATUS_AVAILABLE);
        license.setMobileLicenseStatusType(availableType);
        MobileApplicationType appType = license.getMobileApplicationType();
        int total = (int)mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appType.getId());
        int used = (int)mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appType.getId());
        Integer subscriptionTypeId = appType.getSubscriptionType().getId();
        customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customerId, subscriptionTypeId, total, used, userAccountId);
        license.setCustomerMobileDevice(null);
        for(MobileSessionToken token : license.getMobileSessionTokens()){
            token.setExpiryDate(new Date());
            entityDao.saveOrUpdate(token);
        }
        //TODO determine whether to log this activity
        //MobileApplicationType applicationType = this.mobileApplicationTypeService.getMobileApplicationTypeById(applicationTypeId);
        //mobileAppHistoryService.logActivity(license.getCustomer(), null, applicationType, device, WebCoreConstants.MOBILE_APP_ACTIVITY_TYPE_RELEASE, true, "Released device");
        entityDao.saveOrUpdate(license);
    }
    
    @Override
    public CustomerMobileDevice findCustomerMobileDeviceByCustomerAndDeviceUid(Integer customerId, String uid){
        return (CustomerMobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMobileDevice.findCustomerMobileDeviceByCustomerAndUid", new String[]{"customerId", "uid"}, new Object[]{customerId, uid}, true);
    }
    
    /**
     * Returns information about the mobile device, as well as information about the last user to log in using this device and for the specified mobile application.  
     * MobileApplicationTypeId can be null, returning the user who most recently used the device for any application type.
     */
    @Override
    public MobileDeviceInfo findDetailsForCustomerMobileDevice(Integer customerMobileDeviceId, Integer mobileApplicationTypeId, Map<Integer, String> statusNames){
        //TODO under development EMS-4922
        
        MobileDeviceInfo info = new MobileDeviceInfo();
        info.setLoggedIn(false); //until found otherwise
        info.setStatus(statusNames.get(WebCoreConstants.MOBILE_DEVICE_STATUS_INACTIVE));
        info.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_INACTIVE);//until found otherwise
        
        CustomerMobileDevice device = null;
        if(mobileApplicationTypeId == null){
            device = (CustomerMobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMobileDevice.findCustomerMobileDeviceByIdJoinUserDetails", new String[]{"id"}, new Object[]{customerMobileDeviceId}, true);
        }else{
            device = (CustomerMobileDevice)entityDao.findUniqueByNamedQueryAndNamedParam("CustomerMobileDevice.findCustomerMobileDeviceByIdJoinUserDetailsByAppType", new String[]{"id", "appTypeId"}, new Object[]{customerMobileDeviceId, mobileApplicationTypeId}, true);
        }
        // device returns as null from previous queries if device has not been used
        if(device == null){
            device = findCustomerMobileDeviceById(customerMobileDeviceId);
            info.setApplicationCount(0);
        }else{
            List<MobileSessionToken> tokens = new LinkedList<MobileSessionToken>();
            Set<MobileLicense> keys = device.getMobileLicenses();
            info.setApplicationCount(keys.size());
            MobileApplicationType type;
            for(MobileLicense key : keys){
                tokens.addAll(key.getMobileSessionTokens());
                type = key.getMobileApplicationType();
                WebObjectId appRandomId = new WebObjectId(MobileApplicationType.class, type.getId());
                String randomId = appRandomId.toString();
                int status = WebCoreConstants.MOBILE_DEVICE_STATUS_PROVISIONED;
                String statusString = statusNames.get(WebCoreConstants.MOBILE_DEVICE_STATUS_PROVISIONED);
                info.setStatus(statusString);
                info.setStatusId(status);
                info.addAppInfo(type.getName(), randomId, status, statusString);
            }
            Collections.sort(tokens, new Comparator<MobileSessionToken>(){
                @Override
                public int compare(MobileSessionToken arg0, MobileSessionToken arg1) {
                    return arg1.getExpiryDate().compareTo(arg0.getExpiryDate());
                }
            });
            if(tokens.size() > 0){
                MobileSessionToken latestToken = tokens.get(0);
                UserAccount userAccount = latestToken.getUserAccount();
                try {
                    info.setUserName(URLDecoder.decode(userAccount.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
                } catch (UnsupportedEncodingException e) {
                    info.setUserName(userAccount.getUserName());
                }
                info.setFirstName(userAccount.getFirstName());
                info.setLastName(userAccount.getLastName());
                info.setUserRandomId(new WebObjectId(UserAccount.class, latestToken.getUserAccount().getId()).toString());
                info.setLatitude(latestToken.getLatitude());
                info.setLongitude(latestToken.getLongitude());
                if(latestToken.getExpiryDate().after(new Date())){
                    info.setLoggedIn(true);
                }
            }
        }
        if(device.getIsBlocked() == (byte) 1){
            info.setStatus(statusNames.get(WebCoreConstants.MOBILE_DEVICE_STATUS_BLOCKED));
            info.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_BLOCKED);
        }
        info.setName(device.getName());
        info.setDescription(device.getDescription());
        info.setUid(device.getMobileDevice().getUid());
        info.setRandomId(new WebObjectId(CustomerMobileDevice.class, customerMobileDeviceId).toString());
        return info;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CustomerMobileDevice> findCustomerMobileDeviceByCustomer(Integer customerId, Integer page, Integer pageSize) {
        return entityDao.findPageResultByNamedQuery("CustomerMobileDevice.findCustomerMobileDeviceByCustomer", new String[] { "customerId" },
                                                    new Object[] { customerId }, true, page, pageSize);
        
    }
    
    @Override
    public List<CustomerMobileDevice> findCustomerMobileDeviceByCustomerAndApplication(Integer customerId, Integer applicationId, Integer page, Integer pageSize){
        return entityDao.findPageResultByNamedQuery("CustomerMobileDevice.findCustomerMobileDeviceByCustomerAndApplication", new String[]{"customerId", "applicationId"}, new Object[]{customerId, applicationId}, true, page, pageSize);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(CustomerMobileDevice customerMobileDevice){
        entityDao.save(customerMobileDevice);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(CustomerMobileDevice customerMobileDevice){
        entityDao.delete(customerMobileDevice);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdate(CustomerMobileDevice customerMobileDevice){
        entityDao.saveOrUpdate(customerMobileDevice);
    }
}
