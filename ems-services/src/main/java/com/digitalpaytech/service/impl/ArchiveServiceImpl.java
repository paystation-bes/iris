package com.digitalpaytech.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.ArchiveService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.util.WebCoreUtil;

@Service("archiveService")
@Transactional(propagation = Propagation.REQUIRED)
public class ArchiveServiceImpl implements ArchiveService {
    
    private static final Logger LOG = Logger.getLogger(ArchiveServiceImpl.class);
    
    @Autowired
    private RawSensorDataService rawSensorDataService;
    
    @Override
    public void archiveRawSensorData(final RawSensorData rawSensorData) {
        this.rawSensorDataService.archiveRawSensorData(rawSensorData);
     
        
        if (LOG.isInfoEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(rawSensorData.toString()).append(" has been moved to RawSensorDataArchive table due to no PointOfSale serial number (")
                    .append(rawSensorData.getSerialNumber());
            bdr.append(") exists. ");
            LOG.info(bdr.toString());
        }
    }

    
    @Override
	public void handleRawSensorDataTransformationError(final Exception e, final EventData event, final RawSensorData sensor, final int threadId) {
    	if (WebCoreUtil.checkIfDuplicateException(e)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Duplicate sensor found: " + event.toString() + ". Deleting from database.");
            }
            this.rawSensorDataService.deleteRawSensorData(sensor);
        } else {
            LOG.error("Unable to process sensor data: " + event.toString(), e);
            
            if (sensor.getRetryCount() > 50) {
                LOG.error("Max retry time reached for sensor: " + event.toString() + ". Deleting from database.");
                this.rawSensorDataService.deleteRawSensorData(sensor);
            } else {
                sensor.setLastRetryTime(new Date());
                sensor.setRetryCount(sensor.getRetryCount() + 1);
                if (this.rawSensorDataService.rawSensorDataExist(sensor.getId())) {
                    // update retry time and count
                    this.rawSensorDataService.updateRawSensorData(sensor);
                } else {
                    this.archiveRawSensorData(sensor);
                }
            }
        }
	}


	public void setRawSensorDataService(final RawSensorDataService rawSensorDataService) {
        this.rawSensorDataService = rawSensorDataService;
    }
}
