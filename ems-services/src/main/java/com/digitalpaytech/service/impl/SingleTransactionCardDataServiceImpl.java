package com.digitalpaytech.service.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SingleTransactionCardData;
import com.digitalpaytech.service.SingleTransactionCardDataService;

@Service("singleTransactionCardDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class SingleTransactionCardDataServiceImpl implements SingleTransactionCardDataService {
    
    private final static Logger logger = Logger.getLogger(SingleTransactionCardDataServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public SingleTransactionCardData findSingleTransactionCardDataByPurchaseId(Long purchaseId) {
        return (SingleTransactionCardData) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("SingleTransactionCardData.findSingleTransactionCardDataByPurchaseId", "purchaseId", purchaseId, true);
    }
    
    @Override
    public void saveOrUpdate(SingleTransactionCardData singleTransactionCardData) {
        this.entityDao.saveOrUpdate(singleTransactionCardData);
    }
    
    @Override
    public void delete(SingleTransactionCardData singleTransactionCardData) {
        this.entityDao.delete(singleTransactionCardData);
    }
    
    @Override
    public void cleanupOldData(int maxDays) {
        Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -maxDays);
        
        List<SingleTransactionCardData> cardDataList = this.entityDao
                .findByNamedQueryAndNamedParam("SingleTransactionCardData.findSingleTransactionCardDataOlderThanPurchaseGmt", "limitDate", limitDate.getTime());
        if (cardDataList != null && !cardDataList.isEmpty()) {
            for (SingleTransactionCardData cardData : cardDataList) {
                delete(cardData);
            }
        }
    }
}
