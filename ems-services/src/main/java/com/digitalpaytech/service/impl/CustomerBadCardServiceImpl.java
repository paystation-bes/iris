package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.FlushMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.comparator.CustomerBadCardComparator;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.crypto.CryptoConstants;

@Service("customerBadCardService")
@Transactional(propagation = Propagation.SUPPORTS)
public class CustomerBadCardServiceImpl implements CustomerBadCardService {
    private static final Logger LOG = Logger.getLogger(CustomerBadCardServiceImpl.class);
    private static final String PARAM_NAME_CARD_NUMBER = "cardNumber";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
   
    private CustomerBadCardService self() {
        return this.serviceHelper.bean("customerBadCardService", CustomerBadCardService.class);
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }

    /**
     * Uses Hibernate Criteria api to retrieve CustomerBadCard records.
     * 
     * @param customerId
     *            Customer id.
     * @param cardNumber
     *            String it could be partial card number because the logic uses 'LIKE' sql keyworld.
     * @param customerCardTypeId
     *            CardType ID the input could be 0 if user didn't select a value.
     * @return Collection<CustomerBadCard> Return empty list if no result was found.
     */
    @Override
    public Collection<CustomerBadCard> getCustomerBadCards(final Integer customerId, final String cardNumber, final Integer customerCardTypeId) {
        Criteria crit = this.entityDao.createCriteria(CustomerBadCard.class);
        // Prepares CustomerCardType table information by creating Alias.
        crit = crit.createAlias("customerCardType", "cbcCustomerCardType");
        // Adds conditions.
        crit = crit.add(Restrictions.eq("cbcCustomerCardType.customer.id", customerId));
        
        final CustomerCardType cct = this.customerCardTypeService.getCardType(customerCardTypeId);
        if (!StringUtils.isBlank(cardNumber)) {
            if (cardNumber.length() != 4) {
                if (cct.getCardType().getId() == WebCoreConstants.CREDIT_CARD) {
                    try {
                        crit = crit.add(Restrictions.eq("cardNumberOrHash",
                            this.cryptoAlgorithmFactory.getSha1Hash(cardNumber, CryptoConstants.HASH_BAD_CREDIT_CARD)));
                    } catch (CryptoException ce) {
                        throw new RuntimeException("Could not create bad credit card's hash", ce);
                    }
                } else {
                    crit = crit.add(Restrictions.eq("cardNumberOrHash", cardNumber));
                }
            } else {
                crit = crit.add(Restrictions.eq("last4digitsOfCardNumber", Short.parseShort(StringUtils.stripStart(cardNumber, "0"))));
            }
        }
        
        final String custCardTypeId = "customerCardType.id";
        if (cct != null && cct.getParentCustomerCardType() != null) {
            // Checks if CustomerBadCard.customerCardType.id is CustomerCardType.parentCustomerCardType.id.
            crit = crit.add(Restrictions.eq(custCardTypeId, cct.getParentCustomerCardType().getId()));
        } else if (cct != null) {
            // Checks against non-parent CustomerCardType.id.
            crit = crit.add(Restrictions.eq(custCardTypeId, cct.getId()));
        }
        
        crit.addOrder(Order.asc("cbcCustomerCardType.name"));
        crit.addOrder(Order.asc("cardNumberOrHash"));
        
        @SuppressWarnings("unchecked")
        List<CustomerBadCard> list = crit.setCacheable(false).list();
        if (list == null) {
            list = new ArrayList<CustomerBadCard>();
        }
        
        if (LOG.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("CustomerBadCardServiceImpl, getCustomerBadCards , input customerId: ").append(customerId).append(", cardNumber: ")
                    .append(cardNumber).append(", CardType: ");
            if (customerCardTypeId != null && customerCardTypeId.intValue() > 0) {
                bdr.append(customerCardTypeId).append(", ");
            } else {
                bdr.append("<not selected>, ");
            }
            bdr.append("return list size is: ").append(list.size());
            LOG.debug(bdr.toString());
        }
        return list;
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, createBadCreditCard
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createCustomerBadCard(final CustomerBadCard newCustomerBadCard) {
        if (CardProcessingConstants.CARD_TYPE_CREDIT_CARD == newCustomerBadCard.getCustomerCardType().getCardType().getId()) {
            final String newExpiry = newCustomerBadCard.getCardExpiry().toString();
            try {
                newCustomerBadCard.setCardData(this.cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                    newCustomerBadCard.getCardNumberOrHash() + "=" + newExpiry.substring(0, 2) + newExpiry.substring(2)));
                newCustomerBadCard.setCardNumberOrHash(this.cryptoAlgorithmFactory.getSha1Hash(newCustomerBadCard.getCardNumberOrHash(),
                    CryptoConstants.HASH_BAD_CREDIT_CARD));
            } catch (CryptoException ce) {
                throw new RuntimeException("Could not encrypt data", ce);
            }
        }
        
        try {
            newCustomerBadCard.setCustomerCardType(this.customerCardTypeService.createPrimaryTypeIfAbsentFor(newCustomerBadCard.getCustomerCardType()
                    .getId()));
        } catch (Exception e) {
            // DO NOTHING.
        }
        
        this.entityDao.save(newCustomerBadCard);
    }
    
    @Override
    @Async
    public void createCustomerBadCard(final Map<String, CustomerBadCard> badCards) {
        for (CustomerBadCard newCustomerBadCard : badCards.values()) {
            try {
                newCustomerBadCard.setCustomerCardType(this.customerCardTypeService
                        .createPrimaryTypeIfAbsentFor(newCustomerBadCard.getCustomerCardType().getId()));
            } catch (Exception e) {
                // DO NOTHING.
                LOG.debug("Failed to execute [customerCardTypeService.createPrimaryTypeIfAbsentFor] ", e);
            }
            
            this.entityDao.save(newCustomerBadCard);
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCustomerBadCard(final CustomerBadCard customerBadCard) {
        this.entityDao.delete(customerBadCard);
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, updateBadCreditCard <<< MODIFIED
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCustomerBadCard(final CustomerBadCard customerBadCard) {
        try {
            customerBadCard.setCustomerCardType(this.customerCardTypeService.createPrimaryTypeIfAbsentFor(customerBadCard.getCustomerCardType()
                    .getId()));
        } catch (Exception e) {
            // DO NOTHING.
        }
        
        this.entityDao.saveOrUpdate(customerBadCard);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateLast4DigitsOfCardNumber(final Integer id, final Short last4DigitsOfCardNumber) {
        final CustomerBadCard badCard = this.entityDao.get(CustomerBadCard.class, id);
        if (badCard != null) {
            badCard.setLast4digitsOfCardNumber(last4DigitsOfCardNumber);
            this.entityDao.saveOrUpdate(badCard);
        }
        
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.CardServiceImpl, updateBadCreditCards
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateCustomerBadCards(final Collection<CustomerBadCard> customerBadCards) {
        final Iterator<CustomerBadCard> it = customerBadCards.iterator();
        CustomerBadCard badCreditCard;
        while (it.hasNext()) {
            badCreditCard = it.next();
            updateCustomerBadCard(badCreditCard);
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public CustomerBadCard findCustomerBadCardById(final Integer id, final boolean isEvict) {
        final CustomerBadCard customerBadCard = (CustomerBadCard) this.entityDao.get(CustomerBadCard.class, id);
        if (customerBadCard != null && isEvict) {
            this.entityDao.evict(customerBadCard);
        }
        return customerBadCard;
    }
    
    // Warning !!! For CSV import ONLY !
    @Override
    @Async
    public final Future<Integer> processBatchUpdateAsync(final String statusKey, final int customerId, final Collection<CustomerBadCard> badCardList,
        final String mode, final int userId) throws BatchProcessingException {
        return new AsyncResult<Integer>(self().processBatchUpdateWithStatus(statusKey, customerId, badCardList, mode, userId));
    }
    
    // Warning !!! For CSV import ONLY !
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final int processBatchUpdateWithStatus(final String statusKey, final int customerId, final Collection<CustomerBadCard> badCardList,
        final String mode, final int userId) throws BatchProcessingException {
        final int updated = 0;
        final BatchProcessStatus status = this.serviceHelper.getProcessStatus(statusKey);
        try {
            self().processBatchUpdate(customerId, badCardList, mode, userId, status);
            if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail",
                    "label.cardManagement.bannedCard"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.bulkupdate.successMsg", "label.cardManagement.bannedCard"));
            }
        } catch (Throwable t) {
            status.fail((String) null, t);
            LOG.error("Failed to process CustomerBadCard batch update", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return updated;
    }
    
    // Warning !!! For CSV import ONLY !
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final int processBatchUpdate(final int customerId, final Collection<CustomerBadCard> badCardList, final String mode, final int userId,
        final BatchProcessStatus status) throws BatchProcessingException {
        int updated = 0;
        
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        if ("replace".equalsIgnoreCase(mode)) {
            try {
                this.deleteByCustomerId(customerId, (String) null);
            } catch (BatchProcessingException bpe) {
                LOG.error("Error while trying to delete all CustomerBadCards", bpe);
            }
        }
        
        CustomerBadCard first = null;
        if (badCardList.size() > 0) {
            first = badCardList.iterator().next();
            
            final ScrollableResults scroll = this.entityDao.getNamedQuery("CustomerBadCard.findCustomerBadCardByCustomerIdOrderByTypeAndNumber")
                    .setParameter("customerId", customerId).setParameter("cardNumberOrHash", first.getCardNumberOrHash())
                    .scroll(ScrollMode.FORWARD_ONLY);
            
            final Date today = new Date();
            final CustomerBadCardComparator cmprtr = new CustomerBadCardComparator();
            CustomerBadCard current = null;
            int compareResult = -1;
            for (CustomerBadCard card : badCardList) {
                compareResult = (current == null) ? -1 : cmprtr.compare(current, card);
                while (compareResult < 0) {
                    current = (!scroll.next()) ? null : (CustomerBadCard) scroll.get(0);
                    compareResult = (current == null) ? 1 : cmprtr.compare(current, card);
                }
                
                final CustomerBadCard existingCard = (compareResult != 0) ? null : current;
                if (existingCard != null) {
                    if (existingCard.getLast4digitsOfCardNumber() == null) {
                        try {
                            final String decrypted = this.cryptoService.decryptData(existingCard.getCardData());
                            if (decrypted != null) {
                                final String[] parts = decrypted.split("=");
                                if ((parts.length > 0) && (parts[0].length() > 4)) {
                                    existingCard.setLast4digitsOfCardNumber(new Short(parts[0].substring(parts[0].length() - 4)));
                                }
                            }
                        } catch (CryptoException ce) {
                            // DO NOTHING.
                        }
                    }
                    
                    if (!existingCard.getLast4digitsOfCardNumber().equals(card.getLast4digitsOfCardNumber())) {
                        throw new BatchProcessingException("Invalid card record: XXXXXXXXXXXX" + card.getLast4digitsOfCardNumber());
                    }
                    
                    existingCard.setLastModifiedByUserId(userId);
                    existingCard.setLastModifiedGmt(today);
                    
                    existingCard.setCardData(card.getCardData());
                    existingCard.setCardExpiry(card.getCardExpiry());
                    existingCard.setComment(card.getComment());
                    existingCard.setSource(card.getSource());
                    
                    this.entityDao.update(existingCard);
                } else {
                    card.setAddedGmt(today);
                    card.setLastModifiedGmt(today);
                    card.setLastModifiedByUserId(userId);
                    
                    if (CardProcessingConstants.CARD_TYPE_CREDIT_CARD != card.getCustomerCardType().getCardType().getId()) {
                        createCustomerBadCard(card);
                    } else {
                        if (card.getCardData() == null) {
                            createCustomerBadCard(card);
                        } else if (card.getCardData().startsWith("existing")) {
                            throw new BatchProcessingException("Could not locate existing card record: XXXXXXXXXXXX"
                                                               + card.getLast4digitsOfCardNumber());
                        } else {
                            card.setCardData(card.getCardData());
                            this.entityDao.save(card);
                        }
                    }
                }
                
                ++updated;
                
                if ((updated % WebCoreConstants.BATCH_SIZE) == 0) {
                    this.entityDao.getCurrentSession().flush();
                    this.entityDao.getCurrentSession().clear();
                }
            }
        }
        
        this.entityDao.getCurrentSession().flush();
        this.entityDao.getCurrentSession().clear();
        
        return updated;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public final void deleteCustomerBadCardList(final Collection<CustomerBadCard> customerBadCards) {
        for (CustomerBadCard card : customerBadCards) {
            this.entityDao.delete(card);
        }
    }
    
    @SuppressWarnings("unchecked")
    public final Collection<CustomerBadCard> findCustomerBadCardByCustomerId(final Integer customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerBadCard.findCustomerBadCardByCustomerId", "customerId", customerId, false);
    }
    
    @Override
    public final int getTotalBadCreditCard(final String cardData) {
        return this.entityDao.findTotalResultByNamedQuery("CustomerBadCard.getNumBadCreditCardByCardData", new String[] { "cardData" },
            new Object[] { cardData });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final Collection<CustomerBadCard> getBadCreditCardByCardData(final String cardData, final int howManyRows, final boolean isEvict) {
        final List<CustomerBadCard> lists = this.entityDao.findByNamedQuery("CustomerBadCard.getBadCreditCardByCardData", new Object[] { cardData },
            0, howManyRows);
        
        if (isEvict) {
            for (CustomerBadCard card : lists) {
                this.entityDao.evict(card);
            }
        }
        
        return lists;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final Collection<CustomerBadCard> findNoExpiredBadCreditCard(final Integer customerId, final Integer customerCardTypeId) {
        final String[] params = { "customerId", "cardExpiry", "customerCardTypeId" };
        final Object[] values = new Object[] { customerId, (short) DateUtil.createCurrentYYMM(), customerCardTypeId };
        return this.entityDao.findByNamedQueryAndNamedParam("CustomerBadCard.findNoExpiredBadCreditCard", params, values);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final int deleteByCustomerId(final Integer customerId, final String statusKey) throws BatchProcessingException {
        int deleted = 0;
        
        final BatchProcessStatus status = (statusKey == null) ? null : this.serviceHelper.getProcessStatus(statusKey);
        try {
            int retried = 0;
            
            final Map<String, Object> context = new HashMap<String, Object>(2);
            
            final Date now = new Date();
            int currDeleted = 0;
            do {
                try {
                    currDeleted = self().deleteByCustomerIdPaginated(customerId, now, context);
                    deleted += currDeleted;
                } catch (Exception e) {
                    ++retried;
                    if (retried < 1000) {
                        currDeleted = 1;
                        LOG.warn("Encountered error while trying to delete CustomerBadCards. Will retry rightaway", e);
                    } else {
                        currDeleted = -1;
                        LOG.error(
                            "Gave up because there are too many retries to delete CustomerBadCards. "
                                    + "Some of the records have been deleted but not all of them. (CustomerId"
                                    + customerId + ")", e);
                        throw new RuntimeException("Too many retries to delete CustomerBadCards", e);
                    }
                }
            } while (currDeleted > 0);
            
            if (status == null) {
                // DO NOTHING.
            } else if (status.hasWarning()) {
                status.success(this.messageHelper.getMessageWithKeyParams("error.common.bulkupdate.success.with.fail",
                    "label.cardManagement.bannedCard"));
            } else {
                status.success(this.messageHelper.getMessageWithKeyParams("alert.form.deleteAllMsg", "label.cardManagement.bannedCard"));
            }
        } catch (Throwable t) {
            if (status != null) {
                status.fail((String) null, t);
            }
            
            LOG.error("Failed to process CustomerBadCard batch deleteAll", t);
            if (t instanceof BatchProcessingException) {
                throw new BatchProcessingException(t.getMessage(), t);
            } else {
                throw new RuntimeException(t);
            }
        }
        
        return deleted;
    }
    
    // These logic are extracted to different method 
    // just in case in the future there are more than 500 MB of records to be deleted (which will break cluster-db)
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final int deleteByCustomerIdPaginated(final Integer customerId, final Date deleteTimeStamp, final Map<String, Object> context)
        throws BatchProcessingException {
        int deleted = 0;
        
        // Ignore 2nd level cache to reduce memory footprint.
        this.entityDao.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        
        @SuppressWarnings("unchecked")
        final List<Integer> deleteIds = this.entityDao.getNamedQuery("CustomerBadCard.findLowerUpdatedIdsByCustomerId")
                .setParameter("customerId", customerId).setParameter("lastModifiedGmt", deleteTimeStamp).setFirstResult(0)
                .setMaxResults(WebCoreConstants.BATCH_SIZE).list();
        if (deleteIds.size() > 0) {
            deleted = this.entityDao.getNamedQuery("CustomerBadCard.deleteByIds").setParameterList("customerBadCardIds", deleteIds).executeUpdate();
        }
        
        return deleted;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Collection<CustomerBadCard> findCustomerBadCards(final CustomerBadCardSearchCriteria criteria) {
        Criteria query = this.entityDao.createCriteria(CustomerBadCard.class)
                .setCacheable(criteria.getMaxUpdatedTime() == null);
        
        query.add(Subqueries.propertyIn(
            "customerCardType.id",
            DetachedCriteria.forClass(CustomerCardType.class).setProjection(Projections.property("id"))
                    .add(Restrictions.eq("customer.id", criteria.getCustomerId())).add(Restrictions.isNull("archiveDate"))
                    .add(Restrictions.eq("isDeleted", false))));
        
        if ((criteria.getCustomerCardType() != null) && (criteria.getCustomerCardType() > 0)) {
            query = query.add(Restrictions.eq("customerCardType.id", criteria.getCustomerCardType()));
        }
        
        if (!StringUtils.isBlank(criteria.getCardNumber())) {
            final StringBuilder bdr = new StringBuilder();
            // WebCoreConstants.SENSOR_UNIT_HUMIDITY = "%"
            bdr.append(WebCoreConstants.SENSOR_UNIT_HUMIDITY).append(criteria.getCardNumber()).append(WebCoreConstants.SENSOR_UNIT_HUMIDITY);
            
            if (criteria.getCardNumber().length() != 4) {
                query = query.add(Restrictions.like("cardNumberOrHash", bdr.toString()));
            } else {
                final Short num;
                
                /* This treats that case where last4digits of Card is 0000(four zeros) */
                if (StandardConstants.STRING_FOUR_ZEROS.equals(criteria.getCardNumber())) {
                    num = Short.valueOf(StandardConstants.STRING_ZERO);
                } else {
                    num = Short.valueOf(StringUtils.stripStart(criteria.getCardNumber(), "0"));
                }
                
                query = query.add(Restrictions.disjunction().add(Restrictions.like("cardNumberOrHash", bdr.toString()))
                        .add(Restrictions.eq("last4digitsOfCardNumber", num)));
            }
        }
        
        if (criteria.getMaxUpdatedTime() != null) {
            query.add(Restrictions.le("lastModifiedGmt", criteria.getMaxUpdatedTime()));
        }
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        boolean needCCT = false;
        if ((criteria.getOrderColumn() != null) && (criteria.getOrderColumn().length > 0)) {
            for (int i = -1; ++i < criteria.getOrderColumn().length;) {
                final String orderColumnName = criteria.getOrderColumn()[i];
                if (orderColumnName.startsWith("cct.")) {
                    needCCT = true;
                }
                
                if (criteria.isOrderDesc()) {
                    query.addOrder(Order.desc(orderColumnName));
                } else {
                    query.addOrder(Order.asc(orderColumnName));
                }
            }
            
            if (needCCT) {
                query.createAlias("customerCardType", "cct").setFetchMode("cct", FetchMode.JOIN);
            }
        }
        
        return query.list();
    }
    
    @Override
    public final int countByCustomerCardType(final Collection<Integer> customerCardTypeIds) {
        return ((Number) this.entityDao.getNamedQuery("CustomerBadCard.countByCustomerCardTypes")
                .setParameterList("customerCardTypeIds", customerCardTypeIds).uniqueResult()).intValue();
    }
}
