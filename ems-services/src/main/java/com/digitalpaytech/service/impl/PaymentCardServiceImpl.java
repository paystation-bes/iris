package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.util.CardProcessingConstants;

@Service("paymentCardService")
@SuppressWarnings("unchecked")
@Transactional(propagation = Propagation.SUPPORTS)
public class PaymentCardServiceImpl implements PaymentCardService {
    
    private static final String PROCESSOR_TRANSACTION_ID = "processorTransactionId";
    
    private static final String PURCHASE_ID = "purchaseId";
    
    private static final String PROCESSOR_TRANSACTION_TYPE_ID = "processorTransactionTypeId";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public PaymentCard findPaymentCardByPurchaseIdAndProcessorTransactionId(final Long purchaseId, final Long processorTransactionId) {
        return (PaymentCard) this.entityDao.findUniqueByNamedQueryAndNamedParam("PaymentCard.findPaymentCardByPurchaseIdAndProcessorTransactionId",
                                                                                new String[] { PURCHASE_ID, PROCESSOR_TRANSACTION_ID },
                                                                                new Object[] { purchaseId, processorTransactionId }, false);
    }
    
    @Override
    public PaymentCard findByPurchaseId(final Long purchaseId) {
        final List<PaymentCard> paymentCards =
                (List<PaymentCard>) this.entityDao.findByNamedQueryAndNamedParam("PaymentCard.findByPurchaseId", PURCHASE_ID, purchaseId, true);
        return findNonRefundPaymentCard(paymentCards);
    }
    
    /*
     * "FROM PaymentCard pc WHERE pc.purchase.id = :purchaseId AND pc.cardType.id = :cardTypeId"lpaytech.domain.ProcessorTransaction, java.lang.Integer)
     */
    @Override
    public PaymentCard findByPurchaseIdAndCardType(final ProcessorTransaction processorTransaction, final Integer cardTypeId) {
        return findByPurchaseIdAndCardType(processorTransaction, cardTypeId, false);
    }
    
    @Override
    public PaymentCard findByPurchaseIdAndCardType(final ProcessorTransaction processorTransaction, final Integer cardTypeId,
        final boolean cacheable) {
        if (processorTransaction.getPurchase() == null) {
            return null;
        }
        final String[] params = { PURCHASE_ID, "cardTypeId" };
        final Object[] values = new Object[] { processorTransaction.getPurchase().getId(), cardTypeId };
        final List<PaymentCard> paymentCards =
                (List<PaymentCard>) this.entityDao.findByNamedQueryAndNamedParam("PaymentCard.findByPurchaseIdAndCardType", params, values, false);
        return findNonRefundPaymentCard(paymentCards);
    }
    
    @Override
    public PaymentCard findLatestByCustomerCardId(final Integer customerCardId) {
        final List<PaymentCard> paymentCardList =
                this.entityDao.findByNamedQueryAndNamedParam("PaymentCard.findLatestByCustomerCardId", "customerCardId", customerCardId, true);
        
        if (paymentCardList != null && !paymentCardList.isEmpty()) {
            return paymentCardList.get(0);
        }
        
        return null;
    }
    
    @Override
    public void updateProcessorTransactionTypeToSettled(final Long purchaseId, final Long processorTransactionId) {
        updateProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, purchaseId, processorTransactionId);
    }
    
    @Override
    public void updateProcessorTransactionToBatchedSettled(final ProcessorTransaction processorTransaction, final Integer cardTypeId) {
        final PaymentCard pc = findByPurchaseIdAndCardType(processorTransaction, cardTypeId);
        if (pc != null) {
            final String[] params = { PROCESSOR_TRANSACTION_ID, PROCESSOR_TRANSACTION_TYPE_ID, "id" };
            final Object[] values =
                    new Object[] { processorTransaction.getId(), processorTransaction.getProcessorTransactionType().getId(), pc.getId() };
            this.entityDao.updateByNamedQuery("PaymentCard.updateProcessorTransactionAndType", params, values);
        }
    }
    
    @Override
    public void savePaymentCard(final PaymentCard paymentCard) {
        this.entityDao.save(paymentCard);
    }
    
    @Override
    public void saveRefundPaymentCard(final ProcessorTransaction refund) {
        if (refund != null) {
            final Purchase purchase = this.purchaseService.findPurchaseById(refund.getPurchase().getId(), false);
            final CreditCardType cardType = this.creditCardTypeService.getCreditCardTypeByName(refund.getCardType());
            final PaymentCard refundPaymentCard =
                    new PaymentCard(refund, this.cardTypeService.getCardTypesMap().get(CardProcessingConstants.CARD_TYPE_CREDIT_CARD), null, purchase,
                            cardType, refund.getAmount(), refund.getLast4digitsOfCardNumber(), refund.getProcessingDate(),
                            refund.isIsUploadedFromBoss(), refund.isIsRfid(), refund.isIsApproved());
            refundPaymentCard.setMerchantAccount(refund.getMerchantAccount());
            refundPaymentCard.setProcessorTransactionType(refund.getProcessorTransactionType());
            savePaymentCard(refundPaymentCard);
        }
    }
    
    private PaymentCard findNonRefundPaymentCard(final List<PaymentCard> paymentCards) {
        if (paymentCards != null) {
            for (PaymentCard pc : paymentCards) {
                final ProcessorTransactionType ptt = pc.getProcessorTransactionType();
                if (ptt == null || ptt.getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND
                                   && ptt.getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SINGLE_TRANSACTION_REFUND
                                   && ptt.getId() != CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_REFUND) {
                    return pc;
                }
            }
        }
        
        return null;
    }
    
    private void updateProcessorTransactionType(final Integer processorTransactionTypeId, final Long purchaseId, final Long processorTransactionId) {
        final PaymentCard pc = findPaymentCardByPurchaseIdAndProcessorTransactionId(purchaseId, processorTransactionId);
        if (pc != null) {
            final String[] params = { PROCESSOR_TRANSACTION_TYPE_ID, PURCHASE_ID, PROCESSOR_TRANSACTION_ID };
            final Object[] values = { processorTransactionTypeId, purchaseId, processorTransactionId };
            this.entityDao.updateByNamedQuery("PaymentCard.updateProcessorTransactionType", params, values);
        }
    }
    
}
