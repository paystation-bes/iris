package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ReportRepeatTypeService;


@Service("reportRepeatTypeService")
public class ReportRepeatTypeServiceImpl implements ReportRepeatTypeService {

	@Autowired
	private EntityService entityService;

	   public void setEntityService(EntityService entityService) {
	        this.entityService = entityService;
	    }


	@Override
	public List<ReportRepeatType> loadAll() {
		return entityService.loadAll(ReportRepeatType.class);
	}

}
