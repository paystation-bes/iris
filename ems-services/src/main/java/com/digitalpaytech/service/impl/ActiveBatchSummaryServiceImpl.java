package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.service.ActiveBatchSummaryService;

@Service("activeBatchSummaryService")
public class ActiveBatchSummaryServiceImpl implements ActiveBatchSummaryService {
  
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public ActiveBatchSummary findByBatchNoAndMerchantId(short batchNo, int merchantId) {
        return (ActiveBatchSummary) this.entityDao.findUniqueByNamedQueryAndNamedParam("ActiveBatchSummary.findByBatchNoAndMerchantId",
                                                                                       new String[] { "batchNo", "merchantId", }, new Object[] {
                                                                                            batchNo, merchantId, }, true);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
