package com.digitalpaytech.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.service.ETLNotProcessedService;

@Service("etlNotProcessedService")
public class ETLNotProcessedServiceImpl implements ETLNotProcessedService {
    
    @Autowired
    private EntityDao entityDao;
    
    private static final String SQL_GET_PURCHASE = "SELECT COUNT(DISTINCT PurchaseId) FROM ETLNotProcessed WHERE record_insert_time >= UTC_TIMESTAMP() - INTERVAL 1 MINUTE";
    
    @Override
    @Transactional
    public int getPurchase() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_PURCHASE);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    private static final String SQL_GET_PERMIT = "SELECT COUNT(DISTINCT PermitId) FROM ETLNotProcessed WHERE record_insert_time >= UTC_TIMESTAMP() - INTERVAL 1 MINUTE";
    
    @Override
    @Transactional
    public int getPermit() {
        SQLQuery q = entityDao.createSQLQuery(SQL_GET_PERMIT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
}
