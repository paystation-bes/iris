package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.support.RelativeDateTime;

@Service("reportQueueService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class ReportQueueServiceImpl implements ReportQueueService {
    private static final String QN_SEL_SCHEDULED_REPORTS = "ReportQueue.findScheduledReports";
    
    private static final String SQL_GET_LAUNCHED_REPORT_COUNT = "SELECT COUNT(*) FROM ReportQueue rq WHERE rq.ReportStatusTypeId = 3";
    private static final String SQL_GET_SCHEDULED_REPORT_COUNT = "SELECT COUNT(*) FROM ReportQueue rq WHERE rq.ReportStatusTypeId = 2";
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveReportQueue(final ReportQueue reportQueue) {
        this.entityDao.save(reportQueue);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateReportQueue(final ReportQueue reportQueue) {
        this.entityDao.update(reportQueue);
    }
    
    @Override
    public List<ReportQueue> findAll() {
        return this.entityDao.loadAll(ReportQueue.class);
    }
    
    @Override
    public List<ReportQueue> findQueuedReports() {
        return this.entityDao.findByNamedQuery(QN_SEL_SCHEDULED_REPORTS, false);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ReportQueue findQueuedReportWithLock(final String clusterName) {
        final ReportQueue reportQueue = (ReportQueue) this.entityDao.findUniqueByNamedQueryWithLock(QN_SEL_SCHEDULED_REPORTS, "qr");
        if (reportQueue != null) {
            reportQueue.setLockId(clusterName);
            this.entityDao.update(reportQueue);
        }
        return reportQueue;
    }
    
    @Override
    public List<ReportQueue> findQueuedReportsByUserAccountId(final int userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportQueue.findReportQueueByUserAccountId", HibernateConstants.USER_ACCOUNT_ID,
                                                            userAccountId, false);
    }
    
    @Override
    public List<ReportQueue> findQueuedReportsByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportQueue.findReportQueueByCustomerId", HibernateConstants.CUSTOMER_ID, customerId,
                                                            false);
    }
    
    @Override
    public List<ReportQueue> findReportQueueByCustomerIdAndUserAccountId(final int userAccountId, final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportQueue.findReportQueueByCustomerIdAndUserAccoundId", new String[] {
            HibernateConstants.USER_ACCOUNT_ID, HibernateConstants.CUSTOMER_ID, }, new Integer[] { userAccountId, customerId, }, false);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public String fixStalledReports(final String clusterName, final Date idleTime) {
        final String[] params = { "lockId", "idleTime" };
        final Object[] values = { clusterName, idleTime };
        final List<ReportQueue> reportList = this.entityDao.findByNamedQueryAndNamedParam("ReportQueue.findStalledReports", params, values);
        String stalledReportList = null;
        while (reportList.size() > 0) {
            final ReportQueue report = reportList.remove(0);
            report.setLockId(null);
            final ReportStatusType reportStatusType = new ReportStatusType();
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_SCHEDULED);
            report.setReportStatusType(reportStatusType);
            this.entityDao.update(report);
            if (stalledReportList == null) {
                stalledReportList = String.valueOf(report.getReportDefinition().getId());
            } else {
                stalledReportList += "," + report.getReportDefinition().getId();
            }
        }
        return stalledReportList;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean cancelReport(final int reportQueueId) {
        
        final ReportQueue reportQueue = (ReportQueue) this.entityDao.get(ReportQueue.class, reportQueueId, LockMode.PESSIMISTIC_WRITE);
        if (reportQueue == null) {
            return false;
        } else if (ReportingConstants.REPORT_STATUS_LAUNCHED == reportQueue.getReportStatusType().getId()) {
            return false;
        }
        this.entityDao.delete(reportQueue);
        
        final Date now = DateUtil.nowGMT();
        
        final ReportHistory reportHistory = new ReportHistory();
        reportHistory.setReportDefinition(reportQueue.getReportDefinition());
        final ReportStatusType reportStatusType = new ReportStatusType();
        reportStatusType.setId(ReportingConstants.REPORT_STATUS_CANCELLED);
        reportHistory.setReportStatusType(reportStatusType);
        reportHistory.setCustomer(reportQueue.getCustomer());
        reportHistory.setUserAccount(reportQueue.getUserAccount());
        reportHistory.setReportRepeatType(reportQueue.getReportRepeatType());
        reportHistory.setScheduledBeginGmt(reportQueue.getScheduledBeginGmt());
        reportHistory.setQueuedGmt(reportQueue.getQueuedGmt());
        reportHistory.setExecutionBeginGmt(now);
        reportHistory.setExecutionEndGmt(now);
        
        this.entityDao.save(reportHistory);
        return true;
    }
    
    @Override
    public ReportQueue getReportQueue(final int id) {
        return (ReportQueue) this.entityDao.get(ReportQueue.class, id);
        
    }
    
    @Override
    public int getLaunchedReportCount() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_LAUNCHED_REPORT_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public int getScheduledReportCount() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_SCHEDULED_REPORT_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public ReportQueue findRecentlyQueuedReport(final int reportDefinitionId) {
        return (ReportQueue) this.entityDao.getNamedQuery("ReportQueue.findRecentlyQueuedReportByReportDefinitionId")
                .setParameter("reportDefinitionId", reportDefinitionId).setMaxResults(1).setFirstResult(0).uniqueResult();
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:magicnumber" })
    // I suppress magic number here becuase it easier to read when those magic numbers are just index of array.
    public List<QueueInfo> findQueuedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        final Criteria query = this.entityDao.createCriteria(ReportQueue.class);
        query.createAlias("reportDefinition", "rd");
        
        query.setProjection(Projections.projectionList().add(Projections.property(HibernateConstants.ID)).add(Projections.property("rd.title"))
                .add(Projections.property("rd.reportStatusType.id")).add(Projections.property(HibernateConstants.EXECUTION_BEGIN_GMT)));
        
        query.setResultTransformer(new ResultTransformer() {
            private static final long serialVersionUID = -6392102159415075621L;
            
            @Override
            public Object transformTuple(final Object[] tuple, final String[] aliases) {
                final QueueInfo result = new QueueInfo();
                result.setRandomId(keyMapping.getRandomString(ReportQueue.class, tuple[0]));
                result.setTitle((String) tuple[1]);
                result.setStatus((Integer) tuple[2]);
                result.setTime(new RelativeDateTime((Date) tuple[3], timeZone));
                
                return result;
            }
            
            @SuppressWarnings("rawtypes")
            @Override
            public List transformList(final List collection) {
                return collection;
            }
        });
        
        query.add(Restrictions.eq("rd.isSystemAdmin", criteria.isSysadmin()));
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("rd.customer.id", criteria.getCustomerId()));
        }
        
        if (criteria.getUserAccountId() != null) {
            query.add(Restrictions.eq("rd.userAccount.id", criteria.getUserAccountId()));
        }
        
        query.addOrder(Order.desc("queuedGmt"));
        
        return query.list();
    }
}
