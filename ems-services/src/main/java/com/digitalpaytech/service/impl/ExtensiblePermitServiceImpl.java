package com.digitalpaytech.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("extensiblePermitService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ExtensiblePermitServiceImpl implements ExtensiblePermitService {
    
    public static final String SQL_GET_EXTENSIBLEPERMIT_BY_COUNT = "SELECT COUNT(*) FROM ExtensiblePermit";
    
    private static final Logger LOGGER = Logger.getLogger(ExtensiblePermitServiceImpl.class);
    
    private static final String SMSINFO_START = "SELECT alert.Id AS SmsAlertId, alert.PointOfSaleId AS PointOfSaleId, p.Id AS OriginalPermitId, "
                                                + "m.Id AS MobileNumberId, m.Number AS MobileNumber, alert.PermitExpireGMT AS ExpiryDate, "
                                                + "alert.CustomerId AS CustomerId, alert.PermitLocationId AS LocationId, "
                                                + "eep.PermitBeginGMT AS OriginalPurchasedDate, alert.TicketNumber AS TicketNumber, "
                                                + "alert.PermitBeginGMT AS PurchasedDate, cp.PropertyValue AS TimeZone, eep.Last4Digit AS Last4Digit, "
                                                + "p.LicencePlateId AS LicencePlateId, alert.LicencePlate AS PlateNumber, alert.SpaceNumber AS StallNumber, "
                                                + "alert.IsAutoExtended AS IsAutoExtended " + "FROM SmsAlert alert "
                                                + "INNER JOIN MobileNumber m ON alert.MobileNumberId = m.Id "
                                                + "INNER JOIN ExtensiblePermit eep ON alert.MobileNumberId = eep.MobileNumberId "
                                                + "INNER JOIN Permit p ON eep.OriginalPermitId = p.Id "
                                                + "INNER JOIN Customer cus ON cus.Id = alert.CustomerId AND cus.IsMigrated = 1 "
                                                + "INNER JOIN CustomerProperty cp ON alert.CustomerId = cp.CustomerId AND cp.CustomerPropertyTypeId = 1 "
                                                + "INNER JOIN CustomerSubscription cs ON alert.CustomerId = cs.CustomerId AND cs.SubscriptionTypeId = "
                                                + WebCoreConstants.SUBSCRIPTION_TYPE_EXTEND_BY_PHONE + StandardConstants.STRING_EMPTY_SPACE;
    
    private static final String SMSINFO_EXPIRING =
            "INNER JOIN CustomerProperty cp1 ON alert.CustomerId = cp1.CustomerId AND cp1.CustomerPropertyTypeId = 6 "
                                                   + "WHERE alert.IsAlerted = 0 AND alert.IsLocked = 0 AND alert.NumberOfRetries = 0 "
                                                   + "AND UTC_TIMESTAMP() < alert.PermitExpireGMT AND cs.IsEnabled = 1 "
                                                   + "AND DATE_ADD(UTC_TIMESTAMP(), INTERVAL cp1.PropertyValue MINUTE) >= alert.PermitExpireGMT "
                                                   + "ORDER BY alert.PermitExpireGMT ";
    private static final String SMSINFO_EXTENDED =
            "WHERE m.Number = :mobileNumber AND alert.NumberOfRetries < 2 AND alert.IsAlerted = 1 " + "AND alert.IsLocked = 0";
    
    private static final String EXTENSIBLEPERMIT_FIND_BY_MOBILE_NUMBER = "ExtensiblePermit.findByMobileNumber";
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public ExtensiblePermit findByMobileNumber(final String mobileNumber) {
        final List<ExtensiblePermit> list =
                this.entityDao.findByNamedQueryAndNamedParam(EXTENSIBLEPERMIT_FIND_BY_MOBILE_NUMBER, HibernateConstants.NUMBER, mobileNumber);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    @Override
    @SuppressWarnings("PMD.AvoidUsingShortType")
    public final void updateLatestExpiryDate(final Date permitExpireGmt, final String cardData, final short last4digit, final String mobileNumber) {
        
        final ExtensiblePermit permit = findByMobileNumber(mobileNumber);
        if (permit != null) {
            permit.setPermitExpireGmt(permitExpireGmt);
            permit.setCardData(cardData);
            permit.setLast4digit(last4digit);
            this.entityDao.update(permit);
        }
    }
    
    @Override
    public void updateExtensiblePermit(final ExtensiblePermit extensiblePermit) {
        this.entityDao.update(extensiblePermit);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final int deleteExpiredSmsAlert() {
        
        final Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MINUTE, -StandardConstants.CONSTANT_10);
        
        final List<SmsAlert> smsAlerts =
                this.entityDao.findByNamedQueryAndNamedParam("SmsAlert.findExpiredSmsAlert", HibernateConstants.CURRENT_TIME, c.getTime());
        int count = 0;
        for (SmsAlert alert : smsAlerts) {
            this.entityDao.delete(alert);
            count++;
        }
        return count;
    }
    
    @Override
    public final int deleteExpiredEMSExtensiblePermit() {
        
        final Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MINUTE, -StandardConstants.CONSTANT_10);
        
        final List<ExtensiblePermit> extensiblePermits = this.entityDao.findByNamedQueryAndNamedParam("ExtensiblePermit.findExpiredExtensiblePermit",
                                                                                                      HibernateConstants.CURRENT_TIME, c.getTime());
        int count = 0;
        for (ExtensiblePermit permit : extensiblePermits) {
            this.entityDao.delete(permit);
            count++;
        }
        return count;
    }
    
    @Override
    public final Collection<SmsAlertInfo> loadSmsAlertFromDB(final int countOfRows) {
        final StringBuilder qry = new StringBuilder(SMSINFO_START);
        qry.append(SMSINFO_EXPIRING);
        final SQLQuery query = this.entityDao.createSQLQuery(qry.toString());
        query.addScalar(HibernateConstants.COLUMN_SMS_ALERT_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_POINTOFSALE_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_ORIGINAL_PERMIT_ID, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MOBILE_NUMBER_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_MOBILE_NUMBER, new StringType());
        query.addScalar(HibernateConstants.COLUMN_EXPIRY_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_CUSTOMER_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_LOCATION_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_ORIGINAL_PURCHASED_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_TICKET_NUMBER, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_PURCHASED_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_TIME_ZONE, new StringType());
        query.addScalar(HibernateConstants.COLUMN_LAST_4_DIGIT, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_LICENCE_PLATE_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_PLATE_NUMBER, new StringType());
        query.addScalar(HibernateConstants.COLUMN_STALL_NUMBER, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_IS_AUTO_EXTENDED, new BooleanType());
        query.setMaxResults(countOfRows);
        query.setResultTransformer(Transformers.aliasToBean(SmsAlertInfo.class));
        
        return query.list();
    }
    
    @Override
    public final void deleteExtensiblePermitAndSMSAlert(final String mobileNumber) {
        final List<ExtensiblePermit> permits =
                this.entityDao.findByNamedQueryAndNamedParam(EXTENSIBLEPERMIT_FIND_BY_MOBILE_NUMBER, HibernateConstants.NUMBER, mobileNumber, false);
        for (ExtensiblePermit permit : permits) {
            this.entityDao.delete(permit);
        }
        
        final List<SmsAlert> alerts =
                this.entityDao.findByNamedQueryAndNamedParam("SmsAlert.findByMobileNumber", HibernateConstants.MOBILE_NUMBER, mobileNumber, false);
        for (SmsAlert alert : alerts) {
            this.entityDao.delete(alert);
        }
    }
    
    @Override
    public SmsAlertInfo getSmsAlertByParkerReply(final String mobileNumber) {
        final StringBuilder qry = new StringBuilder(SMSINFO_START);
        qry.append(SMSINFO_EXTENDED);
        
        final SQLQuery query = this.entityDao.createSQLQuery(qry.toString());
        query.setParameter(HibernateConstants.MOBILE_NUMBER, mobileNumber);
        query.addScalar(HibernateConstants.COLUMN_SMS_ALERT_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_POINTOFSALE_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_ORIGINAL_PERMIT_ID, new LongType());
        query.addScalar(HibernateConstants.COLUMN_MOBILE_NUMBER_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_MOBILE_NUMBER, new StringType());
        query.addScalar(HibernateConstants.COLUMN_EXPIRY_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_CUSTOMER_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_LOCATION_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_ORIGINAL_PURCHASED_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_TICKET_NUMBER, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_PURCHASED_DATE, new TimestampType());
        query.addScalar(HibernateConstants.COLUMN_TIME_ZONE, new StringType());
        query.addScalar(HibernateConstants.COLUMN_LAST_4_DIGIT, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_LICENCE_PLATE_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_PLATE_NUMBER, new StringType());
        query.addScalar(HibernateConstants.COLUMN_STALL_NUMBER, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_IS_AUTO_EXTENDED, new BooleanType());
        query.setResultTransformer(Transformers.aliasToBean(SmsAlertInfo.class));
        final List<SmsAlertInfo> result = query.list();
        
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }
    
    @Override
    public final void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo,
        final String callbackId, final boolean isFreeParking) {
        if (LOGGER.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("pointOfSaleId: ").append(smsAlertInfo.getPointOfSaleId()).append(", update alert: mobileNumber: ")
                    .append(smsAlertInfo.getMobileNumber()).append(", isAlerted: ").append(isAlerted).append(", callbackId: ").append(callbackId)
                    .append(", numOfRetry: ").append(numOfRetry).append(", isLocked: ").append(isLocked).append(", permitBeginGmt: ")
                    .append(smsAlertInfo.getPurchasedDate()).append(", permitExpireGmt: ").append(smsAlertInfo.getExpiryDate());
            LOGGER.debug(bdr.toString());
        }
        
        if (!isFreeParking) {
            
            final SmsAlert alert = (SmsAlert) this.entityDao.get(SmsAlert.class, smsAlertInfo.getSmsAlertId());
            
            if (alert == null) {
                LOGGER.error("Null SmsAlert for MobileNumber: " + smsAlertInfo.getMobileNumber() + "\n" + smsAlertInfo.toString());
            } else {
                this.entityDao.evict(alert);
                
                alert.setIsAlerted(isAlerted);
                alert.setNumberOfRetries(numOfRetry);
                alert.setIsLocked(isLocked);
                alert.setPermitBeginGmt(smsAlertInfo.getPurchasedDate());
                alert.setPermitExpireGmt(smsAlertInfo.getExpiryDate());
                alert.setPointOfSale((PointOfSale) this.entityDao.get(PointOfSale.class, smsAlertInfo.getPointOfSaleId()));
                if (callbackId != null) {
                    alert.setCallbackId(callbackId);
                }
                
                this.entityDao.update(alert);
            }
        } else {
            LOGGER.debug("Free parking, SMSAlert should be deleted at this point");
        }
    }
    
    @Override
    public final void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo,
        final String callbackId) {
        updateSmsAlert(isAlerted, numOfRetry, isLocked, smsAlertInfo, callbackId, false);
    }
    
    @Override
    public void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo) {
        updateSmsAlert(isAlerted, numOfRetry, isLocked, smsAlertInfo, (String) null, false);
    }
    
    @Override
    public final void updateSmsAlertAndLatestExpiryDate(final boolean isAlerted, final int numOfRetry, final boolean isLocked,
        final Date permitBeginGmt, final Date permitExpireGmt, final boolean isFreeParkingExtended, final SmsAlertInfo smsAlertInfo) {
        
        final ExtensiblePermit extensiblePermit = this.findByMobileNumber(smsAlertInfo.getMobileNumber());
        
        if (extensiblePermit == null) {
            LOGGER.error("Null ExtensiblePermit for MobileNumber: " + smsAlertInfo.getMobileNumber());
        } else {
            this.entityDao.evict(extensiblePermit);
            extensiblePermit.setPermitExpireGmt(permitExpireGmt);
            this.entityDao.update(extensiblePermit);
            
            final SmsAlert smsAlert = (SmsAlert) this.entityDao.get(SmsAlert.class, smsAlertInfo.getSmsAlertId());
            if (smsAlert == null) {
                LOGGER.error("Null SmsAlert for MobileNumber: " + smsAlertInfo.getMobileNumber() + "\n" + smsAlertInfo.toString());
            } else {
                
                this.entityDao.evict(smsAlert);
                smsAlert.setIsAlerted(isAlerted);
                smsAlert.setNumberOfRetries(numOfRetry);
                smsAlert.setIsLocked(isLocked);
                smsAlert.setPermitBeginGmt(smsAlertInfo.getPurchasedDate());
                smsAlert.setPermitExpireGmt(smsAlertInfo.getExpiryDate());
                smsAlert.setIsAutoExtended(isFreeParkingExtended);
                smsAlert.setPointOfSale((PointOfSale) this.entityDao.get(PointOfSale.class, smsAlertInfo.getPointOfSaleId()));
                
                this.entityDao.update(smsAlert);
            }
        }
        
    }
    
    @Override
    public final int getExtensiblePermitByCount() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_EXTENSIBLEPERMIT_BY_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
}
