package com.digitalpaytech.service.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.service.RestSessionTokenService;

@Service("restSessionTokenService")
@Transactional(propagation = Propagation.REQUIRED)
public class RestSessionTokenServiceImpl implements RestSessionTokenService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public List<RestSessionToken> findRestSessionTokenBySessionToken(String sessionToken) {
        
        return entityDao.findByNamedQueryAndNamedParam("RestSessionToken.findRestSessionTokenBySessionToken", "sessionToken", sessionToken);
    }
    
    @Override
    public RestSessionToken findRestSessionTokenByRestAccountId(int restAccountId) {
        return (RestSessionToken) entityDao.findUniqueByNamedQueryAndNamedParam("RestSessionToken.findRestSessionTokenByRestAccountId", "restAccountId",
                                                                                restAccountId);
    }
    
    @Override
    public int findCountBySessionToken(String sessionToken) {
        SQLQuery q = entityDao.createSQLQuery("SELECT Count(*) AS TokenCount FROM RestSessionToken WHERE SessionToken = :sessionToken");
        q.addScalar("TokenCount", new IntegerType());
        q.setParameter("sessionToken", sessionToken);
        List<Object> result = q.list();
        if (result != null && result.size() > 0) {
            return ((Number) result.get(0)).intValue();
        }
        return 0;
    }

    @Override
    public void saveRestSessionToken(RestSessionToken restSessionToken) {
        entityDao.saveOrUpdate(restSessionToken);
    }

    @Override
    public void updateRestSessionToken(RestSessionToken restSessionToken) {
        entityDao.update(restSessionToken);
    }
    
}
