package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.service.PaymentSmartCardService;

@Service("paymentSmartCardService")
@Transactional(propagation=Propagation.SUPPORTS)
public class PaymentSmartCardServiceImpl implements PaymentSmartCardService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public PaymentSmartCard findByPurchaseId(Purchase purchase) {
        List<PaymentSmartCard> list = entityDao.findByNamedQueryAndNamedParam("PaymentSmartCard.findByPurchaseId", "purchaseId", purchase.getId(), true);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
