package com.digitalpaytech.service.impl;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.service.RestAccountService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("restAccountService")
@Transactional(propagation = Propagation.REQUIRED)
public class RestAccountServiceImpl implements RestAccountService {
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public List<RestAccount> findRestAccounts(int customerId) {
        List<RestAccount> accounts = entityDao.findByNamedQueryAndNamedParam("RestAccount.findRestAccountByCustomerId", "customerId", customerId, true);
        return accounts;
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public RestAccount findRestAccountByAccountName(String accountName) {
        return (RestAccount) entityDao.findUniqueByNamedQueryAndNamedParam("RestAccount.findRestAccountByAccountName", "accountName", accountName, true);
    }
    
    @Override
    public List<RestAccount> findNonUniqueRestAccountByAccountName(String accountName) {
        return entityDao.findByNamedQueryAndNamedParam("RestAccount.findRestAccountByAccountName", "accountName", accountName, true);
    }    
    
    @Override
    public RestAccount findRestAccountBySecretKey(String secretKey) {
        return (RestAccount) entityDao.findUniqueByNamedQueryAndNamedParam("RestAccount.findRestAccountBySecretKey", "secretKey", secretKey, true);
    }

    @Override
    public RestAccount findRestAccountBySessionToken(String sessionToken) {
         RestSessionToken rst = (RestSessionToken) entityDao.findUniqueByNamedQueryAndNamedParam("RestAccount.findRestAccountBySessionToken", "sessionToken", sessionToken, false);
         if (rst == null) {
             return null;
         } else {
             return rst.getRestAccount();
         }
         
    }
    
    @Override
    public RestAccount findRestAccountById(int restAccountId) {
        return (RestAccount) entityDao.get(RestAccount.class, restAccountId);
    }

    @Override
    public void saveOrUpdate(RestAccount restAccount) {
        entityDao.saveOrUpdate(restAccount);
    }

    @Override
    public RestAccount findRestAccountByPointOfSaleId(int pointOfSaleId) {
        return (RestAccount) entityDao.findUniqueByNamedQueryAndNamedParam("RestAccount.findRestAccountByPointOfSaleId", "pointOfSaleId", pointOfSaleId, true);
    }

    @Override
    public void delete(RestAccount restAccount) {
        entityDao.delete(restAccount);
    }

}
