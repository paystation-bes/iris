package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.PosEventHistoryService;

@Service("posEventHistoryService")
@Transactional(propagation = Propagation.REQUIRED)
public class PosEventHistoryServiceImpl implements PosEventHistoryService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final void savePosEventHistory(final PosEventHistory posEventHistory) {
        
        this.entityDao.save(posEventHistory);
    }
    
    @Override
    public final PosEventHistory findActivePosEventHistoryById(final Long posEventHistoryId) {
        
        return (PosEventHistory) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosEventHistory.findActivePosEventHistoryById",
                                                                                    HibernateConstants.POS_EVENT_HISTORY_ID, posEventHistoryId);
    }
    
    @Override
    public final PosEventHistory findById(final Long id) {
        return this.entityDao.get(PosEventHistory.class, id);
    }
    
    @Override
    public final PosEventHistory findByIdWithUserName(final Long id) {
        return (PosEventHistory) this.entityDao.findUniqueByNamedQueryAndNamedParam("PosEventHistory.findPosEventHistoryByIdWithUserName",
                                                                                    "posEventHistoryId", id);
                                                                                    
    }
    
}
