package com.digitalpaytech.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SettingsFileContent;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingPayStationInfo;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("paystationSettingService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PaystationSettingServiceImpl implements PaystationSettingService {
    private static Logger log = Logger.getLogger(PaystationSettingServiceImpl.class);
    private static int NAME_FIELD_SIZE = 20;
              
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    @Autowired
    private PosServiceStateService posServiceStateService;
    @Autowired
    private MailerService mailerService;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setPosServiceStateService(PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public PaystationSetting findPaystationSetting(int id) {
        return (PaystationSetting) entityDao.get(PaystationSetting.class, id);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<PaystationSetting> findPaystationSettingsByCustomerId(int customerId) {
        return entityDao.findByNamedQueryAndNamedParam("PaystationSetting.findPaystationSettingsByCustomerId", "customerId", customerId);
    }
    
    @Override
    //TODO In EMS6 PaystationSettings for same customer with same name not unique (Might need to return a list)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PaystationSetting findByCustomerIdAndLotName(Customer customer, String name) {
        final String nameSanitized = getNameSanitized(name);
        String[] params = { "customerId", "name" };
        Object[] values = { customer.getId(), nameSanitized };
        PaystationSetting result = (PaystationSetting) entityDao
                .findUniqueByNamedQueryAndNamedParam("PaystationSetting.findPaystationSettingByCustomerIdAndLotName", params, values, true);
        if (result == null) {
            result = new PaystationSetting();
            result.setCustomer(customer);
            result.setName(nameSanitized);
            result.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            result.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            entityDao.save(result);
            
            StringBuilder bdr = new StringBuilder();
            bdr.append("PaystationSetting name: ").append(name)
                    .append(" is uploaded but couldn't find associated record in PaystationSetting table. A new record is created.");
            mailerService.sendAdminWarnAlert("New PaystationSetting record is created", bdr.toString(), null);
            
        }
        return result;
        
    }
    
    //------------------------------------------------------------------------------------------------------------------
    // For UploadServlet & DownloadServlet.
    //------------------------------------------------------------------------------------------------------------------
    @Override
    public SettingsFile findSettingsFile(Integer id) {
        return (SettingsFile) entityDao.get(SettingsFile.class, id);
    }
    
    @Override
    public SettingsFileContent findSettingsFileContentBySettingsFileId(Integer settingsFileId) {
        return (SettingsFileContent) entityDao.get(SettingsFileContent.class, settingsFileId);
    }
    
    @Override
    public void processSettingsFileUpload(Customer customer, String fileFullPath, SettingsFile newSetting, List<String> serialNumbers)
            throws InvalidDataException {
        // unzip file, extract xml and view
        try {
            // Check if setting with same identifier exists
            SettingsFile old_setting = findSettingsFileByCustomerIdAndUniqueIdentifier(newSetting.getCustomer().getId(), newSetting.getUniqueIdentifier());
            SettingsFile psSetting;
            if (old_setting == null) {
                psSetting = createSettingsFileg(newSetting, fileFullPath);
                findByCustomerIdAndLotName(customer, newSetting.getName());
            } else {
                psSetting = updateSettingsFile(old_setting, newSetting, fileFullPath);
                updatePaystationSetting(customer, old_setting.getName(), newSetting.getName());
            }
            
            if (log.isInfoEnabled()) {
                log.info("++++ serial numbers in settingsFile file: " + serialNumbers);
            }
        } catch (Exception e) {
            throw new InvalidDataException(e);
        }
        
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    private SettingsFile createSettingsFileg(SettingsFile settingsFile, String fileFullPath) throws ApplicationException {
        entityDao.save(settingsFile);
        saveOrUpdateSettingsFileIdContent(settingsFile, fileFullPath, false);
        return settingsFile;
    }
    
    private void saveOrUpdateSettingsFileIdContent(SettingsFile settingsFile, String fileFullPath, boolean isUpdate) throws ApplicationException {
        try {
            SettingsFileContent fileContent = null;
            
            // read record from database if its an update operation
            if (isUpdate || fileFullPath == null) {
                fileContent = (SettingsFileContent) entityDao.get(SettingsFileContent.class, settingsFile.getId());
                if (fileContent == null) {
                    fileContent = new SettingsFileContent();
                    fileContent.setSettingsFile(settingsFile);
                }
            } else {
                // new record
                fileContent = new SettingsFileContent();
            }
            
            // read the file from database
            // update new file content to database
            fileContent.setContent(WebCoreUtil.convertFileToBinary(new File(fileFullPath)));
            fileContent.setSettingsFile(settingsFile);
            entityDao.saveOrUpdate(fileContent);
        } catch (IOException e) {
            throw new ApplicationException("Error writing to Setting Zip File.", e);
        }
    }
    
    /**
     * @deprecated This is handled by FMS.
     * 
     * @param settingsFile
     * @param serialNumbers
     * @param customerId
     */
    private void notifyPaystations(SettingsFile settingsFile, List<String> serialNumbers, Integer customerId) {
        for (String serialNumber : serialNumbers) {
            PointOfSale pos = pointOfSaleService.findPointOfSaleBySerialNumberAndCustomerId(serialNumber, customerId);
            if (pos != null && pos.getPosStatus().isIsActivated()) {
                Date nowGmt = DateUtil.getCurrentGmtDate();
                PosServiceState pss = pos.getPosServiceState();
                pss.setIsNewPaystationSetting(true);
                pss.setSettingsFile(settingsFile);
                pss.setLastPaystationSettingUploadGmt(nowGmt);
                pss.setLastModifiedGmt(nowGmt);
                this.posServiceStateService.updatePosServiceState(pss);
                if (log.isInfoEnabled()) {
                    log.info("+++ set new settingsFile flag for ps: " + serialNumber + " succeeded. +++");
                }
            } else {
                StringBuilder bdr = new StringBuilder();
                if (pos == null) {
                    bdr.append("+++ set new settingsFile flag for pos: ").append(serialNumber)
                            .append(" fails because it does not exist in database for customer: ");
                    bdr.append(settingsFile.getCustomer().getId());
                } else {
                    bdr.append("+++ PS: ").append(serialNumber).append(" is deactivated.");
                }
                log.error(bdr.toString());
            }
        }
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    private SettingsFile updateSettingsFile(SettingsFile oldSetting, SettingsFile newSetting, String fileFullPath) throws ApplicationException {
        if (log.isInfoEnabled()) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("Updating existing settingsFile: ").append(oldSetting.getId()).append(" for customer id:").append(oldSetting.getCustomer().getId());
            log.info(bdr.toString());
        }
        oldSetting.setActivationGmt(newSetting.getActivationGmt());
        oldSetting.setFileName(newSetting.getFileName());
        oldSetting.setName(newSetting.getName());
        oldSetting.setUniqueIdentifier(newSetting.getUniqueIdentifier());
        oldSetting.setUploadGmt(newSetting.getUploadGmt());
        
        entityDao.update(oldSetting);
        saveOrUpdateSettingsFileIdContent(oldSetting, fileFullPath, true);
        return oldSetting;
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    private PaystationSetting updatePaystationSetting(Customer customer, String oldName, String newName) {
        PaystationSetting psSetting = findByCustomerIdAndLotName(customer, oldName);
        psSetting.setName(getNameSanitized(newName));
        entityDao.update(psSetting);
        return psSetting;
    }
    
    //TODO: Consider remove getNameSanitized once we decide to implement https://t2systems.atlassian.net/browse/LDCA-1747
    private String getNameSanitized(final String name) {              
        return StringUtils.substring(name, 0, NAME_FIELD_SIZE);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<SettingsFile> findSettingsFileByCustomerId(Integer customerId) {
        return entityDao.findByNamedQueryAndNamedParam("SettingsFile.findSettingsFileByCustomerId", "customerId", customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<SettingsFile> findSettingsFileByCustomerIdOrderByUploadGmt(Integer customerId) {
        return entityDao.findByNamedQueryAndNamedParam("SettingsFile.findSettingsFileByCustomerIdOrderByUploadGmt", "customerId", customerId);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public SettingsFile findSettingsFileByCustomerIdAndName(Integer customerId, String name) {
        Query q = entityDao.getNamedQuery("SettingsFile.findSettingsFileByCustomerIdAndName");
        q.setParameter("customerId", customerId);
        q.setParameter("name", name);
        q.setMaxResults(1);
        List<SettingsFile> list = q.list();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public SettingsFile findSettingsFileByCustomerIdAndUniqueIdentifier(Integer customerId, Integer uniqueIdentifier) {
        List<SettingsFile> settingsFileList = entityDao.findByNamedQueryAndNamedParam("SettingsFile.findSettingsFileByCustomerIdAndUniqueIdenifier",
                                                                                      new String[] { "customerId", "uniqueIdentifier" }, new Object[] {
                                                                                              customerId, uniqueIdentifier }, true);
        
        if (settingsFileList != null && !settingsFileList.isEmpty()) {
            return settingsFileList.get(0);
        }
        return null;
    }
    
    @Override
    public boolean isPaystationSettingWaitingForUpdate(Integer settingFileId) {
        List<PosServiceState> posServiceStateList = entityDao
                .findByNamedQueryAndNamedParam("PosServiceState.findPosServiceStateWaitingForPaystationSettingUpdate", "settingFileId", settingFileId);
        if (posServiceStateList == null || posServiceStateList.isEmpty())
            return false;
        else
            return true;
    }
    
    @Override
    public void deleteSettingsFileContent(SettingsFile settingsFile) {
        if (settingsFile.getSettingsFileContent() != null) {
            this.entityDao.delete(settingsFile.getSettingsFileContent());
        }
    }
    
    @Override
    public List<CustomerAdminPayStationSettingInfo> findCustomerAdminPayStationSettingInfoListByCustomerId(Integer customerId, String timezone,
                                                                                                           RandomKeyMapping keyMapping) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT IFNULL(pos.SettingsFileId, sf.Id) AS SettingsFileId, sf.Name AS Name, sf.UploadGMT AS UploadGmt, COUNT(pos.Id) AS PayStationCount, ");
        query.append("false AS IsUpdateInProgress, false AS IsNotExist, IF(sfc.SettingsFileId IS NULL, true, false) AS IsFileDeleted ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId AND pss.IsNewPaystationSetting = 0 ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("RIGHT JOIN SettingsFile sf On pos.SettingsFileId = sf.Id ");
        query.append("LEFT JOIN SettingsFileContent sfc ON sf.Id = sfc.SettingsFileId ");
        query.append("WHERE sf.CustomerId = :customerId AND ((pos.Id IS NULL AND sfc.SettingsFileId IS NOT NULL) OR (sf.name = pss.PaystationSettingName)) ");
        query.append("GROUP BY sf.Id ");
        query.append("UNION ");
        query.append("SELECT pss.NextSettingsFileId AS SettingsFileId, sf.Name AS Name, sf.UploadGMT AS UploadGmt, COUNT(pos.Id) AS PayStationCount, ");
        query.append("true As IsUpdateInProgress, false As IsNotExist, IF(sfc.SettingsFileId IS NULL, true, false) AS IsFileDeleted ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId ");
        query.append("AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("LEFT JOIN SettingsFile sf ON pss.NextSettingsFileId = sf.Id ");
        query.append("LEFT JOIN SettingsFileContent sfc on sf.Id = sfc.SettingsFileId ");
        query.append("WHERE pos.CustomerId = :customerId AND pss.IsNewPaystationSetting = 1 ");
        query.append("GROUP BY sf.Id ");
        query.append("UNION ");
        query.append("SELECT NULL AS SettingsFileId, '_No Settings' AS Name, NULL AS UploadGmt, COUNT(pos.Id) AS PayStationCount, ");
        query.append("false As IsUpdateInProgress, true As IsNotExist, true AS IsFileDeleted ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId ");
        query.append("AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("LEFT JOIN SettingsFile sf ON pos.SettingsFileId = sf.Id ");
        query.append("WHERE pos.CustomerId = :customerId AND pss.IsNewPaystationSetting = 0 AND (pos.SettingsFileId IS NULL OR sf.Name != pss.PaystationSettingName) ");
        query.append("Order by Name");
        
        SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SettingsFileId", new IntegerType());
        q.addScalar("Name", new StringType());
        q.addScalar("UploadGmt", new TimestampType());
        q.addScalar("PayStationCount", new IntegerType());
        q.addScalar("IsUpdateInProgress", new BooleanType());
        q.addScalar("IsNotExist", new BooleanType());
        q.addScalar("IsFileDeleted", new BooleanType());
        q.setParameter("customerId", customerId);
        q.setResultTransformer(Transformers.aliasToBean(CustomerAdminPayStationSettingInfo.class));
        
        List<CustomerAdminPayStationSettingInfo> resultList = q.list();
        
        CustomerAdminPayStationSettingInfo previousItem = null;
        for (int i = 0; i < resultList.size(); i++) {
            CustomerAdminPayStationSettingInfo paystationSetting = resultList.get(i);
            if (paystationSetting.getIsNotExist()) {
                paystationSetting.setName(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                paystationSetting.setRandomId(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                if (previousItem != null) {
                    previousItem.setSettingsFileId(null);
                }
            } else {
                // Data migrated from EMS6 and doesn't have a next paystationSetting name during upgrade
                if (paystationSetting.getName() == null) {
                    resultList.remove(i);
                    i--;
                    continue;
                }
                if (previousItem != null && previousItem.getSettingsFileId() != null && paystationSetting.getSettingsFileId() != null
                    && paystationSetting.getSettingsFileId().intValue() == previousItem.getSettingsFileId().intValue()) {
                    previousItem.setPayStationCount(paystationSetting.getPayStationCount() + previousItem.getPayStationCount());
                    if (previousItem.getIsUpdateInProgress() != paystationSetting.getIsUpdateInProgress()) {
                        previousItem.setIsUpdateInProgress(true);
                    }
                    resultList.remove(i);
                    i--;
                    continue;
                }
                SettingsFile settingsFile = new SettingsFile();
                settingsFile.setId(paystationSetting.getSettingsFileId());
                paystationSetting.setRandomId(keyMapping.getRandomString(settingsFile, WebCoreConstants.ID_LOOK_UP_NAME));
                paystationSetting.setUploadDate(DateUtil.getRelativeTimeString(paystationSetting.getUploadGmt(), timezone));
                if (previousItem != null) {
                    previousItem.setSettingsFileId(null);
                }
                previousItem = paystationSetting;
            }
        }
        
        return resultList;
    }
    
    @Override
    public CustomerAdminPayStationSettingInfo findCustomerAdminPayStationSettingInfoBySettingFileId(Integer settingFileId, String timezone,
                                                                                                    RandomKeyMapping keyMapping) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT IFNULL(pos.SettingsFileId, sf.Id) AS SettingsFileId, sf.Name AS SettingsFileName, sf.UploadGMT AS UploadGmt, ");
        query.append("false AS IsUpdateInProgress, false AS IsNotExist, ");
        query.append("pos.Name AS Name, pos.Id AS PointOfSaleId, pos.SerialNumber AS SerialNumber, l.Name LocationName, ");
        query.append("pss.LastPaystationSettingUploadGMT AS DownloadGmt, ps.PaystationTypeId AS PayStationType, pos.Latitude AS Latitude, ");
        query.append("pos.Longitude AS Longitude ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId AND pss.IsNewPaystationSetting = 0 ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("INNER JOIN SettingsFile sf On pos.SettingsFileId = sf.Id ");
        query.append("LEFT JOIN SettingsFileContent sfc ON sf.Id = sfc.SettingsFileId ");
        query.append("LEFT JOIN Location l On pos.LocationId = l.Id LEFT JOIN Paystation ps ON pos.PaystationId = ps.Id ");
        query.append("WHERE sf.Id = :settingFileId AND ((pos.Id IS NULL AND sfc.SettingsFileId IS NOT NULL) OR (sf.name = pss.PaystationSettingName)) ");
        query.append("UNION ");
        query.append("SELECT pss.NextSettingsFileId AS SettingsFileId, sf.Name AS SettingsFileName, sf.UploadGMT AS UploadGmt, ");
        query.append("true As IsUpdateInProgress, false As IsNotExist, ");
        query.append("pos.Name AS Name, pos.Id AS PointOfSaleId, pos.SerialNumber AS SerialNumber, l.Name LocationName, ");
        query.append("NULL AS DownloadGmt, ps.PaystationTypeId AS PayStationType, pos.Latitude AS Latitude, ");
        query.append("pos.Longitude AS Longitude ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId ");
        query.append("AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("LEFT JOIN SettingsFile sf ON pss.NextSettingsFileId = sf.Id ");
        query.append("LEFT JOIN SettingsFileContent sfc on sf.Id = sfc.SettingsFileId ");
        query.append("LEFT JOIN Location l On pos.LocationId = l.Id LEFT JOIN Paystation ps ON pos.PaystationId = ps.Id ");
        query.append("WHERE sf.Id = :settingFileId AND pss.IsNewPaystationSetting = 1 ");

        SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SettingsFileId", new IntegerType());
        q.addScalar("SettingsFileName", new StringType());
        q.addScalar("UploadGmt", new TimestampType());
        q.addScalar("IsUpdateInProgress", new BooleanType());
        q.addScalar("IsNotExist", new BooleanType());
        
        q.addScalar("Name", new StringType());
        q.addScalar("PointOfSaleId", new IntegerType());
        q.addScalar("SerialNumber", new StringType());
        q.addScalar("LocationName", new StringType());
        q.addScalar("DownloadGmt", new TimestampType());
        q.addScalar("PayStationType", new IntegerType());
        q.addScalar("Latitude", new BigDecimalType());
        q.addScalar("Longitude", new BigDecimalType());
        
        q.setParameter("settingFileId", settingFileId);
        q.setResultTransformer(Transformers.aliasToBean(CustomerAdminPayStationSettingPayStationInfo.class));
        
        List<CustomerAdminPayStationSettingPayStationInfo> resultList = q.list();
        if (resultList == null || resultList.isEmpty()) {
            return null;
        } else {
            CustomerAdminPayStationSettingInfo paystationSetting = new CustomerAdminPayStationSettingInfo();
            paystationSetting.setName(resultList.get(0).getSettingsFileName());
            paystationSetting.setIsNotExist(false);
            SettingsFile settingsFile = new SettingsFile();
            settingsFile.setId(resultList.get(0).getSettingsFileId());
            paystationSetting.setRandomId(keyMapping.getRandomString(settingsFile, WebCoreConstants.ID_LOOK_UP_NAME));
            paystationSetting.setSettingsFileId(null);
            paystationSetting.setUploadDate(DateUtil.getRelativeTimeString(resultList.get(0).getUploadGmt(), timezone));
            
            boolean isUpdateInProgress = false;
            if (resultList.size() == 1 && resultList.get(0).getPointOfSaleId() == null) {
                paystationSetting.setPayStationList(null);
                paystationSetting.setPayStationCount(0);
            } else {
                for (CustomerAdminPayStationSettingPayStationInfo paystationInfo : resultList) {
                    if (paystationInfo.getIsUpdateInProgress()) {
                        isUpdateInProgress = true;
                    }
                    PointOfSale pos = new PointOfSale();
                    pos.setId(paystationInfo.getPointOfSaleId());
                    paystationInfo.setRandomId(keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME));
                    paystationInfo.setPointOfSaleId(null);
                    paystationInfo.setDownloadDate(DateUtil.getRelativeTimeString(paystationInfo.getDownloadGmt(), timezone));
                }
                paystationSetting.setPayStationCount(resultList.size());
                paystationSetting.setPayStationList(resultList);
            }
            
            paystationSetting.setIsUpdateInProgress(isUpdateInProgress);
            
            return paystationSetting;
        }
    }
    
    @Override
    public CustomerAdminPayStationSettingInfo findNoSettingCustomerAdminPayStationSettingInfoByCustomerId(Integer customerId, String timezone,
                                                                                                          RandomKeyMapping keyMapping) {
        StringBuilder query = new StringBuilder();
        
        query.append("SELECT NULL AS SettingsFileId, 'No Settings' AS SettingsFileName, NULL AS UploadGmt, ");
        query.append("false As IsUpdateInProgress, true As IsNotExist, ");
        query.append("pos.Name AS Name, pos.Id AS PointOfSaleId, pos.SerialNumber AS SerialNumber, l.Name LocationName, ");
        query.append("pss.LastPaystationSettingUploadGMT AS DownloadGmt, ps.PaystationTypeId AS PayStationType, pos.Latitude AS Latitude, ");
        query.append("pos.Longitude AS Longitude ");
        query.append("FROM PointOfSale pos INNER JOIN POSServiceState pss on pos.Id = pss.PointOfSaleId ");
        query.append("INNER JOIN POSStatus poss ON pos.Id = poss.PointOfSaleId ");
        query.append("AND poss.IsDecommissioned = 0 ");
        query.append("AND poss.IsDeleted = 0 ");
        query.append("AND poss.IsVisible = 1 ");
        query.append("LEFT JOIN SettingsFile sf ON pos.SettingsFileId = sf.Id ");
        query.append("LEFT JOIN Location l ON pos.LocationId = l.Id LEFT JOIN Paystation ps ON pos.PaystationId = ps.Id ");
        query.append("WHERE pos.CustomerId = :customerId AND pss.IsNewPaystationSetting = 0 AND (pos.SettingsFileId IS NULL OR sf.Name != pss.PaystationSettingName) ");
        query.append("Order by Name ");
        
        SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SettingsFileId", new IntegerType());
        q.addScalar("SettingsFileName", new StringType());
        q.addScalar("UploadGmt", new TimestampType());
        q.addScalar("IsUpdateInProgress", new BooleanType());
        q.addScalar("IsNotExist", new BooleanType());
        
        q.addScalar("Name", new StringType());
        q.addScalar("PointOfSaleId", new IntegerType());
        q.addScalar("SerialNumber", new StringType());
        q.addScalar("LocationName", new StringType());
        q.addScalar("DownloadGmt", new TimestampType());
        q.addScalar("PayStationType", new IntegerType());
        q.addScalar("Latitude", new BigDecimalType());
        q.addScalar("Longitude", new BigDecimalType());
        
        q.setParameter("customerId", customerId);
        q.setResultTransformer(Transformers.aliasToBean(CustomerAdminPayStationSettingPayStationInfo.class));
        
        List<CustomerAdminPayStationSettingPayStationInfo> resultList = q.list();
        if (resultList == null || resultList.isEmpty()) {
            return null;
        } else {
            CustomerAdminPayStationSettingInfo paystationSetting = new CustomerAdminPayStationSettingInfo();
            paystationSetting.setName(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
            paystationSetting.setIsNotExist(true);
            paystationSetting.setIsUpdateInProgress(false);
            paystationSetting.setRandomId(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
            paystationSetting.setSettingsFileId(null);
            paystationSetting.setUploadDate(DateUtil.getRelativeTimeString(resultList.get(0).getUploadGmt(), timezone));
            
            if (resultList.size() == 1 && resultList.get(0).getPointOfSaleId() == null) {
                paystationSetting.setPayStationList(null);
                paystationSetting.setPayStationCount(0);
            } else {
                for (CustomerAdminPayStationSettingPayStationInfo paystationInfo : resultList) {
                    PointOfSale pos = new PointOfSale();
                    pos.setId(paystationInfo.getPointOfSaleId());
                    paystationInfo.setRandomId(keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME));
                    paystationInfo.setPointOfSaleId(null);
                    paystationInfo.setDownloadDate(DateUtil.getRelativeTimeString(paystationInfo.getDownloadGmt(), timezone));
                }
                paystationSetting.setPayStationCount(resultList.size());
                paystationSetting.setPayStationList(resultList);
            }
            
            return paystationSetting;
        }
    }
}
