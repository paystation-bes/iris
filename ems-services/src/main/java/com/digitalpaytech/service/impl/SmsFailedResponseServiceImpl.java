package com.digitalpaytech.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SmsFailedResponse;
import com.digitalpaytech.service.SmsFailedResponseService;

@Service("smsFailedResponseService")
@Transactional(propagation=Propagation.REQUIRED)
public class SmsFailedResponseServiceImpl implements SmsFailedResponseService {
    
    @Autowired
    private EntityDao entityDao;

    @Override
    public void saveSmsFailedResponse(SmsFailedResponse smsFailedResponse) {
        this.entityDao.save(smsFailedResponse);
    }
    
    
}
