package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.dto.TransactionSearchCriteria;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.WebCoreConstants;

@Service("processorTransactionService")
@Transactional(propagation = Propagation.REQUIRED)
public class ProcessorTransactionServiceImpl implements ProcessorTransactionService {
    private static final Logger LOG = Logger.getLogger(ProcessorTransactionService.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private OpenSessionDao openSessionDao;
    
    private Map<Integer, ProcessorTransactionType> procTxTypesMap;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public ProcessorTransaction findProcessorTransactionById(final Long id, final boolean isEvict) {
        final ProcessorTransaction pt = (ProcessorTransaction) this.entityDao.get(ProcessorTransaction.class, id);
        if (isEvict) {
            this.entityDao.evict(pt);
        }
        return pt;
    }
    
    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private void retrieveAndCreateProcessorTransactionTypesMap() {
        final List<ProcessorTransactionType> list = this.openSessionDao.loadAll(ProcessorTransactionType.class);
        this.procTxTypesMap = new HashMap<Integer, ProcessorTransactionType>(list.size());
        final Iterator<ProcessorTransactionType> iter = list.iterator();
        while (iter.hasNext()) {
            final ProcessorTransactionType proTxType = iter.next();
            this.procTxTypesMap.put(proTxType.getId(), proTxType);
        }
    }
    
    @Override
    public ProcessorTransactionType getProcessorTransactionType(final Integer processorTransactionTypeId) {
        return this.procTxTypesMap.get(processorTransactionTypeId);
    }
    
    //------------------------------------------------------------------------------------------
    // Copied from EMS 6.3.11 com.digitalpioneer.service.Impl.ProcessorTransactionServiceImpl
    
    /**
     * @param psList
     *            A list of PointOfSale ids.
     */
    @Override
    public Set<ProcessorTransaction> findRefundablePtdsByCard(final short last4DigitsofCardNumber, final int cardChecksum, final String cardHash,
        final String cardType, final Date startDate, final Date endDate, final int merchantAccountId, final Collection<Integer> psList) {
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> chechSumList =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundablePTDsByCard",
                                                             new String[] { "psList", "startDate", "endDate", "merchantAccountId", "cardChecksum",
                                                                 "last4Digits", "cardType" },
                                                             new Object[] { psList, startDate, endDate, merchantAccountId, cardChecksum,
                                                                 last4DigitsofCardNumber, cardType });
        
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> cardHashList =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundablePTDsByCardHash",
                                                             new String[] { "psList", "startDate", "endDate", "merchantAccountId", "cardHash",
                                                                 "last4Digits", "cardType" },
                                                             new Object[] { psList, startDate, endDate, merchantAccountId, cardHash,
                                                                 last4DigitsofCardNumber, cardType });
        
        return mergeProcessorTransactionList(chechSumList, cardHashList);
    }
    
    private Set<ProcessorTransaction> mergeProcessorTransactionList(final List<ProcessorTransaction> list1, final List<ProcessorTransaction> list2) {
        final Set<ProcessorTransaction> mergeList = new HashSet<ProcessorTransaction>();
        Iterator<ProcessorTransaction> it = list1.iterator();
        while (it.hasNext()) {
            mergeList.add(it.next());
        }
        it = list2.iterator();
        while (it.hasNext()) {
            mergeList.add(it.next());
        }
        return mergeList;
    }
    
    /**
     * @param psList
     *            A list of PointOfSale ids.
     */
    @Override
    public Set<ProcessorTransaction> findRefundablePtdsByCardAndAuthNumber(final String authNumber, final short last4DigitsofCardNumber,
        final int cardChecksum, final String cardHash, final String cardType, final Date startDate, final Date endDate, final int merchantAccountId,
        final Collection<Integer> psList) {
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> chechSumList =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundablePTDsByCardAndAuthNum",
                                                             new String[] { "psList", "startDate",
                                                                 "endDate", "merchantAccountId", "cardChecksum", "last4Digits", "cardType",
                                                                 "authNumber" },
                                                             new Object[] { psList, startDate, endDate, merchantAccountId, cardChecksum,
                                                                 last4DigitsofCardNumber, cardType, authNumber });
        
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> cardHashList =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundablePTDsByCardHashAndAuthNum",
                                                             new String[] { "psList", "startDate", "endDate", "merchantAccountId", "cardHash",
                                                                 "last4Digits", "cardType", "authNumber" },
                                                             new Object[] { psList, startDate, endDate, merchantAccountId, cardHash,
                                                                 last4DigitsofCardNumber, cardType, authNumber });
        
        return mergeProcessorTransactionList(chechSumList, cardHashList);
    }
    
    @Override
    public int updateProcessorTransactionType(final Long id, final int typeId, final int newTypeId) {
        
        final String[] params = new String[] { "id", "typeId", "newTypeId" };
        final Object[] values = new Object[] { id, typeId, newTypeId };
        return this.entityDao.updateByNamedQuery("ProcessorTransaction.updateProcessorTransactionType", params, values);
    }
    
    @Override
    public final int markRecoverable(final ProcessorTransaction pt) {
        final String authNumber = pt.getAuthorizationNumberAndPreAuthId();
        
        this.entityDao.getCurrentSession().refresh(pt);
        pt.setAuthorizationNumber(authNumber);
        pt.setProcessorTransactionType(getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_RECOVERABLE));
        this.entityDao.update(pt);
        this.entityDao.flush();
        
        return 1;
    }
    
    @Override
    public ProcessorTransaction getApprovedTransaction(final int pointOfSaleId, final Date purchasedDate, final int ticketNumber) {
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> pts =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.getApprovedTransactionByPSDateAndTicketNum",
                                                             new String[] { "pointOfSaleId", "purchasedDate", "ticketNumber" }, new Object[] {
                                                                 pointOfSaleId, purchasedDate, ticketNumber });
        
        ProcessorTransaction ptd = null;
        if (pts != null && pts.size() > 0) {
            ptd = pts.get(0);
        }
        return ptd;
    }
    
    private String returnBlankIfNull(final String processorTransactionId) {
        if (StringUtils.isNotBlank(processorTransactionId)) {
            return processorTransactionId;
        }
        return "";
    }
    
    @Override
    public void updateProcessorTransactionToSettled(final ProcessorTransaction ptd) {
        final int updated = this.entityDao
                .updateByNamedQuery("ProcessorTransaction.updateProcessorTransactionToSettled",
                                    new String[] { "merchantAccountId", "amountInCents", "cardType", "last4DigitsOfCardNumber", "cardHash",
                                        "processorTransactionId", "authorizationNumber", "referenceNumber", "processingDate", "pointOfSaleId",
                                        "purchasedDate", "ticketNumber", "isRFID" },
                                    new Object[] { ptd.getMerchantAccount().getId(), ptd.getAmount(), ptd.getCardType(),
                                        ptd.getLast4digitsOfCardNumber(), ptd.getCardHash(), returnBlankIfNull(ptd.getProcessorTransactionId()),
                                        ptd.getAuthorizationNumberAndPreAuthId(), ptd.getReferenceNumber(), ptd.getProcessingDate(),
                                        ptd.getPointOfSale().getId(), ptd.getPurchasedDate(), ptd.getTicketNumber(), ptd.isIsRfid() });
        
        LOG.info("Settled " + updated + " ProcessorTransaction.");
    }
    
    @Override
    public void updateProcessorTransactionToBatchedSettled(final ProcessorTransaction ptd) {
        final int updated = this.entityDao
                .updateByNamedQuery("ProcessorTransaction.updateProcessorTransactionToBatchedSettled",
                                    new String[] { "merchantAccountId", "amountInCents", "cardType", "last4DigitsOfCardNumber", "cardHash",
                                        "processorTransactionId", "authorizationNumber", "referenceNumber", "processingDate", "pointOfSaleId",
                                        "purchasedDate", "ticketNumber", "isRFID" },
                                    new Object[] { ptd.getMerchantAccount().getId(), ptd.getAmount(), ptd.getCardType(),
                                        ptd.getLast4digitsOfCardNumber(), ptd.getCardHash(), returnBlankIfNull(ptd.getProcessorTransactionId()),
                                        ptd.getAuthorizationNumberAndPreAuthId(), ptd.getReferenceNumber(), ptd.getProcessingDate(),
                                        ptd.getPointOfSale().getId(), ptd.getPurchasedDate(), ptd.getTicketNumber(), ptd.isIsRfid() });
        
        LOG.info("Batch settled " + updated + " ProcessorTransaction.");
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Collection<ProcessorTransaction> findProcessorTransactionByPurchaseId(final Long purchaseId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findProcessorTransactionByPurchaseId", "purchaseId", purchaseId,
                                                            true);
    }
    
    @Override
    public Collection<ProcessorTransaction> findProcessorTransactionByPurchaseIdAndIsApprovedAndIsRefund(final Long purchaseId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findProcessorTransactionByPurchaseIdAndIsRefundAndIsApproved",
                                                            "purchaseId", purchaseId, true);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Collection<ProcessorTransaction> findRefundedProcessorTransaction(final Long purchaseId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundedProcessorTransaction", "purchaseId", purchaseId, true);
    }
    
    @Override
    public ProcessorTransaction findRefundProcessorTransaction(final Integer pointOfSaleId, final Date purchasedDate, final Integer ticketNumber) {
        final String[] params = { "pointOfSaleId", "purchasedDate", "ticketNumber" };
        final Object[] values = { pointOfSaleId, purchasedDate, ticketNumber };
        
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> list =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundProcessorTransaction", params, values);
        if (list != null && !list.isEmpty()) {
            return (ProcessorTransaction) list.get(0);
        }
        return null;
    }
    
    @Override
    public ProcessorTransaction findPartialSettlementProcessorTransaction(final Integer pointOfSaleId, final Date purchasedDate,
        final Integer ticketNumber, final boolean isEvict) throws ApplicationException {
        final String[] params = { "pointOfSaleId", "purchasedDate", "ticketNumber" };
        final Object[] values = { pointOfSaleId, purchasedDate, ticketNumber };
        
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> list =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findPartialSettlementProcessorTransaction", params, values);
        ProcessorTransaction processorTransaction = null;
        if (list != null && !list.isEmpty()) {
            if (list.size() != 1) {
                throw new InvalidDataException();
            }
            processorTransaction = (ProcessorTransaction) list.get(0);
            //TODO What does this do?
            if (isEvict) {
                this.entityDao.evict(processorTransaction);
            }
        }
        return processorTransaction;
    }
    
    @Override
    public ProcessorTransaction findProcTransByPurchaseIdTicketNoAndPurchasedDate(final Long purchaseId, final Date purchasedDate,
        final Integer ticketNumber) {
        final String[] params = { "purchaseId", "purchasedDate", "ticketNumber" };
        final Object[] values = { purchaseId, purchasedDate, ticketNumber };
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> list = this.entityDao
                .findByNamedQueryAndNamedParam("ProcessorTransaction.findProcTransByPurchaseIdPurchasedDateAndTicketNo", params, values);
        
        if (list != null && !list.isEmpty()) {
            return (ProcessorTransaction) list.get(0);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<ProcessorTransaction> findRefundableTransaction(final TransactionSearchCriteria criteria) {
        final Criteria query = this.entityDao.createCriteria(ProcessorTransaction.class)
                .createAlias("elavonTransactionDetails", "elavonDetails", Criteria.LEFT_JOIN).createAlias("merchantAccount", "merchant")
                .add(Restrictions.in("processorTransactionType.id", new Object[] { 2, 3, 10, 11, 17, 21 })).add(Restrictions.eq("isApproved", true))
                .add(Restrictions.between("purchasedDate", criteria.getStartDate(), criteria.getEndDate()))
                .add(Restrictions.eq("cardType", criteria.getCardType()))
                .add(Restrictions.or(Restrictions.ne("merchant.processor.id", CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX),
                                     Restrictions.eq("elavonDetails.transactionSettlementStatusType.id", 2)));
        
        if (criteria.getAuthNumber() != null) {
            query.add(Restrictions.eq("authorizationNumber", criteria.getAuthNumber()));
        }
        
        if ((criteria.getMerchantAccountIds() != null) && (!criteria.getMerchantAccountIds().isEmpty())) {
            query.add(Restrictions.in("merchantAccount.id", criteria.getMerchantAccountIds()));
        }
        
        if ((criteria.getPointOfSaleIds() != null) && (!criteria.getPointOfSaleIds().isEmpty())) {
            query.add(Restrictions.in("pointOfSale.id", criteria.getPointOfSaleIds()));
        } else {
            query.createAlias("pointOfSale", "pos").createAlias("pos.customer", "cust").add(Restrictions.eq("cust.id", criteria.getCustomerId()));
        }
        
        query.add(Restrictions.or(Restrictions.eq("cardHash", criteria.getCardHash()), Restrictions.eq("cardChecksum", criteria.getCardCheckSum())));
        
        /*
         * query.addOrder(Property.forName("purchasedDate").desc()); ordering this table slow down the query performance dramatically.
         * This happens because of the database indexes are not optimize for this query.
         * query.setCacheable(true); no need to cache this query, it would waste memory rather than being useful because this is for 1 consumer
         * (cache hits are surely super low).
         */
        
        return query.list();
    }
    
    @Override
    public ProcessorTransaction findTransactionPosPurchaseById(final Long transactionId) {
        return (ProcessorTransaction) this.entityDao.getNamedQuery("ProcessorTransaction.findPPPByIds")
                .setParameter("processorTransactionIds", transactionId).uniqueResult();
    }
    
    @Override
    public void removePreAuthsInProcessorTransactions(final List<Long> preAuthIds) {
        for (Long preAuthId : preAuthIds) {
            this.entityDao.getNamedQuery("ProcessorTransaction.deletePreAuthIdByPreAuthId").setParameter("preAuthId", preAuthId).executeUpdate();
        }
    }
    
    @Override
    public final void removePreAuthInProcessorTransactions(final Long preAuthId) {
        final ProcessorTransaction pt = (ProcessorTransaction) this.entityDao
                .getNamedQuery("ProcessorTransaction.findProcessorTransactionByPreAuthId").setParameter("preAuthId", preAuthId).uniqueResult();
        if (pt != null) {
            pt.setPreAuth(null);
            this.entityDao.update(pt);
        }
    }
    
    @Override
    public void updateProcessorTransaction(final ProcessorTransaction ptd) {
        this.entityDao.update(ptd);
    }
    
    @Override
    public void saveProcessorTransaction(final ProcessorTransaction ptd) {
        this.entityDao.save(ptd);
    }
    
    @Override
    public final ProcessorTransaction findByPointOfSaleIdPurchasedDateTicketNumber(final int pointOfSaleId, final Date purchasedDate,
        final int ticketNumber) {
        final String[] params = new String[] { "pointOfSaleId", "purchasedDate", "ticketNumber" };
        final Object[] values = new Object[] { pointOfSaleId, purchasedDate, ticketNumber };
        final List<ProcessorTransaction> list =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findByPointOfSaleIdPurchasedDateTicketNumber", params, values);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    @Override
    public boolean isIncomplete(final ProcessorTransaction pt) {
        return CardProcessingUtil.isNotNullAndIsLink(pt.getMerchantAccount()) && isAllNA(pt);
    }
    
    @Override
    public boolean isIncomplete(final MerchantAccount ma, final ProcessorTransaction pt) {
        return CardProcessingUtil.isNotNullAndIsLink(ma) && isAllNA(pt);
    }
    
    private boolean isAllNA(final ProcessorTransaction pt) {
        return pt.getAuthorizationNumber().equals(WebCoreConstants.N_A_STRING) && pt.getReferenceNumber().equals(WebCoreConstants.N_A_STRING)
               && pt.getProcessorTransactionId().equals(WebCoreConstants.N_A_STRING);
    }
    
    @Override
    public Collection<ProcessorTransaction> findByMercAccIdAndProcTransTypeIdAndHasPurchase(final Integer merchantAccountId,
        final Integer transactionTypeId) {
        @SuppressWarnings("unchecked")
        final Collection<ProcessorTransaction> trans =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findByMerchantAccountIdAndProcessorTransactionTypeIdAndHasPurchase",
                                                             new String[] { "merchantAccountId", "processorTransactionTypeId" }, new Object[] {
                                                                 merchantAccountId, transactionTypeId });
        
        if (trans == null) {
            return new ArrayList<>();
        }
        return trans;
    }
    
    @Override
    public boolean isRefundableTransaction(final ProcessorTransaction ptd) {
        return IntStream.of(CardProcessingConstants.REFUNDABLE_PROCESSOR_TRANSACTION_TYPE_IDS).anyMatch(i -> ptd.getProcessorTransactionType().getId() == i);
    }
}
