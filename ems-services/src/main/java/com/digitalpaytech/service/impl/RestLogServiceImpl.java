package com.digitalpaytech.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestLog;
import com.digitalpaytech.service.RestLogService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("restLogService")
@Transactional(propagation = Propagation.REQUIRED)
public class RestLogServiceImpl implements RestLogService {
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public void saveRestLog(RestAccount restAccount, String method, boolean isError) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        
        String[] parameters = { "restAccountId", "endPointName", "loggingDate" };
        Object[] values = { restAccount.getId(), method, today.getTime() };
        RestLog restLog = (RestLog) this.entityDao.findUniqueByNamedQueryAndNamedParam("RestLog.findRestLogByRestAccountIdAndEndPointNameAndLoggingDate",
                                                                                    parameters, values, false);
        if (restLog == null) {
            restLog = new RestLog();
            restLog.setRestAccount(restAccount);
            restLog.setEndpointName(method);
            restLog.setLoggingDate(today.getTime());
            restLog.setTotalCalls(0);
            restLog.setCustomer(restAccount.getCustomer());
        }
        restLog.setIsError(isError);
        restLog.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        restLog.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        restLog.setTotalCalls(restLog.getTotalCalls() + 1);
        
        this.entityDao.saveOrUpdate(restLog);
        
    }
    
}
