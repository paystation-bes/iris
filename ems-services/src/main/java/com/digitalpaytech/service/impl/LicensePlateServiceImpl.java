package com.digitalpaytech.service.impl;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.client.LicensePlateClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.llps.PreferredParkerFile;
import com.digitalpaytech.clientcommunication.dto.MultipartFileWrapper;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.PlateCheck;
import com.digitalpaytech.dto.PlateCheckResponse;
import com.digitalpaytech.dto.paystation.LicensePlateMessage;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.InvalidTopicTypeException;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.fasterxml.jackson.core.type.TypeReference;

@Service("licensePlateService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings("PMD.ExcessiveImports")
public class LicensePlateServiceImpl implements LicensePlateService {
    
    private static final Logger LOG = Logger.getLogger(LicensePlateServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private ClientFactory clientFactory;
    
    private LicensePlateClient licensePlateClient;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    @Qualifier("linkMessageProducer")
    private AbstractMessageProducer linkMessageProducer;
    
    @Autowired
    private LocationService locationService;
    
    private JSON json;
    
    @PostConstruct
    public final void init() {
        this.json = new JSON();
        
        this.licensePlateClient = this.clientFactory.from(LicensePlateClient.class);
    }
    
    @Override
    public final LicencePlate findByNumber(final String number) {
        return (LicencePlate) this.entityDao.findUniqueByNamedQueryAndNamedParam("LicencePlate.findLicencePlateByNumber", "number", number, true);
    }
    
    @Override
    public final LicencePlate findById(final Integer id) {
        return (LicencePlate) this.entityDao.findUniqueByNamedQueryAndNamedParam("LicencePlate.findLicencePlateById", "id", id, true);
    }
    
    @Override
    public final PreferredParkerFile getCurrentStatus(final String fileId, final Integer unifiId) throws JsonException {
        
        final String jsonStr = this.licensePlateClient.currentStatus(fileId, unifiId).execute().toString(Charset.defaultCharset());
        
        try {
            final ObjectResponse<PreferredParkerFile> fileObj =
                    this.json.deserialize(jsonStr, new TypeReference<ObjectResponse<PreferredParkerFile>>() {
                    });
            if (fileObj == null) {
                return null;
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("getCurrentStatus: " + fileObj.getResponse());
                }
                return fileObj.getResponse();
            }
        } catch (JsonException jex) {
            LOG.error("Cannot getCurrentStatus, fileId: " + fileId + ", unifiId: " + unifiId, jex);
            throw jex;
        }
    }
    
    @Override
    public final PlateCheckResponse checkPlateLookup(final PlateCheck plateCheck) throws JsonException {
        
        final String stringResult = this.licensePlateClient
                .plateCheck(plateCheck.getLicensePlateNo(), plateCheck.getCustomerId(), plateCheck.getLocationName(),
                            plateCheck.getIsPreferredByLocation(), plateCheck.getIsLimitedByLocation(), plateCheck.getLocalDate())
                .execute().toString(Charset.defaultCharset());
        
        final TypeReference<ObjectResponse<PlateCheckResponse>> objectResponseTypeRef = new TypeReference<ObjectResponse<PlateCheckResponse>>() {
        };
        
        final ObjectResponse<PlateCheckResponse> objectResponse = this.json.deserialize(stringResult, objectResponseTypeRef);
        
        return objectResponse.getResponse();
        
    }
    
    @Override
    public final PreferredParkerFile uploadLicensePlates(final Integer unifiId, final boolean isByLocation, final MultipartFile multipartFile)
        throws JsonException {
        
        final Map<String, String> params = new ConcurrentHashMap<String, String>();
        params.put("isByLocation", Boolean.toString(isByLocation));
        params.put("customerId", unifiId.toString());
        
        final MultipartFileWrapper multipartFileWrapper =
                new MultipartFileWrapper(multipartFile, Long.toHexString(Instant.now().toEpochMilli()), params, "file");
        final String response = this.licensePlateClient.uploadLicensePlates(multipartFileWrapper).execute().toString(Charset.defaultCharset());
        
        final ObjectResponse<PreferredParkerFile> objectResponse =
                this.json.deserialize(response, new TypeReference<ObjectResponse<PreferredParkerFile>>() {
                });
        return objectResponse.getResponse();
    }
    
    @Override
    public Future<Boolean> updateLicensePlateService(final TransactionDto txDto, final PointOfSale pointOfSale, final Purchase purchase) {
        LicensePlateMessage message = null;
        try {
            
            final CustomerProperty jurisdictionTypePreferred = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                            WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED);
            
            final CustomerProperty jurisdictionTypeLimited = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                            WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED);
            
            final boolean isPreferred = BooleanUtils.isTrue(txDto.getPreferredRate());
            
            final boolean isLimited = BooleanUtils.isTrue(txDto.getLimitedRate());
            
            if (!StringUtils.isBlank(txDto.getLicensePlateNo()) && (isPreferred || isLimited)) {
                
                final Integer unifiId = pointOfSale.getCustomer().getUnifiId();
                
                final boolean isPreferredByLocation =
                        compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION, jurisdictionTypePreferred);
                
                final boolean isLimitedByLocation =
                        compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION, jurisdictionTypeLimited);
                
                final Location location = this.locationService.findLocationByPointOfSaleId(pointOfSale.getId());
                
                final CustomerProperty timeZoneProperty = this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                
                final String localDateString =
                        StableDateUtil.getDateFormattedWithChronoUnitDays(StableDateUtil.DATE_ONLY_FORMAT, timeZoneProperty.getPropertyValue(),
                                                                          purchase.getPurchaseGmt());
                
                message = new LicensePlateMessage(txDto.getLicensePlateNo(), unifiId, location.getName(), isPreferred, isPreferredByLocation,
                        isLimited, isLimitedByLocation, purchase.getPurchaseGmt(), localDateString);
                
                return this.linkMessageProducer.sendWithByteArray(KafkaKeyConstants.LINK_KAFKA_LIMITED_PREFERRED_RATE_INCREMENT_TOPIC_NAME,
                                                                  txDto.getLicensePlateNo(), message);
                
            }
        } catch (InvalidTopicTypeException | JsonException | TaskRejectedException e) {
            final StringBuilder messageBuilder = new StringBuilder().append("Unable to send message to ").append(LicensePlateClient.ALIAS);
            
            if (message != null) {
                messageBuilder.append('[').append(message.toString()).append(']');
            }
            LOG.error(messageBuilder.toString(), e);
        }
        return CompletableFuture.completedFuture(false);
    }
    
    @Override
    public boolean compareJurisdictionType(final int expectedJurisdictionType, final CustomerProperty jurisdictionTypeProperty) {
        if (jurisdictionTypeProperty == null) {
            return false;
        } else {
            return String.valueOf(expectedJurisdictionType).equals(jurisdictionTypeProperty.getPropertyValue());
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setMessageProducer(final AbstractMessageProducer producer) {
        this.linkMessageProducer = producer;
    }
    
}
