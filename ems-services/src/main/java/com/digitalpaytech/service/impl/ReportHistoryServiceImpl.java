package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ReportContent;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.support.RelativeDateTime;

@Service("reportHistoryService")
@Transactional(propagation = Propagation.SUPPORTS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class ReportHistoryServiceImpl implements ReportHistoryService {
    private static final Integer[] STATUS_FAILED = { 5, 6 };
    
    private static final String HQL_STATUS_TYPE_ID = "reportStatusType.id";
    
    private static final int BYTES_IN_KILOBYTE = 1024;
    
    private static final String SQL_GET_COMPLETED_REPORTS_COUNT = "SELECT COUNT(*) FROM ReportHistory rh WHERE rh.ReportStatusTypeId = 4 AND rh.ExecutionEndGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)";
    private static final String SQL_GET_CANCELLED_REPORTS_COUNT = "SELECT COUNT(*) FROM ReportHistory rh WHERE rh.ReportStatusTypeId = 5 AND rh.ExecutionEndGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)";
    private static final String SQL_GET_FAILED_REPORTS_COUNT = "SELECT COUNT(*) FROM ReportHistory rh WHERE rh.ReportStatusTypeId = 6 AND rh.ExecutionEndGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)";
    private static final String SQL_GET_REPORT_RESPONSE_TIME = "SELECT SUM(timestampdiff(SECOND, rh.ScheduledBeginGMT, rh.ExecutionBeginGMT)) / count(*) from ReportHistory rh WHERE rh.ScheduledBeginGMT IS NOT NULL AND rh.ExecutionEndGMT IS NOT NULL AND rh.ExecutionEndGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)";
    
    @Autowired
    private EntityDao entityDao;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveReportHistory(final ReportHistory reportHistory, final ReportQueue reportQueue, final byte[] fileContent, final String fileName,
        final boolean isWithEmail) {
        
        final ReportQueue existingReportQueue = (ReportQueue) this.entityDao.merge(reportQueue);
        reportHistory.setExecutionEndGmt(new Date());
        
        this.entityDao.save(reportHistory);
        if (fileContent != null) {
            final ReportRepository reportRepository = new ReportRepository();
            reportRepository.setReportHistory(reportHistory);
            reportRepository.setUserAccount(reportHistory.getUserAccount());
            reportRepository.setAddedGmt(new Date());
            reportRepository.setFileSizeKb((short) (fileContent.length / BYTES_IN_KILOBYTE));
            reportRepository.setFileName(fileName);
            reportRepository.setIsReadByUser(isWithEmail);
            reportRepository.setIsSoonToBeDeleted(false);
            this.entityDao.save(reportRepository);
            
            final ReportContent reportContent = new ReportContent();
            reportContent.setReportRepository(reportRepository);
            reportContent.setContent(fileContent);
            
            this.entityDao.save(reportContent);
        }
        
        this.entityDao.delete(existingReportQueue);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateReportHistory(final ReportHistory reportHistory) {
        this.entityDao.update(this.entityDao.merge(reportHistory));
    }
    
    @Override
    public List<ReportHistory> findFailedAndCancelledReportsByUserAccountId(final int userAccountId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportHistory.findFailedAndCancelledReportsByUserAccountId",
                                                            HibernateConstants.USER_ACCOUNT_ID, userAccountId);
    }
    
    @Override
    public List<ReportHistory> findFailedAndCancelledReportsByCustomerId(final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportHistory.findFailedAndCancelledReportsByCustomerId",
                                                            HibernateConstants.CUSTOMER_ID, customerId);
    }
    
    @Override
    public List<ReportHistory> findFailedAndCancelledReportsByCustomerIdAndUserAccountId(final int userAccountId, final int customerId) {
        return this.entityDao.findByNamedQueryAndNamedParam("ReportHistory.findFailedAndCancelledReportsByCustomerIdAndUserAccountId", new String[] {
            HibernateConstants.USER_ACCOUNT_ID, HibernateConstants.CUSTOMER_ID, }, new Integer[] { userAccountId, customerId, });
    }
    
    @Override
    public List<ReportHistory> findFinishedReportsByUserAccountId(final int userAccountId) {
        final List<ReportRepository> repositoryList = this.entityDao.findByNamedQueryAndNamedParam("ReportRepository.findByUserAccountId",
                                                                                                   HibernateConstants.USER_ACCOUNT_ID, userAccountId);
        if (repositoryList != null && repositoryList.size() != 0) {
            final List<Integer> historyIdList = new ArrayList<Integer>();
            for (ReportRepository repository : repositoryList) {
                historyIdList.add(repository.getReportHistory().getId());
            }
            
            return this.entityDao.findByNamedQueryAndNamedParam("ReportHistory.findByHistoryIdList", "historyIdList", historyIdList);
        }
        
        return null;
    }
    
    @Override
    public ReportHistory getReportHistory(final int id) {
        
        return (ReportHistory) this.entityDao.get(ReportHistory.class, id);
    }
    
    @Override
    public void deleteReportHistory(final int id) {
        final ReportHistory history = (ReportHistory) this.entityDao.get(ReportHistory.class, id);
        final ReportStatusType deleteStatus = new ReportStatusType();
        deleteStatus.setId(ReportingConstants.REPORT_STATUS_DELETED);
        
        history.setReportStatusType(deleteStatus);
    }
    
    @Override
    public int getCompletedReports() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_COMPLETED_REPORTS_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public int getCancelledReports() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_CANCELLED_REPORTS_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public int getFailedReports() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_FAILED_REPORTS_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public float getReportResponseTime() {
        try {
            final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_REPORT_RESPONSE_TIME);
            return Float.parseFloat(q.list().get(0).toString());
        } catch (NullPointerException e) {
            return 0;
        }
    }
    
    @Override
    public ReportHistory findRecentlyExecutedReport(final int reportDefinitionId, final Date minExecutionEndGmt) {
        return (ReportHistory) this.entityDao.getNamedQuery("ReportHistory.findRecentlyExecutedReport")
                .setParameter("reportDefinitionId", reportDefinitionId).setParameter("minExecutionEndGmt", minExecutionEndGmt).setFirstResult(0)
                .setMaxResults(1).uniqueResult();
    }
    
    @Override
    public ReportHistory findRecentlyExecutedReport(final AdhocReportInfo adhocReportInfo) {
        ReportHistory result = null;
        if (adhocReportInfo.getReportHistoryId() != null) {
            result = this.entityDao.get(ReportHistory.class, adhocReportInfo.getReportHistoryId());
        } else {
            result = this.findRecentlyExecutedReport((Integer) adhocReportInfo.getReportDefinitionId().getId(),
                                                     new Date(adhocReportInfo.getQueuedGmt()));
        }
        
        return result;
    }
    
    @Override
    @SuppressWarnings({ "checkstyle:magicnumber" })
    // I suppress magic number here becuase it easier to read when those magic numbers are just index of array.
    public List<QueueInfo> findFailedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        final Criteria query = this.entityDao.createCriteria(ReportHistory.class);
        query.createAlias("reportDefinition", "rd");
        
        query.setProjection(Projections.projectionList().add(Projections.property(HibernateConstants.ID)).add(Projections.property("rd.title"))
                .add(Projections.property(HQL_STATUS_TYPE_ID)).add(Projections.property(HibernateConstants.STATUS_EXPLANATION))
                .add(Projections.property(HibernateConstants.EXECUTION_BEGIN_GMT)));
        
        query.setResultTransformer(new ResultTransformer() {
            private static final long serialVersionUID = -5621202242487261862L;
            
            @Override
            public Object transformTuple(final Object[] tuple, final String[] aliases) {
                final QueueInfo result = new QueueInfo();
                result.setRandomId(keyMapping.getRandomString(ReportHistory.class, tuple[0]));
                result.setTitle((String) tuple[1]);
                result.setStatus((Integer) tuple[2]);
                result.setStatusExplanation((String) tuple[3]);
                result.setTime(new RelativeDateTime((Date) tuple[4], timeZone));
                
                return result;
            }
            
            @SuppressWarnings("rawtypes")
            @Override
            public List transformList(final List collection) {
                return collection;
            }
        });
        
        query.add(Restrictions.in(HQL_STATUS_TYPE_ID, STATUS_FAILED));
        query.add(Restrictions.eq("rd.isSystemAdmin", criteria.isSysadmin()));
        
        if (criteria.getCustomerId() != null) {
            query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        }
        
        if (criteria.getUserAccountId() != null) {
            query.add(Restrictions.eq("userAccount.id", criteria.getUserAccountId()));
        }
        
        query.addOrder(Order.desc("executionEndGmt"));
        
        return query.list();
    }
}
