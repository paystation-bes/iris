package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.exception.CardTransactionException;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("preAuthService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PreAuthServiceImpl implements PreAuthService {
    
    @Autowired
    private EntityDao entityDao;
    
    /**
     * @return PreAuth Returns null if couldn't find any record.
     */
    @Override
    public PreAuth getApprovedPreAuthByPsRefId(final int pointOfSaleId, final String psRefId, final String cardHash) {
        final List<PreAuth> preauths = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getApprovedPreAuthByPsRefId",
                                                                                    new String[] { "pointOfSaleId", "psRefId", "cardHash" },
                                                                                    new Object[] { pointOfSaleId, psRefId, cardHash });
        if (preauths != null && preauths.size() > 0) {
            return preauths.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * @return PreAuth Returns null if couldn't find any record.
     */
    @Override
    public PreAuth getApprovedPreAuthByPsRefIdAndPSAmount(final int pointOfSaleId, final String psRefId, final String cardHash, final int amount) {
        final List<PreAuth> preauths = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getApprovedPreAuthByPsRefIdAndPSAmount",
                                                                                    new String[] { "pointOfSaleId", "psRefId", "cardHash", "amount" },
                                                                                    new Object[] { pointOfSaleId, psRefId, cardHash, amount });
        if (preauths != null && !preauths.isEmpty()) {
            return preauths.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public PreAuth getApprovedPreAuthById(final Long id) {
        final List<PreAuth> list =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getApprovedPreAuthById", new String[] { "id" }, new Object[] { id });
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    @Override
    public ScrollableResults getExpiredCardAuthorizationBackUp(final Date expiryDate) {
        /*
         * Need to pass a Collection object to findByNamedQueryAndNamedParam, otherwise Hibernate will throw ClassCastException.
         */
        final List<Integer> ids = PreAuthServiceImpl.convertToIntegerList(CardProcessingConstants.EXCLUDED_PROCESSOR_IDS);
        final ScrollableResults scroll = this.entityDao.getNamedQuery("PreAuth.getExpiredCardAuthorizationBackUp")
                .setParameter("expiryDate", expiryDate).setParameterList("excludedProcessorIds", ids).scroll(ScrollMode.FORWARD_ONLY);
        return scroll;
    }
    
    @Override
    public void cleanupExpiredPreauths(final Date expiredDate) {
        this.entityDao.updateByNamedQuery("PreAuth.cleanupExpiredPreauths", new String[] { "date" }, new Object[] { expiredDate });
    }
    
    @Override
    public void deleteExpiredPreauths(final Date expiredDate) {
        this.entityDao.updateByNamedQuery("PreAuth.deleteExpiredPreauths", new String[] { "date" }, new Object[] { expiredDate });
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deletePreauthsByIds(final Collection<Long> preAuthIds) {
        @SuppressWarnings("unchecked")
        final List<PreAuth> pas = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findPreauthsByIds", new String[] { "preAuthIds" },
                                                                               new Object[] { preAuthIds }, false);
        for (PreAuth pa : pas) {
            this.entityDao.delete(pa);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PreAuth> findByIds(final Long... ids) {
        final List<Long> preAuthIds = new ArrayList<Long>(ids.length);
        for (Long id : ids) {
            preAuthIds.add(id);
        }
        
        return this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findPreauthsByIds", new String[] { "preAuthIds" }, new Object[] { preAuthIds },
                                                            false);
    }
    
    @Override
    public List<PreAuth> findExpiredPreAuths(final Date expiredDate) {
        final String[] params = new String[] { "date" };
        final Object[] values = new Object[] { expiredDate };
        return this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getExpiredPreAuths", params, values, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Long> findExpiredElavonPreAuths(final Date expiryDate, final int maxPreAuthLimit) {
        final String[] params = { "expiryDate", "processorId" };
        final Object[] values = { expiryDate, CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX };
        
        return this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getExpiredPreAuthsByProcessorId", params, values, 0, maxPreAuthLimit, false);
    }
    
    @Override
    public PreAuth getApprovedPreAuthByPSAmountAndAuthNumAndDate(final int pointOfSaleId, final int amount, final String authNumber,
        final long startDate, final long endDate) {
        final String[] params = { "pointOfSaleId", "amount", "authNumber", "startDate", "endDate" };
        final Object[] values = { pointOfSaleId, amount, authNumber, startDate, endDate };
        final List<PreAuth> preauths =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getApprovedPreAuthByPSAmountAndAuthNumAndDate", params, values);
        if (preauths.size() > 0) {
            return preauths.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public PreAuth getApprovedPreAuthByPSAmountAndAuthNum(final int pointOfSaleId, final int amount, final String authNumber) {
        final String[] params = { "pointOfSaleId", "amount", "authNumber" };
        final Object[] values = { pointOfSaleId, amount, authNumber };
        final List<PreAuth> preauths = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getApprovedPreAuthByPSAmountAndAuthNum", params, values);
        if (preauths.size() > 0) {
            return preauths.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public int findTotalPreAuthsByCardData(final String cardData) {
        return this.entityDao.findTotalResultByNamedQuery("PreAuth.findTotalPreAuthsByCardData", new String[] { "cardData" },
                                                          new Object[] { cardData });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Collection<PreAuth> findPreAuthsByCardData(final String cardData, final boolean isEvict) {
        final List<PreAuth> lists = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findPreAuthByCardData", "cardData", cardData);
        
        if (isEvict) {
            for (PreAuth preAuth : lists) {
                this.entityDao.evict(preAuth);
            }
        }
        
        return lists;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePreAuth(final PreAuth preAuth) {
        this.entityDao.update(preAuth);
    }
    
    /**
     * TODO: Replace with original logic AFTER migration is completed .
     */
    @Override
    public void cleanupExpiredPreauthsForMigratedCustomers(final Date expiredDate) {
        this.entityDao.updateByNamedQuery("PreAuth.cleanupExpiredPreauthsForMigratedCustomers", new String[] { "date" },
                                          new Object[] { expiredDate });
    }
    
    @Override
    public final void cleanupExpiredPreauthForMigratedCustomer(final PreAuth preAuth) {
        preAuth.setExpired((short) 1);
        preAuth.setCardData(null);
        this.entityDao.update(preAuth);
    }
    
    /**
     * TODO: Replace with original logic AFTER migration is completed .
     */
    @Override
    public void deleteExpiredPreauthsForMigratedCustomers(final Date expiredDate) {
        this.entityDao.getNamedQuery("PreAuth.deleteExpiredPreauthsForMigratedCustomers").setParameter("date", expiredDate).executeUpdate();
    }
    
    @Override
    public final void deleteExpiredPreauthForMigratedCustomer(final PreAuth preAuth) {
        this.entityDao.delete(preAuth);
    }
    
    /**
     * TODO: Replace with original logic AFTER migration is completed.
     */
    @Override
    public List<PreAuth> findExpiredPreAuthsForMigratedCustomers(final Date expiredDate) {
        final String[] params = new String[] { "date" };
        final Object[] values = new Object[] { expiredDate };
        return this.entityDao.findByNamedQueryAndNamedParam("PreAuth.getExpiredPreAuthsForMigratedCustomers", params, values, false);
    }
    
    @Override
    public PreAuthHolding findByApprovedPreAuthIdAuthorizationNumber(final Long preAuthId, final String authorizationNumber) {
        final String[] params = new String[] { "preAuthId", "authorizationNumber" };
        final Object[] values = new Object[] { preAuthId, authorizationNumber };
        final List<PreAuthHolding> list =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuthHolding.findByApprovedPreAuthIdAuthorizationNumber", params, values, false);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    private static List<Integer> convertToIntegerList(final int[] intArray) {
        final List<Integer> list = new ArrayList<Integer>(intArray.length);
        for (int i = 0; i < intArray.length; i++) {
            list.add(i);
        }
        return list;
    }
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public PreAuth findById(final Long id) {
        return this.entityDao.get(PreAuth.class, id);
    }
    
    /**
     * Find PreAuth record by:
     * - pointOfSaleId
     * - cardHash
     * - amount
     * - refId which is ticketNumber
     * 
     * @return PreAuth Return 'null' if data doesn't exist in the database.
     */
    @Override
    public PreAuth getApprovedByCardRetryData(final Integer pointOfSaleId, final String cardHash, final int amount, final int ticketNumber) {
        final String[] params = new String[] { "pointOfSaleId", "cardHash", "amount", "ticketNumber" };
        final Object[] values = new Object[] { pointOfSaleId, cardHash, amount, String.valueOf(ticketNumber) };
        
        final List<PreAuth> list = this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findApprovedByCardRetryData", params, values, false);
        if (list == null || list.isEmpty()) {
            return null;
        }
        // There should not have more than ONE PreAuth record! Throw an exception to force data stored CardRetryTransaction. 
        if (list.size() > 1) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Too many PreAuth records with these criteria - pointOfSaleId: ").append(pointOfSaleId).append(", cardHash: ")
                    .append(cardHash);
            bdr.append(", amount: ").append(amount).append(", ticketNumber: ").append(ticketNumber).append(". Result list size is: ")
                    .append(list.size());
            throw new CardTransactionException(bdr.toString());
        }
        return (PreAuth) list.get(0);
    }
    
    @Override
    public void savePreauth(final PreAuth preauth) {
        this.entityDao.save(preauth);
    }
    
    @Override
    public Collection<PreAuth> findByMerchantAccountId(final int merchantAccountId) {
        
        @SuppressWarnings("unchecked")
        final Collection<PreAuth> preAuths =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findByMerchantAccountId", "merchantAccountId", merchantAccountId);
        if (preAuths == null) {
            return Collections.emptyList();
        } else {
            return preAuths;
        }
        
    }
    
    @Override
    public Collection<PreAuth> findApprovedByMerchantAcccountId(final Integer merchantAccountId) {
        @SuppressWarnings("unchecked")
        final Collection<PreAuth> preAuths =
                this.entityDao.findByNamedQueryAndNamedParam("PreAuth.findApprovedByMerchantAccountId", "merchantAccountId", merchantAccountId);
        if (preAuths == null) {
            return Collections.emptyList();
        } else {
            return preAuths;
        }
    }
}
