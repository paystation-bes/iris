package com.digitalpaytech.service.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.service.TransactionDetailService;

@Service("transactionDetailService")
@Transactional(propagation = Propagation.SUPPORTS)
public class TransactionDetailServiceImpl implements TransactionDetailService {
    
    private static final String SQL_SELECT = "SELECT DISTINCT pur.Id AS PurchaseRandomId, pur.Id AS PermitId, pur.ChargedAmount AS Amount,"
                                             + " CASE WHEN per.PermitOriginalExpireGMT > UTC_TIMESTAMP THEN 1 ELSE 0 END AS isActive,"
                                             + " CASE WHEN pur.TransactionTypeId = 5 THEN 1 ELSE 0 END AS isCancelled,"
                                             + " CASE WHEN pur.TransactionTypeId = 6 THEN 1 ELSE 0 END AS isTest,"
                                             + " pos.Name AS PaystationName, l.Name AS LocationName, pur.PurchaseNumber AS TicketNumber,"
                                             + " per.SpaceNumber AS SpaceNumber,"
                                             + " pur.PurchaseGMT AS PurchaseDateGmt,"
                                             + " per.PermitOriginalExpireGMT AS PermitExpiryDateGmt,"
                                             + " per.PermitOriginalExpireGMT AS ExpiryDate,"
                                             + " pt.Name AS PaymentType, ct.Name AS CardType, proctrans.CardType AS CreditCardType,"
                                             + " CASE WHEN pt.Id IN(2,5,11,12,13,14,15,16,17) THEN 1 ELSE 0 END AS isCreditCard,"
                                             + " proctrans.AuthorizationNumber AS AuthorizationNumber,"
                                             + " CASE WHEN ct.Id = 2 THEN psc.SmartCardData ELSE cc.CardNumber END AS CardNumber,"
                                             + " CASE WHEN pt.Id IN(7,9) THEN 1 ELSE 0 END AS IsPasscard,"
                                             + " proctrans.Last4DigitsOfCardNumber AS Last4DigitsOfCardNumber, cct.Name AS CustomCardType,"
                                             + " c.Coupon AS CouponNumber, c.IsOffline AS IsCouponOffline, lp.Number AS LicencePlateNumber,"
                                             + " mn.Number AS MobileNumber, ea.Email AS EmailAddress"
                                             + " FROM (";    
    
    private static final String SQL_SELECT_PURCHASE = "SELECT Id, ChargedAmount, TransactionTypeId, PurchaseNumber, PurchaseGMT, CustomerId, "
                                                      + "CreatedGMT, PointOfSaleid, LocationId, CouponId, PaymentTypeId "
                                                      + "from Purchase";
    
    private static final String SQL_JOINS = ") pur "
                                           + "LEFT JOIN Permit per ON pur.Id = per.PurchaseId "
                                           + "INNER JOIN PointOfSale pos ON pur.PointOfSaleId = pos.Id "
                                           + "LEFT join ProcessorTransaction proctrans On pur.Id = proctrans.PurchaseId "
                                           + "AND proctrans.IsApproved = 1 "
                                           + "AND proctrans.ProcessorTransactionTypeId NOT IN (0, 4, 5, 18, 20, 22, 99) "
                                           + "LEFT JOIN Location l ON pur.LocationId = l.Id "
                                           + "LEFT JOIN LicencePlate lp ON per.LicencePlateId = lp.Id " 
                                           + "LEFT JOIN Coupon c ON pur.CouponId = c.Id "
                                           + "LEFT JOIN PaymentCard pcard ON pur.Id = pcard.PurchaseId "
                                           + "LEFT JOIN MobileNumber mn ON mn.Id = per.MobileNumberId "
                                           + "LEFT JOIN PurchaseEmailAddress pea ON pea.PurchaseId = pur.Id " 
                                           + "LEFT JOIN EmailAddress ea ON ea.Id = pea.EmailAddressId " 
                                           + "LEFT JOIN PaymentType pt ON pur.PaymentTypeId = pt.Id "
                                           + "LEFT JOIN CardType ct ON pcard.CardTypeId = ct.Id "
                                           + "LEFT JOIN CustomerCard cc ON pcard.CustomerCardId=cc.Id "
                                           + "LEFT JOIN CustomerCardType cct ON cc.CustomerCardTypeId = cct.Id "
                                           + "LEFT JOIN PaymentSmartCard psc ON psc.PurchaseId = pur.Id";
    
    private static final String SQL_WHERE_TRANSACTION_TYPE = " WHERE pur.TransactionTypeId NOT IN (4, 5, 6) ";

    private static final String SQL_WHERE_CUSTOMERID = " WHERE CustomerId = :customerId";
    private static final String SQL_WHERE_LOCATIONID = " AND LocationId = :locationId";
    private static final String SQL_WHERE_LICENCEPLATE = " lp.Number LIKE :licencePlateNumber";
    private static final String SQL_WHERE_SPACENUMBER = " per.SpaceNumber = :spaceNumber";
    private static final String SQL_WHERE_COUPONNUMBER = " c.Coupon LIKE :couponNumber";
    private static final String SQL_WHERE_MOBILENUMBER = " mn.Number LIKE :mobileNumber";
    private static final String SQL_WHERE_EMAILADDRESS = " ea.Email LIKE :emailAddress";
    private static final String SQL_WHERE_PURCHASEDATERANGE = " AND PurchaseGMT BETWEEN :fromDate AND :toDate";    
    private static final String SQL_WHERE_CARDNUMBER = " (proctrans.Last4DigitsOfCardNumber = :cardNumber OR pcard.CardLast4Digits = :cardNumber)";
    private static final String SQL_WHERE_DATAKEY = " pur.CreatedGMT <= :maxLastUpdatedTime";
    
    private static final String SQL_GROUP_BY = " GROUP BY pur.Id";
    private static final String SQL_ORDER_BY = " ORDER BY isActive desc, per.PermitExpireGmt asc, pur.Id asc";
    
    private static final String SQL_WHERE = " WHERE";
    private static final String SQL_AND = " AND";
    
    @Autowired
    private EntityDao entityDao;    
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<TransactionDetail> findTransactionDetailByCriteria(final TransactionReceiptSearchCriteria criteria) {
        
        final Object[] values = new Object[14];
        final String[] parameters = new String[14];
        final StringBuilder stringBuilder = new StringBuilder();        
        stringBuilder.append(SQL_SELECT);        
        final int numberOfParameters = getTransactionDetailSQL(criteria, values, parameters, stringBuilder);        
        stringBuilder.append(SQL_ORDER_BY);
        
        final SQLQuery q = this.entityDao.createSQLQuery(stringBuilder.toString());
        addTransactionInfoSQLScalars(q);
        
        for (int i = 0; i < numberOfParameters; i++) {
            q.setParameter(parameters[i], values[i]);
        }
        
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            q.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            q.setMaxResults(criteria.getItemsPerPage());
        }
        
        return q.list();
    }
    
    private int getTransactionDetailSQL (final TransactionReceiptSearchCriteria criteria, final Object[] values, 
                                        final String[] parameters, final StringBuilder stringBuilder) {
        
        int i = 0;
        stringBuilder.append(SQL_SELECT_PURCHASE);
        values[i] = criteria.getCustomerId();
        parameters[i++] = "customerId";
        stringBuilder.append(SQL_WHERE_CUSTOMERID);
        
        if (criteria.getLocationId() != null) {
            values[i] = criteria.getLocationId();
            parameters[i++] = "locationId";
            stringBuilder.append(SQL_WHERE_LOCATIONID);
        }
        
        if (criteria.getStartDateTime() != null && criteria.getEndDateTime() != null) {
            stringBuilder.append(SQL_WHERE_PURCHASEDATERANGE);
            values[i] = criteria.getStartDateTime();
            parameters[i++] = "fromDate";
            values[i] = criteria.getEndDateTime();
            parameters[i++] = "toDate";
        }
        
        stringBuilder.append(SQL_JOINS);
        
        stringBuilder.append(SQL_WHERE_TRANSACTION_TYPE);
        boolean setWhere = false;
        
        if (criteria.getEmailAddress() != null) {          
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_EMAILADDRESS);
            values[i] = criteria.getEmailAddress();
            parameters[i++] = "emailAddress";
        }
        
        if (criteria.getLicencePlateNumber() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);            
            stringBuilder.append(SQL_WHERE_LICENCEPLATE);
            values[i] = criteria.getLicencePlateNumber();
            parameters[i++] = "licencePlateNumber";
        }
        
        if (criteria.getSpaceNumber() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_SPACENUMBER);
            values[i] = criteria.getSpaceNumber();
            parameters[i++] = "spaceNumber";
        }
        
        if (criteria.getCouponNumber() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_COUPONNUMBER);
            values[i] = criteria.getCouponNumber();
            parameters[i++] = "couponNumber";
        }
        
        if (criteria.getMobileNumber() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_MOBILENUMBER);
            values[i] = criteria.getMobileNumber();
            parameters[i++] = "mobileNumber";
        }
        
        if (criteria.getCardNumber() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_CARDNUMBER);
            final String cardNumber = criteria.getCardNumber();
            values[i] = cardNumber;
            parameters[i++] = "cardNumber";
            values[i] = cardNumber;
            parameters[i++] = "cardNumber";
            values[i] = cardNumber;
            parameters[i++] = "cardNumber";
        }
        
        if (criteria.getMaxLastUpdatedTime() != null) {
            setWhere = setWhereOrAnd(stringBuilder, setWhere);
            stringBuilder.append(SQL_WHERE_DATAKEY);
            values[i] = criteria.getMaxLastUpdatedTime();
            parameters[i++] = "maxLastUpdatedTime";
        }     
        
        return i;
    }
    
    private boolean setWhereOrAnd(final StringBuilder stringBuilder, final boolean setWhere) {
        if (setWhere) {
            stringBuilder.append(SQL_WHERE);            
        } else {
            stringBuilder.append(SQL_AND);
        }
        return false;
    }
       
    private void addTransactionInfoSQLScalars(final SQLQuery q) {
        q.addScalar("PurchaseRandomId", new StringType());
        q.addScalar("isActive", new BooleanType());
        q.addScalar("isCreditCard", new BooleanType());
        q.addScalar("isPasscard", new BooleanType());
        q.addScalar("PaystationName", new StringType());
        q.addScalar("AuthorizationNumber", new StringType());
        q.addScalar("LocationName", new StringType());
        q.addScalar("LicencePlateNumber", new StringType());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("TicketNumber", new IntegerType());
        q.addScalar("PurchaseDateGmt", new TimestampType());
        q.addScalar("PermitExpiryDateGmt", new TimestampType());
        q.addScalar("ExpiryDate", new DateType());
        q.addScalar("PaymentType", new StringType());
        q.addScalar("CardType", new StringType());
        q.addScalar("CreditCardType", new StringType());
        q.addScalar("Last4DigitsOfCardNumber", new StringType());
        q.addScalar("CardNumber", new StringType());
        q.addScalar("CustomCardType", new StringType());
        q.addScalar("IsCouponOffline", new BooleanType());
        q.addScalar("CouponNumber", new StringType());
        q.addScalar("MobileNumber", new StringType());
        q.addScalar("EmailAddress", new StringType());
        q.addScalar("Amount", new FloatType());
        q.addScalar("IsCancelled", new BooleanType());
        q.addScalar("IsTest", new BooleanType());
        q.addScalar("PermitId", new LongType());
        q.setResultTransformer(Transformers.aliasToBean(TransactionDetail.class));
    }
}
