package com.digitalpaytech.service.impl;

import java.util.List;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import org.apache.log4j.Logger;

import com.digitalpaytech.service.TransactionFileUploadService;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.TransactionFileUpload;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;

@Service("transactionFileUploadService")
public class TransactionFileUploadServiceImpl implements TransactionFileUploadService {
    private static final Logger LOG = Logger.getLogger(TransactionFileUploadServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Override
    public final TransactionFileUpload findTransactionFileUpload(final Customer customer, final String fileName, final String checksum) {
        final String[] params = new String[] { "customerId", "fileName", "checksum" };
        final Object[] values = new Object[] { customer.getId(), fileName, checksum };
        final List<TransactionFileUpload> list =
                this.entityDao.findByNamedQueryAndNamedParam("TransactionFileUpload.findByCustomerIdFileNameAndChecksum", params, values);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void saveTransactionFileUpload(final Customer customer, final String fullPathFileName) {
        String checksum = null;
        try {
            checksum = CardProcessingUtil.createMD5Checksum(fullPathFileName);
        } catch (IOException e) {
            LOG.error(fullPathFileName + " causes IO Exception.", e);
        } catch (NoSuchAlgorithmException nsae) {
            LOG.error("MD5 cannot be recognized as a MessageDigest algorithm.", nsae);
        }
        if (checksum == null) {
            int custId = 0;
            if (customer != null) {
                custId = customer.getId().intValue();
            }
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Cannot create checksum value for customer id: ").append(custId).append(", fullPathFileName: ");
            bdr.append(fullPathFileName).append(". Store a blank to database instead.");
            LOG.error(bdr.toString());
            
            checksum = "";
        }
        final Date currentGmt = DateUtil.getCurrentGmtDate();
        this.entityDao.save(new TransactionFileUpload(customer, fullPathFileName, checksum, currentGmt, StandardConstants.CONSTANT_1));
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateTransactionFileUpload(final TransactionFileUpload transactionFileUpload) {
        this.entityDao.update(transactionFileUpload);
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
