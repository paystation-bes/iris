package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReplenishType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ReplenishTypeService;

@Service("replenishTypeService")
public class ReplenishTypeServiceImpl implements ReplenishTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<ReplenishType> replenishTypes;
	private Map<Integer, ReplenishType> replenishTypesMap;
	private Map<String, ReplenishType> replenishTypeNamesMap;
		
	
	@Override
	public List<ReplenishType> loadAll() {
		if (replenishTypes == null) {
			replenishTypes = entityService.loadAll(ReplenishType.class);
		}
		return replenishTypes;
	}

	@Override
	public Map<Integer, ReplenishType> getReplenishTypesMap() {
		if (replenishTypes == null) {
			loadAll();
		}
		if (replenishTypesMap == null) {
			replenishTypesMap = new HashMap<Integer, ReplenishType>(replenishTypes.size());
			Iterator<ReplenishType> iter = replenishTypes.iterator();
			while (iter.hasNext()) {
				ReplenishType replenishType = iter.next();
				replenishTypesMap.put(replenishType.getId(), replenishType);
			}
		}
		return replenishTypesMap;
	}

	@Override
	public ReplenishType getReplenishType(String name) {
        if (replenishTypes == null) {
            loadAll();
        }
        if (replenishTypeNamesMap == null) {
            replenishTypeNamesMap = new HashMap<String, ReplenishType>(replenishTypes.size());
            Iterator<ReplenishType> iter = replenishTypes.iterator();
            while (iter.hasNext()) {
                ReplenishType replenishType = iter.next();
                replenishTypeNamesMap.put(replenishType.getName().toLowerCase(), replenishType);
            }
        }
        return replenishTypeNamesMap.get(name.toLowerCase());
	}
	
	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	@Override
	public String getText(int transactionId) {
		if (replenishTypesMap == null)
			getReplenishTypesMap();
		
		if (!replenishTypesMap.containsKey(transactionId))
			return null;
		else
			return replenishTypesMap.get(transactionId).getName();
	}
}
