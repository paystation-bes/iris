package com.digitalpaytech.service.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.util.DateUtil;

@Service("permitService")
@Transactional(propagation = Propagation.REQUIRED)
public class PermitServiceImpl implements PermitService {
    private static final String SQL_GROUP_BY_STALL = " GROUP BY p.SpaceNumber ORDER BY p.SpaceNumber asc";
    
    private static final String SQL_GET_VALID_FOR_SPECIFIED_DAY__PURCHASE_EXPIRY_STALL = "SELECT p.SpaceNumber AS SpaceNumber, MAX(p.PermitBeginGmt) AS PermitBeginGmt, MAX(p.PermitExpireGmt) AS PermitExpireGmt "
                                                                                         + "FROM Permit p INNER JOIN Purchase pur ON p.PurchaseId = pur.Id "
                                                                                         + "WHERE pur.PointOfSaleId IN (:posList) AND p.SpaceNumber BETWEEN :startSpace AND :endSpace AND p.PermitBeginGMT BETWEEN (DATE_ADD(:specifiedPosDate, INTERVAL -14 DAY) AND :specifiedPosDate) AND pur.TransactionTypeId!=6";
    
    private static final String SQL_GET_VALID_FOR_SPECIFIED_DAY_STALLINFO_BY_REGION = "SELECT p.SpaceNumber AS SpaceNumber, MAX(p.PermitBeginGmt) AS PermitBeginGmt, MAX(p.PermitExpireGmt) AS PermitExpireGmt "
                                                                                      + "FROM Permit p INNER JOIN Purchase pur ON p.PurchaseId = pur.Id "
                                                                                      + "WHERE p.LocationId = :locationId AND p.SpaceNumber BETWEEN :startSpace AND :endSpace "
                                                                                      + "AND p.PermitBeginGMT BETWEEN (DATE_ADD(:specifiedPosDate, INTERVAL -14 DAY) AND :specifiedPosDate) AND pur.TransactionTypeId!=6";
    
    private static final String SQL_STALL_STATUS_SELECT = "SELECT p.SpaceNumber AS SpaceNumber, ls.Name AS PaystationSettingName, MAX(p.PermitBeginGmt) as PermitBeginGmt, MAX(p.PermitExpireGmt) as PermitExpireGmt";
    private static final String SQL_STALL_STATUS_FROM = " FROM Permit p LEFT JOIN Purchase pur ON p.PurchaseId = pur.Id LEFT JOIN PaystationSetting ls ON pur.PaystationSettingId = ls.Id";
    private static final String SQL_STALL_STATUS_WHERE = " WHERE p.SpaceNumber BETWEEN :startSpace AND :endSpace AND p.PermitBeginGMT BETWEEN (DATE_ADD(:specifiedPosDate, INTERVAL -14 DAY) AND :specifiedPosDate) AND pur.TransactionTypeId!=6";
    private static final String SQL_STALL_STATUS_WHERE_ADD_CUSTOMER = " AND pur.CustomerId = :customerId";
    private static final String SQL_STALL_STATUS_WHERE_ADD_PAYSTATIONSETTINGNAME = " AND ls.Name = :paystationSettingName";
    private static final String SQL_STALL_STATUS_WHERE_ADD_LOCATION = " AND p.LocationId = :locationId";
    private static final String SQL_STALL_STATUS_WHERE_ADD_POSLIST = " AND pur.PointOfSaleId IN (:posList)";
    
    @Autowired
    private EntityDao entityDao;
    
    public void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final Permit findLatestExpiredPermitByPurchaseId(final Long purchaseId) {
        final List<Permit> result = this.entityDao.findByNamedQueryAndNamedParam("Permit.findLatestExpiredPermitByPurchaseId", "purchaseId",
                                                                                 purchaseId);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public final List<ActivePermit> getStallStatusValidForPurchaseExpiryStallBySpecifiedPosDate(final List<PointOfSale> posList,
        final int startSpace, final int endSpace, final Date specifiedPosDate, final int stallType) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_VALID_FOR_SPECIFIED_DAY__PURCHASE_EXPIRY_STALL + SQL_GROUP_BY_STALL);
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameterList("posList", posList);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    public final List<ActivePermit> getStallStatusByLocationBySpecifiedPosDate(final int locationId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_GET_VALID_FOR_SPECIFIED_DAY_STALLINFO_BY_REGION + SQL_GROUP_BY_STALL);
        
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("locationId", locationId);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByCustomerAndSpaceRange(final int customerId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        final StringBuilder query = new StringBuilder(SQL_STALL_STATUS_SELECT);
        query.append(SQL_STALL_STATUS_FROM);
        query.append(SQL_STALL_STATUS_WHERE);
        query.append(SQL_STALL_STATUS_WHERE_ADD_CUSTOMER);
        query.append(SQL_GROUP_BY_STALL);
        final SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("customerId", customerId);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByLocationAndSpaceRange(final int locationId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        final StringBuilder query = new StringBuilder(SQL_STALL_STATUS_SELECT);
        query.append(SQL_STALL_STATUS_FROM);
        query.append(SQL_STALL_STATUS_WHERE);
        query.append(SQL_STALL_STATUS_WHERE_ADD_LOCATION);
        query.append(SQL_GROUP_BY_STALL);
        final SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("locationId", locationId);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(final String paystationSettingName, final int startSpace,
        final int endSpace, final Date specifiedPosDate) {
        final StringBuilder query = new StringBuilder(SQL_STALL_STATUS_SELECT);
        query.append(SQL_STALL_STATUS_FROM);
        query.append(SQL_STALL_STATUS_WHERE);
        query.append(SQL_STALL_STATUS_WHERE_ADD_PAYSTATIONSETTINGNAME);
        query.append(SQL_GROUP_BY_STALL);
        final SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameter("paystationSettingName", paystationSettingName);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPosListAndSpaceRange(final List<PointOfSale> posList, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        final StringBuilder query = new StringBuilder(SQL_STALL_STATUS_SELECT);
        query.append(SQL_STALL_STATUS_FROM);
        query.append(SQL_STALL_STATUS_WHERE);
        query.append(SQL_STALL_STATUS_WHERE_ADD_POSLIST);
        query.append(SQL_GROUP_BY_STALL);
        final SQLQuery q = this.entityDao.createSQLQuery(query.toString());
        q.addScalar("SpaceNumber", new IntegerType());
        q.addScalar("PaystationSettingName", new StringType());
        q.addScalar("PermitBeginGmt", new TimestampType());
        q.addScalar("PermitExpireGmt", new TimestampType());
        q.setParameterList("posList", posList);
        q.setParameter("startSpace", startSpace);
        q.setParameter("endSpace", endSpace);
        q.setParameter("specifiedPosDate", specifiedPosDate);
        q.setResultTransformer(Transformers.aliasToBean(ActivePermit.class));
        return q.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Permit findPermitByIdForSms(final Long id) {
        return (Permit) this.entityDao.get(Permit.class, id);
    }
    
    @Override
    public final void saveExtendedPermit(final Permit originalPermit, final Permit permit) {
        this.entityDao.evict(originalPermit);
        originalPermit.setPermitExpireGmt(permit.getPermitExpireGmt());
        originalPermit.setNumberOfExtensions(originalPermit.getNumberOfExtensions() + 1);
        this.entityDao.update(originalPermit);
        
        final Purchase newPurchase = permit.getPurchase();
        this.entityDao.save(newPurchase);
        this.entityDao.saveOrUpdate(permit);
        
        final ActivePermit activePermit = (ActivePermit) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("ActivePermit.findActivePermitByOriginalPermitId", "originalPermitId", originalPermit.getId());
        this.entityDao.evict(activePermit);
        activePermit.setPermitExpireGmt(permit.getPermitExpireGmt());
        activePermit.setNumberOfExtensions((byte) (activePermit.getNumberOfExtensions() + 1));
        activePermit.setLastModifiedGmt(DateUtil.getCurrentGmtDate());      
        this.entityDao.update(activePermit);
    }
    
    @Override
    public final Permit findPermitByPurchaseId(final Long purchaseId) {
        return (Permit) this.entityDao.findUniqueByNamedQueryAndNamedParam("Permit.findPermitByPurchaseId", "purchaseId", purchaseId, true);
    }
    
    @Override
    public final List<Permit> findPermitByPermitNumber(final int permitNumber) {
        return this.entityDao.findByNamedQueryAndNamedParam("Permit.findPermitByPurchaseId", "permitNumber", permitNumber, true);
    }
    
    @Override
    public final Permit findLatestOriginalPermitByPointOfSaleIdAndPermitNumber(final int pointOfSaleId, final int permitNumber) {
        final Permit result = (Permit) this.entityDao.getNamedQuery("Permit.findOriginalPermitsByPointOfSaleIdAndPermitNumber")
                .setParameter("pointOfSaleId", pointOfSaleId).setParameter("permitNumber", permitNumber).setMaxResults(1).uniqueResult();
        return result;
    }
    
    @Override
    public final Permit findLatestOriginalPermit(final int locationId, final int spaceNumber, final int addTimeNumber, final String licencePlateNumber) {
        final Permit result = (Permit) this.entityDao.getNamedQuery("ActivePermit.findOriginalPermit").setParameter("locationId", locationId)
                .setParameter("spaceNumber", spaceNumber).setParameter("licencePlateNumber", licencePlateNumber)
                .setParameter("addTimeNumber", addTimeNumber).setMaxResults(1).uniqueResult();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<Permit> findExtentionsByOriginalPermitId(final Long originalPermitId) {
        return this.entityDao.findByNamedQueryAndNamedParam("Permit.findExtentionPermitsByOriginalPermitId", "originalPermitId", originalPermitId,
                                                            true);
    }
    
}
