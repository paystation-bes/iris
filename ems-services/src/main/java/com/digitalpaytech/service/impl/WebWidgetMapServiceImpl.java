package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

@Component("webWidgetMapService")
@Transactional(propagation = Propagation.REQUIRED)
public class WebWidgetMapServiceImpl implements WebWidgetService {
    
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private RouteService routeService;
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
    
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
    
    public void setRouteService(RouteService routeService) {
        this.routeService = routeService;
    }
    
    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetData getWidgetData(Widget widget, UserAccount userAccount) {
        WidgetMapInfo widgetMapInfo = new WidgetMapInfo();
        
        int unplacedCount = -1;
        List<MapEntry> mapEntries = null;
        
        int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
        int tier2Id = widget.getWidgetTierTypeByWidgetTier2Type().getId();
        int customerId = userAccount.getCustomer().getId();
        boolean checkTier = widget.isIsSubsetTier1();
        
        if (tier2Id != WidgetConstants.TIER_TYPE_NA) {
            tier1Id = tier2Id;
            List<WidgetSubsetMember> subsetMembers = widget.getWidgetSubsetMembers();
            checkTier = widget.isIsSubsetTier2();
            boolean isChildFound = false;
            
            for (WidgetSubsetMember subsetMember : subsetMembers) {
                if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_ORG)) {
                    customerId = subsetMember.getMemberId();
                    isChildFound = true;
                    break;
                }
            }
            if (!isChildFound) {
                List<Customer> childCustomers = dashboardService.findAllChildCustomers(customerId);
                for (Customer customer : childCustomers) {
                    customerId = customer.getId();
                }
            }
        }
        
        if (checkTier) {
            List<WidgetSubsetMember> subsetMembers = widget.getWidgetSubsetMembers();
            switch (tier1Id) {
                case WidgetConstants.TIER_TYPE_ORG:
                    /* Organization */
                    List<Integer> organizationSubsetIds = new ArrayList<Integer>();
                    for (WidgetSubsetMember subsetMember : subsetMembers) {
                        if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_ORG)) {
                            organizationSubsetIds.add(subsetMember.getMemberId());
                        }
                    }
                    
                    mapEntries = pointOfSaleService.findMapEntriesByCustomerIds(organizationSubsetIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByCustomerIds(organizationSubsetIds);
                    break;
                case WidgetConstants.TIER_TYPE_LOCATION:
                    /* Location */
                    List<Integer> locationSubsetIds = new ArrayList<Integer>();
                    for (WidgetSubsetMember subsetMember : subsetMembers) {
                        if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_LOCATION)) {
                            locationSubsetIds.add(subsetMember.getMemberId());
                        }
                    }
                    mapEntries = pointOfSaleService.findMapEntriesByLocationIds(locationSubsetIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByLocationIds(locationSubsetIds);
                    break;
                case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                    /* Location */
                    List<Integer> parentLocationSubsetIds = new ArrayList<Integer>();
                    for (WidgetSubsetMember subsetMember : subsetMembers) {
                        if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_PARENT_LOCATION)) {
                            List<Location> locationList = this.locationService.getLocationsByParentLocationId(subsetMember.getMemberId());
                            if (locationList != null) {
                                for (Location location : locationList) {
                                    parentLocationSubsetIds.add(location.getId());
                                }
                            }
                        }
                    }
                    mapEntries = pointOfSaleService.findMapEntriesByLocationIds(parentLocationSubsetIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByLocationIds(parentLocationSubsetIds);
                    break;
                case WidgetConstants.TIER_TYPE_ROUTE:
                    /* Route */
                    List<Integer> routeSubsetIds = new ArrayList<Integer>();
                    for (WidgetSubsetMember subsetMember : subsetMembers) {
                        if (subsetMember.getWidgetTierType().getId().equals(WidgetConstants.TIER_TYPE_ROUTE)) {
                            routeSubsetIds.add(subsetMember.getMemberId());
                        }
                    }
                    mapEntries = pointOfSaleService.findMapEntriesByRouteIds(routeSubsetIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByRouteIds(routeSubsetIds);
                    break;
            }
        } else {
            // Nothing selected show all for customer
            switch (tier1Id) {
                case WidgetConstants.TIER_TYPE_ORG:
                    /* Organization */
                    List<Integer> customerIds = new ArrayList<Integer>();
                    if (userAccount.getCustomer().isIsParent()) {
                        List<Customer> childCustomers = dashboardService.findAllChildCustomers(customerId);
                        for (Customer customer : childCustomers) {
                            customerIds.add(customer.getId());
                        }
                    } else {
                        customerIds.add(customerId);
                    }
                    if (!customerIds.isEmpty()) {
                        mapEntries = pointOfSaleService.findMapEntriesByCustomerIds(customerIds);
                        unplacedCount = pointOfSaleService.countUnPlacedByCustomerIds(customerIds);
                    }
                    break;
                case WidgetConstants.TIER_TYPE_LOCATION:
                    /* Location */
                    List<Location> childLocationList = this.locationService.findChildLocationsByCustomerId(customerId);
                    List<Integer> childLocationIds = new ArrayList<Integer>(childLocationList.size());
                    for (Location location : childLocationList) {
                        if (!location.getIsUnassigned()) {
                            childLocationIds.add(location.getId());
                        }
                    }
                    if (!childLocationIds.isEmpty()) {
                        mapEntries = pointOfSaleService.findMapEntriesByLocationIds(childLocationIds);
                        unplacedCount = pointOfSaleService.countUnPlacedByLocationIds(childLocationIds);
                    }
                    break;
                case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
                    /* Parent Location */
                    List<Location> parentLocationList = this.locationService.findParentLocationsByCustomerId(customerId);
                    List<Integer> parentLocationIds = new ArrayList<Integer>();
                    for (Location parentLocation : parentLocationList) {
                        List<Location> childUnderParentList = this.locationService.getLocationsByParentLocationId(parentLocation.getId());
                        for (Location location : childUnderParentList) {
                            parentLocationIds.add(location.getId());
                        }
                    }
                    mapEntries = pointOfSaleService.findMapEntriesByLocationIds(parentLocationIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByLocationIds(parentLocationIds);
                    break;
                case WidgetConstants.TIER_TYPE_ROUTE:
                    /* Route */
                    Collection<Route> routeList = this.routeService.findRoutesByCustomerId(customerId);
                    List<Integer> routeIds = new ArrayList<Integer>(routeList.size());
                    for (Route route : routeList) {
                        routeIds.add(route.getId());
                    }
                    mapEntries = pointOfSaleService.findMapEntriesByRouteIds(routeIds);
                    unplacedCount = pointOfSaleService.countUnPlacedByRouteIds(routeIds);
                    break;
            }
        }
        
        if (unplacedCount > 0) {
            widgetMapInfo.setMissingPayStation(true);
        }
        
        widgetMapInfo.setMapInfo(mapEntries);
        
        //        String customerTimeZone = EmsPropertiesService.CUSTOMER_DEFAULT_TIMEZONE_VALUE;//= WebSecurityUtil.getCustomerTimeZone();
        //        
        //        List<CustomerProperty> customerProperties = customerAdminService.getCustomerPropertyByCustomerId(userAccount.getCustomer().getId());
        //        for (CustomerProperty customerProperty : customerProperties) {
        //            if (Integer.valueOf(customerProperty.getCustomerPropertyType().getId()).equals(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)) {
        //                customerTimeZone = customerProperty.getPropertyValue();
        //            }
        //        }
        //        
        //        ArrayList<Integer> pointOfSaleIds = new ArrayList<Integer>(mapEntries.size());
        //        for (MapEntry entry : mapEntries) {
        //          pointOfSaleIds.add((Integer) entry.getRandomId().getId());
        //        }
        //        
        //        Map<Integer, MapEntry> entriesMap = this.pointOfSaleService.findPointOfSaleMapEntryDetails(pointOfSaleIds, TimeZone.getTimeZone(customerTimeZone));
        //        for(Integer posId : entriesMap.keySet()) {
        //          MapEntry entry = entriesMap.get(posId);
        //          if(entry.getPayStationType() != WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL) {
        //              widgetMapInfo.addMapEntry(entriesMap.get(posId));
        //          }
        //        }
        
        WidgetData widgetData = new WidgetData();
        widgetData.setWidgetMapInfo(widgetMapInfo);
        widgetData.setIsZeroValues(false);
        widgetData.setWidgetName(widget.getName());
        widgetData.setChartType(widget.getWidgetChartType().getId());
        widgetData.setRangeTypeId(widget.getWidgetRangeType().getId());
        widgetData.setMetricTypeId(widget.getWidgetMetricType().getId());
        return widgetData;
    }
}
