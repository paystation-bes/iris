package com.digitalpaytech.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PurchaseTax;
import com.digitalpaytech.service.PurchaseTaxService;

@Service("purchaseTaxService")
@Transactional(propagation = Propagation.REQUIRED)
public class PurchaseTaxServiceImpl implements PurchaseTaxService {
    
    @Autowired
    private EntityDao entityDao;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<PurchaseTax> findPurchaseTaxByPurchaseId(Long purchaseId) {
        List<PurchaseTax> purchaseTaxList = entityDao.findByNamedQueryAndNamedParam("PurchaseTax.findPurchaseTaxByPurchaseId", "purchaseId", purchaseId);
        return purchaseTaxList;
    }
}
