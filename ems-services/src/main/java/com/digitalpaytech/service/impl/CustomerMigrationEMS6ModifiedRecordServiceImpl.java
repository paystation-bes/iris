package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoIRIS;
import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.service.CustomerMigrationEMS6ModifiedRecordService;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
@Service("customerMigrationEMS6ModifiedRecordService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerMigrationEMS6ModifiedRecordServiceImpl implements CustomerMigrationEMS6ModifiedRecordService {
    
    @Autowired
    private EntityDaoIRIS entityDaoIRIS;
    
    public void setEntityDaoIRIS(EntityDaoIRIS entityDaoIRIS) {
        this.entityDaoIRIS = entityDaoIRIS;
    }
    
    @Override
    public void saveCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        this.entityDaoIRIS.save(customerMigrationEMS6ModifiedRecord);
    }
    
    @Override
    public void updateCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        this.entityDaoIRIS.update(customerMigrationEMS6ModifiedRecord);
    }

    @Override
    public void deleteCustomerMigrationEMS6ModifiedRecordService(CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        this.entityDaoIRIS.delete(customerMigrationEMS6ModifiedRecord);
    }

    @Override
    public List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerId(Integer customerId) {
        return this.entityDaoIRIS.findByNamedQueryAndNamedParam("CustomerMigrationEMS6ModifiedRecord.findAllByCustomerId", "customerId", customerId);
    }

    @Override
    public List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerIdAndTableType(Integer customerId, String tableName) {
        String[] parameters = new String[] {"customerId", "tableName"};
        Object[] values = new Object[] {customerId, tableName};
        return this.entityDaoIRIS.findByNamedQueryAndNamedParam("CustomerMigrationEMS6ModifiedRecord.findAllByCustomerIdAndTableName", parameters, values, false);
    }
    

    @Override
    public CustomerMigrationEMS6ModifiedRecord findModifiedRecordIfExists(Integer customerId, Integer tableId, String tableName, String columnName) {
        String[] parameters = new String[] {"customerId", "tableId", "tableName", "columnName"};
        Object[] values = new Object[] {customerId, tableId, tableName, columnName};
        return (CustomerMigrationEMS6ModifiedRecord) this.entityDaoIRIS.findUniqueByNamedQueryAndNamedParam("CustomerMigrationEMS6ModifiedRecord.findExistingRecordIfExists", parameters, values, false);
    }

}
