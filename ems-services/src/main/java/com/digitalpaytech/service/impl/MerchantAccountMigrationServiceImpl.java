package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.MerchantAccountMigrationStatus;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.service.MerchantAccountMigrationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.ReversalService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("merchantAccountMigrationServiceImpl")
public class MerchantAccountMigrationServiceImpl implements MerchantAccountMigrationService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private ReversalService reversalService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Override
    
    public void save(final MerchantAccountMigration merchantAccountMigration) {
        this.entityDao.save(merchantAccountMigration);
    }
    
    @Override
    public void delete(final MerchantAccountMigration merchantAccountMigration) {
        this.entityDao.delete(merchantAccountMigration);
    }
    
    @Override
    public MerchantAccountMigration findById(final Integer id) {
        return (MerchantAccountMigration) this.entityDao.findUniqueByNamedQueryAndNamedParam("MerchantAccountMigration.findById", "id", id);
    }
    
    @Override
    public MerchantAccountMigration findByMerchantAccountId(final Integer merchantAccountId) {
        return (MerchantAccountMigration) this.entityDao.findUniqueByNamedQueryAndNamedParam("MerchantAccountMigration.findByMerchantAccountId",
                                                                                             "merchantAccountId", merchantAccountId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<MerchantAccountMigration> findByCustomerId(final Integer customerId) {
        return (List<MerchantAccountMigration>) this.entityDao.findByNamedQueryAndNamedParam("MerchantAccountMigration.findByCustomerId",
                                                                                             "customerId", customerId);
    }

    @Override
    public Collection<MerchantAccountMigration> findByDelayUntilGMTPastAndPending() {
        @SuppressWarnings("unchecked")
        final Collection<MerchantAccountMigration> migrations =
                this.entityDao.findByNamedQueryAndNamedParam("MerchantAccountMigration.findDelayUntilGMTPastAndPending",
                                                             new String[] { "currentTime", "pendingStatusId" }, new Object[] { new Date(),
                                                                 MerchantAccountMigrationStatus.PENDING });
        if (migrations == null) {
            return Collections.emptyList();
        } else {
            return migrations;
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void disableMerchantAccountAndStartsMigration(final MerchantAccountMigration merchantAccountMigration) {
        
        final MerchantAccount merchantAccount = merchantAccountMigration.getMerchantAccount();
        
        final MerchantStatusType merchantStatusType = new MerchantStatusType();
        merchantStatusType.setId(WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID);
        merchantAccount.setMerchantStatusType(merchantStatusType);
        
        this.merchantAccountService.saveOrUpdateMerchantAccount(merchantAccount);
        
        this.save(merchantAccountMigration);
    }
    
    @Override
    public boolean preAuthBlockMigration(final MerchantAccount ma) {
        return !this.preAuthService.findApprovedByMerchantAcccountId(ma.getId()).isEmpty();
    }
    
    @Override
    public boolean reversalsBlockMigraion(final MerchantAccount ma) {
        return !this.reversalService.getIncompleteReversalsForReversal(ma).isEmpty();
    }
    
    @Override
    public boolean postAuthBlockMigration(final MerchantAccount ma) {
        return !this.processorTransactionService
                .findByMercAccIdAndProcTransTypeIdAndHasPurchase(ma.getId(), CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_AUTHORIZED).isEmpty();
    }
}
