package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.SubscriptionTypeService;

@Service("subscriptionTypeService")
@Transactional(propagation = Propagation.SUPPORTS)
public class SubscriptionTypeServiceImpl implements SubscriptionTypeService {
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EntityService entityService;
    
    public final List<SubscriptionType> loadAll() {
        return this.entityDao.findByNamedQuery("SubscriptionType.loadAll");
    }
    
    @Override
    public final SubscriptionType findById(final Integer id) {
        // TODO Auto-generated method stub
        return (SubscriptionType) this.entityDao.findUniqueByNamedQueryAndNamedParam("SubscriptionType.findById", new String[] { "id" },
                                                                                     new Object[] { id }, true);
    }
}
