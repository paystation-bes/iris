package com.digitalpaytech.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.PosTelemetry;
import com.digitalpaytech.domain.TelemetryType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.TelemetryKeyValueTransformer;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.OutOfOrderException;
import com.digitalpaytech.service.paystation.PosTelemetryService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.KeyValuePair;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posTelemetryService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PosTelemetryServiceImpl implements PosTelemetryService {
    
    private static final String TELEMETRY_LABEL = "label.telemetry";
    private static final String RESET = "Reset";
    private static final String UPDATE = "Update";
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MessageHelper message;
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<KeyValuePair<String, String>> findActiveByPosId(final Integer pointOfSaleId, final boolean includePrivate) {
        final TelemetryKeyValueTransformer transformer = new TelemetryKeyValueTransformer(this.message);
        
        final Query query;
        if (includePrivate) {
            query = this.entityDao.getNamedQuery("PosTelemetry.findAllActiveByPointOfSaleId");
        } else {
            query = this.entityDao.getNamedQuery("PosTelemetry.findPubliclyActiveByPointOfSaleId");
        }
        
        return query.setParameter(HibernateConstants.POINTOFSALE_ID, pointOfSaleId).setResultTransformer(transformer).list();
    }
    
    @Override
    public final void processPosTelemetry(final PointOfSale pointOfSale, final String emvTimeStamp, final Map<String, String> telemetryData,
        final String type, final Integer referenceCounter) throws InvalidDataException, OutOfOrderException {
        
        final Date now = new Date();
        final ReferenceStatus referenceStatus = validateReferenceCounter(type, pointOfSale, referenceCounter, now);
        if (referenceStatus.getIsReset()) {
            // Clear all the old fields
            cleanupOldPosTelemetry(pointOfSale.getId(), now);
        }
        
        final Date timeStamp = DateUtil.parse(emvTimeStamp, DateUtil.COLON_DELIMITED_DATE_FORMAT, WebCoreConstants.GMT);
        
        final Map<String, PosTelemetry> newEntries = new HashMap<String, PosTelemetry>(telemetryData.size() * 2);
        final Map<String, TelemetryType> allTelemetryTypes = findAllLabels();
        for (Entry<String, String> entry : telemetryData.entrySet()) {
            final String key = entry.getKey().trim();
            final String val = entry.getValue();
            if (!StringUtils.isEmpty(key) || !StringUtils.isEmpty(val)) {
                TelemetryType telemetryType = allTelemetryTypes.get(key);
                if (!allTelemetryTypes.keySet().contains(key)) {
                    if (telemetryType == null) {
                        telemetryType = new TelemetryType();
                        telemetryType.setCreatedGMT(now);
                        telemetryType.setIsPrivate(true);
                        telemetryType.setName(key);
                        telemetryType.setLabel(convertNameToLabel(key));
                    
                        this.entityDao.save(telemetryType);
                    }
                }
                
                final PosTelemetry inMemRecord = newEntries.get(key);
                if (inMemRecord != null) {
                    inMemRecord.setArchivedGMT(now);
                    inMemRecord.setIsHistory(true);
                } else if (!referenceStatus.getIsReset()) {
                    final List<PosTelemetry> existingPosTelemetry = this
                            .findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType(pointOfSale.getId(), telemetryType.getId());
                    if (!existingPosTelemetry.isEmpty() && !referenceStatus.getIsOutOfOrder()) {
                        for (PosTelemetry pt : existingPosTelemetry) {
                            pt.setArchivedGMT(now);
                            pt.setIsHistory(true);
                            this.entityDao.update(pt);
                        }
                    }
                }
                
                final PosTelemetry posTelemetry = new PosTelemetry();
                posTelemetry.setCreatedGMT(now);
                if (referenceStatus.getIsOutOfOrder()) {
                    posTelemetry.setArchivedGMT(now);
                    posTelemetry.setIsHistory(true);  
                } else {
                    posTelemetry.setArchivedGMT(null);
                    posTelemetry.setIsHistory(false);
                }
                posTelemetry.setTimestampGMT(timeStamp);
                posTelemetry.setPointOfSale(pointOfSale);
                posTelemetry.setValue(val);
                posTelemetry.setTelemetryType(telemetryType);
                newEntries.put(key, posTelemetry);
                this.entityDao.save(posTelemetry);
                
            }
        }
        
        if (referenceStatus.getIsOutOfOrder()) {
            throw new OutOfOrderException("EMV Telemetry: Missing a previous Reset or Update message");
        }
    }
    
    private ReferenceStatus validateReferenceCounter(final String type, final PointOfSale pointOfSale, final int referenceCounter, final Date now)
        throws InvalidDataException {
        final ReferenceStatus reset = new ReferenceStatus(false, false);
        final PosStatus posStatus = this.entityDao.get(PosStatus.class, pointOfSale.getId());
        if (RESET.equals(type)) {
            // Always update reference counter on 'Reset'
            posStatus.setTelemetryReferenceCounter(referenceCounter);
            reset.setIsReset(true);
        } else if (UPDATE.equals(type)) {
            final Integer current = posStatus.getTelemetryReferenceCounter();
            if (current == null || current != (referenceCounter - 1)) {
                // Error, 'Reset' was missed
                reset.setIsOutOfOrder(true);
            } else {
                posStatus.setTelemetryReferenceCounter(referenceCounter);
            }
        } else {
            throw new InvalidDataException("Invalid EMV Telemetry message type: " + type);
        }
        
        return reset;
    }
    
    
    private void cleanupOldPosTelemetry(final Integer pointOfSaleId, final Date now) {
        final List<PosTelemetry> allCurrentTelemetryForPointOfSale = this.findAllCurrentTelemetryForPointOfSaleByPointOfSaleId(pointOfSaleId);
        for (PosTelemetry posTelemetry : allCurrentTelemetryForPointOfSale) {
            posTelemetry.setArchivedGMT(now);
            posTelemetry.setIsHistory(true);
            this.entityDao.update(posTelemetry);
        }
    }
    
    private String convertNameToLabel(final String name) {
        return TELEMETRY_LABEL.concat(".").concat(name.replaceAll("\\s", "")).toLowerCase();
    }
    
    private Map<String, TelemetryType> findAllLabels() {
        @SuppressWarnings("unchecked")
        final List<TelemetryType> allTelemetryTypes = (ArrayList<TelemetryType>) this.entityDao
                .findByNamedQuery("TelemetryType.findAllTelemetryTypes");
        
        final Map<String, TelemetryType> result = new HashMap<String, TelemetryType>(allTelemetryTypes.size());
        for (TelemetryType telemetryType : allTelemetryTypes) {
            result.put(telemetryType.getName().toLowerCase(), telemetryType);
        }
        
        return result;
    }
    
    @Override
    public final TelemetryType findTelemetryTypeByName(final String telemetryTypeName) {
        return (TelemetryType) this.entityDao.findByNamedQueryAndNamedParam("TelemetryType.findTelemetryTypeByName", "telemetryTypeName",
                                                                            telemetryTypeName);
    }
    
    @Override
    public final TelemetryType findTelemetryTypeByLabel(final String telemetryTypeLabel) {
        return (TelemetryType) this.entityDao.getNamedQuery("TelemetryType.findTelemetryTypeByLabel")
                .setParameter("telemetryTypeLabel", telemetryTypeLabel).setMaxResults(1).uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PosTelemetry> findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType(final Integer pointOfSaleId, final Integer telemetryTypeId) {
        return (List<PosTelemetry>) this.entityDao.getNamedQuery("PosTelemetry.findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType")
                .setParameter(HibernateConstants.POINTOFSALE_ID, pointOfSaleId).setParameter("telemetryTypeId", telemetryTypeId).list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final List<PosTelemetry> findAllCurrentTelemetryForPointOfSaleByPointOfSaleId(final Integer pointOfSaleId) {
        return (List<PosTelemetry>) this.entityDao.findByNamedQueryAndNamedParam("PosTelemetry.findAllCurrentTelemetryForPointOfSaleByPointOfSaleId",
                                                                                 HibernateConstants.POINTOFSALE_ID, pointOfSaleId);
    }
    
    private final class ReferenceStatus {
        
        private boolean isReset;
        private boolean isOutOfOrder;
        
        private ReferenceStatus(final boolean isReset, final boolean isOutOfOrder) {
            this.isReset = isReset;
            this.isOutOfOrder = isOutOfOrder;
        }
        
        public boolean getIsReset() {
            return this.isReset;
        }
        public void setIsReset(final boolean isReset) {
            this.isReset = isReset;
        }
        public boolean getIsOutOfOrder() {
            return this.isOutOfOrder;
        }
        public void setIsOutOfOrder(final boolean isOutOfOrder) {
            this.isOutOfOrder = isOutOfOrder;
        }
   
    }
    
}


