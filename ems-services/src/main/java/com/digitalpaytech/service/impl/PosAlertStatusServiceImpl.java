package com.digitalpaytech.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.AlertsCount;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("posAlertStatusService")
@Transactional(propagation = Propagation.SUPPORTS)
public class PosAlertStatusServiceImpl implements PosAlertStatusService {
    private static final String SQL_ALERTS_SUM = "SELECT COUNT(*) FROM POSEventCurrent apos"
                                                 + " INNER JOIN CustomerAlertType cat ON apos.CustomerAlertTypeId = cat.id"
                                                 + " INNER JOIN AlertThresholdType att ON cat.AlertThresholdTypeId = att.Id"
                                                 + " INNER JOIN AlertType at ON att.AlertTypeId = at.Id"
                                                 + " INNER JOIN AlertClassType act ON at.AlertClassTypeId = act.Id"
                                                 + " WHERE apos.IsActive = 1 AND apos.AlertGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)"
                                                 + " AND act.Id = ";
    
    private static final String SQL_ALERTS_SUM_PAYSTATION = "SELECT COUNT(*) FROM POSEventCurrent apos"
                                                            + " WHERE apos.IsActive = 1 AND apos.AlertGMT >= DATE_SUB(utc_timestamp(), interval 1 minute)"
                                                            + " AND apos.CustomerAlertTypeId IS NULL";
    
    private static final Logger LOGGER = Logger.getLogger(PosAlertStatusServiceImpl.class);
    
    private static final String GETTER = "Get";
    private static final String SETTER = "Set";
    private static final String COMMUNICATION_ID = "1_";
    private static final String COLLECTION_ID = "2_";
    private static final String PAYSTATIONS_ID = "3_";
    private static Map<String, String> POSALERTSTATUS_COLUMN_METHODS = new HashMap<String, String>();
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MessageHelper messageHelper;
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    @PostConstruct
    public final void init() {
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_MINOR, "getCommunicationMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_MAJOR, "getCommunicationMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_CRITICAL, "getCommunicationCritical");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_MINOR, "setCommunicationMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_MAJOR, "setCommunicationMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COMMUNICATION_ID + WebCoreConstants.SEVERITY_CRITICAL, "setCommunicationCritical");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_MINOR, "getCollectionMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_MAJOR, "getCollectionMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_CRITICAL, "getCollectionCritical");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_MINOR, "setCollectionMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_MAJOR, "setCollectionMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + COLLECTION_ID + WebCoreConstants.SEVERITY_CRITICAL, "setCollectionCritical");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_MINOR, "getPayStationMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_MAJOR, "getPayStationMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(GETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_CRITICAL, "getPayStationCritical");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_MINOR, "setPayStationMinor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_MAJOR, "setPayStationMajor");
        POSALERTSTATUS_COLUMN_METHODS.put(SETTER + PAYSTATIONS_ID + WebCoreConstants.SEVERITY_CRITICAL, "setPayStationCritical");
    }
    
    private AlertsCount recalculateAlertsCount(final Integer pointOfSaleId) {
        this.entityDao.flush();
        
        final AlertsCount result = new AlertsCount();
        
        @SuppressWarnings("unchecked")
        /////////////////
        final List<Object[]> buffer = (List<Object[]>) this.entityDao.getNamedQuery("PosEventCurrent.countActivePayStationAlertsByPointOfSaleId")
                .setCacheable(false).setParameter("pointOfSaleId", pointOfSaleId).list();
        for (Object[] alertCount : buffer) {
            switch (((Number) alertCount[0]).intValue()) {
                case WebCoreConstants.SEVERITY_MINOR:
                    result.setMinor(((Number) alertCount[1]).byteValue());
                    break;
                case WebCoreConstants.SEVERITY_MAJOR:
                    result.setMajor(((Number) alertCount[1]).byteValue());
                    break;
                case WebCoreConstants.SEVERITY_CRITICAL:
                    result.setCritical(((Number) alertCount[1]).byteValue());
                    break;
                default:
                    break;
            }
        }
        
        return result;
    }
    
    private void updatePayStationAlertsCount(final PosAlertStatus target, final AlertsCount updates) {
        target.setPayStationMinor(updates.getMinor());
        target.setPayStationMajor(updates.getMajor());
        target.setPayStationCritical(updates.getCritical());
    }
    
    @Override
    public final int getCommunicationAlertsSum() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_ALERTS_SUM + WebCoreConstants.ALERT_CLASS_TYPE_COMMUNICATION);
        final int ret = Integer.parseInt(q.list().get(0).toString());
        return ret;
    }
    
    @Override
    public final int getCollectionAlertsSum() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_ALERTS_SUM + WebCoreConstants.ALERT_CLASS_TYPE_COLLECTION);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public final int getPaystationAlertsSum() {
        final SQLQuery q = this.entityDao.createSQLQuery(SQL_ALERTS_SUM_PAYSTATION);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    public final Map<Integer, PosAlertStatusDetailSummary> findPosAlertDetailSummaryByPointOfSaleId(final int pointOfSaleId) {
        
        final PosAlertStatus posAlertStatus = (PosAlertStatus) this.entityDao
                .findUniqueByNamedQueryAndNamedParam("PosAlertStatus.findByPointOfSaleId", HibernateConstants.POINTOFSALE_ID, pointOfSaleId);
        
        final Map<Integer, PosAlertStatusDetailSummary> resultMap = new HashMap<Integer, PosAlertStatusDetailSummary>();
        final int totalCritical = posAlertStatus.getCollectionCritical() + posAlertStatus.getCommunicationCritical()
                                  + posAlertStatus.getPayStationCritical();
        final int totalMajor = posAlertStatus.getCollectionMajor() + posAlertStatus.getCommunicationMajor() + posAlertStatus.getPayStationMajor();
        // Don't add Minor Collection Alerts to count
        final int totalMinor = posAlertStatus.getCommunicationMinor() + posAlertStatus.getPayStationMinor();
        final PosAlertStatusDetailSummary summary = new PosAlertStatusDetailSummary();
        summary.setTotalCritical(totalCritical);
        summary.setTotalMajor(totalMajor);
        summary.setTotalMinor(totalMinor);
        
        summary.setSeverity(totalCritical > 0 ? WebCoreConstants.SEVERITY_CRITICAL : totalMajor > 0 ? WebCoreConstants.SEVERITY_MAJOR
                : totalMinor + posAlertStatus.getCollectionMinor() > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR);
        
        resultMap.put(pointOfSaleId, summary);
        
        return resultMap;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void updateAlertCount(final PosEventCurrent posEventCurrent, final int originalSeverity, final boolean isSeverityRecalc) {
        
        boolean requireRecalculation = false;
        PosAlertStatus posAlertStatus = this.entityDao
                .get(PosAlertStatus.class, posEventCurrent.getPointOfSale().getId(), LockMode.PESSIMISTIC_WRITE);
        posAlertStatus = (PosAlertStatus) this.entityDao.merge(posAlertStatus);
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("...Recalculates alert count for pos='" + posEventCurrent.getPointOfSale().getId() + "' type='"
                         + ((posEventCurrent.getCustomerAlertType() == null) ? "null" : posEventCurrent.getCustomerAlertType().getName())
                         + "', Device='" + this.messageHelper.getMessage(posEventCurrent.getEventType().getEventDeviceType().getName()) + "...");
        }
        
        String alertClassType = StandardConstants.STRING_EMPTY_STRING;
        if (posEventCurrent.getCustomerAlertType() == null) {
            alertClassType = PAYSTATIONS_ID;
        } else if (posEventCurrent.getCustomerAlertType().getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR) {
            alertClassType = COMMUNICATION_ID;
        } else {
            alertClassType = COLLECTION_ID;
        }
        
        String getterMethodName = StandardConstants.STRING_EMPTY_STRING;
        String setterMethodName = StandardConstants.STRING_EMPTY_STRING;
        byte newValue = 0;
        try {
            Method getterMethod = null;
            Method setterMethod = null;
            if (isSeverityRecalc || !posEventCurrent.isIsActive() && originalSeverity != WebCoreConstants.SEVERITY_CLEAR) {
                getterMethodName = POSALERTSTATUS_COLUMN_METHODS.get(GETTER + alertClassType + originalSeverity);
                setterMethodName = POSALERTSTATUS_COLUMN_METHODS.get(SETTER + alertClassType + originalSeverity);
                getterMethod = posAlertStatus.getClass().getDeclaredMethod(getterMethodName);
                newValue = (byte) ((Byte) getterMethod.invoke(posAlertStatus) - 1);
                requireRecalculation = newValue < 0;
                if (requireRecalculation) {
                    newValue = 0;
                }
                setterMethod = posAlertStatus.getClass().getDeclaredMethod(setterMethodName, Byte.TYPE);
                setterMethod.invoke(posAlertStatus, newValue);
                
            }
            if (posEventCurrent.isIsActive() && posEventCurrent.getEventSeverityType().getId() != WebCoreConstants.SEVERITY_CLEAR) {
                getterMethodName = POSALERTSTATUS_COLUMN_METHODS.get(GETTER + alertClassType + posEventCurrent.getEventSeverityType().getId());
                setterMethodName = POSALERTSTATUS_COLUMN_METHODS.get(SETTER + alertClassType + posEventCurrent.getEventSeverityType().getId());
                getterMethod = posAlertStatus.getClass().getDeclaredMethod(getterMethodName);
                newValue = (byte) ((Byte) getterMethod.invoke(posAlertStatus) + 1);
                setterMethod = posAlertStatus.getClass().getDeclaredMethod(setterMethodName, Byte.TYPE);
                setterMethod.invoke(posAlertStatus, newValue);
            }
        } catch (NoSuchMethodException nsme) {
            LOGGER.error(getterMethodName + StandardConstants.STRING_SLASH + setterMethodName
                         + " doesn't exist in CustomerMigrationValidationStatusServiceImpl.", nsme);
        } catch (IllegalAccessException iae) {
            LOGGER.error(getterMethodName + StandardConstants.STRING_SLASH + setterMethodName
                         + " could not be accessed in CustomerMigrationValidationStatusServiceImpl.", iae);
        } catch (InvocationTargetException ite) {
            LOGGER.error(getterMethodName + StandardConstants.STRING_SLASH + setterMethodName
                         + " could not be called in CustomerMigrationValidationStatusServiceImpl.", ite);
        }
        
        //TODO needs to be fixed
        if (requireRecalculation) {
            updatePayStationAlertsCount(posAlertStatus, recalculateAlertsCount(posAlertStatus.getPointOfSaleId()));
        } else {
            this.entityDao.update(posAlertStatus);
        }
        
    }
}
