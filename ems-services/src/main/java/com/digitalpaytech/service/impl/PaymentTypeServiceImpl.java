package com.digitalpaytech.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.PaymentTypeService;


@Service("paymentTypeService")
public class PaymentTypeServiceImpl implements PaymentTypeService {

	@Autowired
	private EntityService entityService;
	
	private List<PaymentType> paymentTypes;
	private Map<Byte, PaymentType> paymentTypesMap;
	
	
	@Override
	public List<PaymentType> loadAll() {
		if (paymentTypes == null) {
			paymentTypes = entityService.loadAll(PaymentType.class);
		}
		return paymentTypes;
	}

	@Override
	public Map<Byte, PaymentType> getPaymentTypesMap() {
		if (paymentTypes == null) {
			loadAll();
		}
		if (paymentTypesMap == null) {
			paymentTypesMap = new HashMap<Byte, PaymentType>(paymentTypes.size());
			Iterator<PaymentType> iter = paymentTypes.iterator();
			while (iter.hasNext()) {
				PaymentType paymentType = iter.next();
				paymentTypesMap.put(paymentType.getId(), paymentType);
			}
		}
		return paymentTypesMap;
	}

	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	@Override
	public String getText(int paymentId) {
		if (paymentTypesMap == null)
			getPaymentTypesMap();
		
		if (!paymentTypesMap.containsKey((byte)paymentId))
			return null;
		else
			return paymentTypesMap.get((byte)paymentId).getName();
	}
}
