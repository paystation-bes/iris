package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.UserDefinedEventInfo;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.service.UserDefinedEventInfoService;

@Service("userDefinedEventInfoService")
@Transactional(propagation = Propagation.SUPPORTS)
public class UserDefinedEventInfoServiceImpl implements UserDefinedEventInfoService {
    
    @Autowired
    private EntityDao entityDao;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @Override
    public final List<UserDefinedEventInfo> findEventsClassTypeAndLimit(final Class<? extends UserDefinedEventInfo> classType, final Integer limit) {
        return (List<UserDefinedEventInfo>) this.entityDao.loadWithLimit(classType, limit, false);
    }
    
    @Override
    public final List<UserDefinedEventInfo> findEventsByNamedQueryAndPointOfSaleIdList(final String queryName, final List<Integer> pointOfSaleIdList) {
        return this.entityDao.findByNamedQueryAndNamedParam(queryName, new String[] { HibernateConstants.POINTOFSALE_ID_LIST, },
                                                            new Object[] { pointOfSaleIdList, }, false);
    }
    
    @Override
    public final List<UserDefinedEventInfo> findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(final String queryName,
        final List<Integer> pointOfSaleIdList, final Integer customerAlertTypeId) {
        return this.entityDao.findByNamedQueryAndNamedParam(queryName, new String[] { HibernateConstants.POINTOFSALE_ID_LIST,
            HibernateConstants.CUSTOMER_ALERT_TYPE_ID, }, new Object[] { pointOfSaleIdList, customerAlertTypeId }, false);
    }
    
}
