package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoIRIS;
import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.domain.RawSensorDataArchive;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.RawSensorDataService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("rawSensorDataService")
@Transactional(propagation = Propagation.REQUIRED)
public class RawSensorDataServiceImpl implements RawSensorDataService {
    
    private static final int FIFTEEN_MINUTES_MS = 15 * 60 * 1000;
    
    private static final String SQL_RAWSENSORDATA_COUNT = "SELECT COUNT(*) FROM RawSensorData";
    
    private static final String PREFIX_BATTERY_1 = "1:";
    private static final String PREFIX_BATTERY_2 = "2:";
    
    @Autowired
    private EntityDaoIRIS entityDaoIRIS;

    @Autowired
    private ClusterMemberService clusterMemberService;
    
    public void setEntityDaoIRIS(EntityDaoIRIS entityDaoIRIS) {
        this.entityDaoIRIS = entityDaoIRIS;
    }

    public void setClusterMemberService(ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }

    @Override
    public void saveRawSensorData(final String serialNumber, final EventData eventData) {
        final RawSensorData rawSensorData = buildRawSensorData(serialNumber, eventData);
        this.entityDaoIRIS.save(rawSensorData);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public int getRawSensorDataCount() {
        final SQLQuery q = this.entityDaoIRIS.createSQLQuery(SQL_RAWSENSORDATA_COUNT);
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public RawSensorData getRawSensorData(final Long rawSensorDataId) {
        return this.entityDaoIRIS.get(RawSensorData.class, rawSensorDataId);
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<RawSensorData> findRawSensorDataForServer(final int sensorDataBatchSize) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis() - FIFTEEN_MINUTES_MS);
        final Date lastRetryTime = cal.getTime();
        
        final String[] params = { "serverName", "lastRetryTime" };
        final Object[] values = { this.clusterMemberService.getClusterName(), lastRetryTime };
        
        return this.entityDaoIRIS.findByNamedQuery("RawSensorData.findSensorForServer", params, values, 0, sensorDataBatchSize);
    }
    
    @Override
    public void deleteRawSensorData(final RawSensorData rawSensorData) {
        if (rawSensorDataExist(rawSensorData.getId())) {
            this.entityDaoIRIS.delete(rawSensorData);
        }
    }
    
    @Override
    public void updateRawSensorData(final RawSensorData rawSensorData) {
        this.entityDaoIRIS.update(rawSensorData);
    }
    
    @Override
    public void archiveRawSensorData(final RawSensorData rawSensorData) {
        final RawSensorDataArchive rawSensorDataArchive = new RawSensorDataArchive();
        rawSensorDataArchive.setServerName(rawSensorData.getServerName());
        rawSensorDataArchive.setSerialNumber(rawSensorData.getSerialNumber());
        rawSensorDataArchive.setType(rawSensorData.getType());
        rawSensorDataArchive.setAction(rawSensorData.getAction());
        rawSensorDataArchive.setInformation(rawSensorData.getInformation());
        rawSensorDataArchive.setDateField(rawSensorData.getDateField());
        rawSensorDataArchive.setLastRetryTime(rawSensorData.getLastRetryTime());
        rawSensorDataArchive.setRetryCount(rawSensorData.getRetryCount());
        
        this.entityDaoIRIS.save(rawSensorDataArchive);
        this.entityDaoIRIS.delete(rawSensorData);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public boolean rawSensorDataExist(final Long id) {
        if (id == null) {
            return false;
        }
        
        final Object obj = this.entityDaoIRIS.get(RawSensorData.class, id);
        if (obj != null) {
            this.entityDaoIRIS.evict(obj);
            return true;
        }
        return false;
    }
    

 
    private RawSensorData buildRawSensorData(final String serialNumber, final EventData eventData) {
        String info;
        String action = eventData.getAction();
        if (StringUtils.isBlank(eventData.getInformation())) {
            info = "NO INFORMATION";
        } else {
            info = eventData.getInformation();
        }
        if (WebCoreConstants.EVENT_TYPE_SENSOR.equals(eventData.getType())) {
            if (WebCoreConstants.EVENT_ACTION_VOLTAGE_STRING.equals(eventData.getAction())) {
                info = PREFIX_BATTERY_1 + info;
            } else if (WebCoreConstants.BATTERY_1_VOLTAGE.equals(eventData.getAction())) {
                info = PREFIX_BATTERY_1 + info;
                action = WebCoreConstants.EVENT_ACTION_VOLTAGE_STRING;
            } else if (WebCoreConstants.BATTERY_2_VOLTAGE.equals(eventData.getAction())) {
                info = PREFIX_BATTERY_2 + info;
                action = WebCoreConstants.EVENT_ACTION_VOLTAGE_STRING;
            } else if (WebCoreConstants.ACTION_BATTERY_VOLTAGE.equals(eventData.getAction())) {
                info = PREFIX_BATTERY_1 + info;
                action = WebCoreConstants.EVENT_ACTION_VOLTAGE_STRING;
            }
        }
        final RawSensorData rawSensorData = new RawSensorData();
        rawSensorData.setServerName(this.clusterMemberService.getClusterName());
        rawSensorData.setSerialNumber(serialNumber);
        rawSensorData.setType(eventData.getType());
        rawSensorData.setAction(action);
        rawSensorData.setInformation(info);
        rawSensorData.setDateField(eventData.getTimeStamp());
        rawSensorData.setLastRetryTime(new Date(1));
        rawSensorData.setRetryCount(0);
        return rawSensorData;
    }

}
