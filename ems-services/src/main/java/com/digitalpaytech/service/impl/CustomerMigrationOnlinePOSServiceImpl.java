package com.digitalpaytech.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDaoIRIS;
import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;
import com.digitalpaytech.service.CustomerMigrationOnlinePOSService;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
@Service("customerMigrationOnlinePOSService")
@Transactional(propagation = Propagation.REQUIRED)
public class CustomerMigrationOnlinePOSServiceImpl implements CustomerMigrationOnlinePOSService {
    
    @Autowired
    private EntityDaoIRIS entityDaoIRIS;
    
    public void setEntityDaoIRIS(EntityDaoIRIS entityDaoIRIS) {
        this.entityDaoIRIS = entityDaoIRIS;
    }

    @Override
    public void deleteCustomerMigrationOnlinePOSService(CustomerMigrationOnlinePOS customerMigrationOnlinePOS) {
        this.entityDaoIRIS.delete(customerMigrationOnlinePOS);
    }

    @Override
    public List<CustomerMigrationOnlinePOS> findOnlinePOSByCustomerId(Integer customerId) {
        return this.entityDaoIRIS.findByNamedQueryAndNamedParam("CustomerMigrationOnlinePOS.findAllByCustomerId", "customerId", customerId);
    }
    
}
