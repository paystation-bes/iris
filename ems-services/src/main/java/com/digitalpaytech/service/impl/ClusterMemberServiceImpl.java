package com.digitalpaytech.service.impl;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.OpenSessionDao;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.service.ClusterMemberService;

/*
 * Table name "ClusterMember" is changed to "Cluster" but since ClusterMemberService is used in many places, its name stays the same.
 */ 
@Component("clusterMemberService")
@Transactional(propagation=Propagation.SUPPORTS)
public class ClusterMemberServiceImpl implements ClusterMemberService {

	private static final Logger logger = Logger.getLogger(ClusterMemberServiceImpl.class);

	@Autowired
	private OpenSessionDao openSessionDao;
	
	@Autowired
	private EntityDao entityDao;
	
	private int clusterId;
	
	@Value("${cluster.member.name}")
	private String clusterName;
	
	@Value("${cluster.member.localPort}")
	private int clusterLocalPort;
	
	public void setOpenSessionDao(OpenSessionDao openSessionDao) {
		this.openSessionDao = openSessionDao;
	}
	
	public void setEntityDao(EntityDao entityDao) {
		this.entityDao = entityDao;
	}

	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}

	public void setClusterId(int clusterId)
	{
		this.clusterId = clusterId;
	}

	public void setClusterLocalPort(int clusterLocalPort)
	{
		this.clusterLocalPort = clusterLocalPort;
	}

	public String getClusterName()
	{
		return clusterName;
	}
	
	public int getClusterId()
	{
		return clusterId;
	}
	
	public boolean isClusterMember(String name)
	{
		return (getClusterMember(name) != null);
	}

	public boolean isClusterMemberHost(String host)
	{
		return (getClusterMemberByHostname(host) != null);
	}

	@PostConstruct
	private void initCluster() {
		// get the ip address for logging purpose
		String hostname = null;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (Exception e) {
			logger.error("Error getting hostname.", e);
		}

		// verify cluster name
		if (clusterName == null || clusterName.isEmpty()) {
			throw new IllegalStateException(
					"cluster.member.name in cluster.properties cannot be empty for cluster member "
							+ hostname);
		}

		// try to retrieve cluster member from database
		Cluster member = getCluster(clusterName);
		if (member == null) {
			// new server
			member = new Cluster();
			member.setName(clusterName);
			member.setHostname(hostname);
			member.setLocalPort(clusterLocalPort);
			member.setLastModifiedGmt(new Date());
			member.setLastModifiedByUserId(1);
			clusterId = ((Integer)createCluster(member)).intValue();
		} else {
			// update existing member with new host name and port
			member.setHostname(hostname);
			member.setLocalPort(clusterLocalPort);
			updateCluster(member);
			clusterId = member.getId();
		}
	}
	
	public Cluster getCluster(String name) {
		Collection<Cluster> cms = openSessionDao.findByNamedQueryAndNamedParam("Cluster.findByName", 
				new String[] { "name" }, new Object[] { name }, true);
		
		if (cms != null && !cms.isEmpty()) {
			return cms.iterator().next();
		}
		return null;
	}
	
	public Cluster getClusterMember(String name) {
		Collection<Cluster> cms = entityDao.findByNamedQueryAndNamedParam("Cluster.findByName", 
				new String[] { "name" }, new Object[] { name }, true);
		
		if (cms != null && !cms.isEmpty()) {
			return cms.iterator().next();
		}
		return null;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public Serializable createCluster(Cluster cluster) {
		return openSessionDao.save(cluster);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateCluster(Cluster cluster) {
		openSessionDao.update(cluster);
	}
	
	public Cluster getClusterMemberByHostname(String hostname) {
		Collection<Cluster> cms = openSessionDao.findByNamedQueryAndNamedParam(
				"Cluster.findByHostname", new String[] { "hostname" },
				new Object[] { hostname }, true);
		if (cms != null && !cms.isEmpty()) {
			return cms.iterator().next();
		}
		return null;
	}

	public Collection<Cluster> getAllClusterMembers() {
		return entityDao.loadAll(Cluster.class);
	}
}
