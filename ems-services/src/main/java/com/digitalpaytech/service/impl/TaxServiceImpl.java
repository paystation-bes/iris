package com.digitalpaytech.service.impl;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PurchaseTax;
import com.digitalpaytech.domain.Tax;
import com.digitalpaytech.service.TaxService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("taxService")
@Transactional(propagation=Propagation.SUPPORTS)
public class TaxServiceImpl implements TaxService {

    @Autowired
    private EntityDao entityDao;
    
    @Override
    public Tax findByCustomerIdAndName(Integer customerId, String name) {
        String[] params = { "customerId", "name" };
        Object[] values = { customerId, name };
        
        List<Tax> list = entityDao.findByNamedQueryAndNamedParam("Tax.findByCustomerIdAndName", params, values, true);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Tax findTaxById(Integer taxId) {
        List<Tax> taxList = entityDao.findByNamedQueryAndNamedParam("Tax.findTaxById", "taxId", taxId);
        if (taxList == null || taxList.isEmpty()) {
            return null;
        } else {
            return taxList.get(0);
        }
    }
}
