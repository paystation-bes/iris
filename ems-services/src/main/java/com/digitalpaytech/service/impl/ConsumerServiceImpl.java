package com.digitalpaytech.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.dto.customeradmin.ConsumerSearchCriteria;
import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.ServiceException;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ReflectionUtil;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("consumerService")
public class ConsumerServiceImpl implements ConsumerService {
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private MessageHelper msg;
    
    @Autowired
    protected CustomerCardService customerCardService;
    
    @Autowired
    protected CouponService couponService;
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<Consumer> findConsumerByCustomerId(final int customerId, final Integer pageNumber, final Integer itermsPerPage,
        final String column, final String order, final Long currentTime) {
        String namedQuery = "Consumer.findConsumerByCustomerIdOrderByLastNameAsc";
        if (column != null && column.length() != 0 && order != null && order.length() != 0) {
            if (order.toLowerCase().equals("desc")) {
                if (column.toLowerCase().equals("lastname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByLastNameDesc";
                } else if (column.toLowerCase().equals("firstname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByFirstNameDesc";
                } else {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByEmailAddressDesc";
                }
            } else {
                if (column.toLowerCase().equals("lastname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByLastNameAsc";
                } else if (column.toLowerCase().equals("firstname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByFirstNameAsc";
                } else {
                    namedQuery = "Consumer.findConsumerByCustomerIdOrderByEmailAddressAsc";
                }
            }
        }
        final Query query = this.entityDao.getNamedQuery(namedQuery);
        query.setInteger("customerId", customerId);
        final Date timeStamp = new Date(currentTime);
        query.setTimestamp("timeStamp", timeStamp);
        
        final Integer ipp = itermsPerPage != null ? itermsPerPage : WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
        if (pageNumber != null) {
            final int startRow = (pageNumber <= 1 ? 0 : (pageNumber - 1)) * ipp;
            query.setFirstResult(startRow);
        }
        query.setMaxResults(ipp);
        return query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Consumer findConsumerById(final int id) {
        return (Consumer) this.entityDao.get(Consumer.class, id);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateConsumer(final Consumer consumer, final String email, final Collection<Integer> couponIds, final Collection<Integer> cardIds) {
        consumer.setCoupons(null);
        final EmailAddress emailAddress = this.processEmail(consumer.getId(), email);
        consumer.setEmailAddress(emailAddress);
        this.entityDao.getNamedQuery("Coupon.detachConsumer").setParameter("consumerId", consumer.getId()).executeUpdate();
        
        if ((couponIds != null) && (!couponIds.isEmpty())) {
            final List<Coupon> coupons = this.couponService.findByCustomerIdAndCouponIds(consumer.getCustomer().getId(), couponIds);
            consumer.setCoupons(new HashSet<Coupon>(coupons.size() * 2));
            for (Coupon c : coupons) {
                consumer.getCoupons().add(c);
                if ((c.getConsumer() != null) && (!consumer.getId().equals(c.getConsumer().getId()))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.coupon",
                                                                           "label.customerAdmin.consumerAccount"));
                } else {
                    c.setConsumer(consumer);
                    this.entityDao.update(c);
                }
            }
        }
        
        consumer.setCustomerCards(null);
        this.entityDao.getNamedQuery("CustomerCard.updateConsumerId").setParameter("consumerId", consumer.getId()).executeUpdate();
        
        if ((cardIds != null) && (!cardIds.isEmpty())) {
            final List<CustomerCard> cards = this.customerCardService.findByCustomerIdAndCardIds(consumer.getCustomer().getId(), cardIds);
            consumer.setCustomerCards(new HashSet<CustomerCard>(cards.size() * 2));
            for (CustomerCard c : cards) {
                consumer.getCustomerCards().add(c);
                if ((c.getConsumer() != null) && (!consumer.getId().equals(c.getConsumer().getId()))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.customerCard",
                                                                           "label.customerAdmin.consumerAccount"));
                } else {
                    c.setConsumer(consumer);
                    this.entityDao.update(c);
                }
            }
        }
        
        // Deleting the email is done as a separate transaction because there is a database
        // constraint preventing the deletion of emailAddresses that have a consumer
        // attached to them.
        //EmailAddress emailAddress = consumer.getEmailAddress();
        //boolean deleteEmail = this.processEmail(consumer, email);
        //        if(deleteEmail){
        //            consumer.setEmailAddress(null);
        //            this.entityDao.update(consumer);
        //            this.entityDao.flush();
        //            this.deleteEmail(emailAddress.getId(), consumer.getId());
        //        }        
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private EmailAddress processEmail(final Integer consumerId, final String email) {
        final Consumer consumer = this.findConsumerById(consumerId);
        final EmailAddress emailAddress = consumer.getEmailAddress();
        //boolean deleteEmail = false;
        if (consumer.getEmailAddress() != null) {
            if (StringUtils.isEmpty(email)) {
                consumer.setEmailAddress(null);
                this.entityDao.update(consumer);
                this.entityDao.flush();
                this.deleteEmail(emailAddress.getId(), consumer.getId());
                return null;
            } else {
                if (consumer.getEmailAddress().getId() == null) {
                    this.entityDao.save(consumer.getEmailAddress());
                    return consumer.getEmailAddress();
                } else {
                    // We are sending the email address string up separately because with the way
                    // we had to use 2 transactions for delete, shared emails were getting their 
                    // "email" field set to blank when the email string was empty.
                    emailAddress.setEmail(email);
                    this.entityDao.update(emailAddress);
                }
                //this.entityDao.flush();
            }
        }
        
        return emailAddress;
    }
    
    @Transactional
    @Override
    public void createConsumer(final Consumer consumer) throws ServiceException {
        if (consumer.getEmailAddress() != null) {
            this.entityDao.save(consumer.getEmailAddress());
        }
        this.entityDao.save(consumer);
        this.updateCouponsAndCards(consumer);
        this.entityDao.saveOrUpdateAll(consumer.getCoupons());
        this.entityDao.saveOrUpdateAll(consumer.getCustomerCards());
    }
    
    private void updateCouponsAndCards(final Consumer consumer) {
        if (consumer.getCoupons() != null) {
            for (Coupon coupon : consumer.getCoupons()) {
                if ((coupon.getConsumer() != null) && (!consumer.getId().equals(coupon.getConsumer().getId()))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.coupon",
                                                                           "label.customerAdmin.consumerAccount"));
                }
                
                coupon.setConsumer(consumer);
            }
        }
        if (consumer.getCustomerCards() != null) {
            for (CustomerCard cCard : consumer.getCustomerCards()) {
                if ((cCard.getConsumer() != null) && (!consumer.getId().equals(cCard.getConsumer().getId()))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.customerCard",
                                                                           "label.customerAdmin.consumerAccount"));
                }
                
                cCard.setConsumer(consumer);
            }
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public Collection<Consumer> findConsumerByCustomerIdAndFilterValue(final int customerId, final String filterValue, final Integer pageNumber,
        final Integer itermsPerPage, final String column, final String order, final Long currentTime) {
        final Integer ipp = itermsPerPage != null ? itermsPerPage : WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT;
        final int startRow = (pageNumber == null || pageNumber <= 1 ? 0 : pageNumber - 1) * ipp;
        final String fValUpper = filterValue.replaceAll("\\s", "").toUpperCase();
        final String[] names = { "customerId", "firstAndLast", "emailAddress", "timeStamp" };
        //String names [] = {"customerId", "filterValue", "timeStamp"};
        final Date timeStamp = new Date(currentTime);
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(ReportingConstants.MYSQL_MULTIPLE_WILD_CARD).append(fValUpper).append(ReportingConstants.MYSQL_MULTIPLE_WILD_CARD);
        final Object[] values = { customerId, bdr.toString(), bdr.toString(), timeStamp };
        // Object values [] = {customerId, '%' + filterValue + '%', timeStamp};
        String namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByLastNameAsc";
        if (column != null && column.length() != 0 && order != null && order.length() != 0) {
            if (order.toLowerCase().equals("desc")) {
                if (column.toLowerCase().equals("lastname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByLastNameDesc";
                } else if (column.toLowerCase().equals("firstname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByFirstNameDesc";
                } else {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByEmailAddressDesc";
                }
            } else {
                if (column.toLowerCase().equals("lastname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByLastNameAsc";
                } else if (column.toLowerCase().equals("firstname")) {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByFirstNameAsc";
                } else {
                    namedQuery = "Consumer.findConsumerByCustomerIdAndFilterValueOrderByEmailAddressAsc";
                }
            }
        }
        
        return this.entityDao.findByNamedQuery(namedQuery, names, values, startRow, ipp);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Consumer> findConsumerByCriteria(final ConsumerSearchCriteria criteria) {
        final Criteria query = createSearchQuery(criteria);
        if ((criteria.getPage() != null) && (criteria.getItemsPerPage() != null)) {
            query.setFirstResult((criteria.getPage() - 1) * criteria.getItemsPerPage());
            query.setMaxResults(criteria.getItemsPerPage());
        }
        
        return query.list();
    }
    
    @Override
    public int findRecordPage(final Integer consumerId, final ConsumerSearchCriteria criteria) {
        int result = -1;
        
        criteria.setEndConsumerId(consumerId);
        final Criteria query = createSearchQuery(criteria);
        criteria.setEndConsumerId((Integer) null);
        
        query.setProjection(Projections.count("id"));
        
        final double recordNumber = ((Number) query.uniqueResult()).doubleValue();
        if ((recordNumber > 0) && (criteria.getItemsPerPage() != null)) {
            result = (int) Math.ceil(recordNumber / criteria.getItemsPerPage().doubleValue());
        }
        
        return result;
    }
    
    private Criteria createSearchQuery(final ConsumerSearchCriteria criteria) {
        Consumer endConsumer = null;
        if (criteria.getEndConsumerId() != null) {
            endConsumer = this.entityDao.get(Consumer.class, criteria.getEndConsumerId());
        }
        
        final Criteria query = this.entityDao.createCriteria(Consumer.class).setCacheable(criteria.getMaxUpdatedTime() == null);
        
        query.createAlias("emailAddress", "emailAddr", Criteria.LEFT_JOIN);
        
        query.add(Restrictions.eq("customer.id", criteria.getCustomerId()));
        
        Criterion identitiesConditions = null;
        if (criteria.getFirstName() != null) {
            final Criterion condition = Restrictions.ilike("firstName", criteria.getFirstName());
            identitiesConditions = (identitiesConditions == null) ? condition : Restrictions.or(identitiesConditions, condition);
        }
        if (criteria.getLastName() != null) {
            final Criterion condition = Restrictions.ilike("lastName", criteria.getFirstName());
            identitiesConditions = (identitiesConditions == null) ? condition : Restrictions.or(identitiesConditions, condition);
        }
        if (criteria.getFullName() != null) {
            final Criterion condition = Restrictions.ilike("fullName", criteria.getFullName());
            identitiesConditions = (identitiesConditions == null) ? condition : Restrictions.or(identitiesConditions, condition);
        }
        if (criteria.getEmailAddress() != null) {
            final Criterion condition = Restrictions.ilike("emailAddr.email", criteria.getEmailAddress());
            identitiesConditions = (identitiesConditions == null) ? condition : Restrictions.or(identitiesConditions, condition);
        }
        
        if (identitiesConditions != null) {
            query.add(identitiesConditions);
        }
        
        Criterion endConsumerRes = null;
        Criterion prevEndConsumerEq = null;
        if ((criteria.getOrderColumn() != null) && (criteria.getOrderColumn().length > 0)) {
            for (int i = -1; ++i < criteria.getOrderColumn().length;) {
                final String orderCol = criteria.getOrderColumn()[i];
                if (criteria.isOrderDesc()) {
                    query.addOrder(Property.forName(orderCol).desc());
                } else {
                    query.addOrder(Property.forName(orderCol).asc());
                }
                
                if (endConsumer != null) {
                    final Object endConsumerVal = ReflectionUtil.getAttribute(endConsumer, orderCol);
                    
                    endConsumerRes = this.entityDao.nullableOr(endConsumerRes, this.entityDao.nullableAnd(prevEndConsumerEq, (criteria.isOrderDesc())
                            ? Restrictions.gt(orderCol, endConsumerVal) : Restrictions.lt(orderCol, endConsumerVal)));
                    prevEndConsumerEq = this.entityDao.nullableAnd(prevEndConsumerEq, Restrictions.eq(orderCol, endConsumerVal));
                }
            }
        }
        
        query.addOrder(Property.forName("id").asc());
        if (endConsumer != null) {
            endConsumerRes = this.entityDao.nullableOr(endConsumerRes,
                                                       this.entityDao.nullableAnd(prevEndConsumerEq, Restrictions.le("id", endConsumer.getId())));
            query.add(endConsumerRes);
        }
        
        if (criteria.getMaxUpdatedTime() != null) {
            query.add(Restrictions.le("lastModifiedGmt", criteria.getMaxUpdatedTime()));
        }
        
        return query;
    }
    
    @Override
    @Transactional
    public boolean deleteConsumer(final Consumer consumer) {
        final Object[] values = { consumer.getId() };
        final String[] params = { "consumerId" };
        this.entityDao.updateByNamedQuery("CustomerCard.updateConsumerId", params, values);
        this.entityDao.updateByNamedQuery("Coupon.detachConsumer", params, values);
        this.entityDao.delete(consumer);
        if (consumer.getEmailAddress() != null) {
            this.deleteEmail(consumer.getEmailAddress().getId(), consumer.getId());
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    private void deleteEmail(final Integer emailAddressId, final Integer consumerId) {
        final String[] emNames = { "emailAddressId", "consumerId" };
        final Object[] emValues = { emailAddressId, consumerId };
        final List<Consumer> consumerByEmailList = (List<Consumer>) this.entityDao
                .findByNamedQueryAndNamedParam("Consumer.findConsumerByEmailAddressId", emNames, emValues);
        if (consumerByEmailList == null || consumerByEmailList.isEmpty()) {
            //entityDao.delete(consumer.getEmailAddress());
            final Object[] values = { emailAddressId };
            // using this delete method because a straight object delete doesn't work when the EmaildAddress.email field is blank
            this.entityDao.deleteAllByNamedQuery("EmailAddress.deleteById", values);
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public Date findMaxLastModifiedGmtByCustomerId(final int customerId) {
        final List<Date> dates = this.entityDao
                .findByNamedQueryAndNamedParam("Consumer.findMaxLastModifiedGmtByCustomerId", "customerId", customerId);
        if (dates != null && dates.size() > 0) {
            return dates.get(0);
        }
        return null;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Consumer> findOtherConsumerByCustomerIdAndEmailId(final int consumerId, final int customerId, final int emailAddressId) {
        final String[] names = { "consumerId", "customerId", "emailAddressId" };
        final Object[] values = { consumerId, customerId, emailAddressId };
        final List<Consumer> consumerList = (List<Consumer>) this.entityDao
                .findByNamedQueryAndNamedParam("Consumer.findOtherConsumerByCustomerIdAndEmailId", names, values);
        return consumerList;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<EmailAddress> findEmailAddressByEmail(final String email) {
        final String emailAddress = email != null ? email.toUpperCase() : "";
        final List<EmailAddress> emailAddressList = (List<EmailAddress>) this.entityDao.findByNamedQueryAndNamedParam("EmailAddress.findByEmail",
                                                                                                                      "email", emailAddress);
        if (emailAddressList == null || emailAddressList.isEmpty()) {
            return null;
        }
        return emailAddressList;
    }
    
    /**
     * The purpose of this method is to keep save/update of Consumer table ACID.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveConsumer(final Consumer consumerDTO, final Collection<Integer> couponIds, final Collection<Integer> customerCardIds)
        throws ServiceException {
        final boolean isNew = consumerDTO.getId() == null;
        
        if ((consumerDTO.getEmailAddress() != null) && (consumerDTO.getEmailAddress().getEmail() != null)) {
            final EmailAddress addr = (EmailAddress) this.entityDao.getNamedQuery("EmailAddress.findByEmail")
                    .setParameter("email", consumerDTO.getEmailAddress().getEmail().toUpperCase()).uniqueResult();
            if (addr == null) {
                this.entityDao.save(consumerDTO.getEmailAddress());
            } else {
                consumerDTO.setEmailAddress(addr);
            }
        }
        
        Consumer consumer;
        if (isNew) {
            consumer = consumerDTO;
            
            this.entityDao.save(consumer);
        } else {
            consumer = this.entityDao.get(Consumer.class, consumerDTO.getId());
            if (consumer == null) {
                throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.nonexisting", "label.customerAdmin.consumerAccount"));
            }
            
            consumer.setFirstName(consumerDTO.getFirstName());
            consumer.setLastName(consumerDTO.getLastName());
            consumer.setLastModifiedByUserId(consumerDTO.getLastModifiedByUserId());
            consumer.setLastModifiedGmt(consumerDTO.getLastModifiedGmt());
            consumer.setEmailAddress(consumerDTO.getEmailAddress());
            
            this.entityDao.getNamedQuery("Coupon.detachConsumer").setParameter("consumerId", consumer.getId()).executeUpdate();
            
            this.entityDao.getNamedQuery("CustomerCard.updateConsumerId").setParameter("consumerId", consumer.getId()).executeUpdate();
            
            this.entityDao.update(consumer);
        }
        
        consumer.setCoupons(null);
        if ((couponIds != null) && (!couponIds.isEmpty())) {
            final List<Coupon> coupons = this.couponService.findByCustomerIdAndCouponIds(consumer.getCustomer().getId(), couponIds);
            consumer.setCoupons(new HashSet<Coupon>(coupons.size() * 2));
            for (Coupon c : coupons) {
                consumer.getCoupons().add(c);
                if ((c.getConsumer() != null) && (isNew || (!consumer.getId().equals(c.getConsumer().getId())))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.coupon",
                                                                           "label.customerAdmin.consumerAccount"));
                } else {
                    c.setConsumer(consumer);
                    this.entityDao.update(c);
                }
            }
        }
        
        consumer.setCustomerCards(null);
        if ((customerCardIds != null) && (!customerCardIds.isEmpty())) {
            final List<CustomerCard> cards = this.customerCardService.findByCustomerIdAndCardIds(consumer.getCustomer().getId(), customerCardIds);
            consumer.setCustomerCards(new HashSet<CustomerCard>(cards.size() * 2));
            for (CustomerCard c : cards) {
                consumer.getCustomerCards().add(c);
                if ((c.getConsumer() != null) && (isNew || (!consumer.getId().equals(c.getConsumer().getId())))) {
                    throw new ServiceException(this.msg.getMessageWithKeyParams("error.common.duplicated.assignment", "label.customerCard",
                                                                           "label.customerAdmin.consumerAccount"));
                } else {
                    c.setConsumer(consumer);
                    this.entityDao.update(c);
                }
            }
        }
    }
    
    @Override
    public void saveConsumer(final Consumer consumer) {
        this.entityDao.save(consumer);
    }
    
    @Override
    public List<Consumer> findConsumersByFirstLastNameOrEmail(final int customerId, final String firstLastNameOrEmail) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(ReportingConstants.MYSQL_MULTIPLE_WILD_CARD).append(firstLastNameOrEmail.toUpperCase())
                .append(ReportingConstants.MYSQL_MULTIPLE_WILD_CARD);
        
        final String[] params = { "customerId", "firstLastName" };
        final Object[] values = new Object[] { customerId, bdr.toString() };
        return this.entityDao.findByNamedQueryAndNamedParam("Consumer.findConsumerByFirstLastNameOrEmail", params, values);
    }
    
}
